package com.helpshift.support.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.CoreApi;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.FetchDataFromThread;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.support.ContactUsFilter;
import com.helpshift.support.HSSearch;
import com.helpshift.support.activities.ParentActivity;
import com.helpshift.support.compositions.FaqFragment;
import com.helpshift.support.compositions.SectionPagerFragment;
import com.helpshift.support.contracts.SupportScreenView;
import com.helpshift.support.controllers.FaqFlowController;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.conversations.AuthenticationFailureFragment;
import com.helpshift.support.conversations.BaseConversationFragment;
import com.helpshift.support.conversations.ConversationFragment;
import com.helpshift.support.conversations.ConversationalFragment;
import com.helpshift.support.conversations.NewConversationFragment;
import com.helpshift.support.conversations.usersetup.UserSetupFragment;
import com.helpshift.support.fragments.ScreenshotPreviewFragment;
import com.helpshift.support.util.FragmentUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.support.widget.ImagePicker;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.Styles;
import com.helpshift.views.HSMenuItemCompat;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class SupportFragment extends MainFragment implements View.OnClickListener, SupportScreenView, FetchDataFromThread<Integer, Integer>, ImagePicker.ImagePickerListener, MenuItem.OnMenuItemClickListener, IToolbarMenuItemRenderer {
    public static final String SUPPORT_MODE = "support_mode";
    private static final String TAG = "Helpshift_SupportFrag";
    private MenuItem attachImageMenuItem;
    MenuItem contactUsMenuItem;
    private MenuItem doneMenuItem;
    private boolean faqLoaded;
    private List<Integer> fragmentMenuItems;
    private boolean handleNewIntent;
    private ImagePicker imagePicker;
    private boolean isForeground;
    private WeakReference<IMenuItemEventListener> menuItemEventListener;
    private boolean menuItemsPrepared;
    private Bundle newIntentData;
    private int newMessageCount = 0;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private MenuItem startNewConversationMenuItem;
    private SupportController supportController;
    private Toolbar toolbar;
    private int toolbarId;
    private int toolbarImportanceForAccessibility;
    private View viewFaqsLoadError;
    private View viewFaqsLoading;
    private View viewNoFaqs;
    private final List<String> visibleFragments = Collections.synchronizedList(new ArrayList());

    public static class SupportModes {
        public static final int CONVERSATION = 1;
        public static final int DYNAMIC_FORM = 4;
        public static final int FAQ_SECTION = 2;
        public static final int SINGLE_QUESTION = 3;
    }

    public void onFailure(Integer num) {
    }

    public boolean shouldRefreshMenu() {
        return false;
    }

    public static SupportFragment newInstance(Bundle bundle) {
        SupportFragment supportFragment = new SupportFragment();
        supportFragment.setArguments(bundle);
        return supportFragment;
    }

    public SupportController getSupportController() {
        return this.supportController;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        HelpshiftContext.getPlatform().setUIContext(getContext());
        setRetainInstance(true);
        if (this.supportController == null) {
            this.supportController = new SupportController(HelpshiftContext.getApplicationContext(), this, getRetainedChildFragmentManager(), getArguments());
        } else {
            this.supportController.onFragmentManagerUpdate(getRetainedChildFragmentManager());
        }
        if (!isChangingConfigurations()) {
            HelpshiftContext.getCoreApi().getConversationInboxPoller().startAppPoller(true);
        }
    }

    public void onStart() {
        AnalyticsEventType analyticsEventType;
        super.onStart();
        if (getArguments() == null) {
            quitSupportFragment();
            return;
        }
        if (!isChangingConfigurations()) {
            HSLogger.d(TAG, "Helpshift session began.");
            HSSearch.init();
            if (getArguments().getInt(SUPPORT_MODE, 0) == 0) {
                analyticsEventType = AnalyticsEventType.LIBRARY_OPENED;
            } else {
                analyticsEventType = AnalyticsEventType.LIBRARY_OPENED_DECOMP;
            }
            HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(analyticsEventType);
            if (this.handleNewIntent) {
                this.supportController.onNewIntent(this.newIntentData);
                this.handleNewIntent = false;
            }
            HelpshiftContext.getCoreApi().onSDKSessionStarted();
        }
        this.isForeground = true;
    }

    public void onPause() {
        if (!getActivity(this).isChangingConfigurations()) {
            stopLiveUpdates();
        }
        super.onPause();
    }

    public void onStop() {
        if (!isChangingConfigurations()) {
            HSLogger.d(TAG, "Helpshift session ended.");
            CoreApi coreApi = HelpshiftContext.getCoreApi();
            HSSearch.deinit();
            coreApi.getAnalyticsEventDM().pushEvent(AnalyticsEventType.LIBRARY_QUIT);
            this.isForeground = false;
            coreApi.sendAnalyticsEvent();
            coreApi.onSDKSessionEnded();
        }
        HelpshiftContext.getCoreApi().getConversationController().fetchConversationUpdatesListenerReference = null;
        super.onStop();
    }

    private int getMenuResourceId() {
        return R.menu.hs__support_fragment;
    }

    private void attachMenuListeners(Menu menu) {
        this.searchMenuItem = menu.findItem(R.id.hs__search);
        this.searchView = (SearchView) HSMenuItemCompat.getActionView(this.searchMenuItem);
        this.contactUsMenuItem = menu.findItem(R.id.hs__contact_us);
        this.contactUsMenuItem.setTitle(R.string.hs__contact_us_btn);
        this.contactUsMenuItem.setOnMenuItemClickListener(this);
        HSMenuItemCompat.getActionView(this.contactUsMenuItem).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SupportFragment.this.onMenuItemClick(SupportFragment.this.contactUsMenuItem);
            }
        });
        this.doneMenuItem = menu.findItem(R.id.hs__action_done);
        this.doneMenuItem.setOnMenuItemClickListener(this);
        this.startNewConversationMenuItem = menu.findItem(R.id.hs__start_new_conversation);
        this.startNewConversationMenuItem.setOnMenuItemClickListener(this);
        this.attachImageMenuItem = menu.findItem(R.id.hs__attach_screenshot);
        this.attachImageMenuItem.setOnMenuItemClickListener(this);
        this.menuItemsPrepared = true;
        setSearchListeners(null);
        refreshMenu();
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.hs__contact_us) {
            this.supportController.onContactUsClicked(null);
            return true;
        } else if (itemId == R.id.hs__action_done) {
            this.supportController.actionDone();
            return true;
        } else if (itemId == R.id.hs__start_new_conversation) {
            sendMenuEventClickEvent(HSMenuItemType.START_NEW_CONVERSATION);
            return true;
        } else if (itemId != R.id.hs__attach_screenshot) {
            return false;
        } else {
            sendMenuEventClickEvent(HSMenuItemType.SCREENSHOT_ATTACHMENT);
            return true;
        }
    }

    private void sendMenuEventClickEvent(HSMenuItemType hSMenuItemType) {
        if (this.menuItemEventListener != null && this.menuItemEventListener.get() != null) {
            this.menuItemEventListener.get().onMenuItemClicked(hSMenuItemType);
        }
    }

    public void unRegisterSearchListener() {
        if (this.menuItemsPrepared) {
            HSMenuItemCompat.setOnActionExpandListener(this.searchMenuItem, null);
            this.searchView.setOnQueryTextListener(null);
        }
    }

    public void setSearchListeners(FaqFlowController faqFlowController) {
        FaqFlowFragment faqFlowFragment;
        if (this.menuItemsPrepared) {
            if (faqFlowController == null && (faqFlowFragment = FragmentUtil.getFaqFlowFragment(getRetainedChildFragmentManager())) != null) {
                faqFlowController = faqFlowFragment.getFaqFlowController();
            }
            if (faqFlowController != null) {
                HSMenuItemCompat.setOnActionExpandListener(this.searchMenuItem, faqFlowController);
                this.searchView.setOnQueryTextListener(faqFlowController);
            }
        }
    }

    private void setMenuItemColors() {
        Context context = getContext();
        Styles.setActionButtonIconColor(context, this.searchMenuItem.getIcon());
        Styles.setActionButtonIconColor(context, this.contactUsMenuItem.getIcon());
        Styles.setActionButtonIconColor(context, ((TextView) HSMenuItemCompat.getActionView(this.contactUsMenuItem).findViewById(R.id.hs__notification_badge)).getBackground());
        Styles.setActionButtonIconColor(context, this.doneMenuItem.getIcon());
        Styles.setActionButtonIconColor(context, this.startNewConversationMenuItem.getIcon());
        Styles.setActionButtonIconColor(context, this.attachImageMenuItem.getIcon());
    }

    private void hideAllMenuItems() {
        this.searchMenuItem.setVisible(false);
        this.contactUsMenuItem.setVisible(false);
        this.doneMenuItem.setVisible(false);
        this.startNewConversationMenuItem.setVisible(false);
        this.attachImageMenuItem.setVisible(false);
    }

    public void addVisibleFragment(String str) {
        this.visibleFragments.add(str);
        refreshMenu();
    }

    public void removeVisibleFragment(String str) {
        this.visibleFragments.remove(str);
    }

    public void onFaqsLoaded() {
        this.faqLoaded = true;
        if (!this.menuItemsPrepared) {
            return;
        }
        if (this.visibleFragments.contains(FaqFragment.class.getName()) || this.visibleFragments.contains(QuestionListFragment.class.getName())) {
            setSearchMenuVisible(true);
        }
    }

    public void refreshMenu() {
        if (this.menuItemsPrepared) {
            hideAllMenuItems();
            setMenuItemColors();
            synchronized (this.visibleFragments) {
                for (String next : this.visibleFragments) {
                    if (next.equals(FaqFragment.class.getName())) {
                        showFaqFragmentMenu();
                    } else if (next.equals(SearchFragment.class.getName())) {
                        restoreSearchMenuItem();
                    } else {
                        if (next.equals(SingleQuestionFragment.class.getName() + 1)) {
                            showSingleQuestionFragmentMenu();
                        } else if (next.equals(SectionPagerFragment.class.getName())) {
                            showSectionPagerFragmentMenu();
                        } else if (next.equals(QuestionListFragment.class.getName())) {
                            showQuestionListFragmentMenu();
                        } else {
                            if (!next.equals(NewConversationFragment.class.getName()) && !next.equals(ConversationFragment.class.getName())) {
                                if (!next.equals(ConversationalFragment.class.getName())) {
                                    if (next.equals(SingleQuestionFragment.class.getName() + 2)) {
                                        restoreSingleQuestionDoneModeFragmentMenu();
                                    } else if (next.equals(DynamicFormFragment.class.getName())) {
                                        showDynamicFormFragmentMenu();
                                    } else if (next.equals(UserSetupFragment.class.getName()) || next.equals(AuthenticationFailureFragment.class.getName())) {
                                        setRetainSearchFragmentState(true);
                                        setSearchMenuVisible(false);
                                        setContactUsMenuVisible(false);
                                    }
                                }
                            }
                            restoreConversationFragmentMenu();
                        }
                    }
                }
            }
        }
    }

    private void restoreSingleQuestionDoneModeFragmentMenu() {
        this.doneMenuItem.setVisible(true);
    }

    private void restoreConversationFragmentMenu() {
        setRetainSearchFragmentState(true);
        setSearchMenuVisible(false);
        setContactUsMenuVisible(false);
        BaseConversationFragment baseConversationFragment = (BaseConversationFragment) getRetainedChildFragmentManager().findFragmentByTag(NewConversationFragment.FRAGMENT_TAG);
        if (baseConversationFragment == null) {
            baseConversationFragment = (BaseConversationFragment) getRetainedChildFragmentManager().findFragmentByTag(ConversationFragment.FRAGMENT_TAG);
        }
        if (baseConversationFragment != null) {
            this.doneMenuItem.setVisible(false);
        }
    }

    private void showQuestionListFragmentMenu() {
        setSearchMenuVisible(this.faqLoaded);
        setContactUsMenuVisible(ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.ACTION_BAR));
    }

    private void showSectionPagerFragmentMenu() {
        setSearchMenuVisible(true);
        setContactUsMenuVisible(ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.ACTION_BAR));
    }

    private void showSingleQuestionFragmentMenu() {
        if (!isScreenLarge()) {
            setRetainSearchFragmentState(true);
            setSearchMenuVisible(false);
        }
        setContactUsMenuVisible(ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.QUESTION_ACTION_BAR));
    }

    private void setRetainSearchFragmentState(boolean z) {
        FaqFlowFragment faqFlowFragment = (FaqFlowFragment) getRetainedChildFragmentManager().findFragmentByTag(FaqFlowFragment.FRAGMENT_TAG);
        if (faqFlowFragment != null && faqFlowFragment.getFaqFlowController() != null) {
            faqFlowFragment.getFaqFlowController().setRetainSearchFragmentState(z);
        }
    }

    private void restoreSearchMenuItem() {
        SearchFragment searchFragment;
        FaqFlowFragment faqFlowFragment = FragmentUtil.getFaqFlowFragment(getRetainedChildFragmentManager());
        if (!(faqFlowFragment == null || (searchFragment = FragmentUtil.getSearchFragment(faqFlowFragment.getRetainedChildFragmentManager())) == null)) {
            setSearchMenuQuery(searchFragment.getCurrentQuery());
        }
        setContactUsMenuVisible(ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.ACTION_BAR));
        setRetainSearchFragmentState(false);
    }

    private void showFaqFragmentMenu() {
        setSearchMenuVisible(this.faqLoaded);
        setContactUsMenuVisible(ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.ACTION_BAR));
    }

    private void showDynamicFormFragmentMenu() {
        setRetainSearchFragmentState(true);
        setContactUsMenuVisible(false);
        setSearchMenuVisible(false);
    }

    public void setContactUsMenuVisible(boolean z) {
        if (HSMenuItemCompat.isActionViewExpanded(this.searchMenuItem)) {
            this.contactUsMenuItem.setVisible(false);
        } else {
            this.contactUsMenuItem.setVisible(z);
        }
        updateBadgeIcon();
    }

    public void setSearchMenuVisible(boolean z) {
        if (HSMenuItemCompat.isActionViewExpanded(this.searchMenuItem) && !this.visibleFragments.contains(SearchFragment.class.getName())) {
            HSMenuItemCompat.collapseActionView(this.searchMenuItem);
        }
        this.searchMenuItem.setVisible(z);
    }

    public void setSearchMenuQuery(String str) {
        if (!HSMenuItemCompat.isActionViewExpanded(this.searchMenuItem)) {
            HSMenuItemCompat.expandActionView(this.searchMenuItem);
        }
        if (!TextUtils.isEmpty(str)) {
            this.searchView.setQuery(str, false);
        }
    }

    private void updateBadgeIcon() {
        View actionView;
        if (this.contactUsMenuItem != null && this.contactUsMenuItem.isVisible() && (actionView = HSMenuItemCompat.getActionView(this.contactUsMenuItem)) != null) {
            TextView textView = (TextView) actionView.findViewById(R.id.hs__notification_badge);
            View findViewById = actionView.findViewById(R.id.hs__notification_badge_padding);
            if (this.newMessageCount != 0) {
                textView.setText(String.valueOf(this.newMessageCount));
                findViewById.setVisibility(8);
                textView.setVisibility(0);
                return;
            }
            textView.setVisibility(8);
            findViewById.setVisibility(0);
        }
    }

    public void resetNewMessageCount() {
        updateMessageBatchCount(0);
    }

    public void updateFaqLoadingUI(int i) {
        this.viewNoFaqs.setVisibility(8);
        this.viewFaqsLoading.setVisibility(8);
        this.viewFaqsLoadError.setVisibility(8);
        switch (i) {
            case 0:
                this.viewFaqsLoading.setVisibility(0);
                return;
            case 1:
            default:
                return;
            case 2:
                this.viewNoFaqs.setVisibility(0);
                return;
            case 3:
                this.viewFaqsLoadError.setVisibility(0);
                return;
        }
    }

    private void quitSupportFragment() {
        Activity activity = getActivity(this);
        if (activity instanceof ParentActivity) {
            activity.finish();
        } else {
            ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    private void startLiveUpdates() {
        ConversationFragment conversationFragment = (ConversationFragment) getRetainedChildFragmentManager().findFragmentByTag(ConversationFragment.FRAGMENT_TAG);
        if (conversationFragment != null) {
            conversationFragment.startLiveUpdates();
        }
    }

    private void stopLiveUpdates() {
        ConversationFragment conversationFragment = (ConversationFragment) getRetainedChildFragmentManager().findFragmentByTag(ConversationFragment.FRAGMENT_TAG);
        if (conversationFragment != null) {
            conversationFragment.stopLiveUpdates();
        }
    }

    public void onFocusChanged(boolean z) {
        List<Fragment> fragments = getRetainedChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next instanceof ConversationalFragment) {
                    ((ConversationalFragment) next).onFocusChanged(z);
                }
            }
        }
    }

    public boolean onBackPressed() {
        List<Fragment> fragments = getRetainedChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next != null && next.isVisible()) {
                    if ((next instanceof FaqFlowFragment) || (next instanceof BaseConversationFragment)) {
                        FragmentManager childFragmentManager = next.getChildFragmentManager();
                        if (childFragmentManager.getBackStackEntryCount() > 0) {
                            childFragmentManager.popBackStack();
                            return true;
                        } else if ((next instanceof ConversationalFragment) && ((ConversationalFragment) next).onBackPressed()) {
                            return true;
                        } else {
                            if (next instanceof ConversationFragment) {
                                ((ConversationFragment) next).stopLiveUpdates();
                                return false;
                            }
                        }
                    } else if (next instanceof ScreenshotPreviewFragment) {
                        ((ScreenshotPreviewFragment) next).deleteAttachmentLocalCopy();
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public void setTitle(String str) {
        if (this.toolbar != null) {
            this.toolbar.setTitle(str);
            return;
        }
        ActionBar supportActionBar = ((AppCompatActivity) getActivity(this)).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(str);
        }
    }

    public void showToolbarElevation(boolean z) {
        if (Build.VERSION.SDK_INT >= 21) {
            showToolbarElevationLollipop(z);
        } else {
            showToolbarElevationPreLollipop(z);
        }
    }

    @TargetApi(21)
    private void showToolbarElevationLollipop(boolean z) {
        if (this.toolbar == null) {
            ActionBar supportActionBar = ((AppCompatActivity) getActivity(this)).getSupportActionBar();
            if (supportActionBar == null) {
                return;
            }
            if (z) {
                supportActionBar.setElevation(Styles.dpToPx(getContext(), 4.0f));
            } else {
                supportActionBar.setElevation(0.0f);
            }
        } else if (z) {
            this.toolbar.setElevation(Styles.dpToPx(getContext(), 4.0f));
        } else {
            this.toolbar.setElevation(0.0f);
        }
    }

    private void showToolbarElevationPreLollipop(boolean z) {
        FrameLayout frameLayout = (FrameLayout) getActivity(this).findViewById(R.id.flow_fragment_container);
        if (frameLayout == null) {
            return;
        }
        if (z) {
            frameLayout.setForeground(getResources().getDrawable(R.drawable.hs__actionbar_compat_shadow));
        } else {
            frameLayout.setForeground(new ColorDrawable(0));
        }
    }

    public void exitSdkSession() {
        if (getActivity() instanceof ParentActivity) {
            getActivity().finish();
        } else {
            FragmentUtil.removeFragment(getActivity().getSupportFragmentManager(), this);
        }
    }

    public void launchImagePicker(boolean z, Bundle bundle) {
        if (z) {
            getImagePicker().checkPermissionAndLaunchImagePicker(bundle);
        } else {
            getImagePicker().launchImagePicker(bundle);
        }
    }

    public void onNewIntent(Bundle bundle) {
        if (this.isForeground) {
            this.supportController.onNewIntent(bundle);
        } else {
            this.newIntentData = bundle;
        }
        this.handleNewIntent = !this.isForeground;
    }

    private void updateMessageBatchCount(Integer num) {
        this.newMessageCount = num.intValue();
        updateBadgeIcon();
    }

    public void onDataFetched(Integer num) {
        updateMessageBatchCount(num);
    }

    public void registerToolbarMenuEventsListener(IMenuItemEventListener iMenuItemEventListener) {
        this.menuItemEventListener = new WeakReference<>(iMenuItemEventListener);
    }

    public void unRegisterToolbarMenuEventsListener(IMenuItemEventListener iMenuItemEventListener) {
        if (this.menuItemEventListener != null && this.menuItemEventListener.get() == iMenuItemEventListener) {
            this.menuItemEventListener = null;
        }
    }

    public void onClick(View view) {
        FaqFlowFragment faqFlowFragment;
        if (view.getId() == R.id.button_retry && (faqFlowFragment = FragmentUtil.getFaqFlowFragment(getRetainedChildFragmentManager())) != null) {
            faqFlowFragment.retryGetSections();
        }
    }

    private synchronized ImagePicker getImagePicker() {
        if (this.imagePicker == null) {
            this.imagePicker = new ImagePicker(this);
        }
        return this.imagePicker;
    }

    public void updateMenuItemVisibility(HSMenuItemType hSMenuItemType, boolean z) {
        switch (hSMenuItemType) {
            case START_NEW_CONVERSATION:
                if (this.startNewConversationMenuItem != null) {
                    this.startNewConversationMenuItem.setVisible(z);
                    return;
                }
                return;
            case SCREENSHOT_ATTACHMENT:
                if (this.attachImageMenuItem != null) {
                    this.attachImageMenuItem.setVisible(z);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if ((i == 1 || i == 2) && intent != null && i2 == -1) {
            getImagePicker().onImagePickRequestResult(i, intent);
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        List<Fragment> fragments = getRetainedChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next != null && next.isVisible() && (next instanceof BaseConversationFragment)) {
                    next.onRequestPermissionsResult(i, strArr, iArr);
                    return;
                }
            }
        }
        super.onRequestPermissionsResult(i, strArr, iArr);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.toolbarId = arguments.getInt(MainFragment.TOOLBAR_ID);
        }
        if (this.toolbarId == 0) {
            setHasOptionsMenu(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__support_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.viewNoFaqs = view.findViewById(R.id.view_no_faqs);
        this.viewFaqsLoading = view.findViewById(R.id.view_faqs_loading);
        this.viewFaqsLoadError = view.findViewById(R.id.view_faqs_load_error);
        ((Button) view.findViewById(R.id.button_retry)).setOnClickListener(this);
        if (HelpshiftContext.getCoreApi().getSDKConfigurationDM().isHelpshiftBrandingDisabled()) {
            ((ImageView) view.findViewById(R.id.hs_logo)).setVisibility(8);
        }
        if (this.toolbarId != 0) {
            this.toolbar = (Toolbar) getActivity(this).findViewById(this.toolbarId);
            Menu menu = this.toolbar.getMenu();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < menu.size(); i++) {
                arrayList.add(Integer.valueOf(menu.getItem(i).getItemId()));
            }
            this.toolbar.inflateMenu(getMenuResourceId());
            attachMenuListeners(this.toolbar.getMenu());
            Menu menu2 = this.toolbar.getMenu();
            this.fragmentMenuItems = new ArrayList();
            for (int i2 = 0; i2 < menu2.size(); i2++) {
                int itemId = menu2.getItem(i2).getItemId();
                if (!arrayList.contains(Integer.valueOf(itemId))) {
                    this.fragmentMenuItems.add(Integer.valueOf(itemId));
                }
            }
        }
    }

    public void onViewStateRestored(@Nullable Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null) {
            if (this.supportController != null) {
                this.supportController.onViewStateRestored(bundle);
            }
            getImagePicker().onViewStateRestored(bundle);
        }
    }

    public void onResume() {
        super.onResume();
        this.supportController.start();
        setToolbarTitle(getString(R.string.hs__help_header));
        showToolbarElevation(true);
        HelpshiftContext.getCoreApi().getConversationController().fetchConversationUpdatesListenerReference = new AtomicReference<>(this);
        startLiveUpdates();
        updateMessageBatchCount(Integer.valueOf(HelpshiftContext.getCoreApi().getNotificationCountSync()));
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.supportController != null) {
            this.supportController.onSaveInstanceState(bundle);
        }
        getImagePicker().onSaveInstanceState(bundle);
    }

    public void onDestroyView() {
        SnackbarUtil.hideSnackbar(getView());
        if (this.toolbar != null) {
            Menu menu = this.toolbar.getMenu();
            for (Integer intValue : this.fragmentMenuItems) {
                menu.removeItem(intValue.intValue());
            }
        }
        this.viewFaqsLoadError = null;
        this.viewFaqsLoading = null;
        this.viewNoFaqs = null;
        super.onDestroyView();
    }

    public void onDetach() {
        HelpshiftContext.getPlatform().setUIContext(null);
        ApplicationUtil.restoreApplicationLocale();
        if (!isChangingConfigurations()) {
            HelpshiftContext.getCoreApi().getConversationInboxPoller().startAppPoller(true);
        }
        super.onDetach();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(getMenuResourceId(), menu);
        attachMenuListeners(menu);
        if (!(this.menuItemEventListener == null || this.menuItemEventListener.get() == null)) {
            this.menuItemEventListener.get().onCreateOptionMenuCalled();
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public void askForReadStoragePermission() {
        BaseConversationFragment baseConversationFragment = (BaseConversationFragment) getRetainedChildFragmentManager().findFragmentByTag(ConversationFragment.FRAGMENT_TAG);
        if (baseConversationFragment == null) {
            baseConversationFragment = (BaseConversationFragment) getRetainedChildFragmentManager().findFragmentByTag(NewConversationFragment.FRAGMENT_TAG);
        }
        if (baseConversationFragment != null) {
            baseConversationFragment.requestPermission(true, 2);
        }
    }

    public void onImagePickerResultSuccess(ImagePickerFile imagePickerFile, Bundle bundle) {
        getSupportController().startScreenshotPreviewFragment(imagePickerFile, bundle, ScreenshotPreviewFragment.LaunchSource.GALLERY_APP);
    }

    public void onImagePickerResultFailure(int i, Long l) {
        switch (i) {
            case -4:
                SnackbarUtil.showSnackbar(getView(), R.string.hs__network_error_msg, -1);
                return;
            case -3:
                SnackbarUtil.showSnackbar(getView(), String.format(getResources().getString(R.string.hs__screenshot_limit_error), Float.valueOf(((float) l.longValue()) / 1048576.0f)), -1);
                return;
            case -2:
                SnackbarUtil.showSnackbar(getView(), R.string.hs__screenshot_upload_error_msg, -1);
                return;
            case -1:
                SnackbarUtil.showSnackbar(getView(), R.string.hs__screenshot_cloud_attach_error, -1);
                return;
            default:
                return;
        }
    }

    public void setToolbarImportanceForAccessibility(int i) {
        if (Build.VERSION.SDK_INT >= 19) {
            if (this.toolbar != null) {
                this.toolbarImportanceForAccessibility = this.toolbar.getImportantForAccessibility();
                this.toolbar.setImportantForAccessibility(i);
                return;
            }
            Activity activity = getActivity(this);
            if (activity instanceof ParentActivity) {
                ((ParentActivity) activity).setToolbarImportanceForAccessibility(i);
            }
        }
    }

    public void resetToolbarImportanceForAccessibility() {
        if (Build.VERSION.SDK_INT >= 19) {
            if (this.toolbar != null) {
                this.toolbar.setImportantForAccessibility(this.toolbarImportanceForAccessibility);
                return;
            }
            Activity activity = getActivity(this);
            if (activity instanceof ParentActivity) {
                ((ParentActivity) activity).setToolbarImportanceForAccessibility(0);
            }
        }
    }
}
