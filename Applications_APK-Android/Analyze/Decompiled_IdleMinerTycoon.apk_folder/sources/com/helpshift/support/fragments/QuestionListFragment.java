package com.helpshift.support.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.exception.ExceptionType;
import com.helpshift.support.Faq;
import com.helpshift.support.FaqTagFilter;
import com.helpshift.support.HSApiData;
import com.helpshift.support.Section;
import com.helpshift.support.adapters.QuestionListAdapter;
import com.helpshift.support.constants.GetSectionsCallBackStatus;
import com.helpshift.support.contracts.FaqFlowViewParent;
import com.helpshift.support.contracts.FaqFragmentListener;
import com.helpshift.support.util.FragmentUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class QuestionListFragment extends MainFragment {
    private static final String TAG = "Helpshift_QstnListFrag";
    private HSApiData data;
    private boolean eventSent = false;
    private FaqTagFilter faqTagFilter;
    private boolean isConfigurationChanged = false;
    private View.OnClickListener onQuestionClickedListener;
    /* access modifiers changed from: private */
    public RecyclerView questionList;
    private String sectionId;
    private String sectionTitle;

    public static QuestionListFragment newInstance(Bundle bundle) {
        QuestionListFragment questionListFragment = new QuestionListFragment();
        questionListFragment.setArguments(bundle);
        return questionListFragment;
    }

    public FaqFragmentListener getFaqFlowListener() {
        return ((FaqFlowViewParent) getParentFragment()).getFaqFlowListener();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.data = new HSApiData(context);
        this.sectionTitle = getString(R.string.hs__help_header);
    }

    public void onStart() {
        super.onStart();
        this.isConfigurationChanged = isChangingConfigurations();
        this.eventSent = false;
    }

    public void onStop() {
        if (isScreenLarge()) {
            setToolbarTitle(getString(R.string.hs__help_header));
        }
        super.onStop();
    }

    public boolean shouldRefreshMenu() {
        return getParentFragment() instanceof FaqFlowFragment;
    }

    /* access modifiers changed from: package-private */
    public void updateSectionData(Section section) {
        if (this.questionList != null) {
            ArrayList<Faq> faqsForSection = this.data.getFaqsForSection(section.getPublishId(), this.faqTagFilter);
            if (faqsForSection != null && !faqsForSection.isEmpty()) {
                this.questionList.setAdapter(new QuestionListAdapter(faqsForSection, this.onQuestionClickedListener));
                SupportFragment supportFragment = FragmentUtil.getSupportFragment(this);
                if (supportFragment != null) {
                    supportFragment.onFaqsLoaded();
                }
                if (TextUtils.isEmpty(this.sectionId)) {
                    getSectionId(getArguments().getString("sectionPublishId"));
                }
                pushAnalyticEvent();
            } else if (!isDetached()) {
                SnackbarUtil.showErrorSnackbar(103, getView());
            }
        }
    }

    private String getSectionTitle(String str) {
        Section section = this.data.getSection(str);
        if (section != null) {
            return section.getTitle();
        }
        return null;
    }

    private void getSectionId(String str) {
        Section section = this.data.getSection(str);
        if (section != null) {
            this.sectionId = section.getSectionId();
        }
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        pushAnalyticEvent();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.faqTagFilter = (FaqTagFilter) arguments.getSerializable("withTagsMatching");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__question_list_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.questionList = (RecyclerView) view.findViewById(R.id.question_list);
        this.questionList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        this.onQuestionClickedListener = new View.OnClickListener() {
            public void onClick(View view) {
                QuestionListFragment.this.getFaqFlowListener().onQuestionSelected((String) view.getTag(), null);
            }
        };
        String string = getArguments().getString("sectionPublishId");
        if (isScreenLarge()) {
            String sectionTitle2 = getSectionTitle(string);
            if (!TextUtils.isEmpty(sectionTitle2)) {
                this.sectionTitle = sectionTitle2;
            }
        }
        SectionSuccessHandler sectionSuccessHandler = new SectionSuccessHandler(this);
        SectionFailureHandler sectionFailureHandler = new SectionFailureHandler(this);
        if (getArguments().getInt(SupportFragment.SUPPORT_MODE, 0) != 2) {
            this.data.getSectionSync(string, sectionSuccessHandler, sectionFailureHandler);
        } else {
            this.data.getSection(string, sectionSuccessHandler, sectionFailureHandler, this.faqTagFilter);
        }
        HSLogger.d(TAG, "FAQ section loaded : Name : " + this.sectionTitle);
    }

    public void onResume() {
        super.onResume();
        setToolbarTitle(getString(R.string.hs__help_header));
        if (isScreenLarge()) {
            setToolbarTitle(this.sectionTitle);
            Fragment parentFragment = getParentFragment();
            if (parentFragment instanceof FaqFlowFragment) {
                ((FaqFlowFragment) parentFragment).showVerticalDivider(true);
            }
        }
        pushAnalyticEvent();
    }

    public void onDestroyView() {
        SnackbarUtil.hideSnackbar(getView());
        this.questionList.setAdapter(null);
        this.questionList = null;
        super.onDestroyView();
    }

    private void pushAnalyticEvent() {
        if (getUserVisibleHint() && !this.eventSent && !this.isConfigurationChanged && !TextUtils.isEmpty(this.sectionId)) {
            HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.BROWSED_FAQ_LIST, this.sectionId);
            this.eventSent = true;
        }
    }

    private static class SectionSuccessHandler extends Handler {
        private final WeakReference<QuestionListFragment> questionListFragmentWeakReference;

        public SectionSuccessHandler(QuestionListFragment questionListFragment) {
            this.questionListFragmentWeakReference = new WeakReference<>(questionListFragment);
        }

        public void handleMessage(Message message) {
            QuestionListFragment questionListFragment = this.questionListFragmentWeakReference.get();
            if (questionListFragment != null && !questionListFragment.isDetached()) {
                if (message.obj != null) {
                    Section section = (Section) message.obj;
                    questionListFragment.updateSectionData(section);
                    HSLogger.d(QuestionListFragment.TAG, "FAQ section loaded : SectionSuccessHandler : " + section.getTitle());
                    return;
                }
                RecyclerView access$000 = questionListFragment.questionList;
                if (access$000 == null || access$000.getAdapter() == null || access$000.getAdapter().getItemCount() == 0) {
                    SnackbarUtil.showErrorSnackbar(103, questionListFragment.getView());
                }
            }
        }
    }

    private static class SectionFailureHandler extends Handler {
        private final WeakReference<QuestionListFragment> questionListFragmentWeakReference;

        public SectionFailureHandler(QuestionListFragment questionListFragment) {
            this.questionListFragmentWeakReference = new WeakReference<>(questionListFragment);
        }

        public void handleMessage(Message message) {
            QuestionListFragment questionListFragment = this.questionListFragmentWeakReference.get();
            if (questionListFragment != null && !questionListFragment.isDetached()) {
                RecyclerView access$000 = questionListFragment.questionList;
                if (access$000 == null || access$000.getAdapter() == null || access$000.getAdapter().getItemCount() == 0) {
                    ExceptionType exceptionType = null;
                    if (message.obj instanceof ExceptionType) {
                        exceptionType = (ExceptionType) message.obj;
                    }
                    if (exceptionType == null || message.what == GetSectionsCallBackStatus.API_FAILURE_CONTENT_UNCHANGED) {
                        SnackbarUtil.showErrorSnackbar(103, questionListFragment.getView());
                    } else {
                        SnackbarUtil.showSnackbar(exceptionType, questionListFragment.getView());
                    }
                }
            }
        }
    }
}
