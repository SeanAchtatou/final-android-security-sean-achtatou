package com.helpshift.support.flows;

import com.helpshift.support.controllers.SupportController;
import java.util.List;

public class DynamicFormFlow implements Flow {
    private final List<Flow> flowList;
    private final String label;
    private final int labelResId;
    private SupportController supportController;

    public DynamicFormFlow(int i, List<Flow> list) {
        this.labelResId = i;
        this.flowList = list;
        this.label = null;
    }

    public DynamicFormFlow(String str, List<Flow> list) {
        this.label = str;
        this.flowList = list;
        this.labelResId = 0;
    }

    public void setSupportController(SupportController supportController2) {
        this.supportController = supportController2;
    }

    public int getLabelResId() {
        return this.labelResId;
    }

    public String getLabel() {
        return this.label;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.controllers.SupportController.startDynamicForm(int, java.util.List<com.helpshift.support.flows.Flow>, boolean):void
     arg types: [int, java.util.List<com.helpshift.support.flows.Flow>, int]
     candidates:
      com.helpshift.support.controllers.SupportController.startDynamicForm(java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, boolean):void
      com.helpshift.support.controllers.SupportController.startDynamicForm(int, java.util.List<com.helpshift.support.flows.Flow>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.controllers.SupportController.startDynamicForm(java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, boolean):void
     arg types: [java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, int]
     candidates:
      com.helpshift.support.controllers.SupportController.startDynamicForm(int, java.util.List<com.helpshift.support.flows.Flow>, boolean):void
      com.helpshift.support.controllers.SupportController.startDynamicForm(java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, boolean):void */
    public void performAction() {
        if (this.labelResId != 0) {
            this.supportController.startDynamicForm(this.labelResId, this.flowList, true);
        } else {
            this.supportController.startDynamicForm(this.label, this.flowList, true);
        }
    }
}
