package com.helpshift.support.flows;

import android.support.annotation.NonNull;
import com.helpshift.support.ApiConfig;
import com.helpshift.support.SupportInternal;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.util.ConfigUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FAQsFlow implements Flow {
    private final HashMap config;
    private final String label;
    private final int labelResId;
    private SupportController supportController;

    public FAQsFlow(int i) {
        this(i, new HashMap());
    }

    public FAQsFlow(int i, Map map) {
        this.labelResId = i;
        this.config = new HashMap(map);
        this.label = null;
    }

    public FAQsFlow(int i, @NonNull ApiConfig apiConfig) {
        this(i, ConfigUtil.validateAndConvertToMap(apiConfig));
    }

    public FAQsFlow(String str) {
        this(str, new HashMap());
    }

    public FAQsFlow(String str, Map map) {
        this.label = str;
        this.config = new HashMap(map);
        this.labelResId = 0;
    }

    public FAQsFlow(@NonNull String str, @NonNull ApiConfig apiConfig) {
        this(str, ConfigUtil.validateAndConvertToMap(apiConfig));
    }

    public void setSupportController(SupportController supportController2) {
        this.supportController = supportController2;
    }

    public int getLabelResId() {
        return this.labelResId;
    }

    public String getLabel() {
        return this.label;
    }

    public void performAction() {
        this.supportController.startFaqFlow(SupportInternal.cleanConfig(this.config), true, (List) this.config.get("customContactUsFlows"));
    }
}
