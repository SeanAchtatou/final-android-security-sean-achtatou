package com.helpshift.support;

import java.util.HashMap;
import java.util.Map;

public class Metadata {
    private String[] issueTags;
    private Map<String, Object> metadata;

    public Metadata(Map<String, Object> map) {
        this(map, null);
    }

    public Metadata(Map<String, Object> map, String[] strArr) {
        if (map != null) {
            this.metadata = map;
        }
        if (strArr != null && strArr.length > 0) {
            this.issueTags = strArr;
        }
    }

    public Map<String, Object> toMap() {
        HashMap hashMap = new HashMap();
        if (this.metadata != null) {
            hashMap.putAll(this.metadata);
        }
        if (this.issueTags != null) {
            hashMap.put(Support.TagsKey, this.issueTags);
        }
        return hashMap;
    }
}
