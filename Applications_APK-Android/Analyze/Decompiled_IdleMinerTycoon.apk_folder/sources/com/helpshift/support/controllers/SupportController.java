package com.helpshift.support.controllers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dto.ConversationDetailDTO;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.support.contracts.ScreenshotPreviewListener;
import com.helpshift.support.contracts.SearchResultListener;
import com.helpshift.support.contracts.SupportScreenView;
import com.helpshift.support.conversations.AuthenticationFailureFragment;
import com.helpshift.support.conversations.BaseConversationFragment;
import com.helpshift.support.conversations.ConversationFragment;
import com.helpshift.support.conversations.ConversationalFragment;
import com.helpshift.support.conversations.NewConversationFragment;
import com.helpshift.support.conversations.usersetup.UserSetupFragment;
import com.helpshift.support.flows.CustomContactUsFlowListHolder;
import com.helpshift.support.flows.DynamicFormFlowListHolder;
import com.helpshift.support.flows.Flow;
import com.helpshift.support.fragments.DynamicFormFragment;
import com.helpshift.support.fragments.FaqFlowFragment;
import com.helpshift.support.fragments.ScreenshotPreviewFragment;
import com.helpshift.support.fragments.SearchResultFragment;
import com.helpshift.support.fragments.SingleQuestionFragment;
import com.helpshift.support.fragments.SupportFragment;
import com.helpshift.support.fragments.SupportFragmentConstants;
import com.helpshift.support.util.FragmentUtil;
import com.helpshift.support.util.Styles;
import com.helpshift.support.util.SupportNotification;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupportController implements SearchResultListener, ScreenshotPreviewListener {
    private static final String TAG = "Helpshift_SupportContr";
    private final String KEY_CONVERSATION_ADD_TO_BACK_STACK = "key_conversation_add_to_back_stack";
    private final String KEY_CONVERSATION_BUNDLE = "key_conversation_bundle";
    private final String KEY_SUPPORT_CONTROLLER_STARTED_STATE = "key_support_controller_started";
    private final Bundle bundle;
    private final Context context;
    private boolean conversationAddToBackStack;
    private Bundle conversationBundle;
    private FragmentManager fragmentManager;
    private boolean isControllerStarted;
    private boolean searchPerformed = false;
    private String sourceSearchQuery;
    private int supportMode;
    private final SupportScreenView supportScreenView;

    public SupportController(Context context2, SupportScreenView supportScreenView2, FragmentManager fragmentManager2, Bundle bundle2) {
        this.context = context2;
        this.supportScreenView = supportScreenView2;
        this.fragmentManager = fragmentManager2;
        this.bundle = bundle2;
    }

    public void onFragmentManagerUpdate(FragmentManager fragmentManager2) {
        this.fragmentManager = fragmentManager2;
    }

    public void setSearchPerformed(boolean z) {
        this.searchPerformed = z;
    }

    public void start() {
        if (!this.isControllerStarted) {
            this.supportMode = this.bundle.getInt(SupportFragment.SUPPORT_MODE, 0);
            int i = this.supportMode;
            if (i == 1) {
                startConversationFlow(this.bundle, false);
            } else if (i != 4) {
                startFaqFlow(this.bundle, false, CustomContactUsFlowListHolder.getFlowList());
            } else {
                startDynamicForm(DynamicFormFlowListHolder.getFlowList(), false);
            }
        }
        this.isControllerStarted = true;
    }

    private void replaceConversationFlow(Bundle bundle2) {
        boolean z = true;
        boolean z2 = !Long.valueOf(bundle2.getLong(SupportNotification.BUNGLE_ARG_NOTIFICATION_CONVERSATION_ID)).equals(this.conversationBundle != null ? Long.valueOf(this.conversationBundle.getLong(ConversationFragment.BUNDLE_ARG_CONVERSATION_LOCAL_ID)) : null);
        List<Fragment> fragments = this.fragmentManager.getFragments();
        if (z2) {
            clearConversationStack();
        } else if (fragments.size() > 0) {
            Fragment fragment = fragments.get(fragments.size() - 1);
            if (!(fragment instanceof ScreenshotPreviewFragment)) {
                if (fragment instanceof BaseConversationFragment) {
                    z = false;
                }
            } else {
                return;
            }
        }
        if (z) {
            this.conversationBundle = bundle2;
            startConversationFlow();
        }
    }

    public void startConversationFlow(Bundle bundle2, boolean z) {
        this.conversationAddToBackStack = z;
        this.conversationBundle = bundle2;
        startConversationFlow();
    }

    public void startConversationFlow() {
        startConversationFlow(new HashMap());
    }

    public void startConversationFlow(Map<String, Boolean> map) {
        switch (HelpshiftContext.getCoreApi().getUserManagerDM().getActiveUserSetupDM().getState()) {
            case NON_STARTED:
            case IN_PROGRESS:
            case FAILED:
                showUserSetupFragment();
                return;
            case COMPLETED:
                startConversationFlowInternal(map);
                return;
            default:
                return;
        }
    }

    private void startConversationFlowInternal(Map<String, Boolean> map) {
        String name;
        Conversation activeConversationOrPreIssue;
        if (this.conversationBundle == null) {
            this.conversationBundle = this.bundle;
        }
        boolean z = HelpshiftContext.getCoreApi().getSDKConfigurationDM().getBoolean(SDKConfigurationDM.DISABLE_IN_APP_CONVERSATION);
        Long l = null;
        if (!HelpshiftContext.getCoreApi().getSDKConfigurationDM().shouldShowConversationHistory() || z) {
            long j = this.conversationBundle.getLong(SupportNotification.BUNGLE_ARG_NOTIFICATION_CONVERSATION_ID, 0);
            if (j != 0) {
                this.conversationBundle.remove(SupportNotification.BUNGLE_ARG_NOTIFICATION_CONVERSATION_ID);
                if (HelpshiftContext.getCoreApi().getConversationController().shouldOpenConversationFromNotification(j)) {
                    showConversationFragment(false, Long.valueOf(j), map);
                    return;
                }
            }
            if (!z && (activeConversationOrPreIssue = HelpshiftContext.getCoreApi().getActiveConversationOrPreIssue()) != null) {
                l = activeConversationOrPreIssue.localId;
            }
            if (l == null) {
                List<Flow> flowList = CustomContactUsFlowListHolder.getFlowList();
                if (flowList == null || flowList.isEmpty()) {
                    showNewConversationFragment();
                    return;
                }
                FragmentManager.BackStackEntry backStackEntryAt = getFragmentManager().getBackStackEntryAt(this.fragmentManager.getBackStackEntryCount() - 1);
                if (!(backStackEntryAt == null || (name = backStackEntryAt.getName()) == null || !name.equals(ConversationFragment.class.getName()))) {
                    FragmentUtil.popBackStackImmediate(this.fragmentManager, name);
                }
                startDynamicForm(flowList, true);
                return;
            }
            showConversationFragment(false, l, map);
            return;
        }
        showConversationFragment(true, null, map);
    }

    public void onAuthenticationFailure() {
        showAuthenticationFailureFragment();
    }

    public void showAuthenticationFailureFragment() {
        HSLogger.d(TAG, "Starting authentication failure fragment");
        AuthenticationFailureFragment newInstance = AuthenticationFailureFragment.newInstance();
        String name = this.conversationAddToBackStack ? newInstance.getClass().getName() : null;
        clearConversationStack();
        FragmentUtil.startFragment(this.fragmentManager, R.id.flow_fragment_container, newInstance, AuthenticationFailureFragment.FRAGMENT_TAG, name, false, false);
    }

    public void showUserSetupFragment() {
        String str;
        UserSetupFragment newInstance = UserSetupFragment.newInstance();
        if (this.conversationAddToBackStack) {
            str = newInstance.getClass().getName();
            clearConversationStack();
        } else {
            str = null;
        }
        FragmentUtil.startFragment(this.fragmentManager, R.id.flow_fragment_container, newInstance, UserSetupFragment.FRAGMENT_TAG, str, false, false);
    }

    public void startFaqFlow(Bundle bundle2, boolean z, List<Flow> list) {
        if (!isDuplicateFAQScreenAlreadyOpen(bundle2)) {
            FaqFlowFragment newInstance = FaqFlowFragment.newInstance(bundle2, list);
            String str = null;
            if (z) {
                str = FaqFlowFragment.class.getName();
            }
            FragmentUtil.startFragment(this.fragmentManager, R.id.flow_fragment_container, newInstance, FaqFlowFragment.FRAGMENT_TAG, str, false, false);
        }
    }

    private boolean isDuplicateFAQScreenAlreadyOpen(Bundle bundle2) {
        FaqFlowController faqFlowController;
        Fragment topMostFragment = FragmentUtil.getTopMostFragment(this.fragmentManager);
        if (!(topMostFragment instanceof FaqFlowFragment) || (faqFlowController = ((FaqFlowFragment) topMostFragment).getFaqFlowController()) == null) {
            return false;
        }
        Fragment topMostFaqFragment = faqFlowController.getTopMostFaqFragment();
        if (!(topMostFaqFragment instanceof SingleQuestionFragment)) {
            return true;
        }
        String string = bundle2.getString(SingleQuestionFragment.BUNDLE_ARG_QUESTION_PUBLISH_ID);
        String questionPublishId = ((SingleQuestionFragment) topMostFaqFragment).getQuestionPublishId();
        if (string == null || !string.equals(questionPublishId)) {
            return false;
        }
        return true;
    }

    private void showNewConversationFragment() {
        String str;
        HSLogger.d(TAG, "Starting new conversation fragment");
        this.conversationBundle.putBoolean(NewConversationFragment.SEARCH_PERFORMED, this.searchPerformed);
        this.conversationBundle.putString(NewConversationFragment.SOURCE_SEARCH_QUERY, this.sourceSearchQuery);
        NewConversationFragment newInstance = NewConversationFragment.newInstance(this.conversationBundle);
        if (this.conversationAddToBackStack) {
            str = newInstance.getClass().getName();
            clearConversationStack();
        } else {
            str = null;
        }
        FragmentUtil.startFragment(this.fragmentManager, R.id.flow_fragment_container, newInstance, NewConversationFragment.FRAGMENT_TAG, str, false, false);
    }

    private void showConversationFragment(boolean z, @Nullable Long l, Map<String, Boolean> map) {
        HSLogger.d(TAG, "Starting conversation fragment: " + l);
        if (!z) {
            if (l != null) {
                this.conversationBundle.putLong(ConversationFragment.BUNDLE_ARG_CONVERSATION_LOCAL_ID, l.longValue());
            } else {
                return;
            }
        }
        this.conversationBundle.putBoolean(ConversationFragment.BUNDLE_ARG_SHOW_CONVERSATION_HISTORY, z);
        for (String next : map.keySet()) {
            this.conversationBundle.putBoolean(next, map.get(next).booleanValue());
        }
        ConversationalFragment newInstance = ConversationalFragment.newInstance(this.conversationBundle);
        String str = null;
        if (this.conversationAddToBackStack) {
            str = newInstance.getClass().getName();
            clearConversationStack();
        }
        FragmentUtil.startFragment(this.fragmentManager, R.id.flow_fragment_container, newInstance, ConversationFragment.FRAGMENT_TAG, str, false, false);
    }

    public void showConversationSearchResultFragment(Bundle bundle2) {
        FragmentUtil.startFragmentWithBackStack(this.fragmentManager, R.id.flow_fragment_container, SearchResultFragment.newInstance(bundle2, this), SearchResultFragment.FRAGMENT_TAG, false);
    }

    public void startScreenshotPreviewFragment(ImagePickerFile imagePickerFile, Bundle bundle2, ScreenshotPreviewFragment.LaunchSource launchSource) {
        ScreenshotPreviewFragment screenshotPreviewFragment = FragmentUtil.getScreenshotPreviewFragment(getFragmentManager());
        if (screenshotPreviewFragment == null) {
            screenshotPreviewFragment = ScreenshotPreviewFragment.newInstance(this);
            FragmentUtil.startFragmentWithBackStack(getFragmentManager(), R.id.flow_fragment_container, screenshotPreviewFragment, ScreenshotPreviewFragment.FRAGMENT_TAG, false);
        }
        screenshotPreviewFragment.setParams(bundle2, imagePickerFile, launchSource);
    }

    public void startDynamicForm(List<Flow> list, boolean z) {
        FragmentUtil.startFragment(this.fragmentManager, R.id.flow_fragment_container, DynamicFormFragment.newInstance(this.bundle, list, this), DynamicFormFragment.FRAGMENT_TAG, z ? DynamicFormFragment.class.getName() : null, false, false);
    }

    public void startDynamicForm(String str, List<Flow> list, boolean z) {
        if (this.bundle != null) {
            this.bundle.putString(SupportFragmentConstants.FLOW_TITLE, str);
        }
        startDynamicForm(list, z);
    }

    public void startDynamicForm(int i, List<Flow> list, boolean z) {
        if (!(this.bundle == null || i == 0)) {
            this.bundle.putString(SupportFragmentConstants.FLOW_TITLE, this.context.getResources().getString(i));
        }
        startDynamicForm(list, z);
    }

    public int getSupportMode() {
        return this.supportMode;
    }

    public FragmentManager getFragmentManager() {
        return this.fragmentManager;
    }

    public void onContactUsClicked(String str) {
        if (!handleCustomContactUsFlows()) {
            if (!TextUtils.isEmpty(str)) {
                this.sourceSearchQuery = str;
            }
            startConversationFlow(this.bundle, true);
        }
    }

    private boolean handleCustomContactUsFlows() {
        FaqFlowFragment faqFlowFragment;
        List<Flow> customContactUsFlows;
        if (HelpshiftContext.getCoreApi().getActiveConversation() != null || (faqFlowFragment = FragmentUtil.getFaqFlowFragment(this.fragmentManager)) == null || (customContactUsFlows = faqFlowFragment.getCustomContactUsFlows()) == null || customContactUsFlows.isEmpty()) {
            return false;
        }
        startDynamicForm(customContactUsFlows, true);
        return true;
    }

    public void actionDone() {
        sendTicketAvoidedEvent();
        Long localId = HelpshiftContext.getCoreApi().getUserManagerDM().getActiveUser().getLocalId();
        HelpshiftContext.getPlatform().getConversationInboxDAO().saveDescriptionDetail(localId.longValue(), new ConversationDetailDTO("", System.nanoTime(), 0));
        HelpshiftContext.getPlatform().getConversationInboxDAO().saveImageAttachment(localId.longValue(), null);
        if (getSupportMode() == 1) {
            this.supportScreenView.exitSdkSession();
        } else {
            FragmentUtil.popBackStackImmediate(getFragmentManager(), NewConversationFragment.class.getName());
        }
    }

    private void sendTicketAvoidedEvent() {
        SingleQuestionFragment singleQuestionFragment = FragmentUtil.getSingleQuestionFragment(this.fragmentManager);
        if (singleQuestionFragment != null) {
            String questionId = singleQuestionFragment.getQuestionId();
            if (!TextUtils.isEmpty(questionId)) {
                HashMap hashMap = new HashMap();
                hashMap.put("id", questionId);
                ConversationDetailDTO descriptionDetail = HelpshiftContext.getPlatform().getConversationInboxDAO().getDescriptionDetail(HelpshiftContext.getCoreApi().getUserManagerDM().getActiveUser().getLocalId().longValue());
                if (descriptionDetail != null) {
                    hashMap.put(AnalyticsEventKey.STR, descriptionDetail.title);
                }
                HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.TICKET_AVOIDED, hashMap);
            }
        }
    }

    public void onQuestionSelected(String str, ArrayList<String> arrayList) {
        boolean isTablet = Styles.isTablet(this.context);
        this.bundle.putString(SingleQuestionFragment.BUNDLE_ARG_QUESTION_PUBLISH_ID, str);
        if (arrayList != null) {
            this.bundle.putStringArrayList("searchTerms", arrayList);
        }
        FragmentUtil.startFragmentWithBackStack(this.fragmentManager, R.id.flow_fragment_container, SingleQuestionFragment.newInstance(this.bundle, 2, isTablet, null), null, false);
    }

    public void sendAnyway() {
        HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.TICKET_AVOIDANCE_FAILED);
        FragmentUtil.popBackStackImmediate(getFragmentManager(), SearchResultFragment.class.getName());
        NewConversationFragment newConversationFragment = (NewConversationFragment) this.fragmentManager.findFragmentByTag(NewConversationFragment.FRAGMENT_TAG);
        if (newConversationFragment != null) {
            newConversationFragment.startNewConversation();
        }
    }

    public void onAdminSuggestedQuestionSelected(String str, String str2, SingleQuestionFragment.QuestionReadListener questionReadListener) {
        boolean isTablet = Styles.isTablet(this.context);
        this.bundle.putString(SingleQuestionFragment.BUNDLE_ARG_QUESTION_PUBLISH_ID, str);
        this.bundle.putString(SingleQuestionFragment.BUNDLE_ARG_QUESTION_LANGUAGE, str2);
        FragmentUtil.startFragmentWithBackStack(this.fragmentManager, R.id.flow_fragment_container, SingleQuestionFragment.newInstance(this.bundle, 3, isTablet, questionReadListener), null, false);
    }

    public void addScreenshot(ImagePickerFile imagePickerFile) {
        FragmentUtil.popBackStack(this.fragmentManager, ScreenshotPreviewFragment.class.getName());
        NewConversationFragment newConversationFragment = (NewConversationFragment) this.fragmentManager.findFragmentByTag(NewConversationFragment.FRAGMENT_TAG);
        if (newConversationFragment != null) {
            newConversationFragment.handleScreenshotAction(ScreenshotPreviewFragment.ScreenshotAction.ADD, imagePickerFile);
        }
    }

    public void sendScreenshot(ImagePickerFile imagePickerFile, @Nullable String str) {
        FragmentUtil.popBackStack(this.fragmentManager, ScreenshotPreviewFragment.class.getName());
        ConversationFragment conversationFragment = (ConversationFragment) this.fragmentManager.findFragmentByTag(ConversationFragment.FRAGMENT_TAG);
        if (conversationFragment != null) {
            conversationFragment.handleScreenshotAction(ScreenshotPreviewFragment.ScreenshotAction.SEND, imagePickerFile, str);
        }
    }

    public void removeScreenshot() {
        FragmentUtil.popBackStack(this.fragmentManager, ScreenshotPreviewFragment.class.getName());
        NewConversationFragment newConversationFragment = (NewConversationFragment) this.fragmentManager.findFragmentByTag(NewConversationFragment.FRAGMENT_TAG);
        if (newConversationFragment != null) {
            newConversationFragment.handleScreenshotAction(ScreenshotPreviewFragment.ScreenshotAction.REMOVE, null);
        }
    }

    public void changeScreenshot(Bundle bundle2) {
        this.supportScreenView.launchImagePicker(true, bundle2);
    }

    public void removeScreenshotPreviewFragment() {
        FragmentUtil.popBackStack(this.fragmentManager, ScreenshotPreviewFragment.class.getName());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.helpshift.support.controllers.SupportController.startDynamicForm(java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, boolean):void
     arg types: [java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, int]
     candidates:
      com.helpshift.support.controllers.SupportController.startDynamicForm(int, java.util.List<com.helpshift.support.flows.Flow>, boolean):void
      com.helpshift.support.controllers.SupportController.startDynamicForm(java.lang.String, java.util.List<com.helpshift.support.flows.Flow>, boolean):void */
    public void onNewIntent(Bundle bundle2) {
        int i = bundle2.getInt(SupportFragment.SUPPORT_MODE);
        if (i == 1) {
            replaceConversationFlow(bundle2);
        } else if (i != 4) {
            startFaqFlow(bundle2, true, CustomContactUsFlowListHolder.getFlowList());
        } else {
            startDynamicForm(bundle2.getString(SupportFragmentConstants.FLOW_TITLE), DynamicFormFlowListHolder.getFlowList(), true);
        }
    }

    private void clearConversationStack() {
        boolean z;
        List<Fragment> fragments = this.fragmentManager.getFragments();
        for (int size = fragments.size() - 1; size >= 0; size--) {
            Fragment fragment = fragments.get(size);
            if ((fragment instanceof ScreenshotPreviewFragment) || (fragment instanceof BaseConversationFragment) || (fragment instanceof UserSetupFragment) || (fragment instanceof AuthenticationFailureFragment)) {
                if (size == 0) {
                    FragmentUtil.removeFragment(this.fragmentManager, fragment);
                    List<Fragment> fragments2 = this.fragmentManager.getFragments();
                    if (fragments2 != null && fragments2.size() > 0) {
                        FragmentUtil.popBackStack(this.fragmentManager, fragment.getClass().getName());
                    }
                } else {
                    FragmentUtil.popBackStack(this.fragmentManager, fragment.getClass().getName());
                }
            }
        }
        Fragment findFragmentByTag = this.fragmentManager.findFragmentByTag(ConversationFragment.FRAGMENT_TAG);
        if (findFragmentByTag != null) {
            FragmentUtil.popBackStackImmediate(this.fragmentManager, findFragmentByTag.getClass().getName());
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            this.conversationAddToBackStack = true;
        }
    }

    public void onSaveInstanceState(Bundle bundle2) {
        bundle2.putBoolean("key_support_controller_started", this.isControllerStarted);
        bundle2.putBundle("key_conversation_bundle", this.conversationBundle);
        bundle2.putBoolean("key_conversation_add_to_back_stack", this.conversationAddToBackStack);
    }

    public void onViewStateRestored(Bundle bundle2) {
        if (!this.isControllerStarted) {
            if (bundle2.containsKey("key_support_controller_started")) {
                this.isControllerStarted = bundle2.containsKey("key_support_controller_started");
                this.supportMode = this.bundle.getInt(SupportFragment.SUPPORT_MODE, 0);
                if (this.fragmentManager != null) {
                    ScreenshotPreviewFragment screenshotPreviewFragment = (ScreenshotPreviewFragment) this.fragmentManager.findFragmentByTag(ScreenshotPreviewFragment.FRAGMENT_TAG);
                    if (screenshotPreviewFragment != null) {
                        screenshotPreviewFragment.setScreenshotPreviewListener(this);
                    }
                    SearchResultFragment searchResultFragment = (SearchResultFragment) this.fragmentManager.findFragmentByTag(SearchResultFragment.FRAGMENT_TAG);
                    if (searchResultFragment != null) {
                        searchResultFragment.setSearchResultListener(this);
                    }
                    DynamicFormFragment dynamicFormFragment = (DynamicFormFragment) this.fragmentManager.findFragmentByTag(DynamicFormFragment.FRAGMENT_TAG);
                    if (dynamicFormFragment != null) {
                        dynamicFormFragment.setSupportController(this);
                    }
                }
            }
            if (bundle2.containsKey("key_conversation_bundle") && bundle2.containsKey("key_conversation_add_to_back_stack")) {
                this.conversationBundle = bundle2.getBundle("key_conversation_bundle");
                this.conversationAddToBackStack = bundle2.getBoolean("key_conversation_add_to_back_stack");
            }
        }
    }

    public void onUserSetupSyncCompleted() {
        startConversationFlowInternal(new HashMap());
    }
}
