package com.helpshift.support.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.helpshift.R;
import com.helpshift.support.util.AttachmentUtil;

public class HSRoundedImageView extends AppCompatImageView {
    private ImageView.ScaleType SCALE_TYPE;
    private Paint backgroundPaint;
    private Paint bitmapPaint;
    private BitmapShader bitmapShader;
    private Paint borderPaint;
    private RectF borderRect;
    private float borderWidth;
    private float cornerRadius;
    private Bitmap defaultImageBitmap;
    private RectF drawableRect;
    private Bitmap imageBitmap;
    private String imagePath;
    private boolean[] isCornerRounded;
    private final Matrix shaderMatrix;

    public HSRoundedImageView(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public HSRoundedImageView(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.shaderMatrix = new Matrix();
        this.SCALE_TYPE = ImageView.ScaleType.CENTER_CROP;
        this.drawableRect = new RectF();
        this.borderRect = new RectF();
        this.isCornerRounded = new boolean[4];
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.HSRoundedImageView, 0, 0);
        int color = obtainStyledAttributes.getColor(R.styleable.HSRoundedImageView_hs__borderColor, -1);
        int color2 = obtainStyledAttributes.getColor(R.styleable.HSRoundedImageView_hs__backgroundColor, -1);
        this.borderWidth = obtainStyledAttributes.getDimension(R.styleable.HSRoundedImageView_hs__borderWidth, 0.0f);
        if (this.borderWidth < 0.0f) {
            this.borderWidth = 0.0f;
        }
        this.cornerRadius = obtainStyledAttributes.getDimension(R.styleable.HSRoundedImageView_hs__cornerRadius, 0.0f);
        this.isCornerRounded[0] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedTopLeft, true);
        this.isCornerRounded[1] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedTopRight, true);
        this.isCornerRounded[2] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedBottomLeft, true);
        this.isCornerRounded[3] = obtainStyledAttributes.getBoolean(R.styleable.HSRoundedImageView_hs__roundedBottomRight, true);
        Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.HSRoundedImageView_hs__placeholder);
        if (drawable instanceof BitmapDrawable) {
            this.defaultImageBitmap = ((BitmapDrawable) drawable).getBitmap();
        }
        obtainStyledAttributes.recycle();
        this.bitmapPaint = new Paint();
        this.bitmapPaint.setStyle(Paint.Style.FILL);
        this.bitmapPaint.setAntiAlias(true);
        this.borderPaint = new Paint();
        this.borderPaint.setStyle(Paint.Style.STROKE);
        this.borderPaint.setAntiAlias(true);
        this.borderPaint.setColor(color);
        this.borderPaint.setStrokeWidth(this.borderWidth);
        if (color2 != -1) {
            this.backgroundPaint = new Paint();
            this.backgroundPaint.setStyle(Paint.Style.FILL);
            this.backgroundPaint.setColor(color2);
            this.backgroundPaint.setAntiAlias(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        setup();
    }

    private void setup() {
        loadImageBitmap();
        if (this.imageBitmap != null) {
            updateGlobalParamsAndBitmapShader(this.imageBitmap);
        } else if (this.defaultImageBitmap != null) {
            updateGlobalParamsAndBitmapShader(this.defaultImageBitmap);
        } else {
            invalidate();
        }
    }

    private void updateGlobalParamsAndBitmapShader(Bitmap bitmap) {
        if (bitmap != null && getWidth() > 0 && getHeight() > 0) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            this.borderRect.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            this.drawableRect.set(this.borderRect);
            this.borderRect.inset(this.borderWidth / 2.0f, this.borderWidth / 2.0f);
            this.drawableRect.inset(this.borderWidth, this.borderWidth);
            this.bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            updateShaderMatrix(this.bitmapShader, width, height);
            invalidate();
        }
    }

    private void loadImageBitmap() {
        if (TextUtils.isEmpty(this.imagePath) || getWidth() <= 0) {
            this.imageBitmap = null;
        } else {
            this.imageBitmap = AttachmentUtil.getBitmap(this.imagePath, getWidth());
        }
    }

    public void loadImage(String str) {
        if (str != null) {
            String trim = str.trim();
            if (!trim.equals(this.imagePath)) {
                this.imagePath = trim;
                setup();
            } else if (this.imageBitmap == null) {
                setup();
            }
        } else {
            this.imagePath = null;
            setup();
        }
    }

    public ImageView.ScaleType getScaleType() {
        return this.SCALE_TYPE;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.bitmapPaint.setShader(this.bitmapShader);
        if (this.borderWidth > 0.0f) {
            if (this.backgroundPaint != null) {
                canvas.drawRoundRect(this.drawableRect, this.cornerRadius - this.borderWidth, this.cornerRadius - this.borderWidth, this.backgroundPaint);
            }
            canvas.drawRoundRect(this.drawableRect, this.cornerRadius - this.borderWidth, this.cornerRadius - this.borderWidth, this.bitmapPaint);
            canvas.drawRoundRect(this.borderRect, this.cornerRadius, this.cornerRadius, this.borderPaint);
            redrawBitmapForSquareCorners(canvas, this.backgroundPaint);
            redrawBitmapForSquareCorners(canvas, this.bitmapPaint);
            redrawBorderForSquareCorners(canvas);
            return;
        }
        if (this.backgroundPaint != null) {
            canvas.drawRoundRect(this.drawableRect, this.cornerRadius, this.cornerRadius, this.backgroundPaint);
        }
        canvas.drawRoundRect(this.drawableRect, this.cornerRadius, this.cornerRadius, this.bitmapPaint);
        redrawBitmapForSquareCorners(canvas, this.backgroundPaint);
        redrawBitmapForSquareCorners(canvas, this.bitmapPaint);
    }

    private void redrawBitmapForSquareCorners(Canvas canvas, Paint paint) {
        if (this.cornerRadius > 0.0f && paint != null) {
            float f = this.drawableRect.left;
            float f2 = this.drawableRect.top;
            float width = this.drawableRect.width() + f;
            float height = this.drawableRect.height() + f2;
            float f3 = this.cornerRadius;
            RectF rectF = new RectF();
            if (!this.isCornerRounded[0]) {
                rectF.set(f, f2, f + f3, f2 + f3);
                canvas.drawRect(rectF, paint);
            }
            if (!this.isCornerRounded[1]) {
                rectF.set(width - f3, f2, width, f2 + f3);
                canvas.drawRect(rectF, paint);
            }
            if (!this.isCornerRounded[2]) {
                rectF.set(f, height - f3, f + f3, height);
                canvas.drawRect(rectF, paint);
            }
            if (!this.isCornerRounded[3]) {
                rectF.set(width - f3, height - f3, width, height);
                canvas.drawRect(rectF, paint);
            }
        }
    }

    private void redrawBorderForSquareCorners(Canvas canvas) {
        if (this.cornerRadius > 0.0f) {
            float f = this.borderRect.left;
            float f2 = this.borderRect.top;
            float width = f + this.borderRect.width();
            float height = f2 + this.borderRect.height();
            float f3 = this.cornerRadius;
            float f4 = this.borderWidth;
            if (!this.isCornerRounded[0]) {
                canvas.drawLine(f - f4, f2, f + f3, f2, this.borderPaint);
                canvas.drawLine(f, f2 - f4, f, f2 + f3, this.borderPaint);
            }
            if (!this.isCornerRounded[1]) {
                Canvas canvas2 = canvas;
                float f5 = width;
                canvas2.drawLine((width - f3) - f4, f2, f5, f2, this.borderPaint);
                canvas2.drawLine(width, f2 - f4, f5, f2 + f3, this.borderPaint);
            }
            if (!this.isCornerRounded[3]) {
                Canvas canvas3 = canvas;
                float f6 = height;
                canvas3.drawLine((width - f3) - f4, height, width + f4, f6, this.borderPaint);
                canvas3.drawLine(width, height - f3, width, f6, this.borderPaint);
            }
            if (!this.isCornerRounded[2]) {
                canvas.drawLine(f - f4, height, f + f3, height, this.borderPaint);
                canvas.drawLine(f, height - f3, f, height, this.borderPaint);
            }
        }
    }

    private void updateShaderMatrix(BitmapShader bitmapShader2, int i, int i2) {
        float f;
        float f2;
        if (getWidth() > 0 && getHeight() > 0) {
            float f3 = 0.0f;
            if (i > i2) {
                f2 = this.drawableRect.height() / ((float) i2);
                f = (this.drawableRect.width() - (((float) i) * f2)) * 0.5f;
            } else {
                f2 = this.drawableRect.width() / ((float) i);
                f3 = (this.drawableRect.height() - (((float) i2) * f2)) * 0.5f;
                f = 0.0f;
            }
            this.shaderMatrix.setScale(f2, f2);
            this.shaderMatrix.postTranslate(((float) ((int) (f + 0.5f))) + this.borderWidth, ((float) ((int) (f3 + 0.5f))) + this.borderWidth);
            bitmapShader2.setLocalMatrix(this.shaderMatrix);
        }
    }

    private static class Corner {
        static final int BOTTOM_LEFT = 2;
        static final int BOTTOM_RIGHT = 3;
        static final int TOP_LEFT = 0;
        static final int TOP_RIGHT = 1;

        private Corner() {
        }
    }
}
