package com.helpshift.support.views;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.util.Styles;
import java.util.ArrayList;
import java.util.List;

public class HSAdjustableSelectOptionsViewInflater {
    private LinearLayout containerView;
    private Context context;
    private double maxAvailableWidthScaleFactor;
    private int optionColorId;
    private int optionDrawableId;
    private View.OnClickListener optionSelectionListener;
    private int optionTextViewId;
    private List<OptionInput.Option> options;
    private int rightEndPadding;
    private int selectOptionInflaterLayout;

    public HSAdjustableSelectOptionsViewInflater(Context context2, double d, int i, LinearLayout linearLayout, int i2, int i3, int i4, int i5, List<OptionInput.Option> list, View.OnClickListener onClickListener) {
        this.context = context2;
        this.maxAvailableWidthScaleFactor = d;
        this.rightEndPadding = i;
        this.containerView = linearLayout;
        this.selectOptionInflaterLayout = i2;
        this.optionTextViewId = i3;
        this.optionDrawableId = i4;
        this.optionColorId = i5;
        this.optionSelectionListener = onClickListener;
        this.options = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void inflate() {
        ArrayList<LinearLayout> arrayList = new ArrayList<>();
        DisplayMetrics displayMetrics = this.context.getResources().getDisplayMetrics();
        double d = (double) displayMetrics.widthPixels;
        double d2 = this.maxAvailableWidthScaleFactor;
        Double.isNaN(d);
        int i = ((int) (d * d2)) - ((int) (((float) this.rightEndPadding) * displayMetrics.density));
        int size = this.options.size();
        int i2 = 0;
        while (i2 < size) {
            LinearLayout linearLayout = new LinearLayout(this.context);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            linearLayout.setGravity(GravityCompat.END);
            while (true) {
                View inflate = LayoutInflater.from(this.context).inflate(this.selectOptionInflaterLayout, (ViewGroup) null, false);
                TextView textView = (TextView) inflate.findViewById(this.optionTextViewId);
                int paddingLeft = textView.getPaddingLeft();
                int paddingTop = textView.getPaddingTop();
                int paddingRight = textView.getPaddingRight();
                int paddingBottom = textView.getPaddingBottom();
                Styles.setDrawable(this.context, textView, this.optionDrawableId, this.optionColorId);
                textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
                textView.setMaxWidth(i);
                OptionInput.Option option = this.options.get(i2);
                textView.setTag(option);
                textView.setText(option.title);
                textView.setOnClickListener(this.optionSelectionListener);
                linearLayout.addView(inflate);
                linearLayout.measure(0, 0);
                if (linearLayout.getMeasuredWidth() <= i) {
                    if (i2 == size - 1) {
                        arrayList.add(linearLayout);
                    }
                    i2++;
                    if (i2 >= size) {
                        break;
                    }
                } else {
                    if (linearLayout.getChildCount() == 1) {
                        i2++;
                    } else {
                        linearLayout.removeView(inflate);
                    }
                    arrayList.add(linearLayout);
                }
            }
        }
        for (LinearLayout addView : arrayList) {
            this.containerView.addView(addView);
        }
    }
}
