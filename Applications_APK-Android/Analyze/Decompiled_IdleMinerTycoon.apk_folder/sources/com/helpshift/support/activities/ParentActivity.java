package com.helpshift.support.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.helpshift.R;
import com.helpshift.activities.MainActivity;
import com.helpshift.support.fragments.SupportFragment;
import com.helpshift.util.ApplicationUtil;
import com.helpshift.util.HelpshiftContext;
import java.util.List;

public class ParentActivity extends MainActivity {
    FragmentManager fragmentManager;
    private Toolbar toolbar;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!HelpshiftContext.installCallSuccessful.get()) {
            Intent launchIntent = ApplicationUtil.getLaunchIntent(getApplicationContext(), getPackageName());
            if (launchIntent != null) {
                finish();
                startActivity(launchIntent);
                return;
            }
            return;
        }
        setContentView(R.layout.hs__parent_activity);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.fragmentManager = getSupportFragmentManager();
        if (bundle == null) {
            FragmentTransaction beginTransaction = this.fragmentManager.beginTransaction();
            beginTransaction.add(R.id.support_fragment_container, SupportFragment.newInstance(getIntent().getExtras()));
            beginTransaction.commit();
        }
    }

    public void onBackPressed() {
        List<Fragment> fragments = this.fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next != null && next.isVisible() && (next instanceof SupportFragment)) {
                    if (!((SupportFragment) next).onBackPressed()) {
                        FragmentManager childFragmentManager = next.getChildFragmentManager();
                        if (childFragmentManager.getBackStackEntryCount() > 0) {
                            childFragmentManager.popBackStack();
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        List<Fragment> fragments = this.fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment next : fragments) {
                if (next instanceof SupportFragment) {
                    ((SupportFragment) next).onNewIntent(intent.getExtras());
                }
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    public void setToolbarImportanceForAccessibility(int i) {
        if (this.toolbar != null && Build.VERSION.SDK_INT >= 19) {
            this.toolbar.setImportantForAccessibility(i);
        }
    }
}
