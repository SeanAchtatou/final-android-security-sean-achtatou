package com.helpshift.support.util;

import android.content.Context;
import com.helpshift.support.flows.ConversationFlow;
import com.helpshift.support.flows.DynamicFormFlow;
import com.helpshift.support.flows.FAQSectionFlow;
import com.helpshift.support.flows.FAQsFlow;
import com.helpshift.support.flows.Flow;
import com.helpshift.support.flows.SingleFAQFlow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DynamicFormUtil {
    public static List<Flow> toFlowList(Context context, List<HashMap<String, Object>> list) {
        ArrayList arrayList = new ArrayList();
        for (HashMap<String, Object> flow : list) {
            arrayList.add(toFlow(context, flow));
        }
        return arrayList;
    }

    public static Flow toFlow(Context context, HashMap hashMap) {
        Flow flow;
        String str = (String) hashMap.get("type");
        HashMap hashMap2 = new HashMap();
        if (hashMap.containsKey("config")) {
            hashMap2 = (HashMap) hashMap.get("config");
        }
        String str2 = (String) hashMap.get("titleResourceName");
        int i = 0;
        if (str2 != null) {
            i = context.getResources().getIdentifier(str2, "string", context.getPackageName());
        }
        String str3 = "";
        if (i == 0) {
            str3 = (String) hashMap.get("title");
        }
        if (str.equals("faqsFlow")) {
            if (i != 0) {
                return new FAQsFlow(i, hashMap2);
            }
            return new FAQsFlow(str3, hashMap2);
        } else if (!str.equals("conversationFlow")) {
            if (str.equals("faqSectionFlow")) {
                String str4 = (String) hashMap.get("data");
                if (i != 0) {
                    flow = new FAQSectionFlow(i, str4, hashMap2);
                } else {
                    flow = new FAQSectionFlow(str3, str4, hashMap2);
                }
            } else if (str.equals("singleFaqFlow")) {
                String str5 = (String) hashMap.get("data");
                if (i != 0) {
                    flow = new SingleFAQFlow(i, str5, hashMap2);
                } else {
                    flow = new SingleFAQFlow(str3, str5, hashMap2);
                }
            } else if (!str.equals("dynamicFormFlow")) {
                return null;
            } else {
                List<Flow> flowList = toFlowList(context, (ArrayList) hashMap.get("data"));
                if (i != 0) {
                    flow = new DynamicFormFlow(i, flowList);
                } else {
                    flow = new DynamicFormFlow(str3, flowList);
                }
            }
            return flow;
        } else if (i != 0) {
            return new ConversationFlow(i, hashMap2);
        } else {
            return new ConversationFlow(str3, hashMap2);
        }
    }
}
