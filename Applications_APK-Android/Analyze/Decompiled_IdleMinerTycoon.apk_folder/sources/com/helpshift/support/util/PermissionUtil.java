package com.helpshift.support.util;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import com.helpshift.R;
import com.helpshift.util.HSLogger;
import com.helpshift.views.HSSnackbar;

public class PermissionUtil {
    private static final String TAG = "Helpshift_Permissions";

    @TargetApi(9)
    public static void showSettingsPage(Context context) {
        try {
            Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.addCategory("android.intent.category.DEFAULT");
            String packageName = context.getPackageName();
            intent.setData(Uri.parse("package:" + packageName));
            context.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            Intent intent2 = new Intent("android.settings.MANAGE_APPLICATIONS_SETTINGS");
            intent2.addCategory("android.intent.category.DEFAULT");
            context.startActivity(intent2);
        }
    }

    public static Snackbar requestPermissions(final Fragment fragment, final String[] strArr, final int i, View view) {
        HSLogger.d(TAG, "Requesting permission : " + strArr[0]);
        if (fragment.shouldShowRequestPermissionRationale(strArr[0])) {
            Snackbar action = HSSnackbar.make(view, R.string.hs__permission_denied_message, -2).setAction(R.string.hs__permission_rationale_snackbar_action_label, new View.OnClickListener() {
                public void onClick(View view) {
                    fragment.requestPermissions(strArr, i);
                }
            });
            action.show();
            return action;
        }
        fragment.requestPermissions(strArr, i);
        return null;
    }

    public static boolean hasPermissionInManifest(Context context, String str) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            if (packageInfo.requestedPermissions != null) {
                for (String equals : packageInfo.requestedPermissions) {
                    if (equals.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            HSLogger.d(TAG, "Error checking permission in Manifest : ", e);
        }
        return false;
    }
}
