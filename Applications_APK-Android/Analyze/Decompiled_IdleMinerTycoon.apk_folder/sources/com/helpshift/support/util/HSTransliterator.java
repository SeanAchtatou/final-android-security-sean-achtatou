package com.helpshift.support.util;

import com.helpshift.util.AssetsUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import org.json.JSONException;
import org.json.JSONObject;

public class HSTransliterator {
    private static final String TAG = "Helpshift_Transliteratr";
    private static HSCharacters hsCharacters = null;
    private static boolean initDone = false;

    public static boolean isLoaded() {
        return initDone;
    }

    public static void init() {
        if (!initDone) {
            try {
                JSONObject jSONObject = new JSONObject(AssetsUtil.readFileAsString(HelpshiftContext.getApplicationContext(), "hs__data")).getJSONObject("HSCharacters");
                if (jSONObject != null) {
                    hsCharacters = new HSCharacters(jSONObject);
                    initDone = true;
                }
            } catch (JSONException e) {
                HSLogger.w(TAG, "Error reading json : ", e);
            }
        }
    }

    public static void deinit() {
        hsCharacters = null;
        initDone = false;
    }

    public static String unidecode(String str) {
        if (!initDone) {
            init();
        }
        if (str == null || str.length() == 0) {
            return "";
        }
        int i = 0;
        while (i < str.length() && str.charAt(i) <= 128) {
            if (i >= str.length()) {
                return str;
            }
            i++;
        }
        char[] charArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : charArray) {
            if (c < 128) {
                sb.append(c);
            } else {
                int i2 = c >> 8;
                char c2 = c & 255;
                if (hsCharacters == null || !hsCharacters.containsKey(String.valueOf(i2), c2)) {
                    sb.append("");
                } else {
                    sb.append(hsCharacters.get(String.valueOf(i2), c2));
                }
            }
        }
        return sb.toString();
    }
}
