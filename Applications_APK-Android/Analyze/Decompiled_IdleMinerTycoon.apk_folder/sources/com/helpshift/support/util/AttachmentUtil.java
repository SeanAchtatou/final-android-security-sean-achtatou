package com.helpshift.support.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Size;
import com.helpshift.android.commons.downloader.HsUriUtils;
import com.helpshift.common.domain.AttachmentFileManagerDM;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.support.HSApiData;
import com.helpshift.util.FileUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.IOUtils;
import com.helpshift.util.ImageUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public final class AttachmentUtil {
    private static final int IMAGE_MAX_DIMENSION = 1024;
    private static final String TAG = "Helpshift_AttachUtil";

    @RequiresApi(28)
    private static Bitmap loadBitmap(Uri uri, final int i) {
        try {
            return ImageDecoder.decodeBitmap(ImageDecoder.createSource(HelpshiftContext.getApplicationContext().getContentResolver(), uri), new ImageDecoder.OnHeaderDecodedListener() {
                public void onHeaderDecoded(@NonNull ImageDecoder imageDecoder, @NonNull ImageDecoder.ImageInfo imageInfo, @NonNull ImageDecoder.Source source) {
                    Size size = imageInfo.getSize();
                    int width = size.getWidth();
                    int height = size.getHeight();
                    int i = 4;
                    if (i > 0 && width > 0 && height > 0) {
                        int calculateInSampleSize = ImageUtil.calculateInSampleSize(width, height, i, ImageUtil.calculateReqHeight(width, height, i));
                        if (calculateInSampleSize < 4) {
                            calculateInSampleSize++;
                        }
                        i = calculateInSampleSize;
                    }
                    imageDecoder.setTargetSampleSize(i);
                }
            });
        } catch (IOException e) {
            HSLogger.e(TAG, "Error while building bitmap from uri", e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap loadBitmap(String str, int i) {
        int exifRotation;
        if (!doesFilePathExistAndCanRead(str)) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (i > 0) {
            int calculateReqHeight = ImageUtil.calculateReqHeight(options.outWidth, options.outHeight, i);
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = ImageUtil.calculateInSampleSize(options, i, calculateReqHeight);
            if (options.inSampleSize < 4) {
                options.inSampleSize++;
            }
        } else {
            options.inSampleSize = 4;
        }
        options.inJustDecodeBounds = false;
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
        if (decodeFile == null || (exifRotation = getExifRotation(str)) == 0) {
            return decodeFile;
        }
        Matrix matrix = new Matrix();
        matrix.preRotate((float) exifRotation);
        return Bitmap.createBitmap(decodeFile, 0, 0, decodeFile.getWidth(), decodeFile.getHeight(), matrix, false);
    }

    public static Bitmap getBitmap(String str, int i) {
        Uri uri;
        if (!HsUriUtils.isValidUriPath(str)) {
            return loadBitmap(str, i);
        }
        try {
            uri = Uri.parse(str);
        } catch (Exception e) {
            HSLogger.e(TAG, "Error while converting to uri from file path", e);
            uri = null;
        }
        if (uri == null || Build.VERSION.SDK_INT < 28) {
            return null;
        }
        return loadBitmap(uri, i);
    }

    private static boolean doesFilePathExistAndCanRead(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        File file = new File(str);
        if (!file.exists() || !file.canRead()) {
            return false;
        }
        return true;
    }

    public static String copyAttachment(String str, String str2) throws IOException {
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        Context applicationContext = HelpshiftContext.getApplicationContext();
        HSApiData hSApiData = new HSApiData(applicationContext);
        FileOutputStream fileOutputStream2 = null;
        try {
            String buildLocalAttachmentCopyFileName = buildLocalAttachmentCopyFileName(str2, FileUtil.getFileExtension(str));
            File file = new File(applicationContext.getFilesDir(), buildLocalAttachmentCopyFileName);
            String absolutePath = file.getAbsolutePath();
            if (!file.exists()) {
                hSApiData.storeFile(buildLocalAttachmentCopyFileName);
                fileInputStream = new FileInputStream(new File(str));
                try {
                    fileOutputStream = applicationContext.openFileOutput(buildLocalAttachmentCopyFileName, 0);
                } catch (NullPointerException e) {
                    e = e;
                    fileOutputStream = null;
                    try {
                        HSLogger.d(TAG, "NPE", e);
                        IOUtils.closeQuitely(fileOutputStream);
                        IOUtils.closeQuitely(fileInputStream);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        fileOutputStream2 = fileOutputStream;
                        IOUtils.closeQuitely(fileOutputStream2);
                        IOUtils.closeQuitely(fileInputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeQuitely(fileOutputStream2);
                    IOUtils.closeQuitely(fileInputStream);
                    throw th;
                }
                try {
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                    if (ImageUtil.isResizableImage(absolutePath)) {
                        ImageUtil.scaleDownAndSaveWithMaxDimension(absolutePath, 1024);
                    }
                } catch (NullPointerException e2) {
                    e = e2;
                    HSLogger.d(TAG, "NPE", e);
                    IOUtils.closeQuitely(fileOutputStream);
                    IOUtils.closeQuitely(fileInputStream);
                    return null;
                }
            } else {
                fileOutputStream = null;
                fileInputStream = null;
            }
            IOUtils.closeQuitely(fileOutputStream);
            IOUtils.closeQuitely(fileInputStream);
            return absolutePath;
        } catch (NullPointerException e3) {
            e = e3;
            fileOutputStream = null;
            fileInputStream = null;
            HSLogger.d(TAG, "NPE", e);
            IOUtils.closeQuitely(fileOutputStream);
            IOUtils.closeQuitely(fileInputStream);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            IOUtils.closeQuitely(fileOutputStream2);
            IOUtils.closeQuitely(fileInputStream);
            throw th;
        }
    }

    public static void copyAttachment(@NonNull ImagePickerFile imagePickerFile, @Nullable String str) throws IOException {
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        Uri uri = (Uri) imagePickerFile.transientUri;
        if (uri == null) {
            HSLogger.d(TAG, "Can't proceed if uri is null");
            return;
        }
        Context applicationContext = HelpshiftContext.getApplicationContext();
        HSApiData hSApiData = new HSApiData(applicationContext);
        FileOutputStream fileOutputStream2 = null;
        try {
            String buildLocalAttachmentCopyFileName = buildLocalAttachmentCopyFileName(str, "." + FileUtil.getFileExtensionFromMimeType(applicationContext, uri));
            File file = new File(applicationContext.getFilesDir(), buildLocalAttachmentCopyFileName);
            String absolutePath = file.getAbsolutePath();
            if (!file.exists()) {
                hSApiData.storeFile(buildLocalAttachmentCopyFileName);
                inputStream = applicationContext.getContentResolver().openInputStream(uri);
                try {
                    fileOutputStream = applicationContext.openFileOutput(buildLocalAttachmentCopyFileName, 0);
                    try {
                        byte[] bArr = new byte[8192];
                        while (true) {
                            int read = inputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        imagePickerFile.filePath = absolutePath;
                        imagePickerFile.isFileCompressionAndCopyingDone = true;
                        if (ImageUtil.isResizableImage(absolutePath)) {
                            ImageUtil.scaleDownAndSaveWithMaxDimension(absolutePath, 1024);
                        }
                    } catch (Throwable th) {
                        th = th;
                        fileOutputStream2 = fileOutputStream;
                        IOUtils.closeQuitely(fileOutputStream2);
                        IOUtils.closeQuitely(inputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeQuitely(fileOutputStream2);
                    IOUtils.closeQuitely(inputStream);
                    throw th;
                }
            } else {
                imagePickerFile.filePath = absolutePath;
                imagePickerFile.isFileCompressionAndCopyingDone = true;
                fileOutputStream = null;
                inputStream = null;
            }
            IOUtils.closeQuitely(fileOutputStream);
            IOUtils.closeQuitely(inputStream);
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            IOUtils.closeQuitely(fileOutputStream2);
            IOUtils.closeQuitely(inputStream);
            throw th;
        }
    }

    private static String buildLocalAttachmentCopyFileName(@Nullable String str, String str2) {
        if (str == null) {
            str = AttachmentFileManagerDM.LOCAL_RSC_MESSAGE_PREFIX + UUID.randomUUID().toString();
        }
        return str + "0-thumbnail" + str2;
    }

    public static String getFileName(String str) {
        if (str != null) {
            return new File(str).getName();
        }
        return "";
    }

    private static int getExifRotation(String str) {
        try {
            String mimeType = FileUtil.getMimeType(str);
            if (mimeType != null && mimeType.contains("jpeg")) {
                int attributeInt = new ExifInterface(str).getAttributeInt("Orientation", 1);
                if (attributeInt == 6) {
                    return 90;
                }
                if (attributeInt == 3) {
                    return 180;
                }
                if (attributeInt == 8) {
                    return 270;
                }
                return 0;
            }
        } catch (Exception e) {
            HSLogger.e(TAG, "Exception in getting exif rotation", e);
        }
        return 0;
    }
}
