package com.helpshift.support;

import android.content.Context;
import android.os.Environment;
import com.facebook.appevents.AppEventsConstants;
import com.helpshift.account.dao.legacy.AndroidLegacyProfileDAO;
import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.network.NetworkConstants;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dao.ConversationDAO;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.support.storage.LegacyUserDataMigrator;
import com.helpshift.support.storage.SupportKVStoreMigrator;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.VersionName;
import java.io.File;
import java.util.HashSet;
import java.util.List;

public class SupportMigrator {
    public static final String TAG = "Helpshift_SupportMigr";

    public static void migrate(Context context, Platform platform, Domain domain, HSApiData hSApiData, HSStorage hSStorage) {
        VersionName versionName;
        Platform platform2 = platform;
        Domain domain2 = domain;
        HSStorage hSStorage2 = hSStorage;
        String libraryVersion = hSStorage.getLibraryVersion();
        if (libraryVersion.length() > 0 && !libraryVersion.equals("7.6.0")) {
            VersionName versionName2 = new VersionName(AppEventsConstants.EVENT_PARAM_VALUE_NO);
            try {
                versionName = new VersionName(libraryVersion);
            } catch (NumberFormatException e) {
                HSLogger.e(TAG, "Error in creating SemVer: " + e);
                versionName = versionName2;
            }
            if (!versionName.isGreaterThanOrEqualTo(new VersionName("7.0.0"))) {
                LegacyUserDataMigrator legacyUserDataMigrator = new LegacyUserDataMigrator(HelpshiftContext.getCoreApi(), hSStorage, platform.getKVStore(), AndroidLegacyProfileDAO.getInstance(context), platform.getBackupDAO(), platform.getLegacyUserMigrationDataSource(), platform.getLegacyAnalyticsEventIDDAO(), versionName);
                SupportKVStoreMigrator supportKVStoreMigrator = new SupportKVStoreMigrator(hSStorage2);
                legacyUserDataMigrator.backup(versionName);
                supportKVStoreMigrator.backup(versionName);
                hSApiData.clearETagsForFaqs();
                hSStorage.clearDatabase();
                legacyUserDataMigrator.dropProfileDB();
                platform.getConversationDAO().dropAndCreateDatabase();
                HelpshiftContext.getCoreApi().resetUsersSyncStatusAndStartSetupForActiveUser();
                platform.getKVStore().removeAllKeys();
                legacyUserDataMigrator.restore();
                supportKVStoreMigrator.restore();
                domain.getUserManagerDM().getActiveUserSetupDM().startSetup();
            } else {
                fixDuplicateConversations(platform2, domain2, versionName);
                updateRejectConversations(platform2, domain2, versionName);
                removeConfigApiEtag(platform2, versionName);
            }
        }
        hSStorage.clearLegacySearchIndexFile();
        deleteDBLockFilesOnSDKMigration(context);
        if (!"7.6.0".equals(libraryVersion)) {
            hSStorage2.setLibraryVersion("7.6.0");
        }
    }

    private static void removeConfigApiEtag(Platform platform, VersionName versionName) {
        if (versionName.isLessThanOrEqualTo(new VersionName("7.5.0"))) {
            platform.getNetworkRequestDAO().removeETag(NetworkConstants.SUPPORT_CONFIG_ROUTE);
        }
    }

    private static void updateRejectConversations(Platform platform, Domain domain, VersionName versionName) {
        if (versionName.isGreaterThanOrEqualTo(new VersionName("7.0.0")) && versionName.isLessThanOrEqualTo(new VersionName("7.1.0"))) {
            ConversationDAO conversationDAO = platform.getConversationDAO();
            List<UserDM> allUsers = domain.getUserManagerDM().getAllUsers();
            if (!ListUtils.isEmpty(allUsers)) {
                for (UserDM next : allUsers) {
                    List<Conversation> readConversationsWithoutMessages = conversationDAO.readConversationsWithoutMessages(next.getLocalId().longValue());
                    if (!ListUtils.isEmpty(readConversationsWithoutMessages)) {
                        for (Conversation next2 : readConversationsWithoutMessages) {
                            if (next2.state == IssueState.REJECTED && !next2.isStartNewConversationClicked) {
                                next2.userLocalId = next.getLocalId().longValue();
                                domain.getConversationInboxManagerDM().getConversationInboxDM(next).conversationManager.setStartNewConversationButtonClicked(next2, true, true);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void fixDuplicateConversations(Platform platform, Domain domain, VersionName versionName) {
        if (versionName.isGreaterThanOrEqualTo(new VersionName("7.0.0"))) {
            List<UserDM> allUsers = domain.getUserManagerDM().getAllUsers();
            ConversationDAO conversationDAO = platform.getConversationDAO();
            HashSet hashSet = new HashSet();
            HashSet hashSet2 = new HashSet();
            for (UserDM next : allUsers) {
                if (domain.getConversationInboxManagerDM().getConversationInboxDM(next).getActiveConversationFromStorage() != null) {
                    List<Conversation> readConversationsWithoutMessages = conversationDAO.readConversationsWithoutMessages(next.getLocalId().longValue());
                    if (ListUtils.isEmpty(readConversationsWithoutMessages)) {
                        continue;
                    } else {
                        for (Conversation next2 : readConversationsWithoutMessages) {
                            boolean z = true;
                            boolean z2 = !StringUtils.isEmpty(next2.preConversationServerId) && hashSet2.contains(next2.preConversationServerId);
                            if (StringUtils.isEmpty(next2.serverId) || !hashSet.contains(next2.serverId)) {
                                z = false;
                            }
                            if (z2 || z) {
                                conversationDAO.dropAndCreateDatabase();
                                HelpshiftContext.getCoreApi().resetUsersSyncStatusAndStartSetupForActiveUser();
                                return;
                            }
                            if (!StringUtils.isEmpty(next2.preConversationServerId)) {
                                hashSet2.add(next2.preConversationServerId);
                            }
                            if (!StringUtils.isEmpty(next2.serverId)) {
                                hashSet.add(next2.serverId);
                            }
                        }
                        continue;
                    }
                }
            }
        }
    }

    private static void deleteDBLockFilesOnSDKMigration(Context context) {
        try {
            File file = new File(context.getFilesDir() + File.separator + "__hs_supportkvdb_lock");
            if (file.exists()) {
                file.delete();
            }
            File file2 = new File(context.getFilesDir() + File.separator + "__hs_kvdb_lock");
            if (file2.exists()) {
                file2.delete();
            }
            File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(".backups/" + context.getPackageName() + "/helpshift/databases/");
            if (externalStoragePublicDirectory != null && externalStoragePublicDirectory.canWrite()) {
                File file3 = new File(externalStoragePublicDirectory, "__hs__db_profiles");
                if (file3.canWrite()) {
                    file3.delete();
                }
                File file4 = new File(externalStoragePublicDirectory, "__hs__kv_backup");
                if (file4.canWrite()) {
                    file4.delete();
                }
            }
        } catch (Exception e) {
            HSLogger.e(TAG, "Error on deleting lock file: " + e);
        }
    }
}
