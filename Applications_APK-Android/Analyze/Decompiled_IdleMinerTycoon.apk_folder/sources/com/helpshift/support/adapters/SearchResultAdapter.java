package com.helpshift.support.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.ContactUsFilter;
import com.helpshift.support.Faq;
import com.helpshift.support.util.HSTransliterator;
import com.helpshift.util.Styles;
import java.util.ArrayList;
import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_QUESTION = 3;
    private View.OnClickListener onContactUsClickedListener;
    private View.OnClickListener onQuestionClickedListener;
    private List<Faq> questions;

    public SearchResultAdapter(List<Faq> list, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        this.questions = list;
        this.onQuestionClickedListener = onClickListener;
        this.onContactUsClickedListener = onClickListener2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case 1:
                return new HeaderViewHolder((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__search_result_header, viewGroup, false));
            case 2:
                return new FooterViewHolder((LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__search_result_footer, viewGroup, false));
            default:
                return new QuestionViewHolder((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs_simple_recycler_view_item, viewGroup, false));
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof FooterViewHolder) {
            configureFooterViewHolder((FooterViewHolder) viewHolder);
        } else if (viewHolder instanceof QuestionViewHolder) {
            configureQuestionViewHolder((QuestionViewHolder) viewHolder, i);
        }
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 1;
        }
        return isPositionFooter(i) ? 2 : 3;
    }

    public long getItemId(int i) {
        if (i == 0) {
            return 1;
        }
        if (isPositionFooter(i)) {
            return 2;
        }
        return Long.valueOf(this.questions.get(i - 1).publish_id).longValue();
    }

    public int getItemCount() {
        return this.questions.size() + 2;
    }

    private void configureFooterViewHolder(FooterViewHolder footerViewHolder) {
        if (ContactUsFilter.showContactUs(ContactUsFilter.LOCATION.SEARCH_FOOTER)) {
            footerViewHolder.rootView.setVisibility(0);
            footerViewHolder.button.setOnClickListener(this.onContactUsClickedListener);
            return;
        }
        footerViewHolder.rootView.setVisibility(8);
    }

    private void configureQuestionViewHolder(QuestionViewHolder questionViewHolder, int i) {
        QuestionViewHolder questionViewHolder2 = questionViewHolder;
        Faq faq = this.questions.get(i - 1);
        ArrayList<String> arrayList = faq.searchTerms;
        String str = faq.title;
        if (arrayList == null || arrayList.size() <= 0) {
            questionViewHolder2.textView.setText(str);
        } else {
            int color = Styles.getColor(questionViewHolder2.textView.getContext(), R.attr.hs__searchHighlightColor);
            SpannableString spannableString = new SpannableString(str);
            if (str.equals(HSTransliterator.unidecode(str))) {
                String lowerCase = str.toLowerCase();
                for (String next : arrayList) {
                    if (next.length() >= 3) {
                        for (int indexOf = TextUtils.indexOf(lowerCase, next, 0); indexOf >= 0; indexOf = TextUtils.indexOf(lowerCase, next, indexOf + next.length())) {
                            spannableString.setSpan(new BackgroundColorSpan(color), indexOf, next.length() + indexOf, 33);
                        }
                    }
                }
            } else {
                int length = str.length();
                ArrayList arrayList2 = new ArrayList();
                String str2 = "";
                int i2 = 0;
                while (i2 < length) {
                    String unidecode = HSTransliterator.unidecode(str.charAt(i2) + "");
                    String str3 = str2;
                    for (int i3 = 0; i3 < unidecode.length(); i3++) {
                        str3 = str3 + unidecode.charAt(i3);
                        arrayList2.add(Integer.valueOf(i2));
                    }
                    i2++;
                    str2 = str3;
                }
                String lowerCase2 = str2.toLowerCase();
                for (String lowerCase3 : arrayList) {
                    String lowerCase4 = lowerCase3.toLowerCase();
                    if (lowerCase4.length() >= 3) {
                        for (int indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase4, 0); indexOf2 >= 0; indexOf2 = TextUtils.indexOf(lowerCase2, lowerCase4, indexOf2 + lowerCase4.length())) {
                            spannableString.setSpan(new BackgroundColorSpan(color), ((Integer) arrayList2.get(indexOf2)).intValue(), ((Integer) arrayList2.get((lowerCase4.length() + indexOf2) - 1)).intValue() + 1, 33);
                        }
                    }
                }
            }
            questionViewHolder2.textView.setText(spannableString);
        }
        questionViewHolder2.textView.setOnClickListener(this.onQuestionClickedListener);
        questionViewHolder2.textView.setTag(faq.publish_id);
    }

    public Faq getFaq(String str) {
        if (this.questions == null) {
            return null;
        }
        for (Faq next : this.questions) {
            if (next.publish_id.equals(str)) {
                return next;
            }
        }
        return null;
    }

    private boolean isPositionFooter(int i) {
        return i == getItemCount() - 1;
    }

    protected static class QuestionViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public QuestionViewHolder(TextView textView2) {
            super(textView2);
            this.textView = textView2;
        }
    }

    protected static class FooterViewHolder extends RecyclerView.ViewHolder {
        Button button;
        LinearLayout rootView;

        public FooterViewHolder(LinearLayout linearLayout) {
            super(linearLayout);
            this.rootView = linearLayout;
            this.button = (Button) linearLayout.findViewById(R.id.send_anyway_button);
        }
    }

    protected static class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(TextView textView) {
            super(textView);
        }
    }
}
