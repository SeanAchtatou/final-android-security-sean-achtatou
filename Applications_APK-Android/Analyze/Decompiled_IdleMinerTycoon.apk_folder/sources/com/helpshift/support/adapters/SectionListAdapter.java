package com.helpshift.support.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.support.Section;
import java.util.List;

public class SectionListAdapter extends RecyclerView.Adapter<ViewHolder> {
    private View.OnClickListener onClickListener;
    private List<Section> sections;

    public SectionListAdapter(List<Section> list, View.OnClickListener onClickListener2) {
        this.sections = list;
        this.onClickListener = onClickListener2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        TextView textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs_simple_recycler_view_item, viewGroup, false);
        textView.setOnClickListener(this.onClickListener);
        return new ViewHolder(textView);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Section section = this.sections.get(i);
        viewHolder.textView.setText(section.getTitle());
        viewHolder.textView.setTag(section.getPublishId());
    }

    public int getItemCount() {
        return this.sections.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(TextView textView2) {
            super(textView2);
            this.textView = textView2;
        }
    }
}
