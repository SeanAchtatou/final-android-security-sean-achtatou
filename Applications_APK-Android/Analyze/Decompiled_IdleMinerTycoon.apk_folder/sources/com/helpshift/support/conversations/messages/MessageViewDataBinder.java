package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.StringUtils;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.util.HSLinkify;
import com.helpshift.util.HSPattern;
import com.helpshift.util.Styles;

public abstract class MessageViewDataBinder<VH extends RecyclerView.ViewHolder, M extends MessageDM> {
    protected static final float BUBBLE_OPAGUE = 1.0f;
    protected static final float BUBBLE_TRANSLUCENT = 0.5f;
    protected Context context;
    protected MessageItemClickListener messageClickListener;

    public interface MessageItemClickListener {
        void handleAdminImageAttachmentMessageClick(AdminImageAttachmentMessageDM adminImageAttachmentMessageDM);

        void handleGenericAttachmentMessageClick(AdminAttachmentMessageDM adminAttachmentMessageDM);

        void handleOptionSelected(OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z);

        void handleReplyReviewButtonClick(RequestAppReviewMessageDM requestAppReviewMessageDM);

        void launchImagePicker(RequestScreenshotMessageDM requestScreenshotMessageDM);

        void onAdminMessageLinkClicked(String str, MessageDM messageDM);

        void onAdminSuggestedQuestionSelected(MessageDM messageDM, String str, String str2);

        void onCreateContextMenu(ContextMenu contextMenu, String str);

        void onScreenshotMessageClicked(ScreenshotMessageDM screenshotMessageDM);

        void retryMessage(int i);
    }

    public abstract void bind(VH vh, M m);

    public abstract VH createViewHolder(ViewGroup viewGroup);

    public MessageViewDataBinder(Context context2) {
        this.context = context2;
    }

    public void setMessageItemClickListener(MessageItemClickListener messageItemClickListener) {
        this.messageClickListener = messageItemClickListener;
    }

    /* access modifiers changed from: protected */
    public void linkify(TextView textView, HSLinkify.LinkClickListener linkClickListener) {
        HSLinkify.addLinks(textView, 14, linkClickListener);
        HSLinkify.addLinks(textView, HSPattern.getUrlPattern(), (String) null, (HSLinkify.MatchFilter) null, (HSLinkify.TransformFilter) null, linkClickListener);
    }

    /* access modifiers changed from: package-private */
    public String escapeHtml(String str) {
        return Html.fromHtml(str.replace("\n", "<br/>")).toString();
    }

    /* access modifiers changed from: package-private */
    public String getRedactedBodyText(String str) {
        return str + " ";
    }

    /* access modifiers changed from: protected */
    public void setAdminMessageContainerBackground(View view, UIViewState uIViewState) {
        setDrawable(view, uIViewState.isRoundedBackground() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
    }

    /* access modifiers changed from: protected */
    public void setAdminMessageSubText(TextView textView, UIViewState uIViewState, String str) {
        textView.setText(str);
        setViewVisibility(textView, uIViewState.isFooterVisible());
    }

    /* access modifiers changed from: protected */
    public void setUserMessageSubText(TextView textView, UIViewState uIViewState, String str) {
        textView.setText(str);
        setViewVisibility(textView, uIViewState.isFooterVisible());
    }

    /* access modifiers changed from: protected */
    public void setUserMessageContainerBackground(View view, UIViewState uIViewState) {
        setDrawable(view, uIViewState.isRoundedBackground() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_user, R.attr.hs__chatBubbleUserBackgroundColor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* access modifiers changed from: protected */
    public void setUserMessageLayoutMargin(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        TypedValue typedValue = new TypedValue();
        this.context.getResources().getValue(R.dimen.hs__screen_to_conversation_view_ratio, typedValue, true);
        marginLayoutParams.setMargins((int) (((float) this.context.getResources().getDisplayMetrics().widthPixels) * typedValue.getFloat() * 0.2f), marginLayoutParams.topMargin, marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
    }

    /* access modifiers changed from: protected */
    public void setViewVisibility(View view, boolean z) {
        if (z) {
            view.setVisibility(0);
        } else {
            view.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void setDrawable(View view, int i, int i2) {
        Styles.setDrawable(this.context, view, i, i2);
    }

    /* access modifiers changed from: protected */
    public String getAdminMessageContentDesciption(MessageDM messageDM) {
        String displayedAuthorName = messageDM.getDisplayedAuthorName();
        String accessbilityMessageTime = messageDM.getAccessbilityMessageTime();
        if (StringUtils.isEmpty(displayedAuthorName)) {
            return this.context.getString(R.string.hs__agent_message_voice_over, accessbilityMessageTime);
        }
        return this.context.getString(R.string.hs__agent_message_with_name_voice_over, displayedAuthorName, accessbilityMessageTime);
    }

    /* access modifiers changed from: package-private */
    public void applyRedactionStyle(TextView textView) {
        textView.setTypeface(textView.getTypeface(), 2);
        textView.setAlpha(0.55f);
    }
}
