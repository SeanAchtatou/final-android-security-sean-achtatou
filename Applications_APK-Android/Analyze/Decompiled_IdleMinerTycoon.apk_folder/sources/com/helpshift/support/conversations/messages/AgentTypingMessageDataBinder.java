package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.helpshift.R;
import com.helpshift.support.util.Styles;

public class AgentTypingMessageDataBinder {
    private Context context;

    AgentTypingMessageDataBinder(Context context2) {
        this.context = context2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder createViewHolder(ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.context).inflate(R.layout.hs__msg_agent_typing, viewGroup, false);
        Styles.setAdminChatBubbleColor(this.context, inflate.findViewById(R.id.agent_typing_container).getBackground());
        return new RecyclerView.ViewHolder(inflate) {
        };
    }
}
