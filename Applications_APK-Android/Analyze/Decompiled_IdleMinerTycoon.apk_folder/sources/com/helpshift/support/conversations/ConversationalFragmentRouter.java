package com.helpshift.support.conversations;

import com.helpshift.conversation.viewmodel.OptionUIModel;

public interface ConversationalFragmentRouter {
    void handleOptionSelectedForPicker(OptionUIModel optionUIModel, boolean z);

    void onListPickerSearchQueryChange(String str);

    void resetToolbarImportanceForAccessibility();

    void setToolbarImportanceForAccessibility(int i);
}
