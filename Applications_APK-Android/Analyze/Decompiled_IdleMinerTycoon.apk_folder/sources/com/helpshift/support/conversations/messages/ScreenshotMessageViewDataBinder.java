package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.support.util.Styles;
import com.helpshift.support.views.HSRoundedImageView;
import com.helpshift.util.TextUtils;

public class ScreenshotMessageViewDataBinder extends MessageViewDataBinder<ViewHolder, ScreenshotMessageDM> {
    public ScreenshotMessageViewDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(this.context).inflate(R.layout.hs__msg_screenshot_status, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, ScreenshotMessageDM screenshotMessageDM) {
        String str;
        boolean z;
        ViewHolder viewHolder2;
        boolean z2;
        boolean z3;
        int i;
        String str2;
        View.OnClickListener onClickListener;
        ViewHolder viewHolder3 = viewHolder;
        final ScreenshotMessageDM screenshotMessageDM2 = screenshotMessageDM;
        String filePath = screenshotMessageDM.getFilePath();
        int color = Styles.getColor(this.context, 16842808);
        boolean z4 = !TextUtils.isEmpty(filePath);
        float f = 0.5f;
        switch (screenshotMessageDM2.state) {
            case UNSENT_RETRYABLE:
                str2 = this.context.getResources().getString(R.string.hs__sending_fail_msg);
                viewHolder2 = viewHolder3;
                i = Styles.getColor(this.context, R.attr.hs__errorTextColor);
                str = this.context.getString(R.string.hs__user_failed_message_voice_over);
                z3 = true;
                z2 = false;
                z = false;
                break;
            case UNSENT_NOT_RETRYABLE:
                str2 = this.context.getResources().getString(R.string.hs__sending_fail_msg);
                i = Styles.getColor(this.context, R.attr.hs__errorTextColor);
                str = this.context.getString(R.string.hs__user_failed_message_voice_over);
                z3 = false;
                z2 = false;
                viewHolder2 = null;
                z = false;
                break;
            case SENDING:
                i = color;
                str2 = this.context.getResources().getString(R.string.hs__sending_msg);
                str = this.context.getString(R.string.hs__user_sending_message_voice_over);
                z3 = false;
                z2 = true;
                viewHolder2 = null;
                z = false;
                break;
            case SENT:
                f = 1.0f;
                String subText = screenshotMessageDM.getSubText();
                z2 = TextUtils.isEmpty(filePath);
                str = this.context.getString(R.string.hs__user_sent_message_voice_over, screenshotMessageDM.getAccessbilityMessageTime());
                z = !z2;
                viewHolder2 = null;
                i = color;
                str2 = subText;
                z3 = false;
                break;
            default:
                i = color;
                str = "";
                str2 = null;
                z3 = false;
                z2 = false;
                viewHolder2 = null;
                z = false;
                break;
        }
        UIViewState uiViewState = screenshotMessageDM.getUiViewState();
        viewHolder3.roundedImageView.loadImage(filePath);
        viewHolder3.roundedImageView.setAlpha(f);
        setViewVisibility(viewHolder3.roundedImageView, z4);
        viewHolder3.subText.setVisibility(0);
        if (uiViewState.isFooterVisible()) {
            viewHolder3.subText.setText(str2);
            viewHolder3.subText.setTextColor(i);
        }
        setViewVisibility(viewHolder3.subText, uiViewState.isFooterVisible());
        setViewVisibility(viewHolder.progress, z2);
        setViewVisibility(viewHolder3.retryButton, z3);
        if (z3) {
            viewHolder3.retryButton.setOnClickListener(viewHolder2);
            onClickListener = null;
        } else {
            onClickListener = null;
            viewHolder3.retryButton.setOnClickListener(null);
        }
        if (z) {
            viewHolder3.roundedImageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ScreenshotMessageViewDataBinder.this.messageClickListener.onScreenshotMessageClicked(screenshotMessageDM2);
                }
            });
        } else {
            viewHolder3.roundedImageView.setOnClickListener(onClickListener);
        }
        viewHolder3.messageLayout.setContentDescription(str);
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final View messageLayout;
        /* access modifiers changed from: private */
        public final ProgressBar progress;
        final ImageView retryButton;
        final HSRoundedImageView roundedImageView;
        final TextView subText;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.user_image_message_layout);
            this.progress = (ProgressBar) view.findViewById(R.id.upload_attachment_progressbar);
            this.roundedImageView = (HSRoundedImageView) view.findViewById(R.id.user_attachment_imageview);
            this.subText = (TextView) view.findViewById(R.id.date);
            this.retryButton = (ImageView) view.findViewById(R.id.user_message_retry_button);
            Styles.setAccentColor(ScreenshotMessageViewDataBinder.this.context, this.progress.getIndeterminateDrawable());
        }

        public void onClick(View view) {
            if (ScreenshotMessageViewDataBinder.this.messageClickListener != null) {
                ScreenshotMessageViewDataBinder.this.messageClickListener.retryMessage(getAdapterPosition());
            }
        }
    }
}
