package com.helpshift.support.conversations;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;
import android.view.View;
import com.helpshift.R;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.fragments.IMenuItemEventListener;
import com.helpshift.support.fragments.MainFragment;
import com.helpshift.support.fragments.SupportFragment;
import com.helpshift.support.storage.IMAppSessionStorage;
import com.helpshift.support.util.AppSessionConstants;
import com.helpshift.support.util.KeyboardUtil;
import com.helpshift.support.util.PermissionUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.views.HSSnackbar;

public abstract class BaseConversationFragment extends MainFragment implements MenuItem.OnMenuItemClickListener, IMenuItemEventListener {
    public static final int REQUEST_READ_STORAGE_PERMISSION_REQUEST_ID = 2;
    public static final int REQUEST_WRITE_STORAGE_PERMISSION_REQUEST_ID = 3;
    private static final String TAG = "Helpshift_BaseConvFrag";
    private Snackbar permissionDeniedSnackbar;
    private Snackbar showRationaleSnackbar;

    /* access modifiers changed from: protected */
    public abstract int getScreenshotMode();

    /* access modifiers changed from: protected */
    public abstract String getToolbarTitle();

    /* access modifiers changed from: protected */
    public abstract AppSessionConstants.Screen getViewName();

    public boolean onMenuItemClick(MenuItem menuItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract void onPermissionGranted(int i);

    public boolean shouldRefreshMenu() {
        return true;
    }

    public void onStart() {
        super.onStart();
        IMAppSessionStorage.getInstance().set(AppSessionConstants.CURRENT_OPEN_SCREEN, getViewName());
    }

    public void onPause() {
        if (this.showRationaleSnackbar != null && this.showRationaleSnackbar.isShown()) {
            this.showRationaleSnackbar.dismiss();
        }
        if (this.permissionDeniedSnackbar != null && this.permissionDeniedSnackbar.isShown()) {
            this.permissionDeniedSnackbar.dismiss();
        }
        super.onPause();
    }

    public void onStop() {
        AppSessionConstants.Screen screen = (AppSessionConstants.Screen) IMAppSessionStorage.getInstance().get(AppSessionConstants.CURRENT_OPEN_SCREEN);
        if (screen != null && screen.equals(getViewName())) {
            IMAppSessionStorage.getInstance().removeKey(AppSessionConstants.CURRENT_OPEN_SCREEN);
        }
        setToolbarTitle(getString(R.string.hs__help_header));
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public SupportFragment getSupportFragment() {
        return (SupportFragment) getParentFragment();
    }

    /* access modifiers changed from: protected */
    public SupportController getSupportController() {
        return getSupportFragment().getSupportController();
    }

    /* access modifiers changed from: protected */
    public void requestWriteExternalStoragePermission(boolean z) {
        requestPermission(z, 3);
    }

    public void requestPermission(boolean z, int i) {
        String str;
        switch (i) {
            case 2:
                str = "android.permission.READ_EXTERNAL_STORAGE";
                break;
            case 3:
                str = "android.permission.WRITE_EXTERNAL_STORAGE";
                break;
            default:
                str = null;
                break;
        }
        if (z && str != null) {
            KeyboardUtil.hideKeyboard(getContext(), getView());
            this.showRationaleSnackbar = PermissionUtil.requestPermissions(getParentFragment(), new String[]{str}, i, getView());
        } else if (!isDetached()) {
            SnackbarUtil.showSnackbar(getView(), R.string.hs__permission_not_granted, -1);
        }
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        boolean z = false;
        if (iArr.length == 1 && iArr[0] == 0) {
            z = true;
        }
        HSLogger.d(TAG, "onRequestPermissionsResult : request: " + i + ", result: " + z);
        if (z) {
            onPermissionGranted(i);
            return;
        }
        this.permissionDeniedSnackbar = HSSnackbar.make(getView(), R.string.hs__permission_denied_message, -1).setAction(R.string.hs__permission_denied_snackbar_action, new View.OnClickListener() {
            public void onClick(View view) {
                PermissionUtil.showSettingsPage(BaseConversationFragment.this.getContext());
            }
        });
        this.permissionDeniedSnackbar.show();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getSupportFragment().resetNewMessageCount();
    }

    public void onViewCreated(View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        getSupportFragment().registerToolbarMenuEventsListener(this);
    }

    public void onResume() {
        super.onResume();
        setToolbarTitle(getToolbarTitle());
    }

    public void onDestroyView() {
        SnackbarUtil.hideSnackbar(getView());
        getSupportFragment().unRegisterToolbarMenuEventsListener(this);
        super.onDestroyView();
    }
}
