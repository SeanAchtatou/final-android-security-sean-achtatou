package com.helpshift.support.conversations;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStub;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.StringUtils;
import com.helpshift.conversation.activeconversation.ConversationalRenderer;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.viewmodel.ConversationalVM;
import com.helpshift.conversation.viewmodel.OptionUIModel;
import com.helpshift.network.connectivity.HSConnectivityManager;
import com.helpshift.network.connectivity.HSNetworkConnectivityCallback;
import com.helpshift.support.fragments.SingleQuestionFragment;
import com.helpshift.support.fragments.SupportFragment;
import com.helpshift.util.HelpshiftContext;
import java.util.HashMap;

public class ConversationalFragment extends ConversationFragment implements ConversationalFragmentRouter, HSNetworkConnectivityCallback {
    private static final String TAG = "Helpshift_ConvalFrag";
    private boolean shouldShowConversationHistory;

    public static ConversationalFragment newInstance(Bundle bundle) {
        ConversationalFragment conversationalFragment = new ConversationalFragment();
        conversationalFragment.setArguments(bundle);
        return conversationalFragment;
    }

    public void onViewCreated(View view, Bundle bundle) {
        boolean z;
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.shouldShowConversationHistory = arguments.getBoolean(ConversationFragment.BUNDLE_ARG_SHOW_CONVERSATION_HISTORY);
            z = arguments.getBoolean(ConversationalVM.CREATE_NEW_PRE_ISSUE);
        } else {
            z = false;
        }
        super.onViewCreated(view, bundle);
        if (z && bundle == null) {
            this.conversationVM.forceClickOnNewConversationButton();
        }
    }

    /* access modifiers changed from: protected */
    public void inflateReplyBoxView(View view) {
        ViewStub viewStub = (ViewStub) view.findViewById(R.id.replyBoxViewStub);
        viewStub.setLayoutResource(R.layout.hs__conversational_labelledreplyboxview);
        viewStub.inflate();
    }

    /* access modifiers changed from: protected */
    public void initConversationVM() {
        this.conversationVM = HelpshiftContext.getCoreApi().getConversationalViewModel(this.shouldShowConversationHistory, this.conversationId, (ConversationalRenderer) this.renderer, this.retainMessageBoxOnUI);
    }

    /* access modifiers changed from: protected */
    public void initRenderer(RecyclerView recyclerView, View view, View view2, View view3) {
        this.renderer = new ConversationalFragmentRenderer(getContext(), recyclerView, getView(), view, this, view2, view3, getSupportFragment(), this);
    }

    public void onResume() {
        super.onResume();
        HSConnectivityManager.getInstance().registerNetworkConnectivityListener(this);
        HelpshiftContext.getCoreApi().getAutoRetryFailedEventDM().resetBackoff();
        Conversation activeConversation = this.conversationVM.viewableConversation.getActiveConversation();
        String str = activeConversation.serverId;
        String str2 = activeConversation.preConversationServerId;
        if (StringUtils.isEmpty(str)) {
            HashMap hashMap = null;
            if (!StringUtils.isEmpty(str2)) {
                hashMap = new HashMap();
                hashMap.put(AnalyticsEventKey.PREISSUE_ID, str2);
            }
            this.conversationVM.pushAnalyticsEvent(AnalyticsEventType.REPORTED_ISSUE, hashMap);
        }
        HelpshiftContext.getCoreApi().getAutoRetryFailedEventDM().sendEventForcefully(AutoRetryFailedEventDM.EventType.CONVERSATION);
    }

    public void onPause() {
        HSConnectivityManager.getInstance().unregisterNetworkConnectivityListener(this);
        super.onPause();
    }

    public void handleOptionSelected(OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z) {
        ((ConversationalVM) this.conversationVM).handleOptionSelected(optionInputMessageDM, option, z);
    }

    public void onAdminSuggestedQuestionSelected(final MessageDM messageDM, final String str, String str2) {
        getSupportController().onAdminSuggestedQuestionSelected(str, str2, new SingleQuestionFragment.QuestionReadListener() {
            public void onQuestionRead(String str) {
                ((ConversationalVM) ConversationalFragment.this.conversationVM).handleAdminSuggestedQuestionRead(messageDM, str, str);
            }
        });
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        ((ConversationalRenderer) this.renderer).hideReplyValidationFailedError();
        super.onTextChanged(charSequence, i, i2, i3);
    }

    public void onStartNewConversationButtonClick() {
        this.conversationVM.onNewConversationButtonClicked();
    }

    public void onListPickerSearchQueryChange(String str) {
        ((ConversationalVM) this.conversationVM).onListPickerSearchQueryChange(str);
    }

    public void handleOptionSelectedForPicker(OptionUIModel optionUIModel, boolean z) {
        ((ConversationalVM) this.conversationVM).handleOptionSelectedForPicker(optionUIModel, z);
    }

    public void setToolbarImportanceForAccessibility(int i) {
        SupportFragment supportFragment = getSupportFragment();
        if (supportFragment != null) {
            supportFragment.setToolbarImportanceForAccessibility(i);
        }
    }

    public void resetToolbarImportanceForAccessibility() {
        SupportFragment supportFragment = getSupportFragment();
        if (supportFragment != null) {
            supportFragment.resetToolbarImportanceForAccessibility();
        }
    }

    public boolean onBackPressed() {
        return ((ConversationalRenderer) this.renderer).onBackPressed();
    }

    public void onFocusChanged(boolean z) {
        if (this.renderer != null) {
            ((ConversationalRenderer) this.renderer).onFocusChanged(z);
        }
    }

    public void onNetworkAvailable() {
        ((ConversationalVM) this.conversationVM).onNetworkAvailable();
    }

    public void onNetworkUnavailable() {
        ((ConversationalVM) this.conversationVM).onNetworkUnAvailable();
    }
}
