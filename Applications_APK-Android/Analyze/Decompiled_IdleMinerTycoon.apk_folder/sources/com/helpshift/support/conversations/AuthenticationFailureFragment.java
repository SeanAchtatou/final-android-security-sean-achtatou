package com.helpshift.support.conversations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.helpshift.R;
import com.helpshift.support.fragments.MainFragment;
import com.helpshift.util.Styles;

public class AuthenticationFailureFragment extends MainFragment {
    public static final String FRAGMENT_TAG = "HSAuthenticationFailureFragment";

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static AuthenticationFailureFragment newInstance() {
        return new AuthenticationFailureFragment();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__authentication_failure_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, @Nullable Bundle bundle) {
        Styles.setColorFilter(getContext(), ((ImageView) view.findViewById(R.id.info_icon)).getDrawable(), 16842806);
        super.onViewCreated(view, bundle);
    }

    public void onResume() {
        super.onResume();
        setToolbarTitle(getString(R.string.hs__conversation_header));
    }
}
