package com.helpshift.support.conversations;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.helpshift.R;
import com.helpshift.common.StringUtils;
import com.helpshift.common.exception.ExceptionType;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.viewmodel.NewConversationRenderer;
import com.helpshift.support.fragments.HSMenuItemType;
import com.helpshift.support.fragments.IToolbarMenuItemRenderer;
import com.helpshift.support.model.AttachmentFileSize;
import com.helpshift.support.util.AttachmentUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.views.HSToast;
import com.helpshift.widget.TextViewState;
import java.util.ArrayList;

public class NewConversationFragmentRenderer implements NewConversationRenderer {
    private final ImageButton attachmentClearButton;
    private final CardView attachmentContainer;
    private final TextView attachmentFileName;
    private final TextView attachmentFileSize;
    private final ImageView attachmentImage;
    private final Context context;
    private final TextInputEditText descriptionField;
    private final TextInputLayout descriptionFieldWrapper;
    private final TextInputEditText emailField;
    private final TextInputLayout emailFieldWrapper;
    private final IToolbarMenuItemRenderer menuItemRenderer;
    private final TextInputEditText nameField;
    private final TextInputLayout nameFieldWrapper;
    private final NewConversationRouter newConversationRouter;
    private final View parentView;
    private final ProgressBar progressBar;

    public void disableImageAttachmentClickable() {
    }

    public void enableImageAttachmentClickable() {
    }

    NewConversationFragmentRenderer(Context context2, TextInputLayout textInputLayout, TextInputEditText textInputEditText, TextInputLayout textInputLayout2, TextInputEditText textInputEditText2, TextInputLayout textInputLayout3, TextInputEditText textInputEditText3, ProgressBar progressBar2, ImageView imageView, TextView textView, TextView textView2, CardView cardView, ImageButton imageButton, View view, NewConversationRouter newConversationRouter2, IToolbarMenuItemRenderer iToolbarMenuItemRenderer) {
        this.context = context2;
        this.descriptionFieldWrapper = textInputLayout;
        this.descriptionField = textInputEditText;
        this.nameFieldWrapper = textInputLayout2;
        this.nameField = textInputEditText2;
        this.emailFieldWrapper = textInputLayout3;
        this.emailField = textInputEditText3;
        this.progressBar = progressBar2;
        this.attachmentImage = imageView;
        this.attachmentFileName = textView;
        this.attachmentFileSize = textView2;
        this.attachmentContainer = cardView;
        this.attachmentClearButton = imageButton;
        this.parentView = view;
        this.newConversationRouter = newConversationRouter2;
        this.menuItemRenderer = iToolbarMenuItemRenderer;
    }

    private String getText(int i) {
        return this.context.getText(i).toString();
    }

    public void showDescriptionEmptyError() {
        setError(this.descriptionFieldWrapper, getText(R.string.hs__conversation_detail_error));
    }

    public void showDescriptionLessThanMinimumError() {
        setError(this.descriptionFieldWrapper, getText(R.string.hs__description_invalid_length_error));
    }

    public void showDescriptionOnlySpecialCharactersError() {
        setError(this.descriptionFieldWrapper, getText(R.string.hs__invalid_description_error));
    }

    public void clearDescriptionError() {
        setError(this.descriptionFieldWrapper, null);
    }

    public void showNameEmptyError() {
        setError(this.nameFieldWrapper, getText(R.string.hs__username_blank_error));
    }

    public void showNameOnlySpecialCharactersError() {
        setError(this.nameFieldWrapper, getText(R.string.hs__username_blank_error));
    }

    public void clearNameError() {
        setError(this.nameFieldWrapper, null);
    }

    public void showEmailInvalidError() {
        setError(this.emailFieldWrapper, getText(R.string.hs__invalid_email_error));
    }

    public void showEmailEmptyError() {
        setError(this.emailFieldWrapper, getText(R.string.hs__invalid_email_error));
    }

    public void clearEmailError() {
        setError(this.emailFieldWrapper, null);
    }

    public void hideImageAttachmentButton() {
        changeMenuItemVisibility(HSMenuItemType.SCREENSHOT_ATTACHMENT, false);
    }

    public void showImageAttachmentButton() {
        changeMenuItemVisibility(HSMenuItemType.SCREENSHOT_ATTACHMENT, true);
    }

    public void updateImageAttachmentButton(boolean z) {
        changeMenuItemVisibility(HSMenuItemType.SCREENSHOT_ATTACHMENT, z);
    }

    public void showImageAttachmentContainer(@NonNull String str, String str2, Long l) {
        Bitmap bitmap = AttachmentUtil.getBitmap(str, -1);
        if (bitmap != null) {
            this.attachmentImage.setImageBitmap(bitmap);
            TextView textView = this.attachmentFileName;
            if (str2 == null) {
                str2 = "";
            }
            textView.setText(str2);
            String str3 = "";
            if (l != null) {
                str3 = new AttachmentFileSize((double) l.longValue()).getFormattedFileSize();
            }
            this.attachmentFileSize.setText(str3);
            this.attachmentImage.setVisibility(0);
            this.attachmentClearButton.setVisibility(0);
            this.attachmentContainer.setVisibility(0);
        }
    }

    public void hideImageAttachmentContainer() {
        this.attachmentContainer.setVisibility(8);
        this.attachmentImage.setVisibility(8);
        this.attachmentClearButton.setVisibility(8);
    }

    public void updateImageAttachmentPickerFile(ImagePickerFile imagePickerFile) {
        if (imagePickerFile == null || StringUtils.isEmpty(imagePickerFile.filePath)) {
            hideImageAttachmentContainer();
        } else {
            showImageAttachmentContainer(imagePickerFile.filePath, imagePickerFile.originalFileName, imagePickerFile.originalFileSize);
        }
    }

    public void updateImageAttachmentClick(boolean z) {
        if (z) {
            enableImageAttachmentClickable();
        } else {
            disableImageAttachmentClickable();
        }
    }

    public void setDescription(String str) {
        this.descriptionField.setText(str);
        this.descriptionField.setSelection(this.descriptionField.getText().length());
    }

    public void setName(String str) {
        this.nameField.setText(str);
        this.nameField.setSelection(this.nameField.getText().length());
    }

    public void setEmail(String str) {
        this.emailField.setText(str);
        this.emailField.setSelection(this.emailField.getText().length());
    }

    public void showProfileForm() {
        this.nameField.setVisibility(0);
        this.emailField.setVisibility(0);
    }

    public void hideProfileForm() {
        this.nameField.setVisibility(8);
        this.emailField.setVisibility(8);
    }

    public void setEmailRequired() {
        this.emailField.setHint(getText(R.string.hs__email_required_hint));
    }

    public void updateProfileForm(boolean z) {
        if (z) {
            showProfileForm();
        } else {
            hideProfileForm();
        }
    }

    public void updateStartConversationButton(boolean z) {
        changeMenuItemVisibility(HSMenuItemType.START_NEW_CONVERSATION, z);
    }

    public void updateDescriptionErrorState(TextViewState.TextViewStatesError textViewStatesError) {
        if (TextViewState.TextViewStatesError.EMPTY.equals(textViewStatesError)) {
            showDescriptionEmptyError();
        } else if (TextViewState.TextViewStatesError.ONLY_SPECIAL_CHARACTERS.equals(textViewStatesError)) {
            showDescriptionOnlySpecialCharactersError();
        } else if (TextViewState.TextViewStatesError.LESS_THAN_MINIMUM_LENGTH.equals(textViewStatesError)) {
            showDescriptionLessThanMinimumError();
        } else {
            clearDescriptionError();
        }
    }

    public void updateNameErrorState(TextViewState.TextViewStatesError textViewStatesError) {
        if (TextViewState.TextViewStatesError.EMPTY.equals(textViewStatesError)) {
            showNameEmptyError();
        } else if (TextViewState.TextViewStatesError.ONLY_SPECIAL_CHARACTERS.equals(textViewStatesError)) {
            showNameOnlySpecialCharactersError();
        } else {
            clearNameError();
        }
    }

    public void updateEmailErrorState(TextViewState.TextViewStatesError textViewStatesError, boolean z) {
        if (TextViewState.TextViewStatesError.INVALID_EMAIL.equals(textViewStatesError)) {
            showEmailInvalidError();
        } else if (TextViewState.TextViewStatesError.EMPTY.equals(textViewStatesError)) {
            showEmailEmptyError();
        } else {
            clearEmailError();
        }
        if (z) {
            setEmailRequired();
        }
    }

    public void showStartConversationButton() {
        changeMenuItemVisibility(HSMenuItemType.START_NEW_CONVERSATION, true);
    }

    public void hideStartConversationButton() {
        changeMenuItemVisibility(HSMenuItemType.START_NEW_CONVERSATION, false);
    }

    public void gotoConversation(long j) {
        this.newConversationRouter.showConversationScreen();
    }

    public void exit() {
        this.newConversationRouter.exitNewConversationView();
    }

    public void showAttachmentPreviewScreenFromDraft(ImagePickerFile imagePickerFile) {
        this.newConversationRouter.showAttachmentPreviewScreenFromDraft(imagePickerFile);
    }

    public void showProgressBar() {
        this.progressBar.setVisibility(0);
    }

    public void updateProgressBarVisibility(boolean z) {
        if (z) {
            showProgressBar();
        } else {
            hideProgressBar();
        }
    }

    public void hideProgressBar() {
        this.progressBar.setVisibility(8);
    }

    public void showConversationStartedMessage() {
        Toast makeText = HSToast.makeText(this.context, R.string.hs__conversation_started_message, 0);
        makeText.setGravity(16, 0, 0);
        makeText.show();
    }

    public void showSearchResultFragment(ArrayList arrayList) {
        this.newConversationRouter.showSearchResultFragment(arrayList);
    }

    public void showErrorView(ExceptionType exceptionType) {
        SnackbarUtil.showSnackbar(exceptionType, this.parentView);
    }

    public void onAuthenticationFailure() {
        this.newConversationRouter.onAuthenticationFailure();
    }

    private void setError(TextInputLayout textInputLayout, CharSequence charSequence) {
        textInputLayout.setErrorEnabled(!TextUtils.isEmpty(charSequence));
        textInputLayout.setError(charSequence);
    }

    private void changeMenuItemVisibility(HSMenuItemType hSMenuItemType, boolean z) {
        if (this.menuItemRenderer != null) {
            this.menuItemRenderer.updateMenuItemVisibility(hSMenuItemType, z);
        }
    }
}
