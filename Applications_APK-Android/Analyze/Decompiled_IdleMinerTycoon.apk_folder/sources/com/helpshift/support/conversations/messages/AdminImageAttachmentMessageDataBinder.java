package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.support.views.HSRoundedImageView;
import com.helpshift.util.Styles;

class AdminImageAttachmentMessageDataBinder extends MessageViewDataBinder<ViewHolder, AdminImageAttachmentMessageDM> {
    AdminImageAttachmentMessageDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(this.context).inflate(R.layout.hs__msg_attachment_image, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, final AdminImageAttachmentMessageDM adminImageAttachmentMessageDM) {
        String str;
        boolean z;
        String str2;
        String str3;
        boolean z2;
        boolean z3;
        boolean z4;
        String str4;
        String formattedFileSize = adminImageAttachmentMessageDM.getFormattedFileSize();
        boolean z5 = false;
        switch (adminImageAttachmentMessageDM.state) {
            case DOWNLOAD_NOT_STARTED:
                str4 = this.context.getString(R.string.hs__image_not_downloaded_voice_over, adminImageAttachmentMessageDM.getFormattedFileSize());
                str = str4;
                str2 = formattedFileSize;
                str3 = null;
                z4 = false;
                z3 = true;
                z2 = true;
                z5 = true;
                z = true;
                break;
            case THUMBNAIL_DOWNLOADING:
                str4 = this.context.getString(R.string.hs__image_not_downloaded_voice_over, adminImageAttachmentMessageDM.getFormattedFileSize());
                str = str4;
                str2 = formattedFileSize;
                str3 = null;
                z4 = false;
                z3 = true;
                z2 = true;
                z5 = true;
                z = true;
                break;
            case THUMBNAIL_DOWNLOADED:
                String checkAndGetThumbnailFilePath = adminImageAttachmentMessageDM.checkAndGetThumbnailFilePath();
                str3 = checkAndGetThumbnailFilePath;
                str2 = formattedFileSize;
                str = this.context.getString(R.string.hs__image_not_downloaded_voice_over, adminImageAttachmentMessageDM.getFormattedFileSize());
                z4 = false;
                z3 = true;
                z2 = true;
                z5 = true;
                z = true;
                break;
            case IMAGE_DOWNLOADING:
                String checkAndGetThumbnailFilePath2 = adminImageAttachmentMessageDM.checkAndGetThumbnailFilePath();
                String downloadProgressAndFileSize = adminImageAttachmentMessageDM.getDownloadProgressAndFileSize();
                str3 = checkAndGetThumbnailFilePath2;
                str2 = downloadProgressAndFileSize;
                str = this.context.getString(R.string.hs__image_downloading_voice_over, adminImageAttachmentMessageDM.getDownloadedProgressSize(), adminImageAttachmentMessageDM.getFormattedFileSize());
                z4 = true;
                z3 = false;
                z2 = true;
                z5 = true;
                z = false;
                break;
            case IMAGE_DOWNLOADED:
                str3 = adminImageAttachmentMessageDM.checkAndGetFilePath();
                str2 = formattedFileSize;
                str = this.context.getString(R.string.hs__image_downloaded_voice_over);
                z4 = false;
                z3 = false;
                z2 = false;
                z = true;
                break;
            default:
                str = "";
                str2 = formattedFileSize;
                str3 = null;
                z4 = true;
                z3 = false;
                z2 = true;
                z5 = true;
                z = true;
                break;
        }
        setViewVisibility(viewHolder.downloadProgressbarContainer, z5);
        setViewVisibility(viewHolder.progressBarView, z4);
        setViewVisibility(viewHolder.downloadButtonView, z3);
        if (z2) {
            viewHolder.roundedImageView.setAlpha(0.25f);
        } else {
            viewHolder.roundedImageView.setAlpha(1.0f);
        }
        setViewVisibility(viewHolder.fileSize, true);
        viewHolder.roundedImageView.loadImage(str3);
        UIViewState uiViewState = adminImageAttachmentMessageDM.getUiViewState();
        if (uiViewState.isFooterVisible()) {
            viewHolder.subText.setText(adminImageAttachmentMessageDM.getSubText());
        }
        setViewVisibility(viewHolder.subText, uiViewState.isFooterVisible());
        viewHolder.fileSize.setText(str2);
        AnonymousClass1 r0 = new View.OnClickListener() {
            public void onClick(View view) {
                if (AdminImageAttachmentMessageDataBinder.this.messageClickListener != null) {
                    AdminImageAttachmentMessageDataBinder.this.messageClickListener.handleAdminImageAttachmentMessageClick(adminImageAttachmentMessageDM);
                }
            }
        };
        if (z3) {
            viewHolder.downloadButtonView.setOnClickListener(r0);
        } else {
            viewHolder.downloadButtonView.setOnClickListener(null);
        }
        if (z) {
            viewHolder.roundedImageView.setOnClickListener(r0);
        } else {
            viewHolder.roundedImageView.setOnClickListener(null);
        }
        viewHolder.roundedImageView.setContentDescription(str);
        viewHolder.messageLayout.setContentDescription(getAdminMessageContentDesciption(adminImageAttachmentMessageDM));
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder {
        final View downloadButtonView;
        final View downloadProgressbarContainer;
        final TextView fileSize;
        final View messageLayout;
        final ProgressBar progressBarView;
        final HSRoundedImageView roundedImageView;
        final TextView subText;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.admin_image_message_layout);
            this.roundedImageView = (HSRoundedImageView) view.findViewById(R.id.admin_attachment_imageview);
            this.downloadButtonView = view.findViewById(R.id.download_button);
            this.downloadProgressbarContainer = view.findViewById(R.id.download_progressbar_container);
            this.progressBarView = (ProgressBar) view.findViewById(R.id.download_attachment_progressbar);
            this.fileSize = (TextView) view.findViewById(R.id.attachment_file_size);
            this.subText = (TextView) view.findViewById(R.id.date);
            Styles.setColorFilter(AdminImageAttachmentMessageDataBinder.this.context, ((ImageView) view.findViewById(R.id.hs_download_foreground_view)).getDrawable(), R.attr.hs__chatBubbleMediaBackgroundColor);
            com.helpshift.support.util.Styles.setAccentColor(AdminImageAttachmentMessageDataBinder.this.context, this.progressBarView.getIndeterminateDrawable());
        }
    }
}
