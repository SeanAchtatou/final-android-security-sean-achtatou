package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.SystemRedactedConversationMessageDM;

public class SystemRedactedConversationDataBinder extends MessageViewDataBinder<ViewHolder, SystemRedactedConversationMessageDM> {
    public SystemRedactedConversationDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_system_conversation_redacted_layout, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, SystemRedactedConversationMessageDM systemRedactedConversationMessageDM) {
        String str;
        int i = systemRedactedConversationMessageDM.contiguousRedactedConversationsCount;
        if (systemRedactedConversationMessageDM.contiguousRedactedConversationsCount > 1) {
            str = this.context.getString(R.string.hs__conversation_redacted_status_multiple, Integer.valueOf(i));
        } else {
            str = this.context.getString(R.string.hs__conversation_redacted_status);
        }
        viewHolder.redactedTextView.setText(str);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */
        public TextView redactedTextView;

        public ViewHolder(View view) {
            super(view);
            this.redactedTextView = (TextView) view.findViewById(R.id.conversation_redacted_view);
        }
    }
}
