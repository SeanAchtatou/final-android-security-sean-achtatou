package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.SystemDateMessageDM;
import com.helpshift.util.Styles;

public class SystemDateMessageDataBinder extends MessageViewDataBinder<ViewHolder, SystemDateMessageDM> {
    public SystemDateMessageDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_system_layout, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, SystemDateMessageDM systemDateMessageDM) {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) viewHolder.itemView.getLayoutParams();
        if (systemDateMessageDM.isFirstMessageInList) {
            layoutParams.topMargin = (int) Styles.dpToPx(this.context, 18.0f);
        } else {
            layoutParams.topMargin = (int) Styles.dpToPx(this.context, 2.0f);
        }
        viewHolder.itemView.setLayoutParams(layoutParams);
        viewHolder.body.setText(systemDateMessageDM.getBodyText());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */
        public final TextView body;

        public ViewHolder(View view) {
            super(view);
            this.body = (TextView) view.findViewById(R.id.system_message);
        }
    }
}
