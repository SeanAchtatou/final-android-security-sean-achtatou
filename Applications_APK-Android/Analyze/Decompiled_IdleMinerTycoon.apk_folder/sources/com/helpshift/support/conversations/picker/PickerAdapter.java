package com.helpshift.support.conversations.picker;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.ListUtils;
import com.helpshift.conversation.viewmodel.HSRange;
import com.helpshift.conversation.viewmodel.OptionUIModel;
import com.helpshift.support.conversations.ConversationalFragmentRouter;
import com.helpshift.util.Styles;
import java.util.List;

public class PickerAdapter extends RecyclerView.Adapter<ViewHolder> {
    ConversationalFragmentRouter conversationalFragmentRouter;
    /* access modifiers changed from: private */
    public List<OptionUIModel> options;

    public PickerAdapter(List<OptionUIModel> list, ConversationalFragmentRouter conversationalFragmentRouter2) {
        this.options = list;
        this.conversationalFragmentRouter = conversationalFragmentRouter2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__picker_option, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        OptionUIModel optionUIModel = this.options.get(i);
        String str = optionUIModel.option.title;
        if (ListUtils.isEmpty(optionUIModel.titleHighlightInfo)) {
            viewHolder.optionView.setText(str);
        } else {
            int color = Styles.getColor(viewHolder.optionView.getContext(), R.attr.hs__searchHighlightColor);
            SpannableString spannableString = new SpannableString(str);
            for (HSRange next : optionUIModel.titleHighlightInfo) {
                spannableString.setSpan(new BackgroundColorSpan(color), next.index, next.index + next.length, 33);
            }
            viewHolder.optionView.setText(spannableString);
        }
        viewHolder.layoutView.setContentDescription(viewHolder.optionView.getContext().getString(R.string.hs__picker_option_list_item_voice_over, str));
    }

    public int getItemCount() {
        return this.options.size();
    }

    public void dispatchUpdates(List<OptionUIModel> list) {
        this.options.clear();
        this.options.addAll(list);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        /* access modifiers changed from: private */
        public final View layoutView = this.itemView.findViewById(R.id.option_list_item_layout);
        /* access modifiers changed from: private */
        public final TextView optionView = ((TextView) this.itemView.findViewById(R.id.hs__option));

        public ViewHolder(View view) {
            super(view);
            this.layoutView.setOnClickListener(this);
        }

        public void onClick(View view) {
            if (PickerAdapter.this.conversationalFragmentRouter != null) {
                PickerAdapter.this.conversationalFragmentRouter.handleOptionSelectedForPicker((OptionUIModel) PickerAdapter.this.options.get(getAdapterPosition()), false);
            }
        }
    }
}
