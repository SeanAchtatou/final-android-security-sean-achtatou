package com.helpshift.support.conversations;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.ViewGroup;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.ConversationFooterState;
import com.helpshift.conversation.activeconversation.message.HistoryLoadingState;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.support.conversations.messages.ConversationFooterViewBinder;
import com.helpshift.support.conversations.messages.HistoryLoadingViewBinder;
import com.helpshift.support.conversations.messages.MessageViewDataBinder;
import com.helpshift.support.conversations.messages.MessageViewType;
import com.helpshift.support.conversations.messages.MessageViewTypeConverter;
import com.helpshift.support.conversations.messages.MessagesAdapterClickListener;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MessageViewDataBinder.MessageItemClickListener, ConversationFooterViewBinder.ConversationFooterClickListener, HistoryLoadingViewBinder.HistoryLoadingClickListener {
    private ConversationFooterState conversationFooterState = ConversationFooterState.NONE;
    private HistoryLoadingState historyLoadingState = HistoryLoadingState.NONE;
    private boolean isAgentTypingIndicatorVisible = false;
    private MessageViewTypeConverter messageViewTypeConverter;
    private List<MessageDM> messages;
    private MessagesAdapterClickListener messagesAdapterClickListener;

    public MessagesAdapter(Context context, List<MessageDM> list, MessagesAdapterClickListener messagesAdapterClickListener2) {
        this.messageViewTypeConverter = new MessageViewTypeConverter(context);
        this.messages = list;
        this.messagesAdapterClickListener = messagesAdapterClickListener2;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == MessageViewType.HISTORY_LOADING_VIEW.key) {
            HistoryLoadingViewBinder historyLoadingViewBinder = this.messageViewTypeConverter.getHistoryLoadingViewBinder();
            historyLoadingViewBinder.setHistoryLoadingClickListener(this);
            return historyLoadingViewBinder.createViewHolder(viewGroup);
        } else if (i == MessageViewType.CONVERSATION_FOOTER.key) {
            ConversationFooterViewBinder conversationFooterViewBinder = this.messageViewTypeConverter.getConversationFooterViewBinder();
            conversationFooterViewBinder.setConversationFooterClickListener(this);
            return conversationFooterViewBinder.createViewHolder(viewGroup);
        } else if (i == MessageViewType.AGENT_TYPING_FOOTER.key) {
            return this.messageViewTypeConverter.getAgentTypingMessageDataBinder().createViewHolder(viewGroup);
        } else {
            MessageViewDataBinder viewTypeToDataBinder = this.messageViewTypeConverter.viewTypeToDataBinder(i);
            viewTypeToDataBinder.setMessageItemClickListener(this);
            return viewTypeToDataBinder.createViewHolder(viewGroup);
        }
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == MessageViewType.HISTORY_LOADING_VIEW.key) {
            this.messageViewTypeConverter.getHistoryLoadingViewBinder().bind((HistoryLoadingViewBinder.ViewHolder) viewHolder, this.historyLoadingState);
        } else if (itemViewType == MessageViewType.CONVERSATION_FOOTER.key) {
            this.messageViewTypeConverter.getConversationFooterViewBinder().bind((ConversationFooterViewBinder.ViewHolder) viewHolder, this.conversationFooterState);
        } else if (itemViewType != MessageViewType.AGENT_TYPING_FOOTER.key) {
            this.messageViewTypeConverter.viewTypeToDataBinder(itemViewType).bind(viewHolder, getMessageFromUIMessageList(i));
        }
    }

    public int getItemViewType(int i) {
        if (i < getHeaderCount()) {
            return getHeaderViewType();
        }
        if (i < getHeaderCount() + getMessageCount()) {
            return this.messageViewTypeConverter.messageToViewType(getMessageFromUIMessageList(i));
        }
        return getFooterViewType(i);
    }

    public int getItemCount() {
        return getHeaderCount() + getMessageCount() + getFooterCount();
    }

    public void onItemRangeInserted(int i, int i2) {
        notifyItemRangeInserted(i + getHeaderCount(), i2);
    }

    public void onItemRangeChanged(int i, int i2) {
        notifyItemRangeChanged(i + getHeaderCount(), i2);
    }

    public void onItemRangeRemoved(int i, int i2) {
        notifyItemRangeRemoved(i + getHeaderCount(), i2);
    }

    private int getHeaderCount() {
        return this.historyLoadingState != HistoryLoadingState.NONE ? 1 : 0;
    }

    private int getFooterCount() {
        int i = this.isAgentTypingIndicatorVisible ? 1 : 0;
        return this.conversationFooterState != ConversationFooterState.NONE ? i + 1 : i;
    }

    private MessageDM getMessageFromUIMessageList(int i) {
        return this.messages.get(i - getHeaderCount());
    }

    public void onAdminMessageLinkClicked(String str, MessageDM messageDM) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onAdminMessageLinkClicked(str, messageDM);
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, String str) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onCreateContextMenu(contextMenu, str);
        }
    }

    public void retryMessage(int i) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.retryMessage(getMessageFromUIMessageList(i));
        }
    }

    public void launchImagePicker(RequestScreenshotMessageDM requestScreenshotMessageDM) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.launchImagePicker(requestScreenshotMessageDM);
        }
    }

    public void handleReplyReviewButtonClick(RequestAppReviewMessageDM requestAppReviewMessageDM) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.handleReplyReviewButtonClick(requestAppReviewMessageDM);
        }
    }

    public void onScreenshotMessageClicked(ScreenshotMessageDM screenshotMessageDM) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onScreenshotMessageClicked(screenshotMessageDM);
        }
    }

    public void handleGenericAttachmentMessageClick(AdminAttachmentMessageDM adminAttachmentMessageDM) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.handleGenericAttachmentMessageClick(adminAttachmentMessageDM);
        }
    }

    public void handleAdminImageAttachmentMessageClick(AdminImageAttachmentMessageDM adminImageAttachmentMessageDM) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.handleAdminImageAttachmentMessageClick(adminImageAttachmentMessageDM);
        }
    }

    public void handleOptionSelected(OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.handleOptionSelected(optionInputMessageDM, option, z);
        }
    }

    public void onAdminSuggestedQuestionSelected(MessageDM messageDM, String str, String str2) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onAdminSuggestedQuestionSelected(messageDM, str, str2);
        }
    }

    public void setAgentTypingIndicatorVisibility(boolean z) {
        if (this.isAgentTypingIndicatorVisible != z) {
            this.isAgentTypingIndicatorVisible = z;
            if (z) {
                notifyItemRangeInserted(this.messages.size(), 1);
            } else {
                notifyItemRangeRemoved(this.messages.size(), 1);
            }
        }
    }

    public void setHistoryLoadingState(HistoryLoadingState historyLoadingState2) {
        if (historyLoadingState2 != null && this.historyLoadingState != historyLoadingState2) {
            if (this.historyLoadingState == HistoryLoadingState.NONE) {
                this.historyLoadingState = historyLoadingState2;
                notifyItemInserted(0);
            } else if (historyLoadingState2 == HistoryLoadingState.NONE) {
                this.historyLoadingState = historyLoadingState2;
                notifyItemRemoved(0);
            } else {
                this.historyLoadingState = historyLoadingState2;
                notifyItemChanged(0);
            }
        }
    }

    private int getHeaderViewType() {
        return MessageViewType.HISTORY_LOADING_VIEW.key;
    }

    private int getFooterViewType(int i) {
        int headerCount = i - (getHeaderCount() + getMessageCount());
        boolean z = this.conversationFooterState != ConversationFooterState.NONE;
        switch (headerCount) {
            case 0:
                if (this.isAgentTypingIndicatorVisible) {
                    return MessageViewType.AGENT_TYPING_FOOTER.key;
                }
                if (z) {
                    return MessageViewType.CONVERSATION_FOOTER.key;
                }
                break;
            case 1:
                if (z) {
                    return MessageViewType.CONVERSATION_FOOTER.key;
                }
                break;
        }
        return -1;
    }

    public void setConversationFooterState(ConversationFooterState conversationFooterState2) {
        if (conversationFooterState2 == null) {
            conversationFooterState2 = ConversationFooterState.NONE;
        }
        this.conversationFooterState = conversationFooterState2;
        notifyDataSetChanged();
    }

    public void onStartNewConversationButtonClick() {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onStartNewConversationButtonClick();
        }
    }

    public void onCSATSurveySubmitted(int i, String str) {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onCSATSurveySubmitted(i, str);
        }
    }

    public void unregisterAdapterClickListener() {
        this.messagesAdapterClickListener = null;
    }

    public int getMessageCount() {
        return this.messages.size();
    }

    public void onHistoryLoadingRetryClicked() {
        if (this.messagesAdapterClickListener != null) {
            this.messagesAdapterClickListener.onHistoryLoadingRetryClicked();
        }
    }
}
