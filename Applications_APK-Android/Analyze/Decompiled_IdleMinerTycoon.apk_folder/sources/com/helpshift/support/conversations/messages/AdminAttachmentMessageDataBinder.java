package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.support.util.Styles;

class AdminAttachmentMessageDataBinder extends MessageViewDataBinder<ViewHolder, AdminAttachmentMessageDM> {
    AdminAttachmentMessageDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(this.context).inflate(R.layout.hs__msg_attachment_generic, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, final AdminAttachmentMessageDM adminAttachmentMessageDM) {
        String str;
        int i;
        boolean z;
        int color = Styles.getColor(this.context, 16842806);
        String formattedFileSize = adminAttachmentMessageDM.getFormattedFileSize();
        boolean z2 = true;
        boolean z3 = false;
        switch (adminAttachmentMessageDM.state) {
            case DOWNLOAD_NOT_STARTED:
                str = this.context.getString(R.string.hs__attachment_not_downloaded_voice_over, adminAttachmentMessageDM.fileName, adminAttachmentMessageDM.getFormattedFileSize());
                z2 = false;
                z3 = true;
                i = color;
                z = false;
                break;
            case DOWNLOADING:
                formattedFileSize = adminAttachmentMessageDM.getDownloadProgressAndFileSize();
                str = this.context.getString(R.string.hs__attachment_downloading_voice_over, adminAttachmentMessageDM.fileName, adminAttachmentMessageDM.getDownloadedProgressSize(), adminAttachmentMessageDM.getFormattedFileSize());
                z2 = false;
                i = color;
                z = true;
                break;
            case DOWNLOADED:
                color = Styles.getColor(this.context, R.attr.colorAccent);
                str = this.context.getString(R.string.hs__attachment_downloaded__voice_over, adminAttachmentMessageDM.fileName);
                i = color;
                z = false;
                break;
            default:
                str = "";
                z2 = false;
                i = color;
                z = false;
                break;
        }
        setViewVisibility(viewHolder.downloadButton, z3);
        setViewVisibility(viewHolder.attachmentIcon, z2);
        setViewVisibility(viewHolder.progress, z);
        UIViewState uiViewState = adminAttachmentMessageDM.getUiViewState();
        if (uiViewState.isFooterVisible()) {
            viewHolder.subText.setText(adminAttachmentMessageDM.getSubText());
        }
        setViewVisibility(viewHolder.subText, uiViewState.isFooterVisible());
        viewHolder.fileName.setText(adminAttachmentMessageDM.fileName);
        viewHolder.fileSize.setText(formattedFileSize);
        viewHolder.fileName.setTextColor(i);
        viewHolder.messageContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (AdminAttachmentMessageDataBinder.this.messageClickListener != null) {
                    AdminAttachmentMessageDataBinder.this.messageClickListener.handleGenericAttachmentMessageClick(adminAttachmentMessageDM);
                }
            }
        });
        viewHolder.messageContainer.setContentDescription(str);
        viewHolder.messageLayout.setContentDescription(getAdminMessageContentDesciption(adminAttachmentMessageDM));
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView attachmentIcon;
        final View downloadButton;
        final TextView fileName;
        final TextView fileSize;
        final View messageContainer;
        final View messageLayout;
        final ProgressBar progress;
        final TextView subText;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.admin_attachment_message_layout);
            this.fileName = (TextView) view.findViewById(R.id.attachment_file_name);
            this.fileSize = (TextView) view.findViewById(R.id.attachment_file_size);
            this.messageContainer = view.findViewById(R.id.admin_message);
            this.downloadButton = view.findViewById(R.id.download_button);
            this.progress = (ProgressBar) view.findViewById(R.id.progress);
            this.attachmentIcon = (ImageView) view.findViewById(R.id.attachment_icon);
            this.subText = (TextView) view.findViewById(R.id.attachment_date);
            com.helpshift.util.Styles.setColorFilter(AdminAttachmentMessageDataBinder.this.context, ((ImageView) view.findViewById(R.id.hs_download_foreground_view)).getDrawable(), R.attr.hs__chatBubbleMediaBackgroundColor);
            com.helpshift.util.Styles.setColorFilter(AdminAttachmentMessageDataBinder.this.context, this.messageContainer.getBackground(), R.attr.hs__chatBubbleMediaBackgroundColor);
            Styles.setAccentColor(AdminAttachmentMessageDataBinder.this.context, this.progress.getIndeterminateDrawable());
            Styles.setAccentColor(AdminAttachmentMessageDataBinder.this.context, this.attachmentIcon.getDrawable());
        }
    }
}
