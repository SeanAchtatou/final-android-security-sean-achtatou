package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.support.util.Styles;
import com.helpshift.util.HSLinkify;

public class RequestScreenshotMessageDataBinder extends MessageViewDataBinder<ViewHolder, RequestScreenshotMessageDM> {
    public RequestScreenshotMessageDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_request_screenshot, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, final RequestScreenshotMessageDM requestScreenshotMessageDM) {
        viewHolder.adminRequestText.setText(escapeHtml(requestScreenshotMessageDM.body));
        setViewVisibility(viewHolder.attachButton, !requestScreenshotMessageDM.isAnswered);
        UIViewState uiViewState = requestScreenshotMessageDM.getUiViewState();
        setDrawable(viewHolder.adminMessage, uiViewState.isRoundedBackground() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
        if (uiViewState.isFooterVisible()) {
            viewHolder.subText.setText(requestScreenshotMessageDM.getSubText());
        }
        setViewVisibility(viewHolder.subText, uiViewState.isFooterVisible());
        viewHolder.attachButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (requestScreenshotMessageDM.isAttachmentButtonClickable() && RequestScreenshotMessageDataBinder.this.messageClickListener != null) {
                    RequestScreenshotMessageDataBinder.this.messageClickListener.launchImagePicker(requestScreenshotMessageDM);
                }
            }
        });
        viewHolder.messageLayout.setContentDescription(getAdminMessageContentDesciption(requestScreenshotMessageDM));
        linkify(viewHolder.adminRequestText, new HSLinkify.LinkClickListener() {
            public void onLinkClicked(String str) {
                if (RequestScreenshotMessageDataBinder.this.messageClickListener != null) {
                    RequestScreenshotMessageDataBinder.this.messageClickListener.onAdminMessageLinkClicked(str, requestScreenshotMessageDM);
                }
            }
        });
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */
        public final LinearLayout adminMessage;
        final TextView adminRequestText;
        final Button attachButton;
        final View messageLayout;
        final TextView subText;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.agent_screenshot_request_message_layout);
            this.adminRequestText = (TextView) view.findViewById(R.id.admin_attachment_request_text);
            this.attachButton = (Button) view.findViewById(R.id.admin_attach_screenshot_button);
            this.adminMessage = (LinearLayout) view.findViewById(R.id.admin_message);
            this.subText = (TextView) view.findViewById(R.id.admin_date_text);
            Styles.setAdminChatBubbleColor(RequestScreenshotMessageDataBinder.this.context, this.adminMessage.getBackground());
        }
    }
}
