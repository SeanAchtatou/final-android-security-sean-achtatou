package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.conversation.activeconversation.message.UserMessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageState;
import com.helpshift.util.Styles;

public class UserMessageViewDataBinder extends MessageViewDataBinder<ViewHolder, UserMessageDM> {
    public UserMessageViewDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_user, viewGroup, false));
        setUserMessageLayoutMargin(viewHolder.messageBubble.getLayoutParams());
        viewHolder.setListeners();
        return viewHolder;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void bind(ViewHolder viewHolder, UserMessageDM userMessageDM) {
        boolean z;
        UserMessageState state = userMessageDM.getState();
        viewHolder.messageText.setText(escapeHtml(userMessageDM.body));
        String str = "";
        int color = Styles.getColor(this.context, 16842808);
        String str2 = "";
        boolean z2 = true;
        boolean z3 = false;
        float f = 0.5f;
        switch (state) {
            case UNSENT_NOT_RETRYABLE:
                str = this.context.getString(R.string.hs__sending_fail_msg);
                str2 = this.context.getString(R.string.hs__user_failed_message_voice_over);
                color = Styles.getColor(this.context, R.attr.hs__errorTextColor);
                z = false;
                z3 = true;
                break;
            case UNSENT_RETRYABLE:
                str = this.context.getString(R.string.hs__sending_fail_msg);
                str2 = this.context.getString(R.string.hs__user_failed_message_voice_over);
                color = Styles.getColor(this.context, R.attr.hs__errorTextColor);
                z = true;
                z2 = false;
                z3 = true;
                break;
            case SENDING:
                str = this.context.getString(R.string.hs__sending_msg);
                str2 = this.context.getString(R.string.hs__user_sending_message_voice_over);
                z = false;
                z2 = false;
                break;
            case SENT:
                str = userMessageDM.getSubText();
                str2 = this.context.getString(R.string.hs__user_sent_message_voice_over, userMessageDM.getAccessbilityMessageTime());
                f = 1.0f;
                z = false;
                z3 = true;
                break;
            default:
                z = false;
                z3 = true;
                break;
        }
        viewHolder.messageLayout.setContentDescription(str2);
        viewHolder.subText.setTextColor(color);
        viewHolder.messageBubble.setAlpha(f);
        if (z2) {
            linkify(viewHolder.messageText, null);
        }
        viewHolder.messageText.setEnabled(z3);
        setViewVisibility(viewHolder.retryButton, z);
        UIViewState uiViewState = userMessageDM.getUiViewState();
        setUserMessageContainerBackground(viewHolder.messageBubble, uiViewState);
        setUserMessageSubText(viewHolder.subText, uiViewState, str);
        if (z) {
            viewHolder.retryButton.setOnClickListener(viewHolder);
        } else {
            viewHolder.retryButton.setOnClickListener(null);
        }
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, View.OnClickListener {
        final FrameLayout messageBubble;
        final View messageLayout;
        final TextView messageText;
        final ImageView retryButton;
        final TextView subText;

        ViewHolder(View view) {
            super(view);
            this.messageText = (TextView) view.findViewById(R.id.user_message_text);
            this.subText = (TextView) view.findViewById(R.id.user_date_text);
            this.messageBubble = (FrameLayout) view.findViewById(R.id.user_message_container);
            this.retryButton = (ImageView) view.findViewById(R.id.user_message_retry_button);
            this.messageLayout = view.findViewById(R.id.user_text_message_layout);
        }

        /* access modifiers changed from: package-private */
        public void setListeners() {
            this.messageText.setOnCreateContextMenuListener(this);
        }

        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (UserMessageViewDataBinder.this.messageClickListener != null) {
                UserMessageViewDataBinder.this.messageClickListener.onCreateContextMenu(contextMenu, ((TextView) view).getText().toString());
            }
        }

        public void onClick(View view) {
            if (UserMessageViewDataBinder.this.messageClickListener != null) {
                UserMessageViewDataBinder.this.messageClickListener.retryMessage(getAdapterPosition());
            }
        }
    }
}
