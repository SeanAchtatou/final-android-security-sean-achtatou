package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.SystemDividerMessageDM;

public class SystemDividerMessageDataBinder extends MessageViewDataBinder<ViewHolder, SystemDividerMessageDM> {
    public SystemDividerMessageDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_system_divider_layout, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, SystemDividerMessageDM systemDividerMessageDM) {
        String str;
        if (systemDividerMessageDM.showDividerText) {
            viewHolder.dividerHeader.setVisibility(0);
            str = "";
        } else {
            viewHolder.dividerHeader.setVisibility(8);
            str = this.context.getString(R.string.hs__conversations_divider_voice_over);
        }
        viewHolder.dividerView.setContentDescription(str);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */
        public TextView dividerHeader;
        /* access modifiers changed from: private */
        public View dividerView;

        public ViewHolder(View view) {
            super(view);
            this.dividerView = view.findViewById(R.id.conversations_divider);
            this.dividerHeader = (TextView) view.findViewById(R.id.conversation_closed_view);
        }
    }
}
