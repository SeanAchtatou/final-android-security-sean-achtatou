package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.animation.CollapseViewAnimation;
import com.helpshift.common.StringUtils;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.support.conversations.messages.MessageViewDataBinder;
import com.helpshift.support.util.Styles;
import com.helpshift.support.views.HSAdjustableSelectOptionsViewInflater;

public class UserSelectableOptionViewDataBinder extends MessageViewDataBinder<ViewHolder, OptionInputMessageDM> {
    UserSelectableOptionViewDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_user_selectable_options_container, viewGroup, false));
        viewHolder.setIsRecyclable(false);
        return viewHolder;
    }

    public void bind(ViewHolder viewHolder, OptionInputMessageDM optionInputMessageDM) {
        ViewHolder viewHolder2 = viewHolder;
        OptionInputMessageDM optionInputMessageDM2 = optionInputMessageDM;
        viewHolder2.optionsContainer.removeAllViews();
        if (!StringUtils.isEmpty(optionInputMessageDM2.input.inputLabel)) {
            viewHolder2.optionsHeaderTextView.setVisibility(0);
            viewHolder2.optionsHeaderTextView.setText(optionInputMessageDM2.input.inputLabel);
        } else {
            viewHolder2.optionsHeaderTextView.setVisibility(8);
        }
        OnOptionSelectedListener onOptionSelectedListener = new OnOptionSelectedListener(viewHolder, this.messageClickListener, optionInputMessageDM, false);
        new HSAdjustableSelectOptionsViewInflater(this.context, Styles.isTablet(this.context) ? 0.6000000000000001d : 0.8d, (int) this.context.getResources().getDimension(R.dimen.activity_horizontal_margin_medium), viewHolder2.optionsContainer, R.layout.hs__msg_user_selectable_option, R.id.selectable_option_text, R.drawable.hs__pill, R.attr.hs__selectableOptionColor, optionInputMessageDM2.input.options, onOptionSelectedListener).inflate();
        if (optionInputMessageDM2.input.required || StringUtils.isEmpty(optionInputMessageDM2.input.skipLabel)) {
            viewHolder2.optionsSkipTextView.setVisibility(8);
            return;
        }
        int paddingLeft = viewHolder2.optionsSkipTextView.getPaddingLeft();
        int paddingTop = viewHolder2.optionsSkipTextView.getPaddingTop();
        int paddingRight = viewHolder2.optionsSkipTextView.getPaddingRight();
        int paddingBottom = viewHolder2.optionsSkipTextView.getPaddingBottom();
        setDrawable(viewHolder2.optionsSkipTextView, R.drawable.hs__pill_small, R.attr.hs__selectableOptionColor);
        viewHolder2.optionsSkipTextView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        viewHolder2.optionsSkipTextView.setText(optionInputMessageDM2.input.skipLabel);
        viewHolder2.optionsSkipTextView.setVisibility(0);
        viewHolder2.optionsSkipTextView.setOnClickListener(new OnOptionSelectedListener(viewHolder, this.messageClickListener, optionInputMessageDM, true));
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder {
        final LinearLayout optionsContainer;
        final TextView optionsHeaderTextView;
        final LinearLayout optionsMessageView;
        final TextView optionsSkipTextView;

        ViewHolder(View view) {
            super(view);
            this.optionsMessageView = (LinearLayout) view.findViewById(R.id.options_message_view);
            this.optionsContainer = (LinearLayout) view.findViewById(R.id.selectable_options_container);
            this.optionsHeaderTextView = (TextView) view.findViewById(R.id.options_header);
            this.optionsSkipTextView = (TextView) view.findViewById(R.id.selectable_option_skip);
        }
    }

    private final class OnOptionSelectedListener implements View.OnClickListener {
        final boolean isSkip;
        final OptionInputMessageDM message;
        final MessageViewDataBinder.MessageItemClickListener messageClickListener;
        final ViewHolder viewHolder;

        OnOptionSelectedListener(ViewHolder viewHolder2, MessageViewDataBinder.MessageItemClickListener messageItemClickListener, OptionInputMessageDM optionInputMessageDM, boolean z) {
            this.viewHolder = viewHolder2;
            this.messageClickListener = messageItemClickListener;
            this.message = optionInputMessageDM;
            this.isSkip = z;
        }

        public void onClick(View view) {
            final TextView textView = (TextView) view;
            CollapseViewAnimation collapseViewAnimation = new CollapseViewAnimation(this.viewHolder.optionsMessageView);
            long j = (long) 250;
            collapseViewAnimation.setDuration(j);
            collapseViewAnimation.setFillAfter(true);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(j);
            alphaAnimation.setFillAfter(true);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(collapseViewAnimation);
            animationSet.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    OnOptionSelectedListener.this.viewHolder.optionsMessageView.setVisibility(8);
                    if (OnOptionSelectedListener.this.messageClickListener != null) {
                        OnOptionSelectedListener.this.messageClickListener.handleOptionSelected(OnOptionSelectedListener.this.message, (OptionInput.Option) textView.getTag(), OnOptionSelectedListener.this.isSkip);
                    }
                }
            });
            this.viewHolder.optionsMessageView.startAnimation(animationSet);
        }
    }
}
