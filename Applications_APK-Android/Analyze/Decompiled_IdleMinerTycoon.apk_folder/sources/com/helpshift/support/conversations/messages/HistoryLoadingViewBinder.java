package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.HistoryLoadingState;
import com.helpshift.support.util.Styles;

public class HistoryLoadingViewBinder {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public HistoryLoadingClickListener historyLoadingClickListener;

    public interface HistoryLoadingClickListener {
        void onHistoryLoadingRetryClicked();
    }

    public HistoryLoadingViewBinder(Context context2) {
        this.context = context2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__history_loading_view_layout, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, HistoryLoadingState historyLoadingState) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        int i = 0;
        switch (historyLoadingState) {
            case NONE:
                z = false;
                z3 = false;
                z2 = false;
                break;
            case LOADING:
                z = true;
                z2 = false;
                break;
            case ERROR:
                z = false;
                z2 = true;
                break;
            default:
                z = false;
                z2 = false;
                break;
        }
        viewHolder.layoutView.setVisibility(z3 ? 0 : 8);
        viewHolder.loadingStateView.setVisibility(z ? 0 : 8);
        View access$200 = viewHolder.errorStateView;
        if (!z2) {
            i = 8;
        }
        access$200.setVisibility(i);
    }

    public void setHistoryLoadingClickListener(HistoryLoadingClickListener historyLoadingClickListener2) {
        this.historyLoadingClickListener = historyLoadingClickListener2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        /* access modifiers changed from: private */
        public final View errorStateView = this.itemView.findViewById(R.id.loading_error_state_view);
        /* access modifiers changed from: private */
        public final View layoutView = this.itemView.findViewById(R.id.history_loading_layout_view);
        private final View loadingErrorTapToRetry = this.itemView.findViewById(R.id.loading_error_tap_to_retry);
        /* access modifiers changed from: private */
        public final View loadingStateView = this.itemView.findViewById(R.id.loading_state_view);
        private final ProgressBar progress;

        public ViewHolder(View view) {
            super(view);
            this.loadingErrorTapToRetry.setOnClickListener(this);
            this.progress = (ProgressBar) this.itemView.findViewById(R.id.loading_progressbar);
            Styles.setAccentColor(HistoryLoadingViewBinder.this.context, this.progress.getIndeterminateDrawable());
        }

        public void onClick(View view) {
            if (HistoryLoadingViewBinder.this.historyLoadingClickListener != null) {
                HistoryLoadingViewBinder.this.historyLoadingClickListener.onHistoryLoadingRetryClicked();
            }
        }
    }
}
