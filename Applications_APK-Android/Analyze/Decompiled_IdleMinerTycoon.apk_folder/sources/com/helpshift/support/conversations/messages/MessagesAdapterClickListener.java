package com.helpshift.support.conversations.messages;

import android.view.ContextMenu;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;

public interface MessagesAdapterClickListener {
    void handleAdminImageAttachmentMessageClick(AdminImageAttachmentMessageDM adminImageAttachmentMessageDM);

    void handleGenericAttachmentMessageClick(AdminAttachmentMessageDM adminAttachmentMessageDM);

    void handleOptionSelected(OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z);

    void handleReplyReviewButtonClick(RequestAppReviewMessageDM requestAppReviewMessageDM);

    void launchImagePicker(RequestScreenshotMessageDM requestScreenshotMessageDM);

    void onAdminMessageLinkClicked(String str, MessageDM messageDM);

    void onAdminSuggestedQuestionSelected(MessageDM messageDM, String str, String str2);

    void onCSATSurveySubmitted(int i, String str);

    void onCreateContextMenu(ContextMenu contextMenu, String str);

    void onHistoryLoadingRetryClicked();

    void onScreenshotMessageClicked(ScreenshotMessageDM screenshotMessageDM);

    void onStartNewConversationButtonClick();

    void retryMessage(MessageDM messageDM);
}
