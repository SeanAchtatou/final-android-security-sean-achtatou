package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.StringUtils;
import com.helpshift.conversation.activeconversation.message.FAQListMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.util.HSLinkify;

public class AdminSuggestionsMessageViewDataBinder extends MessageViewDataBinder<ViewHolder, FAQListMessageDM> {
    AdminSuggestionsMessageViewDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(this.context).inflate(R.layout.hs__msg_admin_suggesstions_container, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, final FAQListMessageDM fAQListMessageDM) {
        bindAdminMessage(viewHolder, fAQListMessageDM);
        viewHolder.suggestionsList.removeAllViews();
        TableRow tableRow = null;
        for (final FAQListMessageDM.FAQ next : fAQListMessageDM.faqs) {
            View inflate = LayoutInflater.from(this.context).inflate(R.layout.hs__msg_admin_suggesstion_item, (ViewGroup) null);
            ((TextView) inflate.findViewById(R.id.admin_suggestion_message)).setText(next.title);
            TableRow tableRow2 = new TableRow(this.context);
            tableRow2.addView(inflate);
            View inflate2 = LayoutInflater.from(this.context).inflate(R.layout.hs__section_divider, (ViewGroup) null);
            TableRow tableRow3 = new TableRow(this.context);
            tableRow3.addView(inflate2);
            viewHolder.suggestionsList.addView(tableRow2);
            viewHolder.suggestionsList.addView(tableRow3);
            inflate.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AdminSuggestionsMessageViewDataBinder.this.messageClickListener.onAdminSuggestedQuestionSelected(fAQListMessageDM, next.publishId, next.language);
                }
            });
            tableRow = tableRow3;
        }
        viewHolder.suggestionsList.removeView(tableRow);
        UIViewState uiViewState = fAQListMessageDM.getUiViewState();
        setViewVisibility(viewHolder.dateText, uiViewState.isFooterVisible());
        if (uiViewState.isFooterVisible()) {
            viewHolder.dateText.setText(fAQListMessageDM.getSubText());
        }
        viewHolder.messageLayout.setContentDescription(getAdminMessageContentDesciption(fAQListMessageDM));
    }

    private void bindAdminMessage(ViewHolder viewHolder, final MessageDM messageDM) {
        if (StringUtils.isEmpty(messageDM.body)) {
            viewHolder.messageContainer.setVisibility(8);
            return;
        }
        viewHolder.messageContainer.setVisibility(0);
        viewHolder.messageText.setText(escapeHtml(messageDM.body));
        setDrawable(viewHolder.messageContainer, messageDM.getUiViewState().isRoundedBackground() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
        viewHolder.messageContainer.setContentDescription(getAdminMessageContentDesciption(messageDM));
        linkify(viewHolder.messageText, new HSLinkify.LinkClickListener() {
            public void onLinkClicked(String str) {
                if (AdminSuggestionsMessageViewDataBinder.this.messageClickListener != null) {
                    AdminSuggestionsMessageViewDataBinder.this.messageClickListener.onAdminMessageLinkClicked(str, messageDM);
                }
            }
        });
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder {
        final TextView dateText;
        final View messageContainer;
        final View messageLayout;
        final TextView messageText;
        final TableLayout suggestionsList;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.admin_suggestion_message_layout);
            this.suggestionsList = (TableLayout) view.findViewById(R.id.suggestionsListStub);
            this.messageText = (TextView) view.findViewById(R.id.admin_message_text);
            this.messageContainer = view.findViewById(R.id.admin_message_container);
            this.dateText = (TextView) view.findViewById(R.id.admin_date_text);
        }
    }
}
