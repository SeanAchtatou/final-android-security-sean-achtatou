package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;

class ConfirmationRejectedMessageDataBinder extends MessageViewDataBinder<ViewHolder, ConfirmationRejectedMessageDM> {
    ConfirmationRejectedMessageDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_admin, viewGroup, false));
    }

    public void bind(ViewHolder viewHolder, ConfirmationRejectedMessageDM confirmationRejectedMessageDM) {
        viewHolder.message.setText(R.string.hs__cr_msg);
        UIViewState uiViewState = confirmationRejectedMessageDM.getUiViewState();
        setDrawable(viewHolder.messageContainer, uiViewState.isRoundedBackground() ? R.drawable.hs__chat_bubble_rounded : R.drawable.hs__chat_bubble_admin, R.attr.hs__chatBubbleAdminBackgroundColor);
        if (uiViewState.isFooterVisible()) {
            viewHolder.subText.setText(confirmationRejectedMessageDM.getSubText());
        }
        viewHolder.messageLayout.setContentDescription(getAdminMessageContentDesciption(confirmationRejectedMessageDM));
        setViewVisibility(viewHolder.subText, uiViewState.isFooterVisible());
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder {
        final TextView message;
        final View messageContainer;
        final View messageLayout;
        final TextView subText;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.admin_text_message_layout);
            this.message = (TextView) view.findViewById(R.id.admin_message_text);
            this.subText = (TextView) view.findViewById(R.id.admin_date_text);
            this.messageContainer = view.findViewById(R.id.admin_message_container);
        }
    }
}
