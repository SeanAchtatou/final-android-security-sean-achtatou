package com.helpshift.support.conversations;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class HSRecyclerViewScrollListener extends RecyclerView.OnScrollListener {
    /* access modifiers changed from: private */
    public final RecyclerViewScrollCallback callback;
    private boolean isScrollStateChangeHandled = false;
    private final Handler uiHandler;

    interface RecyclerViewScrollCallback {
        void onScrolledToBottom();

        void onScrolledToTop();

        void onScrolling();
    }

    public HSRecyclerViewScrollListener(Handler handler, RecyclerViewScrollCallback recyclerViewScrollCallback) {
        this.uiHandler = handler;
        this.callback = recyclerViewScrollCallback;
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        this.isScrollStateChangeHandled = false;
        if (i == 0) {
            computeAndNotifyCallback(recyclerView);
        }
    }

    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        if (!this.isScrollStateChangeHandled || recyclerView.getScrollState() == 0) {
            this.isScrollStateChangeHandled = true;
            computeAndNotifyCallback(recyclerView);
        }
    }

    private void computeAndNotifyCallback(RecyclerView recyclerView) {
        View childAt;
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        boolean z = true;
        if (layoutManager != null) {
            int itemCount = layoutManager.getItemCount();
            int childCount = layoutManager.getChildCount();
            if (childCount > 0 && (childAt = layoutManager.getChildAt(childCount - 1)) != null) {
                int position = layoutManager.getPosition(childAt);
                int i = position + 1;
                if (!(position == -1 || itemCount == i)) {
                    z = false;
                }
            }
        }
        if (!recyclerView.canScrollVertically(-1)) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    HSRecyclerViewScrollListener.this.callback.onScrolledToTop();
                }
            });
        }
        if (z) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    HSRecyclerViewScrollListener.this.callback.onScrolledToBottom();
                }
            });
        }
        if (!z) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    HSRecyclerViewScrollListener.this.callback.onScrolling();
                }
            });
        }
    }
}
