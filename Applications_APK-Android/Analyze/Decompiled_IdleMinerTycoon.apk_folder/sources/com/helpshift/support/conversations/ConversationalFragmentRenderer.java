package com.helpshift.support.conversations;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.StringUtils;
import com.helpshift.common.exception.ExceptionType;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.conversation.activeconversation.ConversationalRenderer;
import com.helpshift.conversation.activeconversation.message.ConversationFooterState;
import com.helpshift.conversation.activeconversation.message.HistoryLoadingState;
import com.helpshift.conversation.activeconversation.message.input.Input;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.activeconversation.message.input.TextInput;
import com.helpshift.conversation.viewmodel.OptionUIModel;
import com.helpshift.support.conversations.picker.PickerAdapter;
import com.helpshift.support.fragments.IToolbarMenuItemRenderer;
import com.helpshift.support.util.KeyboardUtil;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.Styles;
import com.helpshift.views.bottomsheet.HSBottomSheet;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class ConversationalFragmentRenderer extends ConversationFragmentRenderer implements ConversationalRenderer {
    private static final int OPTIONS_PICKER_PEEK_HEIGHT = 142;
    BottomSheetBehavior bottomSheetBehavior;
    /* access modifiers changed from: private */
    public final ConversationalFragmentRouter conversationalFragmentRouter;
    RecyclerView.ItemDecoration lastMessageItemDecor;
    LinearLayout networkErrorFooter;
    PickerAdapter pickerAdapter;
    ImageView pickerBackView;
    HSBottomSheet pickerBottomSheet;
    ImageView pickerClearView;
    ImageView pickerCollapseView;
    View pickerCollapsedHeader;
    TextView pickerCollapsedHeaderText;
    View pickerCollapsedShadow;
    View pickerEmptySearchResultsView;
    ImageView pickerExpandView;
    View pickerExpandedHeader;
    TextView pickerExpandedHeaderText;
    View pickerExpandedShadow;
    EditText pickerHeaderSearchView;
    RecyclerView pickerOptionsRecycler;
    ImageView pickerSearchView;
    TextView replyValidationFailedView;
    TextView skipBubbleTextView;
    LinearLayout skipOutterBubble;

    public /* bridge */ /* synthetic */ void appendMessages(int i, int i2) {
        super.appendMessages(i, i2);
    }

    public /* bridge */ /* synthetic */ void disableSendReplyButton() {
        super.disableSendReplyButton();
    }

    public /* bridge */ /* synthetic */ void enableSendReplyButton() {
        super.enableSendReplyButton();
    }

    public /* bridge */ /* synthetic */ String getReply() {
        return super.getReply();
    }

    public /* bridge */ /* synthetic */ void hideAgentTypingIndicator() {
        super.hideAgentTypingIndicator();
    }

    public /* bridge */ /* synthetic */ void hideKeyboard() {
        super.hideKeyboard();
    }

    public /* bridge */ /* synthetic */ void initializeMessages(List list) {
        super.initializeMessages(list);
    }

    public /* bridge */ /* synthetic */ boolean isReplyBoxVisible() {
        return super.isReplyBoxVisible();
    }

    public /* bridge */ /* synthetic */ void launchAttachment(String str, String str2) {
        super.launchAttachment(str, str2);
    }

    public /* bridge */ /* synthetic */ void launchScreenshotAttachment(String str, String str2) {
        super.launchScreenshotAttachment(str, str2);
    }

    public /* bridge */ /* synthetic */ void notifyRefreshList() {
        super.notifyRefreshList();
    }

    public /* bridge */ /* synthetic */ void onAuthenticationFailure() {
        super.onAuthenticationFailure();
    }

    public /* bridge */ /* synthetic */ void openAppReviewStore(String str) {
        super.openAppReviewStore(str);
    }

    public /* bridge */ /* synthetic */ void openFreshConversationScreen(Map map) {
        super.openFreshConversationScreen(map);
    }

    public /* bridge */ /* synthetic */ void removeMessages(int i, int i2) {
        super.removeMessages(i, i2);
    }

    public /* bridge */ /* synthetic */ void requestReplyFieldFocus() {
        super.requestReplyFieldFocus();
    }

    public /* bridge */ /* synthetic */ void scrollToBottom() {
        super.scrollToBottom();
    }

    public /* bridge */ /* synthetic */ void setReply(String str) {
        super.setReply(str);
    }

    public /* bridge */ /* synthetic */ void setReplyboxListeners() {
        super.setReplyboxListeners();
    }

    public /* bridge */ /* synthetic */ void showAgentTypingIndicator() {
        super.showAgentTypingIndicator();
    }

    public /* bridge */ /* synthetic */ void showCSATSubmittedView() {
        super.showCSATSubmittedView();
    }

    public /* bridge */ /* synthetic */ void showErrorView(ExceptionType exceptionType) {
        super.showErrorView(exceptionType);
    }

    public /* bridge */ /* synthetic */ void showKeyboard() {
        super.showKeyboard();
    }

    public /* bridge */ /* synthetic */ void unregisterFragmentRenderer() {
        super.unregisterFragmentRenderer();
    }

    public /* bridge */ /* synthetic */ void updateConversationFooterState(ConversationFooterState conversationFooterState) {
        super.updateConversationFooterState(conversationFooterState);
    }

    public /* bridge */ /* synthetic */ void updateConversationResolutionQuestionUI(boolean z) {
        super.updateConversationResolutionQuestionUI(z);
    }

    public /* bridge */ /* synthetic */ void updateHistoryLoadingState(HistoryLoadingState historyLoadingState) {
        super.updateHistoryLoadingState(historyLoadingState);
    }

    public /* bridge */ /* synthetic */ void updateImageAttachmentButtonView(boolean z) {
        super.updateImageAttachmentButtonView(z);
    }

    public /* bridge */ /* synthetic */ void updateMessages(int i, int i2) {
        super.updateMessages(i, i2);
    }

    public /* bridge */ /* synthetic */ void updateScrollJumperView(boolean z, boolean z2) {
        super.updateScrollJumperView(z, z2);
    }

    public /* bridge */ /* synthetic */ void updateSendReplyButton(boolean z) {
        super.updateSendReplyButton(z);
    }

    ConversationalFragmentRenderer(Context context, RecyclerView recyclerView, View view, View view2, ConversationFragmentRouter conversationFragmentRouter, View view3, View view4, IToolbarMenuItemRenderer iToolbarMenuItemRenderer, ConversationalFragmentRouter conversationalFragmentRouter2) {
        super(context, recyclerView, view, view2, conversationFragmentRouter, view3, view4, iToolbarMenuItemRenderer);
        this.skipBubbleTextView = (TextView) view.findViewById(R.id.skipBubbleTextView);
        this.skipOutterBubble = (LinearLayout) view.findViewById(R.id.skipOuterBubble);
        this.replyValidationFailedView = (TextView) view.findViewById(R.id.errorReplyTextView);
        this.networkErrorFooter = (LinearLayout) view.findViewById(R.id.networkErrorFooter);
        this.conversationalFragmentRouter = conversationalFragmentRouter2;
    }

    public void showInput(Input input) {
        if (input == null) {
            resetReplyFieldToNormalTextInput();
            return;
        }
        if (input instanceof TextInput) {
            renderForTextInput((TextInput) input);
        } else if (input instanceof OptionInput) {
            hideSendReplyUI();
            hideKeyboard();
        }
        setMessagesViewBottomPadding();
    }

    public void showSkipButton() {
        Styles.setColorFilter(this.parentView.getContext(), this.skipBubbleTextView.getBackground(), R.attr.hs__selectableOptionColor);
        Styles.setColorFilter(this.parentView.getContext(), this.skipOutterBubble.getBackground(), 16842836);
        this.skipOutterBubble.setVisibility(0);
        this.messagesRecyclerView.removeItemDecoration(this.lastMessageItemDecor);
        createRecyclerViewLastItemDecor();
        this.messagesRecyclerView.addItemDecoration(this.lastMessageItemDecor);
    }

    public void hideSkipButton() {
        this.skipOutterBubble.setVisibility(8);
        this.messagesRecyclerView.removeItemDecoration(this.lastMessageItemDecor);
    }

    public void showReplyValidationFailedError(int i) {
        boolean z = this.parentView.getResources().getConfiguration().orientation == 2;
        String str = "";
        Resources resources = this.context.getResources();
        switch (i) {
            case 1:
                str = resources.getString(R.string.hs__conversation_detail_error);
                break;
            case 2:
                if (!z) {
                    str = resources.getString(R.string.hs__email_input_validation_error);
                    break;
                } else {
                    str = resources.getString(R.string.hs__landscape_email_input_validation_error);
                    break;
                }
            case 3:
                if (!z) {
                    str = resources.getString(R.string.hs__number_input_validation_error);
                    break;
                } else {
                    str = resources.getString(R.string.hs__landscape_number_input_validation_error);
                    break;
                }
            case 4:
                if (!z) {
                    str = resources.getString(R.string.hs__date_input_validation_error);
                    break;
                } else {
                    str = resources.getString(R.string.hs__landscape_date_input_validation_error);
                    break;
                }
        }
        if (z) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.parentView.getContext());
            builder.setTitle(resources.getString(R.string.hs__landscape_input_validation_dialog_title));
            builder.setCancelable(true);
            builder.setMessage(str);
            builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.create().show();
            return;
        }
        this.replyValidationFailedView.setText(str);
        this.replyValidationFailedView.setVisibility(0);
    }

    public void hideReplyValidationFailedError() {
        this.replyValidationFailedView.setVisibility(8);
    }

    public void showNetworkErrorFooter(int i) {
        this.networkErrorFooter.setVisibility(0);
        TextView textView = (TextView) this.networkErrorFooter.findViewById(R.id.networkErrorFooterText);
        ProgressBar progressBar = (ProgressBar) this.networkErrorFooter.findViewById(R.id.networkErrorProgressBar);
        ImageView imageView = (ImageView) this.networkErrorFooter.findViewById(R.id.networkErrorIcon);
        imageView.setVisibility(0);
        Styles.setDrawable(this.context, imageView, R.drawable.hs__network_error, R.attr.hs__errorTextColor);
        progressBar.setVisibility(8);
        Resources resources = this.context.getResources();
        switch (i) {
            case 1:
                textView.setText(resources.getString(R.string.hs__no_internet_error));
                return;
            case 2:
                textView.setText(resources.getString(R.string.hs__network_reconnecting_error));
                imageView.setVisibility(8);
                progressBar.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void hideNetworkErrorFooter() {
        this.networkErrorFooter.setVisibility(8);
    }

    public void updateListPickerOptions(List<OptionUIModel> list) {
        if (this.pickerAdapter != null) {
            showPickerContent();
            this.pickerAdapter.dispatchUpdates(list);
        }
    }

    public void hideListPicker(boolean z) {
        if (this.bottomSheetBehavior != null && this.pickerBottomSheet != null) {
            if (z) {
                this.bottomSheetBehavior.setHideable(true);
                this.pickerBottomSheet.removeAllBottomSheetCallbacks();
                this.pickerBottomSheet.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    public void onSlide(@NonNull View view, float f) {
                    }

                    public void onStateChanged(@NonNull View view, int i) {
                        if (i == 5) {
                            ConversationalFragmentRenderer.this.removePickerViewFromWindow();
                        }
                    }
                });
                this.bottomSheetBehavior.setState(5);
            } else {
                removePickerViewFromWindow();
            }
            resetAccessibilityForToolbar();
            hideKeyboard();
            setBottomOffset(this.parentView, 0);
            hideSkipButton();
        }
    }

    /* access modifiers changed from: package-private */
    public void removePickerViewFromWindow() {
        this.pickerBottomSheet.remove();
        this.pickerBottomSheet = null;
    }

    private void setBottomOffset(View view, int i) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), (int) Styles.dpToPx(this.context, (float) i));
    }

    public void showEmptyListPickerView() {
        showEmptyPickerView();
    }

    private void showPickerContent() {
        if (this.pickerEmptySearchResultsView.isShown()) {
            this.pickerEmptySearchResultsView.setVisibility(8);
        }
        if (!this.pickerOptionsRecycler.isShown()) {
            this.pickerOptionsRecycler.setVisibility(0);
        }
    }

    private void showEmptyPickerView() {
        if (!this.pickerEmptySearchResultsView.isShown()) {
            this.pickerEmptySearchResultsView.setVisibility(0);
        }
        if (this.pickerOptionsRecycler.isShown()) {
            this.pickerOptionsRecycler.setVisibility(8);
        }
    }

    public void showListPicker(List<OptionUIModel> list, String str, boolean z, String str2) {
        if (this.pickerBottomSheet == null) {
            boolean isTablet = com.helpshift.support.util.Styles.isTablet(this.parentView.getContext());
            this.pickerBottomSheet = new HSBottomSheet.Builder(((Activity) this.context).getWindow()).contentView(R.layout.hs__picker_layout).referenceView(this.messagesRecyclerView).enableDimAnimation(true).dimOpacity(isTablet ? 0.8f : 1.0f).inflateAndBuild();
            initPickerViews(str);
            this.bottomSheetBehavior.setPeekHeight((int) Styles.dpToPx(this.context, 142.0f));
            this.pickerAdapter = new PickerAdapter(list, this.conversationalFragmentRouter);
            this.pickerOptionsRecycler.setAdapter(this.pickerAdapter);
            Styles.setGradientBackground(this.pickerCollapsedShadow, ContextCompat.getColor(this.context, R.color.hs__color_40000000), 0, GradientDrawable.Orientation.BOTTOM_TOP);
            hideSendReplyUI();
            handleSkipButtonRenderingForPicker(z, str2);
            hideKeyboard();
            setBottomOffset(this.parentView, 142 - calculatePickerBottomOffset(isTablet));
            registerListeners();
            initBottomSheetCallback();
            showPickerContent();
            this.pickerBottomSheet.show();
        }
    }

    private int calculatePickerBottomOffset(boolean z) {
        if (z) {
            return (int) (((float) (((int) this.parentView.getResources().getDimension(R.dimen.activity_horizontal_margin_large)) + 14 + 4)) + ((CardView) this.parentView.findViewById(R.id.hs__conversation_cardview_container)).getCardElevation());
        }
        return 14;
    }

    private void handleSkipButtonRenderingForPicker(boolean z, String str) {
        if (z || StringUtils.isEmpty(str)) {
            hideSkipButton();
            return;
        }
        setPickerOptionsInputSkipListener();
        this.skipBubbleTextView.setText(str);
        showSkipButton();
    }

    private void initBottomSheetCallback() {
        this.pickerBottomSheet.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            public void onStateChanged(@NonNull View view, int i) {
                if (4 == i) {
                    ConversationalFragmentRenderer.this.onOptionPickerCollapsed();
                } else if (3 == i) {
                    ConversationalFragmentRenderer.this.onOptionPickerExpanded();
                }
            }

            public void onSlide(@NonNull View view, float f) {
                if (((double) f) > 0.5d && ConversationalFragmentRenderer.this.bottomSheetBehavior.getState() == 2) {
                    ConversationalFragmentRenderer.this.onOptionPickerExpanded();
                } else if (ConversationalFragmentRenderer.this.bottomSheetBehavior.getState() == 2) {
                    ConversationalFragmentRenderer.this.onOptionPickerCollapsed();
                }
            }
        });
    }

    public void hidePickerClearButton() {
        if (this.pickerClearView.isShown()) {
            this.pickerClearView.setVisibility(8);
        }
    }

    public void showPickerClearButton() {
        if (!this.pickerClearView.isShown()) {
            this.pickerClearView.setVisibility(0);
        }
    }

    public boolean onBackPressed() {
        if (this.pickerBottomSheet == null || this.bottomSheetBehavior.getState() != 3) {
            return false;
        }
        this.bottomSheetBehavior.setState(4);
        return true;
    }

    /* access modifiers changed from: private */
    public void onOptionPickerExpanded() {
        this.pickerCollapsedShadow.setVisibility(8);
        Styles.setGradientBackground(this.pickerExpandedShadow, ContextCompat.getColor(this.context, R.color.hs__color_40000000), 0, GradientDrawable.Orientation.TOP_BOTTOM);
        this.pickerExpandedHeader.setVisibility(0);
        this.pickerCollapsedHeader.setVisibility(8);
        if (Build.VERSION.SDK_INT >= 19 && this.parentView != null) {
            this.parentView.setImportantForAccessibility(4);
            this.conversationalFragmentRouter.setToolbarImportanceForAccessibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void onOptionPickerCollapsed() {
        this.pickerCollapsedShadow.setVisibility(0);
        Styles.setGradientBackground(this.pickerCollapsedShadow, ContextCompat.getColor(this.context, R.color.hs__color_40000000), 0, GradientDrawable.Orientation.BOTTOM_TOP);
        showPickerContent();
        resetPickerSearchViewToNormalHeader();
        this.pickerCollapsedHeader.setVisibility(0);
        this.pickerExpandedHeader.setVisibility(8);
        this.pickerOptionsRecycler.scrollToPosition(0);
        resetAccessibilityForToolbar();
    }

    private void resetAccessibilityForToolbar() {
        if (Build.VERSION.SDK_INT >= 19 && this.parentView != null) {
            this.parentView.setImportantForAccessibility(0);
            this.conversationalFragmentRouter.resetToolbarImportanceForAccessibility();
        }
    }

    private void registerListeners() {
        this.pickerHeaderSearchView.addTextChangedListener(new TextWatcherAdapter() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence != null) {
                    ConversationalFragmentRenderer.this.conversationalFragmentRouter.onListPickerSearchQueryChange(charSequence.toString());
                }
            }
        });
        this.pickerHeaderSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 3) {
                    return false;
                }
                ConversationalFragmentRenderer.this.hideKeyboard();
                return true;
            }
        });
        this.pickerSearchView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.pickerHeaderSearchView.setVisibility(0);
                ConversationalFragmentRenderer.this.pickerExpandedHeaderText.setVisibility(8);
                ConversationalFragmentRenderer.this.pickerSearchView.setVisibility(8);
                ConversationalFragmentRenderer.this.pickerHeaderSearchView.requestFocus();
                ConversationalFragmentRenderer.this.pickerBottomSheet.setDraggable(false);
                ConversationalFragmentRenderer.this.pickerCollapseView.setVisibility(8);
                ConversationalFragmentRenderer.this.pickerBackView.setVisibility(0);
                KeyboardUtil.showKeyboard(ConversationalFragmentRenderer.this.context, ConversationalFragmentRenderer.this.pickerHeaderSearchView);
                ConversationalFragmentRenderer.this.pickerBottomSheet.setDraggable(false);
            }
        });
        this.pickerBackView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.resetPickerSearchViewToNormalHeader();
            }
        });
        this.pickerClearView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.pickerHeaderSearchView.setText("");
                ConversationalFragmentRenderer.this.pickerClearView.setVisibility(8);
            }
        });
        this.pickerCollapseView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.bottomSheetBehavior.setState(4);
                ConversationalFragmentRenderer.this.hideKeyboard();
            }
        });
        this.pickerCollapsedHeader.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.bottomSheetBehavior.setState(3);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void resetPickerSearchViewToNormalHeader() {
        this.pickerHeaderSearchView.setVisibility(8);
        this.pickerExpandedHeaderText.setVisibility(0);
        this.pickerHeaderSearchView.setText("");
        this.pickerBackView.setVisibility(8);
        this.pickerCollapseView.setVisibility(0);
        this.pickerClearView.setVisibility(8);
        this.pickerSearchView.setVisibility(0);
        hideKeyboard();
        this.pickerBottomSheet.setDraggable(true);
    }

    private void initPickerViews(String str) {
        this.bottomSheetBehavior = this.pickerBottomSheet.getBottomSheetBehaviour();
        View bottomSheetContentView = this.pickerBottomSheet.getBottomSheetContentView();
        this.pickerCollapsedShadow = bottomSheetContentView.findViewById(R.id.hs__picker_collapsed_shadow);
        this.pickerExpandedShadow = bottomSheetContentView.findViewById(R.id.hs__picker_expanded_shadow);
        this.pickerOptionsRecycler = (RecyclerView) bottomSheetContentView.findViewById(R.id.hs__optionsList);
        this.pickerOptionsRecycler.setLayoutManager(new LinearLayoutManager(bottomSheetContentView.getContext(), 1, false));
        this.pickerSearchView = (ImageView) bottomSheetContentView.findViewById(R.id.hs__picker_action_search);
        this.pickerClearView = (ImageView) bottomSheetContentView.findViewById(R.id.hs__picker_action_clear);
        this.pickerCollapseView = (ImageView) bottomSheetContentView.findViewById(R.id.hs__picker_action_collapse);
        this.pickerBackView = (ImageView) bottomSheetContentView.findViewById(R.id.hs__picker_action_back);
        this.pickerHeaderSearchView = (EditText) bottomSheetContentView.findViewById(R.id.hs__picker_header_search);
        this.pickerExpandedHeaderText = (TextView) bottomSheetContentView.findViewById(R.id.hs__expanded_picker_header_text);
        this.pickerExpandedHeader = bottomSheetContentView.findViewById(R.id.hs__picker_expanded_header);
        this.pickerCollapsedHeader = bottomSheetContentView.findViewById(R.id.hs__picker_collapsed_header);
        this.pickerCollapsedHeaderText = (TextView) bottomSheetContentView.findViewById(R.id.hs__collapsed_picker_header_text);
        this.pickerEmptySearchResultsView = bottomSheetContentView.findViewById(R.id.hs__empty_picker_view);
        this.pickerExpandView = (ImageView) bottomSheetContentView.findViewById(R.id.hs__picker_action_expand);
        this.pickerExpandedHeaderText.setText(str);
        this.pickerCollapsedHeaderText.setText(str);
        String string = this.parentView.getResources().getString(R.string.hs__picker_options_expand_header_voice_over, str);
        this.pickerCollapsedHeader.setContentDescription(string);
        this.pickerCollapsedHeaderText.setContentDescription(string);
        Styles.setColorFilter(this.context, this.pickerSearchView.getDrawable(), R.attr.hs__expandedPickerIconColor);
        Styles.setColorFilter(this.context, this.pickerBackView.getDrawable(), R.attr.hs__expandedPickerIconColor);
        Styles.setColorFilter(this.context, this.pickerCollapseView.getDrawable(), R.attr.hs__expandedPickerIconColor);
        Styles.setColorFilter(this.context, this.pickerClearView.getDrawable(), R.attr.hs__expandedPickerIconColor);
        Styles.setColorFilter(this.context, this.pickerExpandView.getDrawable(), R.attr.hs__collapsedPickerIconColor);
    }

    private void resetReplyFieldToNormalTextInput() {
        this.replyField.setInputType(147457);
        this.replyField.setHint(R.string.hs__chat_hint);
    }

    public void onFocusChanged(boolean z) {
        if (!z) {
            hideListPicker(true);
        }
    }

    public void destroy() {
        hideListPicker(true);
        super.destroy();
    }

    public void updateSendReplyUI(boolean z) {
        if (z) {
            showSendReplyUI();
        } else {
            hideSendReplyUI();
        }
    }

    public void showSendReplyUI() {
        super.showSendReplyUI();
        ((LinearLayout) this.parentView.findViewById(R.id.replyBoxLabelLayout)).setVisibility(8);
        this.replyField.setFocusableInTouchMode(true);
        this.replyField.setOnClickListener(null);
        resetReplyFieldToNormalTextInput();
        hideSkipButton();
    }

    public void hideSendReplyUI() {
        super.hideSendReplyUI();
        hideSkipButton();
    }

    private void setTextInputSkipListener() {
        this.skipBubbleTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.conversationFragmentRouter.onSkipClick();
            }
        });
    }

    private void setPickerOptionsInputSkipListener() {
        this.skipBubbleTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationalFragmentRenderer.this.conversationalFragmentRouter.handleOptionSelectedForPicker(null, true);
            }
        });
    }

    private void createRecyclerViewLastItemDecor() {
        if (this.lastMessageItemDecor == null) {
            this.lastMessageItemDecor = new RecyclerView.ItemDecoration() {
                public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
                    RecyclerView.Adapter adapter;
                    int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
                    if (childAdapterPosition != -1 && (adapter = recyclerView.getAdapter()) != null && ConversationalFragmentRenderer.this.skipOutterBubble.getVisibility() == 0 && childAdapterPosition == adapter.getItemCount() - 1) {
                        rect.set(rect.left, rect.top, rect.right, (int) TypedValue.applyDimension(1, 80.0f, recyclerView.getContext().getResources().getDisplayMetrics()));
                    }
                }
            };
        }
    }

    /* access modifiers changed from: package-private */
    public DatePickerDialog createDatePickerForReplyField() {
        AnonymousClass14 r2 = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                Calendar instance = Calendar.getInstance();
                instance.set(i, i2, i3);
                ConversationalFragmentRenderer.this.replyField.setText(HSDateFormatSpec.getDateFormatter(HSDateFormatSpec.DISPLAY_DATE_PATTERN, HelpshiftContext.getCoreApi().getLocaleProviderDM().getCurrentLocale()).format(instance.getTime()));
            }
        };
        Calendar instance = Calendar.getInstance();
        try {
            String obj = this.replyField.getText().toString();
            if (!StringUtils.isEmpty(obj)) {
                instance.setTime(HSDateFormatSpec.getDateFormatter(HSDateFormatSpec.DISPLAY_DATE_PATTERN, HelpshiftContext.getCoreApi().getLocaleProviderDM().getCurrentLocale()).parse(obj));
            }
        } catch (ParseException unused) {
        }
        return new DatePickerDialog(this.parentView.getContext(), r2, instance.get(1), instance.get(2), instance.get(5));
    }

    private void renderForTextInput(TextInput textInput) {
        this.replyField.setFocusableInTouchMode(true);
        this.replyField.setOnClickListener(null);
        if (!TextUtils.isEmpty(textInput.inputLabel)) {
            ((LinearLayout) this.parentView.findViewById(R.id.replyBoxLabelLayout)).setVisibility(0);
            ((TextView) this.replyBoxView.findViewById(R.id.replyFieldLabel)).setText(textInput.inputLabel);
        }
        this.replyField.setHint(TextUtils.isEmpty(textInput.placeholder) ? "" : textInput.placeholder);
        int i = 131072;
        switch (textInput.keyboard) {
            case 1:
                i = 147457;
                break;
            case 2:
                i = 131105;
                break;
            case 3:
                i = 139266;
                break;
            case 4:
                hideKeyboard();
                this.replyField.setFocusableInTouchMode(false);
                this.replyField.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        ConversationalFragmentRenderer.this.createDatePickerForReplyField().show();
                    }
                });
                i = 0;
                break;
            default:
                resetReplyFieldToNormalTextInput();
                break;
        }
        this.replyField.setInputType(i);
        if (textInput.required || TextUtils.isEmpty(textInput.skipLabel)) {
            hideSkipButton();
        } else {
            setTextInputSkipListener();
            this.skipBubbleTextView.setText(textInput.skipLabel);
            showSkipButton();
        }
        this.replyBoxView.setVisibility(0);
    }
}
