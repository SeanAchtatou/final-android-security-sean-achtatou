package com.helpshift.support.conversations;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.domain.Domain;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.viewmodel.NewConversationVM;
import com.helpshift.support.Faq;
import com.helpshift.support.fragments.HSMenuItemType;
import com.helpshift.support.fragments.ScreenshotPreviewFragment;
import com.helpshift.support.fragments.SearchResultFragment;
import com.helpshift.support.util.AppSessionConstants;
import com.helpshift.support.util.KeyboardUtil;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.widget.ButtonViewState;
import com.helpshift.widget.HSObserver;
import com.helpshift.widget.ImageAttachmentViewState;
import com.helpshift.widget.ProfileFormViewState;
import com.helpshift.widget.ProgressBarViewState;
import com.helpshift.widget.TextViewState;
import java.util.ArrayList;

public class NewConversationFragment extends BaseConversationFragment implements NewConversationRouter {
    public static final String FRAGMENT_TAG = "HSNewConversationFragment";
    public static final String SEARCH_PERFORMED = "search_performed";
    public static final String SHOULD_DROP_META = "dropMeta";
    public static final String SOURCE_SEARCH_QUERY = "source_search_query";
    private TextInputEditText descriptionField;
    NewConversationVM newConversationVM;
    /* access modifiers changed from: private */
    public NewConversationFragmentRenderer renderer;
    private ImagePickerFile selectedImageFile;
    private boolean shouldUpdateAttachment;

    /* access modifiers changed from: protected */
    public int getScreenshotMode() {
        return 1;
    }

    public static NewConversationFragment newInstance(Bundle bundle) {
        NewConversationFragment newConversationFragment = new NewConversationFragment();
        newConversationFragment.setArguments(bundle);
        return newConversationFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__new_conversation_fragment, viewGroup, false);
    }

    public void onStart() {
        super.onStart();
        if (!isChangingConfigurations()) {
            HelpshiftContext.getCoreApi().getConversationController().deleteCachedFilesForResolvedConversations();
        }
    }

    public void onResume() {
        super.onResume();
        addViewStateObservers();
        if (!isChangingConfigurations()) {
            HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.REPORTED_ISSUE);
        }
        this.descriptionField.requestFocus();
        KeyboardUtil.showKeyboard(getContext(), this.descriptionField);
        this.newConversationVM.setConversationViewState(1);
    }

    private void addViewStateObservers() {
        Domain domain = HelpshiftContext.getCoreApi().getDomain();
        this.newConversationVM.getDescriptionViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                TextViewState textViewState = (TextViewState) obj;
                NewConversationFragment.this.renderer.setDescription(textViewState.getText());
                NewConversationFragment.this.renderer.updateDescriptionErrorState(textViewState.getError());
            }
        });
        this.newConversationVM.getProgressBarViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                NewConversationFragment.this.renderer.updateProgressBarVisibility(((ProgressBarViewState) obj).isVisible());
            }
        });
        this.newConversationVM.getStartConversationButtonState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                NewConversationFragment.this.renderer.updateStartConversationButton(((ButtonViewState) obj).isVisible());
            }
        });
        this.newConversationVM.getAttachImageButtonViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                NewConversationFragment.this.renderer.updateImageAttachmentButton(((ButtonViewState) obj).isVisible());
            }
        });
        this.newConversationVM.getImageAttachmentViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ImageAttachmentViewState imageAttachmentViewState = (ImageAttachmentViewState) obj;
                NewConversationFragment.this.renderer.updateImageAttachmentPickerFile(imageAttachmentViewState.getImagePickerFile());
                NewConversationFragment.this.renderer.updateImageAttachmentClick(imageAttachmentViewState.isClickable());
            }
        });
        this.newConversationVM.getNameViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                TextViewState textViewState = (TextViewState) obj;
                NewConversationFragment.this.renderer.setName(textViewState.getText());
                NewConversationFragment.this.renderer.updateNameErrorState(textViewState.getError());
            }
        });
        this.newConversationVM.getEmailViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                TextViewState textViewState = (TextViewState) obj;
                NewConversationFragment.this.renderer.setEmail(textViewState.getText());
                NewConversationFragment.this.renderer.updateEmailErrorState(textViewState.getError(), textViewState.isRequired());
            }
        });
        this.newConversationVM.getProfileFormViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                NewConversationFragment.this.renderer.updateProfileForm(((ProfileFormViewState) obj).isVisible());
            }
        });
    }

    private void removeViewStateObservers() {
        this.newConversationVM.getDescriptionViewState().unsubscribe();
        this.newConversationVM.getProgressBarViewState().unsubscribe();
        this.newConversationVM.getStartConversationButtonState().unsubscribe();
        this.newConversationVM.getAttachImageButtonViewState().unsubscribe();
        this.newConversationVM.getImageAttachmentViewState().unsubscribe();
        this.newConversationVM.getNameViewState().unsubscribe();
        this.newConversationVM.getEmailViewState().unsubscribe();
        this.newConversationVM.getProfileFormViewState().unsubscribe();
    }

    /* access modifiers changed from: protected */
    public AppSessionConstants.Screen getViewName() {
        return AppSessionConstants.Screen.NEW_CONVERSATION;
    }

    /* access modifiers changed from: protected */
    public String getToolbarTitle() {
        return getString(R.string.hs__new_conversation_header);
    }

    /* access modifiers changed from: protected */
    public void onPermissionGranted(int i) {
        if (i == 2) {
            Bundle bundle = new Bundle();
            bundle.putInt(ScreenshotPreviewFragment.KEY_SCREENSHOT_MODE, getScreenshotMode());
            getSupportFragment().launchImagePicker(false, bundle);
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        initialize(view);
        super.onViewCreated(view, bundle);
        setViewListeners(view);
    }

    public void onDestroyView() {
        this.newConversationVM.unregisterRenderer(this.renderer);
        this.newConversationVM.setConversationViewState(-1);
        super.onDestroyView();
    }

    public void onPause() {
        removeViewStateObservers();
        super.onPause();
        KeyboardUtil.hideKeyboard(getContext(), this.descriptionField);
    }

    private void initialize(View view) {
        boolean z;
        View view2 = view;
        TextInputLayout textInputLayout = (TextInputLayout) view2.findViewById(R.id.hs__conversationDetailWrapper);
        TextInputLayout textInputLayout2 = textInputLayout;
        textInputLayout.setHintEnabled(false);
        textInputLayout.setHintAnimationEnabled(false);
        this.descriptionField = (TextInputEditText) view2.findViewById(R.id.hs__conversationDetail);
        TextInputLayout textInputLayout3 = (TextInputLayout) view2.findViewById(R.id.hs__usernameWrapper);
        TextInputLayout textInputLayout4 = textInputLayout3;
        textInputLayout3.setHintEnabled(false);
        textInputLayout3.setHintAnimationEnabled(false);
        TextInputEditText textInputEditText = (TextInputEditText) view2.findViewById(R.id.hs__username);
        TextInputLayout textInputLayout5 = (TextInputLayout) view2.findViewById(R.id.hs__emailWrapper);
        textInputLayout5.setHintEnabled(false);
        textInputLayout5.setHintAnimationEnabled(false);
        TextInputEditText textInputEditText2 = (TextInputEditText) view2.findViewById(R.id.hs__email);
        NewConversationFragmentRenderer newConversationFragmentRenderer = r0;
        TextInputEditText textInputEditText3 = textInputEditText;
        Context context = getContext();
        TextInputEditText textInputEditText4 = textInputEditText2;
        TextInputEditText textInputEditText5 = this.descriptionField;
        TextInputEditText textInputEditText6 = textInputEditText4;
        NewConversationFragmentRenderer newConversationFragmentRenderer2 = new NewConversationFragmentRenderer(context, textInputLayout2, textInputEditText5, textInputLayout4, textInputEditText, textInputLayout5, textInputEditText2, (ProgressBar) view2.findViewById(R.id.progress_bar), (ImageView) view2.findViewById(R.id.hs__screenshot), (TextView) view2.findViewById(R.id.attachment_file_name), (TextView) view2.findViewById(R.id.attachment_file_size), (CardView) view2.findViewById(R.id.screenshot_view_container), (ImageButton) view2.findViewById(16908314), getView(), this, getSupportFragment());
        this.renderer = newConversationFragmentRenderer;
        this.newConversationVM = HelpshiftContext.getCoreApi().getNewConversationViewModel(this.renderer);
        if (this.shouldUpdateAttachment) {
            this.newConversationVM.setImageAttachment(this.selectedImageFile);
            z = false;
            this.shouldUpdateAttachment = false;
        } else {
            z = false;
        }
        this.descriptionField.addTextChangedListener(new TextWatcherAdapter() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                NewConversationFragment.this.newConversationVM.setDescription(charSequence.toString());
            }
        });
        textInputEditText3.addTextChangedListener(new TextWatcherAdapter() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                NewConversationFragment.this.newConversationVM.setName(charSequence.toString());
            }
        });
        textInputEditText6.addTextChangedListener(new TextWatcherAdapter() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                NewConversationFragment.this.newConversationVM.setEmail(charSequence.toString());
            }
        });
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.newConversationVM.setSearchQuery(arguments.getString(SOURCE_SEARCH_QUERY));
            this.newConversationVM.setShouldDropCustomMetadata(arguments.getBoolean(SHOULD_DROP_META));
            this.newConversationVM.setWasSearchPerformed(getArguments().getBoolean(SEARCH_PERFORMED, z));
        }
    }

    private void setViewListeners(View view) {
        this.descriptionField = (TextInputEditText) view.findViewById(R.id.hs__conversationDetail);
        this.descriptionField.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.hs__conversationDetail) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    if ((motionEvent.getAction() & 255) == 1) {
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                    }
                }
                return false;
            }
        });
        ImageButton imageButton = (ImageButton) view.findViewById(16908314);
        imageButton.setVisibility(8);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewConversationFragment.this.newConversationVM.handleImageAttachmentClearButtonClick();
            }
        });
        ((ImageView) view.findViewById(R.id.hs__screenshot)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewConversationFragment.this.newConversationVM.handleImageAttachmentClick();
            }
        });
    }

    public boolean handleScreenshotAction(ScreenshotPreviewFragment.ScreenshotAction screenshotAction, ImagePickerFile imagePickerFile) {
        switch (screenshotAction) {
            case ADD:
                if (this.newConversationVM == null) {
                    this.selectedImageFile = imagePickerFile;
                    this.shouldUpdateAttachment = true;
                } else {
                    this.newConversationVM.setImageAttachment(imagePickerFile);
                }
                return true;
            case REMOVE:
                if (this.newConversationVM == null) {
                    this.selectedImageFile = null;
                    this.shouldUpdateAttachment = true;
                } else {
                    this.newConversationVM.setImageAttachment(null);
                }
                return true;
            default:
                return false;
        }
    }

    public void startNewConversation() {
        this.newConversationVM.startNewConversation();
    }

    public void showSearchResultFragment(ArrayList<Faq> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SearchResultFragment.BUNDLE_ARG_SEARCH_RESULTS, arrayList);
        getSupportController().showConversationSearchResultFragment(bundle);
    }

    public void exitNewConversationView() {
        getSupportFragment().exitSdkSession();
    }

    public void showConversationScreen() {
        if (isResumed()) {
            getSupportController().startConversationFlow();
        }
    }

    public void showAttachmentPreviewScreenFromDraft(ImagePickerFile imagePickerFile) {
        Bundle bundle = new Bundle();
        bundle.putInt(ScreenshotPreviewFragment.KEY_SCREENSHOT_MODE, 2);
        getSupportController().startScreenshotPreviewFragment(imagePickerFile, bundle, ScreenshotPreviewFragment.LaunchSource.ATTACHMENT_DRAFT);
    }

    public void onAuthenticationFailure() {
        getSupportController().onAuthenticationFailure();
    }

    public void onCreateOptionMenuCalled() {
        this.renderer.updateStartConversationButton(this.newConversationVM.getStartConversationButtonState().isVisible());
        this.renderer.updateImageAttachmentButton(this.newConversationVM.getAttachImageButtonViewState().isVisible());
    }

    public void onMenuItemClicked(HSMenuItemType hSMenuItemType) {
        switch (hSMenuItemType) {
            case START_NEW_CONVERSATION:
                this.newConversationVM.showSearchOrStartNewConversation();
                return;
            case SCREENSHOT_ATTACHMENT:
                Bundle bundle = new Bundle();
                bundle.putInt(ScreenshotPreviewFragment.KEY_SCREENSHOT_MODE, getScreenshotMode());
                bundle.putString(ScreenshotPreviewFragment.KEY_MESSAGE_REFERS_ID, null);
                getSupportFragment().launchImagePicker(true, bundle);
                return;
            default:
                return;
        }
    }
}
