package com.helpshift.support.conversations;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.android.commons.downloader.HsUriUtils;
import com.helpshift.common.exception.ExceptionType;
import com.helpshift.common.exception.PlatformException;
import com.helpshift.common.util.FileUtil;
import com.helpshift.conversation.activeconversation.ConversationRenderer;
import com.helpshift.conversation.activeconversation.message.ConversationFooterState;
import com.helpshift.conversation.activeconversation.message.HistoryLoadingState;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.support.fragments.HSMenuItemType;
import com.helpshift.support.fragments.IToolbarMenuItemRenderer;
import com.helpshift.support.util.KeyboardUtil;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.support.util.Styles;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.IntentUtil;
import java.io.File;
import java.util.List;
import java.util.Map;

class ConversationFragmentRenderer implements ConversationRenderer {
    private View confirmationBoxView;
    Context context;
    protected ConversationFragmentRouter conversationFragmentRouter;
    private IToolbarMenuItemRenderer menuItemRenderer;
    MessagesAdapter messagesAdapter;
    RecyclerView messagesRecyclerView;
    protected View parentView;
    protected View replyBoxView;
    /* access modifiers changed from: private */
    public ImageButton replyButton;
    protected EditText replyField;
    private View scrollIndicator;
    private View scrollJumpButton;
    private View unreadMessagesIndicatorDot;

    ConversationFragmentRenderer(Context context2, RecyclerView recyclerView, View view, View view2, ConversationFragmentRouter conversationFragmentRouter2, View view3, View view4, IToolbarMenuItemRenderer iToolbarMenuItemRenderer) {
        this.context = context2;
        this.messagesRecyclerView = recyclerView;
        RecyclerView.ItemAnimator itemAnimator = this.messagesRecyclerView.getItemAnimator();
        if (itemAnimator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        this.parentView = view;
        this.replyBoxView = view.findViewById(R.id.relativeLayout1);
        this.replyField = (EditText) this.replyBoxView.findViewById(R.id.hs__messageText);
        this.replyButton = (ImageButton) this.replyBoxView.findViewById(R.id.hs__sendMessageBtn);
        this.replyButton.setImageDrawable(context2.getResources().getDrawable(R.drawable.hs__send).mutate());
        this.scrollJumpButton = view.findViewById(R.id.scroll_jump_button);
        this.confirmationBoxView = view2;
        this.conversationFragmentRouter = conversationFragmentRouter2;
        this.menuItemRenderer = iToolbarMenuItemRenderer;
        this.scrollIndicator = view3;
        this.unreadMessagesIndicatorDot = view4;
    }

    public void initializeMessages(List<MessageDM> list) {
        this.messagesAdapter = new MessagesAdapter(this.context, list, this.conversationFragmentRouter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.context);
        linearLayoutManager.setStackFromEnd(true);
        this.messagesRecyclerView.setLayoutManager(linearLayoutManager);
        this.messagesRecyclerView.setAdapter(this.messagesAdapter);
    }

    public void appendMessages(int i, int i2) {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.onItemRangeInserted(i, i2);
        }
    }

    public void updateMessages(int i, int i2) {
        if (this.messagesAdapter != null) {
            if (i == 0 && i2 == this.messagesAdapter.getMessageCount()) {
                this.messagesAdapter.notifyDataSetChanged();
            } else {
                this.messagesAdapter.onItemRangeChanged(i, i2);
            }
        }
    }

    public void removeMessages(int i, int i2) {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.onItemRangeRemoved(i, i2);
        }
    }

    public void updateImageAttachmentButtonView(boolean z) {
        if (z) {
            changeMenuItemVisibility(HSMenuItemType.SCREENSHOT_ATTACHMENT, true);
        } else {
            changeMenuItemVisibility(HSMenuItemType.SCREENSHOT_ATTACHMENT, false);
        }
    }

    public void updateSendReplyButton(boolean z) {
        if (z) {
            enableSendReplyButton();
        } else {
            disableSendReplyButton();
        }
    }

    public void enableSendReplyButton() {
        this.replyButton.setEnabled(true);
        Styles.setImageAlpha(this.replyButton, 255);
        Styles.setSendMessageButtonIconColor(this.context, this.replyButton.getDrawable(), true);
    }

    public void disableSendReplyButton() {
        this.replyButton.setEnabled(false);
        Styles.setImageAlpha(this.replyButton, 64);
        Styles.setSendMessageButtonIconColor(this.context, this.replyButton.getDrawable(), false);
    }

    public String getReply() {
        return this.replyField.getText().toString();
    }

    public void setReply(String str) {
        this.replyField.setText(str);
        this.replyField.setSelection(this.replyField.getText().length());
    }

    public void updateConversationResolutionQuestionUI(boolean z) {
        if (z) {
            hideKeyboard();
            this.confirmationBoxView.setVisibility(0);
            return;
        }
        this.confirmationBoxView.setVisibility(8);
    }

    public void updateSendReplyUI(boolean z) {
        if (z) {
            showSendReplyUI();
        } else {
            hideSendReplyUI();
        }
    }

    public void showSendReplyUI() {
        setMessagesViewBottomPadding();
        this.replyBoxView.setVisibility(0);
    }

    public void hideSendReplyUI() {
        this.messagesRecyclerView.setPadding(0, 0, 0, 0);
        this.replyBoxView.setVisibility(8);
    }

    private void launchAttachmentIntentInternal(Intent intent, File file) {
        if (intent.resolveActivity(this.context.getPackageManager()) != null) {
            this.context.startActivity(intent);
        } else if (HelpshiftContext.getCoreApi().getDelegate().isDelegateRegistered()) {
            HelpshiftContext.getCoreApi().getDelegate().displayAttachmentFile(file);
        } else {
            showErrorView(PlatformException.NO_APPS_FOR_OPENING_ATTACHMENT);
        }
    }

    public void launchScreenshotAttachment(String str, String str2) {
        File validateAndCreateFile = FileUtil.validateAndCreateFile(str);
        if (validateAndCreateFile != null) {
            launchAttachmentIntentInternal(IntentUtil.createFileProviderIntent(this.context, validateAndCreateFile, str2), validateAndCreateFile);
        } else {
            showErrorView(PlatformException.FILE_NOT_FOUND);
        }
    }

    public void launchAttachment(String str, String str2) {
        Intent intent;
        if (HsUriUtils.isValidUriPath(str)) {
            Uri parse = Uri.parse(str);
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setFlags(1);
            intent2.setDataAndType(parse, str2);
            if (intent2.resolveActivity(this.context.getPackageManager()) != null) {
                this.context.startActivity(intent2);
            } else {
                showErrorView(PlatformException.NO_APPS_FOR_OPENING_ATTACHMENT);
            }
        } else {
            File validateAndCreateFile = FileUtil.validateAndCreateFile(str);
            if (validateAndCreateFile != null) {
                if (Build.VERSION.SDK_INT >= 24) {
                    intent = IntentUtil.createFileProviderIntent(this.context, validateAndCreateFile, str2);
                } else {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    intent3.setDataAndType(Uri.fromFile(validateAndCreateFile), str2);
                    intent = intent3;
                }
                launchAttachmentIntentInternal(intent, validateAndCreateFile);
                return;
            }
            showErrorView(PlatformException.FILE_NOT_FOUND);
        }
    }

    public void showErrorView(ExceptionType exceptionType) {
        SnackbarUtil.showSnackbar(exceptionType, this.parentView);
    }

    public void openAppReviewStore(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        if (intent.resolveActivity(this.context.getPackageManager()) != null) {
            this.context.startActivity(intent);
        } else {
            showErrorView(PlatformException.NO_APPS_FOR_OPENING_ATTACHMENT);
        }
    }

    public void updateConversationFooterState(ConversationFooterState conversationFooterState) {
        if (this.messagesAdapter != null) {
            if (conversationFooterState != ConversationFooterState.NONE) {
                hideKeyboard();
            }
            this.messagesAdapter.setConversationFooterState(conversationFooterState);
        }
    }

    public void updateHistoryLoadingState(HistoryLoadingState historyLoadingState) {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.setHistoryLoadingState(historyLoadingState);
        }
    }

    public void showCSATSubmittedView() {
        SnackbarUtil.showSnackbar(this.parentView, this.context.getResources().getString(R.string.hs__csat_submit_toast), 0);
    }

    public void openFreshConversationScreen(Map<String, Boolean> map) {
        this.conversationFragmentRouter.openFreshConversationScreen(map);
    }

    public void showAgentTypingIndicator() {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.setAgentTypingIndicatorVisibility(true);
        }
    }

    public void hideAgentTypingIndicator() {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.setAgentTypingIndicatorVisibility(false);
        }
    }

    public void onAuthenticationFailure() {
        if (this.conversationFragmentRouter != null) {
            this.conversationFragmentRouter.onAuthenticationFailure();
        }
    }

    public boolean isReplyBoxVisible() {
        return this.replyBoxView.getVisibility() == 0;
    }

    public void setReplyboxListeners() {
        this.replyField.addTextChangedListener(new TextWatcherAdapter() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (ConversationFragmentRenderer.this.conversationFragmentRouter != null) {
                    ConversationFragmentRenderer.this.conversationFragmentRouter.onTextChanged(charSequence, i, i2, i3);
                }
            }
        });
        this.replyField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return false;
                }
                ConversationFragmentRenderer.this.replyButton.performClick();
                return false;
            }
        });
        this.replyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (ConversationFragmentRenderer.this.conversationFragmentRouter != null) {
                    ConversationFragmentRenderer.this.conversationFragmentRouter.onSendButtonClick();
                }
            }
        });
    }

    public void requestReplyFieldFocus() {
        this.replyField.requestFocus();
    }

    public void showKeyboard() {
        KeyboardUtil.showKeyboard(this.context, this.replyField);
    }

    public void hideKeyboard() {
        KeyboardUtil.hideKeyboard(this.context, this.replyField);
    }

    public void notifyRefreshList() {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.notifyDataSetChanged();
        }
    }

    public void destroy() {
        this.conversationFragmentRouter = null;
    }

    public void updateScrollJumperView(boolean z, boolean z2) {
        if (z) {
            showScrollJumperView(z2);
        } else {
            hideScrollJumperView();
        }
    }

    private void showScrollJumperView(boolean z) {
        String str;
        this.scrollIndicator.setVisibility(0);
        if (z) {
            this.unreadMessagesIndicatorDot.setVisibility(0);
            str = this.context.getString(R.string.hs__jump_button_with_new_message_voice_over);
        } else {
            this.unreadMessagesIndicatorDot.setVisibility(8);
            str = this.context.getString(R.string.hs__jump_button_voice_over);
        }
        this.scrollJumpButton.setContentDescription(str);
    }

    private void hideScrollJumperView() {
        this.scrollIndicator.setVisibility(8);
        this.unreadMessagesIndicatorDot.setVisibility(8);
    }

    public void scrollToBottom() {
        int itemCount;
        if (this.messagesAdapter != null && (itemCount = this.messagesAdapter.getItemCount()) > 0) {
            this.messagesRecyclerView.scrollToPosition(itemCount - 1);
        }
    }

    /* access modifiers changed from: protected */
    public void setMessagesViewBottomPadding() {
        this.messagesRecyclerView.setPadding(0, 0, 0, (int) com.helpshift.util.Styles.dpToPx(this.context, 12.0f));
    }

    private void changeMenuItemVisibility(HSMenuItemType hSMenuItemType, boolean z) {
        if (this.menuItemRenderer != null) {
            this.menuItemRenderer.updateMenuItemVisibility(hSMenuItemType, z);
        }
    }

    public void unregisterFragmentRenderer() {
        if (this.messagesAdapter != null) {
            this.messagesAdapter.unregisterAdapterClickListener();
        }
    }
}
