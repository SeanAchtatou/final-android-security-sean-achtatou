package com.helpshift.support.conversations.messages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.helpshift.R;
import com.helpshift.common.StringUtils;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.UIViewState;
import com.helpshift.util.HSLinkify;

class AdminMessageViewDataBinder extends MessageViewDataBinder<ViewHolder, MessageDM> {
    AdminMessageViewDataBinder(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ViewHolder createViewHolder(ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hs__msg_txt_admin, viewGroup, false));
        viewHolder.setListeners();
        return viewHolder;
    }

    public void bind(ViewHolder viewHolder, final MessageDM messageDM) {
        if (StringUtils.isEmpty(messageDM.body)) {
            viewHolder.messageLayout.setVisibility(8);
            return;
        }
        viewHolder.messageLayout.setVisibility(0);
        viewHolder.messageText.setText(escapeHtml(messageDM.body));
        UIViewState uiViewState = messageDM.getUiViewState();
        setAdminMessageContainerBackground(viewHolder.messageContainer, uiViewState);
        setAdminMessageSubText(viewHolder.dateText, uiViewState, messageDM.getSubText());
        viewHolder.messageLayout.setContentDescription(getAdminMessageContentDesciption(messageDM));
        linkify(viewHolder.messageText, new HSLinkify.LinkClickListener() {
            public void onLinkClicked(String str) {
                if (AdminMessageViewDataBinder.this.messageClickListener != null) {
                    AdminMessageViewDataBinder.this.messageClickListener.onAdminMessageLinkClicked(str, messageDM);
                }
            }
        });
    }

    protected final class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        final TextView dateText;
        final View messageContainer;
        final View messageLayout;
        final TextView messageText;

        ViewHolder(View view) {
            super(view);
            this.messageLayout = view.findViewById(R.id.admin_text_message_layout);
            this.messageText = (TextView) view.findViewById(R.id.admin_message_text);
            this.dateText = (TextView) view.findViewById(R.id.admin_date_text);
            this.messageContainer = view.findViewById(R.id.admin_message_container);
        }

        /* access modifiers changed from: package-private */
        public void setListeners() {
            this.messageText.setOnCreateContextMenuListener(this);
        }

        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (AdminMessageViewDataBinder.this.messageClickListener != null) {
                AdminMessageViewDataBinder.this.messageClickListener.onCreateContextMenu(contextMenu, ((TextView) view).getText().toString());
            }
        }
    }
}
