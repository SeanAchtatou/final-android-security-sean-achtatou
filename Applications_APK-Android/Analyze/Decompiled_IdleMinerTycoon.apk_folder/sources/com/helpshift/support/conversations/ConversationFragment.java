package com.helpshift.support.conversations;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageButton;
import com.helpshift.R;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.platform.Device;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.viewmodel.ConversationVM;
import com.helpshift.support.conversations.HSRecyclerViewScrollListener;
import com.helpshift.support.conversations.messages.MessagesAdapterClickListener;
import com.helpshift.support.fragments.HSMenuItemType;
import com.helpshift.support.fragments.IMenuItemEventListener;
import com.helpshift.support.fragments.ScreenshotPreviewFragment;
import com.helpshift.support.util.AppSessionConstants;
import com.helpshift.support.util.SnackbarUtil;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.util.Styles;
import com.helpshift.widget.ButtonViewState;
import com.helpshift.widget.ConversationFooterViewState;
import com.helpshift.widget.HSObserver;
import com.helpshift.widget.HistoryLoadingViewState;
import com.helpshift.widget.ReplyFieldViewState;
import com.helpshift.widget.ScrollJumperViewState;
import com.vungle.warren.ui.JavascriptBridge;
import java.util.HashMap;
import java.util.Map;

public class ConversationFragment extends BaseConversationFragment implements MessagesAdapterClickListener, ConversationFragmentRouter, IMenuItemEventListener, HSRecyclerViewScrollListener.RecyclerViewScrollCallback {
    public static final String BUNDLE_ARG_CONVERSATION_LOCAL_ID = "issueId";
    public static final String BUNDLE_ARG_SHOW_CONVERSATION_HISTORY = "show_conv_history";
    public static final String FRAGMENT_TAG = "HSConversationFragment";
    private static final String TAG = "Helpshift_ConvFragment";
    private final String SHOULD_SHOW_UNREAD_MESSAGE_INDICATOR = "should_show_unread_message_indicator";
    protected Long conversationId;
    ConversationVM conversationVM;
    private HSRecyclerViewScrollListener hsRecyclerViewScrollListener;
    private String imageRefersId;
    private boolean isConversationVMInitialized = false;
    private int lastSoftInputMode;
    private int lastWindowFlags;
    private RecyclerView messagesRecyclerView;
    private AttachmentMessageDM readableAttachmentMessage;
    protected ConversationFragmentRenderer renderer;
    protected boolean retainMessageBoxOnUI;
    private ImagePickerFile selectedImageFile;
    private String selectedImageRefersId;
    private boolean shouldUpdateAttachment;

    /* access modifiers changed from: protected */
    public int getScreenshotMode() {
        return 3;
    }

    public void handleOptionSelected(OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z) {
    }

    public static ConversationFragment newInstance(Bundle bundle) {
        ConversationFragment conversationFragment = new ConversationFragment();
        conversationFragment.setArguments(bundle);
        return conversationFragment;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (isChangingConfigurations() && this.renderer != null) {
            this.retainMessageBoxOnUI = this.renderer.isReplyBoxVisible();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.lastWindowFlags = getActivity().getWindow().getAttributes().flags;
        getActivity().getWindow().addFlags(2048);
        getActivity().getWindow().clearFlags(1024);
        return layoutInflater.inflate(R.layout.hs__conversation_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.conversationId = Long.valueOf(arguments.getLong(BUNDLE_ARG_CONVERSATION_LOCAL_ID));
        }
        inflateReplyBoxView(view);
        initialize(view);
        super.onViewCreated(view, bundle);
        if (bundle != null) {
            this.conversationVM.updateUnreadMessageCountIndicator(bundle.getBoolean("should_show_unread_message_indicator"));
        }
        HSLogger.d(TAG, "Now showing conversation screen");
    }

    /* access modifiers changed from: protected */
    public void inflateReplyBoxView(View view) {
        ViewStub viewStub = (ViewStub) view.findViewById(R.id.replyBoxViewStub);
        viewStub.setLayoutResource(R.layout.hs__conversation_replyboxview);
        viewStub.inflate();
    }

    /* access modifiers changed from: protected */
    public void initConversationVM() {
        this.conversationVM = HelpshiftContext.getCoreApi().getConversationViewModel(this.conversationId, this.renderer, this.retainMessageBoxOnUI);
    }

    /* access modifiers changed from: protected */
    public void initRenderer(RecyclerView recyclerView, View view, View view2, View view3) {
        this.renderer = new ConversationFragmentRenderer(getContext(), recyclerView, getView(), view, this, view2, view3, getSupportFragment());
    }

    /* access modifiers changed from: protected */
    public void initialize(View view) {
        this.messagesRecyclerView = (RecyclerView) view.findViewById(R.id.hs__messagesList);
        View findViewById = view.findViewById(R.id.hs__confirmation);
        View findViewById2 = view.findViewById(R.id.scroll_indicator);
        View findViewById3 = view.findViewById(R.id.unread_indicator_red_dot);
        View findViewById4 = view.findViewById(R.id.unread_indicator_red_dot_image_view);
        if (Build.VERSION.SDK_INT < 21) {
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.hs__ring);
            findViewById2.setBackgroundDrawable(drawable);
            findViewById3.setBackgroundDrawable(drawable);
        }
        Styles.setDrawable(getContext(), findViewById4, R.drawable.hs__circle, R.attr.colorAccent);
        initRenderer(this.messagesRecyclerView, findViewById, findViewById2, findViewById3);
        initConversationVM();
        this.renderer.setReplyboxListeners();
        this.retainMessageBoxOnUI = false;
        this.conversationVM.startLiveUpdates();
        this.isConversationVMInitialized = true;
        if (this.shouldUpdateAttachment) {
            this.conversationVM.sendScreenShot(this.selectedImageFile, this.selectedImageRefersId);
            this.shouldUpdateAttachment = false;
        }
        view.findViewById(R.id.resolution_accepted_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationFragment.this.conversationVM.markConversationResolutionStatus(true);
            }
        });
        view.findViewById(R.id.resolution_rejected_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationFragment.this.renderer.requestReplyFieldFocus();
                ConversationFragment.this.renderer.showKeyboard();
                ConversationFragment.this.conversationVM.markConversationResolutionStatus(false);
            }
        });
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.scroll_jump_button);
        Styles.setDrawable(getContext(), imageButton, R.drawable.hs__circle_shape_scroll_jump, R.attr.hs__composeBackgroundColor);
        Styles.setColorFilter(getContext(), imageButton.getDrawable(), R.attr.hs__selectableOptionColor);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationFragment.this.conversationVM.onScrollJumperViewClicked();
            }
        });
        this.hsRecyclerViewScrollListener = new HSRecyclerViewScrollListener(new Handler(), this);
        this.messagesRecyclerView.addOnScrollListener(this.hsRecyclerViewScrollListener);
    }

    private void addViewStateObservers() {
        Domain domain = HelpshiftContext.getCoreApi().getDomain();
        this.conversationVM.getReplyFieldViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.setReply(((ReplyFieldViewState) obj).getReplyText());
            }
        });
        this.conversationVM.getHistoryLoadingViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.updateHistoryLoadingState(((HistoryLoadingViewState) obj).getState());
            }
        });
        this.conversationVM.getScrollJumperViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ScrollJumperViewState scrollJumperViewState = (ScrollJumperViewState) obj;
                ConversationFragment.this.renderer.updateScrollJumperView(scrollJumperViewState.isVisible(), scrollJumperViewState.shouldShowUnreadMessagesIndicator());
            }
        });
        this.conversationVM.getConversationFooterViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.updateConversationFooterState(((ConversationFooterViewState) obj).getState());
            }
        });
        this.conversationVM.getReplyBoxViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.updateSendReplyUI(((ButtonViewState) obj).isVisible());
            }
        });
        this.conversationVM.getReplyButtonViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.updateSendReplyButton(((ButtonViewState) obj).isEnabled());
            }
        });
        this.conversationVM.getAttachImageButtonViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.updateImageAttachmentButtonView(((ButtonViewState) obj).isVisible());
            }
        });
        this.conversationVM.getConfirmationBoxViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                ConversationFragment.this.renderer.updateConversationResolutionQuestionUI(((ButtonViewState) obj).isVisible());
            }
        });
    }

    private void removeViewStateObservers() {
        this.conversationVM.getReplyFieldViewState().unsubscribe();
        this.conversationVM.getHistoryLoadingViewState().unsubscribe();
        this.conversationVM.getScrollJumperViewState().unsubscribe();
        this.conversationVM.getConversationFooterViewState().unsubscribe();
        this.conversationVM.getAttachImageButtonViewState().unsubscribe();
        this.conversationVM.getReplyBoxViewState().unsubscribe();
        this.conversationVM.getReplyButtonViewState().unsubscribe();
        this.conversationVM.getConfirmationBoxViewState().unsubscribe();
    }

    public void onResume() {
        super.onResume();
        addViewStateObservers();
        this.conversationVM.onResume();
        this.lastSoftInputMode = getActivity().getWindow().getAttributes().softInputMode;
        getActivity().getWindow().setSoftInputMode(16);
        if (!isChangingConfigurations()) {
            String str = this.conversationVM.viewableConversation.getActiveConversation().serverId;
            if (!StringUtils.isEmpty(str)) {
                HashMap hashMap = new HashMap();
                hashMap.put("id", str);
                this.conversationVM.pushAnalyticsEvent(AnalyticsEventType.OPEN_ISSUE, hashMap);
            }
        }
    }

    public void onPause() {
        getActivity().getWindow().setSoftInputMode(this.lastSoftInputMode);
        this.renderer.hideKeyboard();
        removeViewStateObservers();
        this.conversationVM.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public AppSessionConstants.Screen getViewName() {
        return AppSessionConstants.Screen.CONVERSATION;
    }

    public boolean handleScreenshotAction(ScreenshotPreviewFragment.ScreenshotAction screenshotAction, ImagePickerFile imagePickerFile, @Nullable String str) {
        if (AnonymousClass13.$SwitchMap$com$helpshift$support$fragments$ScreenshotPreviewFragment$ScreenshotAction[screenshotAction.ordinal()] != 1) {
            return false;
        }
        if (!this.isConversationVMInitialized || this.conversationVM == null) {
            this.selectedImageFile = imagePickerFile;
            this.selectedImageRefersId = str;
            this.shouldUpdateAttachment = true;
        } else {
            this.conversationVM.sendScreenShot(imagePickerFile, str);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public String getToolbarTitle() {
        return getString(R.string.hs__conversation_header);
    }

    public void onAdminMessageLinkClicked(String str, MessageDM messageDM) {
        this.conversationVM.onAdminMessageLinkClicked(str, messageDM);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, final String str) {
        if (!StringUtils.isEmpty(str)) {
            contextMenu.add(0, 0, 0, R.string.hs__copy).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    ConversationFragment.this.copyToClipboard(str);
                    return true;
                }
            });
        }
    }

    public void retryMessage(MessageDM messageDM) {
        this.conversationVM.retryMessage(messageDM);
    }

    public void launchImagePicker(RequestScreenshotMessageDM requestScreenshotMessageDM) {
        this.imageRefersId = requestScreenshotMessageDM.serverId;
        this.conversationVM.onImageAttachmentButtonClick();
        Bundle bundle = new Bundle();
        bundle.putInt(ScreenshotPreviewFragment.KEY_SCREENSHOT_MODE, getScreenshotMode());
        bundle.putString(ScreenshotPreviewFragment.KEY_MESSAGE_REFERS_ID, this.imageRefersId);
        getSupportFragment().launchImagePicker(true, bundle);
    }

    public void handleReplyReviewButtonClick(RequestAppReviewMessageDM requestAppReviewMessageDM) {
        this.conversationVM.handleAppReviewRequestClick(requestAppReviewMessageDM);
    }

    public void onScreenshotMessageClicked(ScreenshotMessageDM screenshotMessageDM) {
        this.conversationVM.handleScreenshotMessageClick(screenshotMessageDM);
    }

    public void handleGenericAttachmentMessageClick(AdminAttachmentMessageDM adminAttachmentMessageDM) {
        checkWriteStoragePermissionAndDelegateToVM(adminAttachmentMessageDM.isWriteStoragePermissionRequired(), adminAttachmentMessageDM);
    }

    public void handleAdminImageAttachmentMessageClick(AdminImageAttachmentMessageDM adminImageAttachmentMessageDM) {
        checkWriteStoragePermissionAndDelegateToVM(true, adminImageAttachmentMessageDM);
    }

    public void onStartNewConversationButtonClick() {
        this.conversationVM.onNewConversationButtonClicked();
        this.renderer.openFreshConversationScreen(new HashMap());
    }

    public void onCSATSurveySubmitted(int i, String str) {
        this.conversationVM.onCSATSurveySubmitted(i, str);
    }

    public void onAdminSuggestedQuestionSelected(MessageDM messageDM, String str, String str2) {
        getSupportController().onAdminSuggestedQuestionSelected(str, str2, null);
    }

    public void onHistoryLoadingRetryClicked() {
        this.conversationVM.retryHistoryLoadingMessages();
    }

    private void checkWriteStoragePermissionAndDelegateToVM(boolean z, AttachmentMessageDM attachmentMessageDM) {
        this.readableAttachmentMessage = null;
        if (z) {
            Device device = HelpshiftContext.getPlatform().getDevice();
            switch (device.checkPermission(Device.PermissionType.WRITE_STORAGE)) {
                case AVAILABLE:
                    this.conversationVM.handleAdminAttachmentMessageClick(attachmentMessageDM);
                    return;
                case UNAVAILABLE:
                    startDownloadWithSystemService(attachmentMessageDM.attachmentUrl);
                    return;
                case REQUESTABLE:
                    this.readableAttachmentMessage = attachmentMessageDM;
                    requestWriteExternalStoragePermission(true);
                    return;
                default:
                    return;
            }
        } else {
            this.conversationVM.handleAdminAttachmentMessageClick(attachmentMessageDM);
        }
    }

    /* access modifiers changed from: protected */
    public void onPermissionGranted(int i) {
        switch (i) {
            case 2:
                Bundle bundle = new Bundle();
                bundle.putInt(ScreenshotPreviewFragment.KEY_SCREENSHOT_MODE, getScreenshotMode());
                bundle.putString(ScreenshotPreviewFragment.KEY_MESSAGE_REFERS_ID, this.imageRefersId);
                getSupportFragment().launchImagePicker(false, bundle);
                return;
            case 3:
                if (this.readableAttachmentMessage != null) {
                    this.conversationVM.handleAdminAttachmentMessageClick(this.readableAttachmentMessage);
                    this.readableAttachmentMessage = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void startDownloadWithSystemService(String str) {
        DownloadManager downloadManager = (DownloadManager) getContext().getSystemService(JavascriptBridge.MraidHandler.DOWNLOAD_ACTION);
        if (downloadManager != null) {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(str));
            request.setNotificationVisibility(1);
            downloadManager.enqueue(request);
            if (!isDetached()) {
                SnackbarUtil.showSnackbar(getView(), R.string.hs__starting_download, -1);
            }
        }
    }

    public void openFreshConversationScreen(Map<String, Boolean> map) {
        getSupportFragment().getSupportController().startConversationFlow(map);
    }

    public void onDetach() {
        if (!isChangingConfigurations()) {
            HelpshiftContext.getCoreApi().getConversationInboxPoller().startAppPoller(true);
        }
        super.onDetach();
    }

    public void onDestroyView() {
        if (getActivity() != null) {
            getActivity().getWindow().clearFlags(2048);
            getActivity().getWindow().setFlags(this.lastWindowFlags, this.lastWindowFlags);
        }
        this.isConversationVMInitialized = false;
        this.conversationVM.setConversationViewState(-1);
        this.renderer.unregisterFragmentRenderer();
        this.conversationVM.unregisterRenderer();
        this.renderer.destroy();
        this.messagesRecyclerView.removeOnScrollListener(this.hsRecyclerViewScrollListener);
        this.messagesRecyclerView = null;
        super.onDestroyView();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("should_show_unread_message_indicator", this.conversationVM.shouldShowUnreadMessagesIndicator());
    }

    public void startLiveUpdates() {
        if (this.conversationVM != null) {
            this.conversationVM.startLiveUpdates();
        }
    }

    public void stopLiveUpdates() {
        if (this.conversationVM != null) {
            this.conversationVM.stopLiveUpdates();
        }
    }

    public void onCreateOptionMenuCalled() {
        this.conversationVM.renderMenuItems();
        this.renderer.updateImageAttachmentButtonView(this.conversationVM.getAttachImageButtonViewState().isVisible());
    }

    /* renamed from: com.helpshift.support.conversations.ConversationFragment$13  reason: invalid class name */
    static /* synthetic */ class AnonymousClass13 {
        static final /* synthetic */ int[] $SwitchMap$com$helpshift$support$fragments$HSMenuItemType = new int[HSMenuItemType.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$helpshift$support$fragments$ScreenshotPreviewFragment$ScreenshotAction = new int[ScreenshotPreviewFragment.ScreenshotAction.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(11:0|(2:1|2)|3|5|6|7|8|9|10|11|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|5|6|7|8|9|10|11|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0027 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0032 */
        static {
            /*
                com.helpshift.support.fragments.HSMenuItemType[] r0 = com.helpshift.support.fragments.HSMenuItemType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$support$fragments$HSMenuItemType = r0
                r0 = 1
                int[] r1 = com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$support$fragments$HSMenuItemType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.helpshift.support.fragments.HSMenuItemType r2 = com.helpshift.support.fragments.HSMenuItemType.SCREENSHOT_ATTACHMENT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                com.helpshift.common.platform.Device$PermissionState[] r1 = com.helpshift.common.platform.Device.PermissionState.values()
                int r1 = r1.length
                int[] r1 = new int[r1]
                com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$common$platform$Device$PermissionState = r1
                int[] r1 = com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$common$platform$Device$PermissionState     // Catch:{ NoSuchFieldError -> 0x0027 }
                com.helpshift.common.platform.Device$PermissionState r2 = com.helpshift.common.platform.Device.PermissionState.AVAILABLE     // Catch:{ NoSuchFieldError -> 0x0027 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0027 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0027 }
            L_0x0027:
                int[] r1 = com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$common$platform$Device$PermissionState     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.helpshift.common.platform.Device$PermissionState r2 = com.helpshift.common.platform.Device.PermissionState.UNAVAILABLE     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r3 = 2
                r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r1 = com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$common$platform$Device$PermissionState     // Catch:{ NoSuchFieldError -> 0x003d }
                com.helpshift.common.platform.Device$PermissionState r2 = com.helpshift.common.platform.Device.PermissionState.REQUESTABLE     // Catch:{ NoSuchFieldError -> 0x003d }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x003d }
                r3 = 3
                r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x003d }
            L_0x003d:
                com.helpshift.support.fragments.ScreenshotPreviewFragment$ScreenshotAction[] r1 = com.helpshift.support.fragments.ScreenshotPreviewFragment.ScreenshotAction.values()
                int r1 = r1.length
                int[] r1 = new int[r1]
                com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$support$fragments$ScreenshotPreviewFragment$ScreenshotAction = r1
                int[] r1 = com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.$SwitchMap$com$helpshift$support$fragments$ScreenshotPreviewFragment$ScreenshotAction     // Catch:{ NoSuchFieldError -> 0x0050 }
                com.helpshift.support.fragments.ScreenshotPreviewFragment$ScreenshotAction r2 = com.helpshift.support.fragments.ScreenshotPreviewFragment.ScreenshotAction.SEND     // Catch:{ NoSuchFieldError -> 0x0050 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0050 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0050 }
            L_0x0050:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.helpshift.support.conversations.ConversationFragment.AnonymousClass13.<clinit>():void");
        }
    }

    public void onMenuItemClicked(HSMenuItemType hSMenuItemType) {
        if (AnonymousClass13.$SwitchMap$com$helpshift$support$fragments$HSMenuItemType[hSMenuItemType.ordinal()] == 1) {
            this.imageRefersId = null;
            this.conversationVM.onImageAttachmentButtonClick();
            Bundle bundle = new Bundle();
            bundle.putInt(ScreenshotPreviewFragment.KEY_SCREENSHOT_MODE, getScreenshotMode());
            bundle.putString(ScreenshotPreviewFragment.KEY_MESSAGE_REFERS_ID, null);
            getSupportFragment().launchImagePicker(true, bundle);
        }
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.conversationVM.toggleReplySendButton(charSequence != null && !StringUtils.isEmpty(charSequence.toString()));
    }

    public void onSendButtonClick() {
        this.conversationVM.sendTextMessage();
    }

    public void onSkipClick() {
        this.conversationVM.onSkipClick();
    }

    public void onAuthenticationFailure() {
        getSupportController().onAuthenticationFailure();
    }

    public void onScrolledToTop() {
        this.conversationVM.onScrolledToTop();
    }

    public void onScrolledToBottom() {
        this.conversationVM.onScrolledToBottom();
    }

    public void onScrolling() {
        this.conversationVM.onScrolling();
    }
}
