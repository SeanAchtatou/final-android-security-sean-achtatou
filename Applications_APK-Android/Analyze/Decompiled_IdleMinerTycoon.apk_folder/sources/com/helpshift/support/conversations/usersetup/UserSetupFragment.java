package com.helpshift.support.conversations.usersetup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.helpshift.R;
import com.helpshift.common.domain.Domain;
import com.helpshift.conversation.activeconversation.usersetup.UserSetupRenderer;
import com.helpshift.conversation.usersetup.UserSetupVM;
import com.helpshift.network.connectivity.HSConnectivityManager;
import com.helpshift.network.connectivity.HSNetworkConnectivityCallback;
import com.helpshift.support.controllers.SupportController;
import com.helpshift.support.fragments.MainFragment;
import com.helpshift.support.fragments.SupportFragment;
import com.helpshift.support.util.Styles;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.widget.HSObserver;
import com.helpshift.widget.ProgressBarViewState;
import com.helpshift.widget.ProgressDescriptionViewState;
import com.helpshift.widget.UserOfflineErrorViewState;

public class UserSetupFragment extends MainFragment implements UserSetupRenderer, HSNetworkConnectivityCallback {
    public static final String FRAGMENT_TAG = "HSUserSetupFragment";
    private View offlineErrorView;
    private ProgressBar progressBar;
    private View progressDescriptionView;
    private UserSetupVM userSetupVM;

    public boolean shouldRefreshMenu() {
        return true;
    }

    public static UserSetupFragment newInstance() {
        return new UserSetupFragment();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.hs__user_setup_fragment, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        initialize(view);
        super.onViewCreated(view, bundle);
    }

    public void onResume() {
        super.onResume();
        addViewStateObservers();
        setToolbarTitle(getString(R.string.hs__conversation_header));
        HSConnectivityManager.getInstance().registerNetworkConnectivityListener(this);
        this.userSetupVM.onResume();
    }

    public void onDestroyView() {
        this.userSetupVM.onDestroyView();
        super.onDestroyView();
    }

    private void addViewStateObservers() {
        Domain domain = HelpshiftContext.getCoreApi().getDomain();
        this.userSetupVM.getProgressBarViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                if (((ProgressBarViewState) obj).isVisible()) {
                    UserSetupFragment.this.showProgressBar();
                } else {
                    UserSetupFragment.this.hideProgressBar();
                }
            }
        });
        this.userSetupVM.getDescriptionProgressViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                if (((ProgressDescriptionViewState) obj).isVisible()) {
                    UserSetupFragment.this.showProgressDescription();
                } else {
                    UserSetupFragment.this.hideProgressDescription();
                }
            }
        });
        this.userSetupVM.getUserOfflineErrorViewState().subscribe(domain, new HSObserver() {
            public void onChanged(Object obj) {
                if (((UserOfflineErrorViewState) obj).isVisible()) {
                    UserSetupFragment.this.showNoInternetView();
                } else {
                    UserSetupFragment.this.hideNoInternetView();
                }
            }
        });
    }

    private void removeViewStateObservers() {
        this.userSetupVM.getProgressBarViewState().unsubscribe();
        this.userSetupVM.getDescriptionProgressViewState().unsubscribe();
        this.userSetupVM.getUserOfflineErrorViewState().unsubscribe();
    }

    private void initialize(View view) {
        this.progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        Styles.setAccentColor(getContext(), this.progressBar.getIndeterminateDrawable());
        this.progressDescriptionView = view.findViewById(R.id.progress_description_text_view);
        this.offlineErrorView = view.findViewById(R.id.offline_error_view);
        com.helpshift.util.Styles.setColorFilter(getContext(), ((ImageView) view.findViewById(R.id.info_icon)).getDrawable(), 16842806);
        this.userSetupVM = HelpshiftContext.getCoreApi().getUserSetupVM(this);
    }

    public void onPause() {
        removeViewStateObservers();
        HSConnectivityManager.getInstance().unregisterNetworkConnectivityListener(this);
        super.onPause();
    }

    public void showNoInternetView() {
        this.offlineErrorView.setVisibility(0);
    }

    public void hideNoInternetView() {
        this.offlineErrorView.setVisibility(8);
    }

    public void showProgressBar() {
        this.progressBar.setVisibility(0);
    }

    public void hideProgressBar() {
        this.progressBar.setVisibility(8);
    }

    public void showProgressDescription() {
        this.progressDescriptionView.setVisibility(0);
    }

    public void hideProgressDescription() {
        this.progressDescriptionView.setVisibility(8);
    }

    public void onUserSetupComplete() {
        getSupportController().onUserSetupSyncCompleted();
    }

    public void onAuthenticationFailure() {
        getSupportController().onAuthenticationFailure();
    }

    private SupportController getSupportController() {
        return ((SupportFragment) getParentFragment()).getSupportController();
    }

    public void onNetworkAvailable() {
        this.userSetupVM.onNetworkAvailable();
    }

    public void onNetworkUnavailable() {
        this.userSetupVM.onNetworkUnavailable();
    }
}
