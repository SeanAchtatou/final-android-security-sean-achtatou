package com.helpshift.support.conversations;

import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.support.Faq;
import java.util.ArrayList;

public interface NewConversationRouter {
    void exitNewConversationView();

    void onAuthenticationFailure();

    void showAttachmentPreviewScreenFromDraft(ImagePickerFile imagePickerFile);

    void showConversationScreen();

    void showSearchResultFragment(ArrayList<Faq> arrayList);
}
