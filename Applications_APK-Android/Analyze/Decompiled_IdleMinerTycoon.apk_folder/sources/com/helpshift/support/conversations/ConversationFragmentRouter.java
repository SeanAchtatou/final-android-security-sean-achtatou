package com.helpshift.support.conversations;

import com.helpshift.support.conversations.messages.MessagesAdapterClickListener;
import java.util.Map;

public interface ConversationFragmentRouter extends MessagesAdapterClickListener {
    void onAuthenticationFailure();

    void onSendButtonClick();

    void onSkipClick();

    void onTextChanged(CharSequence charSequence, int i, int i2, int i3);

    void openFreshConversationScreen(Map<String, Boolean> map);
}
