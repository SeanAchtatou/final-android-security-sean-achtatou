package com.helpshift.support;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import com.helpshift.R;
import com.helpshift.activities.MainActivity;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.support.activities.ParentActivity;
import com.helpshift.support.conversations.NewConversationFragment;
import com.helpshift.support.fragments.SupportFragment;
import com.helpshift.support.fragments.SupportFragmentConstants;
import com.helpshift.support.storage.IMAppSessionStorage;
import com.helpshift.support.util.AppSessionConstants;
import com.helpshift.util.ActivityUtil;
import com.helpshift.util.HelpshiftContext;
import com.helpshift.views.FontApplier;
import java.util.HashMap;

public final class HSReviewFragment extends DialogFragment {
    private static AlertToRateAppListener alertToRateAppListener;
    private final String TAG = "Helpshift_ReviewFrag";
    private boolean disableReview = true;
    String rurl = "";

    protected static void setAlertToRateAppListener(AlertToRateAppListener alertToRateAppListener2) {
        alertToRateAppListener = alertToRateAppListener2;
    }

    public Dialog onCreateDialog(Bundle bundle) {
        FragmentActivity activity = getActivity();
        Bundle extras = activity.getIntent().getExtras();
        if (extras != null) {
            this.disableReview = extras.getBoolean("disableReview", true);
            this.rurl = extras.getString("rurl");
        }
        return initAlertDialog(activity);
    }

    public void onCancel(DialogInterface dialogInterface) {
        sendReviewActionEvent("later");
        sendAlertToRateAppAction(2);
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.disableReview) {
            HelpshiftContext.getCoreApi().getSDKConfigurationDM().setAppReviewed(true);
        }
        getActivity().finish();
    }

    /* access modifiers changed from: package-private */
    public void gotoApp(String str) {
        if (!TextUtils.isEmpty(str)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(str.trim()));
            if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                getContext().startActivity(intent);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void sendAlertToRateAppAction(int i) {
        if (alertToRateAppListener != null) {
            alertToRateAppListener.onAction(i);
        }
        alertToRateAppListener = null;
    }

    private Dialog initAlertDialog(FragmentActivity fragmentActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(fragmentActivity);
        builder.setMessage(R.string.hs__review_message);
        AlertDialog create = builder.create();
        create.setTitle(R.string.hs__review_title);
        create.setCanceledOnTouchOutside(false);
        create.setButton(-1, getResources().getString(R.string.hs__rate_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (TextUtils.isEmpty(HSReviewFragment.this.rurl)) {
                    HSReviewFragment.this.rurl = HelpshiftContext.getCoreApi().getSDKConfigurationDM().getString(SDKConfigurationDM.REVIEW_URL);
                }
                HSReviewFragment.this.rurl = HSReviewFragment.this.rurl.trim();
                if (!TextUtils.isEmpty(HSReviewFragment.this.rurl)) {
                    HSReviewFragment.this.gotoApp(HSReviewFragment.this.rurl);
                }
                HSReviewFragment.this.sendReviewActionEvent("reviewed");
                HSReviewFragment.this.sendAlertToRateAppAction(0);
            }
        });
        create.setButton(-3, getResources().getString(R.string.hs__feedback_button), new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(DialogInterface dialogInterface, int i) {
                HSReviewFragment.this.sendReviewActionEvent("feedback");
                HSReviewFragment.this.sendAlertToRateAppAction(1);
                AppSessionConstants.Screen screen = (AppSessionConstants.Screen) IMAppSessionStorage.getInstance().get(AppSessionConstants.CURRENT_OPEN_SCREEN);
                if (screen != AppSessionConstants.Screen.NEW_CONVERSATION && screen != AppSessionConstants.Screen.CONVERSATION && screen != AppSessionConstants.Screen.CONVERSATION_INFO && screen != AppSessionConstants.Screen.SCREENSHOT_PREVIEW) {
                    Intent intent = new Intent(HSReviewFragment.this.getContext(), ParentActivity.class);
                    intent.putExtra(SupportFragment.SUPPORT_MODE, 1);
                    intent.putExtra(SupportFragmentConstants.DECOMPOSED, true);
                    intent.putExtra(MainActivity.SHOW_IN_FULLSCREEN, ActivityUtil.isFullScreen(HSReviewFragment.this.getActivity()));
                    intent.putExtra("isRoot", true);
                    intent.putExtra(NewConversationFragment.SEARCH_PERFORMED, true);
                    HSReviewFragment.this.getActivity().startActivity(intent);
                }
            }
        });
        create.setButton(-2, getResources().getString(R.string.hs__review_close_button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                HSReviewFragment.this.sendReviewActionEvent("later");
                HSReviewFragment.this.sendAlertToRateAppAction(2);
            }
        });
        FontApplier.apply(create);
        return create;
    }

    /* access modifiers changed from: package-private */
    public void sendReviewActionEvent(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", "periodic");
        hashMap.put("response", str);
        HelpshiftContext.getCoreApi().getAnalyticsEventDM().pushEvent(AnalyticsEventType.REVIEWED_APP, hashMap);
    }
}
