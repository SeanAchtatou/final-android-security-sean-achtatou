package com.helpshift.support.contracts;

import android.os.Bundle;
import com.helpshift.conversation.dto.ImagePickerFile;

public interface ScreenshotPreviewListener {
    void addScreenshot(ImagePickerFile imagePickerFile);

    void changeScreenshot(Bundle bundle);

    void removeScreenshot();

    void removeScreenshotPreviewFragment();

    void sendScreenshot(ImagePickerFile imagePickerFile, String str);
}
