package com.helpshift.support.contracts;

import android.os.Bundle;

public interface SupportScreenView {
    void exitSdkSession();

    void launchImagePicker(boolean z, Bundle bundle);
}
