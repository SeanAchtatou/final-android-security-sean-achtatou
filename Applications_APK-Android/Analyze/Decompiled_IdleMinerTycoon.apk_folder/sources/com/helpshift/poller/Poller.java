package com.helpshift.poller;

import android.support.annotation.Nullable;
import com.helpshift.common.poller.Delay;
import com.helpshift.network.errors.NetworkError;
import com.helpshift.util.HSLogger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class Poller<V> implements Runnable {
    private static final String TAG = "Helpshift_Poller";
    private final Callable<V> callable;
    private final ExecutorService executorService;
    private final ScheduledExecutorService scheduledExecutorService;
    private boolean started;

    @Nullable
    public abstract Delay getFailDelay(Exception exc);

    @Nullable
    public abstract Delay getSuccessDelay(V v);

    public Poller(Callable<V> callable2, ExecutorService executorService2, ScheduledExecutorService scheduledExecutorService2) {
        this.callable = callable2;
        this.executorService = executorService2;
        this.scheduledExecutorService = scheduledExecutorService2;
    }

    public void shutdown() {
        this.started = false;
        this.scheduledExecutorService.shutdownNow();
        this.executorService.shutdownNow();
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            try {
                this.executorService.execute(this);
            } catch (RejectedExecutionException e) {
                HSLogger.e(TAG, "Rejected execution : ", e);
            }
        }
    }

    public void run() {
        runDelayed(0, TimeUnit.SECONDS);
    }

    /* access modifiers changed from: package-private */
    public void runDelayed(long j, TimeUnit timeUnit) {
        final Delay delay;
        try {
            if (this.started && !this.scheduledExecutorService.isShutdown()) {
                try {
                    delay = getSuccessDelay(this.scheduledExecutorService.schedule(this.callable, j, timeUnit).get());
                } catch (Exception e) {
                    if (e.getCause() instanceof NetworkError) {
                        delay = getFailDelay((NetworkError) e.getCause());
                    } else {
                        delay = getFailDelay(e);
                    }
                }
                if (delay != null) {
                    if (!this.executorService.isShutdown()) {
                        this.executorService.execute(new Runnable() {
                            public void run() {
                                Poller.this.runDelayed(delay.delay, delay.timeUnit);
                            }
                        });
                        return;
                    }
                }
                this.started = false;
            }
        } catch (RejectedExecutionException e2) {
            HSLogger.e(TAG, "Rejected execution of run delayed : ", e2);
        }
    }
}
