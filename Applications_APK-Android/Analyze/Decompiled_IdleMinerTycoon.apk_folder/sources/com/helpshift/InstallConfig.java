package com.helpshift;

import android.support.annotation.NonNull;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.model.AppInfoModel;
import java.util.HashMap;
import java.util.Map;

public class InstallConfig {
    private final boolean enableDefaultFallbackLanguage;
    private final boolean enableInAppNotification;
    private final boolean enableInboxPolling;
    private final boolean enableLogging;
    private final Map<String, Object> extras;
    private final String fontPath;
    private final int largeNotificationIcon;
    private final int notificationIcon;
    private final int notificationSound;
    private final int screenOrientation;
    private final String supportNotificationChannelId;

    InstallConfig(boolean z, int i, int i2, int i3, boolean z2, boolean z3, String str, boolean z4, int i4, String str2, Map<String, Object> map) {
        this.enableInAppNotification = z;
        this.notificationIcon = i;
        this.largeNotificationIcon = i2;
        this.notificationSound = i3;
        this.enableDefaultFallbackLanguage = z2;
        this.enableInboxPolling = z3;
        this.fontPath = str;
        this.screenOrientation = i4;
        this.extras = map;
        this.enableLogging = z4;
        this.supportNotificationChannelId = str2;
    }

    public Map<String, Object> toMap() {
        HashMap hashMap = new HashMap();
        hashMap.put(SDKConfigurationDM.ENABLE_IN_APP_NOTIFICATION, Boolean.valueOf(this.enableInAppNotification));
        if (this.notificationIcon != 0) {
            hashMap.put("notificationIcon", Integer.valueOf(this.notificationIcon));
        }
        if (this.largeNotificationIcon != 0) {
            hashMap.put("largeNotificationIcon", Integer.valueOf(this.largeNotificationIcon));
        }
        if (this.notificationSound != 0) {
            hashMap.put("notificationSound", Integer.valueOf(this.notificationSound));
        }
        hashMap.put("enableDefaultFallbackLanguage", Boolean.valueOf(this.enableDefaultFallbackLanguage));
        hashMap.put("enableInboxPolling", Boolean.valueOf(this.enableInboxPolling));
        hashMap.put("enableLogging", Boolean.valueOf(this.enableLogging));
        hashMap.put("font", this.fontPath);
        hashMap.put(AppInfoModel.SCREEN_ORIENTATION_KEY, Integer.valueOf(this.screenOrientation));
        if (this.extras != null) {
            Object remove = this.extras.remove(SDKConfigurationDM.DISABLE_ERROR_LOGGING);
            if (remove != null) {
                this.extras.put("disableErrorReporting", remove);
            }
            for (String next : this.extras.keySet()) {
                if (this.extras.get(next) != null) {
                    hashMap.put(next, this.extras.get(next));
                }
            }
        }
        hashMap.put(SDKConfigurationDM.SDK_TYPE, "android");
        hashMap.put(SDKConfigurationDM.SUPPORT_NOTIFICATION_CHANNEL_ID, this.supportNotificationChannelId);
        return hashMap;
    }

    public static class Builder {
        private boolean enableDefaultFallbackLanguage = true;
        private boolean enableInAppNotification = true;
        private boolean enableInboxPolling = true;
        private boolean enableLogging = false;
        private Map<String, Object> extras;
        private String fontPath = null;
        private int largeNotificationIcon;
        private int notificationIcon;
        private int notificationSound;
        private int screenOrientation = -1;
        private String supportNotificationChannelId;

        public Builder setEnableInAppNotification(boolean z) {
            this.enableInAppNotification = z;
            return this;
        }

        public Builder setNotificationIcon(int i) {
            if (i != 0) {
                this.notificationIcon = i;
            }
            return this;
        }

        public Builder setLargeNotificationIcon(int i) {
            if (i != 0) {
                this.largeNotificationIcon = i;
            }
            return this;
        }

        public Builder setNotificationSound(int i) {
            if (i != 0) {
                this.notificationSound = i;
            }
            return this;
        }

        public Builder setEnableDefaultFallbackLanguage(boolean z) {
            this.enableDefaultFallbackLanguage = z;
            return this;
        }

        public Builder setEnableInboxPolling(boolean z) {
            this.enableInboxPolling = z;
            return this;
        }

        public Builder setFont(@NonNull String str) {
            this.fontPath = str;
            return this;
        }

        public Builder setScreenOrientation(int i) {
            this.screenOrientation = i;
            return this;
        }

        public Builder setExtras(Map<String, Object> map) {
            this.extras = map;
            return this;
        }

        public Builder setEnableLogging(boolean z) {
            this.enableLogging = z;
            return this;
        }

        public Builder setSupportNotificationChannelId(String str) {
            this.supportNotificationChannelId = str;
            return this;
        }

        public InstallConfig build() {
            return new InstallConfig(this.enableInAppNotification, this.notificationIcon, this.largeNotificationIcon, this.notificationSound, this.enableDefaultFallbackLanguage, this.enableInboxPolling, this.fontPath, this.enableLogging, this.screenOrientation, this.supportNotificationChannelId, this.extras);
        }
    }
}
