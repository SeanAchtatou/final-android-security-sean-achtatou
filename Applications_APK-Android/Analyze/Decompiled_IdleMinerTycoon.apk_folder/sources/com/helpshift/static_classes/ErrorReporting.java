package com.helpshift.static_classes;

public class ErrorReporting {
    private static boolean enableErrorReporting;

    public static void shouldEnable(boolean z) {
        enableErrorReporting = z;
    }

    public static boolean isEnabled() {
        return enableErrorReporting;
    }
}
