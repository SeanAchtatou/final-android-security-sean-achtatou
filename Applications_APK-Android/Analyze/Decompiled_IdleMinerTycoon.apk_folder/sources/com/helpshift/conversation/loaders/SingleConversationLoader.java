package com.helpshift.conversation.loaders;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.ListUtils;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.activeconversation.model.Conversation;
import java.util.Iterator;
import java.util.List;

public class SingleConversationLoader extends ConversationsLoader {
    private Long activeConversationId;
    private boolean isActiveConversationFullyFetched = false;
    private Platform platform;
    private UserDM userDM;

    public SingleConversationLoader(Platform platform2, UserDM userDM2, Long l, RemoteConversationLoader remoteConversationLoader, long j) {
        super(platform2, new SingleConversationDBLoader(platform2.getConversationDAO(), l), remoteConversationLoader, j);
        this.platform = platform2;
        this.userDM = userDM2;
        this.activeConversationId = l;
    }

    public boolean hasMoreMessages() {
        if (this.isActiveConversationFullyFetched) {
            return false;
        }
        if (this.conversationDBLoader.hasMoreMessages()) {
            return true;
        }
        List<Conversation> readConversationsWithoutMessages = this.platform.getConversationDAO().readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
        if (!ListUtils.isEmpty(readConversationsWithoutMessages)) {
            long j = 0;
            Iterator<Conversation> it = readConversationsWithoutMessages.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Conversation next = it.next();
                if (next.localId.equals(this.activeConversationId)) {
                    j = next.getEpochCreatedAtTime();
                    break;
                }
            }
            for (Conversation next2 : readConversationsWithoutMessages) {
                if (!next2.localId.equals(this.activeConversationId) && j > next2.getEpochCreatedAtTime()) {
                    this.isActiveConversationFullyFetched = true;
                    return false;
                }
            }
        }
        return this.remoteConversationLoader.hasMoreMessage();
    }
}
