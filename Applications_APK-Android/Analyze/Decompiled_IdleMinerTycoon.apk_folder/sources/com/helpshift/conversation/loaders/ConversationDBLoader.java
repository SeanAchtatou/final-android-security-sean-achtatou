package com.helpshift.conversation.loaders;

import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.conversation.ConversationUtil;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dao.ConversationDAO;
import java.util.ArrayList;
import java.util.List;

public abstract class ConversationDBLoader {
    protected ConversationDAO conversationDAO;
    private boolean hasMoreMessages = true;

    /* access modifiers changed from: package-private */
    public int compareEpochTime(long j, long j2) {
        if (j > j2) {
            return 1;
        }
        return j < j2 ? -1 : 0;
    }

    public abstract List<Conversation> fetchMessages(String str, String str2, long j);

    protected ConversationDBLoader(ConversationDAO conversationDAO2) {
        this.conversationDAO = conversationDAO2;
    }

    public boolean hasMoreMessages() {
        return this.hasMoreMessages;
    }

    public void setHasMoreMessages(boolean z) {
        this.hasMoreMessages = z;
    }

    /* access modifiers changed from: package-private */
    public List<MessageDM> filterMessages(String str, long j, List<MessageDM> list) {
        List<MessageDM> list2;
        if (ListUtils.isEmpty(list) || j < 1) {
            return new ArrayList();
        }
        ConversationUtil.sortMessagesBasedOnCreatedAt(list);
        if (StringUtils.isEmpty(str)) {
            list2 = list;
        } else {
            long convertToEpochTime = HSDateFormatSpec.convertToEpochTime(str);
            list2 = new ArrayList<>();
            for (MessageDM next : list) {
                if (convertToEpochTime <= next.getEpochCreatedAtTime()) {
                    break;
                }
                list2.add(next);
            }
            if (ListUtils.isEmpty(list2)) {
                return new ArrayList();
            }
        }
        int size = list2.size();
        return list2.subList(Math.max(0, (int) (((long) size) - j)), size);
    }
}
