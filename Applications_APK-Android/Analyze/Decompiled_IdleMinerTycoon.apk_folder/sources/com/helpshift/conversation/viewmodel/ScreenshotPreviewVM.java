package com.helpshift.conversation.viewmodel;

import com.helpshift.account.AuthenticationFailureDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.conversation.activeconversation.ScreenshotPreviewRenderer;

public class ScreenshotPreviewVM implements AuthenticationFailureDM.AuthenticationFailureObserver {
    private final Domain domain;
    /* access modifiers changed from: private */
    public ScreenshotPreviewRenderer renderer;

    public ScreenshotPreviewVM(Domain domain2, ScreenshotPreviewRenderer screenshotPreviewRenderer) {
        this.domain = domain2;
        this.renderer = screenshotPreviewRenderer;
        this.domain.getAuthenticationFailureDM().registerListener(this);
    }

    public void onAuthenticationFailure() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ScreenshotPreviewVM.this.renderer != null) {
                    ScreenshotPreviewVM.this.renderer.onAuthenticationFailure();
                }
            }
        });
    }

    public void unregisterRenderer() {
        this.renderer = null;
        this.domain.getAuthenticationFailureDM().unregisterListener(this);
    }
}
