package com.helpshift.conversation.viewmodel;

import com.helpshift.account.AuthenticationFailureDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.conversation.activeconversation.ConversationInfoRenderer;

public class ConversationInfoVM implements AuthenticationFailureDM.AuthenticationFailureObserver {
    private final Domain domain;
    /* access modifiers changed from: private */
    public ConversationInfoRenderer renderer;

    public ConversationInfoVM(Domain domain2, ConversationInfoRenderer conversationInfoRenderer) {
        this.domain = domain2;
        this.renderer = conversationInfoRenderer;
        this.domain.getAuthenticationFailureDM().registerListener(this);
    }

    public void onAuthenticationFailure() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationInfoVM.this.renderer != null) {
                    ConversationInfoVM.this.renderer.onAuthenticationFailure();
                }
            }
        });
    }

    public void unregisterRenderer() {
        this.renderer = null;
        this.domain.getAuthenticationFailureDM().unregisterListener(this);
    }
}
