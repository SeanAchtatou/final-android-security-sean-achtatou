package com.helpshift.conversation.viewmodel;

import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.ConversationalRenderer;
import com.helpshift.conversation.activeconversation.ViewableConversation;
import com.helpshift.conversation.activeconversation.message.AdminMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM;
import com.helpshift.conversation.activeconversation.message.ConversationFooterState;
import com.helpshift.conversation.activeconversation.message.FAQListMessageDM;
import com.helpshift.conversation.activeconversation.message.FAQListMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.MessageType;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageState;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.activeconversation.message.input.TextInput;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.util.HSLogger;
import com.helpshift.util.ValuePair;
import com.helpshift.widget.ConversationalWidgetGateway;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ConversationalVM extends ConversationVM implements ConversationController.StartNewConversationListener, ListPickerVMCallback {
    public static final String CREATE_NEW_PRE_ISSUE = "create_new_pre_issue";
    public static final int NO_NETWORK_ERROR = 1;
    public static final int POLL_FAILURE_ERROR = 2;
    private static final String TAG = "Helpshift_ConvsatnlVM";
    boolean awaitingUserInputForBotStep;
    /* access modifiers changed from: private */
    public MessageDM botMessageDM;
    boolean isInBetweenBotExecution;
    boolean isNetworkAvailable = true;
    boolean isShowingPollFailureError;
    boolean isUserReplyDraftClearedForBotChange;
    /* access modifiers changed from: private */
    public ListPickerVM listPickerVM;
    private boolean showConversationHistory;

    public ConversationalVM(Platform platform, Domain domain, ConversationController conversationController, ViewableConversation viewableConversation, ConversationalRenderer conversationalRenderer, boolean z, boolean z2) {
        super(platform, domain, conversationController, viewableConversation, conversationalRenderer, z2);
        this.showConversationHistory = z;
    }

    /* access modifiers changed from: protected */
    public void createWidgetGateway() {
        this.widgetGateway = new ConversationalWidgetGateway(this.sdkConfigurationDM, this.conversationController);
    }

    /* access modifiers changed from: protected */
    public void prefillReplyBox() {
        String userReplyText = this.conversationController.getUserReplyText();
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (StringUtils.isEmpty(userReplyText) && !this.conversationManager.containsAtleastOneUserMessage(activeConversation)) {
            userReplyText = this.conversationController.getConversationArchivalPrefillText();
            if (StringUtils.isEmpty(userReplyText)) {
                userReplyText = this.sdkConfigurationDM.getString(SDKConfigurationDM.CONVERSATION_PRE_FILL_TEXT);
            }
        }
        if (userReplyText != null) {
            this.replyFieldViewState.setReplyText(userReplyText);
        }
    }

    /* access modifiers changed from: protected */
    public List<MessageDM> buildUIMessages(Conversation conversation) {
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (!activeConversation.localId.equals(conversation.localId) || !this.conversationManager.shouldOpen(activeConversation)) {
            return new ArrayList(conversation.messageDMs);
        }
        return processMessagesForBots(conversation.messageDMs, false);
    }

    private List<MessageDM> processMessagesForBots(Collection<? extends MessageDM> collection, boolean z) {
        ValuePair<MessageDM, OptionInputMessageDM> valuePair;
        ArrayList arrayList = new ArrayList(collection);
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        this.isInBetweenBotExecution = this.conversationManager.evaluateBotExecutionState(arrayList, z);
        if (this.isInBetweenBotExecution) {
            MessageDM latestUnansweredBotMessage = this.conversationManager.getLatestUnansweredBotMessage(activeConversation);
            MessageDM messageDM = this.botMessageDM;
            if (messageDM == null || latestUnansweredBotMessage == null || !messageDM.serverId.equals(latestUnansweredBotMessage.serverId)) {
                if (latestUnansweredBotMessage == null || !(latestUnansweredBotMessage.messageType == MessageType.ADMIN_TEXT_WITH_OPTION_INPUT || latestUnansweredBotMessage.messageType == MessageType.FAQ_LIST_WITH_OPTION_INPUT)) {
                    this.botMessageDM = latestUnansweredBotMessage;
                } else {
                    int indexOf = arrayList.indexOf(latestUnansweredBotMessage);
                    if (indexOf != -1) {
                        if (latestUnansweredBotMessage.messageType == MessageType.ADMIN_TEXT_WITH_OPTION_INPUT) {
                            valuePair = splitOptionsBotMessage((AdminMessageWithOptionInputDM) latestUnansweredBotMessage);
                        } else {
                            valuePair = splitOptionsBotMessage((FAQListMessageWithOptionInputDM) latestUnansweredBotMessage);
                        }
                        incrementCreatedAt((MessageDM) valuePair.second, (MessageDM) valuePair.first);
                        arrayList.remove(indexOf);
                        arrayList.add(indexOf, valuePair.first);
                        if (((OptionInputMessageDM) valuePair.second).input.type == OptionInput.Type.PILL) {
                            arrayList.add(indexOf + 1, valuePair.second);
                        }
                        this.botMessageDM = (MessageDM) valuePair.second;
                    }
                }
                if (latestUnansweredBotMessage != null) {
                    removeOptionsMessageFromUI();
                    this.awaitingUserInputForBotStep = true;
                } else {
                    this.awaitingUserInputForBotStep = false;
                }
            } else {
                this.awaitingUserInputForBotStep = true;
                return arrayList;
            }
        } else {
            this.awaitingUserInputForBotStep = false;
        }
        return arrayList;
    }

    private void removeOptionsMessageFromUI() {
        if (this.messageListVM != null) {
            List<MessageDM> copyOfUIMessageDMs = this.messageListVM.copyOfUIMessageDMs();
            ArrayList arrayList = new ArrayList();
            if (!ListUtils.isEmpty(copyOfUIMessageDMs)) {
                for (MessageDM next : copyOfUIMessageDMs) {
                    if (next.messageType == MessageType.OPTION_INPUT) {
                        arrayList.add(next);
                    }
                }
                this.messageListVM.remove(arrayList);
            }
            hideListPicker(false);
        }
    }

    public void refreshVM() {
        super.refreshVM();
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (this.conversationManager.isSynced(activeConversation) || !this.conversationManager.containsAtleastOneUserMessage(activeConversation)) {
            if (!this.conversationManager.isSynced(activeConversation) && this.sdkConfigurationDM.shouldAutoFillPreissueFirstMessage()) {
                String string = this.sdkConfigurationDM.getString(SDKConfigurationDM.INITIAL_USER_MESSAGE_TO_AUTOSEND_IN_PREISSUE);
                if (!StringUtils.isEmpty(string)) {
                    HSLogger.d(TAG, "Auto-filing preissue with client set user message.");
                    this.conversationManager.updateIsAutoFilledPreissueFlag(activeConversation, true);
                    createPreIssue(string, false);
                    return;
                }
            }
            if (this.conversationManager.isSynced(activeConversation)) {
                evaluateBotMessages(activeConversation.messageDMs);
            }
            updateReplyBoxVisibility(this.replyBoxViewState.isVisible());
            return;
        }
        MessageDM messageDM = (MessageDM) activeConversation.messageDMs.get(activeConversation.messageDMs.size() - 1);
        if (messageDM instanceof UserMessageDM) {
            UserMessageDM userMessageDM = (UserMessageDM) messageDM;
            if (userMessageDM.getState() != UserMessageState.SENT) {
                this.replyBoxViewState.setVisible(false);
            }
            if (this.conversationController.isPreissueCreationInProgress(activeConversation.localId.longValue())) {
                userMessageDM.setState(UserMessageState.SENDING);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void createPreIssue(String str, boolean z) {
        HSLogger.d(TAG, "Trigger preissue creation. Retrying ? " + z);
        updateLastUserActivityTime();
        clearUserReplyDraft();
        disableUserInputOptions();
        String string = this.sdkConfigurationDM.getString(SDKConfigurationDM.CONVERSATION_GREETING_MESSAGE);
        if (!z) {
            this.conversationManager.addPreissueFirstUserMessage(this.viewableConversation.getActiveConversation(), str);
        }
        if (!this.isNetworkAvailable) {
            onCreateConversationFailure(new Exception("No internet connection."));
        } else {
            this.conversationController.createPreIssue(this.viewableConversation, string, str, this);
        }
    }

    public void onConversationInboxPollFailure() {
        HSLogger.e(TAG, "On conversation inbox poll failure");
        showFakeTypingIndicator(false);
        if (this.platform.isOnline() && !this.awaitingUserInputForBotStep) {
            if ((this.isInBetweenBotExecution || this.viewableConversation.getActiveConversation().isInPreIssueMode()) && this.viewableConversation.getActiveConversation().isIssueInProgress()) {
                this.domain.runOnUI(new F() {
                    public void f() {
                        if (ConversationalVM.this.getConversationalRenderer() != null) {
                            ConversationalVM.this.getConversationalRenderer().showNetworkErrorFooter(2);
                        }
                    }
                });
                this.isShowingPollFailureError = true;
            }
        }
    }

    public void onConversationInboxPollSuccess() {
        if (this.isShowingPollFailureError) {
            this.domain.runOnUI(new F() {
                public void f() {
                    if (ConversationalVM.this.getConversationalRenderer() != null) {
                        ConversationalVM.this.getConversationalRenderer().hideNetworkErrorFooter();
                    }
                }
            });
            this.isShowingPollFailureError = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void showFakeTypingIndicator(final boolean z) {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationalVM.this.renderer != null) {
                    boolean z = false;
                    if ((ConversationalVM.this.viewableConversation.getActiveConversation().isIssueInProgress() || ConversationalVM.this.viewableConversation.getActiveConversation().isInPreIssueMode() || ConversationalVM.this.isInBetweenBotExecution) && (ConversationalVM.this.viewableConversation.isAgentTyping() || z)) {
                        z = true;
                    }
                    ConversationalVM.this.updateTypingIndicatorStatus(z);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void sendTextMessage(final String str) {
        updateLastUserActivityTime();
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (!this.conversationManager.containsAtleastOneUserMessage(activeConversation)) {
            if (StringUtils.userVisibleCharacterCount(str) < this.sdkConfigurationDM.getMinimumConversationDescriptionLength()) {
                getConversationalRenderer().showReplyValidationFailedError(1);
                return;
            } else if (StringUtils.isEmpty(activeConversation.preConversationServerId)) {
                clearReply();
                createPreIssue(str, false);
                return;
            }
        }
        if (!this.isInBetweenBotExecution) {
            super.sendTextMessage(str);
            return;
        }
        MessageDM messageDM = this.botMessageDM;
        if (!(messageDM instanceof AdminMessageWithTextInputDM)) {
            super.sendTextMessage(str);
            return;
        }
        final AdminMessageWithTextInputDM adminMessageWithTextInputDM = (AdminMessageWithTextInputDM) messageDM;
        TextInput textInput = adminMessageWithTextInputDM.input;
        if (!adminMessageWithTextInputDM.input.validate(str)) {
            getConversationalRenderer().showReplyValidationFailedError(textInput.keyboard);
            return;
        }
        getConversationalRenderer().hideReplyValidationFailedError();
        disableUserInputOptions();
        clearReply();
        this.domain.runParallel(new F() {
            public void f() {
                try {
                    ConversationalVM.this.conversationManager.sendTextMessage(ConversationalVM.this.viewableConversation.getActiveConversation(), str, adminMessageWithTextInputDM, false);
                    ConversationalVM.this.showFakeTypingIndicator(true);
                } catch (RootAPIException e) {
                    ConversationalVM.this.showErrorForNoNetwork(e);
                    throw e;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showErrorForNoNetwork(RootAPIException rootAPIException) {
        if ((rootAPIException.exceptionType instanceof NetworkException) && !this.platform.isOnline()) {
            this.domain.runOnUI(new F() {
                public void f() {
                    if (ConversationalVM.this.getConversationalRenderer() != null) {
                        ConversationalVM.this.getConversationalRenderer().showNetworkErrorFooter(1);
                    }
                }
            });
        }
    }

    public void retryMessage(final MessageDM messageDM) {
        this.domain.runParallel(new F() {
            public void f() {
                Conversation activeConversation = ConversationalVM.this.viewableConversation.getActiveConversation();
                if ((messageDM instanceof UserMessageDM) && !ConversationalVM.this.conversationManager.isSynced(activeConversation)) {
                    HSLogger.d(ConversationalVM.TAG, "User retrying message to file preissue.");
                    ((UserMessageDM) messageDM).setState(UserMessageState.SENDING);
                    ConversationalVM.this.createPreIssue(messageDM.body, true);
                } else if (ConversationalVM.this.isNetworkAvailable) {
                    ConversationalVM.this.conversationManager.retryMessage(activeConversation, messageDM);
                    ConversationalVM.this.showFakeTypingIndicator(ConversationalVM.this.isInBetweenBotExecution);
                }
            }
        });
    }

    public void onSkipClick() {
        updateLastUserActivityTime();
        final MessageDM messageDM = this.botMessageDM;
        if (messageDM instanceof AdminMessageWithTextInputDM) {
            clearUserReplyDraft();
            disableUserInputOptions();
            this.domain.runParallel(new F() {
                public void f() {
                    AdminMessageWithTextInputDM adminMessageWithTextInputDM = (AdminMessageWithTextInputDM) messageDM;
                    try {
                        ConversationalVM.this.conversationManager.sendTextMessage(ConversationalVM.this.viewableConversation.getActiveConversation(), adminMessageWithTextInputDM.input.skipLabel, adminMessageWithTextInputDM, true);
                        ConversationalVM.this.showFakeTypingIndicator(true);
                    } catch (RootAPIException e) {
                        ConversationalVM.this.showErrorForNoNetwork(e);
                        throw e;
                    }
                }
            });
        }
        getConversationalRenderer().hideSkipButton();
    }

    public void addAll(Collection<? extends MessageDM> collection) {
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (this.conversationManager.hasBotSwitchedToAnotherBotInPollerResponse(collection)) {
            this.conversationManager.updateMessagesClickOnBotSwitch(activeConversation, false);
        }
        List<MessageDM> evaluateBotMessages = evaluateBotMessages(collection);
        if (!this.isInBetweenBotExecution) {
            this.isUserReplyDraftClearedForBotChange = false;
        } else if (!this.isUserReplyDraftClearedForBotChange && this.conversationManager.containsAtleastOneUserMessage(activeConversation)) {
            clearUserReplyDraft();
            this.isUserReplyDraftClearedForBotChange = true;
        }
        super.addAll(evaluateBotMessages);
    }

    public void update(MessageDM messageDM) {
        updateUserInputState();
        super.update(messageDM);
    }

    /* access modifiers changed from: package-private */
    public ConversationalRenderer getConversationalRenderer() {
        return (ConversationalRenderer) this.renderer;
    }

    public void handleOptionSelected(final OptionInputMessageDM optionInputMessageDM, final OptionInput.Option option, final boolean z) {
        if (this.messageListVM != null) {
            if (optionInputMessageDM.input.type == OptionInput.Type.PILL) {
                int indexOf = this.messageListVM.getUiMessageDMs().indexOf(optionInputMessageDM);
                this.messageListVM.remove(Collections.singletonList(optionInputMessageDM));
                this.renderer.updateMessages(indexOf - 1, 1);
            }
            updateLastUserActivityTime();
            if (optionInputMessageDM.input.type == OptionInput.Type.PILL) {
                disableUserInputOptions();
            } else if (optionInputMessageDM.input.type == OptionInput.Type.PICKER) {
                hideListPicker(true);
            }
            this.domain.runParallel(new F() {
                public void f() {
                    try {
                        ConversationalVM.this.conversationManager.sendOptionInputMessage(ConversationalVM.this.viewableConversation.getActiveConversation(), optionInputMessageDM, option, z);
                        if (ConversationalVM.this.viewableConversation.getActiveConversation().isIssueInProgress()) {
                            ConversationalVM.this.showFakeTypingIndicator(true);
                        }
                    } catch (RootAPIException e) {
                        ConversationalVM.this.showErrorForNoNetwork(e);
                        throw e;
                    }
                }
            });
        }
    }

    private void hideListPicker(final boolean z) {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationalVM.this.getConversationalRenderer() != null) {
                    ConversationalVM.this.getConversationalRenderer().hideListPicker(z);
                }
            }
        });
    }

    private void disableUserInputOptions() {
        if (this.renderer != null) {
            this.renderer.hideKeyboard();
        }
        this.attachImageButtonViewState.setVisible(false);
        disableUserTextInput();
    }

    private List<MessageDM> evaluateBotMessages(Collection<? extends MessageDM> collection) {
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        boolean z = this.isInBetweenBotExecution;
        List<MessageDM> processMessagesForBots = processMessagesForBots(collection, this.isInBetweenBotExecution);
        if (!activeConversation.isInPreIssueMode()) {
            if (z && !this.isInBetweenBotExecution) {
                this.conversationManager.updateMessagesClickOnBotSwitch(activeConversation, this.conversationManager.shouldEnableMessagesClick(activeConversation));
                removeOptionsMessageFromUI();
                updateReplyBoxVisibility(true);
                this.domain.runOnUI(new F() {
                    public void f() {
                        ConversationalVM.this.resetDefaultMenuItemsVisibility();
                        if (ConversationalVM.this.renderer != null) {
                            ConversationalVM.this.getConversationalRenderer().hideReplyValidationFailedError();
                        }
                    }
                });
            } else if (this.isInBetweenBotExecution && !z) {
                this.conversationManager.updateMessagesClickOnBotSwitch(activeConversation, false);
            }
        }
        updateUserInputState();
        return processMessagesForBots;
    }

    private void disableUserTextInput() {
        updateReplyBoxVisibility(false);
    }

    /* access modifiers changed from: private */
    public void updateUserInputState() {
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        IssueState issueState = activeConversation.state;
        boolean z = true;
        boolean z2 = false;
        if (issueState == IssueState.REJECTED) {
            disableUserInputOptions();
        } else if (!(issueState == IssueState.RESOLUTION_REQUESTED || issueState == IssueState.RESOLUTION_ACCEPTED || issueState == IssueState.COMPLETED_ISSUE_CREATED)) {
            if (this.isInBetweenBotExecution) {
                this.attachImageButtonViewState.setVisible(false);
                if (!this.awaitingUserInputForBotStep) {
                    disableUserInputOptions();
                    if (this.messageListVM != null) {
                        int size = activeConversation.messageDMs.size();
                        if (size > 0) {
                            MessageDM messageDM = (MessageDM) activeConversation.messageDMs.get(size - 1);
                            if (((messageDM instanceof UserResponseMessageForTextInputDM) || (messageDM instanceof UserResponseMessageForOptionInput)) && ((UserMessageDM) messageDM).getState() != UserMessageState.SENT) {
                                z = false;
                            }
                        }
                        z2 = z;
                    }
                }
            } else if (activeConversation.isInPreIssueMode() && !StringUtils.isEmpty(activeConversation.preConversationServerId)) {
                disableUserInputOptions();
            }
            z2 = true;
        }
        showFakeTypingIndicator(z2);
    }

    public void onNetworkAvailable() {
        this.domain.runOnUI(new F() {
            public void f() {
                ConversationalVM.this.isNetworkAvailable = true;
                if (ConversationalVM.this.renderer != null) {
                    ConversationalVM.this.updateUserInputState();
                    ConversationalVM.this.getConversationalRenderer().hideNetworkErrorFooter();
                }
            }
        });
    }

    public void onNetworkUnAvailable() {
        this.domain.runOnUI(new F() {
            public void f() {
                boolean z = false;
                ConversationalVM.this.isNetworkAvailable = false;
                if (ConversationalVM.this.renderer != null) {
                    Conversation activeConversation = ConversationalVM.this.viewableConversation.getActiveConversation();
                    ConversationalVM.this.showFakeTypingIndicator(false);
                    boolean z2 = activeConversation.isInPreIssueMode() && !StringUtils.isEmpty(activeConversation.preConversationServerId) && !ConversationalVM.this.awaitingUserInputForBotStep;
                    if (ConversationalVM.this.isInBetweenBotExecution && !ConversationalVM.this.awaitingUserInputForBotStep) {
                        z = true;
                    }
                    if (z2 || z) {
                        ConversationalVM.this.getConversationalRenderer().showNetworkErrorFooter(1);
                    }
                }
            }
        });
    }

    public void onIssueStatusChange(IssueState issueState) {
        if (!this.viewableConversation.getActiveConversation().isInPreIssueMode()) {
            super.onIssueStatusChange(issueState);
            if (this.isInBetweenBotExecution) {
                this.attachImageButtonViewState.setVisible(false);
                return;
            }
            return;
        }
        switch (issueState) {
            case RESOLUTION_ACCEPTED:
                this.awaitingUserInputForBotStep = false;
                showStartNewConversation(ConversationFooterState.START_NEW_CONVERSATION);
                updateUIOnNewMessageReceived();
                break;
            case REJECTED:
                this.awaitingUserInputForBotStep = false;
                removeOptionsMessageFromUI();
                handleConversationRejectedState();
                updateUIOnNewMessageReceived();
                break;
        }
        updateUserInputState();
    }

    public void onCreateConversationSuccess(long j) {
        handlePreIssueCreationSuccess();
    }

    public void handlePreIssueCreationSuccess() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationalVM.this.renderer != null) {
                    HSLogger.d(ConversationalVM.TAG, "Preissue creation success. Handing on UI.");
                    ConversationalVM.this.conversationController.getConversationInboxPoller().startChatPoller();
                    ConversationalVM.this.initMessagesList();
                    ConversationalVM.this.renderer.notifyRefreshList();
                    if (!ConversationalVM.this.isInBetweenBotExecution) {
                        ConversationalVM.this.showFakeTypingIndicator(true);
                    }
                    ConversationalVM.this.getConversationalRenderer().hideNetworkErrorFooter();
                }
            }
        });
    }

    public void onCreateConversationFailure(Exception exc) {
        HSLogger.e(TAG, "Error filing a pre-issue", exc);
        this.domain.runOnUI(new F() {
            public void f() {
                ConversationalVM.this.showFakeTypingIndicator(false);
                if (ConversationalVM.this.getConversationalRenderer() != null) {
                    MessageDM lastUIMessage = ConversationalVM.this.messageListVM.getLastUIMessage();
                    if (lastUIMessage instanceof UserMessageDM) {
                        ((UserMessageDM) lastUIMessage).setState(UserMessageState.UNSENT_RETRYABLE);
                    }
                    if (!ConversationalVM.this.isNetworkAvailable) {
                        ConversationalVM.this.getConversationalRenderer().showNetworkErrorFooter(1);
                    }
                }
            }
        });
    }

    public void handleAdminSuggestedQuestionRead(MessageDM messageDM, String str, String str2) {
        if (!StringUtils.isEmpty(str2)) {
            final Long l = messageDM.conversationLocalId;
            final String str3 = messageDM.serverId;
            final String str4 = str;
            final String str5 = str2;
            this.domain.runParallel(new F() {
                public void f() {
                    Conversation conversation;
                    Iterator<Conversation> it = ConversationalVM.this.viewableConversation.getAllConversations().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            conversation = null;
                            break;
                        }
                        conversation = it.next();
                        if (conversation.localId.equals(l)) {
                            break;
                        }
                    }
                    if (conversation != null) {
                        ConversationalVM.this.conversationManager.handleAdminSuggestedQuestionRead(conversation, str3, str4, str5);
                    }
                }
            });
        }
    }

    public void onNewConversationButtonClicked() {
        super.onNewConversationButtonClicked();
        if (this.showConversationHistory) {
            hideAllFooterWidgets();
            Conversation openConversationWithMessages = this.conversationController.getOpenConversationWithMessages();
            if (openConversationWithMessages == null) {
                openConversationWithMessages = this.conversationController.createLocalPreIssueConversation();
            }
            this.viewableConversation.onNewConversationStarted(openConversationWithMessages);
            refreshVM();
            renderMenuItems();
            initMessagesList();
            this.renderer.notifyRefreshList();
            return;
        }
        HashMap hashMap = new HashMap();
        hashMap.put(CREATE_NEW_PRE_ISSUE, Boolean.valueOf(this.showConversationHistory != this.sdkConfigurationDM.shouldShowConversationHistory()));
        this.renderer.openFreshConversationScreen(hashMap);
    }

    /* access modifiers changed from: package-private */
    public ValuePair<MessageDM, OptionInputMessageDM> splitOptionsBotMessage(FAQListMessageWithOptionInputDM fAQListMessageWithOptionInputDM) {
        if (fAQListMessageWithOptionInputDM == null) {
            return null;
        }
        FAQListMessageDM fAQListMessageDM = new FAQListMessageDM(fAQListMessageWithOptionInputDM);
        OptionInputMessageDM optionInputMessageDM = new OptionInputMessageDM(fAQListMessageWithOptionInputDM);
        optionInputMessageDM.setDependencies(this.domain, this.platform);
        fAQListMessageDM.setDependencies(this.domain, this.platform);
        return new ValuePair<>(fAQListMessageDM, optionInputMessageDM);
    }

    /* access modifiers changed from: package-private */
    public ValuePair<MessageDM, OptionInputMessageDM> splitOptionsBotMessage(AdminMessageWithOptionInputDM adminMessageWithOptionInputDM) {
        if (adminMessageWithOptionInputDM == null) {
            return null;
        }
        AdminMessageDM adminMessageDM = new AdminMessageDM(adminMessageWithOptionInputDM);
        OptionInputMessageDM optionInputMessageDM = new OptionInputMessageDM(adminMessageWithOptionInputDM);
        optionInputMessageDM.setDependencies(this.domain, this.platform);
        adminMessageDM.setDependencies(this.domain, this.platform);
        return new ValuePair<>(adminMessageDM, optionInputMessageDM);
    }

    private void incrementCreatedAt(MessageDM messageDM, MessageDM messageDM2) {
        String format = HSDateFormatSpec.STORAGE_TIME_FORMAT.format(new Date(messageDM2.getEpochCreatedAtTime() + 1));
        long convertToEpochTime = HSDateFormatSpec.convertToEpochTime(format);
        messageDM.setCreatedAt(format);
        messageDM.setEpochCreatedAtTime(convertToEpochTime);
    }

    public void onUIMessageListUpdated() {
        updateReplyBoxVisibility(this.replyBoxViewState.isVisible());
    }

    /* access modifiers changed from: protected */
    public void updateReplyBoxVisibility(boolean z) {
        if (this.botMessageDM == null || !this.isInBetweenBotExecution || this.isConversationRejected) {
            this.replyBoxViewState.setVisible(z);
        } else {
            this.domain.runOnUI(new F() {
                public void f() {
                    if (ConversationalVM.this.renderer != null && ConversationalVM.this.botMessageDM != null) {
                        switch (AnonymousClass18.$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType[ConversationalVM.this.botMessageDM.messageType.ordinal()]) {
                            case 1:
                                ConversationalVM.this.replyBoxViewState.setVisible(true);
                                ConversationalVM.this.getConversationalRenderer().showInput(((AdminMessageWithTextInputDM) ConversationalVM.this.botMessageDM).input);
                                return;
                            case 2:
                                ConversationalVM.this.replyBoxViewState.setVisible(false);
                                ConversationalVM.this.showOptions((OptionInputMessageDM) ConversationalVM.this.botMessageDM);
                                return;
                            default:
                                return;
                        }
                    }
                }
            });
        }
    }

    /* renamed from: com.helpshift.conversation.viewmodel.ConversationalVM$18  reason: invalid class name */
    static /* synthetic */ class AnonymousClass18 {
        static final /* synthetic */ int[] $SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType = new int[MessageType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(11:0|1|2|3|(2:5|6)|7|9|10|11|12|14) */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        static {
            /*
                com.helpshift.conversation.activeconversation.message.MessageType[] r0 = com.helpshift.conversation.activeconversation.message.MessageType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType = r0
                r0 = 1
                int[] r1 = com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.helpshift.conversation.activeconversation.message.MessageType r2 = com.helpshift.conversation.activeconversation.message.MessageType.ADMIN_TEXT_WITH_TEXT_INPUT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.helpshift.conversation.activeconversation.message.MessageType r3 = com.helpshift.conversation.activeconversation.message.MessageType.OPTION_INPUT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.helpshift.conversation.dto.IssueState[] r2 = com.helpshift.conversation.dto.IssueState.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.$SwitchMap$com$helpshift$conversation$dto$IssueState = r2
                int[] r2 = com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.$SwitchMap$com$helpshift$conversation$dto$IssueState     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.helpshift.conversation.dto.IssueState r3 = com.helpshift.conversation.dto.IssueState.RESOLUTION_ACCEPTED     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r0 = com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.$SwitchMap$com$helpshift$conversation$dto$IssueState     // Catch:{ NoSuchFieldError -> 0x003c }
                com.helpshift.conversation.dto.IssueState r2 = com.helpshift.conversation.dto.IssueState.REJECTED     // Catch:{ NoSuchFieldError -> 0x003c }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.helpshift.conversation.viewmodel.ConversationalVM.AnonymousClass18.<clinit>():void");
        }
    }

    /* access modifiers changed from: private */
    public void showOptions(OptionInputMessageDM optionInputMessageDM) {
        if (optionInputMessageDM.input.type == OptionInput.Type.PILL) {
            getConversationalRenderer().showInput(optionInputMessageDM.input);
        } else {
            showListPicker(optionInputMessageDM);
        }
    }

    public void updateListPickerOptions(List<OptionUIModel> list) {
        getConversationalRenderer().updateListPickerOptions(list);
    }

    public void handleOptionSelectedForPicker(OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z) {
        this.listPickerVM = null;
        handleOptionSelected(optionInputMessageDM, option, z);
    }

    public void showEmptyListPickerView() {
        getConversationalRenderer().showEmptyListPickerView();
    }

    private void showListPicker(final OptionInputMessageDM optionInputMessageDM) {
        this.listPickerVM = new ListPickerVM(this.domain, optionInputMessageDM, this);
        this.domain.runOnUI(new F() {
            public void f() {
                ConversationalVM.this.getConversationalRenderer().showListPicker(ConversationalVM.this.listPickerVM.getAllOptions(), optionInputMessageDM.input.inputLabel, optionInputMessageDM.input.required, optionInputMessageDM.input.skipLabel);
            }
        });
    }

    public void onListPickerSearchQueryChange(String str) {
        if (this.listPickerVM != null) {
            this.listPickerVM.onListPickerSearchQueryChange(str);
        }
    }

    public void handleOptionSelectedForPicker(OptionUIModel optionUIModel, boolean z) {
        if (this.listPickerVM != null) {
            this.listPickerVM.handleOptionSelectedForPicker(optionUIModel, z);
        }
    }

    public void showPickerClearButton() {
        getConversationalRenderer().showPickerClearButton();
    }

    public void hidePickerClearButton() {
        getConversationalRenderer().hidePickerClearButton();
    }
}
