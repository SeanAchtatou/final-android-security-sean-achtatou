package com.helpshift.conversation.viewmodel;

import com.helpshift.account.AuthenticationFailureDM;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.platform.Platform;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.ConversationManager;
import com.helpshift.conversation.activeconversation.ConversationRenderer;
import com.helpshift.conversation.activeconversation.UIConversation;
import com.helpshift.conversation.activeconversation.ViewableConversation;
import com.helpshift.conversation.activeconversation.message.AttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.ConversationFooterState;
import com.helpshift.conversation.activeconversation.message.HistoryLoadingState;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.SystemRedactedConversationMessageDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.util.HSLogger;
import com.helpshift.widget.ButtonViewState;
import com.helpshift.widget.ConversationFooterViewState;
import com.helpshift.widget.HistoryLoadingViewState;
import com.helpshift.widget.MutableButtonViewState;
import com.helpshift.widget.MutableConversationFooterViewState;
import com.helpshift.widget.MutableHistoryLoadingViewState;
import com.helpshift.widget.MutableReplyFieldViewState;
import com.helpshift.widget.MutableScrollJumperViewState;
import com.helpshift.widget.ReplyFieldViewState;
import com.helpshift.widget.ScrollJumperViewState;
import com.helpshift.widget.WidgetGateway;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class ConversationVM implements ConversationVMCallback, MessageListVMCallback, AuthenticationFailureDM.AuthenticationFailureObserver, Observer {
    private static final String TAG = "Helpshift_ConvVM";
    MutableButtonViewState attachImageButtonViewState;
    MutableButtonViewState confirmationBoxViewState;
    final ConversationController conversationController;
    MutableConversationFooterViewState conversationFooterViewState;
    ConversationManager conversationManager;
    Domain domain;
    MutableHistoryLoadingViewState historyLoadingViewState = new MutableHistoryLoadingViewState();
    protected boolean isConversationRejected;
    private boolean isScreenCurrentlyVisible;
    MessageListVM messageListVM;
    Platform platform;
    ConversationRenderer renderer;
    MutableButtonViewState replyBoxViewState;
    MutableButtonViewState replyButtonViewState;
    MutableReplyFieldViewState replyFieldViewState = this.widgetGateway.makeReplyFieldViewState();
    private boolean retainMessageBoxOnUI;
    MutableScrollJumperViewState scrollJumperViewState = this.widgetGateway.makeScrollJumperViewState();
    final SDKConfigurationDM sdkConfigurationDM;
    public final ViewableConversation viewableConversation;
    WidgetGateway widgetGateway;

    public void handlePreIssueCreationSuccess() {
    }

    public void onConversationInboxPollFailure() {
    }

    public void onConversationInboxPollSuccess() {
    }

    public void onSkipClick() {
    }

    public void onUIMessageListUpdated() {
    }

    public ConversationVM(Platform platform2, Domain domain2, ConversationController conversationController2, ViewableConversation viewableConversation2, ConversationRenderer conversationRenderer, boolean z) {
        this.domain = domain2;
        this.platform = platform2;
        this.conversationController = conversationController2;
        this.viewableConversation = viewableConversation2;
        this.sdkConfigurationDM = domain2.getSDKConfigurationDM();
        this.retainMessageBoxOnUI = z;
        this.conversationManager = conversationController2.conversationManager;
        this.sdkConfigurationDM.addObserver(this);
        domain2.getAuthenticationFailureDM().registerListener(this);
        createWidgetGateway();
        boolean shouldShowReplyBoxOnConversationRejected = shouldShowReplyBoxOnConversationRejected();
        Conversation activeConversation = viewableConversation2.getActiveConversation();
        this.conversationManager.setEnableMessageClickOnResolutionRejected(activeConversation, shouldShowReplyBoxOnConversationRejected);
        this.conversationFooterViewState = this.widgetGateway.makeConversationFooterViewState(activeConversation, shouldShowReplyBoxOnConversationRejected);
        this.attachImageButtonViewState = this.widgetGateway.makeAttachImageButtonViewState(viewableConversation2.getActiveConversation());
        this.replyButtonViewState = new MutableButtonViewState();
        this.replyBoxViewState = this.widgetGateway.makeReplyBoxViewState(activeConversation, shouldShowReplyBoxOnConversationRejected);
        this.confirmationBoxViewState = this.widgetGateway.makeConfirmationBoxViewState(activeConversation);
        if (this.replyBoxViewState.isVisible()) {
            conversationController2.setConversationViewState(2);
        } else {
            conversationController2.setConversationViewState(-1);
        }
        if (!shouldShowReplyBoxOnConversationRejected && activeConversation.state == IssueState.RESOLUTION_REJECTED) {
            this.conversationManager.handleConversationEnded(activeConversation);
        }
        viewableConversation2.setConversationVMCallback(this);
        this.renderer = conversationRenderer;
        initMessagesList();
    }

    /* access modifiers changed from: protected */
    public void createWidgetGateway() {
        this.widgetGateway = new WidgetGateway(this.sdkConfigurationDM, this.conversationController);
    }

    /* access modifiers changed from: protected */
    public void initMessagesList() {
        if (this.messageListVM != null) {
            this.messageListVM.unregisterMessageListVMCallback();
        }
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        this.viewableConversation.initializeConversationsForUI();
        this.conversationManager.initializeIssueStatusForUI(activeConversation);
        boolean hasMoreMessages = this.viewableConversation.hasMoreMessages();
        this.messageListVM = new MessageListVM(this.platform, this.domain);
        List<UIConversation> uIConversations = this.viewableConversation.getUIConversations();
        ArrayList arrayList = new ArrayList();
        for (Conversation uIMessages : this.viewableConversation.getAllConversations()) {
            arrayList.addAll(getUIMessages(uIMessages));
        }
        this.messageListVM.initializeMessageList(uIConversations, arrayList, hasMoreMessages, this);
        this.renderer.initializeMessages(this.messageListVM.getUiMessageDMs());
        this.viewableConversation.registerMessagesObserver(this);
        this.isConversationRejected = activeConversation.state == IssueState.REJECTED;
        prefillReplyBox();
    }

    /* access modifiers changed from: protected */
    public void prefillReplyBox() {
        this.renderer.setReply(this.conversationController.getUserReplyText());
    }

    private List<MessageDM> getUIMessages(Conversation conversation) {
        ArrayList arrayList = new ArrayList();
        if (conversation.isRedacted) {
            arrayList.add(generateSystemRedactedConversationMessageDM(conversation));
        } else {
            arrayList.addAll(buildUIMessages(conversation));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<MessageDM> buildUIMessages(Conversation conversation) {
        return new ArrayList(conversation.messageDMs);
    }

    private SystemRedactedConversationMessageDM generateSystemRedactedConversationMessageDM(Conversation conversation) {
        SystemRedactedConversationMessageDM systemRedactedConversationMessageDM = new SystemRedactedConversationMessageDM(conversation.getCreatedAt(), conversation.getEpochCreatedAtTime(), 1);
        systemRedactedConversationMessageDM.setDependencies(this.domain, this.platform);
        systemRedactedConversationMessageDM.conversationLocalId = conversation.localId;
        return systemRedactedConversationMessageDM;
    }

    public void onResume() {
        refreshVM();
        renderMenuItems();
        setScreenVisibility(true);
        setUserCanReadMessages(true);
        markMessagesAsSeenOnEntry();
        clearNotifications();
    }

    public void onPause() {
        setScreenVisibility(false);
        setUserCanReadMessages(false);
        markMessagesAsSeenOnExit();
        clearNotifications();
        resetIncrementMessageCountFlag();
        saveReplyText(this.renderer.getReply());
    }

    public boolean shouldShowUnreadMessagesIndicator() {
        return this.scrollJumperViewState.shouldShowUnreadMessagesIndicator();
    }

    public void refreshVM() {
        boolean shouldShowReplyBoxOnConversationRejected = shouldShowReplyBoxOnConversationRejected();
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        this.widgetGateway.updateReplyBoxWidget(this.replyBoxViewState, activeConversation, shouldShowReplyBoxOnConversationRejected);
        this.widgetGateway.updateConfirmationBoxViewState(this.confirmationBoxViewState, activeConversation);
        this.widgetGateway.updateConversationFooterViewState(this.conversationFooterViewState, activeConversation, shouldShowReplyBoxOnConversationRejected);
        if (this.replyBoxViewState.isVisible()) {
            this.conversationController.setConversationViewState(2);
        } else {
            this.conversationController.setConversationViewState(-1);
        }
        this.viewableConversation.registerMessagesObserver(this);
        this.viewableConversation.setConversationVMCallback(this);
        if (activeConversation.serverId != null || activeConversation.preConversationServerId != null) {
            this.conversationController.getConversationInboxPoller().startChatPoller();
        }
    }

    public void unregisterRenderer() {
        this.viewableConversation.unregisterConversationVMCallback();
        if (this.messageListVM != null) {
            this.messageListVM.unregisterMessageListVMCallback();
            this.messageListVM = null;
        }
        this.renderer = null;
        this.sdkConfigurationDM.deleteObserver(this);
        this.domain.getAuthenticationFailureDM().unregisterListener(this);
    }

    private void setUserCanReadMessages(boolean z) {
        this.conversationController.setUserCanReadMessages(z);
        onAgentTypingUpdate(this.viewableConversation.isAgentTyping());
    }

    public void saveReplyText(String str) {
        this.replyFieldViewState.setReplyText(str);
        this.conversationController.saveUserReplyText(str);
    }

    public void clearUserReplyDraft() {
        this.conversationController.saveUserReplyText("");
        this.replyFieldViewState.clearReplyText();
    }

    public void sendTextMessage() {
        String reply = this.renderer.getReply();
        if (!StringUtils.isEmpty(reply)) {
            this.conversationController.setPersistMessageBox(true);
            sendTextMessage(reply.trim());
        }
    }

    /* access modifiers changed from: package-private */
    public void clearReply() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationVM.this.renderer != null) {
                    ConversationVM.this.renderer.setReply("");
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void sendTextMessage(final String str) {
        clearReply();
        this.domain.runParallel(new F() {
            public void f() {
                ConversationVM.this.conversationManager.sendTextMessage(ConversationVM.this.viewableConversation.getActiveConversation(), str);
            }
        });
    }

    public void retryMessage(final MessageDM messageDM) {
        this.domain.runParallel(new F() {
            public void f() {
                ConversationVM.this.conversationManager.retryMessage(ConversationVM.this.viewableConversation.getActiveConversation(), messageDM);
            }
        });
    }

    public void handleScreenshotMessageClick(ScreenshotMessageDM screenshotMessageDM) {
        this.viewableConversation.onScreenshotMessageClicked(screenshotMessageDM);
    }

    public void handleAppReviewRequestClick(RequestAppReviewMessageDM requestAppReviewMessageDM) {
        String trim = this.sdkConfigurationDM.getString(SDKConfigurationDM.REVIEW_URL).trim();
        if (!StringUtils.isEmpty(trim)) {
            this.sdkConfigurationDM.setAppReviewed(true);
            if (this.renderer != null) {
                this.renderer.openAppReviewStore(trim);
            }
        }
        this.conversationManager.handleAppReviewRequestClick(this.viewableConversation.getActiveConversation(), requestAppReviewMessageDM);
    }

    public void launchScreenshotAttachment(String str, String str2) {
        this.renderer.launchScreenshotAttachment(str, str2);
    }

    public boolean isMessageBoxVisible() {
        return this.replyBoxViewState.isVisible();
    }

    public boolean isVisibleOnUI() {
        return this.isScreenCurrentlyVisible;
    }

    private void setScreenVisibility(boolean z) {
        this.isScreenCurrentlyVisible = z;
    }

    public void onAgentTypingUpdate(final boolean z) {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationVM.this.renderer != null) {
                    boolean z = false;
                    if (ConversationVM.this.viewableConversation.getActiveConversation().isIssueInProgress()) {
                        z = z;
                    }
                    ConversationVM.this.updateTypingIndicatorStatus(z);
                }
            }
        });
    }

    private List<MessageDM> getUIMessagesForHistory(Conversation conversation) {
        ArrayList arrayList = new ArrayList();
        if (conversation.isRedacted) {
            arrayList.add(generateSystemRedactedConversationMessageDM(conversation));
        } else {
            arrayList.addAll(conversation.messageDMs);
        }
        return arrayList;
    }

    public void prependConversations(List<Conversation> list, boolean z) {
        if (!ListUtils.isEmpty(list)) {
            List<UIConversation> uIConversations = this.viewableConversation.getUIConversations();
            ArrayList arrayList = new ArrayList();
            for (Conversation uIMessagesForHistory : list) {
                arrayList.addAll(getUIMessagesForHistory(uIMessagesForHistory));
            }
            if (this.messageListVM != null) {
                this.messageListVM.updateUIConversationOrder(uIConversations);
                this.messageListVM.prependMessages(arrayList, z);
            }
        } else if (!z) {
            this.messageListVM.prependMessages(new ArrayList(), false);
        }
    }

    public void onHistoryLoadingSuccess() {
        this.historyLoadingViewState.setState(HistoryLoadingState.NONE);
    }

    public void onHistoryLoadingError() {
        this.historyLoadingViewState.setState(HistoryLoadingState.ERROR);
    }

    public void onHistoryLoadingStarted() {
        this.historyLoadingViewState.setState(HistoryLoadingState.LOADING);
    }

    public void sendScreenShot(final ImagePickerFile imagePickerFile, final String str) {
        this.domain.runParallel(new F() {
            public void f() {
                ConversationVM.this.conversationManager.sendScreenshot(ConversationVM.this.viewableConversation.getActiveConversation(), imagePickerFile, str);
            }
        });
    }

    public void handleAdminAttachmentMessageClick(AttachmentMessageDM attachmentMessageDM) {
        this.viewableConversation.onAdminAttachmentMessageClicked(attachmentMessageDM);
    }

    public void launchAttachment(String str, String str2) {
        this.renderer.launchAttachment(str, str2);
    }

    public void markConversationResolutionStatus(boolean z) {
        HSLogger.d(TAG, "Sending resolution event : Accepted? " + z);
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (activeConversation.state == IssueState.RESOLUTION_REQUESTED) {
            this.conversationManager.markConversationResolutionStatus(activeConversation, z);
        }
    }

    private void markMessagesAsSeenOnEntry() {
        final Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (this.conversationManager.isSynced(activeConversation)) {
            this.domain.runParallel(new F() {
                public void f() {
                    if (activeConversation != null) {
                        ConversationVM.this.conversationManager.markMessagesAsSeen(activeConversation);
                    }
                }
            });
        }
    }

    private void markMessagesAsSeenOnExit() {
        final ArrayList arrayList = new ArrayList(this.viewableConversation.getAllConversations());
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (!this.conversationManager.isSynced(activeConversation)) {
            arrayList.remove(activeConversation);
        }
        this.domain.runParallel(new F() {
            public void f() {
                for (Conversation markMessagesAsSeen : arrayList) {
                    ConversationVM.this.conversationManager.markMessagesAsSeen(markMessagesAsSeen);
                }
            }
        });
    }

    public void renderMenuItems() {
        this.replyButtonViewState.setEnabled(!StringUtils.isEmpty(this.replyFieldViewState.getReplyText()));
        updateAttachmentButtonViewState();
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onIssueStatusChange(com.helpshift.conversation.dto.IssueState r7) {
        /*
            r6 = this;
            java.lang.String r0 = "Helpshift_ConvVM"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Changing conversation status to: "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            com.helpshift.util.HSLogger.d(r0, r1)
            com.helpshift.conversation.activeconversation.ViewableConversation r0 = r6.viewableConversation
            com.helpshift.conversation.activeconversation.model.Conversation r0 = r0.getActiveConversation()
            boolean r1 = com.helpshift.conversation.ConversationUtil.isInProgressState(r7)
            r2 = 2
            r3 = 1
            r4 = 0
            r5 = -1
            if (r1 == 0) goto L_0x002e
            r6.showMessageBox()
            r7 = 0
        L_0x002a:
            r0 = 0
            r5 = 2
            goto L_0x009d
        L_0x002e:
            com.helpshift.conversation.dto.IssueState r1 = com.helpshift.conversation.dto.IssueState.RESOLUTION_REQUESTED
            if (r7 != r1) goto L_0x004c
            com.helpshift.configuration.domainmodel.SDKConfigurationDM r7 = r6.sdkConfigurationDM
            boolean r7 = r7.shouldShowConversationResolutionQuestion()
            if (r7 == 0) goto L_0x003d
            r6.showConfirmationBox()
        L_0x003d:
            com.helpshift.widget.MutableScrollJumperViewState r7 = r6.scrollJumperViewState
            boolean r7 = r7.isVisible()
            if (r7 != 0) goto L_0x0048
            r6.notifyRendererForScrollToBottom()
        L_0x0048:
            r7 = 1
            r0 = 0
            r3 = 0
            goto L_0x009d
        L_0x004c:
            com.helpshift.conversation.dto.IssueState r1 = com.helpshift.conversation.dto.IssueState.REJECTED
            if (r7 != r1) goto L_0x0056
            r6.handleConversationRejectedState()
            r7 = 1
            r0 = 1
            goto L_0x009d
        L_0x0056:
            com.helpshift.conversation.dto.IssueState r1 = com.helpshift.conversation.dto.IssueState.RESOLUTION_ACCEPTED
            if (r7 != r1) goto L_0x0075
            com.helpshift.conversation.domainmodel.ConversationController r7 = r6.conversationController
            java.lang.String r1 = ""
            r7.saveUserReplyText(r1)
            com.helpshift.conversation.activeconversation.ConversationManager r7 = r6.conversationManager
            boolean r7 = r7.shouldShowCSATInFooter(r0)
            if (r7 == 0) goto L_0x006f
            com.helpshift.conversation.activeconversation.message.ConversationFooterState r7 = com.helpshift.conversation.activeconversation.message.ConversationFooterState.CSAT_RATING
            r6.showStartNewConversation(r7)
            goto L_0x009b
        L_0x006f:
            com.helpshift.conversation.activeconversation.message.ConversationFooterState r7 = com.helpshift.conversation.activeconversation.message.ConversationFooterState.START_NEW_CONVERSATION
            r6.showStartNewConversation(r7)
            goto L_0x009b
        L_0x0075:
            com.helpshift.conversation.dto.IssueState r1 = com.helpshift.conversation.dto.IssueState.RESOLUTION_REJECTED
            if (r7 != r1) goto L_0x0088
            com.helpshift.conversation.domainmodel.ConversationController r7 = r6.conversationController
            r7.setPersistMessageBox(r4)
            r6.showMessageBox()
            com.helpshift.conversation.activeconversation.ConversationManager r7 = r6.conversationManager
            r7.setEnableMessageClickOnResolutionRejected(r0, r3)
            r7 = 1
            goto L_0x002a
        L_0x0088:
            com.helpshift.conversation.dto.IssueState r0 = com.helpshift.conversation.dto.IssueState.ARCHIVED
            if (r7 != r0) goto L_0x0092
            com.helpshift.conversation.activeconversation.message.ConversationFooterState r7 = com.helpshift.conversation.activeconversation.message.ConversationFooterState.ARCHIVAL_MESSAGE
            r6.showStartNewConversation(r7)
            goto L_0x009b
        L_0x0092:
            com.helpshift.conversation.dto.IssueState r0 = com.helpshift.conversation.dto.IssueState.AUTHOR_MISMATCH
            if (r7 != r0) goto L_0x009b
            com.helpshift.conversation.activeconversation.message.ConversationFooterState r7 = com.helpshift.conversation.activeconversation.message.ConversationFooterState.AUTHOR_MISMATCH
            r6.showStartNewConversation(r7)
        L_0x009b:
            r7 = 1
            r0 = 0
        L_0x009d:
            if (r3 == 0) goto L_0x00a2
            r6.updateUIOnNewMessageReceived()
        L_0x00a2:
            if (r7 == 0) goto L_0x00a7
            r6.onAgentTypingUpdate(r4)
        L_0x00a7:
            com.helpshift.conversation.domainmodel.ConversationController r7 = r6.conversationController
            r7.setConversationViewState(r5)
            r6.isConversationRejected = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.conversation.viewmodel.ConversationVM.onIssueStatusChange(com.helpshift.conversation.dto.IssueState):void");
    }

    /* access modifiers changed from: package-private */
    public void handleConversationRejectedState() {
        ConversationFooterState conversationFooterState;
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        this.conversationController.saveUserReplyText("");
        if (activeConversation.isRedacted) {
            conversationFooterState = ConversationFooterState.REDACTED_STATE;
        } else {
            conversationFooterState = ConversationFooterState.REJECTED_MESSAGE;
        }
        showStartNewConversation(conversationFooterState);
        this.isConversationRejected = true;
    }

    /* access modifiers changed from: protected */
    public void resetDefaultMenuItemsVisibility() {
        this.attachImageButtonViewState.setVisible(this.widgetGateway.getDefaultVisibilityForAttachImageButton(this.viewableConversation.getActiveConversation()));
    }

    public void onImageAttachmentButtonClick() {
        this.conversationController.setPersistMessageBox(true);
    }

    private boolean shouldShowReplyBoxOnConversationRejected() {
        return !StringUtils.isEmpty(this.conversationController.getUserReplyText()) || this.conversationController.shouldPersistMessageBox() || this.retainMessageBoxOnUI;
    }

    public void onCSATSurveySubmitted(int i, String str) {
        if (this.renderer != null) {
            this.renderer.showCSATSubmittedView();
        }
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        if (!activeConversation.isIssueInProgress()) {
            showStartNewConversation(ConversationFooterState.START_NEW_CONVERSATION);
        }
        HSLogger.d(TAG, "Sending CSAT rating : " + i + ", feedback: " + str);
        this.conversationManager.sendCSATSurvey(activeConversation, i, str);
    }

    public void setConversationViewState(int i) {
        this.conversationController.setConversationViewState(i);
    }

    public void onNewConversationButtonClicked() {
        stopLiveUpdates();
        this.conversationManager.setStartNewConversationButtonClicked(this.viewableConversation.getActiveConversation(), true, true);
    }

    public void forceClickOnNewConversationButton() {
        if (this.viewableConversation.getActiveConversation().isStartNewConversationClicked) {
            onNewConversationButtonClicked();
        }
    }

    private void clearNotifications() {
        Conversation activeConversation = this.viewableConversation.getActiveConversation();
        this.conversationController.clearNotification(activeConversation);
        this.conversationController.resetPushNotificationCount(activeConversation);
    }

    private void resetIncrementMessageCountFlag() {
        this.conversationManager.setShouldIncrementMessageCount(this.viewableConversation.getActiveConversation(), false, true);
    }

    public void pushAnalyticsEvent(AnalyticsEventType analyticsEventType, Map<String, Object> map) {
        this.domain.getAnalyticsEventDM().pushEvent(analyticsEventType, map);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002e A[EDGE_INSN: B:25:0x002e->B:13:0x002e ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAdminMessageLinkClicked(java.lang.String r6, com.helpshift.conversation.activeconversation.message.MessageDM r7) {
        /*
            r5 = this;
            r0 = 0
            java.net.URI r1 = java.net.URI.create(r6)     // Catch:{ Exception -> 0x000c }
            if (r1 == 0) goto L_0x000c
            java.lang.String r1 = r1.getScheme()     // Catch:{ Exception -> 0x000c }
            goto L_0x000d
        L_0x000c:
            r1 = r0
        L_0x000d:
            java.lang.Long r7 = r7.conversationLocalId
            com.helpshift.conversation.activeconversation.ViewableConversation r2 = r5.viewableConversation
            java.util.List r2 = r2.getAllConversations()
            java.util.Iterator r2 = r2.iterator()
        L_0x0019:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x002e
            java.lang.Object r3 = r2.next()
            com.helpshift.conversation.activeconversation.model.Conversation r3 = (com.helpshift.conversation.activeconversation.model.Conversation) r3
            java.lang.Long r4 = r3.localId
            boolean r4 = r4.equals(r7)
            if (r4 == 0) goto L_0x0019
            r0 = r3
        L_0x002e:
            boolean r7 = com.helpshift.common.StringUtils.isEmpty(r1)
            if (r7 != 0) goto L_0x0068
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            if (r0 == 0) goto L_0x0059
            java.lang.String r2 = r0.preConversationServerId
            boolean r2 = com.helpshift.common.StringUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x004a
            java.lang.String r2 = "preissue_id"
            java.lang.String r3 = r0.preConversationServerId
            r7.put(r2, r3)
        L_0x004a:
            java.lang.String r2 = r0.serverId
            boolean r2 = com.helpshift.common.StringUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0059
            java.lang.String r2 = "issue_id"
            java.lang.String r0 = r0.serverId
            r7.put(r2, r0)
        L_0x0059:
            java.lang.String r0 = "p"
            r7.put(r0, r1)
            java.lang.String r0 = "u"
            r7.put(r0, r6)
            com.helpshift.analytics.AnalyticsEventType r6 = com.helpshift.analytics.AnalyticsEventType.ADMIN_MESSAGE_DEEPLINK_CLICKED
            r5.pushAnalyticsEvent(r6, r7)
        L_0x0068:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.conversation.viewmodel.ConversationVM.onAdminMessageLinkClicked(java.lang.String, com.helpshift.conversation.activeconversation.message.MessageDM):void");
    }

    public void startLiveUpdates() {
        this.viewableConversation.startLiveUpdates();
    }

    public void stopLiveUpdates() {
        this.viewableConversation.stopLiveUpdates();
    }

    public void update(final Observable observable, Object obj) {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationVM.this.renderer != null && (observable instanceof SDKConfigurationDM)) {
                    ConversationVM.this.refreshAll();
                }
            }
        });
    }

    public void add(MessageDM messageDM) {
        addAll(Collections.singletonList(messageDM));
    }

    public void addAll(Collection<? extends MessageDM> collection) {
        HSLogger.d(TAG, "addAll called : " + collection.size());
        if (this.messageListVM != null) {
            this.messageListVM.addMessages(collection);
        }
    }

    public void update(MessageDM messageDM) {
        HSLogger.d(TAG, "update called : " + messageDM);
        if (this.messageListVM != null) {
            this.messageListVM.insertOrUpdateMessage(messageDM);
        }
    }

    public void updateLastUserActivityTime() {
        this.conversationManager.updateLastUserActivityTime(this.viewableConversation.getActiveConversation(), System.currentTimeMillis());
    }

    public void appendMessages(int i, int i2) {
        if (this.renderer != null) {
            this.renderer.appendMessages(i, i2);
        }
    }

    public void newAdminMessagesAdded() {
        updateUIOnNewMessageReceived();
    }

    public void newUserMessagesAdded() {
        notifyRendererForScrollToBottom();
    }

    private void showUnreadMessagesIndicator() {
        this.scrollJumperViewState.setShouldShowUnreadMessagesIndicator(true);
    }

    /* access modifiers changed from: protected */
    public void updateUIOnNewMessageReceived() {
        if (this.scrollJumperViewState.isVisible()) {
            showUnreadMessagesIndicator();
        } else {
            notifyRendererForScrollToBottom();
        }
    }

    public void updateMessages(int i, int i2) {
        if (this.renderer != null) {
            this.renderer.updateMessages(i, i2);
        }
    }

    public void refreshAll() {
        if (this.renderer != null) {
            this.renderer.notifyRefreshList();
        }
    }

    public void onAuthenticationFailure() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationVM.this.renderer != null) {
                    ConversationVM.this.renderer.onAuthenticationFailure();
                }
            }
        });
    }

    public void onScrollJumperViewClicked() {
        notifyRendererForScrollToBottom();
    }

    public void onScrolledToBottom() {
        this.scrollJumperViewState.setVisible(false);
        this.scrollJumperViewState.setShouldShowUnreadMessagesIndicator(false);
    }

    public void onScrolling() {
        this.scrollJumperViewState.setVisible(true);
    }

    public void onScrolledToTop() {
        if (this.historyLoadingViewState.getState() == HistoryLoadingState.NONE) {
            loadHistoryMessagesInternal();
        }
    }

    public void retryHistoryLoadingMessages() {
        if (this.historyLoadingViewState.getState() == HistoryLoadingState.ERROR) {
            loadHistoryMessagesInternal();
        }
    }

    private void loadHistoryMessagesInternal() {
        if (this.historyLoadingViewState.getState() != HistoryLoadingState.LOADING) {
            this.domain.runParallel(new F() {
                public void f() {
                    ConversationVM.this.viewableConversation.loadMoreMessages();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void updateTypingIndicatorStatus(boolean z) {
        boolean z2;
        if (z) {
            this.renderer.showAgentTypingIndicator();
            z2 = !this.scrollJumperViewState.isVisible();
        } else {
            this.renderer.hideAgentTypingIndicator();
            z2 = false;
        }
        if (z2) {
            notifyRendererForScrollToBottom();
        }
    }

    private void notifyRendererForScrollToBottom() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (ConversationVM.this.renderer != null) {
                    ConversationVM.this.renderer.scrollToBottom();
                }
            }
        });
    }

    public void updateUnreadMessageCountIndicator(boolean z) {
        this.scrollJumperViewState.setShouldShowUnreadMessagesIndicator(z);
    }

    private void showMessageBox() {
        this.replyBoxViewState.setVisible(true);
        updateAttachmentButtonViewState();
        this.confirmationBoxViewState.setVisible(false);
        this.conversationFooterViewState.setState(ConversationFooterState.NONE);
    }

    private void showConfirmationBox() {
        this.replyBoxViewState.setVisible(false);
        updateAttachmentButtonViewState();
        this.confirmationBoxViewState.setVisible(true);
        this.conversationFooterViewState.setState(ConversationFooterState.NONE);
    }

    private void updateAttachmentButtonViewState() {
        resetDefaultMenuItemsVisibility();
        if (this.attachImageButtonViewState.isVisible()) {
            this.attachImageButtonViewState.setVisible(!this.isConversationRejected && this.replyBoxViewState.isVisible());
        }
    }

    /* access modifiers changed from: protected */
    public void hideAllFooterWidgets() {
        this.replyBoxViewState.setVisible(false);
        updateAttachmentButtonViewState();
        this.confirmationBoxViewState.setVisible(false);
        this.conversationFooterViewState.setState(ConversationFooterState.NONE);
    }

    /* access modifiers changed from: protected */
    public void showStartNewConversation(ConversationFooterState conversationFooterState) {
        this.replyBoxViewState.setVisible(false);
        updateAttachmentButtonViewState();
        this.confirmationBoxViewState.setVisible(false);
        this.conversationFooterViewState.setState(conversationFooterState);
    }

    public void toggleReplySendButton(boolean z) {
        this.replyButtonViewState.setEnabled(z);
    }

    public ReplyFieldViewState getReplyFieldViewState() {
        return this.replyFieldViewState;
    }

    public HistoryLoadingViewState getHistoryLoadingViewState() {
        return this.historyLoadingViewState;
    }

    public ScrollJumperViewState getScrollJumperViewState() {
        return this.scrollJumperViewState;
    }

    public ConversationFooterViewState getConversationFooterViewState() {
        return this.conversationFooterViewState;
    }

    public ButtonViewState getAttachImageButtonViewState() {
        return this.attachImageButtonViewState;
    }

    public ButtonViewState getReplyBoxViewState() {
        return this.replyBoxViewState;
    }

    public ButtonViewState getConfirmationBoxViewState() {
        return this.confirmationBoxViewState;
    }

    public ButtonViewState getReplyButtonViewState() {
        return this.replyButtonViewState;
    }
}
