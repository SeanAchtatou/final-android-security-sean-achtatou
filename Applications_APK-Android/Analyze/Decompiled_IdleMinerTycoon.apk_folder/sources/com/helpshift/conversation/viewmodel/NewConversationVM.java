package com.helpshift.conversation.viewmodel;

import com.helpshift.account.AuthenticationFailureDM;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.util.HSLogger;
import com.helpshift.util.HSPattern;
import com.helpshift.widget.ButtonViewState;
import com.helpshift.widget.ImageAttachmentViewState;
import com.helpshift.widget.MutableButtonViewState;
import com.helpshift.widget.MutableImageAttachmentViewState;
import com.helpshift.widget.MutableProfileFormViewState;
import com.helpshift.widget.MutableProgressBarViewState;
import com.helpshift.widget.MutableTextViewState;
import com.helpshift.widget.ProfileFormViewState;
import com.helpshift.widget.ProgressBarViewState;
import com.helpshift.widget.TextViewState;
import com.helpshift.widget.WidgetGateway;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class NewConversationVM implements ConversationController.StartNewConversationListener, AuthenticationFailureDM.AuthenticationFailureObserver {
    private static final String TAG = "Helpshift_NewConvVM";
    final MutableButtonViewState attachImageButtonViewState;
    final ConversationController conversationController;
    final MutableTextViewState descriptionViewState;
    final Domain domain;
    final MutableTextViewState emailViewState;
    final MutableImageAttachmentViewState imageAttachmentViewState;
    final MutableTextViewState nameViewState;
    final Platform platform;
    final MutableProfileFormViewState profileFormViewState;
    final MutableProgressBarViewState progressBarViewState;
    WeakReference<NewConversationRenderer> rendererWeakRef;
    final SDKConfigurationDM sdkConfigurationDM;
    final MutableButtonViewState startConversationButtonViewState;
    boolean wasSearchPerformed = false;
    final WidgetGateway widgetGateway;

    public NewConversationVM(Platform platform2, Domain domain2, ConversationController conversationController2, NewConversationRenderer newConversationRenderer) {
        this.platform = platform2;
        this.domain = domain2;
        this.sdkConfigurationDM = domain2.getSDKConfigurationDM();
        this.conversationController = conversationController2;
        this.widgetGateway = new WidgetGateway(this.sdkConfigurationDM, conversationController2);
        this.descriptionViewState = this.widgetGateway.makeDescriptionViewState();
        this.nameViewState = this.widgetGateway.makeNameViewState();
        this.emailViewState = this.widgetGateway.makeEmailViewState();
        this.imageAttachmentViewState = this.widgetGateway.makeImageAttachmentWidget();
        this.progressBarViewState = this.widgetGateway.makeProgressBarViewState();
        this.profileFormViewState = this.widgetGateway.makeProfileFormViewState(this.nameViewState, this.emailViewState);
        this.attachImageButtonViewState = this.widgetGateway.makeNewConversationAttachImageButtonViewState(this.imageAttachmentViewState);
        this.startConversationButtonViewState = this.widgetGateway.makeStartConversationButtonViewState();
        conversationController2.registerStartNewConversationListener(this);
        domain2.getAuthenticationFailureDM().registerListener(this);
        this.rendererWeakRef = new WeakReference<>(newConversationRenderer);
    }

    public void setDescription(final String str) {
        this.domain.runSerial(new F() {
            public void f() {
                if (!NewConversationVM.this.descriptionViewState.getText().equals(str)) {
                    NewConversationVM.this.descriptionViewState.setText(str);
                    NewConversationVM.this.widgetGateway.save(NewConversationVM.this.descriptionViewState);
                }
            }
        });
    }

    public void setName(final String str) {
        this.domain.runSerial(new F() {
            public void f() {
                if (!NewConversationVM.this.nameViewState.getText().equals(str)) {
                    NewConversationVM.this.nameViewState.setText(str);
                }
            }
        });
    }

    public void setEmail(final String str) {
        this.domain.runSerial(new F() {
            public void f() {
                if (!NewConversationVM.this.emailViewState.getText().equals(str)) {
                    NewConversationVM.this.emailViewState.setText(str);
                }
            }
        });
    }

    public void setWasSearchPerformed(final boolean z) {
        this.domain.runSerial(new F() {
            public void f() {
                NewConversationVM.this.wasSearchPerformed = z;
                if (NewConversationVM.this.shouldShowSearchOnNewConversation()) {
                    NewConversationVM.this.conversationController.triggerFAQSearchIndexing();
                }
            }
        });
    }

    public void showSearchOrStartNewConversation() {
        showSearchOrStartNewConversationInternal(true);
    }

    public void startNewConversation() {
        showSearchOrStartNewConversationInternal(false);
    }

    private void showSearchOrStartNewConversationInternal(final boolean z) {
        this.domain.runSerial(new F() {
            public void f() {
                if (NewConversationVM.this.isFormValid()) {
                    if (z && NewConversationVM.this.shouldShowSearchOnNewConversation()) {
                        ArrayList fAQSearchResults = NewConversationVM.this.conversationController.getFAQSearchResults(NewConversationVM.this.descriptionViewState.getText());
                        if (fAQSearchResults.size() > 0) {
                            if (NewConversationVM.this.rendererWeakRef.get() != null) {
                                NewConversationVM.this.rendererWeakRef.get().showSearchResultFragment(fAQSearchResults);
                                return;
                            }
                            return;
                        }
                    }
                    final Conversation activeConversationFromStorage = NewConversationVM.this.conversationController.getActiveConversationFromStorage();
                    if (activeConversationFromStorage != null) {
                        NewConversationVM.this.domain.runOnUI(new F() {
                            public void f() {
                                NewConversationVM.this.progressBarViewState.setVisible(false);
                                NewConversationVM.this.startConversationButtonViewState.setVisible(true);
                                NewConversationVM.this.imageAttachmentViewState.setClickable(true);
                                NewConversationVM.this.attachImageButtonViewState.setVisible(StringUtils.isEmpty(NewConversationVM.this.imageAttachmentViewState.getImagePath()));
                                if (NewConversationVM.this.rendererWeakRef.get() != null) {
                                    NewConversationVM.this.rendererWeakRef.get().gotoConversation(activeConversationFromStorage.localId.longValue());
                                }
                            }
                        });
                        return;
                    }
                    HSLogger.d(NewConversationVM.TAG, "Creating new conversation");
                    NewConversationVM.this.progressBarViewState.setVisible(true);
                    NewConversationVM.this.startConversationButtonViewState.setVisible(false);
                    NewConversationVM.this.attachImageButtonViewState.setVisible(false);
                    NewConversationVM.this.imageAttachmentViewState.setClickable(false);
                    NewConversationVM.this.conversationController.startNewConversation(NewConversationVM.this.descriptionViewState.getText(), NewConversationVM.this.nameViewState.getText(), NewConversationVM.this.emailViewState.getText(), NewConversationVM.this.imageAttachmentViewState.getImagePickerFile());
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean shouldShowSearchOnNewConversation() {
        return !this.wasSearchPerformed && this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.SHOW_SEARCH_ON_NEW_CONVERSATION);
    }

    public void onCreateConversationSuccess(final long j) {
        this.progressBarViewState.setVisible(false);
        this.startConversationButtonViewState.setVisible(true);
        this.descriptionViewState.setText(null);
        this.imageAttachmentViewState.setImagePickerFile(null);
        this.attachImageButtonViewState.setVisible(StringUtils.isEmpty(this.imageAttachmentViewState.getImagePath()));
        this.domain.runOnUI(new F() {
            public void f() {
                if (NewConversationVM.this.rendererWeakRef.get() != null) {
                    NewConversationRenderer newConversationRenderer = NewConversationVM.this.rendererWeakRef.get();
                    if (!NewConversationVM.this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.GOTO_CONVERSATION_AFTER_CONTACT_US) || NewConversationVM.this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.DISABLE_IN_APP_CONVERSATION)) {
                        newConversationRenderer.showConversationStartedMessage();
                        newConversationRenderer.exit();
                        return;
                    }
                    newConversationRenderer.gotoConversation(j);
                }
            }
        });
    }

    public void onCreateConversationFailure(Exception exc) {
        this.progressBarViewState.setVisible(false);
        this.startConversationButtonViewState.setVisible(true);
        this.imageAttachmentViewState.setClickable(true);
        this.attachImageButtonViewState.setVisible(StringUtils.isEmpty(this.imageAttachmentViewState.getImagePath()));
        handleException(exc);
    }

    public void unregisterRenderer(NewConversationRenderer newConversationRenderer) {
        if (this.rendererWeakRef != null && this.rendererWeakRef.get() == newConversationRenderer) {
            this.rendererWeakRef = new WeakReference<>(null);
        }
        this.domain.getAuthenticationFailureDM().unregisterListener(this);
        this.conversationController.unregisterStartNewConversationListener(this);
    }

    private void handleException(final Exception exc) {
        this.domain.runOnUI(new F() {
            public void f() {
                if (exc instanceof RootAPIException) {
                    RootAPIException rootAPIException = (RootAPIException) exc;
                    if (NewConversationVM.this.rendererWeakRef.get() != null) {
                        NewConversationVM.this.rendererWeakRef.get().showErrorView(rootAPIException.exceptionType);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isFormValid() {
        this.descriptionViewState.setError(validateDescription());
        this.nameViewState.setError(validateName());
        this.emailViewState.setError(validateEmail());
        return this.descriptionViewState.getError() == null && this.nameViewState.getError() == null && this.emailViewState.getError() == null;
    }

    public TextViewState.TextViewStatesError validateDescription() {
        String text = this.descriptionViewState.getText();
        if (text.length() == 0) {
            return TextViewState.TextViewStatesError.EMPTY;
        }
        if (TextViewState.specialCharactersPattern.matcher(text).matches()) {
            return TextViewState.TextViewStatesError.ONLY_SPECIAL_CHARACTERS;
        }
        if (text.length() < this.sdkConfigurationDM.getMinimumConversationDescriptionLength()) {
            return TextViewState.TextViewStatesError.LESS_THAN_MINIMUM_LENGTH;
        }
        return null;
    }

    public TextViewState.TextViewStatesError validateName() {
        String text = this.nameViewState.getText();
        if (text.length() == 0) {
            return TextViewState.TextViewStatesError.EMPTY;
        }
        if (TextViewState.specialCharactersPattern.matcher(text).matches()) {
            return TextViewState.TextViewStatesError.ONLY_SPECIAL_CHARACTERS;
        }
        return null;
    }

    public TextViewState.TextViewStatesError validateEmail() {
        String text = this.emailViewState.getText();
        if (text.length() == 0) {
            if (this.emailViewState.isRequired()) {
                return TextViewState.TextViewStatesError.EMPTY;
            }
            return null;
        } else if (!HSPattern.isValidEmail(text)) {
            return TextViewState.TextViewStatesError.INVALID_EMAIL;
        } else {
            return null;
        }
    }

    public void setSearchQuery(final String str) {
        this.domain.runSerial(new F() {
            public void f() {
                if (StringUtils.isEmpty(NewConversationVM.this.descriptionViewState.getText()) && !StringUtils.isEmpty(str)) {
                    NewConversationVM.this.descriptionViewState.setText(str.substring(0, 1).toUpperCase() + str.substring(1));
                }
            }
        });
    }

    public void handleImageAttachmentClearButtonClick() {
        if (!this.progressBarViewState.isVisible()) {
            this.domain.runSerial(new F() {
                public void f() {
                    ImagePickerFile imagePickerFile = NewConversationVM.this.imageAttachmentViewState.getImagePickerFile();
                    if (imagePickerFile != null && imagePickerFile.filePath != null) {
                        NewConversationVM.this.domain.getAttachmentFileManagerDM().deleteAttachmentLocalCopy(imagePickerFile);
                    }
                }
            });
            setImageAttachment(null);
        }
    }

    public void setImageAttachment(final ImagePickerFile imagePickerFile) {
        this.domain.runSerial(new F() {
            public void f() {
                NewConversationVM.this.imageAttachmentViewState.setImagePickerFile(imagePickerFile);
                NewConversationVM.this.widgetGateway.save(NewConversationVM.this.imageAttachmentViewState);
                if (NewConversationVM.this.progressBarViewState.isVisible()) {
                    NewConversationVM.this.attachImageButtonViewState.setVisible(false);
                } else {
                    NewConversationVM.this.attachImageButtonViewState.setVisible(StringUtils.isEmpty(NewConversationVM.this.imageAttachmentViewState.getImagePath()));
                }
            }
        });
    }

    public void handleImageAttachmentClick() {
        if (!this.progressBarViewState.isVisible()) {
            this.domain.runSerial(new F() {
                public void f() {
                    final ImagePickerFile imagePickerFile = NewConversationVM.this.imageAttachmentViewState.getImagePickerFile();
                    if (imagePickerFile != null && !StringUtils.isEmpty(imagePickerFile.filePath)) {
                        NewConversationVM.this.domain.runOnUI(new F() {
                            public void f() {
                                if (NewConversationVM.this.rendererWeakRef.get() != null) {
                                    NewConversationVM.this.rendererWeakRef.get().showAttachmentPreviewScreenFromDraft(imagePickerFile);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void setShouldDropCustomMetadata(final boolean z) {
        this.domain.runSerial(new F() {
            public void f() {
                NewConversationVM.this.conversationController.setShouldDropCustomMetadata(z);
            }
        });
    }

    public void setConversationViewState(final int i) {
        this.domain.runSerial(new F() {
            public void f() {
                NewConversationVM.this.conversationController.setConversationViewState(i);
            }
        });
    }

    public void onAuthenticationFailure() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (NewConversationVM.this.rendererWeakRef.get() != null) {
                    NewConversationVM.this.rendererWeakRef.get().onAuthenticationFailure();
                }
            }
        });
    }

    public TextViewState getDescriptionViewState() {
        return this.descriptionViewState;
    }

    public ProgressBarViewState getProgressBarViewState() {
        return this.progressBarViewState;
    }

    public ButtonViewState getStartConversationButtonState() {
        return this.startConversationButtonViewState;
    }

    public ButtonViewState getAttachImageButtonViewState() {
        return this.attachImageButtonViewState;
    }

    public ImageAttachmentViewState getImageAttachmentViewState() {
        return this.imageAttachmentViewState;
    }

    public TextViewState getNameViewState() {
        return this.nameViewState;
    }

    public TextViewState getEmailViewState() {
        return this.emailViewState;
    }

    public ProfileFormViewState getProfileFormViewState() {
        return this.profileFormViewState;
    }
}
