package com.helpshift.conversation.dto;

public class ImagePickerFile {
    public String filePath;
    public boolean isFileCompressionAndCopyingDone;
    public final String originalFileName;
    public final Long originalFileSize;
    public Object transientUri;

    public ImagePickerFile(String str, String str2, Long l) {
        this.filePath = str;
        this.originalFileName = str2;
        this.originalFileSize = l;
    }

    public ImagePickerFile(Object obj, String str, Long l) {
        this.transientUri = obj;
        this.originalFileName = str;
        this.originalFileSize = l;
    }
}
