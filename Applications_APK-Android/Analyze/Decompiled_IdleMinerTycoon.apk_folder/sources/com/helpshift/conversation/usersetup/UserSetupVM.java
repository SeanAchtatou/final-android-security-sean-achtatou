package com.helpshift.conversation.usersetup;

import com.helpshift.account.AuthenticationFailureDM;
import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.account.domainmodel.UserSetupDM;
import com.helpshift.account.domainmodel.UserSetupState;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.activeconversation.usersetup.UserSetupRenderer;
import com.helpshift.widget.MutableProgressBarViewState;
import com.helpshift.widget.MutableProgressDescriptionViewState;
import com.helpshift.widget.MutableUserOfflineErrorViewState;
import com.helpshift.widget.ProgressBarViewState;
import com.helpshift.widget.ProgressDescriptionViewState;
import com.helpshift.widget.UserOfflineErrorViewState;

public class UserSetupVM implements UserSetupDM.UserSetupListener, AuthenticationFailureDM.AuthenticationFailureObserver {
    private Domain domain;
    /* access modifiers changed from: private */
    public final MutableUserOfflineErrorViewState errorViewState = new MutableUserOfflineErrorViewState();
    private Platform platform;
    /* access modifiers changed from: private */
    public final MutableProgressBarViewState progressBarViewState = buildProgressBarWidget();
    private final MutableProgressDescriptionViewState progressDescriptionViewState = new MutableProgressDescriptionViewState();
    /* access modifiers changed from: private */
    public UserSetupRenderer renderer;
    private UserSetupDM userSetupDM;

    public UserSetupVM(Platform platform2, Domain domain2, UserSetupDM userSetupDM2, UserSetupRenderer userSetupRenderer) {
        this.platform = platform2;
        this.userSetupDM = userSetupDM2;
        this.renderer = userSetupRenderer;
        this.domain = domain2;
        this.userSetupDM.registerUserSetupListener(this);
        this.domain.getAuthenticationFailureDM().registerListener(this);
    }

    private MutableProgressBarViewState buildProgressBarWidget() {
        MutableProgressBarViewState mutableProgressBarViewState = new MutableProgressBarViewState();
        mutableProgressBarViewState.setVisible(this.userSetupDM.getState() == UserSetupState.IN_PROGRESS);
        return mutableProgressBarViewState;
    }

    public void onResume() {
        if (this.userSetupDM.getState() == UserSetupState.COMPLETED) {
            handleUserSetupComplete();
        } else {
            this.userSetupDM.startSetup();
        }
    }

    public void onDestroyView() {
        this.renderer = null;
    }

    private void handleUserSetupState(UserSetupState userSetupState) {
        if (!this.platform.isOnline()) {
            onNetworkUnavailable();
            return;
        }
        switch (userSetupState) {
            case NON_STARTED:
            case FAILED:
                this.progressDescriptionViewState.setVisible(true);
                this.progressBarViewState.setVisible(true);
                return;
            case IN_PROGRESS:
                this.progressBarViewState.setVisible(true);
                this.errorViewState.setVisible(false);
                return;
            case COMPLETED:
                handleUserSetupComplete();
                return;
            default:
                return;
        }
    }

    private void handleUserSetupComplete() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (UserSetupVM.this.renderer != null) {
                    UserSetupVM.this.renderer.onUserSetupComplete();
                }
            }
        });
    }

    public void userSetupStateChanged(UserDM userDM, UserSetupState userSetupState) {
        handleUserSetupState(userSetupState);
    }

    public void onAuthenticationFailure() {
        this.domain.runOnUI(new F() {
            public void f() {
                if (UserSetupVM.this.renderer != null) {
                    UserSetupVM.this.renderer.onAuthenticationFailure();
                }
            }
        });
    }

    public void onNetworkAvailable() {
        this.domain.runOnUI(new F() {
            public void f() {
                UserSetupVM.this.progressBarViewState.setVisible(true);
                UserSetupVM.this.errorViewState.setVisible(false);
            }
        });
    }

    public void onNetworkUnavailable() {
        this.domain.runOnUI(new F() {
            public void f() {
                UserSetupVM.this.showOfflineError();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showOfflineError() {
        this.progressBarViewState.setVisible(false);
        this.progressDescriptionViewState.setVisible(false);
        this.errorViewState.setVisible(true);
    }

    public ProgressBarViewState getProgressBarViewState() {
        return this.progressBarViewState;
    }

    public ProgressDescriptionViewState getDescriptionProgressViewState() {
        return this.progressDescriptionViewState;
    }

    public UserOfflineErrorViewState getUserOfflineErrorViewState() {
        return this.errorViewState;
    }
}
