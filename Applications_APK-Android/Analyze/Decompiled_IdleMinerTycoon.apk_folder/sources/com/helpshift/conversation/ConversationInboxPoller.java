package com.helpshift.conversation;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.app.AppLifeCycleStateHolder;
import com.helpshift.common.ListUtils;
import com.helpshift.common.domain.PollFunction;
import com.helpshift.common.domain.Poller;
import com.helpshift.common.domain.PollingInterval;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dao.ConversationDAO;
import com.helpshift.util.HSLogger;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ConversationInboxPoller implements Observer {
    private static final long INITIAL_DELAY = 3000;
    private static final String TAG = "Helpshift_ConvPoller";
    private final ConversationDAO conversationDAO;
    private PollingInterval currentPollingInterval;
    private PollFunction.PollFunctionListener pollFunctionListener = new PollFunction.PollFunctionListener() {
        public void onPollingStoppedViaBackoffStrategy() {
            HSLogger.d(ConversationInboxPoller.TAG, "Poll stopped via backoff, resetting currentPollingInterval");
            ConversationInboxPoller.this.stop();
        }
    };
    public final Poller poller;
    private final SDKConfigurationDM sdkConfigurationDM;
    private final UserDM userDM;

    public ConversationInboxPoller(UserDM userDM2, SDKConfigurationDM sDKConfigurationDM, Poller poller2, ConversationDAO conversationDAO2) {
        this.userDM = userDM2;
        this.sdkConfigurationDM = sDKConfigurationDM;
        this.poller = poller2;
        this.conversationDAO = conversationDAO2;
    }

    private boolean shouldStartSDKPoller() {
        return AppLifeCycleStateHolder.isAppInForeground() && this.userDM.issueExists() && !this.userDM.isPushTokenSynced() && !this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.DISABLE_IN_APP_CONVERSATION);
    }

    public void startAppPoller(boolean z) {
        if (!shouldStartSDKPoller()) {
            stop();
            return;
        }
        List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
        PollingInterval pollingInterval = !ListUtils.isEmpty(readConversationsWithoutMessages) ? ConversationUtil.shouldPollActivelyForConversations(readConversationsWithoutMessages) ^ true : false ? PollingInterval.PASSIVE : PollingInterval.CONSERVATIVE;
        if (this.currentPollingInterval != pollingInterval) {
            stop();
            this.currentPollingInterval = pollingInterval;
            HSLogger.d(TAG, "Listening for conversation updates : " + this.currentPollingInterval);
            this.poller.start(pollingInterval, z ? INITIAL_DELAY : 0, this.pollFunctionListener);
        }
    }

    public void startChatPoller() {
        if (!AppLifeCycleStateHolder.isAppInForeground()) {
            stop();
        } else if (this.currentPollingInterval != PollingInterval.AGGRESSIVE) {
            stop();
            this.currentPollingInterval = PollingInterval.AGGRESSIVE;
            HSLogger.d(TAG, "Listening for in-chat conversation updates");
            this.poller.start(PollingInterval.AGGRESSIVE, 0, this.pollFunctionListener);
        }
    }

    public void stop() {
        HSLogger.d(TAG, "Stopped listening for conversation updates : " + this.currentPollingInterval);
        this.poller.stop();
        this.currentPollingInterval = null;
    }

    public void update(Observable observable, Object obj) {
        refreshPoller(true);
    }

    public void refreshPoller(boolean z) {
        if (!AppLifeCycleStateHolder.isAppInForeground() || !this.userDM.isActiveUser()) {
            stop();
        } else if (this.currentPollingInterval == PollingInterval.AGGRESSIVE) {
            startChatPoller();
        } else {
            startAppPoller(z);
        }
    }
}
