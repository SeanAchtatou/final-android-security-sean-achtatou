package com.helpshift.conversation;

import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.F;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.domainmodel.ConversationController;
import com.helpshift.util.HSLogger;
import java.lang.ref.WeakReference;

public class CreatePreIssueDM extends F {
    private static final String TAG = "Helpshift_CrtePreIsue";
    private final Conversation conversation;
    private final ConversationController conversationController;
    private final String greetingMessage;
    private WeakReference<ConversationController.StartNewConversationListener> listener;
    private final String userMessage;

    public CreatePreIssueDM(ConversationController conversationController2, Conversation conversation2, ConversationController.StartNewConversationListener startNewConversationListener, String str, String str2) {
        this.conversation = conversation2;
        this.conversationController = conversationController2;
        this.listener = new WeakReference<>(startNewConversationListener);
        this.greetingMessage = str;
        this.userMessage = str2;
    }

    public void f() {
        try {
            if (this.conversation.preConversationServerId == null) {
                HSLogger.d(TAG, "Filing preissue with backend.");
                this.conversationController.createPreIssueNetwork(this.conversation, this.greetingMessage, this.userMessage);
                this.conversationController.conversationManager.updateLastUserActivityTime(this.conversation, System.currentTimeMillis());
                if (this.listener.get() != null) {
                    this.listener.get().onCreateConversationSuccess(this.conversation.localId.longValue());
                }
            }
        } catch (RootAPIException e) {
            HSLogger.e(TAG, "Error filing a pre-issue", e);
            if (this.listener.get() != null && StringUtils.isEmpty(this.conversation.getPreIssueId())) {
                this.listener.get().onCreateConversationFailure(e);
            }
        }
    }

    public void setListener(ConversationController.StartNewConversationListener startNewConversationListener) {
        this.listener = new WeakReference<>(startNewConversationListener);
    }
}
