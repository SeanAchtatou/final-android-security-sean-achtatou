package com.helpshift.conversation.domainmodel;

import android.support.annotation.Nullable;
import com.appsflyer.share.Constants;
import com.helpshift.account.domainmodel.IUserSyncExecutor;
import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.account.domainmodel.UserSyncStatus;
import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.AutoRetriableDM;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.FetchDataFromThread;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.domain.One;
import com.helpshift.common.domain.Poller;
import com.helpshift.common.domain.idempotent.AllStatusCodeIdempotentPolicy;
import com.helpshift.common.domain.idempotent.SuccessOrNonRetriableStatusCodeIdempotentPolicy;
import com.helpshift.common.domain.network.AuthenticationFailureNetwork;
import com.helpshift.common.domain.network.FailedAPICallNetworkDecorator;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.IdempotentNetwork;
import com.helpshift.common.domain.network.MetaCorrectedNetwork;
import com.helpshift.common.domain.network.Network;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.domain.network.POSTNetwork;
import com.helpshift.common.domain.network.PUTNetwork;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.ConversationInboxPoller;
import com.helpshift.conversation.ConversationUtil;
import com.helpshift.conversation.CreatePreIssueDM;
import com.helpshift.conversation.IssueType;
import com.helpshift.conversation.activeconversation.ConversationManager;
import com.helpshift.conversation.activeconversation.ConversationUpdate;
import com.helpshift.conversation.activeconversation.LiveUpdateDM;
import com.helpshift.conversation.activeconversation.ViewableConversation;
import com.helpshift.conversation.activeconversation.ViewableSingleConversation;
import com.helpshift.conversation.activeconversation.message.AdminMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dao.ConversationDAO;
import com.helpshift.conversation.dao.ConversationInboxDAO;
import com.helpshift.conversation.dao.PushNotificationData;
import com.helpshift.conversation.dto.ConversationDetailDTO;
import com.helpshift.conversation.dto.ConversationInbox;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.conversation.loaders.RemoteConversationLoader;
import com.helpshift.conversation.loaders.SingleConversationLoader;
import com.helpshift.conversation.states.ConversationCSATState;
import com.helpshift.conversation.util.predicate.ConversationPredicates;
import com.helpshift.conversation.viewmodel.ConversationVMCallback;
import com.helpshift.faq.domainmodel.FAQSearchDM;
import com.helpshift.providers.ICampaignsModuleAPIs;
import com.helpshift.util.Filters;
import com.helpshift.util.HSLogger;
import com.helpshift.util.ValuePair;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class ConversationController implements AutoRetriableDM, IUserSyncExecutor {
    private static final long ACTIVE_ISSUE_NOTIFICATION_COUNT_TIMEOUT = 60000;
    private static final String CREATE_ISSUE_ROUTE = "/issues/";
    private static final String CREATE_ISSUE_UNIQUE_MAPPING_KEY = "issue_default_unique_key";
    private static final String CREATE_PRE_ISSUE_ROUTE = "/preissues/";
    private static final String CREATE_PRE_ISSUE_UNIQUE_MAPPING_KEY = "preissue_default_unique_key";
    private static final long INACTIVE_ISSUES_NOTIFICATION_COUNT_TIMEOUT = 300000;
    public static final long MESSAGES_PAGE_SIZE = 100;
    private static final String TAG = "Helpshift_ConvInboxDM";
    static final Object fetchConversationUpdatesLock = new Object();
    private WeakReference<ViewableConversation> aliveViewableConversation;
    final ConversationDAO conversationDAO;
    private final ConversationInboxDAO conversationInboxDAO;
    private final ConversationInboxPoller conversationInboxPoller;
    public final ConversationManager conversationManager;
    private int conversationViewState = -1;
    final Domain domain;
    private final FAQSearchDM faqSearchDM;
    public AtomicReference<FetchDataFromThread<Integer, Integer>> fetchConversationUpdatesListenerReference = null;
    private Map<String, Integer> inAppNotificationMessageCountMap = new ConcurrentHashMap();
    HashMap<Long, One> inProgressPreIssueCreators = new HashMap<>();
    private boolean isCreateConversationInProgress;
    private long lastNotifCountFetchTime = 0;
    private final LiveUpdateDM liveUpdateDM;
    final Platform platform;
    private RemoteConversationLoader remoteConversationLoader;
    private final SDKConfigurationDM sdkConfigurationDM;
    private boolean shouldDropCustomMetadata;
    private WeakReference<StartNewConversationListener> startNewConversationListenerRef;
    private boolean userCanReadMessages;
    final UserDM userDM;

    public interface StartNewConversationListener {
        void onCreateConversationFailure(Exception exc);

        void onCreateConversationSuccess(long j);
    }

    public ConversationController(Platform platform2, Domain domain2, UserDM userDM2) {
        this.platform = platform2;
        this.domain = domain2;
        this.userDM = userDM2;
        this.conversationInboxDAO = platform2.getConversationInboxDAO();
        this.conversationDAO = platform2.getConversationDAO();
        this.faqSearchDM = platform2.getFAQSearchDM();
        this.sdkConfigurationDM = domain2.getSDKConfigurationDM();
        this.conversationInboxPoller = new ConversationInboxPoller(userDM2, this.sdkConfigurationDM, getPoller(), this.conversationDAO);
        this.liveUpdateDM = new LiveUpdateDM(domain2, platform2);
        this.conversationManager = new ConversationManager(platform2, domain2, userDM2);
        this.remoteConversationLoader = new RemoteConversationLoader(platform2, domain2, userDM2, this.conversationManager);
    }

    public void initialize() {
        this.domain.getAutoRetryFailedEventDM().register(AutoRetryFailedEventDM.EventType.CONVERSATION, this);
        if (this.userDM.getSyncState() == UserSyncStatus.COMPLETED) {
            this.userDM.addObserver(getConversationInboxPoller());
        }
    }

    public ConversationInboxPoller getConversationInboxPoller() {
        return this.conversationInboxPoller;
    }

    /* access modifiers changed from: package-private */
    public void deleteAllConversationsData() {
        deleteConversationsAndMessages();
        this.conversationInboxDAO.deleteUserData(this.userDM.getLocalId().longValue());
    }

    private void deleteConversationsAndMessages() {
        long longValue = this.userDM.getLocalId().longValue();
        for (Conversation next : this.conversationDAO.readConversationsWithoutMessages(longValue)) {
            next.userLocalId = this.userDM.getLocalId().longValue();
            this.conversationManager.deleteCachedScreenshotFiles(next);
        }
        this.conversationDAO.deleteConversations(longValue);
    }

    private Poller getPoller() {
        return new Poller(this.domain, new F() {
            public synchronized void f() {
                ConversationController.this.fetchConversationUpdates();
            }
        });
    }

    public void sendFailedApiCalls(AutoRetryFailedEventDM.EventType eventType) {
        for (Conversation next : this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue())) {
            ViewableConversation aliveViewableConversation2 = getAliveViewableConversation(next.localId);
            if (aliveViewableConversation2 != null) {
                retryCallsForConversation(aliveViewableConversation2.getActiveConversation(), true);
            } else {
                retryCallsForConversation(next, false);
            }
        }
    }

    private void retryCallsForConversation(Conversation conversation, boolean z) {
        conversation.userLocalId = this.userDM.getLocalId().longValue();
        this.conversationManager.retryMessages(conversation, z);
        if (conversation.csatState == ConversationCSATState.SUBMITTED_NOT_SYNCED) {
            try {
                this.conversationManager.sendCSATSurveyInternal(conversation);
            } catch (RootAPIException e) {
                if (e.exceptionType != NetworkException.NON_RETRIABLE) {
                    throw e;
                }
            }
        }
    }

    public void setUserCanReadMessages(boolean z) {
        this.userCanReadMessages = z;
    }

    public ConversationDetailDTO getConversationDetail() {
        return this.conversationInboxDAO.getDescriptionDetail(this.userDM.getLocalId().longValue());
    }

    public String getConversationArchivalPrefillText() {
        return this.conversationInboxDAO.getConversationArchivalPrefillText(this.userDM.getLocalId().longValue());
    }

    public void saveDescriptionDetail(String str, int i) {
        this.conversationInboxDAO.saveDescriptionDetail(this.userDM.getLocalId().longValue(), new ConversationDetailDTO(str, System.nanoTime(), i));
    }

    public void saveName(String str) {
        this.conversationInboxDAO.saveName(this.userDM.getLocalId().longValue(), str);
    }

    public void saveEmail(String str) {
        this.conversationInboxDAO.saveEmail(this.userDM.getLocalId().longValue(), str);
    }

    public String getName() {
        String name = this.conversationInboxDAO.getName(this.userDM.getLocalId().longValue());
        return StringUtils.isEmpty(name) ? this.userDM.getName() : name;
    }

    public String getEmail() {
        String email = this.conversationInboxDAO.getEmail(this.userDM.getLocalId().longValue());
        return StringUtils.isEmpty(email) ? this.userDM.getEmail() : email;
    }

    public void saveImageAttachmentDraft(ImagePickerFile imagePickerFile) {
        this.conversationInboxDAO.saveImageAttachment(this.userDM.getLocalId().longValue(), imagePickerFile);
    }

    public void saveLastConversationsRedactionTime(long j) {
        this.conversationInboxDAO.saveLastConversationsRedactionTime(this.userDM.getLocalId().longValue(), j);
    }

    public Long getLastConversationsRedactionTime() {
        return this.conversationInboxDAO.getLastConversationsRedactionTime(this.userDM.getLocalId().longValue());
    }

    public ImagePickerFile getImageAttachmentDraft() {
        return this.conversationInboxDAO.getImageAttachment(this.userDM.getLocalId().longValue());
    }

    public void saveUserReplyText(String str) {
        this.conversationInboxDAO.saveUserReplyDraft(this.userDM.getLocalId().longValue(), str);
    }

    public String getUserReplyText() {
        return this.conversationInboxDAO.getUserReplyDraft(this.userDM.getLocalId().longValue());
    }

    public ArrayList getFAQSearchResults(String str) {
        return this.faqSearchDM.getSearchResults(str);
    }

    public void triggerFAQSearchIndexing() {
        this.faqSearchDM.startFAQSearchIndexing();
    }

    public void setShouldDropCustomMetadata(boolean z) {
        this.shouldDropCustomMetadata = z;
    }

    public void registerStartNewConversationListener(StartNewConversationListener startNewConversationListener) {
        this.startNewConversationListenerRef = new WeakReference<>(startNewConversationListener);
    }

    public void unregisterStartNewConversationListener(StartNewConversationListener startNewConversationListener) {
        if (this.startNewConversationListenerRef != null && this.startNewConversationListenerRef.get() == startNewConversationListener) {
            this.startNewConversationListenerRef = new WeakReference<>(null);
        }
    }

    public void startNewConversation(String str, String str2, String str3, ImagePickerFile imagePickerFile) {
        this.domain.runParallel(new CreateConversationStateHolder(str, str2, str3, imagePickerFile).getStartNewConversationInternalF());
    }

    /* access modifiers changed from: package-private */
    public void startNewConversationInternal(String str, String str2, String str3, ImagePickerFile imagePickerFile) {
        this.isCreateConversationInProgress = true;
        Conversation tryToStartNewConversation = tryToStartNewConversation(str, str2, str3);
        ViewableSingleConversation viewableSingleConversation = new ViewableSingleConversation(this.platform, this.domain, this.userDM, new SingleConversationLoader(this.platform, this.userDM, tryToStartNewConversation.localId, this.remoteConversationLoader, 100), this.conversationManager);
        viewableSingleConversation.init();
        viewableSingleConversation.setLiveUpdateDM(this.liveUpdateDM);
        setAliveConversation(viewableSingleConversation);
        checkAndTryToUploadImage(viewableSingleConversation.getActiveConversation(), imagePickerFile);
        this.isCreateConversationInProgress = false;
        if (this.startNewConversationListenerRef != null && this.startNewConversationListenerRef.get() != null) {
            this.startNewConversationListenerRef.get().onCreateConversationSuccess(tryToStartNewConversation.localId.longValue());
        }
    }

    private synchronized void setAliveConversation(ViewableConversation viewableConversation) {
        this.aliveViewableConversation = new WeakReference<>(viewableConversation);
    }

    private synchronized void removeInMemoryConversation() {
        this.aliveViewableConversation = null;
    }

    public Conversation tryToStartNewConversation(String str, String str2, String str3) {
        Conversation createConversation;
        try {
            synchronized (fetchConversationUpdatesLock) {
                createConversation = createConversation(str, str2, str3);
            }
            saveDescriptionDetail("", 0);
            if (!this.sdkConfigurationDM.shouldCreateConversationAnonymously()) {
                saveName(str2);
                saveEmail(str3);
            }
            this.conversationInboxDAO.saveConversationArchivalPrefillText(this.userDM.getLocalId().longValue(), null);
            checkAndDropCustomMeta(createConversation);
            sendConversationPostedEvent(createConversation.serverId);
            this.domain.getDelegate().newConversationStarted(str);
            return createConversation;
        } catch (Exception e) {
            this.isCreateConversationInProgress = false;
            if (this.startNewConversationListenerRef.get() != null) {
                this.startNewConversationListenerRef.get().onCreateConversationFailure(e);
            }
            throw e;
        }
    }

    public void checkAndDropCustomMeta(Conversation conversation) {
        if (this.shouldDropCustomMetadata) {
            this.conversationManager.dropCustomMetaData();
        }
    }

    private void checkAndTryToUploadImage(Conversation conversation, ImagePickerFile imagePickerFile) {
        if (imagePickerFile != null && imagePickerFile.filePath != null) {
            try {
                this.conversationManager.sendScreenshot(conversation, imagePickerFile, null);
            } catch (Exception unused) {
            }
            saveImageAttachmentDraft(null);
        }
    }

    private void sendConversationPostedEvent(String str) {
        this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.CONVERSATION_POSTED, str);
    }

    public Conversation createConversation(String str, String str2, String str3) {
        this.domain.getUserManagerDM().registerUserWithServer(this.userDM);
        HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(this.userDM);
        userRequestData.put("user_provided_emails", this.platform.getJsonifier().jsonify(Collections.singletonList(str3)).toString());
        userRequestData.put("user_provided_name", str2);
        userRequestData.put("body", str);
        userRequestData.put("cuid", getCampaignUID());
        userRequestData.put("cdid", getCampaignDID());
        userRequestData.put("device_language", this.domain.getLocaleProviderDM().getDefaultLanguage());
        String sDKLanguage = this.domain.getLocaleProviderDM().getSDKLanguage();
        if (!StringUtils.isEmpty(sDKLanguage)) {
            userRequestData.put("developer_set_language", sDKLanguage);
        }
        userRequestData.put("meta", this.domain.getMetaDataDM().getMetaInfo().toString());
        boolean z = this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.ENABLE_FULL_PRIVACY);
        Object customIssueFieldData = this.domain.getCustomIssueFieldDM().getCustomIssueFieldData();
        if (customIssueFieldData != null) {
            userRequestData.put("custom_fields", customIssueFieldData.toString());
        }
        try {
            Conversation parseReadableConversation = this.platform.getResponseParser().parseReadableConversation(new GuardOKNetwork(new MetaCorrectedNetwork(new TSCorrectedNetwork(new AuthenticationFailureNetwork(new IdempotentNetwork(new POSTNetwork(CREATE_ISSUE_ROUTE, this.domain, this.platform), this.platform, new AllStatusCodeIdempotentPolicy(), CREATE_ISSUE_ROUTE, CREATE_ISSUE_UNIQUE_MAPPING_KEY)), this.platform), this.platform)).makeRequest(new RequestData(userRequestData)).responseString);
            parseReadableConversation.wasFullPrivacyEnabledAtCreation = z;
            parseReadableConversation.userLocalId = this.userDM.getLocalId().longValue();
            if (this.conversationDAO.readConversationWithoutMessages(parseReadableConversation.serverId) == null) {
                this.conversationDAO.insertConversation(parseReadableConversation);
            }
            this.domain.getUserManagerDM().updateIssueExists(this.userDM, true);
            this.domain.getUserManagerDM().sendPushToken();
            this.conversationInboxPoller.startAppPoller(true);
            return parseReadableConversation;
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(this.userDM, e.exceptionType);
            }
            throw e;
        }
    }

    public void createPreIssue(ViewableConversation viewableConversation, String str, String str2, StartNewConversationListener startNewConversationListener) {
        final Conversation activeConversation = viewableConversation.getActiveConversation();
        One one = this.inProgressPreIssueCreators.get(activeConversation.localId);
        if (one != null) {
            HSLogger.d(TAG, "Pre issue creation already in progress: " + activeConversation.localId);
            ((CreatePreIssueDM) one.getF()).setListener(startNewConversationListener);
            return;
        }
        final One one2 = new One(new CreatePreIssueDM(this, activeConversation, startNewConversationListener, str, str2));
        this.inProgressPreIssueCreators.put(activeConversation.localId, one2);
        this.domain.runParallel(new F() {
            public void f() {
                try {
                    synchronized (ConversationController.fetchConversationUpdatesLock) {
                        one2.f();
                    }
                    ConversationController.this.inProgressPreIssueCreators.remove(activeConversation.localId);
                } catch (Throwable th) {
                    ConversationController.this.inProgressPreIssueCreators.remove(activeConversation.localId);
                    throw th;
                }
            }
        });
    }

    public boolean isPreissueCreationInProgress(long j) {
        return this.inProgressPreIssueCreators.containsKey(Long.valueOf(j));
    }

    public void createPreIssueNetwork(Conversation conversation, String str, String str2) {
        HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(this.userDM);
        String name = this.userDM.getName();
        String email = this.userDM.getEmail();
        if (!StringUtils.isEmpty(name)) {
            userRequestData.put("name", name);
        }
        if (!StringUtils.isEmpty(email)) {
            userRequestData.put("email", email);
        }
        userRequestData.put("cuid", getCampaignUID());
        userRequestData.put("cdid", getCampaignDID());
        userRequestData.put("device_language", this.domain.getLocaleProviderDM().getDefaultLanguage());
        String sDKLanguage = this.domain.getLocaleProviderDM().getSDKLanguage();
        if (!StringUtils.isEmpty(sDKLanguage)) {
            userRequestData.put("developer_set_language", sDKLanguage);
        }
        userRequestData.put("meta", this.domain.getMetaDataDM().getMetaInfo().toString());
        boolean z = this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.ENABLE_FULL_PRIVACY);
        Object customIssueFieldData = this.domain.getCustomIssueFieldDM().getCustomIssueFieldData();
        if (customIssueFieldData != null) {
            userRequestData.put("custom_fields", customIssueFieldData.toString());
        }
        if (!StringUtils.isEmpty(str)) {
            userRequestData.put("greeting", str);
        }
        if (!StringUtils.isEmpty(str2)) {
            userRequestData.put("user_message", str2);
        }
        userRequestData.put("is_prefilled", String.valueOf(conversation.isAutoFilledPreIssue));
        try {
            Conversation parseReadableConversation = this.platform.getResponseParser().parseReadableConversation(new GuardOKNetwork(new MetaCorrectedNetwork(new TSCorrectedNetwork(new AuthenticationFailureNetwork(new IdempotentNetwork(new POSTNetwork(CREATE_PRE_ISSUE_ROUTE, this.domain, this.platform), this.platform, new SuccessOrNonRetriableStatusCodeIdempotentPolicy(), CREATE_PRE_ISSUE_ROUTE, CREATE_PRE_ISSUE_UNIQUE_MAPPING_KEY)), this.platform), this.platform)).makeRequest(new RequestData(userRequestData)).responseString);
            if (conversation.serverId == null) {
                conversation.serverId = parseReadableConversation.serverId;
            }
            conversation.title = parseReadableConversation.title;
            conversation.setCreatedAt(parseReadableConversation.getCreatedAt());
            conversation.setEpochCreatedAtTime(parseReadableConversation.getEpochCreatedAtTime());
            conversation.updatedAt = parseReadableConversation.updatedAt;
            conversation.publishId = parseReadableConversation.publishId;
            conversation.showAgentName = parseReadableConversation.showAgentName;
            conversation.state = parseReadableConversation.state;
            conversation.wasFullPrivacyEnabledAtCreation = z;
            conversation.userLocalId = this.userDM.getLocalId().longValue();
            if (StringUtils.isEmpty(conversation.preConversationServerId)) {
                this.conversationDAO.deleteMessagesForConversation(conversation.localId.longValue());
                conversation.messageDMs = parseReadableConversation.messageDMs;
                Iterator<E> it = conversation.messageDMs.iterator();
                while (it.hasNext()) {
                    MessageDM messageDM = (MessageDM) it.next();
                    messageDM.conversationLocalId = conversation.localId;
                    if (messageDM instanceof AdminMessageDM) {
                        messageDM.deliveryState = 1;
                    } else if (messageDM instanceof UserMessageDM) {
                        messageDM.deliveryState = 2;
                    }
                }
            }
            conversation.preConversationServerId = parseReadableConversation.preConversationServerId;
            this.domain.getUserManagerDM().updateIssueExists(this.userDM, true);
            this.domain.getUserManagerDM().sendPushToken();
            this.conversationDAO.updateConversation(conversation);
            if (StringUtils.isEmpty(str2)) {
                str2 = "";
            }
            this.domain.getDelegate().newConversationStarted(str2);
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(this.userDM, e.exceptionType);
            }
            throw e;
        }
    }

    private String getCampaignUID() {
        ICampaignsModuleAPIs campaignModuleAPIs = this.platform.getCampaignModuleAPIs();
        if (campaignModuleAPIs == null) {
            return null;
        }
        return campaignModuleAPIs.getUserIdentifier();
    }

    private String getCampaignDID() {
        ICampaignsModuleAPIs campaignModuleAPIs = this.platform.getCampaignModuleAPIs();
        if (campaignModuleAPIs == null) {
            return null;
        }
        return campaignModuleAPIs.getDeviceIdentifier();
    }

    public boolean isCreateConversationInProgress() {
        return this.isCreateConversationInProgress;
    }

    public ConversationInbox fetchConversationUpdates() {
        ConversationInbox fetchConversationUpdatesInternal;
        synchronized (fetchConversationUpdatesLock) {
            fetchConversationUpdatesInternal = fetchConversationUpdatesInternal(this.conversationInboxDAO.getConversationInboxTimestamp(this.userDM.getLocalId().longValue()));
        }
        return fetchConversationUpdatesInternal;
    }

    public ConversationInbox fetchInitialConversationUpdates() {
        ConversationInbox fetchConversationUpdatesInternal;
        synchronized (fetchConversationUpdatesLock) {
            fetchConversationUpdatesInternal = fetchConversationUpdatesInternal(null);
        }
        return fetchConversationUpdatesInternal;
    }

    private Network buildForwardPollerNetwork() {
        return new GuardOKNetwork(new TSCorrectedNetwork(new FailedAPICallNetworkDecorator(new AuthenticationFailureNetwork(new POSTNetwork("/conversations/updates/", this.domain, this.platform))), this.platform));
    }

    private RequestData buildForwardPollerRequestData(String str) {
        HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(this.userDM);
        if (!StringUtils.isEmpty(str)) {
            userRequestData.put("cursor", str);
        }
        Conversation lastViewableSyncedConversation = getLastViewableSyncedConversation();
        if (lastViewableSyncedConversation != null) {
            if (!StringUtils.isEmpty(lastViewableSyncedConversation.serverId)) {
                userRequestData.put(AnalyticsEventKey.ISSUE_ID, lastViewableSyncedConversation.serverId);
            } else if (!StringUtils.isEmpty(lastViewableSyncedConversation.preConversationServerId)) {
                userRequestData.put(AnalyticsEventKey.PREISSUE_ID, lastViewableSyncedConversation.preConversationServerId);
            }
        }
        userRequestData.put("ucrm", String.valueOf(this.userCanReadMessages));
        return new RequestData(userRequestData);
    }

    private ConversationInbox fetchConversationUpdatesInternal(String str) {
        ViewableConversation aliveViewableConversation2;
        Network buildForwardPollerNetwork = buildForwardPollerNetwork();
        RequestData buildForwardPollerRequestData = buildForwardPollerRequestData(str);
        try {
            ConversationInbox parseConversationInbox = this.platform.getResponseParser().parseConversationInbox(buildForwardPollerNetwork.makeRequest(buildForwardPollerRequestData).responseString);
            this.domain.getUserManagerDM().updateIssueExists(this.userDM, parseConversationInbox.issueExists);
            if (!buildForwardPollerRequestData.body.containsKey("cursor") && parseConversationInbox.hasOlderMessages != null) {
                this.conversationInboxDAO.saveHasOlderMessages(this.userDM.getLocalId().longValue(), parseConversationInbox.hasOlderMessages.booleanValue());
            }
            merge(parseConversationInbox.conversations);
            this.conversationInboxDAO.saveConversationInboxTimestamp(this.userDM.getLocalId().longValue(), parseConversationInbox.cursor);
            return parseConversationInbox;
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(this.userDM, e.exceptionType);
            } else if ((e.exceptionType instanceof NetworkException) && (aliveViewableConversation2 = getAliveViewableConversation()) != null && aliveViewableConversation2.isVisibleOnUI()) {
                aliveViewableConversation2.getConversationVMCallback().onConversationInboxPollFailure();
            }
            throw e;
        }
    }

    private void merge(List<Conversation> list) {
        if (!ListUtils.isEmpty(list)) {
            List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
            HashSet<Conversation> hashSet = new HashSet<>();
            HashMap hashMap = new HashMap();
            HashSet hashSet2 = new HashSet();
            if (list.size() > 1) {
                ConversationUtil.sortConversationsBasedOnCreatedAt(list);
            }
            List filter = Filters.filter(list, ConversationPredicates.allMessagesAfterLastMessageInDbPredicate(this.conversationManager));
            merge(readConversationsWithoutMessages, filter, hashSet, hashSet2, hashMap);
            clearRequestIdForPendingCreateConversationCall(filter);
            putConversations(hashSet, hashSet2, hashMap);
            for (Conversation conversation : hashSet) {
                this.conversationManager.clearMessageUpdates(conversation, (ConversationUpdate) hashMap.get(conversation.localId));
            }
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(hashSet);
            arrayList.addAll(hashSet2);
            checkForReOpen(arrayList);
            markStatusAsResolutionAcceptedIfResolved(hashSet);
            if (!this.userDM.isPushTokenSynced() && this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.ENABLE_IN_APP_NOTIFICATION)) {
                checkAndGenerateNotification(arrayList);
            }
            sendUnreadCountUpdate();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r3.fetchConversationUpdatesListenerReference.get();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void sendUnreadCountUpdate() {
        /*
            r3 = this;
            java.util.concurrent.atomic.AtomicReference<com.helpshift.common.FetchDataFromThread<java.lang.Integer, java.lang.Integer>> r0 = r3.fetchConversationUpdatesListenerReference
            if (r0 == 0) goto L_0x0018
            java.util.concurrent.atomic.AtomicReference<com.helpshift.common.FetchDataFromThread<java.lang.Integer, java.lang.Integer>> r0 = r3.fetchConversationUpdatesListenerReference
            java.lang.Object r0 = r0.get()
            com.helpshift.common.FetchDataFromThread r0 = (com.helpshift.common.FetchDataFromThread) r0
            if (r0 == 0) goto L_0x0018
            com.helpshift.common.domain.Domain r1 = r3.domain
            com.helpshift.conversation.domainmodel.ConversationController$3 r2 = new com.helpshift.conversation.domainmodel.ConversationController$3
            r2.<init>(r0)
            r1.runOnUI(r2)
        L_0x0018:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.conversation.domainmodel.ConversationController.sendUnreadCountUpdate():void");
    }

    private void markStatusAsResolutionAcceptedIfResolved(Collection<Conversation> collection) {
        for (Conversation next : collection) {
            if (next.state == IssueState.RESOLUTION_REQUESTED && !next.isInPreIssueMode() && !this.sdkConfigurationDM.shouldShowConversationResolutionQuestion()) {
                this.conversationManager.markConversationResolutionStatus(next, true);
            }
        }
    }

    private void clearRequestIdForPendingCreateConversationCall(List<Conversation> list) {
        String pendingRequestId = this.platform.getNetworkRequestDAO().getPendingRequestId(CREATE_ISSUE_ROUTE, CREATE_ISSUE_UNIQUE_MAPPING_KEY);
        String pendingRequestId2 = this.platform.getNetworkRequestDAO().getPendingRequestId(CREATE_PRE_ISSUE_ROUTE, CREATE_PRE_ISSUE_UNIQUE_MAPPING_KEY);
        if (pendingRequestId != null || pendingRequestId2 != null) {
            for (Conversation next : list) {
                if (next.createdRequestId != null) {
                    if (next.createdRequestId.equals(pendingRequestId)) {
                        this.platform.getNetworkRequestDAO().deletePendingRequestId(CREATE_ISSUE_ROUTE, CREATE_ISSUE_UNIQUE_MAPPING_KEY);
                    } else if (next.createdRequestId.equals(pendingRequestId2)) {
                        this.platform.getNetworkRequestDAO().deletePendingRequestId(CREATE_PRE_ISSUE_ROUTE, CREATE_PRE_ISSUE_UNIQUE_MAPPING_KEY);
                    }
                }
            }
        }
    }

    private ViewableConversation getAliveViewableConversation() {
        if (this.aliveViewableConversation == null || this.aliveViewableConversation.get() == null) {
            return null;
        }
        return this.aliveViewableConversation.get();
    }

    private void checkAndGenerateNotification(List<Conversation> list) {
        for (Conversation next : list) {
            if (shouldShowInAppNotification(next)) {
                next.userLocalId = this.userDM.getLocalId().longValue();
                showInAppNotification(next, getMessageCountForShowingInAppNotification(next));
            }
        }
    }

    private void showInAppNotification(Conversation conversation, int i) {
        if (i > 0) {
            showNotificationOnUI(conversation.localId, conversation.localUUID, i, this.platform.getDevice().getAppName(), true);
            updateInAppNotificationCountCache(conversation.localUUID, i);
        }
    }

    private int getInAppNotificationCountCache(String str) {
        Integer num = this.inAppNotificationMessageCountMap.get(str);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    private void updateInAppNotificationCountCache(String str, int i) {
        this.inAppNotificationMessageCountMap.put(str, Integer.valueOf(i));
    }

    private void clearInAppNotificationCountCache() {
        this.inAppNotificationMessageCountMap.clear();
    }

    private boolean shouldShowInAppNotification(Conversation conversation) {
        if (this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.ENABLE_IN_APP_NOTIFICATION)) {
            return canShowNotificationForConversation(conversation);
        }
        return false;
    }

    private int getMessageCountForShowingInAppNotification(Conversation conversation) {
        int inAppNotificationCountCache = getInAppNotificationCountCache(conversation.localUUID);
        int unSeenMessageCount = this.conversationManager.getUnSeenMessageCount(conversation);
        if (unSeenMessageCount > 0 && unSeenMessageCount != inAppNotificationCountCache) {
            return unSeenMessageCount;
        }
        return 0;
    }

    private boolean canShowNotificationForConversation(Conversation conversation) {
        Conversation conversation2;
        if (conversation == null || this.userDM.getLocalId().longValue() != conversation.userLocalId || StringUtils.isEmpty(conversation.localUUID)) {
            return false;
        }
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        if (aliveViewableConversation2 != null && aliveViewableConversation2.isVisibleOnUI()) {
            return false;
        }
        if (aliveViewableConversation2 == null) {
            conversation2 = getActiveConversationFromStorage();
        } else {
            conversation2 = aliveViewableConversation2.getActiveConversation();
        }
        if (conversation2 != null) {
            return conversation.localUUID.equals(conversation2.localUUID);
        }
        return true;
    }

    private void showNotificationOnUI(Long l, String str, int i, String str2, boolean z) {
        if (i > 0) {
            final Long l2 = l;
            final String str3 = str;
            final int i2 = i;
            final String str4 = str2;
            final boolean z2 = z;
            this.domain.runOnUI(new F() {
                public void f() {
                    ConversationController.this.platform.showNotification(l2, str3, i2, str4, z2);
                }
            });
        }
    }

    private void checkForReOpen(List<Conversation> list) {
        boolean z;
        Conversation activeConversationFromUIOrStorage = getActiveConversationFromUIOrStorage();
        String str = null;
        boolean z2 = false;
        if (activeConversationFromUIOrStorage != null) {
            if (!activeConversationFromUIOrStorage.isInPreIssueMode()) {
                str = activeConversationFromUIOrStorage.serverId;
            } else {
                z2 = true;
            }
        }
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        for (Conversation next : list) {
            next.userLocalId = this.userDM.getLocalId().longValue();
            if (aliveViewableConversation2 == null || !aliveViewableConversation2.isActiveConversationEqual(next)) {
                z = this.conversationManager.checkForReOpen(next, this.conversationViewState, str, z2);
            } else {
                z = aliveViewableConversation2.checkForReopen(this.conversationViewState, str, z2);
            }
            if (z && shouldShowInAppNotification(activeConversationFromUIOrStorage)) {
                showInAppNotification(activeConversationFromUIOrStorage, getMessageCountForShowingInAppNotification(activeConversationFromUIOrStorage));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void putConversations(Set<Conversation> set, Set<Conversation> set2, Map<Long, ConversationUpdate> map) {
        for (Conversation conversation : set) {
            conversation.userLocalId = this.userDM.getLocalId().longValue();
        }
        for (Conversation conversation2 : set2) {
            conversation2.userLocalId = this.userDM.getLocalId().longValue();
        }
        this.conversationDAO.updateConversations(new ArrayList(set), map);
        this.conversationDAO.insertConversations(new ArrayList(set2));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v2, resolved type: com.helpshift.conversation.activeconversation.model.Conversation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v21, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v4, resolved type: com.helpshift.conversation.activeconversation.model.Conversation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v6, resolved type: com.helpshift.conversation.activeconversation.model.Conversation} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void merge(java.util.List<com.helpshift.conversation.activeconversation.model.Conversation> r18, java.util.List<com.helpshift.conversation.activeconversation.model.Conversation> r19, java.util.Set<com.helpshift.conversation.activeconversation.model.Conversation> r20, java.util.Set<com.helpshift.conversation.activeconversation.model.Conversation> r21, java.util.Map<java.lang.Long, com.helpshift.conversation.activeconversation.ConversationUpdate> r22) {
        /*
            r17 = this;
            r0 = r17
            r1 = r20
            r2 = r21
            r3 = r22
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.util.Iterator r8 = r18.iterator()
        L_0x0020:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x006e
            java.lang.Object r9 = r8.next()
            com.helpshift.conversation.activeconversation.model.Conversation r9 = (com.helpshift.conversation.activeconversation.model.Conversation) r9
            com.helpshift.account.domainmodel.UserDM r10 = r0.userDM
            java.lang.Long r10 = r10.getLocalId()
            long r10 = r10.longValue()
            r9.userLocalId = r10
            java.lang.String r10 = r9.serverId
            boolean r10 = com.helpshift.common.StringUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x0046
            java.lang.String r10 = r9.serverId
            r5.put(r10, r9)
            goto L_0x0020
        L_0x0046:
            java.lang.String r10 = r9.preConversationServerId
            boolean r10 = com.helpshift.common.StringUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x0054
            java.lang.String r10 = r9.preConversationServerId
            r6.put(r10, r9)
            goto L_0x0020
        L_0x0054:
            boolean r10 = r9.isInPreIssueMode()
            if (r10 == 0) goto L_0x0020
            com.helpshift.common.platform.Platform r10 = r0.platform
            com.helpshift.common.platform.network.NetworkRequestDAO r10 = r10.getNetworkRequestDAO()
            java.lang.String r11 = "/preissues/"
            java.lang.String r12 = "preissue_default_unique_key"
            java.lang.String r10 = r10.getPendingRequestId(r11, r12)
            if (r10 == 0) goto L_0x0020
            r7.put(r10, r9)
            goto L_0x0020
        L_0x006e:
            r8 = 0
        L_0x006f:
            int r9 = r19.size()
            r10 = 1
            if (r8 >= r9) goto L_0x015e
            r9 = r19
            java.lang.Object r11 = r9.get(r8)
            com.helpshift.conversation.activeconversation.model.Conversation r11 = (com.helpshift.conversation.activeconversation.model.Conversation) r11
            com.helpshift.account.domainmodel.UserDM r12 = r0.userDM
            java.lang.Long r12 = r12.getLocalId()
            long r12 = r12.longValue()
            r11.userLocalId = r12
            java.lang.String r12 = r11.serverId
            java.lang.String r13 = r11.preConversationServerId
            java.lang.String r14 = r11.createdRequestId
            r15 = 0
            boolean r16 = r5.containsKey(r12)
            if (r16 == 0) goto L_0x009f
            java.lang.Object r12 = r5.get(r12)
            r15 = r12
            com.helpshift.conversation.activeconversation.model.Conversation r15 = (com.helpshift.conversation.activeconversation.model.Conversation) r15
            goto L_0x00db
        L_0x009f:
            boolean r12 = r6.containsKey(r13)
            if (r12 == 0) goto L_0x00ad
            java.lang.Object r12 = r6.get(r13)
            r15 = r12
            com.helpshift.conversation.activeconversation.model.Conversation r15 = (com.helpshift.conversation.activeconversation.model.Conversation) r15
            goto L_0x00db
        L_0x00ad:
            boolean r12 = com.helpshift.common.StringUtils.isEmpty(r14)
            if (r12 != 0) goto L_0x00db
            boolean r12 = r11.isInPreIssueMode()
            if (r12 == 0) goto L_0x00db
            boolean r12 = r7.containsKey(r14)
            if (r12 == 0) goto L_0x00db
            java.lang.Object r12 = r7.get(r14)
            r15 = r12
            com.helpshift.conversation.activeconversation.model.Conversation r15 = (com.helpshift.conversation.activeconversation.model.Conversation) r15
            if (r15 == 0) goto L_0x00db
            java.lang.String r12 = r15.preConversationServerId
            boolean r12 = com.helpshift.common.StringUtils.isEmpty(r12)
            if (r12 == 0) goto L_0x00db
            com.helpshift.conversation.dao.ConversationDAO r12 = r0.conversationDAO
            java.lang.Long r13 = r15.localId
            long r13 = r13.longValue()
            r12.deleteMessagesForConversation(r13)
        L_0x00db:
            if (r15 == 0) goto L_0x0112
            com.helpshift.account.domainmodel.UserDM r10 = r0.userDM
            java.lang.Long r10 = r10.getLocalId()
            long r12 = r10.longValue()
            r15.userLocalId = r12
            java.lang.Long r10 = r15.localId
            boolean r10 = r3.containsKey(r10)
            if (r10 == 0) goto L_0x00fa
            java.lang.Long r10 = r15.localId
            java.lang.Object r10 = r3.get(r10)
            com.helpshift.conversation.activeconversation.ConversationUpdate r10 = (com.helpshift.conversation.activeconversation.ConversationUpdate) r10
            goto L_0x00ff
        L_0x00fa:
            com.helpshift.conversation.activeconversation.ConversationUpdate r10 = new com.helpshift.conversation.activeconversation.ConversationUpdate
            r10.<init>()
        L_0x00ff:
            boolean r12 = r11.isInPreIssueMode()
            if (r12 == 0) goto L_0x0109
            r0.mergePreIssue(r15, r11, r1, r10)
            goto L_0x010c
        L_0x0109:
            r0.mergeIssue(r15, r11, r1, r10)
        L_0x010c:
            java.lang.Long r11 = r15.localId
            r3.put(r11, r10)
            goto L_0x015a
        L_0x0112:
            boolean r12 = r11.isInPreIssueMode()
            if (r12 == 0) goto L_0x0128
            long r12 = java.lang.System.currentTimeMillis()
            r11.lastUserActivityTime = r12
            com.helpshift.conversation.dto.IssueState r12 = r11.state
            com.helpshift.conversation.dto.IssueState r13 = com.helpshift.conversation.dto.IssueState.RESOLUTION_REQUESTED
            if (r12 != r13) goto L_0x0128
            com.helpshift.conversation.dto.IssueState r12 = com.helpshift.conversation.dto.IssueState.RESOLUTION_ACCEPTED
            r11.state = r12
        L_0x0128:
            com.helpshift.conversation.dto.IssueState r12 = r11.state
            if (r12 == 0) goto L_0x0145
            com.helpshift.conversation.dto.IssueState r13 = com.helpshift.conversation.dto.IssueState.RESOLUTION_ACCEPTED
            if (r12 == r13) goto L_0x013c
            com.helpshift.conversation.dto.IssueState r13 = com.helpshift.conversation.dto.IssueState.RESOLUTION_REJECTED
            if (r12 == r13) goto L_0x013c
            com.helpshift.conversation.dto.IssueState r13 = com.helpshift.conversation.dto.IssueState.REJECTED
            if (r12 == r13) goto L_0x013c
            com.helpshift.conversation.dto.IssueState r13 = com.helpshift.conversation.dto.IssueState.ARCHIVED
            if (r12 != r13) goto L_0x0145
        L_0x013c:
            int r13 = r19.size()
            int r13 = r13 - r10
            if (r8 == r13) goto L_0x0145
            r11.isStartNewConversationClicked = r10
        L_0x0145:
            if (r12 == 0) goto L_0x0157
            boolean r12 = r11.isRedacted
            if (r12 == 0) goto L_0x0157
            com.helpshift.conversation.dto.IssueState r12 = r11.state
            com.helpshift.conversation.dto.IssueState r13 = com.helpshift.conversation.dto.IssueState.RESOLUTION_REQUESTED
            if (r12 != r13) goto L_0x0157
            r11.isStartNewConversationClicked = r10
            com.helpshift.conversation.dto.IssueState r10 = com.helpshift.conversation.dto.IssueState.RESOLUTION_ACCEPTED
            r11.state = r10
        L_0x0157:
            r4.add(r11)
        L_0x015a:
            int r8 = r8 + 1
            goto L_0x006f
        L_0x015e:
            int r1 = r4.size()
            if (r1 > r10) goto L_0x0168
            r2.addAll(r4)
            return
        L_0x0168:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r4)
            int r3 = r1.size()
            int r3 = r3 - r10
        L_0x0172:
            if (r3 < 0) goto L_0x01b7
            java.lang.Object r5 = r1.get(r3)
            com.helpshift.conversation.activeconversation.model.Conversation r5 = (com.helpshift.conversation.activeconversation.model.Conversation) r5
            boolean r6 = r5.isInPreIssueMode()
            if (r6 != 0) goto L_0x01b4
            int r6 = r3 + -1
        L_0x0182:
            if (r6 < 0) goto L_0x01b4
            java.lang.Object r7 = r1.get(r6)
            com.helpshift.conversation.activeconversation.model.Conversation r7 = (com.helpshift.conversation.activeconversation.model.Conversation) r7
            java.lang.String r8 = r5.preConversationServerId
            boolean r8 = com.helpshift.common.StringUtils.isEmpty(r8)
            if (r8 != 0) goto L_0x01b1
            java.lang.String r8 = r5.preConversationServerId
            java.lang.String r9 = r7.preConversationServerId
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x01b1
            java.lang.String r8 = r5.serverId
            java.lang.String r9 = r7.serverId
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x01b1
            com.helpshift.common.util.HSObservableList<com.helpshift.conversation.activeconversation.message.MessageDM> r5 = r5.messageDMs
            com.helpshift.common.util.HSObservableList<com.helpshift.conversation.activeconversation.message.MessageDM> r7 = r7.messageDMs
            r5.addAll(r7)
            r4.remove(r6)
            goto L_0x01b4
        L_0x01b1:
            int r6 = r6 + -1
            goto L_0x0182
        L_0x01b4:
            int r3 = r3 + -1
            goto L_0x0172
        L_0x01b7:
            r2.addAll(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.conversation.domainmodel.ConversationController.merge(java.util.List, java.util.List, java.util.Set, java.util.Set, java.util.Map):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r9v2 */
    /* JADX WARN: Type inference failed for: r0v5, types: [com.helpshift.conversation.activeconversation.ViewableSingleConversation] */
    /* JADX WARN: Type inference failed for: r0v6, types: [com.helpshift.conversation.activeconversation.ViewableConversationHistory] */
    /* JADX WARN: Type inference failed for: r0v7, types: [com.helpshift.conversation.activeconversation.ViewableConversationHistory] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.helpshift.conversation.activeconversation.ViewableConversation getViewableConversation(boolean r8, java.lang.Long r9) {
        /*
            r7 = this;
            r0 = 0
            if (r8 == 0) goto L_0x0049
            com.helpshift.conversation.activeconversation.ViewableConversation r8 = r7.getAliveViewableConversation()
            if (r8 == 0) goto L_0x0015
            com.helpshift.conversation.activeconversation.ViewableConversation$ConversationType r9 = r8.getType()
            com.helpshift.conversation.activeconversation.ViewableConversation$ConversationType r1 = com.helpshift.conversation.activeconversation.ViewableConversation.ConversationType.SINGLE
            if (r9 != r1) goto L_0x0015
            r7.removeInMemoryConversation()
            r8 = r0
        L_0x0015:
            if (r8 != 0) goto L_0x007f
            com.helpshift.conversation.loaders.ConversationHistoryLoader r8 = new com.helpshift.conversation.loaders.ConversationHistoryLoader
            com.helpshift.common.platform.Platform r1 = r7.platform
            com.helpshift.account.domainmodel.UserDM r2 = r7.userDM
            com.helpshift.conversation.loaders.RemoteConversationLoader r3 = r7.remoteConversationLoader
            r4 = 100
            r0 = r8
            r0.<init>(r1, r2, r3, r4)
            com.helpshift.conversation.activeconversation.ViewableConversationHistory r9 = new com.helpshift.conversation.activeconversation.ViewableConversationHistory
            com.helpshift.common.platform.Platform r1 = r7.platform
            com.helpshift.common.domain.Domain r2 = r7.domain
            com.helpshift.account.domainmodel.UserDM r3 = r7.userDM
            com.helpshift.conversation.activeconversation.ConversationManager r5 = r7.conversationManager
            r0 = r9
            r4 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            r9.init()
            java.util.List r8 = r9.getAllConversations()
            boolean r8 = com.helpshift.common.ListUtils.isEmpty(r8)
            if (r8 == 0) goto L_0x007e
            com.helpshift.conversation.activeconversation.model.Conversation r8 = r7.createLocalPreIssueConversation()
            r9.onNewConversationStarted(r8)
            goto L_0x007e
        L_0x0049:
            com.helpshift.conversation.activeconversation.ViewableConversation r8 = r7.getAliveViewableConversation(r9)
            if (r8 == 0) goto L_0x005b
            com.helpshift.conversation.activeconversation.ViewableConversation$ConversationType r1 = r8.getType()
            com.helpshift.conversation.activeconversation.ViewableConversation$ConversationType r2 = com.helpshift.conversation.activeconversation.ViewableConversation.ConversationType.HISTORY
            if (r1 != r2) goto L_0x005b
            r7.removeInMemoryConversation()
            r8 = r0
        L_0x005b:
            if (r8 != 0) goto L_0x007f
            com.helpshift.conversation.loaders.SingleConversationLoader r8 = new com.helpshift.conversation.loaders.SingleConversationLoader
            com.helpshift.common.platform.Platform r1 = r7.platform
            com.helpshift.account.domainmodel.UserDM r2 = r7.userDM
            com.helpshift.conversation.loaders.RemoteConversationLoader r4 = r7.remoteConversationLoader
            r5 = 100
            r0 = r8
            r3 = r9
            r0.<init>(r1, r2, r3, r4, r5)
            com.helpshift.conversation.activeconversation.ViewableSingleConversation r9 = new com.helpshift.conversation.activeconversation.ViewableSingleConversation
            com.helpshift.common.platform.Platform r1 = r7.platform
            com.helpshift.common.domain.Domain r2 = r7.domain
            com.helpshift.account.domainmodel.UserDM r3 = r7.userDM
            com.helpshift.conversation.activeconversation.ConversationManager r5 = r7.conversationManager
            r0 = r9
            r4 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            r9.init()
        L_0x007e:
            r8 = r9
        L_0x007f:
            com.helpshift.conversation.activeconversation.LiveUpdateDM r9 = r7.liveUpdateDM
            r8.setLiveUpdateDM(r9)
            r7.setAliveConversation(r8)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.helpshift.conversation.domainmodel.ConversationController.getViewableConversation(boolean, java.lang.Long):com.helpshift.conversation.activeconversation.ViewableConversation");
    }

    /* access modifiers changed from: private */
    public ViewableConversation getAliveViewableConversation(Long l) {
        if (this.aliveViewableConversation == null || this.aliveViewableConversation.get() == null) {
            return null;
        }
        ViewableConversation viewableConversation = this.aliveViewableConversation.get();
        if (l.equals(viewableConversation.getActiveConversation().localId)) {
            return viewableConversation;
        }
        return null;
    }

    public boolean shouldOpenConversationFromNotification(long j) {
        Conversation readConversationWithoutMessages;
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation(Long.valueOf(j));
        if ((aliveViewableConversation2 != null && aliveViewableConversation2.getActiveConversation() != null) || (readConversationWithoutMessages = this.conversationDAO.readConversationWithoutMessages(Long.valueOf(j))) == null) {
            return aliveViewableConversation2 != null && aliveViewableConversation2.shouldOpen();
        }
        readConversationWithoutMessages.userLocalId = this.userDM.getLocalId().longValue();
        return this.conversationManager.shouldOpen(readConversationWithoutMessages);
    }

    public Conversation getActiveConversationOrPreIssue() {
        Conversation activeConversationFromStorage = getActiveConversationFromStorage();
        return (activeConversationFromStorage != null || !this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.CONVERSATIONAL_ISSUE_FILING)) ? activeConversationFromStorage : createLocalPreIssueConversation();
    }

    public Conversation getActiveConversationFromStorage() {
        if (!this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.DISABLE_IN_APP_CONVERSATION)) {
            List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
            ArrayList arrayList = new ArrayList();
            for (Conversation next : readConversationsWithoutMessages) {
                next.userLocalId = this.userDM.getLocalId().longValue();
                if (this.conversationManager.shouldOpen(next)) {
                    arrayList.add(next);
                }
            }
            if (arrayList.size() > 0) {
                return ConversationUtil.getLastConversationBasedOnCreatedAt(arrayList);
            }
        }
        return null;
    }

    public Conversation getOpenConversationWithMessages() {
        List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
        ArrayList arrayList = new ArrayList();
        if (readConversationsWithoutMessages.isEmpty()) {
            return null;
        }
        for (Conversation next : readConversationsWithoutMessages) {
            next.userLocalId = this.userDM.getLocalId().longValue();
            if (next.isIssueInProgress()) {
                arrayList.add(next);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        Conversation lastConversationBasedOnCreatedAt = ConversationUtil.getLastConversationBasedOnCreatedAt(arrayList);
        lastConversationBasedOnCreatedAt.setMessageDMs(this.conversationDAO.readMessages(lastConversationBasedOnCreatedAt.localId.longValue()));
        return lastConversationBasedOnCreatedAt;
    }

    public Conversation createLocalPreIssueConversation() {
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        String str = (String) currentAdjustedTimeForStorage.first;
        long longValue = ((Long) currentAdjustedTimeForStorage.second).longValue();
        Conversation conversation = new Conversation("Pre Issue Conversation", IssueState.NEW, str, longValue, str, null, null, false, IssueType.PRE_ISSUE);
        conversation.userLocalId = this.userDM.getLocalId().longValue();
        conversation.lastUserActivityTime = System.currentTimeMillis();
        this.conversationDAO.insertPreIssueConversation(conversation);
        String string = this.sdkConfigurationDM.getString(SDKConfigurationDM.CONVERSATION_GREETING_MESSAGE);
        if (!StringUtils.isEmpty(string)) {
            AdminMessageDM adminMessageDM = new AdminMessageDM(null, string, str, longValue, "");
            adminMessageDM.conversationLocalId = conversation.localId;
            adminMessageDM.deliveryState = 1;
            adminMessageDM.setDependencies(this.domain, this.platform);
            this.conversationDAO.insertOrUpdateMessage(adminMessageDM);
            conversation.messageDMs.add(adminMessageDM);
        }
        return conversation;
    }

    public boolean isActiveConversationActionable() {
        boolean z;
        boolean z2;
        ConversationVMCallback conversationVMCallback;
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        Conversation activeConversation = aliveViewableConversation2 != null ? aliveViewableConversation2.getActiveConversation() : null;
        if (activeConversation == null) {
            activeConversation = getActiveConversationFromStorage();
        }
        if (activeConversation == null || !this.conversationManager.isSynced(activeConversation)) {
            return false;
        }
        if ((activeConversation.isInPreIssueMode() && !StringUtils.isEmpty(activeConversation.preConversationServerId) && activeConversation.isIssueInProgress()) || activeConversation.isIssueInProgress() || activeConversation.state == IssueState.RESOLUTION_REQUESTED) {
            return true;
        }
        if (activeConversation.state == IssueState.RESOLUTION_REJECTED) {
            if (aliveViewableConversation2 == null || (conversationVMCallback = aliveViewableConversation2.getConversationVMCallback()) == null) {
                z2 = false;
                z = false;
            } else {
                z = conversationVMCallback.isMessageBoxVisible();
                z2 = true;
            }
            if (z2) {
                return z;
            }
            boolean persistMessageBox = this.conversationInboxDAO.getPersistMessageBox(this.userDM.getLocalId().longValue());
            String userReplyDraft = this.conversationInboxDAO.getUserReplyDraft(this.userDM.getLocalId().longValue());
            if (persistMessageBox || !StringUtils.isEmpty(userReplyDraft)) {
                return true;
            }
        }
        return false;
    }

    public boolean shouldPersistMessageBox() {
        return this.conversationInboxDAO.getPersistMessageBox(this.userDM.getLocalId().longValue());
    }

    public void setPersistMessageBox(boolean z) {
        this.conversationInboxDAO.savePersistMessageBox(this.userDM.getLocalId().longValue(), z);
    }

    public void setConversationViewState(int i) {
        this.conversationViewState = i;
    }

    public void handlePushNotification(String str, String str2, String str3) {
        Conversation conversation;
        String str4;
        int i;
        if (IssueType.ISSUE.equals(str)) {
            conversation = this.conversationDAO.readConversationWithoutMessages(str2);
        } else if (IssueType.PRE_ISSUE.equals(str)) {
            conversation = this.conversationDAO.readPreConversationWithoutMessages(str2);
        } else {
            HSLogger.e(TAG, "Cannot handle push for unknown issue type. " + str);
            return;
        }
        if (conversation != null) {
            if (StringUtils.isEmpty(str3)) {
                str3 = this.platform.getDevice().getAppName();
            }
            String str5 = str3;
            PushNotificationData pushNotificationData = this.conversationInboxDAO.getPushNotificationData(conversation.localUUID);
            if (pushNotificationData == null) {
                str4 = str5;
                i = 1;
            } else {
                str4 = pushNotificationData.title;
                i = pushNotificationData.count + 1;
            }
            this.conversationInboxDAO.setPushNotificationData(conversation.localUUID, new PushNotificationData(i, str4));
            if (i > 0 && canShowNotificationForConversation(conversation)) {
                showNotificationOnUI(conversation.localId, conversation.localUUID, i, str5, false);
            }
            sendUnreadCountUpdate();
        }
    }

    public void resetPushNotificationCount(Conversation conversation) {
        this.conversationInboxDAO.setPushNotificationData(conversation.localUUID, null);
        this.domain.getDelegate().didReceiveNotification(0);
    }

    public void clearPushNotifications() {
        for (Conversation clearNotification : this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue())) {
            clearNotification(clearNotification);
        }
    }

    public void showPushNotifications() {
        for (Conversation next : this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue())) {
            PushNotificationData pushNotificationData = this.conversationInboxDAO.getPushNotificationData(next.localUUID);
            if (pushNotificationData != null && pushNotificationData.count > 0) {
                showNotificationOnUI(next.localId, next.localUUID, pushNotificationData.count, pushNotificationData.title, false);
            }
        }
    }

    public void clearNotification(final Conversation conversation) {
        this.domain.runOnUI(new F() {
            public void f() {
                ConversationController.this.platform.clearNotifications(conversation.localUUID);
            }
        });
        clearInAppNotificationCountCache();
    }

    public int getNotificationCountSync() {
        Conversation activeConversationFromUIOrStorage;
        int i = 0;
        if (this.userCanReadMessages || (activeConversationFromUIOrStorage = getActiveConversationFromUIOrStorage()) == null) {
            return 0;
        }
        int unSeenMessageCount = this.conversationManager.getUnSeenMessageCount(activeConversationFromUIOrStorage);
        PushNotificationData pushNotificationData = this.conversationInboxDAO.getPushNotificationData(activeConversationFromUIOrStorage.localUUID);
        if (pushNotificationData != null) {
            i = pushNotificationData.count;
        }
        return Math.max(unSeenMessageCount, i);
    }

    public ValuePair<Integer, Boolean> fetchConversationsAndGetNotificationCount() {
        if (this.userDM == null || !this.userDM.issueExists()) {
            return new ValuePair<>(-1, true);
        }
        if (this.userCanReadMessages) {
            return new ValuePair<>(0, true);
        }
        List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
        if (ListUtils.isEmpty(readConversationsWithoutMessages)) {
            return new ValuePair<>(0, true);
        }
        if (System.currentTimeMillis() - this.lastNotifCountFetchTime < (ConversationUtil.shouldPollActivelyForConversations(readConversationsWithoutMessages) ? ACTIVE_ISSUE_NOTIFICATION_COUNT_TIMEOUT : INACTIVE_ISSUES_NOTIFICATION_COUNT_TIMEOUT)) {
            return new ValuePair<>(Integer.valueOf(getNotificationCountSync()), true);
        }
        this.lastNotifCountFetchTime = System.currentTimeMillis();
        fetchConversationUpdates();
        Conversation activeConversationFromUIOrStorage = getActiveConversationFromUIOrStorage();
        return new ValuePair<>(Integer.valueOf(activeConversationFromUIOrStorage != null ? this.conversationManager.getUnSeenMessageCount(activeConversationFromUIOrStorage) : 0), false);
    }

    private Conversation getActiveConversationFromUIOrStorage() {
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        if (aliveViewableConversation2 != null) {
            return aliveViewableConversation2.getActiveConversation();
        }
        Conversation activeConversationFromStorage = getActiveConversationFromStorage();
        if (activeConversationFromStorage == null) {
            return null;
        }
        activeConversationFromStorage.userLocalId = this.userDM.getLocalId().longValue();
        return activeConversationFromStorage;
    }

    private Conversation getLastViewableSyncedConversation() {
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        if (aliveViewableConversation2 == null) {
            return getLastViewableSyncedConversationFromStorage();
        }
        Conversation activeConversation = aliveViewableConversation2.getActiveConversation();
        if (this.conversationManager.isSynced(activeConversation)) {
            return activeConversation;
        }
        return getLastViewableSyncedConversationFromStorage();
    }

    private Conversation getLastViewableSyncedConversationFromStorage() {
        List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
        if (readConversationsWithoutMessages.isEmpty()) {
            return null;
        }
        List filter = Filters.filter(readConversationsWithoutMessages, ConversationPredicates.newSyncedConversationPredicate(this.conversationManager));
        List filter2 = Filters.filter(filter, ConversationPredicates.newInProgressConversationPredicate());
        if (ListUtils.isEmpty(filter)) {
            return null;
        }
        if (filter2.isEmpty()) {
            return ConversationUtil.getLastConversationBasedOnCreatedAt(filter);
        }
        return ConversationUtil.getLastConversationBasedOnCreatedAt(filter2);
    }

    public void deleteCachedFilesForResolvedConversations() {
        this.domain.runParallel(new F() {
            public void f() {
                for (Conversation next : ConversationController.this.conversationDAO.readConversationsWithoutMessages(ConversationController.this.userDM.getLocalId().longValue())) {
                    next.userLocalId = ConversationController.this.userDM.getLocalId().longValue();
                    if (!ConversationController.this.conversationManager.shouldOpen(next)) {
                        ConversationController.this.conversationManager.deleteCachedScreenshotFiles(next);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void resetPreIssueConversationsForUser(final UserDM userDM2) {
        HSLogger.d(TAG, "Starting preissues reset.");
        List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(userDM2.getLocalId().longValue());
        if (readConversationsWithoutMessages != null && readConversationsWithoutMessages.size() != 0) {
            long preissueResetInterval = this.sdkConfigurationDM.getPreissueResetInterval() * 1000;
            for (final Conversation next : readConversationsWithoutMessages) {
                if (next.isInPreIssueMode()) {
                    if (System.currentTimeMillis() - next.lastUserActivityTime >= preissueResetInterval) {
                        if (StringUtils.isEmpty(next.preConversationServerId) && StringUtils.isEmpty(next.serverId)) {
                            HSLogger.d(TAG, "Deleting offline preissue : " + next.localId);
                            this.conversationDAO.deleteConversation(next.localId.longValue());
                            removeInMemoryConversation();
                        } else if (next.isIssueInProgress() || next.state == IssueState.UNKNOWN) {
                            clearNotification(next);
                            this.domain.runParallel(new F() {
                                public void f() {
                                    Conversation conversation;
                                    try {
                                        HSLogger.d(ConversationController.TAG, "Reseting preissue on backend: " + next.preConversationServerId);
                                        HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM2);
                                        userRequestData.put("state", String.valueOf(IssueState.REJECTED.getValue()));
                                        new GuardOKNetwork(new TSCorrectedNetwork(new PUTNetwork(ConversationController.CREATE_PRE_ISSUE_ROUTE + next.preConversationServerId + Constants.URL_PATH_DELIMITER, ConversationController.this.domain, ConversationController.this.platform), ConversationController.this.platform)).makeRequest(new RequestData(userRequestData));
                                        ViewableConversation access$000 = ConversationController.this.getAliveViewableConversation(next.localId);
                                        if (access$000 == null) {
                                            conversation = next;
                                        } else {
                                            conversation = access$000.getActiveConversation();
                                        }
                                        ConversationController.this.conversationManager.updateIssueStatus(conversation, IssueState.REJECTED);
                                    } catch (RootAPIException e) {
                                        HSLogger.e(ConversationController.TAG, "Error resetting preissue : " + next.preConversationServerId, e);
                                        throw e;
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
    }

    private void mergePreIssue(Conversation conversation, Conversation conversation2, Set<Conversation> set, ConversationUpdate conversationUpdate) {
        boolean z;
        boolean z2;
        String str = conversation2.preConversationServerId;
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        if (aliveViewableConversation2 != null) {
            Conversation findMatchingPreIssueTypeConversation = findMatchingPreIssueTypeConversation(aliveViewableConversation2, str);
            if (findMatchingPreIssueTypeConversation != null) {
                conversation = findMatchingPreIssueTypeConversation;
            }
            z2 = str.equals(aliveViewableConversation2.getActiveConversation().preConversationServerId);
            z = aliveViewableConversation2.isVisibleOnUI();
        } else {
            z2 = false;
            z = false;
        }
        this.conversationManager.sendConversationPostedEvent(conversation, conversation2);
        if (StringUtils.isEmpty(conversation.preConversationServerId) && conversation.isInPreIssueMode() && !StringUtils.isEmpty(str)) {
            if (z2) {
                aliveViewableConversation2.handlePreIssueCreationSuccess();
            } else {
                this.conversationManager.handlePreIssueCreationSuccess(conversation);
            }
        }
        IssueState issueState = conversation.state;
        if (conversation.isInPreIssueMode()) {
            if (z2) {
                aliveViewableConversation2.mergePreIssueForActiveConversation(conversation2, conversationUpdate);
            } else {
                this.conversationManager.mergePreIssue(conversation, conversation2, false, conversationUpdate);
            }
        } else if (!ListUtils.isEmpty(conversation2.messageDMs)) {
            this.conversationManager.updateMessageDMs(conversation, z2, conversation2.messageDMs, conversationUpdate);
        }
        if (!z) {
            this.conversationManager.checkAndIncrementMessageCount(conversation, issueState);
        }
        this.conversationManager.sendConversationEndedDelegateForPreIssue(conversation);
        set.add(conversation);
    }

    @Nullable
    private Conversation findMatchingPreIssueTypeConversation(ViewableConversation viewableConversation, String str) {
        for (Conversation next : viewableConversation.getAllConversations()) {
            if (!StringUtils.isEmpty(str) && str.equals(next.preConversationServerId)) {
                return next;
            }
        }
        return null;
    }

    @Nullable
    private Conversation findMatchingIssueTypeConversation(ViewableConversation viewableConversation, String str) {
        for (Conversation next : viewableConversation.getAllConversations()) {
            if (!StringUtils.isEmpty(str) && str.equals(next.serverId)) {
                return next;
            }
        }
        return null;
    }

    private void mergeIssue(Conversation conversation, Conversation conversation2, Set<Conversation> set, ConversationUpdate conversationUpdate) {
        boolean z;
        boolean z2;
        Conversation activeConversationFromStorage;
        ViewableConversation aliveViewableConversation2 = getAliveViewableConversation();
        if (aliveViewableConversation2 != null) {
            Conversation findMatchingIssueTypeConversation = findMatchingIssueTypeConversation(aliveViewableConversation2, conversation2.serverId);
            if (findMatchingIssueTypeConversation != null) {
                conversation = findMatchingIssueTypeConversation;
            }
            z2 = conversation2.serverId.equals(aliveViewableConversation2.getActiveConversation().serverId);
            z = aliveViewableConversation2.isVisibleOnUI();
        } else {
            z2 = false;
            z = false;
        }
        IssueState issueState = conversation.state;
        if (z2) {
            aliveViewableConversation2.mergeIssueForActiveConversation(conversation2, conversationUpdate);
        } else {
            this.conversationManager.mergeIssue(conversation, conversation2, false, conversationUpdate);
        }
        if ((aliveViewableConversation2 == null || !aliveViewableConversation2.isVisibleOnUI()) && conversation.state == IssueState.REJECTED && (activeConversationFromStorage = getActiveConversationFromStorage()) != null && activeConversationFromStorage.localId.equals(conversation.localId)) {
            this.conversationManager.handleConversationEnded(conversation);
        }
        if (!z) {
            this.conversationManager.checkAndIncrementMessageCount(conversation, issueState);
        }
        set.add(conversation);
    }

    private void fetchConversationHistory() {
        synchronized (fetchConversationUpdatesLock) {
            this.remoteConversationLoader.loadMoreMessages();
        }
    }

    public void executeUserSync() {
        fetchInitialConversationUpdates();
        List<Conversation> readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
        if (!isAtLeastOneConversationNonActionable(readConversationsWithoutMessages)) {
            int i = 0;
            boolean hasMoreMessage = this.remoteConversationLoader.hasMoreMessage();
            while (!isAtLeastOneConversationNonActionable(readConversationsWithoutMessages) && hasMoreMessage && i < 3) {
                fetchConversationHistory();
                readConversationsWithoutMessages = this.conversationDAO.readConversationsWithoutMessages(this.userDM.getLocalId().longValue());
                hasMoreMessage = this.remoteConversationLoader.hasMoreMessage();
                i++;
            }
        }
    }

    private boolean isAtLeastOneConversationNonActionable(List<Conversation> list) {
        if (ListUtils.isEmpty(list)) {
            return false;
        }
        for (Conversation next : list) {
            next.userLocalId = this.userDM.getLocalId().longValue();
            if (!next.isIssueInProgress()) {
                return true;
            }
        }
        return false;
    }

    public Long getOldestConversationCreatedAtTime() {
        return this.conversationDAO.getOldestConversationCreatedAtTime(this.userDM.getLocalId().longValue());
    }

    public void redactConversations() {
        synchronized (fetchConversationUpdatesLock) {
            deleteConversationsAndMessages();
            if (this.aliveViewableConversation != null) {
                this.aliveViewableConversation.clear();
            }
            this.conversationInboxDAO.resetDataAfterConversationsDeletion(this.userDM.getLocalId().longValue());
        }
    }

    private class CreateConversationStateHolder {
        final String description;
        final ImagePickerFile imagePickerFile;
        private final F startNewConversationInternalF = new One(new F() {
            public void f() {
                ConversationController.this.startNewConversationInternal(CreateConversationStateHolder.this.description, CreateConversationStateHolder.this.userProvidedName, CreateConversationStateHolder.this.userProvidedEmail, CreateConversationStateHolder.this.imagePickerFile);
            }
        });
        final String userProvidedEmail;
        final String userProvidedName;

        CreateConversationStateHolder(String str, String str2, String str3, ImagePickerFile imagePickerFile2) {
            this.description = str;
            this.userProvidedName = str2;
            this.userProvidedEmail = str3;
            this.imagePickerFile = imagePickerFile2;
        }

        /* access modifiers changed from: package-private */
        public F getStartNewConversationInternalF() {
            return this.startNewConversationInternalF;
        }
    }
}
