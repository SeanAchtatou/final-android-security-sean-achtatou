package com.helpshift.conversation.activeconversation;

import com.helpshift.auth.dto.WebSocketAuthData;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.platform.Device;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.websockets.HSWebSocket;
import com.helpshift.common.platform.network.websockets.IHSWebSocketListener;
import com.helpshift.conversation.dto.WSPingMessage;
import com.helpshift.conversation.dto.WSTypingActionMessage;
import com.helpshift.conversation.dto.WebSocketMessage;
import com.helpshift.network.HttpStatus;
import com.helpshift.util.HSLogger;
import com.helpshift.websockets.WebSocketExtension;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class LiveUpdateDM implements IHSWebSocketListener {
    public static final int MESSAGE_TYPE_EVENT = 100;
    public static final int MESSAGE_TYPE_PING = 107;
    private static final String TAG = "Helpshift_LiveUpdateDM";
    private final String MESSAGE_TYPE_PONG = "[110]";
    final long NETWORK_PROPOGATION_DELAY = TimeUnit.SECONDS.toMillis(3);
    private final String SDK_VERSION_HEADER_KEY = "hs-sdk-ver";
    private String conversationServerId;
    F disconnectWebSocketF = new F() {
        public void f() {
            if (LiveUpdateDM.this.webSocket == null) {
                return;
            }
            if (LiveUpdateDM.this.isConnecting) {
                LiveUpdateDM.this.shouldDisconnectOnConnect = true;
                return;
            }
            try {
                HSLogger.d(LiveUpdateDM.TAG, "Disconnecting web-socket");
                LiveUpdateDM.this.webSocket.disconnect();
            } catch (Exception e) {
                HSLogger.e(LiveUpdateDM.TAG, "Exception in disconnecting web-socket", e);
            }
            LiveUpdateDM.this.webSocket = null;
        }
    };
    Domain domain;
    boolean isAgentTyping;
    boolean isConnecting;
    boolean isTokenRefreshed;
    TypingIndicatorListener listener;
    AtomicInteger pingCount;
    Platform platform;
    private F refreshAuthTokenAndConnectF = new F() {
        public void f() {
            if (LiveUpdateDM.this.listener != null) {
                LiveUpdateDM.this.domain.getWebSocketAuthDM().refreshAuthToken();
                LiveUpdateDM.this.isTokenRefreshed = true;
                new ConnectWebSocketF(LiveUpdateDM.this.pingCount.incrementAndGet()).f();
            }
        }
    };
    final String sdkVersionHeaderValue;
    boolean shouldDisconnectOnConnect;
    boolean socketConnected;
    AtomicInteger startTypingId;
    HSWebSocket webSocket;

    interface TypingIndicatorListener {
        void onAgentTypingUpdate(boolean z);
    }

    public LiveUpdateDM(Domain domain2, Platform platform2) {
        this.domain = domain2;
        this.platform = platform2;
        this.pingCount = new AtomicInteger(-1);
        this.startTypingId = new AtomicInteger(-1);
        Device device = platform2.getDevice();
        this.sdkVersionHeaderValue = device.getPlatformName().toLowerCase() + "-" + device.getSDKVersion();
    }

    /* access modifiers changed from: package-private */
    public boolean isAgentTyping() {
        return this.isAgentTyping;
    }

    /* access modifiers changed from: package-private */
    public synchronized void registerListener(TypingIndicatorListener typingIndicatorListener, String str) {
        if (this.listener == null) {
            this.listener = typingIndicatorListener;
            this.conversationServerId = str;
            this.isTokenRefreshed = false;
            this.shouldDisconnectOnConnect = false;
            this.domain.runParallel(new ConnectWebSocketF(this.pingCount.incrementAndGet()));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void unregisterListener() {
        if (this.listener != null) {
            this.isAgentTyping = false;
            notifyListener();
            this.startTypingId.incrementAndGet();
            this.pingCount.incrementAndGet();
            this.listener = null;
        }
        this.domain.runParallel(this.disconnectWebSocketF);
    }

    public void onConnected(HSWebSocket hSWebSocket) {
        HSLogger.d(TAG, "web-socket connected");
        this.isConnecting = false;
        this.socketConnected = true;
        if (this.shouldDisconnectOnConnect) {
            this.disconnectWebSocketF.f();
        } else if (this.listener != null) {
            HSLogger.d(TAG, "Subscribing to conversation topic");
            hSWebSocket.sendMessage(getTopicRequest());
            this.domain.runDelayedInParallel(new PingTimeoutF(this.pingCount.incrementAndGet()), TimeUnit.SECONDS.toMillis(60));
        } else {
            this.disconnectWebSocketF.f();
        }
    }

    public void onDisconnected() {
        HSLogger.d(TAG, "web-socket disconnected");
        this.socketConnected = false;
        this.shouldDisconnectOnConnect = false;
    }

    public void onMessage(HSWebSocket hSWebSocket, String str) {
        this.domain.runParallel(new HandleWebSocketMessageF(str));
    }

    public void onError(HSWebSocket hSWebSocket, String str) {
        HSLogger.d(TAG, "Error in web-socket connection: " + str);
        this.isConnecting = false;
        if (this.listener == null) {
            return;
        }
        if (getErrorCode(str) != 403) {
            scheduleConnectionRetry();
        } else if (!this.isTokenRefreshed) {
            this.domain.runParallel(this.refreshAuthTokenAndConnectF);
        }
    }

    private int getErrorCode(String str) {
        String[] split = str.split("The status line is: ");
        if (2 != split.length) {
            return -1;
        }
        String[] split2 = split[1].split(" +");
        if (split2.length < 2 || !"403".equals(split2[1])) {
            return -1;
        }
        return HttpStatus.SC_FORBIDDEN;
    }

    private String getTopicRequest() {
        return "[104, [\"" + "agent_type_act.issue." + this.conversationServerId + "\"]]";
    }

    /* access modifiers changed from: package-private */
    public String getWebSocketPath(WebSocketAuthData webSocketAuthData) {
        String appId = this.platform.getAppId();
        String[] split = this.platform.getDomain().split("\\.");
        String str = "";
        if (split.length == 3) {
            str = split[0];
        }
        String str2 = "";
        try {
            str2 = URLEncoder.encode(webSocketAuthData.authToken, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            HSLogger.e(TAG, "Exception in encoding authToken", e);
        }
        if (StringUtils.isEmpty(str2) || StringUtils.isEmpty(webSocketAuthData.webSocketRoute)) {
            return null;
        }
        return webSocketAuthData.webSocketRoute + "/subscribe/websocket/?origin_v3=" + str2 + "&platform_id=" + appId + "&domain=" + str;
    }

    /* access modifiers changed from: package-private */
    public void scheduleConnectionRetry() {
        this.domain.runDelayedInParallel(new ConnectWebSocketF(this.pingCount.incrementAndGet()), TimeUnit.SECONDS.toMillis(10));
    }

    /* access modifiers changed from: package-private */
    public void notifyListener() {
        if (this.listener != null) {
            this.listener.onAgentTypingUpdate(this.isAgentTyping);
        }
    }

    private class ConnectWebSocketF extends F {
        private final int id;

        ConnectWebSocketF(int i) {
            this.id = i;
        }

        public void f() {
            if (LiveUpdateDM.this.listener != null && this.id == LiveUpdateDM.this.pingCount.get() && !LiveUpdateDM.this.socketConnected && !LiveUpdateDM.this.isConnecting) {
                WebSocketAuthData authToken = LiveUpdateDM.this.domain.getWebSocketAuthDM().getAuthToken();
                if (authToken == null) {
                    LiveUpdateDM.this.scheduleConnectionRetry();
                    return;
                }
                HSLogger.d(LiveUpdateDM.TAG, "Connecting web-socket");
                try {
                    LiveUpdateDM.this.webSocket = new HSWebSocket.Builder(LiveUpdateDM.this.getWebSocketPath(authToken)).setConnectionTimeout((int) TimeUnit.SECONDS.toMillis(60)).addExtension(WebSocketExtension.PERMESSAGE_DEFLATE).addExtension("client_no_context_takeover").addExtension("server_no_context_takeover").addProtocol("dirigent-pubsub-v1").addHeader("hs-sdk-ver", LiveUpdateDM.this.sdkVersionHeaderValue).setWebSocketListener(LiveUpdateDM.this).build();
                    LiveUpdateDM.this.isConnecting = true;
                    LiveUpdateDM.this.webSocket.connect();
                } catch (Exception e) {
                    HSLogger.e(LiveUpdateDM.TAG, "Exception in connecting web-socket", e);
                    LiveUpdateDM.this.scheduleConnectionRetry();
                }
            }
        }
    }

    private class StartTypingTimeoutF extends F {
        int id;

        StartTypingTimeoutF(int i) {
            this.id = i;
        }

        public void f() {
            if (this.id == LiveUpdateDM.this.startTypingId.get() && LiveUpdateDM.this.listener != null) {
                HSLogger.d(LiveUpdateDM.TAG, "Start Typing action timed out, disabling TAI");
                LiveUpdateDM.this.isAgentTyping = false;
                LiveUpdateDM.this.notifyListener();
            }
        }
    }

    private class PingTimeoutF extends F {
        int pingId;

        PingTimeoutF(int i) {
            this.pingId = i;
        }

        public void f() {
            if (this.pingId == LiveUpdateDM.this.pingCount.get() && LiveUpdateDM.this.listener != null) {
                HSLogger.d(LiveUpdateDM.TAG, "Ping timed out, resetting connection");
                LiveUpdateDM.this.disconnectWebSocketF.f();
                new ConnectWebSocketF(LiveUpdateDM.this.pingCount.incrementAndGet()).f();
            }
        }
    }

    private class HandleWebSocketMessageF extends F {
        private final String message;

        HandleWebSocketMessageF(String str) {
            this.message = str;
        }

        public void f() {
            WebSocketMessage parseWebSocketMessage = LiveUpdateDM.this.platform.getResponseParser().parseWebSocketMessage(this.message);
            if (parseWebSocketMessage instanceof WSPingMessage) {
                LiveUpdateDM.this.domain.runDelayedInParallel(new PingTimeoutF(LiveUpdateDM.this.pingCount.incrementAndGet()), ((WSPingMessage) parseWebSocketMessage).pingWaitTimeMillis + LiveUpdateDM.this.NETWORK_PROPOGATION_DELAY);
                if (LiveUpdateDM.this.webSocket != null) {
                    LiveUpdateDM.this.webSocket.sendMessage("[110]");
                }
            } else if (LiveUpdateDM.this.listener != null && (parseWebSocketMessage instanceof WSTypingActionMessage)) {
                WSTypingActionMessage wSTypingActionMessage = (WSTypingActionMessage) parseWebSocketMessage;
                if (wSTypingActionMessage.isAgentTyping) {
                    LiveUpdateDM.this.isAgentTyping = true;
                    LiveUpdateDM.this.domain.runDelayedInParallel(new StartTypingTimeoutF(LiveUpdateDM.this.startTypingId.incrementAndGet()), wSTypingActionMessage.typingActionTimeoutMillis + LiveUpdateDM.this.NETWORK_PROPOGATION_DELAY);
                } else {
                    LiveUpdateDM.this.isAgentTyping = false;
                }
                LiveUpdateDM.this.notifyListener();
            }
        }
    }
}
