package com.helpshift.conversation.activeconversation.message;

import com.helpshift.common.platform.network.Response;
import com.helpshift.common.util.HSDateFormatSpec;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UserResponseMessageForTextInputDM extends UserMessageDM {
    public String botInfo;
    public long dateInMillis;
    public final boolean isMessageEmpty;
    public int keyboard;
    private String referredMessageId;
    public boolean skipped;
    public String timeZoneId;

    public UserResponseMessageForTextInputDM(String str, String str2, long j, String str3, int i, String str4, boolean z, String str5, boolean z2) {
        super(str, str2, j, str3, MessageType.USER_RESP_FOR_TEXT_INPUT);
        this.keyboard = i;
        this.botInfo = str4;
        this.skipped = z;
        this.referredMessageId = str5;
        this.isMessageEmpty = z2;
    }

    public UserResponseMessageForTextInputDM(String str, String str2, long j, String str3, AdminMessageWithTextInputDM adminMessageWithTextInputDM, boolean z) {
        super(str, str2, j, str3, MessageType.USER_RESP_FOR_TEXT_INPUT);
        this.keyboard = adminMessageWithTextInputDM.input.keyboard;
        this.botInfo = adminMessageWithTextInputDM.input.botInfo;
        this.skipped = z;
        this.referredMessageId = adminMessageWithTextInputDM.serverId;
        this.isMessageEmpty = adminMessageWithTextInputDM.isMessageEmpty;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof UserResponseMessageForTextInputDM) {
            UserResponseMessageForTextInputDM userResponseMessageForTextInputDM = (UserResponseMessageForTextInputDM) messageDM;
            this.keyboard = userResponseMessageForTextInputDM.keyboard;
            this.botInfo = userResponseMessageForTextInputDM.botInfo;
            this.skipped = userResponseMessageForTextInputDM.skipped;
            this.referredMessageId = userResponseMessageForTextInputDM.referredMessageId;
            this.dateInMillis = userResponseMessageForTextInputDM.dateInMillis;
            this.timeZoneId = userResponseMessageForTextInputDM.timeZoneId;
        }
    }

    /* access modifiers changed from: protected */
    public String getMessageTypeForRequest() {
        switch (this.keyboard) {
            case 1:
                return this.isMessageEmpty ? "rsp_empty_msg_with_txt_input" : "rsp_txt_msg_with_txt_input";
            case 2:
                return "rsp_txt_msg_with_email_input";
            case 3:
                return "rsp_txt_msg_with_numeric_input";
            case 4:
                return "rsp_txt_msg_with_dt_input";
            default:
                return super.getMessageTypeForRequest();
        }
    }

    public String getReferredMessageId() {
        return this.referredMessageId;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> getData() throws ParseException {
        HashMap hashMap = new HashMap();
        hashMap.put("chatbot_info", this.botInfo);
        hashMap.put(TJAdUnitConstants.String.VIDEO_SKIPPED, String.valueOf(this.skipped));
        if (this.keyboard == 4 && !this.skipped) {
            Date parse = HSDateFormatSpec.getDateFormatter(HSDateFormatSpec.DISPLAY_DATE_PATTERN, this.domain.getLocaleProviderDM().getCurrentLocale()).parse(this.body.trim());
            HashMap hashMap2 = new HashMap();
            this.dateInMillis = parse.getTime();
            this.timeZoneId = this.platform.getDevice().getTimeZoneId();
            hashMap2.put("dt", Long.valueOf(this.dateInMillis));
            hashMap2.put(TapjoyConstants.TJC_DEVICE_TIMEZONE, this.timeZoneId);
            hashMap.put("message_meta", this.platform.getJsonifier().jsonify(hashMap2));
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public UserMessageDM parseResponse(Response response) {
        return this.platform.getResponseParser().parseResponseMessageForTextInput(response.responseString);
    }
}
