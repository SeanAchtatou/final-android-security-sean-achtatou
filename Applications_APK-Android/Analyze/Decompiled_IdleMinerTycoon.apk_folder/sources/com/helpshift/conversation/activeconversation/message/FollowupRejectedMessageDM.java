package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;

public class FollowupRejectedMessageDM extends AutoRetriableMessageDM {
    public static final int REASON_CONVERSATION_FILING = 1;
    public static final int REASON_MESSAGE_FILING = 3;
    public static final int REASON_OPEN_ISSUE = 2;
    public static final int REASON_OPEN_PRE_ISSUE = 4;
    public String openConversationId;
    public int reason;
    public String referredMessageId;

    public boolean isUISupportedMessage() {
        return false;
    }

    public FollowupRejectedMessageDM(String str, String str2, long j, String str3, String str4, int i) {
        super(str, str2, j, str3, false, MessageType.FOLLOWUP_REJECTED, i);
        this.referredMessageId = str4;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof FollowupRejectedMessageDM) {
            this.referredMessageId = ((FollowupRejectedMessageDM) messageDM).referredMessageId;
        }
    }

    public void send(UserDM userDM, ConversationServerInfo conversationServerInfo) {
        if (!StringUtils.isEmpty(conversationServerInfo.getIssueId())) {
            HashMap hashMap = new HashMap();
            hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, Integer.valueOf(this.reason));
            if (this.openConversationId != null) {
                hashMap.put("open_issue_id", String.valueOf(this.openConversationId));
            }
            String jsonify = this.platform.getJsonifier().jsonify(hashMap);
            HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM);
            userRequestData.put("body", "Rejected the follow-up");
            userRequestData.put("type", "rj");
            userRequestData.put("refers", this.referredMessageId);
            userRequestData.put("message_meta", jsonify);
            try {
                FollowupRejectedMessageDM parseFollowupRejectedMessage = this.platform.getResponseParser().parseFollowupRejectedMessage(makeNetworkRequest(getIssueSendMessageRoute(conversationServerInfo), userRequestData).responseString);
                merge(parseFollowupRejectedMessage);
                this.authorId = parseFollowupRejectedMessage.authorId;
                this.serverId = parseFollowupRejectedMessage.serverId;
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("FollowupRejectedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
