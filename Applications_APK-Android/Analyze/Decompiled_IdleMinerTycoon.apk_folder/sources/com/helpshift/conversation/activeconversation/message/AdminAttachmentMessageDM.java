package com.helpshift.conversation.activeconversation.message;

import com.appsflyer.share.Constants;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.network.AuthDataProvider;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.util.FileUtil;
import com.helpshift.conversation.viewmodel.ConversationVMCallback;
import com.helpshift.downloader.AdminFileInfo;
import com.helpshift.downloader.SupportDownloadStateChangeListener;
import com.helpshift.downloader.SupportDownloader;

public class AdminAttachmentMessageDM extends AttachmentMessageDM {
    int downloadProgress = 0;
    public AdminGenericAttachmentState state;

    public enum AdminGenericAttachmentState {
        DOWNLOAD_NOT_STARTED,
        DOWNLOADING,
        DOWNLOADED
    }

    public boolean isUISupportedMessage() {
        return true;
    }

    public AdminAttachmentMessageDM(String str, String str2, String str3, long j, String str4, int i, String str5, String str6, String str7, boolean z) {
        super(str2, str3, j, str4, i, str5, str6, str7, true, z, MessageType.ADMIN_ATTACHMENT);
        this.serverId = str;
        updateState();
    }

    public void setDependencies(Domain domain, Platform platform) {
        super.setDependencies(domain, platform);
        if (isValidUriPath(this.filePath)) {
            updateState();
        }
    }

    public void updateState() {
        if (checkAndGetFilePath() != null) {
            this.state = AdminGenericAttachmentState.DOWNLOADED;
        } else {
            this.state = AdminGenericAttachmentState.DOWNLOAD_NOT_STARTED;
        }
    }

    public String getDownloadProgressAndFileSize() {
        String downloadedProgressSize = getDownloadedProgressSize();
        if (StringUtils.isEmpty(downloadedProgressSize)) {
            return getFormattedFileSize();
        }
        return downloadedProgressSize + Constants.URL_PATH_DELIMITER + getFormattedFileSize();
    }

    public String getDownloadedProgressSize() {
        if (this.state == AdminGenericAttachmentState.DOWNLOADING && this.downloadProgress > 0) {
            double d = (double) (this.size * this.downloadProgress);
            Double.isNaN(d);
            double d2 = d / 100.0d;
            if (d2 < ((double) this.size)) {
                return getFormattedFileSize(d2);
            }
        }
        return null;
    }

    public boolean isWriteStoragePermissionRequired() {
        return this.state == AdminGenericAttachmentState.DOWNLOAD_NOT_STARTED;
    }

    /* access modifiers changed from: package-private */
    public void setState(AdminGenericAttachmentState adminGenericAttachmentState) {
        this.state = adminGenericAttachmentState;
        notifyUpdated();
    }

    public void handleClick(ConversationVMCallback conversationVMCallback) {
        if (this.state == AdminGenericAttachmentState.DOWNLOADED) {
            if (conversationVMCallback != null) {
                conversationVMCallback.launchAttachment(checkAndGetFilePath(), this.contentType);
            }
        } else if (this.state == AdminGenericAttachmentState.DOWNLOAD_NOT_STARTED) {
            setState(AdminGenericAttachmentState.DOWNLOADING);
            this.platform.getDownloader().startDownload(new AdminFileInfo(this.attachmentUrl, this.fileName, this.contentType, this.isSecureAttachment), SupportDownloader.StorageDirType.EXTERNAL_ONLY, new AuthDataProvider(this.domain, this.platform, this.attachmentUrl), new SupportDownloadStateChangeListener() {
                public void onFailure(String str) {
                    AdminAttachmentMessageDM.this.setState(AdminGenericAttachmentState.DOWNLOAD_NOT_STARTED);
                }

                public void onSuccess(String str, String str2) {
                    String unused = AdminAttachmentMessageDM.this.filePath = str2;
                    AdminAttachmentMessageDM.this.platform.getConversationDAO().insertOrUpdateMessage(AdminAttachmentMessageDM.this);
                    AdminAttachmentMessageDM.this.setState(AdminGenericAttachmentState.DOWNLOADED);
                }

                public void onProgressChange(String str, int i) {
                    AdminAttachmentMessageDM.this.downloadProgress = i;
                    AdminAttachmentMessageDM.this.notifyUpdated();
                }
            });
        }
    }

    public String checkAndGetFilePath() {
        if (isValidUriPath(this.filePath)) {
            if (this.platform != null && !this.platform.canReadFileAtUri(this.filePath)) {
                this.filePath = null;
                this.state = AdminGenericAttachmentState.DOWNLOAD_NOT_STARTED;
            }
        } else if (!FileUtil.doesFilePathExistAndCanRead(this.filePath)) {
            this.filePath = null;
            this.state = AdminGenericAttachmentState.DOWNLOAD_NOT_STARTED;
        }
        return this.filePath;
    }
}
