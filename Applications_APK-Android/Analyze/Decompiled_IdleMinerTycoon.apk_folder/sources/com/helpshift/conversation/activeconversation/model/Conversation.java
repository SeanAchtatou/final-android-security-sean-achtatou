package com.helpshift.conversation.activeconversation.model;

import com.helpshift.common.StringUtils;
import com.helpshift.common.util.HSObservableList;
import com.helpshift.conversation.ConversationUtil;
import com.helpshift.conversation.IssueType;
import com.helpshift.conversation.activeconversation.ConversationDMListener;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.RequestForReopenMessageDM;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.conversation.states.ConversationCSATState;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class Conversation implements Observer, ConversationServerInfo {
    public ConversationDMListener conversationDMListener;
    public String createdAt;
    public String createdRequestId;
    public String csatFeedback;
    public int csatRating;
    public ConversationCSATState csatState = ConversationCSATState.NONE;
    public boolean enableMessageClickOnResolutionRejected;
    public long epochCreatedAtTime;
    public boolean isAutoFilledPreIssue;
    public boolean isConversationEndedDelegateSent;
    public boolean isInBetweenBotExecution;
    public boolean isRedacted;
    public boolean isStartNewConversationClicked;
    public String issueType;
    public long lastUserActivityTime;
    public Long localId;
    public String localUUID;
    public String messageCursor;
    public HSObservableList<MessageDM> messageDMs = new HSObservableList<>();
    public String preConversationServerId;
    public String publishId;
    public String serverId;
    public boolean shouldIncrementMessageCount;
    public boolean showAgentName;
    public IssueState state;
    public String title;
    public final Map<String, RequestForReopenMessageDM> unansweredRequestForReopenMessageDMs = new HashMap();
    public String updatedAt;
    public long userLocalId;
    public boolean wasFullPrivacyEnabledAtCreation;

    public Conversation(String str, IssueState issueState, String str2, long j, String str3, String str4, String str5, boolean z, String str6) {
        this.title = str;
        this.createdAt = str2;
        this.epochCreatedAtTime = j;
        this.updatedAt = str3;
        this.publishId = str4;
        this.messageCursor = str5;
        this.showAgentName = z;
        this.state = issueState;
        this.issueType = str6;
    }

    public void setLocalId(long j) {
        this.localId = Long.valueOf(j);
        Iterator<E> it = this.messageDMs.iterator();
        while (it.hasNext()) {
            ((MessageDM) it.next()).conversationLocalId = this.localId;
        }
    }

    public long getEpochCreatedAtTime() {
        return this.epochCreatedAtTime;
    }

    public void setEpochCreatedAtTime(long j) {
        this.epochCreatedAtTime = j;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String str) {
        if (!StringUtils.isEmpty(str)) {
            this.createdAt = str;
        }
    }

    public boolean isInPreIssueMode() {
        return IssueType.PRE_ISSUE.equals(this.issueType);
    }

    public String getIssueId() {
        return this.serverId;
    }

    public String getPreIssueId() {
        return this.preConversationServerId;
    }

    public void setMessageDMs(List<MessageDM> list) {
        this.messageDMs = new HSObservableList<>(list);
        updateStateBasedOnMessages();
    }

    public void update(Observable observable, Object obj) {
        if (observable instanceof MessageDM) {
            MessageDM messageDM = (MessageDM) observable;
            this.messageDMs.setAndNotifyObserver(this.messageDMs.indexOf(messageDM), messageDM);
        }
    }

    private void updateStateBasedOnMessages() {
        if (this.state == IssueState.RESOLUTION_REQUESTED && this.messageDMs != null && this.messageDMs.size() > 0) {
            MessageDM messageDM = null;
            for (int size = this.messageDMs.size() - 1; size >= 0; size--) {
                messageDM = (MessageDM) this.messageDMs.get(size);
                if (!(messageDM instanceof FollowupRejectedMessageDM) && !(messageDM instanceof RequestForReopenMessageDM)) {
                    break;
                }
            }
            if (messageDM instanceof ConfirmationAcceptedMessageDM) {
                this.state = IssueState.RESOLUTION_ACCEPTED;
            } else if (messageDM instanceof ConfirmationRejectedMessageDM) {
                this.state = IssueState.RESOLUTION_REJECTED;
            }
        }
    }

    public boolean isIssueInProgress() {
        return ConversationUtil.isInProgressState(this.state);
    }

    public void setListener(ConversationDMListener conversationDMListener2) {
        this.conversationDMListener = conversationDMListener2;
    }

    public void registerMessagesObserver() {
        Iterator<E> it = this.messageDMs.iterator();
        while (it.hasNext()) {
            ((MessageDM) it.next()).addObserver(this);
        }
    }

    public boolean isLocalPreIssue() {
        return StringUtils.isEmpty(this.preConversationServerId) && StringUtils.isEmpty(this.serverId);
    }
}
