package com.helpshift.conversation.activeconversation.message;

import com.helpshift.common.platform.Platform;

public class RequestScreenshotMessageDM extends MessageDM {
    public boolean isAnswered;
    private boolean isAttachmentButtonClickable = true;

    public boolean isUISupportedMessage() {
        return true;
    }

    public RequestScreenshotMessageDM(String str, String str2, String str3, long j, String str4, boolean z) {
        super(str2, str3, j, str4, true, MessageType.REQUESTED_SCREENSHOT);
        this.serverId = str;
        this.isAnswered = z;
    }

    public boolean isAttachmentButtonClickable() {
        return !this.isAnswered && this.isAttachmentButtonClickable;
    }

    public void setAttachmentButtonClickable(boolean z) {
        this.isAttachmentButtonClickable = z;
        notifyUpdated();
    }

    public void setIsAnswered(Platform platform, boolean z) {
        this.isAnswered = z;
        platform.getConversationDAO().insertOrUpdateMessage(this);
        notifyUpdated();
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof RequestScreenshotMessageDM) {
            this.isAnswered = ((RequestScreenshotMessageDM) messageDM).isAnswered;
        }
    }
}
