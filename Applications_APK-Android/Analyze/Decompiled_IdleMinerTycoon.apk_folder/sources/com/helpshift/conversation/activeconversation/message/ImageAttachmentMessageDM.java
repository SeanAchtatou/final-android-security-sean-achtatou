package com.helpshift.conversation.activeconversation.message;

public abstract class ImageAttachmentMessageDM extends AttachmentMessageDM {
    public String thumbnailFilePath;
    public String thumbnailUrl;

    ImageAttachmentMessageDM(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, int i, boolean z, boolean z2, MessageType messageType) {
        super(str, str2, j, str3, i, str7, str4, str5, z, z2, messageType);
        this.thumbnailUrl = str6;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof ImageAttachmentMessageDM) {
            this.thumbnailUrl = ((ImageAttachmentMessageDM) messageDM).thumbnailUrl;
        }
    }
}
