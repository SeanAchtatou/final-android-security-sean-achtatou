package com.helpshift.conversation.activeconversation.message;

public class UnsupportedAdminMessageWithInputDM extends AdminMessageDM {
    public String botInfo;
    public String input;
    public String type;

    public boolean isUISupportedMessage() {
        return false;
    }

    public UnsupportedAdminMessageWithInputDM(String str, String str2, String str3, long j, String str4, String str5, String str6, String str7) {
        super(str, str2, str3, j, str4, MessageType.UNSUPPORTED_ADMIN_MESSAGE_WITH_INPUT);
        this.type = str5;
        this.botInfo = str6;
        this.input = str7;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof UnsupportedAdminMessageWithInputDM) {
            UnsupportedAdminMessageWithInputDM unsupportedAdminMessageWithInputDM = (UnsupportedAdminMessageWithInputDM) messageDM;
            this.type = unsupportedAdminMessageWithInputDM.type;
            this.botInfo = unsupportedAdminMessageWithInputDM.botInfo;
            this.input = unsupportedAdminMessageWithInputDM.input;
        }
    }
}
