package com.helpshift.conversation.activeconversation.message;

import com.helpshift.common.domain.Domain;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.activeconversation.message.input.TextInput;

public class AdminMessageWithTextInputDM extends AdminMessageDM {
    public TextInput input;
    public final boolean isMessageEmpty;

    public AdminMessageWithTextInputDM(String str, String str2, String str3, long j, String str4, String str5, String str6, boolean z, String str7, String str8, int i, boolean z2) {
        super(str, str2, str3, j, str4, MessageType.ADMIN_TEXT_WITH_TEXT_INPUT);
        this.input = new TextInput(str5, z, str7, str8, str6, i);
        this.isMessageEmpty = z2;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof AdminMessageWithTextInputDM) {
            this.input = ((AdminMessageWithTextInputDM) messageDM).input;
        }
    }

    public void setDependencies(Domain domain, Platform platform) {
        super.setDependencies(domain, platform);
        this.input.setDependencies(domain);
    }
}
