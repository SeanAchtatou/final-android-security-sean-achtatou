package com.helpshift.conversation.activeconversation.message;

public class AdminBotControlMessageDM extends AdminMessageDM {
    public String actionType;
    public String botInfo;
    public boolean hasNextBot;

    public boolean isUISupportedMessage() {
        return false;
    }

    public AdminBotControlMessageDM(String str, String str2, String str3, long j, String str4, String str5, String str6) {
        super(str, str2, str3, j, str4, MessageType.ADMIN_BOT_CONTROL);
        this.actionType = str5;
        this.botInfo = str6;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof AdminBotControlMessageDM) {
            AdminBotControlMessageDM adminBotControlMessageDM = (AdminBotControlMessageDM) messageDM;
            this.actionType = adminBotControlMessageDM.actionType;
            this.botInfo = adminBotControlMessageDM.botInfo;
        }
    }
}
