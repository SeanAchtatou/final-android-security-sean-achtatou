package com.helpshift.conversation.activeconversation.message;

import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.util.ValuePair;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;

public class RequestAppReviewMessageDM extends MessageDM {
    public boolean isAnswered;
    public boolean isReviewButtonClickable = true;

    public boolean isUISupportedMessage() {
        return true;
    }

    public RequestAppReviewMessageDM(String str, String str2, String str3, long j, String str4, boolean z) {
        super(str2, str3, j, str4, true, MessageType.REQUESTED_APP_REVIEW);
        this.serverId = str;
        this.isAnswered = z;
    }

    public void setIsReviewButtonClickable(boolean z) {
        this.isReviewButtonClickable = z;
        notifyUpdated();
    }

    public AcceptedAppReviewMessageDM handleRequestReviewClick(Domain domain, Platform platform) {
        if (this.isAnswered) {
            return null;
        }
        setIsReviewButtonClickable(false);
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(platform);
        AcceptedAppReviewMessageDM acceptedAppReviewMessageDM = new AcceptedAppReviewMessageDM("Accepted review request", (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, this.serverId, 1);
        acceptedAppReviewMessageDM.conversationLocalId = this.conversationLocalId;
        acceptedAppReviewMessageDM.setDependencies(domain, platform);
        platform.getConversationDAO().insertOrUpdateMessage(acceptedAppReviewMessageDM);
        HashMap hashMap = new HashMap();
        hashMap.put("type", "conversation");
        domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.REVIEWED_APP, hashMap);
        domain.getDelegate().userRepliedToConversation("User reviewed the app");
        return acceptedAppReviewMessageDM;
    }

    public void handleAcceptedReviewSuccess(Platform platform) {
        this.isReviewButtonClickable = false;
        this.isAnswered = true;
        notifyUpdated();
        platform.getConversationDAO().insertOrUpdateMessage(this);
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof RequestAppReviewMessageDM) {
            this.isAnswered = ((RequestAppReviewMessageDM) messageDM).isAnswered;
        }
    }
}
