package com.helpshift.conversation.activeconversation.message;

import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import java.util.List;

public class AdminMessageWithOptionInputDM extends AdminMessageDM {
    public OptionInput input;

    public AdminMessageWithOptionInputDM(String str, String str2, String str3, long j, String str4, String str5, boolean z, String str6, String str7, List<OptionInput.Option> list, OptionInput.Type type) {
        super(str, str2, str3, j, str4, MessageType.ADMIN_TEXT_WITH_OPTION_INPUT);
        this.input = new OptionInput(str5, z, str6, str7, list, type);
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof AdminMessageWithOptionInputDM) {
            this.input = ((AdminMessageWithOptionInputDM) messageDM).input;
        }
    }
}
