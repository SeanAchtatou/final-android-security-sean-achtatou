package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.AttachmentFileManagerDM;
import com.helpshift.common.domain.network.AuthDataProvider;
import com.helpshift.common.domain.network.AuthenticationFailureNetwork;
import com.helpshift.common.domain.network.GuardAgainstConversationArchivalNetwork;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.IdempotentNetwork;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.domain.network.UploadNetwork;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.util.FileUtil;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import com.helpshift.conversation.viewmodel.ConversationVMCallback;
import com.helpshift.downloader.AdminFileInfo;
import com.helpshift.downloader.SupportDownloadStateChangeListener;
import com.helpshift.downloader.SupportDownloader;
import java.util.HashMap;
import java.util.UUID;

public class ScreenshotMessageDM extends ImageAttachmentMessageDM {
    public String refersMessageId;
    public UserMessageState state;

    public boolean isUISupportedMessage() {
        return true;
    }

    public ScreenshotMessageDM(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, int i, boolean z) {
        super(str, str2, j, str3, str7, str6, str5, str4, i, false, z, MessageType.SCREENSHOT);
    }

    public void setRefersMessageId(String str) {
        if (str == null) {
            str = AttachmentFileManagerDM.LOCAL_RSC_MESSAGE_PREFIX + UUID.randomUUID().toString();
        }
        this.refersMessageId = str;
    }

    public void handleClick(ConversationVMCallback conversationVMCallback) {
        if (this.state == UserMessageState.SENT && conversationVMCallback != null) {
            conversationVMCallback.launchScreenshotAttachment(getFilePath(), this.contentType);
        }
    }

    public void uploadImage(UserDM userDM, ConversationServerInfo conversationServerInfo, boolean z) {
        String issueId = conversationServerInfo.getIssueId();
        if (StringUtils.isEmpty(issueId)) {
            throw new UnsupportedOperationException("ScreenshotMessageDM send called with conversation in pre issue mode.");
        } else if (getFilePath() != null) {
            if (z) {
                this.filePath = this.platform.compressAndStoreScreenshot(getFilePath(), this.refersMessageId);
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
            }
            setState(UserMessageState.SENDING);
            HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM);
            userRequestData.put("body", "Screenshot sent");
            userRequestData.put("type", "sc");
            userRequestData.put("refers", this.refersMessageId);
            userRequestData.put("screenshot", getFilePath());
            userRequestData.put("originalFileName", this.fileName);
            try {
                String issueSendMessageRoute = getIssueSendMessageRoute(conversationServerInfo);
                GuardAgainstConversationArchivalNetwork guardAgainstConversationArchivalNetwork = new GuardAgainstConversationArchivalNetwork(new AuthenticationFailureNetwork(new IdempotentNetwork(new UploadNetwork(issueSendMessageRoute, this.domain, this.platform), this.platform, getIdempotentPolicy(), issueSendMessageRoute, String.valueOf(this.localId))));
                ScreenshotMessageDM parseScreenshotMessageDM = this.platform.getResponseParser().parseScreenshotMessageDM(new GuardOKNetwork(guardAgainstConversationArchivalNetwork).makeRequest(new RequestData(userRequestData)).responseString);
                this.serverId = parseScreenshotMessageDM.serverId;
                this.authorId = parseScreenshotMessageDM.authorId;
                merge(parseScreenshotMessageDM);
                setState(UserMessageState.SENT);
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
                notifyUpdated();
                HashMap hashMap = new HashMap();
                hashMap.put("id", issueId);
                hashMap.put("body", parseScreenshotMessageDM.attachmentUrl);
                hashMap.put("type", "url");
                this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.MESSAGE_ADDED, hashMap);
                this.domain.getDelegate().userRepliedToConversation("User sent a screenshot");
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
                }
                if (StringUtils.isEmpty(this.serverId)) {
                    setState(UserMessageState.UNSENT_RETRYABLE);
                }
                throw RootAPIException.wrap(e);
            }
        }
    }

    public void setState(UserMessageState userMessageState) {
        this.state = userMessageState;
        notifyUpdated();
    }

    public void updateState(boolean z) {
        if (this.serverId != null) {
            setState(UserMessageState.SENT);
        } else if (this.state != UserMessageState.SENDING) {
            if (z) {
                setState(UserMessageState.UNSENT_RETRYABLE);
            } else {
                setState(UserMessageState.UNSENT_NOT_RETRYABLE);
            }
        }
    }

    public String getFilePath() {
        if (!FileUtil.doesFilePathExistAndCanRead(this.filePath)) {
            this.filePath = null;
        }
        return this.filePath;
    }

    public void checkAndReDownloadImageIfNotExist(final Platform platform) {
        if (this.state == UserMessageState.SENT && !FileUtil.doesFilePathExistAndCanRead(getFilePath())) {
            platform.getDownloader().startDownload(new AdminFileInfo(this.attachmentUrl, this.fileName, this.contentType, this.isSecureAttachment), SupportDownloader.StorageDirType.INTERNAL_ONLY, new AuthDataProvider(this.domain, platform, this.attachmentUrl), new SupportDownloadStateChangeListener() {
                public void onFailure(String str) {
                }

                public void onProgressChange(String str, int i) {
                }

                public void onSuccess(String str, String str2) {
                    String unused = ScreenshotMessageDM.this.filePath = str2;
                    platform.getConversationDAO().insertOrUpdateMessage(ScreenshotMessageDM.this);
                    ScreenshotMessageDM.this.notifyUpdated();
                }
            });
        }
    }
}
