package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.platform.network.Response;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class UserMessageDM extends MessageDM {
    private UserMessageState state;

    /* access modifiers changed from: protected */
    public String getMessageTypeForRequest() {
        return "txt";
    }

    public String getReferredMessageId() {
        return "";
    }

    public boolean isUISupportedMessage() {
        return true;
    }

    public UserMessageDM(String str, String str2, long j, String str3) {
        super(str, str2, j, str3, false, MessageType.USER_TEXT);
    }

    public UserMessageDM(String str, String str2, long j, String str3, MessageType messageType) {
        super(str, str2, j, str3, false, messageType);
    }

    /* access modifiers changed from: protected */
    public Map<String, String> getData() throws ParseException {
        return new HashMap();
    }

    public void send(UserDM userDM, ConversationServerInfo conversationServerInfo) {
        String str;
        if (this.state != UserMessageState.SENDING && this.state != UserMessageState.SENT && this.state != UserMessageState.UNSENT_NOT_RETRYABLE) {
            setState(UserMessageState.SENDING);
            if (conversationServerInfo.isInPreIssueMode()) {
                str = getPreIssueSendMessageRoute(conversationServerInfo);
            } else {
                str = getIssueSendMessageRoute(conversationServerInfo);
            }
            try {
                Map<String, String> data = getData();
                data.putAll(NetworkDataRequestUtil.getUserRequestData(userDM));
                data.put("body", this.body);
                data.put("type", getMessageTypeForRequest());
                data.put("refers", getReferredMessageId());
                UserMessageDM parseResponse = parseResponse(getSendMessageNetwork(str).makeRequest(new RequestData(data)));
                this.state = UserMessageState.SENT;
                merge(parseResponse);
                this.serverId = parseResponse.serverId;
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
                this.authorId = parseResponse.authorId;
                notifyUpdated();
                this.domain.getDelegate().userRepliedToConversation(this.body);
                if (!conversationServerInfo.isInPreIssueMode()) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("id", conversationServerInfo.getIssueId());
                    hashMap.put("body", this.body);
                    hashMap.put("type", "txt");
                    this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.MESSAGE_ADDED, hashMap);
                }
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    setStateAsUnsentRetryable();
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
                } else if (!(e.exceptionType == NetworkException.CONVERSATION_ARCHIVED || e.exceptionType == NetworkException.USER_PRE_CONDITION_FAILED)) {
                    setStateAsUnsentRetryable();
                }
                throw RootAPIException.wrap(e);
            } catch (ParseException e2) {
                setStateAsUnsentRetryable();
                throw RootAPIException.wrap(e2);
            } catch (Throwable th) {
                if (!conversationServerInfo.isInPreIssueMode()) {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("id", conversationServerInfo.getIssueId());
                    hashMap2.put("body", this.body);
                    hashMap2.put("type", "txt");
                    this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.MESSAGE_ADDED, hashMap2);
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public UserMessageDM parseResponse(Response response) {
        return this.platform.getResponseParser().parseReadableUserMessage(response.responseString);
    }

    private void setStateAsUnsentRetryable() {
        if (StringUtils.isEmpty(this.serverId)) {
            setState(UserMessageState.UNSENT_RETRYABLE);
        }
    }

    public void updateState(boolean z) {
        if (!StringUtils.isEmpty(this.serverId)) {
            setState(UserMessageState.SENT);
        } else if (this.state != UserMessageState.SENDING) {
            if (z) {
                setState(UserMessageState.UNSENT_RETRYABLE);
            } else {
                setState(UserMessageState.UNSENT_NOT_RETRYABLE);
            }
        }
    }

    public UserMessageState getState() {
        return this.state;
    }

    public void setState(UserMessageState userMessageState) {
        UserMessageState userMessageState2 = this.state;
        this.state = userMessageState;
        if (userMessageState2 != this.state) {
            notifyUpdated();
        }
    }
}
