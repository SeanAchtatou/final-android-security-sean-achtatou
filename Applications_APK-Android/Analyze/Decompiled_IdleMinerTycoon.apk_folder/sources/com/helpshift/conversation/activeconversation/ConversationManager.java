package com.helpshift.conversation.activeconversation;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.analytics.AnalyticsEventType;
import com.helpshift.bots.BotControlActions;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.domain.idempotent.SuccessOrNonRetriableStatusCodeIdempotentPolicy;
import com.helpshift.common.domain.network.AuthenticationFailureNetwork;
import com.helpshift.common.domain.network.FailedAPICallNetworkDecorator;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.IdempotentNetwork;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.domain.network.POSTNetwork;
import com.helpshift.common.domain.network.PUTNetwork;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.exception.ExceptionType;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.common.util.FileUtil;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.ConversationUtil;
import com.helpshift.conversation.activeconversation.message.AcceptedAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminBotControlMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithOptionInputDM;
import com.helpshift.conversation.activeconversation.message.AdminMessageWithTextInputDM;
import com.helpshift.conversation.activeconversation.message.AttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AutoRetriableMessageDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.ConfirmationRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.FAQListMessageDM;
import com.helpshift.conversation.activeconversation.message.FollowupAcceptedMessageDM;
import com.helpshift.conversation.activeconversation.message.FollowupRejectedMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.MessageType;
import com.helpshift.conversation.activeconversation.message.OptionInputMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestAppReviewMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestForReopenMessageDM;
import com.helpshift.conversation.activeconversation.message.RequestScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.message.UnsupportedAdminMessageWithInputDM;
import com.helpshift.conversation.activeconversation.message.UserBotControlMessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageDM;
import com.helpshift.conversation.activeconversation.message.UserMessageState;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForOptionInput;
import com.helpshift.conversation.activeconversation.message.UserResponseMessageForTextInputDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dao.ConversationDAO;
import com.helpshift.conversation.dto.ImagePickerFile;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.conversation.states.ConversationCSATState;
import com.helpshift.conversation.util.predicate.MessagePredicates;
import com.helpshift.util.Filters;
import com.helpshift.util.HSLogger;
import com.helpshift.util.ValuePair;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import im.getsocial.sdk.pushnotifications.NotificationStatus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConversationManager {
    private static final String TAG = "Helpshift_ConvManager";
    private ConversationDAO conversationDAO;
    Domain domain;
    Platform platform;
    private SDKConfigurationDM sdkConfigurationDM;
    UserDM userDM;

    public ConversationManager(Platform platform2, Domain domain2, UserDM userDM2) {
        this.platform = platform2;
        this.domain = domain2;
        this.userDM = userDM2;
        this.conversationDAO = platform2.getConversationDAO();
        this.sdkConfigurationDM = domain2.getSDKConfigurationDM();
    }

    public boolean checkForReOpen(Conversation conversation, int i, String str, boolean z) {
        final Conversation conversation2 = conversation;
        int i2 = i;
        String str2 = str;
        if (conversation2.messageDMs == null || conversation2.messageDMs.size() <= 0) {
            return false;
        }
        MessageDM messageDM = (MessageDM) conversation2.messageDMs.get(conversation2.messageDMs.size() - 1);
        if (!(messageDM instanceof RequestForReopenMessageDM)) {
            return false;
        }
        RequestForReopenMessageDM requestForReopenMessageDM = (RequestForReopenMessageDM) messageDM;
        if (requestForReopenMessageDM.isAnswered()) {
            return false;
        }
        if (i2 == 1) {
            sendReOpenRejectedMessage(conversation2, 1, null, messageDM.serverId);
            return false;
        } else if (z) {
            sendReOpenRejectedMessage(conversation2, 4, null, messageDM.serverId);
            return false;
        } else if (i2 == 2) {
            sendReOpenRejectedMessage(conversation2, 3, null, messageDM.serverId);
            return false;
        } else if (str2 == null || str2.equals(conversation2.serverId)) {
            conversation2.state = IssueState.WAITING_FOR_AGENT;
            conversation2.isConversationEndedDelegateSent = false;
            this.conversationDAO.updateConversationWithoutMessages(conversation2);
            ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
            final FollowupAcceptedMessageDM followupAcceptedMessageDM = new FollowupAcceptedMessageDM(null, (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, messageDM.serverId, 1);
            followupAcceptedMessageDM.conversationLocalId = conversation2.localId;
            followupAcceptedMessageDM.setDependencies(this.domain, this.platform);
            addMessageToDBAndGlobalList(conversation2, followupAcceptedMessageDM);
            requestForReopenMessageDM.setAnsweredAndNotify(true);
            this.conversationDAO.insertOrUpdateMessage(requestForReopenMessageDM);
            sendMessageWithAutoRetry(new F() {
                public void f() {
                    followupAcceptedMessageDM.send(ConversationManager.this.userDM, conversation2);
                }
            });
            return true;
        } else {
            sendReOpenRejectedMessage(conversation2, 2, str2, messageDM.serverId);
            return false;
        }
    }

    private void sendReOpenRejectedMessage(final Conversation conversation, int i, String str, String str2) {
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        final FollowupRejectedMessageDM followupRejectedMessageDM = new FollowupRejectedMessageDM(null, (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, str2, 1);
        followupRejectedMessageDM.reason = i;
        followupRejectedMessageDM.openConversationId = str;
        followupRejectedMessageDM.conversationLocalId = conversation.localId;
        followupRejectedMessageDM.setDependencies(this.domain, this.platform);
        addMessageToDBAndGlobalList(conversation, followupRejectedMessageDM);
        sendMessageWithAutoRetry(new F() {
            public void f() {
                followupRejectedMessageDM.send(ConversationManager.this.userDM, conversation);
            }
        });
    }

    private void deleteOptionsForAdminMessageWithOptionsInput(OptionInputMessageDM optionInputMessageDM) {
        if (optionInputMessageDM.referredMessageType == MessageType.ADMIN_TEXT_WITH_OPTION_INPUT) {
            AdminMessageWithOptionInputDM adminMessageWithOptionInputDM = (AdminMessageWithOptionInputDM) this.conversationDAO.readMessage(optionInputMessageDM.serverId);
            adminMessageWithOptionInputDM.input.options.clear();
            this.conversationDAO.insertOrUpdateMessage(adminMessageWithOptionInputDM);
        }
    }

    private void addMessageToDBAndGlobalList(Conversation conversation, MessageDM messageDM) {
        this.conversationDAO.insertOrUpdateMessage(messageDM);
        messageDM.setDependencies(this.domain, this.platform);
        messageDM.addObserver(conversation);
        conversation.messageDMs.add(messageDM);
    }

    private void sendMessageWithAutoRetry(final F f) {
        this.domain.runParallel(new F() {
            public void f() {
                try {
                    f.f();
                } catch (RootAPIException e) {
                    ExceptionType exceptionType = e.exceptionType;
                    if (exceptionType != NetworkException.NON_RETRIABLE && exceptionType != NetworkException.USER_PRE_CONDITION_FAILED) {
                        ConversationManager.this.domain.getAutoRetryFailedEventDM().scheduleRetryTaskForEventType(AutoRetryFailedEventDM.EventType.CONVERSATION, e.getServerStatusCode());
                        throw e;
                    }
                }
            }
        });
    }

    public void refreshConversationOnIssueStateUpdate(Conversation conversation) {
        switch (conversation.state) {
            case ARCHIVED:
                ArrayList<UserMessageDM> arrayList = new ArrayList<>();
                for (MessageDM next : this.conversationDAO.readMessages(conversation.localId.longValue())) {
                    if ((next instanceof UserMessageDM) && next.serverId == null) {
                        arrayList.add((UserMessageDM) next);
                    }
                }
                StringBuilder sb = new StringBuilder();
                for (UserMessageDM userMessageDM : arrayList) {
                    sb.append(userMessageDM.body);
                    sb.append("\n");
                }
                this.platform.getConversationInboxDAO().saveConversationArchivalPrefillText(this.userDM.getLocalId().longValue(), sb.toString());
                handleConversationEnded(conversation);
                break;
            case RESOLUTION_ACCEPTED:
            case REJECTED:
                handleConversationEnded(conversation);
                break;
        }
        updateMessagesOnIssueStatusUpdate(conversation);
    }

    /* access modifiers changed from: package-private */
    public void updateMessagesOnIssueStatusUpdate(Conversation conversation) {
        boolean shouldEnableMessagesClick = shouldEnableMessagesClick(conversation);
        Iterator<E> it = conversation.messageDMs.iterator();
        while (it.hasNext()) {
            updateMessageOnConversationUpdate((MessageDM) it.next(), shouldEnableMessagesClick);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateMessageOnConversationUpdate(MessageDM messageDM, boolean z) {
        updateMessageClickableState(messageDM, z);
        if (messageDM instanceof ScreenshotMessageDM) {
            ((ScreenshotMessageDM) messageDM).checkAndReDownloadImageIfNotExist(this.platform);
        }
    }

    private void updateMessageClickableState(MessageDM messageDM, boolean z) {
        if (messageDM instanceof UserMessageDM) {
            ((UserMessageDM) messageDM).updateState(z);
        } else if (messageDM instanceof RequestScreenshotMessageDM) {
            ((RequestScreenshotMessageDM) messageDM).setAttachmentButtonClickable(z);
        } else if (messageDM instanceof ScreenshotMessageDM) {
            ((ScreenshotMessageDM) messageDM).updateState(z);
        }
    }

    public void markConversationResolutionStatus(final Conversation conversation, boolean z) {
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        String str = (String) currentAdjustedTimeForStorage.first;
        long longValue = ((Long) currentAdjustedTimeForStorage.second).longValue();
        if (z) {
            final ConfirmationAcceptedMessageDM confirmationAcceptedMessageDM = new ConfirmationAcceptedMessageDM("Accepted the solution", str, longValue, TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, 1);
            confirmationAcceptedMessageDM.setDependencies(this.domain, this.platform);
            confirmationAcceptedMessageDM.conversationLocalId = conversation.localId;
            this.conversationDAO.insertOrUpdateMessage(confirmationAcceptedMessageDM);
            sendMessageWithAutoRetry(new F() {
                public void f() {
                    try {
                        confirmationAcceptedMessageDM.send(ConversationManager.this.userDM, conversation);
                    } catch (RootAPIException e) {
                        if (e.exceptionType == NetworkException.CONVERSATION_ARCHIVED) {
                            ConversationManager.this.updateIssueStatus(conversation, IssueState.ARCHIVED);
                            return;
                        }
                        throw e;
                    }
                }
            });
            updateIssueStatus(conversation, IssueState.RESOLUTION_ACCEPTED);
            this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.RESOLUTION_ACCEPTED, conversation.serverId);
            this.domain.getDelegate().userRepliedToConversation("User accepted the solution");
            return;
        }
        final ConfirmationRejectedMessageDM confirmationRejectedMessageDM = new ConfirmationRejectedMessageDM("Did not accept the solution", str, longValue, TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, 1);
        confirmationRejectedMessageDM.conversationLocalId = conversation.localId;
        addMessageToDbAndUI(conversation, confirmationRejectedMessageDM);
        sendMessageWithAutoRetry(new F() {
            public void f() {
                try {
                    confirmationRejectedMessageDM.send(ConversationManager.this.userDM, conversation);
                } catch (RootAPIException e) {
                    if (e.exceptionType == NetworkException.CONVERSATION_ARCHIVED) {
                        ConversationManager.this.updateIssueStatus(conversation, IssueState.ARCHIVED);
                        return;
                    }
                    throw e;
                }
            }
        });
        updateIssueStatus(conversation, IssueState.RESOLUTION_REJECTED);
        this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.RESOLUTION_REJECTED, conversation.serverId);
        this.domain.getDelegate().userRepliedToConversation("User rejected the solution");
    }

    public void updateIssueStatus(Conversation conversation, IssueState issueState) {
        if (conversation.state != issueState) {
            HSLogger.d(TAG, "Changing conversation status from: " + conversation.state + ", new status: " + issueState + ", for: " + conversation.serverId);
            conversation.state = issueState;
            refreshConversationOnIssueStateUpdate(conversation);
            this.conversationDAO.updateConversationWithoutMessages(conversation);
            if (conversation.conversationDMListener != null) {
                conversation.conversationDMListener.onIssueStatusChange(conversation.state);
            }
        }
    }

    public boolean filterMessagesOlderThanLastMessageInDb(Conversation conversation) {
        String oldestMessageCursor = this.conversationDAO.getOldestMessageCursor(this.userDM.getLocalId().longValue());
        boolean z = false;
        if (!StringUtils.isEmpty(oldestMessageCursor)) {
            List filter = Filters.filter(conversation.messageDMs, MessagePredicates.olderThanLastDbMessagePredicate(HSDateFormatSpec.convertToEpochTime(oldestMessageCursor)));
            int size = conversation.messageDMs.size();
            int size2 = filter.size();
            if (size != 0 && size2 == 0) {
                z = true;
            }
            if (size != size2) {
                conversation.setMessageDMs(filter);
            }
        }
        return z;
    }

    public void mergeIssue(Conversation conversation, Conversation conversation2, boolean z, ConversationUpdate conversationUpdate) {
        IssueState issueState = conversation2.state;
        IssueState issueState2 = conversation.state;
        if (AnonymousClass12.$SwitchMap$com$helpshift$conversation$dto$IssueState[issueState.ordinal()] == 4 && (conversation.state == IssueState.RESOLUTION_ACCEPTED || conversation.state == IssueState.RESOLUTION_REJECTED)) {
            issueState = issueState2;
        }
        String str = conversation2.messageCursor;
        if (str != null) {
            conversation.messageCursor = str;
        }
        conversation.serverId = conversation2.serverId;
        conversation.preConversationServerId = conversation2.preConversationServerId;
        conversation.issueType = conversation2.issueType;
        conversation.title = conversation2.title;
        conversation.showAgentName = conversation2.showAgentName;
        conversation.publishId = conversation2.publishId;
        conversation.createdAt = conversation2.createdAt;
        conversation.epochCreatedAtTime = conversation2.getEpochCreatedAtTime();
        conversation.isRedacted = conversation2.isRedacted;
        conversation.updatedAt = conversation2.updatedAt;
        if (conversation2.csatState == ConversationCSATState.SUBMITTED_SYNCED) {
            conversation.csatState = conversation2.csatState;
        }
        conversation.state = issueState;
        updateMessageDMs(conversation, z, conversation2.messageDMs, conversationUpdate);
    }

    private void addMessageToDbAndUI(Conversation conversation, MessageDM messageDM) {
        this.conversationDAO.insertOrUpdateMessage(messageDM);
        addMessageToUI(conversation, messageDM);
    }

    /* access modifiers changed from: package-private */
    public void addMessageToUI(Conversation conversation, MessageDM messageDM) {
        messageDM.setDependencies(this.domain, this.platform);
        if (messageDM.isUISupportedMessage()) {
            messageDM.addObserver(conversation);
            conversation.messageDMs.add(messageDM);
            ConversationUtil.sortMessagesBasedOnCreatedAt(conversation.messageDMs);
        }
    }

    public void updateMessageDMs(Conversation conversation, boolean z, List<MessageDM> list, ConversationUpdate conversationUpdate) {
        if (conversationUpdate == null) {
            conversationUpdate = new ConversationUpdate();
        }
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        populateMessageDMLookup(conversation, hashMap, hashMap2);
        ArrayList<MessageDM> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        for (MessageDM next : list) {
            MessageDM messageDMForUpdate = getMessageDMForUpdate(next, hashMap, hashMap2, conversationUpdate);
            if (messageDMForUpdate != null) {
                if (messageDMForUpdate instanceof UserMessageDM) {
                    messageDMForUpdate.merge(next);
                    ((UserMessageDM) messageDMForUpdate).setState(UserMessageState.SENT);
                } else if (messageDMForUpdate instanceof ScreenshotMessageDM) {
                    messageDMForUpdate.merge(next);
                    ((ScreenshotMessageDM) messageDMForUpdate).setState(UserMessageState.SENT);
                    if (messageDMForUpdate.isRedacted) {
                        arrayList2.add((AttachmentMessageDM) messageDMForUpdate);
                    }
                } else if (messageDMForUpdate instanceof AttachmentMessageDM) {
                    messageDMForUpdate.mergeAndNotify(next);
                    if (messageDMForUpdate.isRedacted) {
                        arrayList2.add((AttachmentMessageDM) messageDMForUpdate);
                    }
                } else {
                    messageDMForUpdate.mergeAndNotify(next);
                }
                conversationUpdate.updatedMessageDMs.add(messageDMForUpdate);
            } else {
                arrayList.add(next);
            }
        }
        clearRedactedAttachmentsResources(arrayList2);
        if (!ListUtils.isEmpty(arrayList)) {
            for (MessageDM messageDM : arrayList) {
                messageDM.setDependencies(this.domain, this.platform);
                messageDM.conversationLocalId = conversation.localId;
                messageDM.shouldShowAgentNameForConversation = conversation.showAgentName;
                if (messageDM instanceof UserMessageDM) {
                    ((UserMessageDM) messageDM).setState(UserMessageState.SENT);
                } else if (messageDM instanceof ScreenshotMessageDM) {
                    ((ScreenshotMessageDM) messageDM).setState(UserMessageState.SENT);
                }
                messageDM.addObserver(conversation);
            }
            if (z) {
                ConversationUtil.sortMessagesBasedOnCreatedAt(arrayList);
                conversation.isInBetweenBotExecution = evaluateBotExecutionState(arrayList, conversation.isInBetweenBotExecution);
                conversation.messageDMs.addAll(arrayList);
                for (MessageDM messageDM2 : arrayList) {
                    if (messageDM2 instanceof AdminImageAttachmentMessageDM) {
                        ((AdminImageAttachmentMessageDM) messageDM2).downloadThumbnailImage(this.platform);
                    }
                    updateAcceptedRequestForReopenMessageDMs(conversation, messageDM2);
                }
            } else {
                conversation.messageDMs.addAll(arrayList);
            }
            conversationUpdate.newMessageDMs.addAll(arrayList);
            evaluateBotControlMessages(conversation, arrayList);
        }
    }

    private void clearRedactedAttachmentsResources(final List<AttachmentMessageDM> list) {
        if (list.size() != 0) {
            this.domain.runParallel(new F() {
                public void f() {
                    for (AttachmentMessageDM attachmentMessageDM : list) {
                        try {
                            if (FileUtil.deleteFile(attachmentMessageDM.filePath)) {
                                attachmentMessageDM.filePath = null;
                            }
                        } catch (Exception e) {
                            HSLogger.e(ConversationManager.TAG, "Exception while deleting redacted AttachmentMessageDM file", e);
                        }
                    }
                }
            });
        }
    }

    private void evaluateBotControlMessages(Conversation conversation, Collection<? extends MessageDM> collection) {
        final Conversation conversation2 = conversation;
        for (MessageDM messageDM : collection) {
            if (AnonymousClass12.$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType[messageDM.messageType.ordinal()] == 1) {
                ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
                UnsupportedAdminMessageWithInputDM unsupportedAdminMessageWithInputDM = (UnsupportedAdminMessageWithInputDM) messageDM;
                final UserBotControlMessageDM userBotControlMessageDM = new UserBotControlMessageDM("Unsupported bot input", (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, BotControlActions.BOT_CANCELLED, BotControlActions.UNSUPPORTED_BOT_INPUT, unsupportedAdminMessageWithInputDM.botInfo, unsupportedAdminMessageWithInputDM.serverId, 1);
                userBotControlMessageDM.conversationLocalId = conversation2.localId;
                addMessageToDbAndUI(conversation2, userBotControlMessageDM);
                sendMessageWithAutoRetry(new F() {
                    public void f() {
                        userBotControlMessageDM.send(ConversationManager.this.userDM, conversation2);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void updateAcceptedRequestForReopenMessageDMs(Conversation conversation, MessageDM messageDM) {
        if (messageDM instanceof RequestForReopenMessageDM) {
            RequestForReopenMessageDM requestForReopenMessageDM = (RequestForReopenMessageDM) messageDM;
            if (!requestForReopenMessageDM.isAnswered()) {
                conversation.unansweredRequestForReopenMessageDMs.put(messageDM.serverId, requestForReopenMessageDM);
            }
        } else if (messageDM instanceof FollowupAcceptedMessageDM) {
            String str = ((FollowupAcceptedMessageDM) messageDM).referredMessageId;
            if (conversation.unansweredRequestForReopenMessageDMs.containsKey(str)) {
                RequestForReopenMessageDM remove = conversation.unansweredRequestForReopenMessageDMs.remove(str);
                remove.setDependencies(this.domain, this.platform);
                remove.shouldShowAgentNameForConversation = conversation.showAgentName;
                remove.setAnsweredAndNotify(true);
                this.conversationDAO.insertOrUpdateMessage(remove);
            }
        }
    }

    private void populateMessageDMLookup(Conversation conversation, Map<String, MessageDM> map, Map<String, MessageDM> map2) {
        ArrayList<MessageDM> arrayList = new ArrayList<>();
        List<MessageDM> readMessages = this.conversationDAO.readMessages(conversation.localId.longValue());
        HashMap hashMap = new HashMap();
        Iterator<E> it = conversation.messageDMs.iterator();
        while (it.hasNext()) {
            MessageDM messageDM = (MessageDM) it.next();
            if (messageDM.localId != null) {
                hashMap.put(messageDM.localId, messageDM);
            }
        }
        for (MessageDM next : readMessages) {
            MessageDM messageDM2 = (MessageDM) hashMap.get(next.localId);
            if (messageDM2 == null) {
                arrayList.add(next);
            } else {
                arrayList.add(messageDM2);
            }
        }
        Map<String, String> localIdToPendingRequestIdMessageMap = getLocalIdToPendingRequestIdMessageMap(conversation);
        for (MessageDM messageDM3 : arrayList) {
            if (!StringUtils.isEmpty(messageDM3.serverId)) {
                map.put(messageDM3.serverId, messageDM3);
            }
            if (messageDM3.localId != null) {
                String valueOf = String.valueOf(messageDM3.localId);
                if (localIdToPendingRequestIdMessageMap != null && localIdToPendingRequestIdMessageMap.containsKey(valueOf)) {
                    map2.put(localIdToPendingRequestIdMessageMap.get(valueOf), messageDM3);
                }
            }
        }
    }

    private Map<String, String> getLocalIdToPendingRequestIdMessageMap(Conversation conversation) {
        return this.platform.getNetworkRequestDAO().getPendingRequestIdMapForRoute(getRouteForSendingMessage(conversation));
    }

    private String getRouteForSendingMessage(Conversation conversation) {
        if (conversation.isInPreIssueMode()) {
            return "/preissues/" + conversation.getPreIssueId() + "/messages/";
        }
        return "/issues/" + conversation.getIssueId() + "/messages/";
    }

    private MessageDM getMessageDMForUpdate(MessageDM messageDM, Map<String, MessageDM> map, Map<String, MessageDM> map2, ConversationUpdate conversationUpdate) {
        if (map.containsKey(messageDM.serverId)) {
            return map.get(messageDM.serverId);
        }
        if (!map2.containsKey(messageDM.createdRequestId)) {
            return null;
        }
        MessageDM messageDM2 = map2.get(messageDM.createdRequestId);
        conversationUpdate.localIdsForResolvedRequestIds.add(String.valueOf(messageDM2.localId));
        return messageDM2;
    }

    public boolean evaluateBotExecutionState(List<MessageDM> list, boolean z) {
        if (list == null || list.size() == 0) {
            return z;
        }
        for (int size = list.size() - 1; size >= 0; size--) {
            MessageDM messageDM = list.get(size);
            if (MessageType.ADMIN_BOT_CONTROL == messageDM.messageType) {
                AdminBotControlMessageDM adminBotControlMessageDM = (AdminBotControlMessageDM) messageDM;
                String str = adminBotControlMessageDM.actionType;
                if (BotControlActions.BOT_STARTED.equals(str)) {
                    return true;
                }
                if (BotControlActions.BOT_ENDED.equals(str)) {
                    return adminBotControlMessageDM.hasNextBot;
                }
            }
        }
        return z;
    }

    public void mergePreIssue(Conversation conversation, Conversation conversation2, boolean z, ConversationUpdate conversationUpdate) {
        IssueState issueState = conversation2.state;
        switch (issueState) {
            case RESOLUTION_REQUESTED:
                issueState = IssueState.RESOLUTION_ACCEPTED;
                break;
            case COMPLETED_ISSUE_CREATED:
                issueState = IssueState.COMPLETED_ISSUE_CREATED;
                conversation.serverId = conversation2.serverId;
                break;
        }
        String str = conversation2.messageCursor;
        if (str != null) {
            conversation.messageCursor = str;
        }
        conversation.preConversationServerId = conversation2.preConversationServerId;
        conversation.serverId = conversation2.serverId;
        conversation.issueType = conversation2.issueType;
        conversation.title = conversation2.title;
        conversation.showAgentName = conversation2.showAgentName;
        conversation.publishId = conversation2.publishId;
        conversation.createdAt = conversation2.createdAt;
        conversation.epochCreatedAtTime = conversation2.getEpochCreatedAtTime();
        conversation.updatedAt = conversation2.updatedAt;
        conversation.state = issueState;
        updateMessageDMs(conversation, z, conversation2.messageDMs, conversationUpdate);
    }

    public void setEnableMessageClickOnResolutionRejected(Conversation conversation, boolean z) {
        conversation.enableMessageClickOnResolutionRejected = z;
        if (conversation.state == IssueState.RESOLUTION_REJECTED) {
            updateMessagesOnIssueStatusUpdate(conversation);
        }
    }

    public boolean shouldShowCSATInFooter(Conversation conversation) {
        if (!conversation.isInPreIssueMode() && conversation.csatState == ConversationCSATState.NONE && this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.CUSTOMER_SATISFACTION_SURVEY)) {
            return true;
        }
        return false;
    }

    public void handleConversationEnded(final Conversation conversation) {
        this.domain.runParallel(new F() {
            public void f() {
                ConversationManager.this.deleteOptionsForAdminMessageWithOptionsInput(conversation);
                ConversationManager.this.sendConversationEndedDelegate(conversation);
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteOptionsForAdminMessageWithOptionsInput(Conversation conversation) {
        List<MessageDM> readMessages = this.conversationDAO.readMessages(conversation.localId.longValue(), MessageType.ADMIN_TEXT_WITH_OPTION_INPUT);
        Iterator<MessageDM> it = readMessages.iterator();
        while (it.hasNext()) {
            ((AdminMessageWithOptionInputDM) it.next()).input.options.clear();
        }
        this.conversationDAO.insertOrUpdateMessages(readMessages);
    }

    public void sendConversationEndedDelegate(Conversation conversation) {
        if (!conversation.isConversationEndedDelegateSent) {
            this.domain.getDelegate().conversationEnded();
            conversation.isConversationEndedDelegateSent = true;
            this.conversationDAO.updateConversationWithoutMessages(conversation);
        }
    }

    public void initializeIssueStatusForUI(Conversation conversation) {
        if (conversation.state == IssueState.RESOLUTION_REQUESTED && !this.sdkConfigurationDM.shouldShowConversationResolutionQuestion()) {
            markConversationResolutionStatus(conversation, true);
        }
    }

    public void addPreissueFirstUserMessage(Conversation conversation, String str) {
        HSLogger.d(TAG, "Adding first user message to DB and UI.");
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        UserMessageDM userMessageDM = new UserMessageDM(str, (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE);
        userMessageDM.setDependencies(this.domain, this.platform);
        userMessageDM.conversationLocalId = conversation.localId;
        userMessageDM.setState(UserMessageState.SENDING);
        addMessageToDbAndUI(conversation, userMessageDM);
    }

    public void sendTextMessage(Conversation conversation, String str) {
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        UserMessageDM userMessageDM = new UserMessageDM(str, (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE);
        userMessageDM.conversationLocalId = conversation.localId;
        userMessageDM.updateState(shouldEnableMessagesClick(conversation));
        addMessageToDbAndUI(conversation, userMessageDM);
        sendTextMessage(conversation, userMessageDM);
    }

    private void sendTextMessage(Conversation conversation, UserMessageDM userMessageDM) {
        try {
            userMessageDM.send(this.userDM, conversation);
            if (conversation.state == IssueState.RESOLUTION_REJECTED) {
                updateIssueStatus(conversation, IssueState.WAITING_FOR_AGENT);
            }
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.CONVERSATION_ARCHIVED) {
                updateIssueStatus(conversation, IssueState.ARCHIVED);
            } else if (e.exceptionType == NetworkException.USER_PRE_CONDITION_FAILED) {
                updateIssueStatus(conversation, IssueState.AUTHOR_MISMATCH);
            } else {
                throw e;
            }
        }
    }

    public void sendTextMessage(Conversation conversation, String str, AdminMessageWithTextInputDM adminMessageWithTextInputDM, boolean z) {
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        UserResponseMessageForTextInputDM userResponseMessageForTextInputDM = new UserResponseMessageForTextInputDM(str, (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, adminMessageWithTextInputDM, z);
        userResponseMessageForTextInputDM.conversationLocalId = conversation.localId;
        userResponseMessageForTextInputDM.updateState(true);
        addMessageToDbAndUI(conversation, userResponseMessageForTextInputDM);
        sendTextMessage(conversation, userResponseMessageForTextInputDM);
    }

    public void retryMessage(Conversation conversation, MessageDM messageDM) {
        if (messageDM instanceof UserMessageDM) {
            sendTextMessage(conversation, (UserMessageDM) messageDM);
        } else if (messageDM instanceof ScreenshotMessageDM) {
            sendScreenshotMessageInternal(conversation, (ScreenshotMessageDM) messageDM, false);
        }
    }

    private void sendScreenshotMessageInternal(Conversation conversation, ScreenshotMessageDM screenshotMessageDM, boolean z) {
        try {
            screenshotMessageDM.uploadImage(this.userDM, conversation, z);
            if (conversation.state == IssueState.RESOLUTION_REJECTED) {
                updateIssueStatus(conversation, IssueState.WAITING_FOR_AGENT);
            }
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.CONVERSATION_ARCHIVED) {
                updateIssueStatus(conversation, IssueState.ARCHIVED);
                return;
            }
            throw e;
        }
    }

    public void retryMessages(Conversation conversation, boolean z) {
        List<MessageDM> readMessages = this.conversationDAO.readMessages(conversation.localId.longValue());
        ArrayList<AutoRetriableMessageDM> arrayList = new ArrayList<>();
        ArrayList<MessageDM> arrayList2 = new ArrayList<>();
        HashMap hashMap = new HashMap();
        ArrayList<FAQListMessageDM> arrayList3 = new ArrayList<>();
        for (MessageDM next : readMessages) {
            next.setDependencies(this.domain, this.platform);
            if (next instanceof AutoRetriableMessageDM) {
                AutoRetriableMessageDM autoRetriableMessageDM = (AutoRetriableMessageDM) next;
                if (autoRetriableMessageDM.isRetriable()) {
                    arrayList.add(autoRetriableMessageDM);
                }
            }
            if (!StringUtils.isEmpty(next.readAt) && !next.isMessageSeenSynced) {
                arrayList2.add(next);
            }
            if (next instanceof RequestAppReviewMessageDM) {
                hashMap.put(next.serverId, (RequestAppReviewMessageDM) next);
            }
            if (next instanceof FAQListMessageDM) {
                FAQListMessageDM fAQListMessageDM = (FAQListMessageDM) next;
                if (fAQListMessageDM.isSuggestionsReadEventPending()) {
                    arrayList3.add(fAQListMessageDM);
                }
            }
        }
        for (AutoRetriableMessageDM autoRetriableMessageDM2 : arrayList) {
            if (conversation.state != IssueState.ARCHIVED && conversation.state != IssueState.AUTHOR_MISMATCH) {
                try {
                    autoRetriableMessageDM2.send(this.userDM, conversation);
                    if (autoRetriableMessageDM2 instanceof AcceptedAppReviewMessageDM) {
                        ArrayList arrayList4 = new ArrayList();
                        AcceptedAppReviewMessageDM acceptedAppReviewMessageDM = (AcceptedAppReviewMessageDM) autoRetriableMessageDM2;
                        String str = acceptedAppReviewMessageDM.referredMessageId;
                        if (hashMap.containsKey(str)) {
                            RequestAppReviewMessageDM requestAppReviewMessageDM = (RequestAppReviewMessageDM) hashMap.get(str);
                            requestAppReviewMessageDM.handleAcceptedReviewSuccess(this.platform);
                            arrayList4.add(requestAppReviewMessageDM);
                        }
                        if (z) {
                            arrayList4.add(autoRetriableMessageDM2);
                            addMessageToUI(conversation, acceptedAppReviewMessageDM);
                            updateMessageDMs(conversation, true, arrayList4, null);
                        }
                    }
                } catch (RootAPIException e) {
                    if (e.exceptionType == NetworkException.CONVERSATION_ARCHIVED) {
                        updateIssueStatus(conversation, IssueState.ARCHIVED);
                    } else if (e.exceptionType == NetworkException.USER_PRE_CONDITION_FAILED) {
                        updateIssueStatus(conversation, IssueState.AUTHOR_MISMATCH);
                    } else if (e.exceptionType != NetworkException.NON_RETRIABLE) {
                        throw e;
                    }
                }
            } else {
                return;
            }
        }
        HashMap hashMap2 = new HashMap();
        for (MessageDM messageDM : arrayList2) {
            String str2 = messageDM.readAt;
            List list = (List) hashMap2.get(str2);
            if (list == null) {
                list = new ArrayList();
            }
            list.add(messageDM);
            hashMap2.put(str2, list);
        }
        for (String str3 : hashMap2.keySet()) {
            try {
                markMessagesAsSeen(conversation, (List) hashMap2.get(str3));
            } catch (RootAPIException e2) {
                if (e2.exceptionType != NetworkException.NON_RETRIABLE) {
                    throw e2;
                }
            }
        }
        for (FAQListMessageDM sendSuggestionReadEvent : arrayList3) {
            sendSuggestionReadEvent.sendSuggestionReadEvent(conversation, this.userDM);
        }
    }

    public void markMessagesAsSeen(Conversation conversation) {
        List<MessageDM> readMessages = this.conversationDAO.readMessages(conversation.localId.longValue());
        HashSet hashSet = new HashSet();
        for (MessageDM next : readMessages) {
            if (next.deliveryState != 1) {
                switch (next.messageType) {
                    case ADMIN_TEXT:
                    case ADMIN_TEXT_WITH_TEXT_INPUT:
                    case ADMIN_TEXT_WITH_OPTION_INPUT:
                    case FAQ_LIST:
                    case FAQ_LIST_WITH_OPTION_INPUT:
                    case ADMIN_ATTACHMENT:
                    case ADMIN_IMAGE_ATTACHMENT:
                    case REQUEST_FOR_REOPEN:
                    case REQUESTED_SCREENSHOT:
                    case REQUESTED_APP_REVIEW:
                        hashSet.add(next.localId);
                        continue;
                }
            }
        }
        if (hashSet.size() != 0) {
            markSeenMessagesAsRead(conversation, hashSet);
        }
    }

    private void markSeenMessagesAsRead(Conversation conversation, Set<Long> set) {
        String str = (String) HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform).first;
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Iterator<E> it = conversation.messageDMs.iterator();
        while (it.hasNext()) {
            MessageDM messageDM = (MessageDM) it.next();
            if (messageDM.localId != null) {
                hashMap.put(messageDM.localId, messageDM);
            }
        }
        for (Long l : set) {
            MessageDM messageDM2 = (MessageDM) hashMap.get(l);
            if (messageDM2 != null) {
                messageDM2.readAt = str;
                messageDM2.deliveryState = 1;
                messageDM2.seenAtMessageCursor = conversation.messageCursor;
                arrayList.add(messageDM2);
            }
        }
        if (!ListUtils.isEmpty(arrayList)) {
            this.conversationDAO.insertOrUpdateMessages(arrayList);
            markMessagesAsSeen(conversation, arrayList);
        }
    }

    private void markMessagesAsSeen(Conversation conversation, List<MessageDM> list) {
        if (!ListUtils.isEmpty(list)) {
            String str = list.get(0).readAt;
            String str2 = list.get(0).seenAtMessageCursor;
            HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(this.userDM);
            userRequestData.put("read_at", str);
            userRequestData.put("mc", str2);
            userRequestData.put("md_state", NotificationStatus.READ);
            try {
                new GuardOKNetwork(new FailedAPICallNetworkDecorator(new TSCorrectedNetwork(new AuthenticationFailureNetwork(new PUTNetwork(getRouteForSendingMessage(conversation), this.domain, this.platform)), this.platform))).makeRequest(new RequestData(userRequestData));
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(this.userDM, e.exceptionType);
                } else if (e.exceptionType != NetworkException.NON_RETRIABLE) {
                    throw e;
                }
            }
            for (MessageDM messageDM : list) {
                messageDM.isMessageSeenSynced = true;
            }
            this.conversationDAO.insertOrUpdateMessages(list);
        }
    }

    public void handleAppReviewRequestClick(final Conversation conversation, final RequestAppReviewMessageDM requestAppReviewMessageDM) {
        final AcceptedAppReviewMessageDM handleRequestReviewClick = requestAppReviewMessageDM.handleRequestReviewClick(this.domain, this.platform);
        if (handleRequestReviewClick != null) {
            sendMessageWithAutoRetry(new F() {
                public void f() {
                    try {
                        handleRequestReviewClick.send(ConversationManager.this.userDM, conversation);
                        requestAppReviewMessageDM.handleAcceptedReviewSuccess(ConversationManager.this.platform);
                    } catch (RootAPIException e) {
                        if (e.exceptionType == NetworkException.CONVERSATION_ARCHIVED) {
                            ConversationManager.this.updateIssueStatus(conversation, IssueState.ARCHIVED);
                        } else {
                            requestAppReviewMessageDM.setIsReviewButtonClickable(true);
                            throw e;
                        }
                    }
                }
            });
        }
    }

    public void sendScreenshot(Conversation conversation, ImagePickerFile imagePickerFile, String str) {
        Conversation conversation2 = conversation;
        ImagePickerFile imagePickerFile2 = imagePickerFile;
        String str2 = str;
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        ScreenshotMessageDM screenshotMessageDM = new ScreenshotMessageDM(null, (String) currentAdjustedTimeForStorage.first, ((Long) currentAdjustedTimeForStorage.second).longValue(), TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, null, null, null, null, 0, false);
        screenshotMessageDM.fileName = imagePickerFile2.originalFileName;
        screenshotMessageDM.filePath = imagePickerFile2.filePath;
        screenshotMessageDM.setRefersMessageId(str2);
        screenshotMessageDM.updateState(shouldEnableMessagesClick(conversation));
        screenshotMessageDM.conversationLocalId = conversation2.localId;
        addMessageToDbAndUI(conversation2, screenshotMessageDM);
        if (str2 != null) {
            Iterator<E> it = conversation2.messageDMs.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                MessageDM messageDM = (MessageDM) it.next();
                if (messageDM.serverId != null && messageDM.serverId.equals(str2) && messageDM.messageType == MessageType.REQUESTED_SCREENSHOT) {
                    ((RequestScreenshotMessageDM) messageDM).setIsAnswered(this.platform, true);
                    break;
                }
            }
        }
        sendScreenshotMessageInternal(conversation2, screenshotMessageDM, !imagePickerFile2.isFileCompressionAndCopyingDone);
    }

    public void sendCSATSurvey(final Conversation conversation, int i, String str) {
        if (i > 5) {
            i = 5;
        } else if (i < 0) {
            i = 0;
        }
        conversation.csatRating = i;
        if (str != null) {
            str = str.trim();
        }
        conversation.csatFeedback = str;
        setCSATState(conversation, ConversationCSATState.SUBMITTED_NOT_SYNCED);
        sendMessageWithAutoRetry(new F() {
            public void f() {
                ConversationManager.this.sendCSATSurveyInternal(conversation);
            }
        });
        this.domain.getDelegate().userCompletedCustomerSatisfactionSurvey(conversation.csatRating, conversation.csatFeedback);
    }

    public void sendCSATSurveyInternal(Conversation conversation) {
        String str = "/issues/" + conversation.serverId + "/customer-survey/";
        HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(this.userDM);
        userRequestData.put(CampaignEx.JSON_KEY_STAR, String.valueOf(conversation.csatRating));
        userRequestData.put("feedback", conversation.csatFeedback);
        try {
            new GuardOKNetwork(new FailedAPICallNetworkDecorator(new TSCorrectedNetwork(new IdempotentNetwork(new POSTNetwork(str, this.domain, this.platform), this.platform, new SuccessOrNonRetriableStatusCodeIdempotentPolicy(), str, conversation.serverId), this.platform))).makeRequest(new RequestData(userRequestData));
            ConversationCSATState conversationCSATState = ConversationCSATState.SUBMITTED_SYNCED;
            if (conversationCSATState != null) {
                setCSATState(conversation, conversationCSATState);
            }
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(this.userDM, e.exceptionType);
            } else if (e.exceptionType == NetworkException.NON_RETRIABLE) {
                ConversationCSATState conversationCSATState2 = ConversationCSATState.SUBMITTED_SYNCED;
            }
            throw e;
        } catch (Throwable th) {
            if (0 != 0) {
                setCSATState(conversation, null);
            }
            throw th;
        }
    }

    private void setCSATState(Conversation conversation, ConversationCSATState conversationCSATState) {
        if (conversation.csatState != conversationCSATState) {
            HSLogger.d(TAG, "Update CSAT state : Conversation : " + conversation.serverId + ", state : " + conversationCSATState.toString());
        }
        conversation.csatState = conversationCSATState;
        this.conversationDAO.updateConversationWithoutMessages(conversation);
    }

    public void setStartNewConversationButtonClicked(Conversation conversation, boolean z, boolean z2) {
        conversation.isStartNewConversationClicked = z;
        if (z2) {
            this.conversationDAO.updateConversationWithoutMessages(conversation);
        }
    }

    public void setShouldIncrementMessageCount(Conversation conversation, boolean z, boolean z2) {
        if (conversation.shouldIncrementMessageCount != z) {
            conversation.shouldIncrementMessageCount = z;
            if (z2) {
                this.conversationDAO.updateConversationWithoutMessages(conversation);
            }
        }
    }

    public void updateLastUserActivityTime(Conversation conversation, long j) {
        conversation.lastUserActivityTime = j;
        this.conversationDAO.updateLastUserActivityTimeInConversation(conversation.localId, j);
    }

    public void initializeMessagesForUI(Conversation conversation, boolean z) {
        ConversationUtil.sortMessagesBasedOnCreatedAt(conversation.messageDMs);
        if (z) {
            conversation.isInBetweenBotExecution = evaluateBotExecutionState(conversation.messageDMs, false);
            Iterator<E> it = conversation.messageDMs.iterator();
            while (it.hasNext()) {
                MessageDM messageDM = (MessageDM) it.next();
                messageDM.setDependencies(this.domain, this.platform);
                messageDM.shouldShowAgentNameForConversation = conversation.showAgentName;
                if (messageDM instanceof AdminImageAttachmentMessageDM) {
                    ((AdminImageAttachmentMessageDM) messageDM).downloadThumbnailImage(this.platform);
                }
                updateMessageOnConversationUpdate(messageDM, shouldEnableMessagesClick(conversation));
                updateAcceptedRequestForReopenMessageDMs(conversation, messageDM);
            }
            if (conversation.messageDMs.size() > 0) {
                MessageDM messageDM2 = (MessageDM) conversation.messageDMs.get(conversation.messageDMs.size() - 1);
                if (messageDM2.messageType == MessageType.USER_RESP_FOR_OPTION_INPUT || messageDM2.messageType == MessageType.USER_RESP_FOR_TEXT_INPUT) {
                    MessageDM latestUnansweredBotMessage = getLatestUnansweredBotMessage(conversation);
                    if (conversation.isInBetweenBotExecution && latestUnansweredBotMessage == null) {
                        ((UserMessageDM) messageDM2).updateState(true);
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        Iterator<E> it2 = conversation.messageDMs.iterator();
        while (it2.hasNext()) {
            MessageDM messageDM3 = (MessageDM) it2.next();
            messageDM3.setDependencies(this.domain, this.platform);
            messageDM3.shouldShowAgentNameForConversation = conversation.showAgentName;
            if (messageDM3 instanceof AdminImageAttachmentMessageDM) {
                ((AdminImageAttachmentMessageDM) messageDM3).downloadThumbnailImage(this.platform);
            }
            updateMessageOnConversationUpdate(messageDM3, false);
        }
    }

    public void initializeMessageListForUI(Conversation conversation, List<MessageDM> list, boolean z) {
        for (MessageDM next : list) {
            next.setDependencies(this.domain, this.platform);
            next.shouldShowAgentNameForConversation = conversation.showAgentName;
            updateMessageOnConversationUpdate(next, z);
            updateAcceptedRequestForReopenMessageDMs(conversation, next);
        }
    }

    public boolean isSynced(Conversation conversation) {
        return !StringUtils.isEmpty(conversation.serverId) || !StringUtils.isEmpty(conversation.preConversationServerId);
    }

    public boolean shouldOpen(Conversation conversation) {
        if (!this.sdkConfigurationDM.getBoolean(SDKConfigurationDM.CONVERSATIONAL_ISSUE_FILING) && conversation.isInPreIssueMode() && StringUtils.isEmpty(conversation.preConversationServerId)) {
            return false;
        }
        if (conversation.isInPreIssueMode() && conversation.isIssueInProgress()) {
            return true;
        }
        IssueState issueState = conversation.state;
        if (conversation.isRedacted) {
            return false;
        }
        if (!conversation.isIssueInProgress() && issueState != IssueState.RESOLUTION_REQUESTED) {
            if (issueState == IssueState.RESOLUTION_ACCEPTED || issueState == IssueState.RESOLUTION_REJECTED || issueState == IssueState.ARCHIVED) {
                return !conversation.isStartNewConversationClicked;
            }
            if (issueState != IssueState.REJECTED || conversation.isStartNewConversationClicked) {
                return false;
            }
            if (conversation.isInPreIssueMode() && ConversationUtil.getUserMessageCountForConversationLocalId(this.conversationDAO, conversation.localId) <= 0) {
                return false;
            }
        }
        return true;
    }

    public MessageDM getLatestUnansweredBotMessage(Conversation conversation) {
        boolean z = true;
        for (int size = conversation.messageDMs.size() - 1; size >= 0; size--) {
            MessageDM messageDM = (MessageDM) conversation.messageDMs.get(size);
            if (messageDM.messageType == MessageType.ADMIN_BOT_CONTROL) {
                return null;
            }
            if (messageDM.messageType == MessageType.ADMIN_TEXT_WITH_TEXT_INPUT || messageDM.messageType == MessageType.ADMIN_TEXT_WITH_OPTION_INPUT || messageDM.messageType == MessageType.FAQ_LIST_WITH_OPTION_INPUT || messageDM.messageType == MessageType.OPTION_INPUT) {
                int i = size + 1;
                while (true) {
                    if (i >= conversation.messageDMs.size()) {
                        z = false;
                        break;
                    }
                    MessageDM messageDM2 = (MessageDM) conversation.messageDMs.get(i);
                    if ((messageDM2.messageType == MessageType.USER_RESP_FOR_OPTION_INPUT || messageDM2.messageType == MessageType.USER_RESP_FOR_TEXT_INPUT) && messageDM.serverId.equals(((UserMessageDM) messageDM2).getReferredMessageId())) {
                        break;
                    }
                    i++;
                }
                if (z) {
                    return null;
                }
                return messageDM;
            }
        }
        return null;
    }

    public boolean hasBotSwitchedToAnotherBotInPollerResponse(Collection<? extends MessageDM> collection) {
        if (collection == null || collection.size() == 0) {
            return false;
        }
        ArrayList arrayList = new ArrayList(collection);
        boolean z = false;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            MessageDM messageDM = (MessageDM) arrayList.get(size);
            if (MessageType.ADMIN_BOT_CONTROL == messageDM.messageType) {
                String str = ((AdminBotControlMessageDM) messageDM).actionType;
                if (BotControlActions.BOT_ENDED.equals(str)) {
                    return z;
                }
                if (BotControlActions.BOT_STARTED.equals(str)) {
                    z = true;
                }
            }
        }
        return false;
    }

    public void sendOptionInputMessage(Conversation conversation, OptionInputMessageDM optionInputMessageDM, OptionInput.Option option, boolean z) {
        String str;
        ValuePair<String, Long> currentAdjustedTimeForStorage = HSDateFormatSpec.getCurrentAdjustedTimeForStorage(this.platform);
        String str2 = (String) currentAdjustedTimeForStorage.first;
        long longValue = ((Long) currentAdjustedTimeForStorage.second).longValue();
        if (z) {
            str = optionInputMessageDM.input.skipLabel;
        } else {
            str = option.title;
        }
        UserResponseMessageForOptionInput userResponseMessageForOptionInput = new UserResponseMessageForOptionInput(str, str2, longValue, TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, optionInputMessageDM, z);
        userResponseMessageForOptionInput.conversationLocalId = conversation.localId;
        userResponseMessageForOptionInput.updateState(true);
        addMessageToDbAndUI(conversation, userResponseMessageForOptionInput);
        deleteOptionsForAdminMessageWithOptionsInput(optionInputMessageDM);
        sendTextMessage(conversation, userResponseMessageForOptionInput);
    }

    public void updateMessagesClickOnBotSwitch(Conversation conversation, boolean z) {
        Iterator<E> it = conversation.messageDMs.iterator();
        while (it.hasNext()) {
            updateMessageClickableState((MessageDM) it.next(), z);
        }
    }

    public void handleAdminSuggestedQuestionRead(Conversation conversation, String str, String str2, String str3) {
        Iterator<E> it = conversation.messageDMs.iterator();
        while (it.hasNext()) {
            final MessageDM messageDM = (MessageDM) it.next();
            if ((messageDM instanceof FAQListMessageDM) && str.equals(messageDM.serverId)) {
                final Conversation conversation2 = conversation;
                final String str4 = str2;
                final String str5 = str3;
                sendMessageWithAutoRetry(new F() {
                    public void f() {
                        ((FAQListMessageDM) messageDM).handleSuggestionClick(conversation2, ConversationManager.this.userDM, str4, str5);
                    }
                });
                return;
            }
        }
    }

    public void dropCustomMetaData() {
        this.domain.getMetaDataDM().setCustomMetaDataCallable(null);
        this.domain.getMetaDataDM().clearCustomMetaData();
    }

    public boolean containsAtleastOneUserMessage(Conversation conversation) {
        if (!conversation.isInPreIssueMode()) {
            return true;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<E> it = conversation.messageDMs.iterator();
        while (it.hasNext()) {
            MessageDM messageDM = (MessageDM) it.next();
            if (messageDM.isUISupportedMessage()) {
                if (messageDM instanceof UserMessageDM) {
                    return true;
                }
                arrayList.add(messageDM);
                if (arrayList.size() > 3) {
                    return true;
                }
            }
        }
        return false;
    }

    public void clearMessageUpdates(Conversation conversation, ConversationUpdate conversationUpdate) {
        for (int i = 0; i < conversationUpdate.localIdsForResolvedRequestIds.size(); i++) {
            this.platform.getNetworkRequestDAO().deletePendingRequestId(getRouteForSendingMessage(conversation), conversationUpdate.localIdsForResolvedRequestIds.remove(i));
        }
        conversationUpdate.updatedMessageDMs.clear();
        conversationUpdate.newMessageDMs.clear();
    }

    public void sendConversationPostedEvent(Conversation conversation, Conversation conversation2) {
        if (conversation2.state == IssueState.COMPLETED_ISSUE_CREATED && conversation2.state != conversation.state) {
            this.domain.getAnalyticsEventDM().pushEvent(AnalyticsEventType.CONVERSATION_POSTED, conversation2.serverId);
        }
    }

    public void deleteCachedScreenshotFiles(Conversation conversation) {
        List<MessageDM> readMessages = this.conversationDAO.readMessages(conversation.localId.longValue());
        ArrayList arrayList = new ArrayList();
        for (MessageDM next : readMessages) {
            if (next instanceof ScreenshotMessageDM) {
                ScreenshotMessageDM screenshotMessageDM = (ScreenshotMessageDM) next;
                try {
                    if (FileUtil.deleteFile(screenshotMessageDM.getFilePath())) {
                        screenshotMessageDM.filePath = null;
                        arrayList.add(screenshotMessageDM);
                    }
                } catch (Exception e) {
                    HSLogger.e(TAG, "Exception while deleting ScreenshotMessageDM file", e);
                }
            }
        }
        this.conversationDAO.insertOrUpdateMessages(arrayList);
    }

    public int getUnSeenMessageCount(Conversation conversation) {
        int i = 0;
        if (!shouldOpen(conversation)) {
            return 0;
        }
        List<MessageDM> readMessages = this.conversationDAO.readMessages(conversation.localId.longValue());
        if (readMessages != null) {
            for (MessageDM next : readMessages) {
                if (next.isUISupportedMessage() && next.deliveryState != 1) {
                    switch (next.messageType) {
                        case ADMIN_TEXT:
                        case ADMIN_TEXT_WITH_OPTION_INPUT:
                        case FAQ_LIST:
                        case FAQ_LIST_WITH_OPTION_INPUT:
                        case ADMIN_ATTACHMENT:
                        case ADMIN_IMAGE_ATTACHMENT:
                        case REQUEST_FOR_REOPEN:
                        case REQUESTED_SCREENSHOT:
                        case REQUESTED_APP_REVIEW:
                            i++;
                            continue;
                        case ADMIN_TEXT_WITH_TEXT_INPUT:
                            if ((next instanceof AdminMessageWithTextInputDM) && !((AdminMessageWithTextInputDM) next).isMessageEmpty) {
                                i++;
                                break;
                            }
                    }
                }
            }
        }
        return conversation.shouldIncrementMessageCount ? i + 1 : i;
    }

    public void checkAndIncrementMessageCount(Conversation conversation, IssueState issueState) {
        if (ConversationUtil.isInProgressState(issueState) && (conversation.state == IssueState.RESOLUTION_REQUESTED || conversation.state == IssueState.RESOLUTION_ACCEPTED || conversation.state == IssueState.RESOLUTION_REJECTED)) {
            setShouldIncrementMessageCount(conversation, true, true);
        } else if (conversation.isIssueInProgress()) {
            setShouldIncrementMessageCount(conversation, false, true);
        }
    }

    public void handlePreIssueCreationSuccess(Conversation conversation) {
        conversation.lastUserActivityTime = System.currentTimeMillis();
    }

    public void sendConversationEndedDelegateForPreIssue(Conversation conversation) {
        if (conversation.state == IssueState.RESOLUTION_ACCEPTED) {
            handleConversationEnded(conversation);
        }
    }

    public boolean shouldEnableMessagesClick(Conversation conversation) {
        if (conversation.isInBetweenBotExecution) {
            return false;
        }
        if (conversation.isIssueInProgress()) {
            return true;
        }
        if (conversation.state == IssueState.RESOLUTION_REQUESTED || conversation.state == IssueState.RESOLUTION_ACCEPTED || conversation.state == IssueState.ARCHIVED || conversation.state == IssueState.REJECTED || conversation.state != IssueState.RESOLUTION_REJECTED) {
            return false;
        }
        return conversation.enableMessageClickOnResolutionRejected;
    }

    public void updateStateBasedOnMessages(Conversation conversation) {
        if (conversation.state == IssueState.RESOLUTION_REQUESTED && conversation.messageDMs != null && conversation.messageDMs.size() > 0) {
            MessageDM messageDM = null;
            for (int size = conversation.messageDMs.size() - 1; size >= 0; size--) {
                messageDM = (MessageDM) conversation.messageDMs.get(size);
                if (!(messageDM instanceof FollowupRejectedMessageDM) && !(messageDM instanceof RequestForReopenMessageDM)) {
                    break;
                }
            }
            if (messageDM instanceof ConfirmationAcceptedMessageDM) {
                conversation.state = IssueState.RESOLUTION_ACCEPTED;
            } else if (messageDM instanceof ConfirmationRejectedMessageDM) {
                conversation.state = IssueState.RESOLUTION_REJECTED;
            }
        }
    }

    public void updateIsAutoFilledPreissueFlag(Conversation conversation, boolean z) {
        conversation.isAutoFilledPreIssue = z;
        this.conversationDAO.updateConversationWithoutMessages(conversation);
    }
}
