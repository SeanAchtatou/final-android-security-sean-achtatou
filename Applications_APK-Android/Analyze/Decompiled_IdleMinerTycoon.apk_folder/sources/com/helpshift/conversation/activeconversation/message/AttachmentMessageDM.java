package com.helpshift.conversation.activeconversation.message;

import com.helpshift.common.StringUtils;
import java.util.Locale;

public abstract class AttachmentMessageDM extends MessageDM {
    public String attachmentUrl;
    public String contentType;
    public String fileName;
    public String filePath;
    public boolean isSecureAttachment;
    public int size;

    AttachmentMessageDM(String str, String str2, long j, String str3, int i, String str4, String str5, String str6, boolean z, boolean z2, MessageType messageType) {
        super(str, str2, j, str3, z, messageType);
        this.size = i;
        this.contentType = str4;
        this.attachmentUrl = str5;
        this.fileName = str6;
        this.isSecureAttachment = z2;
    }

    public String getFormattedFileSize() {
        return getFormattedFileSize((double) this.size);
    }

    /* access modifiers changed from: package-private */
    public String getFormattedFileSize(double d) {
        String str;
        if (d < 1024.0d) {
            str = " B";
        } else if (d < 1048576.0d) {
            d /= 1024.0d;
            str = " KB";
        } else {
            d /= 1048576.0d;
            str = " MB";
        }
        if (" MB".equals(str)) {
            return String.format(Locale.US, "%.1f", Double.valueOf(d)) + str;
        }
        return String.format(Locale.US, "%.0f", Double.valueOf(d)) + str;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof AttachmentMessageDM) {
            AttachmentMessageDM attachmentMessageDM = (AttachmentMessageDM) messageDM;
            this.contentType = attachmentMessageDM.contentType;
            this.fileName = attachmentMessageDM.fileName;
            this.attachmentUrl = attachmentMessageDM.attachmentUrl;
            this.size = attachmentMessageDM.size;
            this.isSecureAttachment = attachmentMessageDM.isSecureAttachment;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isValidUriPath(String str) {
        return !StringUtils.isEmpty(str) && str.startsWith("content://");
    }
}
