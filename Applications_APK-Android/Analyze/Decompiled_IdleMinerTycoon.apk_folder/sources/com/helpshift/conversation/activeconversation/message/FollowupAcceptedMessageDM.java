package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import java.util.HashMap;

public class FollowupAcceptedMessageDM extends AutoRetriableMessageDM {
    public String referredMessageId;

    public boolean isUISupportedMessage() {
        return false;
    }

    public FollowupAcceptedMessageDM(String str, String str2, long j, String str3, String str4, int i) {
        super(str, str2, j, str3, false, MessageType.FOLLOWUP_ACCEPTED, i);
        this.referredMessageId = str4;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof FollowupAcceptedMessageDM) {
            this.referredMessageId = ((FollowupAcceptedMessageDM) messageDM).referredMessageId;
        }
    }

    public void send(UserDM userDM, ConversationServerInfo conversationServerInfo) {
        if (!StringUtils.isEmpty(conversationServerInfo.getIssueId())) {
            HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM);
            userRequestData.put("body", "Accepted the follow-up");
            userRequestData.put("type", "ra");
            userRequestData.put("refers", this.referredMessageId);
            try {
                FollowupAcceptedMessageDM parseFollowupAcceptedMessage = this.platform.getResponseParser().parseFollowupAcceptedMessage(makeNetworkRequest(getIssueSendMessageRoute(conversationServerInfo), userRequestData).responseString);
                merge(parseFollowupAcceptedMessage);
                this.authorId = parseFollowupAcceptedMessage.authorId;
                this.serverId = parseFollowupAcceptedMessage.serverId;
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("FollowupAcceptedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
