package com.helpshift.conversation.activeconversation.usersetup;

public interface UserSetupRenderer {
    void hideNoInternetView();

    void hideProgressBar();

    void hideProgressDescription();

    void onAuthenticationFailure();

    void onUserSetupComplete();

    void showNoInternetView();

    void showProgressBar();

    void showProgressDescription();
}
