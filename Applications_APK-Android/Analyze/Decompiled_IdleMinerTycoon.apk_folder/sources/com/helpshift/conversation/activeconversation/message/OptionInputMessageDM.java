package com.helpshift.conversation.activeconversation.message;

import com.helpshift.conversation.activeconversation.message.input.OptionInput;

public class OptionInputMessageDM extends UserMessageDM {
    public final OptionInput input;
    public final MessageType referredMessageType;

    public OptionInputMessageDM(FAQListMessageWithOptionInputDM fAQListMessageWithOptionInputDM) {
        super(fAQListMessageWithOptionInputDM.body, fAQListMessageWithOptionInputDM.getCreatedAt(), fAQListMessageWithOptionInputDM.getEpochCreatedAtTime(), fAQListMessageWithOptionInputDM.authorName, MessageType.OPTION_INPUT);
        this.serverId = fAQListMessageWithOptionInputDM.serverId;
        this.input = fAQListMessageWithOptionInputDM.input;
        this.referredMessageType = fAQListMessageWithOptionInputDM.messageType;
        this.conversationLocalId = fAQListMessageWithOptionInputDM.conversationLocalId;
    }

    public OptionInputMessageDM(AdminMessageWithOptionInputDM adminMessageWithOptionInputDM) {
        super(adminMessageWithOptionInputDM.body, adminMessageWithOptionInputDM.getCreatedAt(), adminMessageWithOptionInputDM.getEpochCreatedAtTime(), adminMessageWithOptionInputDM.authorName, MessageType.OPTION_INPUT);
        this.serverId = adminMessageWithOptionInputDM.serverId;
        this.input = adminMessageWithOptionInputDM.input;
        this.referredMessageType = adminMessageWithOptionInputDM.messageType;
        this.conversationLocalId = adminMessageWithOptionInputDM.conversationLocalId;
    }
}
