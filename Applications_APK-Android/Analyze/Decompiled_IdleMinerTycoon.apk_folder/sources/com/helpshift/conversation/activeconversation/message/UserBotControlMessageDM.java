package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;

public class UserBotControlMessageDM extends AutoRetriableMessageDM {
    public String actionType;
    public String botInfo;
    public String reason;
    public String refersMessageId;

    public boolean isUISupportedMessage() {
        return false;
    }

    public UserBotControlMessageDM(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, int i) {
        super(str, str2, j, str3, false, MessageType.USER_BOT_CONTROL, i);
        this.actionType = str4;
        this.reason = str5;
        this.botInfo = str6;
        this.refersMessageId = str7;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof UserBotControlMessageDM) {
            UserBotControlMessageDM userBotControlMessageDM = (UserBotControlMessageDM) messageDM;
            this.actionType = userBotControlMessageDM.actionType;
            this.reason = userBotControlMessageDM.reason;
            this.botInfo = userBotControlMessageDM.botInfo;
            this.refersMessageId = userBotControlMessageDM.refersMessageId;
        }
    }

    public void send(UserDM userDM, ConversationServerInfo conversationServerInfo) {
        String str;
        HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM);
        userRequestData.put("origin", TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE);
        userRequestData.put("type", this.actionType);
        userRequestData.put("chatbot_cancelled_reason", this.reason);
        userRequestData.put("body", this.body);
        userRequestData.put("chatbot_info", this.botInfo);
        userRequestData.put("refers", this.refersMessageId);
        if (conversationServerInfo.isInPreIssueMode()) {
            str = getPreIssueSendMessageRoute(conversationServerInfo);
        } else {
            str = getIssueSendMessageRoute(conversationServerInfo);
        }
        try {
            UserBotControlMessageDM userBotControlMessageDM = (UserBotControlMessageDM) this.platform.getResponseParser().parseBotControlMessage(makeNetworkRequest(str, userRequestData).responseString, false);
            merge(userBotControlMessageDM);
            this.serverId = userBotControlMessageDM.serverId;
            this.platform.getConversationDAO().insertOrUpdateMessage(this);
        } catch (RootAPIException e) {
            if (e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED || e.exceptionType == NetworkException.INVALID_AUTH_TOKEN) {
                this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
            }
            throw e;
        }
    }
}
