package com.helpshift.conversation.activeconversation;

import com.helpshift.common.exception.ExceptionType;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import java.util.List;
import java.util.Map;

public interface ConversationRenderer {
    void appendMessages(int i, int i2);

    void destroy();

    void disableSendReplyButton();

    void enableSendReplyButton();

    String getReply();

    void hideAgentTypingIndicator();

    void hideKeyboard();

    void hideSendReplyUI();

    void initializeMessages(List<MessageDM> list);

    boolean isReplyBoxVisible();

    void launchAttachment(String str, String str2);

    void launchScreenshotAttachment(String str, String str2);

    void notifyRefreshList();

    void onAuthenticationFailure();

    void openAppReviewStore(String str);

    void openFreshConversationScreen(Map<String, Boolean> map);

    void removeMessages(int i, int i2);

    void requestReplyFieldFocus();

    void scrollToBottom();

    void setReply(String str);

    void setReplyboxListeners();

    void showAgentTypingIndicator();

    void showCSATSubmittedView();

    void showErrorView(ExceptionType exceptionType);

    void showKeyboard();

    void showSendReplyUI();

    void updateConversationResolutionQuestionUI(boolean z);

    void updateImageAttachmentButtonView(boolean z);

    void updateMessages(int i, int i2);

    void updateSendReplyButton(boolean z);

    void updateSendReplyUI(boolean z);
}
