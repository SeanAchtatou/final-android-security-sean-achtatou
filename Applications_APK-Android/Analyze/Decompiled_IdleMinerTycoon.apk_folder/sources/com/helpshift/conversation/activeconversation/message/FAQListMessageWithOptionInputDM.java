package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.platform.KVStore;
import com.helpshift.common.platform.Platform;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import com.helpshift.conversation.activeconversation.message.FAQListMessageDM;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import java.util.ArrayList;
import java.util.List;

public class FAQListMessageWithOptionInputDM extends FAQListMessageDM {
    public static final String KEY_SUGGESTIONS_READ_FAQ_PREFIX = "read_faq_";
    public OptionInput input;
    private ArrayList<String> readFAQs;

    public FAQListMessageWithOptionInputDM(String str, String str2, String str3, long j, String str4, List<FAQListMessageDM.FAQ> list, String str5, boolean z, String str6, String str7, List<OptionInput.Option> list2) {
        super(str, str2, str3, j, str4, list, MessageType.FAQ_LIST_WITH_OPTION_INPUT);
        this.input = new OptionInput(str5, z, str6, str7, list2, OptionInput.Type.PILL);
    }

    public FAQListMessageWithOptionInputDM(String str, String str2, String str3, long j, String str4, List<FAQListMessageDM.FAQ> list, String str5, boolean z, String str6, String str7, List<OptionInput.Option> list2, boolean z2, String str8) {
        super(str, str2, str3, j, str4, list, MessageType.FAQ_LIST_WITH_OPTION_INPUT);
        this.input = new OptionInput(str5, z, str6, str7, list2, OptionInput.Type.PILL);
        this.isSuggestionsReadEventSent = z2;
        this.suggestionsReadFAQPublishId = str8;
    }

    public void setDependencies(Domain domain, Platform platform) {
        super.setDependencies(domain, platform);
        populateReadFAQs();
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof FAQListMessageWithOptionInputDM) {
            this.input = ((FAQListMessageWithOptionInputDM) messageDM).input;
        }
    }

    public void handleSuggestionClick(ConversationServerInfo conversationServerInfo, UserDM userDM, String str, String str2) {
        if (this.readFAQs.size() < 10) {
            this.readFAQs.add(str);
            KVStore kVStore = this.platform.getKVStore();
            kVStore.setSerializable(KEY_SUGGESTIONS_READ_FAQ_PREFIX + this.serverId, this.readFAQs);
        }
        super.handleSuggestionClick(conversationServerInfo, userDM, str, str2);
    }

    private void populateReadFAQs() {
        if (this.readFAQs == null) {
            this.readFAQs = new ArrayList<>();
            KVStore kVStore = this.platform.getKVStore();
            Object serializable = kVStore.getSerializable(KEY_SUGGESTIONS_READ_FAQ_PREFIX + this.serverId);
            if (serializable instanceof ArrayList) {
                this.readFAQs = (ArrayList) serializable;
            }
        }
    }
}
