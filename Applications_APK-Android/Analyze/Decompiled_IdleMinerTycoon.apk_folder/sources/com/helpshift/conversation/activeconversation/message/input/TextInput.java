package com.helpshift.conversation.activeconversation.message.input;

import com.helpshift.common.domain.Domain;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.util.HSPattern;
import java.text.ParseException;

public class TextInput extends Input {
    private Domain domain;
    public final int keyboard;
    public final String placeholder;

    public interface Keyboard {
        public static final int DATE = 4;
        public static final int EMAIL = 2;
        public static final int NUMERIC = 3;
        public static final int TEXT = 1;
    }

    public TextInput(String str, boolean z, String str2, String str3, String str4, int i) {
        super(str, z, str2, str3);
        this.placeholder = str4;
        this.keyboard = i;
    }

    public void setDependencies(Domain domain2) {
        this.domain = domain2;
    }

    public boolean validate(String str) {
        switch (this.keyboard) {
            case 2:
                return HSPattern.isValidEmail(str);
            case 3:
                return HSPattern.isPositiveNumber(str);
            case 4:
                try {
                    HSDateFormatSpec.getDateFormatter(HSDateFormatSpec.DISPLAY_DATE_PATTERN, this.domain.getLocaleProviderDM().getCurrentLocale()).parse(str.trim());
                    return true;
                } catch (ParseException unused) {
                    return false;
                }
            default:
                return true;
        }
    }
}
