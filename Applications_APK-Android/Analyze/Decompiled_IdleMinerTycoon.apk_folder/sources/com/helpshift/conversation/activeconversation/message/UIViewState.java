package com.helpshift.conversation.activeconversation.message;

public class UIViewState {
    private boolean isFooterVisible;
    private boolean isRoundedBackground;

    public UIViewState() {
        this(false, false);
    }

    public UIViewState(boolean z, boolean z2) {
        this.isFooterVisible = z;
        this.isRoundedBackground = z2;
    }

    public boolean isFooterVisible() {
        return this.isFooterVisible;
    }

    public boolean isRoundedBackground() {
        return this.isRoundedBackground;
    }

    public void updateViewState(UIViewState uIViewState) {
        if (uIViewState != null) {
            this.isFooterVisible = uIViewState.isFooterVisible;
            this.isRoundedBackground = uIViewState.isRoundedBackground;
        }
    }

    public boolean equals(Object obj) {
        UIViewState uIViewState = (UIViewState) obj;
        return uIViewState != null && uIViewState.isFooterVisible() == this.isFooterVisible && uIViewState.isRoundedBackground() == this.isRoundedBackground;
    }
}
