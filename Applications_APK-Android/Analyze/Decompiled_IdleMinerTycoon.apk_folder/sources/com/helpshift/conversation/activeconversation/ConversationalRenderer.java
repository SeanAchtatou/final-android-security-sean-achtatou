package com.helpshift.conversation.activeconversation;

import com.helpshift.conversation.activeconversation.message.input.Input;
import com.helpshift.conversation.viewmodel.OptionUIModel;
import java.util.List;

public interface ConversationalRenderer extends ConversationRenderer {
    void hideListPicker(boolean z);

    void hideNetworkErrorFooter();

    void hidePickerClearButton();

    void hideReplyValidationFailedError();

    void hideSkipButton();

    boolean onBackPressed();

    void onFocusChanged(boolean z);

    void showEmptyListPickerView();

    void showInput(Input input);

    void showListPicker(List<OptionUIModel> list, String str, boolean z, String str2);

    void showNetworkErrorFooter(int i);

    void showPickerClearButton();

    void showReplyValidationFailedError(int i);

    void showSkipButton();

    void updateListPickerOptions(List<OptionUIModel> list);
}
