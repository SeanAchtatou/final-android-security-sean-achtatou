package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import java.util.HashMap;

public class AcceptedAppReviewMessageDM extends AutoRetriableMessageDM {
    public String referredMessageId;

    public boolean isUISupportedMessage() {
        return false;
    }

    public AcceptedAppReviewMessageDM(String str, String str2, long j, String str3, String str4, int i) {
        super(str, str2, j, str3, false, MessageType.ACCEPTED_APP_REVIEW, i);
        this.referredMessageId = str4;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof AcceptedAppReviewMessageDM) {
            this.referredMessageId = ((AcceptedAppReviewMessageDM) messageDM).referredMessageId;
        }
    }

    public void send(UserDM userDM, ConversationServerInfo conversationServerInfo) throws RootAPIException {
        if (!StringUtils.isEmpty(conversationServerInfo.getIssueId())) {
            HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM);
            userRequestData.put("body", this.body);
            userRequestData.put("type", "ar");
            userRequestData.put("refers", this.referredMessageId);
            try {
                AcceptedAppReviewMessageDM parseAcceptedAppReviewMessageDM = this.platform.getResponseParser().parseAcceptedAppReviewMessageDM(makeNetworkRequest(getIssueSendMessageRoute(conversationServerInfo), userRequestData).responseString);
                merge(parseAcceptedAppReviewMessageDM);
                this.serverId = parseAcceptedAppReviewMessageDM.serverId;
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("AcceptedAppReviewMessageDM send called with conversation in pre issue mode.");
        }
    }
}
