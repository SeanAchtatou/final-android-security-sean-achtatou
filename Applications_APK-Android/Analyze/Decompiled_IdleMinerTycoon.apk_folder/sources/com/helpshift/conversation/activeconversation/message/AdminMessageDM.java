package com.helpshift.conversation.activeconversation.message;

public class AdminMessageDM extends MessageDM {
    public boolean isUISupportedMessage() {
        return true;
    }

    public AdminMessageDM(String str, String str2, String str3, long j, String str4) {
        super(str2, str3, j, str4, true, MessageType.ADMIN_TEXT);
        this.serverId = str;
    }

    public AdminMessageDM(String str, String str2, String str3, long j, String str4, MessageType messageType) {
        super(str2, str3, j, str4, true, messageType);
        this.serverId = str;
    }

    public AdminMessageDM(AdminMessageWithOptionInputDM adminMessageWithOptionInputDM) {
        super(adminMessageWithOptionInputDM.body, adminMessageWithOptionInputDM.getCreatedAt(), adminMessageWithOptionInputDM.getEpochCreatedAtTime(), adminMessageWithOptionInputDM.authorName, true, MessageType.ADMIN_TEXT);
        this.serverId = adminMessageWithOptionInputDM.serverId;
        this.conversationLocalId = adminMessageWithOptionInputDM.conversationLocalId;
        this.shouldShowAgentNameForConversation = adminMessageWithOptionInputDM.shouldShowAgentNameForConversation;
    }
}
