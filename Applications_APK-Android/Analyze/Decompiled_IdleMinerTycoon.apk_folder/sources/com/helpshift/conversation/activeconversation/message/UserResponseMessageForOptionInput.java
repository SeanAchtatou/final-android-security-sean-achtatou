package com.helpshift.conversation.activeconversation.message;

import com.helpshift.common.platform.KVStore;
import com.helpshift.common.platform.network.Response;
import com.helpshift.conversation.activeconversation.message.input.OptionInput;
import com.tapjoy.TJAdUnitConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserResponseMessageForOptionInput extends UserMessageDM {
    public String botInfo;
    public String optionData;
    private String referredMessageId;
    public MessageType referredMessageType;
    public boolean skipped;

    public UserResponseMessageForOptionInput(String str, String str2, long j, String str3, String str4, boolean z, String str5, String str6, MessageType messageType) {
        super(str, str2, j, str3, MessageType.USER_RESP_FOR_OPTION_INPUT);
        this.botInfo = str4;
        this.skipped = z;
        this.optionData = str5;
        this.referredMessageId = str6;
        this.referredMessageType = messageType;
    }

    public UserResponseMessageForOptionInput(String str, String str2, long j, String str3, OptionInputMessageDM optionInputMessageDM, boolean z) {
        super(str, str2, j, str3, MessageType.USER_RESP_FOR_OPTION_INPUT);
        this.botInfo = optionInputMessageDM.input.botInfo;
        this.skipped = z;
        this.optionData = getSelectedOptionData(optionInputMessageDM.input.options);
        this.referredMessageId = optionInputMessageDM.serverId;
        this.referredMessageType = optionInputMessageDM.referredMessageType;
    }

    public void merge(MessageDM messageDM) {
        super.merge(messageDM);
        if (messageDM instanceof UserResponseMessageForOptionInput) {
            UserResponseMessageForOptionInput userResponseMessageForOptionInput = (UserResponseMessageForOptionInput) messageDM;
            this.botInfo = userResponseMessageForOptionInput.botInfo;
            this.skipped = userResponseMessageForOptionInput.skipped;
            this.optionData = userResponseMessageForOptionInput.optionData;
            this.referredMessageId = userResponseMessageForOptionInput.referredMessageId;
            this.referredMessageType = userResponseMessageForOptionInput.referredMessageType;
        }
    }

    /* access modifiers changed from: protected */
    public Map<String, String> getData() {
        HashMap hashMap = new HashMap();
        hashMap.put("chatbot_info", this.botInfo);
        hashMap.put(TJAdUnitConstants.String.VIDEO_SKIPPED, String.valueOf(this.skipped));
        if (!this.skipped) {
            hashMap.put("option_data", this.optionData);
        }
        if (this.referredMessageType == MessageType.FAQ_LIST_WITH_OPTION_INPUT) {
            List arrayList = new ArrayList();
            KVStore kVStore = this.platform.getKVStore();
            Object serializable = kVStore.getSerializable(FAQListMessageWithOptionInputDM.KEY_SUGGESTIONS_READ_FAQ_PREFIX + this.referredMessageId);
            if (serializable instanceof ArrayList) {
                arrayList = (List) serializable;
            }
            hashMap.put("read_faqs", this.platform.getJsonifier().jsonifyListToJsonArray(arrayList).toString());
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public String getMessageTypeForRequest() {
        switch (this.referredMessageType) {
            case ADMIN_TEXT_WITH_OPTION_INPUT:
                return "rsp_txt_msg_with_option_input";
            case FAQ_LIST_WITH_OPTION_INPUT:
                return "rsp_faq_list_msg_with_option_input";
            default:
                return super.getMessageTypeForRequest();
        }
    }

    public String getReferredMessageId() {
        return this.referredMessageId;
    }

    private String getSelectedOptionData(List<OptionInput.Option> list) {
        for (OptionInput.Option next : list) {
            if (next.title.equals(this.body)) {
                return next.jsonData;
            }
        }
        return "{}";
    }

    /* access modifiers changed from: protected */
    public UserMessageDM parseResponse(Response response) {
        return this.platform.getResponseParser().parseResponseMessageForOptionInput(response.responseString);
    }
}
