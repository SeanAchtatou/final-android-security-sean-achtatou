package com.helpshift.conversation.activeconversation;

public interface ConversationServerInfo {
    String getIssueId();

    String getPreIssueId();

    boolean isInPreIssueMode();
}
