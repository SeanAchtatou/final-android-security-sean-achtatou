package com.helpshift.conversation.activeconversation.message;

import com.tapjoy.TapjoyConstants;

public class SystemMessageDM extends MessageDM {
    public boolean isUISupportedMessage() {
        return true;
    }

    SystemMessageDM(String str, String str2, long j, MessageType messageType) {
        super(str, str2, j, TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, false, messageType);
    }
}
