package com.helpshift.conversation.activeconversation.message;

import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.idempotent.IdempotentPolicy;
import com.helpshift.common.domain.idempotent.SuccessOrNonRetriableStatusCodeIdempotentPolicy;
import com.helpshift.common.domain.network.AuthenticationFailureNetwork;
import com.helpshift.common.domain.network.GuardAgainstConversationArchivalNetwork;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.IdempotentNetwork;
import com.helpshift.common.domain.network.Network;
import com.helpshift.common.domain.network.POSTNetwork;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.domain.network.UserPreConditionsFailedNetwork;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.util.HSDateFormatSpec;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import com.helpshift.support.search.storage.TableSearchToken;
import com.helpshift.util.HSLogger;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;

public abstract class MessageDM extends Observable {
    public static final int DELIVERY_STATE_READ = 1;
    public static final int DELIVERY_STATE_SENT = 2;
    public static final int DELIVERY_STATE_UNREAD = 0;
    private static final String TAG = "Helpshift_MessageDM";
    public String authorId;
    public String authorName;
    public String body;
    public Long conversationLocalId;
    private String createdAt;
    public String createdRequestId;
    public int deliveryState;
    protected Domain domain;
    private long epochCreatedAtTime;
    public final boolean isAdminMessage;
    public boolean isMessageSeenSynced;
    public boolean isRedacted;
    public Long localId;
    public final MessageType messageType;
    protected Platform platform;
    public String readAt;
    public String seenAtMessageCursor;
    public String serverId;
    public boolean shouldShowAgentNameForConversation;
    private final UIViewState uiViewState = new UIViewState();

    public abstract boolean isUISupportedMessage();

    MessageDM(String str, String str2, long j, String str3, boolean z, MessageType messageType2) {
        this.body = str;
        this.createdAt = str2;
        this.epochCreatedAtTime = j;
        this.authorName = str3;
        this.isAdminMessage = z;
        this.messageType = messageType2;
    }

    public void setDependencies(Domain domain2, Platform platform2) {
        this.domain = domain2;
        this.platform = platform2;
    }

    public String getSubText() {
        Date date;
        Locale currentLocale = this.domain.getLocaleProviderDM().getCurrentLocale();
        try {
            date = HSDateFormatSpec.getDateFormatter(HSDateFormatSpec.STORAGE_TIME_PATTERN, currentLocale, "GMT").parse(getCreatedAt());
        } catch (ParseException e) {
            Date date2 = new Date();
            HSLogger.d(TAG, "getSubText : ParseException", e);
            date = date2;
        }
        String format = HSDateFormatSpec.getDateFormatter(this.platform.getDevice().is24HourFormat() ? HSDateFormatSpec.DISPLAY_TIME_PATTERN_24HR : HSDateFormatSpec.DISPLAY_TIME_PATTERN_12HR, currentLocale).format(date);
        String displayedAuthorName = getDisplayedAuthorName();
        if (StringUtils.isEmpty(displayedAuthorName)) {
            return format;
        }
        return displayedAuthorName + TableSearchToken.COMMA_SEP + format;
    }

    public String getAccessbilityMessageTime() {
        Locale currentLocale = this.domain.getLocaleProviderDM().getCurrentLocale();
        Date date = new Date(getEpochCreatedAtTime());
        String format = HSDateFormatSpec.getDateFormatter(this.platform.getDevice().is24HourFormat() ? HSDateFormatSpec.DISPLAY_TIME_PATTERN_24HR : HSDateFormatSpec.DISPLAY_TIME_PATTERN_12HR, currentLocale).format(date);
        String format2 = HSDateFormatSpec.getDateFormatter(HSDateFormatSpec.DISPLAY_DATE_PATTERN, currentLocale).format(date);
        return format + " " + format2;
    }

    public String getDisplayedAuthorName() {
        if (!this.isAdminMessage || !this.shouldShowAgentNameForConversation || !this.domain.getSDKConfigurationDM().getBoolean(SDKConfigurationDM.SHOW_AGENT_NAME) || StringUtils.isEmpty(this.authorName)) {
            return null;
        }
        return this.authorName.trim();
    }

    /* access modifiers changed from: package-private */
    public void notifyUpdated() {
        setChanged();
        notifyObservers();
    }

    public void mergeAndNotify(MessageDM messageDM) {
        merge(messageDM);
        notifyUpdated();
    }

    public void merge(MessageDM messageDM) {
        this.body = messageDM.body;
        this.createdAt = messageDM.getCreatedAt();
        this.epochCreatedAtTime = messageDM.getEpochCreatedAtTime();
        this.authorName = messageDM.authorName;
        if (StringUtils.isEmpty(this.serverId)) {
            this.serverId = messageDM.serverId;
        }
        this.isRedacted = messageDM.isRedacted;
    }

    /* access modifiers changed from: package-private */
    public Network getSendMessageNetwork(String str) {
        return new GuardOKNetwork(new GuardAgainstConversationArchivalNetwork(new AuthenticationFailureNetwork(new UserPreConditionsFailedNetwork(new TSCorrectedNetwork(new IdempotentNetwork(new POSTNetwork(str, this.domain, this.platform), this.platform, getIdempotentPolicy(), str, String.valueOf(this.localId)), this.platform)))));
    }

    /* access modifiers changed from: package-private */
    public String getPreIssueSendMessageRoute(ConversationServerInfo conversationServerInfo) {
        return "/preissues/" + conversationServerInfo.getPreIssueId() + "/messages/";
    }

    /* access modifiers changed from: package-private */
    public String getIssueSendMessageRoute(ConversationServerInfo conversationServerInfo) {
        return "/issues/" + conversationServerInfo.getIssueId() + "/messages/";
    }

    public UIViewState getUiViewState() {
        return this.uiViewState;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String str) {
        if (!StringUtils.isEmpty(str)) {
            this.createdAt = str;
        }
    }

    public long getEpochCreatedAtTime() {
        return this.epochCreatedAtTime;
    }

    public void setEpochCreatedAtTime(long j) {
        this.epochCreatedAtTime = j;
    }

    /* access modifiers changed from: protected */
    public IdempotentPolicy getIdempotentPolicy() {
        return new SuccessOrNonRetriableStatusCodeIdempotentPolicy();
    }
}
