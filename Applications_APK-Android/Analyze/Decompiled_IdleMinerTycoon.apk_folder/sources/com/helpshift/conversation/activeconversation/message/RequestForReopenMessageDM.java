package com.helpshift.conversation.activeconversation.message;

public class RequestForReopenMessageDM extends MessageDM {
    private boolean isAnswered;

    public RequestForReopenMessageDM(String str, String str2, String str3, long j, String str4) {
        super(str2, str3, j, str4, true, MessageType.REQUEST_FOR_REOPEN);
        this.serverId = str;
    }

    public boolean isAnswered() {
        return this.isAnswered;
    }

    public void setAnswered(boolean z) {
        this.isAnswered = z;
    }

    public void setAnsweredAndNotify(boolean z) {
        if (this.isAnswered != z) {
            setAnswered(z);
            notifyUpdated();
        }
    }

    public boolean isUISupportedMessage() {
        return this.isAnswered;
    }
}
