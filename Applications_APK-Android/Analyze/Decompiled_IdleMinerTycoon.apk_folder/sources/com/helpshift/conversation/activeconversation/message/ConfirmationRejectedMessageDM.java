package com.helpshift.conversation.activeconversation.message;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.network.NetworkDataRequestUtil;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.conversation.activeconversation.ConversationServerInfo;
import java.util.HashMap;

public class ConfirmationRejectedMessageDM extends AutoRetriableMessageDM {
    public boolean isUISupportedMessage() {
        return true;
    }

    public ConfirmationRejectedMessageDM(String str, String str2, long j, String str3, int i) {
        super(str, str2, j, str3, false, MessageType.CONFIRMATION_REJECTED, i);
    }

    public void send(UserDM userDM, ConversationServerInfo conversationServerInfo) {
        if (!StringUtils.isEmpty(conversationServerInfo.getIssueId())) {
            HashMap<String, String> userRequestData = NetworkDataRequestUtil.getUserRequestData(userDM);
            userRequestData.put("body", this.body);
            userRequestData.put("type", "ncr");
            userRequestData.put("refers", "");
            try {
                ConfirmationRejectedMessageDM parseConfirmationRejectedMessageDM = this.platform.getResponseParser().parseConfirmationRejectedMessageDM(makeNetworkRequest(getIssueSendMessageRoute(conversationServerInfo), userRequestData).responseString);
                merge(parseConfirmationRejectedMessageDM);
                this.serverId = parseConfirmationRejectedMessageDM.serverId;
                this.authorId = parseConfirmationRejectedMessageDM.authorId;
                notifyUpdated();
                this.platform.getConversationDAO().insertOrUpdateMessage(this);
            } catch (RootAPIException e) {
                if (e.exceptionType == NetworkException.INVALID_AUTH_TOKEN || e.exceptionType == NetworkException.AUTH_TOKEN_NOT_PROVIDED) {
                    this.domain.getAuthenticationFailureDM().notifyAuthenticationFailure(userDM, e.exceptionType);
                }
                throw e;
            }
        } else {
            throw new UnsupportedOperationException("ConfirmationRejectedMessageDM send called with conversation in pre issue mode.");
        }
    }
}
