package com.helpshift.conversation.activeconversation.message.input;

import android.support.annotation.NonNull;
import java.util.List;

public class OptionInput extends Input {
    public final List<Option> options;
    public final Type type;

    public OptionInput(String str, boolean z, String str2, String str3, List<Option> list, Type type2) {
        super(str, z, str2, str3);
        this.options = list;
        this.type = type2;
    }

    public static class Option {
        public final String jsonData;
        public final String title;

        public Option(String str, String str2) {
            this.title = str;
            this.jsonData = str2;
        }

        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof Option)) {
                return false;
            }
            Option option = (Option) obj;
            if (!option.title.equals(this.title) || !option.jsonData.equals(this.jsonData)) {
                return false;
            }
            return true;
        }
    }

    public enum Type {
        PILL("pill"),
        PICKER("picker");
        
        private final String optionInputType;

        private Type(String str) {
            this.optionInputType = str;
        }

        @NonNull
        public String toString() {
            return this.optionInputType;
        }

        public static Type getType(String str, int i) {
            if ("pill".equals(str)) {
                return PILL;
            }
            if ("picker".equals(str)) {
                return PICKER;
            }
            if (i <= 5) {
                return PILL;
            }
            return PICKER;
        }
    }
}
