package com.helpshift.conversation.activeconversation;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.ListUtils;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.util.HSListObserver;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.helpshift.conversation.ConversationUtil;
import com.helpshift.conversation.IssueType;
import com.helpshift.conversation.activeconversation.LiveUpdateDM;
import com.helpshift.conversation.activeconversation.message.AdminAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AdminImageAttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.AttachmentMessageDM;
import com.helpshift.conversation.activeconversation.message.MessageDM;
import com.helpshift.conversation.activeconversation.message.ScreenshotMessageDM;
import com.helpshift.conversation.activeconversation.model.Conversation;
import com.helpshift.conversation.dto.IssueState;
import com.helpshift.conversation.loaders.ConversationsLoader;
import com.helpshift.conversation.viewmodel.ConversationVMCallback;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class ViewableConversation implements ConversationDMListener, LiveUpdateDM.TypingIndicatorListener, ConversationsLoader.LoadMoreConversationsCallback {
    protected ConversationsLoader conversationLoader;
    protected ConversationManager conversationManager;
    private ConversationVMCallback conversationVMCallback;
    protected Domain domain;
    private AtomicBoolean isLoadMoreInProgress = new AtomicBoolean(false);
    protected LiveUpdateDM liveUpdateDM;
    protected Platform platform;
    private SDKConfigurationDM sdkConfigurationDM;
    protected UserDM userDM;

    public enum ConversationType {
        HISTORY,
        SINGLE
    }

    public abstract Conversation getActiveConversation();

    public abstract List<Conversation> getAllConversations();

    public abstract PaginationCursor getPaginationCursor();

    public abstract ConversationType getType();

    public abstract void init();

    public abstract void initializeConversationsForUI();

    public abstract void onNewConversationStarted(Conversation conversation);

    public abstract void prependConversations(List<Conversation> list);

    public abstract void registerMessagesObserver(HSListObserver<MessageDM> hSListObserver);

    public abstract boolean shouldOpen();

    public ViewableConversation(Platform platform2, Domain domain2, UserDM userDM2, ConversationsLoader conversationsLoader, ConversationManager conversationManager2) {
        this.platform = platform2;
        this.domain = domain2;
        this.userDM = userDM2;
        this.conversationLoader = conversationsLoader;
        this.sdkConfigurationDM = domain2.getSDKConfigurationDM();
        this.conversationManager = conversationManager2;
    }

    public void setLiveUpdateDM(LiveUpdateDM liveUpdateDM2) {
        this.liveUpdateDM = liveUpdateDM2;
    }

    public boolean isVisibleOnUI() {
        return this.conversationVMCallback != null && this.conversationVMCallback.isVisibleOnUI();
    }

    public boolean isConversationVMAttached() {
        return this.conversationVMCallback != null;
    }

    public void onScreenshotMessageClicked(ScreenshotMessageDM screenshotMessageDM) {
        screenshotMessageDM.handleClick(this.conversationVMCallback);
    }

    public void onAdminAttachmentMessageClicked(AttachmentMessageDM attachmentMessageDM) {
        switch (attachmentMessageDM.messageType) {
            case ADMIN_IMAGE_ATTACHMENT:
                ((AdminImageAttachmentMessageDM) attachmentMessageDM).handleClick(this.conversationVMCallback);
                return;
            case ADMIN_ATTACHMENT:
                ((AdminAttachmentMessageDM) attachmentMessageDM).handleClick(this.conversationVMCallback);
                return;
            default:
                return;
        }
    }

    public void mergePreIssueForActiveConversation(Conversation conversation, ConversationUpdate conversationUpdate) {
        Conversation activeConversation = getActiveConversation();
        IssueState issueState = activeConversation.state;
        this.conversationManager.mergePreIssue(activeConversation, conversation, true, conversationUpdate);
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onConversationInboxPollSuccess();
        }
        IssueState issueState2 = activeConversation.state;
        if (issueState2 != issueState) {
            this.conversationManager.refreshConversationOnIssueStateUpdate(activeConversation);
            onIssueStatusChange(issueState2);
        }
    }

    public void mergeIssueForActiveConversation(Conversation conversation, ConversationUpdate conversationUpdate) {
        Conversation activeConversation = getActiveConversation();
        IssueState issueState = activeConversation.state;
        String str = activeConversation.issueType;
        this.conversationManager.mergeIssue(activeConversation, conversation, true, conversationUpdate);
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onConversationInboxPollSuccess();
        }
        if (IssueType.PRE_ISSUE.equals(str) && IssueType.ISSUE.equals(activeConversation.issueType)) {
            startLiveUpdates();
        }
        IssueState issueState2 = activeConversation.state;
        if (issueState2 != issueState) {
            this.conversationManager.refreshConversationOnIssueStateUpdate(activeConversation);
            boolean z = false;
            boolean z2 = ConversationUtil.isInProgressState(issueState2) && ConversationUtil.isInProgressState(issueState);
            if (issueState == IssueState.COMPLETED_ISSUE_CREATED) {
                z = true;
            }
            if (z || !z2) {
                onIssueStatusChange(issueState2);
            }
        }
    }

    public boolean checkForReopen(int i, String str, boolean z) {
        Conversation activeConversation = getActiveConversation();
        boolean checkForReOpen = this.conversationManager.checkForReOpen(activeConversation, i, str, z);
        if (checkForReOpen) {
            this.conversationManager.refreshConversationOnIssueStateUpdate(activeConversation);
            onIssueStatusChange(activeConversation.state);
        }
        return checkForReOpen;
    }

    public boolean isActiveConversationEqual(Conversation conversation) {
        Conversation activeConversation;
        if (conversation == null || (activeConversation = getActiveConversation()) == null) {
            return false;
        }
        if (!StringUtils.isEmpty(activeConversation.serverId)) {
            return activeConversation.serverId.equals(conversation.serverId);
        }
        if (!StringUtils.isEmpty(activeConversation.preConversationServerId)) {
            return activeConversation.preConversationServerId.equals(conversation.preConversationServerId);
        }
        return false;
    }

    public void handlePreIssueCreationSuccess() {
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.handlePreIssueCreationSuccess();
        }
    }

    public void onIssueStatusChange(IssueState issueState) {
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onIssueStatusChange(issueState);
        }
    }

    public void startLiveUpdates() {
        Conversation activeConversation = getActiveConversation();
        if (this.liveUpdateDM != null && !activeConversation.isInPreIssueMode() && this.sdkConfigurationDM.shouldEnableTypingIndicator()) {
            this.liveUpdateDM.registerListener(this, activeConversation.serverId);
        }
    }

    public void stopLiveUpdates() {
        if (this.liveUpdateDM != null) {
            this.liveUpdateDM.unregisterListener();
        }
    }

    public boolean isAgentTyping() {
        return this.liveUpdateDM != null && this.liveUpdateDM.isAgentTyping() && this.sdkConfigurationDM.shouldEnableTypingIndicator();
    }

    public void onAgentTypingUpdate(boolean z) {
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onAgentTypingUpdate(z);
        }
    }

    public ConversationVMCallback getConversationVMCallback() {
        return this.conversationVMCallback;
    }

    public void setConversationVMCallback(ConversationVMCallback conversationVMCallback2) {
        this.conversationVMCallback = conversationVMCallback2;
        getActiveConversation().setListener(this);
    }

    public void unregisterConversationVMCallback() {
        this.conversationVMCallback = null;
        getActiveConversation().setListener(null);
    }

    public boolean hasMoreMessages() {
        return this.conversationLoader.hasMoreMessages();
    }

    public List<UIConversation> getUIConversations() {
        List<Conversation> allConversations = getAllConversations();
        ArrayList arrayList = new ArrayList();
        if (ListUtils.isEmpty(allConversations)) {
            return arrayList;
        }
        int size = allConversations.size();
        for (int i = 0; i < size; i++) {
            Conversation conversation = allConversations.get(i);
            arrayList.add(new UIConversation(conversation.localId.longValue(), i, conversation.getCreatedAt(), conversation.getEpochCreatedAtTime(), conversation.publishId, conversation.isInPreIssueMode(), conversation.state, conversation.isRedacted));
        }
        return arrayList;
    }

    public void loadMoreMessages() {
        if (this.isLoadMoreInProgress.compareAndSet(false, true)) {
            this.conversationLoader.loadMoreConversations(getPaginationCursor(), this);
        }
    }

    public void onSuccess(List<Conversation> list, boolean z) {
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onHistoryLoadingSuccess();
        }
        if (ListUtils.isEmpty(list)) {
            this.isLoadMoreInProgress.set(false);
            if (this.conversationVMCallback != null) {
                this.conversationVMCallback.prependConversations(new ArrayList(), z);
                return;
            }
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (Conversation next : list) {
            next.userLocalId = this.userDM.getLocalId().longValue();
            this.conversationManager.initializeMessageListForUI(next, next.messageDMs, isActiveConversationEqual(next) && this.conversationManager.shouldEnableMessagesClick(getActiveConversation()));
            arrayList.add(next);
        }
        prependConversations(arrayList);
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.prependConversations(arrayList, z);
        }
        this.isLoadMoreInProgress.set(false);
    }

    public void onError() {
        this.isLoadMoreInProgress.set(false);
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onHistoryLoadingError();
        }
    }

    public void loading() {
        this.isLoadMoreInProgress.set(false);
        if (this.conversationVMCallback != null) {
            this.conversationVMCallback.onHistoryLoadingStarted();
        }
    }

    /* access modifiers changed from: protected */
    public PaginationCursor buildPaginationCursor(Conversation conversation) {
        String str;
        if (conversation == null) {
            return null;
        }
        String createdAt = conversation.getCreatedAt();
        if (ListUtils.isEmpty(conversation.messageDMs)) {
            str = createdAt;
        } else {
            str = ((MessageDM) conversation.messageDMs.get(0)).getCreatedAt();
        }
        return new PaginationCursor(createdAt, str);
    }
}
