package com.helpshift.conversation.activeconversation.message.input;

public class Input {
    public final String botInfo;
    public final String inputLabel;
    public final boolean required;
    public final String skipLabel;

    public Input(String str, boolean z, String str2, String str3) {
        this.botInfo = str;
        this.required = z;
        this.inputLabel = str2;
        this.skipLabel = str3;
    }
}
