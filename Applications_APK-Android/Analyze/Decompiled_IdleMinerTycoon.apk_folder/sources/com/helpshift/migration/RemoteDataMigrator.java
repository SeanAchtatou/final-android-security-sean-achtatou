package com.helpshift.migration;

import com.helpshift.account.domainmodel.UserDM;
import com.helpshift.common.AutoRetryFailedEventDM;
import com.helpshift.common.StringUtils;
import com.helpshift.common.domain.Domain;
import com.helpshift.common.domain.F;
import com.helpshift.common.domain.network.FailedAPICallNetworkDecorator;
import com.helpshift.common.domain.network.GuardOKNetwork;
import com.helpshift.common.domain.network.POSTNetwork;
import com.helpshift.common.domain.network.TSCorrectedNetwork;
import com.helpshift.common.domain.network.UserNotFoundNetwork;
import com.helpshift.common.domain.network.UserPreConditionsFailedNetwork;
import com.helpshift.common.exception.NetworkException;
import com.helpshift.common.exception.RootAPIException;
import com.helpshift.common.platform.Platform;
import com.helpshift.common.platform.network.RequestData;
import com.helpshift.migration.legacyUser.LegacyProfile;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class RemoteDataMigrator {
    /* access modifiers changed from: private */
    public Domain domain;
    private LegacyProfileMigrationDAO legacyProfileMigrationDAO;
    private Platform platform;
    /* access modifiers changed from: private */
    public WeakReference<RemoteDataMigratorListener> remoteDataMigratorListener;
    /* access modifiers changed from: private */
    public UserDM userDM;

    public interface RemoteDataMigratorListener {
        void onMigrationStateChanged(UserDM userDM, MigrationState migrationState, MigrationState migrationState2);
    }

    public RemoteDataMigrator(Platform platform2, Domain domain2, UserDM userDM2, RemoteDataMigratorListener remoteDataMigratorListener2) {
        this.platform = platform2;
        this.domain = domain2;
        this.userDM = userDM2;
        this.remoteDataMigratorListener = new WeakReference<>(remoteDataMigratorListener2);
        this.legacyProfileMigrationDAO = platform2.getLegacyUserMigrationDataSource();
    }

    public void setAppropriateInitialState() {
        if (getProfileMigrationState() == MigrationState.IN_PROGRESS) {
            updateProfileMigrationStateUpdate(MigrationState.IN_PROGRESS, MigrationState.NOT_STARTED);
        }
    }

    public MigrationState getProfileMigrationState() {
        if (StringUtils.isEmpty(this.userDM.getIdentifier())) {
            return MigrationState.COMPLETED;
        }
        LegacyProfile fetchLegacyProfile = this.legacyProfileMigrationDAO.fetchLegacyProfile(this.userDM.getIdentifier());
        if (fetchLegacyProfile == null) {
            return MigrationState.COMPLETED;
        }
        return fetchLegacyProfile.migrationState;
    }

    public void startProfileMigration() {
        MigrationState profileMigrationState = getProfileMigrationState();
        if (profileMigrationState != MigrationState.COMPLETED && profileMigrationState != MigrationState.IN_PROGRESS) {
            this.domain.runParallel(new F() {
                public void f() {
                    try {
                        RemoteDataMigrator.this.runRemoteMigrationInternal();
                    } catch (RootAPIException e) {
                        RemoteDataMigrator.this.domain.getAutoRetryFailedEventDM().scheduleRetryTaskForEventType(AutoRetryFailedEventDM.EventType.MIGRATION, e.getServerStatusCode());
                        throw e;
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void runRemoteMigrationInternal() {
        LegacyProfile fetchLegacyProfile;
        MigrationState profileMigrationState = getProfileMigrationState();
        if (profileMigrationState != MigrationState.COMPLETED && profileMigrationState != MigrationState.IN_PROGRESS && (fetchLegacyProfile = this.legacyProfileMigrationDAO.fetchLegacyProfile(this.userDM.getIdentifier())) != null) {
            MigrationState migrationState = fetchLegacyProfile.migrationState;
            if (migrationState == MigrationState.NOT_STARTED || migrationState == MigrationState.FAILED) {
                GuardOKNetwork guardOKNetwork = new GuardOKNetwork(new FailedAPICallNetworkDecorator(new UserNotFoundNetwork(new UserPreConditionsFailedNetwork(new TSCorrectedNetwork(new POSTNetwork("/migrate-profile/", this.domain, this.platform), this.platform)))));
                HashMap hashMap = new HashMap();
                hashMap.put("profile-id", fetchLegacyProfile.serverId);
                hashMap.put("did", this.userDM.getDeviceId());
                if (!StringUtils.isEmpty(this.userDM.getIdentifier())) {
                    hashMap.put("uid", this.userDM.getIdentifier());
                }
                if (!StringUtils.isEmpty(this.userDM.getEmail())) {
                    hashMap.put("email", this.userDM.getEmail());
                }
                updateProfileMigrationStateUpdate(migrationState, MigrationState.IN_PROGRESS);
                try {
                    guardOKNetwork.makeRequest(new RequestData(hashMap));
                    updateProfileMigrationStateUpdate(migrationState, MigrationState.COMPLETED);
                } catch (RootAPIException e) {
                    if (e.exceptionType == NetworkException.USER_PRE_CONDITION_FAILED || e.exceptionType == NetworkException.USER_NOT_FOUND) {
                        updateProfileMigrationStateUpdate(migrationState, MigrationState.COMPLETED);
                    } else if (e.exceptionType == NetworkException.NON_RETRIABLE) {
                        updateProfileMigrationStateUpdate(migrationState, MigrationState.COMPLETED);
                    } else {
                        updateProfileMigrationStateUpdate(migrationState, MigrationState.FAILED);
                        throw e;
                    }
                }
            }
        }
    }

    private void updateProfileMigrationStateUpdate(final MigrationState migrationState, final MigrationState migrationState2) {
        if (migrationState2 == MigrationState.COMPLETED) {
            this.legacyProfileMigrationDAO.deleteLegacyProfile(this.userDM.getIdentifier());
        } else {
            this.legacyProfileMigrationDAO.updateMigrationState(this.userDM.getIdentifier(), migrationState2);
        }
        this.domain.runSerial(new F() {
            public void f() {
                if (RemoteDataMigrator.this.remoteDataMigratorListener.get() != null) {
                    ((RemoteDataMigratorListener) RemoteDataMigrator.this.remoteDataMigratorListener.get()).onMigrationStateChanged(RemoteDataMigrator.this.userDM, migrationState, migrationState2);
                }
            }
        });
    }

    public void retry() {
        runRemoteMigrationInternal();
    }
}
