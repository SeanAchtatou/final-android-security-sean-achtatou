package com.onesignal;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.helpshift.common.domain.network.NetworkConstants;
import com.onesignal.AndroidSupportV4Compat;
import com.onesignal.OneSignal;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

class LocationGMS {
    private static final long BACKGROUND_UPDATE_TIME_MS = 570000;
    private static final long FOREGROUND_UPDATE_TIME_MS = 270000;
    private static final long TIME_BACKGROUND_SEC = 600;
    private static final long TIME_FOREGROUND_SEC = 300;
    /* access modifiers changed from: private */
    public static Context classContext;
    private static Thread fallbackFailThread;
    private static boolean locationCoarse;
    private static LocationHandlerThread locationHandlerThread;
    private static ConcurrentHashMap<CALLBACK_TYPE, LocationHandler> locationHandlers = new ConcurrentHashMap<>();
    static LocationUpdateListener locationUpdateListener;
    /* access modifiers changed from: private */
    public static GoogleApiClientCompatProxy mGoogleApiClient;
    /* access modifiers changed from: private */
    public static Location mLastLocation;
    static String requestPermission;
    protected static final Object syncLock = new Object() {
    };

    enum CALLBACK_TYPE {
        STARTUP,
        PROMPT_LOCATION,
        SYNC_SERVICE
    }

    interface LocationHandler {
        void complete(LocationPoint locationPoint);

        CALLBACK_TYPE getType();
    }

    /* access modifiers changed from: private */
    public static int getApiFallbackWait() {
        return NetworkConstants.UPLOAD_CONNECT_TIMEOUT;
    }

    LocationGMS() {
    }

    static class LocationPoint {
        Float accuracy;
        Boolean bg;
        Double lat;
        Double log;
        Long timeStamp;
        Integer type;

        LocationPoint() {
        }
    }

    static boolean scheduleUpdate(Context context) {
        if (!hasLocationPermission(context) || !OneSignal.shareLocation) {
            return false;
        }
        OneSignalSyncServiceUtils.scheduleLocationUpdateTask(context, ((OneSignal.isForeground() ? TIME_FOREGROUND_SEC : TIME_BACKGROUND_SEC) * 1000) - (System.currentTimeMillis() - getLastLocationTime()));
        return true;
    }

    private static void setLastLocationTime(long j) {
        OneSignalPrefs.saveLong(OneSignalPrefs.PREFS_ONESIGNAL, "OS_LAST_LOCATION_TIME", j);
    }

    private static long getLastLocationTime() {
        return OneSignalPrefs.getLong(OneSignalPrefs.PREFS_ONESIGNAL, "OS_LAST_LOCATION_TIME", -600000);
    }

    private static boolean hasLocationPermission(Context context) {
        return AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 || AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0;
    }

    static void getLocation(Context context, boolean z, LocationHandler locationHandler) {
        classContext = context;
        locationHandlers.put(locationHandler.getType(), locationHandler);
        if (!OneSignal.shareLocation) {
            fireFailedComplete();
            return;
        }
        int checkSelfPermission = AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION");
        int i = -1;
        if (checkSelfPermission == -1) {
            i = AndroidSupportV4Compat.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION");
            locationCoarse = true;
        }
        if (Build.VERSION.SDK_INT < 23) {
            if (checkSelfPermission == 0 || i == 0) {
                startGetLocation();
            } else {
                locationHandler.complete(null);
            }
        } else if (checkSelfPermission != 0) {
            try {
                List asList = Arrays.asList(context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions);
                if (asList.contains("android.permission.ACCESS_FINE_LOCATION")) {
                    requestPermission = "android.permission.ACCESS_FINE_LOCATION";
                } else if (asList.contains("android.permission.ACCESS_COARSE_LOCATION") && i != 0) {
                    requestPermission = "android.permission.ACCESS_COARSE_LOCATION";
                }
                if (requestPermission != null && z) {
                    PermissionsActivity.startPrompt();
                } else if (i == 0) {
                    startGetLocation();
                } else {
                    fireFailedComplete();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else {
            startGetLocation();
        }
    }

    static void startGetLocation() {
        if (fallbackFailThread == null) {
            try {
                synchronized (syncLock) {
                    startFallBackThread();
                    if (locationHandlerThread == null) {
                        locationHandlerThread = new LocationHandlerThread();
                    }
                    if (mGoogleApiClient != null) {
                        if (mLastLocation != null) {
                            if (mLastLocation != null) {
                                fireCompleteForLocation(mLastLocation);
                            }
                        }
                    }
                    GoogleApiClientListener googleApiClientListener = new GoogleApiClientListener();
                    mGoogleApiClient = new GoogleApiClientCompatProxy(new GoogleApiClient.Builder(classContext).addApi(LocationServices.API).addConnectionCallbacks(googleApiClientListener).addOnConnectionFailedListener(googleApiClientListener).setHandler(locationHandlerThread.mHandler).build());
                    mGoogleApiClient.connect();
                }
            } catch (Throwable th) {
                OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "Location permission exists but there was an error initializing: ", th);
                fireFailedComplete();
            }
        }
    }

    private static void startFallBackThread() {
        fallbackFailThread = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep((long) LocationGMS.getApiFallbackWait());
                    OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "Location permission exists but GoogleApiClient timed out. Maybe related to mismatch google-play aar versions.");
                    LocationGMS.fireFailedComplete();
                    LocationGMS.scheduleUpdate(LocationGMS.classContext);
                } catch (InterruptedException unused) {
                }
            }
        }, "OS_GMS_LOCATION_FALLBACK");
        fallbackFailThread.start();
    }

    static void fireFailedComplete() {
        PermissionsActivity.answered = false;
        synchronized (syncLock) {
            if (mGoogleApiClient != null) {
                mGoogleApiClient.disconnect();
            }
            mGoogleApiClient = null;
        }
        fireComplete(null);
    }

    private static void fireComplete(LocationPoint locationPoint) {
        Thread thread;
        HashMap hashMap = new HashMap();
        synchronized (LocationGMS.class) {
            hashMap.putAll(locationHandlers);
            locationHandlers.clear();
            thread = fallbackFailThread;
        }
        for (CALLBACK_TYPE callback_type : hashMap.keySet()) {
            ((LocationHandler) hashMap.get(callback_type)).complete(locationPoint);
        }
        if (thread != null && !Thread.currentThread().equals(thread)) {
            thread.interrupt();
        }
        if (thread == fallbackFailThread) {
            synchronized (LocationGMS.class) {
                if (thread == fallbackFailThread) {
                    fallbackFailThread = null;
                }
            }
        }
        setLastLocationTime(System.currentTimeMillis());
    }

    /* access modifiers changed from: private */
    public static void fireCompleteForLocation(Location location) {
        LocationPoint locationPoint = new LocationPoint();
        locationPoint.accuracy = Float.valueOf(location.getAccuracy());
        locationPoint.bg = Boolean.valueOf(!OneSignal.isForeground());
        locationPoint.type = Integer.valueOf(locationCoarse ^ true ? 1 : 0);
        locationPoint.timeStamp = Long.valueOf(location.getTime());
        if (locationCoarse) {
            locationPoint.lat = Double.valueOf(new BigDecimal(location.getLatitude()).setScale(7, RoundingMode.HALF_UP).doubleValue());
            locationPoint.log = Double.valueOf(new BigDecimal(location.getLongitude()).setScale(7, RoundingMode.HALF_UP).doubleValue());
        } else {
            locationPoint.lat = Double.valueOf(location.getLatitude());
            locationPoint.log = Double.valueOf(location.getLongitude());
        }
        fireComplete(locationPoint);
        scheduleUpdate(classContext);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void onFocusChange() {
        /*
            java.lang.Object r0 = com.onesignal.LocationGMS.syncLock
            monitor-enter(r0)
            com.onesignal.GoogleApiClientCompatProxy r1 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x0030 }
            if (r1 == 0) goto L_0x002e
            com.onesignal.GoogleApiClientCompatProxy r1 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x0030 }
            com.google.android.gms.common.api.GoogleApiClient r1 = r1.realInstance()     // Catch:{ all -> 0x0030 }
            boolean r1 = r1.isConnected()     // Catch:{ all -> 0x0030 }
            if (r1 != 0) goto L_0x0014
            goto L_0x002e
        L_0x0014:
            com.onesignal.GoogleApiClientCompatProxy r1 = com.onesignal.LocationGMS.mGoogleApiClient     // Catch:{ all -> 0x0030 }
            com.google.android.gms.common.api.GoogleApiClient r1 = r1.realInstance()     // Catch:{ all -> 0x0030 }
            com.onesignal.LocationGMS$LocationUpdateListener r2 = com.onesignal.LocationGMS.locationUpdateListener     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0025
            com.google.android.gms.location.FusedLocationProviderApi r2 = com.google.android.gms.location.LocationServices.FusedLocationApi     // Catch:{ all -> 0x0030 }
            com.onesignal.LocationGMS$LocationUpdateListener r3 = com.onesignal.LocationGMS.locationUpdateListener     // Catch:{ all -> 0x0030 }
            r2.removeLocationUpdates(r1, r3)     // Catch:{ all -> 0x0030 }
        L_0x0025:
            com.onesignal.LocationGMS$LocationUpdateListener r2 = new com.onesignal.LocationGMS$LocationUpdateListener     // Catch:{ all -> 0x0030 }
            r2.<init>(r1)     // Catch:{ all -> 0x0030 }
            com.onesignal.LocationGMS.locationUpdateListener = r2     // Catch:{ all -> 0x0030 }
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            return
        L_0x002e:
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            return
        L_0x0030:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0030 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.LocationGMS.onFocusChange():void");
    }

    private static class GoogleApiClientListener implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private GoogleApiClientListener() {
        }

        public void onConnected(Bundle bundle) {
            synchronized (LocationGMS.syncLock) {
                PermissionsActivity.answered = false;
                if (LocationGMS.mLastLocation == null) {
                    Location unused = LocationGMS.mLastLocation = FusedLocationApiWrapper.getLastLocation(LocationGMS.mGoogleApiClient.realInstance());
                    if (LocationGMS.mLastLocation != null) {
                        LocationGMS.fireCompleteForLocation(LocationGMS.mLastLocation);
                    }
                }
                LocationGMS.locationUpdateListener = new LocationUpdateListener(LocationGMS.mGoogleApiClient.realInstance());
            }
        }

        public void onConnectionSuspended(int i) {
            LocationGMS.fireFailedComplete();
        }

        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            LocationGMS.fireFailedComplete();
        }
    }

    static class LocationUpdateListener implements LocationListener {
        private GoogleApiClient mGoogleApiClient;

        LocationUpdateListener(GoogleApiClient googleApiClient) {
            this.mGoogleApiClient = googleApiClient;
            long j = OneSignal.isForeground() ? LocationGMS.FOREGROUND_UPDATE_TIME_MS : LocationGMS.BACKGROUND_UPDATE_TIME_MS;
            LocationRequest interval = LocationRequest.create().setFastestInterval(j).setInterval(j);
            double d = (double) j;
            Double.isNaN(d);
            FusedLocationApiWrapper.requestLocationUpdates(this.mGoogleApiClient, interval.setMaxWaitTime((long) (d * 1.5d)).setPriority(102), this);
        }

        public void onLocationChanged(Location location) {
            Location unused = LocationGMS.mLastLocation = location;
            OneSignal.Log(OneSignal.LOG_LEVEL.INFO, "Location Change Detected");
        }
    }

    static class FusedLocationApiWrapper {
        FusedLocationApiWrapper() {
        }

        static void requestLocationUpdates(GoogleApiClient googleApiClient, LocationRequest locationRequest, LocationListener locationListener) {
            try {
                synchronized (LocationGMS.syncLock) {
                    if (googleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
                    }
                }
            } catch (Throwable th) {
                OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "FusedLocationApi.requestLocationUpdates failed!", th);
            }
        }

        static Location getLastLocation(GoogleApiClient googleApiClient) {
            synchronized (LocationGMS.syncLock) {
                if (!googleApiClient.isConnected()) {
                    return null;
                }
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                return lastLocation;
            }
        }
    }

    private static class LocationHandlerThread extends HandlerThread {
        Handler mHandler = new Handler(getLooper());

        LocationHandlerThread() {
            super("OSH_LocationHandlerThread");
            start();
        }
    }
}
