package com.onesignal;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NotificationManagerCompat;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import org.json.JSONArray;
import org.json.JSONObject;

class NotificationOpenedProcessor {
    NotificationOpenedProcessor() {
    }

    static void processFromContext(Context context, Intent intent) {
        if (isOneSignalIntent(intent)) {
            OneSignal.setAppContext(context);
            handleDismissFromActionButtonPress(context, intent);
            processIntent(context, intent);
        }
    }

    private static boolean isOneSignalIntent(Intent intent) {
        return intent.hasExtra("onesignal_data") || intent.hasExtra("summary") || intent.hasExtra("notificationId");
    }

    private static void handleDismissFromActionButtonPress(Context context, Intent intent) {
        if (intent.getBooleanExtra("action_button", false)) {
            NotificationManagerCompat.from(context).cancel(intent.getIntExtra("notificationId", 0));
            context.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006b A[SYNTHETIC, Splitter:B:22:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081 A[SYNTHETIC, Splitter:B:33:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009b A[SYNTHETIC, Splitter:B:40:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void processIntent(android.content.Context r7, android.content.Intent r8) {
        /*
            java.lang.String r0 = "summary"
            java.lang.String r0 = r8.getStringExtra(r0)
            java.lang.String r1 = "dismissed"
            r2 = 0
            boolean r1 = r8.getBooleanExtra(r1, r2)
            r3 = 0
            if (r1 != 0) goto L_0x0043
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x003f }
            java.lang.String r5 = "onesignal_data"
            java.lang.String r5 = r8.getStringExtra(r5)     // Catch:{ Throwable -> 0x003f }
            r4.<init>(r5)     // Catch:{ Throwable -> 0x003f }
            java.lang.String r5 = "notificationId"
            java.lang.String r6 = "notificationId"
            int r6 = r8.getIntExtra(r6, r2)     // Catch:{ Throwable -> 0x003f }
            r4.put(r5, r6)     // Catch:{ Throwable -> 0x003f }
            java.lang.String r5 = "onesignal_data"
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x003f }
            r8.putExtra(r5, r4)     // Catch:{ Throwable -> 0x003f }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x003f }
            java.lang.String r5 = "onesignal_data"
            java.lang.String r5 = r8.getStringExtra(r5)     // Catch:{ Throwable -> 0x003f }
            r4.<init>(r5)     // Catch:{ Throwable -> 0x003f }
            org.json.JSONArray r4 = com.onesignal.NotificationBundleProcessor.newJsonArray(r4)     // Catch:{ Throwable -> 0x003f }
            goto L_0x0044
        L_0x003f:
            r4 = move-exception
            r4.printStackTrace()
        L_0x0043:
            r4 = r3
        L_0x0044:
            com.onesignal.OneSignalDbHelper r5 = com.onesignal.OneSignalDbHelper.getInstance(r7)
            android.database.sqlite.SQLiteDatabase r5 = r5.getWritableDbWithRetries()     // Catch:{ Exception -> 0x0077 }
            r5.beginTransaction()     // Catch:{ Exception -> 0x0071, all -> 0x006f }
            if (r1 != 0) goto L_0x0056
            if (r0 == 0) goto L_0x0056
            addChildNotifications(r4, r0, r5)     // Catch:{ Exception -> 0x0071, all -> 0x006f }
        L_0x0056:
            markNotificationsConsumed(r7, r8, r5)     // Catch:{ Exception -> 0x0071, all -> 0x006f }
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "grp"
            java.lang.String r0 = r8.getStringExtra(r0)     // Catch:{ Exception -> 0x0071, all -> 0x006f }
            if (r0 == 0) goto L_0x0066
            com.onesignal.NotificationSummaryManager.updateSummaryNotificationAfterChildRemoved(r7, r5, r0, r1)     // Catch:{ Exception -> 0x0071, all -> 0x006f }
        L_0x0066:
            r5.setTransactionSuccessful()     // Catch:{ Exception -> 0x0071, all -> 0x006f }
            if (r5 == 0) goto L_0x008d
            r5.endTransaction()     // Catch:{ Throwable -> 0x0085 }
            goto L_0x008d
        L_0x006f:
            r7 = move-exception
            goto L_0x0099
        L_0x0071:
            r0 = move-exception
            r3 = r5
            goto L_0x0078
        L_0x0074:
            r7 = move-exception
            r5 = r3
            goto L_0x0099
        L_0x0077:
            r0 = move-exception
        L_0x0078:
            com.onesignal.OneSignal$LOG_LEVEL r5 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0074 }
            java.lang.String r6 = "Error processing notification open or dismiss record! "
            com.onesignal.OneSignal.Log(r5, r6, r0)     // Catch:{ all -> 0x0074 }
            if (r3 == 0) goto L_0x008d
            r3.endTransaction()     // Catch:{ Throwable -> 0x0085 }
            goto L_0x008d
        L_0x0085:
            r0 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r3 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
            java.lang.String r5 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r3, r5, r0)
        L_0x008d:
            if (r1 != 0) goto L_0x0098
            java.lang.String r0 = "from_alert"
            boolean r8 = r8.getBooleanExtra(r0, r2)
            com.onesignal.OneSignal.handleNotificationOpen(r7, r4, r8)
        L_0x0098:
            return
        L_0x0099:
            if (r5 == 0) goto L_0x00a7
            r5.endTransaction()     // Catch:{ Throwable -> 0x009f }
            goto L_0x00a7
        L_0x009f:
            r8 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
            java.lang.String r1 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r0, r1, r8)
        L_0x00a7:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.NotificationOpenedProcessor.processIntent(android.content.Context, android.content.Intent):void");
    }

    private static void addChildNotifications(JSONArray jSONArray, String str, SQLiteDatabase sQLiteDatabase) {
        Cursor query = sQLiteDatabase.query(OneSignalDbContract.NotificationTable.TABLE_NAME, new String[]{OneSignalDbContract.NotificationTable.COLUMN_NAME_FULL_DATA}, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 0", new String[]{str}, null, null, null);
        if (query.getCount() > 1) {
            query.moveToFirst();
            do {
                try {
                    jSONArray.put(new JSONObject(query.getString(query.getColumnIndex(OneSignalDbContract.NotificationTable.COLUMN_NAME_FULL_DATA))));
                } catch (Throwable unused) {
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                    OneSignal.Log(log_level, "Could not parse JSON of sub notification in group: " + str);
                }
            } while (query.moveToNext());
        }
        query.close();
    }

    private static void markNotificationsConsumed(Context context, Intent intent, SQLiteDatabase sQLiteDatabase) {
        String[] strArr;
        String str;
        String stringExtra = intent.getStringExtra("summary");
        if (stringExtra != null) {
            str = "group_id = ?";
            strArr = new String[]{stringExtra};
        } else {
            str = "android_notification_id = " + intent.getIntExtra("notificationId", 0);
            strArr = null;
        }
        sQLiteDatabase.update(OneSignalDbContract.NotificationTable.TABLE_NAME, newContentValuesWithConsumed(intent), str, strArr);
        BadgeCountUpdater.update(sQLiteDatabase, context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static ContentValues newContentValuesWithConsumed(Intent intent) {
        ContentValues contentValues = new ContentValues();
        if (intent.getBooleanExtra(OneSignalDbContract.NotificationTable.COLUMN_NAME_DISMISSED, false)) {
            contentValues.put(OneSignalDbContract.NotificationTable.COLUMN_NAME_DISMISSED, (Integer) 1);
        } else {
            contentValues.put(OneSignalDbContract.NotificationTable.COLUMN_NAME_OPENED, (Integer) 1);
        }
        return contentValues;
    }
}
