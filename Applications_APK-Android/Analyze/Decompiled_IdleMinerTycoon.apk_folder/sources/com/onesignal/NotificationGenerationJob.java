package com.onesignal;

import android.content.Context;
import android.net.Uri;
import com.onesignal.NotificationExtenderService;
import java.security.SecureRandom;
import org.json.JSONObject;

class NotificationGenerationJob {
    Context context;
    JSONObject jsonPayload;
    Integer orgFlags;
    Uri orgSound;
    CharSequence overriddenBodyFromExtender;
    Integer overriddenFlags;
    Uri overriddenSound;
    CharSequence overriddenTitleFromExtender;
    NotificationExtenderService.OverrideSettings overrideSettings;
    boolean restoring;
    boolean showAsAlert;
    Long shownTimeStamp;

    NotificationGenerationJob(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: package-private */
    public CharSequence getTitle() {
        if (this.overriddenTitleFromExtender != null) {
            return this.overriddenTitleFromExtender;
        }
        return this.jsonPayload.optString("title", null);
    }

    /* access modifiers changed from: package-private */
    public CharSequence getBody() {
        if (this.overriddenBodyFromExtender != null) {
            return this.overriddenBodyFromExtender;
        }
        return this.jsonPayload.optString("alert", null);
    }

    /* access modifiers changed from: package-private */
    public Integer getAndroidId() {
        if (this.overrideSettings == null) {
            this.overrideSettings = new NotificationExtenderService.OverrideSettings();
        }
        if (this.overrideSettings.androidNotificationId == null) {
            this.overrideSettings.androidNotificationId = Integer.valueOf(new SecureRandom().nextInt());
        }
        return this.overrideSettings.androidNotificationId;
    }

    /* access modifiers changed from: package-private */
    public int getAndroidIdWithoutCreate() {
        if (this.overrideSettings == null || this.overrideSettings.androidNotificationId == null) {
            return -1;
        }
        return this.overrideSettings.androidNotificationId.intValue();
    }

    /* access modifiers changed from: package-private */
    public void setAndroidIdWithOutOverriding(Integer num) {
        if (num != null) {
            if (this.overrideSettings == null || this.overrideSettings.androidNotificationId == null) {
                if (this.overrideSettings == null) {
                    this.overrideSettings = new NotificationExtenderService.OverrideSettings();
                }
                this.overrideSettings.androidNotificationId = num;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasExtender() {
        return (this.overrideSettings == null || this.overrideSettings.extender == null) ? false : true;
    }
}
