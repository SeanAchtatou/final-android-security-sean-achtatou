package com.onesignal;

import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsCallback;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import java.security.SecureRandom;

class OneSignalChromeTab {
    private static boolean opened;

    OneSignalChromeTab() {
    }

    static void setup(Context context, String str, String str2, String str3) {
        if (!opened && !OneSignal.mEnterp && str2 != null) {
            try {
                Class.forName("android.support.customtabs.CustomTabsServiceConnection");
                String str4 = "?app_id=" + str + "&user_id=" + str2;
                if (str3 != null) {
                    str4 = str4 + "&ad_id=" + str3;
                }
                opened = CustomTabsClient.bindCustomTabsService(context, "com.android.chrome", new OneSignalCustomTabsServiceConnection(context, str4 + "&cbs_id=" + new SecureRandom().nextInt(Integer.MAX_VALUE)));
            } catch (ClassNotFoundException unused) {
            }
        }
    }

    private static class OneSignalCustomTabsServiceConnection extends CustomTabsServiceConnection {
        private Context mContext;
        private String mParams;

        public void onServiceDisconnected(ComponentName componentName) {
        }

        OneSignalCustomTabsServiceConnection(Context context, String str) {
            this.mContext = context;
            this.mParams = str;
        }

        public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
            if (customTabsClient != null) {
                customTabsClient.warmup(0);
                CustomTabsSession newSession = customTabsClient.newSession(new CustomTabsCallback() {
                    public void onNavigationEvent(int i, Bundle bundle) {
                        super.onNavigationEvent(i, bundle);
                    }

                    public void extraCallback(String str, Bundle bundle) {
                        super.extraCallback(str, bundle);
                    }
                });
                if (newSession != null) {
                    newSession.mayLaunchUrl(Uri.parse("https://onesignal.com/android_frame.html" + this.mParams), null, null);
                }
            }
        }
    }
}
