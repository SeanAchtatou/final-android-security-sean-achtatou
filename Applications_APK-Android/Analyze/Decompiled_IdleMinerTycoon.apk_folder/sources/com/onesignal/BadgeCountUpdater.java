package com.onesignal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import com.onesignal.shortcutbadger.ShortcutBadger;

class BadgeCountUpdater {
    private static int badgesEnabled = -1;

    BadgeCountUpdater() {
    }

    private static boolean areBadgeSettingsEnabled(Context context) {
        if (badgesEnabled == -1) {
            try {
                Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
                if (bundle != null) {
                    badgesEnabled = "DISABLE".equals(bundle.getString("com.onesignal.BadgeCount")) ^ true ? 1 : 0;
                } else {
                    badgesEnabled = 1;
                }
            } catch (Throwable th) {
                badgesEnabled = 0;
                OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Error reading meta-data tag 'com.onesignal.BadgeCount'. Disabling badge setting.", th);
            }
            if (badgesEnabled == 1) {
                return true;
            }
            return false;
        } else if (badgesEnabled == 1) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean areBadgesEnabled(Context context) {
        return areBadgeSettingsEnabled(context) && OSUtils.areNotificationsEnabled(context);
    }

    static void update(SQLiteDatabase sQLiteDatabase, Context context) {
        if (areBadgesEnabled(context)) {
            Cursor query = sQLiteDatabase.query(OneSignalDbContract.NotificationTable.TABLE_NAME, null, "dismissed = 0 AND opened = 0 AND is_summary = 0 ", null, null, null, null);
            updateCount(query.getCount(), context);
            query.close();
        }
    }

    static void updateCount(int i, Context context) {
        if (areBadgeSettingsEnabled(context)) {
            try {
                ShortcutBadger.applyCountOrThrow(context, i);
            } catch (Throwable unused) {
            }
        }
    }
}
