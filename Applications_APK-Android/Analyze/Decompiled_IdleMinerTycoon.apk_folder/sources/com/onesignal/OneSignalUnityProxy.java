package com.onesignal;

import android.app.Activity;
import com.onesignal.OneSignal;
import java.lang.reflect.Method;
import org.json.JSONObject;

public class OneSignalUnityProxy implements OneSignal.NotificationOpenedHandler, OneSignal.NotificationReceivedHandler, OSPermissionObserver, OSSubscriptionObserver, OSEmailSubscriptionObserver {
    private static String unityListenerName;
    private static Method unitySendMessage;

    public OneSignalUnityProxy(String str, String str2, String str3, int i, int i2, boolean z) {
        unityListenerName = str;
        try {
            OneSignal.setRequiresUserPrivacyConsent(z);
            Class<?> cls = Class.forName("com.unity3d.player.UnityPlayer");
            unitySendMessage = cls.getMethod("UnitySendMessage", String.class, String.class, String.class);
            OneSignal.sdkType = "unity";
            OneSignal.setLogLevel(i, i2);
            OneSignal.Builder currentOrNewInitBuilder = OneSignal.getCurrentOrNewInitBuilder();
            currentOrNewInitBuilder.unsubscribeWhenNotificationsAreDisabled(true);
            currentOrNewInitBuilder.filterOtherGCMReceivers(true);
            OneSignal.init((Activity) cls.getField("currentActivity").get(null), str2, str3, this, this);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void notificationOpened(OSNotificationOpenResult oSNotificationOpenResult) {
        unitySafeInvoke("onPushNotificationOpened", oSNotificationOpenResult.toJSONObject().toString());
    }

    public void notificationReceived(OSNotification oSNotification) {
        unitySafeInvoke("onPushNotificationReceived", oSNotification.toJSONObject().toString());
    }

    public void sendTag(String str, String str2) {
        OneSignal.sendTag(str, str2);
    }

    public void sendTags(String str) {
        OneSignal.sendTags(str);
    }

    public void setEmail(String str, String str2) {
        OneSignal.setEmail(str, str2, new OneSignal.EmailUpdateHandler() {
            public void onSuccess() {
                OneSignalUnityProxy.unitySafeInvoke("onSetEmailSuccess", "{\"status\": \"success\"}");
            }

            public void onFailure(OneSignal.EmailUpdateError emailUpdateError) {
                OneSignalUnityProxy.unitySafeInvoke("onSetEmailFailure", "{\"error\": \"" + emailUpdateError.getMessage() + "\"}");
            }
        });
    }

    public void logoutEmail() {
        OneSignal.logoutEmail(new OneSignal.EmailUpdateHandler() {
            public void onSuccess() {
                OneSignalUnityProxy.unitySafeInvoke("onLogoutEmailSuccess", "{\"status\": \"success\"}");
            }

            public void onFailure(OneSignal.EmailUpdateError emailUpdateError) {
                OneSignalUnityProxy.unitySafeInvoke("onLogoutEmailFailure", "{\"error\": \"" + emailUpdateError.getMessage() + "\"}");
            }
        });
    }

    public void getTags() {
        OneSignal.getTags(new OneSignal.GetTagsHandler() {
            public void tagsAvailable(JSONObject jSONObject) {
                OneSignalUnityProxy.unitySafeInvoke("onTagsReceived", jSONObject != null ? jSONObject.toString() : "{}");
            }
        });
    }

    public void deleteTag(String str) {
        OneSignal.deleteTag(str);
    }

    public void deleteTags(String str) {
        OneSignal.deleteTags(str);
    }

    public void idsAvailable() {
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            public void idsAvailable(String str, String str2) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("userId", str);
                    if (str2 != null) {
                        jSONObject.put("pushToken", str2);
                    } else {
                        jSONObject.put("pushToken", "");
                    }
                    OneSignalUnityProxy.unitySafeInvoke("onIdsAvailable", jSONObject.toString());
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        });
    }

    public void enableSound(boolean z) {
        OneSignal.enableSound(z);
    }

    public void enableVibrate(boolean z) {
        OneSignal.enableVibrate(z);
    }

    public void setInFocusDisplaying(int i) {
        OneSignal.setInFocusDisplaying(i);
    }

    public void setSubscription(boolean z) {
        OneSignal.setSubscription(z);
    }

    public void postNotification(String str) {
        OneSignal.postNotification(str, new OneSignal.PostNotificationResponseHandler() {
            public void onSuccess(JSONObject jSONObject) {
                OneSignalUnityProxy.unitySafeInvoke("onPostNotificationSuccess", jSONObject.toString());
            }

            public void onFailure(JSONObject jSONObject) {
                OneSignalUnityProxy.unitySafeInvoke("onPostNotificationFailed", jSONObject.toString());
            }
        });
    }

    public void promptLocation() {
        OneSignal.promptLocation();
    }

    public void syncHashedEmail(String str) {
        OneSignal.syncHashedEmail(str);
    }

    public void clearOneSignalNotifications() {
        OneSignal.clearOneSignalNotifications();
    }

    public void cancelNotification(int i) {
        OneSignal.cancelNotification(i);
    }

    public void cancelGroupedNotifications(String str) {
        OneSignal.cancelGroupedNotifications(str);
    }

    public void addPermissionObserver() {
        OneSignal.addPermissionObserver(this);
    }

    public void removePermissionObserver() {
        OneSignal.removePermissionObserver(this);
    }

    public void addSubscriptionObserver() {
        OneSignal.addSubscriptionObserver(this);
    }

    public void removeSubscriptionObserver() {
        OneSignal.removeSubscriptionObserver(this);
    }

    public void addEmailSubscriptionObserver() {
        OneSignal.addEmailSubscriptionObserver(this);
    }

    public void removeEmailSubscriptionObserver() {
        OneSignal.removeEmailSubscriptionObserver(this);
    }

    public boolean userProvidedPrivacyConsent() {
        return OneSignal.userProvidedPrivacyConsent();
    }

    public void provideUserConsent(boolean z) {
        OneSignal.provideUserConsent(z);
    }

    public void setRequiresUserPrivacyConsent(boolean z) {
        OneSignal.setRequiresUserPrivacyConsent(z);
    }

    public String getPermissionSubscriptionState() {
        return OneSignal.getPermissionSubscriptionState().toJSONObject().toString();
    }

    public void setLocationShared(boolean z) {
        OneSignal.setLocationShared(z);
    }

    public void onOSPermissionChanged(OSPermissionStateChanges oSPermissionStateChanges) {
        unitySafeInvoke("onOSPermissionChanged", oSPermissionStateChanges.toJSONObject().toString());
    }

    public void onOSSubscriptionChanged(OSSubscriptionStateChanges oSSubscriptionStateChanges) {
        unitySafeInvoke("onOSSubscriptionChanged", oSSubscriptionStateChanges.toJSONObject().toString());
    }

    public void onOSEmailSubscriptionChanged(OSEmailSubscriptionStateChanges oSEmailSubscriptionStateChanges) {
        unitySafeInvoke("onOSEmailSubscriptionChanged", oSEmailSubscriptionStateChanges.toJSONObject().toString());
    }

    /* access modifiers changed from: private */
    public static void unitySafeInvoke(String str, String str2) {
        try {
            unitySendMessage.invoke(null, unityListenerName, str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
