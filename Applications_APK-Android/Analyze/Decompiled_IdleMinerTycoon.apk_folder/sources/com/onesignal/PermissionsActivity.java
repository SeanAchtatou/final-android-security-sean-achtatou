package com.onesignal;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.onesignal.ActivityLifecycleHandler;
import com.onesignal.AndroidSupportV4Compat;

public class PermissionsActivity extends Activity {
    private static final int REQUEST_LOCATION = 2;
    private static ActivityLifecycleHandler.ActivityAvailableListener activityAvailableListener;
    static boolean answered;
    static boolean waiting;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        OneSignal.setAppContext(this);
        if (bundle == null || !bundle.getBoolean("android:hasCurrentPermissionsRequest", false)) {
            requestPermission();
        } else {
            waiting = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (OneSignal.initDone) {
            requestPermission();
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT < 23) {
            finish();
        } else if (!waiting) {
            waiting = true;
            AndroidSupportV4Compat.ActivityCompat.requestPermissions(this, new String[]{LocationGMS.requestPermission}, 2);
        }
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        answered = true;
        waiting = false;
        if (i == 2) {
            if (iArr.length <= 0 || iArr[0] != 0) {
                LocationGMS.fireFailedComplete();
            } else {
                LocationGMS.startGetLocation();
            }
        }
        ActivityLifecycleHandler.removeActivityAvailableListener(activityAvailableListener);
        finish();
    }

    static void startPrompt() {
        if (!waiting && !answered) {
            activityAvailableListener = new ActivityLifecycleHandler.ActivityAvailableListener() {
                public void available(Activity activity) {
                    if (!activity.getClass().equals(PermissionsActivity.class)) {
                        Intent intent = new Intent(activity, PermissionsActivity.class);
                        intent.setFlags(131072);
                        activity.startActivity(intent);
                    }
                }
            };
            ActivityLifecycleHandler.setActivityAvailableListener(activityAvailableListener);
        }
    }
}
