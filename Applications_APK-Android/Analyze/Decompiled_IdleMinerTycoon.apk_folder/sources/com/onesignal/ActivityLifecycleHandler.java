package com.onesignal;

import android.app.Activity;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.onesignal.OneSignal;

class ActivityLifecycleHandler {
    static Activity curActivity;
    static FocusHandlerThread focusHandlerThread = new FocusHandlerThread();
    private static ActivityAvailableListener mActivityAvailableListener;
    static boolean nextResumeIsFirstActivity;

    interface ActivityAvailableListener {
        void available(Activity activity);
    }

    static void onActivityCreated(Activity activity) {
    }

    static void onActivityStarted(Activity activity) {
    }

    ActivityLifecycleHandler() {
    }

    static void setActivityAvailableListener(ActivityAvailableListener activityAvailableListener) {
        if (curActivity != null) {
            activityAvailableListener.available(curActivity);
            mActivityAvailableListener = activityAvailableListener;
            return;
        }
        mActivityAvailableListener = activityAvailableListener;
    }

    public static void removeActivityAvailableListener(ActivityAvailableListener activityAvailableListener) {
        mActivityAvailableListener = null;
    }

    private static void setCurActivity(Activity activity) {
        curActivity = activity;
        if (mActivityAvailableListener != null) {
            mActivityAvailableListener.available(curActivity);
        }
    }

    static void onActivityResumed(Activity activity) {
        setCurActivity(activity);
        logCurActivity();
        handleFocus();
    }

    static void onActivityPaused(Activity activity) {
        if (activity == curActivity) {
            curActivity = null;
            handleLostFocus();
        }
        logCurActivity();
    }

    static void onActivityStopped(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.Log(log_level, "onActivityStopped: " + activity.getClass().getName());
        if (activity == curActivity) {
            curActivity = null;
            handleLostFocus();
        }
        logCurActivity();
    }

    static void onActivityDestroyed(Activity activity) {
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        OneSignal.Log(log_level, "onActivityDestroyed: " + activity.getClass().getName());
        if (activity == curActivity) {
            curActivity = null;
            handleLostFocus();
        }
        logCurActivity();
    }

    private static void logCurActivity() {
        String str;
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.DEBUG;
        StringBuilder sb = new StringBuilder();
        sb.append("curActivity is NOW: ");
        if (curActivity != null) {
            str = "" + curActivity.getClass().getName() + ":" + curActivity;
        } else {
            str = "null";
        }
        sb.append(str);
        OneSignal.Log(log_level, sb.toString());
    }

    private static void handleLostFocus() {
        focusHandlerThread.runRunnable(new AppFocusRunnable());
    }

    private static void handleFocus() {
        if (focusHandlerThread.hasBackgrounded() || nextResumeIsFirstActivity) {
            nextResumeIsFirstActivity = false;
            focusHandlerThread.resetBackgroundState();
            OneSignal.onAppFocus();
            return;
        }
        focusHandlerThread.stopScheduledRunnable();
    }

    static class FocusHandlerThread extends HandlerThread {
        private AppFocusRunnable appFocusRunnable;
        Handler mHandler = null;

        FocusHandlerThread() {
            super("FocusHandlerThread");
            start();
            this.mHandler = new Handler(getLooper());
        }

        /* access modifiers changed from: package-private */
        public Looper getHandlerLooper() {
            return this.mHandler.getLooper();
        }

        /* access modifiers changed from: package-private */
        public void resetBackgroundState() {
            if (this.appFocusRunnable != null) {
                boolean unused = this.appFocusRunnable.backgrounded = false;
            }
        }

        /* access modifiers changed from: package-private */
        public void stopScheduledRunnable() {
            this.mHandler.removeCallbacksAndMessages(null);
        }

        /* access modifiers changed from: package-private */
        public void runRunnable(AppFocusRunnable appFocusRunnable2) {
            if (this.appFocusRunnable == null || !this.appFocusRunnable.backgrounded || this.appFocusRunnable.completed) {
                this.appFocusRunnable = appFocusRunnable2;
                this.mHandler.removeCallbacksAndMessages(null);
                this.mHandler.postDelayed(appFocusRunnable2, 2000);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean hasBackgrounded() {
            return this.appFocusRunnable != null && this.appFocusRunnable.backgrounded;
        }
    }

    private static class AppFocusRunnable implements Runnable {
        /* access modifiers changed from: private */
        public boolean backgrounded;
        /* access modifiers changed from: private */
        public boolean completed;

        private AppFocusRunnable() {
        }

        public void run() {
            if (ActivityLifecycleHandler.curActivity == null) {
                this.backgrounded = true;
                OneSignal.onAppLostFocus();
                this.completed = true;
            }
        }
    }
}
