package com.onesignal;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int adSize = 2130903074;
        public static final int adSizes = 2130903075;
        public static final int adUnitId = 2130903076;
        public static final int buttonSize = 2130903127;
        public static final int circleCrop = 2130903163;
        public static final int colorScheme = 2130903186;
        public static final int coordinatorLayoutStyle = 2130903216;
        public static final int font = 2130903266;
        public static final int fontProviderAuthority = 2130903268;
        public static final int fontProviderCerts = 2130903269;
        public static final int fontProviderFetchStrategy = 2130903270;
        public static final int fontProviderFetchTimeout = 2130903271;
        public static final int fontProviderPackage = 2130903272;
        public static final int fontProviderQuery = 2130903273;
        public static final int fontStyle = 2130903274;
        public static final int fontWeight = 2130903276;
        public static final int imageAspectRatio = 2130903359;
        public static final int imageAspectRatioAdjust = 2130903360;
        public static final int keylines = 2130903378;
        public static final int layout_anchor = 2130903383;
        public static final int layout_anchorGravity = 2130903384;
        public static final int layout_behavior = 2130903385;
        public static final int layout_dodgeInsetEdges = 2130903388;
        public static final int layout_insetEdge = 2130903389;
        public static final int layout_keyline = 2130903390;
        public static final int scopeUris = 2130903450;
        public static final int statusBarBackground = 2130903482;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130968576;

        private bool() {
        }
    }

    public static final class color {
        public static final int browser_actions_bg_grey = 2131034156;
        public static final int browser_actions_divider_color = 2131034157;
        public static final int browser_actions_text_color = 2131034158;
        public static final int browser_actions_title_color = 2131034159;
        public static final int common_google_signin_btn_text_dark = 2131034183;
        public static final int common_google_signin_btn_text_dark_default = 2131034184;
        public static final int common_google_signin_btn_text_dark_disabled = 2131034185;
        public static final int common_google_signin_btn_text_dark_focused = 2131034186;
        public static final int common_google_signin_btn_text_dark_pressed = 2131034187;
        public static final int common_google_signin_btn_text_light = 2131034188;
        public static final int common_google_signin_btn_text_light_default = 2131034189;
        public static final int common_google_signin_btn_text_light_disabled = 2131034190;
        public static final int common_google_signin_btn_text_light_focused = 2131034191;
        public static final int common_google_signin_btn_text_light_pressed = 2131034192;
        public static final int common_google_signin_btn_tint = 2131034193;
        public static final int notification_action_color_filter = 2131034324;
        public static final int notification_icon_bg_color = 2131034325;
        public static final int notification_material_background_media_default_color = 2131034326;
        public static final int primary_text_default_material_dark = 2131034333;
        public static final int ripple_material_light = 2131034338;
        public static final int secondary_text_default_material_dark = 2131034339;
        public static final int secondary_text_default_material_light = 2131034340;

        private color() {
        }
    }

    public static final class dimen {
        public static final int browser_actions_context_menu_max_width = 2131099734;
        public static final int browser_actions_context_menu_min_padding = 2131099735;
        public static final int compat_button_inset_horizontal_material = 2131099755;
        public static final int compat_button_inset_vertical_material = 2131099756;
        public static final int compat_button_padding_horizontal_material = 2131099757;
        public static final int compat_button_padding_vertical_material = 2131099758;
        public static final int compat_control_corner_material = 2131099759;
        public static final int notification_action_icon_size = 2131099910;
        public static final int notification_action_text_size = 2131099911;
        public static final int notification_big_circle_margin = 2131099912;
        public static final int notification_content_margin_start = 2131099913;
        public static final int notification_large_icon_height = 2131099914;
        public static final int notification_large_icon_width = 2131099915;
        public static final int notification_main_column_padding_top = 2131099916;
        public static final int notification_media_narrow_margin = 2131099917;
        public static final int notification_right_icon_size = 2131099918;
        public static final int notification_right_side_padding_top = 2131099919;
        public static final int notification_small_icon_background_padding = 2131099920;
        public static final int notification_small_icon_size_as_large = 2131099921;
        public static final int notification_subtext_size = 2131099922;
        public static final int notification_top_pad = 2131099923;
        public static final int notification_top_pad_large_text = 2131099924;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131165314;
        public static final int common_google_signin_btn_icon_dark = 2131165315;
        public static final int common_google_signin_btn_icon_dark_focused = 2131165316;
        public static final int common_google_signin_btn_icon_dark_normal = 2131165317;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131165318;
        public static final int common_google_signin_btn_icon_disabled = 2131165319;
        public static final int common_google_signin_btn_icon_light = 2131165320;
        public static final int common_google_signin_btn_icon_light_focused = 2131165321;
        public static final int common_google_signin_btn_icon_light_normal = 2131165322;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131165323;
        public static final int common_google_signin_btn_text_dark = 2131165324;
        public static final int common_google_signin_btn_text_dark_focused = 2131165325;
        public static final int common_google_signin_btn_text_dark_normal = 2131165326;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131165327;
        public static final int common_google_signin_btn_text_disabled = 2131165328;
        public static final int common_google_signin_btn_text_light = 2131165329;
        public static final int common_google_signin_btn_text_light_focused = 2131165330;
        public static final int common_google_signin_btn_text_light_normal = 2131165331;
        public static final int common_google_signin_btn_text_light_normal_background = 2131165332;
        public static final int googleg_disabled_color_18 = 2131165339;
        public static final int googleg_standard_color_18 = 2131165340;
        public static final int ic_os_notification_fallback_white_24dp = 2131165392;
        public static final int notification_action_background = 2131165445;
        public static final int notification_bg = 2131165446;
        public static final int notification_bg_low = 2131165447;
        public static final int notification_bg_low_normal = 2131165448;
        public static final int notification_bg_low_pressed = 2131165449;
        public static final int notification_bg_normal = 2131165450;
        public static final int notification_bg_normal_pressed = 2131165451;
        public static final int notification_icon_background = 2131165452;
        public static final int notification_template_icon_bg = 2131165453;
        public static final int notification_template_icon_low_bg = 2131165454;
        public static final int notification_tile_bg = 2131165455;
        public static final int notify_panel_notification_icon_bg = 2131165456;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action0 = 2131230726;
        public static final int action_container = 2131230734;
        public static final int action_divider = 2131230736;
        public static final int action_image = 2131230737;
        public static final int action_text = 2131230743;
        public static final int actions = 2131230744;
        public static final int adjust_height = 2131230748;
        public static final int adjust_width = 2131230749;
        public static final int async = 2131230770;
        public static final int auto = 2131230777;
        public static final int blocking = 2131230780;
        public static final int bottom = 2131230781;
        public static final int browser_actions_header_text = 2131230783;
        public static final int browser_actions_menu_item_icon = 2131230784;
        public static final int browser_actions_menu_item_text = 2131230785;
        public static final int browser_actions_menu_items = 2131230786;
        public static final int browser_actions_menu_view = 2131230787;
        public static final int button = 2131230788;
        public static final int cancel_action = 2131230793;
        public static final int center = 2131230795;
        public static final int chronometer = 2131230800;
        public static final int dark = 2131230832;
        public static final int date = 2131230833;
        public static final int end = 2131230849;
        public static final int end_padder = 2131230850;
        public static final int forever = 2131230867;
        public static final int icon = 2131230918;
        public static final int icon_group = 2131230919;
        public static final int icon_only = 2131230920;
        public static final int info = 2131230925;
        public static final int italic = 2131230929;
        public static final int left = 2131230934;
        public static final int light = 2131230935;
        public static final int line1 = 2131230937;
        public static final int line3 = 2131230938;
        public static final int media_actions = 2131230949;
        public static final int none = 2131231017;
        public static final int normal = 2131231018;
        public static final int notification_background = 2131231019;
        public static final int notification_main_column = 2131231021;
        public static final int notification_main_column_container = 2131231022;
        public static final int os_bgimage_notif_bgimage = 2131231031;
        public static final int os_bgimage_notif_bgimage_align_layout = 2131231032;
        public static final int os_bgimage_notif_bgimage_right_aligned = 2131231033;
        public static final int os_bgimage_notif_body = 2131231034;
        public static final int os_bgimage_notif_title = 2131231035;
        public static final int progressBar = 2131231044;
        public static final int radio = 2131231054;
        public static final int right = 2131231068;
        public static final int right_icon = 2131231069;
        public static final int right_side = 2131231070;
        public static final int standard = 2131231127;
        public static final int start = 2131231128;
        public static final int status_bar_latest_event_content = 2131231129;
        public static final int tag_transition_group = 2131231138;
        public static final int text = 2131231141;
        public static final int text2 = 2131231142;
        public static final int time = 2131231153;
        public static final int title = 2131231154;
        public static final int toolbar = 2131231157;
        public static final int top = 2131231158;
        public static final int wide = 2131231190;
        public static final int wrap_content = 2131231192;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131296260;
        public static final int google_play_services_version = 2131296264;
        public static final int status_bar_notification_info_maxnum = 2131296277;

        private integer() {
        }
    }

    public static final class layout {
        public static final int browser_actions_context_menu_page = 2131427356;
        public static final int browser_actions_context_menu_row = 2131427357;
        public static final int notification_action = 2131427460;
        public static final int notification_action_tombstone = 2131427461;
        public static final int notification_media_action = 2131427462;
        public static final int notification_media_cancel_action = 2131427463;
        public static final int notification_template_big_media = 2131427464;
        public static final int notification_template_big_media_custom = 2131427465;
        public static final int notification_template_big_media_narrow = 2131427466;
        public static final int notification_template_big_media_narrow_custom = 2131427467;
        public static final int notification_template_custom_big = 2131427468;
        public static final int notification_template_icon_group = 2131427469;
        public static final int notification_template_lines_media = 2131427470;
        public static final int notification_template_media = 2131427471;
        public static final int notification_template_media_custom = 2131427472;
        public static final int notification_template_part_chronometer = 2131427473;
        public static final int notification_template_part_time = 2131427474;
        public static final int onesignal_bgimage_notif_layout = 2131427475;

        private layout() {
        }
    }

    public static final class raw {
        public static final int consumer_onesignal_keep = 2131623936;

        private raw() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131689543;
        public static final int common_google_play_services_enable_text = 2131689544;
        public static final int common_google_play_services_enable_title = 2131689545;
        public static final int common_google_play_services_install_button = 2131689546;
        public static final int common_google_play_services_install_text = 2131689547;
        public static final int common_google_play_services_install_title = 2131689548;
        public static final int common_google_play_services_notification_channel_name = 2131689549;
        public static final int common_google_play_services_notification_ticker = 2131689550;
        public static final int common_google_play_services_unknown_issue = 2131689551;
        public static final int common_google_play_services_unsupported_text = 2131689552;
        public static final int common_google_play_services_update_button = 2131689553;
        public static final int common_google_play_services_update_text = 2131689554;
        public static final int common_google_play_services_update_title = 2131689555;
        public static final int common_google_play_services_updating_text = 2131689556;
        public static final int common_google_play_services_wear_update_text = 2131689557;
        public static final int common_open_on_phone = 2131689558;
        public static final int common_signin_button_text = 2131689559;
        public static final int common_signin_button_text_long = 2131689560;
        public static final int fcm_fallback_notification_channel_label = 2131689570;
        public static final int s1 = 2131689738;
        public static final int s2 = 2131689739;
        public static final int s3 = 2131689740;
        public static final int s4 = 2131689741;
        public static final int s5 = 2131689742;
        public static final int s6 = 2131689743;
        public static final int s7 = 2131689744;
        public static final int status_bar_notification_info_overflow = 2131689751;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131755322;
        public static final int TextAppearance_Compat_Notification_Info = 2131755323;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131755324;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131755325;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131755326;
        public static final int TextAppearance_Compat_Notification_Media = 2131755327;
        public static final int TextAppearance_Compat_Notification_Time = 2131755328;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131755329;
        public static final int TextAppearance_Compat_Notification_Title = 2131755330;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131755331;
        public static final int Theme_IAPTheme = 2131755385;
        public static final int Widget_Compat_NotificationActionContainer = 2131755501;
        public static final int Widget_Compat_NotificationActionText = 2131755502;
        public static final int Widget_Support_CoordinatorLayout = 2131755549;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.fluffyfairygames.idleminertycoon.R.attr.adSize, com.fluffyfairygames.idleminertycoon.R.attr.adSizes, com.fluffyfairygames.idleminertycoon.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] CoordinatorLayout = {com.fluffyfairygames.idleminertycoon.R.attr.keylines, com.fluffyfairygames.idleminertycoon.R.attr.statusBarBackground};
        public static final int[] CoordinatorLayout_Layout = {16842931, com.fluffyfairygames.idleminertycoon.R.attr.layout_anchor, com.fluffyfairygames.idleminertycoon.R.attr.layout_anchorGravity, com.fluffyfairygames.idleminertycoon.R.attr.layout_behavior, com.fluffyfairygames.idleminertycoon.R.attr.layout_dodgeInsetEdges, com.fluffyfairygames.idleminertycoon.R.attr.layout_insetEdge, com.fluffyfairygames.idleminertycoon.R.attr.layout_keyline};
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily = {com.fluffyfairygames.idleminertycoon.R.attr.fontProviderAuthority, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderCerts, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderFetchStrategy, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderFetchTimeout, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderPackage, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.fluffyfairygames.idleminertycoon.R.attr.font, com.fluffyfairygames.idleminertycoon.R.attr.fontStyle, com.fluffyfairygames.idleminertycoon.R.attr.fontVariationSettings, com.fluffyfairygames.idleminertycoon.R.attr.fontWeight, com.fluffyfairygames.idleminertycoon.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.fluffyfairygames.idleminertycoon.R.attr.circleCrop, com.fluffyfairygames.idleminertycoon.R.attr.imageAspectRatio, com.fluffyfairygames.idleminertycoon.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.fluffyfairygames.idleminertycoon.R.attr.buttonSize, com.fluffyfairygames.idleminertycoon.R.attr.colorScheme, com.fluffyfairygames.idleminertycoon.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
