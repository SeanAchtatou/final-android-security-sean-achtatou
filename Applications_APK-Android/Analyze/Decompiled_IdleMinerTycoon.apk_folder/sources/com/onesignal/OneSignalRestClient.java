package com.onesignal;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.lang.Thread;
import org.json.JSONObject;

class OneSignalRestClient {
    private static final String BASE_URL = "https://onesignal.com/api/v1/";
    private static final int GET_TIMEOUT = 60000;
    private static final int TIMEOUT = 120000;

    private static int getThreadTimeout(int i) {
        return i + 5000;
    }

    static class ResponseHandler {
        /* access modifiers changed from: package-private */
        public void onFailure(int i, String str, Throwable th) {
        }

        /* access modifiers changed from: package-private */
        public void onSuccess(String str) {
        }

        ResponseHandler() {
        }
    }

    OneSignalRestClient() {
    }

    static void put(final String str, final JSONObject jSONObject, final ResponseHandler responseHandler) {
        new Thread(new Runnable() {
            public void run() {
                OneSignalRestClient.makeRequest(str, HttpRequest.METHOD_PUT, jSONObject, responseHandler, OneSignalRestClient.TIMEOUT);
            }
        }).start();
    }

    static void post(final String str, final JSONObject jSONObject, final ResponseHandler responseHandler) {
        new Thread(new Runnable() {
            public void run() {
                OneSignalRestClient.makeRequest(str, HttpRequest.METHOD_POST, jSONObject, responseHandler, OneSignalRestClient.TIMEOUT);
            }
        }).start();
    }

    static void get(final String str, final ResponseHandler responseHandler) {
        new Thread(new Runnable() {
            public void run() {
                OneSignalRestClient.makeRequest(str, null, null, responseHandler, OneSignalRestClient.GET_TIMEOUT);
            }
        }).start();
    }

    static void getSync(String str, ResponseHandler responseHandler) {
        makeRequest(str, null, null, responseHandler, GET_TIMEOUT);
    }

    static void putSync(String str, JSONObject jSONObject, ResponseHandler responseHandler) {
        makeRequest(str, HttpRequest.METHOD_PUT, jSONObject, responseHandler, TIMEOUT);
    }

    static void postSync(String str, JSONObject jSONObject, ResponseHandler responseHandler) {
        makeRequest(str, HttpRequest.METHOD_POST, jSONObject, responseHandler, TIMEOUT);
    }

    /* access modifiers changed from: private */
    public static void makeRequest(String str, String str2, JSONObject jSONObject, ResponseHandler responseHandler, int i) {
        if (str2 == null || !OneSignal.shouldLogUserPrivacyConsentErrorMessageForMethodName(null)) {
            Thread[] threadArr = new Thread[1];
            final Thread[] threadArr2 = threadArr;
            final String str3 = str;
            final String str4 = str2;
            final JSONObject jSONObject2 = jSONObject;
            final ResponseHandler responseHandler2 = responseHandler;
            final int i2 = i;
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    threadArr2[0] = OneSignalRestClient.startHTTPConnection(str3, str4, jSONObject2, responseHandler2, i2);
                }
            }, "OS_HTTPConnection");
            thread.start();
            try {
                thread.join((long) getThreadTimeout(i));
                if (thread.getState() != Thread.State.TERMINATED) {
                    thread.interrupt();
                }
                if (threadArr[0] != null) {
                    threadArr[0].join();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0182, code lost:
        if (r2 != null) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0184, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01d9, code lost:
        if (r2 != null) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01dc, code lost:
        return r5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0196 A[Catch:{ all -> 0x01dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Thread startHTTPConnection(java.lang.String r5, java.lang.String r6, org.json.JSONObject r7, com.onesignal.OneSignalRestClient.ResponseHandler r8, int r9) {
        /*
            r0 = 0
            r1 = -1
            com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            r3.<init>()     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.lang.String r4 = "OneSignalRestClient: Making request to: https://onesignal.com/api/v1/"
            r3.append(r4)     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            r3.append(r5)     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            com.onesignal.OneSignal.Log(r2, r3)     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.net.URL r2 = new java.net.URL     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            r3.<init>()     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.lang.String r4 = "https://onesignal.com/api/v1/"
            r3.append(r4)     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            r3.append(r5)     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Throwable -> 0x018f, all -> 0x018c }
            r3 = 0
            r2.setUseCaches(r3)     // Catch:{ Throwable -> 0x018a }
            r2.setConnectTimeout(r9)     // Catch:{ Throwable -> 0x018a }
            r2.setReadTimeout(r9)     // Catch:{ Throwable -> 0x018a }
            r9 = 1
            if (r7 == 0) goto L_0x0044
            r2.setDoInput(r9)     // Catch:{ Throwable -> 0x018a }
        L_0x0044:
            if (r6 == 0) goto L_0x0053
            java.lang.String r3 = "Content-Type"
            java.lang.String r4 = "application/json; charset=UTF-8"
            r2.setRequestProperty(r3, r4)     // Catch:{ Throwable -> 0x018a }
            r2.setRequestMethod(r6)     // Catch:{ Throwable -> 0x018a }
            r2.setDoOutput(r9)     // Catch:{ Throwable -> 0x018a }
        L_0x0053:
            if (r7 == 0) goto L_0x0088
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x018a }
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG     // Catch:{ Throwable -> 0x018a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x018a }
            r3.<init>()     // Catch:{ Throwable -> 0x018a }
            java.lang.String r4 = "OneSignalRestClient: "
            r3.append(r4)     // Catch:{ Throwable -> 0x018a }
            r3.append(r6)     // Catch:{ Throwable -> 0x018a }
            java.lang.String r4 = " SEND JSON: "
            r3.append(r4)     // Catch:{ Throwable -> 0x018a }
            r3.append(r7)     // Catch:{ Throwable -> 0x018a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x018a }
            com.onesignal.OneSignal.Log(r9, r3)     // Catch:{ Throwable -> 0x018a }
            java.lang.String r9 = "UTF-8"
            byte[] r7 = r7.getBytes(r9)     // Catch:{ Throwable -> 0x018a }
            int r9 = r7.length     // Catch:{ Throwable -> 0x018a }
            r2.setFixedLengthStreamingMode(r9)     // Catch:{ Throwable -> 0x018a }
            java.io.OutputStream r9 = r2.getOutputStream()     // Catch:{ Throwable -> 0x018a }
            r9.write(r7)     // Catch:{ Throwable -> 0x018a }
        L_0x0088:
            int r7 = r2.getResponseCode()     // Catch:{ Throwable -> 0x018a }
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.VERBOSE     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r1.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = "OneSignalRestClient: After con.getResponseCode  to: https://onesignal.com/api/v1/"
            r1.append(r3)     // Catch:{ Throwable -> 0x0188 }
            r1.append(r5)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal.Log(r9, r1)     // Catch:{ Throwable -> 0x0188 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r7 != r9) goto L_0x00fc
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r1.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = "OneSignalRestClient: Successfully finished request to: https://onesignal.com/api/v1/"
            r1.append(r3)     // Catch:{ Throwable -> 0x0188 }
            r1.append(r5)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r5 = r1.toString()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal.Log(r9, r5)     // Catch:{ Throwable -> 0x0188 }
            java.io.InputStream r5 = r2.getInputStream()     // Catch:{ Throwable -> 0x0188 }
            java.util.Scanner r9 = new java.util.Scanner     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = "UTF-8"
            r9.<init>(r5, r1)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r5 = "\\A"
            java.util.Scanner r5 = r9.useDelimiter(r5)     // Catch:{ Throwable -> 0x0188 }
            boolean r5 = r5.hasNext()     // Catch:{ Throwable -> 0x0188 }
            if (r5 == 0) goto L_0x00d8
            java.lang.String r5 = r9.next()     // Catch:{ Throwable -> 0x0188 }
            goto L_0x00da
        L_0x00d8:
            java.lang.String r5 = ""
        L_0x00da:
            r9.close()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r1.<init>()     // Catch:{ Throwable -> 0x0188 }
            r1.append(r6)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = " RECEIVED JSON: "
            r1.append(r3)     // Catch:{ Throwable -> 0x0188 }
            r1.append(r5)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal.Log(r9, r1)     // Catch:{ Throwable -> 0x0188 }
            java.lang.Thread r5 = callResponseHandlerOnSuccess(r8, r5)     // Catch:{ Throwable -> 0x0188 }
            goto L_0x0182
        L_0x00fc:
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r1.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = "OneSignalRestClient: Failed request to: https://onesignal.com/api/v1/"
            r1.append(r3)     // Catch:{ Throwable -> 0x0188 }
            r1.append(r5)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r5 = r1.toString()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal.Log(r9, r5)     // Catch:{ Throwable -> 0x0188 }
            java.io.InputStream r5 = r2.getErrorStream()     // Catch:{ Throwable -> 0x0188 }
            if (r5 != 0) goto L_0x011c
            java.io.InputStream r5 = r2.getInputStream()     // Catch:{ Throwable -> 0x0188 }
        L_0x011c:
            if (r5 == 0) goto L_0x015a
            java.util.Scanner r9 = new java.util.Scanner     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = "UTF-8"
            r9.<init>(r5, r1)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r5 = "\\A"
            java.util.Scanner r5 = r9.useDelimiter(r5)     // Catch:{ Throwable -> 0x0188 }
            boolean r5 = r5.hasNext()     // Catch:{ Throwable -> 0x0188 }
            if (r5 == 0) goto L_0x0136
            java.lang.String r5 = r9.next()     // Catch:{ Throwable -> 0x0188 }
            goto L_0x0138
        L_0x0136:
            java.lang.String r5 = ""
        L_0x0138:
            r9.close()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.WARN     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r1.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = "OneSignalRestClient: "
            r1.append(r3)     // Catch:{ Throwable -> 0x0188 }
            r1.append(r6)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r3 = " RECEIVED JSON: "
            r1.append(r3)     // Catch:{ Throwable -> 0x0188 }
            r1.append(r5)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal.Log(r9, r1)     // Catch:{ Throwable -> 0x0188 }
            goto L_0x017e
        L_0x015a:
            com.onesignal.OneSignal$LOG_LEVEL r5 = com.onesignal.OneSignal.LOG_LEVEL.WARN     // Catch:{ Throwable -> 0x0188 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0188 }
            r9.<init>()     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = "OneSignalRestClient: "
            r9.append(r1)     // Catch:{ Throwable -> 0x0188 }
            r9.append(r6)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = " HTTP Code: "
            r9.append(r1)     // Catch:{ Throwable -> 0x0188 }
            r9.append(r7)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r1 = " No response body!"
            r9.append(r1)     // Catch:{ Throwable -> 0x0188 }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x0188 }
            com.onesignal.OneSignal.Log(r5, r9)     // Catch:{ Throwable -> 0x0188 }
            r5 = r0
        L_0x017e:
            java.lang.Thread r5 = callResponseHandlerOnFailure(r8, r7, r5, r0)     // Catch:{ Throwable -> 0x0188 }
        L_0x0182:
            if (r2 == 0) goto L_0x01dc
        L_0x0184:
            r2.disconnect()
            goto L_0x01dc
        L_0x0188:
            r5 = move-exception
            goto L_0x0192
        L_0x018a:
            r5 = move-exception
            goto L_0x0191
        L_0x018c:
            r5 = move-exception
            r2 = r0
            goto L_0x01de
        L_0x018f:
            r5 = move-exception
            r2 = r0
        L_0x0191:
            r7 = -1
        L_0x0192:
            boolean r9 = r5 instanceof java.net.ConnectException     // Catch:{ all -> 0x01dd }
            if (r9 != 0) goto L_0x01b7
            boolean r9 = r5 instanceof java.net.UnknownHostException     // Catch:{ all -> 0x01dd }
            if (r9 == 0) goto L_0x019b
            goto L_0x01b7
        L_0x019b:
            com.onesignal.OneSignal$LOG_LEVEL r9 = com.onesignal.OneSignal.LOG_LEVEL.WARN     // Catch:{ all -> 0x01dd }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01dd }
            r1.<init>()     // Catch:{ all -> 0x01dd }
            java.lang.String r3 = "OneSignalRestClient: "
            r1.append(r3)     // Catch:{ all -> 0x01dd }
            r1.append(r6)     // Catch:{ all -> 0x01dd }
            java.lang.String r6 = " Error thrown from network stack. "
            r1.append(r6)     // Catch:{ all -> 0x01dd }
            java.lang.String r6 = r1.toString()     // Catch:{ all -> 0x01dd }
            com.onesignal.OneSignal.Log(r9, r6, r5)     // Catch:{ all -> 0x01dd }
            goto L_0x01d5
        L_0x01b7:
            com.onesignal.OneSignal$LOG_LEVEL r6 = com.onesignal.OneSignal.LOG_LEVEL.INFO     // Catch:{ all -> 0x01dd }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01dd }
            r9.<init>()     // Catch:{ all -> 0x01dd }
            java.lang.String r1 = "OneSignalRestClient: Could not send last request, device is offline. Throwable: "
            r9.append(r1)     // Catch:{ all -> 0x01dd }
            java.lang.Class r1 = r5.getClass()     // Catch:{ all -> 0x01dd }
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x01dd }
            r9.append(r1)     // Catch:{ all -> 0x01dd }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x01dd }
            com.onesignal.OneSignal.Log(r6, r9)     // Catch:{ all -> 0x01dd }
        L_0x01d5:
            java.lang.Thread r5 = callResponseHandlerOnFailure(r8, r7, r0, r5)     // Catch:{ all -> 0x01dd }
            if (r2 == 0) goto L_0x01dc
            goto L_0x0184
        L_0x01dc:
            return r5
        L_0x01dd:
            r5 = move-exception
        L_0x01de:
            if (r2 == 0) goto L_0x01e3
            r2.disconnect()
        L_0x01e3:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignalRestClient.startHTTPConnection(java.lang.String, java.lang.String, org.json.JSONObject, com.onesignal.OneSignalRestClient$ResponseHandler, int):java.lang.Thread");
    }

    private static Thread callResponseHandlerOnSuccess(final ResponseHandler responseHandler, final String str) {
        if (responseHandler == null) {
            return null;
        }
        Thread thread = new Thread(new Runnable() {
            public void run() {
                responseHandler.onSuccess(str);
            }
        });
        thread.start();
        return thread;
    }

    private static Thread callResponseHandlerOnFailure(final ResponseHandler responseHandler, final int i, final String str, final Throwable th) {
        if (responseHandler == null) {
            return null;
        }
        Thread thread = new Thread(new Runnable() {
            public void run() {
                responseHandler.onFailure(i, str, th);
            }
        });
        thread.start();
        return thread;
    }
}
