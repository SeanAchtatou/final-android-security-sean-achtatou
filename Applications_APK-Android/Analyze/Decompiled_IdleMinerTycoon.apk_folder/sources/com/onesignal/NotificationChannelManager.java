package com.onesignal;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class NotificationChannelManager {
    private static final String DEFAULT_CHANNEL_ID = "fcm_fallback_notification_channel";
    private static final String RESTORE_CHANNEL_ID = "restored_OS_notifications";

    private static int priorityToImportance(int i) {
        if (i > 9) {
            return 5;
        }
        if (i > 7) {
            return 4;
        }
        if (i > 5) {
            return 3;
        }
        if (i > 3) {
            return 2;
        }
        return i > 1 ? 1 : 0;
    }

    NotificationChannelManager() {
    }

    static String createNotificationChannel(NotificationGenerationJob notificationGenerationJob) {
        if (Build.VERSION.SDK_INT < 26) {
            return DEFAULT_CHANNEL_ID;
        }
        Context context = notificationGenerationJob.context;
        JSONObject jSONObject = notificationGenerationJob.jsonPayload;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME);
        if (notificationGenerationJob.restoring) {
            return createRestoreChannel(notificationManager);
        }
        if (jSONObject.has("oth_chnl")) {
            String optString = jSONObject.optString("oth_chnl");
            if (notificationManager.getNotificationChannel(optString) != null) {
                return optString;
            }
        }
        if (!jSONObject.has("chnl")) {
            return createDefaultChannel(notificationManager);
        }
        try {
            return createChannel(context, notificationManager, jSONObject);
        } catch (JSONException e) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Could not create notification channel due to JSON payload error!", e);
            return DEFAULT_CHANNEL_ID;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x011c  */
    @android.support.annotation.RequiresApi(api = 26)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String createChannel(android.content.Context r6, android.app.NotificationManager r7, org.json.JSONObject r8) throws org.json.JSONException {
        /*
            java.lang.String r0 = "chnl"
            java.lang.Object r0 = r8.opt(r0)
            boolean r1 = r0 instanceof java.lang.String
            if (r1 == 0) goto L_0x0012
            org.json.JSONObject r1 = new org.json.JSONObject
            java.lang.String r0 = (java.lang.String) r0
            r1.<init>(r0)
            goto L_0x0015
        L_0x0012:
            r1 = r0
            org.json.JSONObject r1 = (org.json.JSONObject) r1
        L_0x0015:
            java.lang.String r0 = "id"
            java.lang.String r2 = "fcm_fallback_notification_channel"
            java.lang.String r0 = r1.optString(r0, r2)
            java.lang.String r2 = "miscellaneous"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0027
            java.lang.String r0 = "fcm_fallback_notification_channel"
        L_0x0027:
            java.lang.String r2 = "langs"
            boolean r2 = r1.has(r2)
            if (r2 == 0) goto L_0x0044
            java.lang.String r2 = "langs"
            org.json.JSONObject r2 = r1.getJSONObject(r2)
            java.lang.String r3 = com.onesignal.OSUtils.getCorrectedLanguage()
            boolean r4 = r2.has(r3)
            if (r4 == 0) goto L_0x0044
            org.json.JSONObject r2 = r2.optJSONObject(r3)
            goto L_0x0045
        L_0x0044:
            r2 = r1
        L_0x0045:
            java.lang.String r3 = "nm"
            java.lang.String r4 = "Miscellaneous"
            java.lang.String r3 = r2.optString(r3, r4)
            java.lang.String r4 = "pri"
            r5 = 6
            int r4 = r8.optInt(r4, r5)
            int r4 = priorityToImportance(r4)
            android.app.NotificationChannel r5 = new android.app.NotificationChannel
            r5.<init>(r0, r3, r4)
            java.lang.String r3 = "dscr"
            r4 = 0
            java.lang.String r3 = r2.optString(r3, r4)
            r5.setDescription(r3)
            java.lang.String r3 = "grp_id"
            boolean r3 = r1.has(r3)
            if (r3 == 0) goto L_0x0086
            java.lang.String r3 = "grp_id"
            java.lang.String r1 = r1.optString(r3)
            java.lang.String r3 = "grp_nm"
            java.lang.String r2 = r2.optString(r3)
            android.app.NotificationChannelGroup r3 = new android.app.NotificationChannelGroup
            r3.<init>(r1, r2)
            r7.createNotificationChannelGroup(r3)
            r5.setGroup(r1)
        L_0x0086:
            java.lang.String r1 = "ledc"
            boolean r1 = r8.has(r1)
            if (r1 == 0) goto L_0x00a2
            java.math.BigInteger r1 = new java.math.BigInteger
            java.lang.String r2 = "ledc"
            java.lang.String r2 = r8.optString(r2)
            r3 = 16
            r1.<init>(r2, r3)
            int r1 = r1.intValue()
            r5.setLightColor(r1)
        L_0x00a2:
            java.lang.String r1 = "led"
            r2 = 1
            int r1 = r8.optInt(r1, r2)
            r3 = 0
            if (r1 != r2) goto L_0x00ae
            r1 = 1
            goto L_0x00af
        L_0x00ae:
            r1 = 0
        L_0x00af:
            r5.enableLights(r1)
            java.lang.String r1 = "vib_pt"
            boolean r1 = r8.has(r1)
            if (r1 == 0) goto L_0x00c3
            long[] r1 = com.onesignal.OSUtils.parseVibrationPattern(r8)
            if (r1 == 0) goto L_0x00c3
            r5.setVibrationPattern(r1)
        L_0x00c3:
            java.lang.String r1 = "vib"
            int r1 = r8.optInt(r1, r2)
            if (r1 != r2) goto L_0x00cd
            r1 = 1
            goto L_0x00ce
        L_0x00cd:
            r1 = 0
        L_0x00ce:
            r5.enableVibration(r1)
            java.lang.String r1 = "sound"
            boolean r1 = r8.has(r1)
            if (r1 == 0) goto L_0x00fc
            java.lang.String r1 = "sound"
            java.lang.String r1 = r8.optString(r1, r4)
            android.net.Uri r6 = com.onesignal.OSUtils.getSoundUri(r6, r1)
            if (r6 == 0) goto L_0x00e9
            r5.setSound(r6, r4)
            goto L_0x00fc
        L_0x00e9:
            java.lang.String r6 = "null"
            boolean r6 = r6.equals(r1)
            if (r6 != 0) goto L_0x00f9
            java.lang.String r6 = "nil"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
        L_0x00f9:
            r5.setSound(r4, r4)
        L_0x00fc:
            java.lang.String r6 = "vis"
            int r6 = r8.optInt(r6, r3)
            r5.setLockscreenVisibility(r6)
            java.lang.String r6 = "bdg"
            int r6 = r8.optInt(r6, r2)
            if (r6 != r2) goto L_0x010f
            r6 = 1
            goto L_0x0110
        L_0x010f:
            r6 = 0
        L_0x0110:
            r5.setShowBadge(r6)
            java.lang.String r6 = "bdnd"
            int r6 = r8.optInt(r6, r3)
            if (r6 != r2) goto L_0x011c
            goto L_0x011d
        L_0x011c:
            r2 = 0
        L_0x011d:
            r5.setBypassDnd(r2)
            r7.createNotificationChannel(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.NotificationChannelManager.createChannel(android.content.Context, android.app.NotificationManager, org.json.JSONObject):java.lang.String");
    }

    @RequiresApi(api = 26)
    private static String createDefaultChannel(NotificationManager notificationManager) {
        NotificationChannel notificationChannel = new NotificationChannel(DEFAULT_CHANNEL_ID, "Miscellaneous", 3);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationManager.createNotificationChannel(notificationChannel);
        return DEFAULT_CHANNEL_ID;
    }

    @RequiresApi(api = 26)
    private static String createRestoreChannel(NotificationManager notificationManager) {
        notificationManager.createNotificationChannel(new NotificationChannel(RESTORE_CHANNEL_ID, "Restored", 2));
        return RESTORE_CHANNEL_ID;
    }

    static void processChannelList(Context context, JSONObject jSONObject) {
        if (Build.VERSION.SDK_INT >= 26 && jSONObject.has("chnl_lst")) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME);
            HashSet hashSet = new HashSet();
            JSONArray optJSONArray = jSONObject.optJSONArray("chnl_lst");
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    hashSet.add(createChannel(context, notificationManager, optJSONArray.getJSONObject(i)));
                } catch (JSONException e) {
                    OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Could not create notification channel due to JSON payload error!", e);
                }
            }
            for (NotificationChannel id : notificationManager.getNotificationChannels()) {
                String id2 = id.getId();
                if (id2.startsWith("OS_") && !hashSet.contains(id2)) {
                    notificationManager.deleteNotificationChannel(id2);
                }
            }
        }
    }
}
