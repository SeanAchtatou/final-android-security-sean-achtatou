package com.onesignal;

import android.os.Handler;
import android.os.HandlerThread;
import com.google.android.gms.games.Games;
import com.helpshift.support.constants.FaqsColumns;
import com.onesignal.LocationGMS;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalRestClient;
import com.vungle.warren.model.ReportDBAdapter;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

abstract class UserStateSynchronizer {
    protected UserState currentUserState;
    private final Object networkHandlerSyncLock = new Object() {
    };
    HashMap<Integer, NetworkHandlerThread> networkHandlerThreads = new HashMap<>();
    protected boolean nextSyncIsSession = false;
    /* access modifiers changed from: private */
    public AtomicBoolean runningSyncUserState = new AtomicBoolean();
    private ArrayList<OneSignal.ChangeTagsUpdateHandler> sendTagsHandlers = new ArrayList<>();
    protected final Object syncLock = new Object() {
    };
    protected UserState toSyncUserState;
    protected boolean waitingForSessionResponse = false;

    /* access modifiers changed from: protected */
    public abstract void addOnSessionOrCreateExtras(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract void fireEventsForUpdateFailure(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract String getId();

    /* access modifiers changed from: package-private */
    public abstract boolean getSubscribed();

    /* access modifiers changed from: package-private */
    public abstract GetTagsResult getTags(boolean z);

    public abstract boolean getUserSubscribePreference();

    /* access modifiers changed from: package-private */
    public abstract void logoutEmail();

    /* access modifiers changed from: protected */
    public abstract UserState newUserState(String str, boolean z);

    /* access modifiers changed from: protected */
    public abstract void onSuccessfulSync(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract void scheduleSyncToServer();

    public abstract void setPermission(boolean z);

    /* access modifiers changed from: package-private */
    public abstract void setSubscription(boolean z);

    /* access modifiers changed from: package-private */
    public abstract void updateIdDependents(String str);

    /* access modifiers changed from: package-private */
    public abstract void updateState(JSONObject jSONObject);

    UserStateSynchronizer() {
    }

    static class GetTagsResult {
        JSONObject result;
        boolean serverSuccess;

        GetTagsResult(boolean z, JSONObject jSONObject) {
            this.serverSuccess = z;
            this.result = jSONObject;
        }
    }

    /* access modifiers changed from: package-private */
    public String getRegistrationId() {
        return getToSyncUserState().syncValues.optString(SettingsJsonConstants.APP_IDENTIFIER_KEY, null);
    }

    class NetworkHandlerThread extends HandlerThread {
        static final int MAX_RETRIES = 3;
        static final int NETWORK_CALL_DELAY_TO_BUFFER_MS = 5000;
        protected static final int NETWORK_HANDLER_USERSTATE = 0;
        int currentRetry;
        Handler mHandler = null;
        int mType;

        NetworkHandlerThread(int i) {
            super("OSH_NetworkHandlerThread");
            this.mType = i;
            start();
            this.mHandler = new Handler(getLooper());
        }

        /* access modifiers changed from: package-private */
        public void runNewJobDelayed() {
            synchronized (this.mHandler) {
                this.currentRetry = 0;
                this.mHandler.removeCallbacksAndMessages(null);
                this.mHandler.postDelayed(getNewRunnable(), 5000);
            }
        }

        private Runnable getNewRunnable() {
            if (this.mType != 0) {
                return null;
            }
            return new Runnable() {
                public void run() {
                    if (!UserStateSynchronizer.this.runningSyncUserState.get()) {
                        UserStateSynchronizer.this.syncUserState(false);
                    }
                }
            };
        }

        /* access modifiers changed from: package-private */
        public void stopScheduledRunnable() {
            this.mHandler.removeCallbacksAndMessages(null);
        }

        /* access modifiers changed from: package-private */
        public boolean doRetry() {
            boolean hasMessages;
            synchronized (this.mHandler) {
                boolean z = this.currentRetry < 3;
                boolean hasMessages2 = this.mHandler.hasMessages(0);
                if (z && !hasMessages2) {
                    this.currentRetry++;
                    this.mHandler.postDelayed(getNewRunnable(), (long) (this.currentRetry * 15000));
                }
                hasMessages = this.mHandler.hasMessages(0);
            }
            return hasMessages;
        }
    }

    /* access modifiers changed from: protected */
    public JSONObject generateJsonDiff(JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, Set<String> set) {
        JSONObject generateJsonDiff;
        synchronized (this.syncLock) {
            generateJsonDiff = JSONUtils.generateJsonDiff(jSONObject, jSONObject2, jSONObject3, set);
        }
        return generateJsonDiff;
    }

    /* access modifiers changed from: protected */
    public UserState getToSyncUserState() {
        synchronized (this.syncLock) {
            if (this.toSyncUserState == null) {
                this.toSyncUserState = newUserState("TOSYNC_STATE", true);
            }
        }
        return this.toSyncUserState;
    }

    /* access modifiers changed from: package-private */
    public void initUserState() {
        synchronized (this.syncLock) {
            if (this.currentUserState == null) {
                this.currentUserState = newUserState("CURRENT_STATE", true);
            }
        }
        getToSyncUserState();
    }

    /* access modifiers changed from: package-private */
    public void clearLocation() {
        getToSyncUserState().clearLocation();
        getToSyncUserState().persistState();
    }

    /* access modifiers changed from: package-private */
    public boolean persist() {
        boolean z = false;
        if (this.toSyncUserState == null) {
            return false;
        }
        synchronized (this.syncLock) {
            if (this.currentUserState.generateJsonDiff(this.toSyncUserState, isSessionCall()) != null) {
                z = true;
            }
            this.toSyncUserState.persistState();
        }
        return z;
    }

    private boolean isSessionCall() {
        return this.nextSyncIsSession && !this.waitingForSessionResponse;
    }

    private boolean syncEmailLogout() {
        return getToSyncUserState().dependValues.optBoolean("logoutEmail", false);
    }

    /* access modifiers changed from: package-private */
    public void syncUserState(boolean z) {
        this.runningSyncUserState.set(true);
        internalSyncUserState(z);
        this.runningSyncUserState.set(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006c, code lost:
        if (r1 == false) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006e, code lost:
        if (r8 == false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0071, code lost:
        doCreateOrNewSession(r0, r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0075, code lost:
        doPutSync(r0, r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void internalSyncUserState(boolean r8) {
        /*
            r7 = this;
            java.lang.String r0 = r7.getId()
            boolean r1 = r7.syncEmailLogout()
            if (r1 == 0) goto L_0x0010
            if (r0 == 0) goto L_0x0010
            r7.doEmailLogout(r0)
            return
        L_0x0010:
            com.onesignal.UserState r1 = r7.currentUserState
            if (r1 != 0) goto L_0x0017
            r7.initUserState()
        L_0x0017:
            boolean r1 = r7.isSessionCall()
            java.lang.Object r2 = r7.syncLock
            monitor-enter(r2)
            com.onesignal.UserState r3 = r7.currentUserState     // Catch:{ all -> 0x0079 }
            com.onesignal.UserState r4 = r7.getToSyncUserState()     // Catch:{ all -> 0x0079 }
            org.json.JSONObject r3 = r3.generateJsonDiff(r4, r1)     // Catch:{ all -> 0x0079 }
            com.onesignal.UserState r4 = r7.currentUserState     // Catch:{ all -> 0x0079 }
            org.json.JSONObject r4 = r4.dependValues     // Catch:{ all -> 0x0079 }
            com.onesignal.UserState r5 = r7.getToSyncUserState()     // Catch:{ all -> 0x0079 }
            org.json.JSONObject r5 = r5.dependValues     // Catch:{ all -> 0x0079 }
            r6 = 0
            org.json.JSONObject r4 = r7.generateJsonDiff(r4, r5, r6, r6)     // Catch:{ all -> 0x0079 }
            if (r3 != 0) goto L_0x0064
            com.onesignal.UserState r8 = r7.currentUserState     // Catch:{ all -> 0x0079 }
            r8.persistStateAfterSync(r4, r6)     // Catch:{ all -> 0x0079 }
            java.util.ArrayList<com.onesignal.OneSignal$ChangeTagsUpdateHandler> r8 = r7.sendTagsHandlers     // Catch:{ all -> 0x0079 }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x0079 }
        L_0x0044:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x005d
            java.lang.Object r0 = r8.next()     // Catch:{ all -> 0x0079 }
            com.onesignal.OneSignal$ChangeTagsUpdateHandler r0 = (com.onesignal.OneSignal.ChangeTagsUpdateHandler) r0     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0044
            r1 = 0
            com.onesignal.UserStateSynchronizer$GetTagsResult r1 = com.onesignal.OneSignalStateSynchronizer.getTags(r1)     // Catch:{ all -> 0x0079 }
            org.json.JSONObject r1 = r1.result     // Catch:{ all -> 0x0079 }
            r0.onSuccess(r1)     // Catch:{ all -> 0x0079 }
            goto L_0x0044
        L_0x005d:
            java.util.ArrayList<com.onesignal.OneSignal$ChangeTagsUpdateHandler> r8 = r7.sendTagsHandlers     // Catch:{ all -> 0x0079 }
            r8.clear()     // Catch:{ all -> 0x0079 }
            monitor-exit(r2)     // Catch:{ all -> 0x0079 }
            return
        L_0x0064:
            com.onesignal.UserState r5 = r7.getToSyncUserState()     // Catch:{ all -> 0x0079 }
            r5.persistState()     // Catch:{ all -> 0x0079 }
            monitor-exit(r2)     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x0075
            if (r8 == 0) goto L_0x0071
            goto L_0x0075
        L_0x0071:
            r7.doCreateOrNewSession(r0, r3, r4)
            goto L_0x0078
        L_0x0075:
            r7.doPutSync(r0, r3, r4)
        L_0x0078:
            return
        L_0x0079:
            r8 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0079 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.UserStateSynchronizer.internalSyncUserState(boolean):void");
    }

    private void doEmailLogout(String str) {
        String str2 = "players/" + str + "/email_logout";
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = this.currentUserState.dependValues;
            if (jSONObject2.has("email_auth_hash")) {
                jSONObject.put("email_auth_hash", jSONObject2.optString("email_auth_hash"));
            }
            JSONObject jSONObject3 = this.currentUserState.syncValues;
            if (jSONObject3.has("parent_player_id")) {
                jSONObject.put("parent_player_id", jSONObject3.optString("parent_player_id"));
            }
            jSONObject.put("app_id", jSONObject3.optString("app_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OneSignalRestClient.postSync(str2, jSONObject, new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.WARN;
                OneSignal.Log(log_level, "Failed last request. statusCode: " + i + "\nresponse: " + str);
                if (UserStateSynchronizer.this.response400WithErrorsContaining(i, str, "already logged out of email")) {
                    UserStateSynchronizer.this.logoutEmailSyncSuccess();
                } else if (UserStateSynchronizer.this.response400WithErrorsContaining(i, str, "not a valid device_type")) {
                    UserStateSynchronizer.this.handlePlayerDeletedFromServer();
                } else {
                    UserStateSynchronizer.this.handleNetworkFailure();
                }
            }

            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                UserStateSynchronizer.this.logoutEmailSyncSuccess();
            }
        });
    }

    /* access modifiers changed from: private */
    public void logoutEmailSyncSuccess() {
        getToSyncUserState().dependValues.remove("logoutEmail");
        this.toSyncUserState.dependValues.remove("email_auth_hash");
        this.toSyncUserState.syncValues.remove("parent_player_id");
        this.toSyncUserState.persistState();
        this.currentUserState.dependValues.remove("email_auth_hash");
        this.currentUserState.syncValues.remove("parent_player_id");
        String optString = this.currentUserState.syncValues.optString("email");
        this.currentUserState.syncValues.remove("email");
        OneSignalStateSynchronizer.setSyncAsNewSessionForEmail();
        OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
        OneSignal.Log(log_level, "Device successfully logged out of email: " + optString);
        OneSignal.handleSuccessfulEmailLogout();
    }

    private void doPutSync(String str, final JSONObject jSONObject, final JSONObject jSONObject2) {
        if (str == null) {
            Iterator<OneSignal.ChangeTagsUpdateHandler> it = this.sendTagsHandlers.iterator();
            while (it.hasNext()) {
                OneSignal.ChangeTagsUpdateHandler next = it.next();
                if (next != null) {
                    next.onFailure(new OneSignal.SendTagsError(-1, "Unable to update tags: the current user is not registered with OneSignal"));
                }
            }
            this.sendTagsHandlers.clear();
            return;
        }
        final ArrayList arrayList = (ArrayList) this.sendTagsHandlers.clone();
        this.sendTagsHandlers.clear();
        OneSignalRestClient.putSync("players/" + str, jSONObject, new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.WARN;
                OneSignal.Log(log_level, "Failed last request. statusCode: " + i + "\nresponse: " + str);
                synchronized (UserStateSynchronizer.this.syncLock) {
                    if (UserStateSynchronizer.this.response400WithErrorsContaining(i, str, "No user with this id found")) {
                        UserStateSynchronizer.this.handlePlayerDeletedFromServer();
                    } else {
                        UserStateSynchronizer.this.handleNetworkFailure();
                    }
                }
                if (jSONObject.has(FaqsColumns.TAGS)) {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        OneSignal.ChangeTagsUpdateHandler changeTagsUpdateHandler = (OneSignal.ChangeTagsUpdateHandler) it.next();
                        if (changeTagsUpdateHandler != null) {
                            changeTagsUpdateHandler.onFailure(new OneSignal.SendTagsError(i, str));
                        }
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                synchronized (UserStateSynchronizer.this.syncLock) {
                    UserStateSynchronizer.this.currentUserState.persistStateAfterSync(jSONObject2, jSONObject);
                    UserStateSynchronizer.this.onSuccessfulSync(jSONObject);
                }
                JSONObject jSONObject = OneSignalStateSynchronizer.getTags(false).result;
                if (jSONObject.has(FaqsColumns.TAGS) && jSONObject != null) {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        OneSignal.ChangeTagsUpdateHandler changeTagsUpdateHandler = (OneSignal.ChangeTagsUpdateHandler) it.next();
                        if (changeTagsUpdateHandler != null) {
                            changeTagsUpdateHandler.onSuccess(jSONObject);
                        }
                    }
                }
            }
        });
    }

    private void doCreateOrNewSession(final String str, final JSONObject jSONObject, final JSONObject jSONObject2) {
        String str2;
        if (str == null) {
            str2 = Games.EXTRA_PLAYER_IDS;
        } else {
            str2 = "players/" + str + "/on_session";
        }
        this.waitingForSessionResponse = true;
        addOnSessionOrCreateExtras(jSONObject);
        OneSignalRestClient.postSync(str2, jSONObject, new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                synchronized (UserStateSynchronizer.this.syncLock) {
                    UserStateSynchronizer.this.waitingForSessionResponse = false;
                    OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.WARN;
                    OneSignal.Log(log_level, "Failed last request. statusCode: " + i + "\nresponse: " + str);
                    if (UserStateSynchronizer.this.response400WithErrorsContaining(i, str, "not a valid device_type")) {
                        UserStateSynchronizer.this.handlePlayerDeletedFromServer();
                    } else {
                        UserStateSynchronizer.this.handleNetworkFailure();
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                synchronized (UserStateSynchronizer.this.syncLock) {
                    UserStateSynchronizer userStateSynchronizer = UserStateSynchronizer.this;
                    UserStateSynchronizer.this.waitingForSessionResponse = false;
                    userStateSynchronizer.nextSyncIsSession = false;
                    UserStateSynchronizer.this.currentUserState.persistStateAfterSync(jSONObject2, jSONObject);
                    try {
                        JSONObject jSONObject = new JSONObject(str);
                        if (jSONObject.has("id")) {
                            String optString = jSONObject.optString("id");
                            UserStateSynchronizer.this.updateIdDependents(optString);
                            OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.INFO;
                            OneSignal.Log(log_level, "Device registered, UserId = " + optString);
                        } else {
                            OneSignal.LOG_LEVEL log_level2 = OneSignal.LOG_LEVEL.INFO;
                            OneSignal.Log(log_level2, "session sent, UserId = " + str);
                        }
                        OneSignal.updateOnSessionDependents();
                        UserStateSynchronizer.this.onSuccessfulSync(jSONObject);
                    } catch (Throwable th) {
                        OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "ERROR parsing on_session or create JSON Response.", th);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void handleNetworkFailure() {
        if (!getNetworkHandlerThread(0).doRetry()) {
            JSONObject generateJsonDiff = this.currentUserState.generateJsonDiff(this.toSyncUserState, false);
            if (generateJsonDiff != null) {
                fireEventsForUpdateFailure(generateJsonDiff);
            }
            if (getToSyncUserState().dependValues.optBoolean("logoutEmail", false)) {
                OneSignal.handleFailedEmailLogout();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean response400WithErrorsContaining(int i, String str, String str2) {
        if (i == 400 && str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.has(ReportDBAdapter.ReportColumns.COLUMN_ERRORS) || !jSONObject.optString(ReportDBAdapter.ReportColumns.COLUMN_ERRORS).contains(str2)) {
                    return false;
                }
                return true;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public NetworkHandlerThread getNetworkHandlerThread(Integer num) {
        NetworkHandlerThread networkHandlerThread;
        synchronized (this.networkHandlerSyncLock) {
            if (!this.networkHandlerThreads.containsKey(num)) {
                this.networkHandlerThreads.put(num, new NetworkHandlerThread(num.intValue()));
            }
            networkHandlerThread = this.networkHandlerThreads.get(num);
        }
        return networkHandlerThread;
    }

    /* access modifiers changed from: protected */
    public UserState getUserStateForModification() {
        if (this.toSyncUserState == null) {
            this.toSyncUserState = this.currentUserState.deepClone("TOSYNC_STATE");
        }
        scheduleSyncToServer();
        return this.toSyncUserState;
    }

    /* access modifiers changed from: package-private */
    public void updateDeviceInfo(JSONObject jSONObject) {
        JSONObject jSONObject2 = getUserStateForModification().syncValues;
        generateJsonDiff(jSONObject2, jSONObject, jSONObject2, null);
    }

    /* access modifiers changed from: package-private */
    public void setSyncAsNewSession() {
        this.nextSyncIsSession = true;
    }

    /* access modifiers changed from: package-private */
    public void sendTags(JSONObject jSONObject, OneSignal.ChangeTagsUpdateHandler changeTagsUpdateHandler) {
        this.sendTagsHandlers.add(changeTagsUpdateHandler);
        JSONObject jSONObject2 = getUserStateForModification().syncValues;
        generateJsonDiff(jSONObject2, jSONObject, jSONObject2, null);
    }

    /* access modifiers changed from: package-private */
    public void syncHashedEmail(JSONObject jSONObject) {
        JSONObject jSONObject2 = getUserStateForModification().syncValues;
        generateJsonDiff(jSONObject2, jSONObject, jSONObject2, null);
    }

    /* access modifiers changed from: private */
    public void handlePlayerDeletedFromServer() {
        OneSignal.handleSuccessfulEmailLogout();
        resetCurrentState();
        this.nextSyncIsSession = true;
        scheduleSyncToServer();
    }

    /* access modifiers changed from: package-private */
    public void resetCurrentState() {
        this.currentUserState.syncValues = new JSONObject();
        this.currentUserState.persistState();
    }

    /* access modifiers changed from: package-private */
    public void updateLocation(LocationGMS.LocationPoint locationPoint) {
        getUserStateForModification().setLocation(locationPoint);
    }
}
