package com.onesignal;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import com.facebook.appevents.AppEventsConstants;
import com.helpshift.analytics.AnalyticsEventKey;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationPayload;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class NotificationBundleProcessor {
    static final String DEFAULT_ACTION = "__DEFAULT__";

    NotificationBundleProcessor() {
    }

    static void ProcessFromGCMIntentService(Context context, BundleCompat bundleCompat, NotificationExtenderService.OverrideSettings overrideSettings) {
        OneSignal.setAppContext(context);
        try {
            String string = bundleCompat.getString("json_payload");
            if (string == null) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.Log(log_level, "json_payload key is nonexistent from mBundle passed to ProcessFromGCMIntentService: " + bundleCompat);
                return;
            }
            NotificationGenerationJob notificationGenerationJob = new NotificationGenerationJob(context);
            notificationGenerationJob.restoring = bundleCompat.getBoolean("restoring", false);
            notificationGenerationJob.shownTimeStamp = bundleCompat.getLong("timestamp");
            notificationGenerationJob.jsonPayload = new JSONObject(string);
            if (notificationGenerationJob.restoring || !OneSignal.notValidOrDuplicated(context, notificationGenerationJob.jsonPayload)) {
                if (bundleCompat.containsKey("android_notif_id")) {
                    if (overrideSettings == null) {
                        overrideSettings = new NotificationExtenderService.OverrideSettings();
                    }
                    overrideSettings.androidNotificationId = bundleCompat.getInt("android_notif_id");
                }
                notificationGenerationJob.overrideSettings = overrideSettings;
                ProcessJobForDisplay(notificationGenerationJob);
                if (notificationGenerationJob.restoring) {
                    OSUtils.sleep(100);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static int ProcessJobForDisplay(NotificationGenerationJob notificationGenerationJob) {
        notificationGenerationJob.showAsAlert = OneSignal.getInAppAlertNotificationEnabled() && OneSignal.isAppActive();
        processCollapseKey(notificationGenerationJob);
        if (notificationGenerationJob.hasExtender() || shouldDisplay(notificationGenerationJob.jsonPayload.optString("alert"))) {
            GenerateNotification.fromJsonPayload(notificationGenerationJob);
        }
        if (!notificationGenerationJob.restoring) {
            saveNotification(notificationGenerationJob, false);
            try {
                JSONObject jSONObject = new JSONObject(notificationGenerationJob.jsonPayload.toString());
                jSONObject.put("notificationId", notificationGenerationJob.getAndroidId());
                OneSignal.handleNotificationReceived(newJsonArray(jSONObject), true, notificationGenerationJob.showAsAlert);
            } catch (Throwable unused) {
            }
        }
        return notificationGenerationJob.getAndroidId().intValue();
    }

    /* access modifiers changed from: private */
    public static JSONArray bundleAsJsonArray(Bundle bundle) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(bundleAsJSONObject(bundle));
        return jSONArray;
    }

    private static void saveNotification(Context context, Bundle bundle, boolean z, int i) {
        NotificationGenerationJob notificationGenerationJob = new NotificationGenerationJob(context);
        notificationGenerationJob.jsonPayload = bundleAsJSONObject(bundle);
        notificationGenerationJob.overrideSettings = new NotificationExtenderService.OverrideSettings();
        notificationGenerationJob.overrideSettings.androidNotificationId = Integer.valueOf(i);
        saveNotification(notificationGenerationJob, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x010e A[SYNTHETIC, Splitter:B:47:0x010e] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011a A[SYNTHETIC, Splitter:B:53:0x011a] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void saveNotification(com.onesignal.NotificationGenerationJob r9, boolean r10) {
        /*
            android.content.Context r0 = r9.context
            org.json.JSONObject r1 = r9.jsonPayload
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0127 }
            org.json.JSONObject r3 = r9.jsonPayload     // Catch:{ JSONException -> 0x0127 }
            java.lang.String r4 = "custom"
            java.lang.String r3 = r3.optString(r4)     // Catch:{ JSONException -> 0x0127 }
            r2.<init>(r3)     // Catch:{ JSONException -> 0x0127 }
            android.content.Context r3 = r9.context     // Catch:{ JSONException -> 0x0127 }
            com.onesignal.OneSignalDbHelper r3 = com.onesignal.OneSignalDbHelper.getInstance(r3)     // Catch:{ JSONException -> 0x0127 }
            r4 = 0
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDbWithRetries()     // Catch:{ Exception -> 0x0104 }
            r3.beginTransaction()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            deleteOldNotifications(r3)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            int r5 = r9.getAndroidIdWithoutCreate()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r6 = -1
            if (r5 == r6) goto L_0x0055
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r6 = "android_notification_id = "
            r5.append(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            int r6 = r9.getAndroidIdWithoutCreate()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.append(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r6.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r7 = "dismissed"
            r8 = 1
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r6.put(r7, r8)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r7 = "notification"
            r3.update(r7, r6, r5, r4)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            com.onesignal.BadgeCountUpdater.update(r3, r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x0055:
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r6 = "notification_id"
            java.lang.String r7 = "i"
            java.lang.String r2 = r2.optString(r7)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r6, r2)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r2 = "grp"
            boolean r2 = r1.has(r2)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r2 == 0) goto L_0x0078
            java.lang.String r2 = "group_id"
            java.lang.String r6 = "grp"
            java.lang.String r6 = r1.optString(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r2, r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x0078:
            java.lang.String r2 = "collapse_key"
            boolean r2 = r1.has(r2)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r2 == 0) goto L_0x0099
            java.lang.String r2 = "do_not_collapse"
            java.lang.String r6 = "collapse_key"
            java.lang.String r6 = r1.optString(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            boolean r2 = r2.equals(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r2 != 0) goto L_0x0099
            java.lang.String r2 = "collapse_id"
            java.lang.String r6 = "collapse_key"
            java.lang.String r6 = r1.optString(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r2, r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x0099:
            java.lang.String r2 = "opened"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r2, r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r10 != 0) goto L_0x00b1
            java.lang.String r2 = "android_notification_id"
            int r6 = r9.getAndroidIdWithoutCreate()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r2, r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x00b1:
            java.lang.CharSequence r2 = r9.getTitle()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r2 == 0) goto L_0x00c4
            java.lang.String r2 = "title"
            java.lang.CharSequence r6 = r9.getTitle()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r2, r6)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x00c4:
            java.lang.CharSequence r2 = r9.getBody()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r2 == 0) goto L_0x00d7
            java.lang.String r2 = "message"
            java.lang.CharSequence r9 = r9.getBody()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r2, r9)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x00d7:
            java.lang.String r9 = "full_data"
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            r5.put(r9, r1)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            java.lang.String r9 = "notification"
            r3.insertOrThrow(r9, r4, r5)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r10 != 0) goto L_0x00ea
            com.onesignal.BadgeCountUpdater.update(r3, r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
        L_0x00ea:
            r3.setTransactionSuccessful()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            if (r3 == 0) goto L_0x012b
            r3.endTransaction()     // Catch:{ Throwable -> 0x00f3 }
            goto L_0x012b
        L_0x00f3:
            r9 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r10 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ JSONException -> 0x0127 }
            java.lang.String r0 = "Error closing transaction! "
        L_0x00f8:
            com.onesignal.OneSignal.Log(r10, r0, r9)     // Catch:{ JSONException -> 0x0127 }
            goto L_0x012b
        L_0x00fc:
            r9 = move-exception
            goto L_0x0118
        L_0x00fe:
            r9 = move-exception
            r4 = r3
            goto L_0x0105
        L_0x0101:
            r9 = move-exception
            r3 = r4
            goto L_0x0118
        L_0x0104:
            r9 = move-exception
        L_0x0105:
            com.onesignal.OneSignal$LOG_LEVEL r10 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0101 }
            java.lang.String r0 = "Error saving notification record! "
            com.onesignal.OneSignal.Log(r10, r0, r9)     // Catch:{ all -> 0x0101 }
            if (r4 == 0) goto L_0x012b
            r4.endTransaction()     // Catch:{ Throwable -> 0x0112 }
            goto L_0x012b
        L_0x0112:
            r9 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r10 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ JSONException -> 0x0127 }
            java.lang.String r0 = "Error closing transaction! "
            goto L_0x00f8
        L_0x0118:
            if (r3 == 0) goto L_0x0126
            r3.endTransaction()     // Catch:{ Throwable -> 0x011e }
            goto L_0x0126
        L_0x011e:
            r10 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ JSONException -> 0x0127 }
            java.lang.String r1 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r0, r1, r10)     // Catch:{ JSONException -> 0x0127 }
        L_0x0126:
            throw r9     // Catch:{ JSONException -> 0x0127 }
        L_0x0127:
            r9 = move-exception
            r9.printStackTrace()
        L_0x012b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.NotificationBundleProcessor.saveNotification(com.onesignal.NotificationGenerationJob, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005f A[SYNTHETIC, Splitter:B:20:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006e A[SYNTHETIC, Splitter:B:25:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void markRestoredNotificationAsDismissed(com.onesignal.NotificationGenerationJob r6) {
        /*
            int r0 = r6.getAndroidIdWithoutCreate()
            r1 = -1
            if (r0 != r1) goto L_0x0008
            return
        L_0x0008:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "android_notification_id = "
            r0.append(r1)
            int r1 = r6.getAndroidIdWithoutCreate()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.content.Context r1 = r6.context
            com.onesignal.OneSignalDbHelper r1 = com.onesignal.OneSignalDbHelper.getInstance(r1)
            r2 = 0
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDbWithRetries()     // Catch:{ Exception -> 0x0055 }
            r1.beginTransaction()     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            r3.<init>()     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.String r4 = "dismissed"
            r5 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.String r4 = "notification"
            r1.update(r4, r3, r0, r2)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            android.content.Context r6 = r6.context     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            com.onesignal.BadgeCountUpdater.update(r1, r6)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            if (r1 == 0) goto L_0x006b
            r1.endTransaction()     // Catch:{ Throwable -> 0x0063 }
            goto L_0x006b
        L_0x004d:
            r6 = move-exception
            goto L_0x006c
        L_0x004f:
            r6 = move-exception
            r2 = r1
            goto L_0x0056
        L_0x0052:
            r6 = move-exception
            r1 = r2
            goto L_0x006c
        L_0x0055:
            r6 = move-exception
        L_0x0056:
            com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = "Error saving notification record! "
            com.onesignal.OneSignal.Log(r0, r1, r6)     // Catch:{ all -> 0x0052 }
            if (r2 == 0) goto L_0x006b
            r2.endTransaction()     // Catch:{ Throwable -> 0x0063 }
            goto L_0x006b
        L_0x0063:
            r6 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
            java.lang.String r1 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r0, r1, r6)
        L_0x006b:
            return
        L_0x006c:
            if (r1 == 0) goto L_0x007a
            r1.endTransaction()     // Catch:{ Throwable -> 0x0072 }
            goto L_0x007a
        L_0x0072:
            r0 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
            java.lang.String r2 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r1, r2, r0)
        L_0x007a:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.NotificationBundleProcessor.markRestoredNotificationAsDismissed(com.onesignal.NotificationGenerationJob):void");
    }

    static void deleteOldNotifications(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.delete(OneSignalDbContract.NotificationTable.TABLE_NAME, "created_time < " + ((System.currentTimeMillis() / 1000) - 604800), null);
    }

    static JSONObject bundleAsJSONObject(Bundle bundle) {
        JSONObject jSONObject = new JSONObject();
        for (String next : bundle.keySet()) {
            try {
                jSONObject.put(next, bundle.get(next));
            } catch (JSONException e) {
                OneSignal.LOG_LEVEL log_level = OneSignal.LOG_LEVEL.ERROR;
                OneSignal.Log(log_level, "bundleAsJSONObject error for key: " + next, e);
            }
        }
        return jSONObject;
    }

    private static void unMinifyBundle(Bundle bundle) {
        JSONObject jSONObject;
        String str;
        if (bundle.containsKey("o")) {
            try {
                JSONObject jSONObject2 = new JSONObject(bundle.getString("custom"));
                if (jSONObject2.has("a")) {
                    jSONObject = jSONObject2.getJSONObject("a");
                } else {
                    jSONObject = new JSONObject();
                }
                JSONArray jSONArray = new JSONArray(bundle.getString("o"));
                bundle.remove("o");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                    String string = jSONObject3.getString(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT);
                    jSONObject3.remove(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT);
                    if (jSONObject3.has("i")) {
                        str = jSONObject3.getString("i");
                        jSONObject3.remove("i");
                    } else {
                        str = string;
                    }
                    jSONObject3.put("id", str);
                    jSONObject3.put("text", string);
                    if (jSONObject3.has(AnalyticsEventKey.PROTOCOL)) {
                        jSONObject3.put(SettingsJsonConstants.APP_ICON_KEY, jSONObject3.getString(AnalyticsEventKey.PROTOCOL));
                        jSONObject3.remove(AnalyticsEventKey.PROTOCOL);
                    }
                }
                jSONObject.put("actionButtons", jSONArray);
                jSONObject.put("actionSelected", DEFAULT_ACTION);
                if (!jSONObject2.has("a")) {
                    jSONObject2.put("a", jSONObject);
                }
                bundle.putString("custom", jSONObject2.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    static OSNotificationPayload OSNotificationPayloadFrom(JSONObject jSONObject) {
        OSNotificationPayload oSNotificationPayload = new OSNotificationPayload();
        try {
            JSONObject jSONObject2 = new JSONObject(jSONObject.optString("custom"));
            oSNotificationPayload.notificationID = jSONObject2.optString("i");
            oSNotificationPayload.templateId = jSONObject2.optString("ti");
            oSNotificationPayload.templateName = jSONObject2.optString("tn");
            oSNotificationPayload.rawPayload = jSONObject.toString();
            oSNotificationPayload.additionalData = jSONObject2.optJSONObject("a");
            oSNotificationPayload.launchURL = jSONObject2.optString(AnalyticsEventKey.URL, null);
            oSNotificationPayload.body = jSONObject.optString("alert", null);
            oSNotificationPayload.title = jSONObject.optString("title", null);
            oSNotificationPayload.smallIcon = jSONObject.optString("sicon", null);
            oSNotificationPayload.bigPicture = jSONObject.optString("bicon", null);
            oSNotificationPayload.largeIcon = jSONObject.optString("licon", null);
            oSNotificationPayload.sound = jSONObject.optString("sound", null);
            oSNotificationPayload.groupKey = jSONObject.optString("grp", null);
            oSNotificationPayload.groupMessage = jSONObject.optString("grp_msg", null);
            oSNotificationPayload.smallIconAccentColor = jSONObject.optString("bgac", null);
            oSNotificationPayload.ledColor = jSONObject.optString("ledc", null);
            String optString = jSONObject.optString("vis", null);
            if (optString != null) {
                oSNotificationPayload.lockScreenVisibility = Integer.parseInt(optString);
            }
            oSNotificationPayload.fromProjectNumber = jSONObject.optString("from", null);
            oSNotificationPayload.priority = jSONObject.optInt("pri", 0);
            String optString2 = jSONObject.optString("collapse_key", null);
            if (!"do_not_collapse".equals(optString2)) {
                oSNotificationPayload.collapseId = optString2;
            }
            setActionButtons(oSNotificationPayload);
        } catch (Throwable th) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Error assigning OSNotificationPayload values!", th);
        }
        try {
            setBackgroundImageLayout(oSNotificationPayload, jSONObject);
        } catch (Throwable th2) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Error assigning OSNotificationPayload.backgroundImageLayout values!", th2);
        }
        return oSNotificationPayload;
    }

    private static void setActionButtons(OSNotificationPayload oSNotificationPayload) throws Throwable {
        if (oSNotificationPayload.additionalData != null && oSNotificationPayload.additionalData.has("actionButtons")) {
            JSONArray jSONArray = oSNotificationPayload.additionalData.getJSONArray("actionButtons");
            oSNotificationPayload.actionButtons = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                OSNotificationPayload.ActionButton actionButton = new OSNotificationPayload.ActionButton();
                actionButton.id = jSONObject.optString("id", null);
                actionButton.text = jSONObject.optString("text", null);
                actionButton.icon = jSONObject.optString(SettingsJsonConstants.APP_ICON_KEY, null);
                oSNotificationPayload.actionButtons.add(actionButton);
            }
            oSNotificationPayload.additionalData.remove("actionSelected");
            oSNotificationPayload.additionalData.remove("actionButtons");
        }
    }

    private static void setBackgroundImageLayout(OSNotificationPayload oSNotificationPayload, JSONObject jSONObject) throws Throwable {
        String optString = jSONObject.optString("bg_img", null);
        if (optString != null) {
            JSONObject jSONObject2 = new JSONObject(optString);
            oSNotificationPayload.backgroundImageLayout = new OSNotificationPayload.BackgroundImageLayout();
            oSNotificationPayload.backgroundImageLayout.image = jSONObject2.optString("img");
            oSNotificationPayload.backgroundImageLayout.titleTextColor = jSONObject2.optString("tc");
            oSNotificationPayload.backgroundImageLayout.bodyTextColor = jSONObject2.optString("bc");
        }
    }

    private static void processCollapseKey(NotificationGenerationJob notificationGenerationJob) {
        if (!notificationGenerationJob.restoring && notificationGenerationJob.jsonPayload.has("collapse_key") && !"do_not_collapse".equals(notificationGenerationJob.jsonPayload.optString("collapse_key"))) {
            String optString = notificationGenerationJob.jsonPayload.optString("collapse_key");
            Cursor cursor = null;
            try {
                Cursor query = OneSignalDbHelper.getInstance(notificationGenerationJob.context).getReadableDbWithRetries().query(OneSignalDbContract.NotificationTable.TABLE_NAME, new String[]{OneSignalDbContract.NotificationTable.COLUMN_NAME_ANDROID_NOTIFICATION_ID}, "collapse_id = ? AND dismissed = 0 AND opened = 0 ", new String[]{optString}, null, null, null);
                try {
                    if (query.moveToFirst()) {
                        notificationGenerationJob.setAndroidIdWithOutOverriding(Integer.valueOf(query.getInt(query.getColumnIndex(OneSignalDbContract.NotificationTable.COLUMN_NAME_ANDROID_NOTIFICATION_ID))));
                    }
                    if (query != null && !query.isClosed()) {
                        query.close();
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = query;
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Could not read DB to find existing collapse_key!", th);
                if (cursor != null) {
                }
            }
        }
    }

    static ProcessedBundleResult processBundleFromReceiver(Context context, final Bundle bundle) {
        ProcessedBundleResult processedBundleResult = new ProcessedBundleResult();
        if (OneSignal.getNotificationIdFromGCMBundle(bundle) == null) {
            return processedBundleResult;
        }
        processedBundleResult.isOneSignalPayload = true;
        unMinifyBundle(bundle);
        if (startExtenderService(context, bundle, processedBundleResult)) {
            return processedBundleResult;
        }
        processedBundleResult.isDup = OneSignal.notValidOrDuplicated(context, bundleAsJSONObject(bundle));
        if (!processedBundleResult.isDup && !shouldDisplay(bundle.getString("alert"))) {
            saveNotification(context, bundle, true, -1);
            new Thread(new Runnable() {
                public void run() {
                    OneSignal.handleNotificationReceived(NotificationBundleProcessor.bundleAsJsonArray(bundle), false, false);
                }
            }, "OS_PROC_BUNDLE").start();
        }
        return processedBundleResult;
    }

    private static boolean startExtenderService(Context context, Bundle bundle, ProcessedBundleResult processedBundleResult) {
        Intent intent = NotificationExtenderService.getIntent(context);
        boolean z = false;
        if (intent == null) {
            return false;
        }
        intent.putExtra("json_payload", bundleAsJSONObject(bundle).toString());
        intent.putExtra("timestamp", System.currentTimeMillis() / 1000);
        if (Integer.parseInt(bundle.getString("pri", AppEventsConstants.EVENT_PARAM_VALUE_NO)) > 9) {
            z = true;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            NotificationExtenderService.enqueueWork(context, intent.getComponent(), 2071862121, intent, z);
        } else {
            context.startService(intent);
        }
        processedBundleResult.hasExtenderService = true;
        return true;
    }

    static boolean shouldDisplay(String str) {
        boolean z = str != null && !"".equals(str);
        boolean inAppAlertNotificationEnabled = OneSignal.getInAppAlertNotificationEnabled();
        boolean isAppActive = OneSignal.isAppActive();
        if (!z || (!OneSignal.getNotificationsWhenActiveEnabled() && !inAppAlertNotificationEnabled && isAppActive)) {
            return false;
        }
        return true;
    }

    static JSONArray newJsonArray(JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject);
        return jSONArray;
    }

    static boolean hasRemoteResource(Bundle bundle) {
        return isBuildKeyRemote(bundle, "licon") || isBuildKeyRemote(bundle, "bicon") || bundle.getString("bg_img", null) != null;
    }

    private static boolean isBuildKeyRemote(Bundle bundle, String str) {
        String trim = bundle.getString(str, "").trim();
        return trim.startsWith("http://") || trim.startsWith("https://");
    }

    static class ProcessedBundleResult {
        boolean hasExtenderService;
        boolean isDup;
        boolean isOneSignalPayload;

        ProcessedBundleResult() {
        }

        /* access modifiers changed from: package-private */
        public boolean processed() {
            return !this.isOneSignalPayload || this.hasExtenderService || this.isDup;
        }
    }
}
