package com.onesignal;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import java.util.ArrayList;

class NotificationRestorer {
    static final String[] COLUMNS_FOR_RESTORE = {OneSignalDbContract.NotificationTable.COLUMN_NAME_ANDROID_NOTIFICATION_ID, OneSignalDbContract.NotificationTable.COLUMN_NAME_FULL_DATA, OneSignalDbContract.NotificationTable.COLUMN_NAME_CREATED_TIME};
    private static final int DELAY_BETWEEN_NOTIFICATION_RESTORES_MS = 200;
    private static final String MAX_NUMBER_OF_NOTIFICATIONS_TO_RESTORE = "49";
    private static final int RESTORE_KICKOFF_REQUEST_CODE = 2071862120;
    private static final int RESTORE_NOTIFICATIONS_DELAY_MS = 15000;
    public static boolean restored;

    NotificationRestorer() {
    }

    static void asyncRestore(final Context context) {
        new Thread(new Runnable() {
            public void run() {
                Thread.currentThread().setPriority(10);
                NotificationRestorer.restore(context);
            }
        }, "OS_RESTORE_NOTIFS").start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0044 A[SYNTHETIC, Splitter:B:22:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00fa A[SYNTHETIC, Splitter:B:52:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void restore(android.content.Context r14) {
        /*
            boolean r0 = com.onesignal.NotificationRestorer.restored
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            r0 = 1
            com.onesignal.NotificationRestorer.restored = r0
            com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.INFO
            java.lang.String r1 = "Restoring notifications"
            com.onesignal.OneSignal.Log(r0, r1)
            com.onesignal.OneSignalDbHelper r0 = com.onesignal.OneSignalDbHelper.getInstance(r14)
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r0.getWritableDbWithRetries()     // Catch:{ Throwable -> 0x0039, all -> 0x0035 }
            r2.beginTransaction()     // Catch:{ Throwable -> 0x0033 }
            com.onesignal.NotificationBundleProcessor.deleteOldNotifications(r2)     // Catch:{ Throwable -> 0x0033 }
            r2.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0033 }
            if (r2 == 0) goto L_0x0047
            r2.endTransaction()     // Catch:{ Throwable -> 0x0027 }
            goto L_0x0047
        L_0x0027:
            r2 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r3 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
            java.lang.String r4 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r3, r4, r2)
            goto L_0x0047
        L_0x0030:
            r14 = move-exception
            goto L_0x00f8
        L_0x0033:
            r3 = move-exception
            goto L_0x003b
        L_0x0035:
            r14 = move-exception
            r2 = r1
            goto L_0x00f8
        L_0x0039:
            r3 = move-exception
            r2 = r1
        L_0x003b:
            com.onesignal.OneSignal$LOG_LEVEL r4 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0030 }
            java.lang.String r5 = "Error deleting old notification records! "
            com.onesignal.OneSignal.Log(r4, r5, r3)     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0047
            r2.endTransaction()     // Catch:{ Throwable -> 0x0027 }
        L_0x0047:
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r4 = 604800(0x93a80, double:2.98811E-318)
            long r2 = r2 - r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "created_time > "
            r5.append(r6)
            r5.append(r2)
            java.lang.String r2 = " AND "
            r5.append(r2)
            java.lang.String r2 = "dismissed"
            r5.append(r2)
            java.lang.String r2 = " = 0 AND "
            r5.append(r2)
            java.lang.String r2 = "opened"
            r5.append(r2)
            java.lang.String r2 = " = 0 AND "
            r5.append(r2)
            java.lang.String r2 = "is_summary"
            r5.append(r2)
            java.lang.String r2 = " = 0"
            r5.append(r2)
            java.lang.String r2 = r5.toString()
            r4.<init>(r2)
            skipVisibleNotifications(r14, r4)
            com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.INFO
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "Querying DB for notifs to restore: "
            r3.append(r5)
            java.lang.String r5 = r4.toString()
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            com.onesignal.OneSignal.Log(r2, r3)
            android.database.sqlite.SQLiteDatabase r5 = r0.getReadableDbWithRetries()     // Catch:{ Throwable -> 0x00d8 }
            java.lang.String r6 = "notification"
            java.lang.String[] r7 = com.onesignal.NotificationRestorer.COLUMNS_FOR_RESTORE     // Catch:{ Throwable -> 0x00d8 }
            java.lang.String r8 = r4.toString()     // Catch:{ Throwable -> 0x00d8 }
            r9 = 0
            r10 = 0
            r11 = 0
            java.lang.String r12 = "_id DESC"
            java.lang.String r13 = "49"
            android.database.Cursor r0 = r5.query(r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ Throwable -> 0x00d8 }
            r1 = 200(0xc8, float:2.8E-43)
            showNotifications(r14, r0, r1)     // Catch:{ Throwable -> 0x00d3, all -> 0x00d0 }
            if (r0 == 0) goto L_0x00eb
            boolean r14 = r0.isClosed()
            if (r14 != 0) goto L_0x00eb
            r0.close()
            goto L_0x00eb
        L_0x00d0:
            r14 = move-exception
            r1 = r0
            goto L_0x00ec
        L_0x00d3:
            r14 = move-exception
            r1 = r0
            goto L_0x00d9
        L_0x00d6:
            r14 = move-exception
            goto L_0x00ec
        L_0x00d8:
            r14 = move-exception
        L_0x00d9:
            com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = "Error restoring notification records! "
            com.onesignal.OneSignal.Log(r0, r2, r14)     // Catch:{ all -> 0x00d6 }
            if (r1 == 0) goto L_0x00eb
            boolean r14 = r1.isClosed()
            if (r14 != 0) goto L_0x00eb
            r1.close()
        L_0x00eb:
            return
        L_0x00ec:
            if (r1 == 0) goto L_0x00f7
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x00f7
            r1.close()
        L_0x00f7:
            throw r14
        L_0x00f8:
            if (r2 == 0) goto L_0x0106
            r2.endTransaction()     // Catch:{ Throwable -> 0x00fe }
            goto L_0x0106
        L_0x00fe:
            r0 = move-exception
            com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
            java.lang.String r2 = "Error closing transaction! "
            com.onesignal.OneSignal.Log(r1, r2, r0)
        L_0x0106:
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.NotificationRestorer.restore(android.content.Context):void");
    }

    private static void skipVisibleNotifications(Context context, StringBuilder sb) {
        NotificationManager notificationManager;
        if (Build.VERSION.SDK_INT >= 23 && (notificationManager = (NotificationManager) context.getSystemService((String) OneSignalDbContract.NotificationTable.TABLE_NAME)) != null) {
            try {
                StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();
                if (activeNotifications.length != 0) {
                    ArrayList arrayList = new ArrayList();
                    for (StatusBarNotification id : activeNotifications) {
                        arrayList.add(Integer.valueOf(id.getId()));
                    }
                    sb.append(" AND android_notification_id NOT IN (");
                    sb.append(TextUtils.join(",", arrayList));
                    sb.append(")");
                }
            } catch (Throwable unused) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.onesignal.JobIntentService.enqueueWork(android.content.Context, android.content.ComponentName, int, android.content.Intent, boolean):void
     arg types: [android.content.Context, android.content.ComponentName, int, android.content.Intent, int]
     candidates:
      com.onesignal.JobIntentService.enqueueWork(android.content.Context, java.lang.Class, int, android.content.Intent, boolean):void
      com.onesignal.JobIntentService.enqueueWork(android.content.Context, android.content.ComponentName, int, android.content.Intent, boolean):void */
    static void showNotifications(Context context, Cursor cursor, int i) {
        if (cursor.moveToFirst()) {
            boolean z = NotificationExtenderService.getIntent(context) != null;
            do {
                if (z) {
                    Intent intent = NotificationExtenderService.getIntent(context);
                    addRestoreExtras(intent, cursor);
                    NotificationExtenderService.enqueueWork(context, intent.getComponent(), 2071862121, intent, false);
                } else {
                    RestoreJobService.enqueueWork(context, new ComponentName(context, RestoreJobService.class), 2071862122, addRestoreExtras(new Intent(), cursor), false);
                }
                if (i > 0) {
                    OSUtils.sleep(i);
                }
            } while (cursor.moveToNext());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static Intent addRestoreExtras(Intent intent, Cursor cursor) {
        int i = cursor.getInt(cursor.getColumnIndex(OneSignalDbContract.NotificationTable.COLUMN_NAME_ANDROID_NOTIFICATION_ID));
        String string = cursor.getString(cursor.getColumnIndex(OneSignalDbContract.NotificationTable.COLUMN_NAME_FULL_DATA));
        intent.putExtra("json_payload", string).putExtra("android_notif_id", i).putExtra("restoring", true).putExtra("timestamp", Long.valueOf(cursor.getLong(cursor.getColumnIndex(OneSignalDbContract.NotificationTable.COLUMN_NAME_CREATED_TIME))));
        return intent;
    }

    static void startDelayedRestoreTaskFromReceiver(Context context) {
        if (Build.VERSION.SDK_INT >= 26) {
            OneSignal.Log(OneSignal.LOG_LEVEL.INFO, "scheduleRestoreKickoffJob");
            ((JobScheduler) context.getSystemService("jobscheduler")).schedule(new JobInfo.Builder(RESTORE_KICKOFF_REQUEST_CODE, new ComponentName(context, RestoreKickoffJobService.class)).setOverrideDeadline(MTGInterstitialActivity.WEB_LOAD_TIME).setMinimumLatency(MTGInterstitialActivity.WEB_LOAD_TIME).build());
            return;
        }
        OneSignal.Log(OneSignal.LOG_LEVEL.INFO, "scheduleRestoreKickoffAlarmTask");
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context.getPackageName(), NotificationRestoreService.class.getName()));
        PendingIntent service = PendingIntent.getService(context, RESTORE_KICKOFF_REQUEST_CODE, intent, DriveFile.MODE_READ_ONLY);
        ((AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM)).set(1, System.currentTimeMillis() + MTGInterstitialActivity.WEB_LOAD_TIME, service);
    }
}
