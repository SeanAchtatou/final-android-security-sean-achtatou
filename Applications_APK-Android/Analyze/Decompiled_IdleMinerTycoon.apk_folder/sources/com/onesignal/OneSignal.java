package com.onesignal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.appsflyer.share.Constants;
import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.common.domain.network.NetworkConstants;
import com.onesignal.LocationGMS;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OneSignalDbContract;
import com.onesignal.OneSignalRestClient;
import com.onesignal.PushRegistrator;
import com.onesignal.UserStateSynchronizer;
import com.vungle.warren.model.ReportDBAdapter;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OneSignal {
    static final long MIN_ON_FOCUS_TIME = 60;
    private static final long MIN_ON_SESSION_TIME = 30;
    public static final String VERSION = "031003";
    /* access modifiers changed from: private */
    public static int androidParamsReties = 0;
    static Context appContext;
    static String appId;
    /* access modifiers changed from: private */
    public static JSONObject awl;
    /* access modifiers changed from: private */
    public static boolean awlFired;
    private static OSEmailSubscriptionState currentEmailSubscriptionState;
    private static OSPermissionState currentPermissionState;
    private static OSSubscriptionState currentSubscriptionState;
    static DelayedConsentInitializationParameters delayedInitParams;
    private static int deviceType;
    private static String emailId = null;
    private static EmailUpdateHandler emailLogoutHandler;
    private static OSObservable<OSEmailSubscriptionObserver, OSEmailSubscriptionStateChanges> emailSubscriptionStateChangesObserver;
    private static EmailUpdateHandler emailUpdateHandler;
    private static boolean foreground;
    /* access modifiers changed from: private */
    public static boolean getTagsCall;
    private static IAPUpdateJob iapUpdateJob;
    private static IdsAvailableHandler idsAvailableHandler;
    static boolean initDone;
    static OSEmailSubscriptionState lastEmailSubscriptionState;
    /* access modifiers changed from: private */
    public static LocationGMS.LocationPoint lastLocationPoint;
    static OSPermissionState lastPermissionState;
    /* access modifiers changed from: private */
    public static String lastRegistrationId;
    static OSSubscriptionState lastSubscriptionState;
    static AtomicLong lastTaskId = new AtomicLong();
    private static long lastTrackedFocusTime = 1;
    /* access modifiers changed from: private */
    public static boolean locationFired;
    private static LOG_LEVEL logCatLevel = LOG_LEVEL.WARN;
    static boolean mEnterp;
    /* access modifiers changed from: private */
    public static String mGoogleProjectNumber;
    /* access modifiers changed from: private */
    public static boolean mGoogleProjectNumberIsRemote;
    static Builder mInitBuilder;
    private static PushRegistrator mPushRegistrator;
    private static AdvertisingIdentifierProvider mainAdIdProvider = new AdvertisingIdProviderGPS();
    private static OSUtils osUtils;
    /* access modifiers changed from: private */
    public static GetTagsHandler pendingGetTagsHandler;
    static ExecutorService pendingTaskExecutor;
    private static OSObservable<OSPermissionObserver, OSPermissionStateChanges> permissionStateChangesObserver;
    private static HashSet<String> postedOpenedNotifIds = new HashSet<>();
    /* access modifiers changed from: private */
    public static boolean promptedLocation;
    /* access modifiers changed from: private */
    public static boolean registerForPushFired;
    static boolean requiresUserPrivacyConsent = false;
    public static String sdkType = "native";
    private static boolean sendAsSession;
    static boolean shareLocation = true;
    /* access modifiers changed from: private */
    public static int subscribableStatus;
    private static OSObservable<OSSubscriptionObserver, OSSubscriptionStateChanges> subscriptionStateChangesObserver;
    public static ConcurrentLinkedQueue<Runnable> taskQueueWaitingForInit = new ConcurrentLinkedQueue<>();
    private static TrackAmazonPurchase trackAmazonPurchase;
    private static TrackFirebaseAnalytics trackFirebaseAnalytics;
    private static TrackGooglePurchase trackGooglePurchase;
    private static long unSentActiveTime = -1;
    private static Collection<JSONArray> unprocessedOpenedNotifis = new ArrayList();
    /* access modifiers changed from: private */
    public static boolean useEmailAuth;
    /* access modifiers changed from: private */
    public static String userId = null;
    private static LOG_LEVEL visualLogLevel = LOG_LEVEL.NONE;
    private static boolean waitingToPostStateSync;

    public interface ChangeTagsUpdateHandler {
        void onFailure(SendTagsError sendTagsError);

        void onSuccess(JSONObject jSONObject);
    }

    public enum EmailErrorType {
        VALIDATION,
        REQUIRES_EMAIL_AUTH,
        INVALID_OPERATION,
        NETWORK
    }

    public interface EmailUpdateHandler {
        void onFailure(EmailUpdateError emailUpdateError);

        void onSuccess();
    }

    public interface GetTagsHandler {
        void tagsAvailable(JSONObject jSONObject);
    }

    public interface IdsAvailableHandler {
        void idsAvailable(String str, String str2);
    }

    public enum LOG_LEVEL {
        NONE,
        FATAL,
        ERROR,
        WARN,
        INFO,
        DEBUG,
        VERBOSE
    }

    public interface NotificationOpenedHandler {
        void notificationOpened(OSNotificationOpenResult oSNotificationOpenResult);
    }

    public interface NotificationReceivedHandler {
        void notificationReceived(OSNotification oSNotification);
    }

    public enum OSInFocusDisplayOption {
        None,
        InAppAlert,
        Notification
    }

    public interface PostNotificationResponseHandler {
        void onFailure(JSONObject jSONObject);

        void onSuccess(JSONObject jSONObject);
    }

    /* access modifiers changed from: private */
    public static boolean pushStatusRuntimeError(int i) {
        return i < -6;
    }

    static /* synthetic */ int access$1308() {
        int i = androidParamsReties;
        androidParamsReties = i + 1;
        return i;
    }

    public static class SendTagsError {
        private int code;
        private String message;

        SendTagsError(int i, String str) {
            this.message = str;
            this.code = i;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public static class EmailUpdateError {
        private String message;
        private EmailErrorType type;

        EmailUpdateError(EmailErrorType emailErrorType, String str) {
            this.type = emailErrorType;
            this.message = str;
        }

        public EmailErrorType getType() {
            return this.type;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public static class Builder {
        Context mContext;
        boolean mDisableGmsMissingPrompt;
        OSInFocusDisplayOption mDisplayOption;
        boolean mDisplayOptionCarryOver;
        boolean mFilterOtherGCMReceivers;
        NotificationOpenedHandler mNotificationOpenedHandler;
        NotificationReceivedHandler mNotificationReceivedHandler;
        boolean mPromptLocation;
        boolean mUnsubscribeWhenNotificationsAreDisabled;

        private Builder() {
            this.mDisplayOption = OSInFocusDisplayOption.InAppAlert;
        }

        private Builder(Context context) {
            this.mDisplayOption = OSInFocusDisplayOption.InAppAlert;
            this.mContext = context;
        }

        private void setDisplayOptionCarryOver(boolean z) {
            this.mDisplayOptionCarryOver = z;
        }

        public Builder setNotificationOpenedHandler(NotificationOpenedHandler notificationOpenedHandler) {
            this.mNotificationOpenedHandler = notificationOpenedHandler;
            return this;
        }

        public Builder setNotificationReceivedHandler(NotificationReceivedHandler notificationReceivedHandler) {
            this.mNotificationReceivedHandler = notificationReceivedHandler;
            return this;
        }

        public Builder autoPromptLocation(boolean z) {
            this.mPromptLocation = z;
            return this;
        }

        public Builder disableGmsMissingPrompt(boolean z) {
            this.mDisableGmsMissingPrompt = z;
            return this;
        }

        public Builder inFocusDisplaying(OSInFocusDisplayOption oSInFocusDisplayOption) {
            OneSignal.getCurrentOrNewInitBuilder().mDisplayOptionCarryOver = false;
            this.mDisplayOption = oSInFocusDisplayOption;
            return this;
        }

        public Builder unsubscribeWhenNotificationsAreDisabled(boolean z) {
            this.mUnsubscribeWhenNotificationsAreDisabled = z;
            return this;
        }

        public Builder filterOtherGCMReceivers(boolean z) {
            this.mFilterOtherGCMReceivers = z;
            return this;
        }

        public void init() {
            OneSignal.init(this);
        }
    }

    private static OSPermissionState getCurrentPermissionState(Context context) {
        if (context == null) {
            return null;
        }
        if (currentPermissionState == null) {
            currentPermissionState = new OSPermissionState(false);
            currentPermissionState.observable.addObserverStrong(new OSPermissionChangedInternalObserver());
        }
        return currentPermissionState;
    }

    private static OSPermissionState getLastPermissionState(Context context) {
        if (context == null) {
            return null;
        }
        if (lastPermissionState == null) {
            lastPermissionState = new OSPermissionState(true);
        }
        return lastPermissionState;
    }

    static OSObservable<OSPermissionObserver, OSPermissionStateChanges> getPermissionStateChangesObserver() {
        if (permissionStateChangesObserver == null) {
            permissionStateChangesObserver = new OSObservable<>("onOSPermissionChanged", true);
        }
        return permissionStateChangesObserver;
    }

    /* access modifiers changed from: private */
    public static OSSubscriptionState getCurrentSubscriptionState(Context context) {
        if (context == null) {
            return null;
        }
        if (currentSubscriptionState == null) {
            currentSubscriptionState = new OSSubscriptionState(false, getCurrentPermissionState(context).getEnabled());
            getCurrentPermissionState(context).observable.addObserver(currentSubscriptionState);
            currentSubscriptionState.observable.addObserverStrong(new OSSubscriptionChangedInternalObserver());
        }
        return currentSubscriptionState;
    }

    private static OSSubscriptionState getLastSubscriptionState(Context context) {
        if (context == null) {
            return null;
        }
        if (lastSubscriptionState == null) {
            lastSubscriptionState = new OSSubscriptionState(true, false);
        }
        return lastSubscriptionState;
    }

    static OSObservable<OSSubscriptionObserver, OSSubscriptionStateChanges> getSubscriptionStateChangesObserver() {
        if (subscriptionStateChangesObserver == null) {
            subscriptionStateChangesObserver = new OSObservable<>("onOSSubscriptionChanged", true);
        }
        return subscriptionStateChangesObserver;
    }

    /* access modifiers changed from: private */
    public static OSEmailSubscriptionState getCurrentEmailSubscriptionState(Context context) {
        if (context == null) {
            return null;
        }
        if (currentEmailSubscriptionState == null) {
            currentEmailSubscriptionState = new OSEmailSubscriptionState(false);
            currentEmailSubscriptionState.observable.addObserverStrong(new OSEmailSubscriptionChangedInternalObserver());
        }
        return currentEmailSubscriptionState;
    }

    private static OSEmailSubscriptionState getLastEmailSubscriptionState(Context context) {
        if (context == null) {
            return null;
        }
        if (lastEmailSubscriptionState == null) {
            lastEmailSubscriptionState = new OSEmailSubscriptionState(true);
        }
        return lastEmailSubscriptionState;
    }

    static OSObservable<OSEmailSubscriptionObserver, OSEmailSubscriptionStateChanges> getEmailSubscriptionStateChangesObserver() {
        if (emailSubscriptionStateChangesObserver == null) {
            emailSubscriptionStateChangesObserver = new OSObservable<>("onOSEmailSubscriptionChanged", true);
        }
        return emailSubscriptionStateChangesObserver;
    }

    private static class IAPUpdateJob {
        boolean newAsExisting;
        OneSignalRestClient.ResponseHandler restResponseHandler;
        JSONArray toReport;

        IAPUpdateJob(JSONArray jSONArray) {
            this.toReport = jSONArray;
        }
    }

    public static Builder getCurrentOrNewInitBuilder() {
        if (mInitBuilder == null) {
            mInitBuilder = new Builder();
        }
        return mInitBuilder;
    }

    static void setAppContext(Context context) {
        boolean z = appContext == null;
        appContext = context.getApplicationContext();
        if (z) {
            OneSignalPrefs.startDelayedWrite();
        }
    }

    public static Builder startInit(Context context) {
        return new Builder(context);
    }

    /* access modifiers changed from: private */
    public static void init(Builder builder) {
        if (getCurrentOrNewInitBuilder().mDisplayOptionCarryOver) {
            builder.mDisplayOption = getCurrentOrNewInitBuilder().mDisplayOption;
        }
        mInitBuilder = builder;
        Context context = mInitBuilder.mContext;
        mInitBuilder.mContext = null;
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            String string = bundle.getString("onesignal_google_project_number");
            if (string != null && string.length() > 4) {
                string = string.substring(4);
            }
            setRequiresUserPrivacyConsent("ENABLE".equalsIgnoreCase(bundle.getString("com.onesignal.PrivacyConsent")));
            init(context, string, bundle.getString("onesignal_app_id"), mInitBuilder.mNotificationOpenedHandler, mInitBuilder.mNotificationReceivedHandler);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void init(Context context, String str, String str2) {
        init(context, str, str2, null, null);
    }

    public static void init(Context context, String str, String str2, NotificationOpenedHandler notificationOpenedHandler) {
        init(context, str, str2, notificationOpenedHandler, null);
    }

    public static void init(Context context, String str, String str2, NotificationOpenedHandler notificationOpenedHandler, NotificationReceivedHandler notificationReceivedHandler) {
        setAppContext(context);
        if (!requiresUserPrivacyConsent || userProvidedPrivacyConsent()) {
            mInitBuilder = getCurrentOrNewInitBuilder();
            mInitBuilder.mDisplayOptionCarryOver = false;
            mInitBuilder.mNotificationOpenedHandler = notificationOpenedHandler;
            mInitBuilder.mNotificationReceivedHandler = notificationReceivedHandler;
            if (!mGoogleProjectNumberIsRemote) {
                mGoogleProjectNumber = str;
            }
            osUtils = new OSUtils();
            deviceType = osUtils.getDeviceType();
            subscribableStatus = osUtils.initializationChecker(context, deviceType, str2);
            if (subscribableStatus != -999) {
                if (!initDone) {
                    boolean z = context instanceof Activity;
                    foreground = z;
                    appId = str2;
                    saveFilterOtherGCMReceivers(mInitBuilder.mFilterOtherGCMReceivers);
                    if (z) {
                        ActivityLifecycleHandler.curActivity = (Activity) context;
                        NotificationRestorer.asyncRestore(appContext);
                    } else {
                        ActivityLifecycleHandler.nextResumeIsFirstActivity = true;
                    }
                    lastTrackedFocusTime = SystemClock.elapsedRealtime();
                    OneSignalStateSynchronizer.initUserState();
                    ((Application) appContext).registerActivityLifecycleCallbacks(new ActivityLifecycleListener());
                    try {
                        Class.forName("com.amazon.device.iap.PurchasingListener");
                        trackAmazonPurchase = new TrackAmazonPurchase(appContext);
                    } catch (ClassNotFoundException unused) {
                    }
                    String savedAppId = getSavedAppId();
                    if (savedAppId == null) {
                        BadgeCountUpdater.updateCount(0, appContext);
                        SaveAppId(appId);
                    } else if (!savedAppId.equals(appId)) {
                        Log(LOG_LEVEL.DEBUG, "APP ID changed, clearing user id as it is no longer valid.");
                        SaveAppId(appId);
                        OneSignalStateSynchronizer.resetCurrentState();
                    }
                    OSPermissionChangedInternalObserver.handleInternalChanges(getCurrentPermissionState(appContext));
                    if (foreground || getUserId() == null) {
                        sendAsSession = isPastOnSessionTime();
                        setLastSessionTime(System.currentTimeMillis());
                        startRegistrationOrOnSession();
                    }
                    if (mInitBuilder.mNotificationOpenedHandler != null) {
                        fireCallbackForOpenedNotifications();
                    }
                    if (TrackGooglePurchase.CanTrack(appContext)) {
                        trackGooglePurchase = new TrackGooglePurchase(appContext);
                    }
                    if (TrackFirebaseAnalytics.CanTrack()) {
                        trackFirebaseAnalytics = new TrackFirebaseAnalytics(appContext);
                    }
                    PushRegistratorFCM.disableFirebaseInstanceIdService(appContext);
                    initDone = true;
                    startPendingTasks();
                } else if (mInitBuilder.mNotificationOpenedHandler != null) {
                    fireCallbackForOpenedNotifications();
                }
            }
        } else {
            Log(LOG_LEVEL.VERBOSE, "OneSignal SDK initialization delayed, user privacy consent is set to required for this application.");
            delayedInitParams = new DelayedConsentInitializationParameters(context, str, str2, notificationOpenedHandler, notificationReceivedHandler);
        }
    }

    /* access modifiers changed from: private */
    public static void onTaskRan(long j) {
        if (lastTaskId.get() == j) {
            Log(LOG_LEVEL.INFO, "Last Pending Task has ran, shutting down");
            pendingTaskExecutor.shutdown();
        }
    }

    private static class PendingTaskRunnable implements Runnable {
        private Runnable innerTask;
        /* access modifiers changed from: private */
        public long taskId;

        PendingTaskRunnable(Runnable runnable) {
            this.innerTask = runnable;
        }

        public void run() {
            this.innerTask.run();
            OneSignal.onTaskRan(this.taskId);
        }
    }

    private static void startPendingTasks() {
        if (!taskQueueWaitingForInit.isEmpty()) {
            pendingTaskExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
                public Thread newThread(@NonNull Runnable runnable) {
                    Thread thread = new Thread(runnable);
                    thread.setName("OS_PENDING_EXECUTOR_" + thread.getId());
                    return thread;
                }
            });
            while (!taskQueueWaitingForInit.isEmpty()) {
                pendingTaskExecutor.submit(taskQueueWaitingForInit.poll());
            }
        }
    }

    private static void addTaskToQueue(PendingTaskRunnable pendingTaskRunnable) {
        long unused = pendingTaskRunnable.taskId = lastTaskId.incrementAndGet();
        if (pendingTaskExecutor == null) {
            LOG_LEVEL log_level = LOG_LEVEL.INFO;
            Log(log_level, "Adding a task to the pending queue with ID: " + pendingTaskRunnable.taskId);
            taskQueueWaitingForInit.add(pendingTaskRunnable);
        } else if (!pendingTaskExecutor.isShutdown()) {
            LOG_LEVEL log_level2 = LOG_LEVEL.INFO;
            Log(log_level2, "Executor is still running, add to the executor with ID: " + pendingTaskRunnable.taskId);
            pendingTaskExecutor.submit(pendingTaskRunnable);
        }
    }

    private static boolean shouldRunTaskThroughQueue() {
        if (initDone && pendingTaskExecutor == null) {
            return false;
        }
        if (!initDone && pendingTaskExecutor == null) {
            return true;
        }
        if (pendingTaskExecutor == null || pendingTaskExecutor.isShutdown()) {
            return false;
        }
        return true;
    }

    private static void startRegistrationOrOnSession() {
        if (!waitingToPostStateSync) {
            boolean z = true;
            waitingToPostStateSync = true;
            registerForPushFired = false;
            if (sendAsSession) {
                locationFired = false;
            }
            startLocationUpdate();
            makeAndroidParamsRequest();
            if (!promptedLocation && !mInitBuilder.mPromptLocation) {
                z = false;
            }
            promptedLocation = z;
        }
    }

    private static void startLocationUpdate() {
        LocationGMS.getLocation(appContext, mInitBuilder.mPromptLocation && !promptedLocation, new LocationGMS.LocationHandler() {
            public LocationGMS.CALLBACK_TYPE getType() {
                return LocationGMS.CALLBACK_TYPE.STARTUP;
            }

            public void complete(LocationGMS.LocationPoint locationPoint) {
                LocationGMS.LocationPoint unused = OneSignal.lastLocationPoint = locationPoint;
                boolean unused2 = OneSignal.locationFired = true;
                OneSignal.registerUser();
            }
        });
    }

    private static PushRegistrator getPushRegistrator() {
        if (mPushRegistrator != null) {
            return mPushRegistrator;
        }
        if (deviceType == 2) {
            mPushRegistrator = new PushRegistratorADM();
        } else if (OSUtils.hasFCMLibrary()) {
            mPushRegistrator = new PushRegistratorFCM();
        } else {
            mPushRegistrator = new PushRegistratorGCM();
        }
        return mPushRegistrator;
    }

    /* access modifiers changed from: private */
    public static void registerForPushToken() {
        getPushRegistrator().registerForPush(appContext, mGoogleProjectNumber, new PushRegistrator.RegisteredHandler() {
            public void complete(String str, int i) {
                if (i < 1) {
                    if (OneSignalStateSynchronizer.getRegistrationId() == null && (OneSignal.subscribableStatus == 1 || OneSignal.pushStatusRuntimeError(OneSignal.subscribableStatus))) {
                        int unused = OneSignal.subscribableStatus = i;
                    }
                } else if (OneSignal.pushStatusRuntimeError(OneSignal.subscribableStatus)) {
                    int unused2 = OneSignal.subscribableStatus = i;
                }
                String unused3 = OneSignal.lastRegistrationId = str;
                boolean unused4 = OneSignal.registerForPushFired = true;
                OneSignal.getCurrentSubscriptionState(OneSignal.appContext).setPushToken(str);
                OneSignal.registerUser();
            }
        });
    }

    /* access modifiers changed from: private */
    public static void makeAndroidParamsRequest() {
        if (awlFired) {
            registerForPushToken();
            return;
        }
        AnonymousClass4 r0 = new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            int access$1300 = (OneSignal.androidParamsReties * AbstractSpiCall.DEFAULT_TIMEOUT) + NetworkConstants.UPLOAD_CONNECT_TIMEOUT;
                            if (access$1300 > 90000) {
                                access$1300 = 90000;
                            }
                            LOG_LEVEL log_level = LOG_LEVEL.INFO;
                            OneSignal.Log(log_level, "Failed to get Android parameters, trying again in " + (access$1300 / 1000) + " seconds.");
                            Thread.sleep((long) access$1300);
                        } catch (Throwable unused) {
                        }
                        OneSignal.access$1308();
                        OneSignal.makeAndroidParamsRequest();
                    }
                }, "OS_PARAMS_REQUEST").start();
            }

            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    if (jSONObject.has("android_sender_id")) {
                        boolean unused = OneSignal.mGoogleProjectNumberIsRemote = true;
                        String unused2 = OneSignal.mGoogleProjectNumber = jSONObject.getString("android_sender_id");
                    }
                    OneSignal.mEnterp = jSONObject.optBoolean("enterp", false);
                    boolean unused3 = OneSignal.useEmailAuth = jSONObject.optBoolean("use_email_auth", false);
                    JSONObject unused4 = OneSignal.awl = jSONObject.getJSONObject("awl_list");
                    OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, "GT_FIREBASE_TRACKING_ENABLED", jSONObject.optBoolean("fba", false));
                    NotificationChannelManager.processChannelList(OneSignal.appContext, jSONObject);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                boolean unused5 = OneSignal.awlFired = true;
                OneSignal.registerForPushToken();
            }
        };
        String str = "apps/" + appId + "/android_params.js";
        String userId2 = getUserId();
        if (userId2 != null) {
            str = str + "?player_id=" + userId2;
        }
        Log(LOG_LEVEL.DEBUG, "Starting request to get Android parameters.");
        OneSignalRestClient.get(str, r0);
    }

    private static void fireCallbackForOpenedNotifications() {
        for (JSONArray runNotificationOpenedCallback : unprocessedOpenedNotifis) {
            runNotificationOpenedCallback(runNotificationOpenedCallback, true, false);
        }
        unprocessedOpenedNotifis.clear();
    }

    public static void onesignalLog(LOG_LEVEL log_level, String str) {
        Log(log_level, str);
    }

    public static boolean userProvidedPrivacyConsent() {
        return getSavedUserConsentStatus();
    }

    public static void provideUserConsent(boolean z) {
        boolean userProvidedPrivacyConsent = userProvidedPrivacyConsent();
        saveUserConsentStatus(z);
        if (!userProvidedPrivacyConsent && z && delayedInitParams != null) {
            init(delayedInitParams.context, delayedInitParams.googleProjectNumber, delayedInitParams.appId, delayedInitParams.openedHandler, delayedInitParams.receivedHandler);
            delayedInitParams = null;
        }
    }

    public static void setRequiresUserPrivacyConsent(boolean z) {
        if (!requiresUserPrivacyConsent || z) {
            requiresUserPrivacyConsent = z;
        } else {
            Log(LOG_LEVEL.ERROR, "Cannot change requiresUserPrivacyConsent() from TRUE to FALSE");
        }
    }

    public static boolean requiresUserPrivacyConsent() {
        return requiresUserPrivacyConsent && !userProvidedPrivacyConsent();
    }

    static boolean shouldLogUserPrivacyConsentErrorMessageForMethodName(String str) {
        if (!requiresUserPrivacyConsent || userProvidedPrivacyConsent()) {
            return false;
        }
        if (str == null) {
            return true;
        }
        LOG_LEVEL log_level = LOG_LEVEL.WARN;
        Log(log_level, "Method " + str + " was called before the user provided privacy consent. Your application is set to require the user's privacy consent before the OneSignal SDK can be initialized. Please ensure the user has provided consent before calling this method. You can check the latest OneSignal consent status by calling OneSignal.userProvidedPrivacyConsent()");
        return true;
    }

    public static void setLogLevel(LOG_LEVEL log_level, LOG_LEVEL log_level2) {
        logCatLevel = log_level;
        visualLogLevel = log_level2;
    }

    public static void setLogLevel(int i, int i2) {
        setLogLevel(getLogLevel(i), getLogLevel(i2));
    }

    private static LOG_LEVEL getLogLevel(int i) {
        switch (i) {
            case 0:
                return LOG_LEVEL.NONE;
            case 1:
                return LOG_LEVEL.FATAL;
            case 2:
                return LOG_LEVEL.ERROR;
            case 3:
                return LOG_LEVEL.WARN;
            case 4:
                return LOG_LEVEL.INFO;
            case 5:
                return LOG_LEVEL.DEBUG;
            case 6:
                return LOG_LEVEL.VERBOSE;
            default:
                if (i < 0) {
                    return LOG_LEVEL.NONE;
                }
                return LOG_LEVEL.VERBOSE;
        }
    }

    private static boolean atLogLevel(LOG_LEVEL log_level) {
        return log_level.compareTo(visualLogLevel) < 1 || log_level.compareTo(logCatLevel) < 1;
    }

    static void Log(LOG_LEVEL log_level, String str) {
        Log(log_level, str, null);
    }

    static void Log(final LOG_LEVEL log_level, String str, Throwable th) {
        if (log_level.compareTo((Enum) logCatLevel) < 1) {
            if (log_level == LOG_LEVEL.VERBOSE) {
                Log.v("OneSignal", str, th);
            } else if (log_level == LOG_LEVEL.DEBUG) {
                Log.d("OneSignal", str, th);
            } else if (log_level == LOG_LEVEL.INFO) {
                Log.i("OneSignal", str, th);
            } else if (log_level == LOG_LEVEL.WARN) {
                Log.w("OneSignal", str, th);
            } else if (log_level == LOG_LEVEL.ERROR || log_level == LOG_LEVEL.FATAL) {
                Log.e("OneSignal", str, th);
            }
        }
        if (log_level.compareTo((Enum) visualLogLevel) < 1 && ActivityLifecycleHandler.curActivity != null) {
            try {
                final String str2 = str + "\n";
                if (th != null) {
                    StringWriter stringWriter = new StringWriter();
                    th.printStackTrace(new PrintWriter(stringWriter));
                    str2 = (str2 + th.getMessage()) + stringWriter.toString();
                }
                OSUtils.runOnMainUIThread(new Runnable() {
                    public void run() {
                        if (ActivityLifecycleHandler.curActivity != null) {
                            new AlertDialog.Builder(ActivityLifecycleHandler.curActivity).setTitle(log_level.toString()).setMessage(str2).show();
                        }
                    }
                });
            } catch (Throwable th2) {
                Log.e("OneSignal", "Error showing logging message.", th2);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void logHttpError(String str, int i, Throwable th, String str2) {
        String str3 = "";
        if (str2 != null && atLogLevel(LOG_LEVEL.INFO)) {
            str3 = "\n" + str2 + "\n";
        }
        Log(LOG_LEVEL.WARN, "HTTP code: " + i + " " + str + str3, th);
    }

    @WorkerThread
    static boolean onAppLostFocus() {
        foreground = false;
        LocationGMS.onFocusChange();
        if (!initDone) {
            return false;
        }
        if (trackAmazonPurchase != null) {
            trackAmazonPurchase.checkListener();
        }
        if (lastTrackedFocusTime == -1) {
            return false;
        }
        double elapsedRealtime = (double) (SystemClock.elapsedRealtime() - lastTrackedFocusTime);
        Double.isNaN(elapsedRealtime);
        long j = (long) ((elapsedRealtime / 1000.0d) + 0.5d);
        lastTrackedFocusTime = SystemClock.elapsedRealtime();
        if (j < 0 || j > 86400) {
            return false;
        }
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "Android Context not found, please call OneSignal.init when your app starts.");
            return false;
        }
        boolean scheduleSyncService = scheduleSyncService();
        setLastSessionTime(System.currentTimeMillis());
        long GetUnsentActiveTime = GetUnsentActiveTime() + j;
        SaveUnsentActiveTime(GetUnsentActiveTime);
        if (GetUnsentActiveTime >= MIN_ON_FOCUS_TIME && getUserId() != null) {
            if (!scheduleSyncService) {
                OneSignalSyncServiceUtils.scheduleSyncTask(appContext);
            }
            OneSignalSyncServiceUtils.syncOnFocusTime();
            return false;
        } else if (GetUnsentActiveTime >= MIN_ON_FOCUS_TIME) {
            return true;
        } else {
            return false;
        }
    }

    static boolean scheduleSyncService() {
        boolean persist = OneSignalStateSynchronizer.persist();
        if (persist) {
            OneSignalSyncServiceUtils.scheduleSyncTask(appContext);
        }
        return LocationGMS.scheduleUpdate(appContext) || persist;
    }

    static void sendOnFocus(long j, boolean z) {
        try {
            JSONObject put = new JSONObject().put("app_id", appId).put("type", 1).put("state", "ping").put("active_time", j);
            addNetType(put);
            sendOnFocusToPlayer(getUserId(), put, z);
            String emailId2 = getEmailId();
            if (emailId2 != null) {
                sendOnFocusToPlayer(emailId2, put, z);
            }
        } catch (Throwable th) {
            Log(LOG_LEVEL.ERROR, "Generating on_focus:JSON Failed.", th);
        }
    }

    private static void sendOnFocusToPlayer(String str, JSONObject jSONObject, boolean z) {
        String str2 = "players/" + str + "/on_focus";
        AnonymousClass6 r0 = new OneSignalRestClient.ResponseHandler() {
            /* access modifiers changed from: package-private */
            public void onFailure(int i, String str, Throwable th) {
                OneSignal.logHttpError("sending on_focus Failed", i, th, str);
            }

            /* access modifiers changed from: package-private */
            public void onSuccess(String str) {
                OneSignal.SaveUnsentActiveTime(0);
            }
        };
        if (z) {
            OneSignalRestClient.postSync(str2, jSONObject, r0);
        } else {
            OneSignalRestClient.post(str2, jSONObject, r0);
        }
    }

    static void onAppFocus() {
        foreground = true;
        LocationGMS.onFocusChange();
        lastTrackedFocusTime = SystemClock.elapsedRealtime();
        sendAsSession = isPastOnSessionTime();
        setLastSessionTime(System.currentTimeMillis());
        startRegistrationOrOnSession();
        if (trackGooglePurchase != null) {
            trackGooglePurchase.trackIAP();
        }
        NotificationRestorer.asyncRestore(appContext);
        getCurrentPermissionState(appContext).refreshAsTo();
        if (trackFirebaseAnalytics != null && getFirebaseAnalyticsEnabled(appContext)) {
            trackFirebaseAnalytics.trackInfluenceOpenEvent();
        }
        OneSignalSyncServiceUtils.cancelSyncTask(appContext);
    }

    static boolean isForeground() {
        return foreground;
    }

    private static void addNetType(JSONObject jSONObject) {
        try {
            jSONObject.put("net_type", osUtils.getNetType());
        } catch (Throwable unused) {
        }
    }

    private static int getTimeZoneOffset() {
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        int rawOffset = timeZone.getRawOffset();
        if (timeZone.inDaylightTime(new Date())) {
            rawOffset += timeZone.getDSTSavings();
        }
        return rawOffset / 1000;
    }

    /* access modifiers changed from: private */
    public static void registerUser() {
        LOG_LEVEL log_level = LOG_LEVEL.DEBUG;
        Log(log_level, "registerUser: registerForPushFired:" + registerForPushFired + ", locationFired: " + locationFired + ", awlFired: " + awlFired);
        if (registerForPushFired && locationFired && awlFired) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        OneSignal.registerUserTask();
                        OneSignalChromeTab.setup(OneSignal.appContext, OneSignal.appId, OneSignal.userId, AdvertisingIdProviderGPS.getLastValue());
                    } catch (JSONException e) {
                        OneSignal.Log(LOG_LEVEL.FATAL, "FATAL Error registering device!", e);
                    }
                }
            }, "OS_REG_USER").start();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0066 */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x007c A[Catch:{ Throwable -> 0x00a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0100  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void registerUserTask() throws org.json.JSONException {
        /*
            android.content.Context r0 = com.onesignal.OneSignal.appContext
            java.lang.String r0 = r0.getPackageName()
            android.content.Context r1 = com.onesignal.OneSignal.appContext
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>()
            java.lang.String r3 = "app_id"
            java.lang.String r4 = com.onesignal.OneSignal.appId
            r2.put(r3, r4)
            com.onesignal.AdvertisingIdentifierProvider r3 = com.onesignal.OneSignal.mainAdIdProvider
            android.content.Context r4 = com.onesignal.OneSignal.appContext
            java.lang.String r3 = r3.getIdentifier(r4)
            if (r3 == 0) goto L_0x0027
            java.lang.String r4 = "ad_id"
            r2.put(r4, r3)
        L_0x0027:
            java.lang.String r3 = "device_os"
            java.lang.String r4 = android.os.Build.VERSION.RELEASE
            r2.put(r3, r4)
            java.lang.String r3 = "timezone"
            int r4 = getTimeZoneOffset()
            r2.put(r3, r4)
            java.lang.String r3 = "language"
            java.lang.String r4 = com.onesignal.OSUtils.getCorrectedLanguage()
            r2.put(r3, r4)
            java.lang.String r3 = "sdk"
            java.lang.String r4 = "031003"
            r2.put(r3, r4)
            java.lang.String r3 = "sdk_type"
            java.lang.String r4 = com.onesignal.OneSignal.sdkType
            r2.put(r3, r4)
            java.lang.String r3 = "android_package"
            r2.put(r3, r0)
            java.lang.String r3 = "device_model"
            java.lang.String r4 = android.os.Build.MODEL
            r2.put(r3, r4)
            r3 = 0
            java.lang.String r4 = "game_version"
            android.content.pm.PackageInfo r0 = r1.getPackageInfo(r0, r3)     // Catch:{ NameNotFoundException -> 0x0066 }
            int r0 = r0.versionCode     // Catch:{ NameNotFoundException -> 0x0066 }
            r2.put(r4, r0)     // Catch:{ NameNotFoundException -> 0x0066 }
        L_0x0066:
            java.util.List r0 = r1.getInstalledPackages(r3)     // Catch:{ Throwable -> 0x00a7 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Throwable -> 0x00a7 }
            r1.<init>()     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r4 = "SHA-256"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r4)     // Catch:{ Throwable -> 0x00a7 }
            r5 = 0
        L_0x0076:
            int r6 = r0.size()     // Catch:{ Throwable -> 0x00a7 }
            if (r5 >= r6) goto L_0x00a2
            java.lang.Object r6 = r0.get(r5)     // Catch:{ Throwable -> 0x00a7 }
            android.content.pm.PackageInfo r6 = (android.content.pm.PackageInfo) r6     // Catch:{ Throwable -> 0x00a7 }
            java.lang.String r6 = r6.packageName     // Catch:{ Throwable -> 0x00a7 }
            byte[] r6 = r6.getBytes()     // Catch:{ Throwable -> 0x00a7 }
            r4.update(r6)     // Catch:{ Throwable -> 0x00a7 }
            byte[] r6 = r4.digest()     // Catch:{ Throwable -> 0x00a7 }
            r7 = 2
            java.lang.String r6 = android.util.Base64.encodeToString(r6, r7)     // Catch:{ Throwable -> 0x00a7 }
            org.json.JSONObject r7 = com.onesignal.OneSignal.awl     // Catch:{ Throwable -> 0x00a7 }
            boolean r7 = r7.has(r6)     // Catch:{ Throwable -> 0x00a7 }
            if (r7 == 0) goto L_0x009f
            r1.put(r6)     // Catch:{ Throwable -> 0x00a7 }
        L_0x009f:
            int r5 = r5 + 1
            goto L_0x0076
        L_0x00a2:
            java.lang.String r0 = "pkgs"
            r2.put(r0, r1)     // Catch:{ Throwable -> 0x00a7 }
        L_0x00a7:
            java.lang.String r0 = "net_type"
            com.onesignal.OSUtils r1 = com.onesignal.OneSignal.osUtils
            java.lang.Integer r1 = r1.getNetType()
            r2.put(r0, r1)
            java.lang.String r0 = "carrier"
            com.onesignal.OSUtils r1 = com.onesignal.OneSignal.osUtils
            java.lang.String r1 = r1.getCarrierName()
            r2.put(r0, r1)
            java.lang.String r0 = "rooted"
            boolean r1 = com.onesignal.RootToolsInternalMethods.isRooted()
            r2.put(r0, r1)
            com.onesignal.OneSignalStateSynchronizer.updateDeviceInfo(r2)
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.lang.String r1 = "identifier"
            java.lang.String r2 = com.onesignal.OneSignal.lastRegistrationId
            r0.put(r1, r2)
            java.lang.String r1 = "subscribableStatus"
            int r2 = com.onesignal.OneSignal.subscribableStatus
            r0.put(r1, r2)
            java.lang.String r1 = "androidPermission"
            boolean r2 = areNotificationsEnabledForSubscribedState()
            r0.put(r1, r2)
            java.lang.String r1 = "device_type"
            int r2 = com.onesignal.OneSignal.deviceType
            r0.put(r1, r2)
            com.onesignal.OneSignalStateSynchronizer.updatePushState(r0)
            boolean r0 = com.onesignal.OneSignal.shareLocation
            if (r0 == 0) goto L_0x00fc
            com.onesignal.LocationGMS$LocationPoint r0 = com.onesignal.OneSignal.lastLocationPoint
            if (r0 == 0) goto L_0x00fc
            com.onesignal.LocationGMS$LocationPoint r0 = com.onesignal.OneSignal.lastLocationPoint
            com.onesignal.OneSignalStateSynchronizer.updateLocation(r0)
        L_0x00fc:
            boolean r0 = com.onesignal.OneSignal.sendAsSession
            if (r0 == 0) goto L_0x0103
            com.onesignal.OneSignalStateSynchronizer.setSyncAsNewSession()
        L_0x0103:
            com.onesignal.OneSignal.waitingToPostStateSync = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.registerUserTask():void");
    }

    @Deprecated
    public static void syncHashedEmail(final String str) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("SyncHashedEmail()") && OSUtils.isValidEmail(str)) {
            AnonymousClass8 r0 = new Runnable() {
                public void run() {
                    OneSignalStateSynchronizer.syncHashedEmail(str.trim().toLowerCase());
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "You should initialize OneSignal before calling syncHashedEmail! Moving this operation to a pending task queue.");
                addTaskToQueue(new PendingTaskRunnable(r0));
                return;
            }
            r0.run();
        }
    }

    public static void setEmail(@NonNull String str, EmailUpdateHandler emailUpdateHandler2) {
        setEmail(str, null, emailUpdateHandler2);
    }

    public static void setEmail(@NonNull String str) {
        setEmail(str, null, null);
    }

    public static void setEmail(@NonNull String str, @Nullable String str2) {
        setEmail(str, str2, null);
    }

    public static void setEmail(@NonNull final String str, @Nullable final String str2, @Nullable EmailUpdateHandler emailUpdateHandler2) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("setEmail()")) {
            if (!OSUtils.isValidEmail(str)) {
                if (emailUpdateHandler2 != null) {
                    emailUpdateHandler2.onFailure(new EmailUpdateError(EmailErrorType.VALIDATION, "Email is invalid"));
                }
                Log(LOG_LEVEL.ERROR, "Email is invalid");
            } else if (!useEmailAuth || str2 != null) {
                emailUpdateHandler = emailUpdateHandler2;
                AnonymousClass9 r3 = new Runnable() {
                    public void run() {
                        String trim = str.trim();
                        String str = str2;
                        if (str != null) {
                            str.toLowerCase();
                        }
                        OneSignal.getCurrentEmailSubscriptionState(OneSignal.appContext).setEmailAddress(trim);
                        OneSignalStateSynchronizer.setEmail(trim.toLowerCase(), str);
                    }
                };
                if (appContext == null || shouldRunTaskThroughQueue()) {
                    Log(LOG_LEVEL.ERROR, "You should initialize OneSignal before calling setEmail! Moving this operation to a pending task queue.");
                    addTaskToQueue(new PendingTaskRunnable(r3));
                    return;
                }
                r3.run();
            } else {
                if (emailUpdateHandler2 != null) {
                    emailUpdateHandler2.onFailure(new EmailUpdateError(EmailErrorType.REQUIRES_EMAIL_AUTH, "Email authentication (auth token) is set to REQUIRED for this application. Please provide an auth token from your backend server or change the setting in the OneSignal dashboard."));
                }
                Log(LOG_LEVEL.ERROR, "Email authentication (auth token) is set to REQUIRED for this application. Please provide an auth token from your backend server or change the setting in the OneSignal dashboard.");
            }
        }
    }

    public static void logoutEmail() {
        logoutEmail(null);
    }

    public static void logoutEmail(@Nullable EmailUpdateHandler emailUpdateHandler2) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("logoutEmail()")) {
            if (getEmailId() == null) {
                if (emailUpdateHandler2 != null) {
                    emailUpdateHandler2.onFailure(new EmailUpdateError(EmailErrorType.INVALID_OPERATION, "logoutEmail not valid as email was not set or already logged out!"));
                }
                Log(LOG_LEVEL.ERROR, "logoutEmail not valid as email was not set or already logged out!");
                return;
            }
            emailLogoutHandler = emailUpdateHandler2;
            AnonymousClass10 r3 = new Runnable() {
                public void run() {
                    OneSignalStateSynchronizer.logoutEmail();
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "You should initialize OneSignal before calling logoutEmail! Moving this operation to a pending task queue.");
                addTaskToQueue(new PendingTaskRunnable(r3));
                return;
            }
            r3.run();
        }
    }

    public static void sendTag(String str, String str2) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("sendTag()")) {
            try {
                sendTags(new JSONObject().put(str, str2));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void sendTags(String str) {
        try {
            sendTags(new JSONObject(str));
        } catch (JSONException e) {
            Log(LOG_LEVEL.ERROR, "Generating JSONObject for sendTags failed!", e);
        }
    }

    public static void sendTags(JSONObject jSONObject) {
        sendTags(jSONObject, null);
    }

    public static void sendTags(final JSONObject jSONObject, final ChangeTagsUpdateHandler changeTagsUpdateHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("sendTags()")) {
            AnonymousClass11 r0 = new Runnable() {
                public void run() {
                    if (jSONObject != null) {
                        JSONObject jSONObject = OneSignalStateSynchronizer.getTags(false).result;
                        JSONObject jSONObject2 = new JSONObject();
                        Iterator<String> keys = jSONObject.keys();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            try {
                                Object opt = jSONObject.opt(next);
                                if (!(opt instanceof JSONArray)) {
                                    if (!(opt instanceof JSONObject)) {
                                        if (!jSONObject.isNull(next)) {
                                            if (!"".equals(opt)) {
                                                jSONObject2.put(next, opt.toString());
                                            }
                                        }
                                        if (jSONObject != null && jSONObject.has(next)) {
                                            jSONObject2.put(next, "");
                                        }
                                    }
                                }
                                LOG_LEVEL log_level = LOG_LEVEL.ERROR;
                                OneSignal.Log(log_level, "Omitting key '" + next + "'! sendTags DO NOT supported nested values!");
                            } catch (Throwable unused) {
                            }
                        }
                        if (!jSONObject2.toString().equals("{}")) {
                            OneSignalStateSynchronizer.sendTags(jSONObject2, changeTagsUpdateHandler);
                        } else if (changeTagsUpdateHandler != null) {
                            changeTagsUpdateHandler.onSuccess(jSONObject);
                        }
                    } else if (changeTagsUpdateHandler != null) {
                        changeTagsUpdateHandler.onFailure(new SendTagsError(-1, "Attempted to send null tags"));
                    }
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "You must initialize OneSignal before modifying tags!Moving this operation to a pending task queue.");
                if (changeTagsUpdateHandler != null) {
                    changeTagsUpdateHandler.onFailure(new SendTagsError(-1, "You must initialize OneSignal before modifying tags!Moving this operation to a pending task queue."));
                }
                addTaskToQueue(new PendingTaskRunnable(r0));
                return;
            }
            r0.run();
        }
    }

    public static void postNotification(String str, PostNotificationResponseHandler postNotificationResponseHandler) {
        try {
            postNotification(new JSONObject(str), postNotificationResponseHandler);
        } catch (JSONException unused) {
            LOG_LEVEL log_level = LOG_LEVEL.ERROR;
            Log(log_level, "Invalid postNotification JSON format: " + str);
        }
    }

    public static void postNotification(JSONObject jSONObject, final PostNotificationResponseHandler postNotificationResponseHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("postNotification()")) {
            try {
                if (!jSONObject.has("app_id")) {
                    jSONObject.put("app_id", getSavedAppId());
                }
                OneSignalRestClient.post("notifications/", jSONObject, new OneSignalRestClient.ResponseHandler() {
                    public void onSuccess(String str) {
                        LOG_LEVEL log_level = LOG_LEVEL.DEBUG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("HTTP create notification success: ");
                        sb.append(str != null ? str : "null");
                        OneSignal.Log(log_level, sb.toString());
                        if (postNotificationResponseHandler != null) {
                            try {
                                JSONObject jSONObject = new JSONObject(str);
                                if (jSONObject.has(ReportDBAdapter.ReportColumns.COLUMN_ERRORS)) {
                                    postNotificationResponseHandler.onFailure(jSONObject);
                                } else {
                                    postNotificationResponseHandler.onSuccess(new JSONObject(str));
                                }
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    }

                    /* access modifiers changed from: package-private */
                    /* JADX WARNING: Can't wrap try/catch for region: R(6:(1:3)|4|5|6|7|13) */
                    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
                        return;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
                        return;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
                        r2 = move-exception;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0026, code lost:
                        r2.printStackTrace();
                     */
                    /* JADX WARNING: Failed to process nested try/catch */
                    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0018 */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void onFailure(int r2, java.lang.String r3, java.lang.Throwable r4) {
                        /*
                            r1 = this;
                            java.lang.String r0 = "create notification failed"
                            com.onesignal.OneSignal.logHttpError(r0, r2, r4, r3)
                            com.onesignal.OneSignal$PostNotificationResponseHandler r4 = r3
                            if (r4 == 0) goto L_0x0029
                            if (r2 != 0) goto L_0x000d
                            java.lang.String r3 = "{\"error\": \"HTTP no response error\"}"
                        L_0x000d:
                            com.onesignal.OneSignal$PostNotificationResponseHandler r2 = r3     // Catch:{ Throwable -> 0x0018 }
                            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0018 }
                            r4.<init>(r3)     // Catch:{ Throwable -> 0x0018 }
                            r2.onFailure(r4)     // Catch:{ Throwable -> 0x0018 }
                            goto L_0x0029
                        L_0x0018:
                            com.onesignal.OneSignal$PostNotificationResponseHandler r2 = r3     // Catch:{ JSONException -> 0x0025 }
                            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0025 }
                            java.lang.String r4 = "{\"error\": \"Unknown response!\"}"
                            r3.<init>(r4)     // Catch:{ JSONException -> 0x0025 }
                            r2.onFailure(r3)     // Catch:{ JSONException -> 0x0025 }
                            goto L_0x0029
                        L_0x0025:
                            r2 = move-exception
                            r2.printStackTrace()
                        L_0x0029:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.AnonymousClass12.onFailure(int, java.lang.String, java.lang.Throwable):void");
                    }
                });
            } catch (JSONException e) {
                Log(LOG_LEVEL.ERROR, "HTTP create notification json exception!", e);
                if (postNotificationResponseHandler != null) {
                    try {
                        postNotificationResponseHandler.onFailure(new JSONObject("{'error': 'HTTP create notification json exception!'}"));
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    public static void getTags(final GetTagsHandler getTagsHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("getTags()")) {
            pendingGetTagsHandler = getTagsHandler;
            AnonymousClass13 r0 = new Runnable() {
                public void run() {
                    if (getTagsHandler == null) {
                        OneSignal.Log(LOG_LEVEL.ERROR, "getTagsHandler is null!");
                    } else if (OneSignal.getUserId() != null) {
                        OneSignal.internalFireGetTagsCallback(OneSignal.pendingGetTagsHandler);
                    }
                }
            };
            if (appContext == null) {
                Log(LOG_LEVEL.ERROR, "You must initialize OneSignal before getting tags! Moving this tag operation to a pending queue.");
                taskQueueWaitingForInit.add(r0);
                return;
            }
            r0.run();
        }
    }

    /* access modifiers changed from: private */
    public static void internalFireGetTagsCallback(final GetTagsHandler getTagsHandler) {
        if (getTagsHandler != null) {
            new Thread(new Runnable() {
                public void run() {
                    UserStateSynchronizer.GetTagsResult tags = OneSignalStateSynchronizer.getTags(!OneSignal.getTagsCall);
                    if (tags.serverSuccess) {
                        boolean unused = OneSignal.getTagsCall = true;
                    }
                    if (tags.result == null || tags.toString().equals("{}")) {
                        getTagsHandler.tagsAvailable(null);
                    } else {
                        getTagsHandler.tagsAvailable(tags.result);
                    }
                }
            }, "OS_GETTAGS_CALLBACK").start();
        }
    }

    public static void deleteTag(String str) {
        deleteTag(str, null);
    }

    public static void deleteTag(String str, ChangeTagsUpdateHandler changeTagsUpdateHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("deleteTag()")) {
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(str);
            deleteTags(arrayList, changeTagsUpdateHandler);
        }
    }

    public static void deleteTags(Collection<String> collection) {
        deleteTags(collection, (ChangeTagsUpdateHandler) null);
    }

    public static void deleteTags(Collection<String> collection, ChangeTagsUpdateHandler changeTagsUpdateHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("deleteTags()")) {
            try {
                JSONObject jSONObject = new JSONObject();
                for (String put : collection) {
                    jSONObject.put(put, "");
                }
                sendTags(jSONObject, changeTagsUpdateHandler);
            } catch (Throwable th) {
                Log(LOG_LEVEL.ERROR, "Failed to generate JSON for deleteTags.", th);
            }
        }
    }

    public static void deleteTags(String str) {
        deleteTags(str, (ChangeTagsUpdateHandler) null);
    }

    public static void deleteTags(String str, ChangeTagsUpdateHandler changeTagsUpdateHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("deleteTags()")) {
            try {
                JSONObject jSONObject = new JSONObject();
                JSONArray jSONArray = new JSONArray(str);
                for (int i = 0; i < jSONArray.length(); i++) {
                    jSONObject.put(jSONArray.getString(i), "");
                }
                sendTags(jSONObject, changeTagsUpdateHandler);
            } catch (Throwable th) {
                Log(LOG_LEVEL.ERROR, "Failed to generate JSON for deleteTags.", th);
            }
        }
    }

    public static void idsAvailable(IdsAvailableHandler idsAvailableHandler2) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("idsAvailable()")) {
            idsAvailableHandler = idsAvailableHandler2;
            AnonymousClass15 r2 = new Runnable() {
                public void run() {
                    if (OneSignal.getUserId() != null) {
                        OSUtils.runOnMainUIThread(new Runnable() {
                            public void run() {
                                OneSignal.internalFireIdsAvailableCallback();
                            }
                        });
                    }
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "You must initialize OneSignal before getting tags! Moving this tag operation to a pending queue.");
                addTaskToQueue(new PendingTaskRunnable(r2));
                return;
            }
            r2.run();
        }
    }

    static void fireIdsAvailableCallback() {
        if (idsAvailableHandler != null) {
            OSUtils.runOnMainUIThread(new Runnable() {
                public void run() {
                    OneSignal.internalFireIdsAvailableCallback();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0027, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void internalFireIdsAvailableCallback() {
        /*
            java.lang.Class<com.onesignal.OneSignal> r0 = com.onesignal.OneSignal.class
            monitor-enter(r0)
            com.onesignal.OneSignal$IdsAvailableHandler r1 = com.onesignal.OneSignal.idsAvailableHandler     // Catch:{ all -> 0x0028 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r0)
            return
        L_0x0009:
            java.lang.String r1 = com.onesignal.OneSignalStateSynchronizer.getRegistrationId()     // Catch:{ all -> 0x0028 }
            boolean r2 = com.onesignal.OneSignalStateSynchronizer.getSubscribed()     // Catch:{ all -> 0x0028 }
            r3 = 0
            if (r2 != 0) goto L_0x0015
            r1 = r3
        L_0x0015:
            java.lang.String r2 = getUserId()     // Catch:{ all -> 0x0028 }
            if (r2 != 0) goto L_0x001d
            monitor-exit(r0)
            return
        L_0x001d:
            com.onesignal.OneSignal$IdsAvailableHandler r4 = com.onesignal.OneSignal.idsAvailableHandler     // Catch:{ all -> 0x0028 }
            r4.idsAvailable(r2, r1)     // Catch:{ all -> 0x0028 }
            if (r1 == 0) goto L_0x0026
            com.onesignal.OneSignal.idsAvailableHandler = r3     // Catch:{ all -> 0x0028 }
        L_0x0026:
            monitor-exit(r0)
            return
        L_0x0028:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.internalFireIdsAvailableCallback():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    static void sendPurchases(JSONArray jSONArray, boolean z, OneSignalRestClient.ResponseHandler responseHandler) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("sendPurchases()")) {
            if (getUserId() == null) {
                iapUpdateJob = new IAPUpdateJob(jSONArray);
                iapUpdateJob.newAsExisting = z;
                iapUpdateJob.restResponseHandler = responseHandler;
                return;
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("app_id", appId);
                if (z) {
                    jSONObject.put("existing", true);
                }
                jSONObject.put("purchases", jSONArray);
                OneSignalRestClient.post("players/" + getUserId() + "/on_purchase", jSONObject, responseHandler);
                if (getEmailId() != null) {
                    OneSignalRestClient.post("players/" + getEmailId() + "/on_purchase", jSONObject, null);
                }
            } catch (Throwable th) {
                Log(LOG_LEVEL.ERROR, "Failed to generate JSON for sendPurchases.", th);
            }
        }
    }

    private static boolean openURLFromNotification(Context context, JSONArray jSONArray) {
        if (shouldLogUserPrivacyConsentErrorMessageForMethodName(null)) {
            return false;
        }
        int length = jSONArray.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (jSONObject.has("custom")) {
                    JSONObject jSONObject2 = new JSONObject(jSONObject.optString("custom"));
                    if (jSONObject2.has(AnalyticsEventKey.URL)) {
                        String optString = jSONObject2.optString(AnalyticsEventKey.URL, null);
                        if (!optString.contains("://")) {
                            optString = "http://" + optString;
                        }
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(optString.trim()));
                        intent.addFlags(1476919296);
                        context.startActivity(intent);
                        z = true;
                    }
                }
            } catch (Throwable th) {
                Log(LOG_LEVEL.ERROR, "Error parsing JSON item " + i + Constants.URL_PATH_DELIMITER + length + " for launching a web URL.", th);
            }
        }
        return z;
    }

    private static void runNotificationOpenedCallback(JSONArray jSONArray, boolean z, boolean z2) {
        if (mInitBuilder == null || mInitBuilder.mNotificationOpenedHandler == null) {
            unprocessedOpenedNotifis.add(jSONArray);
        } else {
            fireNotificationOpenedHandler(generateOsNotificationOpenResult(jSONArray, z, z2));
        }
    }

    @NonNull
    private static OSNotificationOpenResult generateOsNotificationOpenResult(JSONArray jSONArray, boolean z, boolean z2) {
        int length = jSONArray.length();
        OSNotificationOpenResult oSNotificationOpenResult = new OSNotificationOpenResult();
        OSNotification oSNotification = new OSNotification();
        oSNotification.isAppInFocus = isAppActive();
        oSNotification.shown = z;
        oSNotification.androidNotificationId = jSONArray.optJSONObject(0).optInt("notificationId");
        String str = null;
        boolean z3 = true;
        for (int i = 0; i < length; i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                oSNotification.payload = NotificationBundleProcessor.OSNotificationPayloadFrom(jSONObject);
                if (str == null && jSONObject.has("actionSelected")) {
                    str = jSONObject.optString("actionSelected", null);
                }
                if (z3) {
                    z3 = false;
                } else {
                    if (oSNotification.groupedNotifications == null) {
                        oSNotification.groupedNotifications = new ArrayList();
                    }
                    oSNotification.groupedNotifications.add(oSNotification.payload);
                }
            } catch (Throwable th) {
                Log(LOG_LEVEL.ERROR, "Error parsing JSON item " + i + Constants.URL_PATH_DELIMITER + length + " for callback.", th);
            }
        }
        oSNotificationOpenResult.notification = oSNotification;
        oSNotificationOpenResult.action = new OSNotificationAction();
        oSNotificationOpenResult.action.actionID = str;
        oSNotificationOpenResult.action.type = str != null ? OSNotificationAction.ActionType.ActionTaken : OSNotificationAction.ActionType.Opened;
        if (z2) {
            oSNotificationOpenResult.notification.displayType = OSNotification.DisplayType.InAppAlert;
        } else {
            oSNotificationOpenResult.notification.displayType = OSNotification.DisplayType.Notification;
        }
        return oSNotificationOpenResult;
    }

    private static void fireNotificationOpenedHandler(final OSNotificationOpenResult oSNotificationOpenResult) {
        OSUtils.runOnMainUIThread(new Runnable() {
            public void run() {
                OneSignal.mInitBuilder.mNotificationOpenedHandler.notificationOpened(oSNotificationOpenResult);
            }
        });
    }

    static void handleNotificationReceived(JSONArray jSONArray, boolean z, boolean z2) {
        OSNotificationOpenResult generateOsNotificationOpenResult = generateOsNotificationOpenResult(jSONArray, z, z2);
        if (trackFirebaseAnalytics != null && getFirebaseAnalyticsEnabled(appContext)) {
            trackFirebaseAnalytics.trackReceivedEvent(generateOsNotificationOpenResult);
        }
        if (mInitBuilder != null && mInitBuilder.mNotificationReceivedHandler != null) {
            mInitBuilder.mNotificationReceivedHandler.notificationReceived(generateOsNotificationOpenResult.notification);
        }
    }

    public static void handleNotificationOpen(Context context, JSONArray jSONArray, boolean z) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName(null)) {
            notificationOpenedRESTCall(context, jSONArray);
            if (trackFirebaseAnalytics != null && getFirebaseAnalyticsEnabled(appContext)) {
                trackFirebaseAnalytics.trackOpenedEvent(generateOsNotificationOpenResult(jSONArray, true, z));
            }
            boolean z2 = false;
            boolean equals = "DISABLE".equals(OSUtils.getManifestMeta(context, "com.onesignal.NotificationOpened.DEFAULT"));
            if (!equals) {
                z2 = openURLFromNotification(context, jSONArray);
            }
            runNotificationOpenedCallback(jSONArray, true, z);
            if (!z && !z2 && !equals) {
                fireIntentFromNotificationOpen(context);
            }
        }
    }

    private static void fireIntentFromNotificationOpen(Context context) {
        Intent launchIntentForPackage;
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName(null) && (launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName())) != null) {
            launchIntentForPackage.setFlags(268566528);
            context.startActivity(launchIntentForPackage);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private static void notificationOpenedRESTCall(Context context, JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                String optString = new JSONObject(jSONArray.getJSONObject(i).optString("custom", null)).optString("i", null);
                if (!postedOpenedNotifIds.contains(optString)) {
                    postedOpenedNotifIds.add(optString);
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("app_id", getSavedAppId(context));
                    jSONObject.put("player_id", getSavedUserId(context));
                    jSONObject.put(OneSignalDbContract.NotificationTable.COLUMN_NAME_OPENED, true);
                    OneSignalRestClient.put("notifications/" + optString, jSONObject, new OneSignalRestClient.ResponseHandler() {
                        /* access modifiers changed from: package-private */
                        public void onFailure(int i, String str, Throwable th) {
                            OneSignal.logHttpError("sending Notification Opened Failed", i, th, str);
                        }
                    });
                }
            } catch (Throwable th) {
                Log(LOG_LEVEL.ERROR, "Failed to generate JSON to send notification opened.", th);
            }
        }
    }

    private static void SaveAppId(String str) {
        if (appContext != null) {
            OneSignalPrefs.saveString(OneSignalPrefs.PREFS_ONESIGNAL, "GT_APP_ID", str);
        }
    }

    static String getSavedAppId() {
        return getSavedAppId(appContext);
    }

    private static String getSavedAppId(Context context) {
        return context == null ? "" : OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, "GT_APP_ID", null);
    }

    static boolean getSavedUserConsentStatus() {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, "ONESIGNAL_USER_PROVIDED_CONSENT", false);
    }

    static void saveUserConsentStatus(boolean z) {
        OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, "ONESIGNAL_USER_PROVIDED_CONSENT", z);
    }

    private static String getSavedUserId(Context context) {
        return context == null ? "" : OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, "GT_PLAYER_ID", null);
    }

    static String getUserId() {
        if (userId == null && appContext != null) {
            userId = OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, "GT_PLAYER_ID", null);
        }
        return userId;
    }

    static void saveUserId(String str) {
        userId = str;
        if (appContext != null) {
            OneSignalPrefs.saveString(OneSignalPrefs.PREFS_ONESIGNAL, "GT_PLAYER_ID", userId);
        }
    }

    static String getEmailId() {
        if ("".equals(emailId)) {
            return null;
        }
        if (emailId == null && appContext != null) {
            emailId = OneSignalPrefs.getString(OneSignalPrefs.PREFS_ONESIGNAL, "OS_EMAIL_ID", null);
        }
        return emailId;
    }

    static void saveEmailId(String str) {
        emailId = str;
        if (appContext != null) {
            OneSignalPrefs.saveString(OneSignalPrefs.PREFS_ONESIGNAL, "OS_EMAIL_ID", "".equals(emailId) ? null : emailId);
        }
    }

    static boolean getFilterOtherGCMReceivers(Context context) {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, "OS_FILTER_OTHER_GCM_RECEIVERS", false);
    }

    static void saveFilterOtherGCMReceivers(boolean z) {
        if (appContext != null) {
            OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, "OS_FILTER_OTHER_GCM_RECEIVERS", z);
        }
    }

    static void updateUserIdDependents(String str) {
        saveUserId(str);
        fireIdsAvailableCallback();
        internalFireGetTagsCallback(pendingGetTagsHandler);
        getCurrentSubscriptionState(appContext).setUserId(str);
        if (iapUpdateJob != null) {
            sendPurchases(iapUpdateJob.toReport, iapUpdateJob.newAsExisting, iapUpdateJob.restResponseHandler);
            iapUpdateJob = null;
        }
        OneSignalStateSynchronizer.refreshEmailState();
        OneSignalChromeTab.setup(appContext, appId, str, AdvertisingIdProviderGPS.getLastValue());
    }

    static void updateEmailIdDependents(String str) {
        saveEmailId(str);
        getCurrentEmailSubscriptionState(appContext).setEmailUserId(str);
        try {
            OneSignalStateSynchronizer.updatePushState(new JSONObject().put("parent_player_id", str));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static boolean getFirebaseAnalyticsEnabled(Context context) {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, "GT_FIREBASE_TRACKING_ENABLED", false);
    }

    public static void enableVibrate(boolean z) {
        if (appContext != null) {
            OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, "GT_VIBRATE_ENABLED", z);
        }
    }

    static boolean getVibrate(Context context) {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, "GT_VIBRATE_ENABLED", true);
    }

    public static void enableSound(boolean z) {
        if (appContext != null) {
            OneSignalPrefs.saveBool(OneSignalPrefs.PREFS_ONESIGNAL, "GT_SOUND_ENABLED", z);
        }
    }

    static boolean getSoundEnabled(Context context) {
        return OneSignalPrefs.getBool(OneSignalPrefs.PREFS_ONESIGNAL, "GT_SOUND_ENABLED", true);
    }

    static void setLastSessionTime(long j) {
        OneSignalPrefs.saveLong(OneSignalPrefs.PREFS_ONESIGNAL, "OS_LAST_SESSION_TIME", j);
    }

    private static long getLastSessionTime(Context context) {
        return OneSignalPrefs.getLong(OneSignalPrefs.PREFS_ONESIGNAL, "OS_LAST_SESSION_TIME", -31000);
    }

    public static void setInFocusDisplaying(OSInFocusDisplayOption oSInFocusDisplayOption) {
        getCurrentOrNewInitBuilder().mDisplayOptionCarryOver = true;
        getCurrentOrNewInitBuilder().mDisplayOption = oSInFocusDisplayOption;
    }

    public static void setInFocusDisplaying(int i) {
        setInFocusDisplaying(getInFocusDisplaying(i));
    }

    public static OSInFocusDisplayOption currentInFocusDisplayOption() {
        return getCurrentOrNewInitBuilder().mDisplayOption;
    }

    private static OSInFocusDisplayOption getInFocusDisplaying(int i) {
        switch (i) {
            case 0:
                return OSInFocusDisplayOption.None;
            case 1:
                return OSInFocusDisplayOption.InAppAlert;
            case 2:
                return OSInFocusDisplayOption.Notification;
            default:
                if (i < 0) {
                    return OSInFocusDisplayOption.None;
                }
                return OSInFocusDisplayOption.Notification;
        }
    }

    static boolean getNotificationsWhenActiveEnabled() {
        if (mInitBuilder == null || mInitBuilder.mDisplayOption == OSInFocusDisplayOption.Notification) {
            return true;
        }
        return false;
    }

    static boolean getInAppAlertNotificationEnabled() {
        if (mInitBuilder != null && mInitBuilder.mDisplayOption == OSInFocusDisplayOption.InAppAlert) {
            return true;
        }
        return false;
    }

    public static void setSubscription(final boolean z) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("setSubscription()")) {
            AnonymousClass19 r0 = new Runnable() {
                public void run() {
                    OneSignal.getCurrentSubscriptionState(OneSignal.appContext).setUserSubscriptionSetting(z);
                    OneSignalStateSynchronizer.setSubscription(z);
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Moving subscription action to a waiting task queue.");
                addTaskToQueue(new PendingTaskRunnable(r0));
                return;
            }
            r0.run();
        }
    }

    public static void setLocationShared(boolean z) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("setLocationShared()")) {
            shareLocation = z;
            if (!z) {
                OneSignalStateSynchronizer.clearLocation();
            }
            LOG_LEVEL log_level = LOG_LEVEL.DEBUG;
            Log(log_level, "shareLocation:" + shareLocation);
        }
    }

    public static void promptLocation() {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("promptLocation()")) {
            AnonymousClass20 r0 = new Runnable() {
                public void run() {
                    LocationGMS.getLocation(OneSignal.appContext, true, new LocationGMS.LocationHandler() {
                        public LocationGMS.CALLBACK_TYPE getType() {
                            return LocationGMS.CALLBACK_TYPE.PROMPT_LOCATION;
                        }

                        public void complete(LocationGMS.LocationPoint locationPoint) {
                            if (!OneSignal.shouldLogUserPrivacyConsentErrorMessageForMethodName("promptLocation()") && locationPoint != null) {
                                OneSignalStateSynchronizer.updateLocation(locationPoint);
                            }
                        }
                    });
                    boolean unused = OneSignal.promptedLocation = true;
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not prompt for location at this time - moving this operation to awaiting queue.");
                addTaskToQueue(new PendingTaskRunnable(r0));
                return;
            }
            r0.run();
        }
    }

    public static void clearOneSignalNotifications() {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("clearOneSignalNotifications()")) {
            AnonymousClass21 r0 = new Runnable() {
                /* JADX WARN: Type inference failed for: r2v0 */
                /* JADX WARN: Type inference failed for: r2v1, types: [android.database.Cursor] */
                /* JADX WARN: Type inference failed for: r2v2 */
                /* JADX WARN: Type inference failed for: r2v4, types: [android.database.sqlite.SQLiteDatabase] */
                /* JADX WARN: Type inference failed for: r2v7 */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                /* JADX WARNING: Code restructure failed: missing block: B:15:0x0067, code lost:
                    r0 = th;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
                    r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR;
                    r2 = "Error closing transaction! ";
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x0086, code lost:
                    r0 = th;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
                    r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR;
                    r2 = "Error closing transaction! ";
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a6, code lost:
                    r0 = th;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a8, code lost:
                    r0 = th;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a9, code lost:
                    r2 = r3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:52:0x00b8, code lost:
                    r2.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:54:0x00be, code lost:
                    r3.close();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
                    return;
                 */
                /* JADX WARNING: Failed to process nested try/catch */
                /* JADX WARNING: Multi-variable type inference failed */
                /* JADX WARNING: Removed duplicated region for block: B:28:0x0082 A[SYNTHETIC, Splitter:B:28:0x0082] */
                /* JADX WARNING: Removed duplicated region for block: B:35:0x0093  */
                /* JADX WARNING: Removed duplicated region for block: B:37:0x0099 A[SYNTHETIC, Splitter:B:37:0x0099] */
                /* JADX WARNING: Removed duplicated region for block: B:43:0x00a6 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0029] */
                /* JADX WARNING: Removed duplicated region for block: B:52:0x00b8  */
                /* JADX WARNING: Removed duplicated region for block: B:54:0x00be  */
                /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Unknown variable types count: 1 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r13 = this;
                        android.content.Context r0 = com.onesignal.OneSignal.appContext
                        java.lang.String r1 = "notification"
                        java.lang.Object r0 = r0.getSystemService(r1)
                        android.app.NotificationManager r0 = (android.app.NotificationManager) r0
                        android.content.Context r1 = com.onesignal.OneSignal.appContext
                        com.onesignal.OneSignalDbHelper r1 = com.onesignal.OneSignalDbHelper.getInstance(r1)
                        r2 = 0
                        android.database.sqlite.SQLiteDatabase r3 = r1.getReadableDbWithRetries()     // Catch:{ Throwable -> 0x00ae }
                        r11 = 1
                        java.lang.String[] r5 = new java.lang.String[r11]     // Catch:{ Throwable -> 0x00ae }
                        java.lang.String r4 = "android_notification_id"
                        r12 = 0
                        r5[r12] = r4     // Catch:{ Throwable -> 0x00ae }
                        java.lang.String r4 = "notification"
                        java.lang.String r6 = "dismissed = 0 AND opened = 0"
                        r7 = 0
                        r8 = 0
                        r9 = 0
                        r10 = 0
                        android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x00ae }
                        boolean r4 = r3.moveToFirst()     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        if (r4 == 0) goto L_0x0042
                    L_0x002f:
                        java.lang.String r4 = "android_notification_id"
                        int r4 = r3.getColumnIndex(r4)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        int r4 = r3.getInt(r4)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        r0.cancel(r4)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        boolean r4 = r3.moveToNext()     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        if (r4 != 0) goto L_0x002f
                    L_0x0042:
                        android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDbWithRetries()     // Catch:{ Throwable -> 0x0078 }
                        r0.beginTransaction()     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        java.lang.String r1 = "opened = 0"
                        android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        r4.<init>()     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        java.lang.String r5 = "dismissed"
                        java.lang.Integer r6 = java.lang.Integer.valueOf(r11)     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        r4.put(r5, r6)     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        java.lang.String r5 = "notification"
                        r0.update(r5, r4, r1, r2)     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        r0.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0072, all -> 0x0070 }
                        if (r0 == 0) goto L_0x008c
                        r0.endTransaction()     // Catch:{ Throwable -> 0x0067, all -> 0x00a6 }
                        goto L_0x008c
                    L_0x0067:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        java.lang.String r2 = "Error closing transaction! "
                    L_0x006c:
                        com.onesignal.OneSignal.Log(r1, r2, r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        goto L_0x008c
                    L_0x0070:
                        r1 = move-exception
                        goto L_0x0097
                    L_0x0072:
                        r1 = move-exception
                        r2 = r0
                        goto L_0x0079
                    L_0x0075:
                        r1 = move-exception
                        r0 = r2
                        goto L_0x0097
                    L_0x0078:
                        r1 = move-exception
                    L_0x0079:
                        com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0075 }
                        java.lang.String r4 = "Error marking all notifications as dismissed! "
                        com.onesignal.OneSignal.Log(r0, r4, r1)     // Catch:{ all -> 0x0075 }
                        if (r2 == 0) goto L_0x008c
                        r2.endTransaction()     // Catch:{ Throwable -> 0x0086, all -> 0x00a6 }
                        goto L_0x008c
                    L_0x0086:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        java.lang.String r2 = "Error closing transaction! "
                        goto L_0x006c
                    L_0x008c:
                        android.content.Context r0 = com.onesignal.OneSignal.appContext     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        com.onesignal.BadgeCountUpdater.updateCount(r12, r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        if (r3 == 0) goto L_0x00bb
                        r3.close()
                        goto L_0x00bb
                    L_0x0097:
                        if (r0 == 0) goto L_0x00a5
                        r0.endTransaction()     // Catch:{ Throwable -> 0x009d, all -> 0x00a6 }
                        goto L_0x00a5
                    L_0x009d:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                        java.lang.String r4 = "Error closing transaction! "
                        com.onesignal.OneSignal.Log(r2, r4, r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                    L_0x00a5:
                        throw r1     // Catch:{ Throwable -> 0x00a8, all -> 0x00a6 }
                    L_0x00a6:
                        r0 = move-exception
                        goto L_0x00bc
                    L_0x00a8:
                        r0 = move-exception
                        r2 = r3
                        goto L_0x00af
                    L_0x00ab:
                        r0 = move-exception
                        r3 = r2
                        goto L_0x00bc
                    L_0x00ae:
                        r0 = move-exception
                    L_0x00af:
                        com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x00ab }
                        java.lang.String r3 = "Error canceling all notifications! "
                        com.onesignal.OneSignal.Log(r1, r3, r0)     // Catch:{ all -> 0x00ab }
                        if (r2 == 0) goto L_0x00bb
                        r2.close()
                    L_0x00bb:
                        return
                    L_0x00bc:
                        if (r3 == 0) goto L_0x00c1
                        r3.close()
                    L_0x00c1:
                        throw r0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.AnonymousClass21.run():void");
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not clear notifications at this time - moving this operation toa waiting task queue.");
                addTaskToQueue(new PendingTaskRunnable(r0));
                return;
            }
            r0.run();
        }
    }

    public static void cancelNotification(final int i) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("cancelNotification()")) {
            AnonymousClass22 r0 = new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                /* JADX WARNING: Removed duplicated region for block: B:20:0x0092 A[SYNTHETIC, Splitter:B:20:0x0092] */
                /* JADX WARNING: Removed duplicated region for block: B:25:0x00a1 A[SYNTHETIC, Splitter:B:25:0x00a1] */
                /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        android.content.Context r0 = com.onesignal.OneSignal.appContext
                        com.onesignal.OneSignalDbHelper r0 = com.onesignal.OneSignalDbHelper.getInstance(r0)
                        r1 = 0
                        android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDbWithRetries()     // Catch:{ Throwable -> 0x006f, all -> 0x006a }
                        r0.beginTransaction()     // Catch:{ Throwable -> 0x0068 }
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0068 }
                        r2.<init>()     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r3 = "android_notification_id = "
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        int r3 = r4     // Catch:{ Throwable -> 0x0068 }
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r3 = " AND "
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r3 = "opened"
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r3 = " = 0 AND "
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r3 = "dismissed"
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r3 = " = 0"
                        r2.append(r3)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0068 }
                        android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0068 }
                        r3.<init>()     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r4 = "dismissed"
                        r5 = 1
                        java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x0068 }
                        r3.put(r4, r5)     // Catch:{ Throwable -> 0x0068 }
                        java.lang.String r4 = "notification"
                        int r1 = r0.update(r4, r3, r2, r1)     // Catch:{ Throwable -> 0x0068 }
                        if (r1 <= 0) goto L_0x0058
                        android.content.Context r1 = com.onesignal.OneSignal.appContext     // Catch:{ Throwable -> 0x0068 }
                        int r2 = r4     // Catch:{ Throwable -> 0x0068 }
                        com.onesignal.NotificationSummaryManager.updatePossibleDependentSummaryOnDismiss(r1, r0, r2)     // Catch:{ Throwable -> 0x0068 }
                    L_0x0058:
                        android.content.Context r1 = com.onesignal.OneSignal.appContext     // Catch:{ Throwable -> 0x0068 }
                        com.onesignal.BadgeCountUpdater.update(r0, r1)     // Catch:{ Throwable -> 0x0068 }
                        r0.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0068 }
                        if (r0 == 0) goto L_0x009e
                        r0.endTransaction()     // Catch:{ Throwable -> 0x0096 }
                        goto L_0x009e
                    L_0x0066:
                        r1 = move-exception
                        goto L_0x009f
                    L_0x0068:
                        r1 = move-exception
                        goto L_0x0073
                    L_0x006a:
                        r0 = move-exception
                        r6 = r1
                        r1 = r0
                        r0 = r6
                        goto L_0x009f
                    L_0x006f:
                        r0 = move-exception
                        r6 = r1
                        r1 = r0
                        r0 = r6
                    L_0x0073:
                        com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x0066 }
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
                        r3.<init>()     // Catch:{ all -> 0x0066 }
                        java.lang.String r4 = "Error marking a notification id "
                        r3.append(r4)     // Catch:{ all -> 0x0066 }
                        int r4 = r4     // Catch:{ all -> 0x0066 }
                        r3.append(r4)     // Catch:{ all -> 0x0066 }
                        java.lang.String r4 = " as dismissed! "
                        r3.append(r4)     // Catch:{ all -> 0x0066 }
                        java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0066 }
                        com.onesignal.OneSignal.Log(r2, r3, r1)     // Catch:{ all -> 0x0066 }
                        if (r0 == 0) goto L_0x009e
                        r0.endTransaction()     // Catch:{ Throwable -> 0x0096 }
                        goto L_0x009e
                    L_0x0096:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
                        java.lang.String r2 = "Error closing transaction! "
                        com.onesignal.OneSignal.Log(r1, r2, r0)
                    L_0x009e:
                        return
                    L_0x009f:
                        if (r0 == 0) goto L_0x00ad
                        r0.endTransaction()     // Catch:{ Throwable -> 0x00a5 }
                        goto L_0x00ad
                    L_0x00a5:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
                        java.lang.String r3 = "Error closing transaction! "
                        com.onesignal.OneSignal.Log(r2, r3, r0)
                    L_0x00ad:
                        throw r1
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.AnonymousClass22.run():void");
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                LOG_LEVEL log_level = LOG_LEVEL.ERROR;
                Log(log_level, "OneSignal.init has not been called. Could not clear notification id: " + i + " at this time - movingthis operation to a waiting task queue. The notification will still be canceledfrom NotificationManager at this time.");
                taskQueueWaitingForInit.add(r0);
                return;
            }
            r0.run();
            ((NotificationManager) appContext.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME)).cancel(i);
        }
    }

    public static void cancelGroupedNotifications(final String str) {
        if (!shouldLogUserPrivacyConsentErrorMessageForMethodName("cancelGroupedNotifications()")) {
            AnonymousClass23 r0 = new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                /* JADX WARNING: Code restructure failed: missing block: B:11:0x004b, code lost:
                    if (r5.isClosed() == false) goto L_0x0076;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:21:0x0074, code lost:
                    if (r5.isClosed() == false) goto L_0x0076;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0076, code lost:
                    r5.close();
                 */
                /* JADX WARNING: Removed duplicated region for block: B:20:0x0070  */
                /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5 A[SYNTHETIC, Splitter:B:28:0x00a5] */
                /* JADX WARNING: Removed duplicated region for block: B:39:0x00d1 A[SYNTHETIC, Splitter:B:39:0x00d1] */
                /* JADX WARNING: Removed duplicated region for block: B:44:0x00e0 A[SYNTHETIC, Splitter:B:44:0x00e0] */
                /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
                /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r13 = this;
                        android.content.Context r0 = com.onesignal.OneSignal.appContext
                        java.lang.String r1 = "notification"
                        java.lang.Object r0 = r0.getSystemService(r1)
                        android.app.NotificationManager r0 = (android.app.NotificationManager) r0
                        android.content.Context r1 = com.onesignal.OneSignal.appContext
                        com.onesignal.OneSignalDbHelper r1 = com.onesignal.OneSignalDbHelper.getInstance(r1)
                        r2 = 0
                        r3 = 0
                        r4 = 1
                        android.database.sqlite.SQLiteDatabase r5 = r1.getReadableDbWithRetries()     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                        java.lang.String[] r7 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                        java.lang.String r6 = "android_notification_id"
                        r7[r3] = r6     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                        java.lang.String r8 = "group_id = ? AND dismissed = 0 AND opened = 0"
                        java.lang.String[] r9 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                        java.lang.String r6 = r4     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                        r9[r3] = r6     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                        java.lang.String r6 = "notification"
                        r10 = 0
                        r11 = 0
                        r12 = 0
                        android.database.Cursor r5 = r5.query(r6, r7, r8, r9, r10, r11, r12)     // Catch:{ Throwable -> 0x0054, all -> 0x0050 }
                    L_0x002e:
                        boolean r6 = r5.moveToNext()     // Catch:{ Throwable -> 0x004e }
                        if (r6 == 0) goto L_0x0045
                        java.lang.String r6 = "android_notification_id"
                        int r6 = r5.getColumnIndex(r6)     // Catch:{ Throwable -> 0x004e }
                        int r6 = r5.getInt(r6)     // Catch:{ Throwable -> 0x004e }
                        r7 = -1
                        if (r6 == r7) goto L_0x002e
                        r0.cancel(r6)     // Catch:{ Throwable -> 0x004e }
                        goto L_0x002e
                    L_0x0045:
                        if (r5 == 0) goto L_0x0079
                        boolean r0 = r5.isClosed()
                        if (r0 != 0) goto L_0x0079
                        goto L_0x0076
                    L_0x004e:
                        r0 = move-exception
                        goto L_0x0056
                    L_0x0050:
                        r0 = move-exception
                        r5 = r2
                        goto L_0x00ee
                    L_0x0054:
                        r0 = move-exception
                        r5 = r2
                    L_0x0056:
                        com.onesignal.OneSignal$LOG_LEVEL r6 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x00ed }
                        java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ed }
                        r7.<init>()     // Catch:{ all -> 0x00ed }
                        java.lang.String r8 = "Error getting android notifications part of group: "
                        r7.append(r8)     // Catch:{ all -> 0x00ed }
                        java.lang.String r8 = r4     // Catch:{ all -> 0x00ed }
                        r7.append(r8)     // Catch:{ all -> 0x00ed }
                        java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00ed }
                        com.onesignal.OneSignal.Log(r6, r7, r0)     // Catch:{ all -> 0x00ed }
                        if (r5 == 0) goto L_0x0079
                        boolean r0 = r5.isClosed()
                        if (r0 != 0) goto L_0x0079
                    L_0x0076:
                        r5.close()
                    L_0x0079:
                        android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDbWithRetries()     // Catch:{ Throwable -> 0x00b1 }
                        r0.beginTransaction()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        java.lang.String r1 = "group_id = ? AND opened = 0 AND dismissed = 0"
                        java.lang.String[] r2 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        java.lang.String r5 = r4     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        r2[r3] = r5     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        r3.<init>()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        java.lang.String r5 = "dismissed"
                        java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        r3.put(r5, r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        java.lang.String r4 = "notification"
                        r0.update(r4, r3, r1, r2)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        android.content.Context r1 = com.onesignal.OneSignal.appContext     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        com.onesignal.BadgeCountUpdater.update(r0, r1)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        r0.setTransactionSuccessful()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a9 }
                        if (r0 == 0) goto L_0x00dd
                        r0.endTransaction()     // Catch:{ Throwable -> 0x00d5 }
                        goto L_0x00dd
                    L_0x00a9:
                        r1 = move-exception
                        goto L_0x00de
                    L_0x00ab:
                        r1 = move-exception
                        r2 = r0
                        goto L_0x00b2
                    L_0x00ae:
                        r1 = move-exception
                        r0 = r2
                        goto L_0x00de
                    L_0x00b1:
                        r1 = move-exception
                    L_0x00b2:
                        com.onesignal.OneSignal$LOG_LEVEL r0 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x00ae }
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ae }
                        r3.<init>()     // Catch:{ all -> 0x00ae }
                        java.lang.String r4 = "Error marking a notifications with group "
                        r3.append(r4)     // Catch:{ all -> 0x00ae }
                        java.lang.String r4 = r4     // Catch:{ all -> 0x00ae }
                        r3.append(r4)     // Catch:{ all -> 0x00ae }
                        java.lang.String r4 = " as dismissed! "
                        r3.append(r4)     // Catch:{ all -> 0x00ae }
                        java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ae }
                        com.onesignal.OneSignal.Log(r0, r3, r1)     // Catch:{ all -> 0x00ae }
                        if (r2 == 0) goto L_0x00dd
                        r2.endTransaction()     // Catch:{ Throwable -> 0x00d5 }
                        goto L_0x00dd
                    L_0x00d5:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r1 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
                        java.lang.String r2 = "Error closing transaction! "
                        com.onesignal.OneSignal.Log(r1, r2, r0)
                    L_0x00dd:
                        return
                    L_0x00de:
                        if (r0 == 0) goto L_0x00ec
                        r0.endTransaction()     // Catch:{ Throwable -> 0x00e4 }
                        goto L_0x00ec
                    L_0x00e4:
                        r0 = move-exception
                        com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.ERROR
                        java.lang.String r3 = "Error closing transaction! "
                        com.onesignal.OneSignal.Log(r2, r3, r0)
                    L_0x00ec:
                        throw r1
                    L_0x00ed:
                        r0 = move-exception
                    L_0x00ee:
                        if (r5 == 0) goto L_0x00f9
                        boolean r1 = r5.isClosed()
                        if (r1 != 0) goto L_0x00f9
                        r5.close()
                    L_0x00f9:
                        throw r0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.AnonymousClass23.run():void");
                }
            };
            if (appContext == null || shouldRunTaskThroughQueue()) {
                LOG_LEVEL log_level = LOG_LEVEL.ERROR;
                Log(log_level, "OneSignal.init has not been called. Could not clear notifications part of group " + str + " - movingthis operation to a waiting task queue.");
                addTaskToQueue(new PendingTaskRunnable(r0));
                return;
            }
            r0.run();
        }
    }

    public static void removeNotificationOpenedHandler() {
        getCurrentOrNewInitBuilder().mNotificationOpenedHandler = null;
    }

    public static void removeNotificationReceivedHandler() {
        getCurrentOrNewInitBuilder().mNotificationReceivedHandler = null;
    }

    public static void addPermissionObserver(OSPermissionObserver oSPermissionObserver) {
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not add permission observer");
            return;
        }
        getPermissionStateChangesObserver().addObserver(oSPermissionObserver);
        if (getCurrentPermissionState(appContext).compare(getLastPermissionState(appContext))) {
            OSPermissionChangedInternalObserver.fireChangesToPublicObserver(getCurrentPermissionState(appContext));
        }
    }

    public static void removePermissionObserver(OSPermissionObserver oSPermissionObserver) {
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not modify permission observer");
        } else {
            getPermissionStateChangesObserver().removeObserver(oSPermissionObserver);
        }
    }

    public static void addSubscriptionObserver(OSSubscriptionObserver oSSubscriptionObserver) {
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not add subscription observer");
            return;
        }
        getSubscriptionStateChangesObserver().addObserver(oSSubscriptionObserver);
        if (getCurrentSubscriptionState(appContext).compare(getLastSubscriptionState(appContext))) {
            OSSubscriptionChangedInternalObserver.fireChangesToPublicObserver(getCurrentSubscriptionState(appContext));
        }
    }

    public static void removeSubscriptionObserver(OSSubscriptionObserver oSSubscriptionObserver) {
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not modify subscription observer");
        } else {
            getSubscriptionStateChangesObserver().removeObserver(oSSubscriptionObserver);
        }
    }

    public static void addEmailSubscriptionObserver(@NonNull OSEmailSubscriptionObserver oSEmailSubscriptionObserver) {
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not add email subscription observer");
            return;
        }
        getEmailSubscriptionStateChangesObserver().addObserver(oSEmailSubscriptionObserver);
        if (getCurrentEmailSubscriptionState(appContext).compare(getLastEmailSubscriptionState(appContext))) {
            OSEmailSubscriptionChangedInternalObserver.fireChangesToPublicObserver(getCurrentEmailSubscriptionState(appContext));
        }
    }

    public static void removeEmailSubscriptionObserver(@NonNull OSEmailSubscriptionObserver oSEmailSubscriptionObserver) {
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not modify email subscription observer");
        } else {
            getEmailSubscriptionStateChangesObserver().removeObserver(oSEmailSubscriptionObserver);
        }
    }

    public static OSPermissionSubscriptionState getPermissionSubscriptionState() {
        if (shouldLogUserPrivacyConsentErrorMessageForMethodName("getPermissionSubscriptionState()")) {
            return null;
        }
        if (appContext == null) {
            Log(LOG_LEVEL.ERROR, "OneSignal.init has not been called. Could not get OSPermissionSubscriptionState");
            return null;
        }
        OSPermissionSubscriptionState oSPermissionSubscriptionState = new OSPermissionSubscriptionState();
        oSPermissionSubscriptionState.subscriptionStatus = getCurrentSubscriptionState(appContext);
        oSPermissionSubscriptionState.permissionStatus = getCurrentPermissionState(appContext);
        oSPermissionSubscriptionState.emailSubscriptionStatus = getCurrentEmailSubscriptionState(appContext);
        return oSPermissionSubscriptionState;
    }

    static long GetUnsentActiveTime() {
        if (unSentActiveTime == -1 && appContext != null) {
            unSentActiveTime = OneSignalPrefs.getLong(OneSignalPrefs.PREFS_ONESIGNAL, "GT_UNSENT_ACTIVE_TIME", 0);
        }
        LOG_LEVEL log_level = LOG_LEVEL.INFO;
        Log(log_level, "GetUnsentActiveTime: " + unSentActiveTime);
        return unSentActiveTime;
    }

    /* access modifiers changed from: private */
    public static void SaveUnsentActiveTime(long j) {
        unSentActiveTime = j;
        if (appContext != null) {
            LOG_LEVEL log_level = LOG_LEVEL.INFO;
            Log(log_level, "SaveUnsentActiveTime: " + unSentActiveTime);
            OneSignalPrefs.saveLong(OneSignalPrefs.PREFS_ONESIGNAL, "GT_UNSENT_ACTIVE_TIME", j);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0066 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean isDuplicateNotification(java.lang.String r12, android.content.Context r13) {
        /*
            r0 = 0
            if (r12 == 0) goto L_0x006d
            java.lang.String r1 = ""
            boolean r1 = r1.equals(r12)
            if (r1 == 0) goto L_0x000c
            goto L_0x006d
        L_0x000c:
            com.onesignal.OneSignalDbHelper r13 = com.onesignal.OneSignalDbHelper.getInstance(r13)
            r1 = 0
            r2 = 1
            android.database.sqlite.SQLiteDatabase r3 = r13.getReadableDbWithRetries()     // Catch:{ Throwable -> 0x003f }
            java.lang.String[] r5 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x003f }
            java.lang.String r13 = "notification_id"
            r5[r0] = r13     // Catch:{ Throwable -> 0x003f }
            java.lang.String[] r7 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x003f }
            r7[r0] = r12     // Catch:{ Throwable -> 0x003f }
            java.lang.String r4 = "notification"
            java.lang.String r6 = "notification_id = ?"
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r13 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x003f }
            boolean r1 = r13.moveToFirst()     // Catch:{ Throwable -> 0x0038, all -> 0x0035 }
            if (r13 == 0) goto L_0x004d
            r13.close()
            goto L_0x004d
        L_0x0035:
            r12 = move-exception
            r1 = r13
            goto L_0x0067
        L_0x0038:
            r1 = move-exception
            r11 = r1
            r1 = r13
            r13 = r11
            goto L_0x0040
        L_0x003d:
            r12 = move-exception
            goto L_0x0067
        L_0x003f:
            r13 = move-exception
        L_0x0040:
            com.onesignal.OneSignal$LOG_LEVEL r3 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x003d }
            java.lang.String r4 = "Could not check for duplicate, assuming unique."
            Log(r3, r4, r13)     // Catch:{ all -> 0x003d }
            if (r1 == 0) goto L_0x004c
            r1.close()
        L_0x004c:
            r1 = 0
        L_0x004d:
            if (r1 == 0) goto L_0x0066
            com.onesignal.OneSignal$LOG_LEVEL r13 = com.onesignal.OneSignal.LOG_LEVEL.DEBUG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Duplicate GCM message received, skip processing of "
            r0.append(r1)
            r0.append(r12)
            java.lang.String r12 = r0.toString()
            Log(r13, r12)
            return r2
        L_0x0066:
            return r0
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            r1.close()
        L_0x006c:
            throw r12
        L_0x006d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignal.isDuplicateNotification(java.lang.String, android.content.Context):boolean");
    }

    static boolean notValidOrDuplicated(Context context, JSONObject jSONObject) {
        String notificationIdFromGCMJsonPayload = getNotificationIdFromGCMJsonPayload(jSONObject);
        return notificationIdFromGCMJsonPayload == null || isDuplicateNotification(notificationIdFromGCMJsonPayload, context);
    }

    static String getNotificationIdFromGCMBundle(Bundle bundle) {
        if (bundle.isEmpty()) {
            return null;
        }
        try {
            if (bundle.containsKey("custom")) {
                JSONObject jSONObject = new JSONObject(bundle.getString("custom"));
                if (jSONObject.has("i")) {
                    return jSONObject.optString("i", null);
                }
                Log(LOG_LEVEL.DEBUG, "Not a OneSignal formatted GCM message. No 'i' field in custom.");
            } else {
                Log(LOG_LEVEL.DEBUG, "Not a OneSignal formatted GCM message. No 'custom' field in the bundle.");
            }
        } catch (Throwable th) {
            Log(LOG_LEVEL.DEBUG, "Could not parse bundle, probably not a OneSignal notification.", th);
        }
        return null;
    }

    private static String getNotificationIdFromGCMJsonPayload(JSONObject jSONObject) {
        try {
            return new JSONObject(jSONObject.optString("custom")).optString("i", null);
        } catch (Throwable unused) {
            return null;
        }
    }

    static boolean isAppActive() {
        return initDone && isForeground();
    }

    static void updateOnSessionDependents() {
        sendAsSession = false;
        setLastSessionTime(System.currentTimeMillis());
    }

    private static boolean isPastOnSessionTime() {
        if (!sendAsSession && (System.currentTimeMillis() - getLastSessionTime(appContext)) / 1000 < MIN_ON_SESSION_TIME) {
            return false;
        }
        return true;
    }

    static boolean areNotificationsEnabledForSubscribedState() {
        if (mInitBuilder.mUnsubscribeWhenNotificationsAreDisabled) {
            return OSUtils.areNotificationsEnabled(appContext);
        }
        return true;
    }

    static void handleSuccessfulEmailLogout() {
        if (emailLogoutHandler != null) {
            emailLogoutHandler.onSuccess();
            emailLogoutHandler = null;
        }
    }

    static void handleFailedEmailLogout() {
        if (emailLogoutHandler != null) {
            emailLogoutHandler.onFailure(new EmailUpdateError(EmailErrorType.NETWORK, "Failed due to network failure. Will retry on next sync."));
            emailLogoutHandler = null;
        }
    }

    static void fireEmailUpdateSuccess() {
        if (emailUpdateHandler != null) {
            emailUpdateHandler.onSuccess();
            emailUpdateHandler = null;
        }
    }

    static void fireEmailUpdateFailure() {
        if (emailUpdateHandler != null) {
            emailUpdateHandler.onFailure(new EmailUpdateError(EmailErrorType.NETWORK, "Failed due to network failure. Will retry on next sync."));
            emailUpdateHandler = null;
        }
    }
}
