package com.onesignal;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import com.onesignal.OneSignal;
import java.util.HashMap;

class OneSignalPrefs {
    static final String PREFS_EXISTING_PURCHASES = "ExistingPurchases";
    static final String PREFS_GT_APP_ID = "GT_APP_ID";
    static final String PREFS_GT_DO_NOT_SHOW_MISSING_GPS = "GT_DO_NOT_SHOW_MISSING_GPS";
    static final String PREFS_GT_FIREBASE_TRACKING_ENABLED = "GT_FIREBASE_TRACKING_ENABLED";
    static final String PREFS_GT_PLAYER_ID = "GT_PLAYER_ID";
    static final String PREFS_GT_REGISTRATION_ID = "GT_REGISTRATION_ID";
    static final String PREFS_GT_SOUND_ENABLED = "GT_SOUND_ENABLED";
    static final String PREFS_GT_UNSENT_ACTIVE_TIME = "GT_UNSENT_ACTIVE_TIME";
    static final String PREFS_GT_VIBRATE_ENABLED = "GT_VIBRATE_ENABLED";
    public static final String PREFS_ONESIGNAL = OneSignal.class.getSimpleName();
    static final String PREFS_ONESIGNAL_ACCEPTED_NOTIFICATION_LAST = "ONESIGNAL_ACCEPTED_NOTIFICATION_LAST";
    static final String PREFS_ONESIGNAL_EMAIL_ADDRESS_LAST = "PREFS_ONESIGNAL_EMAIL_ADDRESS_LAST";
    static final String PREFS_ONESIGNAL_EMAIL_ID_LAST = "PREFS_ONESIGNAL_EMAIL_ID_LAST";
    static final String PREFS_ONESIGNAL_PERMISSION_ACCEPTED_LAST = "ONESIGNAL_PERMISSION_ACCEPTED_LAST";
    static final String PREFS_ONESIGNAL_PLAYER_ID_LAST = "ONESIGNAL_PLAYER_ID_LAST";
    static final String PREFS_ONESIGNAL_PUSH_TOKEN_LAST = "ONESIGNAL_PUSH_TOKEN_LAST";
    static final String PREFS_ONESIGNAL_SUBSCRIPTION = "ONESIGNAL_SUBSCRIPTION";
    static final String PREFS_ONESIGNAL_SUBSCRIPTION_LAST = "ONESIGNAL_SUBSCRIPTION_LAST";
    static final String PREFS_ONESIGNAL_SYNCED_SUBSCRIPTION = "ONESIGNAL_SYNCED_SUBSCRIPTION";
    static final String PREFS_ONESIGNAL_USERSTATE_DEPENDVALYES_ = "ONESIGNAL_USERSTATE_DEPENDVALYES_";
    static final String PREFS_ONESIGNAL_USERSTATE_SYNCVALYES_ = "ONESIGNAL_USERSTATE_SYNCVALYES_";
    static final String PREFS_ONESIGNAL_USER_PROVIDED_CONSENT = "ONESIGNAL_USER_PROVIDED_CONSENT";
    static final String PREFS_OS_EMAIL_ID = "OS_EMAIL_ID";
    static final String PREFS_OS_FILTER_OTHER_GCM_RECEIVERS = "OS_FILTER_OTHER_GCM_RECEIVERS";
    static final String PREFS_OS_LAST_LOCATION_TIME = "OS_LAST_LOCATION_TIME";
    static final String PREFS_OS_LAST_SESSION_TIME = "OS_LAST_SESSION_TIME";
    static final String PREFS_PLAYER_PURCHASES = "GTPlayerPurchases";
    static final String PREFS_PURCHASE_TOKENS = "purchaseTokens";
    public static WritePrefHandlerThread prefsHandler;
    static HashMap<String, HashMap<String, Object>> prefsToApply;

    OneSignalPrefs() {
    }

    static {
        initializePool();
    }

    public static class WritePrefHandlerThread extends HandlerThread {
        private static final int WRITE_CALL_DELAY_TO_BUFFER_MS = 200;
        private long lastSyncTime = 0;
        public Handler mHandler;

        WritePrefHandlerThread() {
            super("OSH_WritePrefs");
            start();
            this.mHandler = new Handler(getLooper());
        }

        /* access modifiers changed from: package-private */
        public void startDelayedWrite() {
            synchronized (this.mHandler) {
                this.mHandler.removeCallbacksAndMessages(null);
                if (this.lastSyncTime == 0) {
                    this.lastSyncTime = System.currentTimeMillis();
                }
                this.mHandler.postDelayed(getNewRunnable(), (this.lastSyncTime - System.currentTimeMillis()) + 200);
            }
        }

        private Runnable getNewRunnable() {
            return new Runnable() {
                public void run() {
                    WritePrefHandlerThread.this.flushBufferToDisk();
                }
            };
        }

        /* access modifiers changed from: private */
        public void flushBufferToDisk() {
            if (OneSignal.appContext != null) {
                for (String next : OneSignalPrefs.prefsToApply.keySet()) {
                    SharedPreferences.Editor edit = OneSignalPrefs.getSharedPrefsByName(next).edit();
                    HashMap hashMap = OneSignalPrefs.prefsToApply.get(next);
                    synchronized (hashMap) {
                        for (String str : hashMap.keySet()) {
                            Object obj = hashMap.get(str);
                            if (obj instanceof String) {
                                edit.putString(str, (String) obj);
                            } else if (obj instanceof Boolean) {
                                edit.putBoolean(str, ((Boolean) obj).booleanValue());
                            } else if (obj instanceof Integer) {
                                edit.putInt(str, ((Integer) obj).intValue());
                            } else if (obj instanceof Long) {
                                edit.putLong(str, ((Long) obj).longValue());
                            }
                        }
                        hashMap.clear();
                    }
                    edit.apply();
                }
                this.lastSyncTime = System.currentTimeMillis();
            }
        }
    }

    public static void initializePool() {
        prefsToApply = new HashMap<>();
        prefsToApply.put(PREFS_ONESIGNAL, new HashMap());
        prefsToApply.put(PREFS_PLAYER_PURCHASES, new HashMap());
        prefsHandler = new WritePrefHandlerThread();
    }

    public static void startDelayedWrite() {
        prefsHandler.startDelayedWrite();
    }

    public static void saveString(String str, String str2, String str3) {
        save(str, str2, str3);
    }

    public static void saveBool(String str, String str2, boolean z) {
        save(str, str2, Boolean.valueOf(z));
    }

    public static void saveInt(String str, String str2, int i) {
        save(str, str2, Integer.valueOf(i));
    }

    public static void saveLong(String str, String str2, long j) {
        save(str, str2, Long.valueOf(j));
    }

    private static void save(String str, String str2, Object obj) {
        HashMap hashMap = prefsToApply.get(str);
        synchronized (hashMap) {
            hashMap.put(str2, obj);
        }
        startDelayedWrite();
    }

    static String getString(String str, String str2, String str3) {
        return (String) get(str, str2, String.class, str3);
    }

    static boolean getBool(String str, String str2, boolean z) {
        return ((Boolean) get(str, str2, Boolean.class, Boolean.valueOf(z))).booleanValue();
    }

    static int getInt(String str, String str2, int i) {
        return ((Integer) get(str, str2, Integer.class, Integer.valueOf(i))).intValue();
    }

    static long getLong(String str, String str2, long j) {
        return ((Long) get(str, str2, Long.class, Long.valueOf(j))).longValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002c, code lost:
        r3 = getSharedPrefsByName(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0030, code lost:
        if (r3 == null) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0038, code lost:
        if (r5.equals(java.lang.String.class) == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        return r3.getString(r4, (java.lang.String) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0047, code lost:
        if (r5.equals(java.lang.Boolean.class) == false) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0057, code lost:
        return java.lang.Boolean.valueOf(r3.getBoolean(r4, ((java.lang.Boolean) r6).booleanValue()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
        if (r5.equals(java.lang.Integer.class) == false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x006e, code lost:
        return java.lang.Integer.valueOf(r3.getInt(r4, ((java.lang.Integer) r6).intValue()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0075, code lost:
        if (r5.equals(java.lang.Long.class) == false) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0085, code lost:
        return java.lang.Long.valueOf(r3.getLong(r4, ((java.lang.Long) r6).longValue()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008c, code lost:
        if (r5.equals(java.lang.Object.class) == false) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0096, code lost:
        return java.lang.Boolean.valueOf(r3.contains(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0097, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0099, code lost:
        return r6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object get(java.lang.String r3, java.lang.String r4, java.lang.Class r5, java.lang.Object r6) {
        /*
            java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, java.lang.Object>> r0 = com.onesignal.OneSignalPrefs.prefsToApply
            java.lang.Object r0 = r0.get(r3)
            java.util.HashMap r0 = (java.util.HashMap) r0
            monitor-enter(r0)
            java.lang.Class<java.lang.Object> r1 = java.lang.Object.class
            boolean r1 = r5.equals(r1)     // Catch:{ all -> 0x009c }
            if (r1 == 0) goto L_0x001e
            boolean r1 = r0.containsKey(r4)     // Catch:{ all -> 0x009c }
            if (r1 == 0) goto L_0x001e
            r3 = 1
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x009c }
            monitor-exit(r0)     // Catch:{ all -> 0x009c }
            return r3
        L_0x001e:
            java.lang.Object r1 = r0.get(r4)     // Catch:{ all -> 0x009c }
            if (r1 != 0) goto L_0x009a
            boolean r2 = r0.containsKey(r4)     // Catch:{ all -> 0x009c }
            if (r2 == 0) goto L_0x002b
            goto L_0x009a
        L_0x002b:
            monitor-exit(r0)     // Catch:{ all -> 0x009c }
            android.content.SharedPreferences r3 = getSharedPrefsByName(r3)
            if (r3 == 0) goto L_0x0099
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0041
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r3 = r3.getString(r4, r6)
            return r3
        L_0x0041:
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0058
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r5 = r6.booleanValue()
            boolean r3 = r3.getBoolean(r4, r5)
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            return r3
        L_0x0058:
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x006f
            java.lang.Integer r6 = (java.lang.Integer) r6
            int r5 = r6.intValue()
            int r3 = r3.getInt(r4, r5)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            return r3
        L_0x006f:
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0086
            java.lang.Long r6 = (java.lang.Long) r6
            long r5 = r6.longValue()
            long r3 = r3.getLong(r4, r5)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            return r3
        L_0x0086:
            java.lang.Class<java.lang.Object> r6 = java.lang.Object.class
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0097
            boolean r3 = r3.contains(r4)
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            return r3
        L_0x0097:
            r3 = 0
            return r3
        L_0x0099:
            return r6
        L_0x009a:
            monitor-exit(r0)     // Catch:{ all -> 0x009c }
            return r1
        L_0x009c:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x009c }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.OneSignalPrefs.get(java.lang.String, java.lang.String, java.lang.Class, java.lang.Object):java.lang.Object");
    }

    /* access modifiers changed from: private */
    public static synchronized SharedPreferences getSharedPrefsByName(String str) {
        synchronized (OneSignalPrefs.class) {
            if (OneSignal.appContext == null) {
                OneSignal.Log(OneSignal.LOG_LEVEL.WARN, "OneSignal.appContext null, could not read " + str + " from getSharedPreferences.", new Throwable());
                return null;
            }
            SharedPreferences sharedPreferences = OneSignal.appContext.getSharedPreferences(str, 0);
            return sharedPreferences;
        }
    }
}
