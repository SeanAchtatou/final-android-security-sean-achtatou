package com.onesignal.onesignalsdk;

public final class R {
    private R() {
    }

    public static final class color {
        public static final int onesignal_bgimage_notif_body_color = 2131034327;
        public static final int onesignal_bgimage_notif_title_color = 2131034328;

        private color() {
        }
    }

    public static final class drawable {
        public static final int ic_stat_onesignal_default = 2131165393;
        public static final int onesignal_bgimage_default_image = 2131165457;

        private drawable() {
        }
    }

    public static final class raw {
        public static final int notification = 2131623937;

        private raw() {
        }
    }
}
