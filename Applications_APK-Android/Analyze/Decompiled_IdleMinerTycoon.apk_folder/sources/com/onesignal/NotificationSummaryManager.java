package com.onesignal;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.onesignal.OneSignal;
import com.onesignal.OneSignalDbContract;
import org.json.JSONException;
import org.json.JSONObject;

class NotificationSummaryManager {
    NotificationSummaryManager() {
    }

    static void updatePossibleDependentSummaryOnDismiss(Context context, SQLiteDatabase sQLiteDatabase, int i) {
        Cursor query = sQLiteDatabase.query(OneSignalDbContract.NotificationTable.TABLE_NAME, new String[]{"group_id"}, "android_notification_id = " + i, null, null, null, null);
        if (query.moveToFirst()) {
            String string = query.getString(query.getColumnIndex("group_id"));
            query.close();
            if (string != null) {
                updateSummaryNotificationAfterChildRemoved(context, sQLiteDatabase, string, true);
                return;
            }
            return;
        }
        query.close();
    }

    static void updateSummaryNotificationAfterChildRemoved(Context context, SQLiteDatabase sQLiteDatabase, String str, boolean z) {
        try {
            Cursor internalUpdateSummaryNotificationAfterChildRemoved = internalUpdateSummaryNotificationAfterChildRemoved(context, sQLiteDatabase, str, z);
            if (internalUpdateSummaryNotificationAfterChildRemoved != null && !internalUpdateSummaryNotificationAfterChildRemoved.isClosed()) {
                internalUpdateSummaryNotificationAfterChildRemoved.close();
            }
        } catch (Throwable th) {
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Error running updateSummaryNotificationAfterChildRemoved!", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static Cursor internalUpdateSummaryNotificationAfterChildRemoved(Context context, SQLiteDatabase sQLiteDatabase, String str, boolean z) {
        Cursor query = sQLiteDatabase.query(OneSignalDbContract.NotificationTable.TABLE_NAME, new String[]{OneSignalDbContract.NotificationTable.COLUMN_NAME_ANDROID_NOTIFICATION_ID, OneSignalDbContract.NotificationTable.COLUMN_NAME_CREATED_TIME}, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 0", new String[]{str}, null, null, "_id DESC");
        int count = query.getCount();
        if (count == 0) {
            query.close();
            Integer summaryNotificationId = getSummaryNotificationId(sQLiteDatabase, str);
            if (summaryNotificationId == null) {
                return query;
            }
            ((NotificationManager) context.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME)).cancel(summaryNotificationId.intValue());
            ContentValues contentValues = new ContentValues();
            contentValues.put(z ? OneSignalDbContract.NotificationTable.COLUMN_NAME_DISMISSED : OneSignalDbContract.NotificationTable.COLUMN_NAME_OPENED, (Integer) 1);
            sQLiteDatabase.update(OneSignalDbContract.NotificationTable.TABLE_NAME, contentValues, "android_notification_id = " + summaryNotificationId, null);
            return query;
        } else if (count == 1) {
            query.close();
            if (getSummaryNotificationId(sQLiteDatabase, str) == null) {
                return query;
            }
            restoreSummary(context, str);
            return query;
        } else {
            try {
                query.moveToFirst();
                Long valueOf = Long.valueOf(query.getLong(query.getColumnIndex(OneSignalDbContract.NotificationTable.COLUMN_NAME_CREATED_TIME)));
                query.close();
                if (getSummaryNotificationId(sQLiteDatabase, str) == null) {
                    return query;
                }
                NotificationGenerationJob notificationGenerationJob = new NotificationGenerationJob(context);
                notificationGenerationJob.restoring = true;
                notificationGenerationJob.shownTimeStamp = valueOf;
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("grp", str);
                notificationGenerationJob.jsonPayload = jSONObject;
                GenerateNotification.updateSummaryNotification(notificationGenerationJob);
                return query;
            } catch (JSONException unused) {
            }
        }
    }

    private static void restoreSummary(Context context, String str) {
        String[] strArr = {str};
        Cursor cursor = null;
        try {
            Cursor query = OneSignalDbHelper.getInstance(context).getReadableDbWithRetries().query(OneSignalDbContract.NotificationTable.TABLE_NAME, NotificationRestorer.COLUMNS_FOR_RESTORE, "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 0", strArr, null, null, null);
            try {
                NotificationRestorer.showNotifications(context, query, 0);
                if (query != null && !query.isClosed()) {
                    query.close();
                }
            } catch (Throwable th) {
                th = th;
                cursor = query;
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            OneSignal.Log(OneSignal.LOG_LEVEL.ERROR, "Error restoring notification records! ", th);
            if (cursor != null) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0056, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        r0 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0059, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005a, code lost:
        r0 = r11;
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0080, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return r11;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0056 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x001d] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Integer getSummaryNotificationId(android.database.sqlite.SQLiteDatabase r11, java.lang.String r12) {
        /*
            r0 = 0
            java.lang.String r2 = "notification"
            r1 = 1
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x0060 }
            java.lang.String r4 = "android_notification_id"
            r5 = 0
            r3[r5] = r4     // Catch:{ Throwable -> 0x0060 }
            java.lang.String r4 = "group_id = ? AND dismissed = 0 AND opened = 0 AND is_summary = 1"
            java.lang.String[] r6 = new java.lang.String[r1]     // Catch:{ Throwable -> 0x0060 }
            r6[r5] = r12     // Catch:{ Throwable -> 0x0060 }
            r7 = 0
            r8 = 0
            r9 = 0
            r1 = r11
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Throwable -> 0x0060 }
            boolean r1 = r11.moveToFirst()     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            if (r1 != 0) goto L_0x0032
            r11.close()     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            if (r11 == 0) goto L_0x0031
            boolean r12 = r11.isClosed()
            if (r12 != 0) goto L_0x0031
            r11.close()
        L_0x0031:
            return r0
        L_0x0032:
            java.lang.String r1 = "android_notification_id"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            int r1 = r11.getInt(r1)     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            r11.close()     // Catch:{ Throwable -> 0x0050, all -> 0x0056 }
            if (r11 == 0) goto L_0x004e
            boolean r12 = r11.isClosed()
            if (r12 != 0) goto L_0x004e
            r11.close()
        L_0x004e:
            r11 = r1
            goto L_0x0083
        L_0x0050:
            r0 = move-exception
            r10 = r0
            r0 = r11
            r11 = r1
            r1 = r10
            goto L_0x0062
        L_0x0056:
            r12 = move-exception
            r0 = r11
            goto L_0x0084
        L_0x0059:
            r1 = move-exception
            r10 = r0
            r0 = r11
            r11 = r10
            goto L_0x0062
        L_0x005e:
            r12 = move-exception
            goto L_0x0084
        L_0x0060:
            r1 = move-exception
            r11 = r0
        L_0x0062:
            com.onesignal.OneSignal$LOG_LEVEL r2 = com.onesignal.OneSignal.LOG_LEVEL.ERROR     // Catch:{ all -> 0x005e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005e }
            r3.<init>()     // Catch:{ all -> 0x005e }
            java.lang.String r4 = "Error getting android notification id for summary notification group: "
            r3.append(r4)     // Catch:{ all -> 0x005e }
            r3.append(r12)     // Catch:{ all -> 0x005e }
            java.lang.String r12 = r3.toString()     // Catch:{ all -> 0x005e }
            com.onesignal.OneSignal.Log(r2, r12, r1)     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x0083
            boolean r12 = r0.isClosed()
            if (r12 != 0) goto L_0x0083
            r0.close()
        L_0x0083:
            return r11
        L_0x0084:
            if (r0 == 0) goto L_0x008f
            boolean r11 = r0.isClosed()
            if (r11 != 0) goto L_0x008f
            r0.close()
        L_0x008f:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.onesignal.NotificationSummaryManager.getSummaryNotificationId(android.database.sqlite.SQLiteDatabase, java.lang.String):java.lang.Integer");
    }
}
