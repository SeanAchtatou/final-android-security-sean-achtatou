package com.applovin.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.c;
import com.applovin.impl.sdk.utils.e;
import com.applovin.nativeAds.AppLovinNativeAdService;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AppLovinSdk {
    private static final String TAG = "AppLovinSdk";
    public static final String VERSION = getVersion();
    public static final int VERSION_CODE = getVersionCode();
    private static final Map<String, AppLovinSdk> sdkInstances = new HashMap();
    private static final Object sdkInstancesLock = new Object();
    private final j mSdkImpl;

    public interface SdkInitializationListener {
        void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration);
    }

    private static class a extends AppLovinSdkSettings {
        a(Context context) {
            super(context);
        }
    }

    private AppLovinSdk(j jVar) {
        this.mSdkImpl = jVar;
    }

    public static List<AppLovinSdk> a() {
        return new ArrayList(sdkInstances.values());
    }

    public static AppLovinSdk getInstance(Context context) {
        return getInstance(new a(context), context);
    }

    public static AppLovinSdk getInstance(AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (context != null) {
            return getInstance(c.a(context).a("applovin.sdk.key", ""), appLovinSdkSettings, context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public static AppLovinSdk getInstance(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (appLovinSdkSettings == null) {
            throw new IllegalArgumentException("No userSettings specified");
        } else if (context != null) {
            synchronized (sdkInstancesLock) {
                if (sdkInstances.containsKey(str)) {
                    AppLovinSdk appLovinSdk = sdkInstances.get(str);
                    return appLovinSdk;
                }
                if (!TextUtils.isEmpty(str) && str.contains(File.separator)) {
                    p.j(TAG, "\n**************************************************\nINVALID SDK KEY: " + str + "\n**************************************************\n");
                    if (!sdkInstances.isEmpty()) {
                        AppLovinSdk next = sdkInstances.values().iterator().next();
                        return next;
                    }
                    str = str.replace(File.separator, "");
                }
                j jVar = new j();
                jVar.a(str, appLovinSdkSettings, context);
                AppLovinSdk appLovinSdk2 = new AppLovinSdk(jVar);
                jVar.a(appLovinSdk2);
                sdkInstances.put(str, appLovinSdk2);
                return appLovinSdk2;
            }
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    private static String getVersion() {
        return "9.11.1";
    }

    private static int getVersionCode() {
        return 91101;
    }

    public static void initializeSdk(Context context) {
        initializeSdk(context, null);
    }

    public static void initializeSdk(Context context, SdkInitializationListener sdkInitializationListener) {
        if (context != null) {
            AppLovinSdk instance = getInstance(context);
            if (instance != null) {
                instance.initializeSdk(sdkInitializationListener);
            } else {
                p.j(TAG, "Unable to initialize AppLovin SDK: SDK object not created");
            }
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    static void reinitializeAll() {
        reinitializeAll(null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.e.a(java.lang.Object, java.lang.Object):java.util.Map<K, V>
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.applovin.impl.sdk.utils.e.a(java.util.Collection<java.lang.String>, int):java.lang.String
      com.applovin.impl.sdk.utils.e.a(java.lang.String, java.lang.String):java.util.List<java.lang.String>
      com.applovin.impl.sdk.utils.e.a(java.lang.Object, java.lang.Object):java.util.Map<K, V> */
    static void reinitializeAll(Boolean bool, Boolean bool2) {
        synchronized (sdkInstancesLock) {
            for (AppLovinSdk next : sdkInstances.values()) {
                next.mSdkImpl.b();
                if (bool != null) {
                    next.getEventService().trackEvent("huc", e.a((Object) "value", (Object) bool.toString()));
                }
                if (bool2 != null) {
                    next.getEventService().trackEvent("dns", e.a((Object) "value", (Object) bool2.toString()));
                }
            }
        }
    }

    public AppLovinAdService getAdService() {
        return this.mSdkImpl.n();
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public Context getApplicationContext() {
        return this.mSdkImpl.C();
    }

    public AppLovinSdkConfiguration getConfiguration() {
        return this.mSdkImpl.l();
    }

    public AppLovinEventService getEventService() {
        return this.mSdkImpl.p();
    }

    @Deprecated
    public p getLogger() {
        return this.mSdkImpl.u();
    }

    public String getMediationProvider() {
        return this.mSdkImpl.m();
    }

    public AppLovinNativeAdService getNativeAdService() {
        return this.mSdkImpl.o();
    }

    public AppLovinPostbackService getPostbackService() {
        return this.mSdkImpl.Q();
    }

    public String getSdkKey() {
        return this.mSdkImpl.s();
    }

    public AppLovinSdkSettings getSettings() {
        return this.mSdkImpl.k();
    }

    public String getUserIdentifier() {
        return this.mSdkImpl.h();
    }

    public AppLovinUserService getUserService() {
        return this.mSdkImpl.q();
    }

    public AppLovinVariableService getVariableService() {
        return this.mSdkImpl.r();
    }

    public boolean hasCriticalErrors() {
        return this.mSdkImpl.t();
    }

    public void initializeSdk() {
    }

    public void initializeSdk(SdkInitializationListener sdkInitializationListener) {
        this.mSdkImpl.a(sdkInitializationListener);
    }

    public boolean isEnabled() {
        return this.mSdkImpl.d();
    }

    public void setMediationProvider(String str) {
        this.mSdkImpl.c(str);
    }

    public void setPluginVersion(String str) {
        this.mSdkImpl.a(str);
    }

    public void setUserIdentifier(String str) {
        this.mSdkImpl.b(str);
    }

    public void showMediationDebugger() {
        this.mSdkImpl.g();
    }

    public String toString() {
        return "AppLovinSdk{sdkKey='" + getSdkKey() + "', isEnabled=" + isEnabled() + ", isFirstSession=" + this.mSdkImpl.G() + '}';
    }
}
