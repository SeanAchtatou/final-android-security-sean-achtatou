package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.q;
import java.util.HashMap;
import java.util.Map;

public class AppLovinSdkSettings {
    private boolean a;
    private long b;
    private String c;
    private String d;
    private boolean e;
    private final Map<String, Object> localSettings;

    public AppLovinSdkSettings() {
        this(null);
    }

    public AppLovinSdkSettings(Context context) {
        this.localSettings = new HashMap();
        this.a = q.a(context);
        this.b = -1;
        this.c = AppLovinAdSize.INTERSTITIAL.getLabel() + "," + AppLovinAdSize.BANNER.getLabel() + "," + AppLovinAdSize.MREC.getLabel();
        this.d = AppLovinAdType.INCENTIVIZED.getLabel() + "," + AppLovinAdType.REGULAR.getLabel() + "," + AppLovinAdType.NATIVE.getLabel();
    }

    @Deprecated
    public String getAutoPreloadSizes() {
        return this.c;
    }

    @Deprecated
    public String getAutoPreloadTypes() {
        return this.d;
    }

    @Deprecated
    public long getBannerAdRefreshSeconds() {
        return this.b;
    }

    public boolean isMuted() {
        return this.e;
    }

    public boolean isVerboseLoggingEnabled() {
        return this.a;
    }

    @Deprecated
    public void setAutoPreloadSizes(String str) {
        this.c = str;
    }

    @Deprecated
    public void setAutoPreloadTypes(String str) {
        this.d = str;
    }

    @Deprecated
    public void setBannerAdRefreshSeconds(long j) {
        this.b = j;
    }

    public void setMuted(boolean z) {
        this.e = z;
    }

    public void setVerboseLogging(boolean z) {
        if (q.a()) {
            p.j("AppLovinSdkSettings", "Ignoring setting of verbose logging - it is configured from Android manifest already or AppLovinSdkSettings was initialized without a context.");
        } else {
            this.a = z;
        }
    }

    public String toString() {
        return "AppLovinSdkSettings{isVerboseLoggingEnabled=" + this.a + ", muted=" + this.e + '}';
    }
}
