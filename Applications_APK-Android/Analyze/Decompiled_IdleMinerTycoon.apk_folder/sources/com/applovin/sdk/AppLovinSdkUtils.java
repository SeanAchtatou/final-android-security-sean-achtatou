package com.applovin.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.ImageView;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import java.io.File;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppLovinSdkUtils {
    private static final Handler a = new Handler(Looper.getMainLooper());

    private static void a(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof BitmapDrawable) {
                ((BitmapDrawable) drawable).getBitmap().recycle();
            }
        }
    }

    public static int dpToPx(Context context, int i) {
        return (int) TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    public static boolean isTablet(Context context) {
        Point a2 = g.a(context);
        return Math.min(a2.x, a2.y) >= dpToPx(context, 600);
    }

    public static boolean isValidString(String str) {
        return !TextUtils.isEmpty(str);
    }

    public static int pxToDp(Context context, int i) {
        return (int) Math.ceil((double) (((float) i) / context.getResources().getDisplayMetrics().density));
    }

    public static void runOnUiThread(Runnable runnable) {
        runOnUiThread(false, runnable);
    }

    public static void runOnUiThread(boolean z, Runnable runnable) {
        if (z || !q.b()) {
            a.post(runnable);
        } else {
            runnable.run();
        }
    }

    public static void runOnUiThreadDelayed(Runnable runnable, long j) {
        if (j > 0) {
            a.postDelayed(runnable, j);
        } else if (q.b()) {
            runnable.run();
        } else {
            a.post(runnable);
        }
    }

    public static void safePopulateImageView(Context context, ImageView imageView, int i, int i2) {
        a(imageView);
        Bitmap a2 = q.a(context, i, i2);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
    }

    public static void safePopulateImageView(ImageView imageView, Bitmap bitmap) {
        a(imageView);
        if (imageView != null && bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
    }

    public static void safePopulateImageView(ImageView imageView, Uri uri, int i) {
        a(imageView);
        Bitmap a2 = q.a(new File(uri.getPath()), i);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
    }

    public static Map<String, String> toMap(JSONObject jSONObject) throws JSONException {
        return i.a(jSONObject);
    }
}
