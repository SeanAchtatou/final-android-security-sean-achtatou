package com.applovin.communicator;

import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import java.util.Collections;
import java.util.List;

public final class AppLovinCommunicator {
    private static AppLovinCommunicator a;
    private static final Object b = new Object();
    private j c;
    private final a d;
    private final MessagingServiceImpl e;

    private AppLovinCommunicator(Context context) {
        this.d = new a(context);
        this.e = new MessagingServiceImpl(context);
    }

    public static AppLovinCommunicator getInstance(Context context) {
        synchronized (b) {
            if (a == null) {
                a = new AppLovinCommunicator(context.getApplicationContext());
            }
        }
        return a;
    }

    public void a(j jVar) {
        p.g("AppLovinCommunicator", "Attaching SDK instance: " + jVar + "...");
        this.c = jVar;
    }

    public AppLovinCommunicatorMessagingService getMessagingService() {
        return this.e;
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        subscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            p.g("AppLovinCommunicator", "Subscribing " + appLovinCommunicatorSubscriber + " to topic: " + next);
            if (this.d.a(appLovinCommunicatorSubscriber, next)) {
                p.g("AppLovinCommunicator", "Subscribed " + appLovinCommunicatorSubscriber + " to topic: " + next);
                this.e.maybeFlushStickyMessages(next);
            } else {
                p.g("AppLovinCommunicator", "Unable to subscribe " + appLovinCommunicatorSubscriber + " to topic: " + next);
            }
        }
    }

    public String toString() {
        return "AppLovinCommunicator{sdk=" + this.c + '}';
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        unsubscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            p.g("AppLovinCommunicator", "Unsubscribing " + appLovinCommunicatorSubscriber + " from topic: " + next);
            this.d.b(appLovinCommunicatorSubscriber, next);
        }
    }
}
