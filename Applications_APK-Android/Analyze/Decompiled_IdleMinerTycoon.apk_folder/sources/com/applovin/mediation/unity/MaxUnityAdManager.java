package com.applovin.mediation.unity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinVariableService;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.unity3d.player.UnityPlayer;
import java.lang.Thread;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

public class MaxUnityAdManager implements MaxAdListener, MaxAdViewAdListener, MaxRewardedAdListener, AppLovinVariableService.OnVariablesUpdateListener {
    private static final String SERIALIZED_KEY_VALUE_PAIR_SEPARATOR = String.valueOf(29);
    private static final String SERIALIZED_KEY_VALUE_SEPARATOR = String.valueOf(28);
    private static final String TAG = "MaxUnityAdManager";
    private static final String VERSION = "2.3.0";
    private static final ScheduledThreadPoolExecutor sThreadPoolExecutor = new ScheduledThreadPoolExecutor(3, new SdkThreadFactory());
    /* access modifiers changed from: private */
    public final Map<String, String> mAdViewPositions = new HashMap(2);
    /* access modifiers changed from: private */
    public final Map<String, MaxAdView> mAdViews = new HashMap(2);
    private final Map<String, MaxInterstitialAd> mInterstitials = new HashMap(2);
    private final Map<String, MaxRewardedAd> mRewardedAds = new HashMap(2);
    private AppLovinSdk sdk;

    public interface Listener {
        void onSdkInitializationComplete(AppLovinSdkConfiguration appLovinSdkConfiguration);
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
    }

    public AppLovinSdk initializeSdkWithCompletionHandler(String str, final Listener listener) {
        Activity currentActivity = Utils.getCurrentActivity();
        this.sdk = AppLovinSdk.getInstance(str, new AppLovinSdkSettings(currentActivity), currentActivity);
        this.sdk.getVariableService().setOnVariablesUpdateListener(this);
        this.sdk.setPluginVersion("Max-Unity-2.3.0");
        this.sdk.setMediationProvider(AppLovinMediationProvider.MAX);
        this.sdk.initializeSdk(new AppLovinSdk.SdkInitializationListener() {
            public void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration) {
                listener.onSdkInitializationComplete(appLovinSdkConfiguration);
                HashMap hashMap = new HashMap(2);
                hashMap.put("name", "OnSdkInitializedEvent");
                hashMap.put("consentDialogState", Integer.toString(appLovinSdkConfiguration.getConsentDialogState().ordinal()));
                MaxUnityAdManager.forwardUnityEventWithArgs(hashMap);
            }
        });
        return this.sdk;
    }

    public void createBanner(final String str, final String str2) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                Log.d(MaxUnityAdManager.TAG, "Creating banner with ad unit id \"" + str + "\" and position: \"" + str2 + "\"");
                MaxAdView access$200 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str, str2);
                if (access$200 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                    return;
                }
                access$200.setVisibility(8);
                if (access$200.getParent() == null) {
                    Activity currentActivity = Utils.getCurrentActivity();
                    RelativeLayout relativeLayout = new RelativeLayout(currentActivity);
                    currentActivity.addContentView(relativeLayout, new LinearLayout.LayoutParams(-1, -1));
                    relativeLayout.addView(access$200);
                    MaxUnityAdManager.this.positionBanner(str, AppLovinSdkUtils.isTablet(currentActivity) ? MaxAdFormat.LEADER : MaxAdFormat.BANNER);
                }
                access$200.loadAd();
            }
        });
    }

    public void setBannerPlacement(final String str, final String str2) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                Log.d(MaxUnityAdManager.TAG, "Setting placement \"" + str2 + "\" for banner with ad unit id \"" + str + "\"");
                MaxAdView access$400 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str);
                if (access$400 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                } else {
                    access$400.setPlacement(str2);
                }
            }
        });
    }

    public void showBanner(final String str) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                Log.d(MaxUnityAdManager.TAG, "Showing banner with ad unit id \"" + str + "\"");
                MaxAdView access$400 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str);
                if (access$400 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                    return;
                }
                access$400.setVisibility(0);
                access$400.startAutoRefresh();
            }
        });
    }

    public void hideBanner(final String str) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                Log.d(MaxUnityAdManager.TAG, "Hiding banner with ad unit id \"" + str + "\"");
                MaxAdView access$400 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str);
                if (access$400 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                    return;
                }
                access$400.setVisibility(8);
                access$400.stopAutoRefresh();
            }
        });
    }

    public void destroyBanner(final String str) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                Log.d(MaxUnityAdManager.TAG, "Destroying banner with ad unit id \"" + str + "\"");
                MaxAdView access$400 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str);
                if (access$400 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                    return;
                }
                access$400.setListener(null);
                access$400.destroy();
                MaxUnityAdManager.this.mAdViews.remove(str);
                MaxUnityAdManager.this.mAdViewPositions.remove(str);
            }
        });
    }

    public void setBannerBackgroundColor(final String str, final String str2) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                Log.d(MaxUnityAdManager.TAG, "Setting banner with ad unit id \"" + str + "\" to color: " + str2);
                MaxAdView access$400 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str);
                if (access$400 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                    return;
                }
                try {
                    access$400.setBackgroundColor(Color.parseColor(str2));
                } catch (Throwable unused) {
                }
            }
        });
    }

    public void setBannerExtraParameter(final String str, final String str2, final String str3) {
        Utils.runSafelyOnUiThread(new Runnable() {
            public void run() {
                MaxAdFormat maxAdFormat;
                Log.d(MaxUnityAdManager.TAG, "Setting banner extra with key: \"" + str2 + "\" value: " + str3);
                MaxAdView access$400 = MaxUnityAdManager.this.retrieveAdViewForAdUnitId(str);
                if (access$400 == null) {
                    Log.e(MaxUnityAdManager.TAG, "Banner does not exist");
                    return;
                }
                access$400.setExtraParameter(str2, str3);
                if ("force_banner".equalsIgnoreCase(str2)) {
                    if (Boolean.valueOf(str3).booleanValue()) {
                        maxAdFormat = MaxAdFormat.BANNER;
                    } else {
                        maxAdFormat = AppLovinSdkUtils.isTablet(Utils.getCurrentActivity()) ? MaxAdFormat.LEADER : MaxAdFormat.BANNER;
                    }
                    MaxUnityAdManager.this.positionBanner(str, maxAdFormat);
                }
            }
        });
    }

    public void loadInterstitial(String str) {
        retrieveInterstitial(str).loadAd();
    }

    public boolean isInterstitialReady(String str) {
        return retrieveInterstitial(str).isReady();
    }

    public void showInterstitial(String str, String str2) {
        retrieveInterstitial(str).showAd(str2);
    }

    public void setInterstitialExtraParameter(String str, String str2, String str3) {
        retrieveInterstitial(str).setExtraParameter(str2, str3);
    }

    public void loadRewardedAd(String str) {
        retrieveRewardedAd(str).loadAd();
    }

    public boolean isRewardedAdReady(String str) {
        return retrieveRewardedAd(str).isReady();
    }

    public void showRewardedAd(String str, String str2) {
        retrieveRewardedAd(str).showAd(str2);
    }

    public void setRewardedAdExtraParameter(String str, String str2, String str3) {
        retrieveRewardedAd(str).setExtraParameter(str2, str3);
    }

    public void trackEvent(String str, String str2) {
        if (this.sdk != null) {
            this.sdk.getEventService().trackEvent(str, deserializeEventParameters(str2));
        }
    }

    public void loadVariables() {
        this.sdk.getVariableService().loadVariables();
    }

    public void onVariablesUpdate(Bundle bundle) {
        HashMap hashMap = new HashMap(1);
        hashMap.put("name", "OnVariablesUpdatedEvent");
        forwardUnityEventWithArgs(hashMap);
    }

    public void onAdLoaded(MaxAd maxAd) {
        String str;
        if (maxAd.getFormat() == MaxAdFormat.BANNER || maxAd.getFormat() == MaxAdFormat.LEADER) {
            str = "OnBannerAdLoadedEvent";
            positionBanner(maxAd);
            MaxAdView retrieveAdViewForAdUnitId = retrieveAdViewForAdUnitId(maxAd.getAdUnitId());
            if (!(retrieveAdViewForAdUnitId == null || retrieveAdViewForAdUnitId.getVisibility() == 0)) {
                retrieveAdViewForAdUnitId.stopAutoRefresh();
            }
        } else if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL) {
            str = "OnInterstitialLoadedEvent";
        } else if (maxAd.getFormat() == MaxAdFormat.REWARDED) {
            str = "OnRewardedAdLoadedEvent";
        } else {
            logInvalidAdFormat(maxAd.getFormat());
            return;
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("name", str);
        hashMap.put("adUnitId", maxAd.getAdUnitId());
        forwardUnityEventWithArgs(hashMap);
    }

    public void onAdLoadFailed(String str, int i) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            logStackTrace(new IllegalArgumentException("adUnitId cannot be null"));
            return;
        }
        if (this.mAdViews.containsKey(str)) {
            str2 = "OnBannerAdLoadFailedEvent";
        } else if (this.mInterstitials.containsKey(str)) {
            str2 = "OnInterstitialLoadFailedEvent";
        } else if (this.mRewardedAds.containsKey(str)) {
            str2 = "OnRewardedAdLoadFailedEvent";
        } else {
            logStackTrace(new IllegalStateException("invalid adUnitId: " + str));
            return;
        }
        HashMap hashMap = new HashMap(3);
        hashMap.put("name", str2);
        hashMap.put("adUnitId", str);
        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, Integer.toString(i));
        forwardUnityEventWithArgs(hashMap);
    }

    public void onAdClicked(MaxAd maxAd) {
        String str;
        if (maxAd.getFormat() == MaxAdFormat.BANNER || maxAd.getFormat() == MaxAdFormat.LEADER) {
            str = "OnBannerAdClickedEvent";
        } else if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL) {
            str = "OnInterstitialClickedEvent";
        } else if (maxAd.getFormat() == MaxAdFormat.REWARDED) {
            str = "OnRewardedAdClickedEvent";
        } else {
            logInvalidAdFormat(maxAd.getFormat());
            return;
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("name", str);
        hashMap.put("adUnitId", maxAd.getAdUnitId());
        forwardUnityEventWithArgs(hashMap);
    }

    public void onAdDisplayed(MaxAd maxAd) {
        if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
            String str = maxAd.getFormat() == MaxAdFormat.INTERSTITIAL ? "OnInterstitialDisplayedEvent" : "OnRewardedAdDisplayedEvent";
            HashMap hashMap = new HashMap(2);
            hashMap.put("name", str);
            hashMap.put("adUnitId", maxAd.getAdUnitId());
            forwardUnityEventWithArgs(hashMap);
        }
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i) {
        if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
            String str = maxAd.getFormat() == MaxAdFormat.INTERSTITIAL ? "OnInterstitialAdFailedToDisplayEvent" : "OnRewardedAdFailedToDisplayEvent";
            HashMap hashMap = new HashMap(3);
            hashMap.put("name", str);
            hashMap.put("adUnitId", maxAd.getAdUnitId());
            hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, Integer.toString(i));
            forwardUnityEventWithArgs(hashMap);
        }
    }

    public void onAdHidden(MaxAd maxAd) {
        if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
            String str = maxAd.getFormat() == MaxAdFormat.INTERSTITIAL ? "OnInterstitialHiddenEvent" : "OnRewardedAdHiddenEvent";
            HashMap hashMap = new HashMap(2);
            hashMap.put("name", str);
            hashMap.put("adUnitId", maxAd.getAdUnitId());
            forwardUnityEventWithArgs(hashMap);
        }
    }

    public void onAdCollapsed(MaxAd maxAd) {
        if (maxAd.getFormat() == MaxAdFormat.BANNER || maxAd.getFormat() == MaxAdFormat.LEADER) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("name", "OnBannerAdCollapsedEvent");
            hashMap.put("adUnitId", maxAd.getAdUnitId());
            forwardUnityEventWithArgs(hashMap);
            return;
        }
        logInvalidAdFormat(maxAd.getFormat());
    }

    public void onAdExpanded(MaxAd maxAd) {
        if (maxAd.getFormat() == MaxAdFormat.BANNER || maxAd.getFormat() == MaxAdFormat.LEADER) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("name", "OnBannerAdExpandedEvent");
            hashMap.put("adUnitId", maxAd.getAdUnitId());
            forwardUnityEventWithArgs(hashMap);
            return;
        }
        logInvalidAdFormat(maxAd.getFormat());
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
        if (maxAd.getFormat() != MaxAdFormat.REWARDED) {
            logInvalidAdFormat(maxAd.getFormat());
            return;
        }
        String label = maxReward != null ? maxReward.getLabel() : "";
        String num = Integer.toString(maxReward != null ? maxReward.getAmount() : 0);
        HashMap hashMap = new HashMap(4);
        hashMap.put("name", "OnRewardedAdReceivedRewardEvent");
        hashMap.put("adUnitId", maxAd.getAdUnitId());
        hashMap.put("rewardLabel", label);
        hashMap.put(IronSourceConstants.EVENTS_REWARD_AMOUNT, num);
        forwardUnityEventWithArgs(hashMap);
    }

    private void logInvalidAdFormat(MaxAdFormat maxAdFormat) {
        logStackTrace(new IllegalStateException("invalid ad format: " + maxAdFormat));
    }

    private void logStackTrace(Exception exc) {
        Log.e(TAG, Log.getStackTraceString(exc));
    }

    private MaxInterstitialAd retrieveInterstitial(String str) {
        MaxInterstitialAd maxInterstitialAd = this.mInterstitials.get(str);
        if (maxInterstitialAd != null) {
            return maxInterstitialAd;
        }
        MaxInterstitialAd maxInterstitialAd2 = new MaxInterstitialAd(str, this.sdk, Utils.getCurrentActivity());
        maxInterstitialAd2.setListener(this);
        this.mInterstitials.put(str, maxInterstitialAd2);
        return maxInterstitialAd2;
    }

    private MaxRewardedAd retrieveRewardedAd(String str) {
        MaxRewardedAd maxRewardedAd = this.mRewardedAds.get(str);
        if (maxRewardedAd != null) {
            return maxRewardedAd;
        }
        MaxRewardedAd instance = MaxRewardedAd.getInstance(str, this.sdk, Utils.getCurrentActivity());
        instance.setListener(this);
        this.mRewardedAds.put(str, instance);
        return instance;
    }

    /* access modifiers changed from: private */
    public MaxAdView retrieveAdViewForAdUnitId(String str) {
        return retrieveAdViewForAdUnitId(str, null);
    }

    /* access modifiers changed from: private */
    public MaxAdView retrieveAdViewForAdUnitId(String str, String str2) {
        MaxAdView maxAdView = this.mAdViews.get(str);
        if (maxAdView != null || str2 == null) {
            return maxAdView;
        }
        MaxAdView maxAdView2 = new MaxAdView(str, this.sdk, Utils.getCurrentActivity());
        maxAdView2.setListener(this);
        this.mAdViews.put(str, maxAdView2);
        this.mAdViewPositions.put(str, str2);
        return maxAdView2;
    }

    private void positionBanner(MaxAd maxAd) {
        positionBanner(maxAd.getAdUnitId(), maxAd.getFormat());
    }

    /* access modifiers changed from: private */
    public void positionBanner(String str, MaxAdFormat maxAdFormat) {
        int i;
        MaxAdView retrieveAdViewForAdUnitId = retrieveAdViewForAdUnitId(str);
        if (retrieveAdViewForAdUnitId == null) {
            Log.e(TAG, "Banner does not exist");
            return;
        }
        String str2 = this.mAdViewPositions.get(str);
        RelativeLayout relativeLayout = (RelativeLayout) retrieveAdViewForAdUnitId.getParent();
        BannerSize bannerSize = getBannerSize(maxAdFormat);
        int dpToPx = AppLovinSdkUtils.dpToPx(Utils.getCurrentActivity(), bannerSize.mWidthDp);
        int dpToPx2 = AppLovinSdkUtils.dpToPx(Utils.getCurrentActivity(), bannerSize.mHeightDp);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) retrieveAdViewForAdUnitId.getLayoutParams();
        layoutParams.height = dpToPx2;
        retrieveAdViewForAdUnitId.setLayoutParams(layoutParams);
        int i2 = 0;
        if ("Centered".equalsIgnoreCase(str2)) {
            i = 17;
        } else {
            if (str2.contains("Top")) {
                i2 = 48;
            } else if (str2.contains("Bottom")) {
                i2 = 80;
            }
            if (str2.contains("Center")) {
                i = i2 | 1;
                layoutParams.width = -1;
            } else {
                layoutParams.width = dpToPx;
                if (str2.contains("Left")) {
                    i = i2 | 3;
                } else {
                    i = str2.contains("Right") ? i2 | 5 : i2;
                }
            }
        }
        relativeLayout.setGravity(i);
    }

    /* access modifiers changed from: private */
    public static void forwardUnityEventWithArgs(final Map<String, String> map) {
        sThreadPoolExecutor.execute(new Runnable() {
            public void run() {
                UnityPlayer.UnitySendMessage("MaxSdkCallbacks", "ForwardEvent", MaxUnityAdManager.propsStrFromDictionary(map));
            }
        });
    }

    /* access modifiers changed from: private */
    public static String propsStrFromDictionary(Map<String, String> map) {
        StringBuilder sb = new StringBuilder(64);
        for (Map.Entry next : map.entrySet()) {
            sb.append((String) next.getKey());
            sb.append(Constants.RequestParameters.EQUAL);
            sb.append((String) next.getValue());
            sb.append("\n");
        }
        return sb.toString();
    }

    private static Map<String, String> deserializeEventParameters(String str) {
        if (TextUtils.isEmpty(str)) {
            return Collections.emptyMap();
        }
        String[] split = str.split(SERIALIZED_KEY_VALUE_PAIR_SEPARATOR);
        HashMap hashMap = new HashMap(split.length);
        for (String split2 : split) {
            String[] split3 = split2.split(SERIALIZED_KEY_VALUE_SEPARATOR);
            if (split3.length == 2) {
                hashMap.put(split3[0], split3[1]);
            }
        }
        return hashMap;
    }

    private static class BannerSize {
        /* access modifiers changed from: private */
        public final int mHeightDp;
        /* access modifiers changed from: private */
        public final int mWidthDp;

        private BannerSize(int i, int i2) {
            this.mWidthDp = i;
            this.mHeightDp = i2;
        }
    }

    private BannerSize getBannerSize(MaxAdFormat maxAdFormat) {
        if (MaxAdFormat.LEADER.equals(maxAdFormat)) {
            return new BannerSize(728, 90);
        }
        return new BannerSize(320, 50);
    }

    private static class SdkThreadFactory implements ThreadFactory {
        private SdkThreadFactory() {
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "AppLovinSdk:Max-Unity-Plugin:shared");
            thread.setDaemon(true);
            thread.setPriority(10);
            thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    Log.e(MaxUnityAdManager.TAG, "Caught unhandled exception", th);
                }
            });
            return thread;
        }
    }
}
