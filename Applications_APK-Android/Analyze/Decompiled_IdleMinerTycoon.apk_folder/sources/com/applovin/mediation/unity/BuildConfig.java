package com.applovin.mediation.unity;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.applovin.mediation.unity";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "standalone";
    public static final int VERSION_CODE = 20300;
    public static final String VERSION_NAME = "2.3.0";
}
