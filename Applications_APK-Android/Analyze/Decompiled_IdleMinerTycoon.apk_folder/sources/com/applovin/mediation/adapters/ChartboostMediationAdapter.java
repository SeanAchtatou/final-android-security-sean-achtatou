package com.applovin.mediation.adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.sdk.AppLovinSdk;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import java.util.concurrent.atomic.AtomicBoolean;

public class ChartboostMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter {
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    private static final ChartboostMediationAdapterRouter ROUTER;
    private static final Application.ActivityLifecycleCallbacks sActivityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            if (ChartboostMediationAdapter.isApplicationActivity(activity)) {
                Chartboost.onCreate(activity);
            }
        }

        public void onActivityStarted(Activity activity) {
            if (ChartboostMediationAdapter.isApplicationActivity(activity)) {
                Chartboost.onStart(activity);
            }
        }

        public void onActivityResumed(Activity activity) {
            if (ChartboostMediationAdapter.isApplicationActivity(activity)) {
                Chartboost.onResume(activity);
            }
        }

        public void onActivityPaused(Activity activity) {
            if (ChartboostMediationAdapter.isApplicationActivity(activity)) {
                Chartboost.onPause(activity);
            }
        }

        public void onActivityStopped(Activity activity) {
            if (ChartboostMediationAdapter.isApplicationActivity(activity)) {
                Chartboost.onStop(activity);
            }
        }

        public void onActivityDestroyed(Activity activity) {
            if (ChartboostMediationAdapter.isApplicationActivity(activity)) {
                Chartboost.onDestroy(activity);
            }
        }
    };
    /* access modifiers changed from: private */
    public static MaxAdapter.InitializationStatus sStatus;
    private String mLocation;

    public String getAdapterVersion() {
        return "7.5.0.2";
    }

    static {
        if (AppLovinSdk.VERSION_CODE >= 90802) {
            ROUTER = (ChartboostMediationAdapterRouter) MediationAdapterRouter.getSharedInstance(ChartboostMediationAdapterRouter.class);
        } else {
            ROUTER = new ChartboostMediationAdapterRouter();
        }
    }

    public static boolean isApplicationActivity(Activity activity) {
        return !activity.getClass().getName().startsWith("com.chartboost.sdk");
    }

    public ChartboostMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        checkExistence(Chartboost.class);
        if (INITIALIZED.compareAndSet(false, true)) {
            if (AppLovinSdk.VERSION_CODE >= 90800) {
                sStatus = MaxAdapter.InitializationStatus.INITIALIZING;
            }
            log("Initializing Chartboost SDK...");
            ROUTER.setOnCompletionListener(onCompletionListener);
            ROUTER.updateConsentStatus(maxAdapterInitializationParameters, activity.getApplicationContext());
            Bundle serverParameters = maxAdapterInitializationParameters.getServerParameters();
            Chartboost.startWithAppId(activity, serverParameters.getString("app_id"), serverParameters.getString("app_signature"));
            Chartboost.setDelegate(ROUTER.getDelegate());
            Chartboost.setMediation(Chartboost.CBMediation.CBMediationOther, AppLovinSdk.VERSION);
            Chartboost.setAutoCacheAds(serverParameters.getBoolean("auto_cache_ads"));
            if (maxAdapterInitializationParameters.isTesting()) {
                Chartboost.setLoggingLevel(CBLogging.Level.ALL);
            }
            if (serverParameters.containsKey("prefetch_video_content")) {
                Chartboost.setShouldPrefetchVideoContent(serverParameters.getBoolean("prefetch_video_content"));
            }
            Chartboost.onCreate(activity);
            Chartboost.onStart(activity);
            Chartboost.onResume(activity);
            activity.getApplication().registerActivityLifecycleCallbacks(sActivityLifecycleCallbacks);
        } else {
            log("Chartboost SDK already initialized...");
            if (AppLovinSdk.VERSION_CODE >= 90800) {
                onCompletionListener.onCompletion(sStatus, null);
            }
        }
        if (AppLovinSdk.VERSION_CODE < 90800) {
            onCompletionListener.onCompletion();
        }
    }

    public String getSdkVersion() {
        return Chartboost.getSDKVersion();
    }

    public void onDestroy() {
        ROUTER.removeAdapter(this, this.mLocation);
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        this.mLocation = retrieveLocation(maxAdapterResponseParameters);
        log("Loading interstitial ad for location \"" + this.mLocation + "\"...");
        ROUTER.updateConsentStatus(maxAdapterResponseParameters, activity.getApplicationContext());
        ROUTER.addInterstitialAdapter(this, maxInterstitialAdapterListener, this.mLocation);
        if (Chartboost.hasInterstitial(this.mLocation)) {
            log("Ad is available already");
            ROUTER.onAdLoaded(this.mLocation);
            return;
        }
        Chartboost.cacheInterstitial(this.mLocation);
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        log("Showing interstitial ad for location \"" + this.mLocation + "\"...");
        updateShowConfigurations(maxAdapterResponseParameters);
        if (Chartboost.hasInterstitial(this.mLocation)) {
            ROUTER.addShowingAdapter(this);
            Chartboost.showInterstitial(this.mLocation);
            return;
        }
        log("Interstitial ad not ready");
        ROUTER.onAdDisplayFailed(this.mLocation, MaxAdapterError.AD_NOT_READY);
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        this.mLocation = retrieveLocation(maxAdapterResponseParameters);
        log("Loading rewarded ad for location \"" + this.mLocation + "\"...");
        ROUTER.updateConsentStatus(maxAdapterResponseParameters, activity.getApplicationContext());
        ROUTER.addRewardedAdapter(this, maxRewardedAdapterListener, this.mLocation);
        if (Chartboost.hasRewardedVideo(this.mLocation)) {
            log("Ad is available already");
            ROUTER.onAdLoaded(this.mLocation);
            return;
        }
        Chartboost.cacheRewardedVideo(this.mLocation);
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        log("Showing rewarded ad for location \"" + this.mLocation + "\"...");
        updateShowConfigurations(maxAdapterResponseParameters);
        if (Chartboost.hasRewardedVideo(this.mLocation)) {
            configureReward(maxAdapterResponseParameters);
            ROUTER.addShowingAdapter(this);
            Chartboost.showRewardedVideo(this.mLocation);
            return;
        }
        log("Rewarded ad not ready");
        maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    private String retrieveLocation(MaxAdapterResponseParameters maxAdapterResponseParameters) {
        return !TextUtils.isEmpty(maxAdapterResponseParameters.getThirdPartyAdPlacementId()) ? maxAdapterResponseParameters.getThirdPartyAdPlacementId() : CBLocation.LOCATION_DEFAULT;
    }

    private void updateShowConfigurations(MaxAdapterResponseParameters maxAdapterResponseParameters) {
        Bundle serverParameters = maxAdapterResponseParameters.getServerParameters();
        if (serverParameters.containsKey("hide_system_ui")) {
            Chartboost.setShouldHideSystemUI(Boolean.valueOf(serverParameters.getBoolean("hide_system_ui")));
        }
        if (serverParameters.containsKey("enable_activity_callbacks")) {
            Chartboost.setActivityCallbacks(serverParameters.getBoolean("enable_activity_callbacks"));
        }
    }

    private static class ChartboostMediationAdapterRouter extends MediationAdapterRouter {
        private final ChartboostDelegate chartboostDelegate;
        /* access modifiers changed from: private */
        public boolean hasGrantedReward;
        /* access modifiers changed from: private */
        public final AtomicBoolean isShowingAd;
        /* access modifiers changed from: private */
        public MaxAdapter.OnCompletionListener onCompletionListener;

        /* access modifiers changed from: package-private */
        public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener2) {
        }

        private ChartboostMediationAdapterRouter() {
            this.isShowingAd = new AtomicBoolean();
            this.chartboostDelegate = new ChartboostDelegate() {
                public void didInitialize() {
                    ChartboostMediationAdapterRouter.this.log("Chartboost SDK initialized");
                    if (AppLovinSdk.VERSION_CODE >= 90800) {
                        MaxAdapter.InitializationStatus unused = ChartboostMediationAdapter.sStatus = MaxAdapter.InitializationStatus.INITIALIZED_UNKNOWN;
                    }
                    if (ChartboostMediationAdapterRouter.this.onCompletionListener != null) {
                        if (AppLovinSdk.VERSION_CODE >= 90800) {
                            ChartboostMediationAdapterRouter.this.onCompletionListener.onCompletion(ChartboostMediationAdapter.sStatus, null);
                        }
                        MaxAdapter.OnCompletionListener unused2 = ChartboostMediationAdapterRouter.this.onCompletionListener = null;
                    }
                }

                public void didCacheInterstitial(String str) {
                    ChartboostMediationAdapterRouter.this.log("Interstitial loaded");
                    ChartboostMediationAdapterRouter.this.onAdLoaded(str);
                }

                public void didFailToLoadInterstitial(String str, CBError.CBImpressionError cBImpressionError) {
                    MaxAdapterError access$300 = ChartboostMediationAdapterRouter.this.toMaxError(cBImpressionError);
                    if (ChartboostMediationAdapterRouter.this.isShowingAd.compareAndSet(true, false)) {
                        ChartboostMediationAdapterRouter chartboostMediationAdapterRouter = ChartboostMediationAdapterRouter.this;
                        chartboostMediationAdapterRouter.log("Interstitial failed to show with error: " + cBImpressionError);
                        ChartboostMediationAdapterRouter.this.onAdDisplayFailed(str, access$300);
                        return;
                    }
                    ChartboostMediationAdapterRouter chartboostMediationAdapterRouter2 = ChartboostMediationAdapterRouter.this;
                    chartboostMediationAdapterRouter2.log("Interstitial failed to load with error: " + cBImpressionError);
                    ChartboostMediationAdapterRouter.this.onAdLoadFailed(str, access$300);
                }

                public void didDisplayInterstitial(String str) {
                    ChartboostMediationAdapterRouter.this.log("Interstitial shown");
                    ChartboostMediationAdapterRouter.this.onAdDisplayed(str);
                }

                public void didClickInterstitial(String str) {
                    ChartboostMediationAdapterRouter.this.log("Interstitial clicked");
                    ChartboostMediationAdapterRouter.this.onAdClicked(str);
                }

                public void didDismissInterstitial(String str) {
                    ChartboostMediationAdapterRouter.this.isShowingAd.set(false);
                    ChartboostMediationAdapterRouter.this.log("Interstitial hidden");
                    ChartboostMediationAdapterRouter.this.onAdHidden(str);
                }

                public void didCacheRewardedVideo(String str) {
                    ChartboostMediationAdapterRouter.this.log("Rewarded ad loaded");
                    ChartboostMediationAdapterRouter.this.onAdLoaded(str);
                }

                public void didFailToLoadRewardedVideo(String str, CBError.CBImpressionError cBImpressionError) {
                    MaxAdapterError access$300 = ChartboostMediationAdapterRouter.this.toMaxError(cBImpressionError);
                    if (ChartboostMediationAdapterRouter.this.isShowingAd.compareAndSet(true, false)) {
                        ChartboostMediationAdapterRouter chartboostMediationAdapterRouter = ChartboostMediationAdapterRouter.this;
                        chartboostMediationAdapterRouter.log("Rewarded ad failed to show with error: " + cBImpressionError);
                        ChartboostMediationAdapterRouter.this.onAdDisplayFailed(str, access$300);
                        return;
                    }
                    ChartboostMediationAdapterRouter chartboostMediationAdapterRouter2 = ChartboostMediationAdapterRouter.this;
                    chartboostMediationAdapterRouter2.log("Rewarded ad failed to load with error: " + cBImpressionError);
                    ChartboostMediationAdapterRouter.this.onAdLoadFailed(str, access$300);
                }

                public void didDisplayRewardedVideo(String str) {
                    ChartboostMediationAdapterRouter.this.log("Rewarded ad shown");
                    ChartboostMediationAdapterRouter.this.onAdDisplayed(str);
                    ChartboostMediationAdapterRouter.this.onRewardedAdVideoStarted(str);
                }

                public void didClickRewardedVideo(String str) {
                    ChartboostMediationAdapterRouter.this.log("Rewarded ad clicked");
                    ChartboostMediationAdapterRouter.this.onAdClicked(str);
                }

                public void didDismissRewardedVideo(String str) {
                    ChartboostMediationAdapterRouter.this.isShowingAd.set(false);
                    if (ChartboostMediationAdapterRouter.this.hasGrantedReward || ChartboostMediationAdapterRouter.this.shouldAlwaysRewardUser(str)) {
                        MaxReward reward = ChartboostMediationAdapterRouter.this.getReward(str);
                        ChartboostMediationAdapterRouter chartboostMediationAdapterRouter = ChartboostMediationAdapterRouter.this;
                        chartboostMediationAdapterRouter.log("Rewarded ad user with reward: " + reward);
                        ChartboostMediationAdapterRouter.this.onUserRewarded(str, reward);
                        boolean unused = ChartboostMediationAdapterRouter.this.hasGrantedReward = false;
                    }
                    ChartboostMediationAdapterRouter.this.log("Rewarded ad hidden");
                    ChartboostMediationAdapterRouter.this.onAdHidden(str);
                }

                public void didCompleteRewardedVideo(String str, int i) {
                    ChartboostMediationAdapterRouter.this.log("Rewarded ad video completed");
                    ChartboostMediationAdapterRouter.this.onRewardedAdVideoCompleted(str);
                    boolean unused = ChartboostMediationAdapterRouter.this.hasGrantedReward = true;
                }

                public void didFailToRecordClick(String str, CBError.CBClickError cBClickError) {
                    ChartboostMediationAdapterRouter chartboostMediationAdapterRouter = ChartboostMediationAdapterRouter.this;
                    chartboostMediationAdapterRouter.log("Failed to record click at \"" + str + "\" because of error: " + cBClickError);
                }
            };
        }

        /* access modifiers changed from: package-private */
        public ChartboostDelegate getDelegate() {
            return this.chartboostDelegate;
        }

        /* access modifiers changed from: package-private */
        public void setOnCompletionListener(MaxAdapter.OnCompletionListener onCompletionListener2) {
            this.onCompletionListener = onCompletionListener2;
        }

        public void addShowingAdapter(MaxAdapter maxAdapter) {
            super.addShowingAdapter(maxAdapter);
            this.isShowingAd.set(true);
        }

        /* access modifiers changed from: package-private */
        public void updateConsentStatus(MaxAdapterParameters maxAdapterParameters, Context context) {
            Chartboost.setPIDataUseConsent(context, maxAdapterParameters.hasUserConsent() ? Chartboost.CBPIDataUseConsent.YES_BEHAVIORAL : Chartboost.CBPIDataUseConsent.NO_BEHAVIORAL);
        }

        /* access modifiers changed from: private */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x006f, code lost:
            if (r6 == com.chartboost.sdk.Model.CBError.CBImpressionError.NO_HOST_ACTIVITY) goto L_0x000c;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.applovin.mediation.adapter.MaxAdapterError toMaxError(com.chartboost.sdk.Model.CBError.CBImpressionError r6) {
            /*
                r5 = this;
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.INTERNAL
                r1 = -5202(0xffffffffffffebae, float:NaN)
                r2 = -5200(0xffffffffffffebb0, float:NaN)
                r3 = -5207(0xffffffffffffeba9, float:NaN)
                r4 = -5209(0xffffffffffffeba7, float:NaN)
                if (r6 != r0) goto L_0x0010
            L_0x000c:
                r1 = -5209(0xffffffffffffeba7, float:NaN)
                goto L_0x0074
            L_0x0010:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.INTERNET_UNAVAILABLE
                if (r6 != r0) goto L_0x0018
            L_0x0014:
                r1 = -5207(0xffffffffffffeba9, float:NaN)
                goto L_0x0074
            L_0x0018:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.TOO_MANY_CONNECTIONS
                if (r6 != r0) goto L_0x001d
                goto L_0x000c
            L_0x001d:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.WRONG_ORIENTATION
                if (r6 != r0) goto L_0x0022
                goto L_0x0074
            L_0x0022:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED
                if (r6 != r0) goto L_0x0027
                goto L_0x0074
            L_0x0027:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.NETWORK_FAILURE
                if (r6 != r0) goto L_0x002c
                goto L_0x0014
            L_0x002c:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.NO_AD_FOUND
                if (r6 != r0) goto L_0x0033
                r1 = 204(0xcc, float:2.86E-43)
                goto L_0x0074
            L_0x0033:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.SESSION_NOT_STARTED
                if (r6 != r0) goto L_0x003a
                r1 = -5204(0xffffffffffffebac, float:NaN)
                goto L_0x0074
            L_0x003a:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE
                if (r6 != r0) goto L_0x0041
                r1 = -5201(0xffffffffffffebaf, float:NaN)
                goto L_0x0074
            L_0x0041:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.USER_CANCELLATION
                if (r6 != r0) goto L_0x0048
            L_0x0045:
                r1 = -5200(0xffffffffffffebb0, float:NaN)
                goto L_0x0074
            L_0x0048:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.INVALID_LOCATION
                if (r6 != r0) goto L_0x004d
                goto L_0x0045
            L_0x004d:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.ASSETS_DOWNLOAD_FAILURE
                if (r6 != r0) goto L_0x0054
                r1 = -5203(0xffffffffffffebad, float:NaN)
                goto L_0x0074
            L_0x0054:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.ASSET_PREFETCH_IN_PROGRESS
                if (r6 != r0) goto L_0x005b
                r1 = -5205(0xffffffffffffebab, float:NaN)
                goto L_0x0074
            L_0x005b:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.ERROR_LOADING_WEB_VIEW
                if (r6 == r0) goto L_0x0072
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.WEB_VIEW_PAGE_LOAD_TIMEOUT
                if (r6 == r0) goto L_0x0072
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.WEB_VIEW_CLIENT_RECEIVED_ERROR
                if (r6 != r0) goto L_0x0068
                goto L_0x0072
            L_0x0068:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.INTERNET_UNAVAILABLE_AT_SHOW
                if (r6 != r0) goto L_0x006d
                goto L_0x0014
            L_0x006d:
                com.chartboost.sdk.Model.CBError$CBImpressionError r0 = com.chartboost.sdk.Model.CBError.CBImpressionError.NO_HOST_ACTIVITY
                if (r6 != r0) goto L_0x0045
                goto L_0x000c
            L_0x0072:
                r1 = -5212(0xffffffffffffeba4, float:NaN)
            L_0x0074:
                com.applovin.mediation.adapter.MaxAdapterError r0 = new com.applovin.mediation.adapter.MaxAdapterError
                int r6 = r6.ordinal()
                r0.<init>(r1, r6)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.applovin.mediation.adapters.ChartboostMediationAdapter.ChartboostMediationAdapterRouter.toMaxError(com.chartboost.sdk.Model.CBError$CBImpressionError):com.applovin.mediation.adapter.MaxAdapterError");
        }
    }
}
