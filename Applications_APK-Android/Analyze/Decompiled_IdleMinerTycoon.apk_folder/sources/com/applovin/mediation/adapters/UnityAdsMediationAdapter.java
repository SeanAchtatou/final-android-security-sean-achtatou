package com.applovin.mediation.adapters;

import android.app.Activity;
import android.content.Context;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.sdk.AppLovinSdk;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.mediation.IUnityAdsExtendedListener;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.MetaData;
import com.unity3d.services.banners.BannerErrorCode;
import com.unity3d.services.banners.BannerErrorInfo;
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.UnityBannerSize;
import java.util.concurrent.atomic.AtomicBoolean;

public class UnityAdsMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter, MaxAdViewAdapter {
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    private static final String KEY_ENABLE_PER_PLACEMENT_LOAD = "enable_per_placement_load";
    private static final String KEY_GAME_ID = "game_id";
    private static final String KEY_SET_MEDIATION_IDENTIFIER = "set_mediation_identifier";
    private static final UnityMediationAdapterRouter ROUTER;
    private static boolean sEnablePerPlacementLoad = false;
    private BannerView bannerView;
    private String placementId;

    public String getAdapterVersion() {
        return "3.4.0.1";
    }

    static {
        if (AppLovinSdk.VERSION_CODE >= 90802) {
            ROUTER = (UnityMediationAdapterRouter) MediationAdapterRouter.getSharedInstance(UnityMediationAdapterRouter.class);
        } else {
            ROUTER = new UnityMediationAdapterRouter();
        }
    }

    public UnityAdsMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        ROUTER.updatePrivacyConsent(maxAdapterInitializationParameters, activity.getApplicationContext());
        if (INITIALIZED.compareAndSet(false, true)) {
            log("Initializing UnityAds SDK...");
            if (maxAdapterInitializationParameters.getServerParameters().getBoolean(KEY_SET_MEDIATION_IDENTIFIER)) {
                MediationMetaData mediationMetaData = new MediationMetaData(activity);
                mediationMetaData.setName(mediationTag());
                mediationMetaData.setVersion(AppLovinSdk.VERSION);
                mediationMetaData.commit();
            }
            UnityAds.setDebugMode(maxAdapterInitializationParameters.isTesting());
            String string = maxAdapterInitializationParameters.getServerParameters().getString(KEY_GAME_ID, null);
            sEnablePerPlacementLoad = maxAdapterInitializationParameters.getServerParameters().getBoolean(KEY_ENABLE_PER_PLACEMENT_LOAD);
            UnityAds.initialize(activity, string, ROUTER, maxAdapterInitializationParameters.isTesting(), sEnablePerPlacementLoad);
            log("UnityAds SDK initialized");
            if (AppLovinSdk.VERSION_CODE >= 90800) {
                onCompletionListener.onCompletion(MaxAdapter.InitializationStatus.INITIALIZED_UNKNOWN, null);
            } else {
                onCompletionListener.onCompletion();
            }
        } else if (AppLovinSdk.VERSION_CODE < 90800) {
            log("UnityAds SDK already initialized");
            onCompletionListener.onCompletion();
        } else if (UnityAds.isInitialized()) {
            log("UnityAds SDK already initialized");
            onCompletionListener.onCompletion(MaxAdapter.InitializationStatus.INITIALIZED_UNKNOWN, null);
        } else {
            log("UnityAds SDK still initializing");
            onCompletionListener.onCompletion(MaxAdapter.InitializationStatus.INITIALIZING, null);
        }
    }

    public String getSdkVersion() {
        return UnityAds.getVersion();
    }

    public void onDestroy() {
        ROUTER.removeAdapter(this, this.placementId);
        if (this.bannerView != null) {
            this.bannerView.destroy();
            this.bannerView = null;
        }
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        log("Loading interstitial ad...");
        this.placementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        ROUTER.updatePrivacyConsent(maxAdapterResponseParameters, activity.getApplicationContext());
        ROUTER.addInterstitialAdapter(this, maxInterstitialAdapterListener, this.placementId);
        if (sEnablePerPlacementLoad) {
            UnityAds.load(this.placementId);
        }
        if (UnityAds.isReady(this.placementId)) {
            ROUTER.onAdLoaded(this.placementId);
        } else if (!sEnablePerPlacementLoad) {
            ROUTER.onAdLoadFailed(this.placementId, getLoadError(this.placementId));
        }
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        log("Showing interstitial ad...");
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        if (UnityAds.isReady(thirdPartyAdPlacementId)) {
            ROUTER.addShowingAdapter(this);
            UnityAds.show(activity, thirdPartyAdPlacementId);
            return;
        }
        log("Interstitial ad not ready");
        maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        log("Loading rewarded ad...");
        this.placementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        ROUTER.updatePrivacyConsent(maxAdapterResponseParameters, activity.getApplicationContext());
        ROUTER.addRewardedAdapter(this, maxRewardedAdapterListener, this.placementId);
        if (sEnablePerPlacementLoad) {
            UnityAds.load(this.placementId);
        }
        if (UnityAds.isReady(this.placementId)) {
            ROUTER.onAdLoaded(this.placementId);
        } else if (!sEnablePerPlacementLoad) {
            ROUTER.onAdLoadFailed(this.placementId, getLoadError(this.placementId));
        }
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        log("Showing rewarded ad...");
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        if (UnityAds.isReady(thirdPartyAdPlacementId)) {
            configureReward(maxAdapterResponseParameters);
            ROUTER.addShowingAdapter(this);
            UnityAds.show(activity, thirdPartyAdPlacementId);
            return;
        }
        log("Rewarded ad not ready");
        maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    public void loadAdViewAd(MaxAdapterResponseParameters maxAdapterResponseParameters, MaxAdFormat maxAdFormat, Activity activity, final MaxAdViewAdapterListener maxAdViewAdapterListener) {
        log("Loading banner ad...");
        ROUTER.updatePrivacyConsent(maxAdapterResponseParameters, activity.getApplicationContext());
        this.bannerView = new BannerView(activity, maxAdapterResponseParameters.getThirdPartyAdPlacementId(), toUnityBannerSize(maxAdFormat));
        this.bannerView.setListener(new BannerView.IListener() {
            public void onBannerLoaded(BannerView bannerView) {
                UnityAdsMediationAdapter.this.log("Banner ad loaded");
                maxAdViewAdapterListener.onAdViewAdLoaded(bannerView);
            }

            public void onBannerFailedToLoad(BannerView bannerView, BannerErrorInfo bannerErrorInfo) {
                UnityAdsMediationAdapter.this.log("Banner ad failed to load");
                maxAdViewAdapterListener.onAdViewAdLoadFailed(UnityAdsMediationAdapter.toMaxError(bannerErrorInfo));
            }

            public void onBannerClick(BannerView bannerView) {
                UnityAdsMediationAdapter.this.log("Banner ad clicked");
                maxAdViewAdapterListener.onAdViewAdClicked();
            }

            public void onBannerLeftApplication(BannerView bannerView) {
                UnityAdsMediationAdapter.this.log("Banner ad left application");
            }
        });
        this.bannerView.load();
    }

    private UnityBannerSize toUnityBannerSize(MaxAdFormat maxAdFormat) {
        if (maxAdFormat == MaxAdFormat.BANNER) {
            return new UnityBannerSize(320, 50);
        }
        if (maxAdFormat == MaxAdFormat.LEADER) {
            return new UnityBannerSize(728, 90);
        }
        throw new IllegalArgumentException("Unsupported ad format: " + maxAdFormat);
    }

    private MaxAdapterError getLoadError(String str) {
        UnityAds.PlacementState placementState = UnityAds.getPlacementState(str);
        if (placementState == UnityAds.PlacementState.NOT_AVAILABLE) {
            return new MaxAdapterError(MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION);
        }
        if (placementState == UnityAds.PlacementState.DISABLED) {
            return new MaxAdapterError(MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION);
        }
        if (placementState == UnityAds.PlacementState.NO_FILL) {
            return MaxAdapterError.NO_FILL;
        }
        return MaxAdapterError.AD_NOT_READY;
    }

    private static MaxAdapterError toMaxError(UnityAds.UnityAdsError unityAdsError) {
        UnityAds.UnityAdsError unityAdsError2 = UnityAds.UnityAdsError.NOT_INITIALIZED;
        int i = MaxAdapterError.ERROR_CODE_INTERNAL_ERROR;
        if (unityAdsError == unityAdsError2) {
            i = MaxAdapterError.ERROR_CODE_NOT_INITIALIZED;
        } else if (unityAdsError != UnityAds.UnityAdsError.INITIALIZE_FAILED) {
            if (unityAdsError != UnityAds.UnityAdsError.INVALID_ARGUMENT) {
                if (unityAdsError != UnityAds.UnityAdsError.VIDEO_PLAYER_ERROR) {
                    if (unityAdsError != UnityAds.UnityAdsError.INIT_SANITY_CHECK_FAIL) {
                        if (unityAdsError == UnityAds.UnityAdsError.AD_BLOCKER_DETECTED) {
                            i = MaxAdapterError.ERROR_CODE_BAD_REQUEST;
                        } else if (!(unityAdsError == UnityAds.UnityAdsError.FILE_IO_ERROR || unityAdsError == UnityAds.UnityAdsError.DEVICE_ID_ERROR || unityAdsError == UnityAds.UnityAdsError.SHOW_ERROR || unityAdsError == UnityAds.UnityAdsError.INTERNAL_ERROR)) {
                            i = MaxAdapterError.ERROR_CODE_UNSPECIFIED;
                        }
                    }
                }
            }
            i = -5201;
        }
        return new MaxAdapterError(i, unityAdsError != null ? unityAdsError.name() : "UNKNOWN");
    }

    /* access modifiers changed from: private */
    public static MaxAdapterError toMaxError(BannerErrorInfo bannerErrorInfo) {
        int i;
        if (bannerErrorInfo.errorCode == BannerErrorCode.NO_FILL) {
            i = 204;
        } else if (bannerErrorInfo.errorCode == BannerErrorCode.NATIVE_ERROR) {
            i = MaxAdapterError.ERROR_CODE_INTERNAL_ERROR;
        } else {
            i = bannerErrorInfo.errorCode == BannerErrorCode.WEBVIEW_ERROR ? MaxAdapterError.ERROR_CODE_WEBVIEW_ERROR : MaxAdapterError.ERROR_CODE_UNSPECIFIED;
        }
        return new MaxAdapterError(i, bannerErrorInfo.errorMessage);
    }

    private static class UnityMediationAdapterRouter extends MediationAdapterRouter implements IUnityAdsListener, IUnityAdsExtendedListener {
        /* access modifiers changed from: package-private */
        public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        }

        public void onUnityAdsReady(String str) {
        }

        private UnityMediationAdapterRouter() {
        }

        /* access modifiers changed from: package-private */
        public void updatePrivacyConsent(MaxAdapterParameters maxAdapterParameters, Context context) {
            MetaData metaData = new MetaData(context);
            metaData.set("gdpr.consent", Boolean.valueOf(maxAdapterParameters.hasUserConsent()));
            if (AppLovinSdk.VERSION_CODE >= 91100) {
                metaData.set("privacy.consent", Boolean.valueOf(!maxAdapterParameters.isDoNotSell()));
            }
            metaData.commit();
        }

        public void onUnityAdsStart(String str) {
            onAdDisplayed(str);
            onRewardedAdVideoStarted(str);
        }

        public void onUnityAdsFinish(String str, UnityAds.FinishState finishState) {
            if (finishState == UnityAds.FinishState.COMPLETED) {
                onRewardedAdVideoCompleted(str);
                onUserRewarded(str, getReward(str));
                onAdHidden(str);
            } else if (finishState == UnityAds.FinishState.SKIPPED) {
                onRewardedAdVideoCompleted(str);
                if (shouldAlwaysRewardUser(str)) {
                    onUserRewarded(str, getReward(str));
                }
                onAdHidden(str);
            } else if (finishState == UnityAds.FinishState.ERROR) {
                log("UnityAds failed to finish ad for placement " + str);
                onAdDisplayFailed(str, MaxAdapterError.INTERNAL_ERROR);
            }
        }

        public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String str) {
            if (unityAdsError != null) {
                unityAdsError.name();
            }
            log("UnityAds did error " + unityAdsError.name() + " with message: " + str);
        }

        public void onUnityAdsClick(String str) {
            onAdClicked(str);
        }

        public void onUnityAdsPlacementStateChanged(String str, UnityAds.PlacementState placementState, UnityAds.PlacementState placementState2) {
            if (placementState2 == UnityAds.PlacementState.READY) {
                log("Placement " + str + " is ready");
                onAdLoaded(str);
            } else if (placementState2 == UnityAds.PlacementState.WAITING) {
                log("Placement " + str + " is loading...");
            } else if (placementState2 == UnityAds.PlacementState.NOT_AVAILABLE) {
                log("Placement " + str + " is not available");
                onAdLoadFailed(str, new MaxAdapterError((int) MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION, "PLACEMENT_NOT_AVAILABLE"));
            } else if (placementState2 == UnityAds.PlacementState.DISABLED) {
                log("Placement " + str + " is disabled - please check your Unity admin tools.");
                onAdLoadFailed(str, new MaxAdapterError((int) MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION, "PLACEMENT_DISABLED"));
            } else if (placementState2 == UnityAds.PlacementState.NO_FILL) {
                log("Placement " + str + " NO FILL'd");
                onAdLoadFailed(str, MaxAdapterError.NO_FILL);
            }
        }
    }
}
