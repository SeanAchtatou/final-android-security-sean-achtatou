package com.applovin.mediation.adapters.tapjoy;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int adSize = 2130903074;
        public static final int adSizes = 2130903075;
        public static final int adUnitId = 2130903076;
        public static final int buttonSize = 2130903127;
        public static final int circleCrop = 2130903163;
        public static final int colorScheme = 2130903186;
        public static final int imageAspectRatio = 2130903359;
        public static final int imageAspectRatioAdjust = 2130903360;
        public static final int scopeUris = 2130903450;

        private attr() {
        }
    }

    public static final class color {
        public static final int applovin_sdk_actionBarBackground = 2131034138;
        public static final int applovin_sdk_checkmarkColor = 2131034139;
        public static final int applovin_sdk_colorEdgeEffect = 2131034140;
        public static final int applovin_sdk_disclosureButtonColor = 2131034141;
        public static final int applovin_sdk_listViewBackground = 2131034142;
        public static final int applovin_sdk_listViewSectionTextColor = 2131034143;
        public static final int applovin_sdk_textColorPrimary = 2131034144;
        public static final int applovin_sdk_xmarkColor = 2131034145;
        public static final int common_google_signin_btn_text_dark = 2131034183;
        public static final int common_google_signin_btn_text_dark_default = 2131034184;
        public static final int common_google_signin_btn_text_dark_disabled = 2131034185;
        public static final int common_google_signin_btn_text_dark_focused = 2131034186;
        public static final int common_google_signin_btn_text_dark_pressed = 2131034187;
        public static final int common_google_signin_btn_text_light = 2131034188;
        public static final int common_google_signin_btn_text_light_default = 2131034189;
        public static final int common_google_signin_btn_text_light_disabled = 2131034190;
        public static final int common_google_signin_btn_text_light_focused = 2131034191;
        public static final int common_google_signin_btn_text_light_pressed = 2131034192;

        private color() {
        }
    }

    public static final class dimen {
        public static final int applovin_sdk_actionBarHeight = 2131099730;
        public static final int applovin_sdk_mediationDebuggerDetailListItemTextSize = 2131099731;
        public static final int applovin_sdk_mediationDebuggerSectionHeight = 2131099732;
        public static final int applovin_sdk_mediationDebuggerSectionTextSize = 2131099733;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int applovin_ic_check_mark = 2131165286;
        public static final int applovin_ic_disclosure_arrow = 2131165287;
        public static final int applovin_ic_x_mark = 2131165288;
        public static final int common_full_open_on_phone = 2131165314;
        public static final int common_google_signin_btn_icon_dark = 2131165315;
        public static final int common_google_signin_btn_icon_dark_focused = 2131165316;
        public static final int common_google_signin_btn_icon_dark_normal = 2131165317;
        public static final int common_google_signin_btn_icon_light = 2131165320;
        public static final int common_google_signin_btn_icon_light_focused = 2131165321;
        public static final int common_google_signin_btn_icon_light_normal = 2131165322;
        public static final int common_google_signin_btn_text_dark = 2131165324;
        public static final int common_google_signin_btn_text_dark_focused = 2131165325;
        public static final int common_google_signin_btn_text_dark_normal = 2131165326;
        public static final int common_google_signin_btn_text_light = 2131165329;
        public static final int common_google_signin_btn_text_light_focused = 2131165330;
        public static final int common_google_signin_btn_text_light_normal = 2131165331;
        public static final int mute_to_unmute = 2131165443;
        public static final int unmute_to_mute = 2131165460;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131230748;
        public static final int adjust_width = 2131230749;
        public static final int auto = 2131230777;
        public static final int dark = 2131230832;
        public static final int icon_only = 2131230920;
        public static final int imageView = 2131230923;
        public static final int light = 2131230935;
        public static final int listView = 2131230941;
        public static final int none = 2131231017;
        public static final int normal = 2131231018;
        public static final int standard = 2131231127;
        public static final int wide = 2131231190;
        public static final int wrap_content = 2131231192;

        private id() {
        }
    }

    public static final class integer {
        public static final int google_play_services_version = 2131296264;

        private integer() {
        }
    }

    public static final class layout {
        public static final int list_item_detail = 2131427437;
        public static final int list_item_right_detail = 2131427438;
        public static final int list_section = 2131427439;
        public static final int mediation_debugger_activity = 2131427441;
        public static final int mediation_debugger_detail_activity = 2131427442;

        private layout() {
        }
    }

    public static final class string {
        public static final int applovin_instructions_dialog_title = 2131689513;
        public static final int applovin_list_item_image_description = 2131689514;
        public static final int cancel = 2131689516;
        public static final int common_google_play_services_enable_button = 2131689543;
        public static final int common_google_play_services_enable_text = 2131689544;
        public static final int common_google_play_services_enable_title = 2131689545;
        public static final int common_google_play_services_install_button = 2131689546;
        public static final int common_google_play_services_install_title = 2131689548;
        public static final int common_google_play_services_notification_ticker = 2131689550;
        public static final int common_google_play_services_unknown_issue = 2131689551;
        public static final int common_google_play_services_unsupported_text = 2131689552;
        public static final int common_google_play_services_update_button = 2131689553;
        public static final int common_google_play_services_update_text = 2131689554;
        public static final int common_google_play_services_update_title = 2131689555;
        public static final int common_google_play_services_updating_text = 2131689556;
        public static final int common_open_on_phone = 2131689558;
        public static final int common_signin_button_text = 2131689559;
        public static final int common_signin_button_text_long = 2131689560;
        public static final int date_format_month_day = 2131689561;
        public static final int date_format_year_month_day = 2131689562;
        public static final int empty = 2131689564;
        public static final int failed_to_get_more = 2131689567;
        public static final int failed_to_load = 2131689568;
        public static final int failed_to_refresh = 2131689569;
        public static final int getting_more = 2131689574;
        public static final int just_before = 2131689713;
        public static final int loading = 2131689714;
        public static final int no = 2131689722;
        public static final int ok = 2131689723;
        public static final int please_wait = 2131689729;
        public static final int pull_down_to_load = 2131689731;
        public static final int pull_down_to_refresh = 2131689732;
        public static final int pull_up_to_get_more = 2131689733;
        public static final int refresh = 2131689734;
        public static final int release_to_get_more = 2131689735;
        public static final int release_to_load = 2131689736;
        public static final int release_to_refresh = 2131689737;
        public static final int search_hint = 2131689745;
        public static final int settings = 2131689747;
        public static final int sign_in = 2131689748;
        public static final int sign_out = 2131689749;
        public static final int sign_up = 2131689750;
        public static final int today = 2131689752;
        public static final int updating = 2131689753;
        public static final int yes = 2131689754;
        public static final int yesterday = 2131689755;

        private string() {
        }
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131755385;
        public static final int com_applovin_mediation_MaxDebuggerActivity_ActionBar = 2131755550;
        public static final int com_applovin_mediation_MaxDebuggerActivity_Theme = 2131755551;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.fluffyfairygames.idleminertycoon.R.attr.adSize, com.fluffyfairygames.idleminertycoon.R.attr.adSizes, com.fluffyfairygames.idleminertycoon.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {com.fluffyfairygames.idleminertycoon.R.attr.circleCrop, com.fluffyfairygames.idleminertycoon.R.attr.imageAspectRatio, com.fluffyfairygames.idleminertycoon.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.fluffyfairygames.idleminertycoon.R.attr.buttonSize, com.fluffyfairygames.idleminertycoon.R.attr.colorScheme, com.fluffyfairygames.idleminertycoon.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
