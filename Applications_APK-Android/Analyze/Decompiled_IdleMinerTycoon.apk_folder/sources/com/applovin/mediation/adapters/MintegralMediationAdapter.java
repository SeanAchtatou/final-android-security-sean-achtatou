package com.applovin.mediation.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.MaxSignalProvider;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;
import com.applovin.sdk.AppLovinSdk;
import com.helpshift.logger.model.LogDatabaseTable;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.activity.MTGCommonActivity;
import com.mintegral.msdk.interstitialvideo.out.InterstitialVideoListener;
import com.mintegral.msdk.interstitialvideo.out.MTGBidInterstitialVideoHandler;
import com.mintegral.msdk.interstitialvideo.out.MTGInterstitialVideoHandler;
import com.mintegral.msdk.mtgbid.out.BidManager;
import com.mintegral.msdk.out.BannerAdListener;
import com.mintegral.msdk.out.BannerSize;
import com.mintegral.msdk.out.MIntegralSDKFactory;
import com.mintegral.msdk.out.MTGBannerView;
import com.mintegral.msdk.out.MTGBidRewardVideoHandler;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.out.MTGRewardVideoHandler;
import com.mintegral.msdk.out.RewardVideoListener;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.mintegral.msdk.system.a;
import java.util.concurrent.atomic.AtomicBoolean;

public class MintegralMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter, MaxAdViewAdapter, MaxSignalProvider {
    private static final String APP_ID_PARAMETER = "app_id";
    private static final String APP_KEY_PARAMETER = "app_key";
    private static final String BAD_REQUEST = "request parameter is null";
    private static final String EXCEPTION_APP_ID_EMPTY = "EXCEPTION_APP_ID_EMPTY";
    private static final String EXCEPTION_APP_NOT_FOUND = "EXCEPTION_APP_NOT_FOUND";
    private static final String EXCEPTION_IV_RECALLNET_INVALIDATE = "EXCEPTION_IV_RECALLNET_INVALIDATE";
    private static final String EXCEPTION_RETURN_EMPTY = "EXCEPTION_RETURN_EMPTY";
    private static final String EXCEPTION_SIGN_ERROR = "EXCEPTION_SIGN_ERROR";
    private static final String EXCEPTION_TIMEOUT = "EXCEPTION_TIMEOUT";
    private static final String EXCEPTION_UNIT_ADTYPE_ERROR = "EXCEPTION_UNIT_ADTYPE_ERROR";
    private static final String EXCEPTION_UNIT_ID_EMPTY = "EXCEPTION_UNIT_ID_EMPTY";
    private static final String EXCEPTION_UNIT_NOT_FOUND = "EXCEPTION_UNIT_NOT_FOUND";
    private static final String EXCEPTION_UNIT_NOT_FOUND_IN_APP = "EXCEPTION_UNIT_NOT_FOUND_IN_APP";
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    private static final String NETWORK_ERROR = "network exception";
    private static final String NOT_INITIALIZED = "init error";
    private static final String NO_FILL_1 = "no ads available can show";
    private static final String NO_FILL_2 = "no ads available";
    private static final String NO_FILL_3 = "no server ads available";
    private static final String NO_FILL_4 = "no ads source";
    private static final String TIMEOUT = "load timeout";
    private static String sSdkVersion;
    /* access modifiers changed from: private */
    public MTGBannerView mtgBannerView;
    private MTGBidInterstitialVideoHandler mtgBidInterstitialVideoHandler;
    private MTGBidRewardVideoHandler mtgBidRewardVideoHandler;
    private MTGInterstitialVideoHandler mtgInterstitialVideoHandler;
    private MTGRewardVideoHandler mtgRewardVideoHandler;

    public String getAdapterVersion() {
        return "10.1.51.2";
    }

    public MintegralMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        checkExistence(MTGInterstitialVideoHandler.class, MTGRewardVideoHandler.class);
        checkExistence(MTGCommonActivity.class, MTGRewardVideoActivity.class);
        MIntegralConstans.DEBUG = maxAdapterInitializationParameters.isTesting();
        if (INITIALIZED.compareAndSet(false, true)) {
            String string = maxAdapterInitializationParameters.getServerParameters().getString("app_id");
            String string2 = maxAdapterInitializationParameters.getServerParameters().getString("app_key");
            a mIntegralSDK = MIntegralSDKFactory.getMIntegralSDK();
            boolean hasUserConsent = maxAdapterInitializationParameters.hasUserConsent();
            mIntegralSDK.setUserPrivateInfoType(activity, MIntegralConstans.AUTHORITY_ALL_INFO, hasUserConsent ? 1 : 0);
            mIntegralSDK.setConsentStatus(activity, hasUserConsent);
            mIntegralSDK.init(mIntegralSDK.getMTGConfigurationMap(string, string2), activity);
        }
        if (AppLovinSdk.VERSION_CODE >= 90800) {
            onCompletionListener.onCompletion(MaxAdapter.InitializationStatus.DOES_NOT_APPLY, null);
        } else {
            onCompletionListener.onCompletion();
        }
    }

    public String getSdkVersion() {
        if (sSdkVersion == null) {
            sSdkVersion = getVersionString(MTGConfiguration.class, LogDatabaseTable.LogTableColumns.SDK_VERSION);
        }
        return sSdkVersion;
    }

    public void collectSignal(MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters, Activity activity, MaxSignalCollectionListener maxSignalCollectionListener) {
        log("Collecting signal...");
        maxSignalCollectionListener.onSignalCollected(BidManager.getBuyerUid(activity));
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, final MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        AnonymousClass1 r0 = new InterstitialVideoListener() {
            public void onVideoLoadSuccess(String str) {
                MintegralMediationAdapter.this.log("Interstitial successfully loaded and video has been downloaded");
                maxInterstitialAdapterListener.onInterstitialAdLoaded();
            }

            public void onLoadSuccess(String str) {
                MintegralMediationAdapter.this.log("Interstitial successfully loaded but video still needs to be downloaded");
            }

            public void onVideoLoadFail(String str) {
                MintegralMediationAdapter mintegralMediationAdapter = MintegralMediationAdapter.this;
                mintegralMediationAdapter.log("Interstitial failed to load: " + str);
                maxInterstitialAdapterListener.onInterstitialAdLoadFailed(MintegralMediationAdapter.this.toMaxError(str));
            }

            public void onAdShow() {
                MintegralMediationAdapter.this.log("Interstitial displayed");
                maxInterstitialAdapterListener.onInterstitialAdDisplayed();
            }

            public void onShowFail(String str) {
                MintegralMediationAdapter mintegralMediationAdapter = MintegralMediationAdapter.this;
                mintegralMediationAdapter.log("Interstitial failed to show: " + str);
                maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MintegralMediationAdapter.this.toMaxError(str));
            }

            public void onVideoAdClicked(String str) {
                MintegralMediationAdapter.this.log("Interstitial clicked");
                maxInterstitialAdapterListener.onInterstitialAdClicked();
            }

            public void onAdClose(boolean z) {
                MintegralMediationAdapter.this.log("Interstitial hidden");
                maxInterstitialAdapterListener.onInterstitialAdHidden();
            }

            public void onVideoComplete(String str) {
                MintegralMediationAdapter.this.log("Interstitial video completed");
            }

            public void onEndcardShow(String str) {
                MintegralMediationAdapter.this.log("Interstitial endcard shown");
            }
        };
        boolean containsKey = maxAdapterResponseParameters.getServerParameters().containsKey("is_muted");
        int i = maxAdapterResponseParameters.getServerParameters().getBoolean("is_muted") ? 1 : 2;
        if (!TextUtils.isEmpty(maxAdapterResponseParameters.getBidResponse())) {
            log("Loading bidding interstitial ad...");
            this.mtgBidInterstitialVideoHandler = new MTGBidInterstitialVideoHandler(activity, maxAdapterResponseParameters.getThirdPartyAdPlacementId());
            this.mtgBidInterstitialVideoHandler.setInterstitialVideoListener(r0);
            if (this.mtgBidInterstitialVideoHandler.isBidReady()) {
                log("A bidding interstitial ad is ready already");
                maxInterstitialAdapterListener.onInterstitialAdLoaded();
                return;
            }
            if (containsKey) {
                this.mtgBidInterstitialVideoHandler.playVideoMute(i);
            }
            this.mtgBidInterstitialVideoHandler.loadFromBid(maxAdapterResponseParameters.getBidResponse());
            return;
        }
        log("Loading mediated interstitial ad...");
        this.mtgInterstitialVideoHandler = new MTGInterstitialVideoHandler(activity, maxAdapterResponseParameters.getThirdPartyAdPlacementId());
        this.mtgInterstitialVideoHandler.setInterstitialVideoListener(r0);
        if (this.mtgInterstitialVideoHandler.isReady()) {
            log("A mediated interstitial ad is ready already");
            maxInterstitialAdapterListener.onInterstitialAdLoaded();
            return;
        }
        if (containsKey) {
            this.mtgInterstitialVideoHandler.playVideoMute(i);
        }
        this.mtgInterstitialVideoHandler.load();
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        if (this.mtgBidInterstitialVideoHandler != null && this.mtgBidInterstitialVideoHandler.isBidReady()) {
            log("Showing bidding interstitial...");
            this.mtgBidInterstitialVideoHandler.showFromBid();
        } else if (this.mtgInterstitialVideoHandler == null || !this.mtgInterstitialVideoHandler.isReady()) {
            log("Unable to show interstitial - no ad loaded...");
            maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
        } else {
            log("Showing mediated interstitial...");
            this.mtgInterstitialVideoHandler.show();
        }
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, final MaxRewardedAdapterListener maxRewardedAdapterListener) {
        AnonymousClass2 r0 = new RewardVideoListener() {
            public void onVideoLoadSuccess(String str) {
                MintegralMediationAdapter.this.log("Rewarded ad successfully loaded and video has been downloaded");
                maxRewardedAdapterListener.onRewardedAdLoaded();
            }

            public void onLoadSuccess(String str) {
                MintegralMediationAdapter.this.log("Rewarded ad successfully loaded but video still needs to be downloaded");
            }

            public void onVideoLoadFail(String str) {
                MintegralMediationAdapter mintegralMediationAdapter = MintegralMediationAdapter.this;
                mintegralMediationAdapter.log("Rewarded ad failed to load: " + str);
                maxRewardedAdapterListener.onRewardedAdLoadFailed(MintegralMediationAdapter.this.toMaxError(str));
            }

            public void onAdShow() {
                MintegralMediationAdapter.this.log("Rewarded ad displayed");
                maxRewardedAdapterListener.onRewardedAdDisplayed();
                maxRewardedAdapterListener.onRewardedAdVideoStarted();
            }

            public void onShowFail(String str) {
                MintegralMediationAdapter mintegralMediationAdapter = MintegralMediationAdapter.this;
                mintegralMediationAdapter.log("Rewarded ad failed to show: " + str);
                maxRewardedAdapterListener.onRewardedAdDisplayFailed(MintegralMediationAdapter.this.toMaxError(str));
            }

            public void onVideoAdClicked(String str) {
                MintegralMediationAdapter.this.log("Rewarded ad clicked");
                maxRewardedAdapterListener.onRewardedAdClicked();
            }

            public void onAdClose(boolean z, String str, float f) {
                MintegralMediationAdapter.this.log("Rewarded ad hidden");
                if (z) {
                    maxRewardedAdapterListener.onRewardedAdVideoCompleted();
                    maxRewardedAdapterListener.onUserRewarded(MintegralMediationAdapter.this.getReward());
                } else if (MintegralMediationAdapter.this.shouldAlwaysRewardUser()) {
                    maxRewardedAdapterListener.onUserRewarded(MintegralMediationAdapter.this.getReward());
                }
                maxRewardedAdapterListener.onRewardedAdHidden();
            }

            public void onVideoComplete(String str) {
                MintegralMediationAdapter.this.log("Rewarded ad video completed");
            }

            public void onEndcardShow(String str) {
                MintegralMediationAdapter.this.log("Rewarded ad endcard shown");
            }
        };
        boolean containsKey = maxAdapterResponseParameters.getServerParameters().containsKey("is_muted");
        int i = maxAdapterResponseParameters.getServerParameters().getBoolean("is_muted") ? 1 : 2;
        if (!TextUtils.isEmpty(maxAdapterResponseParameters.getBidResponse())) {
            log("Loading bidding rewarded ad...");
            this.mtgBidRewardVideoHandler = new MTGBidRewardVideoHandler(activity, maxAdapterResponseParameters.getThirdPartyAdPlacementId());
            this.mtgBidRewardVideoHandler.setRewardVideoListener(r0);
            if (this.mtgBidRewardVideoHandler.isBidReady()) {
                log("A bidding rewarded ad is ready already");
                maxRewardedAdapterListener.onRewardedAdLoaded();
                return;
            }
            if (containsKey) {
                this.mtgBidRewardVideoHandler.playVideoMute(i);
            }
            this.mtgBidRewardVideoHandler.loadFromBid(maxAdapterResponseParameters.getBidResponse());
            return;
        }
        log("Loading mediated rewarded ad...");
        this.mtgRewardVideoHandler = new MTGRewardVideoHandler(activity, maxAdapterResponseParameters.getThirdPartyAdPlacementId());
        this.mtgRewardVideoHandler.setRewardVideoListener(r0);
        if (this.mtgRewardVideoHandler.isReady()) {
            log("A mediated rewarded ad is ready already");
            maxRewardedAdapterListener.onRewardedAdLoaded();
            return;
        }
        if (containsKey) {
            this.mtgRewardVideoHandler.playVideoMute(i);
        }
        this.mtgRewardVideoHandler.load();
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        configureReward(maxAdapterResponseParameters);
        Bundle serverParameters = maxAdapterResponseParameters.getServerParameters();
        String string = serverParameters.getString("reward_id", "");
        String string2 = serverParameters.getString("user_id", "");
        if (this.mtgBidRewardVideoHandler != null && this.mtgBidRewardVideoHandler.isBidReady()) {
            log("Showing bidding rewarded ad...");
            this.mtgBidRewardVideoHandler.showFromBid(string, string2);
        } else if (this.mtgRewardVideoHandler == null || !this.mtgRewardVideoHandler.isReady()) {
            log("Unable to show rewarded ad - no ad loaded...");
            maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
        } else {
            log("Showing mediated rewarded ad...");
            this.mtgRewardVideoHandler.show(string, string2);
        }
    }

    public void loadAdViewAd(MaxAdapterResponseParameters maxAdapterResponseParameters, MaxAdFormat maxAdFormat, Activity activity, final MaxAdViewAdapterListener maxAdViewAdapterListener) {
        BannerSize bannerSize = toBannerSize(maxAdFormat);
        this.mtgBannerView = new MTGBannerView(activity);
        this.mtgBannerView.init(bannerSize, maxAdapterResponseParameters.getThirdPartyAdPlacementId());
        this.mtgBannerView.setAllowShowCloseBtn(false);
        this.mtgBannerView.setRefreshTime(0);
        this.mtgBannerView.setBannerAdListener(new BannerAdListener() {
            public void onLoadSuccessed() {
                MintegralMediationAdapter.this.log("Banner ad loaded");
                maxAdViewAdapterListener.onAdViewAdLoaded(MintegralMediationAdapter.this.mtgBannerView);
            }

            public void onLoadFailed(String str) {
                MintegralMediationAdapter mintegralMediationAdapter = MintegralMediationAdapter.this;
                mintegralMediationAdapter.log("Banner ad failed to load: " + str);
                maxAdViewAdapterListener.onAdViewAdLoadFailed(MintegralMediationAdapter.this.toMaxError(str));
            }

            public void onLogImpression() {
                MintegralMediationAdapter.this.log("Banner ad displayed");
                maxAdViewAdapterListener.onAdViewAdDisplayed();
            }

            public void onClick() {
                MintegralMediationAdapter.this.log("Banner ad clicked");
                maxAdViewAdapterListener.onAdViewAdClicked();
            }

            public void onLeaveApp() {
                MintegralMediationAdapter.this.log("Banner ad will leave application");
            }

            public void showFullScreen() {
                MintegralMediationAdapter.this.log("Banner ad expanded");
                maxAdViewAdapterListener.onAdViewAdExpanded();
            }

            public void closeFullScreen() {
                MintegralMediationAdapter.this.log("Banner ad collapsed");
                maxAdViewAdapterListener.onAdViewAdCollapsed();
            }
        });
        if (!TextUtils.isEmpty(maxAdapterResponseParameters.getBidResponse())) {
            log("Loading bidding banner ad...");
            this.mtgBannerView.loadFromBid(maxAdapterResponseParameters.getBidResponse());
            return;
        }
        log("Loading mediated banner ad...");
        this.mtgBannerView.load();
    }

    public void onDestroy() {
        if (this.mtgInterstitialVideoHandler != null) {
            this.mtgInterstitialVideoHandler.setInterstitialVideoListener(null);
            this.mtgInterstitialVideoHandler = null;
        }
        if (this.mtgBidInterstitialVideoHandler != null) {
            this.mtgBidInterstitialVideoHandler.setInterstitialVideoListener(null);
            this.mtgBidInterstitialVideoHandler = null;
        }
        if (this.mtgRewardVideoHandler != null) {
            this.mtgRewardVideoHandler.setRewardVideoListener(null);
            this.mtgRewardVideoHandler = null;
        }
        if (this.mtgBidRewardVideoHandler != null) {
            this.mtgBidRewardVideoHandler.setRewardVideoListener(null);
            this.mtgBidRewardVideoHandler = null;
        }
        if (this.mtgBannerView != null) {
            this.mtgBannerView.release();
            this.mtgBannerView = null;
        }
    }

    /* access modifiers changed from: private */
    public MaxAdapterError toMaxError(String str) {
        return new MaxAdapterError((NOT_INITIALIZED.equals(str) || EXCEPTION_IV_RECALLNET_INVALIDATE.equalsIgnoreCase(str)) ? MaxAdapterError.ERROR_CODE_NOT_INITIALIZED : (NO_FILL_1.equalsIgnoreCase(str) || NO_FILL_2.equalsIgnoreCase(str) || NO_FILL_3.equalsIgnoreCase(str) || NO_FILL_4.equalsIgnoreCase(str) || EXCEPTION_RETURN_EMPTY.equalsIgnoreCase(str)) ? 204 : NETWORK_ERROR.equalsIgnoreCase(str) ? MaxAdapterError.ERROR_CODE_NO_CONNECTION : BAD_REQUEST.equalsIgnoreCase(str) ? MaxAdapterError.ERROR_CODE_BAD_REQUEST : (TIMEOUT.equalsIgnoreCase(str) || EXCEPTION_TIMEOUT.equalsIgnoreCase(str)) ? MaxAdapterError.ERROR_CODE_TIMEOUT : (EXCEPTION_SIGN_ERROR.equalsIgnoreCase(str) || EXCEPTION_UNIT_NOT_FOUND.equalsIgnoreCase(str) || EXCEPTION_UNIT_ID_EMPTY.equalsIgnoreCase(str) || EXCEPTION_UNIT_NOT_FOUND_IN_APP.equalsIgnoreCase(str) || EXCEPTION_UNIT_ADTYPE_ERROR.equalsIgnoreCase(str) || EXCEPTION_APP_ID_EMPTY.equalsIgnoreCase(str) || EXCEPTION_APP_NOT_FOUND.equalsIgnoreCase(str)) ? MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION : MaxAdapterError.ERROR_CODE_UNSPECIFIED, str);
    }

    private BannerSize toBannerSize(MaxAdFormat maxAdFormat) {
        if (maxAdFormat == MaxAdFormat.BANNER || maxAdFormat == MaxAdFormat.LEADER) {
            return new BannerSize(3, 0, 0);
        }
        if (maxAdFormat == MaxAdFormat.MREC) {
            return new BannerSize(2, 0, 0);
        }
        throw new IllegalArgumentException("Unsupported ad format: " + maxAdFormat);
    }
}
