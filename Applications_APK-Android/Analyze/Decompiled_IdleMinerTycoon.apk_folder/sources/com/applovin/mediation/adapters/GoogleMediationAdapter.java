package com.applovin.mediation.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.sdk.AppLovinSdk;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import java.util.concurrent.atomic.AtomicBoolean;

public class GoogleMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter, MaxAdViewAdapter {
    private static final String APP_ID_PARAMETER = "app_id";
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    /* access modifiers changed from: private */
    public AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardBasedVideoAd;

    public String getAdapterVersion() {
        return "17.2.1.7";
    }

    public GoogleMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        checkExistence(MobileAds.class, InterstitialAd.class, RewardedVideoAd.class, AdListener.class);
        if (INITIALIZED.compareAndSet(false, true)) {
            MobileAds.initialize(activity, maxAdapterInitializationParameters.getServerParameters().getString("app_id", null));
        }
        if (AppLovinSdk.VERSION_CODE >= 90800) {
            onCompletionListener.onCompletion(MaxAdapter.InitializationStatus.DOES_NOT_APPLY, null);
        } else {
            onCompletionListener.onCompletion();
        }
    }

    public String getSdkVersion() {
        return String.valueOf(GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, final MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        this.mInterstitialAd = new InterstitialAd(activity);
        this.mInterstitialAd.setAdUnitId(maxAdapterResponseParameters.getThirdPartyAdPlacementId());
        this.mInterstitialAd.setAdListener(new AdListener() {
            public void onAdClicked() {
            }

            public void onAdImpression() {
            }

            public void onAdLoaded() {
                maxInterstitialAdapterListener.onInterstitialAdLoaded();
            }

            public void onAdFailedToLoad(int i) {
                maxInterstitialAdapterListener.onInterstitialAdLoadFailed(GoogleMediationAdapter.toMaxError(i));
            }

            public void onAdOpened() {
                maxInterstitialAdapterListener.onInterstitialAdDisplayed();
            }

            public void onAdLeftApplication() {
                maxInterstitialAdapterListener.onInterstitialAdClicked();
            }

            public void onAdClosed() {
                maxInterstitialAdapterListener.onInterstitialAdHidden();
            }
        });
        updateMuteState(maxAdapterResponseParameters);
        this.mInterstitialAd.loadAd(createAdRequest(maxAdapterResponseParameters, activity));
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        if (this.mInterstitialAd == null || !this.mInterstitialAd.isLoaded()) {
            maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
        } else {
            this.mInterstitialAd.show();
        }
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, final MaxRewardedAdapterListener maxRewardedAdapterListener) {
        this.mRewardBasedVideoAd = MobileAds.getRewardedVideoAdInstance(activity);
        this.mRewardBasedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            private boolean hasGrantedReward;

            public void onRewardedVideoAdLoaded() {
                maxRewardedAdapterListener.onRewardedAdLoaded();
            }

            public void onRewardedVideoAdFailedToLoad(int i) {
                maxRewardedAdapterListener.onRewardedAdLoadFailed(GoogleMediationAdapter.toMaxError(i));
            }

            public void onRewardedVideoAdOpened() {
                maxRewardedAdapterListener.onRewardedAdDisplayed();
            }

            public void onRewardedVideoStarted() {
                maxRewardedAdapterListener.onRewardedAdVideoStarted();
            }

            public void onRewardedVideoAdLeftApplication() {
                maxRewardedAdapterListener.onRewardedAdClicked();
            }

            public void onRewarded(RewardItem rewardItem) {
                this.hasGrantedReward = true;
            }

            public void onRewardedVideoCompleted() {
                maxRewardedAdapterListener.onRewardedAdVideoCompleted();
            }

            public void onRewardedVideoAdClosed() {
                if (this.hasGrantedReward || GoogleMediationAdapter.this.shouldAlwaysRewardUser()) {
                    maxRewardedAdapterListener.onUserRewarded(GoogleMediationAdapter.this.getReward());
                }
                maxRewardedAdapterListener.onRewardedAdHidden();
            }
        });
        if (this.mRewardBasedVideoAd.isLoaded()) {
            maxRewardedAdapterListener.onRewardedAdLoaded();
            return;
        }
        updateMuteState(maxAdapterResponseParameters);
        this.mRewardBasedVideoAd.loadAd(maxAdapterResponseParameters.getThirdPartyAdPlacementId(), createAdRequest(maxAdapterResponseParameters, activity));
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        if (this.mRewardBasedVideoAd == null || !this.mRewardBasedVideoAd.isLoaded()) {
            maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
            return;
        }
        configureReward(maxAdapterResponseParameters);
        this.mRewardBasedVideoAd.show();
    }

    public void loadAdViewAd(MaxAdapterResponseParameters maxAdapterResponseParameters, MaxAdFormat maxAdFormat, Activity activity, final MaxAdViewAdapterListener maxAdViewAdapterListener) {
        this.mAdView = new AdView(activity);
        this.mAdView.setAdUnitId(maxAdapterResponseParameters.getThirdPartyAdPlacementId());
        this.mAdView.setAdSize(toAdSize(maxAdFormat));
        this.mAdView.setAdListener(new AdListener() {
            public void onAdClicked() {
            }

            public void onAdClosed() {
            }

            public void onAdImpression() {
            }

            public void onAdLeftApplication() {
            }

            public void onAdLoaded() {
                maxAdViewAdapterListener.onAdViewAdLoaded(GoogleMediationAdapter.this.mAdView);
            }

            public void onAdFailedToLoad(int i) {
                maxAdViewAdapterListener.onAdViewAdLoadFailed(GoogleMediationAdapter.toMaxError(i));
            }

            public void onAdOpened() {
                maxAdViewAdapterListener.onAdViewAdClicked();
            }
        });
        this.mAdView.loadAd(createAdRequest(maxAdapterResponseParameters, activity));
    }

    public void onDestroy() {
        if (this.mInterstitialAd != null) {
            this.mInterstitialAd.setAdListener(null);
            this.mInterstitialAd = null;
        }
        if (this.mRewardBasedVideoAd != null) {
            this.mRewardBasedVideoAd.setRewardedVideoAdListener(null);
            this.mRewardBasedVideoAd = null;
        }
        if (this.mAdView != null) {
            this.mAdView.destroy();
            this.mAdView = null;
        }
    }

    /* access modifiers changed from: private */
    public static MaxAdapterError toMaxError(int i) {
        return new MaxAdapterError(i == 0 ? MaxAdapterError.ERROR_CODE_INTERNAL_ERROR : i == 1 ? MaxAdapterError.ERROR_CODE_BAD_REQUEST : i == 2 ? MaxAdapterError.ERROR_CODE_NO_CONNECTION : i == 3 ? 204 : MaxAdapterError.ERROR_CODE_UNSPECIFIED, i);
    }

    private AdSize toAdSize(MaxAdFormat maxAdFormat) {
        if (maxAdFormat == MaxAdFormat.BANNER) {
            return AdSize.BANNER;
        }
        if (maxAdFormat == MaxAdFormat.LEADER) {
            return AdSize.LEADERBOARD;
        }
        if (maxAdFormat == MaxAdFormat.MREC) {
            return AdSize.MEDIUM_RECTANGLE;
        }
        throw new IllegalArgumentException("Unsupported ad format: " + maxAdFormat);
    }

    private static AdRequest createAdRequest(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity) {
        Bundle serverParameters = maxAdapterResponseParameters.getServerParameters();
        AdRequest.Builder builder = new AdRequest.Builder();
        if (serverParameters.containsKey("is_designed_for_families")) {
            builder.setIsDesignedForFamilies(serverParameters.getBoolean("is_designed_for_families"));
        }
        builder.tagForChildDirectedTreatment(maxAdapterResponseParameters.isAgeRestrictedUser());
        String string = serverParameters.getString("test_device_ids", null);
        if (!TextUtils.isEmpty(string)) {
            for (String addTestDevice : string.split(",")) {
                builder.addTestDevice(addTestDevice);
            }
        }
        if (serverParameters.getBoolean("set_mediation_identifier", true)) {
            builder.setRequestAgent(mediationTag());
        }
        Bundle bundle = new Bundle();
        if (!maxAdapterResponseParameters.hasUserConsent()) {
            bundle.putString("npa", "1");
        }
        if (AppLovinSdk.VERSION_CODE >= 91100 && maxAdapterResponseParameters.isDoNotSell()) {
            bundle.putInt("rdp", 1);
            activity.getPreferences(0).edit().putInt("gad_rdp", 1).commit();
        }
        builder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
        return builder.build();
    }

    private static void updateMuteState(MaxAdapterResponseParameters maxAdapterResponseParameters) {
        Bundle serverParameters = maxAdapterResponseParameters.getServerParameters();
        if (serverParameters.containsKey("is_muted")) {
            MobileAds.setAppMuted(serverParameters.getBoolean("is_muted"));
        }
    }
}
