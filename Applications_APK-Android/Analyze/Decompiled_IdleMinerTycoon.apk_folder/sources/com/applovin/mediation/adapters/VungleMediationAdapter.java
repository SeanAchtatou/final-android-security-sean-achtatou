package com.applovin.mediation.adapters;

import android.app.Activity;
import android.os.Bundle;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.sdk.AppLovinSdk;
import com.vungle.warren.AdConfig;
import com.vungle.warren.BuildConfig;
import com.vungle.warren.InitCallback;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;
import com.vungle.warren.error.VungleException;
import java.util.concurrent.atomic.AtomicBoolean;

public class VungleMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter {
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    private static final VungleMediationAdapterRouter ROUTER;
    /* access modifiers changed from: private */
    public static MaxAdapter.InitializationStatus sStatus;
    private String mPlacementId;

    public String getAdapterVersion() {
        return "6.4.11.2";
    }

    static {
        if (AppLovinSdk.VERSION_CODE >= 90802) {
            ROUTER = (VungleMediationAdapterRouter) MediationAdapterRouter.getSharedInstance(VungleMediationAdapterRouter.class);
        } else {
            ROUTER = new VungleMediationAdapterRouter();
        }
    }

    public VungleMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, final MaxAdapter.OnCompletionListener onCompletionListener) {
        log("Initializing Vungle SDK...");
        checkExistence(Vungle.class);
        ROUTER.updateUserConsent(maxAdapterInitializationParameters.hasUserConsent());
        if (INITIALIZED.compareAndSet(false, true)) {
            if (AppLovinSdk.VERSION_CODE >= 90800) {
                sStatus = MaxAdapter.InitializationStatus.INITIALIZING;
            }
            Vungle.init(maxAdapterInitializationParameters.getServerParameters().getString("app_id", null), activity.getApplicationContext(), new InitCallback() {
                public void onSuccess() {
                    VungleMediationAdapter.this.log("Vungle SDK initialized");
                    if (AppLovinSdk.VERSION_CODE >= 90800) {
                        MaxAdapter.InitializationStatus unused = VungleMediationAdapter.sStatus = MaxAdapter.InitializationStatus.INITIALIZED_SUCCESS;
                        onCompletionListener.onCompletion(VungleMediationAdapter.sStatus, null);
                        return;
                    }
                    onCompletionListener.onCompletion();
                }

                public void onError(Throwable th) {
                    VungleMediationAdapter.this.log("Vungle SDK failed to initialize with error", th);
                    if (AppLovinSdk.VERSION_CODE >= 90800) {
                        MaxAdapter.InitializationStatus unused = VungleMediationAdapter.sStatus = MaxAdapter.InitializationStatus.INITIALIZED_FAILURE;
                        onCompletionListener.onCompletion(VungleMediationAdapter.sStatus, th.getLocalizedMessage());
                        return;
                    }
                    onCompletionListener.onCompletion();
                }

                public void onAutoCacheAdAvailable(String str) {
                    VungleMediationAdapter vungleMediationAdapter = VungleMediationAdapter.this;
                    vungleMediationAdapter.log("Auto-cached ad " + str);
                }
            });
            return;
        }
        log("Vungle SDK initialized");
        if (AppLovinSdk.VERSION_CODE >= 90800) {
            onCompletionListener.onCompletion(sStatus, null);
        } else {
            onCompletionListener.onCompletion();
        }
    }

    public String getSdkVersion() {
        return getVersionString(BuildConfig.class, "VERSION_NAME");
    }

    public void onDestroy() {
        ROUTER.removeAdapter(this, this.mPlacementId);
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        log("Loading interstitial ad...");
        this.mPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        ROUTER.addInterstitialAdapter(this, maxInterstitialAdapterListener, this.mPlacementId);
        if (Vungle.canPlayAd(this.mPlacementId)) {
            ROUTER.onAdLoaded(this.mPlacementId);
        } else {
            loadAd(this.mPlacementId, maxAdapterResponseParameters);
        }
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        log("Showing interstitial ad...");
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        if (Vungle.canPlayAd(thirdPartyAdPlacementId)) {
            ROUTER.addShowingAdapter(this);
            showAd(thirdPartyAdPlacementId, maxAdapterResponseParameters.getServerParameters());
            return;
        }
        log("Interstitial ad not ready");
        maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        log("Loading rewarded ad...");
        this.mPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        ROUTER.addRewardedAdapter(this, maxRewardedAdapterListener, this.mPlacementId);
        if (Vungle.canPlayAd(this.mPlacementId)) {
            ROUTER.onAdLoaded(this.mPlacementId);
        } else {
            loadAd(this.mPlacementId, maxAdapterResponseParameters);
        }
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        log("Showing rewarded ad...");
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        if (Vungle.canPlayAd(thirdPartyAdPlacementId)) {
            ROUTER.addShowingAdapter(this);
            configureReward(maxAdapterResponseParameters);
            showAd(thirdPartyAdPlacementId, maxAdapterResponseParameters.getServerParameters());
            return;
        }
        log("Rewarded ad not ready");
        maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    private void loadAd(String str, MaxAdapterResponseParameters maxAdapterResponseParameters) {
        ROUTER.updateUserConsent(maxAdapterResponseParameters.hasUserConsent());
        Vungle.loadAd(str, ROUTER.getLoadAdCallback());
    }

    private void showAd(String str, Bundle bundle) {
        AdConfig adConfig = new AdConfig();
        if (bundle.containsKey("ordinal")) {
            adConfig.setOrdinal(bundle.getInt("ordinal"));
        }
        if (bundle.containsKey("immersive_mode")) {
            adConfig.setImmersiveMode(bundle.getBoolean("immersive_mode"));
        }
        if (bundle.containsKey("flex_view_auto_dismiss_seconds")) {
            adConfig.setFlexViewCloseTime(bundle.getInt("flex_view_auto_dismiss_seconds"));
        }
        if (bundle.containsKey("is_muted")) {
            adConfig.setMuted(bundle.getBoolean("is_muted"));
        }
        Vungle.playAd(str, adConfig, ROUTER.getPlayAdCallback());
    }

    /* access modifiers changed from: private */
    public static MaxAdapterError toMaxError(Throwable th) {
        boolean z = th instanceof VungleException;
        int i = MaxAdapterError.ERROR_CODE_UNSPECIFIED;
        if (z) {
            VungleException vungleException = (VungleException) th;
            int exceptionCode = vungleException.getExceptionCode();
            if (exceptionCode != 0) {
                if (1 == exceptionCode) {
                    i = 204;
                } else if (2 != exceptionCode) {
                    if (3 != exceptionCode) {
                        if (4 == exceptionCode) {
                            i = -5201;
                        } else if (6 == exceptionCode || 7 == exceptionCode || 9 == exceptionCode) {
                            i = MaxAdapterError.ERROR_CODE_NOT_INITIALIZED;
                        } else if (10 == exceptionCode) {
                            i = MaxAdapterError.ERROR_CODE_INTERNAL_ERROR;
                        } else if (11 == exceptionCode) {
                            i = MaxAdapterError.ERROR_CODE_NO_CONNECTION;
                        } else if (13 != exceptionCode) {
                            if (14 == exceptionCode) {
                                i = MaxAdapterError.ERROR_CODE_SERVER_ERROR;
                            }
                        }
                    }
                }
                return new MaxAdapterError(i, toErrorMessage(vungleException));
            }
            i = MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION;
            return new MaxAdapterError(i, toErrorMessage(vungleException));
        }
        return new MaxAdapterError((int) MaxAdapterError.ERROR_CODE_UNSPECIFIED, th.getClass() + ": " + th.getLocalizedMessage());
    }

    private static String toErrorMessage(VungleException vungleException) {
        return vungleException.getExceptionCode() + ": " + vungleException.getLocalizedMessage();
    }

    private static class VungleMediationAdapterRouter extends MediationAdapterRouter {
        private final LoadAdCallback mLoadAdCallback;
        private final PlayAdCallback mPlayAdCallback;

        /* access modifiers changed from: package-private */
        public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        }

        private VungleMediationAdapterRouter() {
            this.mLoadAdCallback = new LoadAdCallback() {
                public void onAdLoad(String str) {
                    VungleMediationAdapterRouter.this.onAdLoaded(str);
                }

                public void onError(String str, Throwable th) {
                    VungleMediationAdapterRouter.this.onAdLoadFailed(str, VungleMediationAdapter.toMaxError(th));
                }
            };
            this.mPlayAdCallback = new PlayAdCallback() {
                public void onAdStart(String str) {
                    VungleMediationAdapterRouter.this.onAdDisplayed(str);
                    VungleMediationAdapterRouter.this.onRewardedAdVideoStarted(str);
                }

                public void onAdEnd(String str, boolean z, boolean z2) {
                    if (z2) {
                        VungleMediationAdapterRouter.this.onAdClicked(str);
                    }
                    if (z) {
                        VungleMediationAdapterRouter.this.onRewardedAdVideoCompleted(str);
                        VungleMediationAdapterRouter.this.onUserRewarded(str, VungleMediationAdapterRouter.this.getReward(str));
                    } else if (VungleMediationAdapterRouter.this.shouldAlwaysRewardUser(str)) {
                        VungleMediationAdapterRouter.this.onUserRewarded(str, VungleMediationAdapterRouter.this.getReward(str));
                    }
                    VungleMediationAdapterRouter.this.onAdHidden(str);
                }

                public void onError(String str, Throwable th) {
                    VungleMediationAdapterRouter.this.onAdDisplayFailed(str, VungleMediationAdapter.toMaxError(th));
                }
            };
        }

        /* access modifiers changed from: package-private */
        public void updateUserConsent(boolean z) {
            Vungle.updateConsentStatus(z ? Vungle.Consent.OPTED_IN : Vungle.Consent.OPTED_OUT, "");
        }

        /* access modifiers changed from: package-private */
        public LoadAdCallback getLoadAdCallback() {
            return this.mLoadAdCallback;
        }

        /* access modifiers changed from: package-private */
        public PlayAdCallback getPlayAdCallback() {
            return this.mPlayAdCallback;
        }
    }
}
