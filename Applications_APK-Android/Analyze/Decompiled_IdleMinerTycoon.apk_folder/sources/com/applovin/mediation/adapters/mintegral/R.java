package com.applovin.mediation.adapters.mintegral;

public final class R {
    private R() {
    }

    public static final class anim {
        public static final int mintegral_anim_scale = 2130771988;
        public static final int mintegral_reward_activity_open = 2130771989;
        public static final int mintegral_reward_activity_stay = 2130771990;

        private anim() {
        }
    }

    public static final class color {
        public static final int applovin_sdk_actionBarBackground = 2131034138;
        public static final int applovin_sdk_checkmarkColor = 2131034139;
        public static final int applovin_sdk_colorEdgeEffect = 2131034140;
        public static final int applovin_sdk_disclosureButtonColor = 2131034141;
        public static final int applovin_sdk_listViewBackground = 2131034142;
        public static final int applovin_sdk_listViewSectionTextColor = 2131034143;
        public static final int applovin_sdk_textColorPrimary = 2131034144;
        public static final int applovin_sdk_xmarkColor = 2131034145;
        public static final int mintegral_interstitial_black = 2131034275;
        public static final int mintegral_interstitial_white = 2131034276;
        public static final int mintegral_reward_black = 2131034277;
        public static final int mintegral_reward_cta_bg = 2131034278;
        public static final int mintegral_reward_desc_textcolor = 2131034279;
        public static final int mintegral_reward_endcard_hor_bg = 2131034280;
        public static final int mintegral_reward_endcard_land_bg = 2131034281;
        public static final int mintegral_reward_endcard_line_bg = 2131034282;
        public static final int mintegral_reward_endcard_vast_bg = 2131034283;
        public static final int mintegral_reward_kiloo_background = 2131034284;
        public static final int mintegral_reward_minicard_bg = 2131034285;
        public static final int mintegral_reward_six_black_transparent = 2131034286;
        public static final int mintegral_reward_title_textcolor = 2131034287;
        public static final int mintegral_reward_white = 2131034288;
        public static final int mintegral_video_common_alertview_bg = 2131034289;
        public static final int mintegral_video_common_alertview_cancel_button_bg_default = 2131034290;
        public static final int mintegral_video_common_alertview_cancel_button_bg_pressed = 2131034291;
        public static final int mintegral_video_common_alertview_cancel_button_textcolor = 2131034292;
        public static final int mintegral_video_common_alertview_confirm_button_bg_default = 2131034293;
        public static final int mintegral_video_common_alertview_confirm_button_bg_pressed = 2131034294;
        public static final int mintegral_video_common_alertview_confirm_button_textcolor = 2131034295;
        public static final int mintegral_video_common_alertview_content_textcolor = 2131034296;
        public static final int mintegral_video_common_alertview_title_textcolor = 2131034297;

        private color() {
        }
    }

    public static final class dimen {
        public static final int applovin_sdk_actionBarHeight = 2131099730;
        public static final int applovin_sdk_mediationDebuggerDetailListItemTextSize = 2131099731;
        public static final int applovin_sdk_mediationDebuggerSectionHeight = 2131099732;
        public static final int applovin_sdk_mediationDebuggerSectionTextSize = 2131099733;
        public static final int mintegral_video_common_alertview_bg_padding = 2131099851;
        public static final int mintegral_video_common_alertview_button_height = 2131099852;
        public static final int mintegral_video_common_alertview_button_margintop = 2131099853;
        public static final int mintegral_video_common_alertview_button_radius = 2131099854;
        public static final int mintegral_video_common_alertview_button_textsize = 2131099855;
        public static final int mintegral_video_common_alertview_button_width = 2131099856;
        public static final int mintegral_video_common_alertview_content_margintop = 2131099857;
        public static final int mintegral_video_common_alertview_content_size = 2131099858;
        public static final int mintegral_video_common_alertview_contentview_maxwidth = 2131099859;
        public static final int mintegral_video_common_alertview_contentview_minwidth = 2131099860;
        public static final int mintegral_video_common_alertview_title_size = 2131099861;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int applovin_ic_check_mark = 2131165286;
        public static final int applovin_ic_disclosure_arrow = 2131165287;
        public static final int applovin_ic_x_mark = 2131165288;
        public static final int mintegral_banner_close = 2131165394;
        public static final int mintegral_close = 2131165395;
        public static final int mintegral_close_background = 2131165396;
        public static final int mintegral_cm_backward = 2131165397;
        public static final int mintegral_cm_backward_disabled = 2131165398;
        public static final int mintegral_cm_backward_nor = 2131165399;
        public static final int mintegral_cm_backward_selected = 2131165400;
        public static final int mintegral_cm_end_animation = 2131165401;
        public static final int mintegral_cm_exits = 2131165402;
        public static final int mintegral_cm_exits_nor = 2131165403;
        public static final int mintegral_cm_exits_selected = 2131165404;
        public static final int mintegral_cm_forward = 2131165405;
        public static final int mintegral_cm_forward_disabled = 2131165406;
        public static final int mintegral_cm_forward_nor = 2131165407;
        public static final int mintegral_cm_forward_selected = 2131165408;
        public static final int mintegral_cm_head = 2131165409;
        public static final int mintegral_cm_highlight = 2131165410;
        public static final int mintegral_cm_progress = 2131165411;
        public static final int mintegral_cm_refresh = 2131165412;
        public static final int mintegral_cm_refresh_nor = 2131165413;
        public static final int mintegral_cm_refresh_selected = 2131165414;
        public static final int mintegral_cm_tail = 2131165415;
        public static final int mintegral_interstitial_close = 2131165416;
        public static final int mintegral_interstitial_over = 2131165417;
        public static final int mintegral_loading_bg = 2131165418;
        public static final int mintegral_reward_activity_ad_end_land_des_rl_hot = 2131165419;
        public static final int mintegral_reward_close = 2131165420;
        public static final int mintegral_reward_end_close_shape_oval = 2131165421;
        public static final int mintegral_reward_end_land_shape = 2131165422;
        public static final int mintegral_reward_end_pager_logo = 2131165423;
        public static final int mintegral_reward_end_shape_oval = 2131165424;
        public static final int mintegral_reward_shape_end_pager = 2131165425;
        public static final int mintegral_reward_shape_progress = 2131165426;
        public static final int mintegral_reward_sound_close = 2131165427;
        public static final int mintegral_reward_sound_open = 2131165428;
        public static final int mintegral_reward_vast_end_close = 2131165429;
        public static final int mintegral_reward_vast_end_ok = 2131165430;
        public static final int mintegral_video_common_alertview_bg = 2131165431;
        public static final int mintegral_video_common_alertview_cancel_bg = 2131165432;
        public static final int mintegral_video_common_alertview_cancel_bg_nor = 2131165433;
        public static final int mintegral_video_common_alertview_cancel_bg_pressed = 2131165434;
        public static final int mintegral_video_common_alertview_confirm_bg = 2131165435;
        public static final int mintegral_video_common_alertview_confirm_bg_nor = 2131165436;
        public static final int mintegral_video_common_alertview_confirm_bg_pressed = 2131165437;
        public static final int mintegral_video_common_full_star = 2131165438;
        public static final int mintegral_video_common_full_while_star = 2131165439;
        public static final int mintegral_video_common_half_star = 2131165440;
        public static final int mute_to_unmute = 2131165443;
        public static final int unmute_to_mute = 2131165460;

        private drawable() {
        }
    }

    public static final class id {
        public static final int imageView = 2131230923;
        public static final int linescroll = 2131230939;
        public static final int listView = 2131230941;
        public static final int mintegral_close = 2131230953;
        public static final int mintegral_close_imageview = 2131230954;
        public static final int mintegral_interstitial_iv_close = 2131230955;
        public static final int mintegral_interstitial_pb = 2131230956;
        public static final int mintegral_interstitial_wv = 2131230957;
        public static final int mintegral_iv_adbanner = 2131230958;
        public static final int mintegral_iv_appicon = 2131230959;
        public static final int mintegral_iv_close = 2131230960;
        public static final int mintegral_iv_hottag = 2131230961;
        public static final int mintegral_iv_icon = 2131230962;
        public static final int mintegral_iv_iconbg = 2131230963;
        public static final int mintegral_iv_vastclose = 2131230964;
        public static final int mintegral_iv_vastok = 2131230965;
        public static final int mintegral_jscommon_checkBox = 2131230966;
        public static final int mintegral_jscommon_okbutton = 2131230967;
        public static final int mintegral_jscommon_webcontent = 2131230968;
        public static final int mintegral_left = 2131230969;
        public static final int mintegral_ll_bottomlayout = 2131230970;
        public static final int mintegral_middle = 2131230971;
        public static final int mintegral_playercommon_ll_loading = 2131230972;
        public static final int mintegral_playercommon_ll_sur_container = 2131230973;
        public static final int mintegral_playercommon_rl_root = 2131230974;
        public static final int mintegral_right = 2131230975;
        public static final int mintegral_rl_bodycontainer = 2131230976;
        public static final int mintegral_rl_bottomcontainer = 2131230977;
        public static final int mintegral_rl_content = 2131230978;
        public static final int mintegral_rl_playing_close = 2131230979;
        public static final int mintegral_rl_topcontainer = 2131230980;
        public static final int mintegral_sound_switch = 2131230981;
        public static final int mintegral_sv_starlevel = 2131230982;
        public static final int mintegral_tv_adtag = 2131230983;
        public static final int mintegral_tv_appdesc = 2131230984;
        public static final int mintegral_tv_apptitle = 2131230985;
        public static final int mintegral_tv_cta = 2131230986;
        public static final int mintegral_tv_desc = 2131230987;
        public static final int mintegral_tv_install = 2131230988;
        public static final int mintegral_tv_sound = 2131230989;
        public static final int mintegral_tv_vasttag = 2131230990;
        public static final int mintegral_tv_vasttitle = 2131230991;
        public static final int mintegral_vfpv = 2131230992;
        public static final int mintegral_video_common_alertview_cancel_button = 2131230993;
        public static final int mintegral_video_common_alertview_confirm_button = 2131230994;
        public static final int mintegral_video_common_alertview_contentview = 2131230995;
        public static final int mintegral_video_common_alertview_titleview = 2131230996;
        public static final int mintegral_video_templete_container = 2131230997;
        public static final int mintegral_video_templete_progressbar = 2131230998;
        public static final int mintegral_video_templete_videoview = 2131230999;
        public static final int mintegral_video_templete_webview_parent = 2131231000;
        public static final int mintegral_view_bottomline = 2131231001;
        public static final int mintegral_view_shadow = 2131231002;
        public static final int mintegral_viewgroup_ctaroot = 2131231003;
        public static final int mintegral_windwv_close = 2131231004;
        public static final int mintegral_windwv_content_rl = 2131231005;
        public static final int progressBar = 2131231044;
        public static final int progressBar1 = 2131231045;
        public static final int textView = 2131231146;

        private id() {
        }
    }

    public static final class layout {
        public static final int list_item_detail = 2131427437;
        public static final int list_item_right_detail = 2131427438;
        public static final int list_section = 2131427439;
        public static final int loading_alert = 2131427440;
        public static final int mediation_debugger_activity = 2131427441;
        public static final int mediation_debugger_detail_activity = 2131427442;
        public static final int mintegral_close_imageview_layout = 2131427443;
        public static final int mintegral_interstitial_activity = 2131427444;
        public static final int mintegral_jscommon_authoritylayout = 2131427445;
        public static final int mintegral_loading_dialog = 2131427446;
        public static final int mintegral_loading_view = 2131427447;
        public static final int mintegral_playercommon_player_view = 2131427448;
        public static final int mintegral_reward_activity_video_templete = 2131427449;
        public static final int mintegral_reward_activity_video_templete_transparent = 2131427450;
        public static final int mintegral_reward_clickable_cta = 2131427451;
        public static final int mintegral_reward_endcard_h5 = 2131427452;
        public static final int mintegral_reward_endcard_native_hor = 2131427453;
        public static final int mintegral_reward_endcard_native_land = 2131427454;
        public static final int mintegral_reward_endcard_vast = 2131427455;
        public static final int mintegral_reward_videoview_item = 2131427456;
        public static final int mintegral_video_common_alertview = 2131427457;

        private layout() {
        }
    }

    public static final class string {
        public static final int app_name = 2131689511;
        public static final int applovin_instructions_dialog_title = 2131689513;
        public static final int applovin_list_item_image_description = 2131689514;
        public static final int mintegral_reward_appdesc = 2131689715;
        public static final int mintegral_reward_apptitle = 2131689716;
        public static final int mintegral_reward_clickable_cta_btntext = 2131689717;
        public static final int mintegral_reward_endcard_ad = 2131689718;
        public static final int mintegral_reward_endcard_vast_notice = 2131689719;
        public static final int mintegral_reward_install = 2131689720;

        private string() {
        }
    }

    public static final class style {
        public static final int com_applovin_mediation_MaxDebuggerActivity_ActionBar = 2131755550;
        public static final int com_applovin_mediation_MaxDebuggerActivity_Theme = 2131755551;
        public static final int mintegral_reward_theme = 2131755560;
        public static final int mintegral_transparent_theme = 2131755561;

        private style() {
        }
    }
}
