package com.applovin.mediation.adapters;

import android.app.Activity;
import android.os.Bundle;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.appevents.AppEventsConstants;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class AdColonyMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter {
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    private static MaxAdapter.InitializationStatus sStatus;
    private InterstitialListener interstitialAdListener;
    /* access modifiers changed from: private */
    public AdColonyInterstitial loadedInterstitialAd;
    /* access modifiers changed from: private */
    public AdColonyInterstitial loadedRewardedAd;
    private RewardedAdListener rewardedAdListener;

    public String getAdapterVersion() {
        return "4.1.2.1";
    }

    public AdColonyMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public String getSdkVersion() {
        return AdColony.getSDKVersion();
    }

    public void onDestroy() {
        this.loadedInterstitialAd = null;
        this.interstitialAdListener = null;
        this.loadedRewardedAd = null;
        this.rewardedAdListener = null;
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        if (INITIALIZED.compareAndSet(false, true)) {
            if (AppLovinSdk.VERSION_CODE >= 90800) {
                sStatus = MaxAdapter.InitializationStatus.INITIALIZING;
            }
            log("Initializing AdColony SDK...");
            checkExistence(AdColony.class);
            if (AppLovinSdk.VERSION_CODE >= 90300) {
                boolean configure = AdColony.configure(activity, getOptions(maxAdapterInitializationParameters), maxAdapterInitializationParameters.getServerParameters().getString("app_id"), getZoneIds(maxAdapterInitializationParameters));
                if (AppLovinSdk.VERSION_CODE >= 90800) {
                    sStatus = configure ? MaxAdapter.InitializationStatus.INITIALIZED_SUCCESS : MaxAdapter.InitializationStatus.INITIALIZED_FAILURE;
                    onCompletionListener.onCompletion(sStatus, null);
                }
            } else {
                throw new IllegalStateException("Please update to AppLovin SDK 9.3.0 or above to use AdColony's adapter.");
            }
        } else if (AppLovinSdk.VERSION_CODE >= 90800) {
            onCompletionListener.onCompletion(sStatus, null);
        }
        if (AppLovinSdk.VERSION_CODE < 90800) {
            onCompletionListener.onCompletion();
        }
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        log("Loading interstitial ad for zone id: " + thirdPartyAdPlacementId + "...");
        if (!isAdColonyConfigured()) {
            log("AdColony SDK is not initialized");
            maxInterstitialAdapterListener.onInterstitialAdLoadFailed(MaxAdapterError.NOT_INITIALIZED);
            return;
        }
        AdColony.setAppOptions(getOptions(maxAdapterResponseParameters));
        this.interstitialAdListener = new InterstitialListener(maxInterstitialAdapterListener);
        AdColony.requestInterstitial(thirdPartyAdPlacementId, this.interstitialAdListener);
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        log("Showing interstitial ad...");
        if (this.loadedInterstitialAd == null) {
            log("Interstitial ad not ready");
            maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.UNSPECIFIED);
        } else if (this.loadedInterstitialAd.isExpired()) {
            log("Interstitial ad is expired");
            maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(new MaxAdapterError(MaxAdapterError.ERROR_CODE_AD_EXPIRED));
        } else if (!this.loadedInterstitialAd.show()) {
            log("Interstitial ad failed to display");
            maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.UNSPECIFIED);
        }
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        log("Loading rewarded ad for zone id: " + thirdPartyAdPlacementId + "...");
        if (!isAdColonyConfigured()) {
            log("AdColony SDK is not initialized");
            maxRewardedAdapterListener.onRewardedAdLoadFailed(MaxAdapterError.NOT_INITIALIZED);
            return;
        }
        AdColony.setAppOptions(getOptions(maxAdapterResponseParameters));
        this.rewardedAdListener = new RewardedAdListener(maxRewardedAdapterListener);
        AdColony.setRewardListener(this.rewardedAdListener);
        AdColony.requestInterstitial(thirdPartyAdPlacementId, this.rewardedAdListener);
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        log("Showing rewarded ad...");
        if (this.loadedRewardedAd == null) {
            log("Rewarded ad not ready");
            maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.UNSPECIFIED);
        } else if (this.loadedRewardedAd.isExpired()) {
            log("Rewarded ad is expired");
            maxRewardedAdapterListener.onRewardedAdDisplayFailed(new MaxAdapterError(MaxAdapterError.ERROR_CODE_AD_EXPIRED));
        } else {
            configureReward(maxAdapterResponseParameters);
            if (!this.loadedRewardedAd.show()) {
                log("Rewarded ad failed to display");
                maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.UNSPECIFIED);
            }
        }
    }

    private boolean isAdColonyConfigured() {
        return !AdColony.getSDKVersion().isEmpty();
    }

    private String[] getZoneIds(MaxAdapterParameters maxAdapterParameters) {
        ArrayList<String> stringArrayList = maxAdapterParameters.getServerParameters().getStringArrayList("zone_ids");
        if (stringArrayList == null || stringArrayList.size() == 0) {
            return new String[0];
        }
        String[] strArr = new String[stringArrayList.size()];
        for (int i = 0; i < stringArrayList.size(); i++) {
            strArr[i] = stringArrayList.get(i);
        }
        return strArr;
    }

    private AdColonyAppOptions getOptions(MaxAdapterParameters maxAdapterParameters) {
        Bundle serverParameters = maxAdapterParameters.getServerParameters();
        AdColonyAppOptions adColonyAppOptions = new AdColonyAppOptions();
        adColonyAppOptions.setTestModeEnabled(maxAdapterParameters.isTesting());
        if (serverParameters.getBoolean("set_mediation_identifier", true)) {
            adColonyAppOptions.setMediationNetwork("AppLovin", AppLovinSdk.VERSION);
        }
        adColonyAppOptions.setGDPRConsentString(maxAdapterParameters.hasUserConsent() ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        if (serverParameters.containsKey("gdpr_applies")) {
            adColonyAppOptions.setGDPRRequired(serverParameters.getBoolean("gdpr_applies"));
        }
        if (serverParameters.containsKey("app_orientation")) {
            adColonyAppOptions.setAppOrientation(serverParameters.getInt("app_orientation"));
        }
        if (serverParameters.containsKey(TapjoyConstants.TJC_APP_VERSION_NAME)) {
            adColonyAppOptions.setAppVersion(serverParameters.getString(TapjoyConstants.TJC_APP_VERSION_NAME));
        }
        if (serverParameters.containsKey("keep_screen_on")) {
            adColonyAppOptions.setKeepScreenOn(serverParameters.getBoolean("keep_screen_on"));
        }
        if (serverParameters.containsKey("multi_window_enabled")) {
            adColonyAppOptions.setMultiWindowEnabled(serverParameters.getBoolean("multi_window_enabled"));
        }
        if (serverParameters.containsKey("origin_store")) {
            adColonyAppOptions.setOriginStore(serverParameters.getString("origin_store"));
        }
        if (serverParameters.containsKey("requested_ad_orientation")) {
            adColonyAppOptions.setRequestedAdOrientation(serverParameters.getInt("requested_ad_orientation"));
        }
        if (serverParameters.containsKey(TapjoyConstants.TJC_PLUGIN) && serverParameters.containsKey("plugin_version")) {
            adColonyAppOptions.setPlugin(serverParameters.getString(TapjoyConstants.TJC_PLUGIN), serverParameters.getString("plugin_version"));
        }
        if (serverParameters.containsKey("user_id")) {
            adColonyAppOptions.setUserID(serverParameters.getString("user_id"));
        }
        return adColonyAppOptions;
    }

    private class InterstitialListener extends AdColonyInterstitialListener {
        final MaxInterstitialAdapterListener listener;

        InterstitialListener(MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
            this.listener = maxInterstitialAdapterListener;
        }

        public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Interstitial loaded");
            AdColonyInterstitial unused = AdColonyMediationAdapter.this.loadedInterstitialAd = adColonyInterstitial;
            this.listener.onInterstitialAdLoaded();
        }

        public void onRequestNotFilled(AdColonyZone adColonyZone) {
            AdColonyMediationAdapter adColonyMediationAdapter = AdColonyMediationAdapter.this;
            adColonyMediationAdapter.log("Interstitial failed to fill for zone: " + adColonyZone.getZoneID());
            this.listener.onInterstitialAdLoadFailed(MaxAdapterError.NO_FILL);
        }

        public void onOpened(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Interstitial shown");
            this.listener.onInterstitialAdDisplayed();
        }

        public void onClosed(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Interstitial hidden");
            this.listener.onInterstitialAdHidden();
        }

        public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter adColonyMediationAdapter = AdColonyMediationAdapter.this;
            adColonyMediationAdapter.log("Interstitial expiring: " + adColonyInterstitial.getZoneID());
        }

        public void onLeftApplication(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Interstitial left application");
        }

        public void onClicked(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Interstitial clicked");
            this.listener.onInterstitialAdClicked();
        }
    }

    private class RewardedAdListener extends AdColonyInterstitialListener implements AdColonyRewardListener {
        private boolean hasGrantedReward;
        private final MaxRewardedAdapterListener listener;

        RewardedAdListener(MaxRewardedAdapterListener maxRewardedAdapterListener) {
            this.listener = maxRewardedAdapterListener;
        }

        public void onReward(AdColonyReward adColonyReward) {
            AdColonyMediationAdapter.this.log("Rewarded ad granted reward");
            this.hasGrantedReward = true;
        }

        public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Rewarded ad loaded");
            AdColonyInterstitial unused = AdColonyMediationAdapter.this.loadedRewardedAd = adColonyInterstitial;
            this.listener.onRewardedAdLoaded();
        }

        public void onRequestNotFilled(AdColonyZone adColonyZone) {
            AdColonyMediationAdapter adColonyMediationAdapter = AdColonyMediationAdapter.this;
            adColonyMediationAdapter.log("Rewarded ad failed to fill for zone: " + adColonyZone.getZoneID());
            this.listener.onRewardedAdLoadFailed(MaxAdapterError.NO_FILL);
        }

        public void onOpened(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Rewarded ad shown");
            this.listener.onRewardedAdDisplayed();
            this.listener.onRewardedAdVideoStarted();
        }

        public void onClosed(AdColonyInterstitial adColonyInterstitial) {
            this.listener.onRewardedAdVideoCompleted();
            if (this.hasGrantedReward || AdColonyMediationAdapter.this.shouldAlwaysRewardUser()) {
                MaxReward reward = AdColonyMediationAdapter.this.getReward();
                AdColonyMediationAdapter adColonyMediationAdapter = AdColonyMediationAdapter.this;
                adColonyMediationAdapter.log("Rewarded user with reward: " + reward);
                this.listener.onUserRewarded(reward);
            }
            AdColonyMediationAdapter.this.log("Rewarded ad hidden");
            this.listener.onRewardedAdHidden();
        }

        public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter adColonyMediationAdapter = AdColonyMediationAdapter.this;
            adColonyMediationAdapter.log("Rewarded ad expiring: " + adColonyInterstitial.getZoneID());
        }

        public void onLeftApplication(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Rewarded ad left application");
        }

        public void onClicked(AdColonyInterstitial adColonyInterstitial) {
            AdColonyMediationAdapter.this.log("Rewarded ad clicked");
            this.listener.onRewardedAdClicked();
        }
    }
}
