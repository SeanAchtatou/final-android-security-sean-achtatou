package com.applovin.mediation.adapters;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.sdk.AppLovinSdk;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyRewardedVideoListener;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.mintegral.msdk.MIntegralConstans;
import java.util.concurrent.atomic.AtomicBoolean;

public class IronSourceMediationAdapter extends MediationAdapterBase implements MaxInterstitialAdapter, MaxRewardedAdapter {
    private static final AtomicBoolean INITIALIZED = new AtomicBoolean();
    private static final IronSourceRouter ROUTER = new IronSourceRouter();
    private static final Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
            if (!activity.getClass().getName().startsWith("com.ironsource.sdk")) {
                IronSource.onResume(activity);
            }
        }

        public void onActivityPaused(Activity activity) {
            if (!activity.getClass().getName().startsWith("com.ironsource.sdk")) {
                IronSource.onPause(activity);
            }
        }
    };
    private String mRouterPlacementIdentifier;

    public String getAdapterVersion() {
        return "6.11.1.0.1";
    }

    public IronSourceMediationAdapter(AppLovinSdk appLovinSdk) {
        super(appLovinSdk);
    }

    public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        checkExistence(InterstitialListener.class, RewardedVideoListener.class);
        if (INITIALIZED.compareAndSet(false, true)) {
            if (maxAdapterInitializationParameters.getServerParameters().getBoolean("set_mediation_identifier")) {
                IronSource.setMediationType(mediationTag());
            }
            IronSource.setAdaptersDebug(maxAdapterInitializationParameters.isTesting());
            IronSource.setConsent(maxAdapterInitializationParameters.hasUserConsent());
            IronSource.setISDemandOnlyInterstitialListener(ROUTER);
            IronSource.setISDemandOnlyRewardedVideoListener(ROUTER);
            if (maxAdapterInitializationParameters.getServerParameters().getBoolean("should_track_network_state", false)) {
                IronSource.shouldTrackNetworkState(activity, true);
            }
            IronSource.initISDemandOnly(activity, maxAdapterInitializationParameters.getServerParameters().getString(MIntegralConstans.APP_KEY), IronSource.AD_UNIT.INTERSTITIAL, IronSource.AD_UNIT.REWARDED_VIDEO);
            activity.getApplication().registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        }
        if (AppLovinSdk.VERSION_CODE >= 90800) {
            onCompletionListener.onCompletion(MaxAdapter.InitializationStatus.DOES_NOT_APPLY, null);
        } else {
            onCompletionListener.onCompletion();
        }
    }

    public String getSdkVersion() {
        return IronSourceUtils.getSDKVersion();
    }

    public void onDestroy() {
        ROUTER.removeAdapter(this, this.mRouterPlacementIdentifier);
    }

    public void loadInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        IronSource.setConsent(maxAdapterResponseParameters.hasUserConsent());
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        log("Loading ironSource interstitial for instance ID: " + thirdPartyAdPlacementId);
        this.mRouterPlacementIdentifier = IronSourceRouter.getInterstitialRouterIdentifier(thirdPartyAdPlacementId);
        ROUTER.addInterstitialAdapter(this, maxInterstitialAdapterListener, this.mRouterPlacementIdentifier);
        if (IronSource.isISDemandOnlyInterstitialReady(thirdPartyAdPlacementId)) {
            log("Ad is available already for instance ID: " + thirdPartyAdPlacementId);
            ROUTER.onAdLoaded(this.mRouterPlacementIdentifier);
            return;
        }
        IronSource.loadISDemandOnlyInterstitial(thirdPartyAdPlacementId);
    }

    public void showInterstitialAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxInterstitialAdapterListener maxInterstitialAdapterListener) {
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        log("Showing ironSource interstitial for instance ID: " + thirdPartyAdPlacementId);
        if (IronSource.isISDemandOnlyInterstitialReady(thirdPartyAdPlacementId)) {
            ROUTER.addShowingAdapter(this);
            IronSource.showISDemandOnlyInterstitial(thirdPartyAdPlacementId);
            return;
        }
        log("Unable to show ironSource interstitial - no ad loaded for instance ID: " + thirdPartyAdPlacementId);
        maxInterstitialAdapterListener.onInterstitialAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    public void loadRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        IronSource.setConsent(maxAdapterResponseParameters.hasUserConsent());
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        log("Loading ironSource rewarded for instance ID: " + thirdPartyAdPlacementId);
        String access$200 = IronSourceRouter.getRewardedVideoRouterIdentifier(thirdPartyAdPlacementId);
        ROUTER.addRewardedAdapter(this, maxRewardedAdapterListener, access$200);
        if (IronSource.isISDemandOnlyRewardedVideoAvailable(thirdPartyAdPlacementId)) {
            log("Ad is available already for instance ID: " + thirdPartyAdPlacementId);
            ROUTER.onAdLoaded(access$200);
            return;
        }
        IronSource.loadISDemandOnlyRewardedVideo(thirdPartyAdPlacementId);
    }

    public void showRewardedAd(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity, MaxRewardedAdapterListener maxRewardedAdapterListener) {
        String thirdPartyAdPlacementId = maxAdapterResponseParameters.getThirdPartyAdPlacementId();
        log("Showing ironSource rewarded for instance ID: " + thirdPartyAdPlacementId);
        if (IronSource.isISDemandOnlyRewardedVideoAvailable(thirdPartyAdPlacementId)) {
            configureReward(maxAdapterResponseParameters);
            ROUTER.addShowingAdapter(this);
            IronSource.showISDemandOnlyRewardedVideo(thirdPartyAdPlacementId);
            return;
        }
        log("Unable to show ironSource rewarded - no ad loaded...");
        maxRewardedAdapterListener.onRewardedAdDisplayFailed(MaxAdapterError.AD_NOT_READY);
    }

    private static class IronSourceRouter extends MediationAdapterRouter implements ISDemandOnlyInterstitialListener, ISDemandOnlyRewardedVideoListener {
        private boolean hasGrantedReward;

        /* access modifiers changed from: package-private */
        public void initialize(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity, MaxAdapter.OnCompletionListener onCompletionListener) {
        }

        private IronSourceRouter() {
        }

        public void onInterstitialAdReady(String str) {
            log("Interstitial loaded for instance ID: " + str);
            onAdLoaded(getInterstitialRouterIdentifier(str));
        }

        public void onInterstitialAdLoadFailed(String str, IronSourceError ironSourceError) {
            log("Interstitial ad failed to load for instance ID: " + str + " with error: " + ironSourceError);
            onAdLoadFailed(getInterstitialRouterIdentifier(str), toMaxError(ironSourceError));
        }

        public void onInterstitialAdOpened(String str) {
            log("Interstitial ad displayed for instance ID: " + str);
            onAdDisplayed(getInterstitialRouterIdentifier(str));
        }

        public void onInterstitialAdClosed(String str) {
            log("Interstitial ad closed for instance ID: " + str);
            onAdHidden(getInterstitialRouterIdentifier(str));
        }

        public void onInterstitialAdShowFailed(String str, IronSourceError ironSourceError) {
            log("Interstitial ad failed to show for instance ID: " + str + " with error: " + ironSourceError);
            onAdDisplayFailed(getInterstitialRouterIdentifier(str), toMaxError(ironSourceError));
        }

        public void onInterstitialAdClicked(String str) {
            log("Interstitial ad clicked for instance ID: " + str);
            onAdClicked(getInterstitialRouterIdentifier(str));
        }

        public void onRewardedVideoAdOpened(String str) {
            log("Rewarded ad shown for instance ID: " + str);
            String rewardedVideoRouterIdentifier = getRewardedVideoRouterIdentifier(str);
            onAdDisplayed(rewardedVideoRouterIdentifier);
            onRewardedAdVideoStarted(rewardedVideoRouterIdentifier);
        }

        public void onRewardedVideoAdClosed(String str) {
            String rewardedVideoRouterIdentifier = getRewardedVideoRouterIdentifier(str);
            if (this.hasGrantedReward || shouldAlwaysRewardUser(rewardedVideoRouterIdentifier)) {
                MaxReward reward = getReward(rewardedVideoRouterIdentifier);
                log("Rewarded  ad rewarded user with reward: " + reward + " for instance ID: " + str);
                onUserRewarded(rewardedVideoRouterIdentifier, reward);
                this.hasGrantedReward = false;
            }
            log("Rewarded ad hidden for instance ID: " + str);
            onAdHidden(rewardedVideoRouterIdentifier);
        }

        public void onRewardedVideoAdLoadSuccess(String str) {
            log("Rewarded ad loaded for instance ID: " + str);
            onAdLoaded(getRewardedVideoRouterIdentifier(str));
        }

        public void onRewardedVideoAdLoadFailed(String str, IronSourceError ironSourceError) {
            log("Rewarded ad failed to load for instance ID: " + str);
            onAdLoadFailed(getRewardedVideoRouterIdentifier(str), toMaxError(ironSourceError));
        }

        public void onRewardedVideoAdRewarded(String str) {
            log("Rewarded ad granted reward for instance ID: " + str);
            onRewardedAdVideoCompleted(getRewardedVideoRouterIdentifier(str));
            this.hasGrantedReward = true;
        }

        public void onRewardedVideoAdShowFailed(String str, IronSourceError ironSourceError) {
            log("Rewarded ad failed to show for instance ID: " + str + " with error: " + ironSourceError);
            onAdDisplayFailed(getRewardedVideoRouterIdentifier(str), toMaxError(ironSourceError));
        }

        public void onRewardedVideoAdClicked(String str) {
            log("Rewarded ad clicked for instance ID: " + str);
            onAdClicked(getRewardedVideoRouterIdentifier(str));
        }

        private MaxAdapterError toMaxError(IronSourceError ironSourceError) {
            int errorCode = ironSourceError.getErrorCode();
            return new MaxAdapterError((501 == errorCode || 505 == errorCode || 506 == errorCode) ? MaxAdapterError.ERROR_CODE_INVALID_CONFIGURATION : 508 == errorCode ? MaxAdapterError.ERROR_CODE_NOT_INITIALIZED : 509 == errorCode ? 204 : 520 == errorCode ? MaxAdapterError.ERROR_CODE_NO_CONNECTION : (524 == errorCode || 526 == errorCode) ? MaxAdapterError.ERROR_CODE_AD_FREQUENCY_CAPPED : MaxAdapterError.ERROR_CODE_UNSPECIFIED, errorCode);
        }

        /* access modifiers changed from: private */
        public static String getInterstitialRouterIdentifier(String str) {
            return str + "-" + IronSource.AD_UNIT.INTERSTITIAL.toString();
        }

        /* access modifiers changed from: private */
        public static String getRewardedVideoRouterIdentifier(String str) {
            return str + "-" + IronSource.AD_UNIT.REWARDED_VIDEO.toString();
        }
    }
}
