package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.s;
import com.applovin.impl.sdk.utils.t;
import com.applovin.sdk.AppLovinErrorCodes;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

public class a {
    private final j a;
    private final p b;
    private b c;

    /* renamed from: com.applovin.impl.sdk.network.a$a  reason: collision with other inner class name */
    public static class C0006a {
        private long a;
        private long b;

        /* access modifiers changed from: private */
        public void a(long j) {
            this.a = j;
        }

        /* access modifiers changed from: private */
        public void b(long j) {
            this.b = j;
        }

        public long a() {
            return this.a;
        }

        public long b() {
            return this.b;
        }
    }

    public static class b {
        private final long a = System.currentTimeMillis();
        private final String b;
        private final long c;
        private final long d;

        b(String str, long j, long j2) {
            this.b = str;
            this.c = j;
            this.d = j2;
        }

        public long a() {
            return this.a;
        }

        public String b() {
            return this.b;
        }

        public long c() {
            return this.c;
        }

        public long d() {
            return this.d;
        }

        public String toString() {
            return "RequestMeasurement{timestampMillis=" + this.a + ", urlHostAndPathString='" + this.b + '\'' + ", responseSize=" + this.c + ", connectionTimeMillis=" + this.d + '}';
        }
    }

    public interface c<T> {
        void a(int i);

        void a(Object obj, int i);
    }

    public a(j jVar) {
        this.a = jVar;
        this.b = jVar.u();
    }

    private int a(Throwable th) {
        if (th instanceof UnknownHostException) {
            return AppLovinErrorCodes.NO_NETWORK;
        }
        if (th instanceof SocketTimeoutException) {
            return AppLovinErrorCodes.FETCH_AD_TIMEOUT;
        }
        if (th instanceof IOException) {
            return -100;
        }
        return th instanceof JSONException ? -104 : -1;
    }

    private <T> T a(String str, T t) throws JSONException, SAXException, ClassCastException {
        if (t == null) {
            return str;
        }
        if (str != null && str.length() >= 3) {
            if (t instanceof JSONObject) {
                return new JSONObject(str);
            }
            if (t instanceof s) {
                return t.a(str, this.a);
            }
            if (t instanceof String) {
                return str;
            }
            p pVar = this.b;
            pVar.e("ConnectionManager", "Failed to process response of type '" + t.getClass().getName() + "'");
        }
        return t;
    }

    private HttpURLConnection a(String str, String str2, Map<String, String> map, int i) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i < 0 ? ((Integer) this.a.a(d.dw)).intValue() : i);
        if (i < 0) {
            i = ((Integer) this.a.a(d.dx)).intValue();
        }
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        if (map != null && map.size() > 0) {
            for (String next : map.keySet()) {
                httpURLConnection.setRequestProperty("AppLovin-" + next, map.get(next));
            }
        }
        return httpURLConnection;
    }

    private void a(int i, String str) {
        if (((Boolean) this.a.a(d.af)).booleanValue()) {
            try {
                d.a(i, str, this.a.C());
            } catch (Throwable th) {
                p u = this.a.u();
                u.b("ConnectionManager", "Failed to track response code for URL: " + str, th);
            }
        }
    }

    private void a(String str) {
        h hVar;
        g gVar;
        if (n.a(str, com.applovin.impl.sdk.utils.h.g(this.a)) || n.a(str, com.applovin.impl.sdk.utils.h.h(this.a))) {
            hVar = this.a.K();
            gVar = g.h;
        } else if (n.a(str, com.applovin.impl.mediation.d.b.a(this.a)) || n.a(str, com.applovin.impl.mediation.d.b.b(this.a))) {
            hVar = this.a.K();
            gVar = g.n;
        } else {
            hVar = this.a.K();
            gVar = g.i;
        }
        hVar.a(gVar);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r8v0, types: [T, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r8v2, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v7, types: [com.applovin.impl.sdk.utils.s] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=T, code=org.json.JSONObject, for r8v0, types: [T, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <T> void a(java.lang.String r5, int r6, java.lang.String r7, org.json.JSONObject r8, boolean r9, com.applovin.impl.sdk.network.a.c<T> r10) {
        /*
            r4 = this;
            com.applovin.impl.sdk.p r0 = r4.b
            java.lang.String r1 = "ConnectionManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r6)
            java.lang.String r3 = " received from \""
            r2.append(r3)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            r0.b(r1, r2)
            com.applovin.impl.sdk.p r0 = r4.b
            java.lang.String r1 = "ConnectionManager"
            r0.a(r1, r5)
            r0 = 200(0xc8, float:2.8E-43)
            if (r6 < r0) goto L_0x00bf
            r0 = 300(0x12c, float:4.2E-43)
            if (r6 >= r0) goto L_0x00bf
            if (r9 == 0) goto L_0x0036
            com.applovin.impl.sdk.j r9 = r4.a
            java.lang.String r9 = r9.s()
            java.lang.String r5 = com.applovin.impl.sdk.utils.l.a(r5, r9)
        L_0x0036:
            if (r5 == 0) goto L_0x0041
            int r9 = r5.length()
            r0 = 2
            if (r9 <= r0) goto L_0x0041
            r9 = 1
            goto L_0x0042
        L_0x0041:
            r9 = 0
        L_0x0042:
            r0 = 204(0xcc, float:2.86E-43)
            if (r6 == r0) goto L_0x00bb
            if (r9 == 0) goto L_0x00bb
            boolean r9 = r8 instanceof java.lang.String     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            if (r9 == 0) goto L_0x004e
        L_0x004c:
            r8 = r5
            goto L_0x00bb
        L_0x004e:
            boolean r9 = r8 instanceof com.applovin.impl.sdk.utils.s     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            if (r9 == 0) goto L_0x0059
            com.applovin.impl.sdk.j r9 = r4.a     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            com.applovin.impl.sdk.utils.s r5 = com.applovin.impl.sdk.utils.t.a(r5, r9)     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            goto L_0x004c
        L_0x0059:
            boolean r9 = r8 instanceof org.json.JSONObject     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            if (r9 == 0) goto L_0x0064
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            r9.<init>(r5)     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            r8 = r9
            goto L_0x00bb
        L_0x0064:
            com.applovin.impl.sdk.p r5 = r4.b     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            java.lang.String r9 = "ConnectionManager"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            r0.<init>()     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            java.lang.String r1 = "Unable to handle '"
            r0.append(r1)     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            java.lang.Class r1 = r8.getClass()     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            java.lang.String r1 = r1.getName()     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            r0.append(r1)     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            java.lang.String r1 = "'"
            r0.append(r1)     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            r5.e(r9, r0)     // Catch:{ JSONException -> 0x009a, SAXException -> 0x008a }
            goto L_0x00bb
        L_0x008a:
            r5 = move-exception
            r4.a(r7)
            com.applovin.impl.sdk.p r9 = r4.b
            java.lang.String r0 = "ConnectionManager"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid XML returned from \""
            goto L_0x00a9
        L_0x009a:
            r5 = move-exception
            r4.a(r7)
            com.applovin.impl.sdk.p r9 = r4.b
            java.lang.String r0 = "ConnectionManager"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid JSON returned from \""
        L_0x00a9:
            r1.append(r2)
            r1.append(r7)
            java.lang.String r7 = "\""
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            r9.b(r0, r7, r5)
        L_0x00bb:
            r10.a(r8, r6)
            goto L_0x00e2
        L_0x00bf:
            com.applovin.impl.sdk.p r5 = r4.b
            java.lang.String r8 = "ConnectionManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r6)
            java.lang.String r0 = " error received from \""
            r9.append(r0)
            r9.append(r7)
            java.lang.String r7 = "\""
            r9.append(r7)
            java.lang.String r7 = r9.toString()
            r5.e(r8, r7)
            r10.a(r6)
        L_0x00e2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.a.a(java.lang.String, int, java.lang.String, java.lang.Object, boolean, com.applovin.impl.sdk.network.a$c):void");
    }

    private void a(String str, String str2, int i, long j) {
        p pVar = this.b;
        pVar.c("ConnectionManager", "Successful " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s over " + com.applovin.impl.sdk.utils.h.f(this.a) + " to \"" + str2 + "\"");
    }

    private void a(String str, String str2, int i, long j, Throwable th) {
        p pVar = this.b;
        pVar.b("ConnectionManager", "Failed " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s over " + com.applovin.impl.sdk.utils.h.f(this.a) + " to \"" + str2 + "\"", th);
    }

    public b a() {
        return this.c;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:85:0x0217 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v0, resolved type: long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v6, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v7, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v8, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v10, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v11, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v12, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v13, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v14, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v15, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v16, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v17, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v18, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v19, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v20, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v21, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v22, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v23, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v24, resolved type: long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v30, resolved type: long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v31, resolved type: long} */
    /* JADX WARN: Type inference failed for: r13v29 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.InputStream, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x02a6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        r1 = r8.b;
        r1.b("ConnectionManager", "Unable to parse response from \"" + r12 + "\"", r0);
        r8.a.K().a(com.applovin.impl.sdk.c.g.i);
        r10.a(-800);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02ef, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02f1, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02f2, code lost:
        r13 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0331, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0332, code lost:
        r5 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0336, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0338, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x033a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x033f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0340, code lost:
        r23 = r13;
        r7 = r0;
        r9 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0349, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x034a, code lost:
        r16 = r5;
        r23 = r13;
        r7 = r0;
        r13 = null;
        r15 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x036a, code lost:
        if (r26.h() == false) goto L_0x0378;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x036d, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x036e, code lost:
        r5 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:?, code lost:
        a(r11, r12, r15, r23);
        r10.a(r26.g(), -901);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:?, code lost:
        r15 = a(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x03d2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x03d3, code lost:
        r5 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0199, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x019a, code lost:
        r7 = r0;
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x019f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01a0, code lost:
        r7 = r0;
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01fd, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01fe, code lost:
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0200, code lost:
        r7 = r0;
        r13 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0204, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0205, code lost:
        r23 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0207, code lost:
        r7 = r0;
        r13 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0211, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0213, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x022b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x022e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:51:0x0195, B:144:0x0328] */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:88:0x0226, B:114:0x0299] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x02ef A[ExcHandler: MalformedURLException (e java.net.MalformedURLException), PHI: r13 r23 
      PHI: (r13v19 java.io.InputStream) = (r13v20 java.io.InputStream), (r13v20 java.io.InputStream), (r13v20 java.io.InputStream), (r13v22 java.io.InputStream), (r13v23 java.io.InputStream) binds: [B:114:0x0299, B:117:0x02a7, B:115:?, B:85:0x0217, B:88:0x0226] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r23v21 long) = (r23v22 long), (r23v22 long), (r23v22 long), (r23v24 long), (r23v24 long) binds: [B:114:0x0299, B:117:0x02a7, B:115:?, B:85:0x0217, B:88:0x0226] A[DONT_GENERATE, DONT_INLINE], Splitter:B:88:0x0226] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x02f1 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:60:0x01ac] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0331 A[ExcHandler: all (th java.lang.Throwable), PHI: r16 
      PHI: (r16v9 java.net.HttpURLConnection) = (r16v10 java.net.HttpURLConnection), (r16v10 java.net.HttpURLConnection), (r16v19 java.net.HttpURLConnection), (r16v21 java.net.HttpURLConnection), (r16v21 java.net.HttpURLConnection) binds: [B:144:0x0328, B:145:?, B:58:0x01a8, B:51:0x0195, B:52:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:51:0x0195] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x033a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:44:0x0176] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0366 A[SYNTHETIC, Splitter:B:161:0x0366] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x036d A[ExcHandler: all (th java.lang.Throwable), PHI: r13 r16 
      PHI: (r13v9 java.io.InputStream) = (r13v10 java.io.InputStream), (r13v10 java.io.InputStream), (r13v10 java.io.InputStream), (r13v20 java.io.InputStream), (r13v20 java.io.InputStream) binds: [B:173:0x0385, B:174:?, B:161:0x0366, B:114:0x0299, B:115:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r16v4 java.net.HttpURLConnection) = (r16v5 java.net.HttpURLConnection), (r16v5 java.net.HttpURLConnection), (r16v5 java.net.HttpURLConnection), (r16v19 java.net.HttpURLConnection), (r16v19 java.net.HttpURLConnection) binds: [B:173:0x0385, B:174:?, B:161:0x0366, B:114:0x0299, B:115:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:114:0x0299] */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x038c  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x03cc A[SYNTHETIC, Splitter:B:194:0x03cc] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:85:0x0217=Splitter:B:85:0x0217, B:114:0x0299=Splitter:B:114:0x0299} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> void a(com.applovin.impl.sdk.network.b<T> r26, com.applovin.impl.sdk.network.a.C0006a r27, com.applovin.impl.sdk.network.a.c<T> r28) {
        /*
            r25 = this;
            r8 = r25
            r0 = r27
            r10 = r28
            if (r26 == 0) goto L_0x0413
            java.lang.String r1 = r26.a()
            java.lang.String r11 = r26.b()
            if (r1 == 0) goto L_0x040b
            if (r11 == 0) goto L_0x0403
            if (r10 == 0) goto L_0x03fb
            java.lang.String r2 = r1.toLowerCase()
            java.lang.String r3 = "http"
            boolean r2 = r2.startsWith(r3)
            if (r2 != 0) goto L_0x0043
            java.lang.String r0 = "ConnectionManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Requested postback submission to non HTTP endpoint "
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = "; skipping..."
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.applovin.impl.sdk.p.j(r0, r1)
            r0 = -900(0xfffffffffffffc7c, float:NaN)
            r10.a(r0)
            return
        L_0x0043:
            com.applovin.impl.sdk.j r2 = r8.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r3 = com.applovin.impl.sdk.b.d.dy
            java.lang.Object r2 = r2.a(r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x0070
            java.lang.String r2 = "https://"
            boolean r2 = r1.contains(r2)
            if (r2 != 0) goto L_0x0070
            com.applovin.impl.sdk.j r2 = r8.a
            com.applovin.impl.sdk.p r2 = r2.u()
            java.lang.String r3 = "ConnectionManager"
            java.lang.String r4 = "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting..."
            r2.d(r3, r4)
            java.lang.String r2 = "http://"
            java.lang.String r3 = "https://"
            java.lang.String r1 = r1.replace(r2, r3)
        L_0x0070:
            boolean r7 = r26.m()
            com.applovin.impl.sdk.j r2 = r8.a
            long r2 = com.applovin.impl.sdk.utils.q.a(r2)
            java.util.Map r4 = r26.c()
            if (r4 == 0) goto L_0x008a
            java.util.Map r4 = r26.c()
            boolean r4 = r4.isEmpty()
            if (r4 == 0) goto L_0x0090
        L_0x008a:
            int r4 = r26.i()
            if (r4 < 0) goto L_0x00c4
        L_0x0090:
            java.util.Map r4 = r26.c()
            if (r4 == 0) goto L_0x00a9
            int r5 = r26.i()
            if (r5 < 0) goto L_0x00a9
            java.lang.String r5 = "current_retry_attempt"
            int r6 = r26.i()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r4.put(r5, r6)
        L_0x00a9:
            if (r7 == 0) goto L_0x00c0
            java.lang.String r4 = com.applovin.impl.sdk.utils.q.a(r4)
            com.applovin.impl.sdk.j r5 = r8.a
            java.lang.String r5 = r5.s()
            java.lang.String r4 = com.applovin.impl.sdk.utils.l.a(r4, r5, r2)
            java.lang.String r5 = "p"
            java.lang.String r1 = com.applovin.impl.sdk.utils.n.a(r1, r5, r4)
            goto L_0x00c4
        L_0x00c0:
            java.lang.String r1 = com.applovin.impl.sdk.utils.n.b(r1, r4)
        L_0x00c4:
            r12 = r1
            long r13 = java.lang.System.currentTimeMillis()
            com.applovin.impl.sdk.p r4 = r8.b     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.lang.String r5 = "ConnectionManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            r6.<init>()     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.lang.String r1 = "Sending "
            r6.append(r1)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            r6.append(r11)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.lang.String r1 = " request to \""
            r6.append(r1)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            r6.append(r12)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.lang.String r1 = "\"..."
            r6.append(r1)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.lang.String r1 = r6.toString()     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            r4.c(r5, r1)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.util.Map r1 = r26.d()     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            int r4 = r26.k()     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            java.net.HttpURLConnection r5 = r8.a(r12, r11, r1, r4)     // Catch:{ Throwable -> 0x03c3, all -> 0x03bf }
            org.json.JSONObject r1 = r26.e()     // Catch:{ Throwable -> 0x03b8, all -> 0x03b5 }
            if (r1 == 0) goto L_0x0176
            if (r7 == 0) goto L_0x011f
            org.json.JSONObject r1 = r26.e()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            com.applovin.impl.sdk.j r4 = r8.a     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r4 = r4.s()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r1 = com.applovin.impl.sdk.utils.l.a(r1, r4, r2)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            goto L_0x0127
        L_0x0115:
            r0 = move-exception
            goto L_0x03c1
        L_0x0118:
            r0 = move-exception
            r7 = r0
            r9 = r5
            r23 = r13
            goto L_0x03c8
        L_0x011f:
            org.json.JSONObject r1 = r26.e()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
        L_0x0127:
            com.applovin.impl.sdk.p r2 = r8.b     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r3 = "ConnectionManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r4.<init>()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r6 = "Request to \""
            r4.append(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r4.append(r12)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r6 = "\" is "
            r4.append(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r4.append(r1)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r2.b(r3, r4)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r3 = "application/json; charset=utf-8"
            r5.setRequestProperty(r2, r3)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r2 = 1
            r5.setDoOutput(r2)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r2 = "UTF-8"
            java.nio.charset.Charset r2 = java.nio.charset.Charset.forName(r2)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            byte[] r2 = r1.getBytes(r2)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            int r2 = r2.length     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r5.setFixedLengthStreamingMode(r2)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.io.PrintWriter r2 = new java.io.PrintWriter     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.io.OutputStream r4 = r5.getOutputStream()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            java.lang.String r6 = "UTF8"
            r3.<init>(r4, r6)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r2.print(r1)     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
            r2.close()     // Catch:{ Throwable -> 0x0118, all -> 0x0115 }
        L_0x0176:
            int r6 = r5.getResponseCode()     // Catch:{ MalformedURLException -> 0x0349, Throwable -> 0x033f, all -> 0x033a }
            if (r6 <= 0) goto L_0x031b
            com.applovin.impl.sdk.j r1 = r8.a     // Catch:{ MalformedURLException -> 0x0312, Throwable -> 0x0306, all -> 0x033a }
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.d.fc     // Catch:{ MalformedURLException -> 0x0312, Throwable -> 0x0306, all -> 0x033a }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ MalformedURLException -> 0x0312, Throwable -> 0x0306, all -> 0x033a }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ MalformedURLException -> 0x0312, Throwable -> 0x0306, all -> 0x033a }
            boolean r1 = r1.booleanValue()     // Catch:{ MalformedURLException -> 0x0312, Throwable -> 0x0306, all -> 0x033a }
            if (r1 == 0) goto L_0x01a5
            r1 = r25
            r2 = r11
            r3 = r12
            r4 = r6
            r16 = r5
            r15 = r6
            r5 = r13
            r1.a(r2, r3, r4, r5)     // Catch:{ MalformedURLException -> 0x019f, Throwable -> 0x0199, all -> 0x0331 }
            goto L_0x01a8
        L_0x0199:
            r0 = move-exception
            r7 = r0
            r23 = r13
            goto L_0x030d
        L_0x019f:
            r0 = move-exception
            r7 = r0
            r23 = r13
            goto L_0x0319
        L_0x01a5:
            r16 = r5
            r15 = r6
        L_0x01a8:
            java.io.InputStream r6 = r16.getInputStream()     // Catch:{ MalformedURLException -> 0x0304, Throwable -> 0x0302, all -> 0x0331 }
            r8.a(r15, r12)     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            r3 = 0
            long r4 = r1 - r13
            com.applovin.impl.sdk.j r1 = r8.a     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.d.fc     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            if (r1 == 0) goto L_0x023e
            com.applovin.impl.sdk.j r1 = r8.a     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            java.lang.String r2 = com.applovin.impl.sdk.utils.h.a(r6, r1)     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            boolean r1 = r26.h()     // Catch:{ MalformedURLException -> 0x02fb, Throwable -> 0x02f5, all -> 0x02f1 }
            if (r1 == 0) goto L_0x0231
            if (r0 == 0) goto L_0x0215
            if (r2 == 0) goto L_0x020b
            int r1 = r2.length()     // Catch:{ MalformedURLException -> 0x0204, Throwable -> 0x01fd, all -> 0x02f1 }
            r23 = r13
            long r13 = (long) r1
            r0.b(r13)     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            boolean r1 = r26.n()     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            if (r1 == 0) goto L_0x020d
            com.applovin.impl.sdk.network.a$b r1 = new com.applovin.impl.sdk.network.a$b     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            java.lang.String r18 = r26.a()     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            int r3 = r2.length()     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            long r13 = (long) r3     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            r17 = r1
            r19 = r13
            r21 = r4
            r17.<init>(r18, r19, r21)     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            r8.c = r1     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            goto L_0x020d
        L_0x01fd:
            r0 = move-exception
            r23 = r13
        L_0x0200:
            r7 = r0
            r13 = r6
            goto L_0x0374
        L_0x0204:
            r0 = move-exception
            r23 = r13
        L_0x0207:
            r7 = r0
            r13 = r6
            goto L_0x0351
        L_0x020b:
            r23 = r13
        L_0x020d:
            r0.a(r4)     // Catch:{ MalformedURLException -> 0x0213, Throwable -> 0x0211, all -> 0x02f1 }
            goto L_0x0217
        L_0x0211:
            r0 = move-exception
            goto L_0x0200
        L_0x0213:
            r0 = move-exception
            goto L_0x0207
        L_0x0215:
            r23 = r13
        L_0x0217:
            int r3 = r16.getResponseCode()     // Catch:{ MalformedURLException -> 0x022e, Throwable -> 0x022b, all -> 0x02f1 }
            java.lang.Object r5 = r26.g()     // Catch:{ MalformedURLException -> 0x022e, Throwable -> 0x022b, all -> 0x02f1 }
            r1 = r25
            r4 = r12
            r13 = r6
            r6 = r7
            r7 = r28
            r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ MalformedURLException -> 0x02ef }
            goto L_0x039d
        L_0x022b:
            r0 = move-exception
            goto L_0x02f8
        L_0x022e:
            r0 = move-exception
            goto L_0x02fe
        L_0x0231:
            r23 = r13
            r13 = r6
            if (r0 == 0) goto L_0x0239
            r0.a(r4)     // Catch:{ MalformedURLException -> 0x02ef }
        L_0x0239:
            r10.a(r2, r15)     // Catch:{ MalformedURLException -> 0x02ef }
            goto L_0x039d
        L_0x023e:
            r23 = r13
            r13 = r6
            r1 = 200(0xc8, float:2.8E-43)
            if (r15 < r1) goto L_0x02df
            r1 = 400(0x190, float:5.6E-43)
            if (r15 >= r1) goto L_0x02df
            if (r0 == 0) goto L_0x024e
            r0.a(r4)     // Catch:{ MalformedURLException -> 0x02ef }
        L_0x024e:
            r1 = r25
            r2 = r11
            r3 = r12
            r21 = r4
            r4 = r15
            r5 = r23
            r1.a(r2, r3, r4, r5)     // Catch:{ MalformedURLException -> 0x02ef }
            com.applovin.impl.sdk.j r1 = r8.a     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r1 = com.applovin.impl.sdk.utils.h.a(r13, r1)     // Catch:{ MalformedURLException -> 0x02ef }
            if (r1 == 0) goto L_0x02d6
            com.applovin.impl.sdk.p r2 = r8.b     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r3 = "ConnectionManager"
            r2.a(r3, r1)     // Catch:{ MalformedURLException -> 0x02ef }
            if (r0 == 0) goto L_0x0273
            int r2 = r1.length()     // Catch:{ MalformedURLException -> 0x02ef }
            long r2 = (long) r2     // Catch:{ MalformedURLException -> 0x02ef }
            r0.b(r2)     // Catch:{ MalformedURLException -> 0x02ef }
        L_0x0273:
            boolean r0 = r26.n()     // Catch:{ MalformedURLException -> 0x02ef }
            if (r0 == 0) goto L_0x028d
            com.applovin.impl.sdk.network.a$b r0 = new com.applovin.impl.sdk.network.a$b     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r18 = r26.a()     // Catch:{ MalformedURLException -> 0x02ef }
            int r2 = r1.length()     // Catch:{ MalformedURLException -> 0x02ef }
            long r2 = (long) r2     // Catch:{ MalformedURLException -> 0x02ef }
            r17 = r0
            r19 = r2
            r17.<init>(r18, r19, r21)     // Catch:{ MalformedURLException -> 0x02ef }
            r8.c = r0     // Catch:{ MalformedURLException -> 0x02ef }
        L_0x028d:
            if (r7 == 0) goto L_0x0299
            com.applovin.impl.sdk.j r0 = r8.a     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r0 = r0.s()     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r1 = com.applovin.impl.sdk.utils.l.a(r1, r0)     // Catch:{ MalformedURLException -> 0x02ef }
        L_0x0299:
            java.lang.Object r0 = r26.g()     // Catch:{ Throwable -> 0x02a6, MalformedURLException -> 0x02ef, all -> 0x036d }
            java.lang.Object r0 = r8.a(r1, r0)     // Catch:{ Throwable -> 0x02a6, MalformedURLException -> 0x02ef, all -> 0x036d }
            r10.a(r0, r15)     // Catch:{ Throwable -> 0x02a6, MalformedURLException -> 0x02ef, all -> 0x036d }
            goto L_0x039d
        L_0x02a6:
            r0 = move-exception
            com.applovin.impl.sdk.p r1 = r8.b     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r2 = "ConnectionManager"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x02ef }
            r3.<init>()     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r4 = "Unable to parse response from \""
            r3.append(r4)     // Catch:{ MalformedURLException -> 0x02ef }
            r3.append(r12)     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r4 = "\""
            r3.append(r4)     // Catch:{ MalformedURLException -> 0x02ef }
            java.lang.String r3 = r3.toString()     // Catch:{ MalformedURLException -> 0x02ef }
            r1.b(r2, r3, r0)     // Catch:{ MalformedURLException -> 0x02ef }
            com.applovin.impl.sdk.j r0 = r8.a     // Catch:{ MalformedURLException -> 0x02ef }
            com.applovin.impl.sdk.c.h r0 = r0.K()     // Catch:{ MalformedURLException -> 0x02ef }
            com.applovin.impl.sdk.c.g r1 = com.applovin.impl.sdk.c.g.i     // Catch:{ MalformedURLException -> 0x02ef }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x02ef }
            r0 = -800(0xfffffffffffffce0, float:NaN)
            r10.a(r0)     // Catch:{ MalformedURLException -> 0x02ef }
            goto L_0x039d
        L_0x02d6:
            java.lang.Object r0 = r26.g()     // Catch:{ MalformedURLException -> 0x02ef }
            r10.a(r0, r15)     // Catch:{ MalformedURLException -> 0x02ef }
            goto L_0x039d
        L_0x02df:
            r7 = 0
            r1 = r25
            r2 = r11
            r3 = r12
            r4 = r15
            r5 = r23
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ MalformedURLException -> 0x02ef }
            r10.a(r15)     // Catch:{ MalformedURLException -> 0x02ef }
            goto L_0x039d
        L_0x02ef:
            r0 = move-exception
            goto L_0x02ff
        L_0x02f1:
            r0 = move-exception
            r13 = r6
            goto L_0x036e
        L_0x02f5:
            r0 = move-exception
            r23 = r13
        L_0x02f8:
            r13 = r6
            goto L_0x0373
        L_0x02fb:
            r0 = move-exception
            r23 = r13
        L_0x02fe:
            r13 = r6
        L_0x02ff:
            r7 = r0
            goto L_0x0351
        L_0x0302:
            r0 = move-exception
            goto L_0x030a
        L_0x0304:
            r0 = move-exception
            goto L_0x0316
        L_0x0306:
            r0 = move-exception
            r16 = r5
            r15 = r6
        L_0x030a:
            r23 = r13
        L_0x030c:
            r7 = r0
        L_0x030d:
            r9 = r16
            r13 = 0
            goto L_0x03ca
        L_0x0312:
            r0 = move-exception
            r16 = r5
            r15 = r6
        L_0x0316:
            r23 = r13
        L_0x0318:
            r7 = r0
        L_0x0319:
            r13 = 0
            goto L_0x0351
        L_0x031b:
            r16 = r5
            r15 = r6
            r23 = r13
            r7 = 0
            r1 = r25
            r2 = r11
            r3 = r12
            r4 = r15
            r5 = r23
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ MalformedURLException -> 0x0338, Throwable -> 0x0336, all -> 0x0331 }
            r10.a(r15)     // Catch:{ MalformedURLException -> 0x0338, Throwable -> 0x0336, all -> 0x0331 }
            r13 = 0
            goto L_0x039d
        L_0x0331:
            r0 = move-exception
            r5 = r16
            goto L_0x03c1
        L_0x0336:
            r0 = move-exception
            goto L_0x030c
        L_0x0338:
            r0 = move-exception
            goto L_0x0318
        L_0x033a:
            r0 = move-exception
            r16 = r5
            goto L_0x03c1
        L_0x033f:
            r0 = move-exception
            r16 = r5
            r23 = r13
            r7 = r0
            r9 = r16
            goto L_0x03c8
        L_0x0349:
            r0 = move-exception
            r16 = r5
            r23 = r13
            r7 = r0
            r13 = 0
            r15 = 0
        L_0x0351:
            r0 = -901(0xfffffffffffffc7b, float:NaN)
            r8.a(r0, r12)     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            com.applovin.impl.sdk.j r1 = r8.a     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.d.fc     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            boolean r1 = r1.booleanValue()     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            if (r1 == 0) goto L_0x0378
            boolean r1 = r26.h()     // Catch:{ Throwable -> 0x0372, all -> 0x036d }
            if (r1 != 0) goto L_0x037e
            goto L_0x0378
        L_0x036d:
            r0 = move-exception
        L_0x036e:
            r5 = r16
            goto L_0x03f0
        L_0x0372:
            r0 = move-exception
        L_0x0373:
            r7 = r0
        L_0x0374:
            r9 = r16
            goto L_0x03ca
        L_0x0378:
            java.lang.Object r1 = r26.g()     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            if (r1 == 0) goto L_0x038c
        L_0x037e:
            r1 = r25
            r2 = r11
            r3 = r12
            r4 = r15
            r5 = r23
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ Throwable -> 0x0372, all -> 0x036d }
            r10.a(r0)     // Catch:{ Throwable -> 0x0372, all -> 0x036d }
            goto L_0x039d
        L_0x038c:
            r1 = r25
            r2 = r11
            r3 = r12
            r4 = r15
            r5 = r23
            r1.a(r2, r3, r4, r5)     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            java.lang.Object r1 = r26.g()     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
            r10.a(r1, r0)     // Catch:{ Throwable -> 0x03af, all -> 0x03aa }
        L_0x039d:
            com.applovin.impl.sdk.j r0 = r8.a
            com.applovin.impl.sdk.utils.q.a(r13, r0)
            com.applovin.impl.sdk.j r0 = r8.a
            r1 = r16
            com.applovin.impl.sdk.utils.q.a(r1, r0)
            goto L_0x03ef
        L_0x03aa:
            r0 = move-exception
            r1 = r16
            r5 = r1
            goto L_0x03f0
        L_0x03af:
            r0 = move-exception
            r1 = r16
            r7 = r0
            r9 = r1
            goto L_0x03ca
        L_0x03b5:
            r0 = move-exception
            r1 = r5
            goto L_0x03c1
        L_0x03b8:
            r0 = move-exception
            r1 = r5
            r23 = r13
            r7 = r0
            r9 = r1
            goto L_0x03c8
        L_0x03bf:
            r0 = move-exception
            r5 = 0
        L_0x03c1:
            r13 = 0
            goto L_0x03f0
        L_0x03c3:
            r0 = move-exception
            r23 = r13
            r7 = r0
            r9 = 0
        L_0x03c8:
            r13 = 0
            r15 = 0
        L_0x03ca:
            if (r15 != 0) goto L_0x03d5
            int r0 = r8.a(r7)     // Catch:{ all -> 0x03d2 }
            r15 = r0
            goto L_0x03d5
        L_0x03d2:
            r0 = move-exception
            r5 = r9
            goto L_0x03f0
        L_0x03d5:
            r8.a(r15, r12)     // Catch:{ all -> 0x03d2 }
            r1 = r25
            r2 = r11
            r3 = r12
            r4 = r15
            r5 = r23
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ all -> 0x03d2 }
            r10.a(r15)     // Catch:{ all -> 0x03d2 }
            com.applovin.impl.sdk.j r0 = r8.a
            com.applovin.impl.sdk.utils.q.a(r13, r0)
            com.applovin.impl.sdk.j r0 = r8.a
            com.applovin.impl.sdk.utils.q.a(r9, r0)
        L_0x03ef:
            return
        L_0x03f0:
            com.applovin.impl.sdk.j r1 = r8.a
            com.applovin.impl.sdk.utils.q.a(r13, r1)
            com.applovin.impl.sdk.j r1 = r8.a
            com.applovin.impl.sdk.utils.q.a(r5, r1)
            throw r0
        L_0x03fb:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No callback specified"
            r0.<init>(r1)
            throw r0
        L_0x0403:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No method specified"
            r0.<init>(r1)
            throw r0
        L_0x040b:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No endpoint specified"
            r0.<init>(r1)
            throw r0
        L_0x0413:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No request specified"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.a.a(com.applovin.impl.sdk.network.b, com.applovin.impl.sdk.network.a$a, com.applovin.impl.sdk.network.a$c):void");
    }
}
