package com.applovin.impl.sdk.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdk;
import com.google.android.gms.drive.DriveFile;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public abstract class q {
    private static Boolean a;

    public static double a(long j) {
        double d = (double) j;
        Double.isNaN(d);
        return d / 1000.0d;
    }

    public static float a(float f) {
        return f * 1000.0f;
    }

    public static int a(JSONObject jSONObject) {
        int b = i.b(jSONObject, "video_completion_percent", -1, (j) null);
        if (b < 0 || b > 100) {
            return 95;
        }
        return b;
    }

    public static long a(j jVar) {
        long longValue = ((Long) jVar.a(d.eT)).longValue();
        long longValue2 = ((Long) jVar.a(d.eU)).longValue();
        long currentTimeMillis = System.currentTimeMillis();
        return (longValue <= 0 || longValue2 <= 0) ? currentTimeMillis : currentTimeMillis + (longValue - longValue2);
    }

    public static Activity a(View view, j jVar) {
        if (view == null) {
            return null;
        }
        int i = 0;
        while (i < 1000) {
            i++;
            try {
                Context context = view.getContext();
                if (context instanceof Activity) {
                    return (Activity) context;
                }
                ViewParent parent = view.getParent();
                if (!(parent instanceof View)) {
                    return null;
                }
                view = (View) parent;
            } catch (Throwable th) {
                jVar.u().b("Utils", "Encountered error while retrieving activity from view", th);
            }
        }
        return null;
    }

    public static Bitmap a(Context context, int i, int i2) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            int i3 = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), i);
            if (options.outHeight > i2 || options.outWidth > i2) {
                double d = (double) i2;
                double max = (double) Math.max(options.outHeight, options.outWidth);
                Double.isNaN(d);
                Double.isNaN(max);
                i3 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d / max) / Math.log(0.5d))));
            }
            new BitmapFactory.Options().inSampleSize = i3;
            return BitmapFactory.decodeResource(context.getResources(), i);
        } catch (Exception unused) {
            return null;
        } finally {
            a((Closeable) null, (j) null);
            a((Closeable) null, (j) null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.FileInputStream, ?[OBJECT, ARRAY]]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    public static Bitmap a(File file, int i) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        BitmapFactory.Options options;
        try {
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            int i2 = 1;
            options2.inJustDecodeBounds = true;
            fileInputStream = new FileInputStream(file);
            try {
                BitmapFactory.decodeStream(fileInputStream, null, options2);
                fileInputStream.close();
                if (options2.outHeight > i || options2.outWidth > i) {
                    double d = (double) i;
                    double max = (double) Math.max(options2.outHeight, options2.outWidth);
                    Double.isNaN(d);
                    Double.isNaN(max);
                    i2 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d / max) / Math.log(0.5d))));
                }
                options = new BitmapFactory.Options();
                options.inSampleSize = i2;
                fileInputStream2 = new FileInputStream(file);
            } catch (Exception unused) {
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (j) null);
                a((Closeable) fileInputStream2, (j) null);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (j) null);
                a((Closeable) fileInputStream2, (j) null);
                throw th;
            }
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream2, null, options);
                fileInputStream2.close();
                a((Closeable) fileInputStream, (j) null);
                a((Closeable) fileInputStream2, (j) null);
                return decodeStream;
            } catch (Exception unused2) {
                a((Closeable) fileInputStream, (j) null);
                a((Closeable) fileInputStream2, (j) null);
                return null;
            } catch (Throwable th2) {
                th = th2;
                a((Closeable) fileInputStream, (j) null);
                a((Closeable) fileInputStream2, (j) null);
                throw th;
            }
        } catch (Exception unused3) {
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (j) null);
            a((Closeable) fileInputStream2, (j) null);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (j) null);
            a((Closeable) fileInputStream2, (j) null);
            throw th;
        }
    }

    public static View a(Context context, View view) {
        View c = c(context);
        return c != null ? c : a(view);
    }

    public static View a(View view) {
        View rootView;
        if (view == null || (rootView = view.getRootView()) == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        return findViewById != null ? findViewById : rootView;
    }

    public static com.applovin.impl.sdk.ad.d a(JSONObject jSONObject, j jVar) {
        return com.applovin.impl.sdk.ad.d.a(AppLovinAdSize.fromString(i.b(jSONObject, "ad_size", (String) null, jVar)), AppLovinAdType.fromString(i.b(jSONObject, "ad_type", (String) null, jVar)), i.b(jSONObject, "zone_id", (String) null, jVar), jVar);
    }

    public static j a(AppLovinSdk appLovinSdk) {
        try {
            Field declaredField = appLovinSdk.getClass().getDeclaredField("mSdkImpl");
            declaredField.setAccessible(true);
            return (j) declaredField.get(appLovinSdk);
        } catch (Throwable th) {
            throw new IllegalStateException("Internal error - unable to retrieve SDK implementation: " + th);
        }
    }

    public static AppLovinAd a(AppLovinAd appLovinAd, j jVar) {
        if (!(appLovinAd instanceof g)) {
            return appLovinAd;
        }
        g gVar = (g) appLovinAd;
        AppLovinAd dequeueAd = jVar.n().dequeueAd(gVar.getAdZone());
        p u = jVar.u();
        u.b("Utils", "Dequeued ad for dummy ad: " + dequeueAd);
        if (dequeueAd == null) {
            return gVar.a();
        }
        gVar.a(dequeueAd);
        ((AppLovinAdBase) dequeueAd).setDummyAd(gVar);
        return dequeueAd;
    }

    public static Object a(Object obj, j jVar) {
        int i;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            HashMap hashMap = new HashMap(map.size());
            for (Map.Entry entry : map.entrySet()) {
                Object key = entry.getKey();
                hashMap.put(key instanceof String ? (String) key : String.valueOf(key), a(entry.getValue(), jVar));
            }
            return hashMap;
        } else if (obj instanceof List) {
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            for (Object a2 : list) {
                arrayList.add(a(a2, jVar));
            }
            return arrayList;
        } else if (obj instanceof Date) {
            return String.valueOf(((Date) obj).getTime());
        } else {
            String valueOf = String.valueOf(obj);
            if (obj instanceof String) {
                i = ((Integer) jVar.a(d.aZ)).intValue();
                if (i <= 0 || valueOf.length() <= i) {
                    return valueOf;
                }
            } else if (!(obj instanceof Uri) || (i = ((Integer) jVar.a(d.ba)).intValue()) <= 0 || valueOf.length() <= i) {
                return valueOf;
            }
            return valueOf.substring(0, i);
        }
    }

    public static String a(f<String> fVar, j jVar) {
        return (String) jVar.a(fVar);
    }

    public static String a(String str) {
        return (str == null || str.length() <= 4) ? "NOKEY" : str.substring(str.length() - 4);
    }

    public static String a(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(Constants.RequestParameters.AMPERSAND);
            }
            sb.append(next.getKey());
            sb.append('=');
            sb.append(next.getValue());
        }
        return sb.toString();
    }

    public static Field a(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Class superclass = cls.getSuperclass();
            if (superclass == null) {
                return null;
            }
            return a(superclass, str);
        }
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, j jVar) {
        return a(str, jSONObject, str2, null, str3, jVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, String str4, j jVar) {
        HashMap hashMap = new HashMap(2);
        hashMap.put("{CLCODE}", str2);
        if (str3 == null) {
            str3 = "";
        }
        hashMap.put("{EVENT_ID}", str3);
        return a(str, jSONObject, hashMap, str4, jVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, Map<String, String> map, String str2, j jVar) {
        JSONObject b = i.b(jSONObject, str, new JSONObject(), jVar);
        ArrayList arrayList = new ArrayList(b.length() + 1);
        if (n.b(str2)) {
            arrayList.add(new a(str2, null));
        }
        if (b.length() > 0) {
            Iterator<String> keys = b.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    if (!TextUtils.isEmpty(next)) {
                        String optString = b.optString(next);
                        String a2 = n.a(next, map);
                        if (!TextUtils.isEmpty(optString)) {
                            optString = n.a(optString, map);
                        }
                        arrayList.add(new a(a2, optString));
                    }
                } catch (Throwable th) {
                    jVar.u().b("Utils", "Failed to create and add postback url.", th);
                }
            }
        }
        return arrayList;
    }

    private static List<Class> a(List<String> list, j jVar) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String next : list) {
            try {
                arrayList.add(Class.forName(next));
            } catch (ClassNotFoundException unused) {
                p u = jVar.u();
                u.e("Utils", "Failed to create class for name: " + next);
            }
        }
        return arrayList;
    }

    public static void a(f<String> fVar, int i, j jVar) {
        if (TextUtils.isEmpty((String) jVar.a(fVar))) {
            double random = Math.random();
            double d = (double) i;
            Double.isNaN(d);
            jVar.a(fVar, String.valueOf(((int) (random * d)) + 1));
        }
    }

    public static void a(AppLovinAdLoadListener appLovinAdLoadListener, com.applovin.impl.sdk.ad.d dVar, int i, j jVar) {
        if (appLovinAdLoadListener != null) {
            try {
                if (appLovinAdLoadListener instanceof m) {
                    ((m) appLovinAdLoadListener).a(dVar, i);
                } else {
                    appLovinAdLoadListener.failedToReceiveAd(i);
                }
            } catch (Throwable th) {
                jVar.u().b("Utils", "Unable process a failure to receive an ad", th);
            }
        }
    }

    public static void a(Closeable closeable, j jVar) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                if (jVar != null) {
                    p u = jVar.u();
                    u.b("Utils", "Unable to close stream: " + closeable, th);
                }
            }
        }
    }

    public static void a(String str, String str2, Map<String, String> map) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    public static void a(String str, JSONObject jSONObject, j jVar) {
        if (jSONObject.has("no_fill_reason")) {
            Object a2 = i.a(jSONObject, "no_fill_reason", new Object(), jVar);
            p.j("AppLovinSdk", "\n**************************************************\nNO FILL received:\n..ID: \"" + str + "\"\n..SDK KEY: \"" + jVar.s() + "\"\n..Reason: " + a2 + "\n**************************************************\n");
        }
    }

    public static void a(HttpURLConnection httpURLConnection, j jVar) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                if (jVar != null) {
                    p u = jVar.u();
                    u.b("Utils", "Unable to disconnect connection: " + httpURLConnection, th);
                }
            }
        }
    }

    public static boolean a() {
        Context D = j.D();
        if (D != null) {
            return c.a(D).a("applovin.sdk.verbose_logging");
        }
        return false;
    }

    public static boolean a(long j, long j2) {
        return (j & j2) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.c.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.utils.c.a(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.sdk.utils.c.a(java.lang.String, boolean):boolean */
    public static boolean a(Context context) {
        if (context == null) {
            context = j.D();
        }
        if (context != null) {
            return c.a(context).a("applovin.sdk.verbose_logging", false);
        }
        return false;
    }

    public static boolean a(Context context, Uri uri, j jVar) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            if (!(context instanceof Activity)) {
                intent.setFlags(DriveFile.MODE_READ_ONLY);
            }
            jVar.X().b();
            context.startActivity(intent);
            z = true;
        } catch (Throwable th) {
            p u = jVar.u();
            u.b("Utils", "Unable to open \"" + uri + "\".", th);
            z = false;
        }
        if (!z) {
            jVar.X().c();
        }
        return z;
    }

    public static boolean a(View view, Activity activity) {
        View rootView;
        if (!(activity == null || view == null)) {
            Window window = activity.getWindow();
            if (window != null) {
                rootView = window.getDecorView();
            } else {
                View findViewById = activity.findViewById(16908290);
                if (findViewById != null) {
                    rootView = findViewById.getRootView();
                }
            }
            return a(view, rootView);
        }
        return false;
    }

    public static boolean a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (a(view, viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(Object obj, List<String> list, j jVar) {
        if (list == null) {
            return false;
        }
        for (Class isInstance : a(list, jVar)) {
            if (isInstance.isInstance(obj)) {
                if (obj instanceof Map) {
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        if (!(entry.getKey() instanceof String)) {
                            jVar.u().b("Utils", "Invalid key type used. Map keys should be of type String.");
                            return false;
                        } else if (!a(entry.getValue(), list, jVar)) {
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof List)) {
                    return true;
                } else {
                    for (Object a2 : (List) obj) {
                        if (!a(a2, list, jVar)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        p u = jVar.u();
        u.b("Utils", "Object '" + obj + "' does not match any of the required types '" + list + "'.");
        return false;
    }

    public static boolean a(String str, List<String> list) {
        for (String startsWith : list) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static int b(Context context) {
        Resources resources;
        Configuration configuration;
        if (context == null || (resources = context.getResources()) == null || (configuration = resources.getConfiguration()) == null) {
            return 0;
        }
        return configuration.orientation;
    }

    public static long b(float f) {
        return c(a(f));
    }

    public static String b(Class cls, String str) {
        try {
            Field a2 = a(cls, str);
            a2.setAccessible(true);
            return (String) a2.get(null);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String b(String str) {
        return str.replace("{PLACEMENT}", "");
    }

    public static Map<String, String> b(Map<String, String> map) {
        HashMap hashMap = new HashMap(map);
        for (String str : hashMap.keySet()) {
            String str2 = (String) hashMap.get(str);
            if (str2 != null) {
                hashMap.put(str, n.e(str2));
            }
        }
        return hashMap;
    }

    public static void b(AppLovinAd appLovinAd, j jVar) {
        if (appLovinAd instanceof AppLovinAdBase) {
            String s = jVar.s();
            String s2 = ((AppLovinAdBase) appLovinAd).getSdk().s();
            if (!s.equals(s2)) {
                p.j("AppLovinAd", "Ad was loaded from sdk with key: " + s2 + ", but is being rendered from sdk with key: " + s);
                jVar.K().a(com.applovin.impl.sdk.c.g.l);
            }
        }
    }

    public static boolean b() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static boolean b(j jVar) {
        if (a == null) {
            try {
                Context D = j.D();
                a = Boolean.valueOf(Class.forName(D.getPackageName() + ".BuildConfig").getField("DEBUG").getBoolean(null));
            } catch (Throwable unused) {
                jVar.u().b("Utils", "Publisher is not in debug mode");
                a = false;
            }
        }
        return a.booleanValue();
    }

    private static long c(float f) {
        return (long) Math.round(f);
    }

    public static View c(Context context) {
        if (!(context instanceof Activity)) {
            return null;
        }
        return ((Activity) context).getWindow().getDecorView().findViewById(16908290);
    }

    public static MaxAdFormat c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase("banner")) {
            return MaxAdFormat.BANNER;
        }
        if (str.equalsIgnoreCase("mrec")) {
            return MaxAdFormat.MREC;
        }
        if (str.equalsIgnoreCase("leaderboard") || str.equalsIgnoreCase("leader")) {
            return MaxAdFormat.LEADER;
        }
        if (str.equalsIgnoreCase("interstitial") || str.equalsIgnoreCase("inter")) {
            return MaxAdFormat.INTERSTITIAL;
        }
        if (str.equalsIgnoreCase(Constants.CONVERT_REWARDED) || str.equalsIgnoreCase(MTGRewardVideoActivity.INTENT_REWARD)) {
            return MaxAdFormat.REWARDED;
        }
        throw new IllegalArgumentException("Unknown format: " + str);
    }

    public static boolean c() {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
        try {
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
        } catch (Throwable th) {
            p.c("Utils", "Exception thrown while getting memory state.", th);
        }
        return runningAppProcessInfo.importance == 100 || runningAppProcessInfo.importance == 200;
    }

    public static String d(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setPackage(context.getPackageName());
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (!queryIntentActivities.isEmpty()) {
            return queryIntentActivities.get(0).activityInfo.name;
        }
        return null;
    }

    public static String d(String str) {
        Uri parse = Uri.parse(str);
        return new Uri.Builder().scheme(parse.getScheme()).authority(parse.getAuthority()).path(parse.getPath()).build().toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000a A[Catch:{ Throwable -> 0x002e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean d() {
        /*
            java.util.Enumeration r0 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ Throwable -> 0x002e }
        L_0x0004:
            boolean r1 = r0.hasMoreElements()     // Catch:{ Throwable -> 0x002e }
            if (r1 == 0) goto L_0x0036
            java.lang.Object r1 = r0.nextElement()     // Catch:{ Throwable -> 0x002e }
            java.net.NetworkInterface r1 = (java.net.NetworkInterface) r1     // Catch:{ Throwable -> 0x002e }
            java.lang.String r1 = r1.getDisplayName()     // Catch:{ Throwable -> 0x002e }
            java.lang.String r2 = "tun"
            boolean r2 = r1.contains(r2)     // Catch:{ Throwable -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ppp"
            boolean r2 = r1.contains(r2)     // Catch:{ Throwable -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ipsec"
            boolean r1 = r1.contains(r2)     // Catch:{ Throwable -> 0x002e }
            if (r1 == 0) goto L_0x0004
        L_0x002c:
            r0 = 1
            return r0
        L_0x002e:
            r0 = move-exception
            java.lang.String r1 = "Utils"
            java.lang.String r2 = "Unable to check Network Interfaces"
            com.applovin.impl.sdk.p.c(r1, r2, r0)
        L_0x0036:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.q.d():boolean");
    }

    public static int e(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null) {
            return 0;
        }
        return windowManager.getDefaultDisplay().getRotation();
    }

    public static boolean e(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            Class.forName(str);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static long f(String str) {
        if (!n.b(str)) {
            return Long.MAX_VALUE;
        }
        try {
            return (long) Color.parseColor(str);
        } catch (Throwable unused) {
            return Long.MAX_VALUE;
        }
    }
}
