package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashSet;
import java.util.Set;

public class e extends BroadcastReceiver {
    public static int a = -1;
    private final AudioManager b;
    private final Context c;
    private final j d;
    private final Set<a> e = new HashSet();
    private final Object f = new Object();
    private boolean g;
    private int h;

    public interface a {
        void onRingerModeChanged(int i);
    }

    e(j jVar) {
        this.d = jVar;
        this.c = jVar.C();
        this.b = (AudioManager) this.c.getSystemService("audio");
    }

    private void a() {
        this.d.u().b("AudioSessionManager", "Observing ringer mode...");
        this.h = a;
        Context context = this.c;
        AudioManager audioManager = this.b;
        context.registerReceiver(this, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        this.d.ae().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        this.d.ae().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public static boolean a(int i) {
        return i == 0 || i == 1;
    }

    private void b() {
        this.d.u().b("AudioSessionManager", "Stopping observation of mute switch state...");
        this.c.unregisterReceiver(this);
        this.d.ae().unregisterReceiver(this);
    }

    private void b(final int i) {
        if (!this.g) {
            p u = this.d.u();
            u.b("AudioSessionManager", "Ringer mode is " + i);
            synchronized (this.f) {
                for (final a next : this.e) {
                    AppLovinSdkUtils.runOnUiThread(new Runnable() {
                        public void run() {
                            next.onRingerModeChanged(i);
                        }
                    });
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.sdk.e.a r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.e$a> r1 = r2.e     // Catch:{ all -> 0x0020 }
            boolean r1 = r1.contains(r3)     // Catch:{ all -> 0x0020 }
            if (r1 == 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x0020 }
            return
        L_0x000d:
            java.util.Set<com.applovin.impl.sdk.e$a> r1 = r2.e     // Catch:{ all -> 0x0020 }
            r1.add(r3)     // Catch:{ all -> 0x0020 }
            java.util.Set<com.applovin.impl.sdk.e$a> r3 = r2.e     // Catch:{ all -> 0x0020 }
            int r3 = r3.size()     // Catch:{ all -> 0x0020 }
            r1 = 1
            if (r3 != r1) goto L_0x001e
            r2.a()     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r0)     // Catch:{ all -> 0x0020 }
            return
        L_0x0020:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0020 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.a(com.applovin.impl.sdk.e$a):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.applovin.impl.sdk.e.a r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.e$a> r1 = r2.e     // Catch:{ all -> 0x001f }
            boolean r1 = r1.contains(r3)     // Catch:{ all -> 0x001f }
            if (r1 != 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            return
        L_0x000d:
            java.util.Set<com.applovin.impl.sdk.e$a> r1 = r2.e     // Catch:{ all -> 0x001f }
            r1.remove(r3)     // Catch:{ all -> 0x001f }
            java.util.Set<com.applovin.impl.sdk.e$a> r3 = r2.e     // Catch:{ all -> 0x001f }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x001f }
            if (r3 == 0) goto L_0x001d
            r2.b()     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            return
        L_0x001f:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.b(com.applovin.impl.sdk.e$a):void");
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        AudioManager audioManager = this.b;
        if (!"android.media.RINGER_MODE_CHANGED".equals(action)) {
            if ("com.applovin.application_paused".equals(action)) {
                this.g = true;
                this.h = this.b.getRingerMode();
                return;
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.g = false;
                if (this.h != this.b.getRingerMode()) {
                    this.h = a;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        b(this.b.getRingerMode());
    }
}
