package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

class h extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public static AlertDialog b;
    /* access modifiers changed from: private */
    public static final AtomicBoolean c = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final i a;
    private o d;

    public interface a {
        void a();

        void b();
    }

    h(i iVar, j jVar) {
        this.a = iVar;
        jVar.ae().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        jVar.ae().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a(long j, final j jVar, final a aVar) {
        if (j > 0) {
            if (b == null || !b.isShowing()) {
                if (c.getAndSet(true)) {
                    if (j < this.d.a()) {
                        p u = jVar.u();
                        u.b("ConsentAlertManager", "Scheduling consent alert earlier (" + j + "ms) than remaining scheduled time (" + this.d.a() + "ms)");
                        this.d.d();
                    } else {
                        p u2 = jVar.u();
                        u2.d("ConsentAlertManager", "Skip scheduling consent alert - one scheduled already with remaining time of " + this.d.a() + " milliseconds");
                        return;
                    }
                }
                p u3 = jVar.u();
                u3.b("ConsentAlertManager", "Scheduling consent alert for " + j + " milliseconds");
                this.d = o.a(j, jVar, new Runnable() {
                    public void run() {
                        p u;
                        String str;
                        String str2;
                        if (h.this.a.c()) {
                            jVar.u().e("ConsentAlertManager", "Consent dialog already showing, skip showing of consent alert");
                            return;
                        }
                        Activity a2 = jVar.Z().a();
                        if (a2 == null || !com.applovin.impl.sdk.utils.h.a(jVar.C())) {
                            if (a2 == null) {
                                u = jVar.u();
                                str = "ConsentAlertManager";
                                str2 = "No parent Activity found - rescheduling consent alert...";
                            } else {
                                u = jVar.u();
                                str = "ConsentAlertManager";
                                str2 = "No internet available - rescheduling consent alert...";
                            }
                            u.e(str, str2);
                            h.c.set(false);
                            h.this.a(((Long) jVar.a(d.aA)).longValue(), jVar, aVar);
                            return;
                        }
                        AppLovinSdkUtils.runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog unused = h.b = new AlertDialog.Builder(jVar.Z().a()).setTitle((CharSequence) jVar.a(d.aB)).setMessage((CharSequence) jVar.a(d.aC)).setCancelable(false).setPositiveButton((CharSequence) jVar.a(d.aD), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.a();
                                        dialogInterface.dismiss();
                                        h.c.set(false);
                                    }
                                }).setNegativeButton((CharSequence) jVar.a(d.aE), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.b();
                                        dialogInterface.dismiss();
                                        h.c.set(false);
                                        h.this.a(((Long) jVar.a(d.az)).longValue(), jVar, aVar);
                                    }
                                }).create();
                                h.b.show();
                            }
                        });
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (this.d != null) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                this.d.b();
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.d.c();
            }
        }
    }
}
