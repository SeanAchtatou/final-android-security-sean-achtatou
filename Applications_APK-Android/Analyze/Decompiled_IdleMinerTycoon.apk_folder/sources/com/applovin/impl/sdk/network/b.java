package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class b<T> {
    private String a;
    private String b;
    private Map<String, String> c;
    private Map<String, String> d;
    private final JSONObject e;
    private String f;
    private final T g;
    private final boolean h;
    private final int i;
    private int j;
    private final int k;
    private final int l;
    private final boolean m;
    private final boolean n;

    public static class a<T> {
        String a;
        String b;
        String c;
        Map<String, String> d;
        Map<String, String> e;
        JSONObject f;
        T g;
        boolean h = true;
        int i = 1;
        int j;
        int k;
        boolean l;
        boolean m;

        public a(j jVar) {
            this.j = ((Integer) jVar.a(d.dw)).intValue();
            this.k = ((Integer) jVar.a(d.dv)).intValue();
            this.l = ((Boolean) jVar.a(d.eR)).booleanValue();
            this.d = new HashMap();
        }

        public a<T> a(int i2) {
            this.i = i2;
            return this;
        }

        public a<T> a(Object obj) {
            this.g = obj;
            return this;
        }

        public a<T> a(String str) {
            this.b = str;
            return this;
        }

        public a<T> a(Map<String, String> map) {
            this.d = map;
            return this;
        }

        public a<T> a(JSONObject jSONObject) {
            this.f = jSONObject;
            return this;
        }

        public a<T> a(boolean z) {
            this.l = z;
            return this;
        }

        public b<T> a() {
            return new b<>(this);
        }

        public a<T> b(int i2) {
            this.j = i2;
            return this;
        }

        public a<T> b(String str) {
            this.a = str;
            return this;
        }

        public a<T> b(boolean z) {
            this.m = z;
            return this;
        }

        public a<T> c(int i2) {
            this.k = i2;
            return this;
        }

        public a<T> c(String str) {
            this.c = str;
            return this;
        }
    }

    protected b(a<T> aVar) {
        this.a = aVar.b;
        this.b = aVar.a;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.c;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.i;
        this.k = aVar.j;
        this.l = aVar.k;
        this.m = aVar.l;
        this.n = aVar.m;
    }

    public static <T> a<T> a(j jVar) {
        return new a<>(jVar);
    }

    public String a() {
        return this.a;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public Map<String, String> c() {
        return this.c;
    }

    public Map<String, String> d() {
        return this.d;
    }

    public JSONObject e() {
        return this.e;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r0 = 1
            if (r4 != r5) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r5 instanceof com.applovin.impl.sdk.network.b
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            com.applovin.impl.sdk.network.b r5 = (com.applovin.impl.sdk.network.b) r5
            java.lang.String r1 = r4.a
            if (r1 == 0) goto L_0x001b
            java.lang.String r1 = r4.a
            java.lang.String r3 = r5.a
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0020
            goto L_0x001f
        L_0x001b:
            java.lang.String r1 = r5.a
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            return r2
        L_0x0020:
            java.util.Map<java.lang.String, java.lang.String> r1 = r4.c
            if (r1 == 0) goto L_0x002f
            java.util.Map<java.lang.String, java.lang.String> r1 = r4.c
            java.util.Map<java.lang.String, java.lang.String> r3 = r5.c
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0034
            goto L_0x0033
        L_0x002f:
            java.util.Map<java.lang.String, java.lang.String> r1 = r5.c
            if (r1 == 0) goto L_0x0034
        L_0x0033:
            return r2
        L_0x0034:
            java.util.Map<java.lang.String, java.lang.String> r1 = r4.d
            if (r1 == 0) goto L_0x0043
            java.util.Map<java.lang.String, java.lang.String> r1 = r4.d
            java.util.Map<java.lang.String, java.lang.String> r3 = r5.d
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0048
            goto L_0x0047
        L_0x0043:
            java.util.Map<java.lang.String, java.lang.String> r1 = r5.d
            if (r1 == 0) goto L_0x0048
        L_0x0047:
            return r2
        L_0x0048:
            java.lang.String r1 = r4.f
            if (r1 == 0) goto L_0x0057
            java.lang.String r1 = r4.f
            java.lang.String r3 = r5.f
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x005c
            goto L_0x005b
        L_0x0057:
            java.lang.String r1 = r5.f
            if (r1 == 0) goto L_0x005c
        L_0x005b:
            return r2
        L_0x005c:
            java.lang.String r1 = r4.b
            if (r1 == 0) goto L_0x006b
            java.lang.String r1 = r4.b
            java.lang.String r3 = r5.b
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0070
            goto L_0x006f
        L_0x006b:
            java.lang.String r1 = r5.b
            if (r1 == 0) goto L_0x0070
        L_0x006f:
            return r2
        L_0x0070:
            org.json.JSONObject r1 = r4.e
            if (r1 == 0) goto L_0x007f
            org.json.JSONObject r1 = r4.e
            org.json.JSONObject r3 = r5.e
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0084
            goto L_0x0083
        L_0x007f:
            org.json.JSONObject r1 = r5.e
            if (r1 == 0) goto L_0x0084
        L_0x0083:
            return r2
        L_0x0084:
            T r1 = r4.g
            if (r1 == 0) goto L_0x0093
            T r1 = r4.g
            T r3 = r5.g
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0098
            goto L_0x0097
        L_0x0093:
            T r1 = r5.g
            if (r1 == 0) goto L_0x0098
        L_0x0097:
            return r2
        L_0x0098:
            boolean r1 = r4.h
            boolean r3 = r5.h
            if (r1 == r3) goto L_0x009f
            return r2
        L_0x009f:
            int r1 = r4.i
            int r3 = r5.i
            if (r1 == r3) goto L_0x00a6
            return r2
        L_0x00a6:
            int r1 = r4.j
            int r3 = r5.j
            if (r1 == r3) goto L_0x00ad
            return r2
        L_0x00ad:
            int r1 = r4.k
            int r3 = r5.k
            if (r1 == r3) goto L_0x00b4
            return r2
        L_0x00b4:
            int r1 = r4.l
            int r3 = r5.l
            if (r1 == r3) goto L_0x00bb
            return r2
        L_0x00bb:
            boolean r1 = r4.m
            boolean r3 = r5.m
            if (r1 == r3) goto L_0x00c2
            return r2
        L_0x00c2:
            boolean r1 = r4.n
            boolean r5 = r5.n
            if (r1 == r5) goto L_0x00c9
            return r2
        L_0x00c9:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.b.equals(java.lang.Object):boolean");
    }

    public String f() {
        return this.f;
    }

    public T g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((((((super.hashCode() * 31) + (this.a != null ? this.a.hashCode() : 0)) * 31) + (this.f != null ? this.f.hashCode() : 0)) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.g != null) {
            i2 = this.g.hashCode();
        }
        int i3 = ((((((((((((((hashCode + i2) * 31) + (this.h ? 1 : 0)) * 31) + this.i) * 31) + this.j) * 31) + this.k) * 31) + this.l) * 31) + (this.m ? 1 : 0)) * 31) + (this.n ? 1 : 0);
        if (this.c != null) {
            i3 = (i3 * 31) + this.c.hashCode();
        }
        if (this.d != null) {
            i3 = (i3 * 31) + this.d.hashCode();
        }
        if (this.e == null) {
            return i3;
        }
        char[] charArray = this.e.toString().toCharArray();
        Arrays.sort(charArray);
        return (i3 * 31) + new String(charArray).hashCode();
    }

    public int i() {
        return this.i - this.j;
    }

    public int j() {
        return this.j;
    }

    public int k() {
        return this.k;
    }

    public int l() {
        return this.l;
    }

    public boolean m() {
        return this.m;
    }

    public boolean n() {
        return this.n;
    }

    public String toString() {
        return "HttpRequest {endpoint=" + this.a + ", backupEndpoint=" + this.f + ", httpMethod=" + this.b + ", httpHeaders=" + this.d + ", body=" + this.e + ", emptyResponse=" + ((Object) this.g) + ", requiresResponse=" + this.h + ", initialRetryAttempts=" + this.i + ", retryAttemptsLeft=" + this.j + ", timeoutMillis=" + this.k + ", retryDelayMillis=" + this.l + ", encodingEnabled=" + this.m + ", trackConnectionSpeed=" + this.n + '}';
    }
}
