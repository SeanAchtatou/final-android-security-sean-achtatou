package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinErrorCodes;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.concurrent.TimeUnit;

public abstract class x<T> extends a implements a.c<T> {
    /* access modifiers changed from: private */
    public final b<T> a;
    private final a.c<T> c;
    protected a.C0006a d;
    /* access modifiers changed from: private */
    public r.a e;
    /* access modifiers changed from: private */
    public d<String> f;
    /* access modifiers changed from: private */
    public d<String> g;

    public x(b<T> bVar, j jVar) {
        this(bVar, jVar, false);
    }

    public x(b<T> bVar, final j jVar, boolean z) {
        super("TaskRepeatRequest", jVar, z);
        this.e = r.a.BACKGROUND;
        this.f = null;
        this.g = null;
        if (bVar != null) {
            this.a = bVar;
            this.d = new a.C0006a();
            this.c = new a.c<T>() {
                public void a(int i) {
                    d dVar;
                    x xVar;
                    boolean z = false;
                    boolean z2 = i < 200 || i >= 500;
                    boolean z3 = i == 429;
                    if (i != -103) {
                        z = true;
                    }
                    if (z && (z2 || z3)) {
                        String f = x.this.a.f();
                        if (x.this.a.j() > 0) {
                            x xVar2 = x.this;
                            xVar2.c("Unable to send request due to server failure (code " + i + "). " + x.this.a.j() + " attempts left, retrying in " + TimeUnit.MILLISECONDS.toSeconds((long) x.this.a.l()) + " seconds...");
                            int j = x.this.a.j() - 1;
                            x.this.a.a(j);
                            if (j == 0) {
                                x.this.c(x.this.f);
                                if (n.b(f) && f.length() >= 4) {
                                    x.this.a.a(f);
                                    x xVar3 = x.this;
                                    xVar3.b("Switching to backup endpoint " + f);
                                }
                            }
                            jVar.J().a(x.this, x.this.e, (long) x.this.a.l());
                            return;
                        }
                        if (f == null || !f.equals(x.this.a.a())) {
                            xVar = x.this;
                            dVar = x.this.f;
                        } else {
                            xVar = x.this;
                            dVar = x.this.g;
                        }
                        xVar.c(dVar);
                    }
                    x.this.a(i);
                }

                public void a(T t, int i) {
                    x.this.a.a(0);
                    x.this.a(t, i);
                }
            };
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.e.a(com.applovin.impl.sdk.b.d<?>, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.d<ST>, ST]
     candidates:
      com.applovin.impl.sdk.b.e.a(java.lang.String, com.applovin.impl.sdk.b.d):com.applovin.impl.sdk.b.d<ST>
      com.applovin.impl.sdk.b.e.a(com.applovin.impl.sdk.b.d<?>, java.lang.Object):void */
    /* access modifiers changed from: private */
    public <ST> void c(d<ST> dVar) {
        if (dVar != null) {
            e B = e().B();
            B.a((d<?>) dVar, (Object) dVar.b());
            B.b();
        }
    }

    public i a() {
        return i.e;
    }

    public abstract void a(int i);

    public void a(d<String> dVar) {
        this.f = dVar;
    }

    public void a(r.a aVar) {
        this.e = aVar;
    }

    public abstract void a(Object obj, int i);

    public void b(d<String> dVar) {
        this.g = dVar;
    }

    public void run() {
        int i;
        a I = e().I();
        if (!e().c() && !e().d()) {
            d("AppLovin SDK is disabled: please check your connection");
            p.j("AppLovinSdk", "AppLovin SDK is disabled: please check your connection");
            i = -22;
        } else if (!n.b(this.a.a()) || this.a.a().length() < 4) {
            d("Task has an invalid or null request endpoint.");
            i = AppLovinErrorCodes.INVALID_URL;
        } else {
            if (TextUtils.isEmpty(this.a.b())) {
                this.a.b(this.a.e() != null ? HttpRequest.METHOD_POST : HttpRequest.METHOD_GET);
            }
            I.a(this.a, this.d, this.c);
            return;
        }
        a(i);
    }
}
