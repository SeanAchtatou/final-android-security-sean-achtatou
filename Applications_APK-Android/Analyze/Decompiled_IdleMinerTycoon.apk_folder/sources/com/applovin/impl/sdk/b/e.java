package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkSettings;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    protected final j a;
    protected final p b;
    protected final Context c;
    protected final SharedPreferences d;
    private final Map<String, Object> e = new HashMap();
    private final Object f = new Object();
    private Map<String, Object> g;
    private final b h;

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x003c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public e(com.applovin.impl.sdk.j r4) {
        /*
            r3 = this;
            r3.<init>()
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.e = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r3.f = r0
            r3.a = r4
            com.applovin.impl.sdk.p r0 = r4.u()
            r3.b = r0
            android.content.Context r0 = r4.C()
            r3.c = r0
            android.content.Context r0 = r3.c
            java.lang.String r1 = "com.applovin.sdk.1"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
            r3.d = r0
            java.lang.Class<com.applovin.impl.sdk.b.d> r0 = com.applovin.impl.sdk.b.d.class
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x003c }
            java.lang.Class.forName(r0)     // Catch:{ Throwable -> 0x003c }
            java.lang.Class<com.applovin.impl.sdk.b.c> r0 = com.applovin.impl.sdk.b.c.class
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x003c }
            java.lang.Class.forName(r0)     // Catch:{ Throwable -> 0x003c }
        L_0x003c:
            com.applovin.sdk.AppLovinSdkSettings r0 = r4.k()     // Catch:{ Throwable -> 0x005a }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Throwable -> 0x005a }
            java.lang.String r1 = "localSettings"
            java.lang.reflect.Field r0 = com.applovin.impl.sdk.utils.q.a(r0, r1)     // Catch:{ Throwable -> 0x005a }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ Throwable -> 0x005a }
            com.applovin.sdk.AppLovinSdkSettings r1 = r4.k()     // Catch:{ Throwable -> 0x005a }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Throwable -> 0x005a }
            java.util.HashMap r0 = (java.util.HashMap) r0     // Catch:{ Throwable -> 0x005a }
            r3.g = r0     // Catch:{ Throwable -> 0x005a }
        L_0x005a:
            com.applovin.impl.sdk.b.b r0 = new com.applovin.impl.sdk.b.b
            r0.<init>(r3, r4)
            r3.h = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.b.e.<init>(com.applovin.impl.sdk.j):void");
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) throws JSONException {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        throw new RuntimeException("SDK Error: unknown value type: " + obj.getClass());
    }

    private <T> T c(d<T> dVar) {
        try {
            return dVar.a(this.g.get(dVar.a()));
        } catch (Throwable unused) {
            return null;
        }
    }

    private String f() {
        return "com.applovin.sdk." + q.a(this.a.s()) + ".";
    }

    public <ST> d<ST> a(String str, d dVar) {
        for (d<ST> next : d.c()) {
            if (next.a().equals(str)) {
                return next;
            }
        }
        return dVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        return r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T a(com.applovin.impl.sdk.b.d r4) {
        /*
            r3 = this;
            if (r4 == 0) goto L_0x0035
            java.lang.Object r0 = r3.f
            monitor-enter(r0)
            java.lang.Object r1 = r3.c(r4)     // Catch:{ all -> 0x0032 }
            if (r1 == 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r1
        L_0x000d:
            java.util.Map<java.lang.String, java.lang.Object> r1 = r3.e     // Catch:{ all -> 0x0032 }
            java.lang.String r2 = r4.a()     // Catch:{ all -> 0x0032 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x0032 }
            if (r1 != 0) goto L_0x002c
            com.applovin.impl.sdk.b.b r1 = r3.h     // Catch:{ all -> 0x0032 }
            java.lang.Object r1 = r1.a(r4)     // Catch:{ all -> 0x0032 }
            if (r1 == 0) goto L_0x0026
            java.lang.Object r4 = r4.a(r1)     // Catch:{ all -> 0x0032 }
            goto L_0x002a
        L_0x0026:
            java.lang.Object r4 = r4.b()     // Catch:{ all -> 0x0032 }
        L_0x002a:
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r4
        L_0x002c:
            java.lang.Object r4 = r4.a(r1)     // Catch:{ all -> 0x0032 }
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r4
        L_0x0032:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            throw r4
        L_0x0035:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "No setting type specified"
            r4.<init>(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.b.e.a(com.applovin.impl.sdk.b.d):java.lang.Object");
    }

    public void a() {
        this.h.a();
    }

    public <T> void a(d<?> dVar, Object obj) {
        if (dVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj != null) {
            synchronized (this.f) {
                this.e.put(dVar.a(), obj);
            }
        } else {
            throw new IllegalArgumentException("No new value specified");
        }
    }

    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        boolean z;
        boolean z2;
        if (appLovinSdkSettings != null) {
            synchronized (this.f) {
                if (((Boolean) this.a.a(d.Z)).booleanValue()) {
                    this.e.put(d.Z.a(), Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled()));
                }
                if (((Boolean) this.a.a(d.bD)).booleanValue()) {
                    String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                    if (!n.b(autoPreloadSizes)) {
                        autoPreloadSizes = "NONE";
                    }
                    if (autoPreloadSizes.equals("NONE")) {
                        this.e.put(d.bd.a(), "");
                    } else {
                        this.e.put(d.bd.a(), autoPreloadSizes);
                    }
                }
                if (((Boolean) this.a.a(d.bE)).booleanValue()) {
                    String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                    if (!n.b(autoPreloadTypes)) {
                        autoPreloadTypes = "NONE";
                    }
                    boolean z3 = false;
                    if (!"NONE".equals(autoPreloadTypes)) {
                        z2 = false;
                        z = false;
                        for (String next : com.applovin.impl.sdk.utils.e.a(autoPreloadTypes)) {
                            if (next.equals(AppLovinAdType.REGULAR.getLabel())) {
                                z3 = true;
                            } else {
                                if (!next.equals(AppLovinAdType.INCENTIVIZED.getLabel()) && !next.contains("INCENT")) {
                                    if (!next.contains("REWARD")) {
                                        if (next.equals(AppLovinAdType.NATIVE.getLabel())) {
                                            z = true;
                                        }
                                    }
                                }
                                z2 = true;
                            }
                        }
                    } else {
                        z2 = false;
                        z = false;
                    }
                    if (!z3) {
                        this.e.put(d.bd.a(), "");
                    }
                    this.e.put(d.be.a(), Boolean.valueOf(z2));
                    this.e.put(d.bf.a(), Boolean.valueOf(z));
                }
            }
        }
    }

    public void a(JSONObject jSONObject) {
        p pVar;
        String str;
        String str2;
        synchronized (this.f) {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next != null && next.length() > 0) {
                    try {
                        d<Long> a2 = a(next, (d) null);
                        if (a2 != null) {
                            this.e.put(a2.a(), a(next, jSONObject, a2.b()));
                            if (a2 == d.eT) {
                                this.e.put(d.eU.a(), Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    } catch (JSONException e2) {
                        th = e2;
                        pVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to parse JSON settingsValues array";
                        pVar.b(str, str2, th);
                    } catch (Throwable th) {
                        th = th;
                        pVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to convert setting object ";
                        pVar.b(str, str2, th);
                    }
                }
            }
        }
    }

    public List<String> b(d<String> dVar) {
        return com.applovin.impl.sdk.utils.e.a((String) a(dVar));
    }

    public void b() {
        if (this.c != null) {
            String f2 = f();
            synchronized (this.f) {
                SharedPreferences.Editor edit = this.d.edit();
                for (d next : d.c()) {
                    Object obj = this.e.get(next.a());
                    if (obj != null) {
                        this.a.a(f2 + next.a(), obj, edit);
                    }
                }
                edit.apply();
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public void c() {
        if (this.c != null) {
            String f2 = f();
            synchronized (this.f) {
                for (d next : d.c()) {
                    try {
                        Object a2 = this.a.a(f2 + next.a(), null, next.b().getClass(), this.d);
                        if (a2 != null) {
                            this.e.put(next.a(), a2);
                        }
                    } catch (Exception e2) {
                        p pVar = this.b;
                        pVar.b("SettingsManager", "Unable to load \"" + next.a() + "\"", e2);
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public void d() {
        synchronized (this.f) {
            this.e.clear();
        }
        this.a.a(this.d);
    }

    public boolean e() {
        return this.a.k().isVerboseLoggingEnabled() || ((Boolean) a(d.Z)).booleanValue();
    }
}
