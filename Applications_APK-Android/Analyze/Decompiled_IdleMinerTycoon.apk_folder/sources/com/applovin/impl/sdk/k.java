package com.applovin.impl.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.d.i;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.ironsource.sdk.constants.Constants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class k {
    private static String e;
    private static String f;
    private static int g;
    private final j a;
    /* access modifiers changed from: private */
    public final p b;
    /* access modifiers changed from: private */
    public final Context c;
    private final Map<Class, Object> d;
    /* access modifiers changed from: private */
    public final AtomicReference<a> h = new AtomicReference<>();

    public static class a {
        public boolean a;
        public String b = "";
    }

    public static class b {
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public long h;
    }

    public static class c {
        public int a = -1;
        public int b = -1;
    }

    public static class d {
        public boolean A;
        public boolean B;
        public boolean C;
        public boolean D;
        public boolean E;
        public int F = -1;
        public String G;
        public long H;
        public long I;
        public e J = new e();
        public Boolean K;
        public Boolean L;
        public Boolean M;
        public boolean N;
        public float O;
        public float P;
        public long Q;
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public int h;
        public String i;
        public String j;
        public Locale k;
        public String l;
        public float m;
        public int n;
        public float o;
        public float p;
        public double q;
        public double r;
        public int s;
        public boolean t;
        public c u;
        public long v;
        public float w;
        public int x;
        public int y;
        public String z;
    }

    public static class e {
        public long a = -1;
        public long b = -1;
        public long c = -1;
        public boolean d = false;
    }

    protected k(j jVar) {
        if (jVar != null) {
            this.a = jVar;
            this.b = jVar.u();
            this.c = jVar.C();
            this.d = Collections.synchronizedMap(new HashMap());
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private d a(d dVar) {
        if (dVar == null) {
            dVar = new d();
        }
        dVar.K = g.a(this.c);
        dVar.L = g.b(this.c);
        dVar.M = g.c(this.c);
        dVar.u = ((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eb)).booleanValue() ? j() : null;
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eo)).booleanValue()) {
            dVar.t = p();
        }
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService("audio");
            if (audioManager != null) {
                dVar.x = (int) (((float) audioManager.getStreamVolume(3)) * ((Float) this.a.a(com.applovin.impl.sdk.b.d.ep)).floatValue());
            }
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect volume", th);
        }
        try {
            dVar.y = (int) ((((float) Settings.System.getInt(this.c.getContentResolver(), "screen_brightness")) / 255.0f) * 100.0f);
        } catch (Settings.SettingNotFoundException e2) {
            this.b.b("DataCollector", "Unable to collect screen brightness", e2);
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eq)).booleanValue()) {
            if (e == null) {
                String t = t();
                if (!n.b(t)) {
                    t = "";
                }
                e = t;
            }
            if (n.b(e)) {
                dVar.z = e;
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ef)).booleanValue()) {
            try {
                dVar.H = Environment.getDataDirectory().getFreeSpace();
                dVar.I = Environment.getDataDirectory().getTotalSpace();
            } catch (Throwable th2) {
                dVar.H = -1;
                dVar.I = -1;
                this.b.b("DataCollector", "Unable to collect total & free space.", th2);
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eg)).booleanValue()) {
            try {
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                ((ActivityManager) this.c.getSystemService("activity")).getMemoryInfo(memoryInfo);
                dVar.J.b = memoryInfo.availMem;
                dVar.J.d = memoryInfo.lowMemory;
                dVar.J.c = memoryInfo.threshold;
                dVar.J.a = memoryInfo.totalMem;
            } catch (Throwable th3) {
                this.b.b("DataCollector", "Unable to collect memory info.", th3);
            }
        }
        String str = (String) this.a.B().a(com.applovin.impl.sdk.b.d.es);
        if (!str.equalsIgnoreCase(f)) {
            try {
                f = str;
                PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo(str, 0);
                dVar.s = packageInfo.versionCode;
                g = packageInfo.versionCode;
            } catch (Throwable unused) {
                g = 0;
            }
        } else {
            dVar.s = g;
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ec)).booleanValue()) {
            dVar.C = AppLovinSdkUtils.isTablet(this.c);
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ed)).booleanValue()) {
            dVar.D = o();
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ee)).booleanValue()) {
            String m = m();
            if (!TextUtils.isEmpty(m)) {
                dVar.G = m;
            }
        }
        dVar.l = g();
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eh)).booleanValue()) {
            dVar.E = q.d();
        }
        if (g.d()) {
            try {
                dVar.F = ((PowerManager) this.c.getSystemService("power")).isPowerSaveMode() ? 1 : 0;
            } catch (Throwable th4) {
                this.b.b("DataCollector", "Unable to collect power saving mode", th4);
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.et)).booleanValue() && this.a.ac() != null) {
            dVar.O = this.a.ac().c();
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eu)).booleanValue() && this.a.ac() != null) {
            dVar.P = this.a.ac().b();
        }
        return dVar;
    }

    private String a(int i) {
        if (i == 1) {
            return "receiver";
        }
        if (i == 2) {
            return "speaker";
        }
        if (i == 4 || i == 3) {
            return "headphones";
        }
        if (i == 8) {
            return "bluetootha2dpoutput";
        }
        if (i == 13 || i == 19 || i == 5 || i == 6 || i == 12 || i == 11) {
            return "lineout";
        }
        if (i == 9 || i == 10) {
            return "hdmioutput";
        }
        return null;
    }

    private boolean a(String str) {
        try {
            return Settings.Secure.getInt(this.c.getContentResolver(), str) == 1;
        } catch (Settings.SettingNotFoundException unused) {
            return false;
        }
    }

    private boolean a(String str, com.applovin.impl.sdk.b.d<String> dVar) {
        for (String startsWith : com.applovin.impl.sdk.utils.e.a((String) this.a.a(dVar))) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private boolean b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("No permission name specified");
        } else if (this.c != null) {
            return com.applovin.impl.sdk.utils.k.a(str, this.c.getPackageName(), this.c.getPackageManager()) == 0;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    private String c(String str) {
        int length = str.length();
        int[] iArr = {11, 12, 10, 3, 2, 1, 15, 10, 15, 14};
        int length2 = iArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = str.charAt(i);
            for (int i2 = length2 - 1; i2 >= 0; i2--) {
                cArr[i] = (char) (cArr[i] ^ iArr[i2]);
            }
        }
        return new String(cArr);
    }

    private Map<String, String> f() {
        return a(null, false, true);
    }

    private String g() {
        String str;
        try {
            int b2 = q.b(this.c);
            if (b2 == 1) {
                str = "portrait";
            } else if (b2 != 2) {
                return Constants.ParametersKeys.ORIENTATION_NONE;
            } else {
                str = "landscape";
            }
            return str;
        } catch (Throwable th) {
            this.a.u().b("DataCollector", "Encountered error while attempting to collect application orientation", th);
            return Constants.ParametersKeys.ORIENTATION_NONE;
        }
    }

    private a h() {
        if (i()) {
            try {
                a aVar = new a();
                AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.c);
                aVar.a = advertisingIdInfo.isLimitAdTrackingEnabled();
                aVar.b = advertisingIdInfo.getId();
                return aVar;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
            }
        } else {
            p.j("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }");
            return new a();
        }
    }

    private boolean i() {
        return q.e("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    private c j() {
        try {
            c cVar = new c();
            Intent registerReceiver = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = -1;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : -1;
            int intExtra2 = registerReceiver != null ? registerReceiver.getIntExtra("scale", -1) : -1;
            if (intExtra <= 0 || intExtra2 <= 0) {
                cVar.b = -1;
            } else {
                cVar.b = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
            }
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("status", -1);
            }
            cVar.a = i;
            return cVar;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect battery info", th);
            return null;
        }
    }

    private long k() {
        List asList = Arrays.asList(n.c(Settings.Secure.getString(this.c.getContentResolver(), "enabled_accessibility_services")).split(":"));
        long j = asList.contains("AccessibilityMenuService") ? 256 : 0;
        if (asList.contains("SelectToSpeakService")) {
            j |= 512;
        }
        if (asList.contains("SoundAmplifierService")) {
            j |= 2;
        }
        if (asList.contains("SpeechToTextAccessibilityService")) {
            j |= 128;
        }
        if (asList.contains("SwitchAccessService")) {
            j |= 4;
        }
        if ((this.c.getResources().getConfiguration().uiMode & 48) == 32) {
            j |= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
        }
        if (a("accessibility_enabled")) {
            j |= 8;
        }
        if (a("touch_exploration_enabled")) {
            j |= 16;
        }
        if (!g.d()) {
            return j;
        }
        if (a("accessibility_display_inversion_enabled")) {
            j |= 32;
        }
        return a("skip_first_use_hints") ? j | 64 : j;
    }

    private float l() {
        try {
            return Settings.System.getFloat(this.c.getContentResolver(), "font_scale");
        } catch (Settings.SettingNotFoundException e2) {
            this.b.b("DataCollector", "Error collecting font scale", e2);
            return -1.0f;
        }
    }

    private String m() {
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService("audio");
            StringBuilder sb = new StringBuilder();
            if (g.e()) {
                for (AudioDeviceInfo type : audioManager.getDevices(2)) {
                    String a2 = a(type.getType());
                    if (!TextUtils.isEmpty(a2)) {
                        sb.append(a2);
                        sb.append(",");
                    }
                }
            } else {
                if (audioManager.isWiredHeadsetOn()) {
                    sb.append("headphones");
                    sb.append(",");
                }
                if (audioManager.isBluetoothA2dpOn()) {
                    sb.append("bluetootha2dpoutput");
                }
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
                sb.deleteCharAt(sb.length() - 1);
            }
            String sb2 = sb.toString();
            if (TextUtils.isEmpty(sb2)) {
                this.b.b("DataCollector", "No sound outputs detected");
            }
            return sb2;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect sound outputs", th);
            return null;
        }
    }

    private double n() {
        double offset = (double) TimeZone.getDefault().getOffset(new Date().getTime());
        Double.isNaN(offset);
        double round = (double) Math.round((offset * 10.0d) / 3600000.0d);
        Double.isNaN(round);
        return round / 10.0d;
    }

    private boolean o() {
        try {
            PackageManager packageManager = this.c.getPackageManager();
            return g.d() ? packageManager.hasSystemFeature("android.software.leanback") : packageManager.hasSystemFeature("android.hardware.type.television");
        } catch (Throwable th) {
            this.b.b("DataCollector", "Failed to determine if device is TV.", th);
            return false;
        }
    }

    private boolean p() {
        try {
            return q() || r();
        } catch (Throwable unused) {
            return false;
        }
    }

    private boolean q() {
        String str = Build.TAGS;
        return str != null && str.contains(c("lz}$blpz"));
    }

    private boolean r() {
        for (String c2 : new String[]{"&zpz}ld&hyy&Z|yl{|zl{'hyb", "&zk`g&z|", "&zpz}ld&k`g&z|", "&zpz}ld&qk`g&z|", "&mh}h&efjhe&qk`g&z|", "&mh}h&efjhe&k`g&z|", "&zpz}ld&zm&qk`g&z|", "&zpz}ld&k`g&oh`ezhol&z|", "&mh}h&efjhe&z|"}) {
            if (new File(c(c2)).exists()) {
                return true;
            }
        }
        return false;
    }

    private boolean s() {
        return a(Build.DEVICE, com.applovin.impl.sdk.b.d.el) || a(Build.HARDWARE, com.applovin.impl.sdk.b.d.ek) || a(Build.MANUFACTURER, com.applovin.impl.sdk.b.d.em) || a(Build.MODEL, com.applovin.impl.sdk.b.d.en);
    }

    private String t() {
        final AtomicReference atomicReference = new AtomicReference();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        new Handler(this.c.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    atomicReference.set(new WebView(k.this.c).getSettings().getUserAgentString());
                } catch (Throwable th) {
                    countDownLatch.countDown();
                    throw th;
                }
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await(((Long) this.a.a(com.applovin.impl.sdk.b.d.er)).longValue(), TimeUnit.MILLISECONDS);
        } catch (Throwable unused) {
        }
        return (String) atomicReference.get();
    }

    /* access modifiers changed from: package-private */
    public String a() {
        String encodeToString = Base64.encodeToString(new JSONObject(f()).toString().getBytes(Charset.defaultCharset()), 2);
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.d.eS)).booleanValue()) {
            return encodeToString;
        }
        return l.a(encodeToString, this.a.s(), q.a(this.a));
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0435  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0455  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0470  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x048b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0498  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x04b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map<java.lang.String, java.lang.String> a(java.util.Map<java.lang.String, java.lang.String> r6, boolean r7, boolean r8) {
        /*
            r5 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.applovin.impl.sdk.k$d r1 = r5.b()
            java.lang.String r2 = "brand"
            java.lang.String r3 = r1.d
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "brand_name"
            java.lang.String r3 = r1.e
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "hardware"
            java.lang.String r3 = r1.f
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "api_level"
            int r3 = r1.h
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "carrier"
            java.lang.String r3 = r1.j
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "country_code"
            java.lang.String r3 = r1.i
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "locale"
            java.util.Locale r3 = r1.k
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "model"
            java.lang.String r3 = r1.a
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "os"
            java.lang.String r3 = r1.b
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "platform"
            java.lang.String r3 = r1.c
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "revision"
            java.lang.String r3 = r1.g
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "orientation_lock"
            java.lang.String r3 = r1.l
            r0.put(r2, r3)
            java.lang.String r2 = "tz_offset"
            double r3 = r1.r
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "aida"
            boolean r3 = r1.N
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "wvvc"
            int r3 = r1.s
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "adns"
            float r3 = r1.m
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "adnsd"
            int r3 = r1.n
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "xdpi"
            float r3 = r1.o
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "ydpi"
            float r3 = r1.p
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "screen_size_in"
            double r3 = r1.q
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "sim"
            boolean r3 = r1.A
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.a(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "gy"
            boolean r3 = r1.B
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.a(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "is_tablet"
            boolean r3 = r1.C
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.a(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "tv"
            boolean r3 = r1.D
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.a(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "vs"
            boolean r3 = r1.E
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.a(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "lpm"
            int r3 = r1.F
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "fs"
            long r3 = r1.H
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "tds"
            long r3 = r1.I
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "fm"
            com.applovin.impl.sdk.k$e r3 = r1.J
            long r3 = r3.b
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "tm"
            com.applovin.impl.sdk.k$e r3 = r1.J
            long r3 = r3.a
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "lmt"
            com.applovin.impl.sdk.k$e r3 = r1.J
            long r3 = r3.c
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "lm"
            com.applovin.impl.sdk.k$e r3 = r1.J
            boolean r3 = r3.d
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "adr"
            boolean r3 = r1.t
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.a(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "volume"
            int r3 = r1.x
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "sb"
            int r3 = r1.y
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "af"
            long r3 = r1.v
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "font"
            float r3 = r1.w
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "ua"
            java.lang.String r3 = r1.z
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            com.applovin.impl.sdk.utils.q.a(r2, r3, r0)
            java.lang.String r2 = "so"
            java.lang.String r3 = r1.G
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            com.applovin.impl.sdk.utils.q.a(r2, r3, r0)
            java.lang.String r2 = "bt_ms"
            long r3 = r1.Q
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            float r2 = r1.O
            r3 = 0
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x01db
            java.lang.String r2 = "da"
            float r4 = r1.O
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r0.put(r2, r4)
        L_0x01db:
            float r2 = r1.P
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x01ec
            java.lang.String r2 = "dm"
            float r3 = r1.P
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
        L_0x01ec:
            com.applovin.impl.sdk.k$c r2 = r1.u
            if (r2 == 0) goto L_0x0206
            java.lang.String r3 = "act"
            int r4 = r2.a
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r0.put(r3, r4)
            java.lang.String r3 = "acm"
            int r2 = r2.b
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r0.put(r3, r2)
        L_0x0206:
            java.lang.Boolean r2 = r1.K
            if (r2 == 0) goto L_0x0213
            java.lang.String r3 = "huc"
            java.lang.String r2 = r2.toString()
            r0.put(r3, r2)
        L_0x0213:
            java.lang.Boolean r2 = r1.L
            if (r2 == 0) goto L_0x0220
            java.lang.String r3 = "aru"
            java.lang.String r2 = r2.toString()
            r0.put(r3, r2)
        L_0x0220:
            java.lang.Boolean r1 = r1.M
            if (r1 == 0) goto L_0x022d
            java.lang.String r2 = "dns"
            java.lang.String r1 = r1.toString()
            r0.put(r2, r1)
        L_0x022d:
            android.content.Context r1 = r5.c
            android.graphics.Point r1 = com.applovin.impl.sdk.utils.g.a(r1)
            java.lang.String r2 = "dx"
            int r3 = r1.x
            java.lang.String r3 = java.lang.Integer.toString(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "dy"
            int r1 = r1.y
            java.lang.String r1 = java.lang.Integer.toString(r1)
            r0.put(r2, r1)
            java.lang.String r1 = "accept"
            java.lang.String r2 = "custom_size,launch_app,video"
            r0.put(r1, r2)
            java.lang.String r1 = "api_did"
            com.applovin.impl.sdk.j r2 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.String> r3 = com.applovin.impl.sdk.b.d.X
            java.lang.Object r2 = r2.a(r3)
            r0.put(r1, r2)
            java.lang.String r1 = "sdk_version"
            java.lang.String r2 = com.applovin.sdk.AppLovinSdk.VERSION
            r0.put(r1, r2)
            java.lang.String r1 = "build"
            r2 = 131(0x83, float:1.84E-43)
            java.lang.String r2 = java.lang.Integer.toString(r2)
            r0.put(r1, r2)
            java.lang.String r1 = "format"
            java.lang.String r2 = "json"
            r0.put(r1, r2)
            com.applovin.impl.sdk.k$b r1 = r5.c()
            java.lang.String r2 = "app_version"
            java.lang.String r3 = r1.b
            java.lang.String r3 = com.applovin.impl.sdk.utils.n.e(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "ia"
            long r3 = r1.h
            java.lang.String r3 = java.lang.Long.toString(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "tg"
            java.lang.String r3 = r1.e
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "ltg"
            java.lang.String r3 = r1.f
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r2, r3)
            java.lang.String r2 = "installer_name"
            java.lang.String r3 = r1.d
            r0.put(r2, r3)
            java.lang.String r2 = "debug"
            java.lang.String r1 = r1.g
            r0.put(r2, r1)
            java.lang.String r1 = "mediation_provider"
            com.applovin.impl.sdk.j r2 = r5.a
            java.lang.String r2 = r2.m()
            java.lang.String r2 = com.applovin.impl.sdk.utils.n.e(r2)
            com.applovin.impl.sdk.utils.q.a(r1, r2, r0)
            java.lang.String r1 = "network"
            com.applovin.impl.sdk.j r2 = r5.a
            java.lang.String r2 = com.applovin.impl.sdk.utils.h.f(r2)
            r0.put(r1, r2)
            java.lang.String r1 = "plugin_version"
            com.applovin.impl.sdk.j r2 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.String> r3 = com.applovin.impl.sdk.b.d.ea
            java.lang.Object r2 = r2.a(r3)
            java.lang.String r2 = (java.lang.String) r2
            com.applovin.impl.sdk.utils.q.a(r1, r2, r0)
            java.lang.String r1 = "preloading"
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r0.put(r1, r7)
            java.lang.String r7 = "first_install"
            com.applovin.impl.sdk.j r1 = r5.a
            boolean r1 = r1.G()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.put(r7, r1)
            java.lang.String r7 = "first_install_v2"
            com.applovin.impl.sdk.j r1 = r5.a
            boolean r1 = r1.H()
            r2 = 1
            r1 = r1 ^ r2
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.put(r7, r1)
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.d.eR
            java.lang.Object r7 = r7.a(r1)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 != 0) goto L_0x0321
            java.lang.String r7 = "sdk_key"
            com.applovin.impl.sdk.j r1 = r5.a
            java.lang.String r1 = r1.s()
            r0.put(r7, r1)
        L_0x0321:
            java.lang.String r7 = "sc"
            com.applovin.impl.sdk.j r1 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.String> r3 = com.applovin.impl.sdk.b.d.aa
            java.lang.Object r1 = r1.a(r3)
            r0.put(r7, r1)
            java.lang.String r7 = "sc2"
            com.applovin.impl.sdk.j r1 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.String> r3 = com.applovin.impl.sdk.b.d.ab
            java.lang.Object r1 = r1.a(r3)
            r0.put(r7, r1)
            java.lang.String r7 = "server_installed_at"
            com.applovin.impl.sdk.j r1 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.String> r3 = com.applovin.impl.sdk.b.d.ac
            java.lang.Object r1 = r1.a(r3)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r1 = com.applovin.impl.sdk.utils.n.e(r1)
            r0.put(r7, r1)
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.b.f<java.lang.String> r1 = com.applovin.impl.sdk.b.f.z
            java.lang.Object r7 = r7.a(r1)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r1 = "persisted_data"
            java.lang.String r7 = com.applovin.impl.sdk.utils.n.e(r7)
            com.applovin.impl.sdk.utils.q.a(r1, r7, r0)
            java.lang.String r7 = "v1"
            java.lang.String r1 = "android.permission.WRITE_EXTERNAL_STORAGE"
            android.content.Context r3 = r5.c
            boolean r1 = com.applovin.impl.sdk.utils.g.a(r1, r3)
            java.lang.String r1 = java.lang.Boolean.toString(r1)
            r0.put(r7, r1)
            java.lang.String r7 = "v2"
            java.lang.String r1 = "true"
            r0.put(r7, r1)
            java.lang.String r7 = "v3"
            java.lang.String r1 = "true"
            r0.put(r7, r1)
            java.lang.String r7 = "v4"
            java.lang.String r1 = "true"
            r0.put(r7, r1)
            java.lang.String r7 = "v5"
            java.lang.String r1 = "true"
            r0.put(r7, r1)
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.d.ex
            java.lang.Object r7 = r7.a(r1)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x03ef
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.c.h r7 = r7.K()
            java.lang.String r1 = "li"
            com.applovin.impl.sdk.c.g r3 = com.applovin.impl.sdk.c.g.b
            long r3 = r7.b(r3)
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r1, r3)
            java.lang.String r1 = "si"
            com.applovin.impl.sdk.c.g r3 = com.applovin.impl.sdk.c.g.d
            long r3 = r7.b(r3)
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r1, r3)
            java.lang.String r1 = "pf"
            com.applovin.impl.sdk.c.g r3 = com.applovin.impl.sdk.c.g.h
            long r3 = r7.b(r3)
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r1, r3)
            java.lang.String r1 = "mpf"
            com.applovin.impl.sdk.c.g r3 = com.applovin.impl.sdk.c.g.n
            long r3 = r7.b(r3)
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r0.put(r1, r3)
            java.lang.String r1 = "gpf"
            com.applovin.impl.sdk.c.g r3 = com.applovin.impl.sdk.c.g.i
            long r3 = r7.b(r3)
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r0.put(r1, r7)
        L_0x03ef:
            java.lang.String r7 = "vz"
            android.content.Context r1 = r5.c
            java.lang.String r1 = r1.getPackageName()
            java.lang.String r1 = com.applovin.impl.sdk.utils.n.f(r1)
            r0.put(r7, r1)
            if (r8 == 0) goto L_0x0423
            java.util.concurrent.atomic.AtomicReference<com.applovin.impl.sdk.k$a> r7 = r5.h
            java.lang.Object r7 = r7.get()
            com.applovin.impl.sdk.k$a r7 = (com.applovin.impl.sdk.k.a) r7
            if (r7 == 0) goto L_0x040e
            r5.e()
            goto L_0x042d
        L_0x040e:
            boolean r7 = com.applovin.impl.sdk.utils.q.b()
            if (r7 == 0) goto L_0x0423
            com.applovin.impl.sdk.k$a r7 = new com.applovin.impl.sdk.k$a
            r7.<init>()
            java.lang.String r8 = "inc"
            java.lang.String r1 = java.lang.Boolean.toString(r2)
            r0.put(r8, r1)
            goto L_0x042d
        L_0x0423:
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.k r7 = r7.N()
            com.applovin.impl.sdk.k$a r7 = r7.d()
        L_0x042d:
            java.lang.String r8 = r7.b
            boolean r1 = com.applovin.impl.sdk.utils.n.b(r8)
            if (r1 == 0) goto L_0x043a
            java.lang.String r1 = "idfa"
            r0.put(r1, r8)
        L_0x043a:
            boolean r7 = r7.a
            java.lang.String r8 = "dnt"
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            r0.put(r8, r7)
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r8 = com.applovin.impl.sdk.b.d.dT
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0460
            java.lang.String r7 = "cuid"
            com.applovin.impl.sdk.j r8 = r5.a
            java.lang.String r8 = r8.h()
            com.applovin.impl.sdk.utils.q.a(r7, r8, r0)
        L_0x0460:
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r8 = com.applovin.impl.sdk.b.d.dW
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x047b
            java.lang.String r7 = "compass_random_token"
            com.applovin.impl.sdk.j r8 = r5.a
            java.lang.String r8 = r8.i()
            r0.put(r7, r8)
        L_0x047b:
            com.applovin.impl.sdk.j r7 = r5.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r8 = com.applovin.impl.sdk.b.d.dY
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0496
            java.lang.String r7 = "applovin_random_token"
            com.applovin.impl.sdk.j r8 = r5.a
            java.lang.String r8 = r8.j()
            r0.put(r7, r8)
        L_0x0496:
            if (r6 == 0) goto L_0x049b
            r0.putAll(r6)
        L_0x049b:
            java.lang.String r6 = "rid"
            java.util.UUID r7 = java.util.UUID.randomUUID()
            java.lang.String r7 = r7.toString()
            r0.put(r6, r7)
            com.applovin.impl.sdk.j r6 = r5.a
            com.applovin.impl.sdk.network.a r6 = r6.I()
            com.applovin.impl.sdk.network.a$b r6 = r6.a()
            if (r6 == 0) goto L_0x04e4
            java.lang.String r7 = "lrm_ts_ms"
            long r1 = r6.a()
            java.lang.String r8 = java.lang.String.valueOf(r1)
            r0.put(r7, r8)
            java.lang.String r7 = "lrm_url"
            java.lang.String r8 = r6.b()
            r0.put(r7, r8)
            java.lang.String r7 = "lrm_ct_ms"
            long r1 = r6.d()
            java.lang.String r8 = java.lang.String.valueOf(r1)
            r0.put(r7, r8)
            java.lang.String r7 = "lrm_rs"
            long r1 = r6.c()
            java.lang.String r6 = java.lang.String.valueOf(r1)
            r0.put(r7, r6)
        L_0x04e4:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.k.a(java.util.Map, boolean, boolean):java.util.Map");
    }

    public d b() {
        d dVar;
        TelephonyManager telephonyManager;
        Object obj = this.d.get(d.class);
        if (obj != null) {
            dVar = (d) obj;
        } else {
            dVar = new d();
            dVar.k = Locale.getDefault();
            dVar.a = Build.MODEL;
            dVar.b = Build.VERSION.RELEASE;
            dVar.c = "android";
            dVar.d = Build.MANUFACTURER;
            dVar.e = Build.BRAND;
            dVar.f = Build.HARDWARE;
            dVar.h = Build.VERSION.SDK_INT;
            dVar.g = Build.DEVICE;
            dVar.r = n();
            dVar.A = s();
            dVar.N = i();
            try {
                dVar.B = ((SensorManager) this.c.getSystemService("sensor")).getDefaultSensor(4) != null;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Unable to retrieve gyroscope availability", th);
            }
            if (b("android.permission.READ_PHONE_STATE") && (telephonyManager = (TelephonyManager) this.c.getSystemService("phone")) != null) {
                dVar.i = telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH);
                String networkOperatorName = telephonyManager.getNetworkOperatorName();
                try {
                    dVar.j = URLEncoder.encode(networkOperatorName, "UTF-8");
                } catch (UnsupportedEncodingException unused) {
                    dVar.j = networkOperatorName;
                }
            }
            try {
                DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
                dVar.m = displayMetrics.density;
                dVar.n = displayMetrics.densityDpi;
                dVar.o = displayMetrics.xdpi;
                dVar.p = displayMetrics.ydpi;
                Point a2 = g.a(this.c);
                double sqrt = Math.sqrt(Math.pow((double) a2.x, 2.0d) + Math.pow((double) a2.y, 2.0d));
                double d2 = (double) dVar.o;
                Double.isNaN(d2);
                dVar.q = sqrt / d2;
            } catch (Throwable unused2) {
            }
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ei)).booleanValue()) {
                dVar.v = k();
            }
            if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.ej)).booleanValue()) {
                dVar.w = l();
            }
            dVar.Q = System.currentTimeMillis() - SystemClock.elapsedRealtime();
            this.d.put(d.class, dVar);
        }
        return a(dVar);
    }

    public b c() {
        PackageInfo packageInfo;
        Object obj = this.d.get(b.class);
        if (obj != null) {
            return (b) obj;
        }
        ApplicationInfo applicationInfo = this.c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.c.getPackageManager();
        String str = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.c.getPackageName(), 0);
            try {
                str = packageManager.getInstallerPackageName(applicationInfo.packageName);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            packageInfo = null;
        }
        b bVar = new b();
        bVar.c = applicationInfo.packageName;
        if (str == null) {
            str = "";
        }
        bVar.d = str;
        bVar.h = lastModified;
        bVar.a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        bVar.b = packageInfo != null ? packageInfo.versionName : "";
        bVar.e = q.a(f.g, this.a);
        bVar.f = q.a(f.h, this.a);
        bVar.g = Boolean.toString(q.b(this.a));
        this.d.put(b.class, bVar);
        return bVar;
    }

    public a d() {
        a h2 = h();
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dS)).booleanValue()) {
            return new a();
        }
        if (!h2.a || ((Boolean) this.a.a(com.applovin.impl.sdk.b.d.dR)).booleanValue()) {
            return h2;
        }
        h2.b = "";
        return h2;
    }

    public void e() {
        this.a.J().a(new i(this.a, new i.a() {
            public void a(a aVar) {
                k.this.h.set(aVar);
            }
        }), r.a.ADVERTISING_INFO_COLLECTION);
    }
}
