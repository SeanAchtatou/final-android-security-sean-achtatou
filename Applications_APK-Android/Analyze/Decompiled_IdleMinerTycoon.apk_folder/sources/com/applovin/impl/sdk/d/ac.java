package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;

public class ac extends a {
    private final Runnable a;

    public ac(j jVar, Runnable runnable) {
        this(jVar, false, runnable);
    }

    public ac(j jVar, boolean z, Runnable runnable) {
        super("TaskRunnable", jVar, z);
        this.a = runnable;
    }

    public i a() {
        return i.f;
    }

    public void run() {
        this.a.run();
    }
}
