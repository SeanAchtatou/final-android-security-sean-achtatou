package com.applovin.impl.sdk.d;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.e;
import com.applovin.impl.a.k;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.Collections;
import java.util.List;

class h extends c {
    private final a c;

    public h(a aVar, j jVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super("TaskCacheVastAd", aVar, jVar, appLovinAdLoadListener);
        this.c = aVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.c.a()) {
            a("Begin caching for VAST streaming ad #" + this.a.getAdIdNumber() + "...");
            d();
            if (this.c.e()) {
                i();
            }
            if (this.c.c() == a.b.COMPANION_AD) {
                k();
                m();
            } else {
                l();
            }
            if (!this.c.e()) {
                i();
            }
            if (this.c.c() == a.b.COMPANION_AD) {
                l();
            } else {
                k();
                m();
            }
        } else {
            a("Begin caching for VAST ad #" + this.a.getAdIdNumber() + "...");
            d();
            k();
            l();
            m();
            i();
        }
        a("Finished caching VAST ad #" + this.c.getAdIdNumber());
        long currentTimeMillis = System.currentTimeMillis() - this.c.getCreatedAtMillis();
        d.a(this.c, this.b);
        d.a(currentTimeMillis, this.c, this.b);
        a(this.c);
        b();
    }

    private void k() {
        String str;
        String str2;
        String str3;
        if (!c()) {
            if (this.c.aM()) {
                b j = this.c.j();
                if (j != null) {
                    e b = j.b();
                    if (b != null) {
                        Uri b2 = b.b();
                        String uri = b2 != null ? b2.toString() : "";
                        String c2 = b.c();
                        if (URLUtil.isValidUrl(uri) || n.b(c2)) {
                            if (b.a() == e.a.STATIC) {
                                a("Caching static companion ad at " + uri + "...");
                                Uri b3 = b(uri, Collections.emptyList(), false);
                                if (b3 != null) {
                                    b.a(b3);
                                } else {
                                    str2 = "Failed to cache static companion ad";
                                }
                            } else if (b.a() == e.a.HTML) {
                                if (n.b(uri)) {
                                    a("Begin caching HTML companion ad. Fetching from " + uri + "...");
                                    c2 = f(uri);
                                    if (n.b(c2)) {
                                        str3 = "HTML fetched. Caching HTML now...";
                                    } else {
                                        str2 = "Unable to load companion ad resources from " + uri;
                                    }
                                } else {
                                    str3 = "Caching provided HTML for companion ad. No fetch required. HTML: " + c2;
                                }
                                a(str3);
                                b.a(a(c2, Collections.emptyList(), this.c));
                            } else if (b.a() == e.a.IFRAME) {
                                str = "Skip caching of iFrame resource...";
                            } else {
                                return;
                            }
                            this.c.a(true);
                            return;
                        }
                        c("Companion ad does not have any resources attached. Skipping...");
                        return;
                    }
                    str2 = "Failed to retrieve non-video resources from companion ad. Skipping...";
                    d(str2);
                    return;
                }
                str = "No companion ad provided. Skipping...";
            } else {
                str = "Companion ad caching disabled. Skipping...";
            }
            a(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.d.c.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri
     arg types: [java.lang.String, java.util.List, int]
     candidates:
      com.applovin.impl.sdk.d.c.a(java.lang.String, java.util.List<java.lang.String>, com.applovin.impl.sdk.ad.f):java.lang.String
      com.applovin.impl.sdk.d.c.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri */
    private void l() {
        k i;
        Uri b;
        if (!c()) {
            if (!this.c.aN()) {
                a("Video caching disabled. Skipping...");
            } else if (this.c.h() != null && (i = this.c.i()) != null && (b = i.b()) != null) {
                Uri a = a(b.toString(), (List<String>) Collections.emptyList(), false);
                if (a != null) {
                    a("Video file successfully cached into: " + a);
                    i.a(a);
                    return;
                }
                d("Failed to cache video file: " + i);
            }
        }
    }

    private void m() {
        String str;
        String str2;
        if (!c()) {
            if (this.c.aL() != null) {
                a("Begin caching HTML template. Fetching from " + this.c.aL() + "...");
                str = a(this.c.aL().toString(), this.c.F());
            } else {
                str = this.c.aK();
            }
            if (n.b(str)) {
                this.c.a(a(str, this.c.F(), this.c));
                str2 = "Finish caching HTML template " + this.c.aK() + " for ad #" + this.c.getAdIdNumber();
            } else {
                str2 = "Unable to load HTML template";
            }
            a(str2);
        }
    }

    public i a() {
        return i.l;
    }

    public void run() {
        super.run();
        AnonymousClass1 r0 = new Runnable() {
            public void run() {
                h.this.j();
            }
        };
        if (this.a.I()) {
            this.b.J().c().execute(r0);
        } else {
            r0.run();
        }
    }
}
