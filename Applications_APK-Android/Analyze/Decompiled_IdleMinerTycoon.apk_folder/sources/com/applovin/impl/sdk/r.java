package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.r;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

abstract class r implements m, AppLovinNativeAdLoadListener {
    protected final j a;
    protected final p b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    private final Map<d, s> d = new HashMap();
    private final Map<d, s> e = new HashMap();
    /* access modifiers changed from: private */
    public final Map<d, Object> f = new HashMap();
    private final Set<d> g = new HashSet();

    r(j jVar) {
        this.a = jVar;
        this.b = jVar.u();
    }

    private void b(final d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        synchronized (this.c) {
            if (this.f.containsKey(dVar)) {
                this.b.d("PreloadManager", "Possibly missing prior registered preload callback.");
            }
            this.f.put(dVar, appLovinAdLoadListener);
        }
        final int intValue = ((Integer) this.a.a(com.applovin.impl.sdk.b.d.bb)).intValue();
        if (intValue > 0) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    synchronized (r.this.c) {
                        Object obj = r.this.f.get(dVar);
                        if (obj != null) {
                            r.this.f.remove(dVar);
                            p pVar = r.this.b;
                            pVar.e("PreloadManager", "Load callback for zone " + dVar + " timed out after " + intValue + " seconds");
                            r.this.a(obj, dVar, AppLovinErrorCodes.FETCH_AD_TIMEOUT);
                        }
                    }
                }
            }, TimeUnit.SECONDS.toMillis((long) intValue));
        }
    }

    private s j(d dVar) {
        return this.d.get(dVar);
    }

    private s k(d dVar) {
        return this.e.get(dVar);
    }

    private boolean l(d dVar) {
        boolean z;
        synchronized (this.c) {
            s j = j(dVar);
            z = j != null && j.c();
        }
        return z;
    }

    private s m(d dVar) {
        synchronized (this.c) {
            s k = k(dVar);
            if (k != null && k.a() > 0) {
                return k;
            }
            s j = j(dVar);
            return j;
        }
    }

    private boolean n(d dVar) {
        boolean contains;
        synchronized (this.c) {
            contains = this.g.contains(dVar);
        }
        return contains;
    }

    /* access modifiers changed from: package-private */
    public abstract d a(j jVar);

    /* access modifiers changed from: package-private */
    public abstract a a(d dVar);

    /* access modifiers changed from: package-private */
    public abstract void a(Object obj, d dVar, int i);

    /* access modifiers changed from: package-private */
    public abstract void a(Object obj, j jVar);

    public void a(LinkedHashSet<d> linkedHashSet) {
        if (this.f != null && !this.f.isEmpty()) {
            synchronized (this.c) {
                Iterator<d> it = this.f.keySet().iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    if (!next.i() && !linkedHashSet.contains(next)) {
                        Object obj = this.f.get(next);
                        it.remove();
                        p.j("AppLovinAdService", "Failed to load ad for zone (" + next.a() + "). Please check that the zone has been added to your AppLovin account and given at least 30 minutes to fully propagate.");
                        a(obj, next, -7);
                    }
                }
            }
        }
    }

    public boolean a(d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        boolean z;
        synchronized (this.c) {
            if (!n(dVar)) {
                b(dVar, appLovinAdLoadListener);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void b(d dVar, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            i(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        Object obj;
        d a2 = a(jVar);
        synchronized (this.c) {
            obj = this.f.get(a2);
            this.f.remove(a2);
            this.g.add(a2);
            j(a2).a(jVar);
            p pVar = this.b;
            pVar.b("PreloadManager", "Ad enqueued: " + jVar);
        }
        if (obj != null) {
            p pVar2 = this.b;
            pVar2.b("PreloadManager", "Called additional callback regarding " + jVar);
            a(obj, new g(a2, this.a));
        }
        p pVar3 = this.b;
        pVar3.b("PreloadManager", "Pulled ad from network and saved to preload cache: " + jVar);
    }

    public boolean b(d dVar) {
        return this.f.containsKey(dVar);
    }

    public j c(d dVar) {
        j f2;
        synchronized (this.c) {
            s m = m(dVar);
            f2 = m != null ? m.f() : null;
        }
        return f2;
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar, int i) {
        Object remove;
        p pVar = this.b;
        pVar.b("PreloadManager", "Failed to pre-load an ad of zone " + dVar + ", error code " + i);
        synchronized (this.c) {
            remove = this.f.remove(dVar);
            this.g.add(dVar);
        }
        if (remove != null) {
            try {
                a(remove, dVar, i);
            } catch (Throwable th) {
                p.c("PreloadManager", "Encountered exception while invoking user callback", th);
            }
        }
    }

    public j d(d dVar) {
        j e2;
        synchronized (this.c) {
            s m = m(dVar);
            e2 = m != null ? m.e() : null;
        }
        return e2;
    }

    public j e(d dVar) {
        g gVar;
        p pVar;
        String str;
        StringBuilder sb;
        String str2;
        g gVar2;
        synchronized (this.c) {
            s j = j(dVar);
            gVar = null;
            if (j != null) {
                s k = k(dVar);
                if (k.c()) {
                    gVar2 = new g(dVar, this.a);
                } else if (j.a() > 0) {
                    k.a(j.e());
                    gVar2 = new g(dVar, this.a);
                }
                gVar = gVar2;
            }
        }
        if (gVar != null) {
            pVar = this.b;
            str = "PreloadManager";
            sb = new StringBuilder();
            str2 = "Retrieved ad of zone ";
        } else {
            pVar = this.b;
            str = "PreloadManager";
            sb = new StringBuilder();
            str2 = "Unable to retrieve ad of zone ";
        }
        sb.append(str2);
        sb.append(dVar);
        sb.append("...");
        pVar.b(str, sb.toString());
        return gVar;
    }

    public void f(d dVar) {
        if (dVar != null) {
            int i = 0;
            synchronized (this.c) {
                s j = j(dVar);
                if (j != null) {
                    i = j.b() - j.a();
                }
            }
            b(dVar, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0021, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.applovin.impl.sdk.ad.d r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.c
            monitor-enter(r0)
            com.applovin.impl.sdk.s r1 = r3.k(r4)     // Catch:{ all -> 0x0022 }
            r2 = 1
            if (r1 == 0) goto L_0x0012
            int r1 = r1.a()     // Catch:{ all -> 0x0022 }
            if (r1 <= 0) goto L_0x0012
            monitor-exit(r0)     // Catch:{ all -> 0x0022 }
            return r2
        L_0x0012:
            com.applovin.impl.sdk.s r4 = r3.j(r4)     // Catch:{ all -> 0x0022 }
            if (r4 == 0) goto L_0x001f
            boolean r4 = r4.d()     // Catch:{ all -> 0x0022 }
            if (r4 != 0) goto L_0x001f
            goto L_0x0020
        L_0x001f:
            r2 = 0
        L_0x0020:
            monitor-exit(r0)     // Catch:{ all -> 0x0022 }
            return r2
        L_0x0022:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0022 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.r.g(com.applovin.impl.sdk.ad.d):boolean");
    }

    public void h(d dVar) {
        synchronized (this.c) {
            s j = j(dVar);
            if (j != null) {
                j.a(dVar.e());
            } else {
                this.d.put(dVar, new s(dVar.e()));
            }
            s k = k(dVar);
            if (k != null) {
                k.a(dVar.f());
            } else {
                this.e.put(dVar, new s(dVar.f()));
            }
        }
    }

    public void i(d dVar) {
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.d.bc)).booleanValue() && !l(dVar)) {
            p pVar = this.b;
            pVar.b("PreloadManager", "Preloading ad for zone " + dVar + "...");
            this.a.J().a(a(dVar), r.a.MAIN, 500);
        }
    }
}
