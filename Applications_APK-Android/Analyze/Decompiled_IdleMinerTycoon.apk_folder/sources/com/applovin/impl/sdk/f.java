package com.applovin.impl.sdk;

import android.os.Bundle;
import com.applovin.communicator.AppLovinCommunicator;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorPublisher;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.communicator.AppLovinSdkTopic;
import com.applovin.impl.communicator.CommunicatorMessageImpl;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.b.a;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.BundleUtils;
import com.applovin.impl.sdk.utils.i;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TapjoyConstants;
import java.util.Map;
import org.json.JSONObject;

public class f implements AppLovinCommunicatorPublisher, AppLovinCommunicatorSubscriber {
    private final j a;
    private final AppLovinCommunicator b;

    f(j jVar) {
        this.a = jVar;
        this.b = AppLovinCommunicator.getInstance(jVar.C());
        if (!"HSrCHRtOan6wp2kwOIGJC1RDtuSrF2mWVbio2aBcMHX9KF3iTJ1lLSzCKP1ZSo5yNolPNw1kCTtWpxELFF4ah1".equalsIgnoreCase(jVar.s())) {
            this.b.a(jVar);
            this.b.subscribe(this, AppLovinSdkTopic.ALL_TOPICS);
        }
    }

    private void a(Bundle bundle, String str) {
        if (!"log".equals(str)) {
            p u = this.a.u();
            u.b("CommunicatorService", "Sending message " + bundle + " for topic: " + str + "...");
        }
        this.b.getMessagingService().publish(CommunicatorMessageImpl.create(bundle, str, this, this.a.b(a.a).contains(str)));
    }

    public void a(com.applovin.impl.mediation.b.a aVar, String str) {
        boolean j = aVar instanceof c ? ((c) aVar).j() : false;
        Bundle bundle = new Bundle();
        bundle.putString("type", str);
        bundle.putString("id", aVar.b());
        bundle.putString("network_name", aVar.D());
        bundle.putString("max_ad_unit_id", aVar.getAdUnitId());
        bundle.putString("third_party_ad_placement_id", aVar.e());
        bundle.putString("ad_format", aVar.getFormat().getLabel());
        bundle.putString("is_fallback_ad", String.valueOf(j));
        a(bundle, "max_ad_events");
    }

    public void a(JSONObject jSONObject, boolean z) {
        Bundle c = i.c(i.b(i.b(jSONObject, "communicator_settings", new JSONObject(), this.a), "safedk_settings", new JSONObject(), this.a));
        Bundle bundle = new Bundle();
        bundle.putString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.s());
        bundle.putString("applovin_random_token", this.a.j());
        bundle.putString(TapjoyConstants.TJC_DEVICE_TYPE_NAME, AppLovinSdkUtils.isTablet(this.a.C()) ? "tablet" : "phone");
        bundle.putString("init_success", String.valueOf(z));
        bundle.putBundle(DownloadManager.SETTINGS, c);
        bundle.putBoolean("debug_mode", ((Boolean) this.a.a(d.eR)).booleanValue());
        a(bundle, "safedk_init");
    }

    public String getCommunicatorId() {
        return "applovin_sdk";
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if (AppLovinSdkTopic.HTTP_REQUEST.equalsIgnoreCase(appLovinCommunicatorMessage.getTopic())) {
            Bundle messageData = appLovinCommunicatorMessage.getMessageData();
            Map<String, String> a2 = i.a(messageData.getBundle("query_params"));
            Map<String, Object> map = BundleUtils.toMap(messageData.getBundle("post_body"));
            Map<String, String> a3 = i.a(messageData.getBundle("headers"));
            if (!map.containsKey(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY)) {
                map.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.s());
            }
            this.a.M().a(new f.a().a(messageData.getString("url")).b(messageData.getString("backup_url")).a(a2).c(map).b(a3).a(((Boolean) this.a.a(d.eR)).booleanValue()).a());
        }
    }
}
