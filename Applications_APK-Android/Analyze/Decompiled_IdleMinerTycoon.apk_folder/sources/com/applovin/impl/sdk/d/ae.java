package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a;
import org.json.JSONObject;

public abstract class ae extends ab {
    protected ae(String str, j jVar) {
        super(str, jVar);
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        c c = c(jSONObject);
        if (c != null) {
            a(c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r3 = java.util.Collections.emptyMap();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.applovin.impl.sdk.a.c c(org.json.JSONObject r3) {
        /*
            r2 = this;
            org.json.JSONObject r0 = com.applovin.impl.sdk.utils.h.a(r3)     // Catch:{ JSONException -> 0x002d }
            com.applovin.impl.sdk.j r1 = r2.b     // Catch:{ JSONException -> 0x002d }
            com.applovin.impl.sdk.utils.h.d(r0, r1)     // Catch:{ JSONException -> 0x002d }
            com.applovin.impl.sdk.j r1 = r2.b     // Catch:{ JSONException -> 0x002d }
            com.applovin.impl.sdk.utils.h.c(r3, r1)     // Catch:{ JSONException -> 0x002d }
            java.lang.String r3 = "params"
            java.lang.Object r3 = r0.get(r3)     // Catch:{ Throwable -> 0x001b }
            org.json.JSONObject r3 = (org.json.JSONObject) r3     // Catch:{ Throwable -> 0x001b }
            java.util.Map r3 = com.applovin.impl.sdk.utils.i.a(r3)     // Catch:{ Throwable -> 0x001b }
            goto L_0x001f
        L_0x001b:
            java.util.Map r3 = java.util.Collections.emptyMap()     // Catch:{ JSONException -> 0x002d }
        L_0x001f:
            java.lang.String r1 = "result"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Throwable -> 0x0026 }
            goto L_0x0028
        L_0x0026:
            java.lang.String r0 = "network_timeout"
        L_0x0028:
            com.applovin.impl.sdk.a.c r3 = com.applovin.impl.sdk.a.c.a(r0, r3)     // Catch:{ JSONException -> 0x002d }
            return r3
        L_0x002d:
            r3 = move-exception
            java.lang.String r0 = "Unable to parse API response"
            r2.a(r0, r3)
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.d.ae.c(org.json.JSONObject):com.applovin.impl.sdk.a.c");
    }

    /* access modifiers changed from: protected */
    public abstract void a(c cVar);

    /* access modifiers changed from: protected */
    public abstract boolean c();

    public void run() {
        a(i(), new a.c<JSONObject>() {
            public void a(int i) {
                if (!ae.this.c()) {
                    ae.this.a(i);
                }
            }

            public void a(JSONObject jSONObject, int i) {
                if (!ae.this.c()) {
                    ae.this.b(jSONObject);
                }
            }
        });
    }
}
