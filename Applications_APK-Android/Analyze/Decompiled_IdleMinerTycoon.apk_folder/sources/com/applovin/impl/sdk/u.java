package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.q;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class u {
    final j a;
    private final AtomicBoolean b = new AtomicBoolean();
    private final AtomicBoolean c = new AtomicBoolean();
    private Date d;
    private Date e;

    u(j jVar) {
        this.a = jVar;
        Application application = (Application) jVar.C();
        application.registerActivityLifecycleCallbacks(new a() {
            public void onActivityResumed(Activity activity) {
                super.onActivityResumed(activity);
                u.this.d();
            }
        });
        application.registerComponentCallbacks(new ComponentCallbacks2() {
            public void onConfigurationChanged(Configuration configuration) {
            }

            public void onLowMemory() {
            }

            public void onTrimMemory(int i) {
                if (i == 20) {
                    u.this.e();
                }
            }
        });
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        application.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.intent.action.USER_PRESENT".equals(action)) {
                    if (q.c()) {
                        u.this.d();
                    }
                } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                    u.this.e();
                }
            }
        }, intentFilter);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.c.compareAndSet(true, false)) {
            g();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.c.compareAndSet(false, true)) {
            f();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.l, com.applovin.impl.sdk.k$a):java.util.HashMap<java.lang.String, java.lang.String>
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.k$a, java.util.Map<java.lang.String, java.lang.String>):void
      com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void */
    private void f() {
        this.a.u().b("SessionTracker", "Application Paused");
        this.a.ae().sendBroadcastSync(new Intent("com.applovin.application_paused"));
        if (!this.b.get() && ((Boolean) this.a.a(d.dQ)).booleanValue()) {
            boolean booleanValue = ((Boolean) this.a.a(d.dN)).booleanValue();
            long millis = TimeUnit.MINUTES.toMillis(((Long) this.a.a(d.dP)).longValue());
            if (this.d == null || System.currentTimeMillis() - this.d.getTime() >= millis) {
                ((EventServiceImpl) this.a.p()).a("paused", false);
                if (booleanValue) {
                    this.d = new Date();
                }
            }
            if (!booleanValue) {
                this.d = new Date();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.l, com.applovin.impl.sdk.k$a):java.util.HashMap<java.lang.String, java.lang.String>
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.k$a, java.util.Map<java.lang.String, java.lang.String>):void
      com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void */
    private void g() {
        this.a.u().b("SessionTracker", "Application Resumed");
        boolean booleanValue = ((Boolean) this.a.a(d.dN)).booleanValue();
        long longValue = ((Long) this.a.a(d.dO)).longValue();
        this.a.ae().sendBroadcastSync(new Intent("com.applovin.application_resumed"));
        if (!this.b.getAndSet(false)) {
            long millis = TimeUnit.MINUTES.toMillis(longValue);
            if (this.e == null || System.currentTimeMillis() - this.e.getTime() >= millis) {
                ((EventServiceImpl) this.a.p()).a("resumed", false);
                if (booleanValue) {
                    this.e = new Date();
                }
            }
            if (!booleanValue) {
                this.e = new Date();
            }
            this.a.K().a(g.k);
        }
    }

    public boolean a() {
        return this.c.get();
    }

    public void b() {
        this.b.set(true);
    }

    public void c() {
        this.b.set(false);
    }
}
