package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.security.NetworkSecurityPolicy;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TapjoyConstants;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {
    private static final int[] a = {7, 4, 2, 1, 11};
    private static final int[] b = {5, 6, 10, 3, 9, 8, 14};
    private static final int[] c = {15, 12, 13};

    public static String a(InputStream inputStream, j jVar) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[((Integer) jVar.a(d.dD)).intValue()];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    return byteArrayOutputStream.toString("UTF-8");
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            jVar.u().b("ConnectionUtils", "Encountered error while reading stream", th);
            return null;
        }
    }

    public static String a(String str, j jVar) {
        return a((String) jVar.a(d.aN), str, jVar);
    }

    public static String a(String str, String str2, j jVar) {
        if (str == null || str.length() < 4) {
            throw new IllegalArgumentException("Invalid domain specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (jVar != null) {
            return str + str2;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static JSONObject a(JSONObject jSONObject) throws JSONException {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.e.a(com.applovin.impl.sdk.b.d<?>, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.d<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.b.e.a(java.lang.String, com.applovin.impl.sdk.b.d):com.applovin.impl.sdk.b.d<ST>
      com.applovin.impl.sdk.b.e.a(com.applovin.impl.sdk.b.d<?>, java.lang.Object):void */
    public static void a(int i, j jVar) {
        String str;
        StringBuilder sb;
        String str2;
        e B = jVar.B();
        if (i == 401) {
            B.a(d.X, "");
            B.a(d.Y, "");
            B.b();
            str = "AppLovinSdk";
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(jVar.s());
            str2 = "\" is rejected by AppLovin. Please make sure the SDK key is correct.";
        } else if (i == 418) {
            B.a((d<?>) d.W, (Object) true);
            B.b();
            str = "AppLovinSdk";
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(jVar.s());
            str2 = "\" has been blocked. Please contact AppLovin support at support@applovin.com.";
        } else if ((i >= 400 && i < 500) || i == -1) {
            jVar.f();
            return;
        } else {
            return;
        }
        sb.append(str2);
        p.j(str, sb.toString());
    }

    public static void a(JSONObject jSONObject, boolean z, j jVar) {
        jVar.ab().a(jSONObject, z);
    }

    public static boolean a() {
        return a((String) null);
    }

    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        if (context.getSystemService("connectivity") == null) {
            return true;
        }
        NetworkInfo b2 = b(context);
        if (b2 != null) {
            return b2.isConnected();
        }
        return false;
    }

    public static boolean a(String str) {
        if (g.e()) {
            return (!g.f() || TextUtils.isEmpty(str)) ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(str);
        }
        return true;
    }

    private static NetworkInfo b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        return null;
    }

    public static String b(String str, j jVar) {
        return a((String) jVar.a(d.aO), str, jVar);
    }

    public static void c(JSONObject jSONObject, j jVar) {
        String b2 = i.b(jSONObject, "persisted_data", (String) null, jVar);
        if (n.b(b2)) {
            jVar.a(f.z, b2);
            jVar.u().c("ConnectionUtils", "Updated persisted data");
        }
    }

    public static void d(JSONObject jSONObject, j jVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (jVar != null) {
            try {
                if (jSONObject.has(DownloadManager.SETTINGS)) {
                    e B = jVar.B();
                    if (!jSONObject.isNull(DownloadManager.SETTINGS)) {
                        B.a(jSONObject.getJSONObject(DownloadManager.SETTINGS));
                        B.b();
                        jVar.u().b("ConnectionUtils", "New settings processed");
                    }
                }
            } catch (JSONException e) {
                jVar.u().b("ConnectionUtils", "Unable to parse settings out of API response", e);
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static Map<String, String> e(j jVar) {
        HashMap hashMap = new HashMap();
        String str = (String) jVar.a(d.Y);
        if (n.b(str)) {
            hashMap.put("device_token", str);
        } else if (!((Boolean) jVar.a(d.eR)).booleanValue()) {
            hashMap.put(TapjoyConstants.TJC_API_KEY, jVar.s());
        }
        hashMap.put("sc", n.e((String) jVar.a(d.aa)));
        hashMap.put("sc2", n.e((String) jVar.a(d.ab)));
        hashMap.put("server_installed_at", n.e((String) jVar.a(d.ac)));
        q.a("persisted_data", n.e((String) jVar.a(f.z)), hashMap);
        return hashMap;
    }

    public static void e(JSONObject jSONObject, j jVar) {
        JSONArray b2 = i.b(jSONObject, "zones", (JSONArray) null, jVar);
        if (b2 != null) {
            Iterator<com.applovin.impl.sdk.ad.d> it = jVar.V().a(b2).iterator();
            while (it.hasNext()) {
                com.applovin.impl.sdk.ad.d next = it.next();
                if (next.d()) {
                    jVar.o().preloadAds(next);
                } else {
                    jVar.n().preloadAds(next);
                }
            }
            jVar.S().a(jVar.V().a());
            jVar.T().a(jVar.V().a());
        }
    }

    public static String f(j jVar) {
        NetworkInfo b2 = b(jVar.C());
        if (b2 == null) {
            return "unknown";
        }
        int type = b2.getType();
        int subtype = b2.getSubtype();
        return type == 1 ? "wifi" : type == 0 ? a(subtype, a) ? "2g" : a(subtype, b) ? ConnectivityService.NETWORK_TYPE_3G : a(subtype, c) ? "4g" : TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE : "unknown";
    }

    public static void f(JSONObject jSONObject, j jVar) {
        JSONObject b2 = i.b(jSONObject, "variables", (JSONObject) null, jVar);
        if (b2 != null) {
            jVar.r().updateVariables(b2);
        }
    }

    public static String g(j jVar) {
        return a((String) jVar.a(d.aL), "4.0/ad", jVar);
    }

    public static String h(j jVar) {
        return a((String) jVar.a(d.aM), "4.0/ad", jVar);
    }

    public static String i(j jVar) {
        return a((String) jVar.a(d.aR), "1.0/variable_config", jVar);
    }

    public static String j(j jVar) {
        return a((String) jVar.a(d.aS), "1.0/variable_config", jVar);
    }
}
