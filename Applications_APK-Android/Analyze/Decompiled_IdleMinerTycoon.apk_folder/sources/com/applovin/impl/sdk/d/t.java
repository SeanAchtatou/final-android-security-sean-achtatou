package com.applovin.impl.sdk.d;

import com.applovin.impl.a.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdLoadListener;
import org.json.JSONObject;

abstract class t extends a {
    private final AppLovinAdLoadListener a;
    private final a c;

    private static final class a extends com.applovin.impl.a.c {
        a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, j jVar) {
            super(jSONObject, jSONObject2, bVar, jVar);
        }

        /* access modifiers changed from: package-private */
        public void a(s sVar) {
            if (sVar != null) {
                this.a.add(sVar);
                return;
            }
            throw new IllegalArgumentException("No aggregated vast response specified");
        }
    }

    private static final class b extends t {
        private final JSONObject a;

        b(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
            super(cVar, appLovinAdLoadListener, jVar);
            if (appLovinAdLoadListener != null) {
                this.a = cVar.c();
                return;
            }
            throw new IllegalArgumentException("No callback specified.");
        }

        public i a() {
            return i.s;
        }

        public void run() {
            d dVar;
            a("Processing SDK JSON response...");
            String b = com.applovin.impl.sdk.utils.i.b(this.a, "xml", (String) null, this.b);
            if (!n.b(b)) {
                d("No VAST response received.");
                dVar = d.NO_WRAPPER_RESPONSE;
            } else if (b.length() < ((Integer) this.b.a(com.applovin.impl.sdk.b.d.eI)).intValue()) {
                try {
                    a(com.applovin.impl.sdk.utils.t.a(b, this.b));
                    return;
                } catch (Throwable th) {
                    a("Unable to parse VAST response", th);
                    a(d.XML_PARSING);
                    this.b.L().a(a());
                    return;
                }
            } else {
                d("VAST response is over max length");
                dVar = d.XML_PARSING;
            }
            a(dVar);
        }
    }

    private static final class c extends t {
        private final s a;

        c(s sVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
            super(cVar, appLovinAdLoadListener, jVar);
            if (sVar == null) {
                throw new IllegalArgumentException("No response specified.");
            } else if (cVar == null) {
                throw new IllegalArgumentException("No context specified.");
            } else if (appLovinAdLoadListener != null) {
                this.a = sVar;
            } else {
                throw new IllegalArgumentException("No callback specified.");
            }
        }

        public i a() {
            return i.t;
        }

        public void run() {
            a("Processing VAST Wrapper response...");
            a(this.a);
        }
    }

    t(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        super("TaskProcessVastResponse", jVar);
        if (cVar != null) {
            this.a = appLovinAdLoadListener;
            this.c = (a) cVar;
            return;
        }
        throw new IllegalArgumentException("No context specified.");
    }

    public static t a(s sVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        return new c(sVar, cVar, appLovinAdLoadListener, jVar);
    }

    public static t a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        return new b(new a(jSONObject, jSONObject2, bVar, jVar), appLovinAdLoadListener, jVar);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        d("Failed to process VAST response due to VAST error code " + dVar);
        com.applovin.impl.a.i.a(this.c, this.a, dVar, -6, this.b);
    }

    /* access modifiers changed from: package-private */
    public void a(s sVar) {
        d dVar;
        a aVar;
        int a2 = this.c.a();
        a("Finished parsing XML at depth " + a2);
        this.c.a(sVar);
        if (com.applovin.impl.a.i.a(sVar)) {
            int intValue = ((Integer) this.b.a(com.applovin.impl.sdk.b.d.eJ)).intValue();
            if (a2 < intValue) {
                a("VAST response is wrapper. Resolving...");
                aVar = new aa(this.c, this.a, this.b);
            } else {
                d("Reached beyond max wrapper depth of " + intValue);
                dVar = d.WRAPPER_LIMIT_REACHED;
                a(dVar);
                return;
            }
        } else if (com.applovin.impl.a.i.b(sVar)) {
            a("VAST response is inline. Rendering ad...");
            aVar = new w(this.c, this.a, this.b);
        } else {
            d("VAST response is an error");
            dVar = d.NO_WRAPPER_RESPONSE;
            a(dVar);
            return;
        }
        this.b.J().a(aVar);
    }
}
