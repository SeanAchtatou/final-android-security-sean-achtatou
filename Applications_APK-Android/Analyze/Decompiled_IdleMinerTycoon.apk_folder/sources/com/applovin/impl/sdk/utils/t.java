package com.applovin.impl.sdk.utils;

import android.util.Xml;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class t {
    /* access modifiers changed from: private */
    public final p a;
    /* access modifiers changed from: private */
    public Stack<a> b;
    /* access modifiers changed from: private */
    public StringBuilder c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public a e;

    private static class a extends s {
        a(String str, Map<String, String> map, s sVar) {
            super(str, map, sVar);
        }

        /* access modifiers changed from: package-private */
        public void a(s sVar) {
            if (sVar != null) {
                this.c.add(sVar);
                return;
            }
            throw new IllegalArgumentException("None specified.");
        }

        /* access modifiers changed from: package-private */
        public void d(String str) {
            this.b = str;
        }
    }

    t(j jVar) {
        if (jVar != null) {
            this.a = jVar.u();
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    public static s a(String str, j jVar) throws SAXException {
        return new t(jVar).a(str);
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(Attributes attributes) {
        if (attributes == null) {
            return Collections.emptyMap();
        }
        int length = attributes.getLength();
        HashMap hashMap = new HashMap(length);
        for (int i = 0; i < length; i++) {
            hashMap.put(attributes.getQName(i), attributes.getValue(i));
        }
        return hashMap;
    }

    public s a(String str) throws SAXException {
        if (str != null) {
            this.c = new StringBuilder();
            this.b = new Stack<>();
            this.e = null;
            Xml.parse(str, new ContentHandler() {
                public void characters(char[] cArr, int i, int i2) {
                    String trim = new String(Arrays.copyOfRange(cArr, i, i2)).trim();
                    if (n.b(trim)) {
                        t.this.c.append(trim);
                    }
                }

                public void endDocument() {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - t.this.d;
                    p a2 = t.this.a;
                    a2.b("XmlParser", "Finished parsing in " + seconds + " seconds");
                }

                public void endElement(String str, String str2, String str3) {
                    a unused = t.this.e = (a) t.this.b.pop();
                    t.this.e.d(t.this.c.toString().trim());
                    t.this.c.setLength(0);
                }

                public void endPrefixMapping(String str) {
                }

                public void ignorableWhitespace(char[] cArr, int i, int i2) {
                }

                public void processingInstruction(String str, String str2) {
                }

                public void setDocumentLocator(Locator locator) {
                }

                public void skippedEntity(String str) {
                }

                public void startDocument() {
                    t.this.a.b("XmlParser", "Begin parsing...");
                    long unused = t.this.d = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                }

                public void startElement(String str, String str2, String str3, Attributes attributes) throws SAXException {
                    a aVar = null;
                    try {
                        if (!t.this.b.isEmpty()) {
                            aVar = (a) t.this.b.peek();
                        }
                        a aVar2 = new a(str2, t.this.a(attributes), aVar);
                        if (aVar != null) {
                            aVar.a(aVar2);
                        }
                        t.this.b.push(aVar2);
                    } catch (Exception e) {
                        p a2 = t.this.a;
                        a2.b("XmlParser", "Unable to process element <" + str2 + ">", e);
                        throw new SAXException("Failed to start element", e);
                    }
                }

                public void startPrefixMapping(String str, String str2) {
                }
            });
            if (this.e != null) {
                return this.e;
            }
            throw new SAXException("Unable to parse XML into node");
        }
        throw new IllegalArgumentException("Unable to parse. No XML specified.");
    }
}
