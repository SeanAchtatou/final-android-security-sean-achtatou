package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.utils.n;
import com.ironsource.sdk.constants.Constants;

public class p {
    private final j a;

    p(j jVar) {
        this.a = jVar;
    }

    private void a(String str, String str2, boolean z) {
        int intValue;
        if (n.b(str2) && (intValue = ((Integer) this.a.a(d.ak)).intValue()) > 0) {
            int length = str2.length();
            int i = ((length + intValue) - 1) / intValue;
            for (int i2 = 0; i2 < i; i2++) {
                int i3 = i2 * intValue;
                int min = Math.min(length, i3 + intValue);
                if (z) {
                    Log.d(str, str2.substring(i3, min));
                } else {
                    b(str, str2.substring(i3, min));
                }
            }
        }
    }

    private boolean a() {
        return this.a.B().e();
    }

    public static void c(String str, String str2, Throwable th) {
        Log.e("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2, th);
    }

    public static void g(String str, String str2) {
        Log.d("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2);
    }

    public static void h(String str, String str2) {
        Log.i("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2);
    }

    public static void i(String str, String str2) {
        Log.w("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2);
    }

    public static void j(String str, String str2) {
        c(str, str2, null);
    }

    private void k(String str, String str2) {
    }

    public void a(String str, Boolean bool, String str2) {
        a(str, bool, str2, null);
    }

    public void a(String str, Boolean bool, String str2, Throwable th) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.e("AppLovinSdk", str3, th);
            k("ERROR", str3 + " : " + th);
        }
        if (bool.booleanValue() && ((Boolean) this.a.a(d.eE)).booleanValue() && this.a.O() != null) {
            this.a.O().a(str2, th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.p.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.p.a(java.lang.String, java.lang.Boolean, java.lang.String):void
      com.applovin.impl.sdk.p.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.applovin.impl.sdk.p.a(java.lang.String, java.lang.String, boolean):void */
    public void a(String str, String str2) {
        if (a()) {
            a(str, str2, false);
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.w("AppLovinSdk", str3, th);
            k("WARN", str3);
        }
    }

    public void b(String str, String str2) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.d("AppLovinSdk", str3);
            k("DEBUG", str3);
        }
    }

    public void b(String str, String str2, Throwable th) {
        a(str, true, str2, th);
    }

    public void c(String str, String str2) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.i("AppLovinSdk", str3);
            k("INFO", str3);
        }
    }

    public void d(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    public void e(String str, String str2) {
        b(str, str2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.p.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.p.a(java.lang.String, java.lang.Boolean, java.lang.String):void
      com.applovin.impl.sdk.p.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.applovin.impl.sdk.p.a(java.lang.String, java.lang.String, boolean):void */
    public void f(String str, String str2) {
        a(str, str2, true);
    }
}
