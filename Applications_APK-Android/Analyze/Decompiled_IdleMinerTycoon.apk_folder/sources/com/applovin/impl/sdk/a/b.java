package com.applovin.impl.sdk.a;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.applovin.impl.adview.m;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.j;

public class b {
    /* access modifiers changed from: private */
    public final j a;
    /* access modifiers changed from: private */
    public final m b;
    /* access modifiers changed from: private */
    public AlertDialog c;

    public b(m mVar, j jVar) {
        this.a = jVar;
        this.b = mVar;
    }

    public void a() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                if (b.this.c != null) {
                    b.this.c.dismiss();
                }
            }
        });
    }

    public void b() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.a.a(d.bR));
                builder.setMessage((CharSequence) b.this.a.a(d.bS));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.a.a(d.bU), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.continueVideo();
                        b.this.b.resumeReportRewardTask();
                    }
                });
                builder.setNegativeButton((CharSequence) b.this.a.a(d.bT), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.skipVideo();
                        b.this.b.resumeReportRewardTask();
                    }
                });
                AlertDialog unused = b.this.c = builder.show();
            }
        });
    }

    public void c() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.a.a(d.bW));
                builder.setMessage((CharSequence) b.this.a.a(d.bX));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.a.a(d.bZ), (DialogInterface.OnClickListener) null);
                builder.setNegativeButton((CharSequence) b.this.a.a(d.bY), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.dismiss();
                    }
                });
                AlertDialog unused = b.this.c = builder.show();
            }
        });
    }

    public boolean d() {
        if (this.c != null) {
            return this.c.isShowing();
        }
        return false;
    }
}
