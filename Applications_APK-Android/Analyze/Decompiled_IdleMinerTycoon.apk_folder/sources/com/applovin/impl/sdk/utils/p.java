package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.b.g;
import com.applovin.impl.sdk.j;
import java.util.Locale;
import java.util.UUID;

public final class p {
    private final j a;
    private String b = d();
    private final String c;
    private final String d;

    public p(j jVar) {
        this.a = jVar;
        this.c = a(f.e, (String) g.b(f.d, (Object) null, jVar.C()));
        this.d = a(f.f, (String) jVar.a(d.X));
    }

    private String a(f<String> fVar, String str) {
        String str2 = (String) g.b(fVar, (Object) null, this.a.C());
        if (n.b(str2)) {
            return str2;
        }
        if (!n.b(str)) {
            str = UUID.randomUUID().toString().toLowerCase(Locale.US);
        }
        g.a(fVar, str, this.a.C());
        return str;
    }

    private String d() {
        if (!((Boolean) this.a.a(d.dZ)).booleanValue()) {
            this.a.b(f.c);
        }
        String str = (String) this.a.a(f.c);
        if (!n.b(str)) {
            return null;
        }
        com.applovin.impl.sdk.p u = this.a.u();
        u.b("AppLovinSdk", "Using identifier (" + str + ") from previous session");
        this.b = str;
        return null;
    }

    public String a() {
        return this.b;
    }

    public void a(String str) {
        if (((Boolean) this.a.a(d.dZ)).booleanValue()) {
            this.a.a(f.c, str);
        }
        this.b = str;
    }

    public String b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }
}
