package com.applovin.impl.sdk;

import android.content.Context;
import android.net.Uri;
import android.support.v4.media.session.PlaybackStateCompat;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.c.g;
import com.appsflyer.share.Constants;
import com.mintegral.msdk.base.entity.CampaignEx;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class n {
    private final String a = "FileManager";
    private final j b;
    private final p c;
    private final Object d = new Object();

    n(j jVar) {
        this.b = jVar;
        this.c = jVar.u();
    }

    private long a() {
        long longValue = ((Long) this.b.a(d.bG)).longValue();
        if (longValue < 0 || !b()) {
            return -1;
        }
        return longValue;
    }

    private long a(long j) {
        return j / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private void a(long j, Context context) {
        p pVar;
        String str;
        String str2;
        if (b()) {
            long intValue = (long) ((Integer) this.b.a(d.bH)).intValue();
            if (intValue == -1) {
                pVar = this.c;
                str = "FileManager";
                str2 = "Cache has no maximum size set; skipping drop...";
            } else if (a(j) > intValue) {
                this.c.b("FileManager", "Cache has exceeded maximum size; dropping...");
                for (File b2 : d(context)) {
                    b(b2);
                }
                this.b.K().a(g.f);
                return;
            } else {
                pVar = this.c;
                str = "FileManager";
                str2 = "Cache is present but under size limit; not dropping...";
            }
            pVar.b(str, str2);
        }
    }

    private boolean a(File file, String str, List<String> list, boolean z, e eVar) {
        if (file == null || !file.exists() || file.isDirectory()) {
            ByteArrayOutputStream a2 = a(str, list, z);
            if (!(eVar == null || a2 == null)) {
                eVar.a((long) a2.size());
            }
            return a(a2, file);
        }
        p pVar = this.c;
        pVar.b("FileManager", "File exists for " + str);
        if (eVar == null) {
            return true;
        }
        eVar.b(file.length());
        return true;
    }

    private boolean b() {
        return ((Boolean) this.b.a(d.bF)).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.FileOutputStream, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x003b=Splitter:B:18:0x003b, B:24:0x004b=Splitter:B:24:0x004b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.io.ByteArrayOutputStream r5, java.io.File r6) {
        /*
            r4 = this;
            com.applovin.impl.sdk.p r0 = r4.c
            java.lang.String r1 = "FileManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Writing resource to filesystem: "
            r2.append(r3)
            java.lang.String r3 = r6.getName()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.b(r1, r2)
            java.lang.Object r0 = r4.d
            monitor-enter(r0)
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x004a, Throwable -> 0x003a }
            r2.<init>(r6)     // Catch:{ IOException -> 0x004a, Throwable -> 0x003a }
            r5.writeTo(r2)     // Catch:{ IOException -> 0x0035, Throwable -> 0x0032, all -> 0x002f }
            r5 = 1
            com.applovin.impl.sdk.j r6 = r4.b     // Catch:{ all -> 0x0060 }
            com.applovin.impl.sdk.utils.q.a(r2, r6)     // Catch:{ all -> 0x0060 }
            goto L_0x0058
        L_0x002f:
            r5 = move-exception
            r1 = r2
            goto L_0x005a
        L_0x0032:
            r5 = move-exception
            r1 = r2
            goto L_0x003b
        L_0x0035:
            r5 = move-exception
            r1 = r2
            goto L_0x004b
        L_0x0038:
            r5 = move-exception
            goto L_0x005a
        L_0x003a:
            r5 = move-exception
        L_0x003b:
            com.applovin.impl.sdk.p r6 = r4.c     // Catch:{ all -> 0x0038 }
            java.lang.String r2 = "FileManager"
            java.lang.String r3 = "Unknown failure to write file."
            r6.b(r2, r3, r5)     // Catch:{ all -> 0x0038 }
            com.applovin.impl.sdk.j r5 = r4.b     // Catch:{ all -> 0x0060 }
        L_0x0046:
            com.applovin.impl.sdk.utils.q.a(r1, r5)     // Catch:{ all -> 0x0060 }
            goto L_0x0057
        L_0x004a:
            r5 = move-exception
        L_0x004b:
            com.applovin.impl.sdk.p r6 = r4.c     // Catch:{ all -> 0x0038 }
            java.lang.String r2 = "FileManager"
            java.lang.String r3 = "Unable to write data to file."
            r6.b(r2, r3, r5)     // Catch:{ all -> 0x0038 }
            com.applovin.impl.sdk.j r5 = r4.b     // Catch:{ all -> 0x0060 }
            goto L_0x0046
        L_0x0057:
            r5 = 0
        L_0x0058:
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            return r5
        L_0x005a:
            com.applovin.impl.sdk.j r6 = r4.b     // Catch:{ all -> 0x0060 }
            com.applovin.impl.sdk.utils.q.a(r1, r6)     // Catch:{ all -> 0x0060 }
            throw r5     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0060 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.n.b(java.io.ByteArrayOutputStream, java.io.File):boolean");
    }

    private boolean b(File file) {
        boolean delete;
        p pVar = this.c;
        pVar.b("FileManager", "Removing file " + file.getName() + " from filesystem...");
        synchronized (this.d) {
            try {
                delete = file.delete();
            } catch (Exception e) {
                p pVar2 = this.c;
                pVar2.b("FileManager", "Failed to remove file " + file.getName() + " from filesystem!", e);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
        return delete;
    }

    private long c(Context context) {
        long j;
        boolean z;
        long a2 = a();
        boolean z2 = a2 != -1;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        synchronized (this.d) {
            List<String> b2 = this.b.b(d.bN);
            j = 0;
            for (File next : d(context)) {
                if (!z2 || b2.contains(next.getName()) || seconds - TimeUnit.MILLISECONDS.toSeconds(next.lastModified()) <= a2) {
                    z = false;
                } else {
                    p pVar = this.c;
                    pVar.b("FileManager", "File " + next.getName() + " has expired, removing...");
                    z = b(next);
                }
                if (z) {
                    this.b.K().a(g.e);
                } else {
                    j += next.length();
                }
            }
        }
        return j;
    }

    private List<File> d(Context context) {
        List<File> asList;
        File e = e(context);
        if (!e.isDirectory()) {
            return Collections.emptyList();
        }
        synchronized (this.d) {
            asList = Arrays.asList(e.listFiles());
        }
        return asList;
    }

    private File e(Context context) {
        return new File(context.getFilesDir(), CampaignEx.JSON_KEY_AD_AL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.ByteArrayOutputStream, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.FileInputStream, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r3, r8.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r2, r8.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003d */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:45:0x0095=Splitter:B:45:0x0095, B:29:0x005b=Splitter:B:29:0x005b, B:37:0x006d=Splitter:B:37:0x006d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.ByteArrayOutputStream a(java.io.File r9) {
        /*
            r8 = this;
            r0 = 0
            if (r9 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.applovin.impl.sdk.p r1 = r8.c
            java.lang.String r2 = "FileManager"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Reading resource from filesystem: "
            r3.append(r4)
            java.lang.String r4 = r9.getName()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.b(r2, r3)
            java.lang.Object r1 = r8.d
            monitor-enter(r1)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0093, IOException -> 0x006b, Throwable -> 0x0059, all -> 0x0056 }
            r2.<init>(r9)     // Catch:{ FileNotFoundException -> 0x0093, IOException -> 0x006b, Throwable -> 0x0059, all -> 0x0056 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
        L_0x0031:
            int r5 = r4.length     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
            r6 = 0
            int r5 = r2.read(r4, r6, r5)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
            if (r5 < 0) goto L_0x0049
            r3.write(r4, r6, r5)     // Catch:{ Exception -> 0x003d }
            goto L_0x0031
        L_0x003d:
            com.applovin.impl.sdk.j r4 = r8.b     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
            com.applovin.impl.sdk.utils.q.a(r3, r4)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, Throwable -> 0x0050 }
            com.applovin.impl.sdk.j r9 = r8.b     // Catch:{ all -> 0x00bb }
            com.applovin.impl.sdk.utils.q.a(r2, r9)     // Catch:{ all -> 0x00bb }
            monitor-exit(r1)     // Catch:{ all -> 0x00bb }
            return r0
        L_0x0049:
            com.applovin.impl.sdk.j r9 = r8.b     // Catch:{ all -> 0x00bb }
            com.applovin.impl.sdk.utils.q.a(r2, r9)     // Catch:{ all -> 0x00bb }
            monitor-exit(r1)     // Catch:{ all -> 0x00bb }
            return r3
        L_0x0050:
            r9 = move-exception
            goto L_0x005b
        L_0x0052:
            r3 = move-exception
            goto L_0x006d
        L_0x0054:
            r9 = move-exception
            goto L_0x0095
        L_0x0056:
            r9 = move-exception
            r2 = r0
            goto L_0x00b5
        L_0x0059:
            r9 = move-exception
            r2 = r0
        L_0x005b:
            com.applovin.impl.sdk.p r3 = r8.c     // Catch:{ all -> 0x00b4 }
            java.lang.String r4 = "FileManager"
            java.lang.String r5 = "Unknown failure to read file."
            r3.b(r4, r5, r9)     // Catch:{ all -> 0x00b4 }
            com.applovin.impl.sdk.j r9 = r8.b     // Catch:{ all -> 0x00bb }
            com.applovin.impl.sdk.utils.q.a(r2, r9)     // Catch:{ all -> 0x00bb }
            monitor-exit(r1)     // Catch:{ all -> 0x00bb }
            return r0
        L_0x006b:
            r3 = move-exception
            r2 = r0
        L_0x006d:
            com.applovin.impl.sdk.p r4 = r8.c     // Catch:{ all -> 0x00b4 }
            java.lang.String r5 = "FileManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b4 }
            r6.<init>()     // Catch:{ all -> 0x00b4 }
            java.lang.String r7 = "Failed to read file: "
            r6.append(r7)     // Catch:{ all -> 0x00b4 }
            java.lang.String r9 = r9.getName()     // Catch:{ all -> 0x00b4 }
            r6.append(r9)     // Catch:{ all -> 0x00b4 }
            r6.append(r3)     // Catch:{ all -> 0x00b4 }
            java.lang.String r9 = r6.toString()     // Catch:{ all -> 0x00b4 }
            r4.b(r5, r9)     // Catch:{ all -> 0x00b4 }
            com.applovin.impl.sdk.j r9 = r8.b     // Catch:{ all -> 0x00bb }
            com.applovin.impl.sdk.utils.q.a(r2, r9)     // Catch:{ all -> 0x00bb }
            monitor-exit(r1)     // Catch:{ all -> 0x00bb }
            return r0
        L_0x0093:
            r9 = move-exception
            r2 = r0
        L_0x0095:
            com.applovin.impl.sdk.p r3 = r8.c     // Catch:{ all -> 0x00b4 }
            java.lang.String r4 = "FileManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b4 }
            r5.<init>()     // Catch:{ all -> 0x00b4 }
            java.lang.String r6 = "File not found. "
            r5.append(r6)     // Catch:{ all -> 0x00b4 }
            r5.append(r9)     // Catch:{ all -> 0x00b4 }
            java.lang.String r9 = r5.toString()     // Catch:{ all -> 0x00b4 }
            r3.c(r4, r9)     // Catch:{ all -> 0x00b4 }
            com.applovin.impl.sdk.j r9 = r8.b     // Catch:{ all -> 0x00bb }
            com.applovin.impl.sdk.utils.q.a(r2, r9)     // Catch:{ all -> 0x00bb }
            monitor-exit(r1)     // Catch:{ all -> 0x00bb }
            return r0
        L_0x00b4:
            r9 = move-exception
        L_0x00b5:
            com.applovin.impl.sdk.j r0 = r8.b     // Catch:{ all -> 0x00bb }
            com.applovin.impl.sdk.utils.q.a(r2, r0)     // Catch:{ all -> 0x00bb }
            throw r9     // Catch:{ all -> 0x00bb }
        L_0x00bb:
            r9 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00bb }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.n.a(java.io.File):java.io.ByteArrayOutputStream");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v5, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v6, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v7, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v8, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v19, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.ByteArrayOutputStream, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void
     arg types: [java.io.InputStream, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.q.a(android.view.View, com.applovin.impl.sdk.j):android.app.Activity
      com.applovin.impl.sdk.utils.q.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.q.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.q.a(org.json.JSONObject, com.applovin.impl.sdk.j):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.q.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.j):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.q.a(java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.b.f<java.lang.String>, com.applovin.impl.sdk.j):java.lang.String
      com.applovin.impl.sdk.utils.q.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.q.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.j):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.q.a(java.net.HttpURLConnection, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.q.a(long, long):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.q.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.q.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.q.a(java.io.Closeable, com.applovin.impl.sdk.j):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:28|29|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r9, r7.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00cd, code lost:
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r1, r7.b);
        com.applovin.impl.sdk.utils.q.a((java.io.Closeable) r9, r7.b);
        com.applovin.impl.sdk.utils.q.a(r10, r7.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00dc, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x00c8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.ByteArrayOutputStream a(java.lang.String r8, java.util.List<java.lang.String> r9, boolean r10) {
        /*
            r7 = this;
            r0 = 0
            if (r10 == 0) goto L_0x0022
            boolean r9 = com.applovin.impl.sdk.utils.q.a(r8, r9)
            if (r9 != 0) goto L_0x0022
            com.applovin.impl.sdk.p r9 = r7.c
            java.lang.String r10 = "FileManager"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Domain is not whitelisted, skipping precache for url: "
            r1.append(r2)
            r1.append(r8)
            java.lang.String r8 = r1.toString()
            r9.b(r10, r8)
            return r0
        L_0x0022:
            com.applovin.impl.sdk.j r9 = r7.b
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r10 = com.applovin.impl.sdk.b.d.dy
            java.lang.Object r9 = r9.a(r10)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x004b
            java.lang.String r9 = "https://"
            boolean r9 = r8.contains(r9)
            if (r9 != 0) goto L_0x004b
            com.applovin.impl.sdk.p r9 = r7.c
            java.lang.String r10 = "FileManager"
            java.lang.String r1 = "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting..."
            r9.d(r10, r1)
            java.lang.String r9 = "http://"
            java.lang.String r10 = "https://"
            java.lang.String r8 = r8.replace(r9, r10)
        L_0x004b:
            com.applovin.impl.sdk.p r9 = r7.c
            java.lang.String r10 = "FileManager"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Loading "
            r1.append(r2)
            r1.append(r8)
            java.lang.String r2 = "..."
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.b(r10, r1)
            java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0126, all -> 0x0122 }
            r9.<init>()     // Catch:{ IOException -> 0x0126, all -> 0x0122 }
            java.net.URL r10 = new java.net.URL     // Catch:{ IOException -> 0x011f, all -> 0x011c }
            r10.<init>(r8)     // Catch:{ IOException -> 0x011f, all -> 0x011c }
            java.net.URLConnection r10 = r10.openConnection()     // Catch:{ IOException -> 0x011f, all -> 0x011c }
            java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ IOException -> 0x011f, all -> 0x011c }
            com.applovin.impl.sdk.j r1 = r7.b     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            com.applovin.impl.sdk.b.d<java.lang.Integer> r2 = com.applovin.impl.sdk.b.d.dw     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            int r1 = r1.intValue()     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r10.setConnectTimeout(r1)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            com.applovin.impl.sdk.j r1 = r7.b     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            com.applovin.impl.sdk.b.d<java.lang.Integer> r2 = com.applovin.impl.sdk.b.d.dx     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            int r1 = r1.intValue()     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r10.setReadTimeout(r1)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r1 = 1
            r10.setDefaultUseCaches(r1)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r10.setUseCaches(r1)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r2 = 0
            r10.setAllowUserInteraction(r2)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r10.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            int r1 = r10.getResponseCode()     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 < r3) goto L_0x0107
            r3 = 300(0x12c, float:4.2E-43)
            if (r1 < r3) goto L_0x00b5
            goto L_0x0107
        L_0x00b5:
            java.io.InputStream r1 = r10.getInputStream()     // Catch:{ IOException -> 0x0119, all -> 0x0117 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0105 }
        L_0x00bd:
            int r4 = r3.length     // Catch:{ IOException -> 0x0105 }
            int r4 = r1.read(r3, r2, r4)     // Catch:{ IOException -> 0x0105 }
            if (r4 < 0) goto L_0x00dd
            r9.write(r3, r2, r4)     // Catch:{ Exception -> 0x00c8 }
            goto L_0x00bd
        L_0x00c8:
            com.applovin.impl.sdk.j r2 = r7.b     // Catch:{ IOException -> 0x0105 }
            com.applovin.impl.sdk.utils.q.a(r9, r2)     // Catch:{ IOException -> 0x0105 }
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r1, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r9, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r10, r8)
            return r0
        L_0x00dd:
            com.applovin.impl.sdk.p r2 = r7.c     // Catch:{ IOException -> 0x0105 }
            java.lang.String r3 = "FileManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0105 }
            r4.<init>()     // Catch:{ IOException -> 0x0105 }
            java.lang.String r5 = "Loaded resource at "
            r4.append(r5)     // Catch:{ IOException -> 0x0105 }
            r4.append(r8)     // Catch:{ IOException -> 0x0105 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0105 }
            r2.b(r3, r4)     // Catch:{ IOException -> 0x0105 }
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r1, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r9, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r10, r8)
            return r9
        L_0x0105:
            r2 = move-exception
            goto L_0x012a
        L_0x0107:
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r0, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r9, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r10, r8)
            return r0
        L_0x0117:
            r8 = move-exception
            goto L_0x0154
        L_0x0119:
            r2 = move-exception
            r1 = r0
            goto L_0x012a
        L_0x011c:
            r8 = move-exception
            r10 = r0
            goto L_0x0154
        L_0x011f:
            r2 = move-exception
            r10 = r0
            goto L_0x0129
        L_0x0122:
            r8 = move-exception
            r9 = r0
            r10 = r9
            goto L_0x0154
        L_0x0126:
            r2 = move-exception
            r9 = r0
            r10 = r9
        L_0x0129:
            r1 = r10
        L_0x012a:
            com.applovin.impl.sdk.p r3 = r7.c     // Catch:{ all -> 0x0152 }
            java.lang.String r4 = "FileManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0152 }
            r5.<init>()     // Catch:{ all -> 0x0152 }
            java.lang.String r6 = "Error loading "
            r5.append(r6)     // Catch:{ all -> 0x0152 }
            r5.append(r8)     // Catch:{ all -> 0x0152 }
            java.lang.String r8 = r5.toString()     // Catch:{ all -> 0x0152 }
            r3.b(r4, r8, r2)     // Catch:{ all -> 0x0152 }
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r1, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r9, r8)
            com.applovin.impl.sdk.j r8 = r7.b
            com.applovin.impl.sdk.utils.q.a(r10, r8)
            return r0
        L_0x0152:
            r8 = move-exception
            r0 = r1
        L_0x0154:
            com.applovin.impl.sdk.j r1 = r7.b
            com.applovin.impl.sdk.utils.q.a(r0, r1)
            com.applovin.impl.sdk.j r0 = r7.b
            com.applovin.impl.sdk.utils.q.a(r9, r0)
            com.applovin.impl.sdk.j r9 = r7.b
            com.applovin.impl.sdk.utils.q.a(r10, r9)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.n.a(java.lang.String, java.util.List, boolean):java.io.ByteArrayOutputStream");
    }

    public File a(String str, Context context) {
        File file;
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            this.c.b("FileManager", "Nothing to look up, skipping...");
            return null;
        }
        p pVar = this.c;
        pVar.b("FileManager", "Looking up cached resource: " + str);
        if (str.contains(SettingsJsonConstants.APP_ICON_KEY)) {
            str = str.replace(Constants.URL_PATH_DELIMITER, EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR).replace(".", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        }
        synchronized (this.d) {
            File e = e(context);
            file = new File(e, str);
            try {
                e.mkdirs();
            } catch (Throwable th) {
                p pVar2 = this.c;
                pVar2.b("FileManager", "Unable to make cache directory at " + e, th);
                return null;
            }
        }
        return file;
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, e eVar) {
        return a(context, str, str2, list, z, false, eVar);
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, boolean z2, e eVar) {
        if (!com.applovin.impl.sdk.utils.n.b(str)) {
            this.c.b("FileManager", "Nothing to cache, skipping...");
            return null;
        }
        String lastPathSegment = Uri.parse(str).getLastPathSegment();
        if (com.applovin.impl.sdk.utils.n.b(lastPathSegment) && com.applovin.impl.sdk.utils.n.b(str2)) {
            lastPathSegment = str2 + lastPathSegment;
        }
        File a2 = a(lastPathSegment, context);
        if (!a(a2, str, list, z, eVar)) {
            return null;
        }
        this.c.b("FileManager", "Caching succeeded for file " + lastPathSegment);
        return z2 ? Uri.fromFile(a2).toString() : lastPathSegment;
    }

    public void a(Context context) {
        if (b() && this.b.c()) {
            this.c.b("FileManager", "Compacting cache...");
            synchronized (this.d) {
                a(c(context), context);
            }
        }
    }

    public boolean a(ByteArrayOutputStream byteArrayOutputStream, File file) {
        if (file == null) {
            return false;
        }
        p pVar = this.c;
        pVar.b("FileManager", "Caching " + file.getAbsolutePath() + "...");
        if (byteArrayOutputStream == null || byteArrayOutputStream.size() <= 0) {
            p pVar2 = this.c;
            pVar2.d("FileManager", "No data for " + file.getAbsolutePath());
            return false;
        } else if (!b(byteArrayOutputStream, file)) {
            p pVar3 = this.c;
            pVar3.e("FileManager", "Unable to cache " + file.getAbsolutePath());
            return false;
        } else {
            p pVar4 = this.c;
            pVar4.b("FileManager", "Caching completed for " + file);
            return true;
        }
    }

    public boolean a(File file, String str, List<String> list, e eVar) {
        return a(file, str, list, true, eVar);
    }

    public void b(Context context) {
        try {
            a(".nomedia", context);
            File file = new File(e(context), ".nomedia");
            if (!file.exists()) {
                p pVar = this.c;
                pVar.b("FileManager", "Creating .nomedia file at " + file.getAbsolutePath());
                if (!file.createNewFile()) {
                    this.c.e("FileManager", "Failed to create .nomedia file");
                }
            }
        } catch (IOException e) {
            this.c.b("FileManager", "Failed to create .nomedia file", e);
        }
    }
}
