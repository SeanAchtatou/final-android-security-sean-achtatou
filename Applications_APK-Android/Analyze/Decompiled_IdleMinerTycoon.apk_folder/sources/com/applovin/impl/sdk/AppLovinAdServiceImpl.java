package com.applovin.impl.sdk;

import android.graphics.PointF;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.adview.AdViewControllerImpl;
import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.d.l;
import com.applovin.impl.sdk.d.m;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdUpdateListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class AppLovinAdServiceImpl implements AppLovinAdService {
    public static String URI_LOAD_URL = "/adservice/load_url";
    public static String URI_NO_OP = "/adservice/no_op";
    public static String URI_TRACK_CLICK_IMMEDIATELY = "/adservice/track_click_now";
    /* access modifiers changed from: private */
    public final j a;
    private final p b;
    private final Handler c = new Handler(Looper.getMainLooper());
    private final Map<d, b> d;
    private final Object e = new Object();

    private class a implements AppLovinAdLoadListener {
        private final b b;

        private a(b bVar) {
            this.b = bVar;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            HashSet<AppLovinAdLoadListener> hashSet;
            d adZone = ((AppLovinAdBase) appLovinAd).getAdZone();
            if (!(appLovinAd instanceof g)) {
                AppLovinAdServiceImpl.this.a.S().adReceived(appLovinAd);
                appLovinAd = new g(adZone, AppLovinAdServiceImpl.this.a);
            }
            synchronized (this.b.a) {
                hashSet = new HashSet<>(this.b.c);
                this.b.c.clear();
                this.b.b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(appLovinAd, a2);
            }
        }

        public void failedToReceiveAd(int i) {
            HashSet<AppLovinAdLoadListener> hashSet;
            synchronized (this.b.a) {
                hashSet = new HashSet<>(this.b.c);
                this.b.c.clear();
                this.b.b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(i, a2);
            }
        }
    }

    private static class b {
        final Object a;
        boolean b;
        final Collection<AppLovinAdLoadListener> c;

        private b() {
            this.a = new Object();
            this.c = new HashSet();
        }

        public String toString() {
            return "AdLoadState{, isWaitingForAd=" + this.b + ", pendingAdListeners=" + this.c + '}';
        }
    }

    AppLovinAdServiceImpl(j jVar) {
        this.a = jVar;
        this.b = jVar.u();
        this.d = new HashMap(5);
        this.d.put(d.c(jVar), new b());
        this.d.put(d.d(jVar), new b());
        this.d.put(d.e(jVar), new b());
        this.d.put(d.f(jVar), new b());
        this.d.put(d.g(jVar), new b());
    }

    private b a(d dVar) {
        b bVar;
        synchronized (this.e) {
            bVar = this.d.get(dVar);
            if (bVar == null) {
                bVar = new b();
                this.d.put(dVar, bVar);
            }
        }
        return bVar;
    }

    private String a(String str, long j, int i, String str2, boolean z) {
        try {
            if (!n.b(str)) {
                return null;
            }
            if (i < 0 || i > 100) {
                i = 0;
            }
            return Uri.parse(str).buildUpon().appendQueryParameter("et_s", Long.toString(j)).appendQueryParameter(NativeAdImpl.QUERY_PARAM_VIDEO_PERCENT_VIEWED, Integer.toString(i)).appendQueryParameter("vid_ts", str2).appendQueryParameter("uvs", Boolean.toString(z)).build().toString();
        } catch (Throwable th) {
            p pVar = this.b;
            pVar.b("AppLovinAdService", "Unknown error parsing the video end url: " + str, th);
            return null;
        }
    }

    private String a(String str, long j, long j2, boolean z, int i) {
        if (!n.b(str)) {
            return null;
        }
        Uri.Builder appendQueryParameter = Uri.parse(str).buildUpon().appendQueryParameter("et_ms", Long.toString(j)).appendQueryParameter("vs_ms", Long.toString(j2));
        if (i != e.a) {
            appendQueryParameter.appendQueryParameter("musw_ch", Boolean.toString(z));
            appendQueryParameter.appendQueryParameter("musw_st", Boolean.toString(e.a(i)));
        }
        return appendQueryParameter.build().toString();
    }

    /* access modifiers changed from: private */
    public void a(final int i, final AppLovinAdLoadListener appLovinAdLoadListener) {
        this.c.post(new Runnable() {
            public void run() {
                try {
                    appLovinAdLoadListener.failedToReceiveAd(i);
                } catch (Throwable th) {
                    p.c("AppLovinAdService", "Unable to notify listener about ad load failure", th);
                }
            }
        });
    }

    private void a(Uri uri, f fVar, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl) {
        if (appLovinAdView != null) {
            if (q.a(appLovinAdView.getContext(), uri, this.a)) {
                j.c(adViewControllerImpl.getAdViewEventListener(), fVar, appLovinAdView);
            }
            adViewControllerImpl.dismissInterstitialIfRequired();
            return;
        }
        this.b.e("AppLovinAdService", "Unable to launch click - adView has been prematurely destroyed");
    }

    private void a(d dVar, a aVar) {
        AppLovinAd appLovinAd = (AppLovinAd) this.a.S().e(dVar);
        if (appLovinAd != null) {
            p pVar = this.b;
            pVar.b("AppLovinAdService", "Using pre-loaded ad: " + appLovinAd + " for " + dVar);
            aVar.adReceived(appLovinAd);
            if (dVar.i() || dVar.g() > 0) {
                this.a.S().i(dVar);
                return;
            }
            return;
        }
        a(new m(dVar, aVar, this.a), aVar);
    }

    private void a(d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        p pVar;
        String str;
        String str2;
        if (dVar == null) {
            throw new IllegalArgumentException("No zone specified");
        } else if (appLovinAdLoadListener != null) {
            p u = this.a.u();
            u.b("AppLovinAdService", "Loading next ad of zone {" + dVar + "}...");
            b a2 = a(dVar);
            synchronized (a2.a) {
                a2.c.add(appLovinAdLoadListener);
                if (!a2.b) {
                    this.b.b("AppLovinAdService", "Loading next ad...");
                    a2.b = true;
                    a aVar = new a(a2);
                    if (!dVar.h()) {
                        this.b.b("AppLovinAdService", "Task merge not necessary.");
                    } else if (this.a.S().a(dVar, aVar)) {
                        pVar = this.b;
                        str = "AppLovinAdService";
                        str2 = "Attaching load listener to initial preload task...";
                    } else {
                        this.b.b("AppLovinAdService", "Skipped attach of initial preload callback.");
                    }
                    a(dVar, aVar);
                } else {
                    pVar = this.b;
                    str = "AppLovinAdService";
                    str2 = "Already waiting on an ad load...";
                }
                pVar.b(str, str2);
            }
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    private void a(com.applovin.impl.sdk.c.a aVar) {
        if (n.b(aVar.a())) {
            this.a.M().a(com.applovin.impl.sdk.network.f.k().a(q.b(aVar.a())).b(n.b(aVar.b()) ? q.b(aVar.b()) : null).a(false).a());
            return;
        }
        this.b.d("AppLovinAdService", "Requested a postback dispatch for a null URL; nothing to do...");
    }

    private void a(com.applovin.impl.sdk.d.a aVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!this.a.d()) {
            p.i("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        this.a.a();
        this.a.J().a(aVar, r.a.MAIN);
    }

    /* access modifiers changed from: private */
    public void a(final AppLovinAd appLovinAd, final AppLovinAdLoadListener appLovinAdLoadListener) {
        this.c.post(new Runnable() {
            public void run() {
                try {
                    appLovinAdLoadListener.adReceived(appLovinAd);
                } catch (Throwable th) {
                    p.c("AppLovinAdService", "Unable to notify listener about a newly loaded ad", th);
                }
            }
        });
    }

    private void a(List<com.applovin.impl.sdk.c.a> list) {
        if (list != null && !list.isEmpty()) {
            for (com.applovin.impl.sdk.c.a a2 : list) {
                a(a2);
            }
        }
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener) {
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
    }

    public AppLovinAd dequeueAd(d dVar) {
        AppLovinAd appLovinAd = (AppLovinAd) this.a.S().d(dVar);
        p pVar = this.b;
        pVar.b("AppLovinAdService", "Dequeued ad: " + appLovinAd + " for zone: " + dVar + "...");
        return appLovinAd;
    }

    public String getBidToken() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        String a2 = this.a.N().a();
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        return a2;
    }

    public boolean hasPreloadedAd(AppLovinAdSize appLovinAdSize) {
        return this.a.S().g(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.a));
    }

    public boolean hasPreloadedAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            p.j("AppLovinAdService", "Unable to check if ad is preloaded - invalid zone id");
            return false;
        }
        return this.a.S().g(d.a(str, this.a));
    }

    public void loadNextAd(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.a), appLovinAdLoadListener);
    }

    public void loadNextAd(String str, AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        p pVar = this.b;
        pVar.b("AppLovinAdService", "Loading next ad of zone {" + str + "} with size " + appLovinAdSize);
        a(d.a(appLovinAdSize, AppLovinAdType.REGULAR, str, this.a), appLovinAdLoadListener);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r10v19, types: [com.applovin.impl.sdk.d.o] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadNextAdForAdToken(java.lang.String r10, com.applovin.sdk.AppLovinAdLoadListener r11) {
        /*
            r9 = this;
            if (r10 == 0) goto L_0x0007
            java.lang.String r10 = r10.trim()
            goto L_0x0008
        L_0x0007:
            r10 = 0
        L_0x0008:
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            r1 = -8
            if (r0 == 0) goto L_0x001a
            java.lang.String r10 = "AppLovinAdService"
            java.lang.String r0 = "Invalid ad token specified"
            com.applovin.impl.sdk.p.j(r10, r0)
            r9.a(r1, r11)
            return
        L_0x001a:
            com.applovin.impl.sdk.ad.c r0 = new com.applovin.impl.sdk.ad.c
            com.applovin.impl.sdk.j r2 = r9.a
            r0.<init>(r10, r2)
            com.applovin.impl.sdk.ad.c$a r10 = r0.b()
            com.applovin.impl.sdk.ad.c$a r2 = com.applovin.impl.sdk.ad.c.a.REGULAR
            if (r10 != r2) goto L_0x004d
            com.applovin.impl.sdk.p r10 = r9.b
            java.lang.String r1 = "AppLovinAdService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Loading next ad for token: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            r10.b(r1, r2)
            com.applovin.impl.sdk.d.o r10 = new com.applovin.impl.sdk.d.o
            com.applovin.impl.sdk.j r1 = r9.a
            r10.<init>(r0, r11, r1)
        L_0x0048:
            r9.a(r10, r11)
            goto L_0x00f7
        L_0x004d:
            com.applovin.impl.sdk.ad.c$a r10 = r0.b()
            com.applovin.impl.sdk.ad.c$a r2 = com.applovin.impl.sdk.ad.c.a.AD_RESPONSE_JSON
            if (r10 != r2) goto L_0x00de
            org.json.JSONObject r4 = r0.d()
            if (r4 == 0) goto L_0x00c5
            com.applovin.impl.sdk.j r10 = r9.a
            com.applovin.impl.sdk.utils.h.f(r4, r10)
            com.applovin.impl.sdk.j r10 = r9.a
            com.applovin.impl.sdk.utils.h.d(r4, r10)
            com.applovin.impl.sdk.j r10 = r9.a
            com.applovin.impl.sdk.utils.h.c(r4, r10)
            java.lang.String r10 = "ads"
            org.json.JSONArray r1 = new org.json.JSONArray
            r1.<init>()
            com.applovin.impl.sdk.j r2 = r9.a
            org.json.JSONArray r10 = com.applovin.impl.sdk.utils.i.b(r4, r10, r1, r2)
            int r10 = r10.length()
            if (r10 <= 0) goto L_0x00a7
            com.applovin.impl.sdk.p r10 = r9.b
            java.lang.String r1 = "AppLovinAdService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Rendering ad for token: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r10.b(r1, r0)
            com.applovin.impl.sdk.j r10 = r9.a
            com.applovin.impl.sdk.ad.d r5 = com.applovin.impl.sdk.utils.q.a(r4, r10)
            com.applovin.impl.sdk.d.s r10 = new com.applovin.impl.sdk.d.s
            com.applovin.impl.sdk.ad.b r6 = com.applovin.impl.sdk.ad.b.DECODED_AD_TOKEN_JSON
            com.applovin.impl.sdk.j r8 = r9.a
            r3 = r10
            r7 = r11
            r3.<init>(r4, r5, r6, r7, r8)
            goto L_0x0048
        L_0x00a7:
            com.applovin.impl.sdk.p r10 = r9.b
            java.lang.String r1 = "AppLovinAdService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "No ad returned from the server for token: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r10.e(r1, r0)
            r10 = 204(0xcc, float:2.86E-43)
            r11.failedToReceiveAd(r10)
            goto L_0x00f7
        L_0x00c5:
            com.applovin.impl.sdk.p r10 = r9.b
            java.lang.String r2 = "AppLovinAdService"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unable to retrieve ad response JSON from token: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r10.e(r2, r0)
            goto L_0x00f4
        L_0x00de:
            java.lang.String r10 = "AppLovinAdService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid ad token specified: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            com.applovin.impl.sdk.p.j(r10, r0)
        L_0x00f4:
            r11.failedToReceiveAd(r1)
        L_0x00f7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.AppLovinAdServiceImpl.loadNextAdForAdToken(java.lang.String, com.applovin.sdk.AppLovinAdLoadListener):void");
    }

    public void loadNextAdForZoneId(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!TextUtils.isEmpty(str)) {
            p pVar = this.b;
            pVar.b("AppLovinAdService", "Loading next ad of zone {" + str + "}");
            a(d.a(str, this.a), appLovinAdLoadListener);
            return;
        }
        throw new IllegalArgumentException("No zone id specified");
    }

    public void loadNextAdForZoneIds(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener) {
        List<String> a2 = e.a(list);
        if (a2 == null || a2.isEmpty()) {
            p.j("AppLovinAdService", "No zones were provided");
            a(-7, appLovinAdLoadListener);
            return;
        }
        p pVar = this.b;
        pVar.b("AppLovinAdService", "Loading next ad for zones: " + a2);
        a(new l(a2, appLovinAdLoadListener, this.a), appLovinAdLoadListener);
    }

    public void loadNextIncentivizedAd(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        p pVar = this.b;
        pVar.b("AppLovinAdService", "Loading next incentivized ad of zone {" + str + "}");
        a(d.c(str, this.a), appLovinAdLoadListener);
    }

    public void preloadAd(AppLovinAdSize appLovinAdSize) {
        this.a.a();
        this.a.S().i(d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.a));
    }

    public void preloadAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            p.j("AppLovinAdService", "Unable to preload ad for invalid zone identifier");
            return;
        }
        d a2 = d.a(str, this.a);
        this.a.S().h(a2);
        this.a.S().i(a2);
    }

    public void preloadAds(d dVar) {
        this.a.S().h(dVar);
        int g = dVar.g();
        if (g == 0 && this.a.S().b(dVar)) {
            g = 1;
        }
        this.a.S().b(dVar, g);
    }

    public void removeAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
    }

    public String toString() {
        return "AppLovinAdService{adLoadStates=" + this.d + '}';
    }

    public void trackAndLaunchClick(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl, Uri uri, PointF pointF) {
        if (appLovinAd == null) {
            this.b.e("AppLovinAdService", "Unable to track ad view click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking click on an ad...");
        f fVar = (f) appLovinAd;
        a(fVar.a(pointF));
        a(uri, fVar, appLovinAdView, adViewControllerImpl);
    }

    public void trackAndLaunchVideoClick(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        if (appLovinAd == null) {
            this.b.e("AppLovinAdService", "Unable to track video click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking VIDEO click on an ad...");
        a(((f) appLovinAd).b(pointF));
        q.a(appLovinAdView.getContext(), uri, this.a);
    }

    public void trackAppKilled(f fVar) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track app killed. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking app killed during ad...");
        List<com.applovin.impl.sdk.c.a> at = fVar.at();
        if (at == null || at.isEmpty()) {
            p pVar = this.b;
            pVar.d("AppLovinAdService", "Unable to track app killed during AD #" + fVar.getAdIdNumber() + ". Missing app killed tracking URL.");
            return;
        }
        for (com.applovin.impl.sdk.c.a next : at) {
            a(new com.applovin.impl.sdk.c.a(next.a(), next.b()));
        }
    }

    public void trackFullScreenAdClosed(f fVar, long j, long j2, boolean z, int i) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track ad closed. No ad specified.");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking ad closed...");
        List<com.applovin.impl.sdk.c.a> as = fVar.as();
        if (as == null || as.isEmpty()) {
            p pVar = this.b;
            pVar.d("AppLovinAdService", "Unable to track ad closed for AD #" + fVar.getAdIdNumber() + ". Missing ad close tracking URL." + fVar.getAdIdNumber());
            return;
        }
        for (com.applovin.impl.sdk.c.a next : as) {
            long j3 = j;
            long j4 = j2;
            boolean z2 = z;
            int i2 = i;
            String a2 = a(next.a(), j3, j4, z2, i2);
            String a3 = a(next.b(), j3, j4, z2, i2);
            if (n.b(a2)) {
                a(new com.applovin.impl.sdk.c.a(a2, a3));
            } else {
                p pVar2 = this.b;
                pVar2.e("AppLovinAdService", "Failed to parse url: " + next.a());
            }
        }
    }

    public void trackImpression(f fVar) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track impression click. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking impression on ad...");
        a(fVar.au());
    }

    public void trackVideoEnd(f fVar, long j, int i, boolean z) {
        if (fVar == null) {
            this.b.e("AppLovinAdService", "Unable to track video end. No ad specified");
            return;
        }
        this.b.b("AppLovinAdService", "Tracking video end on ad...");
        List<com.applovin.impl.sdk.c.a> ar = fVar.ar();
        if (ar == null || ar.isEmpty()) {
            p pVar = this.b;
            pVar.d("AppLovinAdService", "Unable to submit persistent postback for AD #" + fVar.getAdIdNumber() + ". Missing video end tracking URL.");
            return;
        }
        String l = Long.toString(System.currentTimeMillis());
        for (com.applovin.impl.sdk.c.a next : ar) {
            if (n.b(next.a())) {
                long j2 = j;
                int i2 = i;
                String str = l;
                boolean z2 = z;
                String a2 = a(next.a(), j2, i2, str, z2);
                String a3 = a(next.b(), j2, i2, str, z2);
                if (a2 != null) {
                    a(new com.applovin.impl.sdk.c.a(a2, a3));
                } else {
                    p pVar2 = this.b;
                    pVar2.e("AppLovinAdService", "Failed to parse url: " + next.a());
                }
            } else {
                this.b.d("AppLovinAdService", "Requested a postback dispatch for an empty video end URL; nothing to do...");
            }
        }
    }
}
