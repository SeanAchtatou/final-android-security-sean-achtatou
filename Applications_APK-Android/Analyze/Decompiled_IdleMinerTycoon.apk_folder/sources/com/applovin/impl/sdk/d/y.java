package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.n;
import org.json.JSONObject;

public class y extends z {
    private final f a;

    public y(f fVar, j jVar) {
        super("TaskReportAppLovinReward", jVar);
        this.a = fVar;
    }

    public i a() {
        return i.x;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        d("Failed to report reward for ad: " + this.a + " - error code: " + i);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.a.getAdZone().a(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "fire_percent", this.a.ai(), this.b);
        String clCode = this.a.getClCode();
        if (!n.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "clcode", clCode, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/cr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        a("Reported reward successfully for ad: " + this.a);
    }

    /* access modifiers changed from: protected */
    public c c() {
        return this.a.aJ();
    }

    /* access modifiers changed from: protected */
    public void d() {
        d("No reward result was found for ad: " + this.a);
    }
}
