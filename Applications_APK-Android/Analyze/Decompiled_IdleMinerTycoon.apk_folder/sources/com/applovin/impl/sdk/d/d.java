package com.applovin.impl.sdk.d;

import android.net.Uri;
import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.sdk.AppLovinAdLoadListener;

public class d extends c {
    private final a c;
    private boolean d;
    private boolean e;

    public d(a aVar, j jVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super("TaskCacheAppLovinAd", aVar, jVar, appLovinAdLoadListener);
        this.c = aVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        boolean b = this.c.b();
        boolean z = this.e;
        if (b || z) {
            a("Begin caching for streaming ad #" + this.c.getAdIdNumber() + "...");
            d();
            if (b) {
                if (this.d) {
                    i();
                }
                k();
                if (!this.d) {
                    i();
                }
                l();
            } else {
                i();
                k();
            }
        } else {
            a("Begin processing for non-streaming ad #" + this.c.getAdIdNumber() + "...");
            d();
            k();
            l();
            i();
        }
        long currentTimeMillis = System.currentTimeMillis() - this.c.getCreatedAtMillis();
        com.applovin.impl.sdk.c.d.a(this.c, this.b);
        com.applovin.impl.sdk.c.d.a(currentTimeMillis, this.c, this.b);
        a(this.c);
        b();
    }

    private void k() {
        a("Caching HTML resources...");
        this.c.a(a(this.c.a(), this.c.F(), this.c));
        this.c.a(true);
        a("Finish caching non-video resources for ad #" + this.c.getAdIdNumber());
        p u = this.b.u();
        String f = f();
        u.a(f, "Ad updated with cachedHTML = " + this.c.a());
    }

    private void l() {
        Uri e2;
        if (!c() && (e2 = e(this.c.e())) != null) {
            this.c.c();
            this.c.a(e2);
        }
    }

    public i a() {
        return i.i;
    }

    public /* bridge */ /* synthetic */ void a(com.applovin.impl.mediation.b.a aVar) {
        super.a(aVar);
    }

    public void a(boolean z) {
        this.d = z;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public void run() {
        super.run();
        AnonymousClass1 r0 = new Runnable() {
            public void run() {
                d.this.j();
            }
        };
        if (this.a.I()) {
            this.b.J().c().execute(r0);
        } else {
            r0.run();
        }
    }
}
