package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import java.util.Set;

public final class g {
    private static SharedPreferences a;
    private final SharedPreferences b;

    public g(j jVar) {
        this.b = jVar.C().getSharedPreferences("com.applovin.sdk.preferences." + jVar.s(), 0);
    }

    private static SharedPreferences a(Context context) {
        if (a == null) {
            a = context.getSharedPreferences("com.applovin.sdk.shared", 0);
        }
        return a;
    }

    public static <T> T a(String str, Object obj, Class cls, SharedPreferences sharedPreferences) {
        Object obj2;
        long j;
        int i;
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (sharedPreferences.contains(str)) {
                if (Boolean.class.equals(cls)) {
                    obj2 = Boolean.valueOf(obj != null ? sharedPreferences.getBoolean(str, ((Boolean) obj).booleanValue()) : sharedPreferences.getBoolean(str, false));
                } else if (Float.class.equals(cls)) {
                    obj2 = Float.valueOf(obj != null ? sharedPreferences.getFloat(str, ((Float) obj).floatValue()) : sharedPreferences.getFloat(str, 0.0f));
                } else if (Integer.class.equals(cls)) {
                    if (obj != null) {
                        i = sharedPreferences.getInt(str, obj.getClass().equals(Long.class) ? ((Long) obj).intValue() : ((Integer) obj).intValue());
                    } else {
                        i = sharedPreferences.getInt(str, 0);
                    }
                    obj2 = Integer.valueOf(i);
                } else if (Long.class.equals(cls)) {
                    if (obj != null) {
                        j = sharedPreferences.getLong(str, obj.getClass().equals(Integer.class) ? ((Integer) obj).longValue() : ((Long) obj).longValue());
                    } else {
                        j = sharedPreferences.getLong(str, 0);
                    }
                    obj2 = Long.valueOf(j);
                } else {
                    obj2 = String.class.equals(cls) ? sharedPreferences.getString(str, (String) obj) : Set.class.isAssignableFrom(cls) ? sharedPreferences.getStringSet(str, (Set) obj) : obj;
                }
                if (obj2 != null) {
                    return cls.cast(obj2);
                }
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return obj;
            }
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return obj;
        } catch (Throwable th) {
            p.c("SharedPreferencesManager", "Error getting value for key: " + str, th);
            return obj;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    public static <T> void a(f fVar, Context context) {
        a(context).edit().remove(fVar.a()).apply();
    }

    public static <T> void a(f fVar, Object obj, Context context) {
        a(fVar.a(), obj, a(context), (SharedPreferences.Editor) null);
    }

    private static <T> void a(String str, Object obj, SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
        boolean z = false;
        boolean z2 = editor != null;
        if (!z2) {
            editor = sharedPreferences.edit();
        }
        if (obj instanceof Boolean) {
            editor.putBoolean(str, ((Boolean) obj).booleanValue());
        } else if (obj instanceof Float) {
            editor.putFloat(str, ((Float) obj).floatValue());
        } else if (obj instanceof Integer) {
            editor.putInt(str, ((Integer) obj).intValue());
        } else if (obj instanceof Long) {
            editor.putLong(str, ((Long) obj).longValue());
        } else if (obj instanceof String) {
            editor.putString(str, (String) obj);
        } else if (obj instanceof Set) {
            editor.putStringSet(str, (Set) obj);
        } else {
            p.j("SharedPreferencesManager", "Unable to put default value of invalid type: " + obj);
            if (z && !z2) {
                editor.apply();
                return;
            }
        }
        z = true;
        if (z) {
        }
    }

    public static <T> T b(f fVar, Object obj, Context context) {
        return a(fVar.a(), obj, fVar.b(), a(context));
    }

    public void a(SharedPreferences sharedPreferences) {
        sharedPreferences.edit().clear().apply();
    }

    public <T> void a(f fVar) {
        this.b.edit().remove(fVar.a()).apply();
    }

    public <T> void a(f fVar, Object obj) {
        a(fVar, obj, this.b);
    }

    public <T> void a(f fVar, Object obj, SharedPreferences sharedPreferences) {
        a(fVar.a(), obj, sharedPreferences);
    }

    public <T> void a(String str, Object obj, SharedPreferences.Editor editor) {
        a(str, obj, (SharedPreferences) null, editor);
    }

    public <T> void a(String str, Object obj, SharedPreferences sharedPreferences) {
        a(str, obj, sharedPreferences, (SharedPreferences.Editor) null);
    }

    public <T> T b(f<T> fVar, T t) {
        return b(fVar, t, this.b);
    }

    public <T> T b(f fVar, Object obj, SharedPreferences sharedPreferences) {
        return a(fVar.a(), obj, fVar.b(), sharedPreferences);
    }
}
