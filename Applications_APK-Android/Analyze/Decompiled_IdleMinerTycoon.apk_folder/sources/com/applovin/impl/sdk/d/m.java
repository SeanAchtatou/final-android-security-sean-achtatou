package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.helpshift.analytics.AnalyticsEventKey;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class m extends a {
    private final d a;
    private final AppLovinAdLoadListener c;
    private boolean d;

    public m(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, j jVar) {
        this(dVar, appLovinAdLoadListener, "TaskFetchNextAd", jVar);
    }

    m(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, String str, j jVar) {
        super(str, jVar);
        this.d = false;
        this.a = dVar;
        this.c = appLovinAdLoadListener;
    }

    private void a(h hVar) {
        long b = hVar.b(g.c);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.b.a(com.applovin.impl.sdk.b.d.dM)).intValue())) {
            hVar.b(g.c, currentTimeMillis);
            hVar.c(g.d);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        boolean z = i != 204;
        p u = e().u();
        String f = f();
        Boolean valueOf = Boolean.valueOf(z);
        u.a(f, valueOf, "Unable to fetch " + this.a + " ad: server returned " + i);
        if (i == -800) {
            this.b.K().a(g.h);
        }
        try {
            a(i);
        } catch (Throwable th) {
            p.c(f(), "Unable process a failure to recieve an ad", th);
        }
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.h.d(jSONObject, this.b);
        com.applovin.impl.sdk.utils.h.c(jSONObject, this.b);
        com.applovin.impl.sdk.utils.h.e(jSONObject, this.b);
        a a2 = a(jSONObject);
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.eV)).booleanValue()) {
            this.b.J().a(a2);
        } else {
            this.b.J().a(a2, r.a.MAIN);
        }
    }

    public i a() {
        return i.n;
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        return new s(jSONObject, this.a, c(), this.c, this.b);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (this.c == null) {
            return;
        }
        if (this.c instanceof com.applovin.impl.sdk.m) {
            ((com.applovin.impl.sdk.m) this.c).a(this.a, i);
        } else {
            this.c.failedToReceiveAd(i);
        }
    }

    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(4);
        hashMap.put("zone_id", n.e(this.a.a()));
        if (this.a.b() != null) {
            hashMap.put("size", this.a.b().getLabel());
        }
        if (this.a.c() != null) {
            hashMap.put("require", this.a.c().getLabel());
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.ad)).booleanValue()) {
            hashMap.put(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT, String.valueOf(this.b.aa().a(this.a.a())));
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return this.a.i() ? b.APPLOVIN_PRIMARY_ZONE : b.APPLOVIN_CUSTOM_ZONE;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return com.applovin.impl.sdk.utils.h.g(this.b);
    }

    /* access modifiers changed from: protected */
    public String i() {
        return com.applovin.impl.sdk.utils.h.h(this.b);
    }

    public void run() {
        StringBuilder sb;
        String str;
        if (this.d) {
            sb = new StringBuilder();
            str = "Preloading next ad of zone: ";
        } else {
            sb = new StringBuilder();
            str = "Fetching next ad of zone: ";
        }
        sb.append(str);
        sb.append(this.a);
        a(sb.toString());
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.d.eh)).booleanValue() && q.d()) {
            a("User is connected to a VPN");
        }
        h K = this.b.K();
        K.a(g.a);
        if (K.b(g.c) == 0) {
            K.b(g.c, System.currentTimeMillis());
        }
        try {
            Map<String, String> a2 = this.b.N().a(b(), this.d, false);
            a(K);
            AnonymousClass1 r2 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(d()).a(a2).c(i()).b(HttpRequest.METHOD_GET).a((Object) new JSONObject()).a(((Integer) this.b.a(com.applovin.impl.sdk.b.d.dB)).intValue()).b(((Integer) this.b.a(com.applovin.impl.sdk.b.d.dA)).intValue()).b(true).a(), this.b) {
                public void a(int i) {
                    m.this.b(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_latency_millis", this.d.a(), this.b);
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_response_size", this.d.b(), this.b);
                        m.this.b(jSONObject);
                        return;
                    }
                    m.this.b(i);
                }
            };
            r2.a(com.applovin.impl.sdk.b.d.aL);
            r2.b(com.applovin.impl.sdk.b.d.aM);
            this.b.J().a(r2);
        } catch (Throwable th) {
            a("Unable to fetch ad " + this.a, th);
            b(0);
            this.b.L().a(a());
        }
    }
}
