package com.applovin.impl.sdk.d;

import android.app.Activity;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.e;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.TimeUnit;

public class q extends a {
    /* access modifiers changed from: private */
    public final j a;

    public q(j jVar) {
        super("TaskInitializeSdk", jVar);
        this.a = jVar;
    }

    private void a(d<Boolean> dVar) {
        if (((Boolean) this.a.a(dVar)).booleanValue()) {
            this.a.S().f(com.applovin.impl.sdk.ad.d.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, this.a));
        }
    }

    private void b() {
        if (!this.a.w().a()) {
            Activity af = this.a.af();
            if (af != null) {
                this.a.w().a(af);
            } else {
                this.a.J().a(new ac(this.a, true, new Runnable() {
                    public void run() {
                        q.this.a.w().a(q.this.a.Z().a());
                    }
                }), r.a.MAIN, TimeUnit.SECONDS.toMillis(1));
            }
        }
    }

    private void c() {
        this.a.J().a(new b(this.a), r.a.MAIN);
    }

    private void d() {
        this.a.S().a();
        this.a.T().a();
    }

    private void i() {
        j();
        k();
        l();
    }

    private void j() {
        LinkedHashSet<com.applovin.impl.sdk.ad.d> a2 = this.a.V().a();
        if (!a2.isEmpty()) {
            a("Scheduling preload(s) for " + a2.size() + " zone(s)");
            Iterator<com.applovin.impl.sdk.ad.d> it = a2.iterator();
            while (it.hasNext()) {
                com.applovin.impl.sdk.ad.d next = it.next();
                if (next.d()) {
                    this.a.o().preloadAds(next);
                } else {
                    this.a.n().preloadAds(next);
                }
            }
        }
    }

    private void k() {
        d<Boolean> dVar = d.be;
        String str = (String) this.a.a(d.bd);
        boolean z = false;
        if (str.length() > 0) {
            for (String fromString : e.a(str)) {
                AppLovinAdSize fromString2 = AppLovinAdSize.fromString(fromString);
                if (fromString2 != null) {
                    this.a.S().f(com.applovin.impl.sdk.ad.d.a(fromString2, AppLovinAdType.REGULAR, this.a));
                    if (AppLovinAdSize.INTERSTITIAL.getLabel().equals(fromString2.getLabel())) {
                        a(dVar);
                        z = true;
                    }
                }
            }
        }
        if (!z) {
            a(dVar);
        }
    }

    private void l() {
        if (((Boolean) this.a.a(d.bf)).booleanValue()) {
            this.a.T().f(com.applovin.impl.sdk.ad.d.h(this.a));
        }
    }

    public i a() {
        return i.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0145, code lost:
        if (r6.a.d() == false) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0147, code lost:
        r3 = com.facebook.internal.AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x014a, code lost:
        r3 = com.ironsource.sdk.constants.Constants.ParametersKeys.FAILED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x014c, code lost:
        r2.append(r3);
        r2.append(" in ");
        r2.append(java.lang.System.currentTimeMillis() - r0);
        r2.append(im.getsocial.sdk.consts.LanguageCodes.MALAY);
        a(r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0168, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00e9, code lost:
        if (r6.a.d() != false) goto L_0x0147;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Initializing AppLovin SDK "
            r2.append(r3)
            java.lang.String r3 = com.applovin.sdk.AppLovinSdk.VERSION
            r2.append(r3)
            java.lang.String r3 = "..."
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r6.a(r2)
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.c.h r2 = r2.K()     // Catch:{ Throwable -> 0x00ef }
            r2.d()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.c.h r2 = r2.K()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.c.g r3 = com.applovin.impl.sdk.c.g.b     // Catch:{ Throwable -> 0x00ef }
            r2.c(r3)     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.n r2 = r2.U()     // Catch:{ Throwable -> 0x00ef }
            android.content.Context r3 = r6.g()     // Catch:{ Throwable -> 0x00ef }
            r2.a(r3)     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.n r2 = r2.U()     // Catch:{ Throwable -> 0x00ef }
            android.content.Context r3 = r6.g()     // Catch:{ Throwable -> 0x00ef }
            r2.b(r3)     // Catch:{ Throwable -> 0x00ef }
            r6.d()     // Catch:{ Throwable -> 0x00ef }
            r6.i()     // Catch:{ Throwable -> 0x00ef }
            r6.b()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.c.c r2 = r2.W()     // Catch:{ Throwable -> 0x00ef }
            r2.a()     // Catch:{ Throwable -> 0x00ef }
            r6.c()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.k r2 = r2.N()     // Catch:{ Throwable -> 0x00ef }
            r2.e()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.utils.m r2 = r2.ac()     // Catch:{ Throwable -> 0x00ef }
            r2.a()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            r3 = 1
            r2.a(r3)     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.network.e r2 = r2.M()     // Catch:{ Throwable -> 0x00ef }
            r2.a()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.sdk.AppLovinEventService r2 = r2.p()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.EventServiceImpl r2 = (com.applovin.impl.sdk.EventServiceImpl) r2     // Catch:{ Throwable -> 0x00ef }
            r2.maybeTrackAppOpenEvent()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.mediation.j r2 = r2.A()     // Catch:{ Throwable -> 0x00ef }
            r2.a()     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.mediation.a.a r2 = r2.y()     // Catch:{ Throwable -> 0x00ef }
            boolean r2 = r2.a()     // Catch:{ Throwable -> 0x00ef }
            if (r2 == 0) goto L_0x00ac
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ Throwable -> 0x00ef }
            com.applovin.impl.mediation.a.a r2 = r2.y()     // Catch:{ Throwable -> 0x00ef }
            r2.b()     // Catch:{ Throwable -> 0x00ef }
        L_0x00ac:
            com.applovin.impl.sdk.j r2 = r6.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r3 = com.applovin.impl.sdk.b.d.au
            java.lang.Object r2 = r2.a(r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x00cf
            com.applovin.impl.sdk.j r2 = r6.a
            com.applovin.impl.sdk.b.d<java.lang.Long> r3 = com.applovin.impl.sdk.b.d.av
            java.lang.Object r2 = r2.a(r3)
            java.lang.Long r2 = (java.lang.Long) r2
            long r2 = r2.longValue()
            com.applovin.impl.sdk.j r4 = r6.a
            r4.a(r2)
        L_0x00cf:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "AppLovin SDK "
            r2.append(r3)
            java.lang.String r3 = com.applovin.sdk.AppLovinSdk.VERSION
            r2.append(r3)
            java.lang.String r3 = " initialization "
            r2.append(r3)
            com.applovin.impl.sdk.j r3 = r6.a
            boolean r3 = r3.d()
            if (r3 == 0) goto L_0x014a
            goto L_0x0147
        L_0x00ec:
            r2 = move-exception
            goto L_0x0169
        L_0x00ef:
            r2 = move-exception
            java.lang.String r3 = "Unable to initialize SDK."
            r6.a(r3, r2)     // Catch:{ all -> 0x00ec }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ all -> 0x00ec }
            r3 = 0
            r2.a(r3)     // Catch:{ all -> 0x00ec }
            com.applovin.impl.sdk.j r2 = r6.a     // Catch:{ all -> 0x00ec }
            com.applovin.impl.sdk.c.j r2 = r2.L()     // Catch:{ all -> 0x00ec }
            com.applovin.impl.sdk.c.i r3 = r6.a()     // Catch:{ all -> 0x00ec }
            r2.a(r3)     // Catch:{ all -> 0x00ec }
            com.applovin.impl.sdk.j r2 = r6.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r3 = com.applovin.impl.sdk.b.d.au
            java.lang.Object r2 = r2.a(r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x012b
            com.applovin.impl.sdk.j r2 = r6.a
            com.applovin.impl.sdk.b.d<java.lang.Long> r3 = com.applovin.impl.sdk.b.d.av
            java.lang.Object r2 = r2.a(r3)
            java.lang.Long r2 = (java.lang.Long) r2
            long r2 = r2.longValue()
            com.applovin.impl.sdk.j r4 = r6.a
            r4.a(r2)
        L_0x012b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "AppLovin SDK "
            r2.append(r3)
            java.lang.String r3 = com.applovin.sdk.AppLovinSdk.VERSION
            r2.append(r3)
            java.lang.String r3 = " initialization "
            r2.append(r3)
            com.applovin.impl.sdk.j r3 = r6.a
            boolean r3 = r3.d()
            if (r3 == 0) goto L_0x014a
        L_0x0147:
            java.lang.String r3 = "succeeded"
            goto L_0x014c
        L_0x014a:
            java.lang.String r3 = "failed"
        L_0x014c:
            r2.append(r3)
            java.lang.String r3 = " in "
            r2.append(r3)
            long r3 = java.lang.System.currentTimeMillis()
            long r3 = r3 - r0
            r2.append(r3)
            java.lang.String r0 = "ms"
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r6.a(r0)
            return
        L_0x0169:
            com.applovin.impl.sdk.j r3 = r6.a
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r4 = com.applovin.impl.sdk.b.d.au
            java.lang.Object r3 = r3.a(r4)
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x018c
            com.applovin.impl.sdk.j r3 = r6.a
            com.applovin.impl.sdk.b.d<java.lang.Long> r4 = com.applovin.impl.sdk.b.d.av
            java.lang.Object r3 = r3.a(r4)
            java.lang.Long r3 = (java.lang.Long) r3
            long r3 = r3.longValue()
            com.applovin.impl.sdk.j r5 = r6.a
            r5.a(r3)
        L_0x018c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "AppLovin SDK "
            r3.append(r4)
            java.lang.String r4 = com.applovin.sdk.AppLovinSdk.VERSION
            r3.append(r4)
            java.lang.String r4 = " initialization "
            r3.append(r4)
            com.applovin.impl.sdk.j r4 = r6.a
            boolean r4 = r4.d()
            if (r4 == 0) goto L_0x01ab
            java.lang.String r4 = "succeeded"
            goto L_0x01ad
        L_0x01ab:
            java.lang.String r4 = "failed"
        L_0x01ad:
            r3.append(r4)
            java.lang.String r4 = " in "
            r3.append(r4)
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 - r0
            r3.append(r4)
            java.lang.String r0 = "ms"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r6.a(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.d.q.run():void");
    }
}
