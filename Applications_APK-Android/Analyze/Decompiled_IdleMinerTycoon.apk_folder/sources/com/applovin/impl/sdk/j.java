package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.mediation.g;
import com.applovin.impl.mediation.h;
import com.applovin.impl.mediation.k;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.c;
import com.applovin.impl.sdk.c.f;
import com.applovin.impl.sdk.d.ac;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.network.PostbackServiceImpl;
import com.applovin.impl.sdk.network.c;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class j {
    protected static Context a;
    private c A;
    private u B;
    private a C;
    private o D;
    private t E;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.network.c F;
    private f G;
    private m H;
    private e I;
    private PostbackServiceImpl J;
    private e K;
    private h L;
    private g M;
    private MediationServiceImpl N;
    private k O;
    private a P;
    private com.applovin.impl.mediation.j Q;
    /* access modifiers changed from: private */
    public final Object R = new Object();
    private final AtomicBoolean S = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public boolean T = false;
    private boolean U = false;
    private boolean V = false;
    private boolean W = false;
    private boolean X = false;
    private String Y = "";
    private AppLovinSdk.SdkInitializationListener Z;
    private AppLovinSdk.SdkInitializationListener aa;
    /* access modifiers changed from: private */
    public AppLovinSdkConfiguration ab;
    protected com.applovin.impl.sdk.b.e b;
    private String c;
    private WeakReference<Activity> d;
    private long e;
    private AppLovinSdkSettings f;
    private AppLovinAdServiceImpl g;
    private NativeAdServiceImpl h;
    private EventServiceImpl i;
    private UserServiceImpl j;
    private VariableServiceImpl k;
    private AppLovinSdk l;
    /* access modifiers changed from: private */
    public p m;
    /* access modifiers changed from: private */
    public r n;
    private com.applovin.impl.sdk.network.a o;
    private com.applovin.impl.sdk.c.h p;
    private com.applovin.impl.sdk.c.j q;
    private k r;
    private com.applovin.impl.sdk.b.g s;
    private f t;
    private i u;
    private p v;
    private c w;
    private q x;
    private n y;
    private com.applovin.impl.sdk.ad.e z;

    public static Context D() {
        return a;
    }

    private void ag() {
        this.F.a(new c.a() {
            public void a() {
                j.this.m.c("AppLovinSdk", "Connected to internet - re-initializing SDK");
                synchronized (j.this.R) {
                    if (!j.this.T) {
                        j.this.b();
                    }
                }
                j.this.F.b(this);
            }

            public void b() {
            }
        });
    }

    public com.applovin.impl.mediation.j A() {
        return this.Q;
    }

    public com.applovin.impl.sdk.b.e B() {
        return this.b;
    }

    public Context C() {
        return a;
    }

    public Activity E() {
        if (this.d != null) {
            return this.d.get();
        }
        return null;
    }

    public long F() {
        return this.e;
    }

    public boolean G() {
        return this.W;
    }

    public boolean H() {
        return this.X;
    }

    public com.applovin.impl.sdk.network.a I() {
        return this.o;
    }

    public r J() {
        return this.n;
    }

    public com.applovin.impl.sdk.c.h K() {
        return this.p;
    }

    public com.applovin.impl.sdk.c.j L() {
        return this.q;
    }

    public e M() {
        return this.K;
    }

    public k N() {
        return this.r;
    }

    public f O() {
        return this.t;
    }

    public i P() {
        return this.u;
    }

    public PostbackServiceImpl Q() {
        return this.J;
    }

    public AppLovinSdk R() {
        return this.l;
    }

    public c S() {
        return this.w;
    }

    public q T() {
        return this.x;
    }

    public n U() {
        return this.y;
    }

    public com.applovin.impl.sdk.ad.e V() {
        return this.z;
    }

    public com.applovin.impl.sdk.c.c W() {
        return this.A;
    }

    public u X() {
        return this.B;
    }

    public o Y() {
        return this.D;
    }

    public a Z() {
        return this.C;
    }

    public <ST> d<ST> a(String str, d<ST> dVar) {
        return this.b.a(str, dVar);
    }

    public <T> T a(d dVar) {
        return this.b.a(dVar);
    }

    public <T> T a(com.applovin.impl.sdk.b.f fVar) {
        return b(fVar, null);
    }

    public <T> T a(String str, T t2, Class cls, SharedPreferences sharedPreferences) {
        com.applovin.impl.sdk.b.g gVar = this.s;
        return com.applovin.impl.sdk.b.g.a(str, t2, cls, sharedPreferences);
    }

    public void a() {
        synchronized (this.R) {
            if (!this.T && !this.U) {
                b();
            }
        }
    }

    public void a(long j2) {
        this.u.a(j2);
    }

    public void a(SharedPreferences sharedPreferences) {
        this.s.a(sharedPreferences);
    }

    public void a(com.applovin.impl.mediation.b.e eVar) {
        if (!this.n.a()) {
            List<String> b2 = b(com.applovin.impl.sdk.b.c.a);
            if (b2.size() > 0 && this.M.b().containsAll(b2)) {
                this.m.b("AppLovinSdk", "All required adapters initialized");
                this.n.e();
                e();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.g.a(com.applovin.impl.sdk.b.f, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.f<T>, T]
     candidates:
      com.applovin.impl.sdk.b.g.a(com.applovin.impl.sdk.b.f, android.content.Context):void
      com.applovin.impl.sdk.b.g.a(com.applovin.impl.sdk.b.f, java.lang.Object):void */
    public <T> void a(com.applovin.impl.sdk.b.f<T> fVar, T t2) {
        this.s.a((com.applovin.impl.sdk.b.f) fVar, (Object) t2);
    }

    public <T> void a(com.applovin.impl.sdk.b.f fVar, Object obj, SharedPreferences sharedPreferences) {
        this.s.a(fVar, obj, sharedPreferences);
    }

    public void a(AppLovinSdk.SdkInitializationListener sdkInitializationListener) {
        if (!d()) {
            this.Z = sdkInitializationListener;
        } else if (sdkInitializationListener != null) {
            sdkInitializationListener.onSdkInitialized(this.ab);
        }
    }

    public void a(AppLovinSdk appLovinSdk) {
        this.l = appLovinSdk;
    }

    public void a(String str) {
        p.g("AppLovinSdk", "Setting plugin version: " + str);
        this.b.a(d.ea, str);
        this.b.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.g.a(com.applovin.impl.sdk.b.f, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.f<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.b.g.a(com.applovin.impl.sdk.b.f, android.content.Context):void
      com.applovin.impl.sdk.b.g.a(com.applovin.impl.sdk.b.f, java.lang.Object):void */
    public void a(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        com.applovin.impl.sdk.b.g gVar;
        com.applovin.impl.sdk.b.f<String> fVar;
        String bool;
        this.c = str;
        this.e = System.currentTimeMillis();
        this.f = appLovinSdkSettings;
        this.ab = new SdkConfigurationImpl(this);
        a = context.getApplicationContext();
        if (context instanceof Activity) {
            this.d = new WeakReference<>((Activity) context);
        }
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            this.m = new p(this);
            this.s = new com.applovin.impl.sdk.b.g(this);
            this.b = new com.applovin.impl.sdk.b.e(this);
            this.b.c();
            this.b.a();
            this.t = new f(this);
            this.t.b();
            this.y = new n(this);
            this.w = new c(this);
            this.x = new q(this);
            this.z = new com.applovin.impl.sdk.ad.e(this);
            this.i = new EventServiceImpl(this);
            this.j = new UserServiceImpl(this);
            this.k = new VariableServiceImpl(this);
            this.A = new com.applovin.impl.sdk.c.c(this);
            this.n = new r(this);
            this.o = new com.applovin.impl.sdk.network.a(this);
            this.p = new com.applovin.impl.sdk.c.h(this);
            this.q = new com.applovin.impl.sdk.c.j(this);
            this.r = new k(this);
            this.C = new a(context);
            this.g = new AppLovinAdServiceImpl(this);
            this.h = new NativeAdServiceImpl(this);
            this.B = new u(this);
            this.D = new o(this);
            this.J = new PostbackServiceImpl(this);
            this.K = new e(this);
            this.L = new h(this);
            this.M = new g(this);
            this.N = new MediationServiceImpl(this);
            this.P = new a(this);
            this.O = new k();
            this.Q = new com.applovin.impl.mediation.j(this);
            this.u = new i(this);
            this.v = new p(this);
            this.E = new t(this);
            this.H = new m(this);
            this.I = new e(this);
            this.G = new f(this);
            if (((Boolean) this.b.a(d.dG)).booleanValue()) {
                this.F = new com.applovin.impl.sdk.network.c(context);
            }
            if (TextUtils.isEmpty(str)) {
                this.V = true;
                p.j("AppLovinSdk", "Unable to find AppLovin SDK key. Please add  meta-data android:name=\"applovin.sdk.key\" android:value=\"YOUR_SDK_KEY_HERE\" into AndroidManifest.xml.");
                StringWriter stringWriter = new StringWriter();
                new Throwable("").printStackTrace(new PrintWriter(stringWriter));
                String stringWriter2 = stringWriter.toString();
                p.j("AppLovinSdk", "Called with an invalid SDK key from: " + stringWriter2);
            }
            if (!t()) {
                if (((Boolean) this.b.a(d.ae)).booleanValue()) {
                    appLovinSdkSettings.setVerboseLogging(q.a(context));
                    B().a(appLovinSdkSettings);
                    B().b();
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                if (TextUtils.isEmpty((String) this.s.b(com.applovin.impl.sdk.b.f.a, (Object) null, defaultSharedPreferences))) {
                    this.W = true;
                    gVar = this.s;
                    fVar = com.applovin.impl.sdk.b.f.a;
                    bool = Boolean.toString(true);
                } else {
                    gVar = this.s;
                    fVar = com.applovin.impl.sdk.b.f.a;
                    bool = Boolean.toString(false);
                }
                gVar.a(fVar, bool, defaultSharedPreferences);
                if (((Boolean) this.s.b(com.applovin.impl.sdk.b.f.b, false)).booleanValue()) {
                    this.m.b("AppLovinSdk", "Initializing SDK for non-maiden launch");
                    this.X = true;
                } else {
                    this.m.b("AppLovinSdk", "Initializing SDK for maiden launch");
                    this.s.a((com.applovin.impl.sdk.b.f) com.applovin.impl.sdk.b.f.b, (Object) true);
                }
                q.a(com.applovin.impl.sdk.b.f.g, 100, this);
                boolean a2 = com.applovin.impl.sdk.utils.h.a(C());
                if (!((Boolean) this.b.a(d.dH)).booleanValue() || a2) {
                    b();
                }
                if (((Boolean) this.b.a(d.dG)).booleanValue() && !a2) {
                    this.m.c("AppLovinSdk", "SDK initialized with no internet connection - listening for connection");
                    ag();
                }
            } else {
                a(false);
            }
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    public <T> void a(String str, Object obj, SharedPreferences.Editor editor) {
        this.s.a(str, obj, editor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.d.r.a(com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.d.r$a, long, boolean):void
     arg types: [com.applovin.impl.sdk.d.ac, com.applovin.impl.sdk.d.r$a, long, int]
     candidates:
      com.applovin.impl.sdk.d.r.a(java.lang.Runnable, long, java.util.concurrent.ScheduledExecutorService, boolean):void
      com.applovin.impl.sdk.d.r.a(com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.d.r$a, long, boolean):void */
    public void a(boolean z2) {
        synchronized (this.R) {
            this.T = false;
            this.U = z2;
        }
        List<String> b2 = b(com.applovin.impl.sdk.b.c.a);
        if (b2.isEmpty()) {
            this.n.e();
            e();
            return;
        }
        long longValue = ((Long) a(com.applovin.impl.sdk.b.c.b)).longValue();
        ac acVar = new ac(this, true, new Runnable() {
            public void run() {
                if (!j.this.n.a()) {
                    j.this.m.b("AppLovinSdk", "Timing out adapters init...");
                    j.this.n.e();
                    j.this.e();
                }
            }
        });
        p pVar = this.m;
        pVar.b("AppLovinSdk", "Waiting for required adapters to init: " + b2 + " - timing out in " + longValue + "ms...");
        this.n.a((com.applovin.impl.sdk.d.a) acVar, r.a.MEDIATION_TIMEOUT, longValue, true);
    }

    public t aa() {
        return this.E;
    }

    public f ab() {
        return this.G;
    }

    public m ac() {
        return this.H;
    }

    public e ad() {
        return this.I;
    }

    public AppLovinBroadcastManager ae() {
        return AppLovinBroadcastManager.getInstance(a);
    }

    public Activity af() {
        Activity E2 = E();
        if (E2 != null) {
            return E2;
        }
        Activity a2 = Z().a();
        if (a2 != null) {
            return a2;
        }
        return null;
    }

    public <T> T b(com.applovin.impl.sdk.b.f<T> fVar, T t2) {
        return this.s.b(fVar, t2);
    }

    public <T> T b(com.applovin.impl.sdk.b.f<T> fVar, T t2, SharedPreferences sharedPreferences) {
        return this.s.b(fVar, t2, sharedPreferences);
    }

    public List<String> b(d dVar) {
        return this.b.b(dVar);
    }

    public void b() {
        synchronized (this.R) {
            this.T = true;
            J().d();
            J().a(new com.applovin.impl.sdk.d.k(this), r.a.MAIN);
        }
    }

    public <T> void b(com.applovin.impl.sdk.b.f fVar) {
        this.s.a(fVar);
    }

    public void b(String str) {
        p.g("AppLovinSdk", "Setting user id: " + str);
        this.v.a(str);
    }

    public void c(String str) {
        a(com.applovin.impl.sdk.b.f.A, str);
    }

    public boolean c() {
        boolean z2;
        synchronized (this.R) {
            z2 = this.T;
        }
        return z2;
    }

    public boolean d() {
        boolean z2;
        synchronized (this.R) {
            z2 = this.U;
        }
        return z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public void e() {
        if (this.Z != null) {
            final AppLovinSdk.SdkInitializationListener sdkInitializationListener = this.Z;
            if (d()) {
                this.Z = null;
                this.aa = null;
            } else if (this.aa != sdkInitializationListener) {
                if (((Boolean) a(d.ai)).booleanValue()) {
                    this.Z = null;
                } else {
                    this.aa = sdkInitializationListener;
                }
            } else {
                return;
            }
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    j.this.m.b("AppLovinSdk", "Calling back publisher's initialization completion handler...");
                    sdkInitializationListener.onSdkInitialized(j.this.ab);
                }
            }, Math.max(0L, ((Long) a(d.aj)).longValue()));
        }
    }

    public void f() {
        long b2 = this.p.b(com.applovin.impl.sdk.c.g.g);
        this.b.d();
        this.b.b();
        this.p.a();
        this.A.b();
        this.q.b();
        this.p.b(com.applovin.impl.sdk.c.g.g, b2 + 1);
        if (this.S.compareAndSet(true, false)) {
            b();
        } else {
            this.S.set(true);
        }
    }

    public void g() {
        this.P.b();
    }

    public String h() {
        return this.v.a();
    }

    public String i() {
        return this.v.b();
    }

    public String j() {
        return this.v.c();
    }

    public AppLovinSdkSettings k() {
        return this.f;
    }

    public AppLovinSdkConfiguration l() {
        return this.ab;
    }

    public String m() {
        return (String) a(com.applovin.impl.sdk.b.f.A);
    }

    public AppLovinAdServiceImpl n() {
        return this.g;
    }

    public NativeAdServiceImpl o() {
        return this.h;
    }

    public AppLovinEventService p() {
        return this.i;
    }

    public AppLovinUserService q() {
        return this.j;
    }

    public VariableServiceImpl r() {
        return this.k;
    }

    public String s() {
        return this.c;
    }

    public boolean t() {
        return this.V;
    }

    public String toString() {
        return "CoreSdk{sdkKey='" + this.c + '\'' + ", enabled=" + this.U + ", isFirstSession=" + this.W + '}';
    }

    public p u() {
        return this.m;
    }

    public h v() {
        return this.L;
    }

    public g w() {
        return this.M;
    }

    public MediationServiceImpl x() {
        return this.N;
    }

    public a y() {
        return this.P;
    }

    public k z() {
        return this.O;
    }
}
