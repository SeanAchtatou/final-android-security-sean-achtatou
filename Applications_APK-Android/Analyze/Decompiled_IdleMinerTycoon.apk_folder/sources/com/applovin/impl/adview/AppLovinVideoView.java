package com.applovin.impl.adview;

import android.content.Context;
import android.widget.VideoView;
import com.applovin.impl.sdk.j;

public class AppLovinVideoView extends VideoView implements t {
    private final j a;

    public AppLovinVideoView(Context context, j jVar) {
        super(context, null, 0);
        this.a = jVar;
    }

    public void setVideoSize(int i, int i2) {
        try {
            getHolder().setFixedSize(i, i2);
            requestLayout();
            invalidate();
        } catch (Exception unused) {
        }
    }
}
