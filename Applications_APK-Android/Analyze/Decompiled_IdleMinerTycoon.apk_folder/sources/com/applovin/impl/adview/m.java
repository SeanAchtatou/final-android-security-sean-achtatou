package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.impl.adview.i;
import com.applovin.impl.adview.p;
import com.applovin.impl.adview.v;
import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.ad.i;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.e;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.AppKilledService;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.R;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class m extends Activity implements j, e.a {
    public static final String KEY_WRAPPER_ID = "com.applovin.interstitial.wrapper_id";
    public static volatile n lastKnownWrapper;
    private boolean A;
    private final Handler B = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final Handler C = new Handler(Looper.getMainLooper());
    private FrameLayout D;
    /* access modifiers changed from: private */
    public h E;
    /* access modifiers changed from: private */
    public View F;
    /* access modifiers changed from: private */
    public h G;
    /* access modifiers changed from: private */
    public View H;
    /* access modifiers changed from: private */
    public f I;
    private ImageView J;
    /* access modifiers changed from: private */
    public WeakReference<MediaPlayer> K = new WeakReference<>(null);
    private b L;
    /* access modifiers changed from: private */
    public u M;
    /* access modifiers changed from: private */
    public ProgressBar N;
    private v.a O;
    private a P;
    private o Q;
    private a R;
    private BroadcastReceiver S;
    /* access modifiers changed from: private */
    public l a;
    /* access modifiers changed from: private */
    public n b;
    private volatile boolean c = false;
    protected int computedLengthSeconds = 0;
    protected i countdownManager;
    public volatile f currentAd;
    /* access modifiers changed from: private */
    public d d;
    private volatile boolean e = false;
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    private volatile boolean h = false;
    private volatile boolean i = false;
    /* access modifiers changed from: private */
    public volatile boolean j = false;
    /* access modifiers changed from: private */
    public volatile boolean k = false;
    private boolean l = false;
    public p logger;
    private volatile boolean m = false;
    private boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = false;
    private long p = 0;
    protected volatile boolean postitialWasDisplayed = false;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public long r = 0;
    /* access modifiers changed from: private */
    public long s = 0;
    public j sdk;
    /* access modifiers changed from: private */
    public long t = -2;
    private int u = 0;
    private int v = Integer.MIN_VALUE;
    protected volatile boolean videoMuted = false;
    public t videoView;
    private AtomicBoolean w = new AtomicBoolean(false);
    private AtomicBoolean x = new AtomicBoolean(false);
    private AtomicBoolean y = new AtomicBoolean(false);
    private int z = e.a;

    /* access modifiers changed from: private */
    public void A() {
        if (C()) {
            M();
            pauseReportRewardTask();
            this.logger.b("InterActivity", "Prompting incentivized ad close warning");
            this.L.b();
            return;
        }
        skipVideo();
    }

    /* access modifiers changed from: private */
    public void B() {
        c adWebView;
        if (this.currentAd.X() && (adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView()) != null) {
            adWebView.a("javascript:al_onCloseButtonTapped();");
        }
        if (D()) {
            this.logger.b("InterActivity", "Prompting incentivized non-video ad close warning");
            this.L.c();
            return;
        }
        dismiss();
    }

    private boolean C() {
        return G() && !isFullyWatched() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.bQ)).booleanValue() && this.L != null;
    }

    private boolean D() {
        return H() && !F() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.bV)).booleanValue() && this.L != null;
    }

    private int E() {
        if (!(this.currentAd instanceof com.applovin.impl.sdk.ad.a)) {
            return 0;
        }
        float h2 = ((com.applovin.impl.sdk.ad.a) this.currentAd).h();
        if (h2 <= 0.0f) {
            h2 = this.currentAd.n();
        }
        double a2 = q.a(System.currentTimeMillis() - this.p);
        double d2 = (double) h2;
        Double.isNaN(d2);
        return (int) Math.min((a2 / d2) * 100.0d, 100.0d);
    }

    private boolean F() {
        return E() >= this.currentAd.T();
    }

    private boolean G() {
        return AppLovinAdType.INCENTIVIZED.equals(this.currentAd.getType());
    }

    private boolean H() {
        return !this.currentAd.hasVideoUrl() && G();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0066, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0098, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009a, code lost:
        r0 = java.util.concurrent.TimeUnit.SECONDS.toMillis((long) r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void I() {
        /*
            r7 = this;
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            if (r0 == 0) goto L_0x00e6
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.ah()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0018
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            int r0 = r0.ai()
            if (r0 < 0) goto L_0x00e6
        L_0x0018:
            com.applovin.impl.sdk.utils.o r0 = r7.Q
            if (r0 != 0) goto L_0x00e6
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.ah()
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 < 0) goto L_0x002e
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.ah()
            goto L_0x00b6
        L_0x002e:
            boolean r0 = r7.isVastAd()
            if (r0 == 0) goto L_0x0069
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            com.applovin.impl.a.a r0 = (com.applovin.impl.a.a) r0
            com.applovin.impl.a.j r1 = r0.h()
            if (r1 == 0) goto L_0x0051
            int r4 = r1.b()
            if (r4 <= 0) goto L_0x0051
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            int r1 = r1.b()
            long r5 = (long) r1
            long r4 = r4.toMillis(r5)
        L_0x004f:
            long r2 = r2 + r4
            goto L_0x005b
        L_0x0051:
            com.applovin.impl.adview.t r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x005b
            long r4 = (long) r1
            goto L_0x004f
        L_0x005b:
            boolean r1 = r0.aj()
            if (r1 == 0) goto L_0x00a2
            float r0 = r0.n()
            int r0 = (int) r0
            if (r0 <= 0) goto L_0x00a2
            goto L_0x009a
        L_0x0069:
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.ad.a
            if (r0 == 0) goto L_0x00a2
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            com.applovin.impl.sdk.ad.a r0 = (com.applovin.impl.sdk.ad.a) r0
            com.applovin.impl.adview.t r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x007d
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x007d:
            boolean r1 = r0.aj()
            if (r1 == 0) goto L_0x00a2
            float r1 = r0.h()
            int r1 = (int) r1
            if (r1 <= 0) goto L_0x0093
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r1
            long r0 = r0.toMillis(r4)
        L_0x0091:
            long r2 = r2 + r0
            goto L_0x00a2
        L_0x0093:
            float r0 = r0.n()
            int r0 = (int) r0
            if (r0 <= 0) goto L_0x00a2
        L_0x009a:
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r0
            long r0 = r1.toMillis(r4)
            goto L_0x0091
        L_0x00a2:
            double r0 = (double) r2
            com.applovin.impl.sdk.ad.f r2 = r7.currentAd
            int r2 = r2.ai()
            double r2 = (double) r2
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r2)
            double r2 = r2 / r4
            java.lang.Double.isNaN(r0)
            double r0 = r0 * r2
            long r0 = (long) r0
        L_0x00b6:
            com.applovin.impl.sdk.p r2 = r7.logger
            java.lang.String r3 = "InterActivity"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Scheduling report reward in "
            r4.append(r5)
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r5 = r5.toSeconds(r0)
            r4.append(r5)
            java.lang.String r5 = " seconds..."
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.b(r3, r4)
            com.applovin.impl.sdk.j r2 = r7.sdk
            com.applovin.impl.adview.m$17 r3 = new com.applovin.impl.adview.m$17
            r3.<init>()
            com.applovin.impl.sdk.utils.o r0 = com.applovin.impl.sdk.utils.o.a(r0, r2, r3)
            r7.Q = r0
        L_0x00e6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.I():void");
    }

    private void J() {
        p pVar;
        String str;
        StringBuilder sb;
        String str2;
        if (this.a != null) {
            this.a.setAdDisplayListener(new AppLovinAdDisplayListener() {
                public void adDisplayed(AppLovinAd appLovinAd) {
                    if (!m.this.f) {
                        m.this.a(appLovinAd);
                    }
                }

                public void adHidden(AppLovinAd appLovinAd) {
                    m.this.b(appLovinAd);
                }
            });
            this.a.setAdClickListener(new AppLovinAdClickListener() {
                public void adClicked(AppLovinAd appLovinAd) {
                    com.applovin.impl.sdk.utils.j.a(m.this.b.e(), appLovinAd);
                }
            });
            this.currentAd = (f) this.b.b();
            if (this.x.compareAndSet(false, true)) {
                this.sdk.n().trackImpression(this.currentAd);
                this.currentAd.setHasShown(true);
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.D = new FrameLayout(this);
            this.D.setLayoutParams(layoutParams);
            this.D.setBackgroundColor(this.currentAd.C());
            this.countdownManager = new i(this.B, this.sdk);
            j();
            if (this.currentAd.isVideoAd()) {
                this.m = this.currentAd.b();
                if (this.m) {
                    pVar = this.logger;
                    str = "InterActivity";
                    sb = new StringBuilder();
                    str2 = "Preparing stream for ";
                } else {
                    pVar = this.logger;
                    str = "InterActivity";
                    sb = new StringBuilder();
                    str2 = "Preparing cached video playback for ";
                }
                sb.append(str2);
                sb.append(this.currentAd.d());
                pVar.b(str, sb.toString());
                if (this.d != null) {
                    this.d.b(this.m ? 1 : 0);
                }
            }
            this.videoMuted = i();
            Uri d2 = this.currentAd.d();
            a(d2);
            if (d2 == null) {
                I();
            }
            this.E.bringToFront();
            if (n() && this.F != null) {
                this.F.bringToFront();
            }
            if (this.G != null) {
                this.G.bringToFront();
            }
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.fg)).booleanValue()) {
                this.D.addView(this.a);
                this.a.setVisibility(4);
            }
            this.a.renderAd(this.currentAd);
            this.b.a(true);
            if (!this.currentAd.hasVideoUrl()) {
                if (H() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ca)).booleanValue()) {
                    d(this.currentAd);
                }
                showPostitial();
                return;
            }
            return;
        }
        exitWithError("AdView was null");
    }

    private void K() {
        if (this.videoView != null) {
            this.u = getVideoPercentViewed();
            this.videoView.stopPlayback();
        }
    }

    private boolean L() {
        return this.videoMuted;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.j.a(com.applovin.impl.sdk.b.f, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.f<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.j.a(java.lang.String, com.applovin.impl.sdk.b.d):com.applovin.impl.sdk.b.d<ST>
      com.applovin.impl.sdk.j.a(com.applovin.impl.sdk.b.f, java.lang.Object):void */
    private void M() {
        this.sdk.a(com.applovin.impl.sdk.b.f.v, Integer.valueOf(this.videoView != null ? this.videoView.getCurrentPosition() : 0));
        this.sdk.a((com.applovin.impl.sdk.b.f) com.applovin.impl.sdk.b.f.w, (Object) true);
        try {
            this.countdownManager.c();
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to pause countdown timers", th);
        }
        if (this.videoView != null) {
            this.videoView.pause();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void N() {
        long max = Math.max(0L, ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.dl)).longValue());
        if (max > 0) {
            p u2 = this.sdk.u();
            u2.b("InterActivity", "Resuming video with delay of " + max);
            this.C.postDelayed(new Runnable() {
                public void run() {
                    m.this.O();
                }
            }, max);
            return;
        }
        this.sdk.u().b("InterActivity", "Resuming video immediately");
        O();
    }

    /* access modifiers changed from: private */
    public void O() {
        if (!this.postitialWasDisplayed && this.videoView != null && !this.videoView.isPlaying()) {
            this.videoView.seekTo(((Integer) this.sdk.b(com.applovin.impl.sdk.b.f.v, Integer.valueOf(this.videoView.getDuration()))).intValue());
            this.videoView.start();
            this.countdownManager.a();
        }
    }

    private void P() {
        if (!this.i) {
            try {
                int videoPercentViewed = getVideoPercentViewed();
                if (this.currentAd.hasVideoUrl()) {
                    a(this.currentAd, (double) videoPercentViewed, isFullyWatched());
                    if (this.d != null) {
                        this.d.c((long) videoPercentViewed);
                    }
                } else if ((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && H() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.ca)).booleanValue()) {
                    int E2 = E();
                    p pVar = this.logger;
                    pVar.b("InterActivity", "Rewarded playable engaged at " + E2 + " percent");
                    a(this.currentAd, (double) E2, E2 >= this.currentAd.T());
                }
                this.sdk.n().trackVideoEnd(this.currentAd, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - this.p), videoPercentViewed, this.m);
                this.sdk.n().trackFullScreenAdClosed(this.currentAd, SystemClock.elapsedRealtime() - this.r, this.t, this.A, this.z);
            } catch (Throwable th) {
                if (this.logger != null) {
                    this.logger.b("InterActivity", "Failed to notify end listener.", th);
                }
            }
        }
    }

    private int a(int i2) {
        return AppLovinSdkUtils.dpToPx(this, i2);
    }

    private int a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                return 9;
            }
            if (i2 == 2) {
                return 8;
            }
            return i2 == 3 ? 1 : -1;
        } else if (i2 == 0) {
            return 1;
        } else {
            if (i2 == 1) {
                return 0;
            }
            if (i2 == 2) {
                return 9;
            }
            return i2 == 3 ? 8 : -1;
        }
    }

    private void a(long j2, final h hVar) {
        this.C.postDelayed(new Runnable() {
            public void run() {
                if (hVar.equals(m.this.E)) {
                    m.this.m();
                } else if (hVar.equals(m.this.G)) {
                    m.this.o();
                }
            }
        }, j2);
    }

    /* access modifiers changed from: private */
    public void a(PointF pointF) {
        if (!this.currentAd.u() || this.currentAd.g() == null) {
            e();
            f();
            return;
        }
        this.sdk.u().b("InterActivity", "Clicking through video...");
        clickThroughFromVideo(pointF);
    }

    private void a(Uri uri) {
        this.videoView = this.currentAd.aC() ? new p(this.sdk, this, new p.a() {
            public void a(String str) {
                m.this.handleMediaError(str);
            }
        }) : new AppLovinVideoView(this, this.sdk);
        if (uri != null) {
            this.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    WeakReference unused = m.this.K = new WeakReference(mediaPlayer);
                    boolean c = m.this.i();
                    float f = c ^ true ? 1.0f : 0.0f;
                    mediaPlayer.setVolume(f, f);
                    if (m.this.d != null) {
                        m.this.d.e(c ? 1 : 0);
                    }
                    int videoWidth = mediaPlayer.getVideoWidth();
                    int videoHeight = mediaPlayer.getVideoHeight();
                    m.this.computedLengthSeconds = (int) TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration());
                    m.this.videoView.setVideoSize(videoWidth, videoHeight);
                    if (m.this.videoView instanceof AppLovinVideoView) {
                        SurfaceHolder holder = ((AppLovinVideoView) m.this.videoView).getHolder();
                        if (holder.getSurface() != null) {
                            mediaPlayer.setDisplay(holder);
                        }
                    }
                    mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                            m.this.C.post(new Runnable() {
                                public void run() {
                                    m mVar = m.this;
                                    mVar.handleMediaError("Media player error (" + i + "," + i2 + ")");
                                }
                            });
                            return true;
                        }
                    });
                    mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                            if (i != 3) {
                                if (i == 701) {
                                    m.this.y();
                                    if (m.this.d == null) {
                                        return false;
                                    }
                                    m.this.d.h();
                                    return false;
                                } else if (i != 702) {
                                    return false;
                                }
                            }
                            m.this.z();
                            return false;
                        }
                    });
                    if (m.this.q == 0) {
                        m.this.q();
                        m.this.k();
                        m.this.v();
                        m.this.u();
                        m.this.playVideo();
                        m.this.I();
                    }
                }
            });
            this.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    m.this.h();
                }
            });
            this.videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                    m.this.C.post(new Runnable() {
                        public void run() {
                            m mVar = m.this;
                            mVar.handleMediaError("Video view error (" + i + "," + i2);
                        }
                    });
                    return true;
                }
            });
            StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            this.videoView.setVideoURI(uri);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
        this.videoView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.videoView.setOnTouchListener(new AppLovinTouchToClickListener(this.sdk, this, new AppLovinTouchToClickListener.OnClickListener() {
            public void onClick(View view, PointF pointF) {
                m.this.a(pointF);
            }
        }));
        this.D.addView((View) this.videoView);
        setContentView(this.D);
        p();
        x();
    }

    /* access modifiers changed from: private */
    public void a(final View view, final boolean z2, long j2) {
        float f2 = 1.0f;
        float f3 = z2 ? 0.0f : 1.0f;
        if (!z2) {
            f2 = 0.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (!z2) {
                    view.setVisibility(4);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                view.setVisibility(0);
            }
        });
        view.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        com.applovin.impl.sdk.utils.j.a(this.b.d(), appLovinAd);
        this.f = true;
        this.sdk.Y().c();
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                m.this.b(m.this.videoMuted);
            }
        }, ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.dp)).longValue());
    }

    private void a(AppLovinAd appLovinAd, double d2, boolean z2) {
        this.i = true;
        com.applovin.impl.sdk.utils.j.a(this.b.c(), appLovinAd, d2, z2);
    }

    private void a(final String str) {
        if (this.b != null) {
            final AppLovinAdDisplayListener d2 = this.b.d();
            if ((d2 instanceof i) && this.y.compareAndSet(false, true)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        ((i) d2).onAdDisplayFailed(str);
                    }
                });
            }
        }
    }

    private void a(final String str, long j2) {
        if (j2 >= 0) {
            this.C.postDelayed(new Runnable() {
                public void run() {
                    c adWebView = ((AdViewControllerImpl) m.this.a.getAdViewController()).getAdWebView();
                    if (adWebView != null && n.b(str)) {
                        adWebView.a(str);
                    }
                }
            }, j2);
        }
    }

    private void a(boolean z2) {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cY)).booleanValue() && g.d()) {
            AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) getDrawable(z2 ? R.drawable.unmute_to_mute : R.drawable.mute_to_unmute);
            if (animatedVectorDrawable != null) {
                this.J.setScaleType(ImageView.ScaleType.FIT_XY);
                this.J.setImageDrawable(animatedVectorDrawable);
                animatedVectorDrawable.start();
                return;
            }
        }
        Uri aF = z2 ? this.currentAd.aF() : this.currentAd.aG();
        int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.de)).intValue());
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        AppLovinSdkUtils.safePopulateImageView(this.J, aF, a2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    private boolean a() {
        int identifier = getResources().getIdentifier((String) this.sdk.a(com.applovin.impl.sdk.b.d.cV), "bool", "android");
        return identifier > 0 && getResources().getBoolean(identifier);
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public void b() {
        getWindow().getDecorView().setSystemUiVisibility(5894);
    }

    private void b(int i2) {
        try {
            setRequestedOrientation(i2);
        } catch (Throwable th) {
            this.sdk.u().b("InterActivity", "Failed to set requested orientation", th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        if (r7 == 2) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005a, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        if (r7 == 1) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r7 == 1) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r7, boolean r8) {
        /*
            r6 = this;
            com.applovin.impl.sdk.j r0 = r6.sdk
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.d.cT
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            com.applovin.impl.adview.n r1 = r6.b
            com.applovin.impl.sdk.ad.f$b r1 = r1.f()
            com.applovin.impl.sdk.ad.f$b r2 = com.applovin.impl.sdk.ad.f.b.ACTIVITY_PORTRAIT
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 != r2) goto L_0x003c
            r1 = 9
            if (r8 == 0) goto L_0x0031
            if (r7 == r5) goto L_0x0029
            if (r7 == r3) goto L_0x0029
        L_0x0023:
            r6.c = r5
        L_0x0025:
            r6.b(r5)
            goto L_0x0066
        L_0x0029:
            if (r0 == 0) goto L_0x0066
            if (r7 != r5) goto L_0x0025
        L_0x002d:
            r6.b(r1)
            goto L_0x0066
        L_0x0031:
            if (r7 == 0) goto L_0x0036
            if (r7 == r4) goto L_0x0036
            goto L_0x0023
        L_0x0036:
            if (r0 == 0) goto L_0x0066
            if (r7 != 0) goto L_0x002d
            r1 = 1
            goto L_0x002d
        L_0x003c:
            com.applovin.impl.adview.n r1 = r6.b
            com.applovin.impl.sdk.ad.f$b r1 = r1.f()
            com.applovin.impl.sdk.ad.f$b r2 = com.applovin.impl.sdk.ad.f.b.ACTIVITY_LANDSCAPE
            if (r1 != r2) goto L_0x0066
            r1 = 8
            r2 = 0
            if (r8 == 0) goto L_0x005c
            if (r7 == 0) goto L_0x0055
            if (r7 == r4) goto L_0x0055
        L_0x004f:
            r6.c = r5
            r6.b(r2)
            goto L_0x0066
        L_0x0055:
            if (r0 == 0) goto L_0x0066
            if (r7 != r4) goto L_0x005a
            goto L_0x002d
        L_0x005a:
            r1 = 0
            goto L_0x002d
        L_0x005c:
            if (r7 == r5) goto L_0x0061
            if (r7 == r3) goto L_0x0061
            goto L_0x004f
        L_0x0061:
            if (r0 == 0) goto L_0x0066
            if (r7 != r5) goto L_0x002d
            goto L_0x005a
        L_0x0066:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.b(int, boolean):void");
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        dismiss();
        c(appLovinAd);
    }

    private void b(String str) {
        f fVar = this.currentAd;
        if (fVar != null && fVar.Z()) {
            a(str, 0);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        c adWebView;
        if (this.currentAd.W() && (adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView()) != null) {
            try {
                adWebView.a(z2 ? "javascript:al_mute();" : "javascript:al_unmute();");
            } catch (Throwable th) {
                this.logger.b("InterActivity", "Unable to forward mute setting to template.", th);
            }
        }
    }

    private void c(AppLovinAd appLovinAd) {
        if (!this.g) {
            this.g = true;
            if (this.b != null) {
                com.applovin.impl.sdk.utils.j.b(this.b.d(), appLovinAd);
            }
            this.sdk.Y().d();
        }
    }

    private void c(boolean z2) {
        this.videoMuted = z2;
        MediaPlayer mediaPlayer = this.K.get();
        if (mediaPlayer != null) {
            float f2 = z2 ^ true ? 1.0f : 0.0f;
            try {
                mediaPlayer.setVolume(f2, f2);
            } catch (IllegalStateException e2) {
                com.applovin.impl.sdk.p pVar = this.logger;
                pVar.b("InterActivity", "Failed to set MediaPlayer muted: " + z2, e2);
            }
        }
    }

    private boolean c() {
        if (this.b == null || this.sdk == null || ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cN)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cO)).booleanValue() || !this.j) {
            return ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cP)).booleanValue() && this.postitialWasDisplayed;
        }
        return true;
    }

    @SuppressLint({"WrongConstant"})
    private void d() {
        if (this.sdk == null || !isFinishing()) {
            if (!(this.currentAd == null || this.v == Integer.MIN_VALUE)) {
                b(this.v);
            }
            finish();
        }
    }

    private void d(AppLovinAd appLovinAd) {
        if (!this.h) {
            this.h = true;
            com.applovin.impl.sdk.utils.j.a(this.b.c(), appLovinAd);
        }
    }

    private void e() {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cW)).booleanValue() && this.I != null && this.I.getVisibility() != 8) {
            a(this.I, this.I.getVisibility() == 4, 750);
        }
    }

    private void f() {
        s t2 = this.currentAd.t();
        if (t2 != null && t2.e() && !this.postitialWasDisplayed && this.M != null) {
            a(this.M, this.M.getVisibility() == 4, t2.f());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.j.a(com.applovin.impl.sdk.b.f, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.f<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.j.a(java.lang.String, com.applovin.impl.sdk.b.d):com.applovin.impl.sdk.b.d<ST>
      com.applovin.impl.sdk.j.a(com.applovin.impl.sdk.b.f, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.j.a(com.applovin.impl.sdk.b.f, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.f<java.lang.Integer>, int]
     candidates:
      com.applovin.impl.sdk.j.a(java.lang.String, com.applovin.impl.sdk.b.d):com.applovin.impl.sdk.b.d<ST>
      com.applovin.impl.sdk.j.a(com.applovin.impl.sdk.b.f, java.lang.Object):void */
    private void g() {
        if (this.sdk != null) {
            this.sdk.a((com.applovin.impl.sdk.b.f) com.applovin.impl.sdk.b.f.w, (Object) false);
            this.sdk.a((com.applovin.impl.sdk.b.f) com.applovin.impl.sdk.b.f.v, (Object) 0);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        this.e = true;
        showPostitial();
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return ((Integer) this.sdk.b(com.applovin.impl.sdk.b.f.v, 0)).intValue() > 0 ? this.videoMuted : ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dd)).booleanValue() ? this.sdk.k().isMuted() : ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.db)).booleanValue();
    }

    private void j() {
        this.E = h.a(this.sdk, this, this.currentAd.o());
        this.E.setVisibility(8);
        this.E.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                m.this.B();
            }
        });
        int a2 = a(this.currentAd.aa());
        int i2 = 3;
        int i3 = (this.currentAd.ad() ? 3 : 5) | 48;
        if (!this.currentAd.ae()) {
            i2 = 5;
        }
        int i4 = i2 | 48;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, i3 | 48);
        this.E.a(a2);
        int a3 = a(this.currentAd.ab());
        int a4 = a(this.currentAd.ac());
        layoutParams.setMargins(a4, a3, a4, a3);
        this.D.addView(this.E, layoutParams);
        this.G = h.a(this.sdk, this, this.currentAd.p());
        this.G.setVisibility(8);
        this.G.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                m.this.A();
            }
        });
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a2, a2, i4);
        layoutParams2.setMargins(a4, a3, a4, a3);
        this.G.a(a2);
        this.D.addView(this.G, layoutParams2);
        this.G.bringToFront();
        if (n()) {
            int a5 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cc)).intValue());
            this.F = new View(this);
            this.F.setBackgroundColor(0);
            this.F.setVisibility(8);
            this.H = new View(this);
            this.H.setBackgroundColor(0);
            this.H.setVisibility(8);
            int i5 = a2 + a5;
            int a6 = a3 - a(5);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(i5, i5, i3);
            layoutParams3.setMargins(a6, a6, a6, a6);
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(i5, i5, i4);
            layoutParams4.setMargins(a6, a6, a6, a6);
            this.F.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    m.this.E.performClick();
                }
            });
            this.H.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    m.this.G.performClick();
                }
            });
            this.D.addView(this.F, layoutParams3);
            this.F.bringToFront();
            this.D.addView(this.H, layoutParams4);
            this.H.bringToFront();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.J == null) {
            try {
                this.videoMuted = i();
                this.J = new ImageView(this);
                if (!l()) {
                    int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.de)).intValue());
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dg)).intValue());
                    this.J.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    int a3 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.df)).intValue());
                    layoutParams.setMargins(a3, a3, a3, a3);
                    if ((this.videoMuted ? this.currentAd.aF() : this.currentAd.aG()) != null) {
                        com.applovin.impl.sdk.p u2 = this.sdk.u();
                        u2.b("InterActivity", "Added mute button with params: " + layoutParams);
                        a(this.videoMuted);
                        this.J.setClickable(true);
                        this.J.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                m.this.toggleMute();
                            }
                        });
                        this.D.addView(this.J, layoutParams);
                        this.J.bringToFront();
                        return;
                    }
                    this.sdk.u().e("InterActivity", "Attempting to add mute button but could not find uri");
                    return;
                }
                this.sdk.u().b("InterActivity", "Mute button should be hidden");
            } catch (Exception e2) {
                this.sdk.u().a("InterActivity", "Failed to attach mute button", e2);
            }
        }
    }

    private boolean l() {
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cZ)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.da)).booleanValue() || i()) {
            return false;
        }
        return !((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dc)).booleanValue();
    }

    /* access modifiers changed from: private */
    public void m() {
        runOnUiThread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, boolean):boolean
             arg types: [com.applovin.impl.adview.m, int]
             candidates:
              com.applovin.impl.adview.m.a(int, boolean):int
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, long):long
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, java.lang.ref.WeakReference):java.lang.ref.WeakReference
              com.applovin.impl.adview.m.a(long, com.applovin.impl.adview.h):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, android.graphics.PointF):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, com.applovin.sdk.AppLovinAd):void
              com.applovin.impl.adview.m.a(java.lang.String, long):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, boolean):boolean */
            public void run() {
                try {
                    if (m.this.j) {
                        m.this.E.setVisibility(0);
                        return;
                    }
                    long unused = m.this.r = SystemClock.elapsedRealtime();
                    boolean unused2 = m.this.j = true;
                    if (m.this.n() && m.this.F != null) {
                        m.this.F.setVisibility(0);
                        m.this.F.bringToFront();
                    }
                    m.this.E.setVisibility(0);
                    m.this.E.bringToFront();
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(com.applovin.impl.sdk.b.d.cF)).intValue());
                    alphaAnimation.setRepeatCount(0);
                    m.this.E.startAnimation(alphaAnimation);
                } catch (Throwable unused3) {
                    m.this.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean n() {
        return ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cc)).intValue() > 0;
    }

    /* access modifiers changed from: private */
    public void o() {
        runOnUiThread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, boolean):boolean
             arg types: [com.applovin.impl.adview.m, int]
             candidates:
              com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, long):long
              com.applovin.impl.adview.m.b(int, boolean):void
              com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, com.applovin.sdk.AppLovinAd):void
              com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, boolean):boolean */
            public void run() {
                try {
                    if (!m.this.k && m.this.G != null) {
                        long unused = m.this.t = -1;
                        long unused2 = m.this.s = SystemClock.elapsedRealtime();
                        boolean unused3 = m.this.k = true;
                        m.this.G.setVisibility(0);
                        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                        alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(com.applovin.impl.sdk.b.d.cF)).intValue());
                        alphaAnimation.setRepeatCount(0);
                        m.this.G.startAnimation(alphaAnimation);
                        if (m.this.n() && m.this.H != null) {
                            m.this.H.setVisibility(0);
                            m.this.H.bringToFront();
                        }
                    }
                } catch (Throwable th) {
                    com.applovin.impl.sdk.p pVar = m.this.logger;
                    pVar.d("InterActivity", "Unable to show skip button: " + th);
                }
            }
        });
    }

    private void p() {
        if (this.currentAd.m() >= 0.0f) {
            a(q.b(this.currentAd.m()), (!this.l || this.G == null) ? this.E : this.G);
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        boolean z2 = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cM)).booleanValue() && t() > 0;
        if (this.I == null && z2) {
            this.I = new f(this);
            int B2 = this.currentAd.B();
            this.I.setTextColor(B2);
            this.I.setTextSize((float) ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cL)).intValue());
            this.I.setFinishedStrokeColor(B2);
            this.I.setFinishedStrokeWidth((float) ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cK)).intValue());
            this.I.setMax(t());
            this.I.setProgress(t());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cJ)).intValue()), a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cJ)).intValue()), ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cI)).intValue());
            int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.cH)).intValue());
            layoutParams.setMargins(a2, a2, a2, a2);
            this.D.addView(this.I, layoutParams);
            this.I.bringToFront();
            this.I.setVisibility(0);
            final long s2 = s();
            this.countdownManager.a("COUNTDOWN_CLOCK", 1000, new i.a() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, boolean):boolean
                 arg types: [com.applovin.impl.adview.m, int]
                 candidates:
                  com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, long):long
                  com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, boolean):boolean */
                public void a() {
                    if (m.this.I != null) {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(s2 - ((long) m.this.videoView.getCurrentPosition()));
                        if (seconds <= 0) {
                            m.this.I.setVisibility(8);
                            boolean unused = m.this.o = true;
                        } else if (m.this.r()) {
                            m.this.I.setProgress((int) seconds);
                        }
                    }
                }

                public boolean b() {
                    return m.this.r();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public boolean r() {
        return !this.o && !this.postitialWasDisplayed && this.videoView.isPlaying();
    }

    private long s() {
        return TimeUnit.SECONDS.toMillis((long) t());
    }

    private int t() {
        int A2 = this.currentAd.A();
        return (A2 <= 0 && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.f0do)).booleanValue()) ? this.computedLengthSeconds + 1 : A2;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void u() {
        if (this.N == null && this.currentAd.L()) {
            this.logger.c("InterActivity", "Attaching video progress bar...");
            this.N = new ProgressBar(this, null, 16842872);
            this.N.setMax(((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dj)).intValue());
            this.N.setPadding(0, 0, 0, 0);
            if (g.d()) {
                try {
                    this.N.setProgressTintList(ColorStateList.valueOf(this.currentAd.M()));
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Unable to update progress bar color.", th);
                }
            }
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.videoView.getWidth(), 20, 80);
            layoutParams.setMargins(0, 0, 0, ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dk)).intValue());
            this.D.addView(this.N, layoutParams);
            this.N.bringToFront();
            this.countdownManager.a("PROGRESS_BAR", ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.di)).longValue(), new i.a() {
                public void a() {
                    if (m.this.N == null) {
                        return;
                    }
                    if (m.this.shouldContinueFullLengthVideoCountdown()) {
                        m.this.N.setProgress((int) ((((float) m.this.videoView.getCurrentPosition()) / ((float) m.this.videoView.getDuration())) * ((float) ((Integer) m.this.sdk.a(com.applovin.impl.sdk.b.d.dj)).intValue())));
                        return;
                    }
                    m.this.N.setVisibility(8);
                }

                public boolean b() {
                    return m.this.shouldContinueFullLengthVideoCountdown();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        final s t2 = this.currentAd.t();
        if (n.b(this.currentAd.s()) && t2 != null && this.M == null) {
            this.logger.c("InterActivity", "Attaching video button...");
            this.M = w();
            double a2 = (double) t2.a();
            Double.isNaN(a2);
            double b2 = (double) t2.b();
            Double.isNaN(b2);
            int width = this.videoView.getWidth();
            int height = this.videoView.getHeight();
            double d2 = (double) width;
            Double.isNaN(d2);
            double d3 = (double) height;
            Double.isNaN(d3);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) ((a2 / 100.0d) * d2), (int) ((b2 / 100.0d) * d3), t2.d());
            int a3 = a(t2.c());
            layoutParams.setMargins(a3, a3, a3, a3);
            this.D.addView(this.M, layoutParams);
            this.M.bringToFront();
            if (t2.i() > 0.0f) {
                this.M.setVisibility(4);
                this.C.postDelayed(new Runnable() {
                    public void run() {
                        m.this.a(m.this.M, true, t2.g());
                        m.this.M.bringToFront();
                    }
                }, q.b(t2.i()));
            }
            if (t2.j() > 0.0f) {
                this.C.postDelayed(new Runnable() {
                    public void run() {
                        m.this.a(m.this.M, false, t2.h());
                    }
                }, q.b(t2.j()));
            }
        }
    }

    private u w() {
        com.applovin.impl.sdk.p pVar = this.logger;
        pVar.b("InterActivity", "Create video button with HTML = " + this.currentAd.s());
        v vVar = new v(this.sdk);
        this.O = new v.a() {
            public void a(u uVar) {
                m.this.logger.b("InterActivity", "Clicking through from video button...");
                m.this.clickThroughFromVideo(uVar.getAndClearLastClickLocation());
            }

            public void b(u uVar) {
                m.this.logger.b("InterActivity", "Closing ad from video button...");
                m.this.dismiss();
            }

            public void c(u uVar) {
                m.this.logger.b("InterActivity", "Skipping video from video button...");
                m.this.skipVideo();
            }
        };
        vVar.a(new WeakReference(this.O));
        u a2 = u.a(this.sdk, vVar, getApplicationContext());
        a2.a(this.currentAd.s());
        return a2;
    }

    private void x() {
        if (this.m && this.currentAd.N()) {
            this.P = new a(this, ((Integer) this.sdk.a(com.applovin.impl.sdk.b.d.dn)).intValue(), this.currentAd.P());
            this.P.setColor(this.currentAd.Q());
            this.P.setBackgroundColor(this.currentAd.R());
            this.P.setVisibility(8);
            this.D.addView(this.P, new FrameLayout.LayoutParams(-1, -1, 17));
            this.D.bringChildToFront(this.P);
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.P != null) {
            this.P.a();
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        if (this.P != null) {
            this.P.b();
        }
    }

    public void clickThroughFromVideo(PointF pointF) {
        try {
            if (this.currentAd.al() && this.l) {
                o();
            }
            this.sdk.n().trackAndLaunchVideoClick(this.currentAd, this.a, this.currentAd.g(), pointF);
            com.applovin.impl.sdk.utils.j.a(this.b.e(), this.currentAd);
            if (this.d != null) {
                this.d.b();
            }
        } catch (Throwable th) {
            this.sdk.u().b("InterActivity", "Encountered error while clicking through video.", th);
        }
    }

    public void continueVideo() {
        O();
    }

    public void dismiss() {
        com.applovin.impl.sdk.p.g("InterActivity", "Dismissing ad after " + (System.currentTimeMillis() - this.p) + " milliseconds elapsed");
        if (this.sdk != null) {
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cX)).booleanValue()) {
                stopService(new Intent(getBaseContext(), AppKilledService.class));
                this.sdk.ae().unregisterReceiver(this.S);
            }
            this.sdk.ad().b(this);
        }
        g();
        P();
        if (this.b != null) {
            if (this.currentAd != null) {
                c(this.currentAd);
                if (this.d != null) {
                    this.d.c();
                    this.d = null;
                }
                a("javascript:al_onPoststitialDismiss();", (long) this.currentAd.V());
            }
            this.b.a(false);
            this.b.g();
        }
        lastKnownWrapper = null;
        d();
    }

    public void exitWithError(String str) {
        a(str);
        try {
            com.applovin.impl.sdk.p.c("InterActivity", "Failed to properly render an Interstitial Activity, due to error: " + str, new Throwable("Initialized = " + n.b + "; CleanedUp = " + n.c));
            c(new h());
        } catch (Exception e2) {
            com.applovin.impl.sdk.p.c("InterActivity", "Failed to show a video ad due to error:", e2);
        }
        dismiss();
    }

    public boolean getPostitialWasDisplayed() {
        return this.postitialWasDisplayed;
    }

    public int getVideoPercentViewed() {
        if (this.e) {
            return 100;
        }
        if (this.videoView != null) {
            int duration = this.videoView.getDuration();
            if (duration <= 0) {
                return this.u;
            }
            double currentPosition = (double) this.videoView.getCurrentPosition();
            double d2 = (double) duration;
            Double.isNaN(currentPosition);
            Double.isNaN(d2);
            return (int) ((currentPosition / d2) * 100.0d);
        }
        this.logger.e("InterActivity", "No video view detected on video end");
        return 0;
    }

    public void handleMediaError(String str) {
        this.logger.e("InterActivity", str);
        if (this.w.compareAndSet(false, true) && this.currentAd.H()) {
            a(str);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isFullyWatched() {
        return getVideoPercentViewed() >= this.currentAd.T();
    }

    /* access modifiers changed from: protected */
    public boolean isVastAd() {
        return this.currentAd instanceof com.applovin.impl.a.a;
    }

    public void onBackPressed() {
        h hVar;
        if (this.currentAd != null) {
            if (this.currentAd.aD() && !this.postitialWasDisplayed) {
                return;
            }
            if (this.currentAd.aE() && this.postitialWasDisplayed) {
                return;
            }
        }
        if (c()) {
            this.logger.b("InterActivity", "Back button was pressed; forwarding to Android for handling...");
        } else {
            try {
                if (!this.postitialWasDisplayed && this.l && this.G != null && this.G.getVisibility() == 0 && this.G.getAlpha() > 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to skip button.");
                    hVar = this.G;
                } else if (this.E == null || this.E.getVisibility() != 0 || this.E.getAlpha() <= 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed, but was not eligible for dismissal.");
                    b("javascript:al_onBackPressed();");
                    return;
                } else {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to close button.");
                    hVar = this.E;
                }
                hVar.performClick();
                b("javascript:al_onBackPressed();");
                return;
            } catch (Exception unused) {
            }
        }
        super.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation != 0 && (this.videoView instanceof p) && this.K.get() != null) {
            MediaPlayer mediaPlayer = this.K.get();
            this.videoView.setVideoSize(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            super.onCreate(r7)
            if (r7 == 0) goto L_0x0010
            java.lang.String r0 = "instance_impression_tracked"
            boolean r0 = r7.getBoolean(r0)
            java.util.concurrent.atomic.AtomicBoolean r1 = r6.x
            r1.set(r0)
        L_0x0010:
            r0 = 1
            r6.requestWindowFeature(r0)
            android.os.StrictMode$ThreadPolicy r0 = android.os.StrictMode.allowThreadDiskReads()
            android.content.Intent r1 = r6.getIntent()     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r2 = "com.applovin.interstitial.wrapper_id"
            java.lang.String r1 = r1.getStringExtra(r2)     // Catch:{ Throwable -> 0x01b4 }
            if (r1 == 0) goto L_0x01af
            boolean r2 = r1.isEmpty()     // Catch:{ Throwable -> 0x01b4 }
            if (r2 != 0) goto L_0x01af
            com.applovin.impl.adview.n r1 = com.applovin.impl.adview.n.a(r1)     // Catch:{ Throwable -> 0x01b4 }
            r6.b = r1     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.n r1 = r6.b     // Catch:{ Throwable -> 0x01b4 }
            if (r1 != 0) goto L_0x003c
            com.applovin.impl.adview.n r1 = com.applovin.impl.adview.m.lastKnownWrapper     // Catch:{ Throwable -> 0x01b4 }
            if (r1 == 0) goto L_0x003c
            com.applovin.impl.adview.n r1 = com.applovin.impl.adview.m.lastKnownWrapper     // Catch:{ Throwable -> 0x01b4 }
            r6.b = r1     // Catch:{ Throwable -> 0x01b4 }
        L_0x003c:
            com.applovin.impl.adview.n r1 = r6.b     // Catch:{ Throwable -> 0x01b4 }
            if (r1 == 0) goto L_0x019b
            com.applovin.impl.adview.n r1 = r6.b     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.sdk.AppLovinAd r1 = r1.b()     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.n r2 = r6.b     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r2 = r2.a()     // Catch:{ Throwable -> 0x01b4 }
            r6.sdk = r2     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.n r2 = r6.b     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r2 = r2.a()     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.p r2 = r2.u()     // Catch:{ Throwable -> 0x01b4 }
            r6.logger = r2     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r2 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r3 = com.applovin.impl.sdk.b.d.cX     // Catch:{ Throwable -> 0x01b4 }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ Throwable -> 0x01b4 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ Throwable -> 0x01b4 }
            boolean r2 = r2.booleanValue()     // Catch:{ Throwable -> 0x01b4 }
            if (r2 == 0) goto L_0x0091
            com.applovin.impl.adview.m$1 r2 = new com.applovin.impl.adview.m$1     // Catch:{ Throwable -> 0x01b4 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x01b4 }
            r6.S = r2     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r2 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.AppLovinBroadcastManager r2 = r2.ae()     // Catch:{ Throwable -> 0x01b4 }
            android.content.BroadcastReceiver r3 = r6.S     // Catch:{ Throwable -> 0x01b4 }
            android.content.IntentFilter r4 = new android.content.IntentFilter     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r5 = "com.applovin.app_killed"
            r4.<init>(r5)     // Catch:{ Throwable -> 0x01b4 }
            r2.registerReceiver(r3, r4)     // Catch:{ Throwable -> 0x01b4 }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Throwable -> 0x01b4 }
            android.content.Context r3 = r6.getBaseContext()     // Catch:{ Throwable -> 0x01b4 }
            java.lang.Class<com.applovin.impl.sdk.utils.AppKilledService> r4 = com.applovin.impl.sdk.utils.AppKilledService.class
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x01b4 }
            r6.startService(r2)     // Catch:{ Throwable -> 0x01b4 }
        L_0x0091:
            if (r1 == 0) goto L_0x0195
            com.applovin.impl.sdk.ad.f r1 = (com.applovin.impl.sdk.ad.f) r1     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.c.d r2 = new com.applovin.impl.sdk.c.d     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r3 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            r2.<init>(r1, r3)     // Catch:{ Throwable -> 0x01b4 }
            r6.d = r2     // Catch:{ Throwable -> 0x01b4 }
            boolean r2 = r1.aq()     // Catch:{ Throwable -> 0x01b4 }
            if (r2 == 0) goto L_0x00ad
            com.applovin.impl.sdk.j r2 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.e r2 = r2.ad()     // Catch:{ Throwable -> 0x01b4 }
            r2.a(r6)     // Catch:{ Throwable -> 0x01b4 }
        L_0x00ad:
            r2 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r2 = r6.findViewById(r2)     // Catch:{ Throwable -> 0x01b4 }
            if (r2 == 0) goto L_0x00c9
            boolean r3 = r1.hasVideoUrl()     // Catch:{ Throwable -> 0x01b4 }
            if (r3 == 0) goto L_0x00c4
            int r3 = r1.C()     // Catch:{ Throwable -> 0x01b4 }
        L_0x00c0:
            r2.setBackgroundColor(r3)     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x00c9
        L_0x00c4:
            int r3 = r1.D()     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x00c0
        L_0x00c9:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01b4 }
            r6.p = r2     // Catch:{ Throwable -> 0x01b4 }
            boolean r2 = r1.v()     // Catch:{ Throwable -> 0x01b4 }
            if (r2 == 0) goto L_0x00de
            android.view.Window r2 = r6.getWindow()     // Catch:{ Throwable -> 0x01b4 }
            r3 = 16777216(0x1000000, float:2.3509887E-38)
            r2.setFlags(r3, r3)     // Catch:{ Throwable -> 0x01b4 }
        L_0x00de:
            boolean r2 = r1.w()     // Catch:{ Throwable -> 0x01b4 }
            if (r2 == 0) goto L_0x00ed
            android.view.Window r2 = r6.getWindow()     // Catch:{ Throwable -> 0x01b4 }
            r3 = 128(0x80, float:1.794E-43)
            r2.addFlags(r3)     // Catch:{ Throwable -> 0x01b4 }
        L_0x00ed:
            int r2 = com.applovin.impl.sdk.utils.q.e(r6)     // Catch:{ Throwable -> 0x01b4 }
            boolean r3 = com.applovin.sdk.AppLovinSdkUtils.isTablet(r6)     // Catch:{ Throwable -> 0x01b4 }
            int r4 = r6.a(r2, r3)     // Catch:{ Throwable -> 0x01b4 }
            if (r7 != 0) goto L_0x00fe
            r6.v = r4     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x0106
        L_0x00fe:
            java.lang.String r5 = "original_orientation"
            int r7 = r7.getInt(r5, r4)     // Catch:{ Throwable -> 0x01b4 }
            r6.v = r7     // Catch:{ Throwable -> 0x01b4 }
        L_0x0106:
            boolean r7 = r1.z()     // Catch:{ Throwable -> 0x01b4 }
            if (r7 == 0) goto L_0x0138
            r7 = -1
            if (r4 == r7) goto L_0x012b
            com.applovin.impl.sdk.p r7 = r6.logger     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r1 = "InterActivity"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01b4 }
            r2.<init>()     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r3 = "Locking activity orientation to current orientation: "
            r2.append(r3)     // Catch:{ Throwable -> 0x01b4 }
            r2.append(r4)     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01b4 }
            r7.b(r1, r2)     // Catch:{ Throwable -> 0x01b4 }
            r6.b(r4)     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x0142
        L_0x012b:
            com.applovin.impl.sdk.p r7 = r6.logger     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r1 = "InterActivity"
            java.lang.String r4 = "Unable to detect current orientation. Locking to targeted orientation..."
            r7.e(r1, r4)     // Catch:{ Throwable -> 0x01b4 }
        L_0x0134:
            r6.b(r2, r3)     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x0142
        L_0x0138:
            com.applovin.impl.sdk.p r7 = r6.logger     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r1 = "InterActivity"
            java.lang.String r4 = "Locking activity orientation to targeted orientation..."
            r7.b(r1, r4)     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x0134
        L_0x0142:
            com.applovin.impl.adview.l r7 = new com.applovin.impl.adview.l     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r1 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.sdk.AppLovinSdk r1 = r1.R()     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.sdk.AppLovinAdSize r2 = com.applovin.sdk.AppLovinAdSize.INTERSTITIAL     // Catch:{ Throwable -> 0x01b4 }
            r7.<init>(r1, r2, r6)     // Catch:{ Throwable -> 0x01b4 }
            r6.a = r7     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.l r7 = r6.a     // Catch:{ Throwable -> 0x01b4 }
            r1 = 0
            r7.setAutoDestroy(r1)     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.l r7 = r6.a     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.adview.AdViewController r7 = r7.getAdViewController()     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.AdViewControllerImpl r7 = (com.applovin.impl.adview.AdViewControllerImpl) r7     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.c.d r1 = r6.d     // Catch:{ Throwable -> 0x01b4 }
            r7.setStatsManagerHelper(r1)     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.n r7 = r6.b     // Catch:{ Throwable -> 0x01b4 }
            r7.a(r6)     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r7 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.b.d<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.d.dm     // Catch:{ Throwable -> 0x01b4 }
            java.lang.Object r7 = r7.a(r1)     // Catch:{ Throwable -> 0x01b4 }
            java.lang.Boolean r7 = (java.lang.Boolean) r7     // Catch:{ Throwable -> 0x01b4 }
            boolean r7 = r7.booleanValue()     // Catch:{ Throwable -> 0x01b4 }
            r6.l = r7     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.a.b r7 = new com.applovin.impl.sdk.a.b     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r1 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            r7.<init>(r6, r1)     // Catch:{ Throwable -> 0x01b4 }
            r6.L = r7     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.adview.m$12 r7 = new com.applovin.impl.adview.m$12     // Catch:{ Throwable -> 0x01b4 }
            r7.<init>()     // Catch:{ Throwable -> 0x01b4 }
            r6.R = r7     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.j r7 = r6.sdk     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.a r7 = r7.Z()     // Catch:{ Throwable -> 0x01b4 }
            com.applovin.impl.sdk.utils.a r1 = r6.R     // Catch:{ Throwable -> 0x01b4 }
            r7.a(r1)     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x01c7
        L_0x0195:
            java.lang.String r7 = "No current ad found."
        L_0x0197:
            r6.exitWithError(r7)     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x01c7
        L_0x019b:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01b4 }
            r7.<init>()     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r1 = "Wrapper is null; initialized state: "
            r7.append(r1)     // Catch:{ Throwable -> 0x01b4 }
            boolean r1 = com.applovin.impl.adview.n.b     // Catch:{ Throwable -> 0x01b4 }
            r7.append(r1)     // Catch:{ Throwable -> 0x01b4 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x01b4 }
            goto L_0x0197
        L_0x01af:
            java.lang.String r7 = "Wrapper ID is null"
            goto L_0x0197
        L_0x01b2:
            r7 = move-exception
            goto L_0x01da
        L_0x01b4:
            r7 = move-exception
            com.applovin.impl.sdk.p r1 = r6.logger     // Catch:{ all -> 0x01b2 }
            if (r1 == 0) goto L_0x01c2
            com.applovin.impl.sdk.p r1 = r6.logger     // Catch:{ all -> 0x01b2 }
            java.lang.String r2 = "InterActivity"
            java.lang.String r3 = "Encountered error during onCreate."
            r1.b(r2, r3, r7)     // Catch:{ all -> 0x01b2 }
        L_0x01c2:
            java.lang.String r7 = "An error was encountered during interstitial ad creation."
            r6.exitWithError(r7)     // Catch:{ all -> 0x01b2 }
        L_0x01c7:
            android.os.StrictMode.setThreadPolicy(r0)
            r6.g()
            com.applovin.impl.sdk.c.d r7 = r6.d
            if (r7 == 0) goto L_0x01d6
            com.applovin.impl.sdk.c.d r7 = r6.d
            r7.a()
        L_0x01d6:
            r6.J()
            return
        L_0x01da:
            android.os.StrictMode.setThreadPolicy(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r4.currentAd != null) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0079, code lost:
        if (r4.currentAd == null) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007b, code lost:
        P();
        c(r4.currentAd);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0083, code lost:
        super.onDestroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0086, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDestroy() {
        /*
            r4 = this;
            com.applovin.impl.adview.l r0 = r4.a     // Catch:{ Throwable -> 0x0069 }
            r1 = 0
            if (r0 == 0) goto L_0x001d
            com.applovin.impl.adview.l r0 = r4.a     // Catch:{ Throwable -> 0x0069 }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ Throwable -> 0x0069 }
            boolean r2 = r0 instanceof android.view.ViewGroup     // Catch:{ Throwable -> 0x0069 }
            if (r2 == 0) goto L_0x0016
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0     // Catch:{ Throwable -> 0x0069 }
            com.applovin.impl.adview.l r2 = r4.a     // Catch:{ Throwable -> 0x0069 }
            r0.removeView(r2)     // Catch:{ Throwable -> 0x0069 }
        L_0x0016:
            com.applovin.impl.adview.l r0 = r4.a     // Catch:{ Throwable -> 0x0069 }
            r0.destroy()     // Catch:{ Throwable -> 0x0069 }
            r4.a = r1     // Catch:{ Throwable -> 0x0069 }
        L_0x001d:
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x002b
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ Throwable -> 0x0069 }
            r0.pause()     // Catch:{ Throwable -> 0x0069 }
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ Throwable -> 0x0069 }
            r0.stopPlayback()     // Catch:{ Throwable -> 0x0069 }
        L_0x002b:
            com.applovin.impl.sdk.j r0 = r4.sdk     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0047
            java.lang.ref.WeakReference<android.media.MediaPlayer> r0 = r4.K     // Catch:{ Throwable -> 0x0069 }
            java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x0069 }
            android.media.MediaPlayer r0 = (android.media.MediaPlayer) r0     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x003c
            r0.release()     // Catch:{ Throwable -> 0x0069 }
        L_0x003c:
            com.applovin.impl.sdk.j r0 = r4.sdk     // Catch:{ Throwable -> 0x0069 }
            com.applovin.impl.sdk.a r0 = r0.Z()     // Catch:{ Throwable -> 0x0069 }
            com.applovin.impl.sdk.utils.a r2 = r4.R     // Catch:{ Throwable -> 0x0069 }
            r0.b(r2)     // Catch:{ Throwable -> 0x0069 }
        L_0x0047:
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0050
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ Throwable -> 0x0069 }
            r0.b()     // Catch:{ Throwable -> 0x0069 }
        L_0x0050:
            android.os.Handler r0 = r4.C     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0059
            android.os.Handler r0 = r4.C     // Catch:{ Throwable -> 0x0069 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ Throwable -> 0x0069 }
        L_0x0059:
            android.os.Handler r0 = r4.B     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0062
            android.os.Handler r0 = r4.B     // Catch:{ Throwable -> 0x0069 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ Throwable -> 0x0069 }
        L_0x0062:
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            if (r0 == 0) goto L_0x0083
            goto L_0x007b
        L_0x0067:
            r0 = move-exception
            goto L_0x0087
        L_0x0069:
            r0 = move-exception
            com.applovin.impl.sdk.p r1 = r4.logger     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0077
            com.applovin.impl.sdk.p r1 = r4.logger     // Catch:{ all -> 0x0067 }
            java.lang.String r2 = "InterActivity"
            java.lang.String r3 = "Unable to destroy video view"
            r1.a(r2, r3, r0)     // Catch:{ all -> 0x0067 }
        L_0x0077:
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            if (r0 == 0) goto L_0x0083
        L_0x007b:
            r4.P()
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            r4.c(r0)
        L_0x0083:
            super.onDestroy()
            return
        L_0x0087:
            com.applovin.impl.sdk.ad.f r1 = r4.currentAd
            if (r1 == 0) goto L_0x0093
            r4.P()
            com.applovin.impl.sdk.ad.f r1 = r4.currentAd
            r4.c(r1)
        L_0x0093:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onDestroy():void");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if ((i2 == 25 || i2 == 24) && this.currentAd.Y() && L()) {
            toggleMute();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.logger.b("InterActivity", "App paused...");
        this.q = System.currentTimeMillis();
        if (this.postitialWasDisplayed) {
            M();
        }
        this.b.a(false);
        this.L.a();
        pauseReportRewardTask();
        b("javascript:al_onAppPaused();");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        h hVar;
        super.onResume();
        this.logger.b("InterActivity", "App resumed...");
        boolean z2 = true;
        this.b.a(true);
        if (!this.n) {
            if (this.d != null) {
                this.d.d(System.currentTimeMillis() - this.q);
            }
            if (!((Boolean) this.sdk.b(com.applovin.impl.sdk.b.f.w, false)).booleanValue() || this.L.d() || this.postitialWasDisplayed) {
                if (!(this.currentAd instanceof com.applovin.impl.sdk.ad.a) || !((com.applovin.impl.sdk.ad.a) this.currentAd).i()) {
                    z2 = false;
                }
                if (this.currentAd != null && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cG)).booleanValue() && !this.currentAd.x() && this.postitialWasDisplayed && this.E != null && !z2) {
                    hVar = this.E;
                }
                resumeReportRewardTask();
            } else {
                N();
                y();
                if (this.currentAd != null && ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cG)).booleanValue() && !this.currentAd.y() && !this.postitialWasDisplayed && this.l && this.G != null) {
                    hVar = this.G;
                }
                resumeReportRewardTask();
            }
            a(0, hVar);
            resumeReportRewardTask();
        } else if (!this.L.d() && !this.postitialWasDisplayed && this.currentAd != null && this.currentAd.O()) {
            y();
        }
        b("javascript:al_onAppResumed();");
    }

    public void onRingerModeChanged(int i2) {
        String str;
        if (this.z != e.a) {
            this.A = true;
        }
        c adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView();
        if (adWebView != null) {
            if (e.a(i2) && !e.a(this.z)) {
                str = "javascript:al_muteSwitchOn();";
            } else if (i2 == 2) {
                str = "javascript:al_muteSwitchOff();";
            }
            adWebView.a(str);
        }
        this.z = i2;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("instance_impression_tracked", this.x.get());
        bundle.putInt("original_orientation", this.v);
    }

    public void onWindowFocusChanged(boolean z2) {
        String str;
        String str2;
        super.onWindowFocusChanged(z2);
        if (z2) {
            if (this.sdk != null) {
                this.logger.b("InterActivity", "Window gained focus");
                try {
                    if (!g.c() || !((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.dh)).booleanValue() || !a()) {
                        getWindow().setFlags(1024, 1024);
                    } else {
                        b();
                        if (((Long) this.sdk.a(com.applovin.impl.sdk.b.d.cQ)).longValue() > 0) {
                            this.C.postDelayed(new Runnable() {
                                public void run() {
                                    m.this.b();
                                }
                            }, ((Long) this.sdk.a(com.applovin.impl.sdk.b.d.cQ)).longValue());
                        }
                    }
                    if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cR)).booleanValue() && !this.postitialWasDisplayed) {
                        N();
                        resumeReportRewardTask();
                    }
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Setting window flags failed.", th);
                }
                this.n = false;
                b("javascript:al_onWindowFocusChanged( " + z2 + " );");
            }
            str = "InterActivity";
            str2 = "Window gained focus. SDK is null.";
        } else if (this.sdk != null) {
            this.logger.b("InterActivity", "Window lost focus");
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.cR)).booleanValue() && !this.postitialWasDisplayed) {
                M();
                pauseReportRewardTask();
            }
            this.n = false;
            b("javascript:al_onWindowFocusChanged( " + z2 + " );");
        } else {
            str = "InterActivity";
            str2 = "Window lost focus. SDK is null.";
        }
        com.applovin.impl.sdk.p.g(str, str2);
        this.n = false;
        b("javascript:al_onWindowFocusChanged( " + z2 + " );");
    }

    public void pauseReportRewardTask() {
        if (this.Q != null) {
            this.Q.b();
        }
    }

    /* access modifiers changed from: protected */
    public void playVideo() {
        d(this.currentAd);
        this.videoView.start();
        this.countdownManager.a();
    }

    public void resumeReportRewardTask() {
        if (this.Q != null) {
            this.Q.c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldContinueFullLengthVideoCountdown() {
        return !this.e && !this.postitialWasDisplayed;
    }

    public void showPostitial() {
        long j2;
        h hVar;
        try {
            if (this.d != null) {
                this.d.g();
            }
            if (!this.currentAd.af()) {
                K();
            }
            if (this.a != null) {
                ViewParent parent = this.a.getParent();
                if ((parent instanceof ViewGroup) && (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.fg)).booleanValue() || parent != this.D)) {
                    ((ViewGroup) parent).removeView(this.a);
                }
                FrameLayout frameLayout = ((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.fg)).booleanValue() ? this.D : new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                frameLayout.setBackgroundColor(this.currentAd.D());
                if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.fg)).booleanValue()) {
                    this.a.setVisibility(0);
                } else {
                    frameLayout.addView(this.a);
                }
                if (this.currentAd.af()) {
                    K();
                }
                if (this.D != null) {
                    if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.fg)).booleanValue()) {
                        com.applovin.impl.sdk.utils.b.a(this.D, this.a);
                    } else {
                        this.D.removeAllViewsInLayout();
                    }
                }
                if (n() && this.F != null) {
                    if (this.F.getParent() instanceof ViewGroup) {
                        ((ViewGroup) this.F.getParent()).removeView(this.F);
                    }
                    frameLayout.addView(this.F);
                    this.F.bringToFront();
                }
                if (this.E != null) {
                    ViewParent parent2 = this.E.getParent();
                    if (parent2 instanceof ViewGroup) {
                        ((ViewGroup) parent2).removeView(this.E);
                    }
                    frameLayout.addView(this.E);
                    this.E.bringToFront();
                }
                if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.fg)).booleanValue()) {
                    setContentView(frameLayout);
                }
                if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.d.eY)).booleanValue()) {
                    this.a.setVisibility(4);
                    this.a.setVisibility(0);
                }
                a("javascript:al_onPoststitialShow();", (long) this.currentAd.U());
            }
            if (!((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && ((com.applovin.impl.sdk.ad.a) this.currentAd).i())) {
                if (this.currentAd.n() >= 0.0f) {
                    j2 = q.b(this.currentAd.n());
                    hVar = this.E;
                } else if (this.currentAd.n() == -2.0f) {
                    this.E.setVisibility(0);
                } else {
                    j2 = 0;
                    hVar = this.E;
                }
                a(j2, hVar);
            } else {
                this.logger.b("InterActivity", "Skip showing of close button");
            }
            this.postitialWasDisplayed = true;
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Encountered error while showing postitial. Dismissing...", th);
            dismiss();
        }
    }

    public void skipVideo() {
        this.t = SystemClock.elapsedRealtime() - this.s;
        if (this.d != null) {
            this.d.f();
        }
        if (this.currentAd.q()) {
            dismiss();
        } else {
            showPostitial();
        }
    }

    public void toggleMute() {
        boolean z2 = !L();
        if (this.d != null) {
            this.d.i();
        }
        try {
            c(z2);
            a(z2);
            b(z2);
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to set volume to " + z2, th);
        }
    }
}
