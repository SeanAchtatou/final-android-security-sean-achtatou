package com.applovin.impl.adview;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import java.lang.ref.WeakReference;

public class v extends WebViewClient {
    private final p a;
    private WeakReference<a> b;

    public interface a {
        void a(u uVar);

        void b(u uVar);

        void c(u uVar);
    }

    public v(j jVar) {
        this.a = jVar.u();
    }

    private void a(WebView webView, String str) {
        p pVar = this.a;
        pVar.c("WebViewButtonClient", "Processing click on ad URL \"" + str + "\"");
        if (str != null && (webView instanceof u)) {
            u uVar = (u) webView;
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            a aVar = this.b.get();
            if ("applovin".equalsIgnoreCase(scheme) && "com.applovin.sdk".equalsIgnoreCase(host) && aVar != null) {
                if ("/track_click".equals(path)) {
                    aVar.a(uVar);
                } else if ("/close_ad".equals(path)) {
                    aVar.b(uVar);
                } else if ("/skip_ad".equals(path)) {
                    aVar.c(uVar);
                } else {
                    p pVar2 = this.a;
                    pVar2.d("WebViewButtonClient", "Unknown URL: " + str);
                    p pVar3 = this.a;
                    pVar3.d("WebViewButtonClient", "Path: " + path);
                }
            }
        }
    }

    public void a(WeakReference<a> weakReference) {
        this.b = weakReference;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
