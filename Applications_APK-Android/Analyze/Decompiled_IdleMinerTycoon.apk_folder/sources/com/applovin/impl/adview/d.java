package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.net.http.SslError;
import android.view.ViewParent;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.i;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinAd;
import com.helpshift.analytics.AnalyticsEventKey;
import com.tapjoy.TapjoyConstants;

class d extends WebViewClient {
    private final j a;
    private final p b;
    private final AdViewControllerImpl c;

    public d(AdViewControllerImpl adViewControllerImpl, j jVar) {
        this.a = jVar;
        this.b = jVar.u();
        this.c = adViewControllerImpl;
    }

    private void a() {
        this.c.a();
    }

    private void a(PointF pointF) {
        this.c.expandAd(pointF);
    }

    private void a(Uri uri, c cVar) {
        p pVar;
        String str;
        String str2;
        try {
            String queryParameter = uri.getQueryParameter(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT);
            if (n.b(queryParameter)) {
                String queryParameter2 = uri.getQueryParameter("load_type");
                if ("external".equalsIgnoreCase(queryParameter2)) {
                    p pVar2 = this.b;
                    pVar2.b("AdWebView", "Loading new page externally: " + queryParameter);
                    q.a(cVar.getContext(), Uri.parse(queryParameter), this.a);
                    com.applovin.impl.sdk.utils.j.c(this.c.getAdViewEventListener(), this.c.getCurrentAd(), this.c.getParentView());
                    return;
                } else if (TapjoyConstants.LOG_LEVEL_INTERNAL.equalsIgnoreCase(queryParameter2)) {
                    p pVar3 = this.b;
                    pVar3.b("AdWebView", "Loading new page in WebView: " + queryParameter);
                    cVar.loadUrl(queryParameter);
                    String queryParameter3 = uri.getQueryParameter("bg_color");
                    if (n.b(queryParameter3)) {
                        cVar.setBackgroundColor(Color.parseColor(queryParameter3));
                        return;
                    }
                    return;
                } else {
                    pVar = this.b;
                    str = "AdWebView";
                    str2 = "Could not find load type in original uri";
                }
            } else {
                pVar = this.b;
                str = "AdWebView";
                str2 = "Could not find url to load from query in original uri";
            }
            pVar.e(str, str2);
        } catch (Throwable unused) {
            this.b.e("AdWebView", "Failed to load new page from query in original uri");
        }
    }

    private void a(a aVar, c cVar) {
        b j = aVar.j();
        if (j != null) {
            i.a(j.c(), this.c.getSdk());
            a(cVar, j.a());
        }
    }

    private void a(c cVar) {
        ViewParent parent = cVar.getParent();
        if (parent instanceof AppLovinAdView) {
            ((AppLovinAdView) parent).loadNextAd();
        }
    }

    private void a(c cVar, Uri uri) {
        AppLovinAd b2 = cVar.b();
        AppLovinAdView parentView = this.c.getParentView();
        if (parentView == null || b2 == null) {
            p pVar = this.b;
            pVar.e("AdWebView", "Attempting to track click that is null or not an ApplovinAdView instance for clickedUri = " + uri);
            return;
        }
        com.applovin.impl.sdk.c.d c2 = cVar.c();
        if (c2 != null) {
            c2.b();
        }
        this.c.a(b2, parentView, uri, cVar.getAndClearLastClickLocation());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0176, code lost:
        if (r6.k() != false) goto L_0x00a9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(android.webkit.WebView r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            com.applovin.impl.sdk.p r0 = r8.b
            java.lang.String r1 = "AdWebView"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Processing click on ad URL \""
            r2.append(r3)
            r2.append(r10)
            java.lang.String r3 = "\""
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.c(r1, r2)
            r0 = 1
            if (r10 == 0) goto L_0x018a
            boolean r1 = r9 instanceof com.applovin.impl.adview.c
            if (r1 == 0) goto L_0x018a
            android.net.Uri r1 = android.net.Uri.parse(r10)
            r2 = r9
            com.applovin.impl.adview.c r2 = (com.applovin.impl.adview.c) r2
            java.lang.String r3 = r1.getScheme()
            java.lang.String r4 = r1.getHost()
            java.lang.String r5 = r1.getPath()
            com.applovin.impl.adview.AdViewControllerImpl r6 = r8.c
            com.applovin.sdk.AppLovinAd r6 = r6.getCurrentAd()
            java.lang.String r7 = "applovin"
            boolean r7 = r7.equals(r3)
            if (r7 == 0) goto L_0x013b
            java.lang.String r7 = "com.applovin.sdk"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x013b
            java.lang.String r11 = "/adservice/next_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x005a
            r8.a(r2)
            goto L_0x018a
        L_0x005a:
            java.lang.String r11 = "/adservice/close_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0067
            r8.a()
            goto L_0x018a
        L_0x0067:
            java.lang.String r11 = "/adservice/expand_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0078
            android.graphics.PointF r9 = r2.getAndClearLastClickLocation()
            r8.a(r9)
            goto L_0x018a
        L_0x0078:
            java.lang.String r11 = "/adservice/contract_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0085
            r8.b()
            goto L_0x018a
        L_0x0085:
            java.lang.String r11 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_NO_OP
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x008e
            return r0
        L_0x008e:
            java.lang.String r11 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_LOAD_URL
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x009b
            r8.a(r1, r2)
            goto L_0x018a
        L_0x009b:
            java.lang.String r11 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_TRACK_CLICK_IMMEDIATELY
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x00b9
            boolean r9 = r6 instanceof com.applovin.impl.a.a
            if (r9 == 0) goto L_0x00ae
            com.applovin.impl.a.a r6 = (com.applovin.impl.a.a) r6
        L_0x00a9:
            r8.a(r6, r2)
            goto L_0x018a
        L_0x00ae:
            java.lang.String r9 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_TRACK_CLICK_IMMEDIATELY
            android.net.Uri r9 = android.net.Uri.parse(r9)
            r8.a(r2, r9)
            goto L_0x018a
        L_0x00b9:
            if (r5 == 0) goto L_0x010a
            java.lang.String r11 = "/launch/"
            boolean r11 = r5.startsWith(r11)
            if (r11 == 0) goto L_0x010a
            java.util.List r10 = r1.getPathSegments()
            if (r10 == 0) goto L_0x018a
            int r11 = r10.size()
            if (r11 <= r0) goto L_0x018a
            int r11 = r10.size()
            int r11 = r11 - r0
            java.lang.Object r10 = r10.get(r11)
            java.lang.String r10 = (java.lang.String) r10
            android.content.Context r9 = r9.getContext()     // Catch:{ Throwable -> 0x00ef }
            android.content.pm.PackageManager r11 = r9.getPackageManager()     // Catch:{ Throwable -> 0x00ef }
            android.content.Intent r11 = r11.getLaunchIntentForPackage(r10)     // Catch:{ Throwable -> 0x00ef }
            r9.startActivity(r11)     // Catch:{ Throwable -> 0x00ef }
            r9 = 0
            r8.a(r2, r9)     // Catch:{ Throwable -> 0x00ef }
            goto L_0x018a
        L_0x00ef:
            r9 = move-exception
            com.applovin.impl.sdk.p r11 = r8.b
            java.lang.String r1 = "AdWebView"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Threw Exception Trying to Launch App for Package: "
            r2.append(r3)
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r11.b(r1, r10, r9)
            goto L_0x018a
        L_0x010a:
            com.applovin.impl.sdk.p r9 = r8.b
            java.lang.String r11 = "AdWebView"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown URL: "
            r1.append(r2)
            r1.append(r10)
            java.lang.String r10 = r1.toString()
            r9.d(r11, r10)
            com.applovin.impl.sdk.p r9 = r8.b
            java.lang.String r10 = "AdWebView"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r1 = "Path: "
            r11.append(r1)
            r11.append(r5)
            java.lang.String r11 = r11.toString()
            r9.d(r10, r11)
            goto L_0x018a
        L_0x013b:
            if (r11 == 0) goto L_0x0188
            boolean r9 = r6 instanceof com.applovin.impl.sdk.ad.f
            if (r9 == 0) goto L_0x017e
            r9 = r6
            com.applovin.impl.sdk.ad.f r9 = (com.applovin.impl.sdk.ad.f) r9
            java.util.List r10 = r9.aA()
            java.util.List r9 = r9.aB()
            boolean r11 = r10.isEmpty()
            if (r11 != 0) goto L_0x0158
            boolean r10 = r10.contains(r3)
            if (r10 == 0) goto L_0x0165
        L_0x0158:
            boolean r10 = r9.isEmpty()
            if (r10 != 0) goto L_0x016c
            boolean r9 = r9.contains(r4)
            if (r9 == 0) goto L_0x0165
            goto L_0x016c
        L_0x0165:
            com.applovin.impl.sdk.p r9 = r8.b
            java.lang.String r10 = "AdWebView"
            java.lang.String r11 = "URL is not whitelisted - bypassing click"
            goto L_0x0184
        L_0x016c:
            boolean r9 = r6 instanceof com.applovin.impl.a.a
            if (r9 == 0) goto L_0x017a
            com.applovin.impl.a.a r6 = (com.applovin.impl.a.a) r6
            boolean r9 = r6.k()
            if (r9 == 0) goto L_0x017a
            goto L_0x00a9
        L_0x017a:
            r8.a(r2, r1)
            goto L_0x018a
        L_0x017e:
            com.applovin.impl.sdk.p r9 = r8.b
            java.lang.String r10 = "AdWebView"
            java.lang.String r11 = "Bypassing click for ad of invalid type"
        L_0x0184:
            r9.e(r10, r11)
            goto L_0x018a
        L_0x0188:
            r9 = 0
            return r9
        L_0x018a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.d.a(android.webkit.WebView, java.lang.String, boolean):boolean");
    }

    private void b() {
        this.c.contractAd();
    }

    public void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
        p pVar = this.b;
        pVar.c("AdWebView", "Loaded resource: " + str);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.c.onAdHtmlLoaded(webView);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        AppLovinAd currentAd = this.c.getCurrentAd();
        String str3 = "Received error with error code: " + i + " with description \\'" + str + "\\' for URL: " + str2;
        if (currentAd instanceof AppLovinAdBase) {
            this.a.W().a((AppLovinAdBase) currentAd).a(com.applovin.impl.sdk.c.b.C, str3).a();
        }
        this.b.e("AdWebView", str3 + " for ad: " + currentAd);
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        onReceivedError(webView, webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
    }

    @TargetApi(21)
    public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
        super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
        AppLovinAd currentAd = this.c.getCurrentAd();
        if (currentAd instanceof AppLovinAdBase) {
            this.a.W().a((AppLovinAdBase) currentAd).a(com.applovin.impl.sdk.c.b.D).a();
        }
        p pVar = this.b;
        pVar.e("AdWebView", "Received HTTP error: " + webResourceResponse + "for url: " + webResourceRequest.getUrl() + " and ad: " + currentAd);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        AppLovinAd currentAd = this.c.getCurrentAd();
        String str = "Received SSL error: " + sslError;
        if (currentAd instanceof AppLovinAdBase) {
            this.a.W().a((AppLovinAdBase) currentAd).a(com.applovin.impl.sdk.c.b.E, str).a();
        }
        this.b.e("AdWebView", str + " for ad: " + currentAd);
    }

    @TargetApi(24)
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        boolean hasGesture = ((Boolean) this.a.a(com.applovin.impl.sdk.b.d.cb)).booleanValue() ? webResourceRequest.hasGesture() : true;
        Uri url = webResourceRequest.getUrl();
        if (url != null) {
            return a(webView, url.toString(), hasGesture);
        }
        this.b.e("AdWebView", "No url found for request");
        return false;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return a(webView, str, true);
    }
}
