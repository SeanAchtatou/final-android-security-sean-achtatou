package com.applovin.impl.adview;

import android.content.Context;
import android.view.View;
import com.applovin.impl.sdk.j;

public abstract class h extends View {
    protected final j a;
    protected final Context b;

    public enum a {
        WhiteXOnOpaqueBlack(0),
        WhiteXOnTransparentGrey(1),
        Invisible(2);
        
        private final int d;

        private a(int i) {
            this.d = i;
        }

        public int a() {
            return this.d;
        }
    }

    h(j jVar, Context context) {
        super(context);
        this.b = context;
        this.a = jVar;
    }

    public static h a(j jVar, Context context, a aVar) {
        return aVar.equals(a.Invisible) ? new o(jVar, context) : aVar.equals(a.WhiteXOnTransparentGrey) ? new q(jVar, context) : new x(jVar, context);
    }

    public abstract void a(int i);

    public abstract a getStyle();

    public abstract float getViewScale();

    public abstract void setViewScale(float f);
}
