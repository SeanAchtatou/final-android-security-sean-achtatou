package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.webkit.WebView;
import android.webkit.WebViewRenderProcess;
import android.webkit.WebViewRenderProcessClient;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.sdk.AppLovinAd;

@TargetApi(29)
class e {
    /* access modifiers changed from: private */
    public final j a;
    private final WebViewRenderProcessClient b = new WebViewRenderProcessClient() {
        public void onRenderProcessResponsive(WebView webView, WebViewRenderProcess webViewRenderProcess) {
        }

        public void onRenderProcessUnresponsive(WebView webView, WebViewRenderProcess webViewRenderProcess) {
            if (webView instanceof c) {
                AppLovinAd b = ((c) webView).b();
                if (b instanceof AppLovinAdBase) {
                    e.this.a.W().a((AppLovinAdBase) b).a(b.F).a();
                }
                p u = e.this.a.u();
                u.e("AdWebViewRenderProcessClient", "WebView render process unresponsive for ad: " + b);
            }
        }
    };

    e(j jVar) {
        this.a = jVar;
    }

    /* access modifiers changed from: package-private */
    public WebViewRenderProcessClient a() {
        return this.b;
    }
}
