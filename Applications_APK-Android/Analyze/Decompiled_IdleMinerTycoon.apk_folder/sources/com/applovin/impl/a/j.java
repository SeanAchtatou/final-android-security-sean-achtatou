package com.applovin.impl.a;

import android.annotation.TargetApi;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.s;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class j {
    private List<k> a = Collections.EMPTY_LIST;
    private List<String> b = Collections.EMPTY_LIST;
    private int c;
    private Uri d;
    private final Set<g> e = new HashSet();
    private final Map<String, Set<g>> f = new HashMap();

    public enum a {
        UNSPECIFIED,
        LOW,
        MEDIUM,
        HIGH
    }

    private j() {
    }

    private j(c cVar) {
        this.b = cVar.h();
    }

    private static int a(String str, com.applovin.impl.sdk.j jVar) {
        try {
            List<String> a2 = e.a(str, ":");
            if (a2.size() == 3) {
                return (int) (TimeUnit.HOURS.toSeconds((long) n.a(a2.get(0))) + TimeUnit.MINUTES.toSeconds((long) n.a(a2.get(1))) + ((long) n.a(a2.get(2))));
            }
        } catch (Throwable unused) {
            p u = jVar.u();
            u.e("VastVideoCreative", "Unable to parse duration from \"" + str + "\"");
        }
        return 0;
    }

    public static j a(s sVar, j jVar, c cVar, com.applovin.impl.sdk.j jVar2) {
        s b2;
        List<k> a2;
        s b3;
        int a3;
        if (sVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No context specified.");
        } else if (jVar2 != null) {
            if (jVar == null) {
                try {
                    jVar = new j(cVar);
                } catch (Throwable th) {
                    jVar2.u().b("VastVideoCreative", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (jVar.c == 0 && (b3 = sVar.b("Duration")) != null && (a3 = a(b3.c(), jVar2)) > 0) {
                jVar.c = a3;
            }
            s b4 = sVar.b("MediaFiles");
            if (!(b4 == null || (a2 = a(b4, jVar2)) == null || a2.size() <= 0)) {
                if (jVar.a != null) {
                    a2.addAll(jVar.a);
                }
                jVar.a = a2;
            }
            s b5 = sVar.b("VideoClicks");
            if (b5 != null) {
                if (jVar.d == null && (b2 = b5.b("ClickThrough")) != null) {
                    String c2 = b2.c();
                    if (n.b(c2)) {
                        jVar.d = Uri.parse(c2);
                    }
                }
                i.a(b5.a("ClickTracking"), jVar.e, cVar, jVar2);
            }
            i.a(sVar, jVar.f, cVar, jVar2);
            return jVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static List<k> a(s sVar, com.applovin.impl.sdk.j jVar) {
        List<s> a2 = sVar.a("MediaFile");
        ArrayList arrayList = new ArrayList(a2.size());
        List<String> a3 = e.a((String) jVar.a(d.eM));
        List<String> a4 = e.a((String) jVar.a(d.eL));
        for (s a5 : a2) {
            k a6 = k.a(a5, jVar);
            if (a6 != null) {
                try {
                    String d2 = a6.d();
                    if (!n.b(d2) || a3.contains(d2)) {
                        if (((Boolean) jVar.a(d.eN)).booleanValue()) {
                            String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(a6.b().toString());
                            if (n.b(fileExtensionFromUrl) && !a4.contains(fileExtensionFromUrl)) {
                            }
                        }
                        p u = jVar.u();
                        u.d("VastVideoCreative", "Video file not supported: " + a6);
                    }
                    arrayList.add(a6);
                } catch (Throwable th) {
                    p u2 = jVar.u();
                    u2.b("VastVideoCreative", "Failed to validate vidoe file: " + a6, th);
                }
            }
        }
        return arrayList;
    }

    public k a(a aVar) {
        if (this.a == null || this.a.size() == 0) {
            return null;
        }
        List arrayList = new ArrayList(3);
        for (String next : this.b) {
            for (k next2 : this.a) {
                String d2 = next2.d();
                if (n.b(d2) && next.equalsIgnoreCase(d2)) {
                    arrayList.add(next2);
                }
            }
            if (!arrayList.isEmpty()) {
                break;
            }
        }
        if (arrayList.isEmpty()) {
            arrayList = this.a;
        }
        if (g.c()) {
            Collections.sort(arrayList, new Comparator<k>() {
                @TargetApi(19)
                /* renamed from: a */
                public int compare(k kVar, k kVar2) {
                    return Integer.compare(kVar.e(), kVar2.e());
                }
            });
        }
        return (k) arrayList.get(aVar == a.LOW ? 0 : aVar == a.MEDIUM ? arrayList.size() / 2 : arrayList.size() - 1);
    }

    public List<k> a() {
        return this.a;
    }

    public int b() {
        return this.c;
    }

    public Uri c() {
        return this.d;
    }

    public Set<g> d() {
        return this.e;
    }

    public Map<String, Set<g>> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        if (this.c != jVar.c) {
            return false;
        }
        if (this.a == null ? jVar.a != null : !this.a.equals(jVar.a)) {
            return false;
        }
        if (this.d == null ? jVar.d != null : !this.d.equals(jVar.d)) {
            return false;
        }
        if (this.e == null ? jVar.e == null : this.e.equals(jVar.e)) {
            return this.f != null ? this.f.equals(jVar.f) : jVar.f == null;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.a != null ? this.a.hashCode() : 0) * 31) + this.c) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31) + (this.e != null ? this.e.hashCode() : 0)) * 31;
        if (this.f != null) {
            i = this.f.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "VastVideoCreative{videoFiles=" + this.a + ", durationSeconds=" + this.c + ", destinationUri=" + this.d + ", clickTrackers=" + this.e + ", eventTrackers=" + this.f + '}';
    }
}
