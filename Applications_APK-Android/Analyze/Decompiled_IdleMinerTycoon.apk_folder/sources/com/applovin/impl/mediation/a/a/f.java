package com.applovin.impl.mediation.a.a;

import android.content.Context;
import com.applovin.impl.sdk.utils.g;

public class f {
    private final String a;
    private final String b;
    private final boolean c;

    f(String str, String str2, Context context) {
        this.a = str;
        this.b = str2;
        this.c = g.a(str, context);
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }
}
