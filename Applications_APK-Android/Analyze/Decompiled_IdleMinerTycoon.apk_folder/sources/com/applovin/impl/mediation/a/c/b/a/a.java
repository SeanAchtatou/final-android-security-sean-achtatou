package com.applovin.impl.mediation.a.c.b.a;

import android.text.SpannedString;
import com.applovin.impl.mediation.a.a.c;

public class a extends c {
    final String d;
    final int e;
    final int f;
    final boolean g;

    /* renamed from: com.applovin.impl.mediation.a.c.b.a.a$a  reason: collision with other inner class name */
    public static class C0003a {
        SpannedString a;
        SpannedString b;
        String c;
        c.a d = c.a.DETAIL;
        int e;
        int f;
        boolean g = false;

        public C0003a a(int i) {
            this.e = i;
            return this;
        }

        public C0003a a(SpannedString spannedString) {
            this.b = spannedString;
            return this;
        }

        public C0003a a(c.a aVar) {
            this.d = aVar;
            return this;
        }

        public C0003a a(String str) {
            this.a = new SpannedString(str);
            return this;
        }

        public C0003a a(boolean z) {
            this.g = z;
            return this;
        }

        public a a() {
            return new a(this);
        }

        public C0003a b(int i) {
            this.f = i;
            return this;
        }

        public C0003a b(String str) {
            return a(new SpannedString(str));
        }

        public C0003a c(String str) {
            this.c = str;
            return this;
        }
    }

    private a(C0003a aVar) {
        super(aVar.d);
        this.b = aVar.a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
    }

    public static C0003a j() {
        return new C0003a();
    }

    public boolean b() {
        return this.g;
    }

    public int g() {
        return this.e;
    }

    public int h() {
        return this.f;
    }

    public String i() {
        return this.d;
    }

    public String toString() {
        return "NetworkDetailListItemViewModel{text=" + ((Object) this.b) + ", detailText=" + ((Object) this.b) + "}";
    }
}
