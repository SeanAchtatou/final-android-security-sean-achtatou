package com.applovin.impl.mediation.a.b;

import android.os.Build;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */
    public final a.c<JSONObject> a;

    public a(a.c<JSONObject> cVar, j jVar) {
        super("TaskFetchMediationDebuggerInfo", jVar, true);
        this.a = cVar;
    }

    public i a() {
        return i.J;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", String.valueOf(131));
        if (!((Boolean) this.b.a(d.eR)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.s());
        }
        k.b c = this.b.N().c();
        hashMap.put(CampaignEx.JSON_KEY_PACKAGE_NAME, n.e(c.c));
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, n.e(c.b));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
        hashMap.put("os", n.e(Build.VERSION.RELEASE));
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new x<JSONObject>(b.a(this.b).a(com.applovin.impl.mediation.d.b.c(this.b)).c(com.applovin.impl.mediation.d.b.d(this.b)).a(b()).b(HttpRequest.METHOD_GET).a((Object) new JSONObject()).b(((Long) this.b.a(c.g)).intValue()).a(), this.b, h()) {
            public void a(int i) {
                a.this.a.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                a.this.a.a(jSONObject, i);
            }
        };
        r1.a(c.c);
        r1.b(c.d);
        this.b.J().a(r1);
    }
}
