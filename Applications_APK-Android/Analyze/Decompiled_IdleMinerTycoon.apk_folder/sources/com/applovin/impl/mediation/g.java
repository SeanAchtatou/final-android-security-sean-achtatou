package com.applovin.impl.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.c.a;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.mediation.adapter.MaxAdapter;
import com.facebook.internal.AnalyticsEvents;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class g {
    private final j a;
    private final p b;
    private final AtomicBoolean c = new AtomicBoolean();
    private final JSONArray d = new JSONArray();
    private final LinkedHashSet<String> e = new LinkedHashSet<>();
    private final Object f = new Object();

    public g(j jVar) {
        this.a = jVar;
        this.b = jVar.u();
    }

    public void a(Activity activity) {
        if (this.c.compareAndSet(false, true)) {
            this.a.J().a(new a(activity, this.a));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, long j, MaxAdapter.InitializationStatus initializationStatus, String str) {
        boolean z;
        if (initializationStatus != null && initializationStatus != MaxAdapter.InitializationStatus.INITIALIZING) {
            synchronized (this.f) {
                z = !a(eVar);
                if (z) {
                    this.e.add(eVar.C());
                    JSONObject jSONObject = new JSONObject();
                    i.a(jSONObject, "class", eVar.C(), this.a);
                    i.a(jSONObject, "init_status", String.valueOf(initializationStatus.getCode()), this.a);
                    i.a(jSONObject, AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, JSONObject.quote(str), this.a);
                    this.d.put(jSONObject);
                }
            }
            if (z) {
                this.a.a(eVar);
                this.a.x().maybeScheduleAdapterInitializationPostback(eVar, j, initializationStatus, str);
            }
        }
    }

    public void a(e eVar, Activity activity) {
        i a2 = this.a.v().a(eVar);
        if (a2 != null) {
            p pVar = this.b;
            pVar.c("MediationAdapterInitializationManager", "Initializing adapter " + eVar);
            a2.a(MaxAdapterParametersImpl.a(eVar, activity.getApplicationContext()), activity);
        }
    }

    public boolean a() {
        return this.c.get();
    }

    /* access modifiers changed from: package-private */
    public boolean a(e eVar) {
        boolean contains;
        synchronized (this.f) {
            contains = this.e.contains(eVar.C());
        }
        return contains;
    }

    public LinkedHashSet<String> b() {
        LinkedHashSet<String> linkedHashSet;
        synchronized (this.f) {
            linkedHashSet = this.e;
        }
        return linkedHashSet;
    }

    public JSONArray c() {
        JSONArray jSONArray;
        synchronized (this.f) {
            jSONArray = this.d;
        }
        return jSONArray;
    }
}
