package com.applovin.impl.mediation;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;

public class l implements MaxAd {
    private final String a;
    private final MaxAdFormat b;

    public l(String str, MaxAdFormat maxAdFormat) {
        this.a = str;
        this.b = maxAdFormat;
    }

    public String getAdUnitId() {
        return this.a;
    }

    public MaxAdFormat getFormat() {
        return this.b;
    }
}
