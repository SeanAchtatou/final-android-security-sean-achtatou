package com.applovin.impl.mediation.c;

import android.app.Activity;
import android.graphics.Point;
import com.applovin.impl.mediation.d.b;
import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.helpshift.analytics.AnalyticsEventKey;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import im.getsocial.sdk.consts.LanguageCodes;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c extends a {
    private final String a;
    private final MaxAdFormat c;
    private final f d;
    private final JSONArray e;
    private final Activity f;
    private final MaxAdListener g;

    public c(String str, MaxAdFormat maxAdFormat, f fVar, JSONArray jSONArray, Activity activity, j jVar, MaxAdListener maxAdListener) {
        super("TaskFetchMediatedAd " + str, jVar);
        this.a = str;
        this.c = maxAdFormat;
        this.d = fVar;
        this.e = jSONArray;
        this.f = activity;
        this.g = maxAdListener;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        boolean z = i != 204;
        p u = this.b.u();
        String f2 = f();
        Boolean valueOf = Boolean.valueOf(z);
        u.a(f2, valueOf, "Unable to fetch " + this.a + " ad: server returned " + i);
        if (i == -800) {
            this.b.K().a(g.n);
        }
        b(i);
    }

    private void a(h hVar) {
        long b = hVar.b(g.c);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.b.a(d.dM)).intValue())) {
            hVar.b(g.c, currentTimeMillis);
            hVar.c(g.d);
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            com.applovin.impl.sdk.utils.h.d(jSONObject, this.b);
            com.applovin.impl.sdk.utils.h.c(jSONObject, this.b);
            com.applovin.impl.sdk.utils.h.f(jSONObject, this.b);
            b.a(jSONObject, this.b);
            b.b(jSONObject, this.b);
            this.b.J().a(b(jSONObject));
        } catch (Throwable th) {
            a("Unable to process mediated ad response", th);
            b(-800);
        }
    }

    private f b(JSONObject jSONObject) {
        return new f(this.a, this.c, jSONObject, this.f, this.b, this.g);
    }

    private String b() {
        return b.a(this.b);
    }

    private void b(int i) {
        com.applovin.impl.sdk.utils.j.a(this.g, this.a, i);
    }

    private String c() {
        return b.b(this.b);
    }

    private void c(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(Constants.ParametersKeys.LOADED, new JSONArray((Collection) this.b.v().a()));
            jSONObject2.put(Constants.ParametersKeys.FAILED, new JSONArray((Collection) this.b.v().b()));
            jSONObject.put("classname_info", jSONObject2);
            jSONObject.put("initialized_adapters", this.b.w().c());
            jSONObject.put("initialized_adapter_classnames", new JSONArray((Collection) this.b.w().b()));
            jSONObject.put("installed_mediation_adapters", com.applovin.impl.mediation.d.c.a(this.b).a());
        } catch (Exception e2) {
            a("Failed to populate adapter classnames", e2);
        }
    }

    private JSONObject d() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        e(jSONObject);
        f(jSONObject);
        d(jSONObject);
        c(jSONObject);
        jSONObject.put("sc", n.e((String) this.b.a(d.aa)));
        jSONObject.put("sc2", n.e((String) this.b.a(d.ab)));
        jSONObject.put("server_installed_at", n.e((String) this.b.a(d.ac)));
        String str = (String) this.b.a(com.applovin.impl.sdk.b.f.z);
        if (n.b(str)) {
            jSONObject.put("persisted_data", n.e(str));
        }
        if (((Boolean) this.b.a(d.ex)).booleanValue()) {
            h(jSONObject);
        }
        jSONObject.put("mediation_provider", this.b.m());
        return jSONObject;
    }

    private void d(JSONObject jSONObject) throws JSONException {
        if (this.e != null) {
            jSONObject.put("signal_data", this.e);
        }
    }

    private void e(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("ad_unit_id", this.a);
        jSONObject2.put("ad_format", com.applovin.impl.mediation.d.c.b(this.c));
        if (this.d != null && ((Boolean) this.b.a(com.applovin.impl.sdk.b.c.h)).booleanValue()) {
            jSONObject2.put("extra_parameters", i.a((Map<String, ?>) i.a(this.d.a())));
        }
        if (((Boolean) this.b.a(d.ad)).booleanValue()) {
            jSONObject2.put(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT, String.valueOf(this.b.aa().a(this.a)));
        }
        jSONObject.put("ad_info", jSONObject2);
    }

    private void f(JSONObject jSONObject) throws JSONException {
        k N = this.b.N();
        k.d b = N.b();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("brand", b.d);
        jSONObject2.put("brand_name", b.e);
        jSONObject2.put("hardware", b.f);
        jSONObject2.put("api_level", b.h);
        jSONObject2.put("carrier", b.j);
        jSONObject2.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b.i);
        jSONObject2.put("locale", b.k);
        jSONObject2.put("model", b.a);
        jSONObject2.put("os", b.b);
        jSONObject2.put(TapjoyConstants.TJC_PLATFORM, b.c);
        jSONObject2.put("revision", b.g);
        jSONObject2.put("orientation_lock", b.l);
        jSONObject2.put("tz_offset", b.r);
        jSONObject2.put("aida", n.a(b.N));
        jSONObject2.put("wvvc", b.s);
        jSONObject2.put("adns", (double) b.m);
        jSONObject2.put("adnsd", b.n);
        jSONObject2.put("xdpi", (double) b.o);
        jSONObject2.put("ydpi", (double) b.p);
        jSONObject2.put("screen_size_in", b.q);
        jSONObject2.put("sim", n.a(b.A));
        jSONObject2.put("gy", n.a(b.B));
        jSONObject2.put("is_tablet", n.a(b.C));
        jSONObject2.put("tv", n.a(b.D));
        jSONObject2.put("vs", n.a(b.E));
        jSONObject2.put("lpm", b.F);
        jSONObject2.put("fs", b.H);
        jSONObject2.put("tds", b.I);
        jSONObject2.put("fm", b.J.b);
        jSONObject2.put("tm", b.J.a);
        jSONObject2.put("lmt", b.J.c);
        jSONObject2.put("lm", b.J.d);
        jSONObject2.put("adr", n.a(b.t));
        jSONObject2.put("volume", b.x);
        jSONObject2.put("sb", b.y);
        jSONObject2.put("network", com.applovin.impl.sdk.utils.h.f(this.b));
        jSONObject2.put("af", b.v);
        jSONObject2.put("font", (double) b.w);
        if (n.b(b.z)) {
            jSONObject2.put("ua", b.z);
        }
        if (n.b(b.G)) {
            jSONObject2.put("so", b.G);
        }
        jSONObject2.put("bt_ms", String.valueOf(b.Q));
        k.c cVar = b.u;
        if (cVar != null) {
            jSONObject2.put("act", cVar.a);
            jSONObject2.put("acm", cVar.b);
        }
        Boolean bool = b.K;
        if (bool != null) {
            jSONObject2.put("huc", bool.toString());
        }
        Boolean bool2 = b.L;
        if (bool2 != null) {
            jSONObject2.put("aru", bool2.toString());
        }
        Boolean bool3 = b.M;
        if (bool3 != null) {
            jSONObject2.put("dns", bool3.toString());
        }
        Point a2 = com.applovin.impl.sdk.utils.g.a(g());
        jSONObject2.put("dx", Integer.toString(a2.x));
        jSONObject2.put("dy", Integer.toString(a2.y));
        if (b.O > 0.0f) {
            jSONObject2.put(LanguageCodes.DANISH, (double) b.O);
        }
        if (b.P > 0.0f) {
            jSONObject2.put("dm", (double) b.P);
        }
        g(jSONObject2);
        jSONObject.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, jSONObject2);
        k.b c2 = N.c();
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put(CampaignEx.JSON_KEY_PACKAGE_NAME, c2.c);
        jSONObject3.put("installer_name", c2.d);
        jSONObject3.put("app_name", c2.a);
        jSONObject3.put(TapjoyConstants.TJC_APP_VERSION_NAME, c2.b);
        jSONObject3.put("installed_at", c2.h);
        jSONObject3.put("tg", c2.e);
        jSONObject3.put("ltg", c2.f);
        jSONObject3.put("api_did", this.b.a(d.X));
        jSONObject3.put("sdk_version", AppLovinSdk.VERSION);
        jSONObject3.put("build", 131);
        jSONObject3.put("first_install", String.valueOf(this.b.G()));
        jSONObject3.put("first_install_v2", String.valueOf(!this.b.H()));
        jSONObject3.put("debug", Boolean.toString(q.b(this.b)));
        String h = this.b.h();
        if (((Boolean) this.b.a(d.dT)).booleanValue() && n.b(h)) {
            jSONObject3.put("cuid", h);
        }
        if (((Boolean) this.b.a(d.dW)).booleanValue()) {
            jSONObject3.put("compass_random_token", this.b.i());
        }
        if (((Boolean) this.b.a(d.dY)).booleanValue()) {
            jSONObject3.put("applovin_random_token", this.b.j());
        }
        String str = (String) this.b.a(d.ea);
        if (n.b(str)) {
            jSONObject3.put("plugin_version", str);
        }
        jSONObject.put("app_info", jSONObject3);
        a.b a3 = this.b.I().a();
        if (a3 != null) {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("lrm_ts_ms", String.valueOf(a3.a()));
            jSONObject4.put("lrm_url", a3.b());
            jSONObject4.put("lrm_ct_ms", String.valueOf(a3.d()));
            jSONObject4.put("lrm_rs", String.valueOf(a3.c()));
            jSONObject.put("connection_info", jSONObject4);
        }
    }

    private void g(JSONObject jSONObject) {
        try {
            k.a d2 = this.b.N().d();
            String str = d2.b;
            if (n.b(str)) {
                jSONObject.put("idfa", str);
            }
            jSONObject.put("dnt", d2.a);
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    private void h(JSONObject jSONObject) {
        try {
            h K = this.b.K();
            jSONObject.put("li", String.valueOf(K.b(g.b)));
            jSONObject.put("si", String.valueOf(K.b(g.d)));
            jSONObject.put("pf", String.valueOf(K.b(g.h)));
            jSONObject.put("mpf", String.valueOf(K.b(g.n)));
            jSONObject.put("gpf", String.valueOf(K.b(g.i)));
        } catch (Throwable th) {
            a("Failed to populate ad serving info", th);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.C;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.j):java.lang.Boolean
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.j]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.j):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.j):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.j):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.j):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.j):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.j):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.j):java.lang.Boolean */
    public void run() {
        a("Fetching next ad for ad unit id: " + this.a + " and format: " + this.c);
        if (((Boolean) this.b.a(d.eh)).booleanValue() && q.d()) {
            a("User is connected to a VPN");
        }
        h K = this.b.K();
        K.a(g.m);
        if (K.b(g.c) == 0) {
            K.b(g.c, System.currentTimeMillis());
        }
        try {
            JSONObject d2 = d();
            HashMap hashMap = new HashMap();
            hashMap.put("rid", UUID.randomUUID().toString());
            if (d2.has("huc")) {
                hashMap.put("huc", String.valueOf(i.a(d2, "huc", (Boolean) false, this.b)));
            }
            if (d2.has("aru")) {
                hashMap.put("aru", String.valueOf(i.a(d2, "aru", (Boolean) false, this.b)));
            }
            if (d2.has("dns")) {
                hashMap.put("dns", String.valueOf(i.a(d2, "dns", (Boolean) false, this.b)));
            }
            if (!((Boolean) this.b.a(d.eR)).booleanValue()) {
                hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.s());
            }
            a(K);
            AnonymousClass1 r2 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).b(HttpRequest.METHOD_POST).a(b()).c(c()).a((Map<String, String>) hashMap).a(d2).a((Object) new JSONObject()).b(((Long) this.b.a(com.applovin.impl.sdk.b.c.f)).intValue()).a(((Integer) this.b.a(d.dB)).intValue()).c(((Long) this.b.a(com.applovin.impl.sdk.b.c.e)).intValue()).b(true).a(), this.b) {
                public void a(int i) {
                    c.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        i.b(jSONObject, "ad_fetch_latency_millis", this.d.a(), this.b);
                        i.b(jSONObject, "ad_fetch_response_size", this.d.b(), this.b);
                        c.this.a(jSONObject);
                        return;
                    }
                    c.this.a(i);
                }
            };
            r2.a(com.applovin.impl.sdk.b.c.c);
            r2.b(com.applovin.impl.sdk.b.c.d);
            this.b.J().a(r2);
        } catch (Throwable th) {
            a("Unable to fetch ad " + this.a, th);
            a(0);
            this.b.L().a(a());
        }
    }
}
