package com.applovin.impl.mediation.d;

import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import org.json.JSONObject;

public class b extends h {
    public static String a(j jVar) {
        return h.a((String) jVar.a(c.c), "1.0/mediate", jVar);
    }

    public static void a(JSONObject jSONObject, j jVar) {
        if (i.a(jSONObject, "signal_providers")) {
            jVar.a(f.x, jSONObject.toString());
        }
    }

    public static String b(j jVar) {
        return h.a((String) jVar.a(c.d), "1.0/mediate", jVar);
    }

    public static void b(JSONObject jSONObject, j jVar) {
        if (i.a(jSONObject, "auto_init_adapters")) {
            jVar.a(f.y, jSONObject.toString());
        }
    }

    public static String c(j jVar) {
        return h.a((String) jVar.a(c.c), "1.0/mediate_debug", jVar);
    }

    public static String d(j jVar) {
        return h.a((String) jVar.a(c.d), "1.0/mediate_debug", jVar);
    }
}
