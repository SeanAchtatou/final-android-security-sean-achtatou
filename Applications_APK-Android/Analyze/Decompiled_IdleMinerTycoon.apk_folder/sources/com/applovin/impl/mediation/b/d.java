package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.j;
import org.json.JSONObject;

public class d extends a {
    private d(d dVar, i iVar) {
        super(dVar.B(), dVar.A(), iVar, dVar.b);
    }

    public d(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        super(jSONObject, jSONObject2, null, jVar);
    }

    public a a(i iVar) {
        return new d(this, iVar);
    }

    public String toString() {
        return "MediatedNativeAd{format=" + getFormat() + ", adUnitId=" + getAdUnitId() + ", isReady=" + a() + ", adapterClass='" + C() + "', adapterName='" + D() + "', isTesting=" + E() + ", isRefreshEnabled=" + I() + ", getAdRefreshMillis=" + J() + '}';
    }
}
