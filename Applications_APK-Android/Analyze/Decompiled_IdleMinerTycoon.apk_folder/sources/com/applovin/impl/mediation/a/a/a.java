package com.applovin.impl.mediation.a.a;

import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.q;
import org.json.JSONObject;

public class a {
    private final String a;
    private final String b;
    private final boolean c;

    a(JSONObject jSONObject, j jVar) {
        this.a = i.b(jSONObject, "name", "", jVar);
        this.b = i.b(jSONObject, "description", "", jVar);
        this.c = q.e(i.b(jSONObject, "existence_class", "", jVar));
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }
}
