package com.applovin.impl.mediation.c;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.b.f;
import com.applovin.impl.mediation.b.g;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.c.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.n;
import com.applovin.mediation.MaxAdFormat;
import com.facebook.internal.AnalyticsEvents;
import com.helpshift.common.domain.network.NetworkConstants;
import com.tapjoy.TapjoyConstants;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b extends com.applovin.impl.sdk.d.a {
    private static String a;
    /* access modifiers changed from: private */
    public final MaxAdFormat c;
    private final boolean d;
    /* access modifiers changed from: private */
    public final Activity e;
    private final a f;

    public interface a {
        void a(JSONArray jSONArray);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    static {
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(a("APPLOVIN_NETWORK", "com.applovin.mediation.adapters.AppLovinMediationAdapter"));
            a("FACEBOOK_NETWORK", "com.applovin.mediation.adapters.FacebookMediationAdapter").put("run_on_ui_thread", false);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("signal_providers", jSONArray);
            a = jSONObject.toString();
        } catch (JSONException unused) {
        }
    }

    public b(MaxAdFormat maxAdFormat, boolean z, Activity activity, j jVar, a aVar) {
        super("TaskCollectSignals", jVar);
        this.c = maxAdFormat;
        this.d = z;
        this.e = activity;
        this.f = aVar;
    }

    private String a(String str, d<Integer> dVar) {
        int intValue;
        return (!TextUtils.isEmpty(str) && (intValue = ((Integer) this.b.a(dVar)).intValue()) > 0) ? str.substring(0, Math.min(str.length(), intValue)) : "";
    }

    private static JSONObject a(String str, String str2) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", str);
        jSONObject.put("class", str2);
        jSONObject.put("adapter_timeout_ms", (int) NetworkConstants.UPLOAD_CONNECT_TIMEOUT);
        jSONObject.put("max_signal_length", 32768);
        jSONObject.put("scode", "");
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public void a(final g gVar, final f.a aVar) {
        AnonymousClass2 r0 = new Runnable() {
            public void run() {
                b.this.b.x().collectSignal(b.this.c, gVar, b.this.e, aVar);
            }
        };
        if (gVar.F()) {
            a("Running signal collection for " + gVar + " on the main thread");
            this.e.runOnUiThread(r0);
            return;
        }
        a("Running signal collection for " + gVar + " on the background thread");
        r0.run();
    }

    private void a(Collection<f> collection) {
        String str;
        String a2;
        JSONArray jSONArray = new JSONArray();
        for (f next : collection) {
            try {
                JSONObject jSONObject = new JSONObject();
                g a3 = next.a();
                jSONObject.put("name", a3.D());
                jSONObject.put("class", a3.C());
                jSONObject.put(TapjoyConstants.TJC_ADAPTER_VERSION, a(next.c(), c.l));
                jSONObject.put("sdk_version", a(next.b(), c.m));
                JSONObject jSONObject2 = new JSONObject();
                if (n.b(next.e())) {
                    str = AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE;
                    a2 = next.e();
                } else {
                    str = "signal";
                    a2 = a(next.d(), c.n);
                }
                jSONObject2.put(str, a2);
                jSONObject.put("data", jSONObject2);
                jSONArray.put(jSONObject);
                a("Collected signal from " + a3);
            } catch (JSONException e2) {
                a("Failed to create signal data", e2);
            }
        }
        a(jSONArray);
    }

    private void a(JSONArray jSONArray) {
        a aVar = this.f;
        if (aVar != null) {
            aVar.a(jSONArray);
        }
    }

    private void a(JSONArray jSONArray, JSONObject jSONObject) throws JSONException, InterruptedException {
        a("Collecting signals from " + jSONArray.length() + " signal providers(s)...");
        List a2 = e.a(jSONArray.length());
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        CountDownLatch countDownLatch = new CountDownLatch(jSONArray.length());
        ScheduledExecutorService b = this.b.J().b();
        for (int i = 0; i < jSONArray.length(); i++) {
            final g gVar = new g(jSONArray.getJSONObject(i), jSONObject, this.b);
            final AtomicBoolean atomicBoolean2 = atomicBoolean;
            final List list = a2;
            final CountDownLatch countDownLatch2 = countDownLatch;
            b.execute(new Runnable() {
                public void run() {
                    b.this.a(gVar, new f.a() {
                        public void a(f fVar) {
                            if (atomicBoolean2.get() && fVar != null) {
                                list.add(fVar);
                            }
                            countDownLatch2.countDown();
                        }
                    });
                }
            });
        }
        countDownLatch.await(((Long) this.b.a(c.k)).longValue(), TimeUnit.MILLISECONDS);
        atomicBoolean.set(false);
        a(a2);
    }

    private void b(String str, Throwable th) {
        a("No signals collected: " + str, th);
        a(new JSONArray());
    }

    public i a() {
        return i.B;
    }

    public void run() {
        String str;
        try {
            JSONObject jSONObject = new JSONObject((String) this.b.b(com.applovin.impl.sdk.b.f.x, a));
            JSONArray b = com.applovin.impl.sdk.utils.i.b(jSONObject, "signal_providers", (JSONArray) null, this.b);
            if (this.d) {
                List<String> b2 = this.b.b(c.Q);
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < b.length(); i++) {
                    JSONObject a2 = com.applovin.impl.sdk.utils.i.a(b, i, (JSONObject) null, this.b);
                    if (b2.contains(com.applovin.impl.sdk.utils.i.b(a2, "class", (String) null, this.b))) {
                        jSONArray.put(a2);
                    }
                }
                b = jSONArray;
            }
            if (b.length() == 0) {
                b("No signal providers found", null);
            } else {
                a(b, jSONObject);
            }
        } catch (JSONException e2) {
            th = e2;
            str = "Failed to parse signals JSON";
            b(str, th);
        } catch (InterruptedException e3) {
            th = e3;
            str = "Failed to wait for signals";
            b(str, th);
        } catch (Throwable th) {
            th = th;
            str = "Failed to collect signals";
            b(str, th);
        }
    }
}
