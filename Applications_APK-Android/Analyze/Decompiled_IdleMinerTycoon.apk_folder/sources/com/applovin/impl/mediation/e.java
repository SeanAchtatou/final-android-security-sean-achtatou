package com.applovin.impl.mediation;

import com.applovin.impl.sdk.utils.n;

public class e {
    public static final e EMPTY = new e(0);
    private final int errorCode;
    private final String errorMessage;

    public e(int i) {
        this(i, "");
    }

    public e(int i, String str) {
        this.errorCode = i;
        this.errorMessage = n.c(str);
    }

    public e(String str) {
        this(-1, str);
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String toString() {
        return "MaxError{errorCode=" + getErrorCode() + ", errorMessage='" + getErrorMessage() + '\'' + '}';
    }
}
