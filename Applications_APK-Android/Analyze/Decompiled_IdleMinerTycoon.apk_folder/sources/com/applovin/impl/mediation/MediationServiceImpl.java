package com.applovin.impl.mediation;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.f;
import com.applovin.impl.mediation.c.b;
import com.applovin.impl.mediation.c.d;
import com.applovin.impl.mediation.c.g;
import com.applovin.impl.mediation.c.h;
import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

public class MediationServiceImpl {
    /* access modifiers changed from: private */
    public final j a;
    /* access modifiers changed from: private */
    public final p b;

    private class a implements d, MaxAdViewAdListener, MaxRewardedAdListener {
        private final com.applovin.impl.mediation.b.a b;
        /* access modifiers changed from: private */
        public final MaxAdListener c;

        private a(com.applovin.impl.mediation.b.a aVar, MaxAdListener maxAdListener) {
            this.b = aVar;
            this.c = maxAdListener;
        }

        public void a(MaxAd maxAd, e eVar) {
            MediationServiceImpl.this.b(this.b, eVar, this.c);
            if (maxAd.getFormat() == MaxAdFormat.REWARDED && (maxAd instanceof c)) {
                ((c) maxAd).u();
            }
        }

        public void a(String str, e eVar) {
            MediationServiceImpl.this.a(this.b, eVar, this.c);
        }

        public void onAdClicked(MaxAd maxAd) {
            MediationServiceImpl.this.a.ab().a((com.applovin.impl.mediation.b.a) maxAd, "DID_CLICKED");
            MediationServiceImpl.this.c(this.b);
            com.applovin.impl.sdk.utils.j.d(this.c, maxAd);
        }

        public void onAdCollapsed(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.h(this.c, maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            MediationServiceImpl.this.b(this.b, new e(i), this.c);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad via callback...");
            MediationServiceImpl.this.maybeScheduleCallbackAdImpressionPostback(this.b);
            if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                MediationServiceImpl.this.a.Y().c();
            }
            com.applovin.impl.sdk.utils.j.b(this.c, maxAd);
        }

        public void onAdExpanded(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.g(this.c, maxAd);
        }

        public void onAdHidden(final MaxAd maxAd) {
            MediationServiceImpl.this.a.ab().a((com.applovin.impl.mediation.b.a) maxAd, "DID_HIDE");
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                        MediationServiceImpl.this.a.Y().d();
                    }
                    com.applovin.impl.sdk.utils.j.c(a.this.c, maxAd);
                }
            }, maxAd instanceof e ? ((e) maxAd).M() : 0);
        }

        public void onAdLoadFailed(String str, int i) {
            MediationServiceImpl.this.a(this.b, new e(i), this.c);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MediationServiceImpl.this.b(this.b);
            com.applovin.impl.sdk.utils.j.a(this.c, maxAd);
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.f(this.c, maxAd);
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.e(this.c, maxAd);
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            com.applovin.impl.sdk.utils.j.a(this.c, maxAd, maxReward);
            MediationServiceImpl.this.a.J().a(new g((c) maxAd, MediationServiceImpl.this.a), r.a.MEDIATION_REWARD);
        }
    }

    public MediationServiceImpl(j jVar) {
        this.a = jVar;
        this.b = jVar.u();
    }

    private void a(com.applovin.impl.mediation.b.a aVar) {
        p pVar = this.b;
        pVar.b("MediationService", "Firing ad preload postback for " + aVar.D());
        a("mpreload", aVar);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.a aVar, e eVar, MaxAdListener maxAdListener) {
        a(eVar, aVar);
        destroyAd(aVar);
        com.applovin.impl.sdk.utils.j.a(maxAdListener, aVar.getAdUnitId(), eVar.getErrorCode());
    }

    private void a(e eVar, com.applovin.impl.mediation.b.a aVar) {
        long f = aVar.f();
        p pVar = this.b;
        pVar.b("MediationService", "Firing ad load failure postback with load time: " + f);
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(f));
        a("mlerr", hashMap, eVar, aVar);
    }

    private void a(String str, e eVar) {
        a(str, Collections.EMPTY_MAP, (e) null, eVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, com.applovin.impl.mediation.b.g gVar) {
        a("serr", Collections.EMPTY_MAP, new e(str), gVar);
    }

    private void a(String str, Map<String, String> map, e eVar) {
        a(str, map, (e) null, eVar);
    }

    private void a(String str, Map<String, String> map, e eVar, e eVar2) {
        HashMap hashMap = new HashMap(map);
        hashMap.put("{PLACEMENT}", eVar2.N() != null ? eVar2.N() : "");
        if (eVar2 instanceof c) {
            c cVar = (c) eVar2;
            hashMap.put("{PUBLISHER_AD_UNIT_ID}", cVar.m() != null ? cVar.m() : "");
        }
        this.a.J().a(new d(str, hashMap, eVar, eVar2, this.a), r.a.MEDIATION_POSTBACKS);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.b.a aVar) {
        long f = aVar.f();
        p pVar = this.b;
        pVar.b("MediationService", "Firing ad load success postback with load time: " + f);
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(f));
        a("load", hashMap, aVar);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.b.a aVar, e eVar, MaxAdListener maxAdListener) {
        this.a.ab().a(aVar, "DID_FAIL_DISPLAY");
        maybeScheduleAdDisplayErrorPostback(eVar, aVar);
        if (aVar.h().compareAndSet(false, true)) {
            com.applovin.impl.sdk.utils.j.a(maxAdListener, aVar, eVar.getErrorCode());
        }
    }

    /* access modifiers changed from: private */
    public void c(com.applovin.impl.mediation.b.a aVar) {
        a("mclick", aVar);
    }

    public void collectSignal(MaxAdFormat maxAdFormat, final com.applovin.impl.mediation.b.g gVar, Activity activity, final f.a aVar) {
        String str;
        p pVar;
        String str2;
        StringBuilder sb;
        String str3;
        if (gVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (aVar != null) {
            final i a2 = this.a.v().a(gVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(gVar, maxAdFormat, activity.getApplicationContext());
                a2.a(a3, activity);
                AnonymousClass4 r1 = new MaxSignalCollectionListener() {
                    public void onSignalCollected(String str) {
                        aVar.a(f.a(gVar, a2, str));
                    }

                    public void onSignalCollectionFailed(String str) {
                        MediationServiceImpl.this.a(str, gVar);
                        aVar.a(f.b(gVar, a2, str));
                    }
                };
                if (!gVar.b()) {
                    pVar = this.b;
                    str3 = "MediationService";
                    sb = new StringBuilder();
                    str2 = "Collecting signal for adapter: ";
                } else if (this.a.w().a(gVar)) {
                    pVar = this.b;
                    str3 = "MediationService";
                    sb = new StringBuilder();
                    str2 = "Collecting signal for now-initialized adapter: ";
                } else {
                    p pVar2 = this.b;
                    pVar2.e("MediationService", "Skip collecting signal for not-initialized adapter: " + a2.b());
                    str = "Adapter not initialized yet";
                }
                sb.append(str2);
                sb.append(a2.b());
                pVar.b(str3, sb.toString());
                a2.a(a3, gVar, activity, r1);
                return;
            }
            str = "Could not load adapter";
            aVar.a(f.a(gVar, str));
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    public void destroyAd(MaxAd maxAd) {
        if (maxAd instanceof com.applovin.impl.mediation.b.a) {
            p pVar = this.b;
            pVar.c("MediationService", "Destroying " + maxAd);
            com.applovin.impl.mediation.b.a aVar = (com.applovin.impl.mediation.b.a) maxAd;
            i c = aVar.c();
            if (c != null) {
                c.g();
                aVar.i();
            }
        }
    }

    public void loadAd(String str, MaxAdFormat maxAdFormat, f fVar, boolean z, Activity activity, MaxAdListener maxAdListener) {
        final MaxAdListener maxAdListener2 = maxAdListener;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAdListener2 != null) {
            if (!this.a.d()) {
                p.i("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
            }
            this.a.a();
            final c a2 = this.a.A().a(maxAdFormat);
            if (a2 != null) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        maxAdListener2.onAdLoaded(a2);
                    }
                }, a2.k());
            }
            final f fVar2 = fVar;
            final String str2 = str;
            final MaxAdFormat maxAdFormat2 = maxAdFormat;
            final Activity activity2 = activity;
            final MaxAdListener maxAdListener3 = maxAdListener;
            this.a.J().a(new b(maxAdFormat, z, activity, this.a, new b.a() {
                public void a(JSONArray jSONArray) {
                    MediationServiceImpl.this.a.J().a(new com.applovin.impl.mediation.c.c(str2, maxAdFormat2, fVar2 != null ? fVar2 : new f.a().a(), jSONArray, activity2, MediationServiceImpl.this.a, maxAdListener3));
                }
            }), com.applovin.impl.mediation.d.c.a(maxAdFormat));
        } else {
            throw new IllegalArgumentException("No listener specified");
        }
    }

    public void loadThirdPartyMediatedAd(String str, com.applovin.impl.mediation.b.a aVar, Activity activity, MaxAdListener maxAdListener) {
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (activity != null) {
            p pVar = this.b;
            pVar.b("MediationService", "Loading " + aVar + "...");
            this.a.ab().a(aVar, "WILL_LOAD");
            a(aVar);
            i a2 = this.a.v().a(aVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(aVar, activity.getApplicationContext());
                a2.a(a3, activity);
                com.applovin.impl.mediation.b.a a4 = aVar.a(a2);
                a2.a(str, a4);
                a4.g();
                a2.a(str, a3, a4, activity, new a(a4, maxAdListener));
                return;
            }
            p pVar2 = this.b;
            pVar2.d("MediationService", "Failed to load " + aVar + ": adapter not loaded");
            a(aVar, new e((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED), maxAdListener);
        } else {
            throw new IllegalArgumentException("A valid Activity is required");
        }
    }

    public void maybeScheduleAdDisplayErrorPostback(e eVar, com.applovin.impl.mediation.b.a aVar) {
        a("mierr", Collections.EMPTY_MAP, eVar, aVar);
    }

    public void maybeScheduleAdapterInitializationPostback(e eVar, long j, MaxAdapter.InitializationStatus initializationStatus, String str) {
        HashMap hashMap = new HashMap(3);
        hashMap.put("{INIT_STATUS}", String.valueOf(initializationStatus.getCode()));
        hashMap.put("{INIT_TIME_MS}", String.valueOf(j));
        a("minit", hashMap, new e(str), eVar);
    }

    public void maybeScheduleCallbackAdImpressionPostback(com.applovin.impl.mediation.b.a aVar) {
        a("mcimp", aVar);
    }

    public void maybeScheduleRawAdImpressionPostback(com.applovin.impl.mediation.b.a aVar) {
        this.a.ab().a(aVar, "WILL_DISPLAY");
        a("mimp", aVar);
    }

    public void maybeScheduleViewabilityAdImpressionPostback(com.applovin.impl.mediation.b.b bVar, long j) {
        HashMap hashMap = new HashMap(1);
        hashMap.put("{VIEWABILITY_FLAGS}", String.valueOf(j));
        hashMap.put("{USED_VIEWABILITY_TIMER}", String.valueOf(bVar.r()));
        a("mvimp", hashMap, bVar);
    }

    public void showFullscreenAd(MaxAd maxAd, String str, final Activity activity) {
        if (maxAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAd instanceof c) {
            this.a.Y().a(true);
            final c cVar = (c) maxAd;
            final i c = cVar.c();
            if (c != null) {
                cVar.d(str);
                long K = cVar.K();
                p pVar = this.b;
                pVar.c("MediationService", "Showing ad " + maxAd.getAdUnitId() + " with delay of " + K + "ms...");
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        if (cVar.getFormat() == MaxAdFormat.REWARDED) {
                            MediationServiceImpl.this.a.J().a(new h(cVar, MediationServiceImpl.this.a), r.a.MEDIATION_REWARD);
                        }
                        c.a(cVar, activity);
                        MediationServiceImpl.this.a.Y().a(false);
                        MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad manually...");
                        MediationServiceImpl.this.maybeScheduleRawAdImpressionPostback(cVar);
                    }
                }, K);
                return;
            }
            this.a.Y().a(false);
            p pVar2 = this.b;
            pVar2.d("MediationService", "Failed to show " + maxAd + ": adapter not found");
            p.j("MediationService", "There may be an integration problem with the adapter for ad unit id '" + cVar.getAdUnitId() + "'. Please check if you have a supported version of that SDK integrated into your project.");
            throw new IllegalStateException("Could not find adapter for provided ad");
        } else {
            p.j("MediationService", "Unable to show ad for '" + maxAd.getAdUnitId() + "': only REWARDED or INTERSTITIAL ads are eligible for showFullscreenAd(). " + maxAd.getFormat() + " ad was provided.");
            throw new IllegalArgumentException("Provided ad is not a MediatedFullscreenAd");
        }
    }
}
