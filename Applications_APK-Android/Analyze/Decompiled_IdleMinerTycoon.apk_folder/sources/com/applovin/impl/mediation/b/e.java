package com.applovin.impl.mediation.b;

import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinPrivacySettings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    private final JSONObject a;
    protected final j b;
    private final JSONObject c;
    private final Object d = new Object();
    private final Object e = new Object();
    private volatile String f;

    public e(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (jSONObject2 == null) {
            throw new IllegalArgumentException("No full response specified");
        } else if (jSONObject != null) {
            this.b = jVar;
            this.a = jSONObject2;
            this.c = jSONObject;
        } else {
            throw new IllegalArgumentException("No spec object specified");
        }
    }

    private int a() {
        return b("mute_state", a("mute_state", ((Integer) this.b.a(c.S)).intValue()));
    }

    private List<String> a(String str) {
        try {
            return i.b(a(str, new JSONArray()));
        } catch (JSONException unused) {
            return Collections.EMPTY_LIST;
        }
    }

    private List<String> a(List<String> list, Map<String, String> map) {
        Map<String, String> b2 = b();
        ArrayList arrayList = new ArrayList(list.size());
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String next = it.next();
            for (String next2 : b2.keySet()) {
                next = next.replace(next2, f(b2.get(next2)));
            }
            for (String next3 : map.keySet()) {
                next = next.replace(next3, map.get(next3));
            }
            arrayList.add(next);
        }
        return arrayList;
    }

    private Map<String, String> b() {
        try {
            return i.a(new JSONObject((String) this.b.a(c.i)));
        } catch (JSONException unused) {
            return Collections.EMPTY_MAP;
        }
    }

    private List<String> e(String str) {
        try {
            return i.b(b(str, new JSONArray()));
        } catch (JSONException unused) {
            return Collections.EMPTY_LIST;
        }
    }

    private String f(String str) {
        String b2 = b(str, "");
        return n.b(b2) ? b2 : a(str, "");
    }

    /* access modifiers changed from: protected */
    public JSONObject A() {
        JSONObject jSONObject;
        synchronized (this.e) {
            jSONObject = this.a;
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public JSONObject B() {
        JSONObject jSONObject;
        synchronized (this.d) {
            jSONObject = this.c;
        }
        return jSONObject;
    }

    public String C() {
        return b("class", (String) null);
    }

    public String D() {
        return b("name", (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean E() {
        return b("is_testing", (Boolean) false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean F() {
        return b("run_on_ui_thread", (Boolean) true);
    }

    public Bundle G() {
        Bundle c2 = c("server_parameters") instanceof JSONObject ? i.c(a("server_parameters", (JSONObject) null)) : new Bundle();
        int a2 = a();
        if (a2 != -1) {
            if (a2 == 2) {
                c2.putBoolean("is_muted", this.b.k().isMuted());
            } else {
                c2.putBoolean("is_muted", a2 == 0);
            }
        }
        return c2;
    }

    public long H() {
        return b("adapter_timeout_ms", ((Long) this.b.a(c.o)).longValue());
    }

    public boolean I() {
        return J() >= 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long J() {
        long b2 = b("ad_refresh_ms", -1L);
        return b2 >= 0 ? b2 : a("ad_refresh_ms", ((Long) this.b.a(c.r)).longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long K() {
        long b2 = b("fullscreen_display_delay_ms", -1L);
        return b2 >= 0 ? b2 : ((Long) this.b.a(c.A)).longValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long L() {
        return b("init_completion_delay_ms", -1L);
    }

    public long M() {
        return b("ahdm", ((Long) this.b.a(c.B)).longValue());
    }

    public String N() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public float a(String str, float f2) {
        float a2;
        synchronized (this.d) {
            a2 = i.a(this.c, str, f2, this.b);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public int a(String str, int i) {
        int b2;
        synchronized (this.e) {
            b2 = i.b(this.a, str, i, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long a(String str, long j) {
        long a2;
        synchronized (this.e) {
            a2 = i.a(this.a, str, j, this.b);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public String a(String str, String str2) {
        String b2;
        synchronized (this.e) {
            b2 = i.b(this.a, str, str2, this.b);
        }
        return b2;
    }

    public List<String> a(String str, Map<String, String> map) {
        if (str != null) {
            List<String> a2 = a(str);
            List<String> e2 = e(str);
            ArrayList arrayList = new ArrayList(a2.size() + e2.size());
            arrayList.addAll(a2);
            arrayList.addAll(e2);
            return a(arrayList, map);
        }
        throw new IllegalArgumentException("No key specified");
    }

    /* access modifiers changed from: protected */
    public JSONArray a(String str, JSONArray jSONArray) {
        JSONArray b2;
        synchronized (this.e) {
            b2 = i.b(this.a, str, jSONArray, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject a(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.d) {
            b2 = i.b(this.c, str, jSONObject, this.b);
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean a(Context context) {
        return b("huc") ? b("huc", (Boolean) false) : a("huc", Boolean.valueOf(AppLovinPrivacySettings.hasUserConsent(context)));
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.e) {
            booleanValue = i.a(this.a, str, bool, this.b).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public int b(String str, int i) {
        int b2;
        synchronized (this.d) {
            b2 = i.b(this.c, str, i, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long b(String str, long j) {
        long a2;
        synchronized (this.d) {
            a2 = i.a(this.c, str, j, this.b);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        String b2;
        synchronized (this.d) {
            b2 = i.b(this.c, str, str2, this.b);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONArray b(String str, JSONArray jSONArray) {
        JSONArray b2;
        synchronized (this.d) {
            b2 = i.b(this.c, str, jSONArray, this.b);
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean b(Context context) {
        return b("aru") ? b("aru", (Boolean) false) : a("aru", Boolean.valueOf(AppLovinPrivacySettings.isAgeRestrictedUser(context)));
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        boolean has;
        synchronized (this.d) {
            has = this.c.has(str);
        }
        return has;
    }

    /* access modifiers changed from: protected */
    public boolean b(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.d) {
            booleanValue = i.a(this.c, str, bool, this.b).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public Object c(String str) {
        Object opt;
        synchronized (this.d) {
            opt = this.c.opt(str);
        }
        return opt;
    }

    /* access modifiers changed from: protected */
    public void c(String str, long j) {
        synchronized (this.d) {
            i.b(this.c, str, j, this.b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean c(Context context) {
        return b("dns") ? b("dns", (Boolean) false) : a("dns", Boolean.valueOf(AppLovinPrivacySettings.isDoNotSell(context)));
    }

    public void d(String str) {
        this.f = str;
    }

    public String toString() {
        return "MediationAdapterSpec{adapterClass='" + C() + "', adapterName='" + D() + "', isTesting=" + E() + ", isRefreshEnabled=" + I() + ", getAdRefreshMillis=" + J() + '}';
    }
}
