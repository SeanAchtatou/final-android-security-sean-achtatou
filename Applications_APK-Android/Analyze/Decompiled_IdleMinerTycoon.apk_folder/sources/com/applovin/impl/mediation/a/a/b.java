package com.applovin.impl.mediation.a.a;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class b {
    public TextView a;
    public TextView b;
    public ImageView c;
    private c d;

    public c a() {
        return this.d;
    }

    public void a(c cVar) {
        this.d = cVar;
        this.a.setText(cVar.c());
        if (this.b != null) {
            if (!TextUtils.isEmpty(cVar.d())) {
                this.b.setVisibility(0);
                this.b.setText(cVar.d());
            } else {
                this.b.setVisibility(8);
            }
        }
        if (this.c == null) {
            return;
        }
        if (cVar.g() > 0) {
            this.c.setImageResource(cVar.g());
            this.c.setColorFilter(cVar.h());
            this.c.setVisibility(0);
            return;
        }
        this.c.setVisibility(8);
    }
}
