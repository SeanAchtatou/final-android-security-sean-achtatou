package com.applovin.impl.mediation.a.a;

import android.text.SpannedString;
import com.applovin.impl.mediation.a.a.c;

public class g extends c {
    public g(String str) {
        super(c.a.SECTION);
        this.b = new SpannedString(str);
    }

    public String toString() {
        return "SectionListItemViewModel{text=" + ((Object) this.b) + "}";
    }
}
