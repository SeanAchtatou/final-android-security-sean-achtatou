package com.applovin.impl.mediation.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.applovin.impl.mediation.e;
import com.applovin.impl.mediation.j;
import com.applovin.impl.mediation.l;
import com.applovin.impl.sdk.b;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.h;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class MaxFullscreenAdImpl extends a implements b.a {
    private final a a;
    /* access modifiers changed from: private */
    public final com.applovin.impl.sdk.b b;
    /* access modifiers changed from: private */
    public final com.applovin.impl.mediation.b c;
    /* access modifiers changed from: private */
    public final Object d = new Object();
    private com.applovin.impl.mediation.b.c e = null;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.c f = null;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.c g = null;
    private c h = c.IDLE;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean();
    protected final b listenerWrapper;

    public interface a {
        Activity getActivity();
    }

    private class b implements j.b, MaxAdListener, MaxRewardedAdListener {
        private b() {
        }

        public void a(com.applovin.impl.mediation.b.c cVar) {
            if (cVar.getFormat() == MaxFullscreenAdImpl.this.adFormat) {
                onAdLoaded(cVar);
            }
        }

        public void onAdClicked(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.d(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onAdDisplayFailed(MaxAd maxAd, final int i) {
            MaxFullscreenAdImpl.this.a(c.IDLE, new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.b.a();
                    MaxFullscreenAdImpl.this.b();
                    com.applovin.impl.sdk.utils.j.a(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d(), i);
                }
            });
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (!((com.applovin.impl.mediation.b.c) maxAd).j()) {
                MaxFullscreenAdImpl.this.b.a();
            }
            com.applovin.impl.sdk.utils.j.b(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onAdHidden(MaxAd maxAd) {
            MaxFullscreenAdImpl.this.c.a(maxAd);
            MaxFullscreenAdImpl.this.a(c.IDLE, new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.b();
                    com.applovin.impl.sdk.utils.j.c(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
                }
            });
        }

        public void onAdLoadFailed(final String str, final int i) {
            MaxFullscreenAdImpl.this.c();
            if (MaxFullscreenAdImpl.this.g == null) {
                MaxFullscreenAdImpl.this.a(c.IDLE, new Runnable() {
                    public void run() {
                        com.applovin.impl.sdk.utils.j.a(MaxFullscreenAdImpl.this.adListener, str, i);
                    }
                });
            }
        }

        public void onAdLoaded(MaxAd maxAd) {
            com.applovin.impl.mediation.b.c cVar = (com.applovin.impl.mediation.b.c) maxAd;
            MaxFullscreenAdImpl.this.a(cVar);
            if (cVar.j() || !MaxFullscreenAdImpl.this.i.compareAndSet(true, false)) {
                MaxFullscreenAdImpl.this.a(c.READY, new Runnable() {
                    public void run() {
                        com.applovin.impl.sdk.utils.j.a(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
                    }
                });
            } else {
                MaxFullscreenAdImpl.this.loadRequestBuilder.a("expired_ad_ad_unit_id");
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.f(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            com.applovin.impl.sdk.utils.j.e(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d());
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            com.applovin.impl.sdk.utils.j.a(MaxFullscreenAdImpl.this.adListener, MaxFullscreenAdImpl.this.d(), maxReward);
        }
    }

    public enum c {
        IDLE,
        LOADING,
        READY,
        SHOWING,
        DESTROYED
    }

    public MaxFullscreenAdImpl(String str, MaxAdFormat maxAdFormat, a aVar, String str2, com.applovin.impl.sdk.j jVar) {
        super(str, maxAdFormat, str2, jVar);
        this.a = aVar;
        this.listenerWrapper = new b();
        this.b = new com.applovin.impl.sdk.b(jVar, this);
        this.c = new com.applovin.impl.mediation.b(jVar, this.listenerWrapper);
        p.g(str2, "Created new " + str2 + " (" + this + ")");
    }

    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.c a() {
        com.applovin.impl.mediation.b.c cVar;
        synchronized (this.d) {
            cVar = this.f != null ? this.f : this.g;
        }
        return cVar;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x016e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c r7, java.lang.Runnable r8) {
        /*
            r6 = this;
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = r6.h
            java.lang.Object r1 = r6.d
            monitor-enter(r1)
            com.applovin.impl.sdk.p r2 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r4.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Attempting state transition from "
            r4.append(r5)     // Catch:{ all -> 0x0199 }
            r4.append(r0)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = " to "
            r4.append(r5)     // Catch:{ all -> 0x0199 }
            r4.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0199 }
            r2.b(r3, r4)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0199 }
            r3 = 1
            r4 = 0
            if (r0 != r2) goto L_0x005e
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0032
        L_0x002f:
            r4 = 1
            goto L_0x0142
        L_0x0032:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0037
            goto L_0x002f
        L_0x0037:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0044
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "No ad is loading or loaded"
        L_0x003f:
            com.applovin.impl.sdk.p.j(r0, r2)     // Catch:{ all -> 0x0199 }
            goto L_0x0142
        L_0x0044:
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
        L_0x0059:
            r0.e(r2, r3)     // Catch:{ all -> 0x0199 }
            goto L_0x0142
        L_0x005e:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x0099
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0067
            goto L_0x002f
        L_0x0067:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0070
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "An ad is already loading"
            goto L_0x003f
        L_0x0070:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0075
            goto L_0x002f
        L_0x0075:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x007e
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "An ad is not ready to be shown yet"
            goto L_0x003f
        L_0x007e:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0083
            goto L_0x002f
        L_0x0083:
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x0099:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x00d8
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00a2
            goto L_0x002f
        L_0x00a2:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00ab
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "An ad is already loaded"
            goto L_0x003f
        L_0x00ab:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00b6
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = "An ad is already marked as ready"
            goto L_0x0059
        L_0x00b6:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00bc
            goto L_0x002f
        L_0x00bc:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00c2
            goto L_0x002f
        L_0x00c2:
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x00d8:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x011f
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.IDLE     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00e2
            goto L_0x002f
        L_0x00e2:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00ec
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "Can not load another ad while the ad is showing"
            goto L_0x003f
        L_0x00ec:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.READY     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00f8
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = "An ad is already showing, ignoring"
            goto L_0x0059
        L_0x00f8:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0102
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "The ad is already showing, not showing another one"
            goto L_0x003f
        L_0x0102:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0108
            goto L_0x002f
        L_0x0108:
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x011f:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.c.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x0129
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "No operations are allowed on a destroyed instance"
            goto L_0x003f
        L_0x0129:
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unknown state: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r5 = r6.h     // Catch:{ all -> 0x0199 }
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x0142:
            if (r4 == 0) goto L_0x016e
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Transitioning from "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r5 = r6.h     // Catch:{ all -> 0x0199 }
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = " to "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "..."
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            r0.b(r2, r3)     // Catch:{ all -> 0x0199 }
            r6.h = r7     // Catch:{ all -> 0x0199 }
            goto L_0x0190
        L_0x016e:
            com.applovin.impl.sdk.p r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Not allowed transition from "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c r5 = r6.h     // Catch:{ all -> 0x0199 }
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = " to "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r7 = r3.toString()     // Catch:{ all -> 0x0199 }
            r0.d(r2, r7)     // Catch:{ all -> 0x0199 }
        L_0x0190:
            monitor-exit(r1)     // Catch:{ all -> 0x0199 }
            if (r4 == 0) goto L_0x0198
            if (r8 == 0) goto L_0x0198
            r8.run()
        L_0x0198:
            return
        L_0x0199:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0199 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.a(com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$c, java.lang.Runnable):void");
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.c cVar) {
        if (cVar.j()) {
            this.g = cVar;
            p pVar = this.logger;
            String str = this.tag;
            pVar.b(str, "Handle ad loaded for fallback ad: " + cVar);
            return;
        }
        this.f = cVar;
        p pVar2 = this.logger;
        String str2 = this.tag;
        pVar2.b(str2, "Handle ad loaded for regular ad: " + cVar);
        b(cVar);
    }

    private void a(com.applovin.impl.mediation.b.c cVar, Context context, final Runnable runnable) {
        if (cVar == null || !cVar.w() || h.a(context) || !(context instanceof Activity)) {
            runnable.run();
            return;
        }
        AlertDialog create = new AlertDialog.Builder(context).setTitle(cVar.x()).setMessage(cVar.y()).setPositiveButton(cVar.z(), (DialogInterface.OnClickListener) null).create();
        create.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                runnable.run();
            }
        });
        create.show();
    }

    /* access modifiers changed from: private */
    public void a(String str, Activity activity) {
        synchronized (this.d) {
            this.e = a();
            this.sdk.A().b(this.listenerWrapper);
            if (this.e.j()) {
                if (this.e.h().get()) {
                    p pVar = this.logger;
                    String str2 = this.tag;
                    pVar.e(str2, "Failed to display ad: " + this.e + " - displayed already");
                    this.sdk.x().maybeScheduleAdDisplayErrorPostback(new e(-5201, "Ad displayed already"), this.e);
                    com.applovin.impl.sdk.utils.j.a(this.adListener, d(), -5201);
                    return;
                }
                this.sdk.A().a(this.listenerWrapper, this.adFormat);
            }
            this.e.a(this.adUnitId);
            this.c.b(this.e);
            p pVar2 = this.logger;
            String str3 = this.tag;
            pVar2.b(str3, "Showing ad for '" + this.adUnitId + "'; loaded ad: " + this.e + "...");
            this.sdk.x().showFullscreenAd(this.e, str, activity);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        com.applovin.impl.mediation.b.c cVar;
        synchronized (this.d) {
            cVar = this.e;
            this.e = null;
            if (cVar == this.g) {
                this.g = null;
            } else if (cVar == this.f) {
                this.f = null;
            }
        }
        this.sdk.x().destroyAd(cVar);
    }

    private void b(com.applovin.impl.mediation.b.c cVar) {
        long n = cVar.n();
        if (n >= 0) {
            p pVar = this.logger;
            String str = this.tag;
            pVar.b(str, "Scheduling ad expiration " + TimeUnit.MILLISECONDS.toMinutes(n) + " minutes from now for " + getAdUnitId() + " ...");
            this.b.a(n);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        com.applovin.impl.mediation.b.c cVar;
        if (this.i.compareAndSet(true, false)) {
            synchronized (this.d) {
                cVar = this.f;
                this.f = null;
            }
            this.sdk.x().destroyAd(cVar);
            this.loadRequestBuilder.a("expired_ad_ad_unit_id");
        }
    }

    /* access modifiers changed from: private */
    public l d() {
        return new l(this.adUnitId, this.adFormat);
    }

    public void destroy() {
        a(c.DESTROYED, new Runnable() {
            public void run() {
                synchronized (MaxFullscreenAdImpl.this.d) {
                    if (MaxFullscreenAdImpl.this.f != null) {
                        p pVar = MaxFullscreenAdImpl.this.logger;
                        String str = MaxFullscreenAdImpl.this.tag;
                        pVar.b(str, "Destroying ad for '" + MaxFullscreenAdImpl.this.adUnitId + "'; current ad: " + MaxFullscreenAdImpl.this.f + "...");
                        MaxFullscreenAdImpl.this.sdk.x().destroyAd(MaxFullscreenAdImpl.this.f);
                    }
                }
            }
        });
    }

    public boolean isReady() {
        boolean z;
        synchronized (this.d) {
            z = a() != null && a().a() && this.h == c.READY;
        }
        return z;
    }

    public void loadAd(final Activity activity) {
        p pVar = this.logger;
        String str = this.tag;
        pVar.b(str, "Loading ad for '" + this.adUnitId + "'...");
        if (isReady()) {
            p pVar2 = this.logger;
            String str2 = this.tag;
            pVar2.b(str2, "An ad is already loaded for '" + this.adUnitId + "'");
            com.applovin.impl.sdk.utils.j.a(this.adListener, d());
            return;
        }
        a(c.LOADING, new Runnable() {
            public void run() {
                final com.applovin.impl.mediation.b.c c = MaxFullscreenAdImpl.this.a();
                if (c == null || c.j()) {
                    MaxFullscreenAdImpl.this.sdk.A().a(MaxFullscreenAdImpl.this.listenerWrapper);
                    MaxFullscreenAdImpl.this.sdk.x().loadAd(MaxFullscreenAdImpl.this.adUnitId, MaxFullscreenAdImpl.this.adFormat, MaxFullscreenAdImpl.this.loadRequestBuilder.a(), false, activity != null ? activity : MaxFullscreenAdImpl.this.sdk.af(), MaxFullscreenAdImpl.this.listenerWrapper);
                    return;
                }
                MaxFullscreenAdImpl.this.a(c.READY, new Runnable() {
                    public void run() {
                        com.applovin.impl.sdk.utils.j.a(MaxFullscreenAdImpl.this.adListener, c);
                    }
                });
            }
        });
    }

    public void onAdExpired() {
        p pVar = this.logger;
        String str = this.tag;
        pVar.b(str, "Ad expired " + getAdUnitId());
        Activity activity = this.a.getActivity();
        if (activity == null) {
            activity = this.sdk.Z().a();
            if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.c.J)).booleanValue() || activity == null) {
                this.listenerWrapper.onAdLoadFailed(this.adUnitId, MaxErrorCodes.NO_ACTIVITY);
                return;
            }
        }
        this.i.set(true);
        this.loadRequestBuilder.a("expired_ad_ad_unit_id", getAdUnitId());
        this.sdk.x().loadAd(this.adUnitId, this.adFormat, this.loadRequestBuilder.a(), false, activity, this.listenerWrapper);
    }

    public void showAd(final String str, Activity activity) {
        final Activity af = activity != null ? activity : this.sdk.af();
        if (af == null) {
            throw new IllegalArgumentException("Attempting to show ad without a valid activity.");
        } else if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.c.F)).booleanValue() && (this.sdk.Y().a() || this.sdk.Y().b())) {
            p.j(this.tag, "Attempting to show ad when another fullscreen ad is already showing");
            com.applovin.impl.sdk.utils.j.a(this.adListener, a(), -23);
        } else if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.c.G)).booleanValue() || h.a(af)) {
            a(a(), activity, new Runnable() {
                public void run() {
                    MaxFullscreenAdImpl.this.a(c.SHOWING, new Runnable() {
                        public void run() {
                            MaxFullscreenAdImpl.this.a(str, af);
                        }
                    });
                }
            });
        } else {
            p.j(this.tag, "Attempting to show ad with no internet connection");
            com.applovin.impl.sdk.utils.j.a(this.adListener, d(), -5201);
        }
    }

    public String toString() {
        return this.tag + "{adUnitId='" + this.adUnitId + '\'' + ", adListener=" + this.adListener + ", isReady=" + isReady() + '}';
    }
}
