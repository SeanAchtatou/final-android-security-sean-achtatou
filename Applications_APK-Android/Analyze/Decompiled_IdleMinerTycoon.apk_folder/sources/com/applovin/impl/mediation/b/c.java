package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.j;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class c extends a {
    private String c;
    private final AtomicReference<com.applovin.impl.sdk.a.c> d;
    private final AtomicBoolean e;

    private c(c cVar, i iVar) {
        super(cVar.B(), cVar.A(), iVar, cVar.b);
        this.d = cVar.d;
        this.e = cVar.e;
    }

    public c(JSONObject jSONObject, JSONObject jSONObject2, j jVar) {
        super(jSONObject, jSONObject2, null, jVar);
        this.d = new AtomicReference<>();
        this.e = new AtomicBoolean();
    }

    public a a(i iVar) {
        return new c(this, iVar);
    }

    public void a(com.applovin.impl.sdk.a.c cVar) {
        this.d.set(cVar);
    }

    public void a(String str) {
        this.c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean j() {
        return b("fa", (Boolean) false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long k() {
        return b("ifacd_ms", -1L);
    }

    public long l() {
        return b("fard_ms", TimeUnit.HOURS.toMillis(1));
    }

    public String m() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long n() {
        long b = b("ad_expiration_ms", -1L);
        return b >= 0 ? b : a("ad_expiration_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.c.H)).longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long o() {
        long b = b("ad_hidden_timeout_ms", -1L);
        return b >= 0 ? b : a("ad_hidden_timeout_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.c.K)).longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean p() {
        if (b("schedule_ad_hidden_on_ad_dismiss", (Boolean) false)) {
            return true;
        }
        return a("schedule_ad_hidden_on_ad_dismiss", (Boolean) this.b.a(com.applovin.impl.sdk.b.c.L));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long */
    public long q() {
        long b = b("ad_hidden_on_ad_dismiss_callback_delay_ms", -1L);
        return b >= 0 ? b : a("ad_hidden_on_ad_dismiss_callback_delay_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.c.M)).longValue());
    }

    public String r() {
        return b("bcode", "");
    }

    public String s() {
        return a("mcode", "");
    }

    public boolean t() {
        return this.e.get();
    }

    public String toString() {
        return "MediatedFullscreenAd{format=" + getFormat() + ", adUnitId=" + getAdUnitId() + ", isReady=" + a() + ", adapterClass='" + C() + "', adapterName='" + D() + "', isTesting=" + E() + ", isRefreshEnabled=" + I() + ", getAdRefreshMillis=" + J() + '}';
    }

    public void u() {
        this.e.set(true);
    }

    public com.applovin.impl.sdk.a.c v() {
        return this.d.getAndSet(null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.a(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.a(java.util.List<java.lang.String>, java.util.Map<java.lang.String, java.lang.String>):java.util.List<java.lang.String>
      com.applovin.impl.mediation.b.e.a(java.lang.String, float):float
      com.applovin.impl.mediation.b.e.a(java.lang.String, int):int
      com.applovin.impl.mediation.b.e.a(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.a(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>):java.util.List<java.lang.String>
      com.applovin.impl.mediation.b.e.a(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.a(java.lang.String, org.json.JSONObject):org.json.JSONObject
      com.applovin.impl.mediation.b.e.a(java.lang.String, java.lang.Boolean):boolean */
    public boolean w() {
        return b("show_nia", Boolean.valueOf(a("show_nia", (Boolean) false)));
    }

    public String x() {
        return b("nia_title", a("nia_title", ""));
    }

    public String y() {
        return b("nia_message", a("nia_message", ""));
    }

    public String z() {
        return b("nia_button_title", a("nia_button_title", ""));
    }
}
