package com.applovin.impl.mediation;

import android.text.TextUtils;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapters.MediationAdapterBase;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class h {
    private final j a;
    private final p b;
    private final Object c = new Object();
    private final Map<String, Class<? extends MaxAdapter>> d = new HashMap();
    private final Set<String> e = new HashSet();

    public h(j jVar) {
        if (jVar != null) {
            this.a = jVar;
            this.b = jVar.u();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private i a(e eVar, Class<? extends MaxAdapter> cls) {
        try {
            i iVar = new i(eVar, (MediationAdapterBase) cls.getConstructor(AppLovinSdk.class).newInstance(this.a.R()), this.a);
            if (iVar.c()) {
                return iVar;
            }
            p.j("MediationAdapterManager", "Adapter is disabled after initialization: " + eVar);
            return null;
        } catch (Throwable th) {
            p.c("MediationAdapterManager", "Failed to load adapter: " + eVar, th);
            return null;
        }
    }

    private Class<? extends MaxAdapter> a(String str) {
        try {
            Class<?> cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return cls.asSubclass(MaxAdapter.class);
            }
            p.j("MediationAdapterManager", str + " error: not an instance of '" + MaxAdapter.class.getName() + "'.");
            return null;
        } catch (Throwable th) {
            p.c("MediationAdapterManager", "Failed to load: " + str, th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public i a(e eVar) {
        Class<? extends MaxAdapter> cls;
        if (eVar != null) {
            String D = eVar.D();
            String C = eVar.C();
            if (TextUtils.isEmpty(D)) {
                p pVar = this.b;
                pVar.e("MediationAdapterManager", "No adapter name provided for " + C + ", not loading the adapter ");
                return null;
            } else if (TextUtils.isEmpty(C)) {
                p pVar2 = this.b;
                pVar2.e("MediationAdapterManager", "Unable to find default classname for '" + D + "'");
                return null;
            } else {
                synchronized (this.c) {
                    if (!this.e.contains(C)) {
                        if (this.d.containsKey(C)) {
                            cls = this.d.get(C);
                        } else {
                            cls = a(C);
                            if (cls == null) {
                                this.e.add(C);
                                return null;
                            }
                        }
                        i a2 = a(eVar, cls);
                        if (a2 != null) {
                            p pVar3 = this.b;
                            pVar3.b("MediationAdapterManager", "Loaded " + D);
                            this.d.put(C, cls);
                            return a2;
                        }
                        p pVar4 = this.b;
                        pVar4.e("MediationAdapterManager", "Failed to load " + D);
                        this.e.add(C);
                        return null;
                    }
                    p pVar5 = this.b;
                    pVar5.b("MediationAdapterManager", "Not attempting to load " + D + " due to prior errors");
                    return null;
                }
            }
        } else {
            throw new IllegalArgumentException("No adapter spec specified");
        }
    }

    public Collection<String> a() {
        Set unmodifiableSet;
        synchronized (this.c) {
            HashSet hashSet = new HashSet(this.d.size());
            for (Class<? extends MaxAdapter> name : this.d.values()) {
                hashSet.add(name.getName());
            }
            unmodifiableSet = Collections.unmodifiableSet(hashSet);
        }
        return unmodifiableSet;
    }

    public Collection<String> b() {
        Set unmodifiableSet;
        synchronized (this.c) {
            unmodifiableSet = Collections.unmodifiableSet(this.e);
        }
        return unmodifiableSet;
    }
}
