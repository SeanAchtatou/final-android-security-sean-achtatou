package com.chartboost.sdk.Model;

import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Tracking.a;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mintegral.msdk.mtgbid.out.BidResponsed;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b {
    public final String a;
    public final String b;
    public final String c;

    public b(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    private static Map<String, b> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            JSONObject jSONObject2 = jSONObject.getJSONObject(next);
            Iterator<String> keys2 = jSONObject2.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                JSONObject jSONObject3 = jSONObject2.getJSONObject(next2);
                hashMap.put(next2, new b(next, jSONObject3.getString("filename"), jSONObject3.getString("url")));
            }
        }
        return hashMap;
    }

    public static Map<String, b> a(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("videos");
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    String string = jSONObject2.getString("id");
                    hashMap.put(string, new b("videos", string, jSONObject2.getString("video")));
                } catch (JSONException e) {
                    a.a(b.class, "deserializeNativeVideos (file)", e);
                }
            }
        } catch (JSONException e2) {
            a.a(b.class, "deserializeNativeVideos (videos array)", e2);
        }
        return hashMap;
    }

    private static JSONObject a(JSONArray jSONArray) throws JSONException {
        JSONObject a2 = e.a(new e.a[0]);
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            String optString = jSONObject.optString("name");
            String optString2 = jSONObject.optString("type");
            String optString3 = jSONObject.optString("value");
            String optString4 = jSONObject.optString("param");
            if (!optString2.equals("param") && optString4.isEmpty()) {
                JSONObject optJSONObject = a2.optJSONObject(optString2);
                if (optJSONObject == null) {
                    optJSONObject = e.a(new e.a[0]);
                    a2.put(optString2, optJSONObject);
                }
                optJSONObject.put(optString2.equals(TJAdUnitConstants.String.HTML) ? "body" : optString, e.a(e.a("filename", optString), e.a("url", optString3)));
            }
        }
        return a2;
    }

    public static Map<String, b> a(JSONObject jSONObject, int i) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("cache_assets");
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                int i2 = 0;
                if (next.equals("templates")) {
                    JSONArray optJSONArray = jSONObject2.optJSONArray("templates");
                    if (optJSONArray != null) {
                        int min = Math.min(i, optJSONArray.length());
                        while (i2 < min) {
                            for (Map.Entry<String, b> value : b(a(optJSONArray.getJSONObject(i2).getJSONArray(MessengerShareContentUtility.ELEMENTS))).entrySet()) {
                                b bVar = (b) value.getValue();
                                hashMap.put(bVar.b, bVar);
                            }
                            i2++;
                        }
                    }
                } else {
                    JSONArray jSONArray = jSONObject2.getJSONArray(next);
                    while (i2 < jSONArray.length()) {
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                        String string = jSONObject3.getString("name");
                        hashMap.put(string, new b(next, string, jSONObject3.getString("value")));
                        i2++;
                    }
                }
            }
        } catch (JSONException e) {
            a.a(b.class, "v2PrefetchToAssets", e);
        }
        return hashMap;
    }

    public static Map<String, b> a(String str) {
        JSONObject optJSONObject;
        HashMap hashMap = new HashMap();
        try {
            JSONArray optJSONArray = new JSONObject(str).optJSONArray("seatbid");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONArray optJSONArray2 = optJSONArray.optJSONObject(i).optJSONArray(BidResponsed.KEY_BID_ID);
                    if (optJSONArray2 != null) {
                        for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                            JSONObject jSONObject = optJSONArray2.getJSONObject(i2).getJSONObject("ext");
                            if (!(jSONObject == null || (optJSONObject = jSONObject.optJSONObject("bidder")) == null)) {
                                String string = optJSONObject.getString("template");
                                if (TextUtils.isEmpty(string)) {
                                    return null;
                                }
                                String substring = string.substring(string.lastIndexOf(47) + 1);
                                hashMap.put(substring, new b(TJAdUnitConstants.String.HTML, substring, string));
                            }
                        }
                        continue;
                    }
                }
            }
        } catch (JSONException e) {
            a.a(b.class, "bidResponseToAssets", e);
        }
        return hashMap;
    }

    public File a(File file) {
        return new File(file, this.a + Constants.URL_PATH_DELIMITER + this.b);
    }
}
