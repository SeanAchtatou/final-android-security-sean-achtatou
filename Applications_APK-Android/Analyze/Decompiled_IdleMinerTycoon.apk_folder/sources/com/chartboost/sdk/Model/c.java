package com.chartboost.sdk.Model;

import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.d;
import com.chartboost.sdk.e;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.am;
import com.chartboost.sdk.impl.an;
import com.chartboost.sdk.impl.ar;
import com.chartboost.sdk.impl.be;
import com.chartboost.sdk.impl.bh;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.s;
import com.chartboost.sdk.impl.w;
import com.chartboost.sdk.impl.x;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import java.util.Locale;
import org.json.JSONObject;

public class c {
    private boolean A;
    private Boolean B = null;
    private e C;
    private Runnable D;
    public final com.chartboost.sdk.impl.c a;
    public final f b;
    public final aj c;
    public final ar d;
    public final a e;
    public final Handler f;
    public final com.chartboost.sdk.c g;
    public final am h;
    public final d i;
    public final an j;
    public final d k;
    public int l;
    public final String m;
    public int n;
    public final String o;
    public final a p;
    public final SharedPreferences q;
    public boolean r;
    public be s;
    public boolean t = false;
    public boolean u = false;
    public boolean v = false;
    public al w;
    public boolean x;
    public boolean y = false;
    public boolean z = false;

    public c(a aVar, d dVar, f fVar, aj ajVar, ar arVar, SharedPreferences sharedPreferences, a aVar2, Handler handler, com.chartboost.sdk.c cVar, am amVar, d dVar2, an anVar, com.chartboost.sdk.impl.c cVar2, String str, String str2) {
        this.p = aVar;
        this.a = cVar2;
        this.b = fVar;
        this.c = ajVar;
        this.d = arVar;
        this.e = aVar2;
        this.f = handler;
        this.g = cVar;
        this.h = amVar;
        this.i = dVar2;
        this.j = anVar;
        this.k = dVar;
        this.l = 0;
        this.r = false;
        this.x = false;
        this.z = true;
        this.n = 3;
        this.m = str;
        this.o = str2;
        this.A = true;
        this.q = sharedPreferences;
    }

    public boolean a() {
        this.l = 0;
        if (this.p.m == 0) {
            switch (this.a.a) {
                case 0:
                    if (!this.p.A.equals("video")) {
                        this.n = 0;
                        this.C = new w(this, this.f, this.g);
                        break;
                    } else {
                        this.n = 1;
                        this.C = new x(this, this.b, this.f, this.g);
                        this.A = false;
                        break;
                    }
                case 1:
                    this.n = 2;
                    this.C = new x(this, this.b, this.f, this.g);
                    this.A = false;
                    break;
            }
        } else {
            switch (this.a.a) {
                case 0:
                    if (!this.p.A.equals("video")) {
                        this.n = 0;
                        break;
                    } else {
                        this.n = 1;
                        this.A = false;
                        break;
                    }
                case 1:
                    this.n = 2;
                    this.A = false;
                    break;
            }
            this.C = new bh(this, this.b, this.c, this.q, this.e, this.f, this.g, this.i);
        }
        return this.C.a(this.p.l);
    }

    public boolean b() {
        return this.A;
    }

    public void c() {
        this.z = true;
        this.g.b(this);
        this.k.b(this);
    }

    public void d() {
        this.k.a(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.json.JSONObject r7) {
        /*
            r6 = this;
            int r0 = r6.l
            r1 = 0
            r2 = 2
            if (r0 != r2) goto L_0x0049
            boolean r0 = r6.t
            if (r0 == 0) goto L_0x000b
            goto L_0x0049
        L_0x000b:
            com.chartboost.sdk.Model.a r0 = r6.p
            java.lang.String r0 = r0.u
            com.chartboost.sdk.Model.a r2 = r6.p
            java.lang.String r2 = r2.t
            boolean r3 = r2.isEmpty()
            if (r3 != 0) goto L_0x003b
            com.chartboost.sdk.impl.am r3 = r6.h     // Catch:{ Exception -> 0x0031 }
            boolean r3 = r3.a(r2)     // Catch:{ Exception -> 0x0031 }
            if (r3 == 0) goto L_0x002c
            java.lang.Boolean r0 = java.lang.Boolean.TRUE     // Catch:{ Exception -> 0x0027 }
            r6.B = r0     // Catch:{ Exception -> 0x0027 }
            r0 = r2
            goto L_0x003b
        L_0x0027:
            r0 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0032
        L_0x002c:
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ Exception -> 0x0031 }
            r6.B = r2     // Catch:{ Exception -> 0x0031 }
            goto L_0x003b
        L_0x0031:
            r2 = move-exception
        L_0x0032:
            java.lang.Class r3 = r6.getClass()
            java.lang.String r4 = "onClick"
            com.chartboost.sdk.Tracking.a.a(r3, r4, r2)
        L_0x003b:
            boolean r2 = r6.x
            if (r2 == 0) goto L_0x0040
            return r1
        L_0x0040:
            r2 = 1
            r6.x = r2
            r6.z = r1
            r6.a(r0, r7)
            return r2
        L_0x0049:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Model.c.a(org.json.JSONObject):boolean");
    }

    private boolean x() {
        return this.B != null;
    }

    private boolean y() {
        return this.B.booleanValue();
    }

    public void a(CBError.CBImpressionError cBImpressionError) {
        this.k.a(this, cBImpressionError);
    }

    public void e() {
        this.u = true;
        this.A = true;
        if (this.a.a == 1 && i.d != null) {
            i.d.didCompleteRewardedVideo(this.m, this.p.v);
        } else if (this.a.a == 0 && i.d != null) {
            i.d.didCompleteInterstitial(this.m);
        }
        w();
    }

    public void f() {
        this.v = true;
    }

    public boolean g() {
        if (this.C != null) {
            this.C.b();
            if (this.C.e() != null) {
                return true;
            }
        } else {
            CBLogging.b("CBImpression", "reinitializing -- no view protocol exists!!");
        }
        CBLogging.e("CBImpression", "reinitializing -- view not yet created");
        return false;
    }

    public void h() {
        i();
        if (this.r) {
            if (this.C != null) {
                this.C.d();
            }
            this.C = null;
            CBLogging.e("CBImpression", "Destroying the view and view data");
        }
    }

    public void i() {
        if (this.s != null) {
            this.s.b();
            try {
                if (!(this.C == null || this.C.e() == null || this.C.e().getParent() == null)) {
                    this.s.removeView(this.C.e());
                }
            } catch (Exception e2) {
                CBLogging.a("CBImpression", "Exception raised while cleaning up views", e2);
                a.a(getClass(), "cleanUpViews", e2);
            }
            this.s = null;
        }
        if (this.C != null) {
            this.C.f();
        }
        CBLogging.e("CBImpression", "Destroying the view");
    }

    public CBError.CBImpressionError j() {
        try {
            if (this.C != null) {
                return this.C.c();
            }
        } catch (Exception e2) {
            a.a(getClass(), "tryCreatingView", e2);
        }
        return CBError.CBImpressionError.ERROR_CREATING_VIEW;
    }

    public e.a k() {
        if (this.C != null) {
            return this.C.e();
        }
        return null;
    }

    public void l() {
        if (this.C != null && this.C.e() != null) {
            this.C.e().setVisibility(8);
        }
    }

    public void a(Runnable runnable) {
        this.D = runnable;
    }

    public void m() {
        this.t = true;
    }

    public void n() {
        if (this.D != null) {
            this.D.run();
            this.D = null;
        }
        this.t = false;
    }

    public String o() {
        return this.p.q;
    }

    public void p() {
        this.k.c(this);
    }

    public boolean q() {
        if (this.C != null) {
            return this.C.l();
        }
        return false;
    }

    public void r() {
        this.x = false;
        if (this.C != null && this.y) {
            this.y = false;
            this.C.m();
        }
    }

    public void s() {
        this.x = false;
    }

    public void t() {
        if (this.C != null && !this.y) {
            this.y = true;
            this.C.n();
        }
    }

    public e u() {
        return this.C;
    }

    public boolean v() {
        return this.z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.al.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.chartboost.sdk.impl.al.a(com.chartboost.sdk.impl.ai, com.chartboost.sdk.Model.CBError):void
      com.chartboost.sdk.impl.al.a(com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.al.a(java.lang.Object, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.al.a(org.json.JSONObject, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.af.a(com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.af.a(java.lang.Object, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.al.a(java.lang.String, java.lang.Object):void */
    public void w() {
        al alVar = new al("/api/video-complete", this.d, this.e, 2, null);
        alVar.a(FirebaseAnalytics.Param.LOCATION, this.m);
        alVar.a(MTGRewardVideoActivity.INTENT_REWARD, Integer.valueOf(this.p.v));
        alVar.a("currency-name", this.p.w);
        alVar.a("ad_id", o());
        alVar.a("force_close", (Object) false);
        if (!this.p.r.isEmpty()) {
            alVar.a("cgn", this.p.r);
        }
        e u2 = k() != null ? u() : null;
        if (u2 != null) {
            float k2 = u2.k();
            float j2 = u2.j();
            CBLogging.a(getClass().getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", Float.valueOf(j2), Float.valueOf(k2)));
            float f2 = j2 / 1000.0f;
            alVar.a("total_time", Float.valueOf(f2));
            if (k2 <= 0.0f) {
                alVar.a("playback_time", Float.valueOf(f2));
            } else {
                alVar.a("playback_time", Float.valueOf(k2 / 1000.0f));
            }
        }
        this.c.a(alVar);
        this.e.b(this.a.a(this.p.m), o());
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject) {
        e eVar;
        d c2;
        Handler handler = this.f;
        com.chartboost.sdk.impl.c cVar = this.a;
        cVar.getClass();
        handler.post(new c.a(1, this.m, null));
        if (b() && this.l == 2 && (c2 = this.g.c()) != null) {
            c2.b(this);
        }
        if (!s.a().a(str)) {
            al alVar = new al("/api/click", this.d, this.e, 2, null);
            if (!this.p.q.isEmpty()) {
                alVar.a("ad_id", this.p.q);
            }
            if (!this.p.x.isEmpty()) {
                alVar.a("to", this.p.x);
            }
            if (!this.p.r.isEmpty()) {
                alVar.a("cgn", this.p.r);
            }
            if (!this.p.s.isEmpty()) {
                alVar.a("creative", this.p.s);
            }
            if (this.n == 1 || this.n == 2) {
                if (this.p.m != 0 || k() == null) {
                    eVar = (this.p.m != 1 || k() == null) ? null : (bh) u();
                } else {
                    eVar = (x) u();
                }
                if (eVar != null) {
                    float k2 = eVar.k();
                    float j2 = eVar.j();
                    CBLogging.a(getClass().getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", Float.valueOf(j2), Float.valueOf(k2)));
                    float f2 = j2 / 1000.0f;
                    alVar.a("total_time", Float.valueOf(f2));
                    if (k2 <= 0.0f) {
                        alVar.a("playback_time", Float.valueOf(f2));
                    } else {
                        alVar.a("playback_time", Float.valueOf(k2 / 1000.0f));
                    }
                }
            }
            if (jSONObject != null) {
                alVar.a("click_coordinates", jSONObject);
            }
            alVar.a(FirebaseAnalytics.Param.LOCATION, this.m);
            if (x()) {
                alVar.a("retarget_reinstall", Boolean.valueOf(y()));
            }
            this.w = alVar;
            this.h.a(this, str, null);
        } else {
            this.h.a(this, false, str, CBError.CBClickError.URI_INVALID, null);
        }
        this.e.c(this.a.a(this.p.m), this.m, o());
    }
}
