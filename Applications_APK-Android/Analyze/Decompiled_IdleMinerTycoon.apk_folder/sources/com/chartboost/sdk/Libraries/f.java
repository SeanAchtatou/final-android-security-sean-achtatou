package com.chartboost.sdk.Libraries;

import android.content.Context;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.bj;
import com.chartboost.sdk.impl.s;
import com.onesignal.shortcutbadger.impl.NewHtcHomeBadger;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONObject;

public class f {
    public final File a;
    public final File b;
    private final AtomicReference<e> c;
    private final g d;
    private final AtomicReference<g> e;
    private s f;

    public f(s sVar, Context context, AtomicReference<e> atomicReference) {
        g[] gVarArr;
        File[] listFiles;
        g[] gVarArr2;
        f fVar = this;
        fVar.f = sVar;
        fVar.d = new g(context.getCacheDir());
        fVar.e = new AtomicReference<>();
        fVar.c = atomicReference;
        try {
            File b2 = sVar.b();
            if (b2 != null) {
                fVar.e.set(new g(b2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        fVar.b = new File(fVar.d.a, "track");
        fVar.a = new File(fVar.d.a, SettingsJsonConstants.SESSION_KEY);
        g[] gVarArr3 = {fVar.d, fVar.e.get()};
        int length = gVarArr3.length;
        int i = 0;
        while (i < length) {
            g gVar = gVarArr3[i];
            try {
                boolean z = gVar == fVar.d;
                if (gVar == null || (!z && !a())) {
                    gVarArr = gVarArr3;
                    i++;
                    gVarArr3 = gVarArr;
                    fVar = this;
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - TimeUnit.DAYS.toMillis((long) atomicReference.get().w);
                    File file = new File(gVar.a, "templates");
                    if (file.exists() && (listFiles = file.listFiles()) != null) {
                        int length2 = listFiles.length;
                        int i2 = 0;
                        while (i2 < length2) {
                            File file2 = listFiles[i2];
                            if (file2.isDirectory()) {
                                File[] listFiles2 = file2.listFiles();
                                if (listFiles2 != null) {
                                    int length3 = listFiles2.length;
                                    int i3 = 0;
                                    while (i3 < length3) {
                                        File file3 = listFiles2[i3];
                                        if (!z) {
                                            if (file3.lastModified() < currentTimeMillis) {
                                            }
                                            gVarArr = gVarArr3;
                                            i3++;
                                            gVarArr3 = gVarArr;
                                            AtomicReference<e> atomicReference2 = atomicReference;
                                        }
                                        if (!file3.delete()) {
                                            StringBuilder sb = new StringBuilder();
                                            gVarArr = gVarArr3;
                                            try {
                                                sb.append("Unable to delete ");
                                                sb.append(file3.getPath());
                                                CBLogging.b("FileCache", sb.toString());
                                                i3++;
                                                gVarArr3 = gVarArr;
                                                AtomicReference<e> atomicReference22 = atomicReference;
                                            } catch (Exception e3) {
                                                e = e3;
                                                CBLogging.a("FileCache", "Exception while cleaning up templates directory at " + gVar.f.getPath(), e);
                                                e.printStackTrace();
                                                i++;
                                                gVarArr3 = gVarArr;
                                                fVar = this;
                                            }
                                        }
                                        gVarArr = gVarArr3;
                                        i3++;
                                        gVarArr3 = gVarArr;
                                        AtomicReference<e> atomicReference222 = atomicReference;
                                    }
                                }
                                gVarArr2 = gVarArr3;
                                File[] listFiles3 = file2.listFiles();
                                if (listFiles3 != null && listFiles3.length == 0 && !file2.delete()) {
                                    CBLogging.b("FileCache", "Unable to delete " + file2.getPath());
                                }
                            } else {
                                gVarArr2 = gVarArr3;
                            }
                            i2++;
                            gVarArr3 = gVarArr2;
                            AtomicReference<e> atomicReference3 = atomicReference;
                        }
                    }
                    gVarArr = gVarArr3;
                    File file4 = new File(gVar.a, ".adId");
                    if (file4.exists() && ((z || file4.lastModified() < currentTimeMillis) && !file4.delete())) {
                        CBLogging.b("FileCache", "Unable to delete " + file4.getPath());
                    }
                    i++;
                    gVarArr3 = gVarArr;
                    fVar = this;
                }
            } catch (Exception e4) {
                e = e4;
                gVarArr = gVarArr3;
                CBLogging.a("FileCache", "Exception while cleaning up templates directory at " + gVar.f.getPath(), e);
                e.printStackTrace();
                i++;
                gVarArr3 = gVarArr;
                fVar = this;
            }
        }
    }

    public synchronized byte[] a(File file) {
        byte[] bArr;
        if (file == null) {
            return null;
        }
        try {
            bArr = bj.b(file);
        } catch (Exception e2) {
            CBLogging.a("FileCache", "Error loading cache from disk", e2);
            a.a(getClass(), "readByteArrayFromDisk", e2);
            bArr = null;
        }
        return bArr;
    }

    public String a(String str) {
        File file = new File(d().g, str);
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    public boolean a() {
        try {
            String c2 = this.f.c();
            if (c2 != null && c2.equals("mounted") && !i.o) {
                return true;
            }
        } catch (Exception e2) {
            a.a(getClass(), "isExternalStorageAvailable", e2);
        }
        CBLogging.e("FileCache", "External Storage unavailable");
        return false;
    }

    public boolean b(String str) {
        if (d().d == null || str == null) {
            return false;
        }
        return new File(d().d, str).exists();
    }

    public JSONArray b() {
        JSONArray jSONArray = new JSONArray();
        String[] list = d().g.list();
        if (list != null) {
            for (String str : list) {
                if (!str.equals(".nomedia") && !str.endsWith(".tmp")) {
                    jSONArray.put(str);
                }
            }
        }
        return jSONArray;
    }

    public JSONObject c() {
        String[] list;
        JSONObject jSONObject = new JSONObject();
        try {
            File file = d().a;
            for (String next : this.c.get().x) {
                if (!next.equals("templates")) {
                    File file2 = new File(file, next);
                    JSONArray jSONArray = new JSONArray();
                    if (file2.exists() && (list = file2.list()) != null) {
                        for (String str : list) {
                            if (!str.equals(".nomedia") && !str.endsWith(".tmp")) {
                                jSONArray.put(str);
                            }
                        }
                    }
                    e.a(jSONObject, next, jSONArray);
                }
            }
        } catch (Exception e2) {
            a.a(getClass(), "getWebViewCacheAssets", e2);
        }
        return jSONObject;
    }

    public g d() {
        if (a()) {
            g gVar = this.e.get();
            if (gVar == null) {
                try {
                    File b2 = this.f.b();
                    if (b2 != null) {
                        this.e.compareAndSet(null, new g(b2));
                        gVar = this.e.get();
                    }
                } catch (Exception e2) {
                    a.a(getClass(), "currentLocations", e2);
                }
            }
            if (gVar != null) {
                return gVar;
            }
        }
        return this.d;
    }

    public long b(File file) {
        long j = 0;
        if (file != null) {
            try {
                if (file.isDirectory()) {
                    File[] listFiles = file.listFiles();
                    if (listFiles == null) {
                        return 0;
                    }
                    for (File b2 : listFiles) {
                        j += b(b2);
                    }
                    return j;
                }
            } catch (Exception e2) {
                a.a(f.class, "getFolderSize", e2);
                return 0;
            }
        }
        if (file != null) {
            return file.length();
        }
        return 0;
    }

    public JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        g gVar = this.e.get();
        if (gVar != null) {
            e.a(jSONObject, ".chartboost-external-folder-size", Long.valueOf(b(gVar.a)));
        }
        e.a(jSONObject, ".chartboost-internal-folder-size", Long.valueOf(b(this.d.a)));
        File file = d().a;
        String[] list = file.list();
        if (list != null && list.length > 0) {
            for (String file2 : list) {
                File file3 = new File(file, file2);
                JSONObject jSONObject2 = new JSONObject();
                e.a(jSONObject2, file3.getName() + "-size", Long.valueOf(b(file3)));
                String[] list2 = file3.list();
                if (list2 != null) {
                    e.a(jSONObject2, NewHtcHomeBadger.COUNT, Integer.valueOf(list2.length));
                }
                e.a(jSONObject, file3.getName(), jSONObject2);
            }
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0042 A[SYNTHETIC, Splitter:B:29:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(java.io.File r5) {
        /*
            r4 = this;
            r0 = 0
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0026 }
            java.lang.String r2 = "rw"
            r1.<init>(r5, r2)     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0026 }
            r2 = 0
            r1.seek(r2)     // Catch:{ FileNotFoundException -> 0x0021, IOException -> 0x001e, all -> 0x001b }
            int r5 = r1.read()     // Catch:{ FileNotFoundException -> 0x0021, IOException -> 0x001e, all -> 0x001b }
            r1.seek(r2)     // Catch:{ FileNotFoundException -> 0x0021, IOException -> 0x001e, all -> 0x001b }
            r1.write(r5)     // Catch:{ FileNotFoundException -> 0x0021, IOException -> 0x001e, all -> 0x001b }
            r1.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x003f
        L_0x001b:
            r5 = move-exception
            r0 = r1
            goto L_0x0040
        L_0x001e:
            r5 = move-exception
            r0 = r1
            goto L_0x0027
        L_0x0021:
            r5 = move-exception
            r0 = r1
            goto L_0x0035
        L_0x0024:
            r5 = move-exception
            goto L_0x0040
        L_0x0026:
            r5 = move-exception
        L_0x0027:
            java.lang.String r1 = "FileCache"
            java.lang.String r2 = "IOException when attempting to touch file"
            com.chartboost.sdk.Libraries.CBLogging.a(r1, r2, r5)     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x003f
        L_0x0030:
            r0.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x003f
        L_0x0034:
            r5 = move-exception
        L_0x0035:
            java.lang.String r1 = "FileCache"
            java.lang.String r2 = "File not found when attempting to touch"
            com.chartboost.sdk.Libraries.CBLogging.a(r1, r2, r5)     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x003f
            goto L_0x0030
        L_0x003f:
            return
        L_0x0040:
            if (r0 == 0) goto L_0x0045
            r0.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0045:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.f.c(java.io.File):void");
    }
}
