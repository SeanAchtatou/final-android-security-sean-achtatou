package com.chartboost.sdk.Libraries;

public class a {
    public int a;
    public String b;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(android.content.Context r3) {
        /*
            r2 = this;
            r2.<init>()
            r0 = 0
            com.chartboost.sdk.impl.s r1 = com.chartboost.sdk.impl.s.a()     // Catch:{ IllegalStateException -> 0x001d, IOException -> 0x0015, GooglePlayServicesRepairableException -> 0x000d, GooglePlayServicesNotAvailableException -> 0x0024 }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r3 = r1.a(r3)     // Catch:{ IllegalStateException -> 0x001d, IOException -> 0x0015, GooglePlayServicesRepairableException -> 0x000d, GooglePlayServicesNotAvailableException -> 0x0024 }
            goto L_0x0025
        L_0x000d:
            java.lang.String r3 = "ContentValues"
            java.lang.String r1 = "There was a recoverable error connecting to Google Play Services."
            com.chartboost.sdk.Libraries.CBLogging.b(r3, r1)
            goto L_0x0024
        L_0x0015:
            java.lang.String r3 = "ContentValues"
            java.lang.String r1 = "The connection to Google Play Services failed."
            com.chartboost.sdk.Libraries.CBLogging.b(r3, r1)
            goto L_0x0024
        L_0x001d:
            java.lang.String r3 = "ContentValues"
            java.lang.String r1 = "This should have been called off the main thread."
            com.chartboost.sdk.Libraries.CBLogging.b(r3, r1)
        L_0x0024:
            r3 = r0
        L_0x0025:
            if (r3 != 0) goto L_0x002d
            r3 = -1
            r2.a = r3
            r2.b = r0
            goto L_0x0042
        L_0x002d:
            boolean r1 = r3.isLimitAdTrackingEnabled()
            if (r1 == 0) goto L_0x0039
            r3 = 1
            r2.a = r3
            r2.b = r0
            goto L_0x0042
        L_0x0039:
            r0 = 0
            r2.a = r0
            java.lang.String r3 = r3.getId()
            r2.b = r3
        L_0x0042:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.Libraries.a.<init>(android.content.Context):void");
    }
}
