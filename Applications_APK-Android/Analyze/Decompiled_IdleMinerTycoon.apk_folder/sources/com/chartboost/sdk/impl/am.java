package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.google.android.gms.drive.DriveFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Executor;

public class am {
    final ak a;
    final Handler b;
    private final Executor c;
    private final aj d;

    public am(Executor executor, aj ajVar, ak akVar, Handler handler) {
        this.c = executor;
        this.d = ajVar;
        this.a = akVar;
        this.b = handler;
    }

    public void a(c cVar, boolean z, String str, CBError.CBClickError cBClickError, al alVar) {
        if (cVar != null) {
            cVar.x = false;
            if (cVar.b()) {
                cVar.l = 4;
            }
        }
        if (!z) {
            if (i.d != null) {
                i.d.didFailToRecordClick(str, cBClickError);
            }
        } else if (cVar != null && cVar.w != null) {
            this.d.a(cVar.w);
        } else if (alVar != null) {
            this.d.a(alVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.am.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.al):void
     arg types: [com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.al]
     candidates:
      com.chartboost.sdk.impl.am.a(com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.al):void
      com.chartboost.sdk.impl.am.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.al):void */
    public void a(c cVar, String str, Activity activity, al alVar) {
        try {
            String scheme = new URI(str).getScheme();
            if (scheme == null) {
                a(cVar, false, str, CBError.CBClickError.URI_INVALID, alVar);
            } else if (scheme.equals("http") || scheme.equals("https")) {
                final String str2 = str;
                final c cVar2 = cVar;
                final Activity activity2 = activity;
                final al alVar2 = alVar;
                this.c.execute(new Runnable() {
                    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
                        if (r2 != null) goto L_0x0031;
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
                        r2.disconnect();
                     */
                    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0045, code lost:
                        if (r2 != null) goto L_0x0031;
                     */
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x004b A[SYNTHETIC, Splitter:B:25:0x004b] */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void run() {
                        /*
                            r6 = this;
                            java.lang.String r0 = r3     // Catch:{ Exception -> 0x0053 }
                            com.chartboost.sdk.impl.am r1 = com.chartboost.sdk.impl.am.this     // Catch:{ Exception -> 0x0053 }
                            com.chartboost.sdk.impl.ak r1 = r1.a     // Catch:{ Exception -> 0x0053 }
                            boolean r1 = r1.c()     // Catch:{ Exception -> 0x0053 }
                            if (r1 == 0) goto L_0x004f
                            r1 = 0
                            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            java.lang.String r3 = r3     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            r2.<init>(r3)     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x003a, all -> 0x0037 }
                            r1 = 0
                            r2.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0035 }
                            r1 = 10000(0x2710, float:1.4013E-41)
                            r2.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0035 }
                            r2.setReadTimeout(r1)     // Catch:{ Exception -> 0x0035 }
                            java.lang.String r1 = "Location"
                            java.lang.String r1 = r2.getHeaderField(r1)     // Catch:{ Exception -> 0x0035 }
                            if (r1 == 0) goto L_0x002f
                            r0 = r1
                        L_0x002f:
                            if (r2 == 0) goto L_0x004f
                        L_0x0031:
                            r2.disconnect()     // Catch:{ Exception -> 0x0053 }
                            goto L_0x004f
                        L_0x0035:
                            r1 = move-exception
                            goto L_0x003e
                        L_0x0037:
                            r0 = move-exception
                            r2 = r1
                            goto L_0x0049
                        L_0x003a:
                            r2 = move-exception
                            r5 = r2
                            r2 = r1
                            r1 = r5
                        L_0x003e:
                            java.lang.String r3 = "CBURLOpener"
                            java.lang.String r4 = "Exception raised while opening a HTTP Conection"
                            com.chartboost.sdk.Libraries.CBLogging.a(r3, r4, r1)     // Catch:{ all -> 0x0048 }
                            if (r2 == 0) goto L_0x004f
                            goto L_0x0031
                        L_0x0048:
                            r0 = move-exception
                        L_0x0049:
                            if (r2 == 0) goto L_0x004e
                            r2.disconnect()     // Catch:{ Exception -> 0x0053 }
                        L_0x004e:
                            throw r0     // Catch:{ Exception -> 0x0053 }
                        L_0x004f:
                            r6.a(r0)     // Catch:{ Exception -> 0x0053 }
                            goto L_0x005b
                        L_0x0053:
                            r0 = move-exception
                            java.lang.Class<com.chartboost.sdk.impl.am> r1 = com.chartboost.sdk.impl.am.class
                            java.lang.String r2 = "open followTask"
                            com.chartboost.sdk.Tracking.a.a(r1, r2, r0)
                        L_0x005b:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.am.AnonymousClass1.run():void");
                    }

                    private void a(final String str) {
                        AnonymousClass1 r0 = new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.chartboost.sdk.impl.am.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.al):void
                             arg types: [com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.al]
                             candidates:
                              com.chartboost.sdk.impl.am.a(com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.al):void
                              com.chartboost.sdk.impl.am.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.al):void */
                            public void run() {
                                try {
                                    am.this.a(cVar2, str, (Context) activity2, alVar2);
                                } catch (Exception e) {
                                    a.a(am.class, "open openOnUiThread Runnable.run", e);
                                }
                            }
                        };
                        if (activity2 != null) {
                            activity2.runOnUiThread(r0);
                        } else {
                            am.this.b.post(r0);
                        }
                    }
                });
            } else {
                a(cVar, str, (Context) activity, alVar);
            }
        } catch (URISyntaxException unused) {
            a(cVar, false, str, CBError.CBClickError.URI_INVALID, alVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar, String str, Context context, al alVar) {
        String str2;
        Exception e;
        String str3;
        if (cVar != null && cVar.b()) {
            cVar.l = 5;
        }
        if (context == null) {
            context = i.n;
        }
        if (context == null) {
            a(cVar, false, str, CBError.CBClickError.NO_HOST_ACTIVITY, alVar);
            return;
        }
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(DriveFile.MODE_READ_ONLY);
            }
            intent.setData(Uri.parse(str));
            context.startActivity(intent);
        } catch (Exception unused) {
            if (str.startsWith("market://")) {
                try {
                    String str4 = "http://market.android.com/" + str.substring(9);
                    try {
                        Intent intent2 = new Intent("android.intent.action.VIEW");
                        if (!(context instanceof Activity)) {
                            intent2.addFlags(DriveFile.MODE_READ_ONLY);
                        }
                        intent2.setData(Uri.parse(str4));
                        context.startActivity(intent2);
                        str2 = str4;
                    } catch (Exception e2) {
                        e = e2;
                        str3 = str4;
                        CBLogging.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e);
                        a(cVar, false, str3, CBError.CBClickError.URI_UNRECOGNIZED, alVar);
                        return;
                    }
                } catch (Exception e3) {
                    str3 = str;
                    e = e3;
                    CBLogging.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e);
                    a(cVar, false, str3, CBError.CBClickError.URI_UNRECOGNIZED, alVar);
                    return;
                }
            } else {
                a(cVar, false, str, CBError.CBClickError.URI_UNRECOGNIZED, alVar);
            }
        }
        str2 = str;
        a(cVar, true, str2, null, alVar);
    }

    public boolean a(String str) {
        try {
            Context context = i.n;
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(DriveFile.MODE_READ_ONLY);
            }
            intent.setData(Uri.parse(str));
            if (context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            CBLogging.a("CBURLOpener", "Cannot open URL", e);
            a.a(am.class, "canOpenURL", e);
            return false;
        }
    }

    public void a(c cVar, String str, al alVar) {
        a(cVar, str, cVar != null ? cVar.g.a() : null, alVar);
    }
}
