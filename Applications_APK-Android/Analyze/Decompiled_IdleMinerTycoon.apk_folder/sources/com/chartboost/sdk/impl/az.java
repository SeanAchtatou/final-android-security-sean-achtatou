package com.chartboost.sdk.impl;

import android.content.Context;
import android.view.View;
import com.chartboost.sdk.Model.a;

public final class az extends View {
    private boolean a = false;

    public az(Context context) {
        super(context);
        setFocusable(false);
        setBackgroundColor(-1442840576);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.ay.a(boolean, android.view.View, com.chartboost.sdk.Model.a):void
     arg types: [int, com.chartboost.sdk.impl.az, com.chartboost.sdk.Model.a]
     candidates:
      com.chartboost.sdk.impl.ay.a(int, com.chartboost.sdk.Model.c, java.lang.Runnable):void
      com.chartboost.sdk.impl.ay.a(boolean, android.view.View, long):void
      com.chartboost.sdk.impl.ay.a(boolean, android.view.View, com.chartboost.sdk.Model.a):void */
    public void a(ay ayVar, a aVar) {
        if (!this.a) {
            ayVar.a(true, (View) this, aVar);
            this.a = true;
        }
    }
}
