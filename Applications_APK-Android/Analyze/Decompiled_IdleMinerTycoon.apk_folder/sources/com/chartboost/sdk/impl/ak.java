package com.chartboost.sdk.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.i;

public class ak {
    int a = 1;
    private boolean b = false;
    private final b c = new b();
    private a d;

    public int a() {
        return this.a;
    }

    class b extends BroadcastReceiver {
        b() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("noConnectivity", false)) {
                ak.this.a = 0;
                CBLogging.a("CBReachability", "NETWORK TYPE: NO Network");
            } else if (Build.VERSION.SDK_INT >= 23) {
                ak.this.a((Network) null);
            } else {
                ak.this.b();
            }
        }
    }

    class a extends ConnectivityManager.NetworkCallback {
        a() {
        }

        public void onAvailable(Network network) {
            ak.this.a(network);
        }

        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            ak.this.a(networkCapabilities);
        }

        public void onLost(Network network) {
            ak.this.a = 0;
            CBLogging.a("CBReachability", "NETWORK TYPE: NO Network");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(NetworkCapabilities networkCapabilities) {
        if (networkCapabilities == null || networkCapabilities.getLinkDownstreamBandwidthKbps() <= 6000) {
            this.a = 2;
            CBLogging.a("CBReachability", "NETWORK TYPE: low speed");
            return;
        }
        this.a = 1;
        CBLogging.a("CBReachability", "NETWORK TYPE: high speed");
    }

    /* access modifiers changed from: package-private */
    public void a(Network network) {
        ConnectivityManager connectivityManager = (ConnectivityManager) i.n.getSystemService("connectivity");
        if (connectivityManager != null) {
            if (network == null) {
                network = connectivityManager.getActiveNetwork();
            }
            a(connectivityManager.getNetworkCapabilities(network));
        }
    }

    public void b() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) i.n.getSystemService("connectivity");
            if (connectivityManager == null) {
                this.a = -1;
                CBLogging.a("CBReachability", "NETWORK TYPE: unknown");
                return;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                this.a = 0;
                CBLogging.a("CBReachability", "NETWORK TYPE: NO Network");
            } else if (activeNetworkInfo.getType() == 1) {
                this.a = 1;
                CBLogging.a("CBReachability", "NETWORK TYPE: TYPE_WIFI");
            } else {
                this.a = 2;
                CBLogging.a("CBReachability", "NETWORK TYPE: TYPE_MOBILE");
            }
        } catch (SecurityException unused) {
            this.a = -1;
            CBLogging.b("CBReachability", "Chartboost SDK requires 'android.permission.ACCESS_NETWORK_STATE' permission set in your AndroidManifest.xml");
        }
    }

    public boolean c() {
        return (this.a == -1 || this.a == 0) ? false : true;
    }

    public void d() {
        if (this.b) {
            return;
        }
        if (Build.VERSION.SDK_INT > 23) {
            ConnectivityManager connectivityManager = (ConnectivityManager) i.n.getSystemService("connectivity");
            if (connectivityManager != null) {
                this.d = new a();
                connectivityManager.registerDefaultNetworkCallback(this.d);
                this.b = true;
                CBLogging.a("CBReachability", "Network broadcast receiver successfully registered");
            }
        } else if (i.n.registerReceiver(this.c, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")) != null) {
            this.b = true;
            CBLogging.a("CBReachability", "Network broadcast receiver successfully registered");
        } else {
            CBLogging.b("CBReachability", "Network broadcast receiver could not be registered");
        }
    }

    public void e() {
        if (!this.b) {
            return;
        }
        if (Build.VERSION.SDK_INT <= 23) {
            i.n.unregisterReceiver(this.c);
            this.b = false;
            CBLogging.a("CBReachability", "Network broadcast successfully unregistered");
            return;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) i.n.getSystemService("connectivity");
        if (connectivityManager != null) {
            connectivityManager.unregisterNetworkCallback(this.d);
            this.b = false;
            CBLogging.a("CBReachability", "Network broadcast successfully unregistered");
        }
    }
}
