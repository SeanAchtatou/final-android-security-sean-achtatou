package com.chartboost.sdk.impl;

import com.appsflyer.share.Constants;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.c;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.ShareConstants;
import com.helpshift.support.constants.FaqsColumns;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class al extends af<JSONObject> {
    public final JSONObject a = new JSONObject();
    public final a k;
    public boolean l = false;
    protected final ar m;
    private final String n;
    private String o;
    private final com.chartboost.sdk.Tracking.a p;

    public interface a {
        void a(al alVar, CBError cBError);

        void a(al alVar, JSONObject jSONObject);
    }

    public al(String str, ar arVar, com.chartboost.sdk.Tracking.a aVar, int i, a aVar2) {
        super(HttpRequest.METHOD_POST, a(str), i, null);
        this.n = str;
        this.m = arVar;
        this.p = aVar;
        this.k = aVar2;
    }

    public static String a(String str) {
        Object[] objArr = new Object[3];
        objArr[0] = "https://live.chartboost.com";
        objArr[1] = (str == null || !str.startsWith(Constants.URL_PATH_DELIMITER)) ? Constants.URL_PATH_DELIMITER : "";
        if (str == null) {
            str = "";
        }
        objArr[2] = str;
        return String.format("%s%s%s", objArr);
    }

    public void a(String str, Object obj) {
        e.a(this.a, str, obj);
    }

    /* access modifiers changed from: protected */
    public void c() {
        a("app", this.m.s);
        a("model", this.m.f);
        a(TapjoyConstants.TJC_DEVICE_TYPE_NAME, this.m.t);
        a("actual_device_type", this.m.u);
        a("os", this.m.g);
        a(UserDataStore.COUNTRY, this.m.h);
        a(FaqsColumns.LANGUAGE, this.m.i);
        a("sdk", this.m.l);
        a("user_agent", i.w);
        a("timestamp", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.m.e.a())));
        a(SettingsJsonConstants.SESSION_KEY, Integer.valueOf(this.m.d.getInt("cbPrefSessionCount", 0)));
        a("reachability", Integer.valueOf(this.m.b.a()));
        a("scale", this.m.r);
        a("is_portrait", Boolean.valueOf(CBUtility.a(CBUtility.a())));
        a(TJAdUnitConstants.String.BUNDLE, this.m.j);
        a("bundle_id", this.m.k);
        a("carrier", this.m.v);
        a("custom_id", i.b);
        a("mediation", i.i);
        if (i.e != null) {
            a("framework_version", i.g);
            a("wrapper_version", i.c);
        }
        a("rooted_device", Boolean.valueOf(this.m.w));
        a(TapjoyConstants.TJC_DEVICE_TIMEZONE, this.m.x);
        a("mobile_network", this.m.y);
        a("dw", this.m.o);
        a("dh", this.m.p);
        a("dpi", this.m.q);
        a("w", this.m.m);
        a("h", this.m.n);
        a("commit_hash", "7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
        d.a a2 = this.m.a.a();
        a("identity", a2.b);
        if (a2.a != -1) {
            boolean z = true;
            if (a2.a != 1) {
                z = false;
            }
            a("limit_ad_tracking", Boolean.valueOf(z));
        }
        a("pidatauseconsent", Integer.valueOf(i.x.getValue()));
        String str = this.m.c.get().a;
        if (!s.a().a(str)) {
            a("config_variant", str);
        }
        a("certification_providers", o.e());
    }

    public String d() {
        return e();
    }

    public String e() {
        if (this.n == null) {
            return Constants.URL_PATH_DELIMITER;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.n.startsWith(Constants.URL_PATH_DELIMITER) ? "" : Constants.URL_PATH_DELIMITER);
        sb.append(this.n);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.Libraries.e.a(java.lang.String, java.lang.Object):com.chartboost.sdk.Libraries.e$a
     arg types: [java.lang.String, int]
     candidates:
      com.chartboost.sdk.Libraries.e.a(org.json.JSONObject, java.lang.String[]):org.json.JSONObject
      com.chartboost.sdk.Libraries.e.a(java.lang.String, java.lang.Object):com.chartboost.sdk.Libraries.e$a */
    private void a(ai aiVar, CBError cBError) {
        Object obj;
        String str;
        String str2;
        e.a[] aVarArr = new e.a[5];
        aVarArr[0] = e.a("endpoint", e());
        if (aiVar == null) {
            obj = "None";
        } else {
            obj = Integer.valueOf(aiVar.a);
        }
        aVarArr[1] = e.a("statuscode", obj);
        if (cBError == null) {
            str = "None";
        } else {
            str = cBError.a().toString();
        }
        aVarArr[2] = e.a("error", str);
        if (cBError == null) {
            str2 = "None";
        } else {
            str2 = cBError.b();
        }
        aVarArr[3] = e.a("errorDescription", str2);
        aVarArr[4] = e.a("retryCount", (Object) 0);
        this.p.a("request_manager", ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, cBError == null ? "success" : "failure", (String) null, (String) null, (String) null, e.a(aVarArr));
    }

    public void b(String str) {
        this.o = str;
    }

    public ag a() {
        c();
        String jSONObject = this.a.toString();
        String str = i.l;
        String str2 = i.m;
        String b = c.b(c.a(String.format(Locale.US, "%s %s\n%s\n%s", this.b, d(), str2, jSONObject).getBytes()));
        HashMap hashMap = new HashMap();
        hashMap.put("Accept", "application/json");
        hashMap.put("X-Chartboost-Client", CBUtility.b());
        hashMap.put("X-Chartboost-API", "7.5.0");
        hashMap.put("X-Chartboost-App", str);
        hashMap.put("X-Chartboost-Signature", b);
        return new ag(hashMap, jSONObject.getBytes(), "application/json");
    }

    public ah<JSONObject> a(ai aiVar) {
        try {
            if (aiVar.b == null) {
                return ah.a(new CBError(CBError.a.INVALID_RESPONSE, "Response is not a valid json object"));
            }
            JSONObject jSONObject = new JSONObject(new String(aiVar.b));
            CBLogging.c("CBRequest", "Request " + e() + " succeeded. Response code: " + aiVar.a + ", body: " + jSONObject.toString(4));
            if (this.l) {
                int optInt = jSONObject.optInt("status");
                if (optInt == 404) {
                    return ah.a(new CBError(CBError.a.HTTP_NOT_FOUND, "404 error from server"));
                }
                if (optInt < 200 || optInt > 299) {
                    String str = "Request failed due to status code " + optInt + " in message";
                    CBLogging.b("CBRequest", str);
                    return ah.a(new CBError(CBError.a.UNEXPECTED_RESPONSE, str));
                }
            }
            return ah.a(jSONObject);
        } catch (Exception e) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "parseServerResponse", e);
            return ah.a(new CBError(CBError.a.MISCELLANEOUS, e.getLocalizedMessage()));
        }
    }

    public void a(JSONObject jSONObject, ai aiVar) {
        if (!(this.k == null || jSONObject == null)) {
            this.k.a(this, jSONObject);
        }
        if (this.p != null) {
            a(aiVar, (CBError) null);
        }
    }

    public void a(CBError cBError, ai aiVar) {
        if (cBError != null) {
            if (this.k != null) {
                this.k.a(this, cBError);
            }
            if (this.p != null) {
                a(aiVar, cBError);
            }
        }
    }
}
