package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.h;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class an {
    private final f a;
    private final Map<String, h.a> b = new HashMap();

    public an(f fVar) {
        this.a = fVar;
    }

    public h.a a(String str) {
        if (b(str)) {
            if (this.b.containsKey(str)) {
                return this.b.get(str);
            }
            h.a aVar = new h.a(str, new File(this.a.d().d, String.format("%s%s", str, ".png")), this.a);
            this.b.put(str, aVar);
            return aVar;
        } else if (!this.b.containsKey(str)) {
            return null;
        } else {
            this.b.remove(str);
            return null;
        }
    }

    private boolean b(String str) {
        return this.a.b(String.format("%s%s", str, ".png"));
    }
}
