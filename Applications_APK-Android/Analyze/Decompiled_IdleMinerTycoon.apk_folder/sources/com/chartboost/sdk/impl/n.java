package com.chartboost.sdk.impl;

public class n {
    /* JADX WARNING: Can't wrap try/catch for region: R(5:39|40|41|42|43) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00ed */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0114 A[SYNTHETIC, Splitter:B:65:0x0114] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0119 A[SYNTHETIC, Splitter:B:69:0x0119] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.File r11, java.util.Map<java.lang.String, java.lang.String> r12) throws java.lang.Exception {
        /*
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ OutOfMemoryError -> 0x0108, all -> 0x0104 }
            r1.<init>(r11)     // Catch:{ OutOfMemoryError -> 0x0108, all -> 0x0104 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ OutOfMemoryError -> 0x0100, all -> 0x00fd }
            r2.<init>(r1)     // Catch:{ OutOfMemoryError -> 0x0100, all -> 0x00fd }
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r0.<init>()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Set r12 = r12.entrySet()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Iterator r12 = r12.iterator()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x0018:
            boolean r3 = r12.hasNext()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r3 == 0) goto L_0x0042
            java.lang.Object r3 = r12.next()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.Object r4 = r3.getKey()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r5 = "{{"
            boolean r5 = r4.startsWith(r5)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r5 != 0) goto L_0x003a
            java.lang.String r5 = "{%"
            boolean r5 = r4.startsWith(r5)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r5 == 0) goto L_0x0018
        L_0x003a:
            java.lang.Object r3 = r3.getValue()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r0.put(r4, r3)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            goto L_0x0018
        L_0x0042:
            java.util.Set r12 = r0.entrySet()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Iterator r0 = r12.iterator()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r3 = 0
            r4 = 0
        L_0x004c:
            boolean r5 = r0.hasNext()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r5 == 0) goto L_0x0066
            java.lang.Object r5 = r0.next()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.Object r5 = r5.getValue()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r5 = r5.length()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r5 = r5 * 3
            int r4 = r4 + r5
            goto L_0x004c
        L_0x0066:
            long r5 = r11.length()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r11 = (int) r5     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r11 = r11 + r4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r0.<init>(r11)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r4 = 2048(0x800, float:2.87E-42)
            r11.<init>(r4)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x0078:
            java.lang.String r4 = r2.readLine()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r4 == 0) goto L_0x00de
            java.lang.String r5 = "{{"
            int r5 = r4.indexOf(r5)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r6 = "{%"
            int r6 = r4.indexOf(r6)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r7 = -1
            if (r5 == r7) goto L_0x0094
            if (r6 == r7) goto L_0x0094
            int r5 = java.lang.Math.min(r5, r6)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            goto L_0x0098
        L_0x0094:
            int r5 = java.lang.Math.max(r5, r6)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x0098:
            if (r5 != r7) goto L_0x009e
            r0.append(r4)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            goto L_0x00d8
        L_0x009e:
            r11.setLength(r3)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            r11.append(r4)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Iterator r4 = r12.iterator()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x00a8:
            boolean r6 = r4.hasNext()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r6 == 0) goto L_0x00d5
            java.lang.Object r6 = r4.next()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.util.Map$Entry r6 = (java.util.Map.Entry) r6     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.Object r8 = r6.getKey()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.Object r6 = r6.getValue()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r9 = r8.length()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x00c4:
            int r5 = r11.indexOf(r8, r5)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r7 == r5) goto L_0x00a8
            int r10 = r5 + r9
            r11.replace(r5, r10, r6)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r10 = r6.length()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            int r5 = r5 + r10
            goto L_0x00c4
        L_0x00d5:
            r0.append(r11)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x00d8:
            java.lang.String r4 = "\n"
            r0.append(r4)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            goto L_0x0078
        L_0x00de:
            java.lang.String r11 = r0.toString()     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r12 = "{{"
            boolean r12 = r11.contains(r12)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            if (r12 != 0) goto L_0x00f1
            r2.close()     // Catch:{ IOException -> 0x00ed }
        L_0x00ed:
            r1.close()     // Catch:{ IOException -> 0x00f0 }
        L_0x00f0:
            return r11
        L_0x00f1:
            java.lang.IllegalArgumentException r11 = new java.lang.IllegalArgumentException     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            java.lang.String r12 = "Missing required template parameter"
            r11.<init>(r12)     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
            throw r11     // Catch:{ OutOfMemoryError -> 0x00fb, all -> 0x00f9 }
        L_0x00f9:
            r11 = move-exception
            goto L_0x0112
        L_0x00fb:
            r11 = move-exception
            goto L_0x0102
        L_0x00fd:
            r11 = move-exception
            r2 = r0
            goto L_0x0112
        L_0x0100:
            r11 = move-exception
            r2 = r0
        L_0x0102:
            r0 = r1
            goto L_0x010a
        L_0x0104:
            r11 = move-exception
            r1 = r0
            r2 = r1
            goto L_0x0112
        L_0x0108:
            r11 = move-exception
            r2 = r0
        L_0x010a:
            java.lang.Exception r12 = new java.lang.Exception     // Catch:{ all -> 0x0110 }
            r12.<init>(r11)     // Catch:{ all -> 0x0110 }
            throw r12     // Catch:{ all -> 0x0110 }
        L_0x0110:
            r11 = move-exception
            r1 = r0
        L_0x0112:
            if (r2 == 0) goto L_0x0117
            r2.close()     // Catch:{ IOException -> 0x0117 }
        L_0x0117:
            if (r1 == 0) goto L_0x011c
            r1.close()     // Catch:{ IOException -> 0x011c }
        L_0x011c:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.n.a(java.io.File, java.util.Map):java.lang.String");
    }
}
