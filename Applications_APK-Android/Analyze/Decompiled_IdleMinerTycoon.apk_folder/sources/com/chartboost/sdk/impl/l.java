package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import java.io.File;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class l {
    int a = 1;
    private final Executor b;
    private final aj c;
    private final ak d;
    private final AtomicReference<e> e;
    private final i f;
    private final a g;
    private final f h;
    private k i = null;
    private final PriorityQueue<j> j;

    public l(Executor executor, f fVar, aj ajVar, ak akVar, AtomicReference<e> atomicReference, i iVar, a aVar) {
        this.b = executor;
        this.h = fVar;
        this.c = ajVar;
        this.d = akVar;
        this.e = atomicReference;
        this.f = iVar;
        this.g = aVar;
        this.j = new PriorityQueue<>();
    }

    public synchronized void a(int i2, Map<String, b> map, AtomicInteger atomicInteger, h hVar) {
        long b2 = this.f.b();
        AtomicInteger atomicInteger2 = new AtomicInteger();
        AtomicReference atomicReference = new AtomicReference(hVar);
        for (b next : map.values()) {
            long j2 = b2;
            long j3 = b2;
            j jVar = r2;
            j jVar2 = new j(this.f, i2, next.b, next.c, next.a, atomicInteger, atomicReference, j2, atomicInteger2);
            this.j.add(jVar);
            b2 = j3;
        }
        if (this.a == 1 || this.a == 2) {
            d();
        }
    }

    public synchronized void a(AtomicInteger atomicInteger) {
        atomicInteger.set(-10000);
        switch (this.a) {
            case 2:
                if ((this.i.a.e == atomicInteger) && this.i.b()) {
                    this.i = null;
                    d();
                    break;
                }
        }
    }

    public synchronized void a() {
        switch (this.a) {
            case 1:
                CBLogging.a("Downloader", "Change state to PAUSED");
                this.a = 4;
                break;
            case 2:
                if (!this.i.b()) {
                    CBLogging.a("Downloader", "Change state to PAUSING");
                    this.a = 3;
                    break;
                } else {
                    this.j.add(this.i.a);
                    this.i = null;
                    CBLogging.a("Downloader", "Change state to PAUSED");
                    this.a = 4;
                    break;
                }
        }
    }

    public synchronized void b() {
        switch (this.a) {
            case 3:
                CBLogging.a("Downloader", "Change state to DOWNLOADING");
                this.a = 2;
                break;
            case 4:
                CBLogging.a("Downloader", "Change state to IDLE");
                this.a = 1;
                d();
                break;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00d2, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.chartboost.sdk.impl.k r18, com.chartboost.sdk.Model.CBError r19, com.chartboost.sdk.impl.ai r20) {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            r3 = r20
            monitor-enter(r17)
            int r4 = r1.a     // Catch:{ all -> 0x00d3 }
            switch(r4) {
                case 1: goto L_0x00d1;
                case 2: goto L_0x000e;
                case 3: goto L_0x000e;
                default: goto L_0x000c;
            }     // Catch:{ all -> 0x00d3 }
        L_0x000c:
            goto L_0x00d1
        L_0x000e:
            com.chartboost.sdk.impl.k r4 = r1.i     // Catch:{ all -> 0x00d3 }
            if (r0 == r4) goto L_0x0014
            monitor-exit(r17)
            return
        L_0x0014:
            com.chartboost.sdk.impl.j r4 = r0.a     // Catch:{ all -> 0x00d3 }
            r5 = 0
            r1.i = r5     // Catch:{ all -> 0x00d3 }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d3 }
            long r6 = r0.g     // Catch:{ all -> 0x00d3 }
            long r11 = r5.toMillis(r6)     // Catch:{ all -> 0x00d3 }
            java.util.concurrent.atomic.AtomicInteger r5 = r4.f     // Catch:{ all -> 0x00d3 }
            int r6 = (int) r11     // Catch:{ all -> 0x00d3 }
            r5.addAndGet(r6)     // Catch:{ all -> 0x00d3 }
            java.util.concurrent.Executor r5 = r1.b     // Catch:{ all -> 0x00d3 }
            if (r19 != 0) goto L_0x002d
            r6 = 1
            goto L_0x002e
        L_0x002d:
            r6 = 0
        L_0x002e:
            r4.a(r5, r6)     // Catch:{ all -> 0x00d3 }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d3 }
            long r6 = r0.h     // Catch:{ all -> 0x00d3 }
            long r13 = r5.toMillis(r6)     // Catch:{ all -> 0x00d3 }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d3 }
            long r6 = r0.i     // Catch:{ all -> 0x00d3 }
            long r15 = r5.toMillis(r6)     // Catch:{ all -> 0x00d3 }
            if (r19 != 0) goto L_0x0066
            com.chartboost.sdk.Tracking.a r8 = r1.g     // Catch:{ all -> 0x00d3 }
            java.lang.String r9 = r4.c     // Catch:{ all -> 0x00d3 }
            r10 = r11
            r12 = r13
            r14 = r15
            r8.a(r9, r10, r12, r14)     // Catch:{ all -> 0x00d3 }
            java.lang.String r0 = "Downloader"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r2.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r3 = "Downloaded "
            r2.append(r3)     // Catch:{ all -> 0x00d3 }
            java.lang.String r3 = r4.c     // Catch:{ all -> 0x00d3 }
            r2.append(r3)     // Catch:{ all -> 0x00d3 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d3 }
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r2)     // Catch:{ all -> 0x00d3 }
            goto L_0x00be
        L_0x0066:
            java.lang.String r0 = r19.b()     // Catch:{ all -> 0x00d3 }
            com.chartboost.sdk.Tracking.a r8 = r1.g     // Catch:{ all -> 0x00d3 }
            java.lang.String r9 = r4.c     // Catch:{ all -> 0x00d3 }
            r10 = r0
            r8.a(r9, r10, r11, r13, r15)     // Catch:{ all -> 0x00d3 }
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r5.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r6 = "Failed to download "
            r5.append(r6)     // Catch:{ all -> 0x00d3 }
            java.lang.String r4 = r4.c     // Catch:{ all -> 0x00d3 }
            r5.append(r4)     // Catch:{ all -> 0x00d3 }
            if (r3 == 0) goto L_0x0099
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r4.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r6 = " Status code="
            r4.append(r6)     // Catch:{ all -> 0x00d3 }
            int r3 = r3.a     // Catch:{ all -> 0x00d3 }
            r4.append(r3)     // Catch:{ all -> 0x00d3 }
            java.lang.String r3 = r4.toString()     // Catch:{ all -> 0x00d3 }
            goto L_0x009b
        L_0x0099:
            java.lang.String r3 = ""
        L_0x009b:
            r5.append(r3)     // Catch:{ all -> 0x00d3 }
            if (r0 == 0) goto L_0x00b2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r3.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r4 = " Error message="
            r3.append(r4)     // Catch:{ all -> 0x00d3 }
            r3.append(r0)     // Catch:{ all -> 0x00d3 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x00d3 }
            goto L_0x00b4
        L_0x00b2:
            java.lang.String r0 = ""
        L_0x00b4:
            r5.append(r0)     // Catch:{ all -> 0x00d3 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x00d3 }
            com.chartboost.sdk.Libraries.CBLogging.a(r2, r0)     // Catch:{ all -> 0x00d3 }
        L_0x00be:
            int r0 = r1.a     // Catch:{ all -> 0x00d3 }
            r2 = 3
            if (r0 != r2) goto L_0x00ce
            java.lang.String r0 = "Downloader"
            java.lang.String r2 = "Change state to PAUSED"
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r2)     // Catch:{ all -> 0x00d3 }
            r0 = 4
            r1.a = r0     // Catch:{ all -> 0x00d3 }
            goto L_0x00d1
        L_0x00ce:
            r17.d()     // Catch:{ all -> 0x00d3 }
        L_0x00d1:
            monitor-exit(r17)
            return
        L_0x00d3:
            r0 = move-exception
            monitor-exit(r17)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.l.a(com.chartboost.sdk.impl.k, com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ai):void");
    }

    private void d() {
        j poll;
        j peek;
        if (this.i != null && (peek = this.j.peek()) != null && this.i.a.a > peek.a && this.i.b()) {
            this.j.add(this.i.a);
            this.i = null;
        }
        while (this.i == null && (poll = this.j.poll()) != null) {
            if (poll.e.get() > 0) {
                File file = new File(this.h.d().a, poll.d);
                if (file.exists() || file.mkdirs() || file.isDirectory()) {
                    File file2 = new File(file, poll.b);
                    if (file2.exists()) {
                        this.h.c(file2);
                        poll.a(this.b, true);
                    } else {
                        this.i = new k(this, this.d, poll, file2);
                        this.c.a(this.i);
                        this.g.a(poll.c, poll.b);
                    }
                } else {
                    CBLogging.b("Downloader", "Unable to create directory " + file.getPath());
                    poll.a(this.b, false);
                }
            }
        }
        if (this.i != null) {
            if (this.a != 2) {
                CBLogging.a("Downloader", "Change state to DOWNLOADING");
                this.a = 2;
            }
        } else if (this.a != 1) {
            CBLogging.a("Downloader", "Change state to IDLE");
            this.a = 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.Libraries.CBUtility.a(java.io.File, boolean):java.util.ArrayList<java.io.File>
     arg types: [java.io.File, int]
     candidates:
      com.chartboost.sdk.Libraries.CBUtility.a(float, android.content.Context):float
      com.chartboost.sdk.Libraries.CBUtility.a(int, android.content.Context):int
      com.chartboost.sdk.Libraries.CBUtility.a(java.io.File, boolean):java.util.ArrayList<java.io.File> */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0144 A[Catch:{ Exception -> 0x019b }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0186 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void c() {
        /*
            r23 = this;
            r1 = r23
            monitor-enter(r23)
            int r0 = r1.a     // Catch:{ all -> 0x01a7 }
            r2 = 1
            if (r0 == r2) goto L_0x000a
            monitor-exit(r23)
            return
        L_0x000a:
            java.lang.String r0 = "Downloader"
            java.lang.String r3 = "########### Trimming the disk cache"
            com.chartboost.sdk.Libraries.CBLogging.a(r0, r3)     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.f r0 = r1.h     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.g r0 = r0.d()     // Catch:{ Exception -> 0x019b }
            java.io.File r0 = r0.a     // Catch:{ Exception -> 0x019b }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x019b }
            r3.<init>()     // Catch:{ Exception -> 0x019b }
            java.lang.String[] r4 = r0.list()     // Catch:{ Exception -> 0x019b }
            if (r4 == 0) goto L_0x0065
            int r6 = r4.length     // Catch:{ Exception -> 0x019b }
            if (r6 <= 0) goto L_0x0065
            int r6 = r4.length     // Catch:{ Exception -> 0x019b }
            r7 = 0
        L_0x0029:
            if (r7 >= r6) goto L_0x0065
            r8 = r4[r7]     // Catch:{ Exception -> 0x019b }
            java.lang.String r9 = "requests"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x019b }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "track"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x019b }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "session"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x019b }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "videoCompletionEvents"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x019b }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "."
            boolean r9 = r8.contains(r9)     // Catch:{ Exception -> 0x019b }
            if (r9 == 0) goto L_0x0056
            goto L_0x0062
        L_0x0056:
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x019b }
            r9.<init>(r0, r8)     // Catch:{ Exception -> 0x019b }
            java.util.ArrayList r8 = com.chartboost.sdk.Libraries.CBUtility.a(r9, r2)     // Catch:{ Exception -> 0x019b }
            r3.addAll(r8)     // Catch:{ Exception -> 0x019b }
        L_0x0062:
            int r7 = r7 + 1
            goto L_0x0029
        L_0x0065:
            int r0 = r3.size()     // Catch:{ Exception -> 0x019b }
            java.io.File[] r0 = new java.io.File[r0]     // Catch:{ Exception -> 0x019b }
            r3.toArray(r0)     // Catch:{ Exception -> 0x019b }
            int r3 = r0.length     // Catch:{ Exception -> 0x019b }
            if (r3 <= r2) goto L_0x0079
            com.chartboost.sdk.impl.l$1 r3 = new com.chartboost.sdk.impl.l$1     // Catch:{ Exception -> 0x019b }
            r3.<init>()     // Catch:{ Exception -> 0x019b }
            java.util.Arrays.sort(r0, r3)     // Catch:{ Exception -> 0x019b }
        L_0x0079:
            int r3 = r0.length     // Catch:{ Exception -> 0x019b }
            if (r3 <= 0) goto L_0x018f
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.Model.e> r3 = r1.e     // Catch:{ Exception -> 0x019b }
            java.lang.Object r3 = r3.get()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Model.e r3 = (com.chartboost.sdk.Model.e) r3     // Catch:{ Exception -> 0x019b }
            int r4 = r3.u     // Catch:{ Exception -> 0x019b }
            long r6 = (long) r4     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.f r4 = r1.h     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.f r8 = r1.h     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.g r8 = r8.d()     // Catch:{ Exception -> 0x019b }
            java.io.File r8 = r8.g     // Catch:{ Exception -> 0x019b }
            long r8 = r4.b(r8)     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.i r4 = r1.f     // Catch:{ Exception -> 0x019b }
            long r10 = r4.a()     // Catch:{ Exception -> 0x019b }
            java.util.List<java.lang.String> r4 = r3.d     // Catch:{ Exception -> 0x019b }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b }
            r13.<init>()     // Catch:{ Exception -> 0x019b }
            java.lang.String r14 = "Total local file count:"
            r13.append(r14)     // Catch:{ Exception -> 0x019b }
            int r14 = r0.length     // Catch:{ Exception -> 0x019b }
            r13.append(r14)     // Catch:{ Exception -> 0x019b }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r13)     // Catch:{ Exception -> 0x019b }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b }
            r13.<init>()     // Catch:{ Exception -> 0x019b }
            java.lang.String r14 = "Video Folder Size in bytes :"
            r13.append(r14)     // Catch:{ Exception -> 0x019b }
            r13.append(r8)     // Catch:{ Exception -> 0x019b }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r13)     // Catch:{ Exception -> 0x019b }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b }
            r13.<init>()     // Catch:{ Exception -> 0x019b }
            java.lang.String r14 = "Max Bytes allowed:"
            r13.append(r14)     // Catch:{ Exception -> 0x019b }
            r13.append(r6)     // Catch:{ Exception -> 0x019b }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.CBLogging.a(r12, r13)     // Catch:{ Exception -> 0x019b }
            int r12 = r0.length     // Catch:{ Exception -> 0x019b }
            r13 = r8
            r8 = 0
        L_0x00e3:
            if (r8 >= r12) goto L_0x018f
            r9 = r0[r8]     // Catch:{ Exception -> 0x019b }
            java.util.concurrent.TimeUnit r15 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ Exception -> 0x019b }
            long r16 = r9.lastModified()     // Catch:{ Exception -> 0x019b }
            r18 = 0
            r19 = r6
            long r5 = r10 - r16
            long r5 = r15.toDays(r5)     // Catch:{ Exception -> 0x019b }
            int r7 = r3.w     // Catch:{ Exception -> 0x019b }
            r21 = r3
            long r2 = (long) r7     // Catch:{ Exception -> 0x019b }
            int r7 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r7 < 0) goto L_0x0102
            r2 = 1
            goto L_0x0103
        L_0x0102:
            r2 = 0
        L_0x0103:
            java.lang.String r3 = r9.getName()     // Catch:{ Exception -> 0x019b }
            java.lang.String r5 = ".tmp"
            boolean r3 = r3.endsWith(r5)     // Catch:{ Exception -> 0x019b }
            java.io.File r5 = r9.getParentFile()     // Catch:{ Exception -> 0x019b }
            java.lang.String r6 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x019b }
            java.lang.String r7 = "/videos"
            boolean r6 = r6.contains(r7)     // Catch:{ Exception -> 0x019b }
            int r7 = (r13 > r19 ? 1 : (r13 == r19 ? 0 : -1))
            if (r7 <= 0) goto L_0x0123
            if (r6 == 0) goto L_0x0123
            r7 = 1
            goto L_0x0124
        L_0x0123:
            r7 = 0
        L_0x0124:
            long r15 = r9.length()     // Catch:{ Exception -> 0x019b }
            r17 = 0
            int r22 = (r15 > r17 ? 1 : (r15 == r17 ? 0 : -1))
            if (r22 == 0) goto L_0x0141
            if (r3 != 0) goto L_0x0141
            if (r2 != 0) goto L_0x0141
            java.lang.String r2 = r5.getName()     // Catch:{ Exception -> 0x019b }
            boolean r2 = r4.contains(r2)     // Catch:{ Exception -> 0x019b }
            if (r2 != 0) goto L_0x0141
            if (r7 == 0) goto L_0x013f
            goto L_0x0141
        L_0x013f:
            r2 = 0
            goto L_0x0142
        L_0x0141:
            r2 = 1
        L_0x0142:
            if (r2 == 0) goto L_0x0186
            if (r6 == 0) goto L_0x014c
            long r2 = r9.length()     // Catch:{ Exception -> 0x019b }
            r5 = 0
            long r13 = r13 - r2
        L_0x014c:
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b }
            r3.<init>()     // Catch:{ Exception -> 0x019b }
            java.lang.String r5 = "Deleting file at path:"
            r3.append(r5)     // Catch:{ Exception -> 0x019b }
            java.lang.String r5 = r9.getPath()     // Catch:{ Exception -> 0x019b }
            r3.append(r5)     // Catch:{ Exception -> 0x019b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.CBLogging.a(r2, r3)     // Catch:{ Exception -> 0x019b }
            boolean r2 = r9.delete()     // Catch:{ Exception -> 0x019b }
            if (r2 != 0) goto L_0x0186
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b }
            r3.<init>()     // Catch:{ Exception -> 0x019b }
            java.lang.String r5 = "Unable to delete "
            r3.append(r5)     // Catch:{ Exception -> 0x019b }
            java.lang.String r5 = r9.getPath()     // Catch:{ Exception -> 0x019b }
            r3.append(r5)     // Catch:{ Exception -> 0x019b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Libraries.CBLogging.b(r2, r3)     // Catch:{ Exception -> 0x019b }
        L_0x0186:
            int r8 = r8 + 1
            r6 = r19
            r3 = r21
            r2 = 1
            goto L_0x00e3
        L_0x018f:
            com.chartboost.sdk.Libraries.f r0 = r1.h     // Catch:{ Exception -> 0x019b }
            org.json.JSONObject r0 = r0.e()     // Catch:{ Exception -> 0x019b }
            com.chartboost.sdk.Tracking.a r2 = r1.g     // Catch:{ Exception -> 0x019b }
            r2.a(r0)     // Catch:{ Exception -> 0x019b }
            goto L_0x01a5
        L_0x019b:
            r0 = move-exception
            java.lang.Class r2 = r23.getClass()     // Catch:{ all -> 0x01a7 }
            java.lang.String r3 = "reduceCacheSize"
            com.chartboost.sdk.Tracking.a.a(r2, r3, r0)     // Catch:{ all -> 0x01a7 }
        L_0x01a5:
            monitor-exit(r23)
            return
        L_0x01a7:
            r0 = move-exception
            monitor-exit(r23)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.l.c():void");
    }
}
