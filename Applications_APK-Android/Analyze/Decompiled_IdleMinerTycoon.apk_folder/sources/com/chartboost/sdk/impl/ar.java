package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.g;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class ar {
    final d a;
    final ak b;
    final AtomicReference<e> c;
    final SharedPreferences d;
    final i e;
    final String f;
    final String g;
    final String h;
    final String i;
    String j;
    String k;
    final String l;
    final Integer m;
    final Integer n;
    final Integer o;
    final Integer p;
    final String q;
    final Float r;
    final String s;
    final String t;
    final String u;
    final JSONObject v;
    final boolean w;
    final String x;
    final Integer y;

    public ar(Context context, String str, d dVar, ak akVar, AtomicReference<e> atomicReference, SharedPreferences sharedPreferences, i iVar) {
        JSONObject jSONObject;
        int i2;
        int i3;
        WindowManager windowManager;
        String str2;
        String str3;
        this.a = dVar;
        this.b = akVar;
        this.c = atomicReference;
        this.d = sharedPreferences;
        this.e = iVar;
        this.s = str;
        if ("sdk".equals(Build.PRODUCT) || CommonUtils.GOOGLE_SDK.equals(Build.PRODUCT) || (Build.MANUFACTURER != null && Build.MANUFACTURER.contains("Genymotion"))) {
            this.f = "Android Simulator";
        } else {
            this.f = Build.MODEL;
        }
        this.t = Build.MANUFACTURER + " " + Build.MODEL;
        this.u = at.a(context);
        this.g = "Android " + Build.VERSION.RELEASE;
        this.h = Locale.getDefault().getCountry();
        this.i = Locale.getDefault().getLanguage();
        this.l = "7.5.0";
        this.r = Float.valueOf(context.getResources().getDisplayMetrics().density);
        try {
            String packageName = context.getPackageName();
            this.j = context.getPackageManager().getPackageInfo(packageName, 128).versionName;
            this.k = packageName;
        } catch (Exception e2) {
            CBLogging.a("RequestBody", "Exception raised getting package mager object", e2);
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        int i4 = 0;
        if (telephonyManager == null || telephonyManager.getPhoneType() == 0 || telephonyManager.getSimState() != 5) {
            jSONObject = new JSONObject();
        } else {
            String str4 = null;
            try {
                str2 = telephonyManager.getSimOperator();
            } catch (Exception e3) {
                a.a(Chartboost.class, "Unable to retrieve sim operator information", e3);
                str2 = null;
            }
            if (str2 == null || TextUtils.isEmpty(str2)) {
                str3 = null;
            } else {
                str4 = str2.substring(0, 3);
                str3 = str2.substring(3);
            }
            jSONObject = com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("carrier-name", telephonyManager.getNetworkOperatorName()), com.chartboost.sdk.Libraries.e.a("mobile-country-code", str4), com.chartboost.sdk.Libraries.e.a("mobile-network-code", str3), com.chartboost.sdk.Libraries.e.a("iso-country-code", telephonyManager.getNetworkCountryIso()), com.chartboost.sdk.Libraries.e.a("phone-type", Integer.valueOf(telephonyManager.getPhoneType())));
        }
        this.v = jSONObject;
        this.w = CBUtility.c();
        this.x = CBUtility.d();
        this.y = Integer.valueOf(at.b(context));
        try {
            if (context instanceof Activity) {
                Rect rect = new Rect();
                ((Activity) context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                i2 = rect.width();
                try {
                    i4 = rect.height();
                } catch (Exception e4) {
                    e = e4;
                    CBLogging.b("RequestBody", "Exception getting activity size", e);
                    DisplayMetrics displayMetrics = (DisplayMetrics) g.a().a(new DisplayMetrics());
                    displayMetrics.setTo(context.getResources().getDisplayMetrics());
                    windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);
                    i3 = displayMetrics.widthPixels;
                    int i5 = displayMetrics.heightPixels;
                    this.o = Integer.valueOf(i3);
                    this.p = Integer.valueOf(i5);
                    this.q = "" + displayMetrics.densityDpi;
                    i3 = i2;
                    this.m = Integer.valueOf(i3);
                    this.n = Integer.valueOf(i4);
                }
            } else {
                i2 = 0;
            }
        } catch (Exception e5) {
            e = e5;
            i2 = 0;
            CBLogging.b("RequestBody", "Exception getting activity size", e);
            DisplayMetrics displayMetrics2 = (DisplayMetrics) g.a().a(new DisplayMetrics());
            displayMetrics2.setTo(context.getResources().getDisplayMetrics());
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics2);
            i3 = displayMetrics2.widthPixels;
            int i52 = displayMetrics2.heightPixels;
            this.o = Integer.valueOf(i3);
            this.p = Integer.valueOf(i52);
            this.q = "" + displayMetrics2.densityDpi;
            i3 = i2;
            this.m = Integer.valueOf(i3);
            this.n = Integer.valueOf(i4);
        }
        DisplayMetrics displayMetrics22 = (DisplayMetrics) g.a().a(new DisplayMetrics());
        displayMetrics22.setTo(context.getResources().getDisplayMetrics());
        if (Build.VERSION.SDK_INT >= 17 && (windowManager = (WindowManager) context.getSystemService("window")) != null) {
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics22);
        }
        i3 = displayMetrics22.widthPixels;
        int i522 = displayMetrics22.heightPixels;
        this.o = Integer.valueOf(i3);
        this.p = Integer.valueOf(i522);
        this.q = "" + displayMetrics22.densityDpi;
        if (i2 > 0 && i2 <= i3) {
            i3 = i2;
        }
        i4 = (i4 <= 0 || i4 > i522) ? i522 : i4;
        this.m = Integer.valueOf(i3);
        this.n = Integer.valueOf(i4);
    }
}
