package com.chartboost.sdk.impl;

import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.c;
import com.chartboost.sdk.d;
import com.chartboost.sdk.g;
import com.chartboost.sdk.impl.c;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.AnalyticsEvents;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    private final long A = TimeUnit.SECONDS.toNanos(1);
    private final String[] B = {"ASKED_TO_CACHE", "ASKED_TO_SHOW", "REQUESTING_TO_CACHE", "REQUESTING_TO_SHOW", "DOWNLOADING_TO_CACHE", "DOWNLOADING_TO_SHOW", "READY", "ASKING_UI_TO_SHOW_AD", "DONE"};
    final ScheduledExecutorService a;
    public final f b;
    final i c;
    final Handler d;
    final c e;
    final c f;
    int g = 0;
    final Map<String, f> h;
    final SortedSet<f> i;
    final SortedSet<f> j;
    ScheduledFuture<?> k;
    private final l l;
    private final aj m;
    private final ak n;
    private final ar o;
    private final AtomicReference<com.chartboost.sdk.Model.e> p;
    private final SharedPreferences q;
    private final com.chartboost.sdk.Tracking.a r;
    private final am s;
    private final d t;
    private final an u;
    private int v;
    private boolean w;
    private final Map<String, Long> x;
    private final Map<String, Integer> y;
    private final long z = TimeUnit.SECONDS.toNanos(5);

    public e(c cVar, ScheduledExecutorService scheduledExecutorService, l lVar, f fVar, aj ajVar, ak akVar, ar arVar, AtomicReference<com.chartboost.sdk.Model.e> atomicReference, SharedPreferences sharedPreferences, i iVar, com.chartboost.sdk.Tracking.a aVar, Handler handler, c cVar2, am amVar, d dVar, an anVar) {
        this.a = scheduledExecutorService;
        this.l = lVar;
        this.b = fVar;
        this.m = ajVar;
        this.n = akVar;
        this.o = arVar;
        this.p = atomicReference;
        this.q = sharedPreferences;
        this.c = iVar;
        this.r = aVar;
        this.d = handler;
        this.e = cVar2;
        this.s = amVar;
        this.t = dVar;
        this.u = anVar;
        this.f = cVar;
        this.v = 1;
        this.h = new HashMap();
        this.j = new TreeSet();
        this.i = new TreeSet();
        this.x = new HashMap();
        this.y = new HashMap();
        this.w = false;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.g == 0) {
            this.g = 1;
            b();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.w) {
            try {
                this.w = true;
                d();
                if (this.g == 1 && !a(this.j, 1, 3, 1, "show")) {
                    a(this.i, 0, 2, 2, "cache");
                }
                c();
                this.w = false;
            } catch (Throwable th) {
                this.w = false;
                throw th;
            }
        }
    }

    private void c() {
        Long l2;
        boolean z2 = true;
        if (this.g == 1) {
            long b2 = this.c.b();
            l2 = null;
            for (Map.Entry next : this.x.entrySet()) {
                if (this.h.get((String) next.getKey()) != null) {
                    long max = Math.max(this.z, ((Long) next.getValue()).longValue() - b2);
                    if (l2 == null || max < l2.longValue()) {
                        l2 = Long.valueOf(max);
                    }
                }
            }
        } else {
            l2 = null;
        }
        if (!(l2 == null || this.k == null)) {
            if (Math.abs(l2.longValue() - this.k.getDelay(TimeUnit.NANOSECONDS)) > TimeUnit.SECONDS.toNanos(5)) {
                z2 = false;
            }
            if (z2) {
                return;
            }
        }
        if (this.k != null) {
            this.k.cancel(false);
            this.k = null;
        }
        if (l2 != null) {
            this.k = this.a.schedule(new a(2, null, null, null), l2.longValue(), TimeUnit.NANOSECONDS);
        }
    }

    private boolean a(SortedSet<f> sortedSet, int i2, int i3, int i4, String str) {
        Iterator<f> it = sortedSet.iterator();
        while (it.hasNext()) {
            f next = it.next();
            if (next.c != i2 || next.d != null) {
                it.remove();
            } else if (e(next.b)) {
                continue;
            } else if (!this.f.g(next.b)) {
                next.c = 8;
                this.h.remove(next.b);
                it.remove();
            } else {
                next.c = i3;
                it.remove();
                a(next, i4, str);
                return true;
            }
        }
        return false;
    }

    public synchronized com.chartboost.sdk.Model.a a(String str) {
        f fVar = this.h.get(str);
        if (fVar == null || (fVar.c != 6 && fVar.c != 7)) {
            return null;
        }
        return fVar.d;
    }

    public synchronized boolean a(String str, String str2, Map<String, b> map) {
        int i2 = this.v;
        this.v = i2 + 1;
        f fVar = new f(i2, str, 6);
        fVar.e = 1;
        try {
            fVar.d = new t(new JSONObject(str2));
            fVar.d.C = map.values().iterator().next();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        this.h.put(str, fVar);
        this.i.add(fVar);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (e()) {
            c cVar = this.f;
            cVar.getClass();
            this.d.postDelayed(new c.a(4, str, CBError.CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        f fVar = this.h.get(str);
        if (fVar != null && fVar.c == 6 && !a(fVar.d)) {
            this.h.remove(str);
            fVar = null;
        }
        if (fVar == null) {
            int i2 = this.v;
            this.v = i2 + 1;
            fVar = new f(i2, str, 0);
            this.h.put(str, fVar);
            this.i.add(fVar);
        }
        fVar.f = true;
        if (fVar.h == null) {
            fVar.h = Long.valueOf(this.c.b());
        }
        switch (fVar.c) {
            case 6:
            case 7:
                Handler handler = this.d;
                c cVar2 = this.f;
                cVar2.getClass();
                handler.post(new c.a(0, str, null));
                break;
        }
        b();
    }

    private boolean a(com.chartboost.sdk.Model.a aVar) {
        File file = this.b.d().a;
        for (b next : aVar.n.values()) {
            if (!next.a(file).exists()) {
                CBLogging.b("AdUnitManager", "Asset does not exist: " + next.b);
                return false;
            }
        }
        return true;
    }

    private void d() {
        long b2 = this.c.b();
        Iterator<Long> it = this.x.values().iterator();
        while (it.hasNext()) {
            if (b2 - it.next().longValue() >= 0) {
                it.remove();
            }
        }
    }

    private boolean e(String str) {
        return this.x.containsKey(str);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v5, types: [com.chartboost.sdk.impl.af, com.chartboost.sdk.impl.al] */
    /* JADX WARN: Type inference failed for: r15v3, types: [com.chartboost.sdk.impl.al] */
    /* JADX WARN: Type inference failed for: r15v4, types: [com.chartboost.sdk.impl.ao] */
    /* JADX WARN: Type inference failed for: r15v5, types: [com.chartboost.sdk.impl.al] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.al.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.chartboost.sdk.impl.al.a(com.chartboost.sdk.impl.ai, com.chartboost.sdk.Model.CBError):void
      com.chartboost.sdk.impl.al.a(com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.al.a(java.lang.Object, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.al.a(org.json.JSONObject, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.af.a(com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.af.a(java.lang.Object, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.al.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.chartboost.sdk.impl.f r22, int r23, java.lang.String r24) {
        /*
            r21 = this;
            r8 = r21
            r9 = r22
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.Model.e> r0 = r8.p     // Catch:{ Exception -> 0x0112 }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.Model.e r0 = (com.chartboost.sdk.Model.e) r0     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.c r1 = r8.f     // Catch:{ Exception -> 0x0112 }
            int r1 = r1.a     // Catch:{ Exception -> 0x0112 }
            r10 = 2
            r11 = 0
            r12 = 1
            if (r1 != r10) goto L_0x0017
            r13 = 1
            goto L_0x0018
        L_0x0017:
            r13 = 0
        L_0x0018:
            boolean r1 = r0.y     // Catch:{ Exception -> 0x0112 }
            if (r1 == 0) goto L_0x0020
            if (r13 != 0) goto L_0x0020
            r14 = 1
            goto L_0x0021
        L_0x0020:
            r14 = 0
        L_0x0021:
            com.chartboost.sdk.Libraries.i r1 = r8.c     // Catch:{ Exception -> 0x0112 }
            long r4 = r1.b()     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.e$1 r20 = new com.chartboost.sdk.impl.e$1     // Catch:{ Exception -> 0x0112 }
            r1 = r20
            r2 = r21
            r3 = r22
            r6 = r13
            r7 = r14
            r1.<init>(r3, r4, r6, r7)     // Catch:{ Exception -> 0x0112 }
            int r1 = r9.c     // Catch:{ Exception -> 0x0112 }
            if (r1 != r10) goto L_0x003a
            r1 = 1
            goto L_0x003b
        L_0x003a:
            r1 = 0
        L_0x003b:
            if (r13 == 0) goto L_0x0076
            com.chartboost.sdk.impl.al r0 = new com.chartboost.sdk.impl.al     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.c r2 = r8.f     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = r2.d     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.ar r3 = r8.o     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.Tracking.a r4 = r8.r     // Catch:{ Exception -> 0x0112 }
            r15 = r0
            r16 = r2
            r17 = r3
            r18 = r4
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x0112 }
            r0.l = r12     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "location"
            java.lang.String r3 = r9.b     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "cache"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r1)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r1 = "raw"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x0112 }
            r0.a(r1, r2)     // Catch:{ Exception -> 0x0112 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x0112 }
            r9.e = r1     // Catch:{ Exception -> 0x0112 }
            goto L_0x00f3
        L_0x0076:
            if (r14 == 0) goto L_0x00ba
            com.chartboost.sdk.impl.c r2 = r8.f     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = r2.e     // Catch:{ Exception -> 0x0112 }
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x0112 }
            java.lang.String r0 = r0.F     // Catch:{ Exception -> 0x0112 }
            r3[r11] = r0     // Catch:{ Exception -> 0x0112 }
            java.lang.String r16 = java.lang.String.format(r2, r3)     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.ao r0 = new com.chartboost.sdk.impl.ao     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.ar r2 = r8.o     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.Tracking.a r3 = r8.r     // Catch:{ Exception -> 0x0112 }
            r15 = r0
            r17 = r2
            r18 = r3
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.Libraries.f r2 = r8.b     // Catch:{ Exception -> 0x0112 }
            org.json.JSONObject r2 = r2.c()     // Catch:{ Exception -> 0x0112 }
            java.lang.String r3 = "cache_assets"
            r0.a(r3, r2, r11)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "location"
            java.lang.String r3 = r9.b     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r3, r11)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "cache"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r1, r11)     // Catch:{ Exception -> 0x0112 }
            r0.l = r12     // Catch:{ Exception -> 0x0112 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0112 }
            r9.e = r1     // Catch:{ Exception -> 0x0112 }
            goto L_0x00f3
        L_0x00ba:
            com.chartboost.sdk.impl.al r0 = new com.chartboost.sdk.impl.al     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.c r2 = r8.f     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = r2.d     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.ar r3 = r8.o     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.Tracking.a r4 = r8.r     // Catch:{ Exception -> 0x0112 }
            r15 = r0
            r16 = r2
            r17 = r3
            r18 = r4
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "local-videos"
            com.chartboost.sdk.Libraries.f r3 = r8.b     // Catch:{ Exception -> 0x0112 }
            org.json.JSONArray r3 = r3.b()     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x0112 }
            r0.l = r12     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "location"
            java.lang.String r3 = r9.b     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r3)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = "cache"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0112 }
            r0.a(r2, r1)     // Catch:{ Exception -> 0x0112 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x0112 }
            r9.e = r1     // Catch:{ Exception -> 0x0112 }
        L_0x00f3:
            r0.j = r12     // Catch:{ Exception -> 0x0112 }
            r8.g = r10     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.aj r1 = r8.m     // Catch:{ Exception -> 0x0112 }
            r1.a(r0)     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.Tracking.a r0 = r8.r     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.impl.c r1 = r8.f     // Catch:{ Exception -> 0x0112 }
            java.lang.Integer r2 = r9.e     // Catch:{ Exception -> 0x0112 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0112 }
            java.lang.String r1 = r1.a(r2)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r2 = r9.b     // Catch:{ Exception -> 0x0112 }
            r3 = r24
            r0.a(r1, r3, r2)     // Catch:{ Exception -> 0x0112 }
            goto L_0x0128
        L_0x0112:
            r0 = move-exception
            java.lang.Class r1 = r21.getClass()
            java.lang.String r2 = "sendAdGetRequest"
            com.chartboost.sdk.Tracking.a.a(r1, r2, r0)
            com.chartboost.sdk.Model.CBError r0 = new com.chartboost.sdk.Model.CBError
            com.chartboost.sdk.Model.CBError$a r1 = com.chartboost.sdk.Model.CBError.a.MISCELLANEOUS
            java.lang.String r2 = "error sending ad-get request"
            r0.<init>(r1, r2)
            r8.a(r9, r0)
        L_0x0128:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.e.a(com.chartboost.sdk.impl.f, int, java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(f fVar, com.chartboost.sdk.Model.a aVar) {
        this.g = 1;
        fVar.c = fVar.c == 2 ? 4 : 5;
        fVar.d = aVar;
        c(fVar);
        b();
    }

    private void c(final f fVar) {
        if (fVar.d == null) {
            return;
        }
        if (fVar.c == 5 || fVar.c == 4) {
            int i2 = fVar.c == 5 ? 1 : 2;
            if (fVar.g > i2) {
                AnonymousClass2 r1 = new h() {
                    public void a(boolean z, int i, int i2) {
                        e.this.a(fVar, z, i, i2);
                    }
                };
                fVar.g = i2;
                this.l.a(i2, fVar.d.n, new AtomicInteger(), (h) g.a().a(r1));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(f fVar, boolean z2, int i2, int i3) {
        if (fVar.c == 4 || fVar.c == 5) {
            fVar.n = Integer.valueOf(i2);
            fVar.o = Integer.valueOf(i3);
            if (z2) {
                d(fVar);
            } else {
                e(fVar);
            }
        }
        b();
    }

    private void d(f fVar) {
        int i2 = fVar.c;
        long b2 = this.c.b();
        if (fVar.h != null) {
            fVar.k = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - fVar.h.longValue()));
        }
        if (fVar.i != null) {
            fVar.l = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - fVar.i.longValue()));
        }
        b(fVar, "ad-unit-cached");
        fVar.c = 6;
        if (fVar.f) {
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new c.a(0, fVar.b, null));
        }
        if (i2 == 5) {
            h(fVar);
        }
    }

    private void e(f fVar) {
        b(fVar, CBError.CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
        f(fVar);
        g(fVar);
    }

    private void f(f fVar) {
        this.h.remove(fVar.b);
        fVar.c = 8;
        fVar.d = null;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(f fVar, CBError cBError) {
        if (this.g != 0) {
            this.g = 1;
            b(fVar, cBError.c());
            f(fVar);
            g(fVar);
            b();
        }
    }

    private void g(f fVar) {
        com.chartboost.sdk.Model.e eVar = this.p.get();
        long j2 = eVar.s;
        int i2 = eVar.t;
        Integer num = this.y.get(fVar.b);
        if (num == null) {
            num = 0;
        }
        Integer valueOf = Integer.valueOf(Math.min(num.intValue(), i2));
        this.y.put(fVar.b, Integer.valueOf(valueOf.intValue() + 1));
        this.x.put(fVar.b, Long.valueOf(this.c.b() + TimeUnit.MILLISECONDS.toNanos(j2 << valueOf.intValue())));
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        if (e()) {
            c cVar = this.f;
            cVar.getClass();
            this.d.postDelayed(new c.a(4, str, CBError.CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        f fVar = this.h.get(str);
        if (fVar == null) {
            int i2 = this.v;
            this.v = i2 + 1;
            fVar = new f(i2, str, 1);
            this.h.put(str, fVar);
            this.j.add(fVar);
        }
        if (fVar.i == null) {
            fVar.i = Long.valueOf(this.c.b());
        }
        switch (fVar.c) {
            case 0:
                this.i.remove(fVar);
                this.j.add(fVar);
                fVar.c = 1;
                break;
            case 2:
                fVar.c = 3;
                break;
            case 4:
                fVar.c = 5;
                c(fVar);
                break;
            case 6:
                h(fVar);
                break;
        }
        b();
    }

    private void h(f fVar) {
        CBError.CBImpressionError cBImpressionError;
        String str;
        if (!this.n.c()) {
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new c.a(4, fVar.b, CBError.CBImpressionError.INTERNET_UNAVAILABLE_AT_SHOW));
            return;
        }
        com.chartboost.sdk.Model.c cVar2 = null;
        try {
            com.chartboost.sdk.Model.a aVar = fVar.d;
            File file = this.b.d().a;
            if (aVar.m != 0 || (!this.f.g && !aVar.A.equals("video"))) {
                cBImpressionError = null;
            } else {
                cBImpressionError = a(aVar.l);
                if (cBImpressionError != null) {
                    CBLogging.b("AdUnitManager", "Video media unavailable for the impression");
                }
            }
            if (cBImpressionError == null) {
                for (b next : aVar.n.values()) {
                    if (!next.a(file).exists()) {
                        CBLogging.b("AdUnitManager", "Asset does not exist: " + next.b);
                        cBImpressionError = CBError.CBImpressionError.ASSET_MISSING;
                    }
                }
            }
            if (cBImpressionError == null) {
                if (aVar.m == 1) {
                    str = a(aVar, file);
                    if (str == null) {
                        cBImpressionError = CBError.CBImpressionError.ERROR_LOADING_WEB_VIEW;
                    }
                } else {
                    str = null;
                }
                if (cBImpressionError == null) {
                    cVar2 = a(fVar, str);
                }
            }
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "showReady", e2);
            cBImpressionError = CBError.CBImpressionError.INTERNAL;
        }
        if (cBImpressionError == null) {
            fVar.c = 7;
            com.chartboost.sdk.c cVar3 = this.e;
            cVar3.getClass();
            c.C0008c cVar4 = new c.C0008c(10);
            cVar4.d = cVar2;
            fVar.j = Long.valueOf(this.c.b());
            this.d.post(cVar4);
            return;
        }
        b(fVar, cBImpressionError);
        f(fVar);
    }

    private String a(com.chartboost.sdk.Model.a aVar, File file) {
        if (aVar.C == null) {
            CBLogging.b("AdUnitManager", "AdUnit does not have a template body");
            return null;
        }
        File a2 = aVar.C.a(file);
        HashMap hashMap = new HashMap();
        if (aVar instanceof t) {
            hashMap.put("{% encoding %}", "base64");
            hashMap.put("{% adm %}", ((t) aVar).d);
            String str = this.f.a == 0 ? "8" : "9";
            String str2 = this.f.a == 0 ? "true" : "false";
            hashMap.put("{{ ad_type }}", str);
            hashMap.put("{{ show_close_button }}", str2);
            hashMap.put("{{ preroll_popup }}", "false");
            hashMap.put("{{ post_video_reward_toaster_enabled }}", "false");
            if (str.equals("9")) {
                hashMap.put("{{ post_video_reward_toaster_enabled }}", "false");
            }
        }
        hashMap.putAll(aVar.o);
        hashMap.put("{% certification_providers %}", o.a(aVar.D));
        for (Map.Entry next : aVar.n.entrySet()) {
            hashMap.put(next.getKey(), ((b) next.getValue()).b);
        }
        try {
            return n.a(a2, hashMap);
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "loadTemplateHtml", e2);
            return null;
        }
    }

    private com.chartboost.sdk.Model.c a(f fVar, String str) {
        f fVar2 = fVar;
        return new com.chartboost.sdk.Model.c(fVar2.d, new d(this, fVar2), this.b, this.m, this.o, this.q, this.r, this.d, this.e, this.s, this.t, this.u, this.f, fVar2.b, str);
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar, CBError.CBImpressionError cBImpressionError) {
        b(fVar, cBImpressionError);
        if (fVar.c != 7) {
            return;
        }
        if (cBImpressionError == CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE) {
            fVar.c = 6;
            fVar.j = null;
            fVar.i = null;
            fVar.m = null;
            return;
        }
        g(fVar);
        f(fVar);
        b();
    }

    private void b(f fVar, CBError.CBImpressionError cBImpressionError) {
        String str;
        Handler handler = this.d;
        c cVar = this.f;
        cVar.getClass();
        handler.post(new c.a(4, fVar.b, cBImpressionError));
        if (cBImpressionError != CBError.CBImpressionError.NO_AD_FOUND) {
            String str2 = null;
            String str3 = fVar.d != null ? fVar.d.q : null;
            String str4 = (fVar.c == 0 || fVar.c == 2 || fVar.c == 4) ? "cache" : "show";
            Integer valueOf = Integer.valueOf(fVar.d != null ? fVar.d.m : fVar.e.intValue());
            if (valueOf != null) {
                str2 = valueOf.intValue() == 0 ? "native" : AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB;
            }
            String str5 = str2;
            if (fVar.c < 0 || fVar.c >= this.B.length) {
                str = "Unknown state: " + fVar.c;
            } else {
                str = this.B[fVar.c];
            }
            this.r.a(this.f.b, str4, str5, cBImpressionError.toString(), str3, fVar.b, str);
        }
    }

    private void b(f fVar, String str) {
        Integer num;
        String str2;
        f fVar2 = fVar;
        if (this.p.get().p) {
            String str3 = null;
            String str4 = fVar2.d != null ? fVar2.d.q : null;
            String str5 = (fVar2.c == 0 || fVar2.c == 2 || fVar2.c == 4) ? "cache" : "show";
            if (fVar2.d != null) {
                num = Integer.valueOf(fVar2.d.m);
            } else {
                num = fVar2.e;
            }
            if (num != null) {
                str3 = num.intValue() == 0 ? "native" : AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB;
            }
            String str6 = str3;
            if (fVar2.c < 0 || fVar2.c >= this.B.length) {
                str2 = "Unknown state: " + fVar2.c;
            } else {
                str2 = this.B[fVar2.c];
            }
            this.r.a(str, this.f.b, str5, str6, null, null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("adGetRequestSubmitToCallbackMs", fVar2.p), com.chartboost.sdk.Libraries.e.a("downloadRequestToCompletionMs", fVar2.n), com.chartboost.sdk.Libraries.e.a("downloadAccumulatedProcessingMs", fVar2.o), com.chartboost.sdk.Libraries.e.a("adGetRequestGetResponseCodeMs", fVar2.q), com.chartboost.sdk.Libraries.e.a("adGetRequestReadDataMs", fVar2.r), com.chartboost.sdk.Libraries.e.a("cacheRequestToReadyMs", fVar2.k), com.chartboost.sdk.Libraries.e.a("showRequestToReadyMs", fVar2.l), com.chartboost.sdk.Libraries.e.a("showRequestToShownMs", fVar2.m), com.chartboost.sdk.Libraries.e.a("adId", str4), com.chartboost.sdk.Libraries.e.a((String) FirebaseAnalytics.Param.LOCATION, fVar2.b), com.chartboost.sdk.Libraries.e.a("state", str2)), false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar) {
        if (fVar.c == 7) {
            fVar.c = 6;
            fVar.j = null;
            fVar.i = null;
            fVar.m = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(f fVar) {
        if (fVar.c == 7) {
            if (fVar.i != null && fVar.m == null) {
                fVar.m = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(this.c.b() - fVar.i.longValue()));
            }
            b(fVar, "ad-unit-shown");
            this.y.remove(fVar.b);
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new c.a(5, fVar.b, null));
            i(fVar);
            f(fVar);
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        f fVar = this.h.get(str);
        if (fVar != null && fVar.c == 6) {
            f(fVar);
            b();
        }
    }

    private void i(f fVar) {
        al alVar = new al(this.f.f, this.o, this.r, 2, new g(this, fVar.b));
        alVar.j = 1;
        alVar.a("cached", AppEventsConstants.EVENT_PARAM_VALUE_NO);
        String str = fVar.d.q;
        if (!str.isEmpty()) {
            alVar.a("ad_id", str);
        }
        alVar.a(FirebaseAnalytics.Param.LOCATION, fVar.b);
        this.m.a(alVar);
        this.r.b(this.f.a(fVar.d.m), fVar.b, str);
    }

    /* access modifiers changed from: package-private */
    public CBError.CBImpressionError a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return CBError.CBImpressionError.INVALID_RESPONSE;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("assets");
        if (optJSONObject == null) {
            return CBError.CBImpressionError.INVALID_RESPONSE;
        }
        JSONObject optJSONObject2 = optJSONObject.optJSONObject(CBUtility.a(CBUtility.a()) ? "video-portrait" : "video-landscape");
        if (optJSONObject2 == null) {
            return CBError.CBImpressionError.VIDEO_UNAVAILABLE_FOR_CURRENT_ORIENTATION;
        }
        String optString = optJSONObject2.optString("id");
        if (optString.isEmpty()) {
            return CBError.CBImpressionError.VIDEO_ID_MISSING;
        }
        if (new File(this.b.d().g, optString).exists()) {
            return null;
        }
        return CBError.CBImpressionError.VIDEO_UNAVAILABLE;
    }

    private boolean e() {
        if (this.f.a == 0 && !com.chartboost.sdk.i.u && this.q.getInt("cbPrefSessionCount", 0) == 1) {
            return true;
        }
        return false;
    }

    public class a implements Runnable {
        final int a;
        final String b;
        final f c;
        final CBError.CBImpressionError d;

        public a(int i, String str, f fVar, CBError.CBImpressionError cBImpressionError) {
            this.a = i;
            this.b = str;
            this.c = fVar;
            this.d = cBImpressionError;
        }

        public void run() {
            try {
                synchronized (e.this) {
                    int i = this.a;
                    if (i != 0) {
                        switch (i) {
                            case 2:
                                e.this.k = null;
                                e.this.b();
                                break;
                            case 3:
                                e.this.b(this.b);
                                break;
                            case 4:
                                e.this.c(this.b);
                                break;
                            case 5:
                                e.this.b(this.c);
                                break;
                            case 6:
                                e.this.a(this.c, this.d);
                                break;
                            case 7:
                                e.this.a(this.c);
                                break;
                            case 8:
                                e.this.d(this.b);
                                break;
                        }
                    } else {
                        e.this.a();
                    }
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(getClass(), "run", e2);
            }
        }
    }
}
