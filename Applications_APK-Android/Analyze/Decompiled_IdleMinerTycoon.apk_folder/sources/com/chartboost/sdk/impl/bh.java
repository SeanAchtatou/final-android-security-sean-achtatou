package com.chartboost.sdk.impl;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Window;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.d;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import com.chartboost.sdk.h;
import com.chartboost.sdk.i;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.model.Advertisement;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.List;
import org.json.JSONObject;

public class bh extends e {
    int A = 0;
    int B = 0;
    int C = 0;
    int D = 0;
    int E = 0;
    int F = -1;
    boolean G = true;
    int H = -1;
    private final f I;
    private final aj J;
    private String K = null;
    private float L = 0.0f;
    private float M = 0.0f;
    private boolean N = false;
    private int O = 0;
    final com.chartboost.sdk.Tracking.a j;
    final d k;
    final SharedPreferences l;
    public String m = "UNKNOWN";
    String n = null;
    protected int o = 1;
    long p = 0;
    long q = 0;
    boolean r = false;
    int s = 0;
    int t = 0;
    int u = 0;
    int v = 0;
    int w = 0;
    int x = 0;
    int y = 0;
    int z = 0;

    public String a(int i) {
        switch (i) {
            case -1:
                return Constants.ParametersKeys.ORIENTATION_NONE;
            case 0:
                return "landscape";
            case 1:
                return "portrait";
            default:
                return "error";
        }
    }

    public class b extends e.a {
        public bg c;
        public bf d;
        public RelativeLayout e;
        public RelativeLayout f;

        /* access modifiers changed from: protected */
        public void a(int i, int i2) {
        }

        public b(Context context, String str) {
            super(context);
            setFocusable(false);
            g a = g.a();
            this.e = (RelativeLayout) a.a(new RelativeLayout(context));
            this.f = (RelativeLayout) a.a(new RelativeLayout(context));
            this.c = (bg) a.a(new bg(context));
            h.a(context, this.c, bh.this.l);
            this.c.setWebViewClient((WebViewClient) a.a(new a()));
            this.d = (bf) a.a(new bf(this.e, this.f, null, this.c, bh.this, bh.this.a));
            this.c.setWebChromeClient(this.d);
            if (s.a().a(19)) {
                bg bgVar = this.c;
                bg.setWebContentsDebuggingEnabled(true);
            }
            this.c.loadDataWithBaseURL(bh.this.n, str, "text/html", "utf-8", null);
            this.e.addView(this.c);
            this.c.getSettings().setSupportZoom(false);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.e.setLayoutParams(layoutParams);
            this.c.setLayoutParams(layoutParams);
            this.c.setBackgroundColor(0);
            this.f.setVisibility(8);
            this.f.setLayoutParams(layoutParams);
            addView(this.e);
            addView(this.f);
            bh.this.p = System.currentTimeMillis();
            if (context instanceof Activity) {
                bh.this.F = ((Activity) context).getRequestedOrientation();
            } else {
                bh.this.F = -1;
            }
            o.a(this.c, bh.this.e.p.D);
            bh.this.a.postDelayed(new Runnable(bh.this) {
                public void run() {
                    if (!bh.this.r) {
                        CBLogging.a("CBWebViewProtocol", "Webview seems to be taking more time loading the html content, so closing the view.");
                        bh.this.a(CBError.CBImpressionError.WEB_VIEW_PAGE_LOAD_TIMEOUT);
                    }
                }
            }, 3000);
        }
    }

    public bh(c cVar, f fVar, aj ajVar, SharedPreferences sharedPreferences, com.chartboost.sdk.Tracking.a aVar, Handler handler, com.chartboost.sdk.c cVar2, d dVar) {
        super(cVar, handler, cVar2);
        this.I = fVar;
        this.J = ajVar;
        this.j = aVar;
        this.k = dVar;
        this.l = sharedPreferences;
    }

    /* access modifiers changed from: protected */
    public e.a b(Context context) {
        return new b(context, this.K);
    }

    public boolean a(JSONObject jSONObject) {
        File file = this.I.d().a;
        if (file == null) {
            CBLogging.b("CBWebViewProtocol", "External Storage path is unavailable or media not mounted");
            a(CBError.CBImpressionError.ERROR_LOADING_WEB_VIEW);
            return false;
        }
        this.n = Advertisement.FILE_SCHEME + file.getAbsolutePath() + com.appsflyer.share.Constants.URL_PATH_DELIMITER;
        if (s.a().a(this.e.p.p)) {
            CBLogging.b("CBWebViewProtocol", "Invalid adId being passed in the response");
            a(CBError.CBImpressionError.ERROR_DISPLAYING_VIEW);
            return false;
        }
        String str = this.e.o;
        if (str == null) {
            CBLogging.b("CBWebViewProtocol", "No html data found in memory");
            a(CBError.CBImpressionError.ERROR_LOADING_WEB_VIEW);
            return false;
        }
        this.K = str;
        return true;
    }

    public void h() {
        super.h();
        r();
    }

    public void b(String str) {
        List<String> list;
        if (this.e.p.y != null && !TextUtils.isEmpty(str) && (list = this.e.p.y.get(str)) != null) {
            for (String str2 : list) {
                if (!str2.isEmpty()) {
                    this.J.a(new af(HttpRequest.METHOD_GET, str2, 2, null));
                    CBLogging.a("CBWebViewProtocol", "###### Sending VAST Tracking Event: " + str2);
                }
            }
        }
    }

    public void c(String str) {
        this.j.a(this.e.a.a(this.e.p.m), this.e.m, this.e.o(), str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.Tracking.a.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.chartboost.sdk.Tracking.a.a(java.lang.String, java.lang.String, long, long, long):void
      com.chartboost.sdk.Tracking.a.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void */
    public void d(String str) {
        if (s.a().a(str)) {
            str = "Unknown Webview error";
        }
        this.j.a(this.e.a.a(this.e.p.m), this.e.m, this.e.o(), str, true);
        CBLogging.b("CBWebViewProtocol", "Webview error occurred closing the webview" + str);
        a(CBError.CBImpressionError.ERROR_LOADING_WEB_VIEW);
        h();
    }

    public void e(String str) {
        if (s.a().a(str)) {
            str = "Unknown Webview warning message";
        }
        this.j.b(this.e.a.a(this.e.p.m), this.e.m, this.e.o(), str);
        CBLogging.d("CBWebViewProtocol", "Webview warning occurred closing the webview" + str);
    }

    private class a extends WebViewClient {
        @TargetApi(24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return false;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return false;
        }

        private a() {
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (Build.VERSION.SDK_INT >= 26) {
                PackageInfo currentWebViewPackage = WebView.getCurrentWebViewPackage();
                if (currentWebViewPackage != null) {
                    CBLogging.a("CBWebViewProtocol", "WebView version: " + currentWebViewPackage.versionName);
                    return;
                }
                a(at.a(i.n).equalsIgnoreCase("watch") ? "No webview support." : "Device was not set up correctly.");
            }
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            bh.this.r = true;
            bh.this.q = System.currentTimeMillis();
            CBLogging.a("CBWebViewProtocol", "Total web view load response time " + ((bh.this.q - bh.this.p) / 1000));
            Context context = webView.getContext();
            if (context != null) {
                bh.this.c(context);
                bh.this.d(context);
                bh.this.o();
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            a("Error loading " + str2 + ": " + str);
        }

        @TargetApi(23)
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            if (webResourceRequest.isForMainFrame()) {
                a("Error loading " + webResourceRequest.getUrl().toString() + ": " + ((Object) webResourceError.getDescription()));
            }
        }

        @TargetApi(23)
        public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error loading ");
            sb.append(webResourceRequest.getUrl().toString());
            sb.append(": ");
            sb.append(webResourceResponse == null ? "unknown error" : webResourceResponse.getReasonPhrase());
            CBLogging.a("CBWebViewProtocol", sb.toString());
        }

        @TargetApi(26)
        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            String str;
            if (renderProcessGoneDetail.didCrash()) {
                str = "Webview crashed: " + renderProcessGoneDetail.toString();
            } else {
                str = "Webview killed, likely due to low memory";
            }
            a(str);
            return true;
        }

        private void a(String str) {
            CBLogging.b("CBWebViewProtocol", str);
            bh.this.a(CBError.CBImpressionError.WEB_VIEW_CLIENT_RECEIVED_ERROR);
            bh.this.r = true;
            bh.this.k.d(bh.this.e);
        }
    }

    /* access modifiers changed from: package-private */
    public void o() {
        b y2 = e();
        if (y2 == null || !this.r) {
            this.B = this.x;
            this.C = this.y;
            this.D = this.z;
            this.E = this.A;
            return;
        }
        int[] iArr = new int[2];
        y2.getLocationInWindow(iArr);
        int i = iArr[0];
        int i2 = iArr[1] - this.w;
        int width = y2.getWidth();
        int height = y2.getHeight();
        this.x = i;
        this.y = i2;
        this.z = i + width;
        this.A = i2 + height;
        this.B = this.x;
        this.C = this.y;
        this.D = this.z;
        this.E = this.A;
    }

    public String p() {
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("allowOrientationChange", Boolean.valueOf(this.G)), com.chartboost.sdk.Libraries.e.a("forceOrientation", a(this.H))).toString();
    }

    public int f(String str) {
        if (str.equals("portrait")) {
            return 1;
        }
        return str.equals("landscape") ? 0 : -1;
    }

    public void c(JSONObject jSONObject) {
        this.G = jSONObject.optBoolean("allowOrientationChange", this.G);
        this.H = f(jSONObject.optString("forceOrientation", a(this.H)));
        q();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r0.getResources().getConfiguration().orientation == 1) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void q() {
        /*
            r4 = this;
            com.chartboost.sdk.c r0 = r4.b
            android.app.Activity r0 = r0.b()
            if (r0 == 0) goto L_0x0033
            boolean r1 = com.chartboost.sdk.Libraries.CBUtility.a(r0)
            if (r1 == 0) goto L_0x000f
            goto L_0x0033
        L_0x000f:
            int r1 = r4.H
            r2 = 0
            r3 = 1
            if (r1 != r3) goto L_0x0017
        L_0x0015:
            r2 = 1
            goto L_0x002f
        L_0x0017:
            int r1 = r4.H
            if (r1 != 0) goto L_0x001c
            goto L_0x002f
        L_0x001c:
            boolean r1 = r4.G
            if (r1 == 0) goto L_0x0022
            r2 = -1
            goto L_0x002f
        L_0x0022:
            android.content.res.Resources r1 = r0.getResources()
            android.content.res.Configuration r1 = r1.getConfiguration()
            int r1 = r1.orientation
            if (r1 != r3) goto L_0x002f
            goto L_0x0015
        L_0x002f:
            r0.setRequestedOrientation(r2)
            return
        L_0x0033:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.bh.q():void");
    }

    /* access modifiers changed from: package-private */
    public void r() {
        Activity b2 = this.b.b();
        if (b2 != null && !CBUtility.a(b2)) {
            if (b2.getRequestedOrientation() != this.F) {
                b2.setRequestedOrientation(this.F);
            }
            this.G = true;
            this.H = -1;
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.s = displayMetrics.widthPixels;
        this.t = displayMetrics.heightPixels;
    }

    /* access modifiers changed from: package-private */
    public void d(Context context) {
        if (context instanceof Activity) {
            Window window = ((Activity) context).getWindow();
            Rect rect = new Rect();
            window.getDecorView().getWindowVisibleDisplayFrame(rect);
            this.w = a(window);
            if (this.s == 0 || this.t == 0) {
                c(context);
            }
            int width = rect.width();
            int i = this.t - this.w;
            if (width != this.u || i != this.v) {
                this.u = width;
                this.v = i;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a(Window window) {
        return window.findViewById(16908290).getTop();
    }

    public String s() {
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("width", Integer.valueOf(this.u)), com.chartboost.sdk.Libraries.e.a("height", Integer.valueOf(this.v))).toString();
    }

    public String t() {
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("width", Integer.valueOf(this.s)), com.chartboost.sdk.Libraries.e.a("height", Integer.valueOf(this.t))).toString();
    }

    public String u() {
        o();
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("x", Integer.valueOf(this.x)), com.chartboost.sdk.Libraries.e.a("y", Integer.valueOf(this.y)), com.chartboost.sdk.Libraries.e.a("width", Integer.valueOf(this.z)), com.chartboost.sdk.Libraries.e.a("height", Integer.valueOf(this.A))).toString();
    }

    public String v() {
        o();
        return com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("x", Integer.valueOf(this.B)), com.chartboost.sdk.Libraries.e.a("y", Integer.valueOf(this.C)), com.chartboost.sdk.Libraries.e.a("width", Integer.valueOf(this.D)), com.chartboost.sdk.Libraries.e.a("height", Integer.valueOf(this.E))).toString();
    }

    public boolean l() {
        if (this.O == 2 && this.e.a.a == 1) {
            return true;
        }
        d();
        h();
        return true;
    }

    public void m() {
        super.m();
        final b y2 = e();
        if (y2 != null && y2.c != null) {
            this.a.post(new Runnable() {
                public void run() {
                    if (y2.c != null) {
                        bh.this.g("onForeground");
                        y2.c.onResume();
                    }
                }
            });
            this.j.d(this.m, this.e.o());
        }
    }

    public void n() {
        super.n();
        final b y2 = e();
        if (y2 != null && y2.c != null) {
            this.a.post(new Runnable() {
                public void run() {
                    if (y2.c != null) {
                        bh.this.g("onBackground");
                        y2.c.onPause();
                    }
                }
            });
            this.j.e(this.m, this.e.o());
        }
    }

    public void w() {
        if (this.o <= 1) {
            this.e.e();
            this.o++;
        }
    }

    public void d() {
        o.d();
        b y2 = e();
        if (y2 != null) {
            if (y2.c != null) {
                CBLogging.a("CBWebViewProtocol", "Destroying the webview object and cleaning up the references");
                y2.c.destroy();
                y2.c = null;
            }
            if (y2.d != null) {
                y2.d = null;
            }
            if (y2.e != null) {
                y2.e = null;
            }
            if (y2.f != null) {
                y2.f = null;
            }
        }
        super.d();
    }

    public void x() {
        this.j.c(this.m, this.e.o());
    }

    public void b(int i) {
        this.O = i;
    }

    /* renamed from: y */
    public b e() {
        return (b) super.e();
    }

    public void a(float f) {
        this.M = f;
    }

    public void b(float f) {
        this.L = f;
    }

    public float j() {
        return this.L;
    }

    public float k() {
        return this.M;
    }

    public void z() {
        if (this.e.l == 2 && !this.N) {
            this.j.d("", this.e.o());
            this.e.p();
            this.N = true;
            o.c();
        }
    }

    /* access modifiers changed from: package-private */
    public void g(String str) {
        if (e().c != null) {
            String str2 = "javascript:Chartboost.EventHandler.handleNativeEvent(\"" + str + "\", \"\")";
            CBLogging.a("CBWebViewProtocol", "Calling native to javascript: " + str2);
            e().c.loadUrl(str2);
        }
    }
}
