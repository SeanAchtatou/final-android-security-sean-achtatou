package com.chartboost.sdk.impl;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.appsflyer.share.Constants;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Model.b;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class bf extends WebChromeClient {
    private View a;
    private ViewGroup b;
    private boolean c = false;
    private FrameLayout d;
    private WebChromeClient.CustomViewCallback e;
    private a f;
    private final bh g;
    private final Handler h;

    public interface a {
        void a(boolean z);
    }

    public bf(View view, ViewGroup viewGroup, View view2, bg bgVar, bh bhVar, Handler handler) {
        this.a = view;
        this.b = viewGroup;
        this.g = bhVar;
        this.h = handler;
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String simpleName = bf.class.getSimpleName();
        Log.d(simpleName, "Chartboost Webview:" + consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
        return true;
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        try {
            JSONObject jSONObject = new JSONObject(str2);
            jsPromptResult.confirm(a(jSONObject.getJSONObject("eventArgs"), jSONObject.getString("eventType")));
            return true;
        } catch (JSONException unused) {
            CBLogging.b("CBWebChromeClient", "Exception caught parsing the function name from js to native");
            return true;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public String a(JSONObject jSONObject, String str) {
        char c2;
        com.chartboost.sdk.Model.a aVar;
        String str2 = str;
        int i = 8;
        switch (str.hashCode()) {
            case -2012425132:
                if (str2.equals("getDefaultPosition")) {
                    c2 = 18;
                    break;
                }
                c2 = 65535;
                break;
            case -1757019252:
                if (str2.equals("getCurrentPosition")) {
                    c2 = 17;
                    break;
                }
                c2 = 65535;
                break;
            case -1554056650:
                if (str2.equals("currentVideoDuration")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case -1263203643:
                if (str2.equals("openUrl")) {
                    c2 = 14;
                    break;
                }
                c2 = 65535;
                break;
            case -1086137328:
                if (str2.equals("videoCompleted")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -715147645:
                if (str2.equals("getScreenSize")) {
                    c2 = 16;
                    break;
                }
                c2 = 65535;
                break;
            case -640720077:
                if (str2.equals("videoPlaying")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 3529469:
                if (str2.equals("show")) {
                    c2 = 9;
                    break;
                }
                c2 = 65535;
                break;
            case 94750088:
                if (str2.equals("click")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 94756344:
                if (str2.equals("close")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case 95458899:
                if (str2.equals("debug")) {
                    c2 = 12;
                    break;
                }
                c2 = 65535;
                break;
            case 96784904:
                if (str2.equals("error")) {
                    c2 = 10;
                    break;
                }
                c2 = 65535;
                break;
            case 133423073:
                if (str2.equals("setOrientationProperties")) {
                    c2 = 20;
                    break;
                }
                c2 = 65535;
                break;
            case 160987616:
                if (str2.equals("getParameters")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case 937504109:
                if (str2.equals("getOrientationProperties")) {
                    c2 = 19;
                    break;
                }
                c2 = 65535;
                break;
            case 939594121:
                if (str2.equals("videoPaused")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case 1000390722:
                if (str2.equals("videoReplay")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 1082777163:
                if (str2.equals("totalVideoDuration")) {
                    c2 = 8;
                    break;
                }
                c2 = 65535;
                break;
            case 1124446108:
                if (str2.equals("warning")) {
                    c2 = 11;
                    break;
                }
                c2 = 65535;
                break;
            case 1270488759:
                if (str2.equals("tracking")) {
                    c2 = 13;
                    break;
                }
                c2 = 65535;
                break;
            case 1880941391:
                if (str2.equals("getMaxSize")) {
                    c2 = 15;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                if (this.g.e == null || (aVar = this.g.e.p) == null) {
                    return "{}";
                }
                JSONObject a2 = e.a(new e.a[0]);
                for (Map.Entry next : aVar.o.entrySet()) {
                    e.a(a2, (String) next.getKey(), next.getValue());
                }
                for (Map.Entry next2 : aVar.n.entrySet()) {
                    b bVar = (b) next2.getValue();
                    e.a(a2, (String) next2.getKey(), bVar.a + Constants.URL_PATH_DELIMITER + bVar.b);
                }
                return a2.toString();
            case 1:
                i = 0;
                break;
            case 2:
                i = 1;
                break;
            case 3:
                i = 9;
                break;
            case 4:
                i = 11;
                break;
            case 5:
                i = 10;
                break;
            case 6:
                i = 12;
                break;
            case 7:
                i = 2;
                break;
            case 8:
                i = 7;
                break;
            case 9:
                i = 6;
                break;
            case 10:
                Log.d(bg.class.getName(), "Javascript Error occured");
                i = 4;
                break;
            case 11:
                Log.d(bg.class.getName(), "Javascript warning occurred");
                i = 13;
                break;
            case 12:
                i = 3;
                break;
            case 13:
                break;
            case 14:
                i = 5;
                break;
            case 15:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.g.s();
            case 16:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.g.t();
            case 17:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.g.v();
            case 18:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.g.u();
            case 19:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.g.p();
            case 20:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                i = 14;
                break;
            default:
                Log.e("CBWebChromeClient", "JavaScript to native " + str2 + " callback not recognized.");
                return "Function name not recognized.";
        }
        Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
        this.h.post(new bi(this, this.g, i, str, jSONObject));
        return "Native function successfully called.";
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (view instanceof FrameLayout) {
            this.c = true;
            this.d = (FrameLayout) view;
            this.e = customViewCallback;
            this.a.setVisibility(4);
            this.b.addView(this.d, new ViewGroup.LayoutParams(-1, -1));
            this.b.setVisibility(0);
            if (this.f != null) {
                this.f.a(true);
            }
        }
    }

    public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
        onShowCustomView(view, customViewCallback);
    }

    public void onHideCustomView() {
        if (this.c) {
            this.b.setVisibility(4);
            this.b.removeView(this.d);
            this.a.setVisibility(0);
            if (this.e != null && !this.e.getClass().getName().contains(".chromium.")) {
                this.e.onCustomViewHidden();
            }
            this.c = false;
            this.d = null;
            this.e = null;
            if (this.f != null) {
                this.f.a(false);
            }
        }
    }
}
