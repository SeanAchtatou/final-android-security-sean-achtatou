package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.al;
import com.facebook.appevents.UserDataStore;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.helpshift.support.constants.FaqsColumns;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public final class ao extends al {
    private final JSONObject n = new JSONObject();
    private final JSONObject o = new JSONObject();
    private final JSONObject p = new JSONObject();
    private final JSONObject q = new JSONObject();

    public ao(String str, ar arVar, a aVar, int i, al.a aVar2) {
        super(str, arVar, aVar, i, aVar2);
    }

    /* access modifiers changed from: protected */
    public void c() {
        e.a(this.o, "app", this.m.s);
        e.a(this.o, TJAdUnitConstants.String.BUNDLE, this.m.j);
        e.a(this.o, "bundle_id", this.m.k);
        e.a(this.o, "custom_id", i.b);
        e.a(this.o, "session_id", "");
        e.a(this.o, "ui", -1);
        e.a(this.o, "test_mode", false);
        e.a(this.o, "certification_providers", o.f());
        a("app", this.o);
        boolean z = true;
        e.a(this.p, "carrier", e.a(e.a((String) TapjoyConstants.TJC_CARRIER_NAME, this.m.v.optString("carrier-name")), e.a(TapjoyConstants.TJC_MOBILE_COUNTRY_CODE, this.m.v.optString("mobile-country-code")), e.a((String) TapjoyConstants.TJC_MOBILE_NETWORK_CODE, this.m.v.optString("mobile-network-code")), e.a("iso_country_code", this.m.v.optString("iso-country-code")), e.a("phone_type", Integer.valueOf(this.m.v.optInt("phone-type")))));
        e.a(this.p, "model", this.m.f);
        e.a(this.p, TapjoyConstants.TJC_DEVICE_TYPE_NAME, this.m.t);
        e.a(this.p, "actual_device_type", this.m.u);
        e.a(this.p, "os", this.m.g);
        e.a(this.p, UserDataStore.COUNTRY, this.m.h);
        e.a(this.p, FaqsColumns.LANGUAGE, this.m.i);
        e.a(this.p, "timestamp", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.m.e.a())));
        e.a(this.p, "reachability", Integer.valueOf(this.m.b.a()));
        e.a(this.p, "scale", this.m.r);
        e.a(this.p, "is_portrait", Boolean.valueOf(CBUtility.a(CBUtility.a())));
        e.a(this.p, "rooted_device", Boolean.valueOf(this.m.w));
        e.a(this.p, TapjoyConstants.TJC_DEVICE_TIMEZONE, this.m.x);
        e.a(this.p, "mobile_network", this.m.y);
        e.a(this.p, "dw", this.m.o);
        e.a(this.p, "dh", this.m.p);
        e.a(this.p, "dpi", this.m.q);
        e.a(this.p, "w", this.m.m);
        e.a(this.p, "h", this.m.n);
        e.a(this.p, "user_agent", i.w);
        e.a(this.p, "device_family", "");
        e.a(this.p, "retina", false);
        d.a a = this.m.a.a();
        e.a(this.p, "identity", a.b);
        if (a.a != -1) {
            if (a.a != 1) {
                z = false;
            }
            e.a(this.p, "limit_ad_tracking", Boolean.valueOf(z));
        }
        e.a(this.p, "pidatauseconsent", Integer.valueOf(i.x.getValue()));
        a("device", this.p);
        e.a(this.n, "framework", "");
        e.a(this.n, "sdk", this.m.l);
        if (i.e != null) {
            e.a(this.n, "framework_version", i.g);
            e.a(this.n, "wrapper_version", i.c);
        }
        e.a(this.n, "mediation", i.i);
        e.a(this.n, "commit_hash", "7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
        String str = this.m.c.get().a;
        if (!s.a().a(str)) {
            e.a(this.n, "config_variant", str);
        }
        a("sdk", this.n);
        e.a(this.q, SettingsJsonConstants.SESSION_KEY, Integer.valueOf(this.m.d.getInt("cbPrefSessionCount", 0)));
        if (this.q.isNull("cache")) {
            e.a(this.q, "cache", false);
        }
        if (this.q.isNull("amount")) {
            e.a(this.q, "amount", 0);
        }
        if (this.q.isNull("retry_count")) {
            e.a(this.q, "retry_count", 0);
        }
        if (this.q.isNull(FirebaseAnalytics.Param.LOCATION)) {
            e.a(this.q, FirebaseAnalytics.Param.LOCATION, "");
        }
        a("ad", this.q);
    }

    public void a(String str, Object obj, int i) {
        if (i == 0) {
            e.a(this.q, str, obj);
            a("ad", this.q);
        }
    }
}
