package com.chartboost.sdk.impl;

import android.os.Handler;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Tracking.a;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Executor;

public class ap<T> implements Comparable<ap>, Runnable {
    public final af<T> a;
    private final Executor b;
    private final aq c;
    private final ak d;
    private final i e;
    private final Handler f;
    private ah<T> g;
    private ai h;

    private static boolean a(int i) {
        return ((100 <= i && i < 200) || i == 204 || i == 304) ? false : true;
    }

    ap(Executor executor, aq aqVar, ak akVar, i iVar, Handler handler, af<T> afVar) {
        this.b = executor;
        this.c = aqVar;
        this.d = akVar;
        this.e = iVar;
        this.f = handler;
        this.a = afVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.af.a(java.lang.Object, com.chartboost.sdk.impl.ai):void
     arg types: [T, com.chartboost.sdk.impl.ai]
     candidates:
      com.chartboost.sdk.impl.af.a(com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ai):void
      com.chartboost.sdk.impl.af.a(java.lang.Object, com.chartboost.sdk.impl.ai):void */
    public void run() {
        if (this.g != null) {
            try {
                if (this.g.b == null) {
                    this.a.a((Object) this.g.a, this.h);
                } else {
                    this.a.a(this.g.b, this.h);
                }
            } catch (Exception e2) {
                a.a(getClass(), "deliver result", e2);
            }
        } else if (this.a.e.compareAndSet(0, 1)) {
            long b2 = this.e.b();
            try {
                if (this.d.c()) {
                    this.h = a(this.a);
                    int i = this.h.a;
                    if (i < 200 || i >= 300) {
                        CBError.a aVar = CBError.a.NETWORK_FAILURE;
                        this.g = ah.a(new CBError(aVar, "Failure due to HTTP status code " + i));
                    } else {
                        this.g = this.a.a(this.h);
                    }
                } else {
                    this.g = ah.a(new CBError(CBError.a.INTERNET_UNAVAILABLE, "Internet Unavailable"));
                }
                this.a.g = this.e.b() - b2;
                switch (this.a.j) {
                    case 0:
                        this.f.post(this);
                        return;
                    case 1:
                        this.b.execute(this);
                        return;
                    default:
                        return;
                }
            } catch (Throwable th) {
                this.a.g = this.e.b() - b2;
                switch (this.a.j) {
                    case 0:
                        this.f.post(this);
                        break;
                    case 1:
                        this.b.execute(this);
                        break;
                }
                throw th;
            }
        }
    }

    private ai a(af<T> afVar) throws IOException {
        int i = AbstractSpiCall.DEFAULT_TIMEOUT;
        int i2 = 0;
        while (true) {
            try {
                return a(afVar, i);
            } catch (SocketTimeoutException e2) {
                if (i2 < 1) {
                    i *= 2;
                    i2++;
                } else {
                    throw e2;
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v4, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARN: Type inference failed for: r4v6, types: [java.io.DataOutputStream] */
    /* JADX WARN: Type inference failed for: r4v7 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:82|83) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:81|(2:91|92)|93|94) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:22|23|(2:26|27)|28|29) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:64|65|(0)|(0)|76|77) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:38|39|(2:41|(13:43|44|45|46|47|48|49|(2:51|52)|53|54|55|56|(2:58|(2:60|61)(2:62|63)))(5:78|79|80|(1:85)(1:86)|(2:88|89)))(1:95)|96|97|98|99) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:48|49|(2:51|52)|53|54|55|56|(2:58|(2:60|61)(2:62|63))) */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        r0 = r2.getErrorStream();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0080 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00d8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x00db */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x014c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:82:0x0155 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:93:0x0173 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:96:0x0176 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d A[SYNTHETIC, Splitter:B:26:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00e3 A[Catch:{ all -> 0x0153, all -> 0x0189 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0144 A[SYNTHETIC, Splitter:B:70:0x0144] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0149 A[SYNTHETIC, Splitter:B:74:0x0149] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0080=Splitter:B:28:0x0080, B:96:0x0176=Splitter:B:96:0x0176} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:93:0x0173=Splitter:B:93:0x0173, B:55:0x00db=Splitter:B:55:0x00db, B:76:0x014c=Splitter:B:76:0x014c} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.chartboost.sdk.impl.ai a(com.chartboost.sdk.impl.af<T> r10, int r11) throws java.io.IOException {
        /*
            r9 = this;
            com.chartboost.sdk.impl.ag r0 = r10.a()
            java.util.Map<java.lang.String, java.lang.String> r1 = r0.a
            com.chartboost.sdk.impl.aq r2 = r9.c
            java.net.HttpURLConnection r2 = r2.a(r10)
            r2.setConnectTimeout(r11)
            r2.setReadTimeout(r11)
            r11 = 0
            r2.setUseCaches(r11)
            r3 = 1
            r2.setDoInput(r3)
            if (r1 == 0) goto L_0x003d
            java.util.Set r4 = r1.keySet()     // Catch:{ all -> 0x003a }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x003a }
        L_0x0024:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x003a }
            if (r5 == 0) goto L_0x003d
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x003a }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x003a }
            java.lang.Object r6 = r1.get(r5)     // Catch:{ all -> 0x003a }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x003a }
            r2.addRequestProperty(r5, r6)     // Catch:{ all -> 0x003a }
            goto L_0x0024
        L_0x003a:
            r10 = move-exception
            goto L_0x01a9
        L_0x003d:
            java.lang.String r1 = r10.b     // Catch:{ all -> 0x003a }
            r2.setRequestMethod(r1)     // Catch:{ all -> 0x003a }
            java.lang.String r1 = r10.b     // Catch:{ all -> 0x003a }
            java.lang.String r4 = "POST"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x003a }
            r4 = 0
            if (r1 == 0) goto L_0x0081
            byte[] r1 = r0.b     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0081
            r2.setDoOutput(r3)     // Catch:{ all -> 0x003a }
            byte[] r1 = r0.b     // Catch:{ all -> 0x003a }
            int r1 = r1.length     // Catch:{ all -> 0x003a }
            r2.setFixedLengthStreamingMode(r1)     // Catch:{ all -> 0x003a }
            java.lang.String r1 = r0.c     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0065
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = r0.c     // Catch:{ all -> 0x003a }
            r2.addRequestProperty(r1, r3)     // Catch:{ all -> 0x003a }
        L_0x0065:
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ all -> 0x007a }
            java.io.OutputStream r3 = r2.getOutputStream()     // Catch:{ all -> 0x007a }
            r1.<init>(r3)     // Catch:{ all -> 0x007a }
            byte[] r0 = r0.b     // Catch:{ all -> 0x0077 }
            r1.write(r0)     // Catch:{ all -> 0x0077 }
            r1.close()     // Catch:{ IOException -> 0x0081 }
            goto L_0x0081
        L_0x0077:
            r10 = move-exception
            r4 = r1
            goto L_0x007b
        L_0x007a:
            r10 = move-exception
        L_0x007b:
            if (r4 == 0) goto L_0x0080
            r4.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0080:
            throw r10     // Catch:{ all -> 0x003a }
        L_0x0081:
            com.chartboost.sdk.Libraries.i r0 = r9.e     // Catch:{ all -> 0x003a }
            long r0 = r0.b()     // Catch:{ all -> 0x003a }
            int r3 = r2.getResponseCode()     // Catch:{ all -> 0x019d }
            com.chartboost.sdk.Libraries.i r5 = r9.e     // Catch:{ all -> 0x003a }
            long r5 = r5.b()     // Catch:{ all -> 0x003a }
            r7 = 0
            long r0 = r5 - r0
            r10.h = r0     // Catch:{ all -> 0x003a }
            r0 = -1
            if (r3 == r0) goto L_0x0195
            boolean r0 = a(r3)     // Catch:{ all -> 0x0189 }
            if (r0 == 0) goto L_0x0174
            java.io.File r0 = r10.f     // Catch:{ all -> 0x0189 }
            if (r0 == 0) goto L_0x014d
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0189 }
            java.io.File r1 = r10.f     // Catch:{ all -> 0x0189 }
            java.io.File r1 = r1.getParentFile()     // Catch:{ all -> 0x0189 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0189 }
            r7.<init>()     // Catch:{ all -> 0x0189 }
            java.io.File r8 = r10.f     // Catch:{ all -> 0x0189 }
            java.lang.String r8 = r8.getName()     // Catch:{ all -> 0x0189 }
            r7.append(r8)     // Catch:{ all -> 0x0189 }
            java.lang.String r8 = ".tmp"
            r7.append(r8)     // Catch:{ all -> 0x0189 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0189 }
            r0.<init>(r1, r7)     // Catch:{ all -> 0x0189 }
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0189 }
            java.io.InputStream r1 = r2.getInputStream()     // Catch:{ all -> 0x0140 }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ all -> 0x013e }
            r7.<init>(r0)     // Catch:{ all -> 0x013e }
            com.chartboost.sdk.impl.bk.a(r1, r7)     // Catch:{ all -> 0x013b }
            if (r1 == 0) goto L_0x00d8
            r1.close()     // Catch:{ IOException -> 0x00d8 }
        L_0x00d8:
            r7.close()     // Catch:{ IOException -> 0x00db }
        L_0x00db:
            java.io.File r1 = r10.f     // Catch:{ all -> 0x0189 }
            boolean r1 = r0.renameTo(r1)     // Catch:{ all -> 0x0189 }
            if (r1 != 0) goto L_0x0176
            boolean r11 = r0.delete()     // Catch:{ all -> 0x0189 }
            if (r11 != 0) goto L_0x0112
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0189 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0189 }
            r1.<init>()     // Catch:{ all -> 0x0189 }
            java.lang.String r3 = "Unable to delete "
            r1.append(r3)     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0189 }
            r1.append(r0)     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = " after failing to rename to "
            r1.append(r0)     // Catch:{ all -> 0x0189 }
            java.io.File r0 = r10.f     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0189 }
            r1.append(r0)     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0189 }
            r11.<init>(r0)     // Catch:{ all -> 0x0189 }
            throw r11     // Catch:{ all -> 0x0189 }
        L_0x0112:
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0189 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0189 }
            r1.<init>()     // Catch:{ all -> 0x0189 }
            java.lang.String r3 = "Unable to move "
            r1.append(r3)     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0189 }
            r1.append(r0)     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = " to "
            r1.append(r0)     // Catch:{ all -> 0x0189 }
            java.io.File r0 = r10.f     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0189 }
            r1.append(r0)     // Catch:{ all -> 0x0189 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0189 }
            r11.<init>(r0)     // Catch:{ all -> 0x0189 }
            throw r11     // Catch:{ all -> 0x0189 }
        L_0x013b:
            r11 = move-exception
            r4 = r7
            goto L_0x0142
        L_0x013e:
            r11 = move-exception
            goto L_0x0142
        L_0x0140:
            r11 = move-exception
            r1 = r4
        L_0x0142:
            if (r1 == 0) goto L_0x0147
            r1.close()     // Catch:{ IOException -> 0x0147 }
        L_0x0147:
            if (r4 == 0) goto L_0x014c
            r4.close()     // Catch:{ IOException -> 0x014c }
        L_0x014c:
            throw r11     // Catch:{ all -> 0x0189 }
        L_0x014d:
            java.io.InputStream r0 = r2.getInputStream()     // Catch:{ IOException -> 0x0155 }
        L_0x0151:
            r4 = r0
            goto L_0x015a
        L_0x0153:
            r11 = move-exception
            goto L_0x016e
        L_0x0155:
            java.io.InputStream r0 = r2.getErrorStream()     // Catch:{ all -> 0x0153 }
            goto L_0x0151
        L_0x015a:
            if (r4 == 0) goto L_0x0166
            java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0153 }
            r11.<init>(r4)     // Catch:{ all -> 0x0153 }
            byte[] r11 = com.chartboost.sdk.impl.bk.b(r11)     // Catch:{ all -> 0x0153 }
            goto L_0x0168
        L_0x0166:
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0153 }
        L_0x0168:
            if (r4 == 0) goto L_0x0176
            r4.close()     // Catch:{ IOException -> 0x0176 }
            goto L_0x0176
        L_0x016e:
            if (r4 == 0) goto L_0x0173
            r4.close()     // Catch:{ IOException -> 0x0173 }
        L_0x0173:
            throw r11     // Catch:{ all -> 0x0189 }
        L_0x0174:
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0189 }
        L_0x0176:
            com.chartboost.sdk.Libraries.i r0 = r9.e     // Catch:{ all -> 0x003a }
            long r0 = r0.b()     // Catch:{ all -> 0x003a }
            r4 = 0
            long r0 = r0 - r5
            r10.i = r0     // Catch:{ all -> 0x003a }
            com.chartboost.sdk.impl.ai r10 = new com.chartboost.sdk.impl.ai     // Catch:{ all -> 0x003a }
            r10.<init>(r3, r11)     // Catch:{ all -> 0x003a }
            r2.disconnect()
            return r10
        L_0x0189:
            r11 = move-exception
            com.chartboost.sdk.Libraries.i r0 = r9.e     // Catch:{ all -> 0x003a }
            long r0 = r0.b()     // Catch:{ all -> 0x003a }
            r3 = 0
            long r0 = r0 - r5
            r10.i = r0     // Catch:{ all -> 0x003a }
            throw r11     // Catch:{ all -> 0x003a }
        L_0x0195:
            java.io.IOException r10 = new java.io.IOException     // Catch:{ all -> 0x003a }
            java.lang.String r11 = "Could not retrieve response code from HttpUrlConnection."
            r10.<init>(r11)     // Catch:{ all -> 0x003a }
            throw r10     // Catch:{ all -> 0x003a }
        L_0x019d:
            r11 = move-exception
            com.chartboost.sdk.Libraries.i r3 = r9.e     // Catch:{ all -> 0x003a }
            long r3 = r3.b()     // Catch:{ all -> 0x003a }
            r5 = 0
            long r3 = r3 - r0
            r10.h = r3     // Catch:{ all -> 0x003a }
            throw r11     // Catch:{ all -> 0x003a }
        L_0x01a9:
            r2.disconnect()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ap.a(com.chartboost.sdk.impl.af, int):com.chartboost.sdk.impl.ai");
    }

    /* renamed from: a */
    public int compareTo(ap apVar) {
        return this.a.d - apVar.a.d;
    }
}
