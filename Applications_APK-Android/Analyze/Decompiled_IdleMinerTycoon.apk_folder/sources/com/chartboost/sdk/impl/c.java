package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants;

public class c {
    public final int a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final boolean g;
    public final boolean h;

    private c(int i, String str, String str2, String str3, String str4, String str5, boolean z, boolean z2) {
        this.a = i;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = z;
        this.h = z2;
    }

    public static c a() {
        return new c(0, "interstitial", "interstitial", "/interstitial/get", "webview/%s/interstitial/get", "/interstitial/show", false, false);
    }

    public static c b() {
        return new c(1, Constants.CONVERT_REWARDED, "rewarded-video", "/reward/get", "webview/%s/reward/get", "/reward/show", true, false);
    }

    public static c c() {
        return new c(2, "inplay", null, "/inplay/get", "no webview endpoint", "/inplay/show", false, true);
    }

    public String a(int i) {
        Object[] objArr = new Object[2];
        objArr[0] = this.c;
        objArr[1] = i == 1 ? AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB : "native";
        return String.format("%s-%s", objArr);
    }

    public void a(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    i.d.didClickInterstitial(str);
                    return;
                case 1:
                    i.d.didClickRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void b(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    i.d.didCloseInterstitial(str);
                    return;
                case 1:
                    i.d.didCloseRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void c(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    i.d.didDismissInterstitial(str);
                    return;
                case 1:
                    i.d.didDismissRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void d(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    i.d.didCacheInterstitial(str);
                    return;
                case 1:
                    i.d.didCacheRewardedVideo(str);
                    return;
                case 2:
                    i.d.didCacheInPlay(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(String str, CBError.CBImpressionError cBImpressionError) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    i.d.didFailToLoadInterstitial(str, cBImpressionError);
                    return;
                case 1:
                    i.d.didFailToLoadRewardedVideo(str, cBImpressionError);
                    return;
                case 2:
                    i.d.didFailToLoadInPlay(str, cBImpressionError);
                    return;
                default:
                    return;
            }
        }
    }

    public void e(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    i.d.didDisplayInterstitial(str);
                    return;
                case 1:
                    i.d.didDisplayRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public boolean f(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    return i.d.shouldDisplayInterstitial(str);
                case 1:
                    return i.d.shouldDisplayRewardedVideo(str);
            }
        }
        return true;
    }

    public boolean g(String str) {
        if (i.d != null) {
            switch (this.a) {
                case 0:
                    return i.d.shouldRequestInterstitial(str);
            }
        }
        return true;
    }

    public class a implements Runnable {
        private final int b;
        private final String c;
        private final CBError.CBImpressionError d;

        public a(int i, String str, CBError.CBImpressionError cBImpressionError) {
            this.b = i;
            this.c = str;
            this.d = cBImpressionError;
        }

        public void run() {
            switch (this.b) {
                case 0:
                    c.this.d(this.c);
                    return;
                case 1:
                    c.this.a(this.c);
                    return;
                case 2:
                    c.this.b(this.c);
                    return;
                case 3:
                    c.this.c(this.c);
                    return;
                case 4:
                    c.this.a(this.c, this.d);
                    return;
                case 5:
                    c.this.e(this.c);
                    return;
                default:
                    return;
            }
        }
    }
}
