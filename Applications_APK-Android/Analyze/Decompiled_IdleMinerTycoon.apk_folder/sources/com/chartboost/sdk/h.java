package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.ak;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.am;
import com.chartboost.sdk.impl.an;
import com.chartboost.sdk.impl.aq;
import com.chartboost.sdk.impl.ar;
import com.chartboost.sdk.impl.ay;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.e;
import com.chartboost.sdk.impl.l;
import com.chartboost.sdk.impl.m;
import com.chartboost.sdk.impl.o;
import com.chartboost.sdk.impl.s;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class h {
    private static h v;
    public final Executor a;
    final l b;
    public final d c;
    public final e d;
    public final c e;
    public final e f;
    public final c g;
    public final aj h;
    public final m i;
    public final ar j;
    public final e k;
    public final c l;
    public final AtomicReference<com.chartboost.sdk.Model.e> m;
    final SharedPreferences n;
    public final com.chartboost.sdk.Tracking.a o;
    public final Handler p;
    public final c q;
    public final am r;
    boolean s = true;
    boolean t = false;
    boolean u = true;
    private final s w;

    public static h a() {
        return v;
    }

    static void a(h hVar) {
        v = hVar;
    }

    h(Activity activity, String str, String str2, s sVar, ScheduledExecutorService scheduledExecutorService, Handler handler, Executor executor) {
        JSONObject jSONObject;
        s sVar2 = sVar;
        ScheduledExecutorService scheduledExecutorService2 = scheduledExecutorService;
        Handler handler2 = handler;
        g a2 = g.a();
        i.n = activity.getApplicationContext();
        this.c = (d) a2.a(new d(i.n));
        ak akVar = (ak) a2.a(new ak());
        i iVar = (i) a2.a(new i());
        this.h = (aj) a2.a(new aj(scheduledExecutorService, (aq) a2.a(new aq()), akVar, iVar, handler, executor));
        SharedPreferences a3 = a(i.n);
        try {
            jSONObject = new JSONObject(a3.getString("config", "{}"));
        } catch (Exception e2) {
            CBLogging.b("Sdk", "Unable to process config");
            e2.printStackTrace();
            jSONObject = new JSONObject();
        }
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference = new AtomicReference<>(null);
        if (!b.a(atomicReference, jSONObject, a3)) {
            atomicReference.set(new com.chartboost.sdk.Model.e(new JSONObject()));
        }
        this.w = sVar2;
        this.a = scheduledExecutorService2;
        this.m = atomicReference;
        this.n = a3;
        this.p = handler2;
        f fVar = new f(sVar2, i.n, atomicReference);
        if (!atomicReference.get().y) {
            i.w = "";
        } else {
            a(i.n, null, a3);
        }
        ak akVar2 = akVar;
        f fVar2 = fVar;
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference2 = atomicReference;
        SharedPreferences sharedPreferences = a3;
        this.j = (ar) a2.a(new ar(i.n, str, this.c, akVar2, atomicReference2, a3, iVar));
        this.o = (com.chartboost.sdk.Tracking.a) a2.a(new com.chartboost.sdk.Tracking.a(atomicReference));
        this.b = (l) a2.a(new l(scheduledExecutorService, fVar2, this.h, akVar2, atomicReference2, iVar, this.o));
        d dVar = (d) a2.a(new d((ay) g.a().a(new ay(handler2)), this.b, atomicReference, handler2));
        this.r = (am) a2.a(new am(scheduledExecutorService2, this.h, akVar, handler2));
        ak akVar3 = akVar;
        g gVar = a2;
        this.q = (c) gVar.a(new c(activity, akVar3, this, this.o, handler, dVar));
        f fVar3 = fVar2;
        an anVar = (an) gVar.a(new an(fVar3));
        this.e = c.c();
        this.g = c.a();
        this.l = c.b();
        ScheduledExecutorService scheduledExecutorService3 = scheduledExecutorService;
        f fVar4 = fVar3;
        g gVar2 = gVar;
        ak akVar4 = akVar3;
        AtomicReference<com.chartboost.sdk.Model.e> atomicReference3 = atomicReference;
        SharedPreferences sharedPreferences2 = sharedPreferences;
        i iVar2 = iVar;
        Handler handler3 = handler;
        this.d = (e) gVar2.a(new e(this.e, scheduledExecutorService3, this.b, fVar4, this.h, akVar4, this.j, atomicReference3, sharedPreferences2, iVar2, this.o, handler3, this.q, this.r, dVar, anVar));
        this.f = (e) gVar2.a(new e(this.g, scheduledExecutorService, this.b, fVar3, this.h, akVar3, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar, anVar));
        this.k = (e) gVar2.a(new e(this.l, scheduledExecutorService, this.b, fVar3, this.h, akVar3, this.j, atomicReference, sharedPreferences, iVar, this.o, handler, this.q, this.r, dVar, anVar));
        this.i = (m) gVar2.a(new m(this.b, fVar3, this.h, this.j, this.o, atomicReference));
        i.l = str;
        i.m = str2;
        SharedPreferences sharedPreferences3 = sharedPreferences;
        if (!sharedPreferences3.contains("cbLimitTrack") || sharedPreferences3.contains("cbGDPR")) {
            i.x = Chartboost.CBPIDataUseConsent.valueOf(sharedPreferences3.getInt("cbGDPR", i.x.getValue()));
        } else {
            i.x = sharedPreferences3.getBoolean("cbLimitTrack", false) ? Chartboost.CBPIDataUseConsent.NO_BEHAVIORAL : Chartboost.CBPIDataUseConsent.UNKNOWN;
        }
        if (s.a().a(19)) {
            o.a(activity.getApplication(), atomicReference.get().J, !atomicReference.get().K, !atomicReference.get().L);
        }
    }

    private static SharedPreferences a(Context context) {
        return context.getSharedPreferences("cbPrefs", 0);
    }

    /* access modifiers changed from: package-private */
    public void a(final Runnable runnable) {
        this.s = true;
        al alVar = new al("/api/config", this.j, this.o, 1, new al.a() {
            public void a(al alVar, JSONObject jSONObject) {
                h.this.s = false;
                JSONObject a2 = com.chartboost.sdk.Libraries.e.a(jSONObject, "response");
                if (a2 != null && b.a(h.this.m, a2, h.this.n)) {
                    h.this.n.edit().putString("config", a2.toString()).apply();
                }
                if (runnable != null) {
                    runnable.run();
                }
                if (!h.this.t) {
                    a aVar = i.d;
                    if (aVar != null) {
                        aVar.didInitialize();
                    }
                    h.this.t = true;
                }
            }

            public void a(al alVar, CBError cBError) {
                h.this.s = false;
                if (runnable != null) {
                    runnable.run();
                }
                if (!h.this.t) {
                    a aVar = i.d;
                    if (aVar != null) {
                        aVar.didInitialize();
                    }
                    h.this.t = true;
                }
            }
        });
        alVar.l = true;
        this.h.a(alVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        if (this.w.a(23)) {
            b.a((Context) activity);
        }
        if (!this.u && !this.q.e()) {
            this.b.c();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (i.n == null) {
            CBLogging.b("Sdk", "The context must be set through the Chartboost method onCreate() before calling startSession().");
        } else {
            g();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.p.postDelayed(new a(0), 500);
    }

    private void g() {
        this.o.a();
        if (!this.u) {
            a(new a(3));
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.o.b();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (!this.t) {
            if (i.d != null) {
                i.d.didInitialize();
            }
            this.t = true;
        }
    }

    public static void b(Runnable runnable) {
        s a2 = s.a();
        if (!a2.e()) {
            a2.a.post(runnable);
        } else {
            runnable.run();
        }
    }

    static boolean f() {
        h a2 = a();
        if (a2 == null || !a2.m.get().c) {
            return true;
        }
        try {
            throw new Exception("Chartboost Integration Warning: your account has been disabled for this session. This app has no active publishing campaigns, please create a publishing campaign in the Chartboost dashboard and wait at least 30 minutes to re-enable. If you need assistance, please visit http://chartboo.st/publishing .");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public class a implements Runnable {
        final int a;
        String b = null;
        boolean c = false;
        boolean d = false;

        a(int i) {
            this.a = i;
        }

        public void run() {
            try {
                switch (this.a) {
                    case 0:
                        h.this.d();
                        return;
                    case 1:
                        i.t = this.c;
                        return;
                    case 2:
                        i.v = this.d;
                        if (!this.d || !h.f()) {
                            h.this.i.b();
                            return;
                        } else {
                            h.this.i.a();
                            return;
                        }
                    case 3:
                        al alVar = new al("api/install", h.this.j, h.this.o, 2, null);
                        alVar.l = true;
                        h.this.h.a(alVar);
                        Executor executor = h.this.a;
                        e eVar = h.this.d;
                        eVar.getClass();
                        executor.execute(new e.a(0, null, null, null));
                        Executor executor2 = h.this.a;
                        e eVar2 = h.this.f;
                        eVar2.getClass();
                        executor2.execute(new e.a(0, null, null, null));
                        Executor executor3 = h.this.a;
                        e eVar3 = h.this.k;
                        eVar3.getClass();
                        executor3.execute(new e.a(0, null, null, null));
                        h.this.a.execute(new a(4));
                        h.this.u = false;
                        return;
                    case 4:
                        h.this.i.a();
                        return;
                    case 5:
                        if (i.d != null) {
                            i.d.didFailToLoadMoreApps(this.b, CBError.CBImpressionError.END_POINT_DISABLED);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(a.class, "run (" + this.a + ")", e2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r3, android.webkit.WebView r4, android.content.SharedPreferences r5) {
        /*
            java.lang.String r0 = "http.agent"
            java.lang.String r0 = java.lang.System.getProperty(r0)
            r1 = 16
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x004a }
            if (r2 == r1) goto L_0x0012
            java.lang.String r3 = android.webkit.WebSettings.getDefaultUserAgent(r3)     // Catch:{ Exception -> 0x004a }
        L_0x0010:
            r0 = r3
            goto L_0x004a
        L_0x0012:
            if (r4 != 0) goto L_0x0041
            java.lang.String r4 = "user_agent"
            boolean r4 = r5.contains(r4)     // Catch:{ Exception -> 0x004a }
            if (r4 != 0) goto L_0x0038
            android.os.Looper r4 = android.os.Looper.myLooper()     // Catch:{ Exception -> 0x004a }
            android.os.Looper r2 = android.os.Looper.getMainLooper()     // Catch:{ Exception -> 0x004a }
            if (r4 != r2) goto L_0x004a
            android.webkit.WebView r4 = new android.webkit.WebView     // Catch:{ Exception -> 0x004a }
            android.content.Context r3 = r3.getApplicationContext()     // Catch:{ Exception -> 0x004a }
            r4.<init>(r3)     // Catch:{ Exception -> 0x004a }
            android.webkit.WebSettings r3 = r4.getSettings()     // Catch:{ Exception -> 0x004a }
            java.lang.String r3 = r3.getUserAgentString()     // Catch:{ Exception -> 0x004a }
            goto L_0x0010
        L_0x0038:
            java.lang.String r3 = "user_agent"
            java.lang.String r4 = com.chartboost.sdk.i.w     // Catch:{ Exception -> 0x004a }
            java.lang.String r3 = r5.getString(r3, r4)     // Catch:{ Exception -> 0x004a }
            goto L_0x0010
        L_0x0041:
            android.webkit.WebSettings r3 = r4.getSettings()     // Catch:{ Exception -> 0x004a }
            java.lang.String r3 = r3.getUserAgentString()     // Catch:{ Exception -> 0x004a }
            goto L_0x0010
        L_0x004a:
            com.chartboost.sdk.i.w = r0
            int r3 = android.os.Build.VERSION.SDK_INT
            if (r3 != r1) goto L_0x005d
            android.content.SharedPreferences$Editor r3 = r5.edit()
            java.lang.String r4 = "user_agent"
            android.content.SharedPreferences$Editor r3 = r3.putString(r4, r0)
            r3.apply()
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.h.a(android.content.Context, android.webkit.WebView, android.content.SharedPreferences):void");
    }

    static void a(Context context, Chartboost.CBPIDataUseConsent cBPIDataUseConsent) {
        i.x = cBPIDataUseConsent;
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            a2.edit().putInt("cbGDPR", cBPIDataUseConsent.getValue()).apply();
        }
    }
}
