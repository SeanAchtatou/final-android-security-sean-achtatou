package com.vungle.warren;

import android.util.Log;
import com.appsflyer.share.Constants;
import com.vungle.warren.VungleApiClient;

public class Plugin {
    private static final String TAG = "Plugin";

    public static void addWrapperInfo(VungleApiClient.WrapperFramework wrapperFramework, String str) {
        if (wrapperFramework == null || wrapperFramework == VungleApiClient.WrapperFramework.none) {
            Log.e(TAG, "Wrapper is null or is not none");
        } else {
            VungleApiClient.HEADER_UA += ";" + wrapperFramework;
            if (str == null || str.isEmpty()) {
                Log.e(TAG, "Wrapper framework version is empty");
            } else {
                VungleApiClient.HEADER_UA += Constants.URL_PATH_DELIMITER + str;
            }
        }
        if (Vungle.isInitialized()) {
            Log.w(TAG, "VUNGLE WARNING: SDK already initialized, wou should set wrapper info before");
        }
    }
}
