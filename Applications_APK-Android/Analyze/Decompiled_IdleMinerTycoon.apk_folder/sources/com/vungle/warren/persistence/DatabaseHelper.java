package com.vungle.warren.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "vungle_db";
    private final DatabaseFactory databaseFactory;
    private volatile SQLiteDatabase wDb;

    public interface DatabaseFactory {
        void create(SQLiteDatabase sQLiteDatabase);

        void deleteData(SQLiteDatabase sQLiteDatabase);

        void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2);

        void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2);
    }

    public static class DBException extends Exception {
        public DBException(String str) {
            super(str);
        }
    }

    public DatabaseHelper(@NonNull Context context, int i, @NonNull DatabaseFactory databaseFactory2) {
        super(context.getApplicationContext(), DB_NAME, (SQLiteDatabase.CursorFactory) null, i);
        this.databaseFactory = databaseFactory2;
    }

    public synchronized void onCreate(SQLiteDatabase sQLiteDatabase) {
        this.databaseFactory.create(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.databaseFactory.onUpgrade(sQLiteDatabase, i, i2);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.databaseFactory.onDowngrade(sQLiteDatabase, i, i2);
    }

    public long insertWithConflict(String str, ContentValues contentValues, int i) throws DBException {
        try {
            loadWritableDB();
            return this.wDb.insertWithOnConflict(str, null, contentValues, i);
        } catch (SQLException e) {
            throw new DBException(e.getMessage());
        }
    }

    public long update(Query query, ContentValues contentValues) throws DBException {
        try {
            loadWritableDB();
            return (long) this.wDb.update(query.tableName, contentValues, query.selection, query.args);
        } catch (SQLException e) {
            throw new DBException(e.getMessage());
        }
    }

    public Cursor query(Query query) {
        loadWritableDB();
        return this.wDb.query(query.tableName, query.columns, query.selection, query.args, query.groupBy, query.having, query.orderBy, query.limit);
    }

    public Cursor queryRaw(String str, String[] strArr) {
        loadWritableDB();
        return this.wDb.rawQuery(str, strArr);
    }

    public void delete(Query query) throws DBException {
        try {
            getWritableDatabase().delete(query.tableName, query.selection, query.args);
        } catch (SQLException e) {
            throw new DBException(e.getMessage());
        }
    }

    public void execSQL(String str) throws DBException {
        try {
            getWritableDatabase().execSQL(str);
        } catch (SQLException e) {
            throw new DBException(e.getMessage());
        }
    }

    public synchronized void dropDb() {
        loadWritableDB();
        this.databaseFactory.deleteData(this.wDb);
        close();
        loadWritableDB();
        onCreate(this.wDb);
    }

    public void init() {
        loadWritableDB();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001c, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized android.database.sqlite.SQLiteDatabase loadWritableDB() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.database.sqlite.SQLiteDatabase r0 = r1.wDb     // Catch:{ SQLException -> 0x001a, all -> 0x0017 }
            if (r0 == 0) goto L_0x000d
            android.database.sqlite.SQLiteDatabase r0 = r1.wDb     // Catch:{ SQLException -> 0x001a, all -> 0x0017 }
            boolean r0 = r0.isOpen()     // Catch:{ SQLException -> 0x001a, all -> 0x0017 }
            if (r0 != 0) goto L_0x0013
        L_0x000d:
            android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDatabase()     // Catch:{ SQLException -> 0x001a, all -> 0x0017 }
            r1.wDb = r0     // Catch:{ SQLException -> 0x001a, all -> 0x0017 }
        L_0x0013:
            android.database.sqlite.SQLiteDatabase r0 = r1.wDb     // Catch:{ SQLException -> 0x001a, all -> 0x0017 }
            monitor-exit(r1)
            return r0
        L_0x0017:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x001a:
            r0 = 0
            monitor-exit(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.persistence.DatabaseHelper.loadWritableDB():android.database.sqlite.SQLiteDatabase");
    }
}
