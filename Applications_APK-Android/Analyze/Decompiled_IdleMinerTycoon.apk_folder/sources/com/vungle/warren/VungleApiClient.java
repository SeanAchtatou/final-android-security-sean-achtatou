package com.vungle.warren;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.security.NetworkSecurityPolicy;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.annotation.VisibleForTesting;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import com.facebook.share.internal.ShareConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.moat.analytics.mobile.vng.MoatAnalytics;
import com.moat.analytics.mobile.vng.MoatOptions;
import com.tapjoy.TJAdUnitConstants;
import com.vungle.warren.error.VungleError;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.JsonUtil;
import com.vungle.warren.network.VungleApi;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.utility.ViewUtility;
import im.getsocial.sdk.activities.MentionTypes;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.GzipSink;
import okio.Okio;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VungleApiClient {
    private static String BASE_URL = "https://ads.api.vungle.com/";
    static String HEADER_UA = (MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "VungleAmazon/6.4.11" : "VungleDroid/6.4.11");
    static final String MANUFACTURER_AMAZON = "Amazon";
    private static Set<Interceptor> logInterceptors = new HashSet();
    private static Set<Interceptor> networkInterceptors = new HashSet();
    private final String TAG = "VungleApiClient";
    private VungleApi api;
    private JsonObject appBody;
    private CacheManager cacheManager;
    private OkHttpClient client;
    /* access modifiers changed from: private */
    public Context context;
    private boolean defaultIdFallbackDisabled;
    /* access modifiers changed from: private */
    public JsonObject deviceBody;
    private boolean enableMoat;
    private VungleApi gzipApi;
    private String newEndpoint;
    private String reportAdEndpoint;
    private Repository repository;
    private String requestAdEndpoint;
    /* access modifiers changed from: private */
    public Map<String, Long> retryAfterDataMap = new ConcurrentHashMap();
    private String riEndpoint;
    private boolean shouldTransmitIMEI;
    private VungleApi timeoutApi;
    /* access modifiers changed from: private */
    public String uaString = System.getProperty("http.agent");
    private JsonObject userBody;
    private String userImei;
    private boolean willPlayAdEnabled;
    private String willPlayAdEndpoint;
    private int willPlayAdTimeout;

    public enum WrapperFramework {
        admob,
        air,
        cocos2dx,
        corona,
        dfp,
        heyzap,
        marmalade,
        mopub,
        unity,
        fyber,
        ironsource,
        upsight,
        appodeal,
        aerserv,
        adtoapp,
        tapdaq,
        vunglehbs,
        none
    }

    private String getConnectionTypeDetail(int i) {
        switch (i) {
            case 1:
                return "GPRS";
            case 2:
                return "EDGE";
            case 3:
                return "UMTS";
            case 4:
                return "CDMA";
            case 5:
                return "EVDO_0";
            case 6:
                return "EVDO_A";
            case 7:
                return "1xRTT";
            case 8:
                return "HSDPA";
            case 9:
                return "HSUPA";
            case 10:
                return "HSPA";
            case 11:
                return "IDEN";
            case 12:
                return "EVDO_B";
            case 13:
                return "LTE";
            case 14:
                return "EHPRD";
            case 15:
                return "HSPAP";
            case 16:
                return "GSM";
            case 17:
                return "TD_SCDMA";
            case 18:
                return "IWLAN";
            default:
                return "UNKNOWN";
        }
    }

    VungleApiClient(Context context2, String str, CacheManager cacheManager2, Repository repository2) {
        this.context = context2.getApplicationContext();
        OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                int code;
                Request request = chain.request();
                String encodedPath = request.url().encodedPath();
                Long l = (Long) VungleApiClient.this.retryAfterDataMap.get(encodedPath);
                if (l != null) {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(l.longValue() - System.currentTimeMillis());
                    if (seconds > 0) {
                        Response.Builder request2 = new Response.Builder().request(request);
                        return request2.addHeader("Retry-After", "" + seconds).code(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL).protocol(Protocol.HTTP_1_1).message("Server is busy").body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "{\"Error\":\"Retry-After\"}")).build();
                    }
                    VungleApiClient.this.retryAfterDataMap.remove(encodedPath);
                }
                Response proceed = chain.proceed(request);
                if (proceed != null && ((code = proceed.code()) == 429 || code == 500 || code == 502 || code == 503)) {
                    String str = proceed.headers().get("Retry-After");
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            long parseLong = Long.parseLong(str);
                            if (parseLong > 0) {
                                VungleApiClient.this.retryAfterDataMap.put(encodedPath, Long.valueOf((parseLong * 1000) + System.currentTimeMillis()));
                            }
                        } catch (NumberFormatException unused) {
                            Log.d("VungleApiClient", "Retry-After value is not an valid value");
                        }
                    }
                }
                return proceed;
            }
        });
        this.client = addInterceptor.build();
        OkHttpClient build = addInterceptor.addInterceptor(new GzipRequestInterceptor()).build();
        Retrofit build2 = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(this.client).build();
        this.api = (VungleApi) build2.create(VungleApi.class);
        this.gzipApi = (VungleApi) build2.newBuilder().client(build).build().create(VungleApi.class);
        init(context2, str, cacheManager2, repository2);
    }

    static class GzipRequestInterceptor implements Interceptor {
        private static final String CONTENT_ENCODING = "Content-Encoding";
        private static final String GZIP = "gzip";

        GzipRequestInterceptor() {
        }

        @NonNull
        public Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            if (request.body() == null || request.header("Content-Encoding") != null) {
                return chain.proceed(request);
            }
            return chain.proceed(request.newBuilder().header("Content-Encoding", "gzip").method(request.method(), gzip(request.body())).build());
        }

        private RequestBody gzip(final RequestBody requestBody) throws IOException {
            final Buffer buffer = new Buffer();
            BufferedSink buffer2 = Okio.buffer(new GzipSink(buffer));
            requestBody.writeTo(buffer2);
            buffer2.close();
            return new RequestBody() {
                public MediaType contentType() {
                    return requestBody.contentType();
                }

                public long contentLength() {
                    return buffer.size();
                }

                public void writeTo(@NonNull BufferedSink bufferedSink) throws IOException {
                    bufferedSink.write(buffer.snapshot());
                }
            };
        }
    }

    public void updateIMEI(String str, boolean z) {
        this.userImei = str;
        this.shouldTransmitIMEI = z;
    }

    public void setDefaultIdFallbackDisabled(boolean z) {
        this.defaultIdFallbackDisabled = z;
    }

    private synchronized void init(final Context context2, String str, CacheManager cacheManager2, Repository repository2) {
        this.repository = repository2;
        this.shouldTransmitIMEI = false;
        this.cacheManager = cacheManager2;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", str);
        jsonObject.addProperty(TJAdUnitConstants.String.BUNDLE, context2.getPackageName());
        String str2 = null;
        try {
            str2 = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
        }
        if (str2 == null) {
            str2 = "1.0";
        }
        jsonObject.addProperty("ver", str2);
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("make", Build.MANUFACTURER);
        jsonObject2.addProperty("model", Build.MODEL);
        jsonObject2.addProperty("osv", Build.VERSION.RELEASE);
        jsonObject2.addProperty("carrier", ((TelephonyManager) context2.getSystemService("phone")).getNetworkOperatorName());
        jsonObject2.addProperty("os", MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        jsonObject2.addProperty("w", Integer.valueOf(displayMetrics.widthPixels));
        jsonObject2.addProperty("h", Integer.valueOf(displayMetrics.heightPixels));
        JsonObject jsonObject3 = new JsonObject();
        jsonObject3.add("vungle", new JsonObject());
        jsonObject2.add("ext", jsonObject3);
        try {
            if (Build.VERSION.SDK_INT >= 17) {
                this.uaString = getUserAgentFromCookie();
                initUserAgentLazy();
            } else if (Looper.getMainLooper() == Looper.myLooper()) {
                this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
            } else {
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        try {
                            String unused = VungleApiClient.this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
                        } catch (InstantiationException e) {
                            Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
                        }
                        countDownLatch.countDown();
                    }
                });
                if (!countDownLatch.await(2, TimeUnit.SECONDS)) {
                    Log.e("VungleApiClient", "Unable to get User Agent String in specified time");
                }
            }
        } catch (Exception e) {
            Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
        }
        jsonObject2.addProperty("ua", this.uaString);
        this.deviceBody = jsonObject2;
        this.appBody = jsonObject;
    }

    @RequiresApi(api = 17)
    private void initUserAgentLazy() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    String unused = VungleApiClient.this.uaString = WebSettings.getDefaultUserAgent(VungleApiClient.this.context);
                    VungleApiClient.this.deviceBody.addProperty("ua", VungleApiClient.this.uaString);
                    VungleApiClient.this.addUserAgentInCookie(VungleApiClient.this.uaString);
                } catch (Exception e) {
                    Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
                }
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void */
    public retrofit2.Response<JsonObject> config() throws VungleException, IOException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("device", getDeviceBody());
        jsonObject.add("app", this.appBody);
        jsonObject.add(MentionTypes.USER, getUserBody());
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("is_auto_cached_enforced", (Boolean) false);
        jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
        retrofit2.Response<JsonObject> execute = this.api.config(HEADER_UA, jsonObject).execute();
        if (!execute.isSuccessful()) {
            return execute;
        }
        JsonObject body = execute.body();
        Log.d("VungleApiClient", "Config Response: " + body);
        if (JsonUtil.hasNonNull(body, "sleep")) {
            String asString = JsonUtil.hasNonNull(body, TJAdUnitConstants.String.VIDEO_INFO) ? body.get(TJAdUnitConstants.String.VIDEO_INFO).getAsString() : "";
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. " + asString);
            throw new VungleException(3);
        } else if (JsonUtil.hasNonNull(body, "endpoints")) {
            JsonObject asJsonObject = body.getAsJsonObject("endpoints");
            HttpUrl parse = HttpUrl.parse(asJsonObject.get(AppSettingsData.STATUS_NEW).getAsString());
            HttpUrl parse2 = HttpUrl.parse(asJsonObject.get(CampaignUnit.JSON_KEY_ADS).getAsString());
            HttpUrl parse3 = HttpUrl.parse(asJsonObject.get("will_play_ad").getAsString());
            HttpUrl parse4 = HttpUrl.parse(asJsonObject.get("report_ad").getAsString());
            HttpUrl parse5 = HttpUrl.parse(asJsonObject.get("ri").getAsString());
            if (parse == null || parse2 == null || parse3 == null || parse4 == null || parse5 == null) {
                Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
                throw new VungleException(3);
            }
            this.newEndpoint = parse.toString();
            this.requestAdEndpoint = parse2.toString();
            this.willPlayAdEndpoint = parse3.toString();
            this.reportAdEndpoint = parse4.toString();
            this.riEndpoint = parse5.toString();
            JsonObject asJsonObject2 = body.getAsJsonObject("will_play_ad");
            this.willPlayAdTimeout = asJsonObject2.get("request_timeout").getAsInt();
            this.willPlayAdEnabled = asJsonObject2.get(TJAdUnitConstants.String.ENABLED).getAsBoolean();
            this.enableMoat = body.getAsJsonObject("viewability").get("moat").getAsBoolean();
            if (this.willPlayAdEnabled) {
                Log.v("VungleApiClient", "willPlayAd is enabled, generating a timeout client.");
                this.timeoutApi = (VungleApi) new Retrofit.Builder().client(this.client.newBuilder().readTimeout((long) this.willPlayAdTimeout, TimeUnit.MILLISECONDS).build()).addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.vungle.com/").build().create(VungleApi.class);
            }
            if (getMoatEnabled()) {
                MoatOptions moatOptions = new MoatOptions();
                moatOptions.disableAdIdCollection = true;
                moatOptions.disableLocationServices = true;
                moatOptions.loggingEnabled = true;
                MoatAnalytics.getInstance().start(moatOptions, (Application) this.context.getApplicationContext());
            }
            return execute;
        } else {
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
            throw new VungleException(3);
        }
    }

    public Call<JsonObject> reportNew() throws IllegalStateException {
        if (this.newEndpoint != null) {
            HashMap hashMap = new HashMap(2);
            JsonElement jsonElement = this.appBody.get("id");
            JsonElement jsonElement2 = this.deviceBody.get("ifa");
            hashMap.put("app_id", jsonElement != null ? jsonElement.getAsString() : "");
            hashMap.put("ifa", jsonElement2 != null ? jsonElement2.getAsString() : "");
            return this.api.reportNew(HEADER_UA, this.newEndpoint, hashMap);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> requestAd(String str, boolean z) throws IllegalStateException {
        if (this.requestAdEndpoint != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("device", getDeviceBody());
            jsonObject.add("app", this.appBody);
            jsonObject.add(MentionTypes.USER, getUserBody());
            JsonObject jsonObject2 = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(str);
            jsonObject2.add("placements", jsonArray);
            jsonObject2.addProperty("header_bidding", Boolean.valueOf(z));
            jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
            return this.api.ads(HEADER_UA, this.requestAdEndpoint, jsonObject);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> willPlayAd(String str, boolean z, String str2) throws IllegalStateException, VungleError {
        if (this.willPlayAdEndpoint == null) {
            throw new IllegalStateException("API Client not configured yet! Must call /config first.");
        } else if (this.willPlayAdEnabled) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("device", getDeviceBody());
            jsonObject.add("app", this.appBody);
            jsonObject.add(MentionTypes.USER, getUserBody());
            JsonObject jsonObject2 = new JsonObject();
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty("reference_id", str);
            jsonObject3.addProperty("is_auto_cached", Boolean.valueOf(z));
            jsonObject2.add("placement", jsonObject3);
            jsonObject2.addProperty(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_AD_TOKEN, str2);
            jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
            return this.timeoutApi.willPlayAd(HEADER_UA, this.willPlayAdEndpoint, jsonObject);
        } else {
            throw new VungleError(6);
        }
    }

    public Call<JsonObject> reportAd(JsonObject jsonObject) {
        if (this.reportAdEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.add("device", getDeviceBody());
            jsonObject2.add("app", this.appBody);
            jsonObject2.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject);
            jsonObject2.add(MentionTypes.USER, getUserBody());
            return this.gzipApi.reportAd(HEADER_UA, this.reportAdEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> ri(JsonObject jsonObject) {
        if (this.riEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.add("device", getDeviceBody());
            jsonObject2.add("app", this.appBody);
            jsonObject2.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject);
            return this.api.ri(HEADER_UA, this.riEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public boolean pingTPAT(String str) throws ClearTextTrafficException, MalformedURLException {
        boolean z;
        if (TextUtils.isEmpty(str) || HttpUrl.parse(str) == null) {
            throw new MalformedURLException("Invalid URL : " + str);
        }
        try {
            String host = new URL(str).getHost();
            if (Build.VERSION.SDK_INT >= 24) {
                z = NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(host);
            } else {
                z = Build.VERSION.SDK_INT >= 23 ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : true;
            }
            if (z || !URLUtil.isHttpUrl(str)) {
                if (!TextUtils.isEmpty(this.userImei) && this.shouldTransmitIMEI) {
                    str = str.replace("%imei%", this.userImei);
                }
                try {
                    this.api.pingTPAT(this.uaString, str).execute();
                    return true;
                } catch (IOException unused) {
                    return false;
                }
            } else {
                throw new ClearTextTrafficException("Clear Text Traffic is blocked");
            }
        } catch (MalformedURLException unused2) {
            throw new MalformedURLException("Invalid URL : " + str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01b3  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x03ae  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x03b1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x018b  */
    @android.annotation.SuppressLint({"HardwareIds"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.gson.JsonObject getDeviceBody() throws java.lang.IllegalStateException {
        /*
            r11 = this;
            com.google.gson.JsonObject r0 = new com.google.gson.JsonObject
            r0.<init>()
            r1 = 0
            r2 = 0
            r3 = 1
            java.lang.String r4 = "Amazon"
            java.lang.String r5 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0090 }
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x0090 }
            if (r4 == 0) goto L_0x003c
            android.content.Context r4 = r11.context     // Catch:{ SettingNotFoundException -> 0x002d }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x002d }
            java.lang.String r5 = "limit_ad_tracking"
            int r5 = android.provider.Settings.Secure.getInt(r4, r5)     // Catch:{ SettingNotFoundException -> 0x002d }
            if (r5 != r3) goto L_0x0022
            r5 = 1
            goto L_0x0023
        L_0x0022:
            r5 = 0
        L_0x0023:
            java.lang.String r6 = "advertising_id"
            java.lang.String r4 = android.provider.Settings.Secure.getString(r4, r6)     // Catch:{ SettingNotFoundException -> 0x002b }
            goto L_0x0099
        L_0x002b:
            r4 = move-exception
            goto L_0x002f
        L_0x002d:
            r4 = move-exception
            r5 = 1
        L_0x002f:
            java.lang.String r6 = "VungleApiClient"
            java.lang.String r7 = "Error getting Amazon advertising info"
            android.util.Log.w(r6, r7, r4)     // Catch:{ Exception -> 0x0039 }
            r4 = r1
            goto L_0x0099
        L_0x0039:
            r4 = r1
            goto L_0x0092
        L_0x003c:
            android.content.Context r4 = r11.context     // Catch:{ NoClassDefFoundError -> 0x0060 }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r4 = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(r4)     // Catch:{ NoClassDefFoundError -> 0x0060 }
            if (r4 == 0) goto L_0x005a
            java.lang.String r5 = r4.getId()     // Catch:{ NoClassDefFoundError -> 0x0060 }
            boolean r4 = r4.isLimitAdTrackingEnabled()     // Catch:{ NoClassDefFoundError -> 0x0058, Exception -> 0x0056 }
            com.google.gson.JsonObject r6 = r11.deviceBody     // Catch:{ NoClassDefFoundError -> 0x0054 }
            java.lang.String r7 = "ifa"
            r6.addProperty(r7, r5)     // Catch:{ NoClassDefFoundError -> 0x0054 }
            goto L_0x005c
        L_0x0054:
            r6 = move-exception
            goto L_0x0063
        L_0x0056:
            r4 = r5
            goto L_0x0091
        L_0x0058:
            r6 = move-exception
            goto L_0x0062
        L_0x005a:
            r5 = r1
            r4 = 1
        L_0x005c:
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x0099
        L_0x0060:
            r6 = move-exception
            r5 = r1
        L_0x0062:
            r4 = 1
        L_0x0063:
            java.lang.String r7 = "VungleApiClient"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008c }
            r8.<init>()     // Catch:{ Exception -> 0x008c }
            java.lang.String r9 = "Play services Not available: "
            r8.append(r9)     // Catch:{ Exception -> 0x008c }
            java.lang.String r6 = r6.getLocalizedMessage()     // Catch:{ Exception -> 0x008c }
            r8.append(r6)     // Catch:{ Exception -> 0x008c }
            java.lang.String r6 = r8.toString()     // Catch:{ Exception -> 0x008c }
            android.util.Log.e(r7, r6)     // Catch:{ Exception -> 0x008c }
            android.content.Context r6 = r11.context     // Catch:{ Exception -> 0x008c }
            android.content.ContentResolver r6 = r6.getContentResolver()     // Catch:{ Exception -> 0x008c }
            java.lang.String r7 = "advertising_id"
            java.lang.String r6 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ Exception -> 0x008c }
            r5 = r4
            r4 = r6
            goto L_0x0099
        L_0x008c:
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x0092
        L_0x0090:
            r4 = r1
        L_0x0091:
            r5 = 1
        L_0x0092:
            java.lang.String r6 = "VungleApiClient"
            java.lang.String r7 = "Cannot load Advertising ID"
            android.util.Log.e(r6, r7)
        L_0x0099:
            if (r4 == 0) goto L_0x00b5
            java.lang.String r6 = "Amazon"
            java.lang.String r7 = android.os.Build.MANUFACTURER
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x00a8
            java.lang.String r6 = "amazon_advertising_id"
            goto L_0x00aa
        L_0x00a8:
            java.lang.String r6 = "gaid"
        L_0x00aa:
            r0.addProperty(r6, r4)
            com.google.gson.JsonObject r6 = r11.deviceBody
            java.lang.String r7 = "ifa"
            r6.addProperty(r7, r4)
            goto L_0x00e8
        L_0x00b5:
            android.content.Context r4 = r11.context
            android.content.ContentResolver r4 = r4.getContentResolver()
            java.lang.String r6 = "android_id"
            java.lang.String r4 = android.provider.Settings.Secure.getString(r4, r6)
            com.google.gson.JsonObject r6 = r11.deviceBody
            java.lang.String r7 = "ifa"
            boolean r8 = r11.defaultIdFallbackDisabled
            if (r8 == 0) goto L_0x00cc
            java.lang.String r8 = ""
            goto L_0x00d6
        L_0x00cc:
            boolean r8 = android.text.TextUtils.isEmpty(r4)
            if (r8 != 0) goto L_0x00d4
            r8 = r4
            goto L_0x00d6
        L_0x00d4:
            java.lang.String r8 = ""
        L_0x00d6:
            r6.addProperty(r7, r8)
            boolean r6 = android.text.TextUtils.isEmpty(r4)
            if (r6 != 0) goto L_0x00e8
            boolean r6 = r11.defaultIdFallbackDisabled
            if (r6 != 0) goto L_0x00e8
            java.lang.String r6 = "android_id"
            r0.addProperty(r6, r4)
        L_0x00e8:
            com.google.gson.JsonObject r4 = r11.deviceBody
            java.lang.String r6 = "lmt"
            if (r5 == 0) goto L_0x00f0
            r5 = 1
            goto L_0x00f1
        L_0x00f0:
            r5 = 0
        L_0x00f1:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4.addProperty(r6, r5)
            android.content.Context r4 = r11.context
            android.content.pm.PackageManager r4 = r4.getPackageManager()
            r5 = 128(0x80, float:1.794E-43)
            java.util.List r4 = r4.getInstalledPackages(r5)
            java.util.Iterator r4 = r4.iterator()
            r5 = 0
        L_0x0109:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L_0x0121
            java.lang.Object r6 = r4.next()
            android.content.pm.PackageInfo r6 = (android.content.pm.PackageInfo) r6
            java.lang.String r6 = r6.packageName
            java.lang.String r7 = "com.google.android.gms"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 == 0) goto L_0x0109
            r5 = 1
            goto L_0x0109
        L_0x0121:
            java.lang.String r4 = "is_google_play_services_available"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            r0.addProperty(r4, r5)
            android.content.Context r4 = r11.context
            android.content.IntentFilter r5 = new android.content.IntentFilter
            java.lang.String r6 = "android.intent.action.BATTERY_CHANGED"
            r5.<init>(r6)
            android.content.Intent r1 = r4.registerReceiver(r1, r5)
            java.lang.String r4 = "level"
            r5 = -1
            int r4 = r1.getIntExtra(r4, r5)
            java.lang.String r6 = "scale"
            int r6 = r1.getIntExtra(r6, r5)
            if (r4 <= 0) goto L_0x0154
            if (r6 <= 0) goto L_0x0154
            java.lang.String r7 = "battery_level"
            float r4 = (float) r4
            float r6 = (float) r6
            float r4 = r4 / r6
            java.lang.Float r4 = java.lang.Float.valueOf(r4)
            r0.addProperty(r7, r4)
        L_0x0154:
            java.lang.String r4 = "status"
            int r4 = r1.getIntExtra(r4, r5)
            r6 = 4
            if (r4 != r5) goto L_0x0160
            java.lang.String r1 = "UNKNOWN"
            goto L_0x0180
        L_0x0160:
            r7 = 2
            if (r4 == r7) goto L_0x016a
            r7 = 5
            if (r4 != r7) goto L_0x0167
            goto L_0x016a
        L_0x0167:
            java.lang.String r1 = "NOT_CHARGING"
            goto L_0x0180
        L_0x016a:
            java.lang.String r4 = "plugged"
            int r1 = r1.getIntExtra(r4, r5)
            if (r1 == r6) goto L_0x017e
            switch(r1) {
                case 1: goto L_0x017b;
                case 2: goto L_0x0178;
                default: goto L_0x0175;
            }
        L_0x0175:
            java.lang.String r1 = "BATTERY_PLUGGED_OTHERS"
            goto L_0x0180
        L_0x0178:
            java.lang.String r1 = "BATTERY_PLUGGED_USB"
            goto L_0x0180
        L_0x017b:
            java.lang.String r1 = "BATTERY_PLUGGED_AC"
            goto L_0x0180
        L_0x017e:
            java.lang.String r1 = "BATTERY_PLUGGED_WIRELESS"
        L_0x0180:
            java.lang.String r4 = "battery_state"
            r0.addProperty(r4, r1)
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 21
            if (r1 < r4) goto L_0x01a9
            android.content.Context r1 = r11.context
            java.lang.String r4 = "power"
            java.lang.Object r1 = r1.getSystemService(r4)
            android.os.PowerManager r1 = (android.os.PowerManager) r1
            java.lang.String r4 = "battery_saver_enabled"
            if (r1 == 0) goto L_0x01a1
            boolean r1 = r1.isPowerSaveMode()
            if (r1 == 0) goto L_0x01a1
            r1 = 1
            goto L_0x01a2
        L_0x01a1:
            r1 = 0
        L_0x01a2:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.addProperty(r4, r1)
        L_0x01a9:
            android.content.Context r1 = r11.context
            java.lang.String r4 = "android.permission.ACCESS_NETWORK_STATE"
            int r1 = android.support.v4.content.PermissionChecker.checkCallingOrSelfPermission(r1, r4)
            if (r1 != 0) goto L_0x0235
            java.lang.String r1 = "NONE"
            java.lang.String r4 = "NONE"
            android.content.Context r5 = r11.context
            java.lang.String r7 = "connectivity"
            java.lang.Object r5 = r5.getSystemService(r7)
            android.net.ConnectivityManager r5 = (android.net.ConnectivityManager) r5
            if (r5 == 0) goto L_0x01ee
            android.net.NetworkInfo r7 = r5.getActiveNetworkInfo()
            if (r7 == 0) goto L_0x01ee
            int r1 = r7.getType()
            switch(r1) {
                case 0: goto L_0x01e4;
                case 1: goto L_0x01df;
                case 6: goto L_0x01df;
                case 7: goto L_0x01da;
                case 9: goto L_0x01d5;
                default: goto L_0x01d0;
            }
        L_0x01d0:
            java.lang.String r1 = "UNKNOWN"
            java.lang.String r4 = "UNKNOWN"
            goto L_0x01ee
        L_0x01d5:
            java.lang.String r1 = "ETHERNET"
            java.lang.String r4 = "ETHERNET"
            goto L_0x01ee
        L_0x01da:
            java.lang.String r1 = "BLUETOOTH"
            java.lang.String r4 = "BLUETOOTH"
            goto L_0x01ee
        L_0x01df:
            java.lang.String r1 = "WIFI"
            java.lang.String r4 = "WIFI"
            goto L_0x01ee
        L_0x01e4:
            java.lang.String r1 = "MOBILE"
            int r4 = r7.getSubtype()
            java.lang.String r4 = r11.getConnectionTypeDetail(r4)
        L_0x01ee:
            java.lang.String r7 = "connection_type"
            r0.addProperty(r7, r1)
            java.lang.String r1 = "connection_type_detail"
            r0.addProperty(r1, r4)
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 24
            if (r1 < r4) goto L_0x0235
            boolean r1 = r5.isActiveNetworkMetered()
            if (r1 == 0) goto L_0x0225
            int r1 = r5.getRestrictBackgroundStatus()
            switch(r1) {
                case 1: goto L_0x0214;
                case 2: goto L_0x0211;
                case 3: goto L_0x020e;
                default: goto L_0x020b;
            }
        L_0x020b:
            java.lang.String r1 = "UNKNOWN"
            goto L_0x0216
        L_0x020e:
            java.lang.String r1 = "ENABLED"
            goto L_0x0216
        L_0x0211:
            java.lang.String r1 = "WHITELISTED"
            goto L_0x0216
        L_0x0214:
            java.lang.String r1 = "DISABLED"
        L_0x0216:
            java.lang.String r4 = "data_saver_status"
            r0.addProperty(r4, r1)
            java.lang.String r1 = "network_metered"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)
            r0.addProperty(r1, r4)
            goto L_0x0235
        L_0x0225:
            java.lang.String r1 = "data_saver_status"
            java.lang.String r4 = "NOT_APPLICABLE"
            r0.addProperty(r1, r4)
            java.lang.String r1 = "network_metered"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            r0.addProperty(r1, r4)
        L_0x0235:
            java.lang.String r1 = "locale"
            java.util.Locale r4 = java.util.Locale.getDefault()
            java.lang.String r4 = r4.toString()
            r0.addProperty(r1, r4)
            java.lang.String r1 = "language"
            java.util.Locale r4 = java.util.Locale.getDefault()
            java.lang.String r4 = r4.getLanguage()
            r0.addProperty(r1, r4)
            java.lang.String r1 = "time_zone"
            java.util.TimeZone r4 = java.util.TimeZone.getDefault()
            java.lang.String r4 = r4.getID()
            r0.addProperty(r1, r4)
            android.content.Context r1 = r11.context
            if (r1 == 0) goto L_0x0363
            android.content.Context r1 = r11.context
            java.lang.String r4 = "audio"
            java.lang.Object r1 = r1.getSystemService(r4)
            android.media.AudioManager r1 = (android.media.AudioManager) r1
            if (r1 == 0) goto L_0x028f
            r4 = 3
            int r5 = r1.getStreamMaxVolume(r4)
            int r1 = r1.getStreamVolume(r4)
            float r4 = (float) r1
            float r5 = (float) r5
            float r4 = r4 / r5
            java.lang.String r5 = "volume_level"
            java.lang.Float r4 = java.lang.Float.valueOf(r4)
            r0.addProperty(r5, r4)
            java.lang.String r4 = "sound_enabled"
            if (r1 <= 0) goto L_0x0287
            r1 = 1
            goto L_0x0288
        L_0x0287:
            r1 = 0
        L_0x0288:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.addProperty(r4, r1)
        L_0x028f:
            com.vungle.warren.persistence.CacheManager r1 = r11.cacheManager
            java.io.File r1 = r1.getCache()
            r1.getPath()
            boolean r4 = r1.exists()
            if (r4 == 0) goto L_0x02b3
            boolean r1 = r1.isDirectory()
            if (r1 == 0) goto L_0x02b3
            java.lang.String r1 = "storage_bytes_available"
            com.vungle.warren.persistence.CacheManager r4 = r11.cacheManager
            long r4 = r4.getBytesAvailable()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r0.addProperty(r1, r4)
        L_0x02b3:
            java.lang.String r1 = "Amazon"
            java.lang.String r4 = android.os.Build.MANUFACTURER
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x02ce
            android.content.Context r1 = r11.context
            android.content.Context r1 = r1.getApplicationContext()
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            java.lang.String r4 = "amazon.hardware.fire_tv"
            boolean r1 = r1.hasSystemFeature(r4)
            goto L_0x030d
        L_0x02ce:
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 23
            if (r1 < r4) goto L_0x02e8
            android.content.Context r1 = r11.context
            java.lang.String r4 = "uimode"
            java.lang.Object r1 = r1.getSystemService(r4)
            android.app.UiModeManager r1 = (android.app.UiModeManager) r1
            int r1 = r1.getCurrentModeType()
            if (r1 != r6) goto L_0x02e6
        L_0x02e4:
            r1 = 1
            goto L_0x030d
        L_0x02e6:
            r1 = 0
            goto L_0x030d
        L_0x02e8:
            android.content.Context r1 = r11.context
            android.content.Context r1 = r1.getApplicationContext()
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            java.lang.String r4 = "com.google.android.tv"
            boolean r1 = r1.hasSystemFeature(r4)
            if (r1 != 0) goto L_0x02e4
            android.content.Context r1 = r11.context
            android.content.Context r1 = r1.getApplicationContext()
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            java.lang.String r4 = "android.hardware.touchscreen"
            boolean r1 = r1.hasSystemFeature(r4)
            if (r1 != 0) goto L_0x02e6
            goto L_0x02e4
        L_0x030d:
            java.lang.String r4 = "is_tv"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r0.addProperty(r4, r1)
            java.lang.String r1 = "os_api_level"
            int r4 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r0.addProperty(r1, r4)
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ SettingNotFoundException -> 0x0351 }
            r4 = 26
            if (r1 < r4) goto L_0x0341
            android.content.Context r1 = r11.context     // Catch:{ SettingNotFoundException -> 0x0351 }
            java.lang.String r3 = "android.permission.REQUEST_INSTALL_PACKAGES"
            int r1 = r1.checkCallingOrSelfPermission(r3)     // Catch:{ SettingNotFoundException -> 0x0351 }
            if (r1 != 0) goto L_0x0359
            android.content.Context r1 = r11.context     // Catch:{ SettingNotFoundException -> 0x0351 }
            android.content.Context r1 = r1.getApplicationContext()     // Catch:{ SettingNotFoundException -> 0x0351 }
            android.content.pm.PackageManager r1 = r1.getPackageManager()     // Catch:{ SettingNotFoundException -> 0x0351 }
            boolean r1 = r1.canRequestPackageInstalls()     // Catch:{ SettingNotFoundException -> 0x0351 }
            r2 = r1
            goto L_0x0359
        L_0x0341:
            android.content.Context r1 = r11.context     // Catch:{ SettingNotFoundException -> 0x0351 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x0351 }
            java.lang.String r4 = "install_non_market_apps"
            int r1 = android.provider.Settings.Secure.getInt(r1, r4)     // Catch:{ SettingNotFoundException -> 0x0351 }
            if (r1 != r3) goto L_0x0359
            r2 = 1
            goto L_0x0359
        L_0x0351:
            r1 = move-exception
            java.lang.String r3 = "VungleApiClient"
            java.lang.String r4 = "isInstallNonMarketAppsEnabled Settings not found"
            android.util.Log.e(r3, r4, r1)
        L_0x0359:
            java.lang.String r1 = "is_sideload_enabled"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            r0.addProperty(r1, r2)
            goto L_0x0375
        L_0x0363:
            java.lang.String r1 = "volume_level"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            r0.addProperty(r1, r3)
            java.lang.String r1 = "sound_enabled"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0.addProperty(r1, r2)
        L_0x0375:
            java.lang.String r1 = android.os.Environment.getExternalStorageState()
            java.lang.String r2 = "mounted"
            boolean r1 = r1.equals(r2)
            java.lang.String r2 = "sd_card_available"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.addProperty(r2, r1)
            java.lang.String r1 = "os_name"
            java.lang.String r2 = android.os.Build.FINGERPRINT
            r0.addProperty(r1, r2)
            java.lang.String r1 = "vduid"
            java.lang.String r2 = ""
            r0.addProperty(r1, r2)
            com.google.gson.JsonObject r1 = r11.deviceBody
            java.lang.String r2 = "ext"
            com.google.gson.JsonObject r1 = r1.getAsJsonObject(r2)
            java.lang.String r2 = "vungle"
            com.google.gson.JsonObject r1 = r1.getAsJsonObject(r2)
            java.lang.String r2 = "Amazon"
            java.lang.String r3 = android.os.Build.MANUFACTURER
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x03b1
            java.lang.String r2 = "amazon"
            goto L_0x03b3
        L_0x03b1:
            java.lang.String r2 = "android"
        L_0x03b3:
            r1.add(r2, r0)
            com.google.gson.JsonObject r0 = r11.deviceBody
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.VungleApiClient.getDeviceBody():com.google.gson.JsonObject");
    }

    private JsonObject getUserBody() {
        long j;
        String str;
        String str2;
        String str3;
        if (this.userBody == null) {
            this.userBody = new JsonObject();
        }
        Cookie cookie = (Cookie) this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
        if (cookie != null) {
            str2 = cookie.getString("consent_status");
            str = cookie.getString("consent_source");
            j = cookie.getLong("timestamp").longValue();
            str3 = cookie.getString("consent_message_version");
        } else {
            str2 = "unknown";
            str = "no_interaction";
            j = 0;
            str3 = "";
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("consent_status", str2);
        jsonObject.addProperty("consent_source", str);
        jsonObject.addProperty("consent_timestamp", Long.valueOf(j));
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        jsonObject.addProperty("consent_message_version", str3);
        this.userBody.add("gdpr", jsonObject);
        return this.userBody;
    }

    public boolean getMoatEnabled() {
        return this.enableMoat && Build.VERSION.SDK_INT >= 16;
    }

    private String getUserAgentFromCookie() {
        Cookie cookie = (Cookie) this.repository.load("userAgent", Cookie.class).get();
        if (cookie == null) {
            return System.getProperty("http.agent");
        }
        String string = cookie.getString("userAgent");
        return TextUtils.isEmpty(string) ? System.getProperty("http.agent") : string;
    }

    /* access modifiers changed from: private */
    public void addUserAgentInCookie(String str) throws DatabaseHelper.DBException {
        Cookie cookie = new Cookie("userAgent");
        cookie.putValue("userAgent", str);
        this.repository.save(cookie);
    }

    public long getRetryAfterHeaderValue(retrofit2.Response<JsonObject> response) {
        try {
            return Long.parseLong(response.headers().get("Retry-After")) * 1000;
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public static class ClearTextTrafficException extends IOException {
        ClearTextTrafficException(String str) {
            super(str);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void overrideApi(VungleApi vungleApi) {
        this.api = vungleApi;
    }
}
