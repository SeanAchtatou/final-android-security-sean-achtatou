package com.vungle.warren;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.vungle.warren.DownloadStrategy;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.downloader.AssetDownloader;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.error.VungleError;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.AdAsset;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.ui.HackMraid;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.SDKExecutors;
import com.vungle.warren.utility.UnzipUtility;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdLoader {
    public static final long EXPONENTIAL_RATE = 2;
    public static final int RETRY_COUNT = 5;
    public static final long RETRY_DELAY = 2000;
    /* access modifiers changed from: private */
    public static final String TAG = AdLoader.class.getCanonicalName();
    /* access modifiers changed from: private */
    @NonNull
    public final CacheManager cacheManager;
    @NonNull
    private final AssetDownloader downloader;
    @Nullable
    private JobRunner jobRunner;
    /* access modifiers changed from: private */
    public final Map<String, Operation> loadOperations = new ConcurrentHashMap();
    private final Map<String, Operation> pendingOperations = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    @NonNull
    public final Repository repository;
    /* access modifiers changed from: private */
    @NonNull
    public final RuntimeValues runtimeValues;
    /* access modifiers changed from: private */
    @NonNull
    public final SDKExecutors sdkExecutors;
    /* access modifiers changed from: private */
    @NonNull
    public final VungleStaticApi vungleApi;
    /* access modifiers changed from: private */
    @NonNull
    public final VungleApiClient vungleApiClient;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ReschedulePolicy {
        public static final int EXPONENTIAL = 0;
        public static final int EXPONENTIAL_ENDLESS_AD = 1;
    }

    interface DownloadCallback {
        void onDownloadCompleted(@NonNull String str, @NonNull String str2);

        void onDownloadFailed(@NonNull Throwable th, @Nullable String str, @Nullable String str2);

        void onReady(@NonNull String str, @NonNull Placement placement, @NonNull Advertisement advertisement);
    }

    private boolean recoverableServerCode(int i) {
        return i == 408 || (500 <= i && i < 600);
    }

    static class Operation {
        boolean cancelable;
        final long delay;
        @Nullable
        final PublisherDirectDownload directDownload;
        final String id;
        @NonNull
        final Set<LoadAdCallback> loadAdCallbacks = new CopyOnWriteArraySet();
        @NonNull
        final AtomicBoolean loading;
        final int policy;
        final int retry;
        final long retryDelay;
        final int retryLimit;

        public Operation(String str, long j, long j2, int i, int i2, @Nullable PublisherDirectDownload publisherDirectDownload, int i3, boolean z, @Nullable LoadAdCallback... loadAdCallbackArr) {
            this.id = str;
            this.delay = j;
            this.retryDelay = j2;
            this.retryLimit = i;
            this.policy = i2;
            this.directDownload = publisherDirectDownload;
            this.retry = i3;
            this.loading = new AtomicBoolean();
            this.cancelable = z;
            if (loadAdCallbackArr != null) {
                this.loadAdCallbacks.addAll(Arrays.asList(loadAdCallbackArr));
            }
        }

        /* access modifiers changed from: package-private */
        public Operation delay(long j) {
            return new Operation(this.id, j, this.retryDelay, this.retryLimit, this.policy, this.directDownload, this.retry, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
        }

        /* access modifiers changed from: package-private */
        public Operation retryDelay(long j) {
            return new Operation(this.id, this.delay, j, this.retryLimit, this.policy, this.directDownload, this.retry, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
        }

        /* access modifiers changed from: package-private */
        public Operation retry(int i) {
            return new Operation(this.id, this.delay, this.retryDelay, this.retryLimit, this.policy, this.directDownload, i, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
        }
    }

    public void setJobRunner(@NonNull JobRunner jobRunner2) {
        this.jobRunner = jobRunner2;
    }

    public AdLoader(@NonNull SDKExecutors sDKExecutors, @NonNull Repository repository2, @NonNull VungleApiClient vungleApiClient2, @NonNull CacheManager cacheManager2, @NonNull AssetDownloader assetDownloader, @NonNull RuntimeValues runtimeValues2, @NonNull VungleStaticApi vungleStaticApi) {
        this.sdkExecutors = sDKExecutors;
        this.repository = repository2;
        this.vungleApiClient = vungleApiClient2;
        this.cacheManager = cacheManager2;
        this.downloader = assetDownloader;
        this.runtimeValues = runtimeValues2;
        this.vungleApi = vungleStaticApi;
    }

    /* access modifiers changed from: private */
    public boolean canReDownload(Advertisement advertisement) {
        if (advertisement == null || (advertisement.getState() != 0 && advertisement.getState() != 1)) {
            return false;
        }
        List<AdAsset> list = this.repository.loadAllAdAssets(advertisement.getId()).get();
        if (list.size() == 0) {
            return false;
        }
        for (AdAsset adAsset : list) {
            if (adAsset.fileType == 1) {
                if (!fileIsValid(new File(adAsset.localPath), adAsset)) {
                    return false;
                }
            } else if (TextUtils.isEmpty(adAsset.serverPath)) {
                return false;
            }
        }
        return true;
    }

    public boolean canPlayAd(Advertisement advertisement) {
        return advertisement != null && advertisement.getState() == 1 && hasAssetsFor(advertisement.getId());
    }

    public synchronized void clear() {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(this.loadOperations.keySet());
        hashSet.addAll(this.pendingOperations.keySet());
        for (String str : hashSet) {
            onError(this.loadOperations.remove(str), 25);
            onError(this.pendingOperations.remove(str), 25);
        }
    }

    public boolean isLoading(String str) {
        Operation operation = this.loadOperations.get(str);
        return operation != null && operation.loading.get();
    }

    /* access modifiers changed from: private */
    public void setLoading(String str, boolean z) {
        Operation operation = this.loadOperations.get(str);
        if (operation != null) {
            operation.loading.set(z);
        }
    }

    public synchronized void loadPendingInternal(String str) {
        Operation remove = this.pendingOperations.remove(str);
        if (remove != null) {
            load(remove.delay(0));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void load(@android.support.annotation.NonNull com.vungle.warren.AdLoader.Operation r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.vungle.warren.tasks.JobRunner r0 = r5.jobRunner     // Catch:{ all -> 0x0086 }
            if (r0 != 0) goto L_0x000c
            r0 = 9
            r5.onError(r6, r0)     // Catch:{ all -> 0x0086 }
            monitor-exit(r5)
            return
        L_0x000c:
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r5.pendingOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r6.id     // Catch:{ all -> 0x0086 }
            java.lang.Object r0 = r0.remove(r1)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdLoader$Operation r0 = (com.vungle.warren.AdLoader.Operation) r0     // Catch:{ all -> 0x0086 }
            if (r0 == 0) goto L_0x001f
            java.util.Set<com.vungle.warren.LoadAdCallback> r1 = r6.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            java.util.Set<com.vungle.warren.LoadAdCallback> r0 = r0.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            r1.addAll(r0)     // Catch:{ all -> 0x0086 }
        L_0x001f:
            long r0 = r6.delay     // Catch:{ all -> 0x0086 }
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            r0 = 1
            if (r4 > 0) goto L_0x0068
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r1 = r5.loadOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r2 = r6.id     // Catch:{ all -> 0x0086 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdLoader$Operation r1 = (com.vungle.warren.AdLoader.Operation) r1     // Catch:{ all -> 0x0086 }
            if (r1 == 0) goto L_0x0048
            java.util.Set<com.vungle.warren.LoadAdCallback> r2 = r6.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            java.util.Set<com.vungle.warren.LoadAdCallback> r1 = r1.loadAdCallbacks     // Catch:{ all -> 0x0086 }
            r2.addAll(r1)     // Catch:{ all -> 0x0086 }
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r1 = r5.loadOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r2 = r6.id     // Catch:{ all -> 0x0086 }
            r1.put(r2, r6)     // Catch:{ all -> 0x0086 }
            java.lang.String r6 = r6.id     // Catch:{ all -> 0x0086 }
            r5.setLoading(r6, r0)     // Catch:{ all -> 0x0086 }
            goto L_0x0084
        L_0x0048:
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r0 = r5.loadOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r1 = r6.id     // Catch:{ all -> 0x0086 }
            r0.put(r1, r6)     // Catch:{ all -> 0x0086 }
            java.lang.String r0 = r6.id     // Catch:{ all -> 0x0086 }
            com.vungle.warren.DownloadCallbackWrapper r1 = new com.vungle.warren.DownloadCallbackWrapper     // Catch:{ all -> 0x0086 }
            com.vungle.warren.utility.SDKExecutors r2 = r5.sdkExecutors     // Catch:{ all -> 0x0086 }
            java.util.concurrent.ExecutorService r2 = r2.getVungleExecutor()     // Catch:{ all -> 0x0086 }
            com.vungle.warren.AdLoader$DownloadAdCallback r3 = new com.vungle.warren.AdLoader$DownloadAdCallback     // Catch:{ all -> 0x0086 }
            r4 = 0
            r3.<init>()     // Catch:{ all -> 0x0086 }
            r1.<init>(r2, r3)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.PublisherDirectDownload r6 = r6.directDownload     // Catch:{ all -> 0x0086 }
            r5.loadAd(r0, r1, r6)     // Catch:{ all -> 0x0086 }
            goto L_0x0084
        L_0x0068:
            java.util.Map<java.lang.String, com.vungle.warren.AdLoader$Operation> r1 = r5.pendingOperations     // Catch:{ all -> 0x0086 }
            java.lang.String r2 = r6.id     // Catch:{ all -> 0x0086 }
            r1.put(r2, r6)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobRunner r1 = r5.jobRunner     // Catch:{ all -> 0x0086 }
            java.lang.String r2 = r6.id     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobInfo r2 = com.vungle.warren.tasks.DownloadJob.makeJobInfo(r2)     // Catch:{ all -> 0x0086 }
            long r3 = r6.delay     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobInfo r6 = r2.setDelay(r3)     // Catch:{ all -> 0x0086 }
            com.vungle.warren.tasks.JobInfo r6 = r6.setUpdateCurrent(r0)     // Catch:{ all -> 0x0086 }
            r1.execute(r6)     // Catch:{ all -> 0x0086 }
        L_0x0084:
            monitor-exit(r5)
            return
        L_0x0086:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.load(com.vungle.warren.AdLoader$Operation):void");
    }

    private void onError(@Nullable Operation operation, @VungleException.ExceptionCode int i) {
        if (operation != null) {
            for (LoadAdCallback next : operation.loadAdCallbacks) {
                if (next != null) {
                    next.onError(operation.id, new VungleException(i));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public Throwable reposeCodeToVungleException(int i) {
        if (recoverableServerCode(i)) {
            return new VungleException(22);
        }
        return new VungleException(21);
    }

    /* access modifiers changed from: private */
    public Throwable retrofitToVungleException(Throwable th) {
        if (th instanceof IllegalArgumentException) {
            return new VungleError(10);
        }
        return (!(th instanceof UnknownHostException) && (th instanceof IOException)) ? new VungleException(20) : th;
    }

    private void loadAd(@NonNull final String str, @NonNull final DownloadCallbackWrapper downloadCallbackWrapper, final PublisherDirectDownload publisherDirectDownload) {
        this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
            public void run() {
                if (!AdLoader.this.vungleApi.isInitialized()) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(9), str, null);
                    return;
                }
                Placement placement = (Placement) AdLoader.this.repository.load(str, Placement.class).get();
                if (placement == null) {
                    DownloadCallbackWrapper downloadCallbackWrapper = downloadCallbackWrapper;
                    downloadCallbackWrapper.onDownloadFailed(new IllegalArgumentException("Placement ID " + str + " is not valid. Please check your configuration on the vungle dashboard."), str, null);
                    return;
                }
                Advertisement advertisement = AdLoader.this.repository.findValidAdvertisementForPlacement(placement.getId()).get();
                if (AdLoader.this.canPlayAd(advertisement)) {
                    downloadCallbackWrapper.onReady(str, placement, advertisement);
                } else if (AdLoader.this.canReDownload(advertisement)) {
                    Log.d(AdLoader.TAG, "Found valid adv but not ready - downloading content");
                    if (AdLoader.this.cacheManager.getBytesAvailable() < AdLoader.this.runtimeValues.settings.getMinimumSpaceForAd()) {
                        if (advertisement.getState() != 4) {
                            try {
                                AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                            } catch (DatabaseHelper.DBException unused) {
                                downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, null);
                                return;
                            }
                        }
                        downloadCallbackWrapper.onDownloadFailed(new VungleException(19), str, null);
                        return;
                    }
                    AdLoader.this.setLoading(str, true);
                    if (advertisement.getState() != 0) {
                        try {
                            AdLoader.this.repository.saveAndApplyState(advertisement, str, 0);
                        } catch (DatabaseHelper.DBException unused2) {
                            downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, null);
                            return;
                        }
                    }
                    AdLoader.this.downloadAdContent(str, advertisement, publisherDirectDownload, downloadCallbackWrapper);
                } else if (placement.getWakeupTime() > System.currentTimeMillis()) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(1), str, null);
                    String access$400 = AdLoader.TAG;
                    Log.w(access$400, "Placement " + placement.getId() + " is  snoozed");
                    if (placement.isAutoCached()) {
                        String access$4002 = AdLoader.TAG;
                        Log.d(access$4002, "Placement " + placement.getId() + " is sleeping rescheduling it ");
                        AdLoader.this.loadEndless(placement.getId(), placement.getWakeupTime() - System.currentTimeMillis(), publisherDirectDownload);
                    }
                } else {
                    String access$4003 = AdLoader.TAG;
                    Log.i(access$4003, "didn't find cached adv for " + str + " downloading ");
                    if (advertisement != null) {
                        try {
                            AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                        } catch (DatabaseHelper.DBException unused3) {
                            downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, null);
                            return;
                        }
                    }
                    if (AdLoader.this.runtimeValues.settings == null || AdLoader.this.cacheManager.getBytesAvailable() >= AdLoader.this.runtimeValues.settings.getMinimumSpaceForAd()) {
                        String access$4004 = AdLoader.TAG;
                        Log.d(access$4004, "No adv for placement " + placement.getId() + " getting new data ");
                        AdLoader.this.setLoading(str, true);
                        AdLoader.this.fetchAdMetadata(str, publisherDirectDownload, downloadCallbackWrapper, AdLoader.this.runtimeValues.headerBiddingCallback);
                        return;
                    }
                    downloadCallbackWrapper.onDownloadFailed(new VungleException((placement == null || !placement.isAutoCached()) ? 17 : 18), str, null);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void fetchAdMetadata(String str, PublisherDirectDownload publisherDirectDownload, @NonNull DownloadCallback downloadCallback, @Nullable HeaderBiddingCallback headerBiddingCallback) {
        final String str2 = str;
        final DownloadCallback downloadCallback2 = downloadCallback;
        final HeaderBiddingCallback headerBiddingCallback2 = headerBiddingCallback;
        final PublisherDirectDownload publisherDirectDownload2 = publisherDirectDownload;
        this.vungleApiClient.requestAd(str, headerBiddingCallback != null).enqueue(new Callback<JsonObject>() {
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull final Response<JsonObject> response) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        int i;
                        if (!response.isSuccessful()) {
                            long retryAfterHeaderValue = AdLoader.this.vungleApiClient.getRetryAfterHeaderValue(response);
                            Placement placement = (Placement) AdLoader.this.repository.load(str2, Placement.class).get();
                            if (retryAfterHeaderValue <= 0 || placement == null || !placement.isAutoCached()) {
                                Log.e(AdLoader.TAG, "Failed to retrieve advertisement information");
                                downloadCallback2.onDownloadFailed(AdLoader.this.reposeCodeToVungleException(response.code()), str2, null);
                                return;
                            }
                            AdLoader.this.loadEndless(str2, retryAfterHeaderValue);
                            downloadCallback2.onDownloadFailed(new VungleException(14), str2, null);
                            return;
                        }
                        Placement placement2 = (Placement) AdLoader.this.repository.load(str2, Placement.class).get();
                        if (placement2 == null) {
                            Log.e(AdLoader.TAG, "Placement metadata not found for requested advertisement.");
                            downloadCallback2.onDownloadFailed(new VungleException(2), str2, null);
                            return;
                        }
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject == null || !jsonObject.has(CampaignUnit.JSON_KEY_ADS) || jsonObject.get(CampaignUnit.JSON_KEY_ADS).isJsonNull()) {
                            downloadCallback2.onDownloadFailed(new VungleError(0), str2, null);
                            return;
                        }
                        JsonArray asJsonArray = jsonObject.getAsJsonArray(CampaignUnit.JSON_KEY_ADS);
                        if (asJsonArray == null || asJsonArray.size() == 0) {
                            downloadCallback2.onDownloadFailed(new VungleException(1), str2, null);
                            return;
                        }
                        JsonObject asJsonObject = asJsonArray.get(0).getAsJsonObject();
                        try {
                            Advertisement advertisement = new Advertisement(asJsonObject);
                            if (headerBiddingCallback2 != null) {
                                headerBiddingCallback2.onBidTokenAvailable(str2, advertisement.getBidToken());
                            }
                            AdLoader.this.repository.deleteAdvertisement(advertisement.getId());
                            Set<Map.Entry<String, String>> entrySet = advertisement.getDownloadableUrls().entrySet();
                            File file = AdLoader.this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get();
                            for (Map.Entry next : entrySet) {
                                if (!URLUtil.isHttpsUrl((String) next.getValue())) {
                                    if (!URLUtil.isHttpUrl((String) next.getValue())) {
                                        downloadCallback2.onDownloadFailed(new VungleError(10), str2, advertisement.getId());
                                        return;
                                    }
                                }
                                String str = file.getPath() + File.separator + ((String) next.getKey());
                                String str2 = (String) next.getValue();
                                if (!str.endsWith(Advertisement.KEY_POSTROLL)) {
                                    if (!str.endsWith("template")) {
                                        i = 2;
                                        AdAsset adAsset = new AdAsset(advertisement.getId(), str2, str);
                                        adAsset.status = 0;
                                        adAsset.fileType = i;
                                        AdLoader.this.repository.save(adAsset);
                                    }
                                }
                                i = 0;
                                AdAsset adAsset2 = new AdAsset(advertisement.getId(), str2, str);
                                adAsset2.status = 0;
                                adAsset2.fileType = i;
                                AdLoader.this.repository.save(adAsset2);
                            }
                            AdLoader.this.repository.saveAndApplyState(advertisement, str2, 0);
                            AdLoader.this.downloadAdContent(str2, advertisement, publisherDirectDownload2, downloadCallback2);
                        } catch (IllegalArgumentException unused) {
                            JsonObject asJsonObject2 = asJsonObject.getAsJsonObject("ad_markup");
                            if (asJsonObject2.has("sleep")) {
                                int asInt = asJsonObject2.get("sleep").getAsInt();
                                placement2.snooze((long) asInt);
                                try {
                                    AdLoader.this.repository.save(placement2);
                                    if (placement2.isAutoCached()) {
                                        AdLoader.this.loadEndless(str2, (long) (asInt * 1000));
                                    }
                                } catch (DatabaseHelper.DBException unused2) {
                                    downloadCallback2.onDownloadFailed(new VungleException(26), str2, null);
                                    return;
                                }
                            }
                            downloadCallback2.onDownloadFailed(new VungleException(1), str2, null);
                        } catch (DatabaseHelper.DBException unused3) {
                            downloadCallback2.onDownloadFailed(new VungleException(26), str2, null);
                        }
                    }
                });
            }

            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable th) {
                downloadCallback2.onDownloadFailed(AdLoader.this.retrofitToVungleException(th), str2, null);
            }
        });
    }

    /* access modifiers changed from: private */
    public void downloadAdContent(final String str, final Advertisement advertisement, PublisherDirectDownload publisherDirectDownload, @NonNull final DownloadCallback downloadCallback) {
        DirectDownloadStrategy directDownloadStrategy = (publisherDirectDownload == null || TextUtils.isEmpty(advertisement.getAdMarketId())) ? null : new DirectDownloadStrategy(publisherDirectDownload);
        if (directDownloadStrategy != null) {
            directDownloadStrategy.isApplicationAvailable(advertisement.getAdMarketId(), new DownloadStrategy.VerificationCallback() {
                public void onResult(final boolean z) {
                    AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                        public void run() {
                            if (z) {
                                Log.d(AdLoader.TAG, "fetchAdMetadata: downloading assets ");
                                AdLoader.this.downloadAdAssets(advertisement, downloadCallback, str);
                                return;
                            }
                            downloadCallback.onDownloadFailed(new VungleException(5), str, null);
                        }
                    });
                }
            });
        } else {
            downloadAdAssets(advertisement, downloadCallback, str);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void downloadAdAssets(com.vungle.warren.model.Advertisement r12, com.vungle.warren.AdLoader.DownloadCallback r13, java.lang.String r14) {
        /*
            r11 = this;
            java.util.Map r0 = r12.getDownloadableUrls()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x000c:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0062
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getKey()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x003c
            java.lang.Object r2 = r1.getValue()
            java.lang.CharSequence r2 = (java.lang.CharSequence) r2
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x003c
            java.lang.Object r1 = r1.getValue()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = android.webkit.URLUtil.isValidUrl(r1)
            if (r1 != 0) goto L_0x000c
        L_0x003c:
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r1 = 11
            r0.<init>(r1)
            r1 = 0
            r13.onDownloadFailed(r0, r14, r1)
            java.lang.String r13 = com.vungle.warren.AdLoader.TAG
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r0 = "Aborting, Failed to download Ad assets for: "
            r14.append(r0)
            java.lang.String r12 = r12.getId()
            r14.append(r12)
            java.lang.String r12 = r14.toString()
            android.util.Log.e(r13, r12)
            return
        L_0x0062:
            com.vungle.warren.DownloadCallbackWrapper r0 = new com.vungle.warren.DownloadCallbackWrapper
            com.vungle.warren.utility.SDKExecutors r1 = r11.sdkExecutors
            java.util.concurrent.ExecutorService r1 = r1.getUIExecutor()
            r0.<init>(r1, r13)
            java.util.concurrent.atomic.AtomicInteger r13 = new java.util.concurrent.atomic.AtomicInteger
            r13.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.vungle.warren.downloader.AssetDownloadListener r2 = r11.getAssetDownloadListener(r12, r14, r0, r13)
            com.vungle.warren.persistence.Repository r3 = r11.repository
            java.lang.String r4 = r12.getId()
            com.vungle.warren.persistence.Repository$FutureResult r3 = r3.loadAllAdAssets(r4)
            java.lang.Object r3 = r3.get()
            java.util.List r3 = (java.util.List) r3
            java.util.Iterator r3 = r3.iterator()
        L_0x008f:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0128
            java.lang.Object r4 = r3.next()
            com.vungle.warren.model.AdAsset r4 = (com.vungle.warren.model.AdAsset) r4
            int r5 = r4.status
            r6 = 3
            r7 = 24
            r8 = 1
            if (r5 != r6) goto L_0x00c2
            java.io.File r5 = new java.io.File
            java.lang.String r6 = r4.localPath
            r5.<init>(r6)
            boolean r5 = r11.fileIsValid(r5, r4)
            if (r5 == 0) goto L_0x00b1
            goto L_0x008f
        L_0x00b1:
            int r5 = r4.fileType
            if (r5 != r8) goto L_0x00c2
            com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException
            r13.<init>(r7)
            java.lang.String r12 = r12.getId()
            r0.onDownloadFailed(r13, r14, r12)
            return
        L_0x00c2:
            int r5 = r4.status
            r6 = 4
            if (r5 != r6) goto L_0x00cc
            int r5 = r4.fileType
            if (r5 != 0) goto L_0x00cc
            goto L_0x008f
        L_0x00cc:
            java.lang.String r5 = r4.serverPath
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 == 0) goto L_0x00e1
            com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException
            r13.<init>(r7)
            java.lang.String r12 = r12.getId()
            r0.onDownloadFailed(r13, r14, r12)
            return
        L_0x00e1:
            com.vungle.warren.downloader.DownloadRequest r5 = new com.vungle.warren.downloader.DownloadRequest
            java.lang.String r6 = r4.serverPath
            java.lang.String r7 = r4.localPath
            java.lang.String r9 = r4.identifier
            r5.<init>(r6, r7, r9)
            int r6 = r4.status
            if (r6 != r8) goto L_0x00f7
            com.vungle.warren.downloader.AssetDownloader r6 = r11.downloader
            r9 = 1000(0x3e8, double:4.94E-321)
            r6.cancelAndAwait(r5, r9)
        L_0x00f7:
            java.lang.String r6 = com.vungle.warren.AdLoader.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r9 = "Starting download for "
            r7.append(r9)
            r7.append(r4)
            java.lang.String r7 = r7.toString()
            android.util.Log.d(r6, r7)
            r4.status = r8
            com.vungle.warren.persistence.Repository r6 = r11.repository     // Catch:{ DBException -> 0x0119 }
            r6.save(r4)     // Catch:{ DBException -> 0x0119 }
            r1.add(r5)
            goto L_0x008f
        L_0x0119:
            com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException
            r1 = 26
            r13.<init>(r1)
            java.lang.String r12 = r12.getId()
            r0.onDownloadFailed(r13, r14, r12)
            return
        L_0x0128:
            int r3 = r1.size()
            if (r3 != 0) goto L_0x0134
            java.util.List r13 = java.util.Collections.EMPTY_LIST
            r11.onAssetDownloadFinished(r14, r0, r12, r13)
            return
        L_0x0134:
            int r12 = r1.size()
            r13.set(r12)
            java.util.Iterator r12 = r1.iterator()
        L_0x013f:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x0151
            java.lang.Object r13 = r12.next()
            com.vungle.warren.downloader.DownloadRequest r13 = (com.vungle.warren.downloader.DownloadRequest) r13
            com.vungle.warren.downloader.AssetDownloader r14 = r11.downloader
            r14.download(r13, r2)
            goto L_0x013f
        L_0x0151:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.downloadAdAssets(com.vungle.warren.model.Advertisement, com.vungle.warren.AdLoader$DownloadCallback, java.lang.String):void");
    }

    @NonNull
    private AssetDownloadListener getAssetDownloadListener(Advertisement advertisement, String str, DownloadCallback downloadCallback, AtomicInteger atomicInteger) {
        final AtomicInteger atomicInteger2 = atomicInteger;
        final String str2 = str;
        final DownloadCallback downloadCallback2 = downloadCallback;
        final Advertisement advertisement2 = advertisement;
        return new AssetDownloadListener() {
            List<AssetDownloadListener.DownloadError> errors = Collections.synchronizedList(new ArrayList());

            public void onProgress(@NonNull AssetDownloadListener.Progress progress, @NonNull DownloadRequest downloadRequest) {
            }

            public void onError(@NonNull final AssetDownloadListener.DownloadError downloadError, @Nullable final DownloadRequest downloadRequest) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        Log.e(AdLoader.TAG, "Download Failed");
                        if (downloadRequest != null) {
                            String str = downloadRequest.cookieString;
                            AdAsset adAsset = TextUtils.isEmpty(str) ? null : (AdAsset) AdLoader.this.repository.load(str, AdAsset.class).get();
                            if (adAsset != null) {
                                AnonymousClass4.this.errors.add(downloadError);
                                adAsset.status = 2;
                                try {
                                    AdLoader.this.repository.save(adAsset);
                                } catch (DatabaseHelper.DBException unused) {
                                    AnonymousClass4.this.errors.add(new AssetDownloadListener.DownloadError(-1, new VungleException(26), 4));
                                }
                            } else {
                                AnonymousClass4.this.errors.add(new AssetDownloadListener.DownloadError(-1, new IOException("Downloaded file not found!"), 1));
                            }
                        } else {
                            AnonymousClass4.this.errors.add(new AssetDownloadListener.DownloadError(-1, new RuntimeException("error in request"), 4));
                        }
                        if (atomicInteger2.decrementAndGet() <= 0) {
                            AdLoader.this.onAssetDownloadFinished(str2, downloadCallback2, advertisement2, AnonymousClass4.this.errors);
                        }
                    }
                });
            }

            public void onSuccess(@NonNull final File file, @NonNull final DownloadRequest downloadRequest) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        AdAsset adAsset;
                        String str = downloadRequest.cookieString;
                        if (str == null) {
                            adAsset = null;
                        } else {
                            adAsset = (AdAsset) AdLoader.this.repository.load(str, AdAsset.class).get();
                        }
                        if (!file.exists()) {
                            AnonymousClass4.this.onError(new AssetDownloadListener.DownloadError(-1, new IOException("Downloaded file not found!"), 3), downloadRequest);
                        } else if (adAsset == null) {
                            AnonymousClass4.this.onError(new AssetDownloadListener.DownloadError(-1, new IOException("Downloaded file not found!"), 1), downloadRequest);
                        } else {
                            adAsset.fileType = AdLoader.this.isZip(file) ? 0 : 2;
                            adAsset.fileSize = file.length();
                            adAsset.status = 3;
                            try {
                                AdLoader.this.repository.save(adAsset);
                                if (atomicInteger2.decrementAndGet() <= 0) {
                                    AdLoader.this.onAssetDownloadFinished(str2, downloadCallback2, advertisement2, AnonymousClass4.this.errors);
                                }
                            } catch (DatabaseHelper.DBException unused) {
                                AnonymousClass4.this.onError(new AssetDownloadListener.DownloadError(-1, new VungleException(26), 4), downloadRequest);
                            }
                        }
                    }
                });
            }
        };
    }

    /* access modifiers changed from: private */
    public boolean isZip(File file) {
        return file.getName().equals(Advertisement.KEY_POSTROLL) || file.getName().equals("template");
    }

    /* access modifiers changed from: private */
    public void onAssetDownloadFinished(@NonNull String str, @NonNull DownloadCallback downloadCallback, @NonNull Advertisement advertisement, @NonNull List<AssetDownloadListener.DownloadError> list) {
        if (list.isEmpty()) {
            List<AdAsset> list2 = this.repository.loadAllAdAssets(advertisement.getId()).get();
            if (list2.size() == 0) {
                downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
            }
            for (AdAsset adAsset : list2) {
                if (adAsset.status == 3) {
                    File file = new File(adAsset.localPath);
                    if (!fileIsValid(file, adAsset)) {
                        downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                        return;
                    } else if (adAsset.fileType == 0) {
                        try {
                            unzipFile(advertisement, adAsset, file, list2);
                        } catch (DatabaseHelper.DBException | IOException unused) {
                            downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                            return;
                        }
                    }
                } else if (adAsset.fileType == 0 && adAsset.status != 4) {
                    downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                    return;
                }
            }
            if (advertisement.getAdType() == 1) {
                String str2 = TAG;
                Log.d(str2, "saving MRAID for " + advertisement.getId());
                advertisement.setMraidAssetDir(this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get());
                try {
                    this.repository.save(advertisement);
                } catch (DatabaseHelper.DBException unused2) {
                    downloadCallback.onDownloadFailed(new VungleException(26), str, advertisement.getId());
                    return;
                }
            }
            downloadCallback.onDownloadCompleted(str, advertisement.getId());
            return;
        }
        VungleException vungleException = null;
        Iterator<AssetDownloadListener.DownloadError> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AssetDownloadListener.DownloadError next = it.next();
            if (VungleException.getExceptionCode(next.cause) != 26) {
                if (recoverableServerCode(next.serverCode) && next.reason == 1) {
                    vungleException = new VungleException(23);
                } else if (next.reason == 0) {
                    vungleException = new VungleException(23);
                } else {
                    vungleException = new VungleException(24);
                }
                if (vungleException.getExceptionCode() == 24) {
                    break;
                }
            } else {
                vungleException = new VungleException(26);
                break;
            }
        }
        downloadCallback.onDownloadFailed(vungleException, str, advertisement.getId());
    }

    private class DownloadAdCallback implements DownloadCallback {
        private DownloadAdCallback() {
        }

        public void onDownloadCompleted(@NonNull String str, @NonNull String str2) {
            Advertisement advertisement;
            String access$400 = AdLoader.TAG;
            Log.d(access$400, "download completed " + str);
            if (TextUtils.isEmpty(str2)) {
                advertisement = null;
            } else {
                advertisement = (Advertisement) AdLoader.this.repository.load(str2, Advertisement.class).get();
            }
            Placement placement = (Placement) AdLoader.this.repository.load(str, Placement.class).get();
            if (advertisement == null || placement == null) {
                onDownloadFailed(new IllegalStateException("Didn't find adv"), str, str2);
                return;
            }
            try {
                AdLoader.this.repository.saveAndApplyState(advertisement, str, 1);
                onReady(str, placement, advertisement);
            } catch (DatabaseHelper.DBException unused) {
                onDownloadFailed(new VungleException(26), str, str2);
            }
        }

        public void onReady(@NonNull String str, @NonNull Placement placement, @NonNull Advertisement advertisement) {
            synchronized (AdLoader.this) {
                AdLoader.this.setLoading(str, false);
                HeaderBiddingCallback headerBiddingCallback = AdLoader.this.runtimeValues.headerBiddingCallback;
                if (headerBiddingCallback != null) {
                    headerBiddingCallback.adAvailableForBidToken(str, advertisement.getBidToken());
                }
                String access$400 = AdLoader.TAG;
                Log.i(access$400, "found already cached valid adv, calling onAdLoad " + str + " callback ");
                if (placement.isAutoCached() && AdLoader.this.runtimeValues.initCallback != null) {
                    AdLoader.this.runtimeValues.initCallback.onAutoCacheAdAvailable(str);
                }
                Operation operation = (Operation) AdLoader.this.loadOperations.remove(str);
                if (operation != null) {
                    for (LoadAdCallback next : operation.loadAdCallbacks) {
                        if (next != null) {
                            next.onAdLoad(str);
                        }
                    }
                }
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(4:10|11|12|13) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:52|53|54|55) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:63|64|65|66) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:71|72|73|74) */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x015b, code lost:
            return;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:54:0x00dc */
        /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x010f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x013d */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x00b9  */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x0049=Splitter:B:12:0x0049, B:54:0x00dc=Splitter:B:54:0x00dc, B:73:0x013d=Splitter:B:73:0x013d, B:65:0x010f=Splitter:B:65:0x010f} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onDownloadFailed(@android.support.annotation.NonNull java.lang.Throwable r13, java.lang.String r14, java.lang.String r15) {
            /*
                r12 = this;
                com.vungle.warren.AdLoader r0 = com.vungle.warren.AdLoader.this
                monitor-enter(r0)
                com.vungle.warren.AdLoader r1 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                java.util.Map r1 = r1.loadOperations     // Catch:{ all -> 0x015c }
                java.lang.Object r1 = r1.remove(r14)     // Catch:{ all -> 0x015c }
                com.vungle.warren.AdLoader$Operation r1 = (com.vungle.warren.AdLoader.Operation) r1     // Catch:{ all -> 0x015c }
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ all -> 0x015c }
                java.lang.Class<com.vungle.warren.model.Placement> r3 = com.vungle.warren.model.Placement.class
                com.vungle.warren.persistence.Repository$FutureResult r2 = r2.load(r14, r3)     // Catch:{ all -> 0x015c }
                java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x015c }
                com.vungle.warren.model.Placement r2 = (com.vungle.warren.model.Placement) r2     // Catch:{ all -> 0x015c }
                if (r15 != 0) goto L_0x0025
                r15 = 0
                goto L_0x0037
            L_0x0025:
                com.vungle.warren.AdLoader r3 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                com.vungle.warren.persistence.Repository r3 = r3.repository     // Catch:{ all -> 0x015c }
                java.lang.Class<com.vungle.warren.model.Advertisement> r4 = com.vungle.warren.model.Advertisement.class
                com.vungle.warren.persistence.Repository$FutureResult r15 = r3.load(r15, r4)     // Catch:{ all -> 0x015c }
                java.lang.Object r15 = r15.get()     // Catch:{ all -> 0x015c }
                com.vungle.warren.model.Advertisement r15 = (com.vungle.warren.model.Advertisement) r15     // Catch:{ all -> 0x015c }
            L_0x0037:
                r3 = 26
                r4 = 4
                r5 = 0
                if (r2 != 0) goto L_0x006b
                if (r15 == 0) goto L_0x004e
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x0049 }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ DBException -> 0x0049 }
                r2.saveAndApplyState(r15, r14, r4)     // Catch:{ DBException -> 0x0049 }
                goto L_0x004e
            L_0x0049:
                com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x015c }
                r13.<init>(r3)     // Catch:{ all -> 0x015c }
            L_0x004e:
                java.util.Set<com.vungle.warren.LoadAdCallback> r15 = r1.loadAdCallbacks     // Catch:{ all -> 0x015c }
                java.util.Iterator r15 = r15.iterator()     // Catch:{ all -> 0x015c }
            L_0x0054:
                boolean r1 = r15.hasNext()     // Catch:{ all -> 0x015c }
                if (r1 == 0) goto L_0x0064
                java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x015c }
                com.vungle.warren.LoadAdCallback r1 = (com.vungle.warren.LoadAdCallback) r1     // Catch:{ all -> 0x015c }
                r1.onError(r14, r13)     // Catch:{ all -> 0x015c }
                goto L_0x0054
            L_0x0064:
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                r13.setLoading(r14, r5)     // Catch:{ all -> 0x015c }
                monitor-exit(r0)     // Catch:{ all -> 0x015c }
                return
            L_0x006b:
                boolean r2 = r13 instanceof com.vungle.warren.error.VungleException     // Catch:{ all -> 0x015c }
                r6 = 1
                if (r2 == 0) goto L_0x0094
                r2 = r13
                com.vungle.warren.error.VungleException r2 = (com.vungle.warren.error.VungleException) r2     // Catch:{ all -> 0x015c }
                int r2 = r2.getExceptionCode()     // Catch:{ all -> 0x015c }
                if (r2 == r6) goto L_0x0092
                r7 = 14
                if (r2 == r7) goto L_0x0092
                r7 = 20
                if (r2 == r7) goto L_0x008f
                r7 = 25
                if (r2 == r7) goto L_0x0092
                switch(r2) {
                    case 22: goto L_0x008f;
                    case 23: goto L_0x0089;
                    default: goto L_0x0088;
                }     // Catch:{ all -> 0x015c }
            L_0x0088:
                goto L_0x0094
            L_0x0089:
                if (r15 == 0) goto L_0x0094
                r2 = 0
                r7 = 1
                r8 = 0
                goto L_0x0097
            L_0x008f:
                r2 = 0
                r7 = 1
                goto L_0x0096
            L_0x0092:
                r2 = 1
                goto L_0x0095
            L_0x0094:
                r2 = 0
            L_0x0095:
                r7 = 0
            L_0x0096:
                r8 = 4
            L_0x0097:
                java.lang.String r9 = "Vungle"
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x015c }
                r10.<init>()     // Catch:{ all -> 0x015c }
                java.lang.String r11 = "Failed to load ad/assets for "
                r10.append(r11)     // Catch:{ all -> 0x015c }
                r10.append(r14)     // Catch:{ all -> 0x015c }
                java.lang.String r11 = ". Cause:"
                r10.append(r11)     // Catch:{ all -> 0x015c }
                java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x015c }
                android.util.Log.e(r9, r10, r13)     // Catch:{ all -> 0x015c }
                com.vungle.warren.AdLoader r9 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                r9.setLoading(r14, r5)     // Catch:{ all -> 0x015c }
                if (r1 == 0) goto L_0x015a
                int r9 = r1.policy     // Catch:{ all -> 0x015c }
                r10 = 2
                switch(r9) {
                    case 0: goto L_0x00fb;
                    case 1: goto L_0x00c2;
                    default: goto L_0x00c0;
                }     // Catch:{ all -> 0x015c }
            L_0x00c0:
                goto L_0x0131
            L_0x00c2:
                if (r2 != 0) goto L_0x0131
                int r13 = r1.retry     // Catch:{ all -> 0x015c }
                int r2 = r1.retryLimit     // Catch:{ all -> 0x015c }
                if (r13 >= r2) goto L_0x00cf
                if (r7 == 0) goto L_0x00cf
                int r5 = r13 + 1
                goto L_0x00d0
            L_0x00cf:
                r8 = 4
            L_0x00d0:
                if (r15 == 0) goto L_0x00e2
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x00dc }
                com.vungle.warren.persistence.Repository r13 = r13.repository     // Catch:{ DBException -> 0x00dc }
                r13.saveAndApplyState(r15, r14, r8)     // Catch:{ DBException -> 0x00dc }
                goto L_0x00e2
            L_0x00dc:
                com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x015c }
                r13.<init>(r3)     // Catch:{ all -> 0x015c }
                goto L_0x0131
            L_0x00e2:
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                long r14 = r1.retryDelay     // Catch:{ all -> 0x015c }
                com.vungle.warren.AdLoader$Operation r14 = r1.delay(r14)     // Catch:{ all -> 0x015c }
                long r1 = r1.retryDelay     // Catch:{ all -> 0x015c }
                long r1 = r1 * r10
                com.vungle.warren.AdLoader$Operation r14 = r14.retryDelay(r1)     // Catch:{ all -> 0x015c }
                com.vungle.warren.AdLoader$Operation r14 = r14.retry(r5)     // Catch:{ all -> 0x015c }
                r13.load(r14)     // Catch:{ all -> 0x015c }
                monitor-exit(r0)     // Catch:{ all -> 0x015c }
                return
            L_0x00fb:
                int r2 = r1.retry     // Catch:{ all -> 0x015c }
                int r5 = r1.retryLimit     // Catch:{ all -> 0x015c }
                if (r2 >= r5) goto L_0x0131
                if (r7 == 0) goto L_0x0131
                if (r15 == 0) goto L_0x0115
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x010f }
                com.vungle.warren.persistence.Repository r13 = r13.repository     // Catch:{ DBException -> 0x010f }
                r13.saveAndApplyState(r15, r14, r8)     // Catch:{ DBException -> 0x010f }
                goto L_0x0115
            L_0x010f:
                com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x015c }
                r13.<init>(r3)     // Catch:{ all -> 0x015c }
                goto L_0x0131
            L_0x0115:
                com.vungle.warren.AdLoader r13 = com.vungle.warren.AdLoader.this     // Catch:{ all -> 0x015c }
                long r14 = r1.retryDelay     // Catch:{ all -> 0x015c }
                com.vungle.warren.AdLoader$Operation r14 = r1.delay(r14)     // Catch:{ all -> 0x015c }
                long r2 = r1.retryDelay     // Catch:{ all -> 0x015c }
                long r2 = r2 * r10
                com.vungle.warren.AdLoader$Operation r14 = r14.retryDelay(r2)     // Catch:{ all -> 0x015c }
                int r15 = r1.retry     // Catch:{ all -> 0x015c }
                int r15 = r15 + r6
                com.vungle.warren.AdLoader$Operation r14 = r14.retry(r15)     // Catch:{ all -> 0x015c }
                r13.load(r14)     // Catch:{ all -> 0x015c }
                monitor-exit(r0)     // Catch:{ all -> 0x015c }
                return
            L_0x0131:
                if (r15 == 0) goto L_0x0142
                com.vungle.warren.AdLoader r2 = com.vungle.warren.AdLoader.this     // Catch:{ DBException -> 0x013d }
                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ DBException -> 0x013d }
                r2.saveAndApplyState(r15, r14, r4)     // Catch:{ DBException -> 0x013d }
                goto L_0x0142
            L_0x013d:
                com.vungle.warren.error.VungleException r13 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x015c }
                r13.<init>(r3)     // Catch:{ all -> 0x015c }
            L_0x0142:
                java.util.Set<com.vungle.warren.LoadAdCallback> r15 = r1.loadAdCallbacks     // Catch:{ all -> 0x015c }
                java.util.Iterator r15 = r15.iterator()     // Catch:{ all -> 0x015c }
            L_0x0148:
                boolean r1 = r15.hasNext()     // Catch:{ all -> 0x015c }
                if (r1 == 0) goto L_0x015a
                java.lang.Object r1 = r15.next()     // Catch:{ all -> 0x015c }
                com.vungle.warren.LoadAdCallback r1 = (com.vungle.warren.LoadAdCallback) r1     // Catch:{ all -> 0x015c }
                if (r1 == 0) goto L_0x0148
                r1.onError(r14, r13)     // Catch:{ all -> 0x015c }
                goto L_0x0148
            L_0x015a:
                monitor-exit(r0)     // Catch:{ all -> 0x015c }
                return
            L_0x015c:
                r13 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x015c }
                throw r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.AdLoader.DownloadAdCallback.onDownloadFailed(java.lang.Throwable, java.lang.String, java.lang.String):void");
        }
    }

    public void load(String str, LoadAdCallback loadAdCallback, PublisherDirectDownload publisherDirectDownload) {
        load(new Operation(str, 0, 2000, 5, 0, publisherDirectDownload, 0, false, loadAdCallback));
    }

    public void loadEndless(String str, long j, PublisherDirectDownload publisherDirectDownload) {
        load(new Operation(str, j, 2000, 5, 1, publisherDirectDownload, 0, true, new LoadAdCallback[0]));
    }

    public void loadEndless(String str, long j) {
        loadEndless(str, j, this.runtimeValues.publisherDirectDownload);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    private void unzipFile(Advertisement advertisement, AdAsset adAsset, @NonNull final File file, List<AdAsset> list) throws IOException, DatabaseHelper.DBException {
        File file2 = this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get();
        final ArrayList arrayList = new ArrayList();
        for (AdAsset next : list) {
            if (next.fileType == 2) {
                arrayList.add(next.localPath);
            }
        }
        List<File> unzip = UnzipUtility.unzip(file.getPath(), file2.getPath(), new UnzipUtility.Filter() {
            public boolean matches(String str) {
                File file = new File(str);
                for (String file2 : arrayList) {
                    File file3 = new File(file2);
                    if (file3.equals(file)) {
                        return false;
                    }
                    String path = file.getPath();
                    if (path.startsWith(file3.getPath() + File.separator)) {
                        return false;
                    }
                }
                return true;
            }
        });
        if (file.getName().equals("template")) {
            File file3 = new File(file2.getPath() + File.separator + "mraid.js");
            if (file3.exists()) {
                PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file3, true)));
                printWriter.println(HackMraid.VALUE);
                printWriter.close();
            }
        }
        for (File next2 : unzip) {
            AdAsset adAsset2 = new AdAsset(advertisement.getId(), null, next2.getPath());
            adAsset2.fileSize = next2.length();
            adAsset2.fileType = 1;
            adAsset2.parentId = adAsset.identifier;
            adAsset2.status = 3;
            this.repository.save(adAsset2);
        }
        String str = TAG;
        Log.d(str, "Uzipped " + file2);
        FileUtility.printDirectoryTree(file2);
        adAsset.status = 4;
        this.repository.save(adAsset, new Repository.SaveCallback() {
            public void onError(Exception exc) {
            }

            public void onSaved() {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        try {
                            FileUtility.delete(file);
                        } catch (IOException e) {
                            Log.e(AdLoader.TAG, "Error on deleting zip assets archive", e);
                        }
                    }
                });
            }
        });
    }

    public boolean hasAssetsFor(String str) throws IllegalStateException {
        List<AdAsset> list = this.repository.loadAllAdAssets(str).get();
        if (list == null || list.size() == 0) {
            return false;
        }
        for (AdAsset adAsset : list) {
            if (adAsset.fileType == 0) {
                if (adAsset.status != 4) {
                    return false;
                }
            } else if (adAsset.status != 3 || !fileIsValid(new File(adAsset.localPath), adAsset)) {
                return false;
            }
        }
        return true;
    }

    private boolean fileIsValid(File file, AdAsset adAsset) {
        return file.exists() && file.length() == adAsset.fileSize;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Collection<Operation> getPendingOperations() {
        return this.pendingOperations.values();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Collection<Operation> getRunningOperations() {
        return this.loadOperations.values();
    }
}
