package com.vungle.warren.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vungle.warren.AdConfig;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import okhttp3.HttpUrl;

public class Advertisement {
    public static final int DONE = 3;
    public static final int ERROR = 4;
    public static final String FILE_SCHEME = "file://";
    public static final String KEY_POSTROLL = "postroll";
    public static final String KEY_TEMPLATE = "template";
    public static final String KEY_VIDEO = "video";
    public static final int LANDSCAPE = 1;
    public static final int NEW = 0;
    public static final int PORTRAIT = 0;
    public static final int READY = 1;
    public static final int ROTATE = 2;
    public static final String START_MUTED = "START_MUTED";
    private static final String TAG = "Advertisement";
    public static final int TYPE_VUNGLE_LOCAL = 0;
    public static final int TYPE_VUNGLE_MRAID = 1;
    public static final int VIEWING = 2;
    AdConfig adConfig;
    String adMarketId;
    String adToken;
    @AdType
    int adType;
    String appID;
    String bidToken;
    Map<String, Pair<String, String>> cacheableAssets = new HashMap();
    String campaign;
    ArrayList<Checkpoint> checkpoints;
    String[] clickUrls;
    String[] closeUrls;
    int countdown;
    int ctaClickArea = -1;
    String ctaDestinationUrl;
    boolean ctaOverlayEnabled;
    String ctaUrl;
    int delay;
    boolean enableMoat;
    long expireTime;
    String identifier;
    String md5;
    String moatExtraVast;
    Map<String, String> mraidFiles = new HashMap();
    String[] muteUrls;
    String placementId;
    String[] postRollClickUrls;
    String[] postRollViewUrls;
    String postrollBundleUrl;
    boolean requiresNonMarketInstall;
    int retryCount;
    int showCloseDelay;
    int showCloseIncentivized;
    int state = 0;
    String templateId;
    Map<String, String> templateSettings;
    String templateType;
    String templateUrl;
    String[] unmuteUrls;
    String[] videoClickUrls;
    int videoHeight;
    String videoIdentifier;
    String videoUrl;
    int videoWidth;

    public @interface AdType {
    }

    public @interface CacheKey {
    }

    public @interface Orientation {
    }

    public @interface State {
    }

    Advertisement() {
    }

    public String getPlacementId() {
        return this.placementId;
    }

    public void setPlacementId(String str) {
        this.placementId = str;
    }

    public boolean isCtaOverlayEnabled() {
        return this.ctaOverlayEnabled;
    }

    public boolean getCtaClickArea() {
        return this.ctaClickArea > 0 || this.ctaClickArea == -1;
    }

    public boolean isRequiresNonMarketInstall() {
        return this.requiresNonMarketInstall;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x0776  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x022b  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x023a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Advertisement(@android.support.annotation.NonNull com.google.gson.JsonObject r14) throws java.lang.IllegalArgumentException {
        /*
            r13 = this;
            r13.<init>()
            r0 = -1
            r13.ctaClickArea = r0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            r13.mraidFiles = r1
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            r13.cacheableAssets = r1
            r1 = 0
            r13.state = r1
            java.lang.String r2 = "ad_markup"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x0786
            java.lang.String r2 = "ad_markup"
            com.google.gson.JsonObject r14 = r14.getAsJsonObject(r2)
            java.lang.String r2 = ""
            java.lang.String r3 = "adType"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x077e
            java.lang.String r3 = "adType"
            com.google.gson.JsonElement r3 = r14.get(r3)
            java.lang.String r3 = r3.getAsString()
            int r4 = r3.hashCode()
            r5 = -1852456483(0xffffffff9195c1dd, float:-2.3627532E-28)
            r6 = 1
            if (r4 == r5) goto L_0x0053
            r5 = -1851445271(0xffffffff91a52fe9, float:-2.6061937E-28)
            if (r4 == r5) goto L_0x0049
            goto L_0x005d
        L_0x0049:
            java.lang.String r4 = "vungle_mraid"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x005d
            r4 = 1
            goto L_0x005e
        L_0x0053:
            java.lang.String r4 = "vungle_local"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x005d
            r4 = 0
            goto L_0x005e
        L_0x005d:
            r4 = -1
        L_0x005e:
            r5 = 0
            switch(r4) {
                case 0: goto L_0x01e7;
                case 1: goto L_0x007e;
                default: goto L_0x0062;
            }
        L_0x0062:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unknown Ad Type "
            r0.append(r1)
            r0.append(r3)
            java.lang.String r1 = "! Please add this ad type"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r14.<init>(r0)
            throw r14
        L_0x007e:
            r13.adType = r6
            java.lang.String r3 = ""
            r13.postrollBundleUrl = r3
            java.lang.String r3 = "templateSettings"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x01df
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r13.templateSettings = r3
            java.lang.String r3 = "templateSettings"
            com.google.gson.JsonObject r3 = r14.getAsJsonObject(r3)
            java.lang.String r4 = "normal_replacements"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r3, r4)
            if (r4 == 0) goto L_0x00f1
            java.lang.String r4 = "normal_replacements"
            com.google.gson.JsonObject r4 = r3.getAsJsonObject(r4)
            java.util.Set r4 = r4.entrySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x00af:
            boolean r7 = r4.hasNext()
            if (r7 == 0) goto L_0x00f1
            java.lang.Object r7 = r4.next()
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7
            java.lang.Object r8 = r7.getKey()
            java.lang.CharSequence r8 = (java.lang.CharSequence) r8
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 == 0) goto L_0x00c8
            goto L_0x00af
        L_0x00c8:
            java.lang.Object r8 = r7.getValue()
            if (r8 == 0) goto L_0x00e6
            java.lang.Object r8 = r7.getValue()
            com.google.gson.JsonElement r8 = (com.google.gson.JsonElement) r8
            boolean r8 = r8.isJsonNull()
            if (r8 == 0) goto L_0x00db
            goto L_0x00e6
        L_0x00db:
            java.lang.Object r8 = r7.getValue()
            com.google.gson.JsonElement r8 = (com.google.gson.JsonElement) r8
            java.lang.String r8 = r8.getAsString()
            goto L_0x00e7
        L_0x00e6:
            r8 = r5
        L_0x00e7:
            java.util.Map<java.lang.String, java.lang.String> r9 = r13.templateSettings
            java.lang.Object r7 = r7.getKey()
            r9.put(r7, r8)
            goto L_0x00af
        L_0x00f1:
            java.lang.String r4 = "cacheable_replacements"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r3, r4)
            if (r4 == 0) goto L_0x018a
            java.lang.String r4 = "cacheable_replacements"
            com.google.gson.JsonObject r3 = r3.getAsJsonObject(r4)
            java.util.Set r3 = r3.entrySet()
            java.util.Iterator r3 = r3.iterator()
        L_0x0107:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x018a
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            java.lang.Object r7 = r4.getKey()
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x0120
            goto L_0x0107
        L_0x0120:
            java.lang.Object r7 = r4.getValue()
            if (r7 != 0) goto L_0x0127
            goto L_0x0107
        L_0x0127:
            java.lang.Object r7 = r4.getValue()
            com.google.gson.JsonElement r7 = (com.google.gson.JsonElement) r7
            java.lang.String r8 = "url"
            boolean r7 = com.vungle.warren.model.JsonUtil.hasNonNull(r7, r8)
            if (r7 == 0) goto L_0x0107
            java.lang.Object r7 = r4.getValue()
            com.google.gson.JsonElement r7 = (com.google.gson.JsonElement) r7
            java.lang.String r8 = "extension"
            boolean r7 = com.vungle.warren.model.JsonUtil.hasNonNull(r7, r8)
            if (r7 == 0) goto L_0x0107
            java.lang.Object r7 = r4.getValue()
            com.google.gson.JsonElement r7 = (com.google.gson.JsonElement) r7
            com.google.gson.JsonObject r7 = r7.getAsJsonObject()
            java.lang.String r8 = "url"
            com.google.gson.JsonElement r7 = r7.get(r8)
            java.lang.String r7 = r7.getAsString()
            java.lang.Object r8 = r4.getValue()
            com.google.gson.JsonElement r8 = (com.google.gson.JsonElement) r8
            com.google.gson.JsonObject r8 = r8.getAsJsonObject()
            java.lang.String r9 = "extension"
            com.google.gson.JsonElement r8 = r8.get(r9)
            java.lang.String r8 = r8.getAsString()
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.String>> r9 = r13.cacheableAssets
            java.lang.Object r10 = r4.getKey()
            android.util.Pair r11 = new android.util.Pair
            r11.<init>(r7, r8)
            r9.put(r10, r11)
            java.lang.Object r4 = r4.getKey()
            java.lang.String r4 = (java.lang.String) r4
            java.lang.String r8 = "MAIN_VIDEO"
            boolean r4 = r4.equalsIgnoreCase(r8)
            if (r4 == 0) goto L_0x0107
            r2 = r7
            goto L_0x0107
        L_0x018a:
            java.lang.String r3 = "templateId"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x01d7
            java.lang.String r3 = "templateId"
            com.google.gson.JsonElement r3 = r14.get(r3)
            java.lang.String r3 = r3.getAsString()
            r13.templateId = r3
            java.lang.String r3 = "template_type"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x01cf
            java.lang.String r3 = "template_type"
            com.google.gson.JsonElement r3 = r14.get(r3)
            java.lang.String r3 = r3.getAsString()
            r13.templateType = r3
            java.lang.String r3 = "templateURL"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x01c7
            java.lang.String r3 = "templateURL"
            com.google.gson.JsonElement r3 = r14.get(r3)
            java.lang.String r3 = r3.getAsString()
            r13.templateUrl = r3
            goto L_0x0225
        L_0x01c7:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Template URL missing!"
            r14.<init>(r0)
            throw r14
        L_0x01cf:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Template Type missing!"
            r14.<init>(r0)
            throw r14
        L_0x01d7:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing templateID!"
            r14.<init>(r0)
            throw r14
        L_0x01df:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing template adConfig!"
            r14.<init>(r0)
            throw r14
        L_0x01e7:
            r13.adType = r1
            java.lang.String r3 = "postBundle"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x01fc
            java.lang.String r3 = "postBundle"
            com.google.gson.JsonElement r3 = r14.get(r3)
            java.lang.String r3 = r3.getAsString()
            goto L_0x01fe
        L_0x01fc:
            java.lang.String r3 = ""
        L_0x01fe:
            r13.postrollBundleUrl = r3
            java.lang.String r3 = "url"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r3)
            if (r3 == 0) goto L_0x0212
            java.lang.String r2 = "url"
            com.google.gson.JsonElement r2 = r14.get(r2)
            java.lang.String r2 = r2.getAsString()
        L_0x0212:
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r13.templateSettings = r3
            java.lang.String r3 = ""
            r13.templateUrl = r3
            java.lang.String r3 = ""
            r13.templateId = r3
            java.lang.String r3 = ""
            r13.templateType = r3
        L_0x0225:
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L_0x022e
            r13.videoUrl = r2
            goto L_0x0232
        L_0x022e:
            java.lang.String r2 = ""
            r13.videoUrl = r2
        L_0x0232:
            java.lang.String r2 = "id"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x0776
            java.lang.String r2 = "id"
            com.google.gson.JsonElement r2 = r14.get(r2)
            java.lang.String r2 = r2.getAsString()
            r13.identifier = r2
            java.lang.String r2 = "campaign"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x076e
            java.lang.String r2 = "campaign"
            com.google.gson.JsonElement r2 = r14.get(r2)
            java.lang.String r2 = r2.getAsString()
            r13.campaign = r2
            java.lang.String r2 = "app_id"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x0766
            java.lang.String r2 = "app_id"
            com.google.gson.JsonElement r2 = r14.get(r2)
            java.lang.String r2 = r2.getAsString()
            r13.appID = r2
            java.lang.String r2 = "expiry"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            r3 = 1000(0x3e8, double:4.94E-321)
            if (r2 == 0) goto L_0x029f
            java.lang.String r2 = "expiry"
            com.google.gson.JsonElement r2 = r14.get(r2)
            boolean r2 = r2.isJsonNull()
            if (r2 != 0) goto L_0x029f
            java.lang.String r2 = "expiry"
            com.google.gson.JsonElement r2 = r14.get(r2)
            long r7 = r2.getAsLong()
            r9 = 0
            int r2 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r2 <= 0) goto L_0x0297
            r13.expireTime = r7
            goto L_0x02a6
        L_0x0297:
            long r7 = java.lang.System.currentTimeMillis()
            long r7 = r7 / r3
            r13.expireTime = r7
            goto L_0x02a6
        L_0x029f:
            long r7 = java.lang.System.currentTimeMillis()
            long r7 = r7 / r3
            r13.expireTime = r7
        L_0x02a6:
            java.lang.String r2 = "tpat"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x0575
            java.lang.String r2 = "tpat"
            com.google.gson.JsonObject r2 = r14.getAsJsonObject(r2)
            java.util.ArrayList r3 = new java.util.ArrayList
            r4 = 5
            r3.<init>(r4)
            r13.checkpoints = r3
            int r3 = r13.adType
            switch(r3) {
                case 0: goto L_0x02f8;
                case 1: goto L_0x02c9;
                default: goto L_0x02c1;
            }
        L_0x02c1:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unknown Ad Type!"
            r14.<init>(r0)
            throw r14
        L_0x02c9:
            r3 = 0
        L_0x02ca:
            if (r3 >= r4) goto L_0x032e
            int r7 = r3 * 25
            java.util.Locale r8 = java.util.Locale.ENGLISH
            java.lang.String r9 = "checkpoint.%d"
            java.lang.Object[] r10 = new java.lang.Object[r6]
            java.lang.Integer r11 = java.lang.Integer.valueOf(r7)
            r10[r1] = r11
            java.lang.String r8 = java.lang.String.format(r8, r9, r10)
            boolean r9 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r8)
            if (r9 == 0) goto L_0x02ef
            com.vungle.warren.model.Advertisement$Checkpoint r9 = new com.vungle.warren.model.Advertisement$Checkpoint
            com.google.gson.JsonArray r8 = r2.getAsJsonArray(r8)
            byte r7 = (byte) r7
            r9.<init>(r8, r7)
            goto L_0x02f0
        L_0x02ef:
            r9 = r5
        L_0x02f0:
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r7 = r13.checkpoints
            r7.add(r3, r9)
            int r3 = r3 + 1
            goto L_0x02ca
        L_0x02f8:
            java.lang.String r3 = "play_percentage"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r3 == 0) goto L_0x032e
            java.lang.String r3 = "play_percentage"
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r3)
            r4 = 0
        L_0x0307:
            int r7 = r3.size()
            if (r4 >= r7) goto L_0x0329
            com.google.gson.JsonElement r7 = r3.get(r4)
            if (r7 != 0) goto L_0x0314
            goto L_0x0326
        L_0x0314:
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r7 = r13.checkpoints
            com.vungle.warren.model.Advertisement$Checkpoint r8 = new com.vungle.warren.model.Advertisement$Checkpoint
            com.google.gson.JsonElement r9 = r3.get(r4)
            com.google.gson.JsonObject r9 = r9.getAsJsonObject()
            r8.<init>(r9)
            r7.add(r8)
        L_0x0326:
            int r4 = r4 + 1
            goto L_0x0307
        L_0x0329:
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r3 = r13.checkpoints
            java.util.Collections.sort(r3)
        L_0x032e:
            java.lang.String r3 = "clickUrl"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r3 == 0) goto L_0x0361
            java.lang.String r3 = "clickUrl"
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r3)
            int r4 = r3.size()
            java.lang.String[] r4 = new java.lang.String[r4]
            r13.clickUrls = r4
            java.util.Iterator r3 = r3.iterator()
            r4 = 0
        L_0x0349:
            boolean r7 = r3.hasNext()
            if (r7 == 0) goto L_0x0365
            java.lang.Object r7 = r3.next()
            com.google.gson.JsonElement r7 = (com.google.gson.JsonElement) r7
            java.lang.String[] r8 = r13.clickUrls
            int r9 = r4 + 1
            java.lang.String r7 = r7.getAsString()
            r8[r4] = r7
            r4 = r9
            goto L_0x0349
        L_0x0361:
            java.lang.String[] r3 = new java.lang.String[r1]
            r13.clickUrls = r3
        L_0x0365:
            java.lang.String r3 = "moat"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r3 == 0) goto L_0x038c
            java.lang.String r3 = "moat"
            com.google.gson.JsonObject r3 = r2.getAsJsonObject(r3)
            java.lang.String r4 = "is_enabled"
            com.google.gson.JsonElement r4 = r3.get(r4)
            boolean r4 = r4.getAsBoolean()
            r13.enableMoat = r4
            java.lang.String r4 = "extra_vast"
            com.google.gson.JsonElement r3 = r3.get(r4)
            java.lang.String r3 = r3.getAsString()
            r13.moatExtraVast = r3
            goto L_0x0392
        L_0x038c:
            r13.enableMoat = r1
            java.lang.String r3 = ""
            r13.moatExtraVast = r3
        L_0x0392:
            java.lang.String r3 = "video_click"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r3 == 0) goto L_0x03dc
            java.lang.String r3 = "video_click"
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r3)
            int r4 = r3.size()
            java.lang.String[] r4 = new java.lang.String[r4]
            r13.videoClickUrls = r4
            r4 = 0
        L_0x03a9:
            int r7 = r3.size()
            if (r4 >= r7) goto L_0x03e0
            com.google.gson.JsonElement r7 = r3.get(r4)
            if (r7 == 0) goto L_0x03d3
            java.lang.String r7 = "null"
            com.google.gson.JsonElement r8 = r3.get(r4)
            java.lang.String r8 = r8.toString()
            boolean r7 = r7.equalsIgnoreCase(r8)
            if (r7 == 0) goto L_0x03c6
            goto L_0x03d3
        L_0x03c6:
            java.lang.String[] r7 = r13.videoClickUrls
            com.google.gson.JsonElement r8 = r3.get(r4)
            java.lang.String r8 = r8.getAsString()
            r7[r4] = r8
            goto L_0x03d9
        L_0x03d3:
            java.lang.String[] r7 = r13.videoClickUrls
            java.lang.String r8 = ""
            r7[r4] = r8
        L_0x03d9:
            int r4 = r4 + 1
            goto L_0x03a9
        L_0x03dc:
            java.lang.String[] r3 = new java.lang.String[r1]
            r13.videoClickUrls = r3
        L_0x03e0:
            int r3 = r13.adType
            switch(r3) {
                case 0: goto L_0x03f8;
                case 1: goto L_0x03ed;
                default: goto L_0x03e5;
            }
        L_0x03e5:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Unknown AdType!"
            r14.<init>(r0)
            throw r14
        L_0x03ed:
            java.lang.String r3 = "video.mute"
            java.lang.String r4 = "video.unmute"
            java.lang.String r7 = "video.close"
            java.lang.String r8 = "postroll.click"
            java.lang.String r9 = "postroll.view"
            goto L_0x0402
        L_0x03f8:
            java.lang.String r3 = "mute"
            java.lang.String r4 = "unmute"
            java.lang.String r7 = "video_close"
            java.lang.String r8 = "postroll_click"
            java.lang.String r9 = "postroll_view"
        L_0x0402:
            boolean r10 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r10 == 0) goto L_0x0448
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r3)
            int r10 = r3.size()
            java.lang.String[] r10 = new java.lang.String[r10]
            r13.muteUrls = r10
            r10 = 0
        L_0x0415:
            int r11 = r3.size()
            if (r10 >= r11) goto L_0x044c
            com.google.gson.JsonElement r11 = r3.get(r10)
            if (r11 == 0) goto L_0x043f
            java.lang.String r11 = "null"
            com.google.gson.JsonElement r12 = r3.get(r10)
            java.lang.String r12 = r12.toString()
            boolean r11 = r11.equalsIgnoreCase(r12)
            if (r11 == 0) goto L_0x0432
            goto L_0x043f
        L_0x0432:
            java.lang.String[] r11 = r13.muteUrls
            com.google.gson.JsonElement r12 = r3.get(r10)
            java.lang.String r12 = r12.getAsString()
            r11[r10] = r12
            goto L_0x0445
        L_0x043f:
            java.lang.String[] r11 = r13.muteUrls
            java.lang.String r12 = ""
            r11[r10] = r12
        L_0x0445:
            int r10 = r10 + 1
            goto L_0x0415
        L_0x0448:
            java.lang.String[] r3 = new java.lang.String[r1]
            r13.muteUrls = r3
        L_0x044c:
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r4)
            if (r3 == 0) goto L_0x0492
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r4)
            int r4 = r3.size()
            java.lang.String[] r4 = new java.lang.String[r4]
            r13.unmuteUrls = r4
            r4 = 0
        L_0x045f:
            int r10 = r3.size()
            if (r4 >= r10) goto L_0x0496
            com.google.gson.JsonElement r10 = r3.get(r4)
            if (r10 == 0) goto L_0x0489
            java.lang.String r10 = "null"
            com.google.gson.JsonElement r11 = r3.get(r4)
            java.lang.String r11 = r11.toString()
            boolean r10 = r10.equalsIgnoreCase(r11)
            if (r10 == 0) goto L_0x047c
            goto L_0x0489
        L_0x047c:
            java.lang.String[] r10 = r13.unmuteUrls
            com.google.gson.JsonElement r11 = r3.get(r4)
            java.lang.String r11 = r11.getAsString()
            r10[r4] = r11
            goto L_0x048f
        L_0x0489:
            java.lang.String[] r10 = r13.unmuteUrls
            java.lang.String r11 = ""
            r10[r4] = r11
        L_0x048f:
            int r4 = r4 + 1
            goto L_0x045f
        L_0x0492:
            java.lang.String[] r3 = new java.lang.String[r1]
            r13.unmuteUrls = r3
        L_0x0496:
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r7)
            if (r3 == 0) goto L_0x04dc
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r7)
            int r4 = r3.size()
            java.lang.String[] r4 = new java.lang.String[r4]
            r13.closeUrls = r4
            r4 = 0
        L_0x04a9:
            int r7 = r3.size()
            if (r4 >= r7) goto L_0x04e0
            com.google.gson.JsonElement r7 = r3.get(r4)
            if (r7 == 0) goto L_0x04d3
            java.lang.String r7 = "null"
            com.google.gson.JsonElement r10 = r3.get(r4)
            java.lang.String r10 = r10.toString()
            boolean r7 = r7.equalsIgnoreCase(r10)
            if (r7 == 0) goto L_0x04c6
            goto L_0x04d3
        L_0x04c6:
            java.lang.String[] r7 = r13.closeUrls
            com.google.gson.JsonElement r10 = r3.get(r4)
            java.lang.String r10 = r10.getAsString()
            r7[r4] = r10
            goto L_0x04d9
        L_0x04d3:
            java.lang.String[] r7 = r13.closeUrls
            java.lang.String r10 = ""
            r7[r4] = r10
        L_0x04d9:
            int r4 = r4 + 1
            goto L_0x04a9
        L_0x04dc:
            java.lang.String[] r3 = new java.lang.String[r1]
            r13.closeUrls = r3
        L_0x04e0:
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r8)
            if (r3 == 0) goto L_0x0526
            com.google.gson.JsonArray r3 = r2.getAsJsonArray(r8)
            int r4 = r3.size()
            java.lang.String[] r4 = new java.lang.String[r4]
            r13.postRollClickUrls = r4
            r4 = 0
        L_0x04f3:
            int r7 = r3.size()
            if (r4 >= r7) goto L_0x052a
            com.google.gson.JsonElement r7 = r3.get(r4)
            if (r7 == 0) goto L_0x051d
            java.lang.String r7 = "null"
            com.google.gson.JsonElement r8 = r3.get(r4)
            java.lang.String r8 = r8.toString()
            boolean r7 = r7.equalsIgnoreCase(r8)
            if (r7 == 0) goto L_0x0510
            goto L_0x051d
        L_0x0510:
            java.lang.String[] r7 = r13.postRollClickUrls
            com.google.gson.JsonElement r8 = r3.get(r4)
            java.lang.String r8 = r8.getAsString()
            r7[r4] = r8
            goto L_0x0523
        L_0x051d:
            java.lang.String[] r7 = r13.postRollClickUrls
            java.lang.String r8 = ""
            r7[r4] = r8
        L_0x0523:
            int r4 = r4 + 1
            goto L_0x04f3
        L_0x0526:
            java.lang.String[] r3 = new java.lang.String[r1]
            r13.postRollClickUrls = r3
        L_0x052a:
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r9)
            if (r3 == 0) goto L_0x0570
            com.google.gson.JsonArray r2 = r2.getAsJsonArray(r9)
            int r3 = r2.size()
            java.lang.String[] r3 = new java.lang.String[r3]
            r13.postRollViewUrls = r3
            r3 = 0
        L_0x053d:
            int r4 = r2.size()
            if (r3 >= r4) goto L_0x059e
            com.google.gson.JsonElement r4 = r2.get(r3)
            if (r4 == 0) goto L_0x0567
            java.lang.String r4 = "null"
            com.google.gson.JsonElement r7 = r2.get(r3)
            java.lang.String r7 = r7.toString()
            boolean r4 = r4.equalsIgnoreCase(r7)
            if (r4 == 0) goto L_0x055a
            goto L_0x0567
        L_0x055a:
            java.lang.String[] r4 = r13.postRollViewUrls
            com.google.gson.JsonElement r7 = r2.get(r3)
            java.lang.String r7 = r7.getAsString()
            r4[r3] = r7
            goto L_0x056d
        L_0x0567:
            java.lang.String[] r4 = r13.postRollViewUrls
            java.lang.String r7 = ""
            r4[r3] = r7
        L_0x056d:
            int r3 = r3 + 1
            goto L_0x053d
        L_0x0570:
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.postRollViewUrls = r2
            goto L_0x059e
        L_0x0575:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r13.checkpoints = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.muteUrls = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.closeUrls = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.unmuteUrls = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.postRollViewUrls = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.postRollClickUrls = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.clickUrls = r2
            java.lang.String[] r2 = new java.lang.String[r1]
            r13.videoClickUrls = r2
            r13.enableMoat = r1
            java.lang.String r2 = ""
            r13.moatExtraVast = r2
        L_0x059e:
            java.lang.String r2 = "delay"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x05b3
            java.lang.String r2 = "delay"
            com.google.gson.JsonElement r2 = r14.get(r2)
            int r2 = r2.getAsInt()
            r13.delay = r2
            goto L_0x05b5
        L_0x05b3:
            r13.delay = r1
        L_0x05b5:
            java.lang.String r2 = "showClose"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x05ca
            java.lang.String r2 = "showClose"
            com.google.gson.JsonElement r2 = r14.get(r2)
            int r2 = r2.getAsInt()
            r13.showCloseDelay = r2
            goto L_0x05cc
        L_0x05ca:
            r13.showCloseDelay = r1
        L_0x05cc:
            java.lang.String r2 = "showCloseIncentivized"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x05e1
            java.lang.String r2 = "showCloseIncentivized"
            com.google.gson.JsonElement r2 = r14.get(r2)
            int r2 = r2.getAsInt()
            r13.showCloseIncentivized = r2
            goto L_0x05e3
        L_0x05e1:
            r13.showCloseIncentivized = r1
        L_0x05e3:
            java.lang.String r2 = "countdown"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x05f8
            java.lang.String r2 = "countdown"
            com.google.gson.JsonElement r2 = r14.get(r2)
            int r2 = r2.getAsInt()
            r13.countdown = r2
            goto L_0x05fa
        L_0x05f8:
            r13.countdown = r1
        L_0x05fa:
            java.lang.String r2 = "videoWidth"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x075e
            java.lang.String r2 = "videoWidth"
            com.google.gson.JsonElement r2 = r14.get(r2)
            int r2 = r2.getAsInt()
            r13.videoWidth = r2
            java.lang.String r2 = "videoHeight"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x0756
            java.lang.String r2 = "videoHeight"
            com.google.gson.JsonElement r2 = r14.get(r2)
            int r2 = r2.getAsInt()
            r13.videoHeight = r2
            java.lang.String r2 = "md5"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x0637
            java.lang.String r2 = "md5"
            com.google.gson.JsonElement r2 = r14.get(r2)
            java.lang.String r2 = r2.getAsString()
            r13.md5 = r2
            goto L_0x063b
        L_0x0637:
            java.lang.String r2 = ""
            r13.md5 = r2
        L_0x063b:
            java.lang.String r2 = "cta_overlay"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r2)
            if (r2 == 0) goto L_0x068b
            java.lang.String r2 = "cta_overlay"
            com.google.gson.JsonObject r2 = r14.getAsJsonObject(r2)
            java.lang.String r3 = "enabled"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r3 == 0) goto L_0x065e
            java.lang.String r3 = "enabled"
            com.google.gson.JsonElement r3 = r2.get(r3)
            boolean r3 = r3.getAsBoolean()
            r13.ctaOverlayEnabled = r3
            goto L_0x0660
        L_0x065e:
            r13.ctaOverlayEnabled = r1
        L_0x0660:
            java.lang.String r3 = "click_area"
            boolean r3 = com.vungle.warren.model.JsonUtil.hasNonNull(r2, r3)
            if (r3 == 0) goto L_0x0688
            java.lang.String r3 = "click_area"
            com.google.gson.JsonElement r3 = r2.get(r3)
            java.lang.String r3 = r3.getAsString()
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x067b
            r13.ctaClickArea = r0
            goto L_0x068f
        L_0x067b:
            java.lang.String r0 = "click_area"
            com.google.gson.JsonElement r0 = r2.get(r0)
            int r0 = r0.getAsInt()
            r13.ctaClickArea = r0
            goto L_0x068f
        L_0x0688:
            r13.ctaClickArea = r0
            goto L_0x068f
        L_0x068b:
            r13.ctaOverlayEnabled = r1
            r13.ctaClickArea = r0
        L_0x068f:
            java.lang.String r0 = "callToActionDest"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x06a2
            java.lang.String r0 = "callToActionDest"
            com.google.gson.JsonElement r0 = r14.get(r0)
            java.lang.String r0 = r0.getAsString()
            goto L_0x06a3
        L_0x06a2:
            r0 = r5
        L_0x06a3:
            r13.ctaDestinationUrl = r0
            java.lang.String r0 = "callToActionUrl"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x06b7
            java.lang.String r0 = "callToActionUrl"
            com.google.gson.JsonElement r0 = r14.get(r0)
            java.lang.String r5 = r0.getAsString()
        L_0x06b7:
            r13.ctaUrl = r5
            java.lang.String r0 = "retryCount"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x06ce
            java.lang.String r0 = "retryCount"
            com.google.gson.JsonElement r0 = r14.get(r0)
            int r0 = r0.getAsInt()
            r13.retryCount = r0
            goto L_0x06d0
        L_0x06ce:
            r13.retryCount = r6
        L_0x06d0:
            java.lang.String r0 = "ad_token"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x074e
            java.lang.String r0 = "ad_token"
            com.google.gson.JsonElement r0 = r14.get(r0)
            java.lang.String r0 = r0.getAsString()
            r13.adToken = r0
            java.lang.String r0 = "video_object_id"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x06f9
            java.lang.String r0 = "video_object_id"
            com.google.gson.JsonElement r0 = r14.get(r0)
            java.lang.String r0 = r0.getAsString()
            r13.videoIdentifier = r0
            goto L_0x06fd
        L_0x06f9:
            java.lang.String r0 = ""
            r13.videoIdentifier = r0
        L_0x06fd:
            java.lang.String r0 = "requires_sideloading"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x0712
            java.lang.String r0 = "requires_sideloading"
            com.google.gson.JsonElement r0 = r14.get(r0)
            boolean r0 = r0.getAsBoolean()
            r13.requiresNonMarketInstall = r0
            goto L_0x0714
        L_0x0712:
            r13.requiresNonMarketInstall = r1
        L_0x0714:
            java.lang.String r0 = "ad_market_id"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x0729
            java.lang.String r0 = "ad_market_id"
            com.google.gson.JsonElement r0 = r14.get(r0)
            java.lang.String r0 = r0.getAsString()
            r13.adMarketId = r0
            goto L_0x072d
        L_0x0729:
            java.lang.String r0 = ""
            r13.adMarketId = r0
        L_0x072d:
            java.lang.String r0 = "bid_token"
            boolean r0 = com.vungle.warren.model.JsonUtil.hasNonNull(r14, r0)
            if (r0 == 0) goto L_0x0742
            java.lang.String r0 = "bid_token"
            com.google.gson.JsonElement r14 = r14.get(r0)
            java.lang.String r14 = r14.getAsString()
            r13.bidToken = r14
            goto L_0x0746
        L_0x0742:
            java.lang.String r14 = ""
            r13.bidToken = r14
        L_0x0746:
            com.vungle.warren.AdConfig r14 = new com.vungle.warren.AdConfig
            r14.<init>()
            r13.adConfig = r14
            return
        L_0x074e:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "AdToken missing!"
            r14.<init>(r0)
            throw r14
        L_0x0756:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing video height!"
            r14.<init>(r0)
            throw r14
        L_0x075e:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing video width!"
            r14.<init>(r0)
            throw r14
        L_0x0766:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing app Id, cannot process advertisement!"
            r14.<init>(r0)
            throw r14
        L_0x076e:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing campaign information, cannot process advertisement!"
            r14.<init>(r0)
            throw r14
        L_0x0776:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Missing identifier, cannot process advertisement!"
            r14.<init>(r0)
            throw r14
        L_0x077e:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "Advertisement did not contain an adType!"
            r14.<init>(r0)
            throw r14
        L_0x0786:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r0 = "JSON does not contain ad markup!"
            r14.<init>(r0)
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.model.Advertisement.<init>(com.google.gson.JsonObject):void");
    }

    @AdType
    public int getAdType() {
        return this.adType;
    }

    public List<Checkpoint> getCheckpoints() {
        return this.checkpoints;
    }

    public void configure(AdConfig adConfig2) {
        if (adConfig2 == null) {
            this.adConfig = new AdConfig();
        } else {
            this.adConfig = adConfig2;
        }
    }

    public AdConfig getAdConfig() {
        return this.adConfig;
    }

    @Orientation
    public int getOrientation() {
        return this.videoWidth > this.videoHeight ? 1 : 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Advertisement)) {
            return false;
        }
        Advertisement advertisement = (Advertisement) obj;
        if (getId() == null || advertisement.getId() == null || !advertisement.getId().equals(getId()) || advertisement.adType != this.adType || advertisement.expireTime != this.expireTime || advertisement.delay != this.delay || advertisement.showCloseDelay != this.showCloseDelay || advertisement.showCloseIncentivized != this.showCloseIncentivized || advertisement.countdown != this.countdown || advertisement.videoWidth != this.videoWidth || advertisement.videoHeight != this.videoHeight || advertisement.ctaOverlayEnabled != this.ctaOverlayEnabled || advertisement.ctaClickArea != this.ctaClickArea || advertisement.retryCount != this.retryCount || advertisement.enableMoat != this.enableMoat || advertisement.requiresNonMarketInstall != this.requiresNonMarketInstall || !advertisement.identifier.equals(this.identifier) || !advertisement.campaign.equals(this.campaign) || !advertisement.videoUrl.equals(this.videoUrl) || !advertisement.md5.equals(this.md5) || !advertisement.postrollBundleUrl.equals(this.postrollBundleUrl) || !advertisement.ctaDestinationUrl.equals(this.ctaDestinationUrl) || !advertisement.ctaUrl.equals(this.ctaUrl) || !advertisement.adToken.equals(this.adToken) || !advertisement.videoIdentifier.equals(this.videoIdentifier) || !advertisement.moatExtraVast.equals(this.moatExtraVast) || advertisement.state != this.state || advertisement.muteUrls.length != this.muteUrls.length) {
            return false;
        }
        for (int i = 0; i < this.muteUrls.length; i++) {
            if (!advertisement.muteUrls[i].equals(this.muteUrls[i])) {
                return false;
            }
        }
        if (advertisement.unmuteUrls.length != this.unmuteUrls.length) {
            return false;
        }
        for (int i2 = 0; i2 < this.unmuteUrls.length; i2++) {
            if (!advertisement.unmuteUrls[i2].equals(this.unmuteUrls[i2])) {
                return false;
            }
        }
        if (advertisement.closeUrls.length != this.closeUrls.length) {
            return false;
        }
        for (int i3 = 0; i3 < this.closeUrls.length; i3++) {
            if (!advertisement.closeUrls[i3].equals(this.closeUrls[i3])) {
                return false;
            }
        }
        if (advertisement.postRollClickUrls.length != this.postRollClickUrls.length) {
            return false;
        }
        for (int i4 = 0; i4 < this.postRollClickUrls.length; i4++) {
            if (!advertisement.postRollClickUrls[i4].equals(this.postRollClickUrls[i4])) {
                return false;
            }
        }
        if (advertisement.postRollViewUrls.length != this.postRollViewUrls.length) {
            return false;
        }
        for (int i5 = 0; i5 < this.postRollViewUrls.length; i5++) {
            if (!advertisement.postRollViewUrls[i5].equals(this.postRollViewUrls[i5])) {
                return false;
            }
        }
        if (advertisement.videoClickUrls.length != this.videoClickUrls.length) {
            return false;
        }
        for (int i6 = 0; i6 < this.videoClickUrls.length; i6++) {
            if (!advertisement.videoClickUrls[i6].equals(this.videoClickUrls[i6])) {
                return false;
            }
        }
        if (advertisement.checkpoints.size() != this.checkpoints.size()) {
            return false;
        }
        for (int i7 = 0; i7 < this.checkpoints.size(); i7++) {
            if (!advertisement.checkpoints.get(i7).equals(this.checkpoints.get(i7))) {
                return false;
            }
        }
        if (advertisement.adMarketId.equals(this.adMarketId) && advertisement.bidToken.equals(this.bidToken)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0086, code lost:
        if (r10.equals("video.mute") != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d1, code lost:
        if (r10.equals(com.vungle.warren.analytics.AnalyticsEvent.Ad.videoClose) != false) goto L_0x0107;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] getTpatUrls(@android.support.annotation.NonNull java.lang.String r10) {
        /*
            r9 = this;
            int r0 = r9.adType
            r1 = 4
            r2 = 6
            r3 = 2
            r4 = 3
            r5 = 5
            r6 = -1
            r7 = 1
            r8 = 0
            switch(r0) {
                case 0: goto L_0x00b9;
                case 1: goto L_0x0015;
                default: goto L_0x000d;
            }
        L_0x000d:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unknown Advertisement Type!"
            r10.<init>(r0)
            throw r10
        L_0x0015:
            java.lang.String r0 = "checkpoint"
            boolean r0 = r10.startsWith(r0)
            if (r0 == 0) goto L_0x003c
            java.lang.String[] r0 = new java.lang.String[r8]
            java.lang.String r1 = "\\."
            java.lang.String[] r10 = r10.split(r1)
            r10 = r10[r7]
            int r10 = java.lang.Integer.parseInt(r10)
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r1 = r9.checkpoints
            int r10 = r10 / 25
            java.lang.Object r10 = r1.get(r10)
            com.vungle.warren.model.Advertisement$Checkpoint r10 = (com.vungle.warren.model.Advertisement.Checkpoint) r10
            if (r10 == 0) goto L_0x003b
            java.lang.String[] r0 = r10.getUrls()
        L_0x003b:
            return r0
        L_0x003c:
            int r0 = r10.hashCode()
            switch(r0) {
                case -1663300692: goto L_0x0080;
                case -1293192841: goto L_0x0076;
                case -481751803: goto L_0x006c;
                case -32221499: goto L_0x0062;
                case 906443463: goto L_0x0058;
                case 1370600644: goto L_0x004e;
                case 1621415126: goto L_0x0044;
                default: goto L_0x0043;
            }
        L_0x0043:
            goto L_0x0089
        L_0x0044:
            java.lang.String r0 = "postroll.view"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            r1 = 1
            goto L_0x008a
        L_0x004e:
            java.lang.String r0 = "video_click"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            r1 = 6
            goto L_0x008a
        L_0x0058:
            java.lang.String r0 = "clickUrl"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            r1 = 3
            goto L_0x008a
        L_0x0062:
            java.lang.String r0 = "video.close"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            r1 = 0
            goto L_0x008a
        L_0x006c:
            java.lang.String r0 = "video.unmute"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            r1 = 5
            goto L_0x008a
        L_0x0076:
            java.lang.String r0 = "postroll.click"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            r1 = 2
            goto L_0x008a
        L_0x0080:
            java.lang.String r0 = "video.mute"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0089
            goto L_0x008a
        L_0x0089:
            r1 = -1
        L_0x008a:
            switch(r1) {
                case 0: goto L_0x00b6;
                case 1: goto L_0x00b3;
                case 2: goto L_0x00b0;
                case 3: goto L_0x00ad;
                case 4: goto L_0x00aa;
                case 5: goto L_0x00a7;
                case 6: goto L_0x00a4;
                default: goto L_0x008d;
            }
        L_0x008d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown TPAT Event "
            r1.append(r2)
            r1.append(r10)
            java.lang.String r10 = r1.toString()
            r0.<init>(r10)
            throw r0
        L_0x00a4:
            java.lang.String[] r10 = r9.videoClickUrls
            return r10
        L_0x00a7:
            java.lang.String[] r10 = r9.unmuteUrls
            return r10
        L_0x00aa:
            java.lang.String[] r10 = r9.muteUrls
            return r10
        L_0x00ad:
            java.lang.String[] r10 = r9.clickUrls
            return r10
        L_0x00b0:
            java.lang.String[] r10 = r9.postRollClickUrls
            return r10
        L_0x00b3:
            java.lang.String[] r10 = r9.postRollViewUrls
            return r10
        L_0x00b6:
            java.lang.String[] r10 = r9.closeUrls
            return r10
        L_0x00b9:
            int r0 = r10.hashCode()
            switch(r0) {
                case -1964722632: goto L_0x00fc;
                case -840405966: goto L_0x00f2;
                case 3363353: goto L_0x00e8;
                case 109635558: goto L_0x00de;
                case 1370600644: goto L_0x00d4;
                case 1370606900: goto L_0x00cb;
                case 1666667655: goto L_0x00c1;
                default: goto L_0x00c0;
            }
        L_0x00c0:
            goto L_0x0106
        L_0x00c1:
            java.lang.String r0 = "postroll_view"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            r1 = 0
            goto L_0x0107
        L_0x00cb:
            java.lang.String r0 = "video_close"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            goto L_0x0107
        L_0x00d4:
            java.lang.String r0 = "video_click"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            r1 = 6
            goto L_0x0107
        L_0x00de:
            java.lang.String r0 = "postroll_click"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            r1 = 1
            goto L_0x0107
        L_0x00e8:
            java.lang.String r0 = "mute"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            r1 = 2
            goto L_0x0107
        L_0x00f2:
            java.lang.String r0 = "unmute"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            r1 = 3
            goto L_0x0107
        L_0x00fc:
            java.lang.String r0 = "click_url"
            boolean r0 = r10.equals(r0)
            if (r0 == 0) goto L_0x0106
            r1 = 5
            goto L_0x0107
        L_0x0106:
            r1 = -1
        L_0x0107:
            switch(r1) {
                case 0: goto L_0x0133;
                case 1: goto L_0x0130;
                case 2: goto L_0x012d;
                case 3: goto L_0x012a;
                case 4: goto L_0x0127;
                case 5: goto L_0x0124;
                case 6: goto L_0x0121;
                default: goto L_0x010a;
            }
        L_0x010a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown TPAT Event "
            r1.append(r2)
            r1.append(r10)
            java.lang.String r10 = r1.toString()
            r0.<init>(r10)
            throw r0
        L_0x0121:
            java.lang.String[] r10 = r9.videoClickUrls
            return r10
        L_0x0124:
            java.lang.String[] r10 = r9.clickUrls
            return r10
        L_0x0127:
            java.lang.String[] r10 = r9.closeUrls
            return r10
        L_0x012a:
            java.lang.String[] r10 = r9.unmuteUrls
            return r10
        L_0x012d:
            java.lang.String[] r10 = r9.muteUrls
            return r10
        L_0x0130:
            java.lang.String[] r10 = r9.postRollClickUrls
            return r10
        L_0x0133:
            java.lang.String[] r10 = r9.postRollViewUrls
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.model.Advertisement.getTpatUrls(java.lang.String):java.lang.String[]");
    }

    @NonNull
    public String getId() {
        if (this.identifier == null) {
            return "";
        }
        return this.identifier;
    }

    public String getAdToken() {
        return this.adToken;
    }

    public String getAppID() {
        return this.appID;
    }

    /* access modifiers changed from: package-private */
    public String getUrl() {
        return this.videoUrl;
    }

    public String getCampaign() {
        return this.campaign;
    }

    /* access modifiers changed from: package-private */
    public String getTemplateId() {
        return this.templateId;
    }

    public String getTemplateType() {
        return this.templateType;
    }

    public int getShowCloseDelay(boolean z) {
        if (z) {
            return this.showCloseIncentivized * 1000;
        }
        return this.showCloseDelay * 1000;
    }

    public boolean getMoatEnabled() {
        return this.enableMoat;
    }

    public String getMoatVastExtra() {
        return this.moatExtraVast;
    }

    public long getExpireTime() {
        return this.expireTime * 1000;
    }

    public JsonObject createMRAIDArgs() {
        if (this.templateSettings != null) {
            HashMap hashMap = new HashMap(this.templateSettings);
            for (Map.Entry next : this.cacheableAssets.entrySet()) {
                hashMap.put(next.getKey(), ((Pair) next.getValue()).first);
            }
            if (!this.mraidFiles.isEmpty()) {
                hashMap.putAll(this.mraidFiles);
            }
            if (!"true".equalsIgnoreCase((String) hashMap.get(START_MUTED))) {
                hashMap.put(START_MUTED, (getAdConfig().getSettings() & 1) != 0 ? "true" : "false");
            }
            JsonObject jsonObject = new JsonObject();
            for (Map.Entry entry : hashMap.entrySet()) {
                jsonObject.addProperty((String) entry.getKey(), (String) entry.getValue());
            }
            return jsonObject;
        }
        throw new IllegalArgumentException("Advertisement does not have MRAID Arguments!");
    }

    @Nullable
    public String getCTAURL(boolean z) {
        switch (this.adType) {
            case 0:
                return z ? this.ctaUrl : this.ctaDestinationUrl;
            case 1:
                return this.ctaUrl;
            default:
                throw new IllegalArgumentException("Unknown AdType " + this.adType);
        }
    }

    public boolean hasPostroll() {
        return !TextUtils.isEmpty(this.postrollBundleUrl);
    }

    public Map<String, String> getDownloadableUrls() {
        HashMap hashMap = new HashMap();
        switch (this.adType) {
            case 0:
                hashMap.put("video", this.videoUrl);
                if (!TextUtils.isEmpty(this.postrollBundleUrl)) {
                    hashMap.put(KEY_POSTROLL, this.postrollBundleUrl);
                    break;
                }
                break;
            case 1:
                hashMap.put("template", this.templateUrl);
                for (Map.Entry next : this.cacheableAssets.entrySet()) {
                    String str = (String) ((Pair) next.getValue()).first;
                    if (!TextUtils.isEmpty(str) && HttpUrl.parse(str) != null) {
                        hashMap.put(((String) next.getKey()) + "." + ((String) ((Pair) next.getValue()).second), str);
                    }
                }
                break;
            default:
                throw new IllegalStateException("Advertisement created without adType!");
        }
        return hashMap;
    }

    public void setMraidAssetDir(File file) {
        for (Map.Entry next : this.cacheableAssets.entrySet()) {
            File file2 = new File(file, ((String) next.getKey()) + "." + ((String) ((Pair) next.getValue()).second));
            if (file2.exists()) {
                Map<String, String> map = this.mraidFiles;
                Object key = next.getKey();
                map.put(key, FILE_SCHEME + file2.getPath());
            }
        }
    }

    public void setState(@State int i) {
        this.state = i;
    }

    @State
    public int getState() {
        return this.state;
    }

    public String getAdMarketId() {
        return this.adMarketId;
    }

    public String getBidToken() {
        return this.bidToken;
    }

    public static class Checkpoint implements Comparable<Checkpoint> {
        private byte percentage;
        private String[] urls;

        public Checkpoint(JsonObject jsonObject) throws IllegalArgumentException {
            if (JsonUtil.hasNonNull(jsonObject, "checkpoint")) {
                this.percentage = (byte) ((int) (jsonObject.get("checkpoint").getAsFloat() * 100.0f));
                if (JsonUtil.hasNonNull(jsonObject, "urls")) {
                    JsonArray asJsonArray = jsonObject.getAsJsonArray("urls");
                    this.urls = new String[asJsonArray.size()];
                    for (int i = 0; i < asJsonArray.size(); i++) {
                        if (asJsonArray.get(i) == null || "null".equalsIgnoreCase(asJsonArray.get(i).toString())) {
                            this.urls[i] = "";
                        } else {
                            this.urls[i] = asJsonArray.get(i).getAsString();
                        }
                    }
                    return;
                }
                throw new IllegalArgumentException("Checkpoint missing reporting URL!");
            }
            throw new IllegalArgumentException("Checkpoint missing percentage!");
        }

        public Checkpoint(JsonArray jsonArray, byte b) {
            if (jsonArray.size() != 0) {
                this.urls = new String[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    this.urls[i] = jsonArray.get(i).getAsString();
                }
                this.percentage = b;
                return;
            }
            throw new IllegalArgumentException("Empty URLS!");
        }

        public String[] getUrls() {
            return this.urls;
        }

        public byte getPercentage() {
            return this.percentage;
        }

        public int compareTo(@NonNull Checkpoint checkpoint) {
            return Float.compare((float) this.percentage, (float) checkpoint.percentage);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Checkpoint)) {
                return false;
            }
            Checkpoint checkpoint = (Checkpoint) obj;
            if (checkpoint.percentage != this.percentage || checkpoint.urls.length != this.urls.length) {
                return false;
            }
            for (int i = 0; i < this.urls.length; i++) {
                if (!checkpoint.urls[i].equals(this.urls[i])) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            return super.hashCode();
        }
    }

    public String toString() {
        return "Advertisement{adType=" + this.adType + ", identifier='" + this.identifier + '\'' + ", appID='" + this.appID + '\'' + ", expireTime=" + this.expireTime + ", checkpoints=" + this.checkpoints + ", muteUrls=" + Arrays.toString(this.muteUrls) + ", unmuteUrls=" + Arrays.toString(this.unmuteUrls) + ", closeUrls=" + Arrays.toString(this.closeUrls) + ", postRollClickUrls=" + Arrays.toString(this.postRollClickUrls) + ", postRollViewUrls=" + Arrays.toString(this.postRollViewUrls) + ", videoClickUrls=" + Arrays.toString(this.videoClickUrls) + ", clickUrls=" + Arrays.toString(this.clickUrls) + ", delay=" + this.delay + ", campaign='" + this.campaign + '\'' + ", showCloseDelay=" + this.showCloseDelay + ", showCloseIncentivized=" + this.showCloseIncentivized + ", countdown=" + this.countdown + ", videoUrl='" + this.videoUrl + '\'' + ", videoWidth=" + this.videoWidth + ", videoHeight=" + this.videoHeight + ", md5='" + this.md5 + '\'' + ", postrollBundleUrl='" + this.postrollBundleUrl + '\'' + ", ctaOverlayEnabled=" + this.ctaOverlayEnabled + ", ctaClickArea=" + this.ctaClickArea + ", ctaDestinationUrl='" + this.ctaDestinationUrl + '\'' + ", ctaUrl='" + this.ctaUrl + '\'' + ", adConfig=" + this.adConfig + ", retryCount=" + this.retryCount + ", adToken='" + this.adToken + '\'' + ", videoIdentifier='" + this.videoIdentifier + '\'' + ", templateUrl='" + this.templateUrl + '\'' + ", templateSettings=" + this.templateSettings + ", mraidFiles=" + this.mraidFiles + ", cacheableAssets=" + this.cacheableAssets + ", templateId='" + this.templateId + '\'' + ", templateType='" + this.templateType + '\'' + ", enableMoat=" + this.enableMoat + ", moatExtraVast='" + this.moatExtraVast + '\'' + ", requiresNonMarketInstall=" + this.requiresNonMarketInstall + ", adMarketId='" + this.adMarketId + '\'' + ", bidToken='" + this.bidToken + '\'' + ", state=" + this.state + '}';
    }
}
