package com.vungle.warren.downloader;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.NetworkProvider;
import com.vungle.warren.utility.PriorityRunnable;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLException;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okio.GzipSource;
import okio.Okio;

@SuppressLint({"LogNotTimber"})
public class AssetDownloader {
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String ACCEPT_RANGES = "Accept-Ranges";
    private static final String BYTES = "bytes";
    private static final int CONNECTION_RETRY_TIMEOUT = 300;
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String CONTENT_RANGE = "Content-Range";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final int DOWNLOAD_CHUNK_SIZE = 2048;
    private static final String ETAG = "ETag";
    private static final String GZIP = "gzip";
    private static final String IDENTITY = "identity";
    private static final String IF_RANGE = "If-Range";
    private static final String LAST_MODIFIED = "Last-Modified";
    private static final long MAX_PERCENT = 100;
    private static final int MAX_RECONNECT_ATTEMPTS = 10;
    private static final String META_POSTFIX_EXT = ".vng_meta";
    private static final int PROGRESS_STEP = 5;
    private static final String RANGE = "Range";
    private static final int RANGE_NOT_SATISFIABLE = 416;
    private static final int RETRY_COUNT_ON_CONNECTION_LOST = 5;
    /* access modifiers changed from: private */
    public static final String TAG = "AssetDownloader";
    private static final int TIMEOUT = 30;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, DownloadRequest> connections = new ConcurrentHashMap<>();
    private final ExecutorService downloadExecutor;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, AssetDownloadListener> listeners = new ConcurrentHashMap<>();
    int maxReconnectAttempts = 10;
    /* access modifiers changed from: private */
    public final NetworkProvider.NetworkListener networkListener = new NetworkProvider.NetworkListener() {
        public void onChanged(int i) {
            String access$100 = AssetDownloader.TAG;
            Log.d(access$100, "Network changed: " + i);
            AssetDownloader.this.onNetworkChanged(i);
        }
    };
    /* access modifiers changed from: private */
    public final NetworkProvider networkProvider;
    /* access modifiers changed from: private */
    public final OkHttpClient okHttpClient;
    /* access modifiers changed from: private */
    public volatile int progressStep = 5;
    int reconnectTimeout = 300;
    int retryCountOnConnectionLost = 5;
    private final ExecutorService uiExecutor;

    public @interface NetworkType {
        public static final int ANY = 3;
        public static final int CELLULAR = 1;
        public static final int WIFI = 2;
    }

    public AssetDownloader(int i, NetworkProvider networkProvider2, ExecutorService executorService) {
        int max = Math.max(i, 1);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(max, max, 1, TimeUnit.SECONDS, new PriorityBlockingQueue());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        this.downloadExecutor = threadPoolExecutor;
        this.networkProvider = networkProvider2;
        this.uiExecutor = executorService;
        this.okHttpClient = new OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS).cache(null).followRedirects(true).followSslRedirects(true).build();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0077, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void download(final com.vungle.warren.downloader.DownloadRequest r5, final com.vungle.warren.downloader.AssetDownloadListener r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r5 != 0) goto L_0x0011
            if (r6 == 0) goto L_0x000f
            java.util.concurrent.ExecutorService r5 = r4.uiExecutor     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.AssetDownloader$1 r0 = new com.vungle.warren.downloader.AssetDownloader$1     // Catch:{ all -> 0x0092 }
            r0.<init>(r6)     // Catch:{ all -> 0x0092 }
            r5.execute(r0)     // Catch:{ all -> 0x0092 }
        L_0x000f:
            monitor-exit(r4)
            return
        L_0x0011:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r0 = r4.connections     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r5.id     // Catch:{ all -> 0x0092 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.DownloadRequest r0 = (com.vungle.warren.downloader.DownloadRequest) r0     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x0043
            java.lang.String r1 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0092 }
            r2.<init>()     // Catch:{ all -> 0x0092 }
            java.lang.String r3 = "Request with same url and path already present "
            r2.append(r3)     // Catch:{ all -> 0x0092 }
            java.lang.String r0 = r0.id     // Catch:{ all -> 0x0092 }
            r2.append(r0)     // Catch:{ all -> 0x0092 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0092 }
            android.util.Log.d(r1, r0)     // Catch:{ all -> 0x0092 }
            if (r6 == 0) goto L_0x0041
            java.util.concurrent.ExecutorService r0 = r4.uiExecutor     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.AssetDownloader$2 r1 = new com.vungle.warren.downloader.AssetDownloader$2     // Catch:{ all -> 0x0092 }
            r1.<init>(r6, r5)     // Catch:{ all -> 0x0092 }
            r0.execute(r1)     // Catch:{ all -> 0x0092 }
        L_0x0041:
            monitor-exit(r4)
            return
        L_0x0043:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r0 = r4.connections     // Catch:{ all -> 0x0092 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x0092 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0092 }
        L_0x004d:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0092 }
            if (r1 == 0) goto L_0x0078
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.DownloadRequest r1 = (com.vungle.warren.downloader.DownloadRequest) r1     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r1.path     // Catch:{ all -> 0x0092 }
            java.lang.String r2 = r5.path     // Catch:{ all -> 0x0092 }
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x0092 }
            if (r1 == 0) goto L_0x004d
            java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = "Already present request for same path"
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x0092 }
            if (r6 == 0) goto L_0x0076
            java.util.concurrent.ExecutorService r0 = r4.uiExecutor     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.AssetDownloader$3 r1 = new com.vungle.warren.downloader.AssetDownloader$3     // Catch:{ all -> 0x0092 }
            r1.<init>(r6, r5)     // Catch:{ all -> 0x0092 }
            r0.execute(r1)     // Catch:{ all -> 0x0092 }
        L_0x0076:
            monitor-exit(r4)
            return
        L_0x0078:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.AssetDownloadListener> r0 = r4.listeners     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r5.id     // Catch:{ all -> 0x0092 }
            r0.put(r1, r6)     // Catch:{ all -> 0x0092 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r6 = r4.connections     // Catch:{ all -> 0x0092 }
            java.lang.String r0 = r5.id     // Catch:{ all -> 0x0092 }
            r6.put(r0, r5)     // Catch:{ all -> 0x0092 }
            com.vungle.warren.utility.NetworkProvider r6 = r4.networkProvider     // Catch:{ all -> 0x0092 }
            com.vungle.warren.utility.NetworkProvider$NetworkListener r0 = r4.networkListener     // Catch:{ all -> 0x0092 }
            r6.addListener(r0)     // Catch:{ all -> 0x0092 }
            r4.load(r5)     // Catch:{ all -> 0x0092 }
            monitor-exit(r4)
            return
        L_0x0092:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.download(com.vungle.warren.downloader.DownloadRequest, com.vungle.warren.downloader.AssetDownloadListener):void");
    }

    public List<DownloadRequest> getAllRequests() {
        return new ArrayList(this.connections.values());
    }

    private synchronized void load(final DownloadRequest downloadRequest) {
        downloadRequest.set(1);
        this.downloadExecutor.execute(new DownloadPriorityRunnable(downloadRequest.priority) {
            /* JADX INFO: finally extract failed */
            /* JADX WARN: Failed to insert an additional move for type inference into block B:515:0x0012 */
            /* JADX INFO: additional move instructions added (1) to help type inference */
            /* JADX INFO: additional move instructions added (4) to help type inference */
            /* JADX INFO: additional move instructions added (16) to help type inference */
            /* JADX INFO: additional move instructions added (2) to help type inference */
            /* JADX WARN: Type inference failed for: r3v1 */
            /* JADX WARN: Type inference failed for: r3v2, types: [java.io.Closeable, java.io.File] */
            /* JADX WARN: Type inference failed for: r3v3 */
            /* JADX WARN: Type inference failed for: r3v6 */
            /* JADX WARN: Type inference failed for: r3v47 */
            /* JADX WARN: Type inference failed for: r8v102 */
            /* JADX WARN: Type inference failed for: r8v103 */
            /* JADX WARN: Type inference failed for: r5v142 */
            /* JADX WARNING: Code restructure failed: missing block: B:117:0x036a, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:118:0x036b, code lost:
                r3 = r0;
                r25 = r7;
                r8 = r13;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:125:0x0385, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$700(r1.this$0, r9, r3, r4) != false) goto L_0x0387;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:169:0x044e, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:170:0x044f, code lost:
                r11 = null;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:171:0x0450, code lost:
                r3 = r0;
                r25 = r7;
                r19 = r11;
                r22 = r19;
                r8 = r13;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:186:0x047d, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:187:0x047f, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:200:0x04ab, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:201:0x04ac, code lost:
                r3 = r0;
                r25 = r7;
                r8 = r13;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:320:0x073c, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:321:0x073d, code lost:
                r25 = r7;
                r8 = r13;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:323:0x0746, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:324:0x0747, code lost:
                r25 = r7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:335:0x0772, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:336:0x0774, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:337:0x0776, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:338:0x0777, code lost:
                r7 = r24;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:339:0x0779, code lost:
                r3 = r0;
                r10 = r7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:345:0x0789, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:346:0x078a, code lost:
                r25 = r7;
                r8 = r13;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:347:0x078d, code lost:
                r3 = r0;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:383:?, code lost:
                r11 = com.vungle.warren.downloader.AssetDownloader.access$300(r1.this$0, r4);
                r4.setConnected(r11);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:384:0x0817, code lost:
                if (r11 == false) goto L_0x0819;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:385:0x0819, code lost:
                r2.status = 5;
                com.vungle.warren.downloader.AssetDownloader.access$900(r1.this$0, r4.id, r2);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:386:0x082a, code lost:
                if (r4.is(3) == false) goto L_0x082c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:387:0x082c, code lost:
                r14 = r4 + 1;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:388:0x0832, code lost:
                if (r4 < r1.this$0.maxReconnectAttempts) goto L_0x0834;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:389:0x0834, code lost:
                r4 = 0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:391:0x0839, code lost:
                if (r4 < r1.this$0.retryCountOnConnectionLost) goto L_0x083b;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:394:0x0844, code lost:
                r28 = r14;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:396:?, code lost:
                java.lang.Thread.sleep((long) java.lang.Math.max(0, r1.this$0.reconnectTimeout));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:397:0x084b, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:398:0x084d, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:399:0x084e, code lost:
                r28 = r14;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:402:?, code lost:
                r0.printStackTrace();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:404:0x085b, code lost:
                if (r4.is(3) == false) goto L_0x085e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:406:0x085e, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Trying to reconnect");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:407:0x086f, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$300(r1.this$0, r4) != false) goto L_0x0871;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:408:0x0871, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Reconnected, starting download again");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:410:?, code lost:
                r4.setConnected(true);
                r4.set(1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:411:0x0885, code lost:
                r4 = false;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:412:0x0888, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:413:0x0889, code lost:
                r3 = r0;
                r22 = r7;
                r21 = false;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:415:?, code lost:
                r4.setConnected(false);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:416:0x0896, code lost:
                r4 = r4 + 1;
                r14 = r28;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:417:0x089d, code lost:
                r28 = r14;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:419:0x08a1, code lost:
                r28 = r4;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:420:0x08a4, code lost:
                r4 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:421:0x08a5, code lost:
                if (r4 != false) goto L_0x08a7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:424:0x08ae, code lost:
                if (r4.is(3) == false) goto L_0x08b0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:426:0x08b8, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1100(r1.this$0, r4) != false) goto L_0x08ba;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:428:?, code lost:
                r4.set(2);
                com.vungle.warren.downloader.AssetDownloader.access$1200(r1.this$0, r2, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:429:0x08c7, code lost:
                r25 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:430:0x08ca, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:431:0x08cb, code lost:
                r3 = r0;
                r21 = r4;
                r22 = r7;
                r25 = true;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:433:?, code lost:
                r4.set(5);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:434:0x08e5, code lost:
                r6 = new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(r10, r3, com.vungle.warren.downloader.AssetDownloader.access$1300(r1.this$0, r3, r11));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:435:0x08e7, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:436:0x08e8, code lost:
                r3 = r0;
                r21 = r4;
                r22 = r7;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:437:0x08ef, code lost:
                r12 = r4;
                r11 = r6;
                r4 = r28;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:438:0x08f4, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:442:0x08fa, code lost:
                if ((r3 instanceof com.vungle.warren.downloader.AssetDownloader.RequestException) != false) goto L_0x08fc;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:444:0x08fe, code lost:
                r12 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:446:?, code lost:
                r11 = new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(r10, r3, 1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:447:0x0903, code lost:
                r12 = true;
                r11 = new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(r10, r3, 4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:453:0x091b, code lost:
                r5.cancel();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:456:0x0941, code lost:
                if (r12 != false) goto L_0x0943;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:459:0x0949, code lost:
                switch(r4.getStatus()) {
                    case 2: goto L_0x0980;
                    case 3: goto L_0x095d;
                    case 4: goto L_0x0955;
                    case 5: goto L_0x094d;
                    default: goto L_0x094c;
                };
             */
            /* JADX WARNING: Code restructure failed: missing block: B:461:0x094d, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1500(r1.this$0, r11, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:462:0x0955, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1400(r1.this$0, r8, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:463:0x095d, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1600(r1.this$0, r4, r2);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:464:0x0964, code lost:
                if (r25 == false) goto L_0x0966;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:465:0x0966, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).remove(r4.id);
                com.vungle.warren.downloader.AssetDownloader.access$1800(r1.this$0).remove(r4.id);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:466:0x0980, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Removing connections and listener " + com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:467:0x09a1, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:469:0x09a4, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Not removing connections and listener " + com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:471:0x09ce, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).isEmpty() != false) goto L_0x09d0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:472:0x09d0, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$2000(r1.this$0).removeListener(com.vungle.warren.downloader.AssetDownloader.access$1900(r1.this$0));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:474:0x09e0, code lost:
                com.vungle.warren.utility.FileUtility.closeQuietly(r7);
                com.vungle.warren.utility.FileUtility.closeQuietly(r19);
                r8 = r4;
                r6 = r11;
                r5 = r12;
                r7 = r25;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:478:0x09ef, code lost:
                throw r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:488:0x0a08, code lost:
                r5.cancel();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:491:0x0a2e, code lost:
                if (r21 != false) goto L_0x0a30;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:494:0x0a36, code lost:
                switch(r4.getStatus()) {
                    case 2: goto L_0x0a6d;
                    case 3: goto L_0x0a4a;
                    case 4: goto L_0x0a42;
                    case 5: goto L_0x0a3a;
                    default: goto L_0x0a39;
                };
             */
            /* JADX WARNING: Code restructure failed: missing block: B:496:0x0a3a, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1500(r1.this$0, r6, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:497:0x0a42, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1400(r1.this$0, r8, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:498:0x0a4a, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1600(r1.this$0, r4, r2);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:499:0x0a51, code lost:
                if (r25 == false) goto L_0x0a53;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:500:0x0a53, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).remove(r4.id);
                com.vungle.warren.downloader.AssetDownloader.access$1800(r1.this$0).remove(r4.id);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:501:0x0a6d, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Removing connections and listener " + com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:502:0x0a8e, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:504:0x0a91, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Not removing connections and listener " + com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:506:0x0abb, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).isEmpty() != false) goto L_0x0abd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:507:0x0abd, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$2000(r1.this$0).removeListener(com.vungle.warren.downloader.AssetDownloader.access$1900(r1.this$0));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:509:0x0acd, code lost:
                com.vungle.warren.utility.FileUtility.closeQuietly(r22);
                com.vungle.warren.utility.FileUtility.closeQuietly(r19);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:510:0x0ad3, code lost:
                throw r3;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:513:0x0ad5, code lost:
                throw r0;
             */
            /* JADX WARNING: Failed to process nested try/catch */
            /* JADX WARNING: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:105:0x0308 A[Catch:{ all -> 0x0366 }] */
            /* JADX WARNING: Removed duplicated region for block: B:117:0x036a A[ExcHandler: all (r0v69 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:87:0x02a8] */
            /* JADX WARNING: Removed duplicated region for block: B:169:0x044e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:133:0x0394] */
            /* JADX WARNING: Removed duplicated region for block: B:186:0x047d A[ExcHandler: all (th java.lang.Throwable), PHI: r11 
              PHI: (r11v22 java.io.Closeable) = (r11v29 java.io.Closeable), (r11v32 java.io.Closeable) binds: [B:162:0x0429, B:181:0x0471] A[DONT_GENERATE, DONT_INLINE], Splitter:B:162:0x0429] */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x00af A[Catch:{ all -> 0x010c }] */
            /* JADX WARNING: Removed duplicated region for block: B:285:0x0690 A[Catch:{ all -> 0x06f4 }] */
            /* JADX WARNING: Removed duplicated region for block: B:336:0x0774 A[ExcHandler: all (th java.lang.Throwable), PHI: r8 r25 
              PHI: (r8v48 java.io.File) = (r8v49 java.io.File), (r8v49 java.io.File), (r8v49 java.io.File), (r8v55 java.io.File) binds: [B:329:0x0759, B:330:?, B:332:0x0767, B:316:0x0732] A[DONT_GENERATE, DONT_INLINE]
              PHI: (r25v35 boolean) = (r25v36 boolean), (r25v36 boolean), (r25v36 boolean), (r25v42 boolean) binds: [B:329:0x0759, B:330:?, B:332:0x0767, B:316:0x0732] A[DONT_GENERATE, DONT_INLINE], Splitter:B:316:0x0732] */
            /* JADX WARNING: Removed duplicated region for block: B:345:0x0789 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:188:0x0481] */
            /* JADX WARNING: Removed duplicated region for block: B:382:0x0809 A[SYNTHETIC, Splitter:B:382:0x0809] */
            /* JADX WARNING: Removed duplicated region for block: B:406:0x085e A[Catch:{ all -> 0x08f4 }] */
            /* JADX WARNING: Removed duplicated region for block: B:422:0x08a7 A[SYNTHETIC, Splitter:B:422:0x08a7] */
            /* JADX WARNING: Removed duplicated region for block: B:439:0x08f7  */
            /* JADX WARNING: Removed duplicated region for block: B:453:0x091b  */
            /* JADX WARNING: Removed duplicated region for block: B:456:0x0941  */
            /* JADX WARNING: Removed duplicated region for block: B:465:0x0966 A[Catch:{ all -> 0x09a1 }] */
            /* JADX WARNING: Removed duplicated region for block: B:488:0x0a08  */
            /* JADX WARNING: Removed duplicated region for block: B:491:0x0a2e  */
            /* JADX WARNING: Removed duplicated region for block: B:500:0x0a53 A[Catch:{ all -> 0x0a8e }] */
            /* JADX WARNING: Removed duplicated region for block: B:541:0x085d A[SYNTHETIC] */
            /* JADX WARNING: Unknown variable types count: 1 */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r29 = this;
                    r1 = r29
                    com.vungle.warren.downloader.AssetDownloadListener$Progress r2 = new com.vungle.warren.downloader.AssetDownloadListener$Progress
                    r2.<init>()
                    long r3 = java.lang.System.currentTimeMillis()
                    r2.timestampDownloadStart = r3
                    r3 = 0
                    r6 = r3
                    r5 = 0
                    r7 = 0
                    r8 = 0
                L_0x0012:
                    if (r5 != 0) goto L_0x0ad6
                    com.vungle.warren.downloader.DownloadRequest r5 = r4
                    java.lang.String r5 = r5.url
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r9 = r9.path
                    java.lang.String r10 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    java.lang.String r12 = "Start load: "
                    r11.append(r12)
                    com.vungle.warren.downloader.DownloadRequest r12 = r4
                    java.lang.String r12 = r12.id
                    r11.append(r12)
                    java.lang.String r12 = " url: "
                    r11.append(r12)
                    r11.append(r5)
                    java.lang.String r11 = r11.toString()
                    android.util.Log.d(r10, r11)
                    r12 = 1
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    boolean r13 = r13.is(r12)     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    if (r13 != 0) goto L_0x012e
                    java.lang.String r5 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    r9.<init>()     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    java.lang.String r13 = "Abort download, wrong state "
                    r9.append(r13)     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    java.lang.String r13 = r13.debugString(r14)     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    r9.append(r13)     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    android.util.Log.w(r5, r9)     // Catch:{ Throwable -> 0x011f, all -> 0x0110 }
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r8 = "request is done "
                    r5.append(r8)
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r8 = r8.debugString(r9)
                    r5.append(r8)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r4, r5)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r4)
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x010c }
                    int r5 = r5.getStatus()     // Catch:{ all -> 0x010c }
                    switch(r5) {
                        case 2: goto L_0x00c9;
                        case 3: goto L_0x00a6;
                        case 4: goto L_0x009e;
                        case 5: goto L_0x0096;
                        default: goto L_0x0095;
                    }     // Catch:{ all -> 0x010c }
                L_0x0095:
                    goto L_0x00ad
                L_0x0096:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x010c }
                    r2.onError(r6, r5)     // Catch:{ all -> 0x010c }
                    goto L_0x00c9
                L_0x009e:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x010c }
                    r2.onSuccess(r3, r5)     // Catch:{ all -> 0x010c }
                    goto L_0x00c9
                L_0x00a6:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x010c }
                    r5.onCancelled(r6, r2)     // Catch:{ all -> 0x010c }
                L_0x00ad:
                    if (r7 != 0) goto L_0x00c9
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x010c }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x010c }
                    r2.remove(r5)     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.listeners     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x010c }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x010c }
                    r2.remove(r5)     // Catch:{ all -> 0x010c }
                L_0x00c9:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x010c }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
                    r5.<init>()     // Catch:{ all -> 0x010c }
                    java.lang.String r6 = "Removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x010c }
                    java.lang.String r6 = r6.debugString(r7)     // Catch:{ all -> 0x010c }
                    r5.append(r6)     // Catch:{ all -> 0x010c }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x010c }
                    android.util.Log.d(r2, r5)     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x010c }
                    boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x010c }
                    if (r2 == 0) goto L_0x0104
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    com.vungle.warren.utility.NetworkProvider r2 = r2.networkProvider     // Catch:{ all -> 0x010c }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x010c }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r5 = r5.networkListener     // Catch:{ all -> 0x010c }
                    r2.removeListener(r5)     // Catch:{ all -> 0x010c }
                L_0x0104:
                    monitor-exit(r4)     // Catch:{ all -> 0x010c }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r3)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r3)
                    return
                L_0x010c:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r4)     // Catch:{ all -> 0x010c }
                    throw r2
                L_0x0110:
                    r0 = move-exception
                    r5 = r3
                    r8 = r5
                    r9 = r8
                    r19 = r9
                    r22 = r19
                    r25 = r7
                L_0x011a:
                    r21 = 1
                    r3 = r0
                    goto L_0x09f7
                L_0x011f:
                    r0 = move-exception
                    r5 = r3
                    r9 = r5
                    r19 = r9
                    r25 = r7
                    r4 = r8
                    r10 = -1
                    r3 = r0
                    r7 = r19
                    r8 = r7
                    goto L_0x07f6
                L_0x012e:
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    boolean r13 = r13.isConnected(r14)     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    if (r13 == 0) goto L_0x07bf
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    r13.setConnected(r12)     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    java.io.File r13 = new java.io.File     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    r13.<init>(r9)     // Catch:{ Throwable -> 0x07e7, all -> 0x07d8 }
                    java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r15.<init>()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r15.append(r9)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r9 = ".vng_meta"
                    r15.append(r9)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r9 = r15.toString()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r14.<init>(r9)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.io.File r9 = r13.getParentFile()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    if (r9 == 0) goto L_0x018a
                    java.io.File r9 = r13.getParentFile()     // Catch:{ Throwable -> 0x017b, all -> 0x0170 }
                    boolean r9 = r9.exists()     // Catch:{ Throwable -> 0x017b, all -> 0x0170 }
                    if (r9 != 0) goto L_0x018a
                    java.io.File r9 = r13.getParentFile()     // Catch:{ Throwable -> 0x017b, all -> 0x0170 }
                    r9.mkdirs()     // Catch:{ Throwable -> 0x017b, all -> 0x0170 }
                    goto L_0x018a
                L_0x0170:
                    r0 = move-exception
                    r5 = r3
                    r9 = r5
                    r19 = r9
                    r22 = r19
                    r25 = r7
                    r8 = r13
                    goto L_0x011a
                L_0x017b:
                    r0 = move-exception
                    r5 = r3
                    r9 = r5
                    r19 = r9
                    r25 = r7
                    r4 = r8
                    r8 = r13
                    r10 = -1
                    r3 = r0
                    r7 = r19
                    goto L_0x07f6
                L_0x018a:
                    boolean r9 = r13.exists()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    if (r9 == 0) goto L_0x0197
                    long r17 = r13.length()     // Catch:{ Throwable -> 0x017b, all -> 0x0170 }
                    r3 = r17
                    goto L_0x0199
                L_0x0197:
                    r3 = 0
                L_0x0199:
                    java.lang.String r9 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r10.<init>()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r12 = "already downloaded : "
                    r10.append(r12)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r10.append(r3)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r12 = ", file exists = "
                    r10.append(r12)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    boolean r12 = r13.exists()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r10.append(r12)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    com.vungle.warren.downloader.AssetDownloader r12 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    com.vungle.warren.downloader.DownloadRequest r15 = r4     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r12 = r12.debugString(r15)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r10.append(r12)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    android.util.Log.d(r9, r10)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    okhttp3.Request$Builder r9 = new okhttp3.Request$Builder     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    r9.<init>()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    okhttp3.Request$Builder r5 = r9.url(r5)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    java.lang.String r9 = "Accept-Encoding"
                    java.lang.String r10 = "identity"
                    okhttp3.Request$Builder r5 = r5.addHeader(r9, r10)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    boolean r9 = r13.exists()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    if (r9 == 0) goto L_0x025a
                    boolean r9 = r14.exists()     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    if (r9 == 0) goto L_0x025a
                    java.lang.String r9 = r14.getPath()     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.util.Map r9 = com.vungle.warren.utility.FileUtility.readMap(r9)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r10 = "bytes"
                    java.lang.String r12 = "Accept-Ranges"
                    java.lang.Object r12 = r9.get(r12)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r12 = (java.lang.String) r12     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    boolean r10 = r10.equalsIgnoreCase(r12)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    if (r10 == 0) goto L_0x025a
                    java.lang.String r10 = "identity"
                    java.lang.String r12 = "Content-Encoding"
                    java.lang.Object r12 = r9.get(r12)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r12 = (java.lang.String) r12     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    boolean r10 = r10.equalsIgnoreCase(r12)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    if (r10 != 0) goto L_0x0215
                    java.lang.String r10 = "Content-Encoding"
                    java.lang.Object r10 = r9.get(r10)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    if (r10 != 0) goto L_0x025a
                L_0x0215:
                    java.lang.String r10 = "Range"
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    r12.<init>()     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r15 = "bytes="
                    r12.append(r15)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    r12.append(r3)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r15 = "-"
                    r12.append(r15)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r12 = r12.toString()     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    r5.addHeader(r10, r12)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r10 = "ETag"
                    java.lang.Object r10 = r9.get(r10)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    java.lang.String r10 = (java.lang.String) r10     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    if (r10 != 0) goto L_0x0243
                    java.lang.String r10 = "Last-Modified"
                    java.lang.Object r9 = r9.get(r10)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    r10 = r9
                    java.lang.String r10 = (java.lang.String) r10     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                L_0x0243:
                    if (r10 == 0) goto L_0x025a
                    java.lang.String r9 = "If-Range"
                    r5.addHeader(r9, r10)     // Catch:{ Throwable -> 0x0252, all -> 0x024b }
                    goto L_0x025a
                L_0x024b:
                    r0 = move-exception
                    r3 = r0
                    r25 = r7
                    r8 = r13
                    goto L_0x07b1
                L_0x0252:
                    r0 = move-exception
                    r3 = r0
                    r25 = r7
                    r4 = r8
                    r8 = r13
                    goto L_0x07bc
                L_0x025a:
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    okhttp3.OkHttpClient r9 = r9.okHttpClient     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    okhttp3.Request r5 = r5.build()     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    okhttp3.Call r5 = r9.newCall(r5)     // Catch:{ Throwable -> 0x07b3, all -> 0x07ac }
                    okhttp3.Response r9 = r5.execute()     // Catch:{ Throwable -> 0x07a2, all -> 0x079c }
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    long r15 = r10.getContentLength(r9)     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    java.lang.String r10 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    r12.<init>()     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    java.lang.String r11 = "Response code: "
                    r12.append(r11)     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    int r11 = r9.code()     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    r12.append(r11)     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    java.lang.String r11 = " "
                    r12.append(r11)     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    java.lang.String r11 = r11.id     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    r12.append(r11)     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    java.lang.String r11 = r12.toString()     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    android.util.Log.d(r10, r11)     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    int r10 = r9.code()     // Catch:{ Throwable -> 0x0790, all -> 0x0789 }
                    com.vungle.warren.downloader.AssetDownloader r11 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x077c, all -> 0x0789 }
                    com.vungle.warren.downloader.DownloadRequest r12 = r4     // Catch:{ Throwable -> 0x077c, all -> 0x0789 }
                    boolean r11 = r11.fullyDownloadedContent(r13, r9, r12)     // Catch:{ Throwable -> 0x077c, all -> 0x0789 }
                    if (r11 == 0) goto L_0x0379
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                    r4 = 4
                    r3.set(r4)     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                    if (r9 == 0) goto L_0x02bd
                    okhttp3.ResponseBody r3 = r9.body()
                    if (r3 == 0) goto L_0x02bd
                    okhttp3.ResponseBody r3 = r9.body()
                    r3.close()
                L_0x02bd:
                    if (r5 == 0) goto L_0x02c2
                    r5.cancel()
                L_0x02c2:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "request is done "
                    r4.append(r5)
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r8 = r4
                    java.lang.String r5 = r5.debugString(r8)
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    android.util.Log.d(r3, r4)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r3)
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0366 }
                    int r4 = r4.getStatus()     // Catch:{ all -> 0x0366 }
                    switch(r4) {
                        case 2: goto L_0x0322;
                        case 3: goto L_0x02ff;
                        case 4: goto L_0x02f7;
                        case 5: goto L_0x02ef;
                        default: goto L_0x02ee;
                    }     // Catch:{ all -> 0x0366 }
                L_0x02ee:
                    goto L_0x0306
                L_0x02ef:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0366 }
                    r2.onError(r6, r4)     // Catch:{ all -> 0x0366 }
                    goto L_0x0322
                L_0x02f7:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0366 }
                    r2.onSuccess(r13, r4)     // Catch:{ all -> 0x0366 }
                    goto L_0x0322
                L_0x02ff:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0366 }
                    r4.onCancelled(r5, r2)     // Catch:{ all -> 0x0366 }
                L_0x0306:
                    if (r7 != 0) goto L_0x0322
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0366 }
                    java.lang.String r4 = r4.id     // Catch:{ all -> 0x0366 }
                    r2.remove(r4)     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.listeners     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0366 }
                    java.lang.String r4 = r4.id     // Catch:{ all -> 0x0366 }
                    r2.remove(r4)     // Catch:{ all -> 0x0366 }
                L_0x0322:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0366 }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0366 }
                    r4.<init>()     // Catch:{ all -> 0x0366 }
                    java.lang.String r5 = "Removing connections and listener "
                    r4.append(r5)     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x0366 }
                    java.lang.String r5 = r5.debugString(r6)     // Catch:{ all -> 0x0366 }
                    r4.append(r5)     // Catch:{ all -> 0x0366 }
                    java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0366 }
                    android.util.Log.d(r2, r4)     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0366 }
                    boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0366 }
                    if (r2 == 0) goto L_0x035d
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.utility.NetworkProvider r2 = r2.networkProvider     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0366 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r4 = r4.networkListener     // Catch:{ all -> 0x0366 }
                    r2.removeListener(r4)     // Catch:{ all -> 0x0366 }
                L_0x035d:
                    monitor-exit(r3)     // Catch:{ all -> 0x0366 }
                    r2 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    return
                L_0x0366:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r3)     // Catch:{ all -> 0x0366 }
                    throw r2
                L_0x036a:
                    r0 = move-exception
                    r3 = r0
                    r25 = r7
                    r8 = r13
                    goto L_0x07df
                L_0x0371:
                    r0 = move-exception
                    r3 = r0
                    r25 = r7
                    r4 = r8
                    r8 = r13
                    goto L_0x0786
                L_0x0379:
                    r11 = 206(0xce, float:2.89E-43)
                    if (r10 != r11) goto L_0x0387
                    com.vungle.warren.downloader.AssetDownloader r12 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                    boolean r11 = r12.satisfiesPartialDownload(r9, r3, r11)     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                    if (r11 == 0) goto L_0x038b
                L_0x0387:
                    r11 = 416(0x1a0, float:5.83E-43)
                    if (r10 != r11) goto L_0x0466
                L_0x038b:
                    boolean r3 = r13.exists()     // Catch:{ Throwable -> 0x045a, all -> 0x044e }
                    if (r3 == 0) goto L_0x0394
                    r13.delete()     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                L_0x0394:
                    boolean r3 = r14.exists()     // Catch:{ Throwable -> 0x045a, all -> 0x044e }
                    if (r3 == 0) goto L_0x039d
                    r14.delete()     // Catch:{ Throwable -> 0x0371, all -> 0x036a }
                L_0x039d:
                    int r3 = r8 + 1
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x0442, all -> 0x044e }
                    int r4 = r4.maxReconnectAttempts     // Catch:{ Throwable -> 0x0442, all -> 0x044e }
                    if (r8 >= r4) goto L_0x0428
                    if (r9 == 0) goto L_0x03b4
                    okhttp3.ResponseBody r4 = r9.body()
                    if (r4 == 0) goto L_0x03b4
                    okhttp3.ResponseBody r4 = r9.body()
                    r4.close()
                L_0x03b4:
                    if (r5 == 0) goto L_0x03b9
                    r5.cancel()
                L_0x03b9:
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r8 = "request is done "
                    r5.append(r8)
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r8 = r8.debugString(r9)
                    r5.append(r8)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r4, r5)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r4)
                    java.lang.String r5 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0424 }
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0424 }
                    r8.<init>()     // Catch:{ all -> 0x0424 }
                    java.lang.String r9 = "Not removing connections and listener "
                    r8.append(r9)     // Catch:{ all -> 0x0424 }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0424 }
                    com.vungle.warren.downloader.DownloadRequest r10 = r4     // Catch:{ all -> 0x0424 }
                    java.lang.String r9 = r9.debugString(r10)     // Catch:{ all -> 0x0424 }
                    r8.append(r9)     // Catch:{ all -> 0x0424 }
                    java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0424 }
                    android.util.Log.d(r5, r8)     // Catch:{ all -> 0x0424 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0424 }
                    java.util.concurrent.ConcurrentHashMap r5 = r5.connections     // Catch:{ all -> 0x0424 }
                    boolean r5 = r5.isEmpty()     // Catch:{ all -> 0x0424 }
                    if (r5 == 0) goto L_0x0417
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0424 }
                    com.vungle.warren.utility.NetworkProvider r5 = r5.networkProvider     // Catch:{ all -> 0x0424 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0424 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r8 = r8.networkListener     // Catch:{ all -> 0x0424 }
                    r5.removeListener(r8)     // Catch:{ all -> 0x0424 }
                L_0x0417:
                    monitor-exit(r4)     // Catch:{ all -> 0x0424 }
                    r11 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r11)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r11)
                    r8 = r3
                    r3 = r11
                    r5 = 0
                    goto L_0x0012
                L_0x0424:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r4)     // Catch:{ all -> 0x0424 }
                    throw r2
                L_0x0428:
                    r11 = 0
                    com.vungle.warren.downloader.AssetDownloader$RequestException r4 = new com.vungle.warren.downloader.AssetDownloader$RequestException     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    r8.<init>()     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    java.lang.String r12 = "Code: "
                    r8.append(r12)     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    r8.append(r10)     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    java.lang.String r8 = r8.toString()     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    r4.<init>(r8)     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                    throw r4     // Catch:{ Throwable -> 0x0440, all -> 0x047d }
                L_0x0440:
                    r0 = move-exception
                    goto L_0x0444
                L_0x0442:
                    r0 = move-exception
                    r11 = 0
                L_0x0444:
                    r4 = r3
                    r25 = r7
                    r7 = r11
                    r19 = r7
                    r8 = r13
                    r3 = r0
                    goto L_0x07f6
                L_0x044e:
                    r0 = move-exception
                    r11 = 0
                L_0x0450:
                    r3 = r0
                    r25 = r7
                    r19 = r11
                    r22 = r19
                    r8 = r13
                    goto L_0x09f5
                L_0x045a:
                    r0 = move-exception
                    r11 = 0
                L_0x045c:
                    r3 = r0
                    r25 = r7
                    r4 = r8
                    r7 = r11
                    r19 = r7
                    r8 = r13
                    goto L_0x07f6
                L_0x0466:
                    r11 = 0
                    boolean r12 = r9.isSuccessful()     // Catch:{ Throwable -> 0x077c, all -> 0x0789 }
                    if (r12 == 0) goto L_0x0752
                    r12 = 206(0xce, float:2.89E-43)
                    if (r10 == r12) goto L_0x0481
                    boolean r3 = r13.exists()     // Catch:{ Throwable -> 0x047f, all -> 0x047d }
                    if (r3 == 0) goto L_0x047a
                    r13.delete()     // Catch:{ Throwable -> 0x047f, all -> 0x047d }
                L_0x047a:
                    r3 = 0
                    goto L_0x0481
                L_0x047d:
                    r0 = move-exception
                    goto L_0x0450
                L_0x047f:
                    r0 = move-exception
                    goto L_0x045c
                L_0x0481:
                    r14.delete()     // Catch:{ Throwable -> 0x074a, all -> 0x0789 }
                    okhttp3.Headers r12 = r9.headers()     // Catch:{ Throwable -> 0x074a, all -> 0x0789 }
                    java.lang.String r11 = "Content-Encoding"
                    java.lang.String r11 = r12.get(r11)     // Catch:{ Throwable -> 0x074a, all -> 0x0789 }
                    if (r11 == 0) goto L_0x04b2
                    r23 = r8
                    java.lang.String r8 = "gzip"
                    boolean r8 = r8.equalsIgnoreCase(r11)     // Catch:{ Throwable -> 0x04ab, all -> 0x036a }
                    if (r8 != 0) goto L_0x04b4
                    java.lang.String r8 = "identity"
                    boolean r8 = r8.equalsIgnoreCase(r11)     // Catch:{ Throwable -> 0x04ab, all -> 0x036a }
                    if (r8 == 0) goto L_0x04a3
                    goto L_0x04b4
                L_0x04a3:
                    java.io.IOException r3 = new java.io.IOException     // Catch:{ Throwable -> 0x04ab, all -> 0x036a }
                    java.lang.String r4 = "Unknown Content-Encoding"
                    r3.<init>(r4)     // Catch:{ Throwable -> 0x04ab, all -> 0x036a }
                    throw r3     // Catch:{ Throwable -> 0x04ab, all -> 0x036a }
                L_0x04ab:
                    r0 = move-exception
                    r3 = r0
                    r25 = r7
                    r8 = r13
                    goto L_0x0784
                L_0x04b2:
                    r23 = r8
                L_0x04b4:
                    java.util.HashMap r8 = new java.util.HashMap     // Catch:{ Throwable -> 0x0746, all -> 0x0789 }
                    r8.<init>()     // Catch:{ Throwable -> 0x0746, all -> 0x0789 }
                    java.lang.String r11 = "ETag"
                    r24 = r10
                    java.lang.String r10 = "ETag"
                    java.lang.String r10 = r12.get(r10)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    r8.put(r11, r10)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    java.lang.String r10 = "Last-Modified"
                    java.lang.String r11 = "Last-Modified"
                    java.lang.String r11 = r12.get(r11)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    r8.put(r10, r11)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    java.lang.String r10 = "Accept-Ranges"
                    java.lang.String r11 = "Accept-Ranges"
                    java.lang.String r11 = r12.get(r11)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    r8.put(r10, r11)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    java.lang.String r10 = "Content-Encoding"
                    java.lang.String r11 = "Content-Encoding"
                    java.lang.String r11 = r12.get(r11)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    r8.put(r10, r11)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    java.lang.String r10 = r14.getPath()     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    com.vungle.warren.utility.FileUtility.writeMap(r10, r8)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    okhttp3.ResponseBody r8 = r8.decodeGzipIfNeeded(r9)     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    if (r8 == 0) goto L_0x072f
                    okio.BufferedSource r10 = r8.source()     // Catch:{ Throwable -> 0x073c, all -> 0x0789 }
                    java.lang.String r11 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x0721, all -> 0x0718 }
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0721, all -> 0x0718 }
                    r12.<init>()     // Catch:{ Throwable -> 0x0721, all -> 0x0718 }
                    java.lang.String r14 = "Start download from bytes: "
                    r12.append(r14)     // Catch:{ Throwable -> 0x0721, all -> 0x0718 }
                    r12.append(r3)     // Catch:{ Throwable -> 0x0721, all -> 0x0718 }
                    com.vungle.warren.downloader.AssetDownloader r14 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x0721, all -> 0x0718 }
                    r25 = r7
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    java.lang.String r7 = r14.debugString(r7)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    r12.append(r7)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    java.lang.String r7 = r12.toString()     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    android.util.Log.d(r11, r7)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    r7 = 0
                    long r15 = r15 + r3
                    java.lang.String r7 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    r11.<init>()     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    java.lang.String r12 = "final offset = "
                    r11.append(r12)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    r11.append(r3)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    java.lang.String r11 = r11.toString()     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    android.util.Log.d(r7, r11)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    r11 = 0
                    int r7 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
                    if (r7 != 0) goto L_0x0552
                    okio.Sink r7 = okio.Okio.sink(r13)     // Catch:{ Throwable -> 0x054b, all -> 0x0544 }
                    goto L_0x0556
                L_0x0544:
                    r0 = move-exception
                    r3 = r0
                    r19 = r10
                    r8 = r13
                    goto L_0x07e1
                L_0x054b:
                    r0 = move-exception
                    r3 = r0
                    r19 = r10
                    r8 = r13
                    goto L_0x0728
                L_0x0552:
                    okio.Sink r7 = okio.Okio.appendingSink(r13)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                L_0x0556:
                    okio.BufferedSink r7 = okio.Okio.buffer(r7)     // Catch:{ Throwable -> 0x0716, all -> 0x0714 }
                    r11 = 0
                    r2.status = r11     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    long r11 = r8.contentLength()     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    r2.sizeBytes = r11     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    r2.startBytes = r3     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    java.lang.String r11 = r11.id     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    r8.deliverProgress(r11, r2)     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    r8 = 0
                    r11 = 0
                L_0x0571:
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    r26 = r8
                    r8 = 1
                    boolean r14 = r14.is(r8)     // Catch:{ Throwable -> 0x0709, all -> 0x0700 }
                    if (r14 == 0) goto L_0x05f3
                    okio.Buffer r8 = r7.buffer()     // Catch:{ Throwable -> 0x05e5, all -> 0x05d9 }
                    r27 = r13
                    r13 = 2048(0x800, double:1.0118E-320)
                    long r13 = r10.read(r8, r13)     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    r17 = -1
                    int r8 = (r13 > r17 ? 1 : (r13 == r17 ? 0 : -1))
                    if (r8 == 0) goto L_0x05f5
                    r7.emit()     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    r8 = 0
                    long r11 = r11 + r13
                    long r13 = r3 + r11
                    r17 = 0
                    int r8 = (r15 > r17 ? 1 : (r15 == r17 ? 0 : -1))
                    if (r8 <= 0) goto L_0x05a2
                    r19 = 100
                    long r13 = r13 * r19
                    long r13 = r13 / r15
                    int r8 = (int) r13     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    goto L_0x05a4
                L_0x05a2:
                    r8 = r26
                L_0x05a4:
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    boolean r13 = r13.isConnected()     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    if (r13 == 0) goto L_0x05cd
                    int r13 = r2.progressPercent     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    if (r8 > r13) goto L_0x05b1
                    goto L_0x05ca
                L_0x05b1:
                    r2.progressPercent = r8     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    int r13 = r2.progressPercent     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    com.vungle.warren.downloader.AssetDownloader r14 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    int r14 = r14.progressStep     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    int r13 = r13 % r14
                    if (r13 != 0) goto L_0x05ca
                    r13 = 1
                    r2.status = r13     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    java.lang.String r14 = r14.id     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    r13.deliverProgress(r14, r2)     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                L_0x05ca:
                    r13 = r27
                    goto L_0x0571
                L_0x05cd:
                    java.io.IOException r3 = new java.io.IOException     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    java.lang.String r4 = "Request is not connected"
                    r3.<init>(r4)     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    throw r3     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                L_0x05d5:
                    r0 = move-exception
                    goto L_0x05dc
                L_0x05d7:
                    r0 = move-exception
                    goto L_0x05e8
                L_0x05d9:
                    r0 = move-exception
                    r27 = r13
                L_0x05dc:
                    r3 = r0
                    r22 = r7
                    r19 = r10
                    r8 = r27
                    goto L_0x09f5
                L_0x05e5:
                    r0 = move-exception
                    r27 = r13
                L_0x05e8:
                    r3 = r0
                    r19 = r10
                    r4 = r23
                    r10 = r24
                    r8 = r27
                    goto L_0x07f6
                L_0x05f3:
                    r27 = r13
                L_0x05f5:
                    r7.flush()     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    r4 = 1
                    boolean r3 = r3.is(r4)     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    if (r3 == 0) goto L_0x0608
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    r4 = 4
                    r3.set(r4)     // Catch:{ Throwable -> 0x05d7, all -> 0x05d5 }
                    goto L_0x0634
                L_0x0608:
                    r3 = 6
                    r2.status = r3     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    java.lang.String r4 = r4.id     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    r3.deliverProgress(r4, r2)     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    r4.<init>()     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    java.lang.String r8 = "State has changed, cancelling download "
                    r4.append(r8)     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    java.lang.String r8 = r8.debugString(r11)     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    r4.append(r8)     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                    android.util.Log.d(r3, r4)     // Catch:{ Throwable -> 0x06fc, all -> 0x06f8 }
                L_0x0634:
                    if (r9 == 0) goto L_0x0643
                    okhttp3.ResponseBody r3 = r9.body()
                    if (r3 == 0) goto L_0x0643
                    okhttp3.ResponseBody r3 = r9.body()
                    r3.close()
                L_0x0643:
                    if (r5 == 0) goto L_0x0648
                    r5.cancel()
                L_0x0648:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "request is done "
                    r4.append(r5)
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r8 = r4
                    java.lang.String r5 = r5.debugString(r8)
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    android.util.Log.d(r3, r4)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r3)
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x06f4 }
                    int r4 = r4.getStatus()     // Catch:{ all -> 0x06f4 }
                    switch(r4) {
                        case 2: goto L_0x06aa;
                        case 3: goto L_0x0687;
                        case 4: goto L_0x067d;
                        case 5: goto L_0x0675;
                        default: goto L_0x0674;
                    }     // Catch:{ all -> 0x06f4 }
                L_0x0674:
                    goto L_0x068e
                L_0x0675:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x06f4 }
                    r4.onError(r6, r5)     // Catch:{ all -> 0x06f4 }
                    goto L_0x06aa
                L_0x067d:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x06f4 }
                    r8 = r27
                    r4.onSuccess(r8, r5)     // Catch:{ all -> 0x06f4 }
                    goto L_0x06aa
                L_0x0687:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x06f4 }
                    r4.onCancelled(r5, r2)     // Catch:{ all -> 0x06f4 }
                L_0x068e:
                    if (r25 != 0) goto L_0x06aa
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    java.util.concurrent.ConcurrentHashMap r4 = r4.connections     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x06f4 }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x06f4 }
                    r4.remove(r5)     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    java.util.concurrent.ConcurrentHashMap r4 = r4.listeners     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x06f4 }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x06f4 }
                    r4.remove(r5)     // Catch:{ all -> 0x06f4 }
                L_0x06aa:
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06f4 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x06f4 }
                    r5.<init>()     // Catch:{ all -> 0x06f4 }
                    java.lang.String r8 = "Removing connections and listener "
                    r5.append(r8)     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x06f4 }
                    java.lang.String r8 = r8.debugString(r9)     // Catch:{ all -> 0x06f4 }
                    r5.append(r8)     // Catch:{ all -> 0x06f4 }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x06f4 }
                    android.util.Log.d(r4, r5)     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    java.util.concurrent.ConcurrentHashMap r4 = r4.connections     // Catch:{ all -> 0x06f4 }
                    boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x06f4 }
                    if (r4 == 0) goto L_0x06e5
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.utility.NetworkProvider r4 = r4.networkProvider     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r5 = r5.networkListener     // Catch:{ all -> 0x06f4 }
                    r4.removeListener(r5)     // Catch:{ all -> 0x06f4 }
                L_0x06e5:
                    monitor-exit(r3)     // Catch:{ all -> 0x06f4 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r7)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r10)
                    r8 = r23
                    r7 = r25
                    r5 = 1
                    r13 = 0
                    goto L_0x09eb
                L_0x06f4:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r3)     // Catch:{ all -> 0x06f4 }
                    throw r2
                L_0x06f8:
                    r0 = move-exception
                    r8 = r27
                    goto L_0x0702
                L_0x06fc:
                    r0 = move-exception
                    r8 = r27
                    goto L_0x070b
                L_0x0700:
                    r0 = move-exception
                    r8 = r13
                L_0x0702:
                    r3 = r0
                    r22 = r7
                    r19 = r10
                    goto L_0x09f5
                L_0x0709:
                    r0 = move-exception
                    r8 = r13
                L_0x070b:
                    r3 = r0
                    r19 = r10
                    r4 = r23
                    r10 = r24
                    goto L_0x07f6
                L_0x0714:
                    r0 = move-exception
                    goto L_0x071b
                L_0x0716:
                    r0 = move-exception
                    goto L_0x0724
                L_0x0718:
                    r0 = move-exception
                    r25 = r7
                L_0x071b:
                    r8 = r13
                    r3 = r0
                    r19 = r10
                    goto L_0x07e1
                L_0x0721:
                    r0 = move-exception
                    r25 = r7
                L_0x0724:
                    r8 = r13
                    r3 = r0
                    r19 = r10
                L_0x0728:
                    r4 = r23
                    r10 = r24
                    r7 = 0
                    goto L_0x07f6
                L_0x072f:
                    r25 = r7
                    r8 = r13
                    java.io.IOException r3 = new java.io.IOException     // Catch:{ Throwable -> 0x073a, all -> 0x0774 }
                    java.lang.String r4 = "Response body is null"
                    r3.<init>(r4)     // Catch:{ Throwable -> 0x073a, all -> 0x0774 }
                    throw r3     // Catch:{ Throwable -> 0x073a, all -> 0x0774 }
                L_0x073a:
                    r0 = move-exception
                    goto L_0x0740
                L_0x073c:
                    r0 = move-exception
                    r25 = r7
                    r8 = r13
                L_0x0740:
                    r3 = r0
                    r4 = r23
                    r10 = r24
                    goto L_0x0786
                L_0x0746:
                    r0 = move-exception
                    r25 = r7
                    goto L_0x074f
                L_0x074a:
                    r0 = move-exception
                    r25 = r7
                    r23 = r8
                L_0x074f:
                    r24 = r10
                    goto L_0x0782
                L_0x0752:
                    r25 = r7
                    r23 = r8
                    r24 = r10
                    r8 = r13
                    com.vungle.warren.downloader.AssetDownloader$RequestException r3 = new com.vungle.warren.downloader.AssetDownloader$RequestException     // Catch:{ Throwable -> 0x0776, all -> 0x0774 }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0776, all -> 0x0774 }
                    r4.<init>()     // Catch:{ Throwable -> 0x0776, all -> 0x0774 }
                    java.lang.String r7 = "Code: "
                    r4.append(r7)     // Catch:{ Throwable -> 0x0776, all -> 0x0774 }
                    r7 = r24
                    r4.append(r7)     // Catch:{ Throwable -> 0x0772, all -> 0x0774 }
                    java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0772, all -> 0x0774 }
                    r3.<init>(r4)     // Catch:{ Throwable -> 0x0772, all -> 0x0774 }
                    throw r3     // Catch:{ Throwable -> 0x0772, all -> 0x0774 }
                L_0x0772:
                    r0 = move-exception
                    goto L_0x0779
                L_0x0774:
                    r0 = move-exception
                    goto L_0x078d
                L_0x0776:
                    r0 = move-exception
                    r7 = r24
                L_0x0779:
                    r3 = r0
                    r10 = r7
                    goto L_0x0784
                L_0x077c:
                    r0 = move-exception
                    r25 = r7
                    r23 = r8
                    r7 = r10
                L_0x0782:
                    r8 = r13
                    r3 = r0
                L_0x0784:
                    r4 = r23
                L_0x0786:
                    r7 = 0
                    goto L_0x07f4
                L_0x0789:
                    r0 = move-exception
                    r25 = r7
                    r8 = r13
                L_0x078d:
                    r3 = r0
                    goto L_0x07df
                L_0x0790:
                    r0 = move-exception
                    r25 = r7
                    r23 = r8
                    r8 = r13
                    r3 = r0
                    r4 = r23
                    r7 = 0
                    goto L_0x07f3
                L_0x079c:
                    r0 = move-exception
                    r25 = r7
                    r8 = r13
                    r3 = r0
                    goto L_0x07de
                L_0x07a2:
                    r0 = move-exception
                    r25 = r7
                    r23 = r8
                    r8 = r13
                    r3 = r0
                    r4 = r23
                    goto L_0x07bd
                L_0x07ac:
                    r0 = move-exception
                    r25 = r7
                    r8 = r13
                    r3 = r0
                L_0x07b1:
                    r5 = 0
                    goto L_0x07de
                L_0x07b3:
                    r0 = move-exception
                    r25 = r7
                    r23 = r8
                    r8 = r13
                    r3 = r0
                    r4 = r23
                L_0x07bc:
                    r5 = 0
                L_0x07bd:
                    r7 = 0
                    goto L_0x07f2
                L_0x07bf:
                    r25 = r7
                    r23 = r8
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ Throwable -> 0x07d6, all -> 0x07d4 }
                    java.lang.String r4 = "Request is not connected to reuired network"
                    android.util.Log.d(r3, r4)     // Catch:{ Throwable -> 0x07d6, all -> 0x07d4 }
                    java.io.IOException r3 = new java.io.IOException     // Catch:{ Throwable -> 0x07d6, all -> 0x07d4 }
                    java.lang.String r4 = "Not connected to correct network"
                    r3.<init>(r4)     // Catch:{ Throwable -> 0x07d6, all -> 0x07d4 }
                    throw r3     // Catch:{ Throwable -> 0x07d6, all -> 0x07d4 }
                L_0x07d4:
                    r0 = move-exception
                    goto L_0x07db
                L_0x07d6:
                    r0 = move-exception
                    goto L_0x07ec
                L_0x07d8:
                    r0 = move-exception
                    r25 = r7
                L_0x07db:
                    r3 = r0
                    r5 = 0
                    r8 = 0
                L_0x07de:
                    r9 = 0
                L_0x07df:
                    r19 = 0
                L_0x07e1:
                    r21 = 1
                    r22 = 0
                    goto L_0x09f7
                L_0x07e7:
                    r0 = move-exception
                    r25 = r7
                    r23 = r8
                L_0x07ec:
                    r3 = r0
                    r4 = r23
                    r5 = 0
                    r7 = 0
                    r8 = 0
                L_0x07f2:
                    r9 = 0
                L_0x07f3:
                    r10 = -1
                L_0x07f4:
                    r19 = 0
                L_0x07f6:
                    java.lang.String r11 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x09f0 }
                    java.lang.String r12 = "Exception on download"
                    android.util.Log.e(r11, r12, r3)     // Catch:{ all -> 0x09f0 }
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ all -> 0x09f0 }
                    r12 = 5
                    r11.set(r12)     // Catch:{ all -> 0x09f0 }
                    boolean r11 = r3 instanceof java.io.IOException     // Catch:{ all -> 0x09f0 }
                    if (r11 == 0) goto L_0x08f7
                    com.vungle.warren.downloader.AssetDownloader r11 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ all -> 0x08f4 }
                    boolean r11 = r11.isConnected(r13)     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ all -> 0x08f4 }
                    r13.setConnected(r11)     // Catch:{ all -> 0x08f4 }
                    r13 = 3
                    if (r11 != 0) goto L_0x08a1
                    r2.status = r12     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.AssetDownloader r14 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.DownloadRequest r15 = r4     // Catch:{ all -> 0x08f4 }
                    java.lang.String r15 = r15.id     // Catch:{ all -> 0x08f4 }
                    r14.deliverProgress(r15, r2)     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x08f4 }
                    boolean r14 = r14.is(r13)     // Catch:{ all -> 0x08f4 }
                    if (r14 != 0) goto L_0x08a1
                    int r14 = r4 + 1
                    com.vungle.warren.downloader.AssetDownloader r15 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f4 }
                    int r15 = r15.maxReconnectAttempts     // Catch:{ all -> 0x08f4 }
                    if (r4 >= r15) goto L_0x089d
                    r4 = 0
                L_0x0835:
                    com.vungle.warren.downloader.AssetDownloader r15 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f4 }
                    int r15 = r15.retryCountOnConnectionLost     // Catch:{ all -> 0x08f4 }
                    if (r4 >= r15) goto L_0x089d
                    com.vungle.warren.downloader.AssetDownloader r15 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ InterruptedException -> 0x084d }
                    int r15 = r15.reconnectTimeout     // Catch:{ InterruptedException -> 0x084d }
                    r12 = 0
                    int r15 = java.lang.Math.max(r12, r15)     // Catch:{ InterruptedException -> 0x084d }
                    r28 = r14
                    long r13 = (long) r15
                    java.lang.Thread.sleep(r13)     // Catch:{ InterruptedException -> 0x084b }
                    goto L_0x0854
                L_0x084b:
                    r0 = move-exception
                    goto L_0x0850
                L_0x084d:
                    r0 = move-exception
                    r28 = r14
                L_0x0850:
                    r12 = r0
                    r12.printStackTrace()     // Catch:{ all -> 0x08f4 }
                L_0x0854:
                    com.vungle.warren.downloader.DownloadRequest r12 = r4     // Catch:{ all -> 0x08f4 }
                    r13 = 3
                    boolean r12 = r12.is(r13)     // Catch:{ all -> 0x08f4 }
                    if (r12 == 0) goto L_0x085e
                    goto L_0x089f
                L_0x085e:
                    java.lang.String r12 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x08f4 }
                    java.lang.String r13 = "Trying to reconnect"
                    android.util.Log.d(r12, r13)     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.AssetDownloader r12 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ all -> 0x08f4 }
                    boolean r12 = r12.isConnected(r13)     // Catch:{ all -> 0x08f4 }
                    if (r12 == 0) goto L_0x0890
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x08f4 }
                    java.lang.String r12 = "Reconnected, starting download again"
                    android.util.Log.d(r4, r12)     // Catch:{ all -> 0x08f4 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0888 }
                    r12 = 1
                    r4.setConnected(r12)     // Catch:{ all -> 0x0888 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0888 }
                    r4.set(r12)     // Catch:{ all -> 0x0888 }
                    r4 = 0
                    r13 = 0
                    goto L_0x08a5
                L_0x0888:
                    r0 = move-exception
                    r3 = r0
                    r22 = r7
                    r21 = 0
                    goto L_0x09f7
                L_0x0890:
                    com.vungle.warren.downloader.DownloadRequest r12 = r4     // Catch:{ all -> 0x08f4 }
                    r13 = 0
                    r12.setConnected(r13)     // Catch:{ all -> 0x08f4 }
                    int r4 = r4 + 1
                    r14 = r28
                    r12 = 5
                    r13 = 3
                    goto L_0x0835
                L_0x089d:
                    r28 = r14
                L_0x089f:
                    r13 = 0
                    goto L_0x08a4
                L_0x08a1:
                    r13 = 0
                    r28 = r4
                L_0x08a4:
                    r4 = 1
                L_0x08a5:
                    if (r4 == 0) goto L_0x08ef
                    com.vungle.warren.downloader.DownloadRequest r12 = r4     // Catch:{ all -> 0x08e7 }
                    r14 = 3
                    boolean r12 = r12.is(r14)     // Catch:{ all -> 0x08e7 }
                    if (r12 != 0) goto L_0x08ef
                    com.vungle.warren.downloader.AssetDownloader r12 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08e7 }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x08e7 }
                    boolean r12 = r12.shouldPause(r14)     // Catch:{ all -> 0x08e7 }
                    if (r12 == 0) goto L_0x08d4
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x08ca }
                    r10 = 2
                    r3.set(r10)     // Catch:{ all -> 0x08ca }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08ca }
                    com.vungle.warren.downloader.DownloadRequest r10 = r4     // Catch:{ all -> 0x08ca }
                    r3.onPaused(r2, r10)     // Catch:{ all -> 0x08ca }
                    r25 = 1
                    goto L_0x08ef
                L_0x08ca:
                    r0 = move-exception
                    r3 = r0
                    r21 = r4
                    r22 = r7
                    r25 = 1
                    goto L_0x09f7
                L_0x08d4:
                    com.vungle.warren.downloader.DownloadRequest r12 = r4     // Catch:{ all -> 0x08e7 }
                    r14 = 5
                    r12.set(r14)     // Catch:{ all -> 0x08e7 }
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r12 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x08e7 }
                    com.vungle.warren.downloader.AssetDownloader r14 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x08e7 }
                    int r11 = r14.mapExceptionToReason(r3, r11)     // Catch:{ all -> 0x08e7 }
                    r12.<init>(r10, r3, r11)     // Catch:{ all -> 0x08e7 }
                    r6 = r12
                    goto L_0x08ef
                L_0x08e7:
                    r0 = move-exception
                    r3 = r0
                    r21 = r4
                    r22 = r7
                    goto L_0x09f7
                L_0x08ef:
                    r12 = r4
                    r11 = r6
                    r4 = r28
                    goto L_0x090a
                L_0x08f4:
                    r0 = move-exception
                    goto L_0x09f2
                L_0x08f7:
                    r13 = 0
                    boolean r11 = r3 instanceof com.vungle.warren.downloader.AssetDownloader.RequestException     // Catch:{ all -> 0x09f0 }
                    if (r11 == 0) goto L_0x0903
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r11 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x09f0 }
                    r12 = 1
                    r11.<init>(r10, r3, r12)     // Catch:{ all -> 0x08f4 }
                    goto L_0x090a
                L_0x0903:
                    r12 = 1
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r11 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x08f4 }
                    r14 = 4
                    r11.<init>(r10, r3, r14)     // Catch:{ all -> 0x08f4 }
                L_0x090a:
                    if (r9 == 0) goto L_0x0919
                    okhttp3.ResponseBody r3 = r9.body()
                    if (r3 == 0) goto L_0x0919
                    okhttp3.ResponseBody r3 = r9.body()
                    r3.close()
                L_0x0919:
                    if (r5 == 0) goto L_0x091e
                    r5.cancel()
                L_0x091e:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "request is done "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r6 = r6.debugString(r9)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r3, r5)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r3)
                    if (r12 == 0) goto L_0x09a4
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x09a1 }
                    int r5 = r5.getStatus()     // Catch:{ all -> 0x09a1 }
                    switch(r5) {
                        case 2: goto L_0x0980;
                        case 3: goto L_0x095d;
                        case 4: goto L_0x0955;
                        case 5: goto L_0x094d;
                        default: goto L_0x094c;
                    }     // Catch:{ all -> 0x09a1 }
                L_0x094c:
                    goto L_0x0964
                L_0x094d:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x09a1 }
                    r5.onError(r11, r6)     // Catch:{ all -> 0x09a1 }
                    goto L_0x0980
                L_0x0955:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x09a1 }
                    r5.onSuccess(r8, r6)     // Catch:{ all -> 0x09a1 }
                    goto L_0x0980
                L_0x095d:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x09a1 }
                    r5.onCancelled(r6, r2)     // Catch:{ all -> 0x09a1 }
                L_0x0964:
                    if (r25 != 0) goto L_0x0980
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    java.util.concurrent.ConcurrentHashMap r5 = r5.connections     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x09a1 }
                    java.lang.String r6 = r6.id     // Catch:{ all -> 0x09a1 }
                    r5.remove(r6)     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    java.util.concurrent.ConcurrentHashMap r5 = r5.listeners     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x09a1 }
                    java.lang.String r6 = r6.id     // Catch:{ all -> 0x09a1 }
                    r5.remove(r6)     // Catch:{ all -> 0x09a1 }
                L_0x0980:
                    java.lang.String r5 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x09a1 }
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x09a1 }
                    r6.<init>()     // Catch:{ all -> 0x09a1 }
                    java.lang.String r8 = "Removing connections and listener "
                    r6.append(r8)     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x09a1 }
                    java.lang.String r8 = r8.debugString(r9)     // Catch:{ all -> 0x09a1 }
                    r6.append(r8)     // Catch:{ all -> 0x09a1 }
                    java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x09a1 }
                    android.util.Log.d(r5, r6)     // Catch:{ all -> 0x09a1 }
                    goto L_0x09c4
                L_0x09a1:
                    r0 = move-exception
                    r2 = r0
                    goto L_0x09ee
                L_0x09a4:
                    java.lang.String r5 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x09a1 }
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x09a1 }
                    r6.<init>()     // Catch:{ all -> 0x09a1 }
                    java.lang.String r8 = "Not removing connections and listener "
                    r6.append(r8)     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x09a1 }
                    java.lang.String r8 = r8.debugString(r9)     // Catch:{ all -> 0x09a1 }
                    r6.append(r8)     // Catch:{ all -> 0x09a1 }
                    java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x09a1 }
                    android.util.Log.d(r5, r6)     // Catch:{ all -> 0x09a1 }
                L_0x09c4:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    java.util.concurrent.ConcurrentHashMap r5 = r5.connections     // Catch:{ all -> 0x09a1 }
                    boolean r5 = r5.isEmpty()     // Catch:{ all -> 0x09a1 }
                    if (r5 == 0) goto L_0x09df
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.utility.NetworkProvider r5 = r5.networkProvider     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r6 = r6.networkListener     // Catch:{ all -> 0x09a1 }
                    r5.removeListener(r6)     // Catch:{ all -> 0x09a1 }
                L_0x09df:
                    monitor-exit(r3)     // Catch:{ all -> 0x09a1 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r7)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r19)
                    r8 = r4
                    r6 = r11
                    r5 = r12
                    r7 = r25
                L_0x09eb:
                    r3 = 0
                    goto L_0x0012
                L_0x09ee:
                    monitor-exit(r3)     // Catch:{ all -> 0x09a1 }
                    throw r2
                L_0x09f0:
                    r0 = move-exception
                    r12 = 1
                L_0x09f2:
                    r3 = r0
                    r22 = r7
                L_0x09f5:
                    r21 = 1
                L_0x09f7:
                    if (r9 == 0) goto L_0x0a06
                    okhttp3.ResponseBody r4 = r9.body()
                    if (r4 == 0) goto L_0x0a06
                    okhttp3.ResponseBody r4 = r9.body()
                    r4.close()
                L_0x0a06:
                    if (r5 == 0) goto L_0x0a0b
                    r5.cancel()
                L_0x0a0b:
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r7 = "request is done "
                    r5.append(r7)
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r7 = r7.debugString(r9)
                    r5.append(r7)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r4, r5)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r4)
                    if (r21 == 0) goto L_0x0a91
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a8e }
                    int r5 = r5.getStatus()     // Catch:{ all -> 0x0a8e }
                    switch(r5) {
                        case 2: goto L_0x0a6d;
                        case 3: goto L_0x0a4a;
                        case 4: goto L_0x0a42;
                        case 5: goto L_0x0a3a;
                        default: goto L_0x0a39;
                    }     // Catch:{ all -> 0x0a8e }
                L_0x0a39:
                    goto L_0x0a51
                L_0x0a3a:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a8e }
                    r2.onError(r6, r5)     // Catch:{ all -> 0x0a8e }
                    goto L_0x0a6d
                L_0x0a42:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a8e }
                    r2.onSuccess(r8, r5)     // Catch:{ all -> 0x0a8e }
                    goto L_0x0a6d
                L_0x0a4a:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x0a8e }
                    r5.onCancelled(r6, r2)     // Catch:{ all -> 0x0a8e }
                L_0x0a51:
                    if (r25 != 0) goto L_0x0a6d
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a8e }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x0a8e }
                    r2.remove(r5)     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.listeners     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a8e }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x0a8e }
                    r2.remove(r5)     // Catch:{ all -> 0x0a8e }
                L_0x0a6d:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0a8e }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0a8e }
                    r5.<init>()     // Catch:{ all -> 0x0a8e }
                    java.lang.String r6 = "Removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0a8e }
                    java.lang.String r6 = r6.debugString(r7)     // Catch:{ all -> 0x0a8e }
                    r5.append(r6)     // Catch:{ all -> 0x0a8e }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0a8e }
                    android.util.Log.d(r2, r5)     // Catch:{ all -> 0x0a8e }
                    goto L_0x0ab1
                L_0x0a8e:
                    r0 = move-exception
                    r2 = r0
                    goto L_0x0ad4
                L_0x0a91:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0a8e }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0a8e }
                    r5.<init>()     // Catch:{ all -> 0x0a8e }
                    java.lang.String r6 = "Not removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0a8e }
                    java.lang.String r6 = r6.debugString(r7)     // Catch:{ all -> 0x0a8e }
                    r5.append(r6)     // Catch:{ all -> 0x0a8e }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0a8e }
                    android.util.Log.d(r2, r5)     // Catch:{ all -> 0x0a8e }
                L_0x0ab1:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0a8e }
                    boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0a8e }
                    if (r2 == 0) goto L_0x0acc
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.utility.NetworkProvider r2 = r2.networkProvider     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r5 = r5.networkListener     // Catch:{ all -> 0x0a8e }
                    r2.removeListener(r5)     // Catch:{ all -> 0x0a8e }
                L_0x0acc:
                    monitor-exit(r4)     // Catch:{ all -> 0x0a8e }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r22)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r19)
                    throw r3
                L_0x0ad4:
                    monitor-exit(r4)     // Catch:{ all -> 0x0a8e }
                    throw r2
                L_0x0ad6:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.AnonymousClass4.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public ResponseBody decodeGzipIfNeeded(Response response) {
        if (!"gzip".equalsIgnoreCase(response.header("Content-Encoding")) || !HttpHeaders.hasBody(response)) {
            return response.body();
        }
        return new RealResponseBody(response.header("Content-Type"), -1, Okio.buffer(new GzipSource(response.body().source())));
    }

    /* access modifiers changed from: private */
    public void onCancelled(@NonNull final DownloadRequest downloadRequest, @Nullable final AssetDownloadListener.Progress progress) {
        this.connections.remove(downloadRequest.id);
        final AssetDownloadListener remove = this.listeners.remove(downloadRequest.id);
        if (progress == null) {
            progress = new AssetDownloadListener.Progress();
        }
        progress.status = 3;
        if (remove != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    remove.onProgress(progress, downloadRequest);
                }
            });
        }
        String str = TAG;
        Log.d(str, "Cancelled " + debugString(downloadRequest));
    }

    /* access modifiers changed from: private */
    public int mapExceptionToReason(Throwable th, boolean z) {
        if (th instanceof RuntimeException) {
            return 4;
        }
        if (!z || (th instanceof SocketException) || (th instanceof SocketTimeoutException)) {
            return 0;
        }
        return ((th instanceof UnknownHostException) || (th instanceof SSLException)) ? 1 : 2;
    }

    /* access modifiers changed from: private */
    public long getContentLength(Response response) {
        if (response == null) {
            return -1;
        }
        String str = response.headers().get(HttpRequest.HEADER_CONTENT_LENGTH);
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (Throwable unused) {
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public boolean fullyDownloadedContent(File file, Response response, DownloadRequest downloadRequest) {
        boolean z = false;
        if (!file.exists() || file.length() <= 0 || "gzip".equalsIgnoreCase(response.headers().get("Content-Encoding"))) {
            return false;
        }
        int code = response.code();
        long contentLength = getContentLength(response);
        if (code == 200 && contentLength == file.length()) {
            String str = TAG;
            Log.d(str, "200 code, data size matches file size " + debugString(downloadRequest));
            return responseVersionMatches(file, response);
        } else if (code != RANGE_NOT_SATISFIABLE) {
            return false;
        } else {
            String str2 = response.headers().get(CONTENT_RANGE);
            if (TextUtils.isEmpty(str2)) {
                return false;
            }
            RangeResponse rangeResponse = new RangeResponse(str2);
            if (BYTES.equalsIgnoreCase(rangeResponse.dimension) && rangeResponse.total > 0 && rangeResponse.total == file.length() && responseVersionMatches(file, response)) {
                z = true;
            }
            String str3 = TAG;
            Log.d(str3, "416 code, data size matches file size " + debugString(downloadRequest));
            return z;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onSuccess(final File file, @NonNull final DownloadRequest downloadRequest) {
        this.connections.remove(downloadRequest.id);
        final AssetDownloadListener remove = this.listeners.remove(downloadRequest.id);
        String str = TAG;
        Log.d(str, "OnComplete - Removing connections and listener " + downloadRequest.id);
        if (remove != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    remove.onSuccess(file, downloadRequest);
                }
            });
        }
        String str2 = TAG;
        Log.d(str2, "Finished " + debugString(downloadRequest));
    }

    /* access modifiers changed from: private */
    public synchronized void onPaused(AssetDownloadListener.Progress progress, DownloadRequest downloadRequest) {
        String str = TAG;
        Log.d(str, "Pausing download " + debugString(downloadRequest));
        progress.status = 2;
        deliverProgress(downloadRequest.id, progress);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0065, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onError(com.vungle.warren.downloader.AssetDownloadListener.DownloadError r6, @android.support.annotation.Nullable final com.vungle.warren.downloader.DownloadRequest r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            if (r7 != 0) goto L_0x0005
            monitor-exit(r5)
            return
        L_0x0005:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.AssetDownloadListener> r0 = r5.listeners     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0066 }
            java.lang.Object r0 = r0.remove(r1)     // Catch:{ all -> 0x0066 }
            com.vungle.warren.downloader.AssetDownloadListener r0 = (com.vungle.warren.downloader.AssetDownloadListener) r0     // Catch:{ all -> 0x0066 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r1 = r5.connections     // Catch:{ all -> 0x0066 }
            java.lang.String r2 = r7.id     // Catch:{ all -> 0x0066 }
            r1.remove(r2)     // Catch:{ all -> 0x0066 }
            r1 = 5
            r7.set(r1)     // Catch:{ all -> 0x0066 }
            if (r6 != 0) goto L_0x0029
            com.vungle.warren.downloader.AssetDownloadListener$DownloadError r1 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x0066 }
            r2 = -1
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            r4 = 4
            r1.<init>(r2, r3, r4)     // Catch:{ all -> 0x0066 }
            goto L_0x002a
        L_0x0029:
            r1 = r6
        L_0x002a:
            java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = "OnError - Removing connections and listener "
            r3.append(r4)     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = r7.id     // Catch:{ all -> 0x0066 }
            r3.append(r4)     // Catch:{ all -> 0x0066 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0066 }
            android.util.Log.d(r2, r3)     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0064
            java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = "On download error "
            r3.append(r4)     // Catch:{ all -> 0x0066 }
            r3.append(r6)     // Catch:{ all -> 0x0066 }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x0066 }
            android.util.Log.e(r2, r6)     // Catch:{ all -> 0x0066 }
            java.util.concurrent.ExecutorService r6 = r5.uiExecutor     // Catch:{ all -> 0x0066 }
            com.vungle.warren.downloader.AssetDownloader$7 r2 = new com.vungle.warren.downloader.AssetDownloader$7     // Catch:{ all -> 0x0066 }
            r2.<init>(r0, r1, r7)     // Catch:{ all -> 0x0066 }
            r6.execute(r2)     // Catch:{ all -> 0x0066 }
        L_0x0064:
            monitor-exit(r5)
            return
        L_0x0066:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.onError(com.vungle.warren.downloader.AssetDownloadListener$DownloadError, com.vungle.warren.downloader.DownloadRequest):void");
    }

    /* access modifiers changed from: private */
    public boolean satisfiesPartialDownload(Response response, long j, DownloadRequest downloadRequest) {
        RangeResponse rangeResponse = new RangeResponse(response.headers().get(CONTENT_RANGE));
        boolean z = response.code() == 206 && BYTES.equalsIgnoreCase(rangeResponse.dimension) && rangeResponse.rangeStart >= 0 && j == rangeResponse.rangeStart;
        String str = TAG;
        Log.d(str, "satisfies partial download: " + z + " " + debugString(downloadRequest));
        return z;
    }

    /* access modifiers changed from: private */
    public String debugString(DownloadRequest downloadRequest) {
        return " id - " + downloadRequest.id + ", url - " + downloadRequest.url + ", path - " + downloadRequest.path + ", th - " + Thread.currentThread().getName();
    }

    /* access modifiers changed from: private */
    public boolean shouldPause(DownloadRequest downloadRequest) {
        if (!downloadRequest.pauseOnConnectionLost) {
            return false;
        }
        return !isConnected(downloadRequest);
    }

    /* access modifiers changed from: private */
    @TargetApi(21)
    public boolean isConnected(DownloadRequest downloadRequest) {
        int i;
        int currentNetworkType = this.networkProvider.getCurrentNetworkType();
        boolean z = true;
        if (currentNetworkType >= 0 && downloadRequest.networkType == 3) {
            return true;
        }
        switch (currentNetworkType) {
            case 0:
            case 4:
            case 7:
            case 17:
                i = 1;
                break;
            case 1:
            case 6:
            case 9:
                i = 2;
                break;
            default:
                i = -1;
                break;
        }
        if (i <= 0 || (downloadRequest.networkType & i) != i) {
            z = false;
        }
        String str = TAG;
        Log.d(str, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
        return z;
    }

    /* access modifiers changed from: private */
    public void deliverProgress(String str, AssetDownloadListener.Progress progress) {
        final AssetDownloadListener.Progress copy = AssetDownloadListener.Progress.copy(progress);
        String str2 = TAG;
        Log.d(str2, "Progress " + progress.progressPercent + " status " + progress.status + " " + str);
        final AssetDownloadListener assetDownloadListener = this.listeners.get(str);
        final DownloadRequest downloadRequest = this.connections.get(str);
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onProgress(copy, downloadRequest);
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void cancel(@android.support.annotation.Nullable com.vungle.warren.downloader.DownloadRequest r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            if (r4 != 0) goto L_0x0005
            monitor-exit(r3)
            return
        L_0x0005:
            r0 = 3
            int r1 = r4.getAndSetStatus(r0)     // Catch:{ all -> 0x0024 }
            r2 = 1
            if (r1 == r2) goto L_0x0022
            if (r1 == r0) goto L_0x0022
            r0 = 0
            r3.onCancelled(r4, r0)     // Catch:{ all -> 0x0024 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r4 = r3.connections     // Catch:{ all -> 0x0024 }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x0024 }
            if (r4 == 0) goto L_0x0022
            com.vungle.warren.utility.NetworkProvider r4 = r3.networkProvider     // Catch:{ all -> 0x0024 }
            com.vungle.warren.utility.NetworkProvider$NetworkListener r0 = r3.networkListener     // Catch:{ all -> 0x0024 }
            r4.removeListener(r0)     // Catch:{ all -> 0x0024 }
        L_0x0022:
            monitor-exit(r3)
            return
        L_0x0024:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.cancel(com.vungle.warren.downloader.DownloadRequest):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public boolean cancelAndAwait(@Nullable DownloadRequest downloadRequest, long j) {
        Integer num;
        if (downloadRequest == null) {
            return true;
        }
        cancel(downloadRequest);
        String str = downloadRequest.id;
        long currentTimeMillis = System.currentTimeMillis() + Math.max(0L, j);
        while (System.currentTimeMillis() < currentTimeMillis) {
            DownloadRequest downloadRequest2 = this.connections.get(str);
            if (downloadRequest2 == null || downloadRequest2.getStatus() != 3) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Request is not present or status changed - finish await ");
                if (downloadRequest2 == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(downloadRequest2.getStatus());
                }
                sb.append(num);
                Log.d(str2, sb.toString());
                return true;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public synchronized void cancelAll() {
        for (DownloadRequest cancel : this.connections.values()) {
            cancel(cancel);
        }
    }

    public void setProgressStep(int i) {
        if (i != 0) {
            this.progressStep = i;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onNetworkChanged(int i) {
        String str = TAG;
        Log.d(str, "Num of connections: " + this.connections.values().size());
        for (DownloadRequest next : this.connections.values()) {
            if (next.is(3)) {
                Log.d(TAG, "Result cancelled");
            } else {
                boolean isConnected = isConnected(next);
                String str2 = TAG;
                Log.d(str2, "Connected = " + isConnected + " for " + i);
                next.setConnected(isConnected);
                if (next.pauseOnConnectionLost && isConnected && next.is(2)) {
                    load(next);
                    String str3 = TAG;
                    Log.d(str3, "resumed " + next.id);
                }
            }
        }
    }

    private boolean responseVersionMatches(File file, Response response) {
        Map<String, String> readMap = FileUtility.readMap(new File(file.getPath() + META_POSTFIX_EXT).getPath());
        Headers headers = response.headers();
        String str = headers.get("ETag");
        String str2 = headers.get("Last-Modified");
        String str3 = TAG;
        Log.d(str3, "server etag: " + str);
        String str4 = TAG;
        Log.d(str4, "server lastModified: " + str2);
        if (str != null && !str.equals(readMap.get("ETag"))) {
            String str5 = TAG;
            Log.d(str5, "etags miss match current: " + readMap.get("ETag"));
            return false;
        } else if (str2 == null || str2.equals(readMap.get("Last-Modified"))) {
            return true;
        } else {
            String str6 = TAG;
            Log.d(str6, "lastModified miss match current: " + readMap.get("Last-Modified"));
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void shutdown() {
        cancel(null);
        this.listeners.clear();
        this.connections.clear();
        this.uiExecutor.shutdownNow();
        this.downloadExecutor.shutdownNow();
        try {
            this.downloadExecutor.awaitTermination(2, TimeUnit.SECONDS);
            this.uiExecutor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return;
    }

    private static abstract class DownloadPriorityRunnable extends PriorityRunnable {
        private final int priority;

        private DownloadPriorityRunnable(int i) {
            this.priority = i;
        }

        public Integer getPriority() {
            return Integer.valueOf(this.priority);
        }
    }

    private static class RequestException extends Exception {
        RequestException(String str) {
            super(str);
        }
    }
}
