package com.vungle.warren.ui.view;

import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.gson.JsonObject;
import com.helpshift.support.constants.FaqsColumns;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.SDKDownloadClient;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.ui.view.WebViewAPI;
import im.getsocial.sdk.invites.InviteChannelIds;
import java.util.Locale;

public class VungleWebClient extends WebViewClient implements WebViewAPI {
    public static final String TAG = "VungleWebClient";
    private WebViewAPI.MRAIDDelegate MRAIDDelegate;
    private Advertisement advertisement;
    private boolean collectConsent;
    private DirectDownloadAdapter directDownloadAdapter;
    private WebViewAPI.WebClientErrorListener errorListener;
    private String gdprAccept;
    private String gdprBody;
    private String gdprDeny;
    private String gdprTitle;
    private Boolean isViewable;
    private WebView loadedWebView;
    private Placement placement;
    private boolean ready = false;

    public VungleWebClient(Advertisement advertisement2, Placement placement2) {
        this.advertisement = advertisement2;
        this.placement = placement2;
    }

    public void setConsentStatus(boolean z, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4) {
        this.collectConsent = z;
        this.gdprTitle = str;
        this.gdprBody = str2;
        this.gdprAccept = str3;
        this.gdprDeny = str4;
    }

    public void setMRAIDDelegate(WebViewAPI.MRAIDDelegate mRAIDDelegate) {
        this.MRAIDDelegate = mRAIDDelegate;
    }

    public boolean shouldOverrideUrlLoading(final WebView webView, String str) {
        String str2 = TAG;
        Log.d(str2, "MRAID Command " + str);
        if (TextUtils.isEmpty(str)) {
            Log.e(TAG, "Invalid URL ");
            return false;
        }
        Uri parse = Uri.parse(str);
        if (parse.getScheme() == null || !parse.getScheme().equals(CampaignEx.JSON_KEY_MRAID)) {
            return false;
        }
        String host = parse.getHost();
        if (host.equals("propertiesChangeCompleted") && !this.ready) {
            final JsonObject createMRAIDArgs = this.advertisement.createMRAIDArgs();
            if (this.directDownloadAdapter != null) {
                this.directDownloadAdapter.getSdkDownloadClient().setInstallStatusCheck(new SDKDownloadClient.InstallStatusCheck() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
                     arg types: [java.lang.String, int]
                     candidates:
                      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
                      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
                      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
                      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void */
                    public void isAppInstalled(boolean z, boolean z2) {
                        createMRAIDArgs.addProperty("isDirectDownload", (Boolean) true);
                        createMRAIDArgs.addProperty("isDisplayIAP", Boolean.valueOf(z2));
                        createMRAIDArgs.addProperty("isInstalled", Boolean.valueOf(z));
                        createMRAIDArgs.addProperty("locale", Locale.getDefault().toString());
                        createMRAIDArgs.addProperty(FaqsColumns.LANGUAGE, Locale.getDefault().getLanguage());
                        WebView webView = webView;
                        webView.loadUrl("javascript:window.vungle.mraidBridge.notifyReadyEvent(" + createMRAIDArgs + ")");
                    }
                });
                this.directDownloadAdapter.getSdkDownloadClient().installStatusRequest();
            } else {
                webView.loadUrl("javascript:window.vungle.mraidBridge.notifyReadyEvent(" + createMRAIDArgs + ")");
            }
            this.ready = true;
        } else if (this.MRAIDDelegate != null) {
            JsonObject jsonObject = new JsonObject();
            for (String next : parse.getQueryParameterNames()) {
                jsonObject.addProperty(next, parse.getQueryParameter(next));
            }
            if (this.MRAIDDelegate.processCommand(host, jsonObject)) {
                webView.loadUrl("javascript:window.vungle.mraidBridge.notifyCommandComplete()");
            }
        }
        return true;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        switch (this.advertisement.getAdType()) {
            case 0:
                webView.loadUrl("javascript:function actionClicked(action){Android.performAction(action);};");
                return;
            case 1:
                this.loadedWebView = webView;
                this.loadedWebView.setVisibility(0);
                notifyPropertiesChange(true);
                return;
            default:
                throw new IllegalArgumentException("Unknown Client Type!");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void */
    public void notifyPropertiesChange(boolean z) {
        if (this.loadedWebView != null) {
            JsonObject jsonObject = new JsonObject();
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.addProperty("width", Integer.valueOf(this.loadedWebView.getWidth()));
            jsonObject2.addProperty("height", Integer.valueOf(this.loadedWebView.getHeight()));
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty("x", (Number) 0);
            jsonObject3.addProperty("y", (Number) 0);
            jsonObject3.addProperty("width", Integer.valueOf(this.loadedWebView.getWidth()));
            jsonObject3.addProperty("height", Integer.valueOf(this.loadedWebView.getHeight()));
            JsonObject jsonObject4 = new JsonObject();
            jsonObject4.addProperty(InviteChannelIds.SMS, (Boolean) false);
            jsonObject4.addProperty("tel", (Boolean) false);
            jsonObject4.addProperty("calendar", (Boolean) false);
            jsonObject4.addProperty("storePicture", (Boolean) false);
            jsonObject4.addProperty("inlineVideo", (Boolean) false);
            jsonObject.add("maxSize", jsonObject2);
            jsonObject.add("screenSize", jsonObject2);
            jsonObject.add("defaultPosition", jsonObject3);
            jsonObject.add("currentPosition", jsonObject3);
            jsonObject.add("supports", jsonObject4);
            jsonObject.addProperty("placementType", this.advertisement.getTemplateType());
            if (this.isViewable != null) {
                jsonObject.addProperty(Constants.ParametersKeys.IS_VIEWABLE, this.isViewable);
            }
            jsonObject.addProperty("os", "android");
            jsonObject.addProperty("osVersion", Integer.toString(Build.VERSION.SDK_INT));
            jsonObject.addProperty("incentivized", Boolean.valueOf(this.placement.isIncentivized()));
            jsonObject.addProperty("enableBackImmediately", Boolean.valueOf(this.advertisement.getShowCloseDelay(this.placement.isIncentivized()) == 0));
            jsonObject.addProperty("version", "1.0");
            if (this.collectConsent) {
                jsonObject.addProperty("consentRequired", (Boolean) true);
                jsonObject.addProperty("consentTitleText", this.gdprTitle);
                jsonObject.addProperty("consentBodyText", this.gdprBody);
                jsonObject.addProperty("consentAcceptButtonText", this.gdprAccept);
                jsonObject.addProperty("consentDenyButtonText", this.gdprDeny);
            } else {
                jsonObject.addProperty("consentRequired", (Boolean) false);
            }
            String str = TAG;
            Log.d(str, "loadJsjavascript:window.vungle.mraidBridge.notifyPropertiesChange(" + jsonObject + "," + z + ")");
            WebView webView = this.loadedWebView;
            webView.loadUrl("javascript:window.vungle.mraidBridge.notifyPropertiesChange(" + jsonObject + "," + z + ")");
        }
    }

    public void setAdVisibility(boolean z) {
        this.isViewable = Boolean.valueOf(z);
        notifyPropertiesChange(false);
    }

    public void setDownloadAdapter(DirectDownloadAdapter directDownloadAdapter2) {
        this.directDownloadAdapter = directDownloadAdapter2;
    }

    public void setErrorListener(WebViewAPI.WebClientErrorListener webClientErrorListener) {
        this.errorListener = webClientErrorListener;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        if (Build.VERSION.SDK_INT < 23) {
            Log.e(TAG, "Error desc " + str);
            Log.e(TAG, "Error for URL " + str2);
            String str3 = str2 + " " + str;
            if (this.errorListener != null) {
                this.errorListener.onReceivedError(str3);
            }
        }
    }

    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        if (Build.VERSION.SDK_INT >= 23) {
            Log.e(TAG, "Error desc " + webResourceError.getDescription().toString());
            Log.e(TAG, "Error for URL " + webResourceRequest.getUrl().toString());
            String str = webResourceRequest.getUrl().toString() + " " + webResourceError.getDescription().toString();
            if (this.errorListener != null) {
                this.errorListener.onReceivedError(str);
            }
        }
    }
}
