package com.vungle.warren.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import com.vungle.warren.ui.CloseDelegate;
import com.vungle.warren.ui.OrientationDelegate;
import com.vungle.warren.ui.contract.WebAdContract;

public class MRAIDAdView extends BaseAdView<WebAdContract.WebAdPresenter> implements WebAdContract.WebAdView {
    private WebAdContract.WebAdPresenter presenter;

    public MRAIDAdView(@NonNull Context context, @NonNull FullAdWidget fullAdWidget, @NonNull OrientationDelegate orientationDelegate, @NonNull CloseDelegate closeDelegate) {
        super(context, fullAdWidget, orientationDelegate, closeDelegate);
    }

    public void updateWindow(boolean z) {
        this.view.updateWindow(z);
    }

    public void setVisibility(boolean z) {
        this.view.setVisibility(z ? 0 : 8);
    }

    public void setPresenter(@NonNull WebAdContract.WebAdPresenter webAdPresenter) {
        this.presenter = webAdPresenter;
    }

    public void showWebsite(@NonNull String str) {
        this.view.showWebsite(str);
    }
}
