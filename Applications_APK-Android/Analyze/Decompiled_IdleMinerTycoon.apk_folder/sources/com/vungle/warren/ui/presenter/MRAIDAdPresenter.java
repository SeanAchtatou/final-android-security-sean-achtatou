package com.vungle.warren.ui.presenter;

import android.content.ActivityNotFoundException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.analytics.AdAnalytics;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.Report;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.contract.WebAdContract;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.WebViewAPI;
import com.vungle.warren.utility.AsyncFileUtils;
import com.vungle.warren.utility.Scheduler;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class MRAIDAdPresenter implements WebAdContract.WebAdPresenter, WebViewAPI.MRAIDDelegate, WebViewAPI.WebClientErrorListener {
    private static final String ACTION = "action";
    private static final String ACTION_WITH_VALUE = "actionWithValue";
    private static final String CANCEL_DOWNLOAD = "cancelDownload";
    private static final String CLOSE = "close";
    private static final String CONSENT_ACTION = "consentAction";
    private static final String EXTRA_INCENTIVIZED_SENT = "incentivized_sent";
    private static String EXTRA_REPORT = "saved_report";
    private static final String EXTRA_WEB_READY = "web_ready";
    protected static final double NINE_BY_SIXTEEN_ASPECT_RATIO = 0.5625d;
    private static final String OPEN = "open";
    private static final String OPEN_APP_IN_DEVICE = "openAppInDevice";
    private static final String OPEN_PRIVACY = "openPrivacy";
    private static final String START_DOWNLOAD_APP_ON_DEVICE = "startDownloadAppOnDevice";
    private static final String SUCCESSFUL_VIEW = "successfulView";
    private static final String TAG = MRAIDAdPresenter.class.getCanonicalName();
    private static final String TPAT = "tpat";
    private static final String USE_CUSTOM_CLOSE = "useCustomClose";
    private static final String USE_CUSTOM_PRIVACY = "useCustomPrivacy";
    private static final String VIDEO_VIEWED = "videoViewed";
    private long adStartTime;
    /* access modifiers changed from: private */
    public WebAdContract.WebAdView adView;
    private Advertisement advertisement;
    private final AdAnalytics analytics;
    private File assetDir;
    /* access modifiers changed from: private */
    public boolean backEnabled;
    /* access modifiers changed from: private */
    public AdContract.AdvertisementPresenter.EventListener bus;
    private Map<String, Cookie> cookieMap = new HashMap();
    private DirectDownloadAdapter directDownloadAdapter;
    private boolean directDownloadApkEnabled;
    private long duration;
    private boolean hasSend80Percent = false;
    private boolean hasSendStart = false;
    private final ExecutorService ioExecutor;
    private AtomicBoolean isDestroying = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public Placement placement;
    /* access modifiers changed from: private */
    public Repository.SaveCallback repoCallback = new Repository.SaveCallback() {
        boolean errorHappened = false;

        public void onSaved() {
        }

        public void onError(Exception exc) {
            if (!this.errorHappened) {
                this.errorHappened = true;
                if (MRAIDAdPresenter.this.bus != null) {
                    MRAIDAdPresenter.this.bus.onError(new VungleException(26), MRAIDAdPresenter.this.placement.getId());
                }
                MRAIDAdPresenter.this.closeView();
            }
        }
    };
    /* access modifiers changed from: private */
    public Report report;
    /* access modifiers changed from: private */
    public Repository repository;
    private final Scheduler scheduler;
    private AtomicBoolean sendReportIncentivized = new AtomicBoolean(false);
    private final ExecutorService uiExecutor;
    private WebViewAPI webClient;

    public MRAIDAdPresenter(@NonNull Advertisement advertisement2, @NonNull Placement placement2, @NonNull Repository repository2, @NonNull Scheduler scheduler2, @NonNull AdAnalytics adAnalytics, @NonNull WebViewAPI webViewAPI, @Nullable DirectDownloadAdapter directDownloadAdapter2, @Nullable OptionsState optionsState, @NonNull File file, @NonNull ExecutorService executorService, @NonNull ExecutorService executorService2) {
        this.advertisement = advertisement2;
        this.repository = repository2;
        this.placement = placement2;
        this.scheduler = scheduler2;
        this.analytics = adAnalytics;
        this.webClient = webViewAPI;
        this.directDownloadAdapter = directDownloadAdapter2;
        this.assetDir = file;
        this.ioExecutor = executorService;
        this.uiExecutor = executorService2;
        load(optionsState);
    }

    public void setEventListener(@Nullable AdContract.AdvertisementPresenter.EventListener eventListener) {
        this.bus = eventListener;
    }

    public void reportAction(@NonNull String str, @Nullable String str2) {
        if (str.equals("videoLength")) {
            this.duration = Long.parseLong(str2);
            this.report.setVideoLength(this.duration);
            this.repository.save(this.report, this.repoCallback);
            return;
        }
        this.report.recordAction(str, str2, System.currentTimeMillis());
        this.repository.save(this.report, this.repoCallback);
    }

    public void onViewConfigurationChanged() {
        this.adView.updateWindow(this.advertisement.getTemplateType().equals("flexview"));
        this.webClient.notifyPropertiesChange(true);
    }

    public void attach(@NonNull WebAdContract.WebAdView webAdView, @Nullable OptionsState optionsState) {
        boolean z = false;
        this.isDestroying.set(false);
        this.adView = webAdView;
        webAdView.setPresenter(this);
        int settings = this.advertisement.getAdConfig().getSettings();
        if (settings > 0) {
            this.backEnabled = (settings & 2) == 2;
            if ((settings & 32) == 32) {
                z = true;
            }
            this.directDownloadApkEnabled = z;
        }
        int i = 4;
        if ((this.advertisement.getAdConfig().getSettings() & 16) != 16) {
            switch (this.advertisement.getOrientation()) {
                case 0:
                    i = 7;
                    break;
                case 1:
                    i = 6;
                    break;
                case 2:
                    break;
                default:
                    i = -1;
                    break;
            }
        }
        webAdView.setOrientation(i);
        prepare(optionsState);
    }

    public void detach(boolean z) {
        stop(z, true);
        this.adView.destroyAdView();
    }

    private void prepare(@Nullable OptionsState optionsState) {
        this.webClient.setMRAIDDelegate(this);
        this.webClient.setDownloadAdapter(this.directDownloadAdapter);
        this.webClient.setErrorListener(this);
        loadMraid(new File(this.assetDir.getPath() + File.separator + "template"));
        Cookie cookie = this.cookieMap.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        if ("flexview".equals(this.advertisement.getTemplateType()) && this.advertisement.getAdConfig().getFlexViewCloseTime() > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    MRAIDAdPresenter.this.report.recordAction("mraidCloseByTimer", "", currentTimeMillis);
                    MRAIDAdPresenter.this.report.recordAction("mraidClose", "", currentTimeMillis);
                    MRAIDAdPresenter.this.repository.save(MRAIDAdPresenter.this.report, MRAIDAdPresenter.this.repoCallback);
                    MRAIDAdPresenter.this.closeView();
                }
            }, (long) (this.advertisement.getAdConfig().getFlexViewCloseTime() * 1000));
        }
        String string = cookie == null ? null : cookie.getString("userID");
        if (this.report == null) {
            this.adStartTime = System.currentTimeMillis();
            this.report = new Report(this.advertisement, this.placement, this.adStartTime, string);
            this.repository.save(this.report, this.repoCallback);
        }
        Cookie cookie2 = this.cookieMap.get(Cookie.CONSENT_COOKIE);
        if (cookie2 != null) {
            boolean z = cookie2.getBoolean("is_country_data_protected").booleanValue() && "unknown".equals(cookie2.getString("consent_status"));
            this.webClient.setConsentStatus(z, cookie2.getString("consent_title"), cookie2.getString("consent_message"), cookie2.getString("button_accept"), cookie2.getString("button_deny"));
            if (z) {
                cookie2.putValue("consent_status", "opted_out_by_timeout");
                cookie2.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                cookie2.putValue("consent_source", "vungle_modal");
                this.repository.save(cookie2, this.repoCallback);
            }
        }
        int showCloseDelay = this.advertisement.getShowCloseDelay(this.placement.isIncentivized());
        if (showCloseDelay > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    boolean unused = MRAIDAdPresenter.this.backEnabled = true;
                }
            }, (long) showCloseDelay);
        } else {
            this.backEnabled = true;
        }
        this.adView.updateWindow(this.advertisement.getTemplateType().equals("flexview"));
        if (this.bus != null) {
            this.bus.onNext("start", null, this.placement.getId());
        }
    }

    private void loadMraid(@NonNull File file) {
        File file2 = new File(file.getParent());
        final File file3 = new File(file2.getPath() + File.separator + "index.html");
        new AsyncFileUtils(this.ioExecutor, this.uiExecutor).isFileExistAsync(file3, new AsyncFileUtils.FileExist() {
            public void status(boolean z) {
                if (!z) {
                    if (MRAIDAdPresenter.this.bus != null) {
                        MRAIDAdPresenter.this.bus.onError(new VungleException(10), MRAIDAdPresenter.this.placement.getId());
                    }
                    MRAIDAdPresenter.this.adView.close();
                    return;
                }
                WebAdContract.WebAdView access$700 = MRAIDAdPresenter.this.adView;
                access$700.showWebsite(Advertisement.FILE_SCHEME + file3.getPath());
            }
        });
    }

    public void start() {
        this.webClient.setAdVisibility(true);
        this.adView.setImmersiveMode();
        this.adView.resumeWeb();
        setAdVisibility(true);
    }

    public void stop(boolean z, boolean z2) {
        this.adView.pauseWeb();
        setAdVisibility(false);
        if (!z && z2 && !this.isDestroying.getAndSet(true)) {
            String str = null;
            if (this.webClient != null) {
                this.webClient.setMRAIDDelegate(null);
            }
            if (this.bus != null) {
                AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
                if (this.report.isCTAClicked()) {
                    str = "isCTAClicked";
                }
                eventListener.onNext("end", str, this.placement.getId());
            }
            if (this.directDownloadAdapter != null) {
                this.directDownloadAdapter.getSdkDownloadClient().cleanUp();
            }
        }
    }

    public void setAdVisibility(boolean z) {
        this.webClient.setAdVisibility(z);
    }

    public void generateSaveState(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            this.repository.save(this.report, this.repoCallback);
            optionsState.put(EXTRA_REPORT, this.report.getId());
            optionsState.put(EXTRA_INCENTIVIZED_SENT, this.sendReportIncentivized.get());
        }
    }

    public void restoreFromSave(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            boolean z = optionsState.getBoolean(EXTRA_INCENTIVIZED_SENT, false);
            if (z) {
                this.sendReportIncentivized.set(z);
            }
            if (this.report == null) {
                this.adView.close();
            }
        }
    }

    public boolean handleExit(@Nullable String str) {
        if (str == null) {
            if (this.backEnabled) {
                this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            }
            return false;
        } else if (this.advertisement == null || this.placement == null) {
            Log.e(TAG, "Unable to close advertisement");
            return false;
        } else if (!this.placement.getId().equals(str)) {
            Log.e(TAG, "Cannot close FlexView Ad with invalid placement reference id");
            return false;
        } else if (!"flexview".equals(this.advertisement.getTemplateType())) {
            Log.e(TAG, "Cannot close a Non FlexView ad");
            return false;
        } else {
            this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            reportAction("mraidCloseByApi", null);
            return true;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01dd, code lost:
        if (r9.equals(com.tapjoy.TJAdUnitConstants.String.VISIBLE) == false) goto L_0x01f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0189, code lost:
        if (r9.equals("false") == false) goto L_0x01a0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01f8  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x020f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01bb A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean processCommand(@android.support.annotation.NonNull java.lang.String r9, @android.support.annotation.NonNull com.google.gson.JsonObject r10) {
        /*
            r8 = this;
            int r0 = r9.hashCode()
            r1 = 2
            r2 = -1
            r3 = 0
            r4 = 1
            switch(r0) {
                case -1912374177: goto L_0x008c;
                case -1891064718: goto L_0x0081;
                case -1422950858: goto L_0x0077;
                case -1382780692: goto L_0x006c;
                case -735200587: goto L_0x0062;
                case -660787472: goto L_0x0058;
                case -511324706: goto L_0x004d;
                case -503430878: goto L_0x0042;
                case -348095344: goto L_0x0038;
                case 3417674: goto L_0x002e;
                case 3566511: goto L_0x0023;
                case 94756344: goto L_0x0018;
                case 1614272768: goto L_0x000d;
                default: goto L_0x000b;
            }
        L_0x000b:
            goto L_0x0097
        L_0x000d:
            java.lang.String r0 = "useCustomClose"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 6
            goto L_0x0098
        L_0x0018:
            java.lang.String r0 = "close"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 0
            goto L_0x0098
        L_0x0023:
            java.lang.String r0 = "tpat"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 3
            goto L_0x0098
        L_0x002e:
            java.lang.String r0 = "open"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 5
            goto L_0x0098
        L_0x0038:
            java.lang.String r0 = "useCustomPrivacy"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 7
            goto L_0x0098
        L_0x0042:
            java.lang.String r0 = "cancelDownload"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 11
            goto L_0x0098
        L_0x004d:
            java.lang.String r0 = "openPrivacy"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 8
            goto L_0x0098
        L_0x0058:
            java.lang.String r0 = "consentAction"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 1
            goto L_0x0098
        L_0x0062:
            java.lang.String r0 = "actionWithValue"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 2
            goto L_0x0098
        L_0x006c:
            java.lang.String r0 = "startDownloadAppOnDevice"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 10
            goto L_0x0098
        L_0x0077:
            java.lang.String r0 = "action"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 4
            goto L_0x0098
        L_0x0081:
            java.lang.String r0 = "openAppInDevice"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 12
            goto L_0x0098
        L_0x008c:
            java.lang.String r0 = "successfulView"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0097
            r9 = 9
            goto L_0x0098
        L_0x0097:
            r9 = -1
        L_0x0098:
            r0 = 3178655(0x30809f, float:4.454244E-39)
            r5 = 0
            switch(r9) {
                case 0: goto L_0x03d0;
                case 1: goto L_0x038f;
                case 2: goto L_0x0253;
                case 3: goto L_0x023d;
                case 4: goto L_0x023c;
                case 5: goto L_0x0210;
                case 6: goto L_0x01bc;
                case 7: goto L_0x0168;
                case 8: goto L_0x0158;
                case 9: goto L_0x00ca;
                case 10: goto L_0x00bc;
                case 11: goto L_0x00ae;
                case 12: goto L_0x00a0;
                default: goto L_0x009f;
            }
        L_0x009f:
            return r3
        L_0x00a0:
            com.vungle.warren.DirectDownloadAdapter r9 = r8.directDownloadAdapter
            if (r9 == 0) goto L_0x00ad
            com.vungle.warren.DirectDownloadAdapter r9 = r8.directDownloadAdapter
            com.vungle.warren.SDKDownloadClient r9 = r9.getSdkDownloadClient()
            r9.sendOpenPackageRequest()
        L_0x00ad:
            return r4
        L_0x00ae:
            com.vungle.warren.DirectDownloadAdapter r9 = r8.directDownloadAdapter
            if (r9 == 0) goto L_0x00bb
            com.vungle.warren.DirectDownloadAdapter r9 = r8.directDownloadAdapter
            com.vungle.warren.SDKDownloadClient r9 = r9.getSdkDownloadClient()
            r9.cancelDownloadRequest()
        L_0x00bb:
            return r4
        L_0x00bc:
            com.vungle.warren.DirectDownloadAdapter r9 = r8.directDownloadAdapter
            if (r9 == 0) goto L_0x00c9
            com.vungle.warren.DirectDownloadAdapter r9 = r8.directDownloadAdapter
            com.vungle.warren.SDKDownloadClient r9 = r9.getSdkDownloadClient()
            r9.sendDownloadRequest()
        L_0x00c9:
            return r4
        L_0x00ca:
            com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter$EventListener r9 = r8.bus
            if (r9 == 0) goto L_0x00e1
            com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter$EventListener r9 = r8.bus
            java.lang.String r10 = "successfulView"
            com.vungle.warren.model.Placement r0 = r8.placement
            if (r0 != 0) goto L_0x00d8
            r0 = r5
            goto L_0x00de
        L_0x00d8:
            com.vungle.warren.model.Placement r0 = r8.placement
            java.lang.String r0 = r0.getId()
        L_0x00de:
            r9.onNext(r10, r5, r0)
        L_0x00e1:
            java.util.Map<java.lang.String, com.vungle.warren.model.Cookie> r9 = r8.cookieMap
            java.lang.String r10 = "configSettings"
            java.lang.Object r9 = r9.get(r10)
            com.vungle.warren.model.Cookie r9 = (com.vungle.warren.model.Cookie) r9
            com.vungle.warren.model.Placement r10 = r8.placement
            boolean r10 = r10.isIncentivized()
            if (r10 == 0) goto L_0x0157
            if (r9 == 0) goto L_0x0157
            java.lang.String r10 = "isReportIncentivizedEnabled"
            java.lang.Boolean r9 = r9.getBoolean(r10)
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x0157
            java.util.concurrent.atomic.AtomicBoolean r9 = r8.sendReportIncentivized
            boolean r9 = r9.getAndSet(r4)
            if (r9 != 0) goto L_0x0157
            com.google.gson.JsonObject r9 = new com.google.gson.JsonObject
            r9.<init>()
            java.lang.String r10 = "placement_reference_id"
            com.google.gson.JsonPrimitive r0 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Placement r1 = r8.placement
            java.lang.String r1 = r1.getId()
            r0.<init>(r1)
            r9.add(r10, r0)
            java.lang.String r10 = "app_id"
            com.google.gson.JsonPrimitive r0 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Advertisement r1 = r8.advertisement
            java.lang.String r1 = r1.getAppID()
            r0.<init>(r1)
            r9.add(r10, r0)
            java.lang.String r10 = "adStartTime"
            com.google.gson.JsonPrimitive r0 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r1 = r8.report
            long r1 = r1.getAdStartTime()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            r0.<init>(r1)
            r9.add(r10, r0)
            java.lang.String r10 = "user"
            com.google.gson.JsonPrimitive r0 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r1 = r8.report
            java.lang.String r1 = r1.getUserID()
            r0.<init>(r1)
            r9.add(r10, r0)
            com.vungle.warren.analytics.AdAnalytics r10 = r8.analytics
            r10.ri(r9)
        L_0x0157:
            return r4
        L_0x0158:
            java.lang.String r9 = "url"
            com.google.gson.JsonElement r9 = r10.get(r9)
            java.lang.String r9 = r9.getAsString()
            com.vungle.warren.ui.contract.WebAdContract$WebAdView r10 = r8.adView
            r10.open(r9)
            return r4
        L_0x0168:
            java.lang.String r9 = "useCustomPrivacy"
            com.google.gson.JsonElement r9 = r10.get(r9)
            java.lang.String r9 = r9.getAsString()
            int r10 = r9.hashCode()
            if (r10 == r0) goto L_0x0196
            r0 = 3569038(0x36758e, float:5.001287E-39)
            if (r10 == r0) goto L_0x018c
            r0 = 97196323(0x5cb1923, float:1.9099262E-35)
            if (r10 == r0) goto L_0x0183
            goto L_0x01a0
        L_0x0183:
            java.lang.String r10 = "false"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x01a0
            goto L_0x01a1
        L_0x018c:
            java.lang.String r10 = "true"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x01a0
            r1 = 1
            goto L_0x01a1
        L_0x0196:
            java.lang.String r10 = "gone"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x01a0
            r1 = 0
            goto L_0x01a1
        L_0x01a0:
            r1 = -1
        L_0x01a1:
            switch(r1) {
                case 0: goto L_0x01bb;
                case 1: goto L_0x01bb;
                case 2: goto L_0x01bb;
                default: goto L_0x01a4;
            }
        L_0x01a4:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unknown value "
            r0.append(r1)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            r10.<init>(r9)
            throw r10
        L_0x01bb:
            return r4
        L_0x01bc:
            java.lang.String r9 = "sdkCloseButton"
            com.google.gson.JsonElement r9 = r10.get(r9)
            java.lang.String r9 = r9.getAsString()
            int r10 = r9.hashCode()
            r5 = -1901805651(0xffffffff8ea4bfad, float:-4.06137E-30)
            if (r10 == r5) goto L_0x01ea
            if (r10 == r0) goto L_0x01e0
            r0 = 466743410(0x1bd1f072, float:3.4731534E-22)
            if (r10 == r0) goto L_0x01d7
            goto L_0x01f4
        L_0x01d7:
            java.lang.String r10 = "visible"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x01f4
            goto L_0x01f5
        L_0x01e0:
            java.lang.String r10 = "gone"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x01f4
            r1 = 0
            goto L_0x01f5
        L_0x01ea:
            java.lang.String r10 = "invisible"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x01f4
            r1 = 1
            goto L_0x01f5
        L_0x01f4:
            r1 = -1
        L_0x01f5:
            switch(r1) {
                case 0: goto L_0x020f;
                case 1: goto L_0x020f;
                case 2: goto L_0x020f;
                default: goto L_0x01f8;
            }
        L_0x01f8:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unknown value "
            r0.append(r1)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            r10.<init>(r9)
            throw r10
        L_0x020f:
            return r4
        L_0x0210:
            java.lang.String r9 = "download"
            r8.reportAction(r9, r5)
            java.lang.String r9 = "mraidOpen"
            r8.reportAction(r9, r5)
            java.lang.String r9 = "url"
            com.google.gson.JsonElement r9 = r10.get(r9)
            java.lang.String r9 = r9.getAsString()
            boolean r10 = r8.directDownloadApkEnabled
            com.vungle.warren.model.Advertisement r0 = r8.advertisement
            boolean r0 = r0.isRequiresNonMarketInstall()
            boolean r10 = com.vungle.warren.download.APKDirectDownloadManager.isDirectDownloadEnabled(r10, r0)
            if (r10 == 0) goto L_0x0236
            com.vungle.warren.download.APKDirectDownloadManager.download(r9)
            goto L_0x023b
        L_0x0236:
            com.vungle.warren.ui.contract.WebAdContract$WebAdView r10 = r8.adView
            r10.open(r9)
        L_0x023b:
            return r4
        L_0x023c:
            return r4
        L_0x023d:
            java.lang.String r9 = "event"
            com.google.gson.JsonElement r9 = r10.get(r9)
            java.lang.String r9 = r9.getAsString()
            com.vungle.warren.analytics.AdAnalytics r10 = r8.analytics
            com.vungle.warren.model.Advertisement r0 = r8.advertisement
            java.lang.String[] r9 = r0.getTpatUrls(r9)
            r10.ping(r9)
            return r4
        L_0x0253:
            java.lang.String r9 = "event"
            com.google.gson.JsonElement r9 = r10.get(r9)
            java.lang.String r9 = r9.getAsString()
            java.lang.String r0 = "value"
            com.google.gson.JsonElement r10 = r10.get(r0)
            java.lang.String r10 = r10.getAsString()
            com.vungle.warren.model.Report r0 = r8.report
            long r1 = java.lang.System.currentTimeMillis()
            r0.recordAction(r9, r10, r1)
            com.vungle.warren.persistence.Repository r0 = r8.repository
            com.vungle.warren.model.Report r1 = r8.report
            com.vungle.warren.persistence.Repository$SaveCallback r2 = r8.repoCallback
            r0.save(r1, r2)
            java.lang.String r0 = "videoViewed"
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x0371
            long r0 = r8.duration
            r6 = 0
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x0371
            float r0 = java.lang.Float.parseFloat(r10)     // Catch:{ NumberFormatException -> 0x0297 }
            long r1 = r8.duration     // Catch:{ NumberFormatException -> 0x0297 }
            float r1 = (float) r1
            float r0 = r0 / r1
            r1 = 1120403456(0x42c80000, float:100.0)
            float r0 = r0 * r1
            int r0 = (int) r0
            goto L_0x029f
        L_0x0297:
            java.lang.String r0 = com.vungle.warren.ui.presenter.MRAIDAdPresenter.TAG
            java.lang.String r1 = "value for videoViewed is null !"
            android.util.Log.e(r0, r1)
            r0 = 0
        L_0x029f:
            if (r0 <= 0) goto L_0x0371
            com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter$EventListener r1 = r8.bus
            if (r1 == 0) goto L_0x02c7
            com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter$EventListener r1 = r8.bus
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r6 = "percentViewed:"
            r2.append(r6)
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.vungle.warren.model.Placement r6 = r8.placement
            if (r6 != 0) goto L_0x02be
            r6 = r5
            goto L_0x02c4
        L_0x02be:
            com.vungle.warren.model.Placement r6 = r8.placement
            java.lang.String r6 = r6.getId()
        L_0x02c4:
            r1.onNext(r2, r5, r6)
        L_0x02c7:
            boolean r1 = r8.hasSendStart
            if (r1 != 0) goto L_0x02de
            if (r0 <= r4) goto L_0x02de
            r8.hasSendStart = r4
            com.vungle.warren.DirectDownloadAdapter r1 = r8.directDownloadAdapter
            if (r1 == 0) goto L_0x02de
            com.vungle.warren.DirectDownloadAdapter r1 = r8.directDownloadAdapter
            com.vungle.warren.SDKDownloadClient r1 = r1.getSdkDownloadClient()
            com.vungle.warren.DirectDownloadAdapter$CONTRACT_TYPE r2 = com.vungle.warren.DirectDownloadAdapter.CONTRACT_TYPE.CPI
            r1.sendADDisplayingNotify(r3, r2)
        L_0x02de:
            boolean r1 = r8.hasSend80Percent
            if (r1 != 0) goto L_0x02f7
            r1 = 80
            if (r0 <= r1) goto L_0x02f7
            r8.hasSend80Percent = r4
            com.vungle.warren.DirectDownloadAdapter r1 = r8.directDownloadAdapter
            if (r1 == 0) goto L_0x02f7
            com.vungle.warren.DirectDownloadAdapter r1 = r8.directDownloadAdapter
            com.vungle.warren.SDKDownloadClient r1 = r1.getSdkDownloadClient()
            com.vungle.warren.DirectDownloadAdapter$CONTRACT_TYPE r2 = com.vungle.warren.DirectDownloadAdapter.CONTRACT_TYPE.CPI
            r1.sendADDisplayingNotify(r4, r2)
        L_0x02f7:
            java.util.Map<java.lang.String, com.vungle.warren.model.Cookie> r1 = r8.cookieMap
            java.lang.String r2 = "configSettings"
            java.lang.Object r1 = r1.get(r2)
            com.vungle.warren.model.Cookie r1 = (com.vungle.warren.model.Cookie) r1
            com.vungle.warren.model.Placement r2 = r8.placement
            boolean r2 = r2.isIncentivized()
            if (r2 == 0) goto L_0x0371
            r2 = 75
            if (r0 <= r2) goto L_0x0371
            if (r1 == 0) goto L_0x0371
            java.lang.String r0 = "isReportIncentivizedEnabled"
            java.lang.Boolean r0 = r1.getBoolean(r0)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0371
            java.util.concurrent.atomic.AtomicBoolean r0 = r8.sendReportIncentivized
            boolean r0 = r0.getAndSet(r4)
            if (r0 != 0) goto L_0x0371
            com.google.gson.JsonObject r0 = new com.google.gson.JsonObject
            r0.<init>()
            java.lang.String r1 = "placement_reference_id"
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Placement r3 = r8.placement
            java.lang.String r3 = r3.getId()
            r2.<init>(r3)
            r0.add(r1, r2)
            java.lang.String r1 = "app_id"
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Advertisement r3 = r8.advertisement
            java.lang.String r3 = r3.getAppID()
            r2.<init>(r3)
            r0.add(r1, r2)
            java.lang.String r1 = "adStartTime"
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r3 = r8.report
            long r5 = r3.getAdStartTime()
            java.lang.Long r3 = java.lang.Long.valueOf(r5)
            r2.<init>(r3)
            r0.add(r1, r2)
            java.lang.String r1 = "user"
            com.google.gson.JsonPrimitive r2 = new com.google.gson.JsonPrimitive
            com.vungle.warren.model.Report r3 = r8.report
            java.lang.String r3 = r3.getUserID()
            r2.<init>(r3)
            r0.add(r1, r2)
            com.vungle.warren.analytics.AdAnalytics r1 = r8.analytics
            r1.ri(r0)
        L_0x0371:
            java.lang.String r0 = "videoLength"
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x0389
            long r0 = java.lang.Long.parseLong(r10)
            r8.duration = r0
            java.lang.String r9 = "videoLength"
            r8.reportAction(r9, r10)
            com.vungle.warren.ui.view.WebViewAPI r9 = r8.webClient
            r9.notifyPropertiesChange(r4)
        L_0x0389:
            com.vungle.warren.ui.contract.WebAdContract$WebAdView r9 = r8.adView
            r9.setVisibility(r4)
            return r4
        L_0x038f:
            java.util.Map<java.lang.String, com.vungle.warren.model.Cookie> r9 = r8.cookieMap
            java.lang.String r0 = "consentIsImportantToVungle"
            java.lang.Object r9 = r9.get(r0)
            com.vungle.warren.model.Cookie r9 = (com.vungle.warren.model.Cookie) r9
            if (r9 != 0) goto L_0x03a2
            com.vungle.warren.model.Cookie r9 = new com.vungle.warren.model.Cookie
            java.lang.String r0 = "consentIsImportantToVungle"
            r9.<init>(r0)
        L_0x03a2:
            java.lang.String r0 = "event"
            com.google.gson.JsonElement r10 = r10.get(r0)
            java.lang.String r10 = r10.getAsString()
            java.lang.String r0 = "consent_status"
            r9.putValue(r0, r10)
            java.lang.String r10 = "consent_source"
            java.lang.String r0 = "vungle_modal"
            r9.putValue(r10, r0)
            java.lang.String r10 = "timestamp"
            long r0 = java.lang.System.currentTimeMillis()
            r2 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 / r2
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r9.putValue(r10, r0)
            com.vungle.warren.persistence.Repository r10 = r8.repository
            com.vungle.warren.persistence.Repository$SaveCallback r0 = r8.repoCallback
            r10.save(r9, r0)
            return r4
        L_0x03d0:
            java.lang.String r9 = "mraidClose"
            r8.reportAction(r9, r5)
            r8.closeView()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.presenter.MRAIDAdPresenter.processCommand(java.lang.String, com.google.gson.JsonObject):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMraidAction(@android.support.annotation.NonNull java.lang.String r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            r1 = -314498168(0xffffffffed412388, float:-3.7358476E27)
            if (r0 == r1) goto L_0x0028
            r1 = 94756344(0x5a5ddf8, float:1.5598064E-35)
            if (r0 == r1) goto L_0x001e
            r1 = 1427818632(0x551ac888, float:1.06366291E13)
            if (r0 == r1) goto L_0x0014
            goto L_0x0032
        L_0x0014:
            java.lang.String r0 = "download"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 1
            goto L_0x0033
        L_0x001e:
            java.lang.String r0 = "close"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 0
            goto L_0x0033
        L_0x0028:
            java.lang.String r0 = "privacy"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 2
            goto L_0x0033
        L_0x0032:
            r0 = -1
        L_0x0033:
            switch(r0) {
                case 0: goto L_0x0051;
                case 1: goto L_0x004d;
                case 2: goto L_0x0054;
                default: goto L_0x0036;
            }
        L_0x0036:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown action "
            r1.append(r2)
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            r0.<init>(r4)
            throw r0
        L_0x004d:
            r3.download()
            goto L_0x0054
        L_0x0051:
            r3.closeView()
        L_0x0054:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.presenter.MRAIDAdPresenter.onMraidAction(java.lang.String):void");
    }

    private void download() {
        reportAction("cta", "");
        try {
            this.analytics.ping(new String[]{this.advertisement.getCTAURL(true)});
            this.adView.open(this.advertisement.getCTAURL(false));
        } catch (ActivityNotFoundException unused) {
        }
    }

    /* access modifiers changed from: private */
    public void closeView() {
        this.report.setAdDuration((int) (System.currentTimeMillis() - this.adStartTime));
        this.repository.save(this.report, this.repoCallback);
        this.adView.close();
        this.scheduler.cancelAll();
    }

    private void load(OptionsState optionsState) {
        this.cookieMap.put(Cookie.INCENTIVIZED_TEXT_COOKIE, this.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get());
        this.cookieMap.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookieMap.put(Cookie.CONFIG_COOKIE, this.repository.load(Cookie.CONFIG_COOKIE, Cookie.class).get());
        if (optionsState != null) {
            String string = optionsState.getString(EXTRA_REPORT);
            this.report = TextUtils.isEmpty(string) ? null : (Report) this.repository.load(string, Report.class).get();
        }
    }

    public void onReceivedError(String str) {
        if (this.report != null) {
            this.report.recordError(str);
            this.repository.save(this.report, this.repoCallback);
        }
    }
}
