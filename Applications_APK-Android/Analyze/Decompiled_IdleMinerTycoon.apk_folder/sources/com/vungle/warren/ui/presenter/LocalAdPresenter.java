package com.vungle.warren.ui.presenter;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.vungle.warren.analytics.AdAnalytics;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.analytics.AnalyticsVideoTracker;
import com.vungle.warren.download.APKDirectDownloadManager;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.Report;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.contract.LocalAdContract;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.WebViewAPI;
import com.vungle.warren.utility.AsyncFileUtils;
import com.vungle.warren.utility.Scheduler;
import im.getsocial.sdk.activities.MentionTypes;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class LocalAdPresenter implements LocalAdContract.LocalPresenter, WebViewAPI.WebClientErrorListener {
    static final String EXTRA_INCENTIVIZED_SENT = "incentivized_sent";
    static final String EXTRA_IN_POST = "in_post_roll";
    static final String EXTRA_REPORT = "saved_report";
    static final String EXTRA_VIDEO_POSITION = "videoPosition";
    static final String HTTPS_VUNGLE_COM_PRIVACY = "https://vungle.com/privacy/";
    public static final int INCENTIVIZED_TRESHOLD = 75;
    static final String TAG = "LocalAdPresenter";
    private long adStartTime;
    /* access modifiers changed from: private */
    public LocalAdContract.LocalView adView;
    /* access modifiers changed from: private */
    public Advertisement advertisement;
    /* access modifiers changed from: private */
    public final AdAnalytics analytics;
    private File assetDir;
    /* access modifiers changed from: private */
    public AdContract.AdvertisementPresenter.EventListener bus;
    private AtomicBoolean busy = new AtomicBoolean(false);
    private LinkedList<Advertisement.Checkpoint> checkpointList = new LinkedList<>();
    private final HashMap<String, Cookie> cookies = new HashMap<>();
    private String dialogBody = "If you exit now, you will not get your reward";
    private String dialogClose = "Close";
    private String dialogContinue = "Continue";
    private String dialogTitle = "Are you sure?";
    private boolean directDownloadApkEnabled;
    private int duration;
    /* access modifiers changed from: private */
    public boolean inPost;
    private final ExecutorService ioExecutor;
    private AtomicBoolean isDestroying = new AtomicBoolean(false);
    private boolean muted;
    /* access modifiers changed from: private */
    public Placement placement;
    private int progress;
    private Repository.SaveCallback repoCallback = new Repository.SaveCallback() {
        boolean errorHappened = false;

        public void onSaved() {
        }

        public void onError(Exception exc) {
            if (!this.errorHappened) {
                this.errorHappened = true;
                if (LocalAdPresenter.this.bus != null) {
                    LocalAdPresenter.this.bus.onError(new VungleException(26), LocalAdPresenter.this.placement.getId());
                }
                LocalAdPresenter.this.closeAndReport();
            }
        }
    };
    private Report report;
    /* access modifiers changed from: private */
    public Repository repository;
    private final Scheduler scheduler;
    private AtomicBoolean sendReportIncentivized = new AtomicBoolean(false);
    private final ExecutorService uiExecutor;
    /* access modifiers changed from: private */
    public boolean userExitEnabled;
    private int videoPosition;
    /* access modifiers changed from: private */
    public final AnalyticsVideoTracker videoTracker;
    private final WebViewAPI webViewAPI;

    public LocalAdPresenter(@NonNull Advertisement advertisement2, @NonNull Placement placement2, @NonNull Repository repository2, @NonNull Scheduler scheduler2, @NonNull AdAnalytics adAnalytics, @NonNull AnalyticsVideoTracker analyticsVideoTracker, @NonNull WebViewAPI webViewAPI2, @Nullable OptionsState optionsState, @NonNull File file, @NonNull ExecutorService executorService, @NonNull ExecutorService executorService2) {
        this.advertisement = advertisement2;
        this.placement = placement2;
        this.scheduler = scheduler2;
        this.analytics = adAnalytics;
        this.videoTracker = analyticsVideoTracker;
        this.webViewAPI = webViewAPI2;
        this.repository = repository2;
        this.assetDir = file;
        this.ioExecutor = executorService;
        this.uiExecutor = executorService2;
        if (advertisement2.getCheckpoints() != null) {
            this.checkpointList.addAll(advertisement2.getCheckpoints());
            Collections.sort(this.checkpointList);
        }
        loadData(optionsState);
    }

    public void setEventListener(@Nullable AdContract.AdvertisementPresenter.EventListener eventListener) {
        this.bus = eventListener;
    }

    public void reportError(@NonNull String str) {
        this.report.recordError(str);
        this.repository.save(this.report, this.repoCallback);
        if (this.inPost || !this.advertisement.hasPostroll()) {
            if (this.bus != null) {
                this.bus.onError(new Throwable(str), this.placement.getId());
            }
            this.adView.close();
            return;
        }
        playPost();
    }

    public void reportAction(@NonNull String str, @Nullable String str2) {
        if (str.equals("videoLength")) {
            this.duration = Integer.parseInt(str2);
            this.report.setVideoLength((long) this.duration);
            this.repository.save(this.report, this.repoCallback);
            return;
        }
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != -840405966) {
            if (hashCode != 3363353) {
                if (hashCode == 1370606900 && str.equals(AnalyticsEvent.Ad.videoClose)) {
                    c = 2;
                }
            } else if (str.equals("mute")) {
                c = 0;
            }
        } else if (str.equals("unmute")) {
            c = 1;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
                this.analytics.ping(this.advertisement.getTpatUrls(str));
                break;
        }
        this.report.recordAction(str, str2, System.currentTimeMillis());
        this.repository.save(this.report, this.repoCallback);
    }

    public void onViewConfigurationChanged() {
        this.webViewAPI.notifyPropertiesChange(true);
    }

    public void attach(@NonNull LocalAdContract.LocalView localView, @Nullable OptionsState optionsState) {
        boolean z = false;
        this.isDestroying.set(false);
        this.adView = localView;
        localView.setPresenter(this);
        int settings = this.advertisement.getAdConfig().getSettings();
        if (settings > 0) {
            this.muted = (settings & 1) == 1;
            this.userExitEnabled = (settings & 2) == 2;
            if ((settings & 32) == 32) {
                z = true;
            }
            this.directDownloadApkEnabled = z;
        }
        int i = 4;
        if ((this.advertisement.getAdConfig().getSettings() & 16) != 16) {
            switch (this.advertisement.getOrientation()) {
                case 0:
                    i = 7;
                    break;
                case 1:
                    i = 6;
                    break;
                case 2:
                    break;
                default:
                    i = -1;
                    break;
            }
        }
        Log.d(TAG, "requested orientation " + i);
        localView.setOrientation(i);
        prepare(optionsState);
    }

    public void detach(boolean z) {
        stop(z, true);
        this.adView.destroyAdView();
    }

    private void playPost() {
        File file = new File(this.assetDir.getPath());
        final File file2 = new File(file.getPath() + File.separator + "index.html");
        new AsyncFileUtils(this.ioExecutor, this.uiExecutor).isFileExistAsync(file2, new AsyncFileUtils.FileExist() {
            public void status(boolean z) {
                if (z) {
                    if (LocalAdPresenter.this.videoTracker != null) {
                        LocalAdPresenter.this.videoTracker.stop();
                    }
                    LocalAdContract.LocalView access$400 = LocalAdPresenter.this.adView;
                    access$400.showWebsite(Advertisement.FILE_SCHEME + file2.getPath());
                    LocalAdPresenter.this.analytics.ping(LocalAdPresenter.this.advertisement.getTpatUrls(AnalyticsEvent.Ad.postrollView));
                    boolean unused = LocalAdPresenter.this.inPost = true;
                    return;
                }
                if (LocalAdPresenter.this.bus != null) {
                    LocalAdPresenter.this.bus.onError(new VungleException(10), LocalAdPresenter.this.placement.getId());
                }
                LocalAdPresenter.this.closeAndReport();
            }
        });
    }

    private void prepare(@Nullable OptionsState optionsState) {
        String str;
        restoreFromSave(optionsState);
        Cookie cookie = this.cookies.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        if (cookie == null) {
            str = null;
        } else {
            str = cookie.getString("userID");
        }
        if (this.report == null) {
            this.adStartTime = System.currentTimeMillis();
            this.report = new Report(this.advertisement, this.placement, this.adStartTime, str);
            this.repository.save(this.report, this.repoCallback);
        }
        this.webViewAPI.setErrorListener(this);
        this.adView.showCTAOverlay(this.advertisement.isCtaOverlayEnabled(), this.advertisement.getCtaClickArea());
        if (this.bus != null) {
            this.bus.onNext("start", null, this.placement.getId());
        }
    }

    private boolean needShowGDPR(@Nullable Cookie cookie) {
        return cookie != null && cookie.getBoolean("is_country_data_protected").booleanValue() && "unknown".equals(cookie.getString("consent_status"));
    }

    private void showGDPR(@NonNull final Cookie cookie) {
        AnonymousClass3 r5 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String str = "opted_out_by_timeout";
                switch (i) {
                    case -2:
                        str = "opted_out";
                        break;
                    case -1:
                        str = "opted_in";
                        break;
                }
                cookie.putValue("consent_status", str);
                cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                cookie.putValue("consent_source", "vungle_modal");
                LocalAdPresenter.this.repository.save(cookie, null);
                LocalAdPresenter.this.start();
            }
        };
        cookie.putValue("consent_status", "opted_out_by_timeout");
        cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
        cookie.putValue("consent_source", "vungle_modal");
        this.repository.save(cookie, this.repoCallback);
        showDialog(cookie.getString("consent_title"), cookie.getString("consent_message"), cookie.getString("button_accept"), cookie.getString("button_deny"), r5);
    }

    public boolean handleExit(@Nullable String str) {
        if (this.inPost) {
            closeAndReport();
            return true;
        } else if (!this.userExitEnabled) {
            return false;
        } else {
            if (!this.placement.isIncentivized() || this.progress > 75) {
                reportAction(AnalyticsEvent.Ad.videoClose, null);
                if (this.advertisement.hasPostroll()) {
                    playPost();
                    return false;
                }
                closeAndReport();
                return true;
            }
            showIncetivizedDialog();
            return false;
        }
    }

    private void showIncetivizedDialog() {
        Cookie cookie = this.cookies.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        String str = this.dialogTitle;
        String str2 = this.dialogBody;
        String str3 = this.dialogContinue;
        String str4 = this.dialogClose;
        if (cookie != null) {
            str = cookie.getString("title") == null ? this.dialogTitle : cookie.getString("title");
            str2 = cookie.getString("body") == null ? this.dialogBody : cookie.getString("body");
            str3 = cookie.getString("continue") == null ? this.dialogContinue : cookie.getString("continue");
            str4 = cookie.getString("close") == null ? this.dialogClose : cookie.getString("close");
        }
        showDialog(str, str2, str3, str4, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == -2) {
                    LocalAdPresenter.this.reportAction(AnalyticsEvent.Ad.videoClose, null);
                    LocalAdPresenter.this.continueWithPostroll();
                }
            }
        });
    }

    private void showDialog(String str, String str2, String str3, String str4, DialogInterface.OnClickListener onClickListener) {
        this.adView.pauseVideo();
        this.adView.showDialog(str, str2, str3, str4, onClickListener);
    }

    /* access modifiers changed from: private */
    public void continueWithPostroll() {
        if (this.advertisement.hasPostroll()) {
            playPost();
        } else {
            closeAndReport();
        }
    }

    private boolean isWebPageBlank() {
        String websiteUrl = this.adView.getWebsiteUrl();
        return TextUtils.isEmpty(websiteUrl) || "about:blank".equalsIgnoreCase(websiteUrl);
    }

    public void start() {
        this.adView.setImmersiveMode();
        this.adView.resumeWeb();
        Cookie cookie = this.cookies.get(Cookie.CONSENT_COOKIE);
        if (needShowGDPR(cookie)) {
            showGDPR(cookie);
        } else if (this.inPost) {
            if (isWebPageBlank()) {
                playPost();
            }
        } else if (!this.adView.isVideoPlaying() && !this.adView.isDialogVisible()) {
            this.adView.playVideo(new File(this.assetDir.getPath() + File.separator + "video"), this.muted, this.videoPosition);
            int showCloseDelay = this.advertisement.getShowCloseDelay(this.placement.isIncentivized());
            if (showCloseDelay > 0) {
                this.scheduler.schedule(new Runnable() {
                    public void run() {
                        boolean unused = LocalAdPresenter.this.userExitEnabled = true;
                        if (!LocalAdPresenter.this.inPost) {
                            LocalAdPresenter.this.adView.showCloseButton();
                        }
                    }
                }, (long) showCloseDelay);
                return;
            }
            this.userExitEnabled = true;
            this.adView.showCloseButton();
        }
    }

    public void stop(boolean z, boolean z2) {
        this.adView.pauseWeb();
        if (this.adView.isVideoPlaying()) {
            this.videoPosition = this.adView.getVideoPosition();
            this.adView.pauseVideo();
        }
        if (z || !z2) {
            if (this.inPost || z2) {
                this.adView.showWebsite("about:blank");
            }
        } else if (!this.isDestroying.getAndSet(true)) {
            String str = null;
            reportAction("close", null);
            this.scheduler.cancelAll();
            if (this.bus != null) {
                AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
                if (this.report.isCTAClicked()) {
                    str = "isCTAClicked";
                }
                eventListener.onNext("end", str, this.placement.getId());
            }
        }
    }

    public void onProgressUpdate(int i, float f) {
        this.progress = (int) ((((float) i) / f) * 100.0f);
        this.videoPosition = i;
        if (this.bus != null) {
            AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
            eventListener.onNext("percentViewed:" + this.progress, null, this.placement.getId());
        }
        reportAction("video_viewed", String.format(Locale.ENGLISH, "%d", Integer.valueOf(i)));
        this.videoTracker.onProgress(this.progress);
        if (this.progress == 100) {
            this.videoTracker.stop();
            if (this.checkpointList.peekLast() != null && this.checkpointList.peekLast().getPercentage() == 100) {
                this.analytics.ping(this.checkpointList.pollLast().getUrls());
            }
            continueWithPostroll();
        }
        this.report.recordProgress(this.videoPosition);
        this.repository.save(this.report, this.repoCallback);
        while (this.checkpointList.peek() != null && this.progress > this.checkpointList.peek().getPercentage()) {
            this.analytics.ping(this.checkpointList.poll().getUrls());
        }
        Cookie cookie = this.cookies.get(Cookie.CONFIG_COOKIE);
        if (this.placement.isIncentivized() && this.progress > 75 && cookie != null && cookie.getBoolean("isReportIncentivizedEnabled").booleanValue() && !this.sendReportIncentivized.getAndSet(true)) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("placement_reference_id", new JsonPrimitive(this.placement.getId()));
            jsonObject.add("app_id", new JsonPrimitive(this.advertisement.getAppID()));
            jsonObject.add(ReportDBAdapter.ReportColumns.COLUMN_AD_START_TIME, new JsonPrimitive((Number) Long.valueOf(this.report.getAdStartTime())));
            jsonObject.add(MentionTypes.USER, new JsonPrimitive(this.report.getUserID()));
            this.analytics.ri(jsonObject);
        }
    }

    public void onVideoStart(int i, float f) {
        reportAction("videoLength", String.format(Locale.ENGLISH, "%d", Integer.valueOf((int) f)));
    }

    public void onMute(boolean z) {
        if (z) {
            reportAction("mute", "true");
        } else {
            reportAction("unmute", "false");
        }
    }

    public void onDownload() {
        download();
    }

    public boolean onMediaError(@NonNull String str) {
        reportError(str);
        return false;
    }

    public void onPrivacy() {
        this.adView.open(HTTPS_VUNGLE_COM_PRIVACY);
    }

    public void generateSaveState(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            this.repository.save(this.report, this.repoCallback);
            optionsState.put(EXTRA_REPORT, this.report == null ? null : this.report.getId());
            optionsState.put(EXTRA_INCENTIVIZED_SENT, this.sendReportIncentivized.get());
            optionsState.put(EXTRA_IN_POST, this.inPost);
            optionsState.put(EXTRA_VIDEO_POSITION, this.adView == null ? this.videoPosition : this.adView.getVideoPosition());
        }
    }

    public void restoreFromSave(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            if (optionsState.getBoolean(EXTRA_INCENTIVIZED_SENT, false)) {
                this.sendReportIncentivized.set(true);
            }
            this.inPost = optionsState.getBoolean(EXTRA_IN_POST, this.inPost);
            this.videoPosition = optionsState.getInt(EXTRA_VIDEO_POSITION, this.videoPosition).intValue();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMraidAction(@android.support.annotation.NonNull java.lang.String r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            r1 = -314498168(0xffffffffed412388, float:-3.7358476E27)
            if (r0 == r1) goto L_0x0028
            r1 = 94756344(0x5a5ddf8, float:1.5598064E-35)
            if (r0 == r1) goto L_0x001e
            r1 = 1427818632(0x551ac888, float:1.06366291E13)
            if (r0 == r1) goto L_0x0014
            goto L_0x0032
        L_0x0014:
            java.lang.String r0 = "download"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 1
            goto L_0x0033
        L_0x001e:
            java.lang.String r0 = "close"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 0
            goto L_0x0033
        L_0x0028:
            java.lang.String r0 = "privacy"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0032
            r0 = 2
            goto L_0x0033
        L_0x0032:
            r0 = -1
        L_0x0033:
            switch(r0) {
                case 0: goto L_0x0054;
                case 1: goto L_0x004d;
                case 2: goto L_0x0057;
                default: goto L_0x0036;
            }
        L_0x0036:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown action "
            r1.append(r2)
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            r0.<init>(r4)
            throw r0
        L_0x004d:
            r3.download()
            r3.closeAndReport()
            goto L_0x0057
        L_0x0054:
            r3.closeAndReport()
        L_0x0057:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.presenter.LocalAdPresenter.onMraidAction(java.lang.String):void");
    }

    private void download() {
        reportAction("cta", "");
        try {
            this.analytics.ping(this.advertisement.getTpatUrls(AnalyticsEvent.Ad.postrollClick));
            this.analytics.ping(this.advertisement.getTpatUrls("click_url"));
            this.analytics.ping(this.advertisement.getTpatUrls("video_click"));
            this.analytics.ping(new String[]{this.advertisement.getCTAURL(true)});
            reportAction(JavascriptBridge.MraidHandler.DOWNLOAD_ACTION, null);
            String ctaurl = this.advertisement.getCTAURL(false);
            if (APKDirectDownloadManager.isDirectDownloadEnabled(this.directDownloadApkEnabled, this.advertisement.isRequiresNonMarketInstall())) {
                APKDirectDownloadManager.download(ctaurl);
            } else {
                this.adView.open(ctaurl);
            }
        } catch (ActivityNotFoundException unused) {
            Log.e(TAG, "Unable to find destination activity");
        }
    }

    /* access modifiers changed from: private */
    public void closeAndReport() {
        if (this.adView.isVideoPlaying()) {
            this.videoTracker.stop();
        }
        if (this.busy.get()) {
            Log.w(TAG, "Busy with closing");
            return;
        }
        this.busy.set(true);
        reportAction("close", null);
        this.scheduler.cancelAll();
        this.report.setAdDuration((int) (System.currentTimeMillis() - this.adStartTime));
        this.adView.close();
    }

    private void loadData(OptionsState optionsState) {
        this.cookies.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookies.put(Cookie.INCENTIVIZED_TEXT_COOKIE, this.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get());
        this.cookies.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookies.put(Cookie.CONFIG_COOKIE, this.repository.load(Cookie.CONFIG_COOKIE, Cookie.class).get());
        if (optionsState != null) {
            String string = optionsState.getString(EXTRA_REPORT);
            Report report2 = TextUtils.isEmpty(string) ? null : (Report) this.repository.load(string, Report.class).get();
            if (report2 != null) {
                this.report = report2;
            }
        }
    }

    public void onReceivedError(String str) {
        if (this.report != null) {
            this.report.recordError(str);
            this.repository.save(this.report, this.repoCallback);
        }
    }
}
