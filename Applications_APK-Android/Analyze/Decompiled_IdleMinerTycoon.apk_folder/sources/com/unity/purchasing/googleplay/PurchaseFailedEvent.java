package com.unity.purchasing.googleplay;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.unity.purchasing.common.PurchaseFailureReason;
import com.unity.purchasing.common.SaneJSONObject;

public class PurchaseFailedEvent {
    public static String jsonEncodePurchaseFailure(String str, PurchaseFailureReason purchaseFailureReason, String str2) {
        SaneJSONObject saneJSONObject = new SaneJSONObject();
        saneJSONObject.put("productId", str);
        saneJSONObject.put(IronSourceConstants.EVENTS_ERROR_REASON, purchaseFailureReason);
        saneJSONObject.put("message", str2);
        return saneJSONObject.toString();
    }
}
