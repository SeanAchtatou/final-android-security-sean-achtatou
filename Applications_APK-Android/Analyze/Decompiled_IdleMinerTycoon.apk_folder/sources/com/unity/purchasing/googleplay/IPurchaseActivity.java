package com.unity.purchasing.googleplay;

import android.content.Intent;

public interface IPurchaseActivity {
    void processActivityResult(int i, int i2, Intent intent);
}
