package com.unity.purchasing.common;

import com.helpshift.support.search.storage.TableSearchToken;
import java.math.BigDecimal;

public class ProductMetadata {
    public final String isoCurrencyCode;
    public final String localizedDescription;
    public final BigDecimal localizedPrice;
    public final String localizedPriceString;
    public final String localizedTitle;

    public ProductMetadata(String str, String str2, String str3, String str4, BigDecimal bigDecimal) {
        this.localizedPriceString = str == null ? "" : str;
        this.localizedTitle = str2;
        this.localizedDescription = str3;
        this.isoCurrencyCode = str4;
        this.localizedPrice = bigDecimal;
    }

    public String toString() {
        return "{ProductMetadata: localizedPriceString = " + this.localizedPriceString + TableSearchToken.COMMA_SEP + "localizedTitle = " + this.localizedTitle + TableSearchToken.COMMA_SEP + "localizedDescription = " + this.localizedDescription + TableSearchToken.COMMA_SEP + "isoCurrencyCode = " + this.isoCurrencyCode + TableSearchToken.COMMA_SEP + "localizedPrice = " + this.localizedPrice + TableSearchToken.COMMA_SEP + "}";
    }
}
