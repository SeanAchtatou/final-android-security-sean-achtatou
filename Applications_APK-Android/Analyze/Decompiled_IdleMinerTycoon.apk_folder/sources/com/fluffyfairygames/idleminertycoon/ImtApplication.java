package com.fluffyfairygames.idleminertycoon;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import io.fabric.unity.android.FabricInitializer;

public class ImtApplication extends Application {
    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void onCreate() {
        super.onCreate();
        FabricInitializer.initializeFabric(this, FabricInitializer.Caller.Android);
    }
}
