package com.fluffyfairygames.idleminertycoon;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.fluffyfairygames.idleminertycoon";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 39226;
    public static final String VERSION_NAME = "2.86.0";
}
