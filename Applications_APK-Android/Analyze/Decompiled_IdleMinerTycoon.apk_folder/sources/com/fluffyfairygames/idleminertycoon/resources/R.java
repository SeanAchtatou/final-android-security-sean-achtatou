package com.fluffyfairygames.idleminertycoon.resources;

public final class R {
    private R() {
    }

    public static final class drawable {
        public static final int app_icon = 2131165285;
        public static final int ic_background = 2131165386;
        public static final int ic_foreground = 2131165387;

        private drawable() {
        }
    }

    public static final class layout {
        public static final int stub = 2131427479;

        private layout() {
        }
    }

    public static final class xml {
        public static final int network_security_config = 2131886081;

        private xml() {
        }
    }
}
