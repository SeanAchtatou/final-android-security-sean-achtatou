package com.moat.analytics.mobile.iro;

import android.util.Log;

class n extends Exception {
    private static final Long a = 60000L;
    private static Long b;
    private static Exception c = null;

    n(String str) {
        super(str);
    }

    static String a(String str, Exception exc) {
        if (exc instanceof n) {
            return str + " failed: " + exc.getMessage();
        }
        return str + " failed unexpectedly";
    }

    static void a() {
        if (c != null) {
            b(c);
            c = null;
        }
    }

    static void a(Exception exc) {
        if (w.a().b) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            b(exc);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x019a A[Catch:{ Exception -> 0x01bc }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(java.lang.Exception r12) {
        /*
            com.moat.analytics.mobile.iro.w r0 = com.moat.analytics.mobile.iro.w.a()     // Catch:{ Exception -> 0x01bc }
            com.moat.analytics.mobile.iro.w$d r0 = r0.a     // Catch:{ Exception -> 0x01bc }
            com.moat.analytics.mobile.iro.w$d r1 = com.moat.analytics.mobile.iro.w.d.ON     // Catch:{ Exception -> 0x01bc }
            if (r0 != r1) goto L_0x01ba
            com.moat.analytics.mobile.iro.w r0 = com.moat.analytics.mobile.iro.w.a()     // Catch:{ Exception -> 0x01bc }
            int r0 = r0.e     // Catch:{ Exception -> 0x01bc }
            if (r0 != 0) goto L_0x0013
            return
        L_0x0013:
            r1 = 100
            if (r0 >= r1) goto L_0x0027
            double r1 = (double) r0
            r3 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r1)
            double r1 = r1 / r3
            double r3 = java.lang.Math.random()     // Catch:{ Exception -> 0x01bc }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0027
            return
        L_0x0027:
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = "https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r6.<init>(r5)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r5.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r7 = "&zt="
            r5.append(r7)     // Catch:{ Exception -> 0x01bc }
            boolean r7 = r12 instanceof com.moat.analytics.mobile.iro.n     // Catch:{ Exception -> 0x01bc }
            r8 = 1
            r9 = 0
            if (r7 == 0) goto L_0x0048
            r7 = 1
            goto L_0x0049
        L_0x0048:
            r7 = 0
        L_0x0049:
            r5.append(r7)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r5)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r5.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r7 = "&zr="
            r5.append(r7)     // Catch:{ Exception -> 0x01bc }
            r5.append(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c0 }
            r0.<init>()     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r5 = "&zm="
            r0.append(r5)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00c0 }
            if (r5 != 0) goto L_0x007a
            java.lang.String r5 = "null"
            goto L_0x008e
        L_0x007a:
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r7 = "UTF-8"
            byte[] r5 = r5.getBytes(r7)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r5 = android.util.Base64.encodeToString(r5, r9)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r7 = "UTF-8"
            java.lang.String r5 = java.net.URLEncoder.encode(r5, r7)     // Catch:{ Exception -> 0x00c0 }
        L_0x008e:
            r0.append(r5)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00c0 }
            r6.append(r0)     // Catch:{ Exception -> 0x00c0 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c0 }
            r0.<init>()     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r5 = "&k="
            r0.append(r5)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r12 = android.util.Log.getStackTraceString(r12)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r5 = "UTF-8"
            byte[] r12 = r12.getBytes(r5)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r12 = android.util.Base64.encodeToString(r12, r9)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r12 = java.net.URLEncoder.encode(r12, r5)     // Catch:{ Exception -> 0x00c0 }
            r0.append(r12)     // Catch:{ Exception -> 0x00c0 }
            java.lang.String r12 = r0.toString()     // Catch:{ Exception -> 0x00c0 }
            r6.append(r12)     // Catch:{ Exception -> 0x00c0 }
        L_0x00c0:
            java.lang.String r12 = "IRO"
            java.lang.String r0 = "&zMoatMMAKv=c61b082e4837b5ed783132b87857decbf9e39340"
            r6.append(r0)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r0 = "2.6.0"
            com.moat.analytics.mobile.iro.s$a r1 = com.moat.analytics.mobile.iro.s.c()     // Catch:{ Exception -> 0x00f0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f0 }
            r3.<init>()     // Catch:{ Exception -> 0x00f0 }
            java.lang.String r5 = "&zMoatMMAKan="
            r3.append(r5)     // Catch:{ Exception -> 0x00f0 }
            java.lang.String r5 = r1.a()     // Catch:{ Exception -> 0x00f0 }
            r3.append(r5)     // Catch:{ Exception -> 0x00f0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00f0 }
            r6.append(r3)     // Catch:{ Exception -> 0x00f0 }
            java.lang.String r1 = r1.b()     // Catch:{ Exception -> 0x00f0 }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00f5 }
            goto L_0x00f6
        L_0x00f0:
            r1 = r2
            goto L_0x00f5
        L_0x00f2:
            r12 = r1
        L_0x00f3:
            r1 = r2
            r0 = r3
        L_0x00f5:
            r2 = r4
        L_0x00f6:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r3.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r4 = "&d=Android:"
            r3.append(r4)     // Catch:{ Exception -> 0x01bc }
            r3.append(r12)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r12 = ":"
            r3.append(r12)     // Catch:{ Exception -> 0x01bc }
            r3.append(r1)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r12 = ":-"
            r3.append(r12)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r12 = r3.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r12)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r12.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r1 = "&bo="
            r12.append(r1)     // Catch:{ Exception -> 0x01bc }
            r12.append(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r12)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r12.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r0 = "&bd="
            r12.append(r0)     // Catch:{ Exception -> 0x01bc }
            r12.append(r2)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r12)     // Catch:{ Exception -> 0x01bc }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01bc }
            java.lang.Long r12 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r0.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r1 = "&t="
            r0.append(r1)     // Catch:{ Exception -> 0x01bc }
            r0.append(r12)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bc }
            r0.<init>()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r1 = "&de="
            r0.append(r1)     // Catch:{ Exception -> 0x01bc }
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ Exception -> 0x01bc }
            java.lang.String r2 = "%.0f"
            java.lang.Object[] r3 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01bc }
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x01bc }
            r7 = 4621819117588971520(0x4024000000000000, double:10.0)
            r10 = 4622945017495814144(0x4028000000000000, double:12.0)
            double r7 = java.lang.Math.pow(r7, r10)     // Catch:{ Exception -> 0x01bc }
            double r4 = r4 * r7
            double r4 = java.lang.Math.floor(r4)     // Catch:{ Exception -> 0x01bc }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x01bc }
            r3[r9] = r4     // Catch:{ Exception -> 0x01bc }
            java.lang.String r1 = java.lang.String.format(r1, r2, r3)     // Catch:{ Exception -> 0x01bc }
            r0.append(r1)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01bc }
            r6.append(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.String r0 = "&cs=0"
            r6.append(r0)     // Catch:{ Exception -> 0x01bc }
            java.lang.Long r0 = com.moat.analytics.mobile.iro.n.b     // Catch:{ Exception -> 0x01bc }
            if (r0 == 0) goto L_0x01b0
            long r0 = r12.longValue()     // Catch:{ Exception -> 0x01bc }
            java.lang.Long r2 = com.moat.analytics.mobile.iro.n.b     // Catch:{ Exception -> 0x01bc }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x01bc }
            r4 = 0
            long r0 = r0 - r2
            java.lang.Long r2 = com.moat.analytics.mobile.iro.n.a     // Catch:{ Exception -> 0x01bc }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x01bc }
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x01bc
        L_0x01b0:
            java.lang.String r0 = r6.toString()     // Catch:{ Exception -> 0x01bc }
            com.moat.analytics.mobile.iro.q.b(r0)     // Catch:{ Exception -> 0x01bc }
            com.moat.analytics.mobile.iro.n.b = r12     // Catch:{ Exception -> 0x01bc }
            goto L_0x01bc
        L_0x01ba:
            com.moat.analytics.mobile.iro.n.c = r12     // Catch:{ Exception -> 0x01bc }
        L_0x01bc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.iro.n.b(java.lang.Exception):void");
    }
}
