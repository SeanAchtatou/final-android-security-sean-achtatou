package com.moat.analytics.mobile.cha;

import com.ironsource.sdk.constants.Constants;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(0.0d);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ  reason: contains not printable characters */
    static final Integer f257 = Integer.MIN_VALUE;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final Double f258 = Double.valueOf(Double.NaN);

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Long f259;

    /* renamed from: ˊ  reason: contains not printable characters */
    Double f260;

    /* renamed from: ˏ  reason: contains not printable characters */
    Integer f261;

    /* renamed from: ॱ  reason: contains not printable characters */
    MoatAdEventType f262;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Double f263;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f259 = Long.valueOf(System.currentTimeMillis());
        this.f262 = moatAdEventType;
        this.f260 = d;
        this.f261 = num;
        this.f263 = Double.valueOf(r.m383());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f258);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f257, f258);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final Map<String, Object> m229() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f260);
        hashMap.put("playhead", this.f261);
        hashMap.put("aTimeStamp", this.f259);
        hashMap.put("type", this.f262.toString());
        hashMap.put(Constants.RequestParameters.DEVICE_VOLUME, this.f263);
        return hashMap;
    }
}
