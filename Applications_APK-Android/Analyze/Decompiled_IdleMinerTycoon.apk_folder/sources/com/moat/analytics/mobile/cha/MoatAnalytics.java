package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.cha.NoOp;

public abstract class MoatAnalytics {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static MoatAnalytics f266;

    @UiThread
    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f266 == null) {
                try {
                    f266 = new f();
                } catch (Exception e) {
                    o.m359(e);
                    f266 = new NoOp.MoatAnalytics();
                }
            }
            moatAnalytics = f266;
        }
        return moatAnalytics;
    }
}
