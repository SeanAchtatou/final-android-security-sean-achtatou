package com.moat.analytics.mobile.cha.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Optional<?> f291 = new Optional<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final T f292;

    private Optional() {
        this.f292 = null;
    }

    public static <T> Optional<T> empty() {
        return f291;
    }

    private Optional(T t) {
        if (t != null) {
            this.f292 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        if (this.f292 != null) {
            return this.f292;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f292 != null;
    }

    public final T orElse(T t) {
        return this.f292 != null ? this.f292 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        Optional optional = (Optional) obj;
        return this.f292 == optional.f292 || !(this.f292 == null || optional.f292 == null || !this.f292.equals(optional.f292));
    }

    public final int hashCode() {
        if (this.f292 == null) {
            return 0;
        }
        return this.f292.hashCode();
    }

    public final String toString() {
        if (this.f292 == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", this.f292);
    }
}
