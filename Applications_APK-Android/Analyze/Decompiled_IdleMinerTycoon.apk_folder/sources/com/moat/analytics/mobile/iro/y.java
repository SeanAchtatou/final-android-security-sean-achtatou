package com.moat.analytics.mobile.iro;

import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

class y extends c implements ReactiveVideoTracker {
    private Integer l;

    public y(String str) {
        super(str);
        p.a(3, "ReactiveVideoTracker", this, "Initializing.");
        p.a("[SUCCESS] ", a() + " created");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "ReactiveVideoTracker";
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.b.equals(MoatAdEvent.a) && !a(moatAdEvent.b, this.l)) {
            moatAdEvent.d = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.a(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    public void a(List<String> list) {
        if (this.l.intValue() >= 1000) {
            super.a(list);
            return;
        }
        throw new n(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", this.l));
    }

    /* access modifiers changed from: package-private */
    public Map<String, Object> i() {
        HashMap hashMap = new HashMap();
        View view = (View) this.k.get();
        int i = 0;
        int i2 = 0;
        if (view != null) {
            i = Integer.valueOf(view.getWidth());
            i2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put(IronSourceConstants.EVENTS_DURATION, this.l);
        hashMap.put("width", i);
        hashMap.put("height", i2);
        return hashMap;
    }

    public boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            c();
            d();
            this.l = num;
            return super.a(map, view);
        } catch (Exception e) {
            a("trackVideoAd", e);
            return false;
        }
    }
}
