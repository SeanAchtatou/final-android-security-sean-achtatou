package com.moat.analytics.mobile.cha;

import java.util.Iterator;
import java.util.LinkedHashSet;

final class x {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final LinkedHashSet<String> f481 = new LinkedHashSet<>();

    x() {
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [android.view.View, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @android.support.annotation.NonNull
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.moat.analytics.mobile.cha.base.functional.Optional<android.webkit.WebView> m431(android.view.ViewGroup r11, boolean r12) {
        /*
            if (r11 != 0) goto L_0x0007
            com.moat.analytics.mobile.cha.base.functional.Optional r11 = com.moat.analytics.mobile.cha.base.functional.Optional.empty()     // Catch:{ Exception -> 0x0081 }
            return r11
        L_0x0007:
            boolean r0 = r11 instanceof android.webkit.WebView     // Catch:{ Exception -> 0x0081 }
            if (r0 == 0) goto L_0x0012
            android.webkit.WebView r11 = (android.webkit.WebView) r11     // Catch:{ Exception -> 0x0081 }
            com.moat.analytics.mobile.cha.base.functional.Optional r11 = com.moat.analytics.mobile.cha.base.functional.Optional.of(r11)     // Catch:{ Exception -> 0x0081 }
            return r11
        L_0x0012:
            java.util.LinkedList r0 = new java.util.LinkedList     // Catch:{ Exception -> 0x0081 }
            r0.<init>()     // Catch:{ Exception -> 0x0081 }
            r0.add(r11)     // Catch:{ Exception -> 0x0081 }
            r11 = 0
            r1 = 0
            r3 = r1
            r2 = 0
        L_0x001e:
            boolean r4 = r0.isEmpty()     // Catch:{ Exception -> 0x0081 }
            if (r4 != 0) goto L_0x007c
            r4 = 100
            if (r2 >= r4) goto L_0x007c
            int r2 = r2 + 1
            java.lang.Object r4 = r0.poll()     // Catch:{ Exception -> 0x0081 }
            android.view.ViewGroup r4 = (android.view.ViewGroup) r4     // Catch:{ Exception -> 0x0081 }
            int r5 = r4.getChildCount()     // Catch:{ Exception -> 0x0081 }
            r6 = r3
            r3 = 0
        L_0x0036:
            if (r3 >= r5) goto L_0x007a
            android.view.View r7 = r4.getChildAt(r3)     // Catch:{ Exception -> 0x0081 }
            boolean r8 = r7 instanceof android.webkit.WebView     // Catch:{ Exception -> 0x0081 }
            if (r8 == 0) goto L_0x006e
            java.lang.String r8 = "WebViewHound"
            java.lang.String r9 = "Found WebView"
            r10 = 3
            com.moat.analytics.mobile.cha.a.m235(r10, r8, r7, r9)     // Catch:{ Exception -> 0x0081 }
            if (r12 != 0) goto L_0x0058
            int r8 = r7.hashCode()     // Catch:{ Exception -> 0x0081 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0081 }
            boolean r8 = m432(r8)     // Catch:{ Exception -> 0x0081 }
            if (r8 == 0) goto L_0x006e
        L_0x0058:
            if (r6 != 0) goto L_0x005e
            r6 = r7
            android.webkit.WebView r6 = (android.webkit.WebView) r6     // Catch:{ Exception -> 0x0081 }
            goto L_0x006e
        L_0x005e:
            java.lang.String r3 = "WebViewHound"
            java.lang.String r4 = "Ambiguous ad container: multiple WebViews reside within it."
            com.moat.analytics.mobile.cha.a.m235(r10, r3, r7, r4)     // Catch:{ Exception -> 0x0081 }
            java.lang.String r3 = "[ERROR] "
            java.lang.String r4 = "WebAdTracker not created, ambiguous ad container: multiple WebViews reside within it"
            com.moat.analytics.mobile.cha.a.m232(r3, r4)     // Catch:{ Exception -> 0x0081 }
            r3 = r1
            goto L_0x001e
        L_0x006e:
            boolean r8 = r7 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x0081 }
            if (r8 == 0) goto L_0x0077
            android.view.ViewGroup r7 = (android.view.ViewGroup) r7     // Catch:{ Exception -> 0x0081 }
            r0.add(r7)     // Catch:{ Exception -> 0x0081 }
        L_0x0077:
            int r3 = r3 + 1
            goto L_0x0036
        L_0x007a:
            r3 = r6
            goto L_0x001e
        L_0x007c:
            com.moat.analytics.mobile.cha.base.functional.Optional r11 = com.moat.analytics.mobile.cha.base.functional.Optional.ofNullable(r3)     // Catch:{ Exception -> 0x0081 }
            return r11
        L_0x0081:
            com.moat.analytics.mobile.cha.base.functional.Optional r11 = com.moat.analytics.mobile.cha.base.functional.Optional.empty()
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.x.m431(android.view.ViewGroup, boolean):com.moat.analytics.mobile.cha.base.functional.Optional");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m432(String str) {
        try {
            boolean add = f481.add(str);
            if (f481.size() > 50) {
                Iterator<String> it = f481.iterator();
                for (int i = 0; i < 25 && it.hasNext(); i++) {
                    it.next();
                    it.remove();
                }
            }
            a.m235(3, "WebViewHound", null, add ? "Newly Found WebView" : "Already Found WebView");
            return add;
        } catch (Exception e) {
            o.m359(e);
            return false;
        }
    }
}
