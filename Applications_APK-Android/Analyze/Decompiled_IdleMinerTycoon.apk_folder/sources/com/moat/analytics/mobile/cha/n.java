package com.moat.analytics.mobile.cha;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class n implements LocationListener {

    /* renamed from: ˎ  reason: contains not printable characters */
    private static n f388;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Location f389;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ScheduledExecutorService f390;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f391;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ScheduledFuture<?> f392;

    /* renamed from: ˏ  reason: contains not printable characters */
    private ScheduledFuture<?> f393;

    /* renamed from: ॱ  reason: contains not printable characters */
    private LocationManager f394;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private boolean f395;

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static n m345() {
        if (f388 == null) {
            f388 = new n();
        }
        return f388;
    }

    private n() {
        try {
            this.f391 = ((f) MoatAnalytics.getInstance()).f315;
            if (this.f391) {
                a.m235(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f390 = Executors.newScheduledThreadPool(1);
            this.f394 = (LocationManager) c.m256().getSystemService(FirebaseAnalytics.Param.LOCATION);
            if (this.f394.getAllProviders().size() == 0) {
                a.m235(3, "LocationManager", this, "Device has no location providers");
            } else {
                m343();
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: ˊ  reason: contains not printable characters */
    public final Location m354() {
        if (this.f391 || this.f394 == null) {
            return null;
        }
        return this.f389;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m355() {
        m343();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m356() {
        m350(false);
    }

    public final void onLocationChanged(Location location) {
        try {
            a.m235(3, "LocationManager", this, "Received an updated location = " + location.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f389 = m348(this.f389, location);
                a.m235(3, "LocationManager", this, "fetchCompleted");
                m350(true);
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m343() {
        try {
            if (this.f391) {
                return;
            }
            if (this.f394 != null) {
                if (this.f395) {
                    a.m235(3, "LocationManager", this, "already updating location");
                }
                a.m235(3, "LocationManager", this, "starting location fetch");
                this.f389 = m348(this.f389, m340());
                if (this.f389 != null) {
                    a.m235(3, "LocationManager", this, "Have a valid location, won't fetch = " + this.f389.toString());
                    m346();
                    return;
                }
                m338();
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public void m350(boolean z) {
        try {
            a.m235(3, "LocationManager", this, "stopping location fetch");
            m339();
            m342();
            if (z) {
                m346();
            } else {
                m353();
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private Location m340() {
        Location location;
        try {
            boolean r1 = m347();
            boolean r2 = m352();
            if (r1 && r2) {
                location = m348(this.f394.getLastKnownLocation("gps"), this.f394.getLastKnownLocation("network"));
            } else if (r1) {
                location = this.f394.getLastKnownLocation("gps");
            } else if (!r2) {
                return null;
            } else {
                location = this.f394.getLastKnownLocation("network");
            }
            return location;
        } catch (SecurityException e) {
            o.m359(e);
            return null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m338() {
        try {
            if (!this.f395) {
                a.m235(3, "LocationManager", this, "Attempting to start update");
                if (m347()) {
                    a.m235(3, "LocationManager", this, "start updating gps location");
                    this.f394.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f395 = true;
                }
                if (m352()) {
                    a.m235(3, "LocationManager", this, "start updating network location");
                    this.f394.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f395 = true;
                }
                if (this.f395) {
                    m342();
                    this.f393 = this.f390.schedule(new Runnable() {
                        public final void run() {
                            try {
                                a.m235(3, "LocationManager", this, "fetchTimedOut");
                                n.this.m350(true);
                            } catch (Exception e) {
                                o.m359(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            o.m359(e);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m339() {
        try {
            a.m235(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.checkSelfPermission(c.m256().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.checkSelfPermission(c.m256().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f394 != null) {
                this.f394.removeUpdates(this);
                this.f395 = false;
            }
        } catch (SecurityException e) {
            o.m359(e);
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private void m342() {
        if (this.f393 != null && !this.f393.isCancelled()) {
            this.f393.cancel(true);
            this.f393 = null;
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private void m353() {
        if (this.f392 != null && !this.f392.isCancelled()) {
            this.f392.cancel(true);
            this.f392 = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* renamed from: ˏॱ  reason: contains not printable characters */
    private void m346() {
        a.m235(3, "LocationManager", this, "Resetting fetch timer");
        m353();
        float f = 600.0f;
        if (this.f389 != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - this.f389.getTime()) / 1000)), 0.0f);
        }
        this.f392 = this.f390.schedule(new Runnable() {
            public final void run() {
                try {
                    a.m235(3, "LocationManager", this, "fetchTimerCompleted");
                    n.this.m343();
                } catch (Exception e) {
                    o.m359(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Location m348(Location location, Location location2) {
        boolean r0 = m351(location);
        boolean r1 = m351(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static boolean m351(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != 0.0d || location.getLongitude() != 0.0d) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static boolean m344(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean m347() {
        return (ContextCompat.checkSelfPermission(c.m256().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f394.getProvider("gps") != null && this.f394.isProviderEnabled("gps");
    }

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private boolean m352() {
        boolean z;
        if (!(ContextCompat.checkSelfPermission(c.m256().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
            if (!(ContextCompat.checkSelfPermission(c.m256().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                z = false;
                return !z && this.f394.getProvider("network") != null && this.f394.isProviderEnabled("network");
            }
        }
        z = true;
        if (!z) {
        }
    }
}
