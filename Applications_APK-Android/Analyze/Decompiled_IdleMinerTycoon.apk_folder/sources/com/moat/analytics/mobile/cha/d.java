package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants;
import com.moat.analytics.mobile.cha.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class d {

    /* renamed from: ʻ  reason: contains not printable characters */
    private WeakReference<View> f298;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f299;

    /* renamed from: ʽ  reason: contains not printable characters */
    final boolean f300;

    /* renamed from: ˊ  reason: contains not printable characters */
    TrackerListener f301;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f302;

    /* renamed from: ˋ  reason: contains not printable characters */
    final String f303;

    /* renamed from: ˎ  reason: contains not printable characters */
    j f304;

    /* renamed from: ˏ  reason: contains not printable characters */
    WeakReference<WebView> f305;

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean f306;

    /* renamed from: ॱ  reason: contains not printable characters */
    o f307 = null;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final u f308;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public abstract String m265();

    d(@Nullable View view, boolean z, boolean z2) {
        String str;
        a.m235(3, "BaseTracker", this, "Initializing.");
        if (z) {
            str = "m" + hashCode();
        } else {
            str = "";
        }
        this.f303 = str;
        this.f298 = new WeakReference<>(view);
        this.f299 = z;
        this.f300 = z2;
        this.f302 = false;
        this.f306 = false;
        this.f308 = new u();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f301 = trackerListener;
    }

    public void removeListener() {
        this.f301 = null;
    }

    public void startTracking() {
        try {
            a.m235(3, "BaseTracker", this, "In startTracking method.");
            m268();
            if (this.f301 != null) {
                this.f301.onTrackingStarted("Tracking started on " + a.m234(this.f298.get()));
            }
            String str = "startTracking succeeded for " + a.m234(this.f298.get());
            a.m235(3, "BaseTracker", this, str);
            a.m232("[SUCCESS] ", m265() + " " + str);
        } catch (Exception e) {
            m271("startTracking", e);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            a.m235(3, "BaseTracker", this, "In stopTracking method.");
            this.f306 = true;
            if (this.f304 != null) {
                this.f304.m325(this);
                z = true;
            }
        } catch (Exception e) {
            o.m359(e);
        }
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        a.m235(3, "BaseTracker", this, sb.toString());
        String str = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m265());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : Constants.ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(a.m234(this.f298.get()));
        a.m232(str, sb2.toString());
        if (this.f301 != null) {
            this.f301.onTrackingStopped("");
            this.f301 = null;
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        a.m235(3, "BaseTracker", this, "changing view to " + a.m234(view));
        this.f298 = new WeakReference<>(view);
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m268() throws o {
        a.m235(3, "BaseTracker", this, "Attempting to start impression.");
        m267();
        if (this.f302) {
            throw new o("Tracker already started");
        } else if (!this.f306) {
            m266(new ArrayList());
            if (this.f304 != null) {
                this.f304.m326(this);
                this.f302 = true;
                a.m235(3, "BaseTracker", this, "Impression started.");
                return;
            }
            a.m235(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        } else {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m270(WebView webView) throws o {
        if (webView != null) {
            this.f305 = new WeakReference<>(webView);
            if (this.f304 == null) {
                if (!(this.f299 || this.f300)) {
                    a.m235(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f305.get() != null) {
                        this.f304 = new j(this.f305.get(), j.e.f377);
                        a.m235(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f304 = null;
                        a.m235(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            if (this.f304 != null) {
                this.f304.m324(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m267() throws o {
        if (this.f307 != null) {
            throw new o("Tracker initialization failed: " + this.f307.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m264() {
        return this.f302 && !this.f306;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final View m262() {
        return this.f298.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m263() {
        this.f308.m425(this.f303, this.f298.get());
        return this.f308.f470;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m271(String str, Exception exc) {
        try {
            o.m359(exc);
            String r3 = o.m358(str, exc);
            if (this.f301 != null) {
                this.f301.onTrackingFailedToStart(r3);
            }
            a.m235(3, "BaseTracker", this, r3);
            a.m232("[ERROR] ", m265() + " " + r3);
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m269() throws o {
        if (this.f302) {
            throw new o("Tracker already started");
        } else if (this.f306) {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m266(List<String> list) throws o {
        if (this.f298.get() == null && !this.f300) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m261() {
        return a.m234(this.f298.get());
    }
}
