package com.moat.analytics.mobile.cha;

import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class w extends b implements ReactiveVideoTracker {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private Integer f480;

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m427() {
        return "ReactiveVideoTracker";
    }

    public w(String str) {
        super(str);
        a.m235(3, "ReactiveVideoTracker", this, "Initializing.");
        a.m232("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m430() throws o {
        HashMap hashMap = new HashMap();
        View view = (View) this.f282.get();
        int i = 0;
        int i2 = 0;
        if (view != null) {
            i = Integer.valueOf(view.getWidth());
            i2 = Integer.valueOf(view.getHeight());
        }
        hashMap.put(IronSourceConstants.EVENTS_DURATION, this.f480);
        hashMap.put("width", i);
        hashMap.put("height", i2);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m267();
            m269();
            this.f480 = num;
            return super.m250(map, view);
        } catch (Exception e) {
            m271("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m429(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f262 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f261.equals(MoatAdEvent.f257) && !m242(moatAdEvent.f261, this.f480)) {
            moatAdEvent.f262 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m247(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m428(List<String> list) throws o {
        if (this.f480.intValue() >= 1000) {
            super.m245(list);
            return;
        }
        throw new o(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", this.f480));
    }
}
