package com.moat.analytics.mobile.cha;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class h {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final h f330 = new h();
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public ScheduledFuture<?> f331;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final Map<j, String> f332 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public ScheduledFuture<?> f333;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final ScheduledExecutorService f334 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public final Map<d, String> f335 = new WeakHashMap();

    /* renamed from: ˊ  reason: contains not printable characters */
    static h m287() {
        return f330;
    }

    private h() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m293(final Context context, j jVar) {
        if (jVar != null) {
            this.f332.put(jVar, "");
            if (this.f331 == null || this.f331.isDone()) {
                a.m235(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
                this.f331 = this.f334.scheduleWithFixedDelay(new Runnable() {
                    public final void run() {
                        try {
                            LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_METADATA"));
                            if (h.this.f332.isEmpty()) {
                                h.this.f331.cancel(true);
                            }
                        } catch (Exception e) {
                            o.m359(e);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m292(j jVar) {
        if (jVar != null) {
            a.m235(3, "JSUpdateLooper", this, "removeSetupNeededBridge" + jVar.hashCode());
            this.f332.remove(jVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m294(final Context context, d dVar) {
        if (dVar != null) {
            a.m235(3, "JSUpdateLooper", this, "addActiveTracker" + dVar.hashCode());
            if (!this.f335.containsKey(dVar)) {
                this.f335.put(dVar, "");
                if (this.f333 == null || this.f333.isDone()) {
                    a.m235(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f333 = this.f334.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(new Intent("UPDATE_VIEW_INFO"));
                                if (h.this.f335.isEmpty()) {
                                    a.m235(3, "JSUpdateLooper", h.this, "No more active trackers");
                                    h.this.f333.cancel(true);
                                }
                            } catch (Exception e) {
                                o.m359(e);
                            }
                        }
                    }, 0, (long) t.m403().f435, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m295(d dVar) {
        if (dVar != null) {
            a.m235(3, "JSUpdateLooper", this, "removeActiveTracker" + dVar.hashCode());
            this.f335.remove(dVar);
        }
    }
}
