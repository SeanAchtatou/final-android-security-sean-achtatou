package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class c {
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean f293 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Application f294 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static boolean f295 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    static WeakReference<Activity> f296;
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public static int f297;

    c() {
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m258(Application application) {
        f294 = application;
        if (!f295) {
            f295 = true;
            f294.registerActivityLifecycleCallbacks(new a());
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Application m256() {
        return f294;
    }

    static class a implements Application.ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            int unused = c.f297 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                c.f296 = new WeakReference<>(activity);
                int unused = c.f297 = 2;
                if (!c.f293) {
                    m260(true);
                }
                boolean unused2 = c.f293 = true;
                a.m235(3, "ActivityState", this, "Activity started: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m359(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                c.f296 = new WeakReference<>(activity);
                int unused = c.f297 = 3;
                t.m403().m409();
                a.m235(3, "ActivityState", this, "Activity resumed: " + activity.getClass() + "@" + activity.hashCode());
                if (((f) MoatAnalytics.getInstance()).f317) {
                    e.m272(activity);
                }
            } catch (Exception e) {
                o.m359(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                int unused = c.f297 = 4;
                if (c.m254(activity)) {
                    c.f296 = new WeakReference<>(null);
                }
                a.m235(3, "ActivityState", this, "Activity paused: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m359(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (c.f297 != 3) {
                    boolean unused = c.f293 = false;
                    m260(false);
                }
                int unused2 = c.f297 = 5;
                if (c.m254(activity)) {
                    c.f296 = new WeakReference<>(null);
                }
                a.m235(3, "ActivityState", this, "Activity stopped: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m359(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(c.f297 == 3 || c.f297 == 5)) {
                    if (c.f293) {
                        m260(false);
                    }
                    boolean unused = c.f293 = false;
                }
                int unused2 = c.f297 = 6;
                a.m235(3, "ActivityState", this, "Activity destroyed: " + activity.getClass() + "@" + activity.hashCode());
                if (c.m254(activity)) {
                    c.f296 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                o.m359(e);
            }
        }

        /* renamed from: ॱ  reason: contains not printable characters */
        private static void m260(boolean z) {
            if (z) {
                a.m235(3, "ActivityState", null, "App became visible");
                if (t.m403().f439 == t.a.f450 && !((f) MoatAnalytics.getInstance()).f315) {
                    n.m345().m355();
                    return;
                }
                return;
            }
            a.m235(3, "ActivityState", null, "App became invisible");
            if (t.m403().f439 == t.a.f450 && !((f) MoatAnalytics.getInstance()).f315) {
                n.m345().m356();
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static /* synthetic */ boolean m254(Activity activity) {
        return f296 != null && f296.get() == activity;
    }
}
