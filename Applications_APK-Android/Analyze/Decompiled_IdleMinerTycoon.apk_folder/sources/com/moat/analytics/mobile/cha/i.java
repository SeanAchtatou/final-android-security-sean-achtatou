package com.moat.analytics.mobile.cha;

import android.support.annotation.CallSuper;
import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class i extends b {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private int f340 = c.f350;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private int f341 = Integer.MIN_VALUE;

    /* renamed from: ͺ  reason: contains not printable characters */
    private int f342 = Integer.MIN_VALUE;

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private double f343 = Double.NaN;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private int f344 = Integer.MIN_VALUE;

    /* renamed from: ॱˎ  reason: contains not printable characters */
    private int f345 = 0;

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public abstract boolean m298();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public abstract Integer m300();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public abstract boolean m301();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public abstract Integer m302();

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class c extends Enum<c> {

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final int f347 = 2;

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f348 = 4;

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f349 = 3;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f350 = 1;

        /* renamed from: ॱ  reason: contains not printable characters */
        public static final int f351 = 5;

        static {
            int[] iArr = {1, 2, 3, 4, 5};
        }
    }

    i(String str) {
        super(str);
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public final boolean m299(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m250(map, view);
            if (!r4) {
                return r4;
            }
            this.f289.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!i.this.m298() || i.this.m246()) {
                            i.this.m249();
                        } else if (Boolean.valueOf(i.this.m296()).booleanValue()) {
                            i.this.f289.postDelayed(this, 200);
                        } else {
                            i.this.m249();
                        }
                    } catch (Exception e) {
                        i.this.m249();
                        o.m359(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            a.m235(3, "IntervalVideoTracker", this, "Problem with video loop");
            m271("trackVideoAd", e);
            return false;
        }
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            o.m359(e);
        }
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.f343 = m244().doubleValue();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m297(MoatAdEvent moatAdEvent) {
        Integer num;
        if (!moatAdEvent.f261.equals(MoatAdEvent.f257)) {
            num = moatAdEvent.f261;
        } else {
            try {
                num = m300();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f341);
            }
            moatAdEvent.f261 = num;
        }
        if (moatAdEvent.f261.intValue() < 0 || (moatAdEvent.f261.intValue() == 0 && moatAdEvent.f262 == MoatAdEventType.AD_EVT_COMPLETE && this.f341 > 0)) {
            num = Integer.valueOf(this.f341);
            moatAdEvent.f261 = num;
        }
        if (moatAdEvent.f262 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || this.f342 == Integer.MIN_VALUE || !m242(num, Integer.valueOf(this.f342))) {
                this.f340 = c.f348;
                moatAdEvent.f262 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f340 = c.f351;
            }
        }
        return super.m247(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    /* renamed from: ʻॱ  reason: contains not printable characters */
    public final boolean m296() throws o {
        if (!m298() || m246()) {
            return false;
        }
        try {
            int intValue = m300().intValue();
            if (this.f341 >= 0 && intValue < 0) {
                return false;
            }
            this.f341 = intValue;
            if (intValue == 0) {
                return true;
            }
            int intValue2 = m302().intValue();
            boolean r4 = m301();
            double d = (double) intValue2;
            Double.isNaN(d);
            double d2 = d / 4.0d;
            double doubleValue = m244().doubleValue();
            MoatAdEventType moatAdEventType = null;
            if (intValue > this.f344) {
                this.f344 = intValue;
            }
            if (this.f342 == Integer.MIN_VALUE) {
                this.f342 = intValue2;
            }
            if (r4) {
                if (this.f340 == c.f350) {
                    moatAdEventType = MoatAdEventType.AD_EVT_START;
                    this.f340 = c.f349;
                } else if (this.f340 == c.f347) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                    this.f340 = c.f349;
                } else {
                    double d3 = (double) intValue;
                    Double.isNaN(d3);
                    int floor = ((int) Math.floor(d3 / d2)) - 1;
                    if (floor >= 0 && floor < 3) {
                        MoatAdEventType moatAdEventType2 = f279[floor];
                        if (!this.f280.containsKey(moatAdEventType2)) {
                            this.f280.put(moatAdEventType2, 1);
                            moatAdEventType = moatAdEventType2;
                        }
                    }
                }
            } else if (this.f340 != c.f347) {
                moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                this.f340 = c.f347;
            }
            boolean z = moatAdEventType != null;
            if (!z && !Double.isNaN(this.f343) && Math.abs(this.f343 - doubleValue) > 0.05d) {
                moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                z = true;
            }
            if (z) {
                dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m251()));
            }
            this.f343 = doubleValue;
            this.f345 = 0;
            return true;
        } catch (Exception unused) {
            int i = this.f345;
            this.f345 = i + 1;
            return i < 5;
        }
    }
}
