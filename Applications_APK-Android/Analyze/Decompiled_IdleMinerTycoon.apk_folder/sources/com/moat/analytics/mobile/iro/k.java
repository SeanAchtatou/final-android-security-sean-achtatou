package com.moat.analytics.mobile.iro;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.iro.g;
import com.moat.analytics.mobile.iro.w;
import java.lang.ref.WeakReference;

class k extends MoatAnalytics implements w.b {
    boolean a = false;
    boolean b = false;
    @Nullable
    g c;
    WeakReference<Context> d;
    private boolean e = false;
    private String f;
    private MoatOptions g;

    k() {
    }

    private void a(MoatOptions moatOptions, Application application) {
        if (this.e) {
            p.a(3, "Analytics", this, "Moat SDK has already been started.");
            return;
        }
        this.g = moatOptions;
        w.a().b();
        if (application != null) {
            if (moatOptions.loggingEnabled && s.b(application.getApplicationContext())) {
                this.a = true;
            }
            this.d = new WeakReference<>(application.getApplicationContext());
            this.e = true;
            this.b = moatOptions.autoTrackGMAInterstitials;
            a.a(application);
            w.a().a(this);
            if (!moatOptions.disableAdIdCollection) {
                s.a(application);
            }
            p.a("[SUCCESS] ", "Moat Analytics SDK Version 2.6.0 started");
            return;
        }
        throw new n("Moat Analytics SDK didn't start, application was null");
    }

    @UiThread
    private void d() {
        if (this.c == null) {
            this.c = new g(a.a(), g.a.DISPLAY);
            this.c.a(this.f);
            p.a(3, "Analytics", this, "Preparing native display tracking with partner code " + this.f);
            p.a("[SUCCESS] ", "Prepared for native display tracking with partner code " + this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.e;
    }

    public void b() {
        n.a();
        if (this.f != null) {
            try {
                d();
            } catch (Exception e2) {
                n.a(e2);
            }
        }
    }

    public void c() {
    }

    @UiThread
    public void prepareNativeDisplayTracking(String str) {
        this.f = str;
        if (w.a().a != w.d.OFF) {
            try {
                d();
            } catch (Exception e2) {
                n.a(e2);
            }
        }
    }

    public void start(Application application) {
        start(new MoatOptions(), application);
    }

    public void start(MoatOptions moatOptions, Application application) {
        try {
            a(moatOptions, application);
        } catch (Exception e2) {
            n.a(e2);
        }
    }
}
