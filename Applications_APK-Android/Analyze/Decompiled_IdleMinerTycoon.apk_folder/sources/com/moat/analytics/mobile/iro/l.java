package com.moat.analytics.mobile.iro;

import android.app.Application;
import android.media.AudioManager;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;

public class l {
    private static final Long a = 200L;
    private static final l b = new l();
    private AudioManager c;
    private double d = 0.0d;
    private Long e;

    private l() {
        c();
    }

    public static l a() {
        return b;
    }

    private void c() {
        Application a2 = a.a();
        if (a2 != null) {
            this.c = (AudioManager) a2.getSystemService("audio");
        }
    }

    @Nullable
    private AudioManager d() {
        if (this.c == null) {
            c();
        }
        return this.c;
    }

    private void e() {
        try {
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            if (this.e == null || valueOf.longValue() - this.e.longValue() > a.longValue()) {
                this.e = valueOf;
                AudioManager d2 = d();
                if (d2 != null) {
                    double streamVolume = (double) d2.getStreamVolume(3);
                    double streamMaxVolume = (double) d2.getStreamMaxVolume(3);
                    Double.isNaN(streamVolume);
                    Double.isNaN(streamMaxVolume);
                    this.d = streamVolume / streamMaxVolume;
                }
            }
        } catch (Exception e2) {
            n.a(e2);
            this.d = 0.0d;
        }
    }

    /* access modifiers changed from: package-private */
    @FloatRange(from = 0.0d, to = 1.0d)
    public double b() {
        try {
            e();
            return this.d;
        } catch (Exception e2) {
            n.a(e2);
            return 0.0d;
        }
    }
}
