package com.moat.analytics.mobile.cha;

import android.support.annotation.VisibleForTesting;
import com.moat.analytics.mobile.cha.base.asserts.Asserts;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final Object[] f401 = new Object[0];

    /* renamed from: ˊ  reason: contains not printable characters */
    private final c<T> f402;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f403;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final LinkedList<p<T>.d> f404 = new LinkedList<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final Class<T> f405;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private T f406;

    interface c<T> {
        /* renamed from: ˋ  reason: contains not printable characters */
        Optional<T> m367() throws o;
    }

    class d {
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public final WeakReference[] f408;
        /* access modifiers changed from: private */

        /* renamed from: ˋ  reason: contains not printable characters */
        public final Method f409;

        /* renamed from: ˎ  reason: contains not printable characters */
        private final LinkedList<Object> f410;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f410 = new LinkedList<>();
            objArr = objArr == null ? p.f401 : objArr;
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f410.add(obj);
                }
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2++;
            }
            this.f408 = weakReferenceArr;
            this.f409 = method;
        }
    }

    @VisibleForTesting
    private p(c<T> cVar, Class<T> cls) throws o {
        Asserts.checkNotNull(cVar);
        Asserts.checkNotNull(cls);
        this.f402 = cVar;
        this.f405 = cls;
        t.m403().m408(new t.b() {
            /* renamed from: ˎ  reason: contains not printable characters */
            public final void m366() throws o {
                p.this.m363();
            }
        });
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static <T> T m362(c<T> cVar, Class<T> cls) throws o {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(cVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static Boolean m361(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return true;
            }
            return null;
        } catch (Exception e) {
            o.m359(e);
            return null;
        }
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class<?> declaringClass = method.getDeclaringClass();
            t r0 = t.m403();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f405;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f405.getName());
            } else if (!this.f403 || this.f406 != null) {
                if (r0.f439 == t.a.f450) {
                    m363();
                    if (this.f406 != null) {
                        return method.invoke(this.f406, objArr);
                    }
                }
                if (r0.f439 == t.a.f451 && (!this.f403 || this.f406 != null)) {
                    if (this.f404.size() >= 15) {
                        this.f404.remove(5);
                    }
                    this.f404.add(new d(this, method, objArr, (byte) 0));
                }
                return m361(method);
            } else {
                this.f404.clear();
                return m361(method);
            }
        } catch (Exception e) {
            o.m359(e);
            return m361(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m363() throws o {
        if (!this.f403) {
            try {
                this.f406 = this.f402.m367().orElse(null);
            } catch (Exception e) {
                a.m237("OnOffTrackerProxy", this, "Could not create instance", e);
                o.m359(e);
            }
            this.f403 = true;
        }
        if (this.f406 != null) {
            Iterator<p<T>.d> it = this.f404.iterator();
            while (it.hasNext()) {
                d next = it.next();
                try {
                    Object[] objArr = new Object[next.f408.length];
                    WeakReference[] r3 = next.f408;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        objArr[i2] = r3[i].get();
                        i++;
                        i2++;
                    }
                    next.f409.invoke(this.f406, objArr);
                } catch (Exception e2) {
                    o.m359(e2);
                }
            }
            this.f404.clear();
        }
    }
}
