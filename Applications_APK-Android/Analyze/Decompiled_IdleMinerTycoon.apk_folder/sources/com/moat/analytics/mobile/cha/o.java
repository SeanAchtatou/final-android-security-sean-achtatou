package com.moat.analytics.mobile.cha;

import android.util.Log;

final class o extends Exception {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Exception f398 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Long f399 = 60000L;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Long f400;

    o(String str) {
        super(str);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m358(String str, Exception exc) {
        if (exc instanceof o) {
            return str + " failed: " + exc.getMessage();
        }
        return str + " failed unexpectedly";
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m359(Exception exc) {
        if (t.m403().f440) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            m357(exc);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x017c A[Catch:{ Exception -> 0x01a4 }] */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m357(java.lang.Exception r12) {
        /*
            com.moat.analytics.mobile.cha.t r0 = com.moat.analytics.mobile.cha.t.m403()     // Catch:{ Exception -> 0x01a4 }
            int r0 = r0.f439     // Catch:{ Exception -> 0x01a4 }
            int r1 = com.moat.analytics.mobile.cha.t.a.f450     // Catch:{ Exception -> 0x01a4 }
            if (r0 != r1) goto L_0x01a1
            com.moat.analytics.mobile.cha.t r0 = com.moat.analytics.mobile.cha.t.m403()     // Catch:{ Exception -> 0x01a4 }
            int r0 = r0.f442     // Catch:{ Exception -> 0x01a4 }
            if (r0 != 0) goto L_0x0013
            return
        L_0x0013:
            r1 = 100
            if (r0 >= r1) goto L_0x0027
            double r1 = (double) r0
            r3 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r1)
            double r1 = r1 / r3
            double r3 = java.lang.Math.random()     // Catch:{ Exception -> 0x01a4 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0027
            return
        L_0x0027:
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = "https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r7 = "&zt="
            r5.<init>(r7)     // Catch:{ Exception -> 0x01a4 }
            boolean r7 = r12 instanceof com.moat.analytics.mobile.cha.o     // Catch:{ Exception -> 0x01a4 }
            r8 = 1
            r9 = 0
            if (r7 == 0) goto L_0x0045
            r7 = 1
            goto L_0x0046
        L_0x0045:
            r7 = 0
        L_0x0046:
            r5.append(r7)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r5)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r7 = "&zr="
            r5.<init>(r7)     // Catch:{ Exception -> 0x01a4 }
            r5.append(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r5 = "&zm="
            r0.<init>(r5)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00b4 }
            if (r5 != 0) goto L_0x0071
            java.lang.String r5 = "null"
            goto L_0x0085
        L_0x0071:
            java.lang.String r5 = r12.getMessage()     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r7 = "UTF-8"
            byte[] r5 = r5.getBytes(r7)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r5 = android.util.Base64.encodeToString(r5, r9)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r7 = "UTF-8"
            java.lang.String r5 = java.net.URLEncoder.encode(r5, r7)     // Catch:{ Exception -> 0x00b4 }
        L_0x0085:
            r0.append(r5)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00b4 }
            r6.append(r0)     // Catch:{ Exception -> 0x00b4 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r5 = "&k="
            r0.<init>(r5)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r12 = android.util.Log.getStackTraceString(r12)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r5 = "UTF-8"
            byte[] r12 = r12.getBytes(r5)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r12 = android.util.Base64.encodeToString(r12, r9)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r5 = "UTF-8"
            java.lang.String r12 = java.net.URLEncoder.encode(r12, r5)     // Catch:{ Exception -> 0x00b4 }
            r0.append(r12)     // Catch:{ Exception -> 0x00b4 }
            java.lang.String r12 = r0.toString()     // Catch:{ Exception -> 0x00b4 }
            r6.append(r12)     // Catch:{ Exception -> 0x00b4 }
        L_0x00b4:
            java.lang.String r12 = "CHA"
            java.lang.String r0 = "&zMoatMMAKv=35d482907bc2811c2e46b96f16eb5f9fe00185f3"
            r6.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "2.4.1"
            com.moat.analytics.mobile.cha.r$e r1 = com.moat.analytics.mobile.cha.r.m376()     // Catch:{ Exception -> 0x00e1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r5 = "&zMoatMMAKan="
            r3.<init>(r5)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r5 = r1.m387()     // Catch:{ Exception -> 0x00e1 }
            r3.append(r5)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00e1 }
            r6.append(r3)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r1 = r1.m386()     // Catch:{ Exception -> 0x00e1 }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00e6 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00e6 }
            goto L_0x00e7
        L_0x00e1:
            r1 = r2
            goto L_0x00e6
        L_0x00e3:
            r12 = r1
        L_0x00e4:
            r1 = r2
            r0 = r3
        L_0x00e6:
            r2 = r4
        L_0x00e7:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r4 = "&d=Android:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x01a4 }
            r3.append(r12)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r12 = ":"
            r3.append(r12)     // Catch:{ Exception -> 0x01a4 }
            r3.append(r1)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r12 = ":-"
            r3.append(r12)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r12 = r3.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r12)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r1 = "&bo="
            r12.<init>(r1)     // Catch:{ Exception -> 0x01a4 }
            r12.append(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r12)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r0 = "&bd="
            r12.<init>(r0)     // Catch:{ Exception -> 0x01a4 }
            r12.append(r2)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r12)     // Catch:{ Exception -> 0x01a4 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01a4 }
            java.lang.Long r12 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r1 = "&t="
            r0.<init>(r1)     // Catch:{ Exception -> 0x01a4 }
            r0.append(r12)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r1 = "&de="
            r0.<init>(r1)     // Catch:{ Exception -> 0x01a4 }
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r2 = "%.0f"
            java.lang.Object[] r3 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x01a4 }
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x01a4 }
            r7 = 4621819117588971520(0x4024000000000000, double:10.0)
            r10 = 4622945017495814144(0x4028000000000000, double:12.0)
            double r7 = java.lang.Math.pow(r7, r10)     // Catch:{ Exception -> 0x01a4 }
            double r4 = r4 * r7
            double r4 = java.lang.Math.floor(r4)     // Catch:{ Exception -> 0x01a4 }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x01a4 }
            r3[r9] = r4     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r1 = java.lang.String.format(r1, r2, r3)     // Catch:{ Exception -> 0x01a4 }
            r0.append(r1)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01a4 }
            r6.append(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r0 = "&cs=0"
            r6.append(r0)     // Catch:{ Exception -> 0x01a4 }
            java.lang.Long r0 = com.moat.analytics.mobile.cha.o.f400     // Catch:{ Exception -> 0x01a4 }
            if (r0 == 0) goto L_0x0192
            long r0 = r12.longValue()     // Catch:{ Exception -> 0x01a4 }
            java.lang.Long r2 = com.moat.analytics.mobile.cha.o.f400     // Catch:{ Exception -> 0x01a4 }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x01a4 }
            r4 = 0
            long r0 = r0 - r2
            java.lang.Long r2 = com.moat.analytics.mobile.cha.o.f399     // Catch:{ Exception -> 0x01a4 }
            long r2 = r2.longValue()     // Catch:{ Exception -> 0x01a4 }
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x01a0
        L_0x0192:
            java.lang.String r0 = r6.toString()     // Catch:{ Exception -> 0x01a4 }
            com.moat.analytics.mobile.cha.m$2 r1 = new com.moat.analytics.mobile.cha.m$2     // Catch:{ Exception -> 0x01a4 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x01a4 }
            r1.start()     // Catch:{ Exception -> 0x01a4 }
            com.moat.analytics.mobile.cha.o.f400 = r12     // Catch:{ Exception -> 0x01a4 }
        L_0x01a0:
            return
        L_0x01a1:
            com.moat.analytics.mobile.cha.o.f398 = r12     // Catch:{ Exception -> 0x01a4 }
            return
        L_0x01a4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.o.m357(java.lang.Exception):void");
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m360() {
        if (f398 != null) {
            m357(f398);
            f398 = null;
        }
    }
}
