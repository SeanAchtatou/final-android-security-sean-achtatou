package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.FloatRange;
import android.telephony.TelephonyManager;
import com.chartboost.sdk.impl.b;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.lang.ref.WeakReference;

final class r {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int f414 = 1;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static e f415 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static d f416 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static int f417 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public static String f418;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static int[] f419 = {-39340411, 1646369784, -593413711, -1069164445, -50787683, -1327220997, 423245644, -742130253, 54775946, -495304555, 1880137505, 1742082653, 65717847, 1497802820, 828947133, -614454858, 941569790, -1897799303};

    r() {
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    /* renamed from: ॱ  reason: contains not printable characters */
    static double m383() {
        try {
            double r1 = (double) m374();
            double streamMaxVolume = (double) ((AudioManager) c.m256().getSystemService(m380(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamMaxVolume(3);
            Double.isNaN(r1);
            Double.isNaN(streamMaxVolume);
            return r1 / streamMaxVolume;
        } catch (Exception e2) {
            o.m359(e2);
            return 0.0d;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m374() {
        try {
            return ((AudioManager) c.m256().getSystemService(m380(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamVolume(3);
        } catch (Exception e2) {
            o.m359(e2);
            return 0;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m381(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            String unused = r.f418 = advertisingIdInfo.getId();
                            a.m235(3, "Util", this, "Retrieved Advertising ID = " + r.f418);
                            return;
                        }
                        a.m235(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m359(e);
                    }
                }
            });
        } catch (Exception e2) {
            o.m359(e2);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m379() {
        return f418;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Context m382() {
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f314;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static e m376() {
        if (f415 == null || !f415.f429) {
            f415 = new e((byte) 0);
        }
        return f415;
    }

    static class e {

        /* renamed from: ˊ  reason: contains not printable characters */
        private String f427;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f428;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public boolean f429;

        /* renamed from: ॱ  reason: contains not printable characters */
        private String f430;

        /* synthetic */ e(byte b) {
            this();
        }

        private e() {
            this.f429 = false;
            this.f427 = "_unknown_";
            this.f428 = "_unknown_";
            this.f430 = "_unknown_";
            try {
                Context r0 = r.m382();
                if (r0 != null) {
                    this.f429 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f428 = r0.getPackageName();
                    this.f427 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f430 = packageManager.getInstallerPackageName(this.f428);
                    return;
                }
                a.m235(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m359(e);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final String m387() {
            return this.f427;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m386() {
            return this.f428;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ॱ  reason: contains not printable characters */
        public final String m388() {
            return this.f430 != null ? this.f430 : "_unknown_";
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static String m380(int[] iArr, int i) {
        char[] cArr = new char[4];
        char[] cArr2 = new char[(iArr.length << 1)];
        int[] iArr2 = (int[]) f419.clone();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                return new String(cArr2, 0, i);
            }
            cArr[0] = iArr[i2] >>> 16;
            cArr[1] = (char) iArr[i2];
            int i3 = i2 + 1;
            cArr[2] = iArr[i3] >>> 16;
            cArr[3] = (char) iArr[i3];
            b.a(cArr, iArr2, false);
            int i4 = i2 << 1;
            cArr2[i4] = cArr[0];
            cArr2[i4 + 1] = cArr[1];
            cArr2[i4 + 2] = cArr[2];
            cArr2[i4 + 3] = cArr[3];
            i2 += 2;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static d m377() {
        if (f416 == null || !f416.f421) {
            f416 = new d((byte) 0);
        }
        return f416;
    }

    static class d {

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f421;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f422;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f423;

        /* renamed from: ˎ  reason: contains not printable characters */
        String f424;

        /* renamed from: ˏ  reason: contains not printable characters */
        String f425;

        /* renamed from: ॱ  reason: contains not printable characters */
        Integer f426;

        /* synthetic */ d(byte b) {
            this();
        }

        private d() {
            this.f424 = "_unknown_";
            this.f425 = "_unknown_";
            this.f426 = -1;
            this.f423 = false;
            this.f422 = false;
            this.f421 = false;
            try {
                Context r0 = r.m382();
                if (r0 != null) {
                    this.f421 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService("phone");
                    this.f424 = telephonyManager.getSimOperatorName();
                    this.f425 = telephonyManager.getNetworkOperatorName();
                    this.f426 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f423 = r.m373();
                    this.f422 = r.m378(r0);
                }
            } catch (Exception e) {
                o.m359(e);
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m378(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static /* synthetic */ boolean m373() {
        Context context;
        int i;
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f314;
        if (weakReference != null) {
            context = weakReference.get();
        } else {
            context = null;
        }
        if (context != null) {
            int i2 = f414 + 27;
            f417 = i2 % 128;
            int i3 = i2 % 2;
            if ((Build.VERSION.SDK_INT < 17 ? (char) 22 : 19) != 22) {
                int i4 = f414 + 87;
                f417 = i4 % 128;
                int i5 = i4 % 2;
                i = Settings.Global.getInt(context.getContentResolver(), m380(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            } else {
                i = Settings.Secure.getInt(context.getContentResolver(), m380(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            }
        } else {
            i = 0;
        }
        if (!(i == 1)) {
            return false;
        }
        int i6 = f417 + 33;
        f414 = i6 % 128;
        int i7 = i6 % 2;
        return true;
    }
}
