package com.moat.analytics.mobile.vng;

import android.app.Activity;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants;
import com.moat.analytics.mobile.vng.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class b {
    m a = null;
    WeakReference<WebView> b;
    j c;
    TrackerListener d;
    final String e;
    final boolean f;
    private WeakReference<View> g;
    private final y h;
    private final boolean i;
    private boolean j;
    private boolean k;

    b(@Nullable View view, boolean z, boolean z2) {
        String str;
        o.a(3, "BaseTracker", this, "Initializing.");
        if (z) {
            str = "m" + hashCode();
        } else {
            str = "";
        }
        this.e = str;
        this.g = new WeakReference<>(view);
        this.i = z;
        this.f = z2;
        this.j = false;
        this.k = false;
        this.h = new y();
    }

    private void i() {
        String str;
        String str2;
        o.a(3, "BaseTracker", this, "Attempting bridge installation.");
        if (this.b.get() != null) {
            this.c = new j(this.b.get(), j.a.WEBVIEW);
            str = "BaseTracker";
            str2 = "Bridge installed.";
        } else {
            this.c = null;
            str = "BaseTracker";
            str2 = "Bridge not installed, WebView is null.";
        }
        o.a(3, str, this, str2);
    }

    private void j() {
        if (this.j) {
            throw new m("Tracker already started");
        }
    }

    private void k() {
        if (this.k) {
            throw new m("Tracker already stopped");
        }
    }

    private boolean l() {
        return this.i || this.f;
    }

    /* access modifiers changed from: package-private */
    public abstract String a();

    /* access modifiers changed from: package-private */
    public void a(WebView webView) {
        if (webView != null) {
            this.b = new WeakReference<>(webView);
            if (this.c == null && !l()) {
                i();
            }
            if (this.c != null) {
                this.c.a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        this.c = jVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, Exception exc) {
        try {
            m.a(exc);
            String a2 = m.a(str, exc);
            if (this.d != null) {
                this.d.onTrackingFailedToStart(a2);
            }
            o.a(3, "BaseTracker", this, a2);
            o.a("[ERROR] ", a() + " " + a2);
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    public void a(List<String> list) {
        if (f() == null && !this.f) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new m(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: package-private */
    @CallSuper
    public void b() {
        o.a(3, "BaseTracker", this, "Attempting to start impression.");
        c();
        d();
        a(new ArrayList());
        if (this.c != null) {
            this.c.b(this);
            this.j = true;
            o.a(3, "BaseTracker", this, "Impression started.");
            return;
        }
        o.a(3, "BaseTracker", this, "Bridge is null, won't start tracking");
        throw new m("Bridge is null");
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.a != null) {
            throw new m("Tracker initialization failed: " + this.a.getMessage());
        }
    }

    @CallSuper
    public void changeTargetView(View view) {
        o.a(3, "BaseTracker", this, "changing view to " + o.a(view));
        this.g = new WeakReference<>(view);
    }

    /* access modifiers changed from: package-private */
    public void d() {
        j();
        k();
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.j && !this.k;
    }

    /* access modifiers changed from: package-private */
    public View f() {
        return this.g.get();
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return o.a(f());
    }

    /* access modifiers changed from: package-private */
    public String h() {
        this.h.a(this.e, f());
        return this.h.a;
    }

    public void removeListener() {
        this.d = null;
    }

    @Deprecated
    public void setActivity(Activity activity) {
    }

    public void setListener(TrackerListener trackerListener) {
        this.d = trackerListener;
    }

    public void startTracking() {
        try {
            o.a(3, "BaseTracker", this, "In startTracking method.");
            b();
            if (this.d != null) {
                this.d.onTrackingStarted("Tracking started on " + g());
            }
            String str = "startTracking succeeded for " + g();
            o.a(3, "BaseTracker", this, str);
            o.a("[SUCCESS] ", a() + " " + str);
        } catch (Exception e2) {
            a("startTracking", e2);
        }
    }

    @CallSuper
    public void stopTracking() {
        boolean z = false;
        try {
            o.a(3, "BaseTracker", this, "In stopTracking method.");
            this.k = true;
            if (this.c != null) {
                this.c.c(this);
                z = true;
            }
        } catch (Exception e2) {
            m.a(e2);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        o.a(3, "BaseTracker", this, sb.toString());
        String str = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(a());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : Constants.ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(g());
        o.a(str, sb2.toString());
        if (this.d != null) {
            this.d.onTrackingStopped("");
            this.d = null;
        }
    }
}
