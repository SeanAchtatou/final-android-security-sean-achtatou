package com.moat.analytics.mobile.cha;

import android.media.MediaPlayer;
import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class s extends i implements NativeVideoTracker {

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private WeakReference<MediaPlayer> f431;

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m389() {
        return "NativeVideoTracker";
    }

    s(String str) {
        super(str);
        a.m235(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String sb2 = sb.toString();
            String str2 = "NativeDisplayTracker creation problem, " + sb2;
            a.m235(3, "NativeVideoTracker", this, str2);
            a.m232("[ERROR] ", str2);
            this.f307 = new o(sb2);
        }
        a.m232("[SUCCESS] ", "NativeVideoTracker created");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public final boolean m391() {
        return (this.f431 == null || this.f431.get() == null) ? false : true;
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m267();
            m269();
            if (mediaPlayer != null) {
                mediaPlayer.getCurrentPosition();
                this.f431 = new WeakReference<>(mediaPlayer);
                return super.m299(map, view);
            }
            throw new o("Null player instance");
        } catch (Exception unused) {
            throw new o("Playback has already completed");
        } catch (Exception e) {
            o.m359(e);
            String r2 = o.m358("trackVideoAd", e);
            if (this.f301 != null) {
                this.f301.onTrackingFailedToStart(r2);
            }
            a.m235(3, "NativeVideoTracker", this, r2);
            a.m232("[ERROR] ", "NativeVideoTracker " + r2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public final Integer m392() {
        return Integer.valueOf(this.f431.get().getCurrentPosition());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final boolean m393() {
        return this.f431.get().isPlaying();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public final Integer m394() {
        return Integer.valueOf(this.f431.get().getDuration());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m395() throws o {
        MediaPlayer mediaPlayer = this.f431.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put(IronSourceConstants.EVENTS_DURATION, Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m390(List<String> list) throws o {
        if (!((this.f431 == null || this.f431.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m245(list);
    }
}
