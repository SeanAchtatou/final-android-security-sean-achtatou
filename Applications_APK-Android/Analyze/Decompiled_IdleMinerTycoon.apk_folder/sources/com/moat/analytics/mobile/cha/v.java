package com.moat.analytics.mobile.cha;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.webkit.WebView;

final class v extends d implements WebAdTracker {
    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m426() {
        return "WebAdTracker";
    }

    v(@Nullable ViewGroup viewGroup) {
        this(x.m431(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "WebAdTracker initialization not successful, " + "Target ViewGroup is null";
            a.m235(3, "WebAdTracker", this, str);
            a.m232("[ERROR] ", str);
            this.f307 = new o("Target ViewGroup is null");
        }
        if (this.f305 == null) {
            String str2 = "WebAdTracker initialization not successful, " + "No WebView to track inside of ad container";
            a.m235(3, "WebAdTracker", this, str2);
            a.m232("[ERROR] ", str2);
            this.f307 = new o("No WebView to track inside of ad container");
        }
    }

    v(@Nullable WebView webView) {
        super(webView, false, false);
        a.m235(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebAdTracker initialization not successful, " + "WebView is null";
            a.m235(3, "WebAdTracker", this, str);
            a.m232("[ERROR] ", str);
            this.f307 = new o("WebView is null");
            return;
        }
        try {
            super.m270(webView);
            a.m232("[SUCCESS] ", "WebAdTracker created for " + m261());
        } catch (o e) {
            this.f307 = e;
        }
    }
}
