package com.moat.analytics.mobile.iro;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.ironsource.sdk.constants.Constants;
import java.lang.ref.WeakReference;

class s {
    /* access modifiers changed from: private */
    public static String a;
    private static a b;
    private static b c;

    static class a {
        /* access modifiers changed from: private */
        public boolean a;
        private String b;
        private String c;
        private String d;

        private a() {
            this.a = false;
            this.b = "_unknown_";
            this.c = "_unknown_";
            this.d = "_unknown_";
            try {
                Context b2 = s.b();
                if (b2 != null) {
                    this.a = true;
                    PackageManager packageManager = b2.getPackageManager();
                    this.c = b2.getPackageName();
                    this.b = packageManager.getApplicationLabel(b2.getApplicationInfo()).toString();
                    this.d = packageManager.getInstallerPackageName(this.c);
                    return;
                }
                p.a(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                n.a(e);
            }
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public String b() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public String c() {
            return this.d != null ? this.d : "_unknown_";
        }
    }

    static class b {
        String a;
        String b;
        Integer c;
        boolean d;
        boolean e;
        boolean f;

        private b() {
            this.a = "_unknown_";
            this.b = "_unknown_";
            this.c = -1;
            this.d = false;
            this.e = false;
            this.f = false;
            try {
                Context b2 = s.b();
                if (b2 != null) {
                    this.f = true;
                    TelephonyManager telephonyManager = (TelephonyManager) b2.getSystemService("phone");
                    this.a = telephonyManager.getSimOperatorName();
                    this.b = telephonyManager.getNetworkOperatorName();
                    this.c = Integer.valueOf(telephonyManager.getPhoneType());
                    this.d = s.g();
                    this.e = s.b(b2);
                }
            } catch (Exception e2) {
                n.a(e2);
            }
        }
    }

    s() {
    }

    static String a() {
        return a;
    }

    static void a(final Context context) {
        try {
            AsyncTask.execute(new Runnable() {
                public void run() {
                    String str;
                    String str2;
                    String str3;
                    String str4;
                    try {
                        Class<?> cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                        Class<?> cls2 = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
                        Object invoke = cls.getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
                        if (!((Boolean) cls2.getMethod(Constants.RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])).booleanValue()) {
                            String unused = s.a = (String) cls2.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
                            str3 = "Util";
                            str4 = "Retrieved Advertising ID = " + s.a;
                        } else {
                            str3 = "Util";
                            str4 = "User has limited ad tracking";
                        }
                        p.a(3, str3, this, str4);
                    } catch (ClassNotFoundException e) {
                        e = e;
                        str2 = "Util";
                        str = "ClassNotFoundException while retrieving Advertising ID";
                        p.a(str2, this, str, e);
                    } catch (NoSuchMethodException e2) {
                        e = e2;
                        str2 = "Util";
                        str = "NoSuchMethodException while retrieving Advertising ID";
                        p.a(str2, this, str, e);
                    } catch (Exception e3) {
                        n.a(e3);
                    }
                }
            });
        } catch (Exception e) {
            n.a(e);
        }
    }

    static Context b() {
        WeakReference<Context> weakReference = ((k) MoatAnalytics.getInstance()).d;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    static boolean b(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    static a c() {
        if (b == null || !b.a) {
            b = new a();
        }
        return b;
    }

    static b d() {
        if (c == null || !c.f) {
            c = new b();
        }
        return c;
    }

    /* access modifiers changed from: private */
    public static boolean g() {
        Context b2 = b();
        return (b2 != null ? Build.VERSION.SDK_INT >= 17 ? Settings.Global.getInt(b2.getContentResolver(), "adb_enabled", 0) : Settings.Secure.getInt(b2.getContentResolver(), "adb_enabled", 0) : 0) == 1;
    }
}
