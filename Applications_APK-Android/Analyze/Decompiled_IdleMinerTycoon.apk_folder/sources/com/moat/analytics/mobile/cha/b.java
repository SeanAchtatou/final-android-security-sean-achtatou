package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.support.annotation.CallSuper;
import android.text.TextUtils;
import android.view.View;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.moat.analytics.mobile.cha.a;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class b extends d {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final MoatAdEventType[] f279 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʼ  reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f280;

    /* renamed from: ʼॱ  reason: contains not printable characters */
    private final String f281;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    WeakReference<View> f282;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private boolean f283;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private Map<String, String> f284;

    /* renamed from: ͺ  reason: contains not printable characters */
    private Double f285;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public VideoTrackerListener f286;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private final Set<MoatAdEventType> f287;
    /* access modifiers changed from: private */

    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final a f288 = new a(c.m256(), a.d.f277);

    /* renamed from: ᐝ  reason: contains not printable characters */
    final Handler f289;

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public abstract Map<String, Object> m252() throws o;

    b(String str) {
        super(null, false, true);
        a.m235(3, "BaseVideoTracker", this, "Initializing.");
        this.f281 = str;
        this.f304 = this.f288.f273;
        try {
            super.m270(this.f288.f271);
        } catch (o e) {
            this.f307 = e;
        }
        this.f280 = new HashMap();
        this.f287 = new HashSet();
        this.f289 = new Handler();
        this.f283 = false;
        this.f285 = Double.valueOf(1.0d);
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f286 = videoTrackerListener;
    }

    public void removeVideoListener() {
        this.f286 = null;
    }

    @CallSuper
    /* renamed from: ॱ  reason: contains not printable characters */
    public boolean m250(Map<String, String> map, View view) {
        try {
            m267();
            m269();
            if (view == null) {
                a.m235(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f284 = map;
            this.f282 = new WeakReference<>(view);
            m248();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new JSONObject(map).toString(), a.m234(view));
            a.m235(3, "BaseVideoTracker", this, format);
            a.m232("[SUCCESS] ", m265() + " " + format);
            if (this.f301 != null) {
                this.f301.onTrackingStarted(m261());
            }
            return true;
        } catch (Exception e) {
            m271("trackVideoAd", e);
            return false;
        }
    }

    public void changeTargetView(View view) {
        a.m235(3, "BaseVideoTracker", this, "changing view to " + a.m234(view));
        this.f282 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m245(List<String> list) throws o {
        if (this.f284 == null) {
            list.add("Null adIds object");
        }
        if (list.isEmpty()) {
            super.m266(list);
            return;
        }
        throw new o(TextUtils.join(" and ", list));
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m249();
            if (this.f286 != null) {
                this.f286 = null;
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˊ  reason: contains not printable characters */
    public final Double m251() {
        return this.f285;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m248() throws o {
        super.changeTargetView(this.f282.get());
        super.m268();
        Map<String, Object> r0 = m252();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get(IronSourceConstants.EVENTS_DURATION);
        a.m235(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", num2, num, num3));
        this.f288.m240(this.f281, this.f284, num, num2, num3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public JSONObject m247(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f260.doubleValue())) {
            moatAdEvent.f260 = this.f285;
        }
        return new JSONObject(moatAdEvent.m229());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏॱ  reason: contains not printable characters */
    public final void m249() {
        if (!this.f283) {
            this.f283 = true;
            this.f289.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        a.m235(3, "BaseVideoTracker", this, "Shutting down.");
                        a r0 = b.this.f288;
                        a.m235(3, "GlobalWebView", r0, "Cleaning up");
                        r0.f273.m323();
                        r0.f273 = null;
                        r0.f271.destroy();
                        r0.f271 = null;
                        VideoTrackerListener unused = b.this.f286 = null;
                    } catch (Exception e) {
                        o.m359(e);
                    }
                }
            }, 500);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋॱ  reason: contains not printable characters */
    public final boolean m246() {
        return this.f280.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f280.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f280.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m242(Integer num, Integer num2) {
        int abs = Math.abs(num2.intValue() - num.intValue());
        double intValue = (double) num2.intValue();
        Double.isNaN(intValue);
        return ((double) abs) <= Math.min(750.0d, intValue * 0.05d);
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f285.doubleValue() * r.m383());
        if (!d.equals(this.f285)) {
            a.m235(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", d));
            this.f285 = d;
            if (!valueOf.equals(Double.valueOf(this.f285.doubleValue() * r.m383()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f257, this.f285));
            }
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m247(moatAdEvent);
            boolean z = false;
            a.m235(3, "BaseVideoTracker", this, String.format("Received event: %s", r0.toString()));
            a.m232("[SUCCESS] ", m265() + String.format(" Received event: %s", r0.toString()));
            if (m264() && this.f304 != null) {
                this.f304.m331(this.f288.f270, r0);
                if (!this.f287.contains(moatAdEvent.f262)) {
                    this.f287.add(moatAdEvent.f262);
                    if (this.f286 != null) {
                        this.f286.onVideoEventReported(moatAdEvent.f262);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f262;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f280.put(moatAdEventType, 1);
                if (this.f304 != null) {
                    this.f304.m325(this);
                }
                m249();
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊॱ  reason: contains not printable characters */
    public final Double m244() {
        return Double.valueOf(this.f285.doubleValue() * r.m383());
    }
}
