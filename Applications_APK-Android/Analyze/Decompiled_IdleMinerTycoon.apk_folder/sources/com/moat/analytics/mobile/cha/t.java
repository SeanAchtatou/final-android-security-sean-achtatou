package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.os.Looper;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class t {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static t f432;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final Queue<e> f433 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public long f434 = 60000;

    /* renamed from: ˊ  reason: contains not printable characters */
    volatile int f435 = 200;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private long f436 = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;

    /* renamed from: ˋ  reason: contains not printable characters */
    volatile boolean f437 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ  reason: contains not printable characters */
    public final AtomicBoolean f438 = new AtomicBoolean(false);

    /* renamed from: ˎ  reason: contains not printable characters */
    volatile int f439 = a.f451;

    /* renamed from: ˏ  reason: contains not printable characters */
    volatile boolean f440 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ  reason: contains not printable characters */
    public final AtomicBoolean f441 = new AtomicBoolean(false);

    /* renamed from: ॱ  reason: contains not printable characters */
    volatile int f442 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public final AtomicInteger f443 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ  reason: contains not printable characters */
    public volatile long f444 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ  reason: contains not printable characters */
    public Handler f445;

    interface b {
        /* renamed from: ˎ  reason: contains not printable characters */
        void m411() throws o;
    }

    interface c {
        /* renamed from: ˏ  reason: contains not printable characters */
        void m412(g gVar) throws o;
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class a extends Enum<a> {

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f450 = 2;

        /* renamed from: ॱ  reason: contains not printable characters */
        public static final int f451 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static synchronized t m403() {
        t tVar;
        synchronized (t.class) {
            if (f432 == null) {
                f432 = new t();
            }
            tVar = f432;
        }
        return tVar;
    }

    private t() {
        try {
            this.f445 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            o.m359(e2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m409() {
        if (System.currentTimeMillis() - this.f444 > this.f436) {
            m404(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m404(final long j) {
        if (this.f441.compareAndSet(false, true)) {
            a.m235(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    handler.postDelayed(new d(BuildConfig.NAMESPACE, handler, new c() {
                        /* renamed from: ˏ  reason: contains not printable characters */
                        public final void m410(g gVar) throws o {
                            synchronized (t.f433) {
                                boolean z = ((f) MoatAnalytics.getInstance()).f316;
                                if (t.this.f439 != gVar.m283() || (t.this.f439 == a.f451 && z)) {
                                    t.this.f439 = gVar.m283();
                                    if (t.this.f439 == a.f451 && z) {
                                        t.this.f439 = a.f450;
                                    }
                                    if (t.this.f439 == a.f450) {
                                        a.m235(3, "OnOff", this, "Moat enabled - Version 2.4.1");
                                    }
                                    for (e eVar : t.f433) {
                                        if (t.this.f439 == a.f450) {
                                            eVar.f460.m411();
                                        }
                                    }
                                }
                                while (!t.f433.isEmpty()) {
                                    t.f433.remove();
                                }
                            }
                        }
                    }), j);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m408(b bVar) throws o {
        if (this.f439 == a.f450) {
            bVar.m411();
            return;
        }
        m397();
        f433.add(new e(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f438.compareAndSet(false, true)) {
            this.f445.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (t.f433.size() > 0) {
                            t.m397();
                            t.this.f445.postDelayed(this, 60000);
                            return;
                        }
                        t.this.f438.compareAndSet(true, false);
                        t.this.f445.removeCallbacks(this);
                    } catch (Exception e) {
                        o.m359(e);
                    }
                }
            }, 60000);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m397() {
        synchronized (f433) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<e> it = f433.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - it.next().f458.longValue() >= 60000) {
                    it.remove();
                }
            }
            if (f433.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f433.remove();
                }
            }
        }
    }

    class d implements Runnable {

        /* renamed from: ˎ  reason: contains not printable characters */
        private final String f453;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public final AnonymousClass2.AnonymousClass2 f454;

        /* renamed from: ॱ  reason: contains not printable characters */
        private final Handler f455;

        private d(String str, Handler handler, AnonymousClass2.AnonymousClass2 r4) {
            this.f454 = r4;
            this.f455 = handler;
            this.f453 = "https://z.moatads.com/" + str + "/android/" + BuildConfig.REVISION.substring(0, 7) + "/status.json";
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        private String m413() {
            try {
                return m.m337(this.f453 + "?ts=" + System.currentTimeMillis() + "&v=2.4.1").get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m413();
                final g gVar = new g(r0);
                t.this.f440 = gVar.m282();
                t.this.f437 = gVar.m286();
                t.this.f435 = gVar.m285();
                t.this.f442 = gVar.m284();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f454.m412(gVar);
                        } catch (Exception e) {
                            o.m359(e);
                        }
                    }
                });
                long unused = t.this.f444 = System.currentTimeMillis();
                t.this.f441.compareAndSet(true, false);
                if (r0 != null) {
                    t.this.f443.set(0);
                } else if (t.this.f443.incrementAndGet() < 10) {
                    t.this.m404(t.this.f434);
                }
            } catch (Exception e) {
                o.m359(e);
            }
            this.f455.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    class e {

        /* renamed from: ˎ  reason: contains not printable characters */
        final Long f458;

        /* renamed from: ॱ  reason: contains not printable characters */
        final b f460;

        e(Long l, b bVar) {
            this.f458 = l;
            this.f460 = bVar;
        }
    }
}
