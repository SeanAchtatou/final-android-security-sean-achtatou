package com.moat.analytics.mobile.vng;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.content.LocalBroadcastManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import com.facebook.appevents.AppEventsConstants;
import com.moat.analytics.mobile.vng.q;
import com.moat.analytics.mobile.vng.v;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class j {
    /* access modifiers changed from: private */
    public int a = 0;
    private boolean b = false;
    private boolean c = false;
    private final AtomicBoolean d = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    private boolean g = false;
    @NonNull
    private final WeakReference<WebView> h;
    private final Map<b, String> i;
    private final LinkedList<String> j;
    /* access modifiers changed from: private */
    public final long k;
    private final String l;
    private final List<String> m;
    private final a n;
    private final BroadcastReceiver o = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                j.this.d();
            } catch (Exception e) {
                m.a(e);
            }
            if (System.currentTimeMillis() - j.this.k > 30000) {
                j.this.i();
            }
        }
    };
    private final BroadcastReceiver p = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                j.this.e();
            } catch (Exception e) {
                m.a(e);
            }
        }
    };

    enum a {
        WEBVIEW,
        NATIVE_DISPLAY,
        NATIVE_VIDEO
    }

    j(WebView webView, a aVar) {
        this.h = new WeakReference<>(webView);
        this.n = aVar;
        this.j = new LinkedList<>();
        this.m = new ArrayList();
        this.i = new WeakHashMap();
        this.k = System.currentTimeMillis();
        this.l = String.format("javascript:(function(d,k){function l(){function d(a,b){var c=ipkn[b]||ipkn[kuea];if(c){var h=function(b){var c=b.b;c.ts=b.i;c.ticks=b.g;c.buffered=!0;a(c)};h(c.first);c.a.forEach(function(a){h(a)})}}function e(a){var b=a.a,c=a.c,h=a.b;a=a.f;var d=[];if(c)b[c]&&d.push(b[c].fn[0]);else for(key in b)if(b[key])for(var g=0,e=b[key].fn.length;g<e;g++)d.push(b[key].fn[g]);g=0;for(e=d.length;g<e;g++){var f=d[g];if('function'===typeof f)try{h?f(h):f()}catch(k){}a&&delete b[c]}}function f(a,b,c){'function'===typeof a&& (b===kuea&&c[b]?c[b].fn.push(a):c[b]={ts:+new Date,fn:[a]},c===yhgt&&d(a,b))}kuea=+new Date;iymv={};briz=!1;ewat=+new Date;bnkr=[];bjmk={};dptk={};uqaj={};ryup={};yhgt={};ipkn={};csif={};this.h=function(a){this.namespace=a.namespace;this.version=a.version;this.appName=a.appName;this.deviceOS=a.deviceOS;this.isNative=a.isNative;this.versionHash=a.versionHash;this.aqzx=a.aqzx;this.appId=a.appId;this.metadata=a};this.nvsj=function(a){briz||(f(a,ewat,iymv),briz=!0)};this.bpsy=function(a,b){var c=b||kuea; c!==kuea&&bjmk[c]||f(a,c,bjmk)};this.qmrv=function(a,b){var c=b||kuea;c!==kuea&&uqaj[c]||f(a,c,uqaj)};this.lgpr=function(a,b){f(a,b||kuea,yhgt)};this.hgen=function(a,b){f(a,b||kuea,csif)};this.xrnk=function(a){delete yhgt[a||kuea]};this.vgft=function(a){return dptk[a||kuea]||!1};this.lkpu=function(a){return ryup[a||kuea]||!1};this.crts=function(a){var b={a:iymv,b:a,c:ewat};briz?e(b):bnkr.push(a)};this.mqjh=function(a){var b=a||kuea;dptk[b]=!0;var c={a:bjmk,f:!0};b!==kuea&&(c.b=a,c.c=a);e(c)};this.egpw= function(a){var b=a||kuea;ryup[b]=!0;var c={a:uqaj,f:!0};b!==kuea&&(c.b=a,c.c=a);e(c)};this.sglu=function(a){var b=a.adKey||kuea,c={a:yhgt,b:a.event||a,g:1,i:+new Date,f:!1};b!==kuea&&(c.c=a.adKey);a=0<Object.keys(yhgt).length;if(!a||!this.isNative)if(ipkn[b]){var d=ipkn[b].a.slice(-1)[0]||ipkn[b].first;JSON.stringify(c.b)==JSON.stringify(d.b)?d.g+=1:(5<=ipkn[b].a.length&&ipkn[b].a.shift(),ipkn[b].a.push(c))}else ipkn[b]={first:c,a:[]};a&&e(c);return a};this.ucbx=function(a){e({c:a.adKey||kuea,a:csif, b:a.event,f:!1})}}'undefined'===typeof d.MoatMAK&&(d.MoatMAK=new l,d.MoatMAK.h(k),d.__zMoatInit__=!0)})(window,%s);", h());
        if (d("Initialize")) {
            IntentFilter intentFilter = new IntentFilter("UPDATE_METADATA");
            IntentFilter intentFilter2 = new IntentFilter("UPDATE_VIEW_INFO");
            LocalBroadcastManager.getInstance(q.c()).registerReceiver(this.o, intentFilter);
            LocalBroadcastManager.getInstance(q.c()).registerReceiver(this.p, intentFilter2);
            d();
            i.a().a(q.c(), this);
            o.a(3, "JavaScriptBridge", this, "bridge initialization succeeded");
        }
    }

    private boolean a(WebView webView) {
        return webView.getSettings().getJavaScriptEnabled();
    }

    static /* synthetic */ int b(j jVar) {
        int i2 = jVar.a;
        jVar.a = i2 + 1;
        return i2;
    }

    private void c() {
        for (Map.Entry<b, String> key : this.i.entrySet()) {
            b bVar = (b) key.getKey();
            if (bVar.e()) {
                g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.mqjh(\"%s\");}", bVar.e));
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            if (v.a().a != v.d.OFF) {
                if (!this.c) {
                    o.a(3, "JavaScriptBridge", this, "Attempting to establish communication (setting environment variables).");
                    this.c = true;
                }
                g(this.l);
            }
        } catch (Exception e2) {
            o.a("JavaScriptBridge", this, "Attempt failed to establish communication (did not set environment variables).", e2);
        }
    }

    private void d(b bVar) {
        o.a(3, "JavaScriptBridge", this, "Stopping view update loop");
        if (bVar != null) {
            i.a().a(bVar);
        }
    }

    private boolean d(String str) {
        WebView g2 = g();
        if (g2 == null) {
            o.a(6, "JavaScriptBridge", this, "WebView is null. Can't " + str);
            throw new m("WebView is null");
        } else if (a(g2)) {
            return true;
        } else {
            o.a(6, "JavaScriptBridge", this, "JavaScript is not enabled in the given WebView. Can't " + str);
            throw new m("JavaScript is not enabled in the WebView");
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public void e() {
        try {
            if (v.a().a != v.d.OFF) {
                if (this.g) {
                    o.a(3, "JavaScriptBridge", this, "Can't send info, already cleaned up");
                    return;
                }
                if (f()) {
                    if (!this.b || g().getUrl() != null) {
                        if (g().getUrl() != null) {
                            this.b = true;
                        }
                        for (Map.Entry<b, String> key : this.i.entrySet()) {
                            b bVar = (b) key.getKey();
                            if (bVar == null || bVar.f() == null) {
                                o.a(3, "JavaScriptBridge", this, "Tracker has no subject");
                                if (bVar != null) {
                                    if (!bVar.f) {
                                    }
                                }
                                c(bVar);
                            }
                            if (bVar.e()) {
                                if (!this.d.get()) {
                                    g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.mqjh(\"%s\");}", bVar.e));
                                }
                                String format = String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.sglu(%s);}", bVar.h());
                                if (Build.VERSION.SDK_INT >= 19) {
                                    g().evaluateJavascript(format, new ValueCallback<String>() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.moat.analytics.mobile.vng.j.a(com.moat.analytics.mobile.vng.j, boolean):boolean
                                         arg types: [com.moat.analytics.mobile.vng.j, int]
                                         candidates:
                                          com.moat.analytics.mobile.vng.j.a(com.moat.analytics.mobile.vng.j, int):int
                                          com.moat.analytics.mobile.vng.j.a(java.lang.String, org.json.JSONObject):void
                                          com.moat.analytics.mobile.vng.j.a(com.moat.analytics.mobile.vng.j, boolean):boolean */
                                        /* renamed from: a */
                                        public void onReceiveValue(String str) {
                                            String str2;
                                            if (str == null || str.equalsIgnoreCase("null") || str.equalsIgnoreCase("false")) {
                                                j jVar = j.this;
                                                StringBuilder sb = new StringBuilder();
                                                sb.append("Received value is:");
                                                if (str == null) {
                                                    str2 = "null";
                                                } else {
                                                    str2 = "(String)" + str;
                                                }
                                                sb.append(str2);
                                                o.a(3, "JavaScriptBridge", jVar, sb.toString());
                                                if (j.this.a >= 150) {
                                                    o.a(3, "JavaScriptBridge", j.this, "Giving up on finding ad");
                                                    j.this.b();
                                                }
                                                j.b(j.this);
                                                if (str != null && str.equalsIgnoreCase("false") && !j.this.e) {
                                                    boolean unused = j.this.e = true;
                                                    o.a(3, "JavaScriptBridge", j.this, "Bridge connection established");
                                                }
                                            } else if (str.equalsIgnoreCase("true")) {
                                                if (!j.this.f) {
                                                    boolean unused2 = j.this.f = true;
                                                    o.a(3, "JavaScriptBridge", j.this, "Javascript has found ad");
                                                    j.this.a();
                                                }
                                                int unused3 = j.this.a = 0;
                                            } else {
                                                o.a(3, "JavaScriptBridge", j.this, "Received unusual value from Javascript:" + str);
                                            }
                                        }
                                    });
                                } else {
                                    g().loadUrl(format);
                                }
                            }
                        }
                        return;
                    }
                }
                StringBuilder sb = new StringBuilder();
                sb.append("WebView became null");
                sb.append(g() == null ? "" : "based on null url");
                sb.append(", stopping tracking loop");
                o.a(3, "JavaScriptBridge", this, sb.toString());
                b();
            }
        } catch (Exception e2) {
            m.a(e2);
            b();
        }
    }

    private void e(String str) {
        if (this.m.size() >= 50) {
            this.m.subList(0, 25).clear();
        }
        this.m.add(str);
    }

    private void f(String str) {
        if (this.d.get()) {
            g(str);
        } else {
            e(str);
        }
    }

    private boolean f() {
        return g() != null;
    }

    private WebView g() {
        return this.h.get();
    }

    @UiThread
    private void g(String str) {
        if (this.g) {
            o.a(3, "JavaScriptBridge", this, "Can't send, already cleaned up");
        } else if (f()) {
            o.b(2, "JavaScriptBridge", this, str);
            if (Build.VERSION.SDK_INT >= 19) {
                g().evaluateJavascript(str, null);
            } else {
                g().loadUrl(str);
            }
        }
    }

    private String h() {
        try {
            q.a d2 = q.d();
            q.b e2 = q.e();
            HashMap hashMap = new HashMap();
            String a2 = d2.a();
            String b2 = d2.b();
            String c2 = d2.c();
            String num = Integer.toString(Build.VERSION.SDK_INT);
            String b3 = q.b();
            String str = this.n == a.WEBVIEW ? AppEventsConstants.EVENT_PARAM_VALUE_NO : "1";
            String str2 = e2.e ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO;
            String str3 = e2.d ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO;
            hashMap.put("versionHash", "fd28fb8353d87dc1a1db3246752e21ccc3328cbf");
            hashMap.put("appName", a2);
            hashMap.put("namespace", "VNG");
            hashMap.put("version", "2.5.1");
            hashMap.put("deviceOS", num);
            hashMap.put("isNative", str);
            hashMap.put("appId", b2);
            hashMap.put("source", c2);
            hashMap.put("carrier", e2.b);
            hashMap.put("sim", e2.a);
            hashMap.put("phone", String.valueOf(e2.c));
            hashMap.put("buildFp", Build.FINGERPRINT);
            hashMap.put("buildModel", Build.MODEL);
            hashMap.put("buildMfg", Build.MANUFACTURER);
            hashMap.put("buildBrand", Build.BRAND);
            hashMap.put("buildProduct", Build.PRODUCT);
            hashMap.put("buildTags", Build.TAGS);
            hashMap.put("f1", str3);
            hashMap.put("f2", str2);
            if (b3 != null) {
                hashMap.put("aqzx", b3);
            }
            return new JSONObject(hashMap).toString();
        } catch (Exception unused) {
            return "{}";
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        o.a(3, "JavaScriptBridge", this, "Stopping metadata reporting loop");
        i.a().a(this);
        LocalBroadcastManager.getInstance(q.c()).unregisterReceiver(this.o);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        o.a(3, "JavaScriptBridge", this, "webViewReady");
        if (this.d.compareAndSet(false, true)) {
            o.a(3, "JavaScriptBridge", this, "webViewReady first time");
            i();
            for (String g2 : this.m) {
                g(g2);
            }
            this.m.clear();
        }
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        if (bVar != null) {
            o.a(3, "JavaScriptBridge", this, "adding tracker" + bVar.e);
            this.i.put(bVar, "");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        f(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.crts(%s);}", str));
    }

    /* access modifiers changed from: package-private */
    @UiThread
    public void a(String str, JSONObject jSONObject) {
        if (this.g) {
            o.a(3, "JavaScriptBridge", this, "Can't dispatch, already cleaned up");
            return;
        }
        String jSONObject2 = jSONObject.toString();
        if (!this.d.get() || !f()) {
            this.j.add(jSONObject2);
            return;
        }
        g(String.format("javascript:%s.dispatchEvent(%s);", str, jSONObject2));
    }

    /* access modifiers changed from: package-private */
    public void b() {
        o.a(3, "JavaScriptBridge", this, "Cleaning up");
        this.g = true;
        i();
        for (Map.Entry<b, String> key : this.i.entrySet()) {
            d((b) key.getKey());
        }
        this.i.clear();
        LocalBroadcastManager.getInstance(q.c()).unregisterReceiver(this.p);
    }

    /* access modifiers changed from: package-private */
    public void b(b bVar) {
        if (d("startTracking")) {
            o.a(3, "JavaScriptBridge", this, "Starting tracking on tracker" + bVar.e);
            g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.mqjh(\"%s\");}", bVar.e));
            i.a().a(q.c(), bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        o.a(3, "JavaScriptBridge", this, "markUserInteractionEvent:" + str);
        f(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.ucbx(%s);}", str));
    }

    /* access modifiers changed from: package-private */
    public void c(b bVar) {
        m mVar = null;
        if (!this.g) {
            try {
                if (d("stopTracking")) {
                    try {
                        o.a(3, "JavaScriptBridge", this, "Ending tracking on tracker" + bVar.e);
                        g(String.format("javascript: if(typeof MoatMAK !== 'undefined'){MoatMAK.egpw(\"%s\");}", bVar.e));
                    } catch (Exception e2) {
                        o.a("JavaScriptBridge", this, "Failed to end impression.", e2);
                    }
                }
            } catch (m e3) {
                mVar = e3;
            }
            if (this.n == a.NATIVE_DISPLAY) {
                d(bVar);
            } else {
                b();
            }
            this.i.remove(bVar);
        }
        if (mVar != null) {
            throw mVar;
        }
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        o.a(3, "JavaScriptBridge", this, "flushDispatchQueue");
        if (this.j.size() >= 200) {
            LinkedList linkedList = new LinkedList();
            for (int i2 = 0; i2 < 10; i2++) {
                linkedList.addFirst(this.j.removeFirst());
            }
            int min = Math.min(Math.min(this.j.size() / 200, 10) + 200, this.j.size());
            for (int i3 = 0; i3 < min; i3++) {
                this.j.removeFirst();
            }
            Iterator it = linkedList.iterator();
            while (it.hasNext()) {
                this.j.addFirst((String) it.next());
            }
        }
        if (!this.j.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            String str2 = "";
            int i4 = 1;
            while (!this.j.isEmpty() && i4 < 200) {
                i4++;
                String removeFirst = this.j.removeFirst();
                if (sb.length() + removeFirst.length() > 2000) {
                    break;
                }
                sb.append(str2);
                sb.append(removeFirst);
                str2 = ",";
            }
            g(String.format("javascript:%s.dispatchMany([%s])", str, sb.toString()));
        }
        this.j.clear();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            super.finalize();
            o.a(3, "JavaScriptBridge", this, "finalize");
            b();
        } catch (Exception e2) {
            m.a(e2);
        }
    }
}
