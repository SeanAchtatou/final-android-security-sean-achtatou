package com.moat.analytics.mobile.vng;

import android.os.Handler;
import android.os.Looper;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class v {
    private static v h;
    /* access modifiers changed from: private */
    public static final Queue<c> i = new ConcurrentLinkedQueue();
    volatile d a = d.OFF;
    volatile boolean b = false;
    volatile boolean c = false;
    volatile int d = 200;
    volatile int e = 10;
    private long f = TapjoyConstants.SESSION_ID_INACTIVITY_TIME;
    /* access modifiers changed from: private */
    public long g = 60000;
    /* access modifiers changed from: private */
    public Handler j;
    /* access modifiers changed from: private */
    public final AtomicBoolean k = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public volatile long l = 0;
    /* access modifiers changed from: private */
    public final AtomicInteger m = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean(false);

    private class a implements Runnable {
        private final Handler b;
        private final String c;
        /* access modifiers changed from: private */
        public final e d;

        private a(String str, Handler handler, e eVar) {
            this.d = eVar;
            this.b = handler;
            this.c = "https://z.moatads.com/" + str + "/android/" + "fd28fb8353d87dc1a1db3246752e21ccc3328cbf".substring(0, 7) + "/status.json";
        }

        private void a() {
            String b2 = b();
            final l lVar = new l(b2);
            v.this.b = lVar.a();
            v.this.c = lVar.b();
            v.this.d = lVar.c();
            v.this.e = lVar.d();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    try {
                        a.this.d.a(lVar);
                    } catch (Exception e) {
                        m.a(e);
                    }
                }
            });
            long unused = v.this.l = System.currentTimeMillis();
            v.this.n.compareAndSet(true, false);
            if (b2 != null) {
                v.this.m.set(0);
            } else if (v.this.m.incrementAndGet() < 10) {
                v.this.a(v.this.g);
            }
        }

        private String b() {
            try {
                return p.a(this.c + "?ts=" + System.currentTimeMillis() + "&v=" + "2.5.1").b();
            } catch (Exception unused) {
                return null;
            }
        }

        public void run() {
            try {
                a();
            } catch (Exception e) {
                m.a(e);
            }
            this.b.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }
    }

    interface b {
        void b();

        void c();
    }

    private class c {
        final Long a;
        final b b;

        c(Long l, b bVar) {
            this.a = l;
            this.b = bVar;
        }
    }

    enum d {
        OFF,
        ON
    }

    interface e {
        void a(l lVar);
    }

    private v() {
        try {
            this.j = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            m.a(e2);
        }
    }

    static synchronized v a() {
        v vVar;
        synchronized (v.class) {
            if (h == null) {
                h = new v();
            }
            vVar = h;
        }
        return vVar;
    }

    /* access modifiers changed from: private */
    public void a(final long j2) {
        if (this.n.compareAndSet(false, true)) {
            o.a(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    handler.postDelayed(new a("VNG", handler, new e() {
                        public void a(l lVar) {
                            synchronized (v.i) {
                                boolean z = ((k) MoatAnalytics.getInstance()).a;
                                if (v.this.a != lVar.e() || (v.this.a == d.OFF && z)) {
                                    v.this.a = lVar.e();
                                    if (v.this.a == d.OFF && z) {
                                        v.this.a = d.ON;
                                    }
                                    if (v.this.a == d.ON) {
                                        o.a(3, "OnOff", this, "Moat enabled - Version 2.5.1");
                                    }
                                    for (c cVar : v.i) {
                                        if (v.this.a == d.ON) {
                                            cVar.b.b();
                                        } else {
                                            cVar.b.c();
                                        }
                                    }
                                }
                                while (!v.i.isEmpty()) {
                                    v.i.remove();
                                }
                            }
                        }
                    }), j2);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        synchronized (i) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator<c> it = i.iterator();
            while (it.hasNext()) {
                if (currentTimeMillis - it.next().a.longValue() >= 60000) {
                    it.remove();
                }
            }
            if (i.size() >= 15) {
                for (int i2 = 0; i2 < 5; i2++) {
                    i.remove();
                }
            }
        }
    }

    private void e() {
        if (this.k.compareAndSet(false, true)) {
            this.j.postDelayed(new Runnable() {
                public void run() {
                    try {
                        if (v.i.size() > 0) {
                            v.this.d();
                            v.this.j.postDelayed(this, 60000);
                            return;
                        }
                        v.this.k.compareAndSet(true, false);
                        v.this.j.removeCallbacks(this);
                    } catch (Exception e) {
                        m.a(e);
                    }
                }
            }, 60000);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar) {
        if (this.a == d.ON) {
            bVar.b();
            return;
        }
        d();
        i.add(new c(Long.valueOf(System.currentTimeMillis()), bVar));
        e();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (System.currentTimeMillis() - this.l > this.f) {
            a(0);
        }
    }
}
