package com.moat.analytics.mobile.cha;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.NoOp;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.p;
import java.lang.ref.WeakReference;
import java.util.Map;

final class k extends MoatFactory {
    k() throws o {
        if (!((f) f.getInstance()).m274()) {
            String str = "Failed to initialize MoatFactory" + ", SDK was not started";
            a.m235(3, "Factory", this, str);
            a.m232("[ERROR] ", str);
            throw new o("Failed to initialize MoatFactory");
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull WebView webView) {
        try {
            final WeakReference weakReference = new WeakReference(webView);
            return (WebAdTracker) p.m362(new p.c<WebAdTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<WebAdTracker> m335() {
                    WebView webView = (WebView) weakReference.get();
                    String str = "Attempting to create WebAdTracker for " + a.m234(webView);
                    a.m235(3, "Factory", this, str);
                    a.m232("[INFO] ", str);
                    return Optional.of(new v(webView));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m359(e);
            return new NoOp.e();
        }
    }

    public final WebAdTracker createWebAdTracker(@NonNull ViewGroup viewGroup) {
        try {
            final WeakReference weakReference = new WeakReference(viewGroup);
            return (WebAdTracker) p.m362(new p.c<WebAdTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<WebAdTracker> m334() throws o {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    String str = "Attempting to create WebAdTracker for adContainer " + a.m234(viewGroup);
                    a.m235(3, "Factory", this, str);
                    a.m232("[INFO] ", str);
                    return Optional.of(new v(viewGroup));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m359(e);
            return new NoOp.e();
        }
    }

    public final NativeDisplayTracker createNativeDisplayTracker(@NonNull View view, @NonNull final Map<String, String> map) {
        try {
            final WeakReference weakReference = new WeakReference(view);
            return (NativeDisplayTracker) p.m362(new p.c<NativeDisplayTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<NativeDisplayTracker> m332() {
                    View view = (View) weakReference.get();
                    String str = "Attempting to create NativeDisplayTracker for " + a.m234(view);
                    a.m235(3, "Factory", this, str);
                    a.m232("[INFO] ", str);
                    return Optional.of(new q(view, map));
                }
            }, NativeDisplayTracker.class);
        } catch (Exception e) {
            o.m359(e);
            return new NoOp.c();
        }
    }

    public final NativeVideoTracker createNativeVideoTracker(final String str) {
        try {
            return (NativeVideoTracker) p.m362(new p.c<NativeVideoTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<NativeVideoTracker> m333() {
                    a.m235(3, "Factory", this, "Attempting to create NativeVideoTracker");
                    a.m232("[INFO] ", "Attempting to create NativeVideoTracker");
                    return Optional.of(new s(str));
                }
            }, NativeVideoTracker.class);
        } catch (Exception e) {
            o.m359(e);
            return new NoOp.b();
        }
    }

    public final <T> T createCustomTracker(l<T> lVar) {
        try {
            return lVar.create();
        } catch (Exception e) {
            o.m359(e);
            return lVar.createNoOp();
        }
    }
}
