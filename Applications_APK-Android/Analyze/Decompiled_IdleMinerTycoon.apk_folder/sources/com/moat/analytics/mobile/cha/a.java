package com.moat.analytics.mobile.cha;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.moat.analytics.mobile.cha.j;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class a {

    /* renamed from: ˊ  reason: contains not printable characters */
    final String f270;

    /* renamed from: ˋ  reason: contains not printable characters */
    WebView f271;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f272;

    /* renamed from: ˏ  reason: contains not printable characters */
    j f273;

    /* renamed from: ॱ  reason: contains not printable characters */
    private final int f274;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class d extends Enum<d> {

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f277 = 2;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f278 = 1;

        static {
            int[] iArr = {1, 2};
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    a(Application application, int i) {
        this.f274 = i;
        this.f272 = false;
        this.f270 = String.format(Locale.ROOT, "_moatTracker%d", Integer.valueOf((int) (Math.random() * 1.0E8d)));
        this.f271 = new WebView(application);
        WebSettings settings = this.f271.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        try {
            this.f273 = new j(this.f271, i == d.f277 ? j.e.f375 : j.e.f376);
        } catch (o e) {
            o.m359(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m239(String str) {
        if (this.f274 == d.f278) {
            this.f271.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f272) {
                        try {
                            boolean unused = a.this.f272 = true;
                            a.this.f273.m328();
                        } catch (Exception e) {
                            o.m359(e);
                        }
                    }
                }
            });
            WebView webView = this.f271;
            webView.loadData("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/" + str + "/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>", "text/html", "utf-8");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m240(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f274 == d.f277) {
            this.f271.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f272) {
                        try {
                            boolean unused = a.this.f272 = true;
                            a.this.f273.m328();
                            a.this.f273.m327(a.this.f270);
                        } catch (Exception e) {
                            o.m359(e);
                        }
                    }
                }
            });
            JSONObject jSONObject = new JSONObject(map);
            WebView webView = this.f271;
            String str2 = this.f270;
            webView.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", "mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), str2, str, jSONObject.toString(), num3), "text/html", null);
        }
    }

    a() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m235(int i, String str, Object obj, String str2) {
        if (!t.m403().f440) {
            return;
        }
        if (obj == null) {
            Log.println(i, "Moat" + str, String.format("message = %s", str2));
            return;
        }
        Log.println(i, "Moat" + str, String.format("id = %s, message = %s", Integer.valueOf(obj.hashCode()), str2));
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m236(String str, Object obj, String str2) {
        Object obj2;
        if (t.m403().f437) {
            String str3 = "Moat" + str;
            Object[] objArr = new Object[2];
            if (obj == null) {
                obj2 = "null";
            } else {
                obj2 = Integer.valueOf(obj.hashCode());
            }
            objArr[0] = obj2;
            objArr[1] = str2;
            Log.println(2, str3, String.format("id = %s, message = %s", objArr));
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m237(String str, Object obj, String str2, Exception exc) {
        if (t.m403().f440) {
            Log.e("Moat" + str, String.format("id = %s, message = %s", Integer.valueOf(obj.hashCode()), str2), exc);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static void m232(String str, String str2) {
        if (!t.m403().f440 && ((f) MoatAnalytics.getInstance()).f316) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            Log.println(i, "MoatAnalytics", str + str2);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static String m234(View view) {
        if (view == null) {
            return "null";
        }
        return view.getClass().getSimpleName() + "@" + view.hashCode();
    }
}
