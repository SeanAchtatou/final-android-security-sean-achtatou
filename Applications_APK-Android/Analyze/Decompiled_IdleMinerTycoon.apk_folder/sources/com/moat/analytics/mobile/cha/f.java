package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.cha.a;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class f extends MoatAnalytics implements t.b {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f311 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f312;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MoatOptions f313;

    /* renamed from: ˊ  reason: contains not printable characters */
    WeakReference<Context> f314;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f315 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f316 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f317 = false;
    @Nullable

    /* renamed from: ॱ  reason: contains not printable characters */
    a f318;

    f() {
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    @UiThread
    public final void prepareNativeDisplayTracking(String str) {
        this.f312 = str;
        if (t.m403().f439 != t.a.f451) {
            try {
                m273();
            } catch (Exception e) {
                o.m359(e);
            }
        }
    }

    @UiThread
    /* renamed from: ˏ  reason: contains not printable characters */
    private void m273() {
        if (this.f318 == null) {
            this.f318 = new a(c.m256(), a.d.f278);
            this.f318.m239(this.f312);
            a.m235(3, "Analytics", this, "Preparing native display tracking with partner code " + this.f312);
            a.m232("[SUCCESS] ", "Prepared for native display tracking with partner code " + this.f312);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m274() {
        return this.f311;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final boolean m275() {
        return this.f313 != null && this.f313.disableLocationServices;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m276() throws o {
        o.m360();
        n.m345();
        if (this.f312 != null) {
            try {
                m273();
            } catch (Exception e) {
                o.m359(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f311) {
                a.m235(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f313 = moatOptions;
            t.m403().m409();
            this.f315 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && r.m378(application.getApplicationContext())) {
                    this.f316 = true;
                }
                this.f314 = new WeakReference<>(application.getApplicationContext());
                this.f311 = true;
                this.f317 = moatOptions.autoTrackGMAInterstitials;
                c.m258(application);
                t.m403().m408(this);
                if (!moatOptions.disableAdIdCollection) {
                    r.m381(application);
                }
                a.m232("[SUCCESS] ", "Moat Analytics SDK Version 2.4.1 started");
                return;
            }
            throw new o("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            o.m359(e);
        }
    }
}
