package com.moat.analytics.mobile.cha;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.cha.NativeDisplayTracker;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class q extends d implements NativeDisplayTracker {

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private final Map<String, String> f412;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Set<NativeDisplayTracker.MoatUserInteractionType> f413 = new HashSet();

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m372() {
        return "NativeDisplayTracker";
    }

    q(View view, Map<String, String> map) {
        super(view, true, false);
        a.m235(3, "NativeDisplayTracker", this, "Initializing.");
        this.f412 = map;
        if (view == null) {
            String str = "NativeDisplayTracker initialization not successful, " + "Target view is null";
            a.m235(3, "NativeDisplayTracker", this, str);
            a.m232("[ERROR] ", str);
            this.f307 = new o("Target view is null");
        } else if (map == null || map.isEmpty()) {
            String str2 = "NativeDisplayTracker initialization not successful, " + "AdIds is null or empty";
            a.m235(3, "NativeDisplayTracker", this, str2);
            a.m232("[ERROR] ", str2);
            this.f307 = new o("AdIds is null or empty");
        } else {
            a aVar = ((f) f.getInstance()).f318;
            if (aVar == null) {
                String str3 = "NativeDisplayTracker initialization not successful, " + "prepareNativeDisplayTracking was not called successfully";
                a.m235(3, "NativeDisplayTracker", this, str3);
                a.m232("[ERROR] ", str3);
                this.f307 = new o("prepareNativeDisplayTracking was not called successfully");
                return;
            }
            this.f304 = aVar.f273;
            try {
                super.m270(aVar.f271);
                if (this.f304 != null) {
                    this.f304.m329(m370());
                }
                a.m232("[SUCCESS] ", "NativeDisplayTracker created for " + m261() + ", with adIds:" + map.toString());
            } catch (o e) {
                this.f307 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType moatUserInteractionType) {
        try {
            a.m235(3, "NativeDisplayTracker", this, "reportUserInteractionEvent:" + moatUserInteractionType.name());
            if (!this.f413.contains(moatUserInteractionType)) {
                this.f413.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f303);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f304 != null) {
                    this.f304.m330(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            a.m236("NativeDisplayTracker", this, "Got JSON exception");
            o.m359(e);
        } catch (Exception e2) {
            o.m359(e2);
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private String m370() {
        try {
            Map<String, String> map = this.f412;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                String str = "moatClientLevel" + i;
                if (map.containsKey(str)) {
                    linkedHashMap.put(str, map.get(str));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                String str2 = "moatClientSlicer" + i2;
                if (map.containsKey(str2)) {
                    linkedHashMap.put(str2, map.get(str2));
                }
            }
            for (String next : map.keySet()) {
                if (!linkedHashMap.containsKey(next)) {
                    linkedHashMap.put(next, map.get(next));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            a.m235(3, "NativeDisplayTracker", this, "Parsed ad ids = " + jSONObject);
            return "{\"adIds\":" + jSONObject + ", \"adKey\":\"" + this.f303 + "\", \"adSize\":" + m371() + "}";
        } catch (Exception e) {
            o.m359(e);
            return "";
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String m371() {
        try {
            Rect r0 = u.m418(super.m262());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m359(e);
            return null;
        }
    }
}
