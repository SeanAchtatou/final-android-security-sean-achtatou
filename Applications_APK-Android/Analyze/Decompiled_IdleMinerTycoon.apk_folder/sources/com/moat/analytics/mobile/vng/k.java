package com.moat.analytics.mobile.vng;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import com.moat.analytics.mobile.vng.g;
import com.moat.analytics.mobile.vng.v;
import java.lang.ref.WeakReference;

class k extends MoatAnalytics implements v.b {
    boolean a = false;
    boolean b = false;
    @Nullable
    g c;
    WeakReference<Context> d;
    private boolean e = false;
    private String f;
    private MoatOptions g;

    k() {
    }

    private void a(MoatOptions moatOptions, Application application) {
        if (this.e) {
            o.a(3, "Analytics", this, "Moat SDK has already been started.");
            return;
        }
        this.g = moatOptions;
        v.a().b();
        if (application != null) {
            if (moatOptions.loggingEnabled && q.b(application.getApplicationContext())) {
                this.a = true;
            }
            this.d = new WeakReference<>(application.getApplicationContext());
            this.e = true;
            this.b = moatOptions.autoTrackGMAInterstitials;
            a.a(application);
            v.a().a(this);
            if (!moatOptions.disableAdIdCollection) {
                q.a(application);
            }
            o.a("[SUCCESS] ", "Moat Analytics SDK Version 2.5.1 started");
            return;
        }
        throw new m("Moat Analytics SDK didn't start, application was null");
    }

    @UiThread
    private void d() {
        if (this.c == null) {
            this.c = new g(a.a(), g.a.DISPLAY);
            this.c.a(this.f);
            o.a(3, "Analytics", this, "Preparing native display tracking with partner code " + this.f);
            o.a("[SUCCESS] ", "Prepared for native display tracking with partner code " + this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.e;
    }

    public void b() {
        m.a();
        if (this.f != null) {
            try {
                d();
            } catch (Exception e2) {
                m.a(e2);
            }
        }
    }

    public void c() {
    }

    @UiThread
    public void prepareNativeDisplayTracking(String str) {
        this.f = str;
        if (v.a().a != v.d.OFF) {
            try {
                d();
            } catch (Exception e2) {
                m.a(e2);
            }
        }
    }

    public void start(Application application) {
        start(new MoatOptions(), application);
    }

    public void start(MoatOptions moatOptions, Application application) {
        try {
            a(moatOptions, application);
        } catch (Exception e2) {
            m.a(e2);
        }
    }
}
