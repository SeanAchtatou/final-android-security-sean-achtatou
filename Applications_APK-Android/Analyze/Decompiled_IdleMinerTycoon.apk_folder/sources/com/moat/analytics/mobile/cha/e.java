package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class e {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static WeakReference<Activity> f309 = new WeakReference<>(null);
    @Nullable

    /* renamed from: ˋ  reason: contains not printable characters */
    private static WebAdTracker f310;

    e() {
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m272(Activity activity) {
        try {
            if (t.m403().f439 != t.a.f451) {
                String name = activity.getClass().getName();
                a.m235(3, "GMAInterstitialHelper", activity, "Activity name: " + name);
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f310 != null) {
                        a.m235(3, "GMAInterstitialHelper", f309.get(), "Stopping to track GMA interstitial");
                        f310.stopTracking();
                        f310 = null;
                    }
                    f309 = new WeakReference<>(null);
                } else if (f309.get() == null || f309.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional<WebView> r0 = x.m431((ViewGroup) decorView, true);
                        if (r0.isPresent()) {
                            f309 = new WeakReference<>(activity);
                            a.m235(3, "GMAInterstitialHelper", f309.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(r0.get());
                            f310 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        a.m235(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            o.m359(e);
        }
    }
}
