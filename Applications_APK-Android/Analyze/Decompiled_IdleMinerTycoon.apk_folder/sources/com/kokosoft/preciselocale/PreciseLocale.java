package com.kokosoft.preciselocale;

import android.content.res.Resources;
import android.text.TextUtils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Currency;
import java.util.Locale;

public class PreciseLocale {
    private static final Currency currency = Currency.getInstance(locale);
    private static final Locale locale = Resources.getSystem().getConfiguration().locale;

    public static String getLanguageID() {
        String region = getRegion();
        String language = getLanguage();
        if (TextUtils.isEmpty(region)) {
            return language;
        }
        return language + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + region;
    }

    public static String getLanguage() {
        return locale.getLanguage();
    }

    public static String getRegion() {
        return locale.getCountry();
    }

    public static String getCurrencySymbol() {
        return currency.getSymbol();
    }

    public static String getCurrencyCode() {
        return currency.getCurrencyCode();
    }
}
