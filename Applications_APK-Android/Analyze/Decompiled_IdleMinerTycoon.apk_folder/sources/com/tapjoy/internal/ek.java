package com.tapjoy.internal;

import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;

public final class ek {
    final it a;
    private long b = 0;
    private long c = Long.MAX_VALUE;
    private int d;
    private int e = 2;
    private int f = -1;
    private long g = -1;
    private eg h;

    public ek(it itVar) {
        this.a = itVar;
    }

    public final long a() {
        if (this.e == 2) {
            int i = this.d + 1;
            this.d = i;
            if (i <= 65) {
                long j = this.g;
                this.g = -1;
                this.e = 6;
                return j;
            }
            throw new IOException("Wire recursion limit exceeded");
        }
        throw new IllegalStateException("Unexpected call to beginMessage()");
    }

    public final void a(long j) {
        if (this.e == 6) {
            int i = this.d - 1;
            this.d = i;
            if (i < 0 || this.g != -1) {
                throw new IllegalStateException("No corresponding call to beginMessage()");
            } else if (this.b == this.c || this.d == 0) {
                this.c = j;
            } else {
                throw new IOException("Expected to end at " + this.c + " but was " + this.b);
            }
        } else {
            throw new IllegalStateException("Unexpected call to endMessage()");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b8 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int b() {
        /*
            r6 = this;
            int r0 = r6.e
            r1 = 2
            r2 = 7
            if (r0 != r2) goto L_0x000b
            r6.e = r1
            int r0 = r6.f
            return r0
        L_0x000b:
            int r0 = r6.e
            r2 = 6
            if (r0 != r2) goto L_0x00c2
        L_0x0010:
            long r2 = r6.b
            long r4 = r6.c
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c0
            com.tapjoy.internal.it r0 = r6.a
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x00c0
            int r0 = r6.i()
            if (r0 == 0) goto L_0x00b8
            int r2 = r0 >> 3
            r6.f = r2
            r0 = r0 & 7
            switch(r0) {
                case 0: goto L_0x00ae;
                case 1: goto L_0x00a4;
                case 2: goto L_0x005b;
                case 3: goto L_0x0055;
                case 4: goto L_0x004d;
                case 5: goto L_0x0043;
                default: goto L_0x002f;
            }
        L_0x002f:
            java.net.ProtocolException r1 = new java.net.ProtocolException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Unexpected field encoding: "
            r2.<init>(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x0043:
            com.tapjoy.internal.eg r0 = com.tapjoy.internal.eg.FIXED32
            r6.h = r0
            r0 = 5
            r6.e = r0
            int r0 = r6.f
            return r0
        L_0x004d:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Unexpected end group"
            r0.<init>(r1)
            throw r0
        L_0x0055:
            int r0 = r6.f
            r6.a(r0)
            goto L_0x0010
        L_0x005b:
            com.tapjoy.internal.eg r0 = com.tapjoy.internal.eg.LENGTH_DELIMITED
            r6.h = r0
            r6.e = r1
            int r0 = r6.i()
            if (r0 < 0) goto L_0x0090
            long r1 = r6.g
            r3 = -1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x008a
            long r1 = r6.c
            r6.g = r1
            long r1 = r6.b
            long r3 = (long) r0
            long r1 = r1 + r3
            r6.c = r1
            long r0 = r6.c
            long r2 = r6.g
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 > 0) goto L_0x0084
            int r0 = r6.f
            return r0
        L_0x0084:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            throw r0
        L_0x008a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0090:
            java.net.ProtocolException r1 = new java.net.ProtocolException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Negative length: "
            r2.<init>(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x00a4:
            com.tapjoy.internal.eg r0 = com.tapjoy.internal.eg.FIXED64
            r6.h = r0
            r0 = 1
            r6.e = r0
            int r0 = r6.f
            return r0
        L_0x00ae:
            com.tapjoy.internal.eg r0 = com.tapjoy.internal.eg.VARINT
            r6.h = r0
            r0 = 0
            r6.e = r0
            int r0 = r6.f
            return r0
        L_0x00b8:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Unexpected tag 0"
            r0.<init>(r1)
            throw r0
        L_0x00c0:
            r0 = -1
            return r0
        L_0x00c2:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Unexpected call to nextTag()"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.ek.b():int");
    }

    public final eg c() {
        return this.h;
    }

    private void a(int i) {
        while (this.b < this.c && !this.a.b()) {
            int i2 = i();
            if (i2 != 0) {
                int i3 = i2 >> 3;
                int i4 = i2 & 7;
                switch (i4) {
                    case 0:
                        this.e = 0;
                        e();
                        break;
                    case 1:
                        this.e = 1;
                        g();
                        break;
                    case 2:
                        long i5 = (long) i();
                        this.b += i5;
                        this.a.d(i5);
                        break;
                    case 3:
                        a(i3);
                        break;
                    case 4:
                        if (i3 != i) {
                            throw new ProtocolException("Unexpected end group");
                        }
                        return;
                    case 5:
                        this.e = 5;
                        f();
                        break;
                    default:
                        throw new ProtocolException("Unexpected field encoding: " + i4);
                }
            } else {
                throw new ProtocolException("Unexpected tag 0");
            }
        }
        throw new EOFException();
    }

    public final int d() {
        if (this.e == 0 || this.e == 2) {
            int i = i();
            b(0);
            return i;
        }
        throw new ProtocolException("Expected VARINT or LENGTH_DELIMITED but was " + this.e);
    }

    private int i() {
        this.b++;
        byte c2 = this.a.c();
        if (c2 >= 0) {
            return c2;
        }
        byte b2 = c2 & Byte.MAX_VALUE;
        this.b++;
        byte c3 = this.a.c();
        if (c3 >= 0) {
            return b2 | (c3 << 7);
        }
        byte b3 = b2 | ((c3 & Byte.MAX_VALUE) << 7);
        this.b++;
        byte c4 = this.a.c();
        if (c4 >= 0) {
            return b3 | (c4 << 14);
        }
        byte b4 = b3 | ((c4 & Byte.MAX_VALUE) << 14);
        this.b++;
        byte c5 = this.a.c();
        if (c5 >= 0) {
            return b4 | (c5 << 21);
        }
        byte b5 = b4 | ((c5 & Byte.MAX_VALUE) << 21);
        this.b++;
        byte c6 = this.a.c();
        byte b6 = b5 | (c6 << 28);
        if (c6 >= 0) {
            return b6;
        }
        for (int i = 0; i < 5; i++) {
            this.b++;
            if (this.a.c() >= 0) {
                return b6;
            }
        }
        throw new ProtocolException("Malformed VARINT");
    }

    public final long e() {
        if (this.e == 0 || this.e == 2) {
            long j = 0;
            for (int i = 0; i < 64; i += 7) {
                this.b++;
                byte c2 = this.a.c();
                j |= ((long) (c2 & Byte.MAX_VALUE)) << i;
                if ((c2 & 128) == 0) {
                    b(0);
                    return j;
                }
            }
            throw new ProtocolException("WireInput encountered a malformed varint");
        }
        throw new ProtocolException("Expected VARINT or LENGTH_DELIMITED but was " + this.e);
    }

    public final int f() {
        if (this.e == 5 || this.e == 2) {
            this.a.a(4);
            this.b += 4;
            int e2 = this.a.e();
            b(5);
            return e2;
        }
        throw new ProtocolException("Expected FIXED32 or LENGTH_DELIMITED but was " + this.e);
    }

    public final long g() {
        if (this.e == 1 || this.e == 2) {
            this.a.a(8);
            this.b += 8;
            long f2 = this.a.f();
            b(1);
            return f2;
        }
        throw new ProtocolException("Expected FIXED64 or LENGTH_DELIMITED but was " + this.e);
    }

    private void b(int i) {
        if (this.e == i) {
            this.e = 6;
        } else if (this.b > this.c) {
            throw new IOException("Expected to end at " + this.c + " but was " + this.b);
        } else if (this.b == this.c) {
            this.c = this.g;
            this.g = -1;
            this.e = 6;
        } else {
            this.e = 7;
        }
    }

    /* access modifiers changed from: package-private */
    public final long h() {
        if (this.e == 2) {
            long j = this.c - this.b;
            this.a.a(j);
            this.e = 6;
            this.b = this.c;
            this.c = this.g;
            this.g = -1;
            return j;
        }
        throw new ProtocolException("Expected LENGTH_DELIMITED but was " + this.e);
    }
}
