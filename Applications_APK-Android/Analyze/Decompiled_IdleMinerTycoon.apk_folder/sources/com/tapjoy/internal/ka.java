package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants;
import com.tapjoy.internal.kf;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public abstract class ka implements kf {
    /* access modifiers changed from: private */
    public static final Logger a = Logger.getLogger(ka.class.getName());
    private final kf b = new kc() {
        /* access modifiers changed from: protected */
        public final void a() {
            new Executor() {
                public final void execute(Runnable runnable) {
                    new Thread(runnable, ka.this.getClass().getSimpleName()).start();
                }
            }.execute(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
                 arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
                 candidates:
                  ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
                  ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
                  ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
                  ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
                public final void run() {
                    try {
                        ka.this.b();
                        AnonymousClass1.this.c();
                        if (AnonymousClass1.this.f() == kf.a.RUNNING) {
                            ka.this.d();
                        }
                        ka.this.c();
                        AnonymousClass1.this.d();
                    } catch (Throwable th) {
                        AnonymousClass1.this.a(th);
                        throw jr.a(th);
                    }
                }
            });
        }

        /* access modifiers changed from: protected */
        public final void b() {
            ka.this.a();
        }
    };

    public void a() {
    }

    public void b() {
    }

    public void c() {
    }

    public abstract void d();

    public String toString() {
        return getClass().getSimpleName() + " [" + f() + Constants.RequestParameters.RIGHT_BRACKETS;
    }

    public final ke e() {
        return this.b.e();
    }

    public final kf.a f() {
        return this.b.f();
    }
}
