package com.tapjoy.internal;

import android.content.Context;
import android.graphics.Matrix;
import android.view.MotionEvent;

public class ai extends aj {
    private int a = 0;
    private final Matrix b = new Matrix();
    private final float[] c = new float[2];

    public ai(Context context) {
        super(context);
    }

    public int getRotationCount() {
        return this.a;
    }

    public void setRotationCount(int i) {
        this.a = i & 3;
    }

    public void onMeasure(int i, int i2) {
        if (this.a % 2 == 0) {
            super.onMeasure(i, i2);
            return;
        }
        super.onMeasure(i2, i);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(14:4|5|6|7|8|(1:12)|13|14|15|17|23|24|30|31) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x002c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispatchDraw(android.graphics.Canvas r9) {
        /*
            r8 = this;
            int r0 = r8.a
            if (r0 != 0) goto L_0x0008
            super.dispatchDraw(r9)
            return
        L_0x0008:
            r9.save()
            int r0 = r8.getWidth()
            int r1 = r8.getHeight()
            r2 = 0
            r9.clipRect(r2, r2, r0, r1)
            android.view.ViewParent r3 = r8.getParent()     // Catch:{ Exception -> 0x004e }
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3     // Catch:{ Exception -> 0x004e }
            android.view.ViewParent r4 = r3.getParent()     // Catch:{ Exception -> 0x002c }
            android.view.ViewGroup r4 = (android.view.ViewGroup) r4     // Catch:{ Exception -> 0x002c }
            boolean r5 = r4 instanceof android.widget.ScrollView     // Catch:{ Exception -> 0x002c }
            if (r5 != 0) goto L_0x002b
            boolean r5 = r4 instanceof android.widget.HorizontalScrollView     // Catch:{ Exception -> 0x002c }
            if (r5 == 0) goto L_0x002c
        L_0x002b:
            r3 = r4
        L_0x002c:
            int r4 = r8.getLeft()     // Catch:{ Exception -> 0x004e }
            int r5 = r3.getScrollX()     // Catch:{ Exception -> 0x004e }
            int r4 = r4 - r5
            int r5 = r8.getTop()     // Catch:{ Exception -> 0x004e }
            int r6 = r3.getScrollY()     // Catch:{ Exception -> 0x004e }
            int r5 = r5 - r6
            int r6 = 0 - r4
            int r2 = r2 - r5
            int r7 = r3.getWidth()     // Catch:{ Exception -> 0x004e }
            int r7 = r7 - r4
            int r3 = r3.getHeight()     // Catch:{ Exception -> 0x004e }
            int r3 = r3 - r5
            r9.clipRect(r6, r2, r7, r3)     // Catch:{ Exception -> 0x004e }
        L_0x004e:
            int r2 = r8.a
            int r2 = r2 * 90
            float r2 = (float) r2
            r9.rotate(r2)
            int r2 = r8.a
            r3 = 0
            switch(r2) {
                case 1: goto L_0x0070;
                case 2: goto L_0x0068;
                case 3: goto L_0x0062;
                default: goto L_0x005c;
            }
        L_0x005c:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            r9.<init>()
            throw r9
        L_0x0062:
            int r2 = -r1
            float r2 = (float) r2
            r9.translate(r2, r3)
            goto L_0x0075
        L_0x0068:
            int r2 = -r0
            float r2 = (float) r2
            int r4 = -r1
            float r4 = (float) r4
            r9.translate(r2, r4)
            goto L_0x0075
        L_0x0070:
            int r2 = -r0
            float r2 = (float) r2
            r9.translate(r3, r2)
        L_0x0075:
            android.graphics.Matrix r2 = r8.b
            int r4 = r8.a
            int r4 = r4 * -90
            float r4 = (float) r4
            r2.setRotate(r4)
            int r2 = r8.a
            switch(r2) {
                case 1: goto L_0x009f;
                case 2: goto L_0x0093;
                case 3: goto L_0x008a;
                default: goto L_0x0084;
            }
        L_0x0084:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            r9.<init>()
            throw r9
        L_0x008a:
            android.graphics.Matrix r0 = r8.b
            int r1 = r1 + -1
            float r1 = (float) r1
            r0.postTranslate(r1, r3)
            goto L_0x00a7
        L_0x0093:
            android.graphics.Matrix r2 = r8.b
            int r0 = r0 + -1
            float r0 = (float) r0
            int r1 = r1 + -1
            float r1 = (float) r1
            r2.postTranslate(r0, r1)
            goto L_0x00a7
        L_0x009f:
            android.graphics.Matrix r1 = r8.b
            int r0 = r0 + -1
            float r0 = (float) r0
            r1.postTranslate(r3, r0)
        L_0x00a7:
            super.dispatchDraw(r9)
            r9.restore()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.ai.dispatchDraw(android.graphics.Canvas):void");
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.a == 0) {
            return super.dispatchTouchEvent(motionEvent);
        }
        float[] fArr = this.c;
        fArr[0] = motionEvent.getX();
        fArr[1] = motionEvent.getY();
        this.b.mapPoints(fArr);
        motionEvent.setLocation(fArr[0], fArr[1]);
        return super.dispatchTouchEvent(motionEvent);
    }
}
