package com.tapjoy.internal;

import javax.annotation.Nullable;

public final class cc implements Runnable {
    private final ca a;
    @Nullable
    private final cf b;

    protected cc(ca caVar, @Nullable cf cfVar) {
        this.a = caVar;
        this.b = cfVar;
    }

    public final void run() {
        try {
            Object f = this.a.f();
            if (this.b != null && !(this.b instanceof cg)) {
                this.b.a(this.a, f);
            }
        } catch (Throwable unused) {
            if (this.b != null && !(this.b instanceof cg)) {
                this.b.a(this.a);
            }
        }
    }
}
