package com.tapjoy.internal;

import com.ironsource.sdk.constants.Constants;

public enum cw {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE(Constants.ParametersKeys.ORIENTATION_NONE);
    
    private final String d;

    private cw(String str) {
        this.d = str;
    }

    public final String toString() {
        return this.d;
    }
}
