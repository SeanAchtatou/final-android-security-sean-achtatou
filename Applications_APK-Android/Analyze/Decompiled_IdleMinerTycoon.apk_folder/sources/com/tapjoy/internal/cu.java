package com.tapjoy.internal;

import android.webkit.WebView;
import java.util.ArrayList;
import java.util.List;

public final class cu {
    public final cx a;
    final WebView b;
    public final List c = new ArrayList();
    final String d;
    public final String e;
    public final cv f;

    private cu(cx cxVar, String str, List list, String str2) {
        cv cvVar;
        this.a = cxVar;
        this.b = null;
        this.d = str;
        if (list != null) {
            this.c.addAll(list);
            cvVar = cv.NATIVE;
        } else {
            cvVar = cv.HTML;
        }
        this.f = cvVar;
        this.e = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tapjoy.internal.do.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.tapjoy.internal.do.a(java.lang.String, java.lang.String):void
      com.tapjoy.internal.do.a(java.lang.Object, java.lang.String):void */
    public static cu a(cx cxVar, String str, List list, String str2) {
        Cdo.a(cxVar, "Partner is null");
        Cdo.a((Object) str, "OM SDK JS script content is null");
        Cdo.a(list, "VerificationScriptResources is null");
        if (str2.length() <= 256) {
            return new cu(cxVar, str, list, str2);
        }
        throw new IllegalArgumentException("CustomReferenceData is greater than 256 characters");
    }
}
