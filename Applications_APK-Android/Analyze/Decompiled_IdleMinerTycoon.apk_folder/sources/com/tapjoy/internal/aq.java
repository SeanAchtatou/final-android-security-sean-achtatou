package com.tapjoy.internal;

import javax.annotation.Nullable;

public abstract class aq implements an {
    /* access modifiers changed from: protected */
    @Nullable
    public abstract ao a(Object obj, boolean z);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tapjoy.internal.aq.a(java.lang.Object, boolean):com.tapjoy.internal.ao
     arg types: [java.lang.Object, int]
     candidates:
      com.tapjoy.internal.aq.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.an.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.aq.a(java.lang.Object, boolean):com.tapjoy.internal.ao */
    public final Object a(Object obj) {
        ao a = a(obj, false);
        if (a != null) {
            return a.a();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tapjoy.internal.aq.a(java.lang.Object, boolean):com.tapjoy.internal.ao
     arg types: [java.lang.Object, int]
     candidates:
      com.tapjoy.internal.aq.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.an.a(java.lang.Object, java.lang.Object):void
      com.tapjoy.internal.aq.a(java.lang.Object, boolean):com.tapjoy.internal.ao */
    public void a(Object obj, Object obj2) {
        a(obj, true).a(obj2);
    }
}
