package com.tapjoy.internal;

import com.applovin.mediation.unity.BuildConfig;
import com.vungle.warren.model.Advertisement;

public enum da {
    PREROLL("preroll"),
    MIDROLL("midroll"),
    POSTROLL(Advertisement.KEY_POSTROLL),
    STANDALONE(BuildConfig.FLAVOR);
    
    private final String e;

    private da(String str) {
        this.e = str;
    }

    public final String toString() {
        return this.e;
    }
}
