package com.tapjoy.internal;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewParent;
import com.tapjoy.internal.di;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public final class dv implements di.a {
    public static Handler a = new Handler(Looper.getMainLooper());
    private static dv c = new dv();
    /* access modifiers changed from: private */
    public static Handler d = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public final void run() {
            dv.b(dv.a());
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public final void run() {
            if (dv.d != null) {
                dv.d.post(dv.j);
                dv.d.postDelayed(dv.k, 200);
            }
        }
    };
    public List b = new ArrayList();
    private int e;
    private dj f = new dj();
    private dw g = new dw();
    /* access modifiers changed from: private */
    public ed h = new ed(new dz());
    private long i;

    dv() {
    }

    public static dv a() {
        return c;
    }

    private void a(long j2) {
        if (this.b.size() > 0) {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                it.next();
                TimeUnit.NANOSECONDS.toMillis(j2);
            }
        }
    }

    private void a(View view, di diVar, JSONObject jSONObject, int i2) {
        diVar.a(view, jSONObject, this, i2 == ee.a);
    }

    public static void b() {
        if (d == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            d = handler;
            handler.post(j);
            d.postDelayed(k, 200);
        }
    }

    public static void c() {
        if (d != null) {
            d.removeCallbacks(k);
            d = null;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.View r7, com.tapjoy.internal.di r8, org.json.JSONObject r9) {
        /*
            r6 = this;
            java.lang.String r0 = com.tapjoy.internal.dp.c(r7)
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x000a
            r0 = 1
            goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            if (r0 != 0) goto L_0x000e
            return
        L_0x000e:
            com.tapjoy.internal.dw r0 = r6.g
            java.util.HashSet r3 = r0.d
            boolean r3 = r3.contains(r7)
            if (r3 == 0) goto L_0x001b
            int r0 = com.tapjoy.internal.ee.a
            goto L_0x0024
        L_0x001b:
            boolean r0 = r0.h
            if (r0 == 0) goto L_0x0022
            int r0 = com.tapjoy.internal.ee.b
            goto L_0x0024
        L_0x0022:
            int r0 = com.tapjoy.internal.ee.c
        L_0x0024:
            int r3 = com.tapjoy.internal.ee.c
            if (r0 != r3) goto L_0x0029
            return
        L_0x0029:
            org.json.JSONObject r3 = r8.a(r7)
            com.tapjoy.internal.dm.a(r9, r3)
            com.tapjoy.internal.dw r9 = r6.g
            java.util.HashMap r4 = r9.a
            int r4 = r4.size()
            r5 = 0
            if (r4 != 0) goto L_0x003d
            r4 = r5
            goto L_0x004c
        L_0x003d:
            java.util.HashMap r4 = r9.a
            java.lang.Object r4 = r4.get(r7)
            java.lang.String r4 = (java.lang.String) r4
            if (r4 == 0) goto L_0x004c
            java.util.HashMap r9 = r9.a
            r9.remove(r7)
        L_0x004c:
            if (r4 == 0) goto L_0x0056
            com.tapjoy.internal.dm.a(r3, r4)
            com.tapjoy.internal.dw r9 = r6.g
            r9.h = r2
            r1 = 1
        L_0x0056:
            if (r1 != 0) goto L_0x007e
            com.tapjoy.internal.dw r9 = r6.g
            java.util.HashMap r1 = r9.c
            int r1 = r1.size()
            if (r1 != 0) goto L_0x0063
            goto L_0x0076
        L_0x0063:
            java.util.HashMap r1 = r9.c
            java.lang.Object r1 = r1.get(r7)
            r5 = r1
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            if (r5 == 0) goto L_0x0076
            java.util.HashMap r9 = r9.c
            r9.remove(r7)
            java.util.Collections.sort(r5)
        L_0x0076:
            if (r5 == 0) goto L_0x007b
            com.tapjoy.internal.dm.a(r3, r5)
        L_0x007b:
            r6.a(r7, r8, r3, r0)
        L_0x007e:
            int r7 = r6.e
            int r7 = r7 + r2
            r6.e = r7
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.dv.a(android.view.View, com.tapjoy.internal.di, org.json.JSONObject):void");
    }

    static /* synthetic */ void b(dv dvVar) {
        String str;
        dvVar.e = 0;
        dvVar.i = System.nanoTime();
        dw dwVar = dvVar.g;
        dd a2 = dd.a();
        if (a2 != null) {
            for (cz czVar : Collections.unmodifiableCollection(a2.b)) {
                View c2 = czVar.c();
                if (czVar.d()) {
                    String str2 = czVar.f;
                    if (c2 != null) {
                        if (!c2.hasWindowFocus()) {
                            str = "noWindowFocus";
                        } else {
                            HashSet hashSet = new HashSet();
                            View view = c2;
                            while (true) {
                                if (view == null) {
                                    dwVar.d.addAll(hashSet);
                                    str = null;
                                    break;
                                }
                                String c3 = dp.c(view);
                                if (c3 != null) {
                                    str = c3;
                                    break;
                                }
                                hashSet.add(view);
                                ViewParent parent = view.getParent();
                                view = parent instanceof View ? (View) parent : null;
                            }
                        }
                        if (str == null) {
                            dwVar.e.add(str2);
                            dwVar.a.put(c2, str2);
                            dwVar.a(czVar);
                        } else {
                            dwVar.f.add(str2);
                            dwVar.b.put(str2, c2);
                            dwVar.g.put(str2, str);
                        }
                    } else {
                        dwVar.f.add(str2);
                        dwVar.g.put(str2, "noAdView");
                    }
                }
            }
        }
        long nanoTime = System.nanoTime();
        dk dkVar = dvVar.f.b;
        if (dvVar.g.f.size() > 0) {
            Iterator it = dvVar.g.f.iterator();
            while (it.hasNext()) {
                String str3 = (String) it.next();
                JSONObject a3 = dkVar.a(null);
                View view2 = (View) dvVar.g.b.get(str3);
                dl dlVar = dvVar.f.a;
                String str4 = (String) dvVar.g.g.get(str3);
                if (str4 != null) {
                    JSONObject a4 = dlVar.a(view2);
                    dm.a(a4, str3);
                    dm.b(a4, str4);
                    dm.a(a3, a4);
                }
                dm.a(a3);
                HashSet hashSet2 = new HashSet();
                hashSet2.add(str3);
                ed edVar = dvVar.h;
                edVar.a.a(new eb(edVar, hashSet2, a3, nanoTime));
            }
        }
        if (dvVar.g.e.size() > 0) {
            JSONObject a5 = dkVar.a(null);
            dvVar.a(null, dkVar, a5, ee.a);
            dm.a(a5);
            ed edVar2 = dvVar.h;
            edVar2.a.a(new ec(edVar2, dvVar.g.e, a5, nanoTime));
        } else {
            dvVar.h.b();
        }
        dw dwVar2 = dvVar.g;
        dwVar2.a.clear();
        dwVar2.b.clear();
        dwVar2.c.clear();
        dwVar2.d.clear();
        dwVar2.e.clear();
        dwVar2.f.clear();
        dwVar2.g.clear();
        dwVar2.h = false;
        dvVar.a(System.nanoTime() - dvVar.i);
    }
}
