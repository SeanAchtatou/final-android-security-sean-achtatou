package com.tapjoy.internal;

import android.graphics.Point;
import android.graphics.Rect;
import com.tapjoy.TJAdUnitConstants;

public final class bj {
    public static final bi a = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            Point point = new Point();
            bnVar.h();
            while (bnVar.j()) {
                String l = bnVar.l();
                if ("x".equals(l)) {
                    point.x = bnVar.r();
                } else if ("y".equals(l)) {
                    point.y = bnVar.r();
                } else {
                    bnVar.s();
                }
            }
            bnVar.i();
            return point;
        }
    };
    public static final bi b = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            Rect rect = new Rect();
            switch (AnonymousClass3.a[bnVar.k().ordinal()]) {
                case 1:
                    bnVar.f();
                    rect.left = bnVar.r();
                    rect.top = bnVar.r();
                    rect.right = bnVar.r();
                    rect.bottom = bnVar.r();
                    while (bnVar.j()) {
                        bnVar.s();
                    }
                    bnVar.g();
                    break;
                case 2:
                    bnVar.h();
                    while (bnVar.j()) {
                        String l = bnVar.l();
                        if (TJAdUnitConstants.String.LEFT.equals(l)) {
                            rect.left = bnVar.r();
                        } else if (TJAdUnitConstants.String.TOP.equals(l)) {
                            rect.top = bnVar.r();
                        } else if (TJAdUnitConstants.String.RIGHT.equals(l)) {
                            rect.right = bnVar.r();
                        } else if (TJAdUnitConstants.String.BOTTOM.equals(l)) {
                            rect.bottom = bnVar.r();
                        } else {
                            bnVar.s();
                        }
                    }
                    bnVar.i();
                    break;
                default:
                    throw new IllegalStateException("Unexpected token: " + bnVar.k());
            }
            return rect;
        }
    };

    /* renamed from: com.tapjoy.internal.bj$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] a = new int[bs.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.tapjoy.internal.bs[] r0 = com.tapjoy.internal.bs.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.tapjoy.internal.bj.AnonymousClass3.a = r0
                int[] r0 = com.tapjoy.internal.bj.AnonymousClass3.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.tapjoy.internal.bs r1 = com.tapjoy.internal.bs.BEGIN_ARRAY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.tapjoy.internal.bj.AnonymousClass3.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.tapjoy.internal.bs r1 = com.tapjoy.internal.bs.BEGIN_OBJECT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.bj.AnonymousClass3.<clinit>():void");
        }
    }
}
