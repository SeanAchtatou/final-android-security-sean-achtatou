package com.tapjoy.internal;

import im.getsocial.sdk.ErrorCode;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public final class ce implements cd {
    private final String a;
    private final URL b;

    public ce(String str, URL url) {
        this.a = str;
        this.b = url;
    }

    public final Object a(ca caVar) {
        URI uri;
        URL url = new URL(this.b, caVar.c());
        String b2 = caVar.b();
        if (HttpRequest.METHOD_GET.equals(b2) || HttpRequest.METHOD_DELETE.equals(b2)) {
            Map e = caVar.e();
            if (!e.isEmpty()) {
                url = new URL(url, url.getPath() + "?" + fk.a(e));
            }
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) fj.a(url);
        httpURLConnection.setRequestMethod(b2);
        httpURLConnection.setRequestProperty("User-Agent", this.a);
        for (Map.Entry entry : caVar.a().entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), entry.getValue().toString());
        }
        if (!HttpRequest.METHOD_GET.equals(b2) && !HttpRequest.METHOD_DELETE.equals(b2)) {
            if (HttpRequest.METHOD_POST.equals(b2) || HttpRequest.METHOD_PUT.equals(b2)) {
                String d = caVar.d();
                if (d == null) {
                    fk.a(httpURLConnection, "application/x-www-form-urlencoded", fk.a(caVar.e()), jm.c);
                } else if ("application/json".equals(d)) {
                    fk.a(httpURLConnection, "application/json; charset=utf-8", bh.a((Object) caVar.e()), jm.c);
                } else {
                    throw new IllegalArgumentException("Unknown content type: " + d);
                }
            } else {
                throw new IllegalArgumentException("Unknown method: " + b2);
            }
        }
        httpURLConnection.connect();
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode != 409) {
            switch (responseCode) {
                case 200:
                case ErrorCode.ACTION_DENIED:
                    break;
                default:
                    throw new IOException("Unexpected status code: " + httpURLConnection.getResponseCode());
            }
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        try {
            uri = httpURLConnection.getURL().toURI();
        } catch (URISyntaxException unused) {
            uri = null;
        }
        try {
            return caVar.a(uri, inputStream);
        } finally {
            inputStream.close();
        }
    }
}
