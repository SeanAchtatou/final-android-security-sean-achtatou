package com.tapjoy.internal;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public final class io extends RelativeLayout {
    private hr a;
    /* access modifiers changed from: private */
    public a b;
    private aa c = aa.UNSPECIFIED;
    private int d = 0;
    private int e = 0;
    private ia f = null;
    private ArrayList g = null;
    private ArrayList h = null;

    public interface a {
        void a();

        void a(hz hzVar);
    }

    public io(Context context, hr hrVar, a aVar) {
        super(context);
        this.a = hrVar;
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.b.a();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r17, int r18) {
        /*
            r16 = this;
            r0 = r16
            int r1 = android.view.View.MeasureSpec.getSize(r17)
            int r2 = android.view.View.MeasureSpec.getSize(r18)
            if (r1 < r2) goto L_0x001a
            com.tapjoy.internal.aa r3 = r0.c
            com.tapjoy.internal.aa r4 = com.tapjoy.internal.aa.LANDSCAPE
            if (r3 == r4) goto L_0x0027
            com.tapjoy.internal.aa r3 = com.tapjoy.internal.aa.LANDSCAPE
            r0.c = r3
            r16.a()
            goto L_0x0027
        L_0x001a:
            com.tapjoy.internal.aa r3 = r0.c
            com.tapjoy.internal.aa r4 = com.tapjoy.internal.aa.PORTRAIT
            if (r3 == r4) goto L_0x0027
            com.tapjoy.internal.aa r3 = com.tapjoy.internal.aa.PORTRAIT
            r0.c = r3
            r16.a()
        L_0x0027:
            int r3 = r0.d
            if (r3 != r1) goto L_0x002f
            int r3 = r0.e
            if (r3 == r2) goto L_0x0116
        L_0x002f:
            r0.d = r1
            r0.e = r2
            float r1 = (float) r1
            float r2 = (float) r2
            com.tapjoy.internal.ia r3 = r0.f
            r4 = 0
            r5 = 1073741824(0x40000000, float:2.0)
            if (r3 == 0) goto L_0x0081
            com.tapjoy.internal.ia r3 = r0.f
            android.graphics.PointF r3 = r3.b
            if (r3 == 0) goto L_0x0081
            com.tapjoy.internal.ia r3 = r0.f
            android.graphics.PointF r3 = r3.b
            float r3 = r3.y
            float r3 = r3 * r1
            com.tapjoy.internal.ia r6 = r0.f
            android.graphics.PointF r6 = r6.b
            float r6 = r6.x
            float r3 = r3 / r6
            float r3 = r3 / r2
            r6 = 1065353216(0x3f800000, float:1.0)
            int r7 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r7 >= 0) goto L_0x006a
            com.tapjoy.internal.ia r3 = r0.f
            android.graphics.PointF r3 = r3.b
            float r3 = r3.y
            float r3 = r3 * r1
            com.tapjoy.internal.ia r6 = r0.f
            android.graphics.PointF r6 = r6.b
            float r6 = r6.x
            float r3 = r3 / r6
            float r2 = r2 - r3
            float r2 = r2 / r5
            goto L_0x0083
        L_0x006a:
            int r3 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x0081
            com.tapjoy.internal.ia r3 = r0.f
            android.graphics.PointF r3 = r3.b
            float r3 = r3.x
            float r3 = r3 * r2
            com.tapjoy.internal.ia r6 = r0.f
            android.graphics.PointF r6 = r6.b
            float r6 = r6.y
            float r3 = r3 / r6
            float r1 = r1 - r3
            float r1 = r1 / r5
            r4 = r1
            r1 = r3
        L_0x0081:
            r3 = r2
            r2 = 0
        L_0x0083:
            java.lang.Iterable r6 = com.tapjoy.internal.ac.a(r16)
            java.util.Iterator r6 = r6.iterator()
        L_0x008b:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x0116
            java.lang.Object r7 = r6.next()
            android.view.View r7 = (android.view.View) r7
            android.view.ViewGroup$LayoutParams r8 = r7.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r8 = (android.widget.RelativeLayout.LayoutParams) r8
            java.lang.Object r7 = r7.getTag()
            com.tapjoy.internal.hz r7 = (com.tapjoy.internal.hz) r7
            com.tapjoy.internal.ib r9 = r7.a
            float r9 = r9.a(r1, r3)
            com.tapjoy.internal.ib r10 = r7.b
            float r10 = r10.a(r1, r3)
            com.tapjoy.internal.ib r11 = r7.c
            float r11 = r11.a(r1, r3)
            com.tapjoy.internal.ib r12 = r7.d
            float r12 = r12.a(r1, r3)
            int r13 = r7.e
            int r7 = r7.f
            r14 = 14
            if (r13 != r14) goto L_0x00c9
            float r13 = r1 - r11
            float r13 = r13 / r5
            float r9 = r9 + r13
            r13 = 9
        L_0x00c9:
            r14 = 15
            r15 = 10
            if (r7 != r14) goto L_0x00d5
            float r7 = r3 - r12
            float r7 = r7 / r5
            float r10 = r10 + r7
            r7 = 10
        L_0x00d5:
            r14 = -1
            r8.addRule(r13, r14)
            r8.addRule(r7, r14)
            int r11 = java.lang.Math.round(r11)
            r8.width = r11
            int r11 = java.lang.Math.round(r12)
            r8.height = r11
            r11 = 9
            if (r13 != r11) goto L_0x00f4
            float r9 = r9 + r4
            int r9 = java.lang.Math.round(r9)
            r8.leftMargin = r9
            goto L_0x00ff
        L_0x00f4:
            r11 = 11
            if (r13 != r11) goto L_0x00ff
            float r9 = r9 + r4
            int r9 = java.lang.Math.round(r9)
            r8.rightMargin = r9
        L_0x00ff:
            if (r7 != r15) goto L_0x0109
            float r10 = r10 + r2
            int r7 = java.lang.Math.round(r10)
            r8.topMargin = r7
            goto L_0x008b
        L_0x0109:
            r9 = 12
            if (r7 != r9) goto L_0x008b
            float r10 = r10 + r2
            int r7 = java.lang.Math.round(r10)
            r8.bottomMargin = r7
            goto L_0x008b
        L_0x0116:
            super.onMeasure(r17, r18)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.io.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public final void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i == 0) {
            if (this.h != null) {
                Iterator it = this.h.iterator();
                while (it.hasNext()) {
                    ig igVar = (ig) ((WeakReference) it.next()).get();
                    if (igVar != null) {
                        igVar.setVisibility(4);
                        igVar.b();
                    }
                }
            }
            if (this.g != null) {
                Iterator it2 = this.g.iterator();
                while (it2.hasNext()) {
                    ig igVar2 = (ig) ((WeakReference) it2.next()).get();
                    if (igVar2 != null) {
                        igVar2.setVisibility(0);
                        igVar2.a();
                    }
                }
                return;
            }
            return;
        }
        if (this.g != null) {
            Iterator it3 = this.g.iterator();
            while (it3.hasNext()) {
                ig igVar3 = (ig) ((WeakReference) it3.next()).get();
                if (igVar3 != null) {
                    igVar3.b();
                }
            }
        }
        if (this.h != null) {
            Iterator it4 = this.h.iterator();
            while (it4.hasNext()) {
                ig igVar4 = (ig) ((WeakReference) it4.next()).get();
                if (igVar4 != null) {
                    igVar4.b();
                }
            }
        }
    }

    private void a() {
        ig igVar;
        ig igVar2;
        Bitmap bitmap;
        Iterator it = this.a.a.iterator();
        ia iaVar = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ia iaVar2 = (ia) it.next();
            if (iaVar2.a == this.c) {
                iaVar = iaVar2;
                break;
            } else if (iaVar2.a == aa.UNSPECIFIED) {
                iaVar = iaVar2;
            }
        }
        removeAllViews();
        if (this.g != null) {
            Iterator it2 = this.g.iterator();
            while (it2.hasNext()) {
                ig igVar3 = (ig) ((WeakReference) it2.next()).get();
                if (igVar3 != null) {
                    igVar3.c();
                }
            }
            this.g.clear();
        }
        if (this.h != null) {
            Iterator it3 = this.h.iterator();
            while (it3.hasNext()) {
                ig igVar4 = (ig) ((WeakReference) it3.next()).get();
                if (igVar4 != null) {
                    igVar4.c();
                }
            }
            this.h.clear();
        }
        if (iaVar != null) {
            this.f = iaVar;
            Context context = getContext();
            Iterator it4 = iaVar.c.iterator();
            while (it4.hasNext()) {
                hz hzVar = (hz) it4.next();
                RelativeLayout relativeLayout = new RelativeLayout(context);
                if (hzVar.l.c != null) {
                    ig igVar5 = new ig(context);
                    igVar5.setScaleType(ImageView.ScaleType.FIT_XY);
                    igVar5.a(hzVar.l.d, hzVar.l.c);
                    if (this.g == null) {
                        this.g = new ArrayList();
                    }
                    this.g.add(new WeakReference(igVar5));
                    igVar = igVar5;
                } else {
                    igVar = null;
                }
                if (hzVar.m == null || hzVar.m.c == null) {
                    igVar2 = null;
                } else {
                    ig igVar6 = new ig(context);
                    igVar6.setScaleType(ImageView.ScaleType.FIT_XY);
                    igVar6.a(hzVar.m.d, hzVar.m.c);
                    if (this.h == null) {
                        this.h = new ArrayList();
                    }
                    this.h.add(new WeakReference(igVar6));
                    igVar2 = igVar6;
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(0, 0);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                Bitmap bitmap2 = hzVar.l.b;
                if (hzVar.m != null) {
                    bitmap = hzVar.m.b;
                } else {
                    bitmap = null;
                }
                final BitmapDrawable bitmapDrawable = bitmap2 != null ? new BitmapDrawable((Resources) null, bitmap2) : null;
                final BitmapDrawable bitmapDrawable2 = bitmap != null ? new BitmapDrawable((Resources) null, bitmap) : null;
                if (bitmapDrawable != null) {
                    ab.a(relativeLayout, bitmapDrawable);
                }
                if (igVar != null) {
                    relativeLayout.addView(igVar, layoutParams2);
                    igVar.a();
                }
                if (igVar2 != null) {
                    relativeLayout.addView(igVar2, layoutParams2);
                    igVar2.setVisibility(4);
                }
                final ig igVar7 = igVar2;
                final ig igVar8 = igVar;
                relativeLayout.setOnTouchListener(new View.OnTouchListener() {
                    public final boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == 0) {
                            if (!(igVar7 == null && bitmapDrawable2 == null)) {
                                if (igVar8 != null) {
                                    igVar8.b();
                                    igVar8.setVisibility(4);
                                }
                                ab.a(view, null);
                            }
                            if (bitmapDrawable2 != null) {
                                ab.a(view, bitmapDrawable2);
                            } else if (igVar7 != null) {
                                igVar7.setVisibility(0);
                                igVar7.a();
                            }
                        } else {
                            boolean z = true;
                            if (motionEvent.getAction() == 1) {
                                float x = motionEvent.getX();
                                float y = motionEvent.getY();
                                if (x >= 0.0f && x < ((float) view.getWidth()) && y >= 0.0f && y < ((float) view.getHeight())) {
                                    z = false;
                                }
                                if (z) {
                                    if (bitmapDrawable != null) {
                                        ab.a(view, bitmapDrawable);
                                    } else if (bitmapDrawable2 != null) {
                                        ab.a(view, null);
                                    }
                                }
                                if (igVar7 != null) {
                                    igVar7.b();
                                    igVar7.setVisibility(4);
                                }
                                if (!((igVar7 == null && bitmapDrawable2 == null) || igVar8 == null || !z)) {
                                    igVar8.setVisibility(0);
                                    igVar8.a();
                                }
                            }
                        }
                        return false;
                    }
                });
                final RelativeLayout relativeLayout2 = relativeLayout;
                final hz hzVar2 = hzVar;
                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        if (igVar7 != null) {
                            igVar7.b();
                            relativeLayout2.removeView(igVar7);
                        }
                        if (igVar8 != null) {
                            igVar8.b();
                            relativeLayout2.removeView(igVar8);
                        }
                        io.this.b.a(hzVar2);
                    }
                });
                relativeLayout.setTag(hzVar);
                addView(relativeLayout, layoutParams);
            }
        }
    }
}
