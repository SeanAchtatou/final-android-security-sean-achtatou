package com.tapjoy.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import com.tapjoy.TapjoyConnectFlag;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.internal.eu;
import com.tapjoy.internal.fa;
import com.tapjoy.internal.fe;
import com.tapjoy.internal.fh;
import im.getsocial.sdk.consts.LanguageCodes;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

public final class hc {
    public static final String a = UUID.randomUUID().toString();
    private static hc d;
    public final fh.a b = new fh.a();
    public final hj c;
    private final fa.a e = new fa.a();
    private final eu.a f = new eu.a();
    private final Context g;

    public static synchronized hc a(Context context) {
        hc hcVar;
        synchronized (hc.class) {
            if (d == null) {
                d = new hc(context, hj.a(context));
            }
            hcVar = d;
        }
        return hcVar;
    }

    private hc(Context context, hj hjVar) {
        hn.a();
        this.e.p = "12.4.0/Android";
        this.e.g = "Android";
        this.e.h = Build.VERSION.RELEASE;
        this.e.e = Build.MANUFACTURER;
        this.e.f = Build.MODEL;
        this.e.l = Locale.getDefault().toString();
        this.e.m = TimeZone.getDefault().getID();
        Context applicationContext = context.getApplicationContext();
        this.g = applicationContext;
        fa.a aVar = this.e;
        SharedPreferences sharedPreferences = applicationContext.getSharedPreferences(TapjoyConstants.TJC_PREFERENCE, 0);
        File file = new File(gz.c(applicationContext), "deviceid");
        String string = sharedPreferences.getString(TapjoyConstants.PREF_ANALYTICS_ID, null);
        if (jq.c(string)) {
            String b2 = file.exists() ? jq.b(bg.a(file)) : null;
            string = b2 == null ? UUID.randomUUID().toString() : b2;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString(TapjoyConstants.PREF_ANALYTICS_ID, string);
            edit.apply();
        }
        aVar.d = string;
        if (!ga.b().a(TapjoyConnectFlag.DISABLE_ANDROID_ID_AS_ANALYTICS_ID, true)) {
            fa.a aVar2 = this.e;
            String string2 = Settings.Secure.getString(applicationContext.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            aVar2.t = !"9774d56d682e549c".equals(string2) ? jq.b(string2) : null;
        }
        PackageManager packageManager = applicationContext.getPackageManager();
        TelephonyManager telephonyManager = (TelephonyManager) applicationContext.getSystemService("phone");
        if (telephonyManager != null) {
            String simCountryIso = telephonyManager.getSimCountryIso();
            if (!jq.c(simCountryIso)) {
                this.e.q = simCountryIso.toUpperCase(Locale.US);
            }
            String networkCountryIso = telephonyManager.getNetworkCountryIso();
            if (!jq.c(networkCountryIso)) {
                this.e.r = networkCountryIso.toUpperCase(Locale.US);
            }
        }
        String packageName = applicationContext.getPackageName();
        this.e.n = packageName;
        fa.a aVar3 = this.e;
        Signature[] e2 = z.e(packageManager, packageName);
        aVar3.o = jq.a((e2 == null || e2.length <= 0) ? null : Base64.encodeToString(ch.a(e2[0].toByteArray()), 2));
        this.f.c = z.a(packageManager, packageName);
        this.f.d = Integer.valueOf(z.b(packageManager, packageName));
        String installerPackageName = packageManager.getInstallerPackageName(packageName);
        if (!jq.c(installerPackageName)) {
            this.f.f = installerPackageName;
        }
        String a2 = a(packageManager, packageName);
        if (!jq.c(a2)) {
            this.f.g = a2;
        }
        a();
        this.c = hjVar;
        String a3 = this.c.c.a();
        if (a3 != null && a3.length() > 0) {
            fa.a aVar4 = this.e;
            aVar4.p = a3 + " 12.4.0/Android";
        }
        String b3 = this.c.b();
        if (b3 != null) {
            this.b.d = b3;
        }
        fh.a aVar5 = this.b;
        hj hjVar2 = this.c;
        long j = hjVar2.b.getLong(LanguageCodes.ITALIAN, 0);
        if (j == 0) {
            Context context2 = hjVar2.a;
            j = z.c(context2.getPackageManager(), context2.getPackageName());
            if (j == 0) {
                j = gz.d(hjVar2.a).lastModified();
                if (j == 0) {
                    Context context3 = hjVar2.a;
                    j = new File(z.d(context3.getPackageManager(), context3.getPackageName())).lastModified();
                    if (j == 0) {
                        j = System.currentTimeMillis();
                    }
                }
            }
            hjVar2.b.edit().putLong(LanguageCodes.ITALIAN, j).apply();
        }
        aVar5.c = Long.valueOf(j);
        int b4 = this.c.f.b();
        this.b.e = Integer.valueOf(a(7, b4));
        this.b.f = Integer.valueOf(a(30, b4));
        int b5 = this.c.h.b();
        if (b5 > 0) {
            this.b.h = Integer.valueOf(b5);
        }
        long a4 = this.c.i.a();
        if (a4 > 0) {
            this.b.i = Long.valueOf(a4);
        }
        long a5 = this.c.j.a();
        if (a5 > 0) {
            this.b.j = Long.valueOf(a5);
        }
        long a6 = this.c.k.a();
        if (a6 > 0) {
            this.b.k = Long.valueOf(a6);
        }
        String a7 = this.c.l.a();
        if (a7 != null) {
            this.b.l = a7;
        }
        int b6 = this.c.m.b();
        if (b6 > 0) {
            this.b.m = Integer.valueOf(b6);
        }
        double a8 = this.c.n.a();
        if (a8 != 0.0d) {
            this.b.n = Double.valueOf(a8);
        }
        long a9 = this.c.o.a();
        if (a9 > 0) {
            this.b.o = Long.valueOf(a9);
        }
        double a10 = this.c.p.a();
        if (a10 != 0.0d) {
            this.b.p = Double.valueOf(a10);
        }
        String a11 = this.c.g.a();
        if (a11 != null) {
            try {
                byte[] decode = Base64.decode(a11, 2);
                this.b.g.clear();
                this.b.g.addAll(((ff) ff.c.a(decode)).d);
            } catch (IllegalArgumentException unused) {
                this.c.g.c();
            } catch (IOException unused2) {
                this.c.g.c();
            }
        }
        this.f.e = this.c.q.a();
        this.b.s = this.c.r.a();
        int intValue = this.c.s.a().intValue();
        this.b.t = intValue != -1 ? Integer.valueOf(intValue) : null;
        int intValue2 = this.c.t.a().intValue();
        this.b.u = intValue2 != -1 ? Integer.valueOf(intValue2) : null;
        this.b.v = this.c.u.a();
        this.b.w = this.c.v.a();
        this.b.x = this.c.w.a();
        this.b.y = this.c.x.a();
        this.b.z = this.c.y.a();
        String a12 = this.c.z.a();
        if (a12 != null) {
            try {
                byte[] decode2 = Base64.decode(a12, 2);
                this.b.A.clear();
                this.b.A.addAll(((fg) fg.c.a(decode2)).d);
            } catch (IllegalArgumentException unused3) {
                this.c.z.c();
            } catch (IOException unused4) {
                this.c.z.c();
            }
        }
        String a13 = this.c.A.a();
        boolean booleanValue = this.c.B.a().booleanValue();
        if (a13 != null) {
            this.b.q = a13;
            this.b.r = Boolean.valueOf(booleanValue);
        } else {
            this.b.q = null;
            this.b.r = null;
        }
        this.b.B = this.c.C.a();
    }

    private static String a(PackageManager packageManager, String str) {
        Object obj;
        try {
            Bundle bundle = packageManager.getApplicationInfo(str, 128).metaData;
            if (bundle == null || (obj = bundle.get("com.tapjoy.appstore")) == null) {
                return null;
            }
            return obj.toString().trim();
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0057 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r4 = this;
            monitor-enter(r4)
            android.util.DisplayMetrics r0 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x0057 }
            r0.<init>()     // Catch:{ Exception -> 0x0057 }
            android.content.Context r1 = r4.g     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = "window"
            java.lang.Object r1 = r1.getSystemService(r2)     // Catch:{ Exception -> 0x0057 }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ Exception -> 0x0057 }
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ Exception -> 0x0057 }
            r1.getMetrics(r0)     // Catch:{ Exception -> 0x0057 }
            android.app.Activity r1 = com.tapjoy.internal.gr.a()     // Catch:{ Exception -> 0x0057 }
            if (r1 == 0) goto L_0x0036
            android.view.Window r1 = r1.getWindow()     // Catch:{ Exception -> 0x0057 }
            if (r1 == 0) goto L_0x0036
            int r2 = r0.heightPixels     // Catch:{ Exception -> 0x0057 }
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ Exception -> 0x0057 }
            r3.<init>()     // Catch:{ Exception -> 0x0057 }
            android.view.View r1 = r1.getDecorView()     // Catch:{ Exception -> 0x0057 }
            r1.getWindowVisibleDisplayFrame(r3)     // Catch:{ Exception -> 0x0057 }
            int r1 = r3.top     // Catch:{ Exception -> 0x0057 }
            int r2 = r2 - r1
            r0.heightPixels = r2     // Catch:{ Exception -> 0x0057 }
        L_0x0036:
            com.tapjoy.internal.fa$a r1 = r4.e     // Catch:{ Exception -> 0x0057 }
            int r2 = r0.densityDpi     // Catch:{ Exception -> 0x0057 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0057 }
            r1.i = r2     // Catch:{ Exception -> 0x0057 }
            com.tapjoy.internal.fa$a r1 = r4.e     // Catch:{ Exception -> 0x0057 }
            int r2 = r0.widthPixels     // Catch:{ Exception -> 0x0057 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0057 }
            r1.j = r2     // Catch:{ Exception -> 0x0057 }
            com.tapjoy.internal.fa$a r1 = r4.e     // Catch:{ Exception -> 0x0057 }
            int r0 = r0.heightPixels     // Catch:{ Exception -> 0x0057 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0057 }
            r1.k = r0     // Catch:{ Exception -> 0x0057 }
            goto L_0x0057
        L_0x0055:
            r0 = move-exception
            goto L_0x0059
        L_0x0057:
            monitor-exit(r4)     // Catch:{ all -> 0x0055 }
            return
        L_0x0059:
            monitor-exit(r4)     // Catch:{ all -> 0x0055 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.hc.a():void");
    }

    public final fb b() {
        fb fbVar;
        synchronized (this) {
            this.e.l = Locale.getDefault().toString();
            this.e.m = TimeZone.getDefault().getID();
            boolean z = false;
            long currentTimeMillis = System.currentTimeMillis() - 259200000;
            Iterator it = this.b.g.iterator();
            while (it.hasNext()) {
                if (((fe) it.next()).g.longValue() <= currentTimeMillis) {
                    it.remove();
                    z = true;
                }
            }
            if (z) {
                g();
            }
            fbVar = new fb(this.e.b(), this.f.b(), this.b.b());
        }
        return fbVar;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        String a2;
        synchronized (this) {
            a2 = this.c.d.a();
        }
        return a2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00dc  */
    @javax.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.tapjoy.internal.fc d() {
        /*
            r20 = this;
            r1 = r20
            monitor-enter(r20)
            java.util.Calendar r0 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x0141 }
            r2 = 1
            int r3 = r0.get(r2)     // Catch:{ all -> 0x0141 }
            int r3 = r3 * 10000
            r4 = 2
            int r5 = r0.get(r4)     // Catch:{ all -> 0x0141 }
            int r5 = r5 * 100
            int r3 = r3 + r5
            int r3 = r3 + 100
            r5 = 5
            int r6 = r0.get(r5)     // Catch:{ all -> 0x0141 }
            int r3 = r3 + r6
            com.tapjoy.internal.hj r6 = r1.c     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.k r6 = r6.e     // Catch:{ all -> 0x0141 }
            java.lang.Integer r6 = r6.a()     // Catch:{ all -> 0x0141 }
            int r6 = r6.intValue()     // Catch:{ all -> 0x0141 }
            r7 = 0
            if (r6 == r3) goto L_0x013f
            if (r6 != 0) goto L_0x004a
            com.tapjoy.internal.fh$a r0 = r1.b     // Catch:{ all -> 0x0141 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0141 }
            r0.e = r4     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.fh$a r0 = r1.b     // Catch:{ all -> 0x0141 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0141 }
            r0.f = r4     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.fc r0 = new com.tapjoy.internal.fc     // Catch:{ all -> 0x0141 }
            java.lang.String r4 = "fq7_0_1"
            java.lang.String r5 = "fq30_0_1"
            r0.<init>(r4, r5, r7)     // Catch:{ all -> 0x0141 }
            goto L_0x012f
        L_0x004a:
            com.tapjoy.internal.hj r8 = r1.c     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.k r8 = r8.f     // Catch:{ all -> 0x0141 }
            java.lang.Integer r8 = r8.a()     // Catch:{ all -> 0x0141 }
            int r8 = r8.intValue()     // Catch:{ all -> 0x0141 }
            r9 = 7
            int r10 = a(r9, r8)     // Catch:{ all -> 0x0141 }
            r11 = 30
            int r12 = a(r11, r8)     // Catch:{ all -> 0x0141 }
            java.util.Calendar r13 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x0141 }
            int r14 = r6 / 10000
            int r15 = r6 / 100
            int r15 = r15 % 100
            int r15 = r15 - r2
            int r6 = r6 % 100
            r13.set(r14, r15, r6)     // Catch:{ all -> 0x0141 }
            int r6 = r0.get(r2)     // Catch:{ all -> 0x0141 }
            int r14 = r13.get(r2)     // Catch:{ all -> 0x0141 }
            int r6 = r6 - r14
            int r6 = java.lang.Integer.signum(r6)     // Catch:{ all -> 0x0141 }
            r14 = -1
            if (r6 == r14) goto L_0x00a8
            if (r6 == r2) goto L_0x008e
            r4 = 6
            int r0 = r0.get(r4)     // Catch:{ all -> 0x0141 }
            int r4 = r13.get(r4)     // Catch:{ all -> 0x0141 }
            int r0 = r0 - r4
            goto L_0x00d4
        L_0x008e:
            java.lang.Object r14 = r0.clone()     // Catch:{ all -> 0x0141 }
            java.util.Calendar r14 = (java.util.Calendar) r14     // Catch:{ all -> 0x0141 }
            int r15 = r13.get(r2)     // Catch:{ all -> 0x0141 }
            int r4 = r13.get(r4)     // Catch:{ all -> 0x0141 }
            int r13 = r13.get(r5)     // Catch:{ all -> 0x0141 }
            r14.set(r15, r4, r13)     // Catch:{ all -> 0x0141 }
            long r16 = r0.getTimeInMillis()     // Catch:{ all -> 0x0141 }
            goto L_0x00c1
        L_0x00a8:
            java.lang.Object r14 = r13.clone()     // Catch:{ all -> 0x0141 }
            java.util.Calendar r14 = (java.util.Calendar) r14     // Catch:{ all -> 0x0141 }
            int r15 = r0.get(r2)     // Catch:{ all -> 0x0141 }
            int r4 = r0.get(r4)     // Catch:{ all -> 0x0141 }
            int r0 = r0.get(r5)     // Catch:{ all -> 0x0141 }
            r14.set(r15, r4, r0)     // Catch:{ all -> 0x0141 }
            long r16 = r13.getTimeInMillis()     // Catch:{ all -> 0x0141 }
        L_0x00c1:
            r0 = 0
        L_0x00c2:
            long r18 = r14.getTimeInMillis()     // Catch:{ all -> 0x0141 }
            int r4 = (r18 > r16 ? 1 : (r18 == r16 ? 0 : -1))
            if (r4 >= 0) goto L_0x00d0
            r14.add(r5, r2)     // Catch:{ all -> 0x0141 }
            int r0 = r0 + 1
            goto L_0x00c2
        L_0x00d0:
            if (r6 <= 0) goto L_0x00d3
            goto L_0x00d4
        L_0x00d3:
            int r0 = -r0
        L_0x00d4:
            int r4 = java.lang.Math.abs(r0)     // Catch:{ all -> 0x0141 }
            if (r4 < r11) goto L_0x00dc
            r15 = 0
            goto L_0x00e4
        L_0x00dc:
            if (r0 < 0) goto L_0x00e1
            int r15 = r8 << r0
            goto L_0x00e4
        L_0x00e1:
            int r0 = -r0
            int r15 = r8 >> r0
        L_0x00e4:
            r2 = r2 | r15
            int r0 = a(r9, r2)     // Catch:{ all -> 0x0141 }
            int r4 = a(r11, r2)     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.fh$a r5 = r1.b     // Catch:{ all -> 0x0141 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0141 }
            r5.e = r6     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.fh$a r5 = r1.b     // Catch:{ all -> 0x0141 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0141 }
            r5.f = r6     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.fc r5 = new com.tapjoy.internal.fc     // Catch:{ all -> 0x0141 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0141 }
            java.lang.String r8 = "fq7_"
            r6.<init>(r8)     // Catch:{ all -> 0x0141 }
            r6.append(r10)     // Catch:{ all -> 0x0141 }
            java.lang.String r8 = "_"
            r6.append(r8)     // Catch:{ all -> 0x0141 }
            r6.append(r0)     // Catch:{ all -> 0x0141 }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x0141 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0141 }
            java.lang.String r8 = "fq30_"
            r6.<init>(r8)     // Catch:{ all -> 0x0141 }
            r6.append(r12)     // Catch:{ all -> 0x0141 }
            java.lang.String r8 = "_"
            r6.append(r8)     // Catch:{ all -> 0x0141 }
            r6.append(r4)     // Catch:{ all -> 0x0141 }
            java.lang.String r4 = r6.toString()     // Catch:{ all -> 0x0141 }
            r5.<init>(r0, r4, r7)     // Catch:{ all -> 0x0141 }
            r0 = r5
        L_0x012f:
            com.tapjoy.internal.hj r4 = r1.c     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.k r4 = r4.e     // Catch:{ all -> 0x0141 }
            r4.a(r3)     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.hj r3 = r1.c     // Catch:{ all -> 0x0141 }
            com.tapjoy.internal.k r3 = r3.f     // Catch:{ all -> 0x0141 }
            r3.a(r2)     // Catch:{ all -> 0x0141 }
            monitor-exit(r20)     // Catch:{ all -> 0x0141 }
            return r0
        L_0x013f:
            monitor-exit(r20)     // Catch:{ all -> 0x0141 }
            return r7
        L_0x0141:
            r0 = move-exception
            monitor-exit(r20)     // Catch:{ all -> 0x0141 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.hc.d():com.tapjoy.internal.fc");
    }

    private static int a(int i, int i2) {
        return Integer.bitCount(((1 << i) - 1) & i2);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(String str, long j, boolean z) {
        synchronized (this) {
            int size = this.b.g.size();
            int i = 0;
            while (i < size) {
                fe feVar = (fe) this.b.g.get(i);
                if (!feVar.f.equals(str)) {
                    i++;
                } else if (!z) {
                    return false;
                } else {
                    fe.a b2 = feVar.b();
                    b2.d = Long.valueOf(j);
                    this.b.g.set(i, b2.b());
                    return true;
                }
            }
            this.b.g.add(new fe(str, Long.valueOf(j)));
            g();
            return true;
        }
    }

    private void g() {
        this.c.g.a(Base64.encodeToString(ff.c.b(new ff(this.b.g)), 2));
    }

    public final boolean a(String str) {
        boolean z;
        synchronized (this) {
            this.c.q.a(str);
            z = true;
            if (str != null) {
                z = true ^ jo.a(this.f.e, str);
                this.f.e = str;
            } else {
                if (this.f.e == null) {
                    z = false;
                }
                this.f.e = null;
            }
        }
        return z;
    }

    public final boolean b(String str) {
        boolean z;
        synchronized (this) {
            this.c.r.a(str);
            z = !jo.a(this.b.s, str);
            if (z) {
                this.b.s = str;
            }
        }
        return z;
    }

    public final boolean a(Integer num) {
        boolean z;
        synchronized (this) {
            this.c.s.a(num);
            z = !jo.a(this.b.t, num);
            if (z) {
                this.b.t = num;
            }
        }
        return z;
    }

    public final boolean b(Integer num) {
        boolean z;
        synchronized (this) {
            this.c.t.a(num);
            z = !jo.a(this.b.u, num);
            if (z) {
                this.b.u = num;
            }
        }
        return z;
    }

    public final boolean a(int i, String str) {
        boolean z;
        synchronized (this) {
            z = false;
            switch (i) {
                case 1:
                    this.c.u.a(str);
                    z = !jo.a(this.b.v, str);
                    if (z) {
                        this.b.v = str;
                        break;
                    }
                    break;
                case 2:
                    this.c.v.a(str);
                    z = !jo.a(this.b.w, str);
                    if (z) {
                        this.b.w = str;
                        break;
                    }
                    break;
                case 3:
                    this.c.w.a(str);
                    z = !jo.a(this.b.x, str);
                    if (z) {
                        this.b.x = str;
                        break;
                    }
                    break;
                case 4:
                    this.c.x.a(str);
                    z = !jo.a(this.b.y, str);
                    if (z) {
                        this.b.y = str;
                        break;
                    }
                    break;
                case 5:
                    this.c.y.a(str);
                    z = !jo.a(this.b.z, str);
                    if (z) {
                        this.b.z = str;
                        break;
                    }
                    break;
            }
        }
        return z;
    }

    public final Set e() {
        HashSet hashSet;
        synchronized (this) {
            hashSet = new HashSet(this.b.A);
        }
        return hashSet;
    }

    public final boolean c(String str) {
        synchronized (this) {
            for (int size = this.b.g.size() - 1; size >= 0; size--) {
                fe feVar = (fe) this.b.g.get(size);
                if (feVar.f.equals(str)) {
                    fe.a b2 = feVar.b();
                    b2.e = Long.valueOf(System.currentTimeMillis());
                    this.b.g.set(size, b2.b());
                    g();
                    return true;
                }
            }
            return false;
        }
    }

    public final boolean f() {
        return ((Boolean) jo.b(this.b.B, fh.r)).booleanValue();
    }

    public final boolean a(boolean z) {
        boolean z2;
        synchronized (this) {
            this.c.C.a(z);
            z2 = z != ((Boolean) jo.b(this.b.B, fh.r)).booleanValue();
            this.b.B = Boolean.valueOf(z);
        }
        return z2;
    }
}
