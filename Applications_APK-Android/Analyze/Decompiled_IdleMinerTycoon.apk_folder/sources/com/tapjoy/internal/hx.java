package com.tapjoy.internal;

import android.graphics.Bitmap;
import com.tapjoy.internal.ap;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

public final class hx {
    public static final bi e = new bi() {
        public final /* synthetic */ Object a(bn bnVar) {
            return new hx(bnVar);
        }
    };
    private static final an f;
    public URL a;
    public Bitmap b;
    public byte[] c;
    public ie d;

    static {
        an arVar = new ar();
        if (!(arVar instanceof as)) {
            arVar = new ap.a((aq) arVar);
        }
        f = arVar;
    }

    public hx(URL url) {
        this.a = url;
    }

    public final boolean a() {
        return (this.b == null && this.c == null) ? false : true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0099, code lost:
        r12 = java.lang.Long.parseLong(r8.substring(8));
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r14 = this;
            com.tapjoy.internal.fy r0 = com.tapjoy.internal.ga.b()
            java.lang.String r1 = "mm_external_cache_enabled"
            r2 = 1
            boolean r0 = r0.a(r1, r2)
            r1 = r0 ^ 1
            if (r1 == 0) goto L_0x0020
            com.tapjoy.internal.an r2 = com.tapjoy.internal.hx.f
            java.net.URL r3 = r14.a
            java.lang.Object r2 = r2.a(r3)
            android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
            r14.b = r2
            android.graphics.Bitmap r2 = r14.b
            if (r2 == 0) goto L_0x0020
            return
        L_0x0020:
            if (r0 == 0) goto L_0x0063
            com.tapjoy.internal.ht r2 = com.tapjoy.internal.ht.a
            java.net.URL r3 = r14.a
            java.io.File r2 = r2.a(r3)
            if (r2 == 0) goto L_0x0063
            r3 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0043, all -> 0x003e }
            r4.<init>(r2)     // Catch:{ IOException -> 0x0043, all -> 0x003e }
            r14.a(r4)     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            com.tapjoy.internal.jz.a(r4)
            goto L_0x0046
        L_0x0039:
            r0 = move-exception
            r3 = r4
            goto L_0x003f
        L_0x003c:
            r3 = r4
            goto L_0x0043
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            com.tapjoy.internal.jz.a(r3)
            throw r0
        L_0x0043:
            com.tapjoy.internal.jz.a(r3)
        L_0x0046:
            android.graphics.Bitmap r3 = r14.b
            if (r3 != 0) goto L_0x0053
            byte[] r3 = r14.c
            if (r3 == 0) goto L_0x004f
            goto L_0x0053
        L_0x004f:
            r2.delete()
            goto L_0x0063
        L_0x0053:
            if (r1 == 0) goto L_0x0062
            android.graphics.Bitmap r0 = r14.b
            if (r0 == 0) goto L_0x0062
            com.tapjoy.internal.an r0 = com.tapjoy.internal.hx.f
            java.net.URL r1 = r14.a
            android.graphics.Bitmap r2 = r14.b
            r0.a(r1, r2)
        L_0x0062:
            return
        L_0x0063:
            java.net.URL r2 = r14.a
            java.net.URLConnection r2 = com.tapjoy.internal.fj.a(r2)
            r3 = 0
            java.lang.String r5 = "Cache-Control"
            java.lang.String r5 = r2.getHeaderField(r5)
            boolean r6 = com.tapjoy.internal.jq.c(r5)
            if (r6 != 0) goto L_0x009e
            java.lang.String r6 = ","
            java.lang.String[] r5 = r5.split(r6)
            int r6 = r5.length
            r7 = 0
        L_0x007f:
            if (r7 >= r6) goto L_0x009e
            r8 = r5[r7]
            java.lang.String r8 = r8.trim()
            java.lang.String r9 = "max-age="
            boolean r9 = r8.startsWith(r9)
            if (r9 == 0) goto L_0x009b
            r5 = 8
            java.lang.String r5 = r8.substring(r5)
            long r5 = java.lang.Long.parseLong(r5)     // Catch:{ NumberFormatException -> 0x009e }
            r12 = r5
            goto L_0x009f
        L_0x009b:
            int r7 = r7 + 1
            goto L_0x007f
        L_0x009e:
            r12 = r3
        L_0x009f:
            java.io.InputStream r2 = r2.getInputStream()
            java.io.ByteArrayInputStream r11 = r14.a(r2)
            com.tapjoy.internal.jz.a(r2)
            com.tapjoy.internal.ht r2 = com.tapjoy.internal.ht.a
            boolean r2 = com.tapjoy.internal.ht.a(r12)
            if (r2 == 0) goto L_0x00cf
            if (r0 == 0) goto L_0x00cf
            android.graphics.Bitmap r0 = r14.b
            if (r0 != 0) goto L_0x00bc
            byte[] r0 = r14.c
            if (r0 == 0) goto L_0x00cf
        L_0x00bc:
            com.tapjoy.internal.ht r9 = com.tapjoy.internal.ht.a
            java.net.URL r10 = r14.a
            android.content.Context r0 = r9.b
            if (r0 == 0) goto L_0x00cf
            java.util.concurrent.ExecutorService r0 = r9.e
            com.tapjoy.internal.ht$2 r2 = new com.tapjoy.internal.ht$2
            r8 = r2
            r8.<init>(r10, r11, r12)
            r0.submit(r2)
        L_0x00cf:
            if (r1 == 0) goto L_0x00de
            android.graphics.Bitmap r0 = r14.b
            if (r0 == 0) goto L_0x00de
            com.tapjoy.internal.an r0 = com.tapjoy.internal.hx.f
            java.net.URL r1 = r14.a
            android.graphics.Bitmap r2 = r14.b
            r0.a(r1, r2)
        L_0x00de:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.internal.hx.b():void");
    }

    private ByteArrayInputStream a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        jx.a(inputStream, byteArrayOutputStream);
        byteArrayOutputStream.close();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        Cif ifVar = new Cif();
        ifVar.a(byteArray);
        ie a2 = ifVar.a();
        if (a2.b == 0) {
            this.c = byteArray;
            this.d = a2;
        } else {
            s sVar = s.a;
            this.b = s.a(byteArrayInputStream);
            byteArrayInputStream.reset();
        }
        return byteArrayInputStream;
    }

    hx(bn bnVar) {
        if (bnVar.k() == bs.STRING) {
            this.a = bnVar.e();
            return;
        }
        bnVar.h();
        String l = bnVar.l();
        while (bnVar.j()) {
            if ("url".equals(l)) {
                this.a = bnVar.e();
            } else {
                bnVar.s();
            }
        }
        bnVar.i();
    }
}
