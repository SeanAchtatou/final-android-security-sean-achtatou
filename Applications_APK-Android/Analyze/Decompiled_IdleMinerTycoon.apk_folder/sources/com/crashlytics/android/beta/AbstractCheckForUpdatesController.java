package com.crashlytics.android.beta;

import android.annotation.SuppressLint;
import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.ApiKey;
import io.fabric.sdk.android.services.common.CurrentTimeProvider;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.persistence.PreferenceStore;
import io.fabric.sdk.android.services.settings.BetaSettingsData;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractCheckForUpdatesController implements UpdatesController {
    static final long LAST_UPDATE_CHECK_DEFAULT = 0;
    static final String LAST_UPDATE_CHECK_KEY = "last_update_check";
    private static final long MILLIS_PER_SECOND = 1000;
    private Beta beta;
    private BetaSettingsData betaSettings;
    private BuildProperties buildProps;
    private Context context;
    private CurrentTimeProvider currentTimeProvider;
    private final AtomicBoolean externallyReady;
    private HttpRequestFactory httpRequestFactory;
    private IdManager idManager;
    private final AtomicBoolean initialized;
    private long lastCheckTimeMillis;
    private PreferenceStore preferenceStore;

    public AbstractCheckForUpdatesController() {
        this(false);
    }

    public AbstractCheckForUpdatesController(boolean z) {
        this.initialized = new AtomicBoolean();
        this.lastCheckTimeMillis = 0;
        this.externallyReady = new AtomicBoolean(z);
    }

    public void initialize(Context context2, Beta beta2, IdManager idManager2, BetaSettingsData betaSettingsData, BuildProperties buildProperties, PreferenceStore preferenceStore2, CurrentTimeProvider currentTimeProvider2, HttpRequestFactory httpRequestFactory2) {
        this.context = context2;
        this.beta = beta2;
        this.idManager = idManager2;
        this.betaSettings = betaSettingsData;
        this.buildProps = buildProperties;
        this.preferenceStore = preferenceStore2;
        this.currentTimeProvider = currentTimeProvider2;
        this.httpRequestFactory = httpRequestFactory2;
        if (signalInitialized()) {
            checkForUpdates();
        }
    }

    /* access modifiers changed from: protected */
    public boolean signalExternallyReady() {
        this.externallyReady.set(true);
        return this.initialized.get();
    }

    /* access modifiers changed from: package-private */
    public boolean signalInitialized() {
        this.initialized.set(true);
        return this.externallyReady.get();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"CommitPrefEdits"})
    public void checkForUpdates() {
        synchronized (this.preferenceStore) {
            if (this.preferenceStore.get().contains(LAST_UPDATE_CHECK_KEY)) {
                this.preferenceStore.save(this.preferenceStore.edit().remove(LAST_UPDATE_CHECK_KEY));
            }
        }
        long currentTimeMillis = this.currentTimeProvider.getCurrentTimeMillis();
        long j = ((long) this.betaSettings.updateSuspendDurationSeconds) * MILLIS_PER_SECOND;
        Logger logger = Fabric.getLogger();
        logger.d(Beta.TAG, "Check for updates delay: " + j);
        Logger logger2 = Fabric.getLogger();
        logger2.d(Beta.TAG, "Check for updates last check time: " + getLastCheckTimeMillis());
        long lastCheckTimeMillis2 = getLastCheckTimeMillis() + j;
        Logger logger3 = Fabric.getLogger();
        logger3.d(Beta.TAG, "Check for updates current time: " + currentTimeMillis + ", next check time: " + lastCheckTimeMillis2);
        if (currentTimeMillis >= lastCheckTimeMillis2) {
            try {
                performUpdateCheck();
            } finally {
                setLastCheckTimeMillis(currentTimeMillis);
            }
        } else {
            Fabric.getLogger().d(Beta.TAG, "Check for updates next check time was not passed");
        }
    }

    private void performUpdateCheck() {
        Fabric.getLogger().d(Beta.TAG, "Performing update check");
        new CheckForUpdatesRequest(this.beta, this.beta.getOverridenSpiEndpoint(), this.betaSettings.updateUrl, this.httpRequestFactory, new CheckForUpdatesResponseTransform()).invoke(new ApiKey().getValue(this.context), this.idManager.getDeviceIdentifiers().get(IdManager.DeviceIdentifierType.FONT_TOKEN), this.buildProps);
    }

    /* access modifiers changed from: package-private */
    public void setLastCheckTimeMillis(long j) {
        this.lastCheckTimeMillis = j;
    }

    /* access modifiers changed from: package-private */
    public long getLastCheckTimeMillis() {
        return this.lastCheckTimeMillis;
    }
}
