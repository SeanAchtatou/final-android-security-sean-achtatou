package com.appsflyer;

import android.support.annotation.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerConfigHandler {
    @Nullable
    /* renamed from: ॱ  reason: contains not printable characters */
    static JSONObject m97(String str) {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject(str);
            try {
                if (jSONObject.optBoolean("monitor", false)) {
                    r.m185().m191();
                } else {
                    r.m185().m199();
                    r.m185().m196();
                }
            } catch (JSONException unused) {
                r.m185().m199();
                r.m185().m196();
                return jSONObject;
            } catch (Throwable th) {
                th = th;
                AFLogger.afErrorLog(th.getMessage(), th);
                r.m185().m199();
                r.m185().m196();
                return jSONObject;
            }
        } catch (JSONException unused2) {
            jSONObject = null;
            r.m185().m199();
            r.m185().m196();
            return jSONObject;
        } catch (Throwable th2) {
            th = th2;
            jSONObject = null;
            AFLogger.afErrorLog(th.getMessage(), th);
            r.m185().m199();
            r.m185().m196();
            return jSONObject;
        }
        return jSONObject;
    }

    public static String getUrl(String str) {
        return String.format(str, AppsFlyerLib.getInstance().getHost());
    }
}
