package com.appsflyer;

import android.os.AsyncTask;
import android.os.Build;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AFExecutor {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static AFExecutor f4;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ScheduledExecutorService f5;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Executor f6;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Executor f7;

    private AFExecutor() {
    }

    public static AFExecutor getInstance() {
        if (f4 == null) {
            f4 = new AFExecutor();
        }
        return f4;
    }

    public Executor getSerialExecutor() {
        if (this.f7 == null) {
            if (Build.VERSION.SDK_INT < 11) {
                return Executors.newSingleThreadExecutor();
            }
            this.f7 = AsyncTask.SERIAL_EXECUTOR;
        }
        return this.f7;
    }

    public Executor getThreadPoolExecutor() {
        if (this.f6 == null || ((this.f6 instanceof ThreadPoolExecutor) && (((ThreadPoolExecutor) this.f6).isShutdown() || ((ThreadPoolExecutor) this.f6).isTerminated() || ((ThreadPoolExecutor) this.f6).isTerminating()))) {
            if (Build.VERSION.SDK_INT < 11) {
                return Executors.newSingleThreadExecutor();
            }
            this.f6 = Executors.newFixedThreadPool(2);
        }
        return this.f6;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final ScheduledThreadPoolExecutor m2() {
        if (this.f5 == null || this.f5.isShutdown() || this.f5.isTerminated()) {
            this.f5 = Executors.newScheduledThreadPool(2);
        }
        return (ScheduledThreadPoolExecutor) this.f5;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m1() {
        try {
            m0(this.f5);
            if (this.f6 instanceof ThreadPoolExecutor) {
                m0((ThreadPoolExecutor) this.f6);
            }
        } catch (Throwable th) {
            AFLogger.afErrorLog("failed to stop Executors", th);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static void m0(ExecutorService executorService) {
        try {
            AFLogger.afRDLog("shut downing executor ...");
            executorService.shutdown();
            executorService.awaitTermination(10, TimeUnit.SECONDS);
            if (!executorService.isTerminated()) {
                AFLogger.afRDLog("killing non-finished tasks");
            }
            executorService.shutdownNow();
        } catch (InterruptedException unused) {
            AFLogger.afRDLog("InterruptedException!!!");
            if (!executorService.isTerminated()) {
                AFLogger.afRDLog("killing non-finished tasks");
            }
            executorService.shutdownNow();
        } catch (Throwable th) {
            if (!executorService.isTerminated()) {
                AFLogger.afRDLog("killing non-finished tasks");
            }
            executorService.shutdownNow();
            throw th;
        }
    }
}
