package com.appsflyer;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class f implements SensorEventListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private double f120;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final int f121;
    @NonNull

    /* renamed from: ˊ  reason: contains not printable characters */
    private final String f122;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final long[] f123 = new long[2];
    @NonNull

    /* renamed from: ˎ  reason: contains not printable characters */
    private final String f124;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final float[][] f125 = new float[2][];

    /* renamed from: ॱ  reason: contains not printable characters */
    private final int f126;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private long f127;

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    private f(int i, @Nullable String str, @Nullable String str2) {
        this.f126 = i;
        this.f124 = str == null ? "" : str;
        this.f122 = str2 == null ? "" : str2;
        this.f121 = ((((i + 31) * 31) + this.f124.hashCode()) * 31) + this.f122.hashCode();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static f m125(Sensor sensor) {
        return new f(sensor.getType(), sensor.getName(), sensor.getVendor());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static double m121(@NonNull float[] fArr, @NonNull float[] fArr2) {
        int min = Math.min(fArr.length, fArr2.length);
        double d = 0.0d;
        for (int i = 0; i < min; i++) {
            d += StrictMath.pow((double) (fArr[i] - fArr2[i]), 2.0d);
        }
        return Math.sqrt(d);
    }

    @NonNull
    /* renamed from: ॱ  reason: contains not printable characters */
    private static List<Float> m126(@NonNull float[] fArr) {
        ArrayList arrayList = new ArrayList(fArr.length);
        for (float valueOf : fArr) {
            arrayList.add(Float.valueOf(valueOf));
        }
        return arrayList;
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent != null && sensorEvent.values != null) {
            Sensor sensor = sensorEvent.sensor;
            if ((sensor == null || sensor.getName() == null || sensor.getVendor() == null) ? false : true) {
                int type = sensorEvent.sensor.getType();
                String name = sensorEvent.sensor.getName();
                String vendor = sensorEvent.sensor.getVendor();
                long j = sensorEvent.timestamp;
                float[] fArr = sensorEvent.values;
                if (m124(type, name, vendor)) {
                    long currentTimeMillis = System.currentTimeMillis();
                    float[] fArr2 = this.f125[0];
                    if (fArr2 == null) {
                        this.f125[0] = Arrays.copyOf(fArr, fArr.length);
                        this.f123[0] = currentTimeMillis;
                        return;
                    }
                    float[] fArr3 = this.f125[1];
                    if (fArr3 == null) {
                        float[] copyOf = Arrays.copyOf(fArr, fArr.length);
                        this.f125[1] = copyOf;
                        this.f123[1] = currentTimeMillis;
                        this.f120 = m121(fArr2, copyOf);
                    } else if (50000000 <= j - this.f127) {
                        this.f127 = j;
                        if (Arrays.equals(fArr3, fArr)) {
                            this.f123[1] = currentTimeMillis;
                            return;
                        }
                        double r0 = m121(fArr2, fArr);
                        if (r0 > this.f120) {
                            this.f125[1] = Arrays.copyOf(fArr, fArr.length);
                            this.f123[1] = currentTimeMillis;
                            this.f120 = r0;
                        }
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.f.ˊ(java.util.Map<com.appsflyer.f, java.util.Map<java.lang.String, java.lang.Object>>, boolean):void
     arg types: [java.util.Map<com.appsflyer.f, java.util.Map<java.lang.String, java.lang.Object>>, int]
     candidates:
      com.appsflyer.f.ˊ(float[], float[]):double
      com.appsflyer.f.ˊ(java.util.Map<com.appsflyer.f, java.util.Map<java.lang.String, java.lang.Object>>, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m129(@NonNull Map<f, Map<String, Object>> map) {
        m123(map, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.f.ˊ(java.util.Map<com.appsflyer.f, java.util.Map<java.lang.String, java.lang.Object>>, boolean):void
     arg types: [java.util.Map<com.appsflyer.f, java.util.Map<java.lang.String, java.lang.Object>>, int]
     candidates:
      com.appsflyer.f.ˊ(float[], float[]):double
      com.appsflyer.f.ˊ(java.util.Map<com.appsflyer.f, java.util.Map<java.lang.String, java.lang.Object>>, boolean):void */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m128(Map<f, Map<String, Object>> map) {
        m123(map, false);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean m124(int i, @NonNull String str, @NonNull String str2) {
        return this.f126 == i && this.f124.equals(str) && this.f122.equals(str2);
    }

    @NonNull
    /* renamed from: ˊ  reason: contains not printable characters */
    private Map<String, Object> m122() {
        HashMap hashMap = new HashMap(7);
        hashMap.put("sT", Integer.valueOf(this.f126));
        hashMap.put("sN", this.f124);
        hashMap.put("sV", this.f122);
        float[] fArr = this.f125[0];
        if (fArr != null) {
            hashMap.put("sVS", m126(fArr));
        }
        float[] fArr2 = this.f125[1];
        if (fArr2 != null) {
            hashMap.put("sVE", m126(fArr2));
        }
        return hashMap;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private void m127() {
        for (int i = 0; i < 2; i++) {
            this.f125[i] = null;
        }
        for (int i2 = 0; i2 < 2; i2++) {
            this.f123[i2] = 0;
        }
        this.f120 = 0.0d;
        this.f127 = 0;
    }

    public final int hashCode() {
        return this.f121;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return m124(fVar.f126, fVar.f124, fVar.f122);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m123(@NonNull Map<f, Map<String, Object>> map, boolean z) {
        boolean z2 = false;
        if (this.f125[0] != null) {
            z2 = true;
        }
        if (z2) {
            map.put(this, m122());
            if (z) {
                m127();
            }
        } else if (!map.containsKey(this)) {
            map.put(this, m122());
        }
    }
}
