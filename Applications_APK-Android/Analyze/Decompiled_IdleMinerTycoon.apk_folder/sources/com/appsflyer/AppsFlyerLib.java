package com.appsflyer;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.OneLinkHttpTask;
import com.appsflyer.b;
import com.appsflyer.cache.CacheManager;
import com.appsflyer.cache.RequestCacheData;
import com.appsflyer.i;
import com.appsflyer.j;
import com.appsflyer.o;
import com.appsflyer.s;
import com.appsflyer.share.Constants;
import com.google.android.gms.common.GoogleApiAvailability;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TapjoyConstants;
import im.getsocial.sdk.usermanagement.AuthIdentityProviderIds;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.NetworkInterface;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AppsFlyerLib implements a {
    public static final String AF_PRE_INSTALL_PATH = "AF_PRE_INSTALL_PATH";
    public static final String ATTRIBUTION_ID_COLUMN_NAME = "aid";
    public static final String ATTRIBUTION_ID_CONTENT_URI = "content://com.facebook.katana.provider.AttributionIdProvider";
    public static final String IS_STOP_TRACKING_USED = "is_stop_tracking_used";
    public static final String LOG_TAG = "AppsFlyer_4.8.18";
    public static final String PRE_INSTALL_SYSTEM_DEFAULT = "/data/local/tmp/pre_install.appsflyer";
    public static final String PRE_INSTALL_SYSTEM_DEFAULT_ETC = "/etc/pre_install.appsflyer";
    public static final String PRE_INSTALL_SYSTEM_RO_PROP = "ro.appsflyer.preinstall.path";

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final String f16;

    /* renamed from: ʼॱ  reason: contains not printable characters */
    private static AppsFlyerLib f17 = new AppsFlyerLib();

    /* renamed from: ʽ  reason: contains not printable characters */
    private static String f18;

    /* renamed from: ˊ  reason: contains not printable characters */
    static AppsFlyerInAppPurchaseValidatorListener f19 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    static final String f20 = BuildConfig.VERSION_NAME.substring(0, BuildConfig.VERSION_NAME.indexOf("."));

    /* renamed from: ˎ  reason: contains not printable characters */
    static final String f21;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ  reason: contains not printable characters */
    public static AppsFlyerConversionListener f22 = null;
    /* access modifiers changed from: private */

    /* renamed from: ͺ  reason: contains not printable characters */
    public static final List<String> f23 = Arrays.asList(AuthIdentityProviderIds.GOOGLE_PLAY, "playstore", "googleplaystore");

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private static final List<String> f24 = Arrays.asList("is_cache");

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private static String f25;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private static String f26;

    /* renamed from: ᐝॱ  reason: contains not printable characters */
    private static AppsFlyerTrackingRequestListener f27 = null;
    protected Uri latestDeepLink = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f28 = -1;
    /* access modifiers changed from: private */

    /* renamed from: ʻॱ  reason: contains not printable characters */
    public boolean f29 = false;

    /* renamed from: ʽॱ  reason: contains not printable characters */
    private long f30;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f31;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public ScheduledExecutorService f32 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private o.d f33;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f34 = false;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private Map<Long, String> f35;

    /* renamed from: ˊˋ  reason: contains not printable characters */
    private String f36;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private long f37 = -1;

    /* renamed from: ˊᐝ  reason: contains not printable characters */
    private long f38;

    /* renamed from: ˋˊ  reason: contains not printable characters */
    private boolean f39 = false;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private q f40 = new q();

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private long f41 = TimeUnit.SECONDS.toMillis(5);

    /* renamed from: ˋᐝ  reason: contains not printable characters */
    private boolean f42;

    /* renamed from: ˌ  reason: contains not printable characters */
    private boolean f43;

    /* renamed from: ˍ  reason: contains not printable characters */
    private boolean f44 = false;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private boolean f45 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    String f46;

    /* renamed from: ॱ  reason: contains not printable characters */
    String f47;
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ  reason: contains not printable characters */
    public Map<String, String> f48;

    /* renamed from: ॱˎ  reason: contains not printable characters */
    private d f49 = null;
    /* access modifiers changed from: private */

    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public long f50;

    /* renamed from: ॱ  reason: contains not printable characters */
    static /* synthetic */ void m65(Map map) {
        if (f22 != null) {
            try {
                f22.onAppOpenAttribution(map);
            } catch (Throwable th) {
                AFLogger.afErrorLog(th.getLocalizedMessage(), th);
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static /* synthetic */ boolean m38(AppsFlyerLib appsFlyerLib) {
        return appsFlyerLib.f48 != null && appsFlyerLib.f48.size() > 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, int):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* renamed from: ˎ  reason: contains not printable characters */
    static /* synthetic */ void m45(AppsFlyerLib appsFlyerLib, Context context, String str, String str2, String str3, String str4, boolean z, boolean z2, Intent intent) {
        AppsFlyerLib appsFlyerLib2 = appsFlyerLib;
        Context context2 = context;
        if (context2 == null) {
            AFLogger.afDebugLog("sendTrackingWithEvent - got null context. skipping event/launch.");
            return;
        }
        boolean z3 = false;
        SharedPreferences sharedPreferences = context2.getSharedPreferences("appsflyer-data", 0);
        AppsFlyerProperties.getInstance().saveProperties(sharedPreferences);
        if (!appsFlyerLib.isTrackingStopped()) {
            StringBuilder sb = new StringBuilder("sendTrackingWithEvent from activity: ");
            sb.append(context.getClass().getName());
            AFLogger.afInfoLog(sb.toString());
        }
        boolean z4 = str2 == null;
        Map<String, Object> r4 = appsFlyerLib.m72(context, str, str2, str3, str4, z, sharedPreferences, z4, intent);
        String str5 = (String) r4.get("appsflyerKey");
        if (str5 == null || str5.length() == 0) {
            AFLogger.afDebugLog("Not sending data yet, waiting for dev key");
            return;
        }
        if (!appsFlyerLib.isTrackingStopped()) {
            AFLogger.afInfoLog("AppsFlyerLib.sendTrackingWithEvent");
        }
        String url = ServerConfigHandler.getUrl(z4 ? z2 ? f26 : f25 : f18);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(url);
        sb2.append(context.getPackageName());
        String obj = sb2.toString();
        int r7 = m48(sharedPreferences, "appsFlyerCount", false);
        if (!(AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.COLLECT_ANDROID_ID_FORCE_BY_USER, false) || AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.COLLECT_IMEI_FORCE_BY_USER, false)) && r4.get(ServerParameters.ADVERTISING_ID_PARAM) != null) {
            try {
                if (TextUtils.isEmpty(appsFlyerLib2.f47) && r4.remove(TapjoyConstants.TJC_ANDROID_ID) != null) {
                    AFLogger.afInfoLog("validateGaidAndIMEI :: removing: android_id");
                }
                if (TextUtils.isEmpty(appsFlyerLib2.f46) && r4.remove("imei") != null) {
                    AFLogger.afInfoLog("validateGaidAndIMEI :: removing: imei");
                }
            } catch (Exception e2) {
                AFLogger.afErrorLog("failed to remove IMEI or AndroidID key from params; ", e2);
            }
        }
        e eVar = new e(appsFlyerLib, obj, r4, context.getApplicationContext(), z4, r7, (byte) 0);
        if (z4 && m56(context)) {
            if (appsFlyerLib2.f48 != null && appsFlyerLib2.f48.size() > 0) {
                z3 = true;
            }
            if (!z3) {
                AFLogger.afDebugLog("Failed to get new referrer, wait ...");
                m66(AFExecutor.getInstance().m2(), eVar, 500, TimeUnit.MILLISECONDS);
                return;
            }
        }
        eVar.run();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static /* synthetic */ void m54(AppsFlyerLib appsFlyerLib, String str, String str2, String str3, WeakReference weakReference, String str4, boolean z) throws IOException {
        URL url = new URL(str);
        StringBuilder sb = new StringBuilder("url: ");
        sb.append(url.toString());
        AFLogger.afInfoLog(sb.toString());
        j.AnonymousClass2.m152("data: ".concat(String.valueOf(str2)));
        m64((Context) weakReference.get(), LOG_TAG, "EVENT_DATA", str2);
        try {
            appsFlyerLib.m26(url, str2, str3, weakReference, str4, z);
        } catch (IOException e2) {
            AFLogger.afErrorLog("Exception in sendRequestToServer. ", e2);
            if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.USE_HTTP_FALLBACK, false)) {
                appsFlyerLib.m26(new URL(str.replace("https:", "http:")), str2, str3, weakReference, str4, z);
                return;
            }
            StringBuilder sb2 = new StringBuilder("failed to send requeset to server. ");
            sb2.append(e2.getLocalizedMessage());
            AFLogger.afInfoLog(sb2.toString());
            m64((Context) weakReference.get(), LOG_TAG, "ERROR", e2.getLocalizedMessage());
            throw e2;
        }
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(f20);
        sb.append("/androidevent?buildnumber=4.8.18&app_id=");
        f16 = sb.toString();
        StringBuilder sb2 = new StringBuilder("https://attr.%s/api/v");
        sb2.append(f16);
        f26 = sb2.toString();
        StringBuilder sb3 = new StringBuilder("https://t.%s/api/v");
        sb3.append(f16);
        f25 = sb3.toString();
        StringBuilder sb4 = new StringBuilder("https://events.%s/api/v");
        sb4.append(f16);
        f18 = sb4.toString();
        StringBuilder sb5 = new StringBuilder("https://register.%s/api/v");
        sb5.append(f16);
        f21 = sb5.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m73() {
        this.f31 = System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m70() {
        this.f30 = System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m74(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra(AppsFlyerProperties.IS_MONITOR);
        if (stringExtra != null) {
            AFLogger.afInfoLog("Turning on monitoring.");
            AppsFlyerProperties.getInstance().set(AppsFlyerProperties.IS_MONITOR, stringExtra.equals("true"));
            m64(context, (String) null, "START_TRACKING", context.getPackageName());
            return;
        }
        AFLogger.afInfoLog("****** onReceive called *******");
        AppsFlyerProperties.getInstance().setOnReceiveCalled();
        String stringExtra2 = intent.getStringExtra("referrer");
        AFLogger.afInfoLog("Play store referrer: ".concat(String.valueOf(stringExtra2)));
        if (stringExtra2 != null) {
            if ("AppsFlyer_Test".equals(intent.getStringExtra("TestIntegrationMode"))) {
                SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
                edit.clear();
                if (Build.VERSION.SDK_INT >= 9) {
                    edit.apply();
                } else {
                    edit.commit();
                }
                AppsFlyerProperties.getInstance().setFirstLaunchCalled(false);
                AFLogger.afInfoLog("Test mode started..");
                this.f38 = System.currentTimeMillis();
            }
            SharedPreferences.Editor edit2 = context.getSharedPreferences("appsflyer-data", 0).edit();
            edit2.putString("referrer", stringExtra2);
            if (Build.VERSION.SDK_INT >= 9) {
                edit2.apply();
            } else {
                edit2.commit();
            }
            AppsFlyerProperties.getInstance().setReferrer(stringExtra2);
            if (AppsFlyerProperties.getInstance().isFirstLaunchCalled()) {
                AFLogger.afInfoLog("onReceive: isLaunchCalled");
                if (stringExtra2 != null && stringExtra2.length() > 5) {
                    ScheduledThreadPoolExecutor r0 = AFExecutor.getInstance().m2();
                    m66(r0, new c(this, new WeakReference(context.getApplicationContext()), null, null, null, stringExtra2, r0, true, intent, (byte) 0), 5, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static void m46(JSONObject jSONObject) {
        String str;
        ArrayList arrayList = new ArrayList();
        Iterator<String> keys = jSONObject.keys();
        while (true) {
            if (!keys.hasNext()) {
                break;
            }
            try {
                JSONArray jSONArray = new JSONArray((String) jSONObject.get(keys.next()));
                for (int i = 0; i < jSONArray.length(); i++) {
                    arrayList.add(Long.valueOf(jSONArray.getLong(i)));
                }
            } catch (JSONException unused) {
            }
        }
        Collections.sort(arrayList);
        Iterator<String> keys2 = jSONObject.keys();
        loop2:
        while (true) {
            str = null;
            while (keys2.hasNext() && str == null) {
                String next = keys2.next();
                try {
                    JSONArray jSONArray2 = new JSONArray((String) jSONObject.get(next));
                    String str2 = str;
                    int i2 = 0;
                    while (i2 < jSONArray2.length()) {
                        try {
                            if (jSONArray2.getLong(i2) != ((Long) arrayList.get(0)).longValue() && jSONArray2.getLong(i2) != ((Long) arrayList.get(1)).longValue() && jSONArray2.getLong(i2) != ((Long) arrayList.get(arrayList.size() - 1)).longValue()) {
                                i2++;
                                str2 = next;
                            }
                        } catch (JSONException unused2) {
                        }
                    }
                    str = str2;
                } catch (JSONException unused3) {
                }
            }
        }
        if (str != null) {
            jSONObject.remove(str);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static void m35(Context context, String str) {
        JSONObject jSONObject;
        JSONArray jSONArray;
        AFLogger.afDebugLog("received a new (extra) referrer: ".concat(String.valueOf(str)));
        try {
            long currentTimeMillis = System.currentTimeMillis();
            String string = context.getSharedPreferences("appsflyer-data", 0).getString("extraReferrers", null);
            if (string == null) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject = jSONObject2;
                jSONArray = new JSONArray();
            } else {
                jSONObject = new JSONObject(string);
                if (jSONObject.has(str)) {
                    jSONArray = new JSONArray((String) jSONObject.get(str));
                } else {
                    jSONArray = new JSONArray();
                }
            }
            if (((long) jSONArray.length()) < 5) {
                jSONArray.put(currentTimeMillis);
            }
            if (((long) jSONObject.length()) >= 4) {
                m46(jSONObject);
            }
            jSONObject.put(str, jSONArray.toString());
            String jSONObject3 = jSONObject.toString();
            SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
            edit.putString("extraReferrers", jSONObject3);
            if (Build.VERSION.SDK_INT >= 9) {
                edit.apply();
            } else {
                edit.commit();
            }
        } catch (JSONException unused) {
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder("Couldn't save referrer - ");
            sb.append(str);
            sb.append(": ");
            AFLogger.afErrorLog(sb.toString(), th);
        }
    }

    private AppsFlyerLib() {
        AFVersionDeclaration.init();
    }

    public static AppsFlyerLib getInstance() {
        return f17;
    }

    public void stopTracking(boolean z, Context context) {
        this.f45 = z;
        CacheManager.getInstance().clearCache(context);
        if (this.f45) {
            SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
            edit.putBoolean(IS_STOP_TRACKING_USED, true);
            if (Build.VERSION.SDK_INT >= 9) {
                edit.apply();
            } else {
                edit.commit();
            }
        }
    }

    public String getSdkVersion() {
        r.m185().m193("getSdkVersion", new String[0]);
        return "version: 4.8.18 (build 413)";
    }

    public void onPause(Context context) {
        i.b.m142(context);
        j r2 = j.m147(context);
        r2.f158.post(r2.f153);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m23(Application application) {
        AppsFlyerProperties.getInstance().loadProperties(application.getApplicationContext());
        if (Build.VERSION.SDK_INT < 14) {
            AFLogger.afInfoLog("SDK<14 call trackEvent manually");
            AFLogger.afInfoLog("onBecameForeground");
            getInstance().f31 = System.currentTimeMillis();
            getInstance().m76(application, (String) null, (Map<String, Object>) null);
            AFLogger.resetDeltaTime();
        } else if (Build.VERSION.SDK_INT >= 14 && this.f33 == null) {
            o.m163();
            this.f33 = new o.d() {
                /* renamed from: ˊ  reason: contains not printable characters */
                public final void m80(Activity activity) {
                    if (2 > AppsFlyerLib.m29(AppsFlyerLib.m57(activity))) {
                        j r0 = j.m147(activity);
                        r0.f158.post(r0.f153);
                        r0.f158.post(r0.f161);
                    }
                    AFLogger.afInfoLog("onBecameForeground");
                    AppsFlyerLib.getInstance().m73();
                    AppsFlyerLib.getInstance().m76(activity, (String) null, (Map<String, Object>) null);
                    AFLogger.resetDeltaTime();
                }

                /* renamed from: ˊ  reason: contains not printable characters */
                public final void m81(WeakReference<Context> weakReference) {
                    i.b.m142(weakReference.get());
                    j r2 = j.m147(weakReference.get());
                    r2.f158.post(r2.f153);
                }
            };
            o.m166().m169(application, this.f33);
        }
    }

    @Deprecated
    public void setGCMProjectID(String str) {
        r.m185().m193("setGCMProjectID", str);
        AFLogger.afWarnLog("Method 'setGCMProjectNumber' is deprecated. Please follow the documentation.");
        enableUninstallTracking(str);
    }

    @Deprecated
    public void setGCMProjectNumber(String str) {
        r.m185().m193("setGCMProjectNumber", str);
        AFLogger.afWarnLog("Method 'setGCMProjectNumber' is deprecated. Please follow the documentation.");
        enableUninstallTracking(str);
    }

    @Deprecated
    public void setGCMProjectNumber(Context context, String str) {
        r.m185().m193("setGCMProjectNumber", str);
        AFLogger.afWarnLog("Method 'setGCMProjectNumber' is deprecated. Please use 'enableUninstallTracking'.");
        enableUninstallTracking(str);
    }

    public void enableUninstallTracking(String str) {
        r.m185().m193("enableUninstallTracking", str);
        AppsFlyerProperties.getInstance().set("gcmProjectNumber", str);
    }

    public void updateServerUninstallToken(Context context, String str) {
        if (str != null) {
            y.m227(context, new b.e.C0007b(str));
        }
    }

    public void setDebugLog(boolean z) {
        r.m185().m193("setDebugLog", String.valueOf(z));
        AppsFlyerProperties.getInstance().set("shouldLog", z);
        AppsFlyerProperties.getInstance().set("logLevel", (z ? AFLogger.LogLevel.DEBUG : AFLogger.LogLevel.NONE).getLevel());
    }

    public void setImeiData(String str) {
        r.m185().m193("setImeiData", str);
        this.f46 = str;
    }

    public void setAndroidIdData(String str) {
        r.m185().m193("setAndroidIdData", str);
        this.f47 = str;
    }

    public AppsFlyerLib enableLocationCollection(boolean z) {
        this.f34 = z;
        return this;
    }

    @Deprecated
    public void setAppUserId(String str) {
        r.m185().m193("setAppUserId", str);
        setCustomerUserId(str);
    }

    public void setCustomerUserId(String str) {
        r.m185().m193("setCustomerUserId", str);
        AFLogger.afInfoLog("setCustomerUserId = ".concat(String.valueOf(str)));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.APP_USER_ID, str);
    }

    public void waitForCustomerUserId(boolean z) {
        AFLogger.afInfoLog("initAfterCustomerUserID: ".concat(String.valueOf(z)), true);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, z);
    }

    public void setCustomerIdAndTrack(String str, @NonNull Context context) {
        if (context != null) {
            boolean z = false;
            if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false) && AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID) == null) {
                z = true;
            }
            if (z) {
                setCustomerUserId(str);
                StringBuilder sb = new StringBuilder("CustomerUserId set: ");
                sb.append(str);
                sb.append(" - Initializing AppsFlyer Tacking");
                AFLogger.afInfoLog(sb.toString(), true);
                String referrer = AppsFlyerProperties.getInstance().getReferrer(context);
                String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
                if (referrer == null) {
                    referrer = "";
                }
                m37(context, string, null, null, referrer, context instanceof Activity ? ((Activity) context).getIntent() : null);
                if (AppsFlyerProperties.getInstance().getString("afUninstallToken") != null) {
                    m75(context, AppsFlyerProperties.getInstance().getString("afUninstallToken"));
                    return;
                }
                return;
            }
            setCustomerUserId(str);
            AFLogger.afInfoLog("waitForCustomerUserId is false; setting CustomerUserID: ".concat(String.valueOf(str)), true);
        }
    }

    public String getOutOfStore(Context context) {
        String string = AppsFlyerProperties.getInstance().getString("api_store_value");
        if (string != null) {
            return string;
        }
        String r3 = m61(new WeakReference(context), "AF_STORE");
        if (r3 != null) {
            return r3;
        }
        AFLogger.afInfoLog("No out-of-store value set");
        return null;
    }

    public void setOutOfStore(String str) {
        if (str != null) {
            String lowerCase = str.toLowerCase();
            AppsFlyerProperties.getInstance().set("api_store_value", lowerCase);
            AFLogger.afInfoLog("Store API set with value: ".concat(String.valueOf(lowerCase)), true);
            return;
        }
        AFLogger.m11("Cannot set setOutOfStore with null");
    }

    public void setAppInviteOneLink(String str) {
        r.m185().m193("setAppInviteOneLink", str);
        AFLogger.afInfoLog("setAppInviteOneLink = ".concat(String.valueOf(str)));
        if (str == null || !str.equals(AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.ONELINK_ID))) {
            AppsFlyerProperties.getInstance().remove(AppsFlyerProperties.ONELINK_DOMAIN);
            AppsFlyerProperties.getInstance().remove("onelinkVersion");
            AppsFlyerProperties.getInstance().remove(AppsFlyerProperties.ONELINK_SCHEME);
        }
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.ONELINK_ID, str);
    }

    public void setAdditionalData(HashMap<String, Object> hashMap) {
        if (hashMap != null) {
            r.m185().m193("setAdditionalData", hashMap.toString());
            AppsFlyerProperties.getInstance().setCustomData(new JSONObject(hashMap).toString());
        }
    }

    public void sendDeepLinkData(Activity activity) {
        if (activity != null && activity.getIntent() != null) {
            r r3 = r.m185();
            StringBuilder sb = new StringBuilder("activity_intent_");
            sb.append(activity.getIntent().toString());
            r3.m193("sendDeepLinkData", activity.getLocalClassName(), sb.toString());
        } else if (activity != null) {
            r.m185().m193("sendDeepLinkData", activity.getLocalClassName(), "activity_intent_null");
        } else {
            r.m185().m193("sendDeepLinkData", "activity_null");
        }
        try {
            m23(activity.getApplication());
            StringBuilder sb2 = new StringBuilder("getDeepLinkData with activity ");
            sb2.append(activity.getIntent().getDataString());
            AFLogger.afInfoLog(sb2.toString());
        } catch (Exception e2) {
            AFLogger.afInfoLog("getDeepLinkData Exception: ".concat(String.valueOf(e2)));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0168  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendPushNotificationData(android.app.Activity r15) {
        /*
            r14 = this;
            r0 = 1
            r1 = 0
            r2 = 2
            if (r15 == 0) goto L_0x0035
            android.content.Intent r3 = r15.getIntent()
            if (r3 == 0) goto L_0x0035
            com.appsflyer.r r3 = com.appsflyer.r.m185()
            java.lang.String r4 = "sendPushNotificationData"
            java.lang.String[] r5 = new java.lang.String[r2]
            java.lang.String r6 = r15.getLocalClassName()
            r5[r1] = r6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r6 = "activity_intent_"
            r1.<init>(r6)
            android.content.Intent r6 = r15.getIntent()
            java.lang.String r6 = r6.toString()
            r1.append(r6)
            java.lang.String r1 = r1.toString()
            r5[r0] = r1
            r3.m193(r4, r5)
            goto L_0x005c
        L_0x0035:
            if (r15 == 0) goto L_0x004d
            com.appsflyer.r r3 = com.appsflyer.r.m185()
            java.lang.String r4 = "sendPushNotificationData"
            java.lang.String[] r5 = new java.lang.String[r2]
            java.lang.String r6 = r15.getLocalClassName()
            r5[r1] = r6
            java.lang.String r1 = "activity_intent_null"
            r5[r0] = r1
            r3.m193(r4, r5)
            goto L_0x005c
        L_0x004d:
            com.appsflyer.r r3 = com.appsflyer.r.m185()
            java.lang.String r4 = "sendPushNotificationData"
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.String r5 = "activity_null"
            r0[r1] = r5
            r3.m193(r4, r0)
        L_0x005c:
            boolean r0 = r15 instanceof android.app.Activity
            r1 = 0
            if (r0 == 0) goto L_0x0092
            r0 = r15
            android.app.Activity r0 = (android.app.Activity) r0
            android.content.Intent r3 = r0.getIntent()
            if (r3 == 0) goto L_0x0092
            android.os.Bundle r4 = r3.getExtras()
            if (r4 == 0) goto L_0x0092
            java.lang.String r5 = "af"
            java.lang.String r5 = r4.getString(r5)
            if (r5 == 0) goto L_0x0093
            java.lang.String r6 = "Push Notification received af payload = "
            java.lang.String r7 = java.lang.String.valueOf(r5)
            java.lang.String r6 = r6.concat(r7)
            com.appsflyer.AFLogger.afInfoLog(r6)
            java.lang.String r6 = "af"
            r4.remove(r6)
            android.content.Intent r3 = r3.putExtras(r4)
            r0.setIntent(r3)
            goto L_0x0093
        L_0x0092:
            r5 = r1
        L_0x0093:
            r14.f36 = r5
            java.lang.String r0 = r14.f36
            if (r0 == 0) goto L_0x0199
            long r3 = java.lang.System.currentTimeMillis()
            java.util.Map<java.lang.Long, java.lang.String> r0 = r14.f35
            if (r0 != 0) goto L_0x00b0
            java.lang.String r0 = "pushes: initializing pushes history.."
            com.appsflyer.AFLogger.afInfoLog(r0)
            java.util.concurrent.ConcurrentHashMap r0 = new java.util.concurrent.ConcurrentHashMap
            r0.<init>()
            r14.f35 = r0
            r7 = r3
            goto L_0x0156
        L_0x00b0:
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x013b }
            java.lang.String r5 = "pushPayloadMaxAging"
            r6 = 1800000(0x1b7740, double:8.89318E-318)
            long r5 = r0.getLong(r5, r6)     // Catch:{ Throwable -> 0x013b }
            java.util.Map<java.lang.Long, java.lang.String> r0 = r14.f35     // Catch:{ Throwable -> 0x013b }
            java.util.Set r0 = r0.keySet()     // Catch:{ Throwable -> 0x013b }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Throwable -> 0x013b }
            r7 = r3
        L_0x00c8:
            boolean r9 = r0.hasNext()     // Catch:{ Throwable -> 0x0139 }
            if (r9 == 0) goto L_0x0156
            java.lang.Object r9 = r0.next()     // Catch:{ Throwable -> 0x0139 }
            java.lang.Long r9 = (java.lang.Long) r9     // Catch:{ Throwable -> 0x0139 }
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r11 = r14.f36     // Catch:{ Throwable -> 0x0139 }
            r10.<init>(r11)     // Catch:{ Throwable -> 0x0139 }
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0139 }
            java.util.Map<java.lang.Long, java.lang.String> r12 = r14.f35     // Catch:{ Throwable -> 0x0139 }
            java.lang.Object r12 = r12.get(r9)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ Throwable -> 0x0139 }
            r11.<init>(r12)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r12 = "pid"
            java.lang.Object r12 = r10.get(r12)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r13 = "pid"
            java.lang.Object r13 = r11.get(r13)     // Catch:{ Throwable -> 0x0139 }
            boolean r12 = r12.equals(r13)     // Catch:{ Throwable -> 0x0139 }
            if (r12 == 0) goto L_0x011b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r5 = "PushNotificationMeasurement: A previous payload with same PID was already acknowledged! (old: "
            r0.<init>(r5)     // Catch:{ Throwable -> 0x0139 }
            r0.append(r11)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r5 = ", new: "
            r0.append(r5)     // Catch:{ Throwable -> 0x0139 }
            r0.append(r10)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r5 = ")"
            r0.append(r5)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0139 }
            com.appsflyer.AFLogger.afInfoLog(r0)     // Catch:{ Throwable -> 0x0139 }
            r14.f36 = r1     // Catch:{ Throwable -> 0x0139 }
            return
        L_0x011b:
            long r10 = r9.longValue()     // Catch:{ Throwable -> 0x0139 }
            r12 = 0
            long r10 = r3 - r10
            int r12 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r12 <= 0) goto L_0x012b
            java.util.Map<java.lang.Long, java.lang.String> r10 = r14.f35     // Catch:{ Throwable -> 0x0139 }
            r10.remove(r9)     // Catch:{ Throwable -> 0x0139 }
        L_0x012b:
            long r10 = r9.longValue()     // Catch:{ Throwable -> 0x0139 }
            int r12 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r12 > 0) goto L_0x00c8
            long r9 = r9.longValue()     // Catch:{ Throwable -> 0x0139 }
            r7 = r9
            goto L_0x00c8
        L_0x0139:
            r0 = move-exception
            goto L_0x013d
        L_0x013b:
            r0 = move-exception
            r7 = r3
        L_0x013d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r5 = "Error while handling push notification measurement: "
            r1.<init>(r5)
            java.lang.Class r5 = r0.getClass()
            java.lang.String r5 = r5.getSimpleName()
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            com.appsflyer.AFLogger.afErrorLog(r1, r0)
        L_0x0156:
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r1 = "pushPayloadHistorySize"
            int r0 = r0.getInt(r1, r2)
            java.util.Map<java.lang.Long, java.lang.String> r1 = r14.f35
            int r1 = r1.size()
            if (r1 != r0) goto L_0x0187
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "pushes: removing oldest overflowing push (oldest push:"
            r0.<init>(r1)
            r0.append(r7)
            java.lang.String r1 = ")"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.appsflyer.AFLogger.afInfoLog(r0)
            java.util.Map<java.lang.Long, java.lang.String> r0 = r14.f35
            java.lang.Long r1 = java.lang.Long.valueOf(r7)
            r0.remove(r1)
        L_0x0187:
            java.util.Map<java.lang.Long, java.lang.String> r0 = r14.f35
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            java.lang.String r2 = r14.f36
            r0.put(r1, r2)
            android.app.Application r15 = r15.getApplication()
            r14.m23(r15)
        L_0x0199:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.sendPushNotificationData(android.app.Activity):void");
    }

    @Deprecated
    public void setUserEmail(String str) {
        r.m185().m193("setUserEmail", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.USER_EMAIL, str);
    }

    public void setUserEmails(String... strArr) {
        r.m185().m193("setUserEmails", strArr);
        setUserEmails(AppsFlyerProperties.EmailsCryptType.NONE, strArr);
    }

    public void setUserEmails(AppsFlyerProperties.EmailsCryptType emailsCryptType, String... strArr) {
        String str;
        ArrayList arrayList = new ArrayList(strArr.length + 1);
        arrayList.add(emailsCryptType.toString());
        arrayList.addAll(Arrays.asList(strArr));
        r.m185().m193("setUserEmails", (String[]) arrayList.toArray(new String[(strArr.length + 1)]));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.EMAIL_CRYPT_TYPE, emailsCryptType.getValue());
        HashMap hashMap = new HashMap();
        ArrayList arrayList2 = new ArrayList();
        String str2 = null;
        for (String str3 : strArr) {
            switch (AnonymousClass4.f55[emailsCryptType.ordinal()]) {
                case 2:
                    str = "md5_el_arr";
                    arrayList2.add(t.m220(str3));
                    break;
                case 3:
                    str = "sha256_el_arr";
                    arrayList2.add(t.m219(str3));
                    break;
                case 4:
                    str = "plain_el_arr";
                    arrayList2.add(str3);
                    break;
                default:
                    str = "sha1_el_arr";
                    arrayList2.add(t.m221(str3));
                    break;
            }
            str2 = str;
        }
        hashMap.put(str2, arrayList2);
        AppsFlyerProperties.getInstance().setUserEmails(new JSONObject(hashMap).toString());
    }

    /* renamed from: com.appsflyer.AppsFlyerLib$4  reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 {

        /* renamed from: ˎ  reason: contains not printable characters */
        static final /* synthetic */ int[] f55 = new int[AppsFlyerProperties.EmailsCryptType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.appsflyer.AppsFlyerProperties$EmailsCryptType[] r0 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.appsflyer.AppsFlyerLib.AnonymousClass4.f55 = r0
                int[] r0 = com.appsflyer.AppsFlyerLib.AnonymousClass4.f55     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.SHA1     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.appsflyer.AppsFlyerLib.AnonymousClass4.f55     // Catch:{ NoSuchFieldError -> 0x001f }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.MD5     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.appsflyer.AppsFlyerLib.AnonymousClass4.f55     // Catch:{ NoSuchFieldError -> 0x002a }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.SHA256     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.appsflyer.AppsFlyerLib.AnonymousClass4.f55     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.NONE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.AnonymousClass4.<clinit>():void");
        }
    }

    public void setCollectAndroidID(boolean z) {
        r.m185().m193("setCollectAndroidID", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_ANDROID_ID, Boolean.toString(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_ANDROID_ID_FORCE_BY_USER, Boolean.toString(z));
    }

    public void setCollectIMEI(boolean z) {
        r.m185().m193("setCollectIMEI", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_IMEI, Boolean.toString(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_IMEI_FORCE_BY_USER, Boolean.toString(z));
    }

    @Deprecated
    public void setCollectFingerPrint(boolean z) {
        r.m185().m193("setCollectFingerPrint", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_FINGER_PRINT, Boolean.toString(z));
    }

    public AppsFlyerLib init(String str, AppsFlyerConversionListener appsFlyerConversionListener) {
        r r0 = r.m185();
        String[] strArr = new String[2];
        strArr[0] = str;
        strArr[1] = appsFlyerConversionListener == null ? "null" : "conversionDataListener";
        r0.m193("init", strArr);
        AFLogger.m14(String.format("Initializing AppsFlyer SDK: (v%s.%s)", BuildConfig.VERSION_NAME, "413"));
        this.f42 = true;
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.AF_KEY, str);
        j.AnonymousClass2.m153(str);
        f22 = appsFlyerConversionListener;
        return this;
    }

    public AppsFlyerLib init(String str, AppsFlyerConversionListener appsFlyerConversionListener, Context context) {
        if (context != null && m56(context)) {
            if (this.f49 == null) {
                this.f49 = new d();
                this.f49.m117(context, this);
            } else {
                AFLogger.afWarnLog("AFInstallReferrer instance already created");
            }
        }
        return init(str, appsFlyerConversionListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, int):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* renamed from: ˏ  reason: contains not printable characters */
    private static boolean m56(@NonNull Context context) {
        if (m48(context.getSharedPreferences("appsflyer-data", 0), "appsFlyerCount", false) > 2) {
            AFLogger.afRDLog("Install referrer will not load, the counter > 2, ");
            return false;
        }
        try {
            Class.forName("com.android.installreferrer.api.InstallReferrerClient");
            if (j.AnonymousClass1.m151(context, "com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE")) {
                AFLogger.afDebugLog("Install referrer is allowed");
                return true;
            }
            AFLogger.afDebugLog("Install referrer is not allowed");
            return false;
        } catch (ClassNotFoundException unused) {
            AFLogger.afRDLog("Class com.android.installreferrer.api.InstallReferrerClient not found");
            return false;
        } catch (Throwable th) {
            AFLogger.afErrorLog("An error occurred while trying to verify manifest : com.android.installreferrer.api.InstallReferrerClient", th);
            return false;
        }
    }

    public void startTracking(Application application) {
        if (!this.f42) {
            AFLogger.afWarnLog("ERROR: AppsFlyer SDK is not initialized! The API call 'startTracking(Application)' must be called after the 'init(String, AppsFlyerConversionListener)' API method, which should be called on the Application's onCreate.");
        } else {
            startTracking(application, null);
        }
    }

    public void startTracking(Application application, String str) {
        startTracking(application, str, null);
    }

    public void startTracking(Application application, String str, AppsFlyerTrackingRequestListener appsFlyerTrackingRequestListener) {
        r.m185().m193("startTracking", str);
        AFLogger.afInfoLog(String.format("Starting AppsFlyer Tracking: (v%s.%s)", BuildConfig.VERSION_NAME, "413"));
        AFLogger.afInfoLog("Build Number: 413");
        AppsFlyerProperties.getInstance().loadProperties(application.getApplicationContext());
        if (!TextUtils.isEmpty(str)) {
            AppsFlyerProperties.getInstance().set(AppsFlyerProperties.AF_KEY, str);
            j.AnonymousClass2.m153(str);
        } else if (TextUtils.isEmpty(AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY))) {
            AFLogger.afWarnLog("ERROR: AppsFlyer SDK is not initialized! You must provide AppsFlyer Dev-Key either in the 'init' API method (should be called on Application's onCreate),or in the startTracking API method (should be called on Activity's onCreate).");
            return;
        }
        if (appsFlyerTrackingRequestListener != null) {
            f27 = appsFlyerTrackingRequestListener;
        }
        m23(application);
    }

    public void setAppId(String str) {
        r.m185().m193("setAppId", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.APP_ID, str);
    }

    public void setExtension(String str) {
        r.m185().m193("setExtension", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.EXTENSION, str);
    }

    public void setIsUpdate(boolean z) {
        r.m185().m193("setIsUpdate", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.IS_UPDATE, z);
    }

    public void setCurrencyCode(String str) {
        r.m185().m193("setCurrencyCode", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.CURRENCY_CODE, str);
    }

    public void trackLocation(Context context, double d2, double d3) {
        r.m185().m193("trackLocation", String.valueOf(d2), String.valueOf(d3));
        HashMap hashMap = new HashMap();
        hashMap.put(AFInAppEventParameterName.LONGTITUDE, Double.toString(d3));
        hashMap.put(AFInAppEventParameterName.LATITUDE, Double.toString(d2));
        m76(context, AFInAppEventType.LOCATION_COORDINATES, hashMap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, int):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m71(WeakReference<Context> weakReference) {
        if (weakReference.get() != null) {
            AFLogger.afInfoLog("app went to background");
            SharedPreferences sharedPreferences = weakReference.get().getSharedPreferences("appsflyer-data", 0);
            AppsFlyerProperties.getInstance().saveProperties(sharedPreferences);
            long j = this.f30 - this.f31;
            HashMap hashMap = new HashMap();
            String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
            if (string == null) {
                AFLogger.afWarnLog("[callStats] AppsFlyer's SDK cannot send any event without providing DevKey.");
                return;
            }
            String string2 = AppsFlyerProperties.getInstance().getString("KSAppsFlyerId");
            if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, false)) {
                hashMap.put(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, "true");
            }
            k r7 = n.m162(weakReference.get().getContentResolver());
            if (r7 != null) {
                hashMap.put("amazon_aid", r7.m155());
                hashMap.put("amazon_aid_limit", String.valueOf(r7.m154()));
            }
            String string3 = AppsFlyerProperties.getInstance().getString(ServerParameters.ADVERTISING_ID_PARAM);
            if (string3 != null) {
                hashMap.put(ServerParameters.ADVERTISING_ID_PARAM, string3);
            }
            hashMap.put("app_id", weakReference.get().getPackageName());
            hashMap.put("devkey", string);
            hashMap.put("uid", p.m174(weakReference));
            hashMap.put("time_in_app", String.valueOf(j / 1000));
            hashMap.put("statType", "user_closed_app");
            hashMap.put(TapjoyConstants.TJC_PLATFORM, "Android");
            hashMap.put("launch_counter", Integer.toString(m48(sharedPreferences, "appsFlyerCount", false)));
            hashMap.put("gcd_conversion_data_timing", Long.toString(sharedPreferences.getLong("appsflyerGetConversionDataTiming", 0)));
            hashMap.put(AppsFlyerProperties.CHANNEL, m40(weakReference));
            if (string2 == null) {
                string2 = "";
            }
            hashMap.put("originalAppsflyerId", string2);
            if (this.f44) {
                try {
                    m mVar = new m(null, isTrackingStopped());
                    mVar.f181 = hashMap;
                    if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                        AFLogger.afDebugLog("Main thread detected. Running callStats task in a new thread.");
                        mVar.execute(ServerConfigHandler.getUrl("https://stats.%s/stats"));
                        return;
                    }
                    StringBuilder sb = new StringBuilder("Running callStats task (on current thread: ");
                    sb.append(Thread.currentThread().toString());
                    sb.append(" )");
                    AFLogger.afDebugLog(sb.toString());
                    mVar.onPreExecute();
                    mVar.onPostExecute(mVar.doInBackground(ServerConfigHandler.getUrl("https://stats.%s/stats")));
                } catch (Throwable th) {
                    AFLogger.afErrorLog("Could not send callStats request", th);
                }
            } else {
                AFLogger.afDebugLog("Stats call is disabled, ignore ...");
            }
        }
    }

    public void trackAppLaunch(Context context, String str) {
        if (m56(context)) {
            if (this.f49 == null) {
                this.f49 = new d();
                this.f49.m117(context, this);
            } else {
                AFLogger.afWarnLog("AFInstallReferrer instance already created");
            }
        }
        m37(context, str, null, null, "", null);
    }

    /* access modifiers changed from: protected */
    public void setDeepLinkData(Intent intent) {
        if (intent != null) {
            try {
                if ("android.intent.action.VIEW".equals(intent.getAction())) {
                    this.latestDeepLink = intent.getData();
                    StringBuilder sb = new StringBuilder("Unity setDeepLinkData = ");
                    sb.append(this.latestDeepLink);
                    AFLogger.afDebugLog(sb.toString());
                }
            } catch (Throwable th) {
                AFLogger.afErrorLog("Exception while setting deeplink data (unity). ", th);
            }
        }
    }

    public void reportTrackSession(Context context) {
        r.m185().m193("reportTrackSession", new String[0]);
        r.m185().m190();
        m76(context, (String) null, (Map<String, Object>) null);
    }

    public void trackEvent(Context context, String str, Map<String, Object> map) {
        r.m185().m193("trackEvent", str, new JSONObject(map == null ? new HashMap<>() : map).toString());
        m76(context, str, map);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m76(Context context, String str, Map<String, Object> map) {
        Intent intent;
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            intent = activity.getIntent();
            AFDeepLinkManager.getInstance().currentActivityHash = System.identityHashCode(activity);
        } else {
            intent = null;
        }
        Intent intent2 = intent;
        if (AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY) == null) {
            AFLogger.afWarnLog("[TrackEvent/Launch] AppsFlyer's SDK cannot send any event without providing DevKey.");
            return;
        }
        if (map == null) {
            map = new HashMap<>();
        }
        JSONObject jSONObject = new JSONObject(map);
        String referrer = AppsFlyerProperties.getInstance().getReferrer(context);
        String jSONObject2 = jSONObject.toString();
        if (referrer == null) {
            referrer = "";
        }
        m37(context, null, str, jSONObject2, referrer, intent2);
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static void m64(Context context, String str, String str2, String str3) {
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.IS_MONITOR, false)) {
            Intent intent = new Intent("com.appsflyer.MonitorBroadcast");
            intent.setPackage("com.appsflyer.nightvision");
            intent.putExtra("message", str2);
            intent.putExtra("value", str3);
            intent.putExtra("packageName", "true");
            intent.putExtra(Constants.URL_MEDIA_SOURCE, new Integer(Process.myPid()));
            intent.putExtra("eventIdentifier", str);
            intent.putExtra("sdk", BuildConfig.VERSION_NAME);
            context.sendBroadcast(intent);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, int):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m75(Context context, String str) {
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false) && AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID) == null) {
            AFLogger.afInfoLog("CustomerUserId not set, Tracking is disabled", true);
            return;
        }
        HashMap hashMap = new HashMap();
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
        if (string == null) {
            AFLogger.afWarnLog("[registerUninstall] AppsFlyer's SDK cannot send any event without providing DevKey.");
            return;
        }
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            hashMap.put("app_version_code", Integer.toString(packageInfo.versionCode));
            hashMap.put("app_version_name", packageInfo.versionName);
            hashMap.put("app_name", packageManager.getApplicationLabel(packageInfo.applicationInfo).toString());
            long j = packageInfo.firstInstallTime;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmssZ", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            hashMap.put("installDate", simpleDateFormat.format(new Date(j)));
        } catch (Throwable th) {
            AFLogger.afErrorLog("Exception while collecting application version info.", th);
        }
        m25(context, hashMap);
        String string2 = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID);
        if (string2 != null) {
            hashMap.put("appUserId", string2);
        }
        try {
            hashMap.put("model", Build.MODEL);
            hashMap.put("brand", Build.BRAND);
        } catch (Throwable th2) {
            AFLogger.afErrorLog("Exception while collecting device brand and model.", th2);
        }
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, false)) {
            hashMap.put(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, "true");
        }
        k r4 = n.m162(context.getContentResolver());
        if (r4 != null) {
            hashMap.put("amazon_aid", r4.m155());
            hashMap.put("amazon_aid_limit", String.valueOf(r4.m154()));
        }
        String string3 = AppsFlyerProperties.getInstance().getString(ServerParameters.ADVERTISING_ID_PARAM);
        if (string3 != null) {
            hashMap.put(ServerParameters.ADVERTISING_ID_PARAM, string3);
        }
        hashMap.put("devkey", string);
        hashMap.put("uid", p.m174(new WeakReference(context)));
        hashMap.put("af_gcm_token", str);
        hashMap.put("launch_counter", Integer.toString(m48(context.getSharedPreferences("appsflyer-data", 0), "appsFlyerCount", false)));
        hashMap.put("sdk", Integer.toString(Build.VERSION.SDK_INT));
        String r12 = m40(new WeakReference(context));
        if (r12 != null) {
            hashMap.put(AppsFlyerProperties.CHANNEL, r12);
        }
        try {
            m mVar = new m(context, isTrackingStopped());
            mVar.f181 = hashMap;
            StringBuilder sb = new StringBuilder();
            sb.append(ServerConfigHandler.getUrl(f21));
            sb.append(packageName);
            mVar.execute(sb.toString());
        } catch (Throwable th3) {
            AFLogger.afErrorLog(th3.getMessage(), th3);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static void m51(Context context, String str) {
        Intent intent = new Intent("com.appsflyer.testIntgrationBroadcast");
        intent.putExtra("params", str);
        if (Build.VERSION.SDK_INT < 26) {
            context.sendBroadcast(intent);
        } else if (context.getPackageManager().queryBroadcastReceivers(intent, 0).toString().contains("com.appsflyer.referrerSender")) {
            Intent intent2 = new Intent(intent);
            intent2.setComponent(new ComponentName("com.appsflyer.referrerSender", "com.appsflyer.referrerSender.Receiver"));
            context.sendBroadcast(intent2);
        }
    }

    public void setDeviceTrackingDisabled(boolean z) {
        r.m185().m193("setDeviceTrackingDisabled", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, z);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public static Map<String, String> m33(Context context) throws l {
        String string = context.getSharedPreferences("appsflyer-data", 0).getString("attributionId", null);
        if (string != null && string.length() > 0) {
            return m34(string);
        }
        throw new l();
    }

    public void registerConversionListener(Context context, AppsFlyerConversionListener appsFlyerConversionListener) {
        r.m185().m193("registerConversionListener", new String[0]);
        if (appsFlyerConversionListener != null) {
            f22 = appsFlyerConversionListener;
        }
    }

    public void unregisterConversionListener() {
        r.m185().m193("unregisterConversionListener", new String[0]);
        f22 = null;
    }

    public void registerValidatorListener(Context context, AppsFlyerInAppPurchaseValidatorListener appsFlyerInAppPurchaseValidatorListener) {
        r.m185().m193("registerValidatorListener", new String[0]);
        AFLogger.afDebugLog("registerValidatorListener called");
        if (appsFlyerInAppPurchaseValidatorListener == null) {
            AFLogger.afDebugLog("registerValidatorListener null listener");
        } else {
            f19 = appsFlyerInAppPurchaseValidatorListener;
        }
    }

    /* access modifiers changed from: protected */
    public void getConversionData(Context context, final ConversionDataListener conversionDataListener) {
        f22 = new AppsFlyerConversionListener() {
            public final void onAppOpenAttribution(Map<String, String> map) {
            }

            public final void onAttributionFailure(String str) {
            }

            public final void onInstallConversionDataLoaded(Map<String, String> map) {
                conversionDataListener.onConversionDataLoaded(map);
            }

            public final void onInstallConversionFailure(String str) {
                conversionDataListener.onConversionFailure(str);
            }
        };
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static Map<String, String> m42(Context context, String str) {
        int i;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        boolean z = false;
        for (String str2 : str.split(Constants.RequestParameters.AMPERSAND)) {
            int indexOf = str2.indexOf(Constants.RequestParameters.EQUAL);
            String substring = indexOf > 0 ? str2.substring(0, indexOf) : str2;
            if (!linkedHashMap.containsKey(substring)) {
                if (substring.equals("c")) {
                    substring = "campaign";
                } else if (substring.equals(com.appsflyer.share.Constants.URL_MEDIA_SOURCE)) {
                    substring = "media_source";
                } else if (substring.equals("af_prt")) {
                    substring = "agency";
                    z = true;
                }
                linkedHashMap.put(substring, "");
            }
            linkedHashMap.put(substring, (indexOf <= 0 || str2.length() <= (i = indexOf + 1)) ? null : str2.substring(i));
        }
        try {
            if (!linkedHashMap.containsKey("install_time")) {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                long j = packageInfo.firstInstallTime;
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                linkedHashMap.put("install_time", simpleDateFormat.format(new Date(j)));
            }
        } catch (Exception e2) {
            AFLogger.afErrorLog("Could not fetch install time. ", e2);
        }
        if (!linkedHashMap.containsKey("af_status")) {
            linkedHashMap.put("af_status", "Non-organic");
        }
        if (z) {
            linkedHashMap.remove("media_source");
        }
        return linkedHashMap;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public static Map<String, String> m34(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (!f24.contains(next)) {
                    String string = jSONObject.getString(next);
                    if (!TextUtils.isEmpty(string) && !"null".equals(string)) {
                        hashMap.put(next, string);
                    }
                }
            }
            return hashMap;
        } catch (JSONException e2) {
            AFLogger.afErrorLog(e2.getMessage(), e2);
            return null;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m37(Context context, String str, String str2, String str3, String str4, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        boolean z = false;
        boolean z2 = str2 == null;
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false) && AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID) == null) {
            z = true;
        }
        if (z) {
            AFLogger.afInfoLog("CustomerUserId not set, Tracking is disabled", true);
            return;
        }
        if (z2) {
            if (!AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.LAUNCH_PROTECT_ENABLED, true)) {
                AFLogger.afInfoLog("Allowing multiple launches within a 5 second time window.");
            } else if (m55()) {
                return;
            }
            this.f28 = System.currentTimeMillis();
        }
        ScheduledThreadPoolExecutor r1 = AFExecutor.getInstance().m2();
        m66(r1, new c(this, new WeakReference(applicationContext), str, str2, str3, str4, r1, false, intent, (byte) 0), 150, TimeUnit.MILLISECONDS);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean m55() {
        if (this.f28 > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.f28;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z", Locale.US);
            long j = this.f28;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String format = simpleDateFormat.format(new Date(j));
            long j2 = this.f37;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String format2 = simpleDateFormat.format(new Date(j2));
            if (currentTimeMillis < this.f41 && !isTrackingStopped()) {
                AFLogger.afInfoLog(String.format(Locale.US, "Last Launch attempt: %s;\nLast successful Launch event: %s;\nThis launch is blocked: %s ms < %s ms", format, format2, Long.valueOf(currentTimeMillis), Long.valueOf(this.f41)));
                return true;
            } else if (!isTrackingStopped()) {
                AFLogger.afInfoLog(String.format(Locale.US, "Last Launch attempt: %s;\nLast successful Launch event: %s;\nSending launch (+%s ms)", format, format2, Long.valueOf(currentTimeMillis)));
            }
        } else if (!isTrackingStopped()) {
            AFLogger.afInfoLog("Sending first launch for this session!");
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v122, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v68, resolved type: java.lang.String} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x038c A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x03a0 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x03ac A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x03b4 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x03c0 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x03c8 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x03d4 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x03e3 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03e4 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x051e A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x0530 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x0539 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x05d0 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x05db A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x05f4 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:279:0x0640 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0646 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x0651 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x0661 A[Catch:{ Exception -> 0x0667 }] */
    /* JADX WARNING: Removed duplicated region for block: B:326:0x071b A[Catch:{ Throwable -> 0x07b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x077b A[Catch:{ Throwable -> 0x07b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:343:0x07c5 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:346:0x07d5 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:349:0x07e6 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:352:0x0810 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:355:0x081b A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:361:0x0846 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:364:0x0874 A[ADDED_TO_REGION, Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:370:0x0883 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:373:0x08b0 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:379:0x08d8 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:389:0x093c A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:390:0x093e A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:397:0x0965 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:400:0x09ad A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:418:0x0b19 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x0b35 A[Catch:{ Exception -> 0x00ac, Throwable -> 0x0b5d }] */
    /* renamed from: ˎ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.String, java.lang.Object> m72(android.content.Context r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, boolean r24, android.content.SharedPreferences r25, boolean r26, android.content.Intent r27) {
        /*
            r18 = this;
            r1 = r18
            r2 = r19
            r3 = r20
            r4 = r21
            r5 = r22
            r6 = r25
            r7 = r26
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            com.appsflyer.n.m161(r2, r8)
            java.util.Date r9 = new java.util.Date
            r9.<init>()
            long r9 = r9.getTime()
            java.lang.String r11 = "af_timestamp"
            java.lang.String r12 = java.lang.Long.toString(r9)
            r8.put(r11, r12)
            java.lang.String r9 = com.appsflyer.b.m101(r2, r9)
            if (r9 == 0) goto L_0x0033
            java.lang.String r10 = "cksm_v1"
            r8.put(r10, r9)
        L_0x0033:
            boolean r9 = r18.isTrackingStopped()     // Catch:{ Throwable -> 0x0b5d }
            if (r9 != 0) goto L_0x0051
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "******* sendTrackingWithEvent: "
            r9.<init>(r10)     // Catch:{ Throwable -> 0x0b5d }
            if (r7 == 0) goto L_0x0045
            java.lang.String r10 = "Launch"
            goto L_0x0046
        L_0x0045:
            r10 = r4
        L_0x0046:
            r9.append(r10)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afInfoLog(r9)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0056
        L_0x0051:
            java.lang.String r9 = "SDK tracking has been stopped"
            com.appsflyer.AFLogger.afInfoLog(r9)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0056:
            java.lang.String r9 = "AppsFlyer_4.8.18"
            java.lang.String r10 = "EVENT_CREATED_WITH_NAME"
            if (r7 == 0) goto L_0x005f
            java.lang.String r11 = "Launch"
            goto L_0x0060
        L_0x005f:
            r11 = r4
        L_0x0060:
            m64(r2, r9, r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.cache.CacheManager r9 = com.appsflyer.cache.CacheManager.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r9.init(r2)     // Catch:{ Throwable -> 0x0b5d }
            r9 = 0
            android.content.pm.PackageManager r10 = r19.getPackageManager()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r11 = r19.getPackageName()     // Catch:{ Exception -> 0x00ac }
            r12 = 4096(0x1000, float:5.74E-42)
            android.content.pm.PackageInfo r10 = r10.getPackageInfo(r11, r12)     // Catch:{ Exception -> 0x00ac }
            java.lang.String[] r10 = r10.requestedPermissions     // Catch:{ Exception -> 0x00ac }
            java.util.List r10 = java.util.Arrays.asList(r10)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r11 = "android.permission.INTERNET"
            boolean r11 = r10.contains(r11)     // Catch:{ Exception -> 0x00ac }
            if (r11 != 0) goto L_0x0091
            java.lang.String r11 = "Permission android.permission.INTERNET is missing in the AndroidManifest.xml"
            com.appsflyer.AFLogger.afWarnLog(r11)     // Catch:{ Exception -> 0x00ac }
            java.lang.String r11 = "PERMISSION_INTERNET_MISSING"
            m64(r2, r9, r11, r9)     // Catch:{ Exception -> 0x00ac }
        L_0x0091:
            java.lang.String r11 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r11 = r10.contains(r11)     // Catch:{ Exception -> 0x00ac }
            if (r11 != 0) goto L_0x009e
            java.lang.String r11 = "Permission android.permission.ACCESS_NETWORK_STATE is missing in the AndroidManifest.xml"
            com.appsflyer.AFLogger.afWarnLog(r11)     // Catch:{ Exception -> 0x00ac }
        L_0x009e:
            java.lang.String r11 = "android.permission.ACCESS_WIFI_STATE"
            boolean r10 = r10.contains(r11)     // Catch:{ Exception -> 0x00ac }
            if (r10 != 0) goto L_0x00b3
            java.lang.String r10 = "Permission android.permission.ACCESS_WIFI_STATE is missing in the AndroidManifest.xml"
            com.appsflyer.AFLogger.afWarnLog(r10)     // Catch:{ Exception -> 0x00ac }
            goto L_0x00b3
        L_0x00ac:
            r0 = move-exception
            r10 = r0
            java.lang.String r11 = "Exception while validation permissions. "
            com.appsflyer.AFLogger.afErrorLog(r11, r10)     // Catch:{ Throwable -> 0x0b5d }
        L_0x00b3:
            if (r24 == 0) goto L_0x00bc
            java.lang.String r10 = "af_events_api"
            java.lang.String r11 = "1"
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
        L_0x00bc:
            java.lang.String r10 = "brand"
            java.lang.String r11 = android.os.Build.BRAND     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "device"
            java.lang.String r11 = android.os.Build.DEVICE     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "product"
            java.lang.String r11 = android.os.Build.PRODUCT     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "sdk"
            int r11 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = java.lang.Integer.toString(r11)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "model"
            java.lang.String r11 = android.os.Build.MODEL     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "deviceType"
            java.lang.String r11 = android.os.Build.TYPE     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "deviceRm"
            java.lang.String r11 = android.os.Build.DISPLAY     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r10, r11)     // Catch:{ Throwable -> 0x0b5d }
            r12 = 0
            r14 = 1
            r15 = 0
            if (r7 == 0) goto L_0x01f8
            java.lang.String r10 = "appsflyer-data"
            android.content.SharedPreferences r10 = r2.getSharedPreferences(r10, r15)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "appsFlyerCount"
            boolean r10 = r10.contains(r11)     // Catch:{ Throwable -> 0x0b5d }
            r10 = r10 ^ r14
            if (r10 == 0) goto L_0x01ab
            com.appsflyer.AppsFlyerProperties r10 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            boolean r10 = r10.isOtherSdkStringDisabled()     // Catch:{ Throwable -> 0x0b5d }
            if (r10 != 0) goto L_0x011d
            float r10 = m17(r19)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "batteryLevel"
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r11, r10)     // Catch:{ Throwable -> 0x0b5d }
        L_0x011d:
            java.lang.String r11 = "OPPO"
            java.lang.String r10 = android.os.Build.BRAND     // Catch:{ Throwable -> 0x0b5d }
            boolean r10 = r11.equals(r10)     // Catch:{ Throwable -> 0x0b5d }
            if (r10 == 0) goto L_0x012f
            r10 = 23
            java.lang.String r11 = "OPPO device found"
            com.appsflyer.AFLogger.afRDLog(r11)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0131
        L_0x012f:
            r10 = 18
        L_0x0131:
            int r11 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0b5d }
            if (r11 < r10) goto L_0x0193
            java.lang.String r10 = "keyPropDisableAFKeystore"
            com.appsflyer.AppsFlyerProperties r11 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            boolean r10 = r11.getBoolean(r10, r15)     // Catch:{ Throwable -> 0x0b5d }
            if (r10 != 0) goto L_0x0193
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "OS SDK is="
            r10.<init>(r11)     // Catch:{ Throwable -> 0x0b5d }
            int r11 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0b5d }
            r10.append(r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "; use KeyStore"
            r10.append(r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afRDLog(r10)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFKeystoreWrapper r10 = new com.appsflyer.AFKeystoreWrapper     // Catch:{ Throwable -> 0x0b5d }
            r10.<init>(r2)     // Catch:{ Throwable -> 0x0b5d }
            boolean r11 = r10.m9()     // Catch:{ Throwable -> 0x0b5d }
            if (r11 != 0) goto L_0x0171
            java.lang.ref.WeakReference r11 = new java.lang.ref.WeakReference     // Catch:{ Throwable -> 0x0b5d }
            r11.<init>(r2)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = com.appsflyer.p.m174(r11)     // Catch:{ Throwable -> 0x0b5d }
            r10.m7(r11)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0174
        L_0x0171:
            r10.m10()     // Catch:{ Throwable -> 0x0b5d }
        L_0x0174:
            java.lang.String r11 = "KSAppsFlyerId"
            java.lang.String r14 = r10.m6()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r9.set(r11, r14)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = "KSAppsFlyerRICounter"
            int r10 = r10.m8()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AppsFlyerProperties r11 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r11.set(r9, r10)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x01ab
        L_0x0193:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "OS SDK is="
            r9.<init>(r10)     // Catch:{ Throwable -> 0x0b5d }
            int r10 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0b5d }
            r9.append(r10)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "; no KeyStore usage"
            r9.append(r10)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afRDLog(r9)     // Catch:{ Throwable -> 0x0b5d }
        L_0x01ab:
            java.lang.String r9 = "timepassedsincelastlaunch"
            java.lang.String r10 = "appsflyer-data"
            android.content.SharedPreferences r10 = r2.getSharedPreferences(r10, r15)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "AppsFlyerTimePassedSincePrevLaunch"
            long r10 = r10.getLong(r11, r12)     // Catch:{ Throwable -> 0x0b5d }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r14 = "AppsFlyerTimePassedSincePrevLaunch"
            m36(r2, r14, r12)     // Catch:{ Throwable -> 0x0b5d }
            r16 = 0
            int r14 = (r10 > r16 ? 1 : (r10 == r16 ? 0 : -1))
            if (r14 <= 0) goto L_0x01ce
            long r12 = r12 - r10
            r10 = 1000(0x3e8, double:4.94E-321)
            long r10 = r12 / r10
            goto L_0x01d0
        L_0x01ce:
            r10 = -1
        L_0x01d0:
            java.lang.String r10 = java.lang.Long.toString(r10)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r9, r10)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r10 = "oneLinkSlug"
            java.lang.String r9 = r9.getString(r10)     // Catch:{ Throwable -> 0x0b5d }
            if (r9 == 0) goto L_0x026b
            java.lang.String r10 = "onelink_id"
            r8.put(r10, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = "ol_ver"
            com.appsflyer.AppsFlyerProperties r10 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "onelinkVersion"
            java.lang.String r10 = r10.getString(r11)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r9, r10)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x026b
        L_0x01f8:
            java.lang.String r9 = "appsflyer-data"
            android.content.SharedPreferences r9 = r2.getSharedPreferences(r9, r15)     // Catch:{ Throwable -> 0x0b5d }
            android.content.SharedPreferences$Editor r10 = r9.edit()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r11 = "prev_event_name"
            r12 = 0
            java.lang.String r11 = r9.getString(r11, r12)     // Catch:{ Exception -> 0x0264 }
            if (r11 == 0) goto L_0x0243
            org.json.JSONObject r12 = new org.json.JSONObject     // Catch:{ Exception -> 0x0264 }
            r12.<init>()     // Catch:{ Exception -> 0x0264 }
            java.lang.String r13 = "prev_event_timestamp"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0264 }
            r14.<init>()     // Catch:{ Exception -> 0x0264 }
            java.lang.String r15 = "prev_event_timestamp"
            r6 = -1
            long r6 = r9.getLong(r15, r6)     // Catch:{ Exception -> 0x0264 }
            r14.append(r6)     // Catch:{ Exception -> 0x0264 }
            java.lang.String r6 = r14.toString()     // Catch:{ Exception -> 0x0264 }
            r12.put(r13, r6)     // Catch:{ Exception -> 0x0264 }
            java.lang.String r6 = "prev_event_value"
            java.lang.String r7 = "prev_event_value"
            r13 = 0
            java.lang.String r7 = r9.getString(r7, r13)     // Catch:{ Exception -> 0x0264 }
            r12.put(r6, r7)     // Catch:{ Exception -> 0x0264 }
            java.lang.String r6 = "prev_event_name"
            r12.put(r6, r11)     // Catch:{ Exception -> 0x0264 }
            java.lang.String r6 = "prev_event"
            java.lang.String r7 = r12.toString()     // Catch:{ Exception -> 0x0264 }
            r8.put(r6, r7)     // Catch:{ Exception -> 0x0264 }
        L_0x0243:
            java.lang.String r6 = "prev_event_name"
            r10.putString(r6, r4)     // Catch:{ Exception -> 0x0264 }
            java.lang.String r6 = "prev_event_value"
            r10.putString(r6, r5)     // Catch:{ Exception -> 0x0264 }
            java.lang.String r6 = "prev_event_timestamp"
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0264 }
            r10.putLong(r6, r11)     // Catch:{ Exception -> 0x0264 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0264 }
            r7 = 9
            if (r6 < r7) goto L_0x0260
            r10.apply()     // Catch:{ Exception -> 0x0264 }
            goto L_0x026b
        L_0x0260:
            r10.commit()     // Catch:{ Exception -> 0x0264 }
            goto L_0x026b
        L_0x0264:
            r0 = move-exception
            r6 = r0
            java.lang.String r7 = "Error while processing previous event."
            com.appsflyer.AFLogger.afErrorLog(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x026b:
            java.lang.String r6 = "KSAppsFlyerId"
            com.appsflyer.AppsFlyerProperties r7 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r7.getString(r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "KSAppsFlyerRICounter"
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = r9.getString(r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x0297
            if (r7 == 0) goto L_0x0297
            java.lang.Integer r9 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x0b5d }
            int r9 = r9.intValue()     // Catch:{ Throwable -> 0x0b5d }
            if (r9 <= 0) goto L_0x0297
            java.lang.String r9 = "reinstallCounter"
            r8.put(r9, r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "originalAppsflyerId"
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0297:
            java.lang.String r6 = "additionalCustomData"
            com.appsflyer.AppsFlyerProperties r7 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r7.getString(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x02a8
            java.lang.String r7 = "customData"
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x02a8:
            android.content.pm.PackageManager r6 = r19.getPackageManager()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r7 = r19.getPackageName()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r6 = r6.getInstallerPackageName(r7)     // Catch:{ Exception -> 0x02bc }
            if (r6 == 0) goto L_0x02c3
            java.lang.String r7 = "installer_package"
            r8.put(r7, r6)     // Catch:{ Exception -> 0x02bc }
            goto L_0x02c3
        L_0x02bc:
            r0 = move-exception
            r6 = r0
            java.lang.String r7 = "Exception while getting the app's installer package. "
            com.appsflyer.AFLogger.afErrorLog(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x02c3:
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "sdkExtension"
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x02da
            int r7 = r6.length()     // Catch:{ Throwable -> 0x0b5d }
            if (r7 <= 0) goto L_0x02da
            java.lang.String r7 = "sdkExtension"
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x02da:
            java.lang.ref.WeakReference r6 = new java.lang.ref.WeakReference     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r2)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = m40(r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = m67(r2, r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r7 == 0) goto L_0x02ee
            java.lang.String r9 = "channel"
            r8.put(r9, r7)     // Catch:{ Throwable -> 0x0b5d }
        L_0x02ee:
            if (r7 == 0) goto L_0x02f6
            boolean r9 = r7.equals(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r9 == 0) goto L_0x02fa
        L_0x02f6:
            if (r7 != 0) goto L_0x02ff
            if (r6 == 0) goto L_0x02ff
        L_0x02fa:
            java.lang.String r7 = "af_latestchannel"
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x02ff:
            java.lang.String r6 = "appsflyer-data"
            r7 = 0
            android.content.SharedPreferences r6 = r2.getSharedPreferences(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "INSTALL_STORE"
            boolean r7 = r6.contains(r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r7 == 0) goto L_0x0316
            java.lang.String r7 = "INSTALL_STORE"
            r9 = 0
            java.lang.String r6 = r6.getString(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0333
        L_0x0316:
            java.lang.String r6 = "appsflyer-data"
            r7 = 0
            android.content.SharedPreferences r6 = r2.getSharedPreferences(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "appsFlyerCount"
            boolean r6 = r6.contains(r7)     // Catch:{ Throwable -> 0x0b5d }
            r7 = 1
            r6 = r6 ^ r7
            if (r6 == 0) goto L_0x032d
            java.lang.String r9 = m18(r19)     // Catch:{ Throwable -> 0x0b5d }
            r6 = r9
            goto L_0x032e
        L_0x032d:
            r6 = 0
        L_0x032e:
            java.lang.String r7 = "INSTALL_STORE"
            m53(r2, r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0333:
            if (r6 == 0) goto L_0x033e
            java.lang.String r7 = "af_installstore"
            java.lang.String r6 = r6.toLowerCase()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x033e:
            java.lang.String r6 = "appsflyer-data"
            r7 = 0
            android.content.SharedPreferences r6 = r2.getSharedPreferences(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "preInstallName"
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = r9.getString(r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r7 != 0) goto L_0x0401
            java.lang.String r9 = "preInstallName"
            boolean r9 = r6.contains(r9)     // Catch:{ Throwable -> 0x0b5d }
            if (r9 == 0) goto L_0x0363
            java.lang.String r7 = "preInstallName"
            r9 = 0
            java.lang.String r6 = r6.getString(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            r7 = r6
            goto L_0x03f6
        L_0x0363:
            java.lang.String r6 = "appsflyer-data"
            r9 = 0
            android.content.SharedPreferences r6 = r2.getSharedPreferences(r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = "appsFlyerCount"
            boolean r6 = r6.contains(r9)     // Catch:{ Throwable -> 0x0b5d }
            r9 = 1
            r6 = r6 ^ r9
            if (r6 == 0) goto L_0x03ef
            java.lang.String r6 = "ro.appsflyer.preinstall.path"
            java.lang.String r6 = m20(r6)     // Catch:{ Throwable -> 0x0b5d }
            java.io.File r6 = m49(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x0389
            boolean r7 = r6.exists()     // Catch:{ Throwable -> 0x0b5d }
            if (r7 != 0) goto L_0x0387
            goto L_0x0389
        L_0x0387:
            r7 = 0
            goto L_0x038a
        L_0x0389:
            r7 = 1
        L_0x038a:
            if (r7 == 0) goto L_0x039e
            java.lang.String r6 = "AF_PRE_INSTALL_PATH"
            android.content.pm.PackageManager r7 = r19.getPackageManager()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = r19.getPackageName()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = m21(r6, r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.io.File r6 = m49(r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x039e:
            if (r6 == 0) goto L_0x03a9
            boolean r7 = r6.exists()     // Catch:{ Throwable -> 0x0b5d }
            if (r7 != 0) goto L_0x03a7
            goto L_0x03a9
        L_0x03a7:
            r7 = 0
            goto L_0x03aa
        L_0x03a9:
            r7 = 1
        L_0x03aa:
            if (r7 == 0) goto L_0x03b2
            java.lang.String r6 = "/data/local/tmp/pre_install.appsflyer"
            java.io.File r6 = m49(r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x03b2:
            if (r6 == 0) goto L_0x03bd
            boolean r7 = r6.exists()     // Catch:{ Throwable -> 0x0b5d }
            if (r7 != 0) goto L_0x03bb
            goto L_0x03bd
        L_0x03bb:
            r7 = 0
            goto L_0x03be
        L_0x03bd:
            r7 = 1
        L_0x03be:
            if (r7 == 0) goto L_0x03c6
            java.lang.String r6 = "/etc/pre_install.appsflyer"
            java.io.File r6 = m49(r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x03c6:
            if (r6 == 0) goto L_0x03d1
            boolean r7 = r6.exists()     // Catch:{ Throwable -> 0x0b5d }
            if (r7 != 0) goto L_0x03cf
            goto L_0x03d1
        L_0x03cf:
            r7 = 0
            goto L_0x03d2
        L_0x03d1:
            r7 = 1
        L_0x03d2:
            if (r7 != 0) goto L_0x03e0
            java.lang.String r7 = r19.getPackageName()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = m39(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r9 == 0) goto L_0x03e0
            r7 = r9
            goto L_0x03e1
        L_0x03e0:
            r7 = 0
        L_0x03e1:
            if (r7 == 0) goto L_0x03e4
            goto L_0x03ef
        L_0x03e4:
            java.lang.ref.WeakReference r6 = new java.lang.ref.WeakReference     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r2)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "AF_PRE_INSTALL_NAME"
            java.lang.String r7 = m61(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
        L_0x03ef:
            if (r7 == 0) goto L_0x03f6
            java.lang.String r6 = "preInstallName"
            m53(r2, r6, r7)     // Catch:{ Throwable -> 0x0b5d }
        L_0x03f6:
            if (r7 == 0) goto L_0x0401
            java.lang.String r6 = "preInstallName"
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r9.set(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0401:
            if (r7 == 0) goto L_0x040c
            java.lang.String r6 = "af_preinstall_name"
            java.lang.String r7 = r7.toLowerCase()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
        L_0x040c:
            java.lang.String r6 = m18(r19)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x041b
            java.lang.String r7 = "af_currentstore"
            java.lang.String r6 = r6.toLowerCase()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x041b:
            if (r3 == 0) goto L_0x0429
            int r6 = r20.length()     // Catch:{ Throwable -> 0x0b5d }
            if (r6 < 0) goto L_0x0429
            java.lang.String r6 = "appsflyerKey"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0440
        L_0x0429:
            java.lang.String r3 = "AppsFlyerKey"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r6.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0b4a
            int r6 = r3.length()     // Catch:{ Throwable -> 0x0b5d }
            if (r6 < 0) goto L_0x0b4a
            java.lang.String r6 = "appsflyerKey"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0440:
            java.lang.String r3 = "AppUserId"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r6.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0451
            java.lang.String r6 = "appUserId"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0451:
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "userEmails"
            java.lang.String r3 = r3.getString(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0463
            java.lang.String r6 = "user_emails"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0478
        L_0x0463:
            java.lang.String r3 = "userEmail"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r6.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0478
            java.lang.String r6 = "sha1_el"
            java.lang.String r3 = com.appsflyer.t.m221(r3)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0478:
            if (r4 == 0) goto L_0x0486
            java.lang.String r3 = "eventName"
            r8.put(r3, r4)     // Catch:{ Throwable -> 0x0b5d }
            if (r5 == 0) goto L_0x0486
            java.lang.String r3 = "eventValue"
            r8.put(r3, r5)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0486:
            java.lang.String r3 = "appid"
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r5.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x04a1
            java.lang.String r3 = "appid"
            java.lang.String r5 = "appid"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = r6.getString(r5)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r3, r5)     // Catch:{ Throwable -> 0x0b5d }
        L_0x04a1:
            java.lang.String r3 = "currencyCode"
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r5.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            r5 = 3
            if (r3 == 0) goto L_0x04cf
            int r6 = r3.length()     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == r5) goto L_0x04ca
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "WARNING: currency code should be 3 characters!!! '"
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0b5d }
            r6.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "' is not a legal value."
            r6.append(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afWarnLog(r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x04ca:
            java.lang.String r6 = "currency"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x04cf:
            java.lang.String r3 = "IS_UPDATE"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r6.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x04e0
            java.lang.String r6 = "isUpdate"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x04e0:
            boolean r3 = r18.isPreInstalledApp(r19)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "af_preinstalled"
            java.lang.String r3 = java.lang.Boolean.toString(r3)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "collectFacebookAttrId"
            r7 = 1
            boolean r3 = r3.getBoolean(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0523
            android.content.pm.PackageManager r3 = r19.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0516, Throwable -> 0x050d }
            java.lang.String r6 = "com.facebook.katana"
            r7 = 0
            r3.getApplicationInfo(r6, r7)     // Catch:{ NameNotFoundException -> 0x0516, Throwable -> 0x050d }
            android.content.ContentResolver r3 = r19.getContentResolver()     // Catch:{ NameNotFoundException -> 0x0516, Throwable -> 0x050d }
            java.lang.String r9 = r1.getAttributionId(r3)     // Catch:{ NameNotFoundException -> 0x0516, Throwable -> 0x050d }
            goto L_0x051c
        L_0x050d:
            r0 = move-exception
            r3 = r0
            java.lang.String r6 = "Exception while collecting facebook's attribution ID. "
            com.appsflyer.AFLogger.afErrorLog(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0514:
            r9 = 0
            goto L_0x051c
        L_0x0516:
            java.lang.String r3 = "Exception while collecting facebook's attribution ID. "
            com.appsflyer.AFLogger.afWarnLog(r3)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0514
        L_0x051c:
            if (r9 == 0) goto L_0x0523
            java.lang.String r3 = "fb"
            r8.put(r3, r9)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0523:
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "deviceTrackingDisabled"
            r7 = 0
            boolean r3 = r3.getBoolean(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0539
            java.lang.String r3 = "deviceTrackingDisabled"
            java.lang.String r6 = "true"
            r8.put(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0656
        L_0x0539:
            java.lang.String r3 = "appsflyer-data"
            r6 = 0
            android.content.SharedPreferences r3 = r2.getSharedPreferences(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "collectIMEI"
            r9 = 1
            boolean r6 = r6.getBoolean(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "imeiCached"
            r9 = 0
            java.lang.String r7 = r3.getString(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x05c6
            java.lang.String r6 = r1.f46     // Catch:{ Throwable -> 0x0b5d }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x05c6
            boolean r6 = m68(r19)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x05cd
            java.lang.String r6 = "phone"
            java.lang.Object r6 = r2.getSystemService(r6)     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            android.telephony.TelephonyManager r6 = (android.telephony.TelephonyManager) r6     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            java.lang.Class r9 = r6.getClass()     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            java.lang.String r10 = "getDeviceId"
            r11 = 0
            java.lang.Class[] r12 = new java.lang.Class[r11]     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            java.lang.reflect.Method r9 = r9.getMethod(r10, r12)     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            java.lang.Object[] r10 = new java.lang.Object[r11]     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            java.lang.Object r6 = r9.invoke(r6, r10)     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            r9 = r6
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            if (r9 == 0) goto L_0x0583
            goto L_0x05ce
        L_0x0583:
            if (r7 == 0) goto L_0x05cd
            java.lang.String r6 = "use cached IMEI: "
            java.lang.String r9 = java.lang.String.valueOf(r7)     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            java.lang.String r6 = r6.concat(r9)     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            com.appsflyer.AFLogger.afDebugLog(r6)     // Catch:{ InvocationTargetException -> 0x05ae, Exception -> 0x0594 }
            r9 = r7
            goto L_0x05ce
        L_0x0594:
            r0 = move-exception
            r6 = r0
            if (r7 == 0) goto L_0x05a7
            java.lang.String r9 = "use cached IMEI: "
            java.lang.String r10 = java.lang.String.valueOf(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = r9.concat(r10)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afDebugLog(r9)     // Catch:{ Throwable -> 0x0b5d }
            r9 = r7
            goto L_0x05a8
        L_0x05a7:
            r9 = 0
        L_0x05a8:
            java.lang.String r7 = "WARNING: other reason: "
            com.appsflyer.AFLogger.afErrorLog(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x05ce
        L_0x05ae:
            if (r7 == 0) goto L_0x05bf
            java.lang.String r6 = "use cached IMEI: "
            java.lang.String r9 = java.lang.String.valueOf(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.concat(r9)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afDebugLog(r6)     // Catch:{ Throwable -> 0x0b5d }
            r9 = r7
            goto L_0x05c0
        L_0x05bf:
            r9 = 0
        L_0x05c0:
            java.lang.String r6 = "WARNING: READ_PHONE_STATE is missing."
            com.appsflyer.AFLogger.afWarnLog(r6)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x05ce
        L_0x05c6:
            java.lang.String r6 = r1.f46     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x05cd
            java.lang.String r9 = r1.f46     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x05ce
        L_0x05cd:
            r9 = 0
        L_0x05ce:
            if (r9 == 0) goto L_0x05db
            java.lang.String r6 = "imeiCached"
            m53(r2, r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "imei"
            r8.put(r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x05e0
        L_0x05db:
            java.lang.String r6 = "IMEI was not collected."
            com.appsflyer.AFLogger.afInfoLog(r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x05e0:
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "collectAndroidId"
            r9 = 1
            boolean r6 = r6.getBoolean(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "androidIdCached"
            r9 = 0
            java.lang.String r3 = r3.getString(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x063c
            java.lang.String r6 = r1.f47     // Catch:{ Throwable -> 0x0b5d }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x063c
            boolean r6 = m68(r19)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x0643
            android.content.ContentResolver r6 = r19.getContentResolver()     // Catch:{ Exception -> 0x0620 }
            java.lang.String r7 = "android_id"
            java.lang.String r9 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ Exception -> 0x0620 }
            if (r9 == 0) goto L_0x060f
            goto L_0x0644
        L_0x060f:
            if (r3 == 0) goto L_0x0643
            java.lang.String r6 = "use cached AndroidId: "
            java.lang.String r7 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0620 }
            java.lang.String r6 = r6.concat(r7)     // Catch:{ Exception -> 0x0620 }
            com.appsflyer.AFLogger.afDebugLog(r6)     // Catch:{ Exception -> 0x0620 }
            r9 = r3
            goto L_0x0644
        L_0x0620:
            r0 = move-exception
            r6 = r0
            if (r3 == 0) goto L_0x0633
            java.lang.String r7 = "use cached AndroidId: "
            java.lang.String r9 = java.lang.String.valueOf(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = r7.concat(r9)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afDebugLog(r7)     // Catch:{ Throwable -> 0x0b5d }
            r9 = r3
            goto L_0x0634
        L_0x0633:
            r9 = 0
        L_0x0634:
            java.lang.String r3 = r6.getMessage()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afErrorLog(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0644
        L_0x063c:
            java.lang.String r3 = r1.f47     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0643
            java.lang.String r9 = r1.f47     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0644
        L_0x0643:
            r9 = 0
        L_0x0644:
            if (r9 == 0) goto L_0x0651
            java.lang.String r3 = "androidIdCached"
            m53(r2, r3, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "android_id"
            r8.put(r3, r9)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0656
        L_0x0651:
            java.lang.String r3 = "Android ID was not collected."
            com.appsflyer.AFLogger.afInfoLog(r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0656:
            java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference     // Catch:{ Exception -> 0x0667 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0667 }
            java.lang.String r3 = com.appsflyer.p.m174(r3)     // Catch:{ Exception -> 0x0667 }
            if (r3 == 0) goto L_0x067e
            java.lang.String r6 = "uid"
            r8.put(r6, r3)     // Catch:{ Exception -> 0x0667 }
            goto L_0x067e
        L_0x0667:
            r0 = move-exception
            r3 = r0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "ERROR: could not get uid "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = r3.getMessage()     // Catch:{ Throwable -> 0x0b5d }
            r6.append(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afErrorLog(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x067e:
            java.lang.String r3 = "lang"
            java.util.Locale r6 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x068c }
            java.lang.String r6 = r6.getDisplayLanguage()     // Catch:{ Exception -> 0x068c }
            r8.put(r3, r6)     // Catch:{ Exception -> 0x068c }
            goto L_0x0693
        L_0x068c:
            r0 = move-exception
            r3 = r0
            java.lang.String r6 = "Exception while collecting display language name. "
            com.appsflyer.AFLogger.afErrorLog(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0693:
            java.lang.String r3 = "lang_code"
            java.util.Locale r6 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x06a1 }
            java.lang.String r6 = r6.getLanguage()     // Catch:{ Exception -> 0x06a1 }
            r8.put(r3, r6)     // Catch:{ Exception -> 0x06a1 }
            goto L_0x06a8
        L_0x06a1:
            r0 = move-exception
            r3 = r0
            java.lang.String r6 = "Exception while collecting display language code. "
            com.appsflyer.AFLogger.afErrorLog(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x06a8:
            java.lang.String r3 = "country"
            java.util.Locale r6 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x06b6 }
            java.lang.String r6 = r6.getCountry()     // Catch:{ Exception -> 0x06b6 }
            r8.put(r3, r6)     // Catch:{ Exception -> 0x06b6 }
            goto L_0x06bd
        L_0x06b6:
            r0 = move-exception
            r3 = r0
            java.lang.String r6 = "Exception while collecting country name. "
            com.appsflyer.AFLogger.afErrorLog(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x06bd:
            java.lang.String r3 = "platformextension"
            com.appsflyer.q r6 = r1.f40     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.m177()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
            m25(r2, r8)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "yyyy-MM-dd_HHmmssZ"
            java.text.SimpleDateFormat r6 = new java.text.SimpleDateFormat     // Catch:{ Throwable -> 0x0b5d }
            java.util.Locale r7 = java.util.Locale.US     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r3, r7)     // Catch:{ Throwable -> 0x0b5d }
            android.content.pm.PackageManager r3 = r19.getPackageManager()     // Catch:{ Exception -> 0x06fb }
            java.lang.String r7 = r19.getPackageName()     // Catch:{ Exception -> 0x06fb }
            r9 = 0
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r7, r9)     // Catch:{ Exception -> 0x06fb }
            long r9 = r3.firstInstallTime     // Catch:{ Exception -> 0x06fb }
            java.lang.String r3 = "installDate"
            java.lang.String r7 = "UTC"
            java.util.TimeZone r7 = java.util.TimeZone.getTimeZone(r7)     // Catch:{ Exception -> 0x06fb }
            r6.setTimeZone(r7)     // Catch:{ Exception -> 0x06fb }
            java.util.Date r7 = new java.util.Date     // Catch:{ Exception -> 0x06fb }
            r7.<init>(r9)     // Catch:{ Exception -> 0x06fb }
            java.lang.String r7 = r6.format(r7)     // Catch:{ Exception -> 0x06fb }
            r8.put(r3, r7)     // Catch:{ Exception -> 0x06fb }
            goto L_0x0702
        L_0x06fb:
            r0 = move-exception
            r3 = r0
            java.lang.String r7 = "Exception while collecting install date. "
            com.appsflyer.AFLogger.afErrorLog(r7, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0702:
            android.content.pm.PackageManager r3 = r19.getPackageManager()     // Catch:{ Throwable -> 0x07b6 }
            java.lang.String r7 = r19.getPackageName()     // Catch:{ Throwable -> 0x07b6 }
            r9 = 0
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r7, r9)     // Catch:{ Throwable -> 0x07b6 }
            java.lang.String r7 = "versionCode"
            r10 = r25
            int r7 = r10.getInt(r7, r9)     // Catch:{ Throwable -> 0x07b4 }
            int r11 = r3.versionCode     // Catch:{ Throwable -> 0x07b4 }
            if (r11 <= r7) goto L_0x0727
            java.lang.String r7 = "appsflyerConversionDataRequestRetries"
            m52(r2, r7, r9)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r7 = "versionCode"
            int r9 = r3.versionCode     // Catch:{ Throwable -> 0x07b4 }
            m52(r2, r7, r9)     // Catch:{ Throwable -> 0x07b4 }
        L_0x0727:
            java.lang.String r7 = "app_version_code"
            int r9 = r3.versionCode     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ Throwable -> 0x07b4 }
            r8.put(r7, r9)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r7 = "app_version_name"
            java.lang.String r9 = r3.versionName     // Catch:{ Throwable -> 0x07b4 }
            r8.put(r7, r9)     // Catch:{ Throwable -> 0x07b4 }
            long r11 = r3.firstInstallTime     // Catch:{ Throwable -> 0x07b4 }
            long r13 = r3.lastUpdateTime     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r3 = "date1"
            java.lang.String r7 = "UTC"
            java.util.TimeZone r7 = java.util.TimeZone.getTimeZone(r7)     // Catch:{ Throwable -> 0x07b4 }
            r6.setTimeZone(r7)     // Catch:{ Throwable -> 0x07b4 }
            java.util.Date r7 = new java.util.Date     // Catch:{ Throwable -> 0x07b4 }
            r7.<init>(r11)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r7 = r6.format(r7)     // Catch:{ Throwable -> 0x07b4 }
            r8.put(r3, r7)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r3 = "date2"
            java.lang.String r7 = "UTC"
            java.util.TimeZone r7 = java.util.TimeZone.getTimeZone(r7)     // Catch:{ Throwable -> 0x07b4 }
            r6.setTimeZone(r7)     // Catch:{ Throwable -> 0x07b4 }
            java.util.Date r7 = new java.util.Date     // Catch:{ Throwable -> 0x07b4 }
            r7.<init>(r13)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r7 = r6.format(r7)     // Catch:{ Throwable -> 0x07b4 }
            r8.put(r3, r7)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r3 = "appsflyer-data"
            r7 = 0
            android.content.SharedPreferences r3 = r2.getSharedPreferences(r3, r7)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r9 = "appsFlyerFirstInstall"
            r11 = 0
            java.lang.String r3 = r3.getString(r9, r11)     // Catch:{ Throwable -> 0x07b4 }
            if (r3 != 0) goto L_0x07a1
            java.lang.String r3 = "appsflyer-data"
            android.content.SharedPreferences r3 = r2.getSharedPreferences(r3, r7)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r7 = "appsFlyerCount"
            boolean r3 = r3.contains(r7)     // Catch:{ Throwable -> 0x07b4 }
            r7 = 1
            r3 = r3 ^ r7
            if (r3 == 0) goto L_0x079a
            java.lang.String r3 = "AppsFlyer: first launch detected"
            com.appsflyer.AFLogger.afDebugLog(r3)     // Catch:{ Throwable -> 0x07b4 }
            java.util.Date r3 = new java.util.Date     // Catch:{ Throwable -> 0x07b4 }
            r3.<init>()     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r3 = r6.format(r3)     // Catch:{ Throwable -> 0x07b4 }
            goto L_0x079c
        L_0x079a:
            java.lang.String r3 = ""
        L_0x079c:
            java.lang.String r6 = "appsFlyerFirstInstall"
            m53(r2, r6, r3)     // Catch:{ Throwable -> 0x07b4 }
        L_0x07a1:
            java.lang.String r6 = "AppsFlyer: first launch date: "
            java.lang.String r7 = java.lang.String.valueOf(r3)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r6 = r6.concat(r7)     // Catch:{ Throwable -> 0x07b4 }
            com.appsflyer.AFLogger.afInfoLog(r6)     // Catch:{ Throwable -> 0x07b4 }
            java.lang.String r6 = "firstLaunchDate"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x07b4 }
            goto L_0x07bf
        L_0x07b4:
            r0 = move-exception
            goto L_0x07b9
        L_0x07b6:
            r0 = move-exception
            r10 = r25
        L_0x07b9:
            r3 = r0
            java.lang.String r6 = "Exception while collecting app version data "
            com.appsflyer.AFLogger.afErrorLog(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x07bf:
            int r3 = r23.length()     // Catch:{ Throwable -> 0x0b5d }
            if (r3 <= 0) goto L_0x07cc
            java.lang.String r3 = "referrer"
            r6 = r23
            r8.put(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x07cc:
            java.lang.String r3 = "extraReferrers"
            r6 = 0
            java.lang.String r3 = r10.getString(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x07da
            java.lang.String r6 = "extraReferrers"
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x07da:
            java.lang.String r3 = "afUninstallToken"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r6.getString(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x07f3
            com.appsflyer.b$e$b r3 = com.appsflyer.b.e.C0007b.m111(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "af_gcm_token"
            java.lang.String r3 = r3.m112()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r6, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x07f3:
            boolean r3 = com.appsflyer.y.m225(r19)     // Catch:{ Throwable -> 0x0b5d }
            r1.f43 = r3     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "didConfigureTokenRefreshService="
            r3.<init>(r6)     // Catch:{ Throwable -> 0x0b5d }
            boolean r6 = r1.f43     // Catch:{ Throwable -> 0x0b5d }
            r3.append(r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.AFLogger.afDebugLog(r3)     // Catch:{ Throwable -> 0x0b5d }
            boolean r3 = r1.f43     // Catch:{ Throwable -> 0x0b5d }
            if (r3 != 0) goto L_0x0817
            java.lang.String r3 = "tokenRefreshConfigured"
            java.lang.Boolean r6 = java.lang.Boolean.FALSE     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0817:
            r3 = r26
            if (r3 == 0) goto L_0x0842
            com.appsflyer.AFDeepLinkManager r6 = com.appsflyer.AFDeepLinkManager.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r7 = r27
            r6.processIntentForDeepLink(r7, r2, r8)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r1.f36     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x083f
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = r1.f36     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "isPush"
            java.lang.String r9 = "true"
            r6.put(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "af_deeplink"
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x083f:
            r6 = 0
            r1.f36 = r6     // Catch:{ Throwable -> 0x0b5d }
        L_0x0842:
            boolean r6 = r1.f39     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x085e
            java.lang.String r6 = "testAppMode_retargeting"
            java.lang.String r7 = "true"
            r8.put(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r8)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0b5d }
            m51(r2, r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "Sent retargeting params to test app"
            com.appsflyer.AFLogger.afInfoLog(r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x085e:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0b5d }
            long r11 = r1.f38     // Catch:{ Throwable -> 0x0b5d }
            r9 = 0
            long r6 = r6 - r11
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = r9.getReferrer(r2)     // Catch:{ Throwable -> 0x0b5d }
            r11 = 30000(0x7530, double:1.4822E-319)
            int r13 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r13 > 0) goto L_0x0880
            if (r9 == 0) goto L_0x0880
            java.lang.String r6 = "AppsFlyer_Test"
            boolean r6 = r9.contains(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x0880
            r6 = 1
            goto L_0x0881
        L_0x0880:
            r6 = 0
        L_0x0881:
            if (r6 == 0) goto L_0x08a4
            java.lang.String r6 = "testAppMode"
            java.lang.String r7 = "true"
            r8.put(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r8)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0b5d }
            m51(r2, r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "Sent params to test app"
            com.appsflyer.AFLogger.afInfoLog(r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "Test mode ended!"
            com.appsflyer.AFLogger.afInfoLog(r6)     // Catch:{ Throwable -> 0x0b5d }
            r6 = 0
            r1.f38 = r6     // Catch:{ Throwable -> 0x0b5d }
        L_0x08a4:
            java.lang.String r6 = "advertiserId"
            com.appsflyer.AppsFlyerProperties r7 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r7.getString(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 != 0) goto L_0x08ce
            com.appsflyer.n.m161(r2, r8)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "advertiserId"
            com.appsflyer.AppsFlyerProperties r7 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r7.getString(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x08c7
            java.lang.String r6 = "GAID_retry"
            java.lang.String r7 = "true"
            r8.put(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x08ce
        L_0x08c7:
            java.lang.String r6 = "GAID_retry"
            java.lang.String r7 = "false"
            r8.put(r6, r7)     // Catch:{ Throwable -> 0x0b5d }
        L_0x08ce:
            android.content.ContentResolver r6 = r19.getContentResolver()     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.k r6 = com.appsflyer.n.m162(r6)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x08ee
            java.lang.String r7 = "amazon_aid"
            java.lang.String r9 = r6.m155()     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "amazon_aid_limit"
            boolean r6 = r6.m154()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x08ee:
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = r6.getReferrer(r2)     // Catch:{ Throwable -> 0x0b5d }
            if (r6 == 0) goto L_0x090b
            int r7 = r6.length()     // Catch:{ Throwable -> 0x0b5d }
            if (r7 <= 0) goto L_0x090b
            java.lang.String r7 = "referrer"
            java.lang.Object r7 = r8.get(r7)     // Catch:{ Throwable -> 0x0b5d }
            if (r7 != 0) goto L_0x090b
            java.lang.String r7 = "referrer"
            r8.put(r7, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x090b:
            java.lang.String r6 = "true"
            java.lang.String r7 = "sentSuccessfully"
            java.lang.String r9 = ""
            java.lang.String r7 = r10.getString(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            boolean r6 = r6.equals(r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "sentRegisterRequestToAF"
            r9 = 0
            boolean r7 = r10.getBoolean(r7, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = "registeredUninstall"
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r9, r7)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r7 = "appsFlyerCount"
            int r7 = m48(r10, r7, r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = "counter"
            java.lang.String r11 = java.lang.Integer.toString(r7)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r9, r11)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = "iaecounter"
            if (r4 == 0) goto L_0x093e
            r4 = 1
            goto L_0x093f
        L_0x093e:
            r4 = 0
        L_0x093f:
            java.lang.String r11 = "appsFlyerInAppEventCount"
            int r4 = m48(r10, r11, r4)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r9, r4)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x096f
            r4 = 1
            if (r7 != r4) goto L_0x096f
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r4.setFirstLaunchCalled()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r4 = "waitForCustomerId"
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            r11 = 0
            boolean r4 = r9.getBoolean(r4, r11)     // Catch:{ Throwable -> 0x0b5d }
            if (r4 == 0) goto L_0x096f
            java.lang.String r4 = "wait_cid"
            r9 = 1
            java.lang.String r11 = java.lang.Boolean.toString(r9)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r4, r11)     // Catch:{ Throwable -> 0x0b5d }
        L_0x096f:
            java.lang.String r4 = "isFirstCall"
            r9 = 1
            r6 = r6 ^ r9
            java.lang.String r6 = java.lang.Boolean.toString(r6)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r4, r6)     // Catch:{ Throwable -> 0x0b5d }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Throwable -> 0x0b5d }
            r4.<init>()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "cpu_abi"
            java.lang.String r9 = "ro.product.cpu.abi"
            java.lang.String r9 = m20(r9)     // Catch:{ Throwable -> 0x0b5d }
            r4.put(r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "cpu_abi2"
            java.lang.String r9 = "ro.product.cpu.abi2"
            java.lang.String r9 = m20(r9)     // Catch:{ Throwable -> 0x0b5d }
            r4.put(r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "arch"
            java.lang.String r9 = "os.arch"
            java.lang.String r9 = m20(r9)     // Catch:{ Throwable -> 0x0b5d }
            r4.put(r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = "build_display_id"
            java.lang.String r9 = "ro.build.display.id"
            java.lang.String r9 = m20(r9)     // Catch:{ Throwable -> 0x0b5d }
            r4.put(r6, r9)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x0a28
            boolean r3 = r1.f34     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x09f0
            com.appsflyer.c r3 = com.appsflyer.c.a.f108     // Catch:{ Throwable -> 0x0b5d }
            android.location.Location r3 = com.appsflyer.c.m115(r19)     // Catch:{ Throwable -> 0x0b5d }
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>(r5)     // Catch:{ Throwable -> 0x0b5d }
            if (r3 == 0) goto L_0x09e5
            java.lang.String r5 = "lat"
            double r11 = r3.getLatitude()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = java.lang.String.valueOf(r11)     // Catch:{ Throwable -> 0x0b5d }
            r6.put(r5, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = "lon"
            double r11 = r3.getLongitude()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r9 = java.lang.String.valueOf(r11)     // Catch:{ Throwable -> 0x0b5d }
            r6.put(r5, r9)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = "ts"
            long r11 = r3.getTime()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = java.lang.String.valueOf(r11)     // Catch:{ Throwable -> 0x0b5d }
            r6.put(r5, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x09e5:
            boolean r3 = r6.isEmpty()     // Catch:{ Throwable -> 0x0b5d }
            if (r3 != 0) goto L_0x09f0
            java.lang.String r3 = "loc"
            r4.put(r3, r6)     // Catch:{ Throwable -> 0x0b5d }
        L_0x09f0:
            com.appsflyer.e r3 = com.appsflyer.e.b.f117     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.e$d r3 = r3.m118(r2)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = "btl"
            float r6 = r3.m120()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r6 = java.lang.Float.toString(r6)     // Catch:{ Throwable -> 0x0b5d }
            r4.put(r5, r6)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = r3.m119()     // Catch:{ Throwable -> 0x0b5d }
            if (r5 == 0) goto L_0x0a12
            java.lang.String r5 = "btch"
            java.lang.String r3 = r3.m119()     // Catch:{ Throwable -> 0x0b5d }
            r4.put(r5, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0a12:
            r3 = 2
            if (r3 < r7) goto L_0x0a28
            com.appsflyer.j r3 = com.appsflyer.j.m147(r19)     // Catch:{ Throwable -> 0x0b5d }
            java.util.List r3 = r3.m148()     // Catch:{ Throwable -> 0x0b5d }
            boolean r5 = r3.isEmpty()     // Catch:{ Throwable -> 0x0b5d }
            if (r5 != 0) goto L_0x0a28
            java.lang.String r5 = "sensors"
            r4.put(r5, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0a28:
            java.util.Map r3 = com.appsflyer.AFScreenManager.getScreenMetrics(r19)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = "dim"
            r4.put(r5, r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "deviceData"
            r8.put(r3, r4)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.t r3 = new com.appsflyer.t     // Catch:{ Throwable -> 0x0b5d }
            r3.<init>()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "appsflyerKey"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r4 = "af_timestamp"
            java.lang.Object r4 = r8.get(r4)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = "uid"
            java.lang.Object r5 = r8.get(r5)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            r6.<init>()     // Catch:{ Throwable -> 0x0b5d }
            r7 = 7
            r9 = 0
            java.lang.String r3 = r3.substring(r9, r7)     // Catch:{ Throwable -> 0x0b5d }
            r6.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r5.substring(r9, r7)     // Catch:{ Throwable -> 0x0b5d }
            r6.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            int r3 = r4.length()     // Catch:{ Throwable -> 0x0b5d }
            int r3 = r3 - r7
            java.lang.String r3 = r4.substring(r3)     // Catch:{ Throwable -> 0x0b5d }
            r6.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r6.toString()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = com.appsflyer.t.m221(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r4 = "af_v"
            r8.put(r4, r3)     // Catch:{ Throwable -> 0x0b5d }
            com.appsflyer.t r3 = new com.appsflyer.t     // Catch:{ Throwable -> 0x0b5d }
            r3.<init>()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "appsflyerKey"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            r4.<init>()     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "af_timestamp"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r4.toString()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            r4.<init>()     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "uid"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r4.toString()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            r4.<init>()     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "installDate"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r4.toString()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            r4.<init>()     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "counter"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r4.toString()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0b5d }
            r4.<init>()     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "iaecounter"
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Throwable -> 0x0b5d }
            r4.append(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = r4.toString()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = com.appsflyer.t.m220(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = com.appsflyer.t.m221(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r4 = "af_v2"
            r8.put(r4, r3)     // Catch:{ Throwable -> 0x0b5d }
            boolean r2 = m69(r19)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "ivc"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r3, r2)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r2 = "is_stop_tracking_used"
            boolean r2 = r10.contains(r2)     // Catch:{ Throwable -> 0x0b5d }
            if (r2 == 0) goto L_0x0b29
            java.lang.String r2 = "istu"
            java.lang.String r3 = "is_stop_tracking_used"
            r4 = 0
            boolean r3 = r10.getBoolean(r3, r4)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Throwable -> 0x0b5d }
            r8.put(r2, r3)     // Catch:{ Throwable -> 0x0b5d }
        L_0x0b29:
            com.appsflyer.AppsFlyerProperties r2 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "consumeAfDeepLink"
            java.lang.Object r2 = r2.getObject(r3)     // Catch:{ Throwable -> 0x0b5d }
            if (r2 == 0) goto L_0x0b66
            com.appsflyer.AppsFlyerProperties r2 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "consumeAfDeepLink"
            r4 = 0
            boolean r2 = r2.getBoolean(r3, r4)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "is_dp_api"
            r8.put(r3, r2)     // Catch:{ Throwable -> 0x0b5d }
            goto L_0x0b66
        L_0x0b4a:
            java.lang.String r3 = "AppsFlyer dev key is missing!!! Please use  AppsFlyerLib.getInstance().setAppsFlyerKey(...) to set it. "
            com.appsflyer.AFLogger.afInfoLog(r3)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r3 = "AppsFlyer_4.8.18"
            java.lang.String r4 = "DEV_KEY_MISSING"
            r5 = 0
            m64(r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0b5d }
            java.lang.String r2 = "AppsFlyer will not track this event."
            com.appsflyer.AFLogger.afInfoLog(r2)     // Catch:{ Throwable -> 0x0b5d }
            return r5
        L_0x0b5d:
            r0 = move-exception
            r2 = r0
            java.lang.String r3 = r2.getLocalizedMessage()
            com.appsflyer.AFLogger.afErrorLog(r3, r2)
        L_0x0b66:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.m72(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, android.content.SharedPreferences, boolean, android.content.Intent):java.util.Map");
    }

    public void setConsumeAFDeepLinks(boolean z) {
        AppsFlyerProperties.getInstance().set("consumeAfDeepLink", z);
        r.m185().m193("setConsumeAFDeepLinks: ".concat(String.valueOf(z)), new String[0]);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static void m25(Context context, Map<String, ? super String> map) {
        i iVar = i.c.f149;
        i.b r2 = i.m141(context);
        map.put("network", r2.m143());
        if (r2.m144() != null) {
            map.put("operator", r2.m144());
        }
        if (r2.m145() != null) {
            map.put("carrier", r2.m145());
        }
    }

    /* access modifiers changed from: protected */
    public void handleDeepLinkCallback(Context context, Map<String, Object> map, Uri uri) {
        final Map map2;
        map.put("af_deeplink", uri.toString());
        if (uri.getQueryParameter("af_deeplink") != null) {
            this.f39 = "AppsFlyer_Test".equals(uri.getQueryParameter("media_source")) && Boolean.parseBoolean(uri.getQueryParameter("is_retargeting"));
            map2 = m42(context, uri.getQuery());
            String path = uri.getPath();
            if (path != null) {
                map2.put("path", path);
            }
            String scheme = uri.getScheme();
            if (scheme != null) {
                map2.put("scheme", scheme);
            }
            String host = uri.getHost();
            if (host != null) {
                map2.put("host", host);
            }
        } else {
            map2 = new HashMap();
            map2.put("link", uri.toString());
        }
        final WeakReference weakReference = new WeakReference(context);
        s sVar = new s(uri, this);
        sVar.setConnProvider(new OneLinkHttpTask.HttpsUrlConnectionProvider());
        if (sVar.m207()) {
            sVar.m205(new s.c() {
                /* renamed from: ˎ  reason: contains not printable characters */
                public final void m79(String str) {
                    if (AppsFlyerLib.f22 != null) {
                        m77(map2);
                        AppsFlyerLib.f22.onAttributionFailure(str);
                    }
                }

                /* renamed from: ˋ  reason: contains not printable characters */
                private void m77(Map<String, String> map) {
                    if (weakReference.get() != null) {
                        AppsFlyerLib.m53((Context) weakReference.get(), "deeplinkAttribution", new JSONObject(map).toString());
                    }
                }

                /* renamed from: ˊ  reason: contains not printable characters */
                public final void m78(Map<String, String> map) {
                    for (String next : map.keySet()) {
                        map2.put(next, map.get(next));
                    }
                    m77(map2);
                    AppsFlyerLib.m65(map2);
                }
            });
            AFExecutor.getInstance().getThreadPoolExecutor().execute(sVar);
        } else if (f22 != null) {
            try {
                f22.onAppOpenAttribution(map2);
            } catch (Throwable th) {
                AFLogger.afErrorLog(th.getLocalizedMessage(), th);
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m27(Context context) {
        try {
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == 0) {
                return true;
            }
        } catch (Throwable th) {
            AFLogger.afErrorLog("WARNING:  Google play services is unavailable. ", th);
        }
        try {
            context.getPackageManager().getPackageInfo("com.google.android.gms", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            AFLogger.afErrorLog("WARNING:  Google Play Services is unavailable. ", e2);
            return false;
        }
    }

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private static boolean m68(Context context) {
        if ((AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.COLLECT_ANDROID_ID_FORCE_BY_USER, false) || AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.COLLECT_IMEI_FORCE_BY_USER, false)) || !m27(context)) {
            return true;
        }
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static String m18(Context context) {
        String string = AppsFlyerProperties.getInstance().getString("api_store_value");
        if (string != null) {
            return string;
        }
        String r2 = m61(new WeakReference(context), "AF_STORE");
        if (r2 != null) {
            return r2;
        }
        return null;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m20(String str) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class).invoke(null, str);
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return null;
        }
    }

    @Nullable
    /* renamed from: ॱ  reason: contains not printable characters */
    private static String m61(WeakReference<Context> weakReference, String str) {
        if (weakReference.get() == null) {
            return null;
        }
        return m21(str, weakReference.get().getPackageManager(), weakReference.get().getPackageName());
    }

    @Nullable
    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m21(String str, PackageManager packageManager, String str2) {
        Object obj;
        try {
            Bundle bundle = packageManager.getApplicationInfo(str2, 128).metaData;
            if (bundle == null || (obj = bundle.get(str)) == null) {
                return null;
            }
            return obj.toString();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder("Could not find ");
            sb.append(str);
            sb.append(" value in the manifest");
            AFLogger.afErrorLog(sb.toString(), th);
            return null;
        }
    }

    public void setPreinstallAttribution(String str, String str2, String str3) {
        AFLogger.afDebugLog("setPreinstallAttribution API called");
        JSONObject jSONObject = new JSONObject();
        if (str != null) {
            try {
                jSONObject.put(com.appsflyer.share.Constants.URL_MEDIA_SOURCE, str);
            } catch (JSONException e2) {
                AFLogger.afErrorLog(e2.getMessage(), e2);
            }
        }
        if (str2 != null) {
            jSONObject.put("c", str2);
        }
        if (str3 != null) {
            jSONObject.put(com.appsflyer.share.Constants.URL_SITE_ID, str3);
        }
        if (jSONObject.has(com.appsflyer.share.Constants.URL_MEDIA_SOURCE)) {
            AppsFlyerProperties.getInstance().set("preInstallName", jSONObject.toString());
            return;
        }
        AFLogger.afWarnLog("Cannot set preinstall attribution data without a media source");
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x0042 */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0034 A[SYNTHETIC, Splitter:B:18:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0060 A[SYNTHETIC, Splitter:B:32:0x0060] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x0042=Splitter:B:24:0x0042, B:15:0x002b=Splitter:B:15:0x002b} */
    /* renamed from: ˎ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m39(java.io.File r4, java.lang.String r5) {
        /*
            r0 = 0
            java.util.Properties r1 = new java.util.Properties     // Catch:{ FileNotFoundException -> 0x0041, Throwable -> 0x0029, all -> 0x0026 }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0041, Throwable -> 0x0029, all -> 0x0026 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0041, Throwable -> 0x0029, all -> 0x0026 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0041, Throwable -> 0x0029, all -> 0x0026 }
            r1.load(r2)     // Catch:{ FileNotFoundException -> 0x0042, Throwable -> 0x0024 }
            java.lang.String r3 = "Found PreInstall property!"
            com.appsflyer.AFLogger.afInfoLog(r3)     // Catch:{ FileNotFoundException -> 0x0042, Throwable -> 0x0024 }
            java.lang.String r5 = r1.getProperty(r5)     // Catch:{ FileNotFoundException -> 0x0042, Throwable -> 0x0024 }
            r2.close()     // Catch:{ Throwable -> 0x001b }
            goto L_0x0023
        L_0x001b:
            r4 = move-exception
            java.lang.String r0 = r4.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r0, r4)
        L_0x0023:
            return r5
        L_0x0024:
            r4 = move-exception
            goto L_0x002b
        L_0x0026:
            r4 = move-exception
            r2 = r0
            goto L_0x005e
        L_0x0029:
            r4 = move-exception
            r2 = r0
        L_0x002b:
            java.lang.String r5 = r4.getMessage()     // Catch:{ all -> 0x005d }
            com.appsflyer.AFLogger.afErrorLog(r5, r4)     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x005c
            r2.close()     // Catch:{ Throwable -> 0x0038 }
            goto L_0x005c
        L_0x0038:
            r4 = move-exception
            java.lang.String r5 = r4.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r5, r4)
            goto L_0x005c
        L_0x0041:
            r2 = r0
        L_0x0042:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            java.lang.String r1 = "PreInstall file wasn't found: "
            r5.<init>(r1)     // Catch:{ all -> 0x005d }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ all -> 0x005d }
            r5.append(r4)     // Catch:{ all -> 0x005d }
            java.lang.String r4 = r5.toString()     // Catch:{ all -> 0x005d }
            com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x005c
            r2.close()     // Catch:{ Throwable -> 0x0038 }
        L_0x005c:
            return r0
        L_0x005d:
            r4 = move-exception
        L_0x005e:
            if (r2 == 0) goto L_0x006c
            r2.close()     // Catch:{ Throwable -> 0x0064 }
            goto L_0x006c
        L_0x0064:
            r5 = move-exception
            java.lang.String r0 = r5.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r0, r5)
        L_0x006c:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.m39(java.io.File, java.lang.String):java.lang.String");
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static File m49(String str) {
        if (str == null) {
            return null;
        }
        try {
            if (str.trim().length() > 0) {
                return new File(str.trim());
            }
            return null;
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return null;
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    /* renamed from: ˎ  reason: contains not printable characters */
    public static String m40(WeakReference<Context> weakReference) {
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.CHANNEL);
        if (string == null) {
            string = m61(weakReference, "CHANNEL");
        }
        if (string == null || !string.equals("")) {
            return string;
        }
        return null;
    }

    public boolean isPreInstalledApp(Context context) {
        try {
            if ((context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).flags & 1) != 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            AFLogger.afErrorLog("Could not check if app is pre installed", e2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱॱ  reason: contains not printable characters */
    public static String m67(Context context, String str) throws PackageManager.NameNotFoundException {
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        if (sharedPreferences.contains("CACHED_CHANNEL")) {
            return sharedPreferences.getString("CACHED_CHANNEL", null);
        }
        m53(context, "CACHED_CHANNEL", str);
        return str;
    }

    public String getAttributionId(ContentResolver contentResolver) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor query = contentResolver2.query(Uri.parse(ATTRIBUTION_ID_CONTENT_URI), new String[]{ATTRIBUTION_ID_COLUMN_NAME}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex(ATTRIBUTION_ID_COLUMN_NAME));
                    if (query != null) {
                        try {
                            query.close();
                        } catch (Exception e2) {
                            AFLogger.afErrorLog(e2.getMessage(), e2);
                        }
                    }
                    return string;
                }
            } catch (Exception e3) {
                AFLogger.afErrorLog("Could not collect cursor attribution. ", e3);
                if (query == null) {
                    return null;
                }
                try {
                    query.close();
                    return null;
                } catch (Exception e4) {
                    AFLogger.afErrorLog(e4.getMessage(), e4);
                    return null;
                }
            } catch (Throwable th) {
                if (query != null) {
                    try {
                        query.close();
                    } catch (Exception e5) {
                        AFLogger.afErrorLog(e5.getMessage(), e5);
                    }
                }
                throw th;
            }
        }
        if (query != null) {
            try {
                query.close();
            } catch (Exception e6) {
                AFLogger.afErrorLog(e6.getMessage(), e6);
            }
        }
        return null;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static SharedPreferences m57(Context context) {
        return context.getSharedPreferences("appsflyer-data", 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, int):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* renamed from: ˋ  reason: contains not printable characters */
    static int m29(SharedPreferences sharedPreferences) {
        return m48(sharedPreferences, "appsFlyerCount", false);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static int m48(SharedPreferences sharedPreferences, String str, boolean z) {
        int i = sharedPreferences.getInt(str, 0);
        if (z) {
            i++;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt(str, i);
            if (Build.VERSION.SDK_INT >= 9) {
                edit.apply();
            } else {
                edit.commit();
            }
        }
        if (r.m185().m201()) {
            r.m185().m197(String.valueOf(i));
        }
        return i;
    }

    public String getAppsFlyerUID(Context context) {
        r.m185().m193("getAppsFlyerUID", new String[0]);
        return p.m174(new WeakReference(context));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, int):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.Context, java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerLib.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x022a A[Catch:{ l -> 0x0209, all -> 0x022e }] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x021e A[SYNTHETIC, Splitter:B:99:0x021e] */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m26(java.net.URL r18, java.lang.String r19, java.lang.String r20, java.lang.ref.WeakReference<android.content.Context> r21, java.lang.String r22, boolean r23) throws java.io.IOException {
        /*
            r17 = this;
            r1 = r17
            r0 = r19
            r2 = r20
            r3 = r22
            java.lang.Object r5 = r21.get()
            android.content.Context r5 = (android.content.Context) r5
            r6 = 1
            r7 = 0
            if (r23 == 0) goto L_0x0018
            com.appsflyer.AppsFlyerConversionListener r8 = com.appsflyer.AppsFlyerLib.f22
            if (r8 == 0) goto L_0x0018
            r8 = 1
            goto L_0x0019
        L_0x0018:
            r8 = 0
        L_0x0019:
            r9 = 0
            com.appsflyer.r r10 = com.appsflyer.r.m185()     // Catch:{ all -> 0x0230 }
            java.lang.String r11 = r18.toString()     // Catch:{ all -> 0x0230 }
            r10.m198(r11, r0)     // Catch:{ all -> 0x0230 }
            java.net.URLConnection r10 = r18.openConnection()     // Catch:{ all -> 0x0230 }
            java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ all -> 0x0230 }
            java.lang.String r11 = "POST"
            r10.setRequestMethod(r11)     // Catch:{ all -> 0x022e }
            byte[] r11 = r19.getBytes()     // Catch:{ all -> 0x022e }
            int r11 = r11.length     // Catch:{ all -> 0x022e }
            java.lang.String r12 = "Content-Length"
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x022e }
            r10.setRequestProperty(r12, r11)     // Catch:{ all -> 0x022e }
            java.lang.String r11 = "Content-Type"
            java.lang.String r12 = "application/json"
            r10.setRequestProperty(r11, r12)     // Catch:{ all -> 0x022e }
            r11 = 10000(0x2710, float:1.4013E-41)
            r10.setConnectTimeout(r11)     // Catch:{ all -> 0x022e }
            r10.setDoOutput(r6)     // Catch:{ all -> 0x022e }
            java.io.OutputStreamWriter r11 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x021b }
            java.io.OutputStream r12 = r10.getOutputStream()     // Catch:{ all -> 0x021b }
            java.lang.String r13 = "UTF-8"
            r11.<init>(r12, r13)     // Catch:{ all -> 0x021b }
            r11.write(r0)     // Catch:{ all -> 0x0218 }
            r11.close()     // Catch:{ all -> 0x022e }
            int r0 = r10.getResponseCode()     // Catch:{ all -> 0x022e }
            java.lang.String r11 = m31(r10)     // Catch:{ all -> 0x022e }
            com.appsflyer.r r12 = com.appsflyer.r.m185()     // Catch:{ all -> 0x022e }
            java.lang.String r13 = r18.toString()     // Catch:{ all -> 0x022e }
            r12.m195(r13, r0, r11)     // Catch:{ all -> 0x022e }
            java.lang.String r12 = "response code: "
            java.lang.String r13 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x022e }
            java.lang.String r12 = r12.concat(r13)     // Catch:{ all -> 0x022e }
            com.appsflyer.AFLogger.afInfoLog(r12)     // Catch:{ all -> 0x022e }
            java.lang.String r12 = "AppsFlyer_4.8.18"
            java.lang.String r13 = "SERVER_RESPONSE_CODE"
            java.lang.String r14 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x022e }
            m64(r5, r12, r13, r14)     // Catch:{ all -> 0x022e }
            java.lang.String r12 = "appsflyer-data"
            android.content.SharedPreferences r12 = r5.getSharedPreferences(r12, r7)     // Catch:{ all -> 0x022e }
            r13 = 200(0xc8, float:2.8E-43)
            if (r0 != r13) goto L_0x015b
            java.lang.Object r0 = r21.get()     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x00aa
            if (r23 == 0) goto L_0x00aa
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x022e }
            r1.f37 = r13     // Catch:{ all -> 0x022e }
            com.appsflyer.AppsFlyerTrackingRequestListener r0 = com.appsflyer.AppsFlyerLib.f27     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x00aa
            com.appsflyer.AppsFlyerTrackingRequestListener r0 = com.appsflyer.AppsFlyerLib.f27     // Catch:{ all -> 0x022e }
            r0.onTrackingRequestSuccess()     // Catch:{ all -> 0x022e }
        L_0x00aa:
            java.lang.String r0 = "afUninstallToken"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x022e }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x00e1
            java.lang.String r4 = "Uninstall Token exists: "
            java.lang.String r13 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x022e }
            java.lang.String r4 = r4.concat(r13)     // Catch:{ all -> 0x022e }
            com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ all -> 0x022e }
            java.lang.String r4 = "sentRegisterRequestToAF"
            boolean r4 = r12.getBoolean(r4, r7)     // Catch:{ all -> 0x022e }
            if (r4 != 0) goto L_0x0101
            java.lang.String r4 = "Resending Uninstall token to AF servers: "
            java.lang.String r13 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x022e }
            java.lang.String r4 = r4.concat(r13)     // Catch:{ all -> 0x022e }
            com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ all -> 0x022e }
            com.appsflyer.b$e$b r4 = new com.appsflyer.b$e$b     // Catch:{ all -> 0x022e }
            r4.<init>(r0)     // Catch:{ all -> 0x022e }
            com.appsflyer.y.m227(r5, r4)     // Catch:{ all -> 0x022e }
            goto L_0x0101
        L_0x00e1:
            java.lang.String r0 = "gcmProjectNumber"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x022e }
            java.lang.String r0 = r4.getString(r0)     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x0101
            java.lang.String r0 = "GCM Project number exists. Fetching token and sending to AF servers"
            com.appsflyer.AFLogger.afDebugLog(r0)     // Catch:{ all -> 0x022e }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x022e }
            r0.<init>(r5)     // Catch:{ all -> 0x022e }
            com.appsflyer.y$b r4 = new com.appsflyer.y$b     // Catch:{ all -> 0x022e }
            r4.<init>(r0)     // Catch:{ all -> 0x022e }
            java.lang.Void[] r0 = new java.lang.Void[r7]     // Catch:{ all -> 0x022e }
            r4.execute(r0)     // Catch:{ all -> 0x022e }
        L_0x0101:
            android.net.Uri r0 = r1.latestDeepLink     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x0107
            r1.latestDeepLink = r9     // Catch:{ all -> 0x022e }
        L_0x0107:
            if (r3 == 0) goto L_0x0110
            com.appsflyer.cache.CacheManager r0 = com.appsflyer.cache.CacheManager.getInstance()     // Catch:{ all -> 0x022e }
            r0.deleteRequest(r3, r5)     // Catch:{ all -> 0x022e }
        L_0x0110:
            java.lang.Object r0 = r21.get()     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x014e
            if (r3 != 0) goto L_0x014e
            java.lang.String r0 = "sentSuccessfully"
            java.lang.String r3 = "true"
            m53(r5, r0, r3)     // Catch:{ all -> 0x022e }
            boolean r0 = r1.f29     // Catch:{ all -> 0x022e }
            if (r0 != 0) goto L_0x014e
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x022e }
            long r13 = r1.f50     // Catch:{ all -> 0x022e }
            r0 = 0
            long r3 = r3 - r13
            r13 = 15000(0x3a98, double:7.411E-320)
            int r0 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r0 >= 0) goto L_0x0132
            goto L_0x014e
        L_0x0132:
            java.util.concurrent.ScheduledExecutorService r0 = r1.f32     // Catch:{ all -> 0x022e }
            if (r0 != 0) goto L_0x014e
            com.appsflyer.AFExecutor r0 = com.appsflyer.AFExecutor.getInstance()     // Catch:{ all -> 0x022e }
            java.util.concurrent.ScheduledThreadPoolExecutor r0 = r0.m2()     // Catch:{ all -> 0x022e }
            r1.f32 = r0     // Catch:{ all -> 0x022e }
            com.appsflyer.AppsFlyerLib$d r0 = new com.appsflyer.AppsFlyerLib$d     // Catch:{ all -> 0x022e }
            r0.<init>(r5)     // Catch:{ all -> 0x022e }
            java.util.concurrent.ScheduledExecutorService r3 = r1.f32     // Catch:{ all -> 0x022e }
            r13 = 1
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x022e }
            m66(r3, r0, r13, r4)     // Catch:{ all -> 0x022e }
        L_0x014e:
            org.json.JSONObject r0 = com.appsflyer.ServerConfigHandler.m97(r11)     // Catch:{ all -> 0x022e }
            java.lang.String r3 = "send_background"
            boolean r0 = r0.optBoolean(r3, r7)     // Catch:{ all -> 0x022e }
            r1.f44 = r0     // Catch:{ all -> 0x022e }
            goto L_0x016e
        L_0x015b:
            com.appsflyer.AppsFlyerTrackingRequestListener r3 = com.appsflyer.AppsFlyerLib.f27     // Catch:{ all -> 0x022e }
            if (r3 == 0) goto L_0x016e
            java.lang.String r3 = "Failure: "
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x022e }
            java.lang.String r0 = r3.concat(r0)     // Catch:{ all -> 0x022e }
            com.appsflyer.AppsFlyerTrackingRequestListener r3 = com.appsflyer.AppsFlyerLib.f27     // Catch:{ all -> 0x022e }
            r3.onTrackingRequestFailure(r0)     // Catch:{ all -> 0x022e }
        L_0x016e:
            java.lang.String r0 = "appsflyerConversionDataRequestRetries"
            int r0 = r12.getInt(r0, r7)     // Catch:{ all -> 0x022e }
            java.lang.String r3 = "appsflyerConversionDataCacheExpiration"
            r13 = 0
            long r3 = r12.getLong(r3, r13)     // Catch:{ all -> 0x022e }
            int r11 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r11 == 0) goto L_0x0199
            long r15 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x022e }
            r11 = 0
            long r15 = r15 - r3
            r3 = 5184000000(0x134fd9000, double:2.561236308E-314)
            int r11 = (r15 > r3 ? 1 : (r15 == r3 ? 0 : -1))
            if (r11 <= 0) goto L_0x0199
            java.lang.String r3 = "attributionId"
            m53(r5, r3, r9)     // Catch:{ all -> 0x022e }
            java.lang.String r3 = "appsflyerConversionDataCacheExpiration"
            m36(r5, r3, r13)     // Catch:{ all -> 0x022e }
        L_0x0199:
            java.lang.String r3 = "attributionId"
            java.lang.String r3 = r12.getString(r3, r9)     // Catch:{ all -> 0x022e }
            if (r3 != 0) goto L_0x01c5
            if (r2 == 0) goto L_0x01c5
            if (r8 == 0) goto L_0x01c5
            com.appsflyer.AppsFlyerConversionListener r3 = com.appsflyer.AppsFlyerLib.f22     // Catch:{ all -> 0x022e }
            if (r3 == 0) goto L_0x01c5
            r3 = 5
            if (r0 > r3) goto L_0x01c5
            com.appsflyer.AFExecutor r0 = com.appsflyer.AFExecutor.getInstance()     // Catch:{ all -> 0x022e }
            java.util.concurrent.ScheduledThreadPoolExecutor r0 = r0.m2()     // Catch:{ all -> 0x022e }
            com.appsflyer.AppsFlyerLib$b r3 = new com.appsflyer.AppsFlyerLib$b     // Catch:{ all -> 0x022e }
            android.content.Context r4 = r5.getApplicationContext()     // Catch:{ all -> 0x022e }
            r3.<init>(r4, r2, r0)     // Catch:{ all -> 0x022e }
            r4 = 10
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x022e }
            m66(r0, r3, r4, r2)     // Catch:{ all -> 0x022e }
            goto L_0x0211
        L_0x01c5:
            if (r2 != 0) goto L_0x01cd
            java.lang.String r0 = "AppsFlyer dev key is missing."
            com.appsflyer.AFLogger.afWarnLog(r0)     // Catch:{ all -> 0x022e }
            goto L_0x0211
        L_0x01cd:
            if (r8 == 0) goto L_0x0211
            com.appsflyer.AppsFlyerConversionListener r0 = com.appsflyer.AppsFlyerLib.f22     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x0211
            java.lang.String r0 = "attributionId"
            java.lang.String r0 = r12.getString(r0, r9)     // Catch:{ all -> 0x022e }
            if (r0 == 0) goto L_0x0211
            java.lang.String r0 = "appsFlyerCount"
            int r0 = m48(r12, r0, r7)     // Catch:{ all -> 0x022e }
            if (r0 <= r6) goto L_0x0211
            java.util.Map r0 = m33(r5)     // Catch:{ l -> 0x0209 }
            if (r0 == 0) goto L_0x0211
            java.lang.String r2 = "is_first_launch"
            boolean r2 = r0.containsKey(r2)     // Catch:{ Throwable -> 0x0200 }
            if (r2 != 0) goto L_0x01fa
            java.lang.String r2 = "is_first_launch"
            java.lang.String r3 = java.lang.Boolean.toString(r7)     // Catch:{ Throwable -> 0x0200 }
            r0.put(r2, r3)     // Catch:{ Throwable -> 0x0200 }
        L_0x01fa:
            com.appsflyer.AppsFlyerConversionListener r2 = com.appsflyer.AppsFlyerLib.f22     // Catch:{ Throwable -> 0x0200 }
            r2.onInstallConversionDataLoaded(r0)     // Catch:{ Throwable -> 0x0200 }
            goto L_0x0211
        L_0x0200:
            r0 = move-exception
            java.lang.String r2 = r0.getLocalizedMessage()     // Catch:{ l -> 0x0209 }
            com.appsflyer.AFLogger.afErrorLog(r2, r0)     // Catch:{ l -> 0x0209 }
            goto L_0x0211
        L_0x0209:
            r0 = move-exception
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x022e }
            com.appsflyer.AFLogger.afErrorLog(r2, r0)     // Catch:{ all -> 0x022e }
        L_0x0211:
            if (r10 == 0) goto L_0x0217
            r10.disconnect()
            return
        L_0x0217:
            return
        L_0x0218:
            r0 = move-exception
            r9 = r11
            goto L_0x021c
        L_0x021b:
            r0 = move-exception
        L_0x021c:
            if (r9 != 0) goto L_0x022a
            com.appsflyer.AppsFlyerTrackingRequestListener r2 = com.appsflyer.AppsFlyerLib.f27     // Catch:{ all -> 0x022e }
            if (r2 == 0) goto L_0x022d
            com.appsflyer.AppsFlyerTrackingRequestListener r2 = com.appsflyer.AppsFlyerLib.f27     // Catch:{ all -> 0x022e }
            java.lang.String r3 = "No Connectivity"
            r2.onTrackingRequestFailure(r3)     // Catch:{ all -> 0x022e }
            goto L_0x022d
        L_0x022a:
            r9.close()     // Catch:{ all -> 0x022e }
        L_0x022d:
            throw r0     // Catch:{ all -> 0x022e }
        L_0x022e:
            r0 = move-exception
            goto L_0x0232
        L_0x0230:
            r0 = move-exception
            r10 = r9
        L_0x0232:
            if (r10 == 0) goto L_0x0237
            r10.disconnect()
        L_0x0237:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.m26(java.net.URL, java.lang.String, java.lang.String, java.lang.ref.WeakReference, java.lang.String, boolean):void");
    }

    public void validateAndTrackInAppPurchase(Context context, String str, String str2, String str3, String str4, String str5, Map<String, String> map) {
        Context context2 = context;
        String str6 = str3;
        String str7 = str4;
        String str8 = str5;
        r r1 = r.m185();
        String[] strArr = new String[6];
        strArr[0] = str;
        strArr[1] = str2;
        strArr[2] = str6;
        strArr[3] = str7;
        strArr[4] = str8;
        strArr[5] = map == null ? "" : map.toString();
        r1.m193("validateAndTrackInAppPurchase", strArr);
        if (!isTrackingStopped()) {
            StringBuilder sb = new StringBuilder("Validate in app called with parameters: ");
            sb.append(str6);
            sb.append(" ");
            sb.append(str7);
            sb.append(" ");
            sb.append(str8);
            AFLogger.afInfoLog(sb.toString());
        }
        if (str != null && str7 != null && str2 != null && str8 != null && str6 != null) {
            ScheduledThreadPoolExecutor r11 = AFExecutor.getInstance().m2();
            m66(r11, new h(context.getApplicationContext(), AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY), str, str2, str3, str4, str5, map, r11, context2 instanceof Activity ? ((Activity) context2).getIntent() : null), 10, TimeUnit.MILLISECONDS);
        } else if (f19 != null) {
            f19.onValidateInAppFailure("Please provide purchase parameters");
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static void m66(ScheduledExecutorService scheduledExecutorService, Runnable runnable, long j, TimeUnit timeUnit) {
        if (scheduledExecutorService != null) {
            try {
                if (!scheduledExecutorService.isShutdown() && !scheduledExecutorService.isTerminated()) {
                    scheduledExecutorService.schedule(runnable, j, timeUnit);
                    return;
                }
            } catch (RejectedExecutionException e2) {
                AFLogger.afErrorLog("scheduleJob failed with RejectedExecutionException Exception", e2);
                return;
            } catch (Throwable th) {
                AFLogger.afErrorLog("scheduleJob failed with Exception", th);
                return;
            }
        }
        AFLogger.afWarnLog("scheduler is null, shut downed or terminated");
    }

    public void onHandleReferrer(Map<String, String> map) {
        this.f48 = map;
    }

    public boolean isTrackingStopped() {
        return this.f45;
    }

    class c implements Runnable {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f64;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f65;

        /* renamed from: ʽ  reason: contains not printable characters */
        private ExecutorService f66;

        /* renamed from: ˊ  reason: contains not printable characters */
        private String f67;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f68;

        /* renamed from: ˎ  reason: contains not printable characters */
        private WeakReference<Context> f69;

        /* renamed from: ˏ  reason: contains not printable characters */
        private final Intent f70;

        /* renamed from: ॱ  reason: contains not printable characters */
        private String f71;

        /* renamed from: ॱॱ  reason: contains not printable characters */
        private String f72;

        /* synthetic */ c(AppsFlyerLib appsFlyerLib, WeakReference weakReference, String str, String str2, String str3, String str4, ExecutorService executorService, boolean z, Intent intent, byte b) {
            this(weakReference, str, str2, str3, str4, executorService, z, intent);
        }

        /* JADX WARN: Type inference failed for: r7v0, types: [boolean, java.util.concurrent.ExecutorService] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private c(java.lang.ref.WeakReference<android.content.Context> r2, java.lang.String r3, java.lang.String r4, java.lang.String r5, java.lang.String r6, boolean r7, boolean r8, android.content.Intent r9) {
            /*
                r0 = this;
                com.appsflyer.AppsFlyerLib.this = r1
                r0.<init>()
                r0.f69 = r2
                r0.f68 = r3
                r0.f67 = r4
                r0.f71 = r5
                r0.f72 = r6
                r1 = 1
                r0.f64 = r1
                r0.f66 = r7
                r0.f65 = r8
                r0.f70 = r9
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.c.<init>(com.appsflyer.AppsFlyerLib, java.lang.ref.WeakReference, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.concurrent.ExecutorService, boolean, android.content.Intent):void");
        }

        public final void run() {
            AppsFlyerLib.m45(AppsFlyerLib.this, this.f69.get(), this.f68, this.f67, this.f71, this.f72, this.f64, this.f65, this.f70);
        }
    }

    class e implements Runnable {

        /* renamed from: ˊ  reason: contains not printable characters */
        private int f76;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f77;

        /* renamed from: ˎ  reason: contains not printable characters */
        private Map<String, Object> f78;

        /* renamed from: ˏ  reason: contains not printable characters */
        private WeakReference<Context> f79;

        /* renamed from: ॱ  reason: contains not printable characters */
        private boolean f80;

        /* synthetic */ e(AppsFlyerLib appsFlyerLib, String str, Map map, Context context, boolean z, int i, byte b) {
            this(str, map, context, z, i);
        }

        private e(String str, Map<String, Object> map, Context context, boolean z, int i) {
            this.f79 = null;
            this.f77 = str;
            this.f78 = map;
            this.f79 = new WeakReference<>(context);
            this.f80 = z;
            this.f76 = i;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x005f, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0060, code lost:
            r9 = r1;
            r1 = r0;
            r0 = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0064, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0065, code lost:
            com.appsflyer.AFLogger.afErrorLog(r0.getMessage(), r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x006c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0083, code lost:
            com.appsflyer.cache.CacheManager.getInstance().cacheRequest(new com.appsflyer.cache.RequestCacheData(r10.f77, r0, com.appsflyer.BuildConfig.VERSION_NAME), r10.f79.get());
            com.appsflyer.AFLogger.afErrorLog(r1.getMessage(), r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0064 A[ExcHandler: Throwable (r0v8 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:11:0x003c] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r10 = this;
                com.appsflyer.AppsFlyerLib r0 = com.appsflyer.AppsFlyerLib.this
                boolean r0 = r0.isTrackingStopped()
                if (r0 == 0) goto L_0x0009
                return
            L_0x0009:
                r0 = 0
                boolean r1 = r10.f80
                if (r1 == 0) goto L_0x0028
                int r1 = r10.f76
                r2 = 2
                if (r1 > r2) goto L_0x0028
                com.appsflyer.AppsFlyerLib r1 = com.appsflyer.AppsFlyerLib.this
                boolean r1 = com.appsflyer.AppsFlyerLib.m38(r1)
                if (r1 == 0) goto L_0x0028
                java.util.Map<java.lang.String, java.lang.Object> r1 = r10.f78
                java.lang.String r2 = "rfr"
                com.appsflyer.AppsFlyerLib r3 = com.appsflyer.AppsFlyerLib.this
                java.util.Map r3 = r3.f48
                r1.put(r2, r3)
            L_0x0028:
                java.util.Map<java.lang.String, java.lang.Object> r1 = r10.f78
                com.appsflyer.b$e r2 = new com.appsflyer.b$e
                java.util.Map<java.lang.String, java.lang.Object> r3 = r10.f78
                java.lang.ref.WeakReference<android.content.Context> r4 = r10.f79
                java.lang.Object r4 = r4.get()
                android.content.Context r4 = (android.content.Context) r4
                r2.<init>(r3, r4)
                r1.putAll(r2)
                java.util.Map<java.lang.String, java.lang.Object> r1 = r10.f78     // Catch:{ IOException -> 0x006d, Throwable -> 0x0064 }
                java.lang.String r2 = "appsflyerKey"
                java.lang.Object r1 = r1.get(r2)     // Catch:{ IOException -> 0x006d, Throwable -> 0x0064 }
                r5 = r1
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x006d, Throwable -> 0x0064 }
                java.util.Map<java.lang.String, java.lang.Object> r1 = r10.f78     // Catch:{ IOException -> 0x006d, Throwable -> 0x0064 }
                org.json.JSONObject r1 = com.appsflyer.AFHelper.convertToJsonObject(r1)     // Catch:{ IOException -> 0x006d, Throwable -> 0x0064 }
                java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x006d, Throwable -> 0x0064 }
                com.appsflyer.AppsFlyerLib r2 = com.appsflyer.AppsFlyerLib.this     // Catch:{ IOException -> 0x005f, Throwable -> 0x0064 }
                java.lang.String r3 = r10.f77     // Catch:{ IOException -> 0x005f, Throwable -> 0x0064 }
                java.lang.ref.WeakReference<android.content.Context> r6 = r10.f79     // Catch:{ IOException -> 0x005f, Throwable -> 0x0064 }
                r7 = 0
                boolean r8 = r10.f80     // Catch:{ IOException -> 0x005f, Throwable -> 0x0064 }
                r4 = r1
                com.appsflyer.AppsFlyerLib.m54(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ IOException -> 0x005f, Throwable -> 0x0064 }
                return
            L_0x005f:
                r0 = move-exception
                r9 = r1
                r1 = r0
                r0 = r9
                goto L_0x006e
            L_0x0064:
                r0 = move-exception
                java.lang.String r1 = r0.getMessage()
                com.appsflyer.AFLogger.afErrorLog(r1, r0)
                return
            L_0x006d:
                r1 = move-exception
            L_0x006e:
                java.lang.String r2 = "Exception while sending request to server. "
                com.appsflyer.AFLogger.afErrorLog(r2, r1)
                if (r0 == 0) goto L_0x00a2
                java.lang.ref.WeakReference<android.content.Context> r2 = r10.f79
                if (r2 == 0) goto L_0x00a2
                java.lang.String r2 = r10.f77
                java.lang.String r3 = "&isCachedRequest=true&timeincache="
                boolean r2 = r2.contains(r3)
                if (r2 != 0) goto L_0x00a2
                com.appsflyer.cache.CacheManager r2 = com.appsflyer.cache.CacheManager.getInstance()
                com.appsflyer.cache.RequestCacheData r3 = new com.appsflyer.cache.RequestCacheData
                java.lang.String r4 = r10.f77
                java.lang.String r5 = "4.8.18"
                r3.<init>(r4, r0, r5)
                java.lang.ref.WeakReference<android.content.Context> r0 = r10.f79
                java.lang.Object r0 = r0.get()
                android.content.Context r0 = (android.content.Context) r0
                r2.cacheRequest(r3, r0)
                java.lang.String r0 = r1.getMessage()
                com.appsflyer.AFLogger.afErrorLog(r0, r1)
            L_0x00a2:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.e.run():void");
        }
    }

    class b extends a {
        public b(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
            super(context, str, scheduledExecutorService);
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final String m85() {
            return ServerConfigHandler.getUrl("https://api.%s/install_data/v3/");
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final void m87(Map<String, String> map) {
            map.put("is_first_launch", Boolean.toString(true));
            AppsFlyerLib.f22.onInstallConversionDataLoaded(map);
            AppsFlyerLib.m52(this.f61.get(), "appsflyerConversionDataRequestRetries", 0);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m86(String str, int i) {
            AppsFlyerLib.f22.onInstallConversionFailure(str);
            if (i >= 400 && i < 500) {
                AppsFlyerLib.m52(this.f61.get(), "appsflyerConversionDataRequestRetries", AppsFlyerLib.m57(this.f61.get()).getInt("appsflyerConversionDataRequestRetries", 0) + 1);
            }
        }
    }

    abstract class a implements Runnable {

        /* renamed from: ˊ  reason: contains not printable characters */
        private ScheduledExecutorService f58;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f59;

        /* renamed from: ˎ  reason: contains not printable characters */
        private AtomicInteger f60 = new AtomicInteger(0);

        /* renamed from: ˏ  reason: contains not printable characters */
        WeakReference<Context> f61 = null;

        /* renamed from: ˊ  reason: contains not printable characters */
        public abstract String m82();

        /* access modifiers changed from: protected */
        /* renamed from: ˊ  reason: contains not printable characters */
        public abstract void m83(String str, int i);

        /* access modifiers changed from: protected */
        /* renamed from: ˎ  reason: contains not printable characters */
        public abstract void m84(Map<String, String> map);

        a(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
            this.f61 = new WeakReference<>(context);
            this.f59 = str;
            if (scheduledExecutorService == null) {
                this.f58 = AFExecutor.getInstance().m2();
            } else {
                this.f58 = scheduledExecutorService;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:74:0x0232 A[Catch:{ all -> 0x0228 }] */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x0247  */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x0257  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r11 = this;
                java.lang.String r0 = r11.f59
                if (r0 == 0) goto L_0x025b
                java.lang.String r0 = r11.f59
                int r0 = r0.length()
                if (r0 != 0) goto L_0x000e
                goto L_0x025b
            L_0x000e:
                com.appsflyer.AppsFlyerLib r0 = com.appsflyer.AppsFlyerLib.this
                boolean r0 = r0.isTrackingStopped()
                if (r0 == 0) goto L_0x0017
                return
            L_0x0017:
                java.util.concurrent.atomic.AtomicInteger r0 = r11.f60
                r0.incrementAndGet()
                r0 = 0
                r1 = 0
                java.lang.ref.WeakReference<android.content.Context> r2 = r11.f61     // Catch:{ Throwable -> 0x022b }
                java.lang.Object r2 = r2.get()     // Catch:{ Throwable -> 0x022b }
                android.content.Context r2 = (android.content.Context) r2     // Catch:{ Throwable -> 0x022b }
                if (r2 != 0) goto L_0x002e
                java.util.concurrent.atomic.AtomicInteger r0 = r11.f60
                r0.decrementAndGet()
                return
            L_0x002e:
                long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x022b }
                java.lang.ref.WeakReference r5 = new java.lang.ref.WeakReference     // Catch:{ Throwable -> 0x022b }
                r5.<init>(r2)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r5 = com.appsflyer.AppsFlyerLib.m40(r5)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r5 = com.appsflyer.AppsFlyerLib.m67(r2, r5)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = ""
                r7 = 1
                if (r5 == 0) goto L_0x006a
                java.util.List r8 = com.appsflyer.AppsFlyerLib.f23     // Catch:{ Throwable -> 0x022b }
                java.lang.String r9 = r5.toLowerCase()     // Catch:{ Throwable -> 0x022b }
                boolean r8 = r8.contains(r9)     // Catch:{ Throwable -> 0x022b }
                if (r8 != 0) goto L_0x005d
                java.lang.String r6 = "-"
                java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = r6.concat(r5)     // Catch:{ Throwable -> 0x022b }
                goto L_0x006a
            L_0x005d:
                java.lang.String r8 = "AF detected using redundant Google-Play channel for attribution - %s. Using without channel postfix."
                java.lang.Object[] r9 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x022b }
                r9[r1] = r5     // Catch:{ Throwable -> 0x022b }
                java.lang.String r5 = java.lang.String.format(r8, r9)     // Catch:{ Throwable -> 0x022b }
                com.appsflyer.AFLogger.afWarnLog(r5)     // Catch:{ Throwable -> 0x022b }
            L_0x006a:
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022b }
                r5.<init>()     // Catch:{ Throwable -> 0x022b }
                java.lang.String r8 = r11.m82()     // Catch:{ Throwable -> 0x022b }
                r5.append(r8)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r8 = r2.getPackageName()     // Catch:{ Throwable -> 0x022b }
                r5.append(r8)     // Catch:{ Throwable -> 0x022b }
                r5.append(r6)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = "?devkey="
                r5.append(r6)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = r11.f59     // Catch:{ Throwable -> 0x022b }
                r5.append(r6)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = "&device_id="
                r5.append(r6)     // Catch:{ Throwable -> 0x022b }
                java.lang.ref.WeakReference r6 = new java.lang.ref.WeakReference     // Catch:{ Throwable -> 0x022b }
                r6.<init>(r2)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = com.appsflyer.p.m174(r6)     // Catch:{ Throwable -> 0x022b }
                r5.append(r6)     // Catch:{ Throwable -> 0x022b }
                com.appsflyer.r r6 = com.appsflyer.r.m185()     // Catch:{ Throwable -> 0x022b }
                java.lang.String r8 = r5.toString()     // Catch:{ Throwable -> 0x022b }
                java.lang.String r9 = ""
                r6.m198(r8, r9)     // Catch:{ Throwable -> 0x022b }
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x022b }
                java.lang.String r8 = "Calling server for attribution url: "
                r6.<init>(r8)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r8 = r5.toString()     // Catch:{ Throwable -> 0x022b }
                r6.append(r8)     // Catch:{ Throwable -> 0x022b }
                java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x022b }
                com.appsflyer.j.AnonymousClass2.m152(r6)     // Catch:{ Throwable -> 0x022b }
                java.net.URL r6 = new java.net.URL     // Catch:{ Throwable -> 0x022b }
                java.lang.String r8 = r5.toString()     // Catch:{ Throwable -> 0x022b }
                r6.<init>(r8)     // Catch:{ Throwable -> 0x022b }
                java.net.URLConnection r6 = r6.openConnection()     // Catch:{ Throwable -> 0x022b }
                java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ Throwable -> 0x022b }
                java.lang.String r0 = "GET"
                r6.setRequestMethod(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r0 = 10000(0x2710, float:1.4013E-41)
                r6.setConnectTimeout(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r0 = "Connection"
                java.lang.String r8 = "close"
                r6.setRequestProperty(r0, r8)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r6.connect()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                int r0 = r6.getResponseCode()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r8 = com.appsflyer.AppsFlyerLib.m31(r6)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.r r9 = com.appsflyer.r.m185()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r10 = r5.toString()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r9.m195(r10, r0, r8)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r9 = 200(0xc8, float:2.8E-43)
                if (r0 != r9) goto L_0x01ec
                long r9 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r0 = "appsflyerGetConversionDataTiming"
                r5 = 0
                long r9 = r9 - r3
                r3 = 1000(0x3e8, double:4.94E-321)
                long r9 = r9 / r3
                com.appsflyer.AppsFlyerLib.m36(r2, r0, r9)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r0 = "Attribution data: "
                java.lang.String r3 = java.lang.String.valueOf(r8)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r0 = r0.concat(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.j.AnonymousClass2.m152(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                int r0 = r8.length()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r0 <= 0) goto L_0x0218
                if (r2 == 0) goto L_0x0218
                java.util.Map r0 = com.appsflyer.AppsFlyerLib.m34(r8)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r3 = "iscache"
                java.lang.Object r3 = r0.get(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r3 = (java.lang.String) r3     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r3 == 0) goto L_0x013b
                java.lang.String r4 = java.lang.Boolean.toString(r1)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                boolean r4 = r4.equals(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r4 == 0) goto L_0x013b
                java.lang.String r4 = "appsflyerConversionDataCacheExpiration"
                long r9 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.AppsFlyerLib.m36(r2, r4, r9)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x013b:
                java.lang.String r4 = "af_siteid"
                boolean r4 = r0.containsKey(r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r4 == 0) goto L_0x0178
                java.lang.String r4 = "af_channel"
                boolean r4 = r0.containsKey(r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r4 == 0) goto L_0x0165
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = "[Invite] Detected App-Invite via channel: "
                r4.<init>(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = "af_channel"
                java.lang.Object r5 = r0.get(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r4.append(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                goto L_0x0178
            L_0x0165:
                java.lang.String r4 = "[CrossPromotion] App was installed via %s's Cross Promotion"
                java.lang.Object[] r5 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r9 = "af_siteid"
                java.lang.Object r9 = r0.get(r9)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r5[r1] = r9     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r4 = java.lang.String.format(r4, r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x0178:
                java.lang.String r4 = "af_siteid"
                boolean r4 = r0.containsKey(r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r4 == 0) goto L_0x0199
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = "[Invite] Detected App-Invite via channel: "
                r4.<init>(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = "af_channel"
                java.lang.Object r5 = r0.get(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r4.append(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x0199:
                java.lang.String r4 = "is_first_launch"
                java.lang.String r5 = java.lang.Boolean.toString(r1)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r0.put(r4, r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r4.<init>(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r4 == 0) goto L_0x01b3
                java.lang.String r5 = "attributionId"
                com.appsflyer.AppsFlyerLib.m53(r2, r5, r4)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                goto L_0x01b8
            L_0x01b3:
                java.lang.String r4 = "attributionId"
                com.appsflyer.AppsFlyerLib.m53(r2, r4, r8)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x01b8:
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r5 = "iscache="
                r4.<init>(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r4.append(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r3 = " caching conversion data"
                r4.append(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r3 = r4.toString()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.AFLogger.afDebugLog(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.AppsFlyerConversionListener r3 = com.appsflyer.AppsFlyerLib.f22     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r3 == 0) goto L_0x0218
                java.util.concurrent.atomic.AtomicInteger r3 = r11.f60     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                int r3 = r3.intValue()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r3 > r7) goto L_0x0218
                java.util.Map r2 = com.appsflyer.AppsFlyerLib.m33(r2)     // Catch:{ l -> 0x01e2 }
                r0 = r2
                goto L_0x01e8
            L_0x01e2:
                r2 = move-exception
                java.lang.String r3 = "Exception while trying to fetch attribution data. "
                com.appsflyer.AFLogger.afErrorLog(r3, r2)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x01e8:
                r11.m84(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                goto L_0x0218
            L_0x01ec:
                com.appsflyer.AppsFlyerConversionListener r2 = com.appsflyer.AppsFlyerLib.f22     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                if (r2 == 0) goto L_0x01ff
                java.lang.String r2 = "Error connection to server: "
                java.lang.String r3 = java.lang.String.valueOf(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r2 = r2.concat(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r11.m83(r2, r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x01ff:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r3 = "AttributionIdFetcher response code: "
                r2.<init>(r3)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r2.append(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r0 = "  url: "
                r2.append(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                r2.append(r5)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                java.lang.String r0 = r2.toString()     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
                com.appsflyer.j.AnonymousClass2.m152(r0)     // Catch:{ Throwable -> 0x0225, all -> 0x0223 }
            L_0x0218:
                java.util.concurrent.atomic.AtomicInteger r0 = r11.f60
                r0.decrementAndGet()
                if (r6 == 0) goto L_0x024a
                r6.disconnect()
                goto L_0x024a
            L_0x0223:
                r1 = move-exception
                goto L_0x0250
            L_0x0225:
                r2 = move-exception
                r0 = r6
                goto L_0x022c
            L_0x0228:
                r1 = move-exception
                r6 = r0
                goto L_0x0250
            L_0x022b:
                r2 = move-exception
            L_0x022c:
                com.appsflyer.AppsFlyerConversionListener r3 = com.appsflyer.AppsFlyerLib.f22     // Catch:{ all -> 0x0228 }
                if (r3 == 0) goto L_0x0239
                java.lang.String r3 = r2.getMessage()     // Catch:{ all -> 0x0228 }
                r11.m83(r3, r1)     // Catch:{ all -> 0x0228 }
            L_0x0239:
                java.lang.String r1 = r2.getMessage()     // Catch:{ all -> 0x0228 }
                com.appsflyer.AFLogger.afErrorLog(r1, r2)     // Catch:{ all -> 0x0228 }
                java.util.concurrent.atomic.AtomicInteger r1 = r11.f60
                r1.decrementAndGet()
                if (r0 == 0) goto L_0x024a
                r0.disconnect()
            L_0x024a:
                java.util.concurrent.ScheduledExecutorService r0 = r11.f58
                r0.shutdown()
                return
            L_0x0250:
                java.util.concurrent.atomic.AtomicInteger r0 = r11.f60
                r0.decrementAndGet()
                if (r6 == 0) goto L_0x025a
                r6.disconnect()
            L_0x025a:
                throw r1
            L_0x025b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.a.run():void");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005d, code lost:
        if (r3 != null) goto L_0x002c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[SYNTHETIC, Splitter:B:27:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0086 A[SYNTHETIC, Splitter:B:45:0x0086] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x008b A[Catch:{ Throwable -> 0x008e }] */
    @android.support.annotation.NonNull
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String m31(java.net.HttpURLConnection r7) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 0
            java.io.InputStream r2 = r7.getErrorStream()     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            if (r2 != 0) goto L_0x0010
            java.io.InputStream r2 = r7.getInputStream()     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
        L_0x0010:
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0038 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0038 }
        L_0x001a:
            java.lang.String r1 = r2.readLine()     // Catch:{ Throwable -> 0x0033, all -> 0x0030 }
            if (r1 == 0) goto L_0x0029
            r0.append(r1)     // Catch:{ Throwable -> 0x0033, all -> 0x0030 }
            r1 = 10
            r0.append(r1)     // Catch:{ Throwable -> 0x0033, all -> 0x0030 }
            goto L_0x001a
        L_0x0029:
            r2.close()     // Catch:{ Throwable -> 0x0060 }
        L_0x002c:
            r3.close()     // Catch:{ Throwable -> 0x0060 }
            goto L_0x0060
        L_0x0030:
            r7 = move-exception
            r1 = r2
            goto L_0x0084
        L_0x0033:
            r1 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x003f
        L_0x0038:
            r2 = move-exception
            goto L_0x003f
        L_0x003a:
            r7 = move-exception
            r3 = r1
            goto L_0x0084
        L_0x003d:
            r2 = move-exception
            r3 = r1
        L_0x003f:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0083 }
            java.lang.String r5 = "Could not read connection response from: "
            r4.<init>(r5)     // Catch:{ all -> 0x0083 }
            java.net.URL r7 = r7.getURL()     // Catch:{ all -> 0x0083 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0083 }
            r4.append(r7)     // Catch:{ all -> 0x0083 }
            java.lang.String r7 = r4.toString()     // Catch:{ all -> 0x0083 }
            com.appsflyer.AFLogger.afErrorLog(r7, r2)     // Catch:{ all -> 0x0083 }
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ Throwable -> 0x0060 }
        L_0x005d:
            if (r3 == 0) goto L_0x0060
            goto L_0x002c
        L_0x0060:
            java.lang.String r7 = r0.toString()
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x006a }
            r0.<init>(r7)     // Catch:{ JSONException -> 0x006a }
            return r7
        L_0x006a:
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.lang.String r1 = "string_response"
            r0.put(r1, r7)     // Catch:{ JSONException -> 0x0079 }
            java.lang.String r7 = r0.toString()     // Catch:{ JSONException -> 0x0079 }
            return r7
        L_0x0079:
            org.json.JSONObject r7 = new org.json.JSONObject
            r7.<init>()
            java.lang.String r7 = r7.toString()
            return r7
        L_0x0083:
            r7 = move-exception
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.close()     // Catch:{ Throwable -> 0x008e }
        L_0x0089:
            if (r3 == 0) goto L_0x008e
            r3.close()     // Catch:{ Throwable -> 0x008e }
        L_0x008e:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.m31(java.net.HttpURLConnection):java.lang.String");
    }

    class d implements Runnable {

        /* renamed from: ˋ  reason: contains not printable characters */
        private WeakReference<Context> f74 = null;

        public d(Context context) {
            this.f74 = new WeakReference<>(context);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.appsflyer.AppsFlyerLib.ˊ(com.appsflyer.AppsFlyerLib, boolean):boolean
         arg types: [com.appsflyer.AppsFlyerLib, int]
         candidates:
          com.appsflyer.AppsFlyerLib.ˊ(android.content.Context, java.lang.String):java.lang.String
          com.appsflyer.AppsFlyerLib.ˊ(android.content.Context, java.util.Map<java.lang.String, ? super java.lang.String>):void
          com.appsflyer.AppsFlyerLib.ˊ(com.appsflyer.AppsFlyerLib, boolean):boolean */
        public final void run() {
            if (!AppsFlyerLib.this.f29) {
                long unused = AppsFlyerLib.this.f50 = System.currentTimeMillis();
                if (this.f74 != null) {
                    boolean unused2 = AppsFlyerLib.this.f29 = true;
                    try {
                        String r1 = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
                        synchronized (this.f74) {
                            for (RequestCacheData next : CacheManager.getInstance().getCachedRequests(this.f74.get())) {
                                StringBuilder sb = new StringBuilder("resending request: ");
                                sb.append(next.getRequestURL());
                                AFLogger.afInfoLog(sb.toString());
                                try {
                                    long currentTimeMillis = System.currentTimeMillis();
                                    long parseLong = Long.parseLong(next.getCacheKey(), 10);
                                    AppsFlyerLib appsFlyerLib = AppsFlyerLib.this;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(next.getRequestURL());
                                    sb2.append("&isCachedRequest=true&timeincache=");
                                    sb2.append(Long.toString((currentTimeMillis - parseLong) / 1000));
                                    AppsFlyerLib.m54(appsFlyerLib, sb2.toString(), next.getPostData(), r1, this.f74, next.getCacheKey(), false);
                                } catch (Exception e) {
                                    AFLogger.afErrorLog("Failed to resend cached request", e);
                                }
                            }
                        }
                    } catch (Exception e2) {
                        try {
                            AFLogger.afErrorLog("failed to check cache. ", e2);
                        } catch (Throwable th) {
                            boolean unused3 = AppsFlyerLib.this.f29 = false;
                            throw th;
                        }
                    }
                    boolean unused4 = AppsFlyerLib.this.f29 = false;
                    AppsFlyerLib.this.f32.shutdown();
                    ScheduledExecutorService unused5 = AppsFlyerLib.this.f32 = null;
                }
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static float m17(Context context) {
        try {
            Intent registerReceiver = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int intExtra = registerReceiver.getIntExtra("level", -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra == -1 || intExtra2 == -1) {
                return 50.0f;
            }
            return (((float) intExtra) / ((float) intExtra2)) * 100.0f;
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return 1.0f;
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private static boolean m69(Context context) {
        if (context != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                    for (Network networkCapabilities : connectivityManager.getAllNetworks()) {
                        NetworkCapabilities networkCapabilities2 = connectivityManager.getNetworkCapabilities(networkCapabilities);
                        if (networkCapabilities2.hasTransport(4) && !networkCapabilities2.hasCapability(15)) {
                            return true;
                        }
                    }
                    return false;
                } catch (Exception e2) {
                    AFLogger.afErrorLog("Failed collecting ivc data", e2);
                }
            } else if (Build.VERSION.SDK_INT >= 16) {
                ArrayList arrayList = new ArrayList();
                try {
                    Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
                    while (it.hasNext()) {
                        NetworkInterface networkInterface = (NetworkInterface) it.next();
                        if (networkInterface.isUp()) {
                            arrayList.add(networkInterface.getName());
                        }
                    }
                    return arrayList.contains("tun0");
                } catch (Exception e3) {
                    AFLogger.afErrorLog("Failed collecting ivc data", e3);
                }
            }
        }
        return false;
    }

    public void setLogLevel(AFLogger.LogLevel logLevel) {
        AppsFlyerProperties.getInstance().set("logLevel", logLevel.getLevel());
    }

    public void setHostName(String str) {
        AppsFlyerProperties.getInstance().set("custom_host", str);
    }

    public String getHost() {
        String string = AppsFlyerProperties.getInstance().getString("custom_host");
        return string != null ? string : ServerParameters.DEFAULT_HOST;
    }

    public void setMinTimeBetweenSessions(int i) {
        this.f41 = TimeUnit.SECONDS.toMillis((long) i);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public static void m53(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
        edit.putString(str, str2);
        if (Build.VERSION.SDK_INT >= 9) {
            edit.apply();
        } else {
            edit.commit();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public static void m52(Context context, String str, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
        edit.putInt(str, i);
        if (Build.VERSION.SDK_INT >= 9) {
            edit.apply();
        } else {
            edit.commit();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public static void m36(Context context, String str, long j) {
        SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
        edit.putLong(str, j);
        if (Build.VERSION.SDK_INT >= 9) {
            edit.apply();
        } else {
            edit.commit();
        }
    }
}
