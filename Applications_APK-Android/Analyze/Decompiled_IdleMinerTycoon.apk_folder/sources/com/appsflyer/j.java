package com.appsflyer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class j {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final BitSet f150 = new BitSet(6);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile j f151;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private static final Handler f152 = new Handler(Looper.getMainLooper());

    /* renamed from: ʽ  reason: contains not printable characters */
    final Runnable f153 = new Runnable() {
        public final void run() {
            synchronized (j.this.f156) {
                if (j.this.f159) {
                    j.this.f158.removeCallbacks(j.this.f161);
                    j.this.f158.removeCallbacks(j.this.f154);
                    j.this.m149();
                    j.this.f159 = false;
                }
            }
        }
    };

    /* renamed from: ˊ  reason: contains not printable characters */
    final Runnable f154 = new AnonymousClass1();

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private final Map<f, Map<String, Object>> f155 = new HashMap(f150.size());

    /* renamed from: ˋ  reason: contains not printable characters */
    final Object f156 = new Object();

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private boolean f157;

    /* renamed from: ˎ  reason: contains not printable characters */
    final Handler f158;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f159;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private final SensorManager f160;

    /* renamed from: ॱ  reason: contains not printable characters */
    final Runnable f161 = new AnonymousClass2();

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Map<f, f> f162 = new HashMap(f150.size());

    static {
        f150.set(1);
        f150.set(2);
        f150.set(4);
    }

    /* renamed from: com.appsflyer.j$1  reason: invalid class name */
    class AnonymousClass1 implements Runnable {
        AnonymousClass1() {
        }

        public final void run() {
            synchronized (j.this.f156) {
                j.this.m149();
                j.this.f158.postDelayed(j.this.f161, TapjoyConstants.SESSION_ID_INACTIVITY_TIME);
            }
        }

        AnonymousClass1() {
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        static boolean m151(Context context, String str) {
            int checkSelfPermission = ContextCompat.checkSelfPermission(context, str);
            StringBuilder sb = new StringBuilder("is Permission Available: ");
            sb.append(str);
            sb.append("; res: ");
            sb.append(checkSelfPermission);
            AFLogger.afRDLog(sb.toString());
            return checkSelfPermission == 0;
        }
    }

    /* renamed from: com.appsflyer.j$2  reason: invalid class name */
    class AnonymousClass2 implements Runnable {

        /* renamed from: ˋ  reason: contains not printable characters */
        private static String f164;

        /* renamed from: ˏ  reason: contains not printable characters */
        private static String f165;

        AnonymousClass2() {
        }

        public final void run() {
            synchronized (j.this.f156) {
                j.this.m150();
                j.this.f158.postDelayed(j.this.f154, 500);
                j.this.f159 = true;
            }
        }

        AnonymousClass2() {
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        static void m153(String str) {
            f164 = str;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                if (i == 0 || i == str.length() - 1) {
                    sb.append(str.charAt(i));
                } else {
                    sb.append("*");
                }
            }
            f165 = sb.toString();
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        static void m152(String str) {
            if (f164 == null) {
                m153(AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY));
            }
            if (f164 != null && str.contains(f164)) {
                AFLogger.afInfoLog(str.replace(f164, f165));
            }
        }
    }

    private j(@NonNull SensorManager sensorManager, Handler handler) {
        this.f160 = sensorManager;
        this.f158 = handler;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static j m147(Context context) {
        return m146((SensorManager) context.getApplicationContext().getSystemService("sensor"), f152);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static j m146(SensorManager sensorManager, Handler handler) {
        if (f151 == null) {
            synchronized (j.class) {
                if (f151 == null) {
                    f151 = new j(sensorManager, handler);
                }
            }
        }
        return f151;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m150() {
        try {
            for (Sensor next : this.f160.getSensorList(-1)) {
                int type = next.getType();
                if (type >= 0 && f150.get(type)) {
                    f r3 = f.m125(next);
                    if (!this.f162.containsKey(r3)) {
                        this.f162.put(r3, r3);
                    }
                    this.f160.registerListener(this.f162.get(r3), next, 0);
                }
            }
        } catch (Throwable unused) {
        }
        this.f157 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m149() {
        try {
            if (!this.f162.isEmpty()) {
                for (f next : this.f162.values()) {
                    this.f160.unregisterListener(next);
                    next.m129(this.f155);
                }
            }
        } catch (Throwable unused) {
        }
        this.f157 = false;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: ˊ  reason: contains not printable characters */
    public final List<Map<String, Object>> m148() {
        synchronized (this.f156) {
            if (!this.f162.isEmpty() && this.f157) {
                for (f r2 : this.f162.values()) {
                    r2.m128(this.f155);
                }
            }
            if (this.f155.isEmpty()) {
                List<Map<String, Object>> emptyList = Collections.emptyList();
                return emptyList;
            }
            ArrayList arrayList = new ArrayList(this.f155.values());
            return arrayList;
        }
    }
}
