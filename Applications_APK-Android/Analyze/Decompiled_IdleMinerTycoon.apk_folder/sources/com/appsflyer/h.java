package com.appsflyer;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

final class h implements Runnable {

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private static String f133;

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f134;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public Map<String, String> f135;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ScheduledExecutorService f136;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f137;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f138;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public WeakReference<Context> f139 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String f140;

    /* renamed from: ͺ  reason: contains not printable characters */
    private final Intent f141;

    /* renamed from: ॱ  reason: contains not printable characters */
    private String f142;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String f143;

    static {
        StringBuilder sb = new StringBuilder("https://validate.%s/api/v");
        sb.append(AppsFlyerLib.f20);
        sb.append("/androidevent?buildnumber=4.8.18&app_id=");
        f133 = sb.toString();
    }

    h(Context context, String str, String str2, String str3, String str4, String str5, String str6, Map<String, String> map, ScheduledExecutorService scheduledExecutorService, Intent intent) {
        this.f139 = new WeakReference<>(context);
        this.f140 = str;
        this.f137 = str2;
        this.f142 = str4;
        this.f143 = str5;
        this.f134 = str6;
        this.f135 = map;
        this.f138 = str3;
        this.f136 = scheduledExecutorService;
        this.f141 = intent;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x011e A[Catch:{ all -> 0x0117 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r10 = this;
            java.lang.String r0 = r10.f140
            if (r0 == 0) goto L_0x0144
            java.lang.String r0 = r10.f140
            int r0 = r0.length()
            if (r0 != 0) goto L_0x000e
            goto L_0x0144
        L_0x000e:
            com.appsflyer.AppsFlyerLib r0 = com.appsflyer.AppsFlyerLib.getInstance()
            boolean r0 = r0.isTrackingStopped()
            if (r0 == 0) goto L_0x0019
            return
        L_0x0019:
            r0 = 0
            r1 = 0
            java.lang.ref.WeakReference<android.content.Context> r2 = r10.f139     // Catch:{ Throwable -> 0x0119 }
            java.lang.Object r2 = r2.get()     // Catch:{ Throwable -> 0x0119 }
            android.content.Context r2 = (android.content.Context) r2     // Catch:{ Throwable -> 0x0119 }
            if (r2 != 0) goto L_0x0026
            return
        L_0x0026:
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Throwable -> 0x0119 }
            r3.<init>()     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r4 = "public-key"
            java.lang.String r5 = r10.f137     // Catch:{ Throwable -> 0x0119 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r4 = "sig-data"
            java.lang.String r5 = r10.f142     // Catch:{ Throwable -> 0x0119 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r4 = "signature"
            java.lang.String r5 = r10.f138     // Catch:{ Throwable -> 0x0119 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x0119 }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Throwable -> 0x0119 }
            r4.<init>()     // Catch:{ Throwable -> 0x0119 }
            r4.putAll(r3)     // Catch:{ Throwable -> 0x0119 }
            java.util.concurrent.ScheduledExecutorService r5 = java.util.concurrent.Executors.newSingleThreadScheduledExecutor()     // Catch:{ Throwable -> 0x0119 }
            com.appsflyer.h$1 r6 = new com.appsflyer.h$1     // Catch:{ Throwable -> 0x0119 }
            r6.<init>(r4)     // Catch:{ Throwable -> 0x0119 }
            r7 = 5
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ Throwable -> 0x0119 }
            r5.schedule(r6, r7, r4)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r4 = "dev_key"
            java.lang.String r5 = r10.f140     // Catch:{ Throwable -> 0x0119 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r4 = "app_id"
            java.lang.String r5 = r2.getPackageName()     // Catch:{ Throwable -> 0x0119 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r4 = "uid"
            com.appsflyer.AppsFlyerLib r5 = com.appsflyer.AppsFlyerLib.getInstance()     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r2 = r5.getAppsFlyerUID(r2)     // Catch:{ Throwable -> 0x0119 }
            r3.put(r4, r2)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r2 = "advertiserId"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r5 = "advertiserId"
            java.lang.String r4 = r4.getString(r5)     // Catch:{ Throwable -> 0x0119 }
            r3.put(r2, r4)     // Catch:{ Throwable -> 0x0119 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0119 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0119 }
            java.lang.String r3 = "https://sdk-services.%s/validate-android-signature"
            java.lang.String r3 = com.appsflyer.ServerConfigHandler.getUrl(r3)     // Catch:{ Throwable -> 0x0119 }
            com.appsflyer.r r4 = com.appsflyer.r.m185()     // Catch:{ Throwable -> 0x0119 }
            r4.m198(r3, r2)     // Catch:{ Throwable -> 0x0119 }
            java.net.HttpURLConnection r2 = m140(r2, r3)     // Catch:{ Throwable -> 0x0119 }
            r0 = -1
            if (r2 == 0) goto L_0x00b0
            int r0 = r2.getResponseCode()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            goto L_0x00b0
        L_0x00a6:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x013e
        L_0x00ab:
            r0 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x011a
        L_0x00b0:
            com.appsflyer.AppsFlyerLib.getInstance()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r4 = com.appsflyer.AppsFlyerLib.m31(r2)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            com.appsflyer.r r5 = com.appsflyer.r.m185()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r5.m195(r3, r0, r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r4 = "code"
            r3.put(r4, r0)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r0 != r4) goto L_0x00ff
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r4 = "Validate response 200 ok: "
            r0.<init>(r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r4 = r3.toString()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r0.append(r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            com.appsflyer.AFLogger.afInfoLog(r0)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r0 = "result"
            boolean r0 = r3.optBoolean(r0)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            if (r0 == 0) goto L_0x00f0
            java.lang.String r0 = "result"
            boolean r0 = r3.getBoolean(r0)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            goto L_0x00f1
        L_0x00f0:
            r0 = 0
        L_0x00f1:
            java.lang.String r4 = r10.f142     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r5 = r10.f143     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r6 = r10.f134     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            m137(r0, r4, r5, r6, r3)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            goto L_0x0111
        L_0x00ff:
            java.lang.String r0 = "Failed Validate request"
            com.appsflyer.AFLogger.afInfoLog(r0)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r0 = r10.f142     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r4 = r10.f143     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r5 = r10.f134     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            m137(r1, r0, r4, r5, r3)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
        L_0x0111:
            if (r2 == 0) goto L_0x013d
            r2.disconnect()
            return
        L_0x0117:
            r1 = move-exception
            goto L_0x013e
        L_0x0119:
            r2 = move-exception
        L_0x011a:
            com.appsflyer.AppsFlyerInAppPurchaseValidatorListener r3 = com.appsflyer.AppsFlyerLib.f19     // Catch:{ all -> 0x0117 }
            if (r3 == 0) goto L_0x0130
            java.lang.String r3 = "Failed Validate request + ex"
            com.appsflyer.AFLogger.afErrorLog(r3, r2)     // Catch:{ all -> 0x0117 }
            java.lang.String r3 = r10.f142     // Catch:{ all -> 0x0117 }
            java.lang.String r4 = r10.f143     // Catch:{ all -> 0x0117 }
            java.lang.String r5 = r10.f134     // Catch:{ all -> 0x0117 }
            java.lang.String r6 = r2.getMessage()     // Catch:{ all -> 0x0117 }
            m137(r1, r3, r4, r5, r6)     // Catch:{ all -> 0x0117 }
        L_0x0130:
            java.lang.String r1 = r2.getMessage()     // Catch:{ all -> 0x0117 }
            com.appsflyer.AFLogger.afErrorLog(r1, r2)     // Catch:{ all -> 0x0117 }
            if (r0 == 0) goto L_0x013d
            r0.disconnect()
            return
        L_0x013d:
            return
        L_0x013e:
            if (r0 == 0) goto L_0x0143
            r0.disconnect()
        L_0x0143:
            throw r1
        L_0x0144:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.h.run():void");
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static HttpURLConnection m140(String str, String str2) throws IOException {
        try {
            m mVar = new m(null, AppsFlyerLib.getInstance().isTrackingStopped());
            mVar.f182 = str;
            mVar.m157();
            if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                StringBuilder sb = new StringBuilder("Main thread detected. Calling ");
                sb.append(str2);
                sb.append(" in a new thread.");
                AFLogger.afDebugLog(sb.toString());
                mVar.execute(str2);
            } else {
                StringBuilder sb2 = new StringBuilder("Calling ");
                sb2.append(str2);
                sb2.append(" (on current thread: ");
                sb2.append(Thread.currentThread().toString());
                sb2.append(" )");
                AFLogger.afDebugLog(sb2.toString());
                mVar.onPreExecute();
                mVar.onPostExecute(mVar.doInBackground(str2));
            }
            return mVar.m160();
        } catch (Throwable th) {
            AFLogger.afErrorLog("Could not send callStats request", th);
            return null;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static void m137(boolean z, String str, String str2, String str3, String str4) {
        if (AppsFlyerLib.f19 != null) {
            StringBuilder sb = new StringBuilder("Validate callback parameters: ");
            sb.append(str);
            sb.append(" ");
            sb.append(str2);
            sb.append(" ");
            sb.append(str3);
            AFLogger.afDebugLog(sb.toString());
            if (z) {
                AFLogger.afDebugLog("Validate in app purchase success: ".concat(String.valueOf(str4)));
                AppsFlyerLib.f19.onValidateInApp();
                return;
            }
            AFLogger.afDebugLog("Validate in app purchase failed: ".concat(String.valueOf(str4)));
            AppsFlyerInAppPurchaseValidatorListener appsFlyerInAppPurchaseValidatorListener = AppsFlyerLib.f19;
            if (str4 == null) {
                str4 = "Failed validating";
            }
            appsFlyerInAppPurchaseValidatorListener.onValidateInAppFailure(str4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLib.ˎ(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, android.content.SharedPreferences, boolean, android.content.Intent):java.util.Map<java.lang.String, java.lang.Object>
     arg types: [android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.SharedPreferences, int, android.content.Intent]
     candidates:
      com.appsflyer.AppsFlyerLib.ˎ(com.appsflyer.AppsFlyerLib, android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, android.content.Intent):void
      com.appsflyer.AppsFlyerLib.ˎ(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, android.content.SharedPreferences, boolean, android.content.Intent):java.util.Map<java.lang.String, java.lang.Object> */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* renamed from: ˏ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void m138(com.appsflyer.h r15, java.util.Map r16, java.util.Map r17, java.lang.ref.WeakReference r18) {
        /*
            r0 = r15
            java.lang.Object r1 = r18.get()
            if (r1 == 0) goto L_0x0156
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = com.appsflyer.h.f133
            java.lang.String r2 = com.appsflyer.ServerConfigHandler.getUrl(r2)
            r1.append(r2)
            java.lang.Object r2 = r18.get()
            android.content.Context r2 = (android.content.Context) r2
            java.lang.String r2 = r2.getPackageName()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.Object r2 = r18.get()
            android.content.Context r2 = (android.content.Context) r2
            java.lang.String r3 = "appsflyer-data"
            r4 = 0
            android.content.SharedPreferences r12 = r2.getSharedPreferences(r3, r4)
            java.lang.String r2 = "referrer"
            r3 = 0
            java.lang.String r2 = r12.getString(r2, r3)
            if (r2 != 0) goto L_0x003e
            java.lang.String r2 = ""
        L_0x003e:
            r10 = r2
            com.appsflyer.AppsFlyerLib r5 = com.appsflyer.AppsFlyerLib.getInstance()
            java.lang.Object r2 = r18.get()
            r6 = r2
            android.content.Context r6 = (android.content.Context) r6
            java.lang.String r7 = r0.f140
            java.lang.String r8 = "af_purchase"
            java.lang.String r9 = ""
            r11 = 1
            r13 = 0
            android.content.Intent r14 = r0.f141
            java.util.Map r2 = r5.m72(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            java.lang.String r4 = "price"
            java.lang.String r5 = r0.f143
            r2.put(r4, r5)
            java.lang.String r4 = "currency"
            java.lang.String r0 = r0.f134
            r2.put(r4, r0)
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>(r2)
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.util.Set r2 = r16.entrySet()     // Catch:{ JSONException -> 0x0098 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ JSONException -> 0x0098 }
        L_0x0078:
            boolean r5 = r2.hasNext()     // Catch:{ JSONException -> 0x0098 }
            if (r5 == 0) goto L_0x0092
            java.lang.Object r5 = r2.next()     // Catch:{ JSONException -> 0x0098 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ JSONException -> 0x0098 }
            java.lang.Object r6 = r5.getKey()     // Catch:{ JSONException -> 0x0098 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ JSONException -> 0x0098 }
            java.lang.Object r5 = r5.getValue()     // Catch:{ JSONException -> 0x0098 }
            r0.put(r6, r5)     // Catch:{ JSONException -> 0x0098 }
            goto L_0x0078
        L_0x0092:
            java.lang.String r2 = "receipt_data"
            r4.put(r2, r0)     // Catch:{ JSONException -> 0x0098 }
            goto L_0x009e
        L_0x0098:
            r0 = move-exception
            java.lang.String r2 = "Failed to build 'receipt_data'"
            com.appsflyer.AFLogger.afErrorLog(r2, r0)
        L_0x009e:
            if (r17 == 0) goto L_0x00d3
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.util.Set r2 = r17.entrySet()     // Catch:{ JSONException -> 0x00cd }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ JSONException -> 0x00cd }
        L_0x00ad:
            boolean r5 = r2.hasNext()     // Catch:{ JSONException -> 0x00cd }
            if (r5 == 0) goto L_0x00c7
            java.lang.Object r5 = r2.next()     // Catch:{ JSONException -> 0x00cd }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ JSONException -> 0x00cd }
            java.lang.Object r6 = r5.getKey()     // Catch:{ JSONException -> 0x00cd }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ JSONException -> 0x00cd }
            java.lang.Object r5 = r5.getValue()     // Catch:{ JSONException -> 0x00cd }
            r0.put(r6, r5)     // Catch:{ JSONException -> 0x00cd }
            goto L_0x00ad
        L_0x00c7:
            java.lang.String r2 = "extra_prms"
            r4.put(r2, r0)     // Catch:{ JSONException -> 0x00cd }
            goto L_0x00d3
        L_0x00cd:
            r0 = move-exception
            java.lang.String r2 = "Failed to build 'extra_prms'"
            com.appsflyer.AFLogger.afErrorLog(r2, r0)
        L_0x00d3:
            java.lang.String r0 = r4.toString()
            com.appsflyer.r r2 = com.appsflyer.r.m185()
            r2.m198(r1, r0)
            java.net.HttpURLConnection r2 = m140(r0, r1)     // Catch:{ Throwable -> 0x0142 }
            r0 = -1
            if (r2 == 0) goto L_0x00ef
            int r0 = r2.getResponseCode()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            goto L_0x00ef
        L_0x00ea:
            r0 = move-exception
            goto L_0x0150
        L_0x00ec:
            r0 = move-exception
            r3 = r2
            goto L_0x0143
        L_0x00ef:
            com.appsflyer.AppsFlyerLib.getInstance()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r3 = com.appsflyer.AppsFlyerLib.m31(r2)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            com.appsflyer.r r4 = com.appsflyer.r.m185()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            r4.m195(r1, r0, r3)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            r1.<init>(r3)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            r3 = 200(0xc8, float:2.8E-43)
            if (r0 != r3) goto L_0x011c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r3 = "Validate-WH response - 200: "
            r0.<init>(r3)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            r0.append(r1)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            com.appsflyer.AFLogger.afInfoLog(r0)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            goto L_0x0139
        L_0x011c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r4 = "Validate-WH response failed - "
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            r3.append(r0)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r0 = ": "
            r3.append(r0)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r0 = r1.toString()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            r3.append(r0)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            java.lang.String r0 = r3.toString()     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
            com.appsflyer.AFLogger.afWarnLog(r0)     // Catch:{ Throwable -> 0x00ec, all -> 0x00ea }
        L_0x0139:
            if (r2 == 0) goto L_0x0156
            r2.disconnect()
            return
        L_0x013f:
            r0 = move-exception
            r2 = r3
            goto L_0x0150
        L_0x0142:
            r0 = move-exception
        L_0x0143:
            java.lang.String r1 = r0.getMessage()     // Catch:{ all -> 0x013f }
            com.appsflyer.AFLogger.afErrorLog(r1, r0)     // Catch:{ all -> 0x013f }
            if (r3 == 0) goto L_0x0156
            r3.disconnect()
            return
        L_0x0150:
            if (r2 == 0) goto L_0x0155
            r2.disconnect()
        L_0x0155:
            throw r0
        L_0x0156:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.h.m138(com.appsflyer.h, java.util.Map, java.util.Map, java.lang.ref.WeakReference):void");
    }
}
