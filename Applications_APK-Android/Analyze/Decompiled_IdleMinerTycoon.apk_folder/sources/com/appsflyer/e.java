package com.appsflyer;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import com.facebook.internal.FacebookRequestErrorClassification;

final class e {

    /* renamed from: ˎ  reason: contains not printable characters */
    private IntentFilter f116 = new IntentFilter("android.intent.action.BATTERY_CHANGED");

    static final class b {

        /* renamed from: ˊ  reason: contains not printable characters */
        static final e f117 = new e();
    }

    e() {
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: ˋ  reason: contains not printable characters */
    public final d m118(Context context) {
        String str;
        String str2 = null;
        float f = 0.0f;
        try {
            Intent registerReceiver = context.registerReceiver(null, this.f116);
            if (registerReceiver != null) {
                if (2 == registerReceiver.getIntExtra("status", -1)) {
                    int intExtra = registerReceiver.getIntExtra("plugged", -1);
                    if (intExtra != 4) {
                        switch (intExtra) {
                            case 1:
                                str = "ac";
                                break;
                            case 2:
                                str = "usb";
                                break;
                            default:
                                str = FacebookRequestErrorClassification.KEY_OTHER;
                                break;
                        }
                    } else {
                        str = "wireless";
                    }
                } else {
                    str = "no";
                }
                str2 = str;
                int intExtra2 = registerReceiver.getIntExtra("level", -1);
                int intExtra3 = registerReceiver.getIntExtra("scale", -1);
                if (!(-1 == intExtra2 || -1 == intExtra3)) {
                    f = (((float) intExtra2) * 100.0f) / ((float) intExtra3);
                }
            }
        } catch (Throwable unused) {
        }
        return new d(f, str2);
    }

    static final class d {

        /* renamed from: ˋ  reason: contains not printable characters */
        private final String f118;

        /* renamed from: ॱ  reason: contains not printable characters */
        private final float f119;

        d(float f, String str) {
            this.f119 = f;
            this.f118 = str;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final float m120() {
            return this.f119;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m119() {
            return this.f118;
        }

        d() {
        }
    }
}
