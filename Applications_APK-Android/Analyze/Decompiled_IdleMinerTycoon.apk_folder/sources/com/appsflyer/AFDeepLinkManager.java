package com.appsflyer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.Map;

public class AFDeepLinkManager {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static AFDeepLinkManager f1;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Uri f2;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static Uri f3;
    protected int currentActivityHash = -1;

    private AFDeepLinkManager() {
    }

    public static AFDeepLinkManager getInstance() {
        if (f1 == null) {
            f1 = new AFDeepLinkManager();
        }
        return f1;
    }

    /* access modifiers changed from: protected */
    public void processIntentForDeepLink(Intent intent, Context context, Map<String, Object> map) {
        Uri uri;
        if (intent == null || !"android.intent.action.VIEW".equals(intent.getAction())) {
            uri = null;
        } else {
            uri = intent.getData();
        }
        if (uri != null) {
            boolean z = AppsFlyerProperties.getInstance().getBoolean("consumeAfDeepLink", false);
            boolean z2 = (intent.getFlags() & 4194304) == 0;
            if (intent.hasExtra("appsflyer_click_ts") && !z) {
                long longExtra = intent.getLongExtra("appsflyer_click_ts", 0);
                long j = AppsFlyerProperties.getInstance().getLong("appsflyer_click_consumed_ts", 0);
                if (longExtra == 0 || longExtra == j) {
                    StringBuilder sb = new StringBuilder("skipping re-use of previously consumed deep link: ");
                    sb.append(uri.toString());
                    sb.append(" w/Ex: ");
                    sb.append(String.valueOf(longExtra));
                    AFLogger.afInfoLog(sb.toString());
                    return;
                }
                AppsFlyerLib.getInstance().handleDeepLinkCallback(context, map, uri);
                AppsFlyerProperties.getInstance().set("appsflyer_click_consumed_ts", longExtra);
            } else if (z || z2) {
                Boolean valueOf = Boolean.valueOf(z2);
                if (f2 == null || !uri.equals(f2)) {
                    AppsFlyerLib.getInstance().handleDeepLinkCallback(context, map, uri);
                    f2 = uri;
                    return;
                }
                StringBuilder sb2 = new StringBuilder("skipping re-use of previously consumed deep link: ");
                sb2.append(uri.toString());
                sb2.append(valueOf.booleanValue() ? " w/sT" : " w/cAPI");
                AFLogger.afInfoLog(sb2.toString());
            } else {
                if (this.currentActivityHash != AppsFlyerProperties.getInstance().getInt("lastActivityHash", 0)) {
                    AppsFlyerLib.getInstance().handleDeepLinkCallback(context, map, uri);
                    AppsFlyerProperties.getInstance().set("lastActivityHash", this.currentActivityHash);
                    return;
                }
                StringBuilder sb3 = new StringBuilder("skipping re-use of previously consumed deep link: ");
                sb3.append(uri.toString());
                sb3.append(" w/hC: ");
                sb3.append(String.valueOf(this.currentActivityHash));
                AFLogger.afInfoLog(sb3.toString());
            }
        } else if (f3 != null) {
            AppsFlyerLib.getInstance().handleDeepLinkCallback(context, map, f3);
            StringBuilder sb4 = new StringBuilder("using trampoline Intent fallback with URI : ");
            sb4.append(f3.toString());
            AFLogger.afInfoLog(sb4.toString());
            f3 = null;
        } else if (AppsFlyerLib.getInstance().latestDeepLink != null) {
            AppsFlyerLib.getInstance().handleDeepLinkCallback(context, map, AppsFlyerLib.getInstance().latestDeepLink);
        }
    }

    /* access modifiers changed from: protected */
    public void collectIntentsFromActivities(Intent intent) {
        Uri uri;
        if (intent == null || !"android.intent.action.VIEW".equals(intent.getAction())) {
            uri = null;
        } else {
            uri = intent.getData();
        }
        if (uri != null && intent.getData() != f3) {
            f3 = intent.getData();
        }
    }
}
