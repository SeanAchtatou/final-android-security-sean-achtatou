package com.appsflyer;

import android.content.Context;
import android.util.Log;
import com.appsflyer.CreateOneLinkHttpTask;
import com.appsflyer.share.CrossPromotionHelper;
import com.appsflyer.share.LinkGenerator;
import com.appsflyer.share.ShareInviteHelper;
import com.unity3d.player.UnityPlayer;
import java.util.HashMap;
import java.util.Map;

public class UnityShareHelper {
    /* access modifiers changed from: private */
    public static String callbackMethodFailedName;
    /* access modifiers changed from: private */
    public static String callbackMethodName;
    /* access modifiers changed from: private */
    public static String callbackObjectName;
    private static UnityShareHelper instance;

    public static UnityShareHelper getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new UnityShareHelper();
        return instance;
    }

    public void trackAndOpenStore(Context context, String str, String str2, Map<String, Object> map) {
        if (str2 != null && str != null) {
            if (map != null) {
                HashMap hashMap = new HashMap();
                for (Map.Entry next : map.entrySet()) {
                    if (!(next.getKey().toString() == null || next.getValue().toString() == null)) {
                        hashMap.put(next.getKey().toString(), next.getValue().toString());
                    }
                }
                CrossPromotionHelper.trackAndOpenStore(context, str, str2, hashMap);
                return;
            }
            CrossPromotionHelper.trackAndOpenStore(context, str, str2);
        }
    }

    public void createOneLinkInviteListener(Context context, Map<String, Object> map, String str, String str2, String str3) {
        callbackObjectName = str;
        callbackMethodName = str2;
        callbackMethodFailedName = str3;
        LinkGenerator generateInviteUrl = ShareInviteHelper.generateInviteUrl(context);
        Object obj = map.get(AppsFlyerProperties.CHANNEL);
        Object obj2 = map.get("campaign");
        Object obj3 = map.get("referrerName");
        Object obj4 = map.get("referrerImageUrl");
        Object obj5 = map.get("customerID");
        Object obj6 = map.get("baseDeepLink");
        if (obj != null) {
            generateInviteUrl.setChannel(obj.toString());
            map.remove(AppsFlyerProperties.CHANNEL);
        }
        if (obj2 != null) {
            generateInviteUrl.setCampaign(obj2.toString());
            map.remove("campaign");
        }
        if (obj3 != null) {
            generateInviteUrl.setReferrerName(obj3.toString());
            map.remove("referrerName");
        }
        if (obj4 != null) {
            generateInviteUrl.setReferrerImageURL(obj4.toString());
            map.remove("referrerImageUrl");
        }
        if (obj5 != null) {
            generateInviteUrl.setReferrerCustomerId(obj5.toString());
            map.remove("customerID");
        }
        if (obj6 != null) {
            generateInviteUrl.setBaseDeeplink(obj6.toString());
            map.remove("baseDeepLink");
        }
        for (Map.Entry next : map.entrySet()) {
            if (!(next.getKey() == null || next.getValue() == null)) {
                generateInviteUrl.addParameter(next.getKey().toString(), next.getValue().toString());
            }
        }
        generateInviteUrl.generateLink(context, new inviteCallbacksImpl());
    }

    private class inviteCallbacksImpl implements CreateOneLinkHttpTask.ResponseListener {
        private inviteCallbacksImpl() {
        }

        public void onResponse(String str) {
            Log.i("AppsFlyerLibUnityhelper", "CreateOneLinkHttpTask.ResponseListener called.");
            UnityPlayer.UnitySendMessage(UnityShareHelper.callbackObjectName, UnityShareHelper.callbackMethodName, str);
        }

        public void onResponseError(String str) {
            Log.i("AppsFlyerLibUnityhelper", "onValidateInAppFailure called.");
            UnityPlayer.UnitySendMessage(UnityShareHelper.callbackObjectName, UnityShareHelper.callbackMethodFailedName, str);
        }
    }
}
