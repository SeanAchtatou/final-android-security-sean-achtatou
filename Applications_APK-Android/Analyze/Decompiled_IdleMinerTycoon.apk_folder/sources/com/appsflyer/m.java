package com.appsflyer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.appsflyer.j;
import com.helpshift.analytics.AnalyticsEventKey;
import com.helpshift.common.domain.network.NetworkConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import org.json.JSONObject;

final class m extends AsyncTask<String, Void, String> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f175;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f176;

    /* renamed from: ʽ  reason: contains not printable characters */
    private WeakReference<Context> f177;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f178 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f179 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f180 = "";

    /* renamed from: ˏ  reason: contains not printable characters */
    Map<String, String> f181;

    /* renamed from: ॱ  reason: contains not printable characters */
    String f182;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private URL f183;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private HttpURLConnection f184;

    /* access modifiers changed from: protected */
    public final void onCancelled() {
    }

    m(Context context, boolean z) {
        this.f177 = new WeakReference<>(context);
        this.f175 = true;
        this.f176 = true;
        this.f179 = z;
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        if (this.f182 == null) {
            this.f182 = new JSONObject(this.f181).toString();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final String doInBackground(String... strArr) {
        if (this.f179) {
            return null;
        }
        try {
            this.f183 = new URL(strArr[0]);
            if (this.f175) {
                r.m185().m198(this.f183.toString(), this.f182);
                int length = this.f182.getBytes("UTF-8").length;
                StringBuilder sb = new StringBuilder("call = ");
                sb.append(this.f183);
                sb.append("; size = ");
                sb.append(length);
                sb.append(" byte");
                sb.append(length > 1 ? AnalyticsEventKey.SEARCH_QUERY : "");
                sb.append("; body = ");
                sb.append(this.f182);
                j.AnonymousClass2.m152(sb.toString());
            }
            this.f184 = (HttpURLConnection) this.f183.openConnection();
            this.f184.setReadTimeout(NetworkConstants.UPLOAD_CONNECT_TIMEOUT);
            this.f184.setConnectTimeout(NetworkConstants.UPLOAD_CONNECT_TIMEOUT);
            this.f184.setRequestMethod(HttpRequest.METHOD_POST);
            this.f184.setDoInput(true);
            this.f184.setDoOutput(true);
            this.f184.setRequestProperty(HttpRequest.HEADER_CONTENT_TYPE, "application/json");
            OutputStream outputStream = this.f184.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write(this.f182);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            this.f184.connect();
            int responseCode = this.f184.getResponseCode();
            if (this.f176) {
                AppsFlyerLib.getInstance();
                this.f180 = AppsFlyerLib.m31(this.f184);
            }
            if (this.f175) {
                r.m185().m195(this.f183.toString(), responseCode, this.f180);
            }
            if (responseCode == 200) {
                AFLogger.afInfoLog("Status 200 ok");
                Context context = this.f177.get();
                if (this.f183.toString().startsWith(ServerConfigHandler.getUrl(AppsFlyerLib.f21)) && context != null) {
                    SharedPreferences.Editor edit = context.getSharedPreferences("appsflyer-data", 0).edit();
                    edit.putBoolean("sentRegisterRequestToAF", true);
                    edit.apply();
                    AFLogger.afDebugLog("Successfully registered for Uninstall Tracking");
                }
            } else {
                this.f178 = true;
            }
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder("Error while calling ");
            sb2.append(this.f183.toString());
            AFLogger.afErrorLog(sb2.toString(), th);
            this.f178 = true;
        }
        return this.f180;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void onPostExecute(String str) {
        if (this.f178) {
            AFLogger.afInfoLog("Connection error: ".concat(String.valueOf(str)));
        } else {
            AFLogger.afInfoLog("Connection call succeeded: ".concat(String.valueOf(str)));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m159() {
        this.f175 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final HttpURLConnection m160() {
        return this.f184;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m157() {
        this.f176 = false;
    }
}
