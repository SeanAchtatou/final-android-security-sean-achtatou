package com.appsflyer.share;

import android.content.Context;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.CreateOneLinkHttpTask;
import com.appsflyer.ServerConfigHandler;
import com.ironsource.sdk.constants.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class LinkGenerator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f240;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f241;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f242;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f243;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f244;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private String f245;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f246;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String f247;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private Map<String, String> f248 = new HashMap();

    /* renamed from: ͺ  reason: contains not printable characters */
    private Map<String, String> f249 = new HashMap();

    /* renamed from: ॱ  reason: contains not printable characters */
    private String f250;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private String f251;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String f252;

    public LinkGenerator(String str) {
        this.f244 = str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final LinkGenerator m214(String str) {
        this.f252 = str;
        return this;
    }

    public LinkGenerator setDeeplinkPath(String str) {
        this.f242 = str;
        return this;
    }

    public LinkGenerator setBaseDeeplink(String str) {
        this.f245 = str;
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final LinkGenerator m215(String str) {
        this.f241 = str;
        return this;
    }

    public LinkGenerator setChannel(String str) {
        this.f247 = str;
        return this;
    }

    public String getChannel() {
        return this.f247;
    }

    public LinkGenerator setReferrerCustomerId(String str) {
        this.f250 = str;
        return this;
    }

    public String getMediaSource() {
        return this.f244;
    }

    public Map<String, String> getParameters() {
        return this.f249;
    }

    public LinkGenerator setCampaign(String str) {
        this.f243 = str;
        return this;
    }

    public String getCampaign() {
        return this.f243;
    }

    public LinkGenerator addParameter(String str, String str2) {
        this.f249.put(str, str2);
        return this;
    }

    public LinkGenerator addParameters(Map<String, String> map) {
        if (map != null) {
            this.f249.putAll(map);
        }
        return this;
    }

    public LinkGenerator setReferrerUID(String str) {
        this.f246 = str;
        return this;
    }

    public LinkGenerator setReferrerName(String str) {
        this.f240 = str;
        return this;
    }

    public LinkGenerator setReferrerImageURL(String str) {
        this.f251 = str;
        return this;
    }

    public LinkGenerator setBaseURL(String str, String str2, String str3) {
        if (str == null || str.length() <= 0) {
            this.f241 = String.format(Constants.AF_BASE_URL_FORMAT, ServerConfigHandler.getUrl(Constants.APPSFLYER_DEFAULT_APP_DOMAIN), str3);
        } else {
            if (str2 == null || str2.length() < 5) {
                str2 = Constants.ONELINK_DEFAULT_DOMAIN;
            }
            this.f241 = String.format(Constants.AF_BASE_URL_FORMAT, str2, str);
        }
        return this;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private StringBuilder m212() {
        StringBuilder sb = new StringBuilder();
        if (this.f241 == null || !this.f241.startsWith("http")) {
            sb.append(ServerConfigHandler.getUrl(Constants.BASE_URL_APP_APPSFLYER_COM));
        } else {
            sb.append(this.f241);
        }
        if (this.f252 != null) {
            sb.append('/');
            sb.append(this.f252);
        }
        this.f248.put(Constants.URL_MEDIA_SOURCE, this.f244);
        sb.append('?');
        sb.append("pid=");
        sb.append(m213(this.f244, "media source"));
        if (this.f246 != null) {
            this.f248.put(Constants.URL_REFERRER_UID, this.f246);
            sb.append('&');
            sb.append("af_referrer_uid=");
            sb.append(m213(this.f246, "referrerUID"));
        }
        if (this.f247 != null) {
            this.f248.put("af_channel", this.f247);
            sb.append('&');
            sb.append("af_channel=");
            sb.append(m213(this.f247, AppsFlyerProperties.CHANNEL));
        }
        if (this.f250 != null) {
            this.f248.put(Constants.URL_REFERRER_CUSTOMER_ID, this.f250);
            sb.append('&');
            sb.append("af_referrer_customer_id=");
            sb.append(m213(this.f250, "referrerCustomerId"));
        }
        if (this.f243 != null) {
            this.f248.put("c", this.f243);
            sb.append('&');
            sb.append("c=");
            sb.append(m213(this.f243, "campaign"));
        }
        if (this.f240 != null) {
            this.f248.put(Constants.URL_REFERRER_NAME, this.f240);
            sb.append('&');
            sb.append("af_referrer_name=");
            sb.append(m213(this.f240, "referrerName"));
        }
        if (this.f251 != null) {
            this.f248.put(Constants.URL_REFERRER_IMAGE_URL, this.f251);
            sb.append('&');
            sb.append("af_referrer_image_url=");
            sb.append(m213(this.f251, "referrerImageURL"));
        }
        if (this.f245 != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.f245);
            sb2.append(this.f245.endsWith(Constants.URL_PATH_DELIMITER) ? "" : Constants.URL_PATH_DELIMITER);
            if (this.f242 != null) {
                sb2.append(this.f242);
            }
            this.f248.put(Constants.URL_BASE_DEEPLINK, sb2.toString());
            sb.append('&');
            sb.append("af_dp=");
            sb.append(m213(this.f245, "baseDeeplink"));
            if (this.f242 != null) {
                sb.append(this.f245.endsWith(Constants.URL_PATH_DELIMITER) ? "" : "%2F");
                sb.append(m213(this.f242, "deeplinkPath"));
            }
        }
        for (String next : this.f249.keySet()) {
            String obj = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(next);
            sb3.append(Constants.RequestParameters.EQUAL);
            sb3.append(m213(this.f249.get(next), next));
            if (!obj.contains(sb3.toString())) {
                sb.append('&');
                sb.append(next);
                sb.append('=');
                sb.append(m213(this.f249.get(next), next));
            }
        }
        return sb;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static String m213(String str, String str2) {
        try {
            return URLEncoder.encode(str, "utf8");
        } catch (UnsupportedEncodingException unused) {
            StringBuilder sb = new StringBuilder("Illegal ");
            sb.append(str2);
            sb.append(": ");
            sb.append(str);
            AFLogger.afInfoLog(sb.toString());
            return "";
        } catch (Throwable unused2) {
            return "";
        }
    }

    public String generateLink() {
        return m212().toString();
    }

    public void generateLink(Context context, CreateOneLinkHttpTask.ResponseListener responseListener) {
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.ONELINK_ID);
        if (!this.f249.isEmpty()) {
            for (Map.Entry next : this.f249.entrySet()) {
                this.f248.put(next.getKey(), next.getValue());
            }
        }
        m212();
        ShareInviteHelper.generateUserInviteLink(context, string, this.f248, responseListener);
    }
}
