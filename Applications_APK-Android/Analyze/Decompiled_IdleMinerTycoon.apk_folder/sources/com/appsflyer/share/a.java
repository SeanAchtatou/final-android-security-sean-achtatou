package com.appsflyer.share;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.gms.drive.DriveFile;

final class a {

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f253;

    a() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m217(String str) {
        this.f253 = str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m216(Context context) {
        if (this.f253 != null) {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.f253)).setFlags(DriveFile.MODE_READ_ONLY));
        }
    }
}
