package com.appsflyer;

final class q implements b {

    /* renamed from: ˊ  reason: contains not printable characters */
    private b f192 = this;

    interface b {
        /* renamed from: ˊ  reason: contains not printable characters */
        Class<?> m178(String str) throws ClassNotFoundException;
    }

    enum e {
        UNITY("android_unity", "com.unity3d.player.UnityPlayer"),
        REACT_NATIVE("android_reactNative", "com.facebook.react.ReactApplication"),
        CORDOVA("android_cordova", "org.apache.cordova.CordovaActivity"),
        SEGMENT("android_segment", "com.segment.analytics.integrations.Integration"),
        COCOS2DX("android_cocos2dx", "org.cocos2dx.lib.Cocos2dxActivity"),
        DEFAULT("android_native", "android_native");
        

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f200;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f201;

        private e(String str, String str2) {
            this.f201 = str;
            this.f200 = str2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final String m177() {
        for (e eVar : e.values()) {
            if (m175(e.m180(eVar))) {
                return e.m179(eVar);
            }
        }
        return e.m179(e.DEFAULT);
    }

    q() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean m175(String str) {
        try {
            this.f192.m178(str);
            StringBuilder sb = new StringBuilder("Class: ");
            sb.append(str);
            sb.append(" is found.");
            AFLogger.afRDLog(sb.toString());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Class<?> m176(String str) throws ClassNotFoundException {
        return Class.forName(str);
    }
}
