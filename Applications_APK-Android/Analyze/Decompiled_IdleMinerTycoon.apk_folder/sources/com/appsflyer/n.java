package com.appsflyer;

import android.content.ContentResolver;
import android.os.Build;
import android.provider.Settings;
import com.appsflyer.k;

final class n {
    n() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static k m162(ContentResolver contentResolver) {
        String str;
        if (contentResolver == null || AppsFlyerProperties.getInstance().getString("amazon_aid") != null || !"Amazon".equals(Build.MANUFACTURER)) {
            return null;
        }
        int i = Settings.Secure.getInt(contentResolver, "limit_ad_tracking", 2);
        if (i == 0) {
            return new k(k.b.AMAZON, Settings.Secure.getString(contentResolver, "advertising_id"), false);
        } else if (i == 2) {
            return null;
        } else {
            try {
                str = Settings.Secure.getString(contentResolver, "advertising_id");
            } catch (Throwable th) {
                AFLogger.afErrorLog("Couldn't fetch Amazon Advertising ID (Ad-Tracking is limited!)", th);
                str = "";
            }
            return new k(k.b.AMAZON, str, true);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0087 A[SYNTHETIC, Splitter:B:30:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0133 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void m161(android.content.Context r9, java.util.Map<java.lang.String, java.lang.Object> r10) {
        /*
            java.lang.String r0 = "Trying to fetch GAID.."
            com.appsflyer.AFLogger.afInfoLog(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            com.google.android.gms.common.GoogleApiAvailability r1 = com.google.android.gms.common.GoogleApiAvailability.getInstance()     // Catch:{ Throwable -> 0x0013 }
            int r1 = r1.isGooglePlayServicesAvailable(r9)     // Catch:{ Throwable -> 0x0013 }
            goto L_0x001c
        L_0x0013:
            r1 = move-exception
            java.lang.String r2 = r1.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r2, r1)
            r1 = -1
        L_0x001c:
            r2 = 0
            r3 = 1
            r4 = 0
            java.lang.String r5 = "com.google.android.gms.ads.identifier.AdvertisingIdClient"
            java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x005d }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r5 = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(r9)     // Catch:{ Throwable -> 0x005d }
            if (r5 == 0) goto L_0x0050
            java.lang.String r6 = r5.getId()     // Catch:{ Throwable -> 0x005d }
            boolean r5 = r5.isLimitAdTrackingEnabled()     // Catch:{ Throwable -> 0x004e }
            r5 = r5 ^ r3
            java.lang.String r5 = java.lang.Boolean.toString(r5)     // Catch:{ Throwable -> 0x004e }
            if (r6 == 0) goto L_0x0046
            int r2 = r6.length()     // Catch:{ Throwable -> 0x0040 }
            if (r2 != 0) goto L_0x004b
            goto L_0x0046
        L_0x0040:
            r2 = move-exception
            r4 = 1
            r8 = r5
            r5 = r2
            r2 = r8
            goto L_0x005f
        L_0x0046:
            java.lang.String r2 = "emptyOrNull |"
            r0.append(r2)     // Catch:{ Throwable -> 0x0040 }
        L_0x004b:
            r4 = 1
            goto L_0x00e9
        L_0x004e:
            r5 = move-exception
            goto L_0x005f
        L_0x0050:
            java.lang.String r5 = "gpsAdInfo-null |"
            r0.append(r5)     // Catch:{ Throwable -> 0x005d }
            com.appsflyer.n$d r5 = new com.appsflyer.n$d     // Catch:{ Throwable -> 0x005d }
            java.lang.String r6 = "GpsAdIndo is null"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x005d }
            throw r5     // Catch:{ Throwable -> 0x005d }
        L_0x005d:
            r5 = move-exception
            r6 = r2
        L_0x005f:
            java.lang.String r7 = r5.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r7, r5)
            java.lang.Class r5 = r5.getClass()
            java.lang.String r5 = r5.getSimpleName()
            r0.append(r5)
            java.lang.String r5 = " |"
            r0.append(r5)
            java.lang.String r5 = "WARNING: Google Play Services is missing."
            com.appsflyer.AFLogger.afInfoLog(r5)
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r7 = "enableGpsFallback"
            boolean r5 = r5.getBoolean(r7, r3)
            if (r5 == 0) goto L_0x00e8
            com.appsflyer.g$d r2 = com.appsflyer.g.m130(r9)     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r6 = r2.m134()     // Catch:{ Throwable -> 0x00a6 }
            boolean r2 = r2.m135()     // Catch:{ Throwable -> 0x00a6 }
            r2 = r2 ^ r3
            java.lang.String r5 = java.lang.Boolean.toString(r2)     // Catch:{ Throwable -> 0x00a6 }
            if (r6 == 0) goto L_0x00a0
            int r2 = r6.length()     // Catch:{ Throwable -> 0x00a6 }
            if (r2 != 0) goto L_0x00e9
        L_0x00a0:
            java.lang.String r2 = "emptyOrNull (bypass) |"
            r0.append(r2)     // Catch:{ Throwable -> 0x00a6 }
            goto L_0x00e9
        L_0x00a6:
            r2 = move-exception
            java.lang.String r3 = r2.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r3, r2)
            java.lang.Class r3 = r2.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r0.append(r3)
            java.lang.String r3 = " |"
            r0.append(r3)
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r5 = "advertiserId"
            java.lang.String r6 = r3.getString(r5)
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r5 = "advertiserIdEnabled"
            java.lang.String r5 = r3.getString(r5)
            java.lang.String r3 = r2.getLocalizedMessage()
            if (r3 == 0) goto L_0x00e0
            java.lang.String r2 = r2.getLocalizedMessage()
            com.appsflyer.AFLogger.afInfoLog(r2)
            goto L_0x00e9
        L_0x00e0:
            java.lang.String r2 = r2.toString()
            com.appsflyer.AFLogger.afInfoLog(r2)
            goto L_0x00e9
        L_0x00e8:
            r5 = r2
        L_0x00e9:
            java.lang.Class r9 = r9.getClass()
            java.lang.String r9 = r9.getName()
            java.lang.String r2 = "android.app.ReceiverRestrictedContext"
            boolean r9 = r9.equals(r2)
            if (r9 == 0) goto L_0x0112
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r2 = "advertiserId"
            java.lang.String r6 = r9.getString(r2)
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r2 = "advertiserIdEnabled"
            java.lang.String r5 = r9.getString(r2)
            java.lang.String r9 = "context = android.app.ReceiverRestrictedContext |"
            r0.append(r9)
        L_0x0112:
            int r9 = r0.length()
            if (r9 <= 0) goto L_0x0131
            java.lang.String r9 = "gaidError"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r1)
            java.lang.String r1 = ": "
            r2.append(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r10.put(r9, r0)
        L_0x0131:
            if (r6 == 0) goto L_0x015a
            if (r5 == 0) goto L_0x015a
            java.lang.String r9 = "advertiserId"
            r10.put(r9, r6)
            java.lang.String r9 = "advertiserIdEnabled"
            r10.put(r9, r5)
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r0 = "advertiserId"
            r9.set(r0, r6)
            com.appsflyer.AppsFlyerProperties r9 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r0 = "advertiserIdEnabled"
            r9.set(r0, r5)
            java.lang.String r9 = "isGaidWithGps"
            java.lang.String r0 = java.lang.String.valueOf(r4)
            r10.put(r9, r0)
        L_0x015a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.n.m161(android.content.Context, java.util.Map):void");
    }

    static class d extends IllegalStateException {
        d(String str) {
            super(str);
        }
    }
}
