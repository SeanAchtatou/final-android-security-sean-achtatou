package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.common.BitArray;
import java.util.Arrays;
import java.util.Map;

public abstract class UPCEANReader extends OneDReader {
    static final int[] END_PATTERN = {1, 1, 1, 1, 1, 1};
    static final int[][] L_AND_G_PATTERNS = new int[20][];
    static final int[][] L_PATTERNS = {new int[]{3, 2, 1, 1}, new int[]{2, 2, 2, 1}, new int[]{2, 1, 2, 2}, new int[]{1, 4, 1, 1}, new int[]{1, 1, 3, 2}, new int[]{1, 2, 3, 1}, new int[]{1, 1, 1, 4}, new int[]{1, 3, 1, 2}, new int[]{1, 2, 1, 3}, new int[]{3, 1, 1, 2}};
    private static final float MAX_AVG_VARIANCE = 0.48f;
    private static final float MAX_INDIVIDUAL_VARIANCE = 0.7f;
    static final int[] MIDDLE_PATTERN = {1, 1, 1, 1, 1};
    static final int[] START_END_PATTERN = {1, 1, 1};
    private final StringBuilder decodeRowStringBuffer = new StringBuilder(20);
    private final EANManufacturerOrgSupport eanManSupport = new EANManufacturerOrgSupport();
    private final UPCEANExtensionSupport extensionReader = new UPCEANExtensionSupport();

    /* access modifiers changed from: protected */
    public abstract int decodeMiddle(BitArray bitArray, int[] iArr, StringBuilder sb) throws NotFoundException;

    /* access modifiers changed from: package-private */
    public abstract BarcodeFormat getBarcodeFormat();

    static {
        System.arraycopy(L_PATTERNS, 0, L_AND_G_PATTERNS, 0, 10);
        for (int i = 10; i < 20; i++) {
            int[] iArr = L_PATTERNS[i - 10];
            int[] iArr2 = new int[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                iArr2[i2] = iArr[(iArr.length - i2) - 1];
            }
            L_AND_G_PATTERNS[i] = iArr2;
        }
    }

    protected UPCEANReader() {
    }

    static int[] findStartGuardPattern(BitArray bitArray) throws NotFoundException {
        int[] iArr = new int[START_END_PATTERN.length];
        int[] iArr2 = null;
        boolean z = false;
        int i = 0;
        while (!z) {
            Arrays.fill(iArr, 0, START_END_PATTERN.length, 0);
            iArr2 = findGuardPattern(bitArray, i, false, START_END_PATTERN, iArr);
            int i2 = iArr2[0];
            int i3 = iArr2[1];
            int i4 = i2 - (i3 - i2);
            if (i4 >= 0) {
                z = bitArray.isRange(i4, i2, false);
            }
            i = i3;
        }
        return iArr2;
    }

    public Result decodeRow(int i, BitArray bitArray, Map<DecodeHintType, ?> map) throws NotFoundException, ChecksumException, FormatException {
        return decodeRow(i, bitArray, findStartGuardPattern(bitArray), map);
    }

    /* JADX WARN: Type inference failed for: r13v4, types: [java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.zxing.Result decodeRow(int r12, com.google.zxing.common.BitArray r13, int[] r14, java.util.Map<com.google.zxing.DecodeHintType, ?> r15) throws com.google.zxing.NotFoundException, com.google.zxing.ChecksumException, com.google.zxing.FormatException {
        /*
            r11 = this;
            r0 = 0
            if (r15 != 0) goto L_0x0005
            r1 = r0
            goto L_0x000d
        L_0x0005:
            com.google.zxing.DecodeHintType r1 = com.google.zxing.DecodeHintType.NEED_RESULT_POINT_CALLBACK
            java.lang.Object r1 = r15.get(r1)
            com.google.zxing.ResultPointCallback r1 = (com.google.zxing.ResultPointCallback) r1
        L_0x000d:
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 1
            r4 = 0
            if (r1 == 0) goto L_0x0023
            com.google.zxing.ResultPoint r5 = new com.google.zxing.ResultPoint
            r6 = r14[r4]
            r7 = r14[r3]
            int r6 = r6 + r7
            float r6 = (float) r6
            float r6 = r6 / r2
            float r7 = (float) r12
            r5.<init>(r6, r7)
            r1.foundPossibleResultPoint(r5)
        L_0x0023:
            java.lang.StringBuilder r5 = r11.decodeRowStringBuffer
            r5.setLength(r4)
            int r6 = r11.decodeMiddle(r13, r14, r5)
            if (r1 == 0) goto L_0x0038
            com.google.zxing.ResultPoint r7 = new com.google.zxing.ResultPoint
            float r8 = (float) r6
            float r9 = (float) r12
            r7.<init>(r8, r9)
            r1.foundPossibleResultPoint(r7)
        L_0x0038:
            int[] r6 = r11.decodeEnd(r13, r6)
            if (r1 == 0) goto L_0x004e
            com.google.zxing.ResultPoint r7 = new com.google.zxing.ResultPoint
            r8 = r6[r4]
            r9 = r6[r3]
            int r8 = r8 + r9
            float r8 = (float) r8
            float r8 = r8 / r2
            float r9 = (float) r12
            r7.<init>(r8, r9)
            r1.foundPossibleResultPoint(r7)
        L_0x004e:
            r1 = r6[r3]
            r7 = r6[r4]
            int r7 = r1 - r7
            int r7 = r7 + r1
            int r8 = r13.getSize()
            if (r7 >= r8) goto L_0x0108
            boolean r1 = r13.isRange(r1, r7, r4)
            if (r1 == 0) goto L_0x0108
            java.lang.String r1 = r5.toString()
            int r5 = r1.length()
            r7 = 8
            if (r5 < r7) goto L_0x0103
            boolean r5 = r11.checkChecksum(r1)
            if (r5 == 0) goto L_0x00fe
            r5 = r14[r3]
            r14 = r14[r4]
            int r5 = r5 + r14
            float r14 = (float) r5
            float r14 = r14 / r2
            r5 = r6[r3]
            r7 = r6[r4]
            int r5 = r5 + r7
            float r5 = (float) r5
            float r5 = r5 / r2
            com.google.zxing.BarcodeFormat r2 = r11.getBarcodeFormat()
            com.google.zxing.Result r7 = new com.google.zxing.Result
            r8 = 2
            com.google.zxing.ResultPoint[] r8 = new com.google.zxing.ResultPoint[r8]
            com.google.zxing.ResultPoint r9 = new com.google.zxing.ResultPoint
            float r10 = (float) r12
            r9.<init>(r14, r10)
            r8[r4] = r9
            com.google.zxing.ResultPoint r14 = new com.google.zxing.ResultPoint
            r14.<init>(r5, r10)
            r8[r3] = r14
            r7.<init>(r1, r0, r8, r2)
            com.google.zxing.oned.UPCEANExtensionSupport r14 = r11.extensionReader     // Catch:{ ReaderException -> 0x00c4 }
            r5 = r6[r3]     // Catch:{ ReaderException -> 0x00c4 }
            com.google.zxing.Result r12 = r14.decodeRow(r12, r13, r5)     // Catch:{ ReaderException -> 0x00c4 }
            com.google.zxing.ResultMetadataType r13 = com.google.zxing.ResultMetadataType.UPC_EAN_EXTENSION     // Catch:{ ReaderException -> 0x00c4 }
            java.lang.String r14 = r12.getText()     // Catch:{ ReaderException -> 0x00c4 }
            r7.putMetadata(r13, r14)     // Catch:{ ReaderException -> 0x00c4 }
            java.util.Map r13 = r12.getResultMetadata()     // Catch:{ ReaderException -> 0x00c4 }
            r7.putAllMetadata(r13)     // Catch:{ ReaderException -> 0x00c4 }
            com.google.zxing.ResultPoint[] r13 = r12.getResultPoints()     // Catch:{ ReaderException -> 0x00c4 }
            r7.addResultPoints(r13)     // Catch:{ ReaderException -> 0x00c4 }
            java.lang.String r12 = r12.getText()     // Catch:{ ReaderException -> 0x00c4 }
            int r12 = r12.length()     // Catch:{ ReaderException -> 0x00c4 }
            goto L_0x00c5
        L_0x00c4:
            r12 = 0
        L_0x00c5:
            if (r15 != 0) goto L_0x00c8
            goto L_0x00d1
        L_0x00c8:
            com.google.zxing.DecodeHintType r13 = com.google.zxing.DecodeHintType.ALLOWED_EAN_EXTENSIONS
            java.lang.Object r13 = r15.get(r13)
            r0 = r13
            int[] r0 = (int[]) r0
        L_0x00d1:
            if (r0 == 0) goto L_0x00e8
            int r13 = r0.length
            r14 = 0
        L_0x00d5:
            if (r14 >= r13) goto L_0x00df
            r15 = r0[r14]
            if (r12 != r15) goto L_0x00dc
            goto L_0x00e0
        L_0x00dc:
            int r14 = r14 + 1
            goto L_0x00d5
        L_0x00df:
            r3 = 0
        L_0x00e0:
            if (r3 == 0) goto L_0x00e3
            goto L_0x00e8
        L_0x00e3:
            com.google.zxing.NotFoundException r12 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r12
        L_0x00e8:
            com.google.zxing.BarcodeFormat r12 = com.google.zxing.BarcodeFormat.EAN_13
            if (r2 == r12) goto L_0x00f0
            com.google.zxing.BarcodeFormat r12 = com.google.zxing.BarcodeFormat.UPC_A
            if (r2 != r12) goto L_0x00fd
        L_0x00f0:
            com.google.zxing.oned.EANManufacturerOrgSupport r12 = r11.eanManSupport
            java.lang.String r12 = r12.lookupCountryIdentifier(r1)
            if (r12 == 0) goto L_0x00fd
            com.google.zxing.ResultMetadataType r13 = com.google.zxing.ResultMetadataType.POSSIBLE_COUNTRY
            r7.putMetadata(r13, r12)
        L_0x00fd:
            return r7
        L_0x00fe:
            com.google.zxing.ChecksumException r12 = com.google.zxing.ChecksumException.getChecksumInstance()
            throw r12
        L_0x0103:
            com.google.zxing.FormatException r12 = com.google.zxing.FormatException.getFormatInstance()
            throw r12
        L_0x0108:
            com.google.zxing.NotFoundException r12 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.oned.UPCEANReader.decodeRow(int, com.google.zxing.common.BitArray, int[], java.util.Map):com.google.zxing.Result");
    }

    /* access modifiers changed from: package-private */
    public boolean checkChecksum(String str) throws FormatException {
        return checkStandardUPCEANChecksum(str);
    }

    static boolean checkStandardUPCEANChecksum(CharSequence charSequence) throws FormatException {
        int length = charSequence.length();
        if (length == 0) {
            return false;
        }
        int i = length - 1;
        return getStandardUPCEANChecksum(charSequence.subSequence(0, i)) == Character.digit(charSequence.charAt(i), 10);
    }

    static int getStandardUPCEANChecksum(CharSequence charSequence) throws FormatException {
        int length = charSequence.length();
        int i = 0;
        for (int i2 = length - 1; i2 >= 0; i2 -= 2) {
            int charAt = charSequence.charAt(i2) - '0';
            if (charAt < 0 || charAt > 9) {
                throw FormatException.getFormatInstance();
            }
            i += charAt;
        }
        int i3 = i * 3;
        for (int i4 = length - 2; i4 >= 0; i4 -= 2) {
            int charAt2 = charSequence.charAt(i4) - '0';
            if (charAt2 < 0 || charAt2 > 9) {
                throw FormatException.getFormatInstance();
            }
            i3 += charAt2;
        }
        return (1000 - i3) % 10;
    }

    /* access modifiers changed from: package-private */
    public int[] decodeEnd(BitArray bitArray, int i) throws NotFoundException {
        return findGuardPattern(bitArray, i, false, START_END_PATTERN);
    }

    static int[] findGuardPattern(BitArray bitArray, int i, boolean z, int[] iArr) throws NotFoundException {
        return findGuardPattern(bitArray, i, z, iArr, new int[iArr.length]);
    }

    private static int[] findGuardPattern(BitArray bitArray, int i, boolean z, int[] iArr, int[] iArr2) throws NotFoundException {
        int size = bitArray.getSize();
        int nextUnset = z ? bitArray.getNextUnset(i) : bitArray.getNextSet(i);
        int length = iArr.length;
        int i2 = nextUnset;
        int i3 = 0;
        while (nextUnset < size) {
            boolean z2 = true;
            if (bitArray.get(nextUnset) != z) {
                iArr2[i3] = iArr2[i3] + 1;
            } else {
                if (i3 != length - 1) {
                    i3++;
                } else if (patternMatchVariance(iArr2, iArr, MAX_INDIVIDUAL_VARIANCE) < MAX_AVG_VARIANCE) {
                    return new int[]{i2, nextUnset};
                } else {
                    i2 += iArr2[0] + iArr2[1];
                    int i4 = i3 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i4);
                    iArr2[i4] = 0;
                    iArr2[i3] = 0;
                    i3--;
                }
                iArr2[i3] = 1;
                if (z) {
                    z2 = false;
                }
                z = z2;
            }
            nextUnset++;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    static int decodeDigit(BitArray bitArray, int[] iArr, int i, int[][] iArr2) throws NotFoundException {
        recordPattern(bitArray, i, iArr);
        int length = iArr2.length;
        float f = MAX_AVG_VARIANCE;
        int i2 = -1;
        for (int i3 = 0; i3 < length; i3++) {
            float patternMatchVariance = patternMatchVariance(iArr, iArr2[i3], MAX_INDIVIDUAL_VARIANCE);
            if (patternMatchVariance < f) {
                i2 = i3;
                f = patternMatchVariance;
            }
        }
        if (i2 >= 0) {
            return i2;
        }
        throw NotFoundException.getNotFoundInstance();
    }
}
