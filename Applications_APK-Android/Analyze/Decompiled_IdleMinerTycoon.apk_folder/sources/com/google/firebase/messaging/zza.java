package com.google.firebase.messaging;

import android.support.v4.app.NotificationCompat;

public final class zza {
    public final int id = 0;
    public final String tag;
    public final NotificationCompat.Builder zzds;

    zza(NotificationCompat.Builder builder, String str, int i) {
        this.zzds = builder;
        this.tag = str;
    }
}
