package com.google.firebase.unity;

public final class R {
    private R() {
    }

    public static final class string {
        public static final int default_web_client_id = 2131689563;
        public static final int firebase_database_url = 2131689571;
        public static final int gcm_defaultSenderId = 2131689573;
        public static final int google_api_key = 2131689575;
        public static final int google_app_id = 2131689576;
        public static final int google_crash_reporting_api_key = 2131689577;
        public static final int google_storage_bucket = 2131689578;
        public static final int project_id = 2131689730;

        private string() {
        }
    }
}
