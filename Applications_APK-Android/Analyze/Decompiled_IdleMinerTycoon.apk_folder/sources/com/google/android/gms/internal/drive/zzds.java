package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.List;

final class zzds extends zzav {
    private final /* synthetic */ List zzfz;
    private final /* synthetic */ zzdp zzgo;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzds(zzdp zzdp, GoogleApiClient googleApiClient, List list) {
        super(googleApiClient);
        this.zzgo = zzdp;
        this.zzfz = list;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzeo) ((zzaw) anyClient).getService()).zza(new zzgq(this.zzgo.zzk, this.zzfz), new zzgs(this));
    }
}
