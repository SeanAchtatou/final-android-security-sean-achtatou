package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.controller.BannerJSAdapter;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONException;
import org.json.JSONObject;

@ParametersAreNonnullByDefault
@zzard
public final class zzahu implements zzaho<Object> {
    private final Object lock = new Object();
    @GuardedBy("lock")
    private final Map<String, zzahw> zzdaq = new HashMap();

    public final void zza(String str, zzahw zzahw) {
        synchronized (this.lock) {
            this.zzdaq.put(str, zzahw);
        }
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str;
        String str2 = map.get("id");
        String str3 = map.get(BannerJSAdapter.FAIL);
        String str4 = map.get("fail_reason");
        String str5 = map.get("fail_stack");
        String str6 = map.get(IronSourceConstants.EVENTS_RESULT);
        if (TextUtils.isEmpty(str5)) {
            str4 = "Unknown Fail Reason.";
        }
        if (TextUtils.isEmpty(str5)) {
            str = "";
        } else {
            String valueOf = String.valueOf(str5);
            str = valueOf.length() != 0 ? "\n".concat(valueOf) : new String("\n");
        }
        synchronized (this.lock) {
            zzahw remove = this.zzdaq.remove(str2);
            if (remove == null) {
                String valueOf2 = String.valueOf(str2);
                zzawz.zzep(valueOf2.length() != 0 ? "Received result for unexpected method invocation: ".concat(valueOf2) : new String("Received result for unexpected method invocation: "));
            } else if (!TextUtils.isEmpty(str3)) {
                String valueOf3 = String.valueOf(str4);
                String valueOf4 = String.valueOf(str);
                remove.onFailure(valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3));
            } else if (str6 == null) {
                remove.zzc(null);
            } else {
                try {
                    JSONObject jSONObject = new JSONObject(str6);
                    if (zzawz.zzvj()) {
                        String valueOf5 = String.valueOf(jSONObject.toString(2));
                        zzawz.zzds(valueOf5.length() != 0 ? "Result GMSG: ".concat(valueOf5) : new String("Result GMSG: "));
                    }
                    remove.zzc(jSONObject);
                } catch (JSONException e) {
                    remove.onFailure(e.getMessage());
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: EngineT
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <EngineT extends com.google.android.gms.internal.ads.zzakg> com.google.android.gms.internal.ads.zzbbh<org.json.JSONObject> zza(EngineT r5, java.lang.String r6, org.json.JSONObject r7) {
        /*
            r4 = this;
            com.google.android.gms.internal.ads.zzbbr r0 = new com.google.android.gms.internal.ads.zzbbr
            r0.<init>()
            com.google.android.gms.ads.internal.zzk.zzlg()
            java.lang.String r1 = com.google.android.gms.internal.ads.zzaxi.zzwb()
            com.google.android.gms.internal.ads.zzahv r2 = new com.google.android.gms.internal.ads.zzahv
            r2.<init>(r4, r0)
            r4.zza(r1, r2)
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0027 }
            r2.<init>()     // Catch:{ Exception -> 0x0027 }
            java.lang.String r3 = "id"
            r2.put(r3, r1)     // Catch:{ Exception -> 0x0027 }
            java.lang.String r1 = "args"
            r2.put(r1, r7)     // Catch:{ Exception -> 0x0027 }
            r5.zzb(r6, r2)     // Catch:{ Exception -> 0x0027 }
            goto L_0x002b
        L_0x0027:
            r5 = move-exception
            r0.setException(r5)
        L_0x002b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzahu.zza(com.google.android.gms.internal.ads.zzakg, java.lang.String, org.json.JSONObject):com.google.android.gms.internal.ads.zzbbh");
    }
}
