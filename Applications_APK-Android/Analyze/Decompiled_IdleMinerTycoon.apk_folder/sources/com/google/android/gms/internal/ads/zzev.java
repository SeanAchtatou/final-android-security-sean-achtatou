package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbp;
import java.lang.reflect.InvocationTargetException;

public final class zzev extends zzfk {
    public zzev(zzdy zzdy, String str, String str2, zzbp.zza.C0029zza zza, int i, int i2) {
        super(zzdy, str, str2, zza, i, 11);
    }

    /* access modifiers changed from: protected */
    public final void zzcx() throws IllegalAccessException, InvocationTargetException {
        this.zzzm.zzao(((Long) this.zzzw.invoke(null, this.zzvd.getContext())).longValue());
    }
}
