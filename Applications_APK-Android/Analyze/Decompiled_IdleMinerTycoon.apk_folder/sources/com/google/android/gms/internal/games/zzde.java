package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.multiplayer.ParticipantResult;

final class zzde extends zzdu {
    private final /* synthetic */ String zzey;
    private final /* synthetic */ byte[] zzla;
    private final /* synthetic */ String zzlb;
    private final /* synthetic */ ParticipantResult[] zzlc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzde(zzcz zzcz, GoogleApiClient googleApiClient, String str, byte[] bArr, String str2, ParticipantResult[] participantResultArr) {
        super(googleApiClient, null);
        this.zzey = str;
        this.zzla = bArr;
        this.zzlb = str2;
        this.zzlc = participantResultArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzey, this.zzla, this.zzlb, this.zzlc);
    }
}
