package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbsm;

public final class zzcoe<AdT, AdapterT, ListenerT extends zzbsm> implements zzcjv<AdT> {
    private final zzcjz<AdapterT, ListenerT> zzfef;
    private final zzczt zzgbk;
    private final zzcka<AdT, AdapterT, ListenerT> zzgch;
    private final zzbbl zzgci;

    public zzcoe(zzczt zzczt, zzbbl zzbbl, zzcjz<AdapterT, ListenerT> zzcjz, zzcka<AdT, AdapterT, ListenerT> zzcka) {
        this.zzgbk = zzczt;
        this.zzgci = zzbbl;
        this.zzgch = zzcka;
        this.zzfef = zzcjz;
    }

    public final boolean zza(zzcxu zzcxu, zzcxm zzcxm) {
        return !zzcxm.zzgkf.isEmpty();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ListenerT
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final com.google.android.gms.internal.ads.zzbbh<AdT> zzb(com.google.android.gms.internal.ads.zzcxu r6, com.google.android.gms.internal.ads.zzcxm r7) {
        /*
            r5 = this;
            java.util.List<java.lang.String> r0 = r7.zzgkf
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x001b
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            com.google.android.gms.internal.ads.zzcjz<AdapterT, ListenerT> r2 = r5.zzfef     // Catch:{ Throwable -> 0x0006 }
            org.json.JSONObject r3 = r7.zzgkh     // Catch:{ Throwable -> 0x0006 }
            com.google.android.gms.internal.ads.zzcjy r1 = r2.zzd(r1, r3)     // Catch:{ Throwable -> 0x0006 }
            goto L_0x001c
        L_0x001b:
            r1 = 0
        L_0x001c:
            if (r1 != 0) goto L_0x002a
            com.google.android.gms.internal.ads.zzcmk r6 = new com.google.android.gms.internal.ads.zzcmk
            java.lang.String r7 = "unable to instantiate mediation adapter class"
            r6.<init>(r7)
            com.google.android.gms.internal.ads.zzbbf r6 = com.google.android.gms.internal.ads.zzbar.zzd(r6)
            return r6
        L_0x002a:
            com.google.android.gms.internal.ads.zzbbr r0 = new com.google.android.gms.internal.ads.zzbbr
            r0.<init>()
            com.google.android.gms.internal.ads.zzcoh r2 = new com.google.android.gms.internal.ads.zzcoh
            r2.<init>(r5, r0, r1)
            ListenerT r3 = r1.zzfzn
            r3.zza(r2)
            boolean r2 = r7.zzdpc
            if (r2 == 0) goto L_0x0065
            com.google.android.gms.internal.ads.zzcxr r2 = r6.zzgkx
            com.google.android.gms.internal.ads.zzcxv r2 = r2.zzfjp
            com.google.android.gms.internal.ads.zzxz r2 = r2.zzghg
            android.os.Bundle r2 = r2.zzcgv
            java.lang.Class<com.google.ads.mediation.admob.AdMobAdapter> r3 = com.google.ads.mediation.admob.AdMobAdapter.class
            java.lang.String r3 = r3.getName()
            android.os.Bundle r3 = r2.getBundle(r3)
            if (r3 != 0) goto L_0x005f
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.Class<com.google.ads.mediation.admob.AdMobAdapter> r4 = com.google.ads.mediation.admob.AdMobAdapter.class
            java.lang.String r4 = r4.getName()
            r2.putBundle(r4, r3)
        L_0x005f:
            java.lang.String r2 = "render_test_ad_label"
            r4 = 1
            r3.putBoolean(r2, r4)
        L_0x0065:
            com.google.android.gms.internal.ads.zzczt r2 = r5.zzgbk
            com.google.android.gms.internal.ads.zzczs r3 = com.google.android.gms.internal.ads.zzczs.ADAPTER_LOAD_AD_SYN
            com.google.android.gms.internal.ads.zzczj r2 = r2.zzv(r3)
            com.google.android.gms.internal.ads.zzcof r3 = new com.google.android.gms.internal.ads.zzcof
            r3.<init>(r5, r6, r7, r1)
            com.google.android.gms.internal.ads.zzbbl r4 = r5.zzgci
            com.google.android.gms.internal.ads.zzczl r2 = r2.zza(r3, r4)
            com.google.android.gms.internal.ads.zzczs r3 = com.google.android.gms.internal.ads.zzczs.ADAPTER_LOAD_AD_ACK
            com.google.android.gms.internal.ads.zzczl r2 = r2.zzx(r3)
            com.google.android.gms.internal.ads.zzczl r0 = r2.zzb(r0)
            com.google.android.gms.internal.ads.zzczs r2 = com.google.android.gms.internal.ads.zzczs.ADAPTER_WRAP_ADAPTER
            com.google.android.gms.internal.ads.zzczl r0 = r0.zzx(r2)
            com.google.android.gms.internal.ads.zzcog r2 = new com.google.android.gms.internal.ads.zzcog
            r2.<init>(r5, r6, r7, r1)
            com.google.android.gms.internal.ads.zzczl r6 = r0.zzb(r2)
            com.google.android.gms.internal.ads.zzcze r6 = r6.zzane()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcoe.zzb(com.google.android.gms.internal.ads.zzcxu, com.google.android.gms.internal.ads.zzcxm):com.google.android.gms.internal.ads.zzbbh");
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(zzcxu zzcxu, zzcxm zzcxm, zzcjy zzcjy, Void voidR) throws Exception {
        return this.zzgch.zzb(zzcxu, zzcxm, zzcjy);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzcxu zzcxu, zzcxm zzcxm, zzcjy zzcjy) throws Exception {
        this.zzgch.zza(zzcxu, zzcxm, zzcjy);
    }
}
