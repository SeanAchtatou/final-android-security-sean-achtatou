package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;

final class zzade extends zzadc {
    zzade() {
    }

    public final String zzg(@Nullable String str, String str2) {
        return str != null ? str : str2;
    }
}
