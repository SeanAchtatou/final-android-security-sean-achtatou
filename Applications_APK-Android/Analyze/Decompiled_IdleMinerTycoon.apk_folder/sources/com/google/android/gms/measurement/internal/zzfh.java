package com.google.android.gms.measurement.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

final class zzfh<V> extends FutureTask<V> implements Comparable<zzfh> {
    private final String zzns;
    private final /* synthetic */ zzfc zznt;
    private final long zznw = zzfc.zznj.getAndIncrement();
    final boolean zznx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzfh(zzfc zzfc, Callable callable, boolean z, String str) {
        super(callable);
        this.zznt = zzfc;
        Preconditions.checkNotNull(str);
        this.zzns = str;
        this.zznx = z;
        if (this.zznw == Long.MAX_VALUE) {
            zzfc.zzab().zzgk().zzao("Tasks index overflow");
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzfh(zzfc zzfc, Runnable runnable, boolean z, String str) {
        super(runnable, null);
        this.zznt = zzfc;
        Preconditions.checkNotNull(str);
        this.zzns = str;
        this.zznx = false;
        if (this.zznw == Long.MAX_VALUE) {
            zzfc.zzab().zzgk().zzao("Tasks index overflow");
        }
    }

    /* access modifiers changed from: protected */
    public final void setException(Throwable th) {
        this.zznt.zzab().zzgk().zza(this.zzns, th);
        if (th instanceof zzff) {
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
        }
        super.setException(th);
    }

    public final /* synthetic */ int compareTo(@NonNull Object obj) {
        zzfh zzfh = (zzfh) obj;
        if (this.zznx != zzfh.zznx) {
            return this.zznx ? -1 : 1;
        }
        if (this.zznw < zzfh.zznw) {
            return -1;
        }
        if (this.zznw > zzfh.zznw) {
            return 1;
        }
        this.zznt.zzab().zzgl().zza("Two tasks share the same index. index", Long.valueOf(this.zznw));
        return 0;
    }
}
