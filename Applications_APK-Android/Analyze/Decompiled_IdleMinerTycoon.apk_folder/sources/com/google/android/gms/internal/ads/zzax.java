package com.google.android.gms.internal.ads;

import android.support.annotation.GuardedBy;
import android.support.annotation.Nullable;
import com.ironsource.sdk.constants.Constants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.UnsupportedEncodingException;

public class zzax extends zzr<String> {
    private final Object mLock = new Object();
    @Nullable
    @GuardedBy("mLock")
    private zzaa<String> zzcm;

    public zzax(int i, String str, zzaa<String> zzaa, @Nullable zzz zzz) {
        super(i, str, zzz);
        this.zzcm = zzaa;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzh */
    public void zza(String str) {
        zzaa<String> zzaa;
        synchronized (this.mLock) {
            zzaa = this.zzcm;
        }
        if (zzaa != null) {
            zzaa.zzb(str);
        }
    }

    /* access modifiers changed from: protected */
    public final zzy<String> zza(zzp zzp) {
        String str;
        try {
            byte[] bArr = zzp.data;
            String str2 = "ISO-8859-1";
            String str3 = zzp.zzab.get(HttpRequest.HEADER_CONTENT_TYPE);
            if (str3 != null) {
                String[] split = str3.split(";", 0);
                int i = 1;
                while (true) {
                    if (i >= split.length) {
                        break;
                    }
                    String[] split2 = split[i].trim().split(Constants.RequestParameters.EQUAL, 0);
                    if (split2.length == 2 && split2[0].equals(HttpRequest.PARAM_CHARSET)) {
                        str2 = split2[1];
                        break;
                    }
                    i++;
                }
            }
            str = new String(bArr, str2);
        } catch (UnsupportedEncodingException unused) {
            str = new String(zzp.data);
        }
        return zzy.zza(str, zzaq.zzb(zzp));
    }
}
