package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzk;
import com.helpshift.analytics.AnalyticsEventKey;
import com.ironsource.sdk.ISNAdView.ISNAdViewConstants;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TJAdUnitConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbml implements zzalm<zzbmp> {
    private final zzty zzffp;
    private final Context zzlj;
    private final PowerManager zzyt;

    public zzbml(Context context, zzty zzty) {
        this.zzlj = context;
        this.zzffp = zzty;
        this.zzyt = (PowerManager) context.getSystemService("power");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* renamed from: zza */
    public final JSONObject zzj(zzbmp zzbmp) throws JSONException {
        JSONObject jSONObject;
        boolean z;
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        if (zzbmp.zzfge == null) {
            jSONObject = new JSONObject();
        } else {
            zzud zzud = zzbmp.zzfge;
            if (this.zzffp.zzmh() != null) {
                boolean z2 = zzud.zzbtp;
                JSONObject jSONObject3 = new JSONObject();
                JSONObject put = jSONObject3.put("afmaVersion", this.zzffp.zzmg()).put("activeViewJSON", this.zzffp.zzmh()).put("timestamp", zzbmp.timestamp).put("adFormat", this.zzffp.zzmf()).put("hashCode", this.zzffp.zzmi());
                zzty zzty = this.zzffp;
                JSONObject put2 = put.put("isMraid", false).put("isStopped", false).put("isPaused", zzbmp.zzfgb).put("isNative", this.zzffp.zzmj());
                if (Build.VERSION.SDK_INT >= 20) {
                    z = this.zzyt.isInteractive();
                } else {
                    z = this.zzyt.isScreenOn();
                }
                put2.put("isScreenOn", z).put("appMuted", zzk.zzll().zzpr()).put("appVolume", (double) zzk.zzll().zzpq()).put(Constants.RequestParameters.DEVICE_VOLUME, (double) zzaya.zzba(this.zzlj.getApplicationContext()));
                Rect rect = new Rect();
                Display defaultDisplay = ((WindowManager) this.zzlj.getSystemService("window")).getDefaultDisplay();
                rect.right = defaultDisplay.getWidth();
                rect.bottom = defaultDisplay.getHeight();
                jSONObject3.put("windowVisibility", zzud.zzza).put("isAttachedToWindow", z2).put("viewBox", new JSONObject().put(TJAdUnitConstants.String.TOP, zzud.zzbtq.top).put(TJAdUnitConstants.String.BOTTOM, zzud.zzbtq.bottom).put(TJAdUnitConstants.String.LEFT, zzud.zzbtq.left).put(TJAdUnitConstants.String.RIGHT, zzud.zzbtq.right)).put("adBox", new JSONObject().put(TJAdUnitConstants.String.TOP, zzud.zzbtr.top).put(TJAdUnitConstants.String.BOTTOM, zzud.zzbtr.bottom).put(TJAdUnitConstants.String.LEFT, zzud.zzbtr.left).put(TJAdUnitConstants.String.RIGHT, zzud.zzbtr.right)).put("globalVisibleBox", new JSONObject().put(TJAdUnitConstants.String.TOP, zzud.zzbts.top).put(TJAdUnitConstants.String.BOTTOM, zzud.zzbts.bottom).put(TJAdUnitConstants.String.LEFT, zzud.zzbts.left).put(TJAdUnitConstants.String.RIGHT, zzud.zzbts.right)).put("globalVisibleBoxVisible", zzud.zzbtt).put("localVisibleBox", new JSONObject().put(TJAdUnitConstants.String.TOP, zzud.zzbtu.top).put(TJAdUnitConstants.String.BOTTOM, zzud.zzbtu.bottom).put(TJAdUnitConstants.String.LEFT, zzud.zzbtu.left).put(TJAdUnitConstants.String.RIGHT, zzud.zzbtu.right)).put("localVisibleBoxVisible", zzud.zzbtv).put("hitBox", new JSONObject().put(TJAdUnitConstants.String.TOP, zzud.zzbtw.top).put(TJAdUnitConstants.String.BOTTOM, zzud.zzbtw.bottom).put(TJAdUnitConstants.String.LEFT, zzud.zzbtw.left).put(TJAdUnitConstants.String.RIGHT, zzud.zzbtw.right)).put("screenDensity", (double) this.zzlj.getResources().getDisplayMetrics().density);
                jSONObject3.put(ISNAdViewConstants.IS_VISIBLE_KEY, zzbmp.zzbtk);
                if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcql)).booleanValue()) {
                    JSONArray jSONArray2 = new JSONArray();
                    if (zzud.zzbty != null) {
                        for (Rect next : zzud.zzbty) {
                            jSONArray2.put(new JSONObject().put(TJAdUnitConstants.String.TOP, next.top).put(TJAdUnitConstants.String.BOTTOM, next.bottom).put(TJAdUnitConstants.String.LEFT, next.left).put(TJAdUnitConstants.String.RIGHT, next.right));
                        }
                    }
                    jSONObject3.put("scrollableContainerBoxes", jSONArray2);
                }
                if (!TextUtils.isEmpty(zzbmp.zzfgd)) {
                    jSONObject3.put("doneReasonCode", AnalyticsEventKey.URL);
                }
                jSONObject = jSONObject3;
            } else {
                throw new JSONException("Active view Info cannot be null.");
            }
        }
        jSONArray.put(jSONObject);
        jSONObject2.put("units", jSONArray);
        return jSONObject2;
    }
}
