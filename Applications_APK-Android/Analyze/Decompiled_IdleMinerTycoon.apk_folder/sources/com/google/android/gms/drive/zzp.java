package com.google.android.gms.drive;

import com.google.android.gms.drive.ExecutionOptions;

public final class zzp extends ExecutionOptions.Builder {
    private boolean zzar = true;

    public final /* synthetic */ ExecutionOptions build() {
        zzn();
        return new zzn(this.zzao, this.zzap, this.zzaq, this.zzar);
    }

    public final /* synthetic */ ExecutionOptions.Builder setConflictStrategy(int i) {
        super.setConflictStrategy(i);
        return this;
    }

    public final /* synthetic */ ExecutionOptions.Builder setNotifyOnCompletion(boolean z) {
        super.setNotifyOnCompletion(z);
        return this;
    }

    public final /* synthetic */ ExecutionOptions.Builder setTrackingTag(String str) {
        super.setTrackingTag(str);
        return this;
    }
}
