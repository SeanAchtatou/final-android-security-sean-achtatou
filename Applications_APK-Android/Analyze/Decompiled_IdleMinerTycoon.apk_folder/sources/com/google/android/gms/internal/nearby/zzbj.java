package com.google.android.gms.internal.nearby;

import com.google.android.gms.common.api.internal.BaseImplementation;

final /* synthetic */ class zzbj implements zzbw {
    private final long zzck;

    zzbj(long j) {
        this.zzck = j;
    }

    public final void zza(zzx zzx, BaseImplementation.ResultHolder resultHolder) {
        zzx.zza(resultHolder, this.zzck);
    }
}
