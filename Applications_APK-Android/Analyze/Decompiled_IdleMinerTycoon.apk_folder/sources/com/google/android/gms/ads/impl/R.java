package com.google.android.gms.ads.impl;

public final class R {
    private R() {
    }

    public static final class string {
        public static final int s1 = 2131689738;
        public static final int s2 = 2131689739;
        public static final int s3 = 2131689740;
        public static final int s4 = 2131689741;
        public static final int s5 = 2131689742;
        public static final int s6 = 2131689743;
        public static final int s7 = 2131689744;

        private string() {
        }
    }
}
