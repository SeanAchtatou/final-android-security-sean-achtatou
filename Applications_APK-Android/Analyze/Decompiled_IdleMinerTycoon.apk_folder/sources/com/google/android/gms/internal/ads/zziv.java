package com.google.android.gms.internal.ads;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public final class zziv implements zzid, zzio {
    private final zzkm zzamg = new zzkm(zzkj.zzaqt);
    private final zzkm zzamh = new zzkm(4);
    private final zzkm zzami = new zzkm(16);
    private final Stack<zzir> zzamj = new Stack<>();
    private int zzamk = 0;
    private long zzaml;
    private int zzamm;
    private long zzamn;
    private int zzamo;
    private zzkm zzamp;
    private int zzamq;
    private int zzamr;
    private int zzams;
    private zzif zzamt;
    private zziw[] zzamu;

    public final boolean zzfc() {
        return true;
    }

    public final void zza(zzif zzif) {
        this.zzamt = zzif;
    }

    public final void zzfh() {
        this.zzaml = 0;
        this.zzamr = 0;
        this.zzams = 0;
    }

    public final int zza(zzie zzie, zzij zzij) throws IOException, InterruptedException {
        boolean z;
        zziv zziv;
        zzir zzir;
        boolean z2;
        int i;
        zziv zziv2;
        ArrayList arrayList;
        zzix zza;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        long j;
        zzkm zzkm;
        zzkm zzkm2;
        int i9;
        int i10;
        zzij zzij2;
        zziv zziv3 = this;
        while (true) {
            zzie zzie2 = zzie;
            while (true) {
                zzij zzij3 = zzij;
                switch (zziv3.zzamk) {
                    case 0:
                        zziv zziv4 = zziv3;
                        zzie zzie3 = zzie;
                        boolean z3 = false;
                        if (zzie3.zza(zziv4.zzami.data, 0, 8, true)) {
                            zziv4.zzami.setPosition(0);
                            zziv4.zzamn = zziv4.zzami.zzge();
                            zziv4.zzamm = zziv4.zzami.readInt();
                            if (zziv4.zzamn == 1) {
                                zzie3.readFully(zziv4.zzami.data, 8, 8);
                                zziv4.zzamn = zziv4.zzami.readLong();
                                zziv4.zzaml += 16;
                                zziv4.zzamo = 16;
                            } else {
                                zziv4.zzaml += 8;
                                zziv4.zzamo = 8;
                            }
                            int i11 = zziv4.zzamm;
                            if (i11 == zziq.zzako || i11 == zziq.zzakq || i11 == zziq.zzakr || i11 == zziq.zzaks || i11 == zziq.zzakt) {
                                if (zziv4.zzamn == 1) {
                                    zziv4.zzamj.add(new zzir(zziv4.zzamm, (zziv4.zzaml + zziv4.zzamn) - ((long) zziv4.zzamo)));
                                } else {
                                    zziv4.zzamj.add(new zzir(zziv4.zzamm, (zziv4.zzaml + zziv4.zzamn) - ((long) zziv4.zzamo)));
                                }
                                zziv4.zzamk = 0;
                            } else {
                                int i12 = zziv4.zzamm;
                                if (i12 == zziq.zzakz || i12 == zziq.zzakp || i12 == zziq.zzala || i12 == zziq.zzalp || i12 == zziq.zzalq || i12 == zziq.zzalb || i12 == zziq.zzaka || i12 == zziq.zzaku || i12 == zziq.zzake || i12 == zziq.zzakc || i12 == zziq.zzals || i12 == zziq.zzalt || i12 == zziq.zzalu || i12 == zziq.zzalv || i12 == zziq.zzalw || i12 == zziq.zzalx || i12 == zziq.zzaly || i12 == zziq.zzaky) {
                                    zzkh.checkState(zziv4.zzamn < 2147483647L);
                                    zziv4.zzamp = new zzkm((int) zziv4.zzamn);
                                    System.arraycopy(zziv4.zzami.data, 0, zziv4.zzamp.data, 0, 8);
                                    zziv4.zzamk = 1;
                                } else {
                                    zziv4.zzamp = null;
                                    zziv4.zzamk = 1;
                                }
                            }
                            z3 = true;
                        }
                        if (!z3) {
                            return -1;
                        }
                        zziv3 = zziv4;
                        zzie2 = zzie3;
                        break;
                    case 1:
                        zziv3.zzamk = 0;
                        zziv3.zzaml += zziv3.zzamn - ((long) zziv3.zzamo);
                        long j2 = zziv3.zzamn - ((long) zziv3.zzamo);
                        boolean z4 = zziv3.zzamp == null && (zziv3.zzamn >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE || zziv3.zzamn > 2147483647L);
                        if (z4) {
                            zzij3.zzahv = zziv3.zzaml;
                        } else if (zziv3.zzamp != null) {
                            zzie2.readFully(zziv3.zzamp.data, zziv3.zzamo, (int) j2);
                            if (!zziv3.zzamj.isEmpty()) {
                                zziv3.zzamj.peek().zzama.add(new zzis(zziv3.zzamm, zziv3.zzamp));
                            }
                        } else {
                            zzie2.zzr((int) j2);
                        }
                        while (!zziv3.zzamj.isEmpty() && zziv3.zzamj.peek().zzalz == zziv3.zzaml) {
                            zzir pop = zziv3.zzamj.pop();
                            if (pop.type == zziq.zzako) {
                                ArrayList arrayList2 = new ArrayList();
                                int i13 = 0;
                                while (i13 < pop.zzamb.size()) {
                                    zzir zzir2 = pop.zzamb.get(i13);
                                    if (zzir2.type == zziq.zzakq && (zza = zzit.zza(zzir2, pop.zzv(zziq.zzakp))) != null && (zza.type == 1936684398 || zza.type == 1986618469)) {
                                        zzir zzw = zzir2.zzw(zziq.zzakr).zzw(zziq.zzaks).zzw(zziq.zzakt);
                                        zzkm zzkm3 = zzw.zzv(zziq.zzalw).zzamc;
                                        zzis zzv = zzw.zzv(zziq.zzalx);
                                        if (zzv == null) {
                                            zzv = zzw.zzv(zziq.zzaly);
                                        }
                                        zzkm zzkm4 = zzv.zzamc;
                                        zzkm zzkm5 = zzw.zzv(zziq.zzalv).zzamc;
                                        zzkm zzkm6 = zzw.zzv(zziq.zzals).zzamc;
                                        zzis zzv2 = zzw.zzv(zziq.zzalt);
                                        zzkm zzkm7 = zzv2 != null ? zzv2.zzamc : null;
                                        zzis zzv3 = zzw.zzv(zziq.zzalu);
                                        zzkm zzkm8 = zzv3 != null ? zzv3.zzamc : null;
                                        zzkm3.setPosition(12);
                                        int zzgg = zzkm3.zzgg();
                                        int zzgg2 = zzkm3.zzgg();
                                        int[] iArr = new int[zzgg2];
                                        long[] jArr = new long[zzgg2];
                                        z2 = z4;
                                        long[] jArr2 = new long[zzgg2];
                                        zzir = pop;
                                        int[] iArr2 = new int[zzgg2];
                                        ArrayList arrayList3 = arrayList2;
                                        zzkm4.setPosition(12);
                                        int i14 = i13;
                                        int zzgg3 = zzkm4.zzgg();
                                        zzkm5.setPosition(12);
                                        int zzgg4 = zzkm5.zzgg() - 1;
                                        zzix zzix = zza;
                                        if (zzkm5.readInt() == 1) {
                                            int zzgg5 = zzkm5.zzgg();
                                            zzkm5.zzac(4);
                                            if (zzgg4 > 0) {
                                                i2 = zzkm5.zzgg() - 1;
                                                i3 = 12;
                                            } else {
                                                i3 = 12;
                                                i2 = -1;
                                            }
                                            zzkm6.setPosition(i3);
                                            int zzgg6 = zzkm6.zzgg() - 1;
                                            int zzgg7 = zzkm6.zzgg();
                                            int zzgg8 = zzkm6.zzgg();
                                            if (zzkm8 != null) {
                                                zzkm8.setPosition(i3);
                                                i6 = zzkm8.zzgg() - 1;
                                                i5 = zzkm8.zzgg();
                                                i4 = zzkm8.readInt();
                                            } else {
                                                i6 = 0;
                                                i5 = 0;
                                                i4 = 0;
                                            }
                                            if (zzkm7 != null) {
                                                zzkm7.setPosition(i3);
                                                int zzgg9 = zzkm7.zzgg();
                                                i8 = zzkm7.zzgg() - 1;
                                                i7 = zzgg9;
                                            } else {
                                                i8 = -1;
                                                i7 = 0;
                                            }
                                            int i15 = zzgg4;
                                            if (zzv.type == zziq.zzalx) {
                                                j = zzkm4.zzge();
                                            } else {
                                                j = zzkm4.zzgh();
                                            }
                                            zzkm zzkm9 = zzkm4;
                                            zzkm zzkm10 = zzkm5;
                                            int i16 = i2;
                                            int i17 = zzgg8;
                                            int i18 = i4;
                                            int i19 = i8;
                                            int i20 = 0;
                                            int i21 = 0;
                                            int i22 = zzgg5;
                                            int i23 = zzgg6;
                                            int i24 = i22;
                                            int i25 = zzgg7;
                                            long j3 = 0;
                                            while (i20 < zzgg2) {
                                                jArr2[i20] = j;
                                                iArr[i20] = zzgg == 0 ? zzkm3.zzgg() : zzgg;
                                                int[] iArr3 = iArr;
                                                long[] jArr3 = jArr2;
                                                jArr[i20] = j3 + ((long) i18);
                                                iArr2[i20] = zzkm7 == null ? 1 : 0;
                                                if (i20 == i19) {
                                                    iArr2[i20] = 1;
                                                    i7--;
                                                    if (i7 > 0) {
                                                        i19 = zzkm7.zzgg() - 1;
                                                    }
                                                }
                                                j3 += (long) i17;
                                                i25--;
                                                if (i25 == 0 && i23 > 0) {
                                                    i23--;
                                                    i25 = zzkm6.zzgg();
                                                    i17 = zzkm6.zzgg();
                                                }
                                                if (zzkm8 != null && i5 - 1 == 0 && i6 > 0) {
                                                    i6--;
                                                    i5 = zzkm8.zzgg();
                                                    i18 = zzkm8.readInt();
                                                }
                                                i24--;
                                                if (i24 == 0) {
                                                    int i26 = i21 + 1;
                                                    if (i26 < zzgg3) {
                                                        zzkm = zzkm7;
                                                        if (zzv.type == zziq.zzalx) {
                                                            j = zzkm9.zzge();
                                                        } else {
                                                            j = zzkm9.zzgh();
                                                        }
                                                    } else {
                                                        zzkm = zzkm7;
                                                    }
                                                    int i27 = i16;
                                                    if (i26 == i27) {
                                                        i22 = zzkm10.zzgg();
                                                        i9 = i27;
                                                        zzkm2 = zzkm10;
                                                        zzkm2.zzac(4);
                                                        i15--;
                                                        if (i15 > 0) {
                                                            i9 = zzkm2.zzgg() - 1;
                                                        }
                                                    } else {
                                                        i9 = i27;
                                                        zzkm2 = zzkm10;
                                                    }
                                                    if (i26 < zzgg3) {
                                                        i21 = i26;
                                                        i24 = i22;
                                                    } else {
                                                        i21 = i26;
                                                    }
                                                    i16 = i9;
                                                } else {
                                                    zzkm = zzkm7;
                                                    zzkm2 = zzkm10;
                                                    j += (long) iArr3[i20];
                                                }
                                                i20++;
                                                zzkm10 = zzkm2;
                                                iArr = iArr3;
                                                jArr2 = jArr3;
                                                zzkm7 = zzkm;
                                            }
                                            int[] iArr4 = iArr;
                                            long[] jArr4 = jArr2;
                                            zzix zzix2 = zzix;
                                            zzkq.zza(jArr, 1000000, zzix2.zzcs);
                                            zzkh.checkArgument(i7 == 0);
                                            zzkh.checkArgument(i25 == 0);
                                            zzkh.checkArgument(i24 == 0);
                                            zzkh.checkArgument(i23 == 0);
                                            zzkh.checkArgument(i6 == 0);
                                            zziz zziz = new zziz(jArr4, iArr4, jArr, iArr2);
                                            if (zziz.zzand != 0) {
                                                zziv2 = this;
                                                i = i14;
                                                zziw zziw = new zziw(zzix2, zziz, zziv2.zzamt.zzs(i));
                                                zziw.zzamx.zza(zzix2.zzame);
                                                arrayList = arrayList3;
                                                arrayList.add(zziw);
                                            } else {
                                                arrayList = arrayList3;
                                                i = i14;
                                                zziv2 = this;
                                            }
                                        } else {
                                            throw new IllegalStateException(String.valueOf("stsc first chunk must be 1"));
                                        }
                                    } else {
                                        z2 = z4;
                                        zzir = pop;
                                        i = i13;
                                        zziv2 = zziv3;
                                        arrayList = arrayList2;
                                    }
                                    i13 = i + 1;
                                    arrayList2 = arrayList;
                                    zziv3 = zziv2;
                                    z4 = z2;
                                    pop = zzir;
                                }
                                z = z4;
                                zziv = zziv3;
                                zziv.zzamu = (zziw[]) arrayList2.toArray(new zziw[0]);
                                zziv.zzamt.zzfi();
                                zziv.zzamt.zza(zziv);
                                zziv.zzamk = 2;
                            } else {
                                z = z4;
                                zzir zzir3 = pop;
                                zziv = zziv3;
                                if (!zziv.zzamj.isEmpty()) {
                                    zziv.zzamj.peek().zzamb.add(zzir3);
                                }
                            }
                            zziv3 = zziv;
                            z4 = z;
                        }
                        boolean z5 = z4;
                        zziv zziv5 = zziv3;
                        if (z5) {
                            return 1;
                        }
                        zziv3 = zziv5;
                        break;
                    default:
                        zziv zziv6 = zziv3;
                        zzie zzie4 = zzie2;
                        long j4 = Long.MAX_VALUE;
                        int i28 = -1;
                        for (int i29 = 0; i29 < zziv6.zzamu.length; i29++) {
                            zziw zziw2 = zziv6.zzamu[i29];
                            int i30 = zziw2.zzamy;
                            if (i30 != zziw2.zzamw.zzand) {
                                long j5 = zziw2.zzamw.zzahq[i30];
                                if (j5 < j4) {
                                    i28 = i29;
                                    j4 = j5;
                                }
                            }
                        }
                        if (i28 == -1) {
                            return -1;
                        }
                        zziw zziw3 = zziv6.zzamu[i28];
                        int i31 = zziw3.zzamy;
                        long j6 = zziw3.zzamw.zzahq[i31];
                        long position = (j6 - zzie.getPosition()) + ((long) zziv6.zzamr);
                        if (position < 0) {
                            i10 = 1;
                            zzij2 = zzij;
                        } else if (position >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
                            zzij2 = zzij;
                            i10 = 1;
                        } else {
                            zzie4.zzr((int) position);
                            zziv6.zzamq = zziw3.zzamw.zzahp[i31];
                            if (zziw3.zzamv.zzamf != -1) {
                                byte[] bArr = zziv6.zzamh.data;
                                bArr[0] = 0;
                                bArr[1] = 0;
                                bArr[2] = 0;
                                int i32 = zziw3.zzamv.zzamf;
                                int i33 = 4 - zziw3.zzamv.zzamf;
                                while (zziv6.zzamr < zziv6.zzamq) {
                                    if (zziv6.zzams == 0) {
                                        zzie4.readFully(zziv6.zzamh.data, i33, i32);
                                        zziv6.zzamh.setPosition(0);
                                        zziv6.zzams = zziv6.zzamh.zzgg();
                                        zziv6.zzamg.setPosition(0);
                                        zziw3.zzamx.zza(zziv6.zzamg, 4);
                                        zziv6.zzamr += 4;
                                        zziv6.zzamq += i33;
                                    } else {
                                        int zza2 = zziw3.zzamx.zza(zzie4, zziv6.zzams);
                                        zziv6.zzamr += zza2;
                                        zziv6.zzams -= zza2;
                                    }
                                }
                            } else {
                                while (zziv6.zzamr < zziv6.zzamq) {
                                    int zza3 = zziw3.zzamx.zza(zzie4, zziv6.zzamq - zziv6.zzamr);
                                    zziv6.zzamr += zza3;
                                    zziv6.zzams -= zza3;
                                }
                            }
                            zziw3.zzamx.zza(zziw3.zzamw.zzane[i31], zziw3.zzamw.zzajr[i31], zziv6.zzamq, 0, null);
                            zziw3.zzamy++;
                            zziv6.zzamr = 0;
                            zziv6.zzams = 0;
                            return 0;
                        }
                        zzij2.zzahv = j6;
                        return i10;
                }
            }
        }
    }

    public final long zzdq(long j) {
        int i;
        long j2 = Long.MAX_VALUE;
        for (int i2 = 0; i2 < this.zzamu.length; i2++) {
            zziz zziz = this.zzamu[i2].zzamw;
            int zza = zzkq.zza(zziz.zzane, j, true, false);
            while (true) {
                if (zza >= 0) {
                    if (zziz.zzane[zza] <= j && (zziz.zzajr[zza] & 1) != 0) {
                        break;
                    }
                    zza--;
                } else {
                    zza = -1;
                    break;
                }
            }
            if (i == -1) {
                i = zzkq.zzb(zziz.zzane, j, true, false);
                while (true) {
                    if (i < zziz.zzane.length) {
                        if (zziz.zzane[i] >= j && (zziz.zzajr[i] & 1) != 0) {
                            break;
                        }
                        i++;
                    } else {
                        i = -1;
                        break;
                    }
                }
            }
            this.zzamu[i2].zzamy = i;
            long j3 = zziz.zzahq[this.zzamu[i2].zzamy];
            if (j3 < j2) {
                j2 = j3;
            }
        }
        return j2;
    }
}
