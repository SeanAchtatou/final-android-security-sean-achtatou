package com.google.android.gms.games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zzbe;
import com.google.android.gms.games.internal.zzbm;
import com.google.android.gms.internal.games.zzt;
import com.google.android.gms.tasks.Task;

public class PlayersClient extends zzt {
    public static final String EXTRA_PLAYER_SEARCH_RESULTS = "player_search_results";
    private static final PendingResultUtil.ResultConverter<Players.LoadPlayersResult, PlayerBuffer> zzdb = new zzax();
    private static final zzbm<Players.LoadPlayersResult> zzdc = new zzay();
    private static final PendingResultUtil.ResultConverter<Players.LoadPlayersResult, Player> zzdd = new zzaz();

    PlayersClient(@NonNull Context context, @NonNull Games.GamesOptions gamesOptions) {
        super(context, gamesOptions);
    }

    PlayersClient(@NonNull Activity activity, @NonNull Games.GamesOptions gamesOptions) {
        super(activity, gamesOptions);
    }

    public Task<String> getCurrentPlayerId() {
        return doRead(new zzat(this));
    }

    public Task<Player> getCurrentPlayer() {
        return doRead(new zzau(this));
    }

    public Task<AnnotatedData<Player>> loadPlayer(@NonNull String str) {
        return loadPlayer(str, false);
    }

    public Task<AnnotatedData<Player>> loadPlayer(@NonNull String str, boolean z) {
        return zzbe.zza(Games.Players.loadPlayer(asGoogleApiClient(), str, z), zzdd, zzdc);
    }

    public Task<AnnotatedData<PlayerBuffer>> loadRecentlyPlayedWithPlayers(@IntRange(from = 1, to = 25) int i, boolean z) {
        return zzbe.zzb(Games.Players.loadRecentlyPlayedWithPlayers(asGoogleApiClient(), i, z), zzdb);
    }

    public Task<AnnotatedData<PlayerBuffer>> loadMoreRecentlyPlayedWithPlayers(@IntRange(from = 1, to = 25) int i) {
        return zzbe.zzb(Games.Players.loadMoreRecentlyPlayedWithPlayers(asGoogleApiClient(), i), zzdb);
    }

    public Task<Intent> getCompareProfileIntent(@NonNull Player player) {
        return doRead(new zzav(this, player));
    }

    public Task<Intent> getPlayerSearchIntent() {
        return doRead(new zzaw(this));
    }
}
