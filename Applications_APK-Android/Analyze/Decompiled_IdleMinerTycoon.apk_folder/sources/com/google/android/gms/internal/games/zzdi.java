package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdi extends zzdk {
    private final /* synthetic */ String zzey;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdi(zzcz zzcz, String str, GoogleApiClient googleApiClient, String str2) {
        super(str, googleApiClient);
        this.zzey = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzf(this, this.zzey);
    }
}
