package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzbp extends zzbu {
    private final /* synthetic */ String zzha;
    private final /* synthetic */ String zzkl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbp(zzbn zzbn, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient, null);
        this.zzkl = str;
        this.zzha = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzkl, this.zzha);
    }
}
