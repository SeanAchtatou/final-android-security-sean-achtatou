package com.google.android.gms.internal.nearby;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.ListenerHolder;

final class zzct extends zzcy {
    private final /* synthetic */ String zzcv;
    private final /* synthetic */ ListenerHolder zzdh;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzct(zzca zzca, GoogleApiClient googleApiClient, String str, ListenerHolder listenerHolder) {
        super(googleApiClient, null);
        this.zzcv = str;
        this.zzdh = listenerHolder;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzx) anyClient).zza(this, this.zzcv, this.zzdh);
    }
}
