package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveFile;

final class zzgf extends zzl {
    private final BaseImplementation.ResultHolder<DriveApi.DriveContentsResult> zzdv;
    private final DriveFile.DownloadProgressListener zzia;

    zzgf(BaseImplementation.ResultHolder<DriveApi.DriveContentsResult> resultHolder, DriveFile.DownloadProgressListener downloadProgressListener) {
        this.zzdv = resultHolder;
        this.zzia = downloadProgressListener;
    }

    public final void zza(Status status) throws RemoteException {
        this.zzdv.setResult(new zzal(status, null));
    }

    public final void zza(zzfb zzfb) throws RemoteException {
        this.zzdv.setResult(new zzal(zzfb.zzhf ? new Status(-1) : Status.RESULT_SUCCESS, new zzbi(zzfb.zzeq)));
    }

    public final void zza(zzff zzff) throws RemoteException {
        if (this.zzia != null) {
            this.zzia.onProgress(zzff.zzhi, zzff.zzhj);
        }
    }
}
