package com.google.android.gms.internal.nearby;

import android.os.ParcelFileDescriptor;

public final class zzfj {
    private final zzfh zzdz = new zzfh();

    public final zzfj zzb(long j) {
        long unused = this.zzdz.id = j;
        return this;
    }

    public final zzfj zzb(byte[] bArr) {
        byte[] unused = this.zzdz.zzy = bArr;
        return this;
    }

    public final zzfj zzc(long j) {
        long unused = this.zzdz.zzdx = j;
        return this;
    }

    public final zzfj zzc(ParcelFileDescriptor parcelFileDescriptor) {
        ParcelFileDescriptor unused = this.zzdz.zzdv = parcelFileDescriptor;
        return this;
    }

    public final zzfj zzd(int i) {
        int unused = this.zzdz.type = i;
        return this;
    }

    public final zzfj zzd(ParcelFileDescriptor parcelFileDescriptor) {
        ParcelFileDescriptor unused = this.zzdz.zzdy = parcelFileDescriptor;
        return this;
    }

    public final zzfj zze(String str) {
        String unused = this.zzdz.zzdw = str;
        return this;
    }

    public final zzfh zzr() {
        return this.zzdz;
    }
}
