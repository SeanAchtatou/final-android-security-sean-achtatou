package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class zzil {
    private final byte[] buffer;
    private int zzadp;
    private int zzadq = 64;
    private int zzadr = 67108864;
    private int zzady;
    private int zzaea;
    private int zzaeb = Integer.MAX_VALUE;
    private final int zzaog;
    private final int zzaoh;
    private int zzaoi;
    private int zzaoj;
    private zzeb zzaok;

    public static zzil zzj(byte[] bArr, int i, int i2) {
        return new zzil(bArr, 0, i2);
    }

    public final int zzsg() throws IOException {
        if (this.zzaoj == this.zzaoi) {
            this.zzaea = 0;
            return 0;
        }
        this.zzaea = zzta();
        if (this.zzaea != 0) {
            return this.zzaea;
        }
        throw new zzit("Protocol message contained an invalid tag (zero).");
    }

    private final void zzat(int i) throws zzit {
        if (this.zzaea != i) {
            throw new zzit("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final boolean zzau(int i) throws IOException {
        int zzsg;
        switch (i & 7) {
            case 0:
                zzta();
                return true;
            case 1:
                zztf();
                zztf();
                zztf();
                zztf();
                zztf();
                zztf();
                zztf();
                zztf();
                return true;
            case 2:
                zzay(zzta());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                zztf();
                zztf();
                zztf();
                zztf();
                return true;
            default:
                throw new zzit("Protocol message tag had invalid wire type.");
        }
        do {
            zzsg = zzsg();
            if (zzsg != 0) {
            }
            zzat(((i >>> 3) << 3) | 4);
            return true;
        } while (zzau(zzsg));
        zzat(((i >>> 3) << 3) | 4);
        return true;
    }

    public final boolean zzsm() throws IOException {
        return zzta() != 0;
    }

    public final String readString() throws IOException {
        int zzta = zzta();
        if (zzta < 0) {
            throw zzit.zzxe();
        } else if (zzta <= this.zzaoi - this.zzaoj) {
            String str = new String(this.buffer, this.zzaoj, zzta, zziu.UTF_8);
            this.zzaoj += zzta;
            return str;
        } else {
            throw zzit.zzxd();
        }
    }

    public final void zza(zziw zziw) throws IOException {
        int zzta = zzta();
        if (this.zzadp >= this.zzadq) {
            throw new zzit("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
        } else if (zzta >= 0) {
            int i = zzta + this.zzaoj;
            int i2 = this.zzaeb;
            if (i <= i2) {
                this.zzaeb = i;
                zzte();
                this.zzadp++;
                zziw.zza(this);
                zzat(0);
                this.zzadp--;
                this.zzaeb = i2;
                zzte();
                return;
            }
            throw zzit.zzxd();
        } else {
            throw zzit.zzxe();
        }
    }

    public final int zzta() throws IOException {
        byte zztf = zztf();
        if (zztf >= 0) {
            return zztf;
        }
        byte b = zztf & Byte.MAX_VALUE;
        byte zztf2 = zztf();
        if (zztf2 >= 0) {
            return b | (zztf2 << 7);
        }
        byte b2 = b | ((zztf2 & Byte.MAX_VALUE) << 7);
        byte zztf3 = zztf();
        if (zztf3 >= 0) {
            return b2 | (zztf3 << 14);
        }
        byte b3 = b2 | ((zztf3 & Byte.MAX_VALUE) << 14);
        byte zztf4 = zztf();
        if (zztf4 >= 0) {
            return b3 | (zztf4 << 21);
        }
        byte b4 = b3 | ((zztf4 & Byte.MAX_VALUE) << 21);
        byte zztf5 = zztf();
        byte b5 = b4 | (zztf5 << 28);
        if (zztf5 >= 0) {
            return b5;
        }
        for (int i = 0; i < 5; i++) {
            if (zztf() >= 0) {
                return b5;
            }
        }
        throw zzit.zzxf();
    }

    public final long zztb() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte zztf = zztf();
            j |= ((long) (zztf & Byte.MAX_VALUE)) << i;
            if ((zztf & 128) == 0) {
                return j;
            }
        }
        throw zzit.zzxf();
    }

    private zzil(byte[] bArr, int i, int i2) {
        this.buffer = bArr;
        this.zzaog = 0;
        int i3 = i2 + 0;
        this.zzaoi = i3;
        this.zzaoh = i3;
        this.zzaoj = 0;
    }

    public final <T extends zzey<T, ?>> T zza(zzgr zzgr) throws IOException {
        try {
            if (this.zzaok == null) {
                this.zzaok = zzeb.zzd(this.buffer, this.zzaog, this.zzaoh);
            }
            int zzsx = this.zzaok.zzsx();
            int i = this.zzaoj - this.zzaog;
            if (zzsx <= i) {
                this.zzaok.zzay(i - zzsx);
                this.zzaok.zzav(this.zzadq - this.zzadp);
                T t = (zzey) this.zzaok.zza(zzgr, zzel.zztq());
                zzau(this.zzaea);
                return t;
            }
            throw new IOException(String.format("CodedInputStream read ahead of CodedInputByteBufferNano: %s > %s", Integer.valueOf(zzsx), Integer.valueOf(i)));
        } catch (zzfi e) {
            throw new zzit("", e);
        }
    }

    private final void zzte() {
        this.zzaoi += this.zzady;
        int i = this.zzaoi;
        if (i > this.zzaeb) {
            this.zzady = i - this.zzaeb;
            this.zzaoi -= this.zzady;
            return;
        }
        this.zzady = 0;
    }

    public final int getPosition() {
        return this.zzaoj - this.zzaog;
    }

    public final byte[] zzt(int i, int i2) {
        if (i2 == 0) {
            return zzix.zzaph;
        }
        byte[] bArr = new byte[i2];
        System.arraycopy(this.buffer, this.zzaog + i, bArr, 0, i2);
        return bArr;
    }

    /* access modifiers changed from: package-private */
    public final void zzu(int i, int i2) {
        if (i > this.zzaoj - this.zzaog) {
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i);
            sb.append(" is beyond current ");
            sb.append(this.zzaoj - this.zzaog);
            throw new IllegalArgumentException(sb.toString());
        } else if (i >= 0) {
            this.zzaoj = this.zzaog + i;
            this.zzaea = i2;
        } else {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    private final byte zztf() throws IOException {
        if (this.zzaoj != this.zzaoi) {
            byte[] bArr = this.buffer;
            int i = this.zzaoj;
            this.zzaoj = i + 1;
            return bArr[i];
        }
        throw zzit.zzxd();
    }

    private final void zzay(int i) throws IOException {
        if (i < 0) {
            throw zzit.zzxe();
        } else if (this.zzaoj + i > this.zzaeb) {
            zzay(this.zzaeb - this.zzaoj);
            throw zzit.zzxd();
        } else if (i <= this.zzaoi - this.zzaoj) {
            this.zzaoj += i;
        } else {
            throw zzit.zzxd();
        }
    }
}
