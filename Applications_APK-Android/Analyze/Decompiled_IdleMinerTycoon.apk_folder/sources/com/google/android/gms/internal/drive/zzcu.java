package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.common.internal.ICancelToken;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzcu extends RegisterListenerMethod<zzaw, OpenFileCallback> {
    private final /* synthetic */ DriveFile zzfq;
    private final /* synthetic */ int zzfr;
    private final /* synthetic */ zzg zzfs;
    private final /* synthetic */ ListenerHolder zzft;
    private final /* synthetic */ zzch zzfu;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcu(zzch zzch, ListenerHolder listenerHolder, DriveFile driveFile, int i, zzg zzg, ListenerHolder listenerHolder2) {
        super(listenerHolder);
        this.zzfu = zzch;
        this.zzfq = driveFile;
        this.zzfr = i;
        this.zzfs = zzg;
        this.zzft = listenerHolder2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void registerListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        this.zzfs.setCancelToken(ICancelToken.Stub.asInterface(((zzeo) ((zzaw) anyClient).getService()).zza(new zzgd(this.zzfq.getDriveId(), this.zzfr, 0), new zzdk(this.zzfu, this.zzfs, this.zzft)).zzgq));
        taskCompletionSource.setResult(null);
    }
}
