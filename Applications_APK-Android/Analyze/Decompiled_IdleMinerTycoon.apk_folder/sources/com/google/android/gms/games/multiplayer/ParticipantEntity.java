package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import java.util.ArrayList;
import java.util.List;

@RetainForClient
@UsedByReflection("GamesClientImpl.java")
@SafeParcelable.Class(creator = "ParticipantEntityCreator")
@SafeParcelable.Reserved({1000})
public final class ParticipantEntity extends GamesDowngradeableSafeParcel implements Participant {
    public static final Parcelable.Creator<ParticipantEntity> CREATOR = new zza();
    @SafeParcelable.Field(getter = "getStatus", id = 5)
    private final int status;
    @Nullable
    @SafeParcelable.Field(getter = "getIconImageUrl", id = 11)
    private final String zzac;
    @Nullable
    @SafeParcelable.Field(getter = "getHiResImageUrl", id = 12)
    private final String zzad;
    @Nullable
    @SafeParcelable.Field(getter = "getPlayer", id = 8)
    private final PlayerEntity zzfj;
    @SafeParcelable.Field(getter = "getDisplayName", id = 2)
    private final String zzn;
    @SafeParcelable.Field(getter = "getParticipantId", id = 1)
    private final String zzpg;
    @SafeParcelable.Field(getter = "getClientAddress", id = 6)
    private final String zzph;
    @SafeParcelable.Field(getter = "isConnectedToRoom", id = 7)
    private final boolean zzpi;
    @SafeParcelable.Field(getter = "getCapabilities", id = 9)
    private final int zzpj;
    @Nullable
    @SafeParcelable.Field(getter = "getResult", id = 10)
    private final ParticipantResult zzpk;
    @Nullable
    @SafeParcelable.Field(getter = "getIconImageUri", id = 3)
    private final Uri zzr;
    @Nullable
    @SafeParcelable.Field(getter = "getHiResImageUri", id = 4)
    private final Uri zzs;

    static final class zza extends zzc {
        zza() {
        }

        public final ParticipantEntity zzf(Parcel parcel) {
            Uri uri;
            Uri uri2;
            if (ParticipantEntity.zzb(ParticipantEntity.getUnparcelClientVersion()) || ParticipantEntity.canUnparcelSafely(ParticipantEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z = false;
            boolean z2 = parcel.readInt() > 0;
            if (parcel.readInt() > 0) {
                z = true;
            }
            PlayerEntity createFromParcel = z ? PlayerEntity.CREATOR.createFromParcel(parcel) : null;
            if (readString3 == null) {
                uri = null;
            } else {
                uri = Uri.parse(readString3);
            }
            if (readString4 == null) {
                uri2 = null;
            } else {
                uri2 = Uri.parse(readString4);
            }
            return new ParticipantEntity(readString, readString2, uri, uri2, readInt, readString5, z2, createFromParcel, 7, null, null, null);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    public static ArrayList<ParticipantEntity> zza(@NonNull List<Participant> list) {
        ParticipantEntity participantEntity;
        ArrayList<ParticipantEntity> arrayList = new ArrayList<>(list.size());
        for (Participant next : list) {
            if (next instanceof ParticipantEntity) {
                participantEntity = (ParticipantEntity) next;
            } else {
                participantEntity = new ParticipantEntity(next);
            }
            arrayList.add(participantEntity);
        }
        return arrayList;
    }

    public final Participant freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ParticipantEntity(com.google.android.gms.games.multiplayer.Participant r3) {
        /*
            r2 = this;
            com.google.android.gms.games.Player r0 = r3.getPlayer()
            if (r0 != 0) goto L_0x0008
            r0 = 0
            goto L_0x000e
        L_0x0008:
            com.google.android.gms.games.PlayerEntity r1 = new com.google.android.gms.games.PlayerEntity
            r1.<init>(r0)
            r0 = r1
        L_0x000e:
            r2.<init>(r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.multiplayer.ParticipantEntity.<init>(com.google.android.gms.games.multiplayer.Participant):void");
    }

    private ParticipantEntity(Participant participant, @Nullable PlayerEntity playerEntity) {
        this.zzpg = participant.getParticipantId();
        this.zzn = participant.getDisplayName();
        this.zzr = participant.getIconImageUri();
        this.zzs = participant.getHiResImageUri();
        this.status = participant.getStatus();
        this.zzph = participant.zzdn();
        this.zzpi = participant.isConnectedToRoom();
        this.zzfj = playerEntity;
        this.zzpj = participant.getCapabilities();
        this.zzpk = participant.getResult();
        this.zzac = participant.getIconImageUrl();
        this.zzad = participant.getHiResImageUrl();
    }

    @SafeParcelable.Constructor
    ParticipantEntity(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) String str2, @Nullable @SafeParcelable.Param(id = 3) Uri uri, @Nullable @SafeParcelable.Param(id = 4) Uri uri2, @SafeParcelable.Param(id = 5) int i, @SafeParcelable.Param(id = 6) String str3, @SafeParcelable.Param(id = 7) boolean z, @Nullable @SafeParcelable.Param(id = 8) PlayerEntity playerEntity, @SafeParcelable.Param(id = 9) int i2, @Nullable @SafeParcelable.Param(id = 10) ParticipantResult participantResult, @Nullable @SafeParcelable.Param(id = 11) String str4, @Nullable @SafeParcelable.Param(id = 12) String str5) {
        this.zzpg = str;
        this.zzn = str2;
        this.zzr = uri;
        this.zzs = uri2;
        this.status = i;
        this.zzph = str3;
        this.zzpi = z;
        this.zzfj = playerEntity;
        this.zzpj = i2;
        this.zzpk = participantResult;
        this.zzac = str4;
        this.zzad = str5;
    }

    public final int getStatus() {
        return this.status;
    }

    public final String zzdn() {
        return this.zzph;
    }

    public final boolean isConnectedToRoom() {
        return this.zzpi;
    }

    public final String getDisplayName() {
        if (this.zzfj == null) {
            return this.zzn;
        }
        return this.zzfj.getDisplayName();
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        if (this.zzfj != null) {
            this.zzfj.getDisplayName(charArrayBuffer);
        } else if (this.zzn == null) {
            charArrayBuffer.sizeCopied = 0;
        } else {
            DataUtils.copyStringToBuffer(this.zzn, charArrayBuffer);
        }
    }

    @Nullable
    public final Uri getIconImageUri() {
        if (this.zzfj == null) {
            return this.zzr;
        }
        return this.zzfj.getIconImageUri();
    }

    @Nullable
    public final String getIconImageUrl() {
        if (this.zzfj == null) {
            return this.zzac;
        }
        return this.zzfj.getIconImageUrl();
    }

    @Nullable
    public final Uri getHiResImageUri() {
        if (this.zzfj == null) {
            return this.zzs;
        }
        return this.zzfj.getHiResImageUri();
    }

    public final String getHiResImageUrl() {
        if (this.zzfj == null) {
            return this.zzad;
        }
        return this.zzfj.getHiResImageUrl();
    }

    public final String getParticipantId() {
        return this.zzpg;
    }

    public final Player getPlayer() {
        return this.zzfj;
    }

    public final ParticipantResult getResult() {
        return this.zzpk;
    }

    public final int getCapabilities() {
        return this.zzpj;
    }

    public final void setShouldDowngrade(boolean z) {
        super.setShouldDowngrade(z);
        if (this.zzfj != null) {
            this.zzfj.setShouldDowngrade(z);
        }
    }

    public final int hashCode() {
        return zza(this);
    }

    static int zza(Participant participant) {
        return Objects.hashCode(participant.getPlayer(), Integer.valueOf(participant.getStatus()), participant.zzdn(), Boolean.valueOf(participant.isConnectedToRoom()), participant.getDisplayName(), participant.getIconImageUri(), participant.getHiResImageUri(), Integer.valueOf(participant.getCapabilities()), participant.getResult(), participant.getParticipantId());
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    static boolean zza(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return Objects.equal(participant2.getPlayer(), participant.getPlayer()) && Objects.equal(Integer.valueOf(participant2.getStatus()), Integer.valueOf(participant.getStatus())) && Objects.equal(participant2.zzdn(), participant.zzdn()) && Objects.equal(Boolean.valueOf(participant2.isConnectedToRoom()), Boolean.valueOf(participant.isConnectedToRoom())) && Objects.equal(participant2.getDisplayName(), participant.getDisplayName()) && Objects.equal(participant2.getIconImageUri(), participant.getIconImageUri()) && Objects.equal(participant2.getHiResImageUri(), participant.getHiResImageUri()) && Objects.equal(Integer.valueOf(participant2.getCapabilities()), Integer.valueOf(participant.getCapabilities())) && Objects.equal(participant2.getResult(), participant.getResult()) && Objects.equal(participant2.getParticipantId(), participant.getParticipantId());
    }

    public final String toString() {
        return zzb(this);
    }

    static String zzb(Participant participant) {
        return Objects.toStringHelper(participant).add("ParticipantId", participant.getParticipantId()).add("Player", participant.getPlayer()).add("Status", Integer.valueOf(participant.getStatus())).add("ClientAddress", participant.zzdn()).add("ConnectedToRoom", Boolean.valueOf(participant.isConnectedToRoom())).add("DisplayName", participant.getDisplayName()).add("IconImage", participant.getIconImageUri()).add("IconImageUrl", participant.getIconImageUrl()).add("HiResImage", participant.getHiResImageUri()).add("HiResImageUrl", participant.getHiResImageUrl()).add("Capabilities", Integer.valueOf(participant.getCapabilities())).add("Result", participant.getResult()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        if (!shouldDowngrade()) {
            int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeString(parcel, 1, getParticipantId(), false);
            SafeParcelWriter.writeString(parcel, 2, getDisplayName(), false);
            SafeParcelWriter.writeParcelable(parcel, 3, getIconImageUri(), i, false);
            SafeParcelWriter.writeParcelable(parcel, 4, getHiResImageUri(), i, false);
            SafeParcelWriter.writeInt(parcel, 5, getStatus());
            SafeParcelWriter.writeString(parcel, 6, this.zzph, false);
            SafeParcelWriter.writeBoolean(parcel, 7, isConnectedToRoom());
            SafeParcelWriter.writeParcelable(parcel, 8, getPlayer(), i, false);
            SafeParcelWriter.writeInt(parcel, 9, this.zzpj);
            SafeParcelWriter.writeParcelable(parcel, 10, getResult(), i, false);
            SafeParcelWriter.writeString(parcel, 11, getIconImageUrl(), false);
            SafeParcelWriter.writeString(parcel, 12, getHiResImageUrl(), false);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
            return;
        }
        parcel.writeString(this.zzpg);
        parcel.writeString(this.zzn);
        String str = null;
        parcel.writeString(this.zzr == null ? null : this.zzr.toString());
        if (this.zzs != null) {
            str = this.zzs.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.status);
        parcel.writeString(this.zzph);
        parcel.writeInt(this.zzpi ? 1 : 0);
        if (this.zzfj == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        this.zzfj.writeToParcel(parcel, i);
    }
}
