package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class zzgo<T> implements zzgx<T> {
    private final zzgi zzakn;
    private final boolean zzako;
    private final zzhp<?, ?> zzakx;
    private final zzen<?> zzaky;

    private zzgo(zzhp<?, ?> zzhp, zzen<?> zzen, zzgi zzgi) {
        this.zzakx = zzhp;
        this.zzako = zzen.zze(zzgi);
        this.zzaky = zzen;
        this.zzakn = zzgi;
    }

    static <T> zzgo<T> zza(zzhp<?, ?> zzhp, zzen<?> zzen, zzgi zzgi) {
        return new zzgo<>(zzhp, zzen, zzgi);
    }

    public final T newInstance() {
        return this.zzakn.zzup().zzuf();
    }

    public final boolean equals(T t, T t2) {
        if (!this.zzakx.zzx(t).equals(this.zzakx.zzx(t2))) {
            return false;
        }
        if (this.zzako) {
            return this.zzaky.zzh(t).equals(this.zzaky.zzh(t2));
        }
        return true;
    }

    public final int hashCode(T t) {
        int hashCode = this.zzakx.zzx(t).hashCode();
        return this.zzako ? (hashCode * 53) + this.zzaky.zzh(t).hashCode() : hashCode;
    }

    public final void zzc(T t, T t2) {
        zzgz.zza(this.zzakx, t, t2);
        if (this.zzako) {
            zzgz.zza(this.zzaky, t, t2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzim.zza(int, java.lang.Object):void
     arg types: [int, com.google.android.gms.internal.measurement.zzdp]
     candidates:
      com.google.android.gms.internal.measurement.zzim.zza(int, double):void
      com.google.android.gms.internal.measurement.zzim.zza(int, float):void
      com.google.android.gms.internal.measurement.zzim.zza(int, long):void
      com.google.android.gms.internal.measurement.zzim.zza(int, com.google.android.gms.internal.measurement.zzdp):void
      com.google.android.gms.internal.measurement.zzim.zza(int, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.measurement.zzim.zza(int, java.lang.Object):void */
    public final void zza(T t, zzim zzim) throws IOException {
        Iterator<Map.Entry<?, Object>> it = this.zzaky.zzh(t).iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            zzeq zzeq = (zzeq) next.getKey();
            if (zzeq.zztx() != zzij.MESSAGE || zzeq.zzty() || zzeq.zztz()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzfl) {
                zzim.zza(zzeq.zzlg(), (Object) ((zzfl) next).zzve().zzrs());
            } else {
                zzim.zza(zzeq.zzlg(), next.getValue());
            }
        }
        zzhp<?, ?> zzhp = this.zzakx;
        zzhp.zzc(zzhp.zzx(t), zzim);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.google.android.gms.internal.measurement.zzey$zze} */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0098 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r9, byte[] r10, int r11, int r12, com.google.android.gms.internal.measurement.zzdk r13) throws java.io.IOException {
        /*
            r8 = this;
            r0 = r9
            com.google.android.gms.internal.measurement.zzey r0 = (com.google.android.gms.internal.measurement.zzey) r0
            com.google.android.gms.internal.measurement.zzhs r1 = r0.zzahz
            com.google.android.gms.internal.measurement.zzhs r2 = com.google.android.gms.internal.measurement.zzhs.zzwq()
            if (r1 != r2) goto L_0x0011
            com.google.android.gms.internal.measurement.zzhs r1 = com.google.android.gms.internal.measurement.zzhs.zzwr()
            r0.zzahz = r1
        L_0x0011:
            com.google.android.gms.internal.measurement.zzey$zzb r9 = (com.google.android.gms.internal.measurement.zzey.zzb) r9
            r9.zzuq()
            r9 = 0
            r0 = r9
        L_0x0018:
            if (r11 >= r12) goto L_0x00a3
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r10, r11, r13)
            int r2 = r13.zzada
            r11 = 11
            r3 = 2
            if (r2 == r11) goto L_0x0051
            r11 = r2 & 7
            if (r11 != r3) goto L_0x004c
            com.google.android.gms.internal.measurement.zzen<?> r11 = r8.zzaky
            com.google.android.gms.internal.measurement.zzel r0 = r13.zzadd
            com.google.android.gms.internal.measurement.zzgi r3 = r8.zzakn
            int r5 = r2 >>> 3
            java.lang.Object r11 = r11.zza(r0, r3, r5)
            r0 = r11
            com.google.android.gms.internal.measurement.zzey$zze r0 = (com.google.android.gms.internal.measurement.zzey.zze) r0
            if (r0 != 0) goto L_0x0043
            r3 = r10
            r5 = r12
            r6 = r1
            r7 = r13
            int r11 = com.google.android.gms.internal.measurement.zzdl.zza(r2, r3, r4, r5, r6, r7)
            goto L_0x0018
        L_0x0043:
            com.google.android.gms.internal.measurement.zzgt.zzvy()
            java.lang.NoSuchMethodError r9 = new java.lang.NoSuchMethodError
            r9.<init>()
            throw r9
        L_0x004c:
            int r11 = com.google.android.gms.internal.measurement.zzdl.zza(r2, r10, r4, r12, r13)
            goto L_0x0018
        L_0x0051:
            r11 = 0
            r11 = r9
            r2 = 0
        L_0x0054:
            if (r4 >= r12) goto L_0x0098
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r10, r4, r13)
            int r5 = r13.zzada
            int r6 = r5 >>> 3
            r7 = r5 & 7
            switch(r6) {
                case 2: goto L_0x007a;
                case 3: goto L_0x0064;
                default: goto L_0x0063;
            }
        L_0x0063:
            goto L_0x008f
        L_0x0064:
            if (r0 != 0) goto L_0x0071
            if (r7 != r3) goto L_0x008f
            int r4 = com.google.android.gms.internal.measurement.zzdl.zze(r10, r4, r13)
            java.lang.Object r11 = r13.zzadc
            com.google.android.gms.internal.measurement.zzdp r11 = (com.google.android.gms.internal.measurement.zzdp) r11
            goto L_0x0054
        L_0x0071:
            com.google.android.gms.internal.measurement.zzgt.zzvy()
            java.lang.NoSuchMethodError r9 = new java.lang.NoSuchMethodError
            r9.<init>()
            throw r9
        L_0x007a:
            if (r7 != 0) goto L_0x008f
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r10, r4, r13)
            int r2 = r13.zzada
            com.google.android.gms.internal.measurement.zzen<?> r0 = r8.zzaky
            com.google.android.gms.internal.measurement.zzel r5 = r13.zzadd
            com.google.android.gms.internal.measurement.zzgi r6 = r8.zzakn
            java.lang.Object r0 = r0.zza(r5, r6, r2)
            com.google.android.gms.internal.measurement.zzey$zze r0 = (com.google.android.gms.internal.measurement.zzey.zze) r0
            goto L_0x0054
        L_0x008f:
            r6 = 12
            if (r5 == r6) goto L_0x0098
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r5, r10, r4, r12, r13)
            goto L_0x0054
        L_0x0098:
            if (r11 == 0) goto L_0x00a0
            int r2 = r2 << 3
            r2 = r2 | r3
            r1.zzb(r2, r11)
        L_0x00a0:
            r11 = r4
            goto L_0x0018
        L_0x00a3:
            if (r11 != r12) goto L_0x00a6
            return
        L_0x00a6:
            com.google.android.gms.internal.measurement.zzfi r9 = com.google.android.gms.internal.measurement.zzfi.zzva()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgo.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.measurement.zzdk):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzdp):void
     arg types: [?, int, com.google.android.gms.internal.measurement.zzdp]
     candidates:
      com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, long):void
      com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzdp):void */
    public final void zza(Object obj, zzgy zzgy, zzel zzel) throws IOException {
        boolean z;
        zzhp<?, ?> zzhp = this.zzakx;
        zzen<?> zzen = this.zzaky;
        Object zzy = zzhp.zzy(obj);
        zzeo<?> zzi = zzen.zzi(obj);
        do {
            try {
                if (zzgy.zzsy() == Integer.MAX_VALUE) {
                    zzhp.zzf(obj, zzy);
                    return;
                }
                int tag = zzgy.getTag();
                if (tag == 11) {
                    Object obj2 = null;
                    zzdp zzdp = null;
                    int i = 0;
                    while (zzgy.zzsy() != Integer.MAX_VALUE) {
                        int tag2 = zzgy.getTag();
                        if (tag2 == 16) {
                            i = zzgy.zzsp();
                            obj2 = zzen.zza(zzel, this.zzakn, i);
                        } else if (tag2 == 26) {
                            if (obj2 != null) {
                                zzen.zza(zzgy, obj2, zzel, zzi);
                            } else {
                                zzdp = zzgy.zzso();
                            }
                        } else if (!zzgy.zzsz()) {
                            break;
                        }
                    }
                    if (zzgy.getTag() != 12) {
                        throw zzfi.zzux();
                    } else if (zzdp != null) {
                        if (obj2 != null) {
                            zzen.zza(zzdp, obj2, zzel, zzi);
                        } else {
                            zzhp.zza((Object) zzy, i, zzdp);
                        }
                    }
                } else if ((tag & 7) == 2) {
                    Object zza = zzen.zza(zzel, this.zzakn, tag >>> 3);
                    if (zza != null) {
                        zzen.zza(zzgy, zza, zzel, zzi);
                    } else {
                        z = zzhp.zza(zzy, zzgy);
                        continue;
                    }
                } else {
                    z = zzgy.zzsz();
                    continue;
                }
                z = true;
                continue;
            } finally {
                zzhp.zzf(obj, zzy);
            }
        } while (z);
    }

    public final void zzj(T t) {
        this.zzakx.zzj(t);
        this.zzaky.zzj(t);
    }

    public final boolean zzv(T t) {
        return this.zzaky.zzh(t).isInitialized();
    }

    public final int zzt(T t) {
        zzhp<?, ?> zzhp = this.zzakx;
        int zzz = zzhp.zzz(zzhp.zzx(t)) + 0;
        return this.zzako ? zzz + this.zzaky.zzh(t).zzts() : zzz;
    }
}
