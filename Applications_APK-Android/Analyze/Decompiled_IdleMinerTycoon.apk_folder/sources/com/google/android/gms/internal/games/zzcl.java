package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.snapshot.SnapshotMetadata;

final class zzcl extends zzcp {
    private final /* synthetic */ SnapshotMetadata zzkv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcl(zzch zzch, GoogleApiClient googleApiClient, SnapshotMetadata snapshotMetadata) {
        super(googleApiClient, null);
        this.zzkv = snapshotMetadata;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzi(this, this.zzkv.getSnapshotId());
    }
}
