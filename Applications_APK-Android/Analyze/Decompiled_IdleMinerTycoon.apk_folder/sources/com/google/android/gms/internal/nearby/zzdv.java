package com.google.android.gms.internal.nearby;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.facebook.ads.AdError;
import com.ironsource.mediationsdk.utils.IronSourceConstants;

public final class zzdv extends zza implements zzdu {
    zzdv(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.nearby.internal.connection.INearbyConnectionService");
    }

    public final void zza(zzcz zzcz) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzcz);
        transactAndReadExceptionReturnVoid(AdError.INTERSTITIAL_AD_TIMEOUT, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzfm zzfm) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzfm);
        transactAndReadExceptionReturnVoid(2007, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzfq zzfq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzfq);
        transactAndReadExceptionReturnVoid(IronSourceConstants.IS_INSTANCE_OPENED, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzfu zzfu) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzfu);
        transactAndReadExceptionReturnVoid(AdError.REMOTE_ADS_SERVICE_ERROR, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzfy zzfy) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzfy);
        transactAndReadExceptionReturnVoid(2001, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgc zzgc) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgc);
        transactAndReadExceptionReturnVoid(2003, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgg zzgg) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgg);
        transactAndReadExceptionReturnVoid(2002, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgj zzgj) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgj);
        transactAndReadExceptionReturnVoid(2010, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgm zzgm) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgm);
        transactAndReadExceptionReturnVoid(2004, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzm zzm) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzm);
        transactAndReadExceptionReturnVoid(2006, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzq zzq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzq);
        transactAndReadExceptionReturnVoid(2012, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzu zzu) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzu);
        transactAndReadExceptionReturnVoid(2011, obtainAndWriteInterfaceToken);
    }
}
