package com.google.android.gms.internal.ads;

import android.content.Context;
import com.ironsource.sdk.constants.Constants;
import java.io.IOException;

public final class zzjt implements zzkf {
    private final zzkf zzapz;
    private final zzkf zzaqa;
    private final zzkf zzaqb;
    private zzkf zzaqc;

    public zzjt(Context context, String str) {
        this(context, null, str, false);
    }

    private zzjt(Context context, zzke zzke, String str, boolean z) {
        this(context, null, new zzjs(str, null, null, 8000, 8000, false));
    }

    private zzjt(Context context, zzke zzke, zzkf zzkf) {
        this.zzapz = (zzkf) zzkh.checkNotNull(zzkf);
        this.zzaqa = new zzjv(null);
        this.zzaqb = new zzjm(context, null);
    }

    public final long zza(zzjq zzjq) throws IOException {
        zzkh.checkState(this.zzaqc == null);
        String scheme = zzjq.uri.getScheme();
        if ("http".equals(scheme) || "https".equals(scheme)) {
            this.zzaqc = this.zzapz;
        } else if (Constants.ParametersKeys.FILE.equals(scheme)) {
            if (zzjq.uri.getPath().startsWith("/android_asset/")) {
                this.zzaqc = this.zzaqb;
            } else {
                this.zzaqc = this.zzaqa;
            }
        } else if ("asset".equals(scheme)) {
            this.zzaqc = this.zzaqb;
        } else {
            throw new zzju(scheme);
        }
        return this.zzaqc.zza(zzjq);
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        return this.zzaqc.read(bArr, i, i2);
    }

    public final void close() throws IOException {
        if (this.zzaqc != null) {
            try {
                this.zzaqc.close();
            } finally {
                this.zzaqc = null;
            }
        }
    }
}
