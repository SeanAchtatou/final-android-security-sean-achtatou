package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzbr extends zzbw {
    private final /* synthetic */ boolean zzjz;
    private final /* synthetic */ String[] zzkn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbr(zzbn zzbn, GoogleApiClient googleApiClient, boolean z, String[] strArr) {
        super(googleApiClient, null);
        this.zzjz = z;
        this.zzkn = strArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzjz, this.zzkn);
    }
}
