package com.google.android.gms.internal.ads;

import java.util.HashMap;

final class zzbga implements Runnable {
    private final /* synthetic */ String zzdli;
    private final /* synthetic */ String zzehu;
    private final /* synthetic */ zzbft zzehy;
    private final /* synthetic */ long zzeic;

    zzbga(zzbft zzbft, String str, String str2, long j) {
        this.zzehy = zzbft;
        this.zzdli = str;
        this.zzehu = str2;
        this.zzeic = j;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put("event", "precacheComplete");
        hashMap.put("src", this.zzdli);
        hashMap.put("cachedSrc", this.zzehu);
        hashMap.put("totalDuration", Long.toString(this.zzeic));
        this.zzehy.zza("onPrecacheEvent", hashMap);
    }
}
