package com.google.android.gms.internal.nearby;

import com.google.android.gms.common.api.internal.ListenerHolder;

abstract class zzau<T> implements ListenerHolder.Notifier<T> {
    private zzau() {
    }

    public void onNotifyListenerFailed() {
    }
}
