package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "HandleClientLifecycleEventRequestCreator")
public final class zzj extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzj> CREATOR = new zzk();
    @SafeParcelable.VersionField(id = 1)
    private final int versionCode;
    @Nullable
    @SafeParcelable.Field(id = 2)
    @Deprecated
    private final ClientAppContext zzhi;
    @SafeParcelable.Field(id = 3)
    private final int zzhj;

    public zzj(int i) {
        this(1, null, i);
    }

    @SafeParcelable.Constructor
    zzj(@SafeParcelable.Param(id = 1) int i, @SafeParcelable.Param(id = 2) ClientAppContext clientAppContext, @SafeParcelable.Param(id = 3) int i2) {
        this.versionCode = i;
        this.zzhi = clientAppContext;
        this.zzhj = i2;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzhi, i, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zzhj);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
