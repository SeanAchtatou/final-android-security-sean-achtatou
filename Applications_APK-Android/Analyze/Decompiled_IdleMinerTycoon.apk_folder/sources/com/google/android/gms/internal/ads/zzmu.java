package com.google.android.gms.internal.ads;

final class zzmu implements zzmn {
    private final /* synthetic */ zzms zzaxw;

    private zzmu(zzms zzms) {
        this.zzaxw = zzms;
    }

    public final void zzag(int i) {
        this.zzaxw.zzaxq.zzah(i);
        zzms.zzag(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzms.zza(com.google.android.gms.internal.ads.zzms, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzms, int]
     candidates:
      com.google.android.gms.internal.ads.zzms.zza(com.google.android.gms.internal.ads.zzpg, com.google.android.gms.internal.ads.zzlh):int
      com.google.android.gms.internal.ads.zzms.zza(int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzpe.zza(com.google.android.gms.internal.ads.zzpg, com.google.android.gms.internal.ads.zzlh):int
      com.google.android.gms.internal.ads.zzks.zza(int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzks.zza(com.google.android.gms.internal.ads.zzlh[], long):void
      com.google.android.gms.internal.ads.zzkx.zza(int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzms.zza(com.google.android.gms.internal.ads.zzms, boolean):boolean */
    public final void zzgt() {
        zzms.zzhv();
        boolean unused = this.zzaxw.zzaxv = true;
    }

    public final void zze(int i, long j, long j2) {
        this.zzaxw.zzaxq.zzd(i, j, j2);
        zzms.zzc(i, j, j2);
    }
}
