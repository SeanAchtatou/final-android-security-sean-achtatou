package com.google.android.gms.internal.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import org.json.JSONObject;

public interface zzasc {
    void zza(Context context, String str, @NonNull JSONObject jSONObject);
}
