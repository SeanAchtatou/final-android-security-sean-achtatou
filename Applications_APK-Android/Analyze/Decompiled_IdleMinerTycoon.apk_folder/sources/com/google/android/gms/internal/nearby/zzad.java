package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.zze;

final class zzad extends zzau<ConnectionLifecycleCallback> {
    private final /* synthetic */ zzef zzbl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzad(zzz zzz, zzef zzef) {
        super();
        this.zzbl = zzef;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        this.zzbl.zzg();
        new zze(this.zzbl.getQuality());
    }
}
