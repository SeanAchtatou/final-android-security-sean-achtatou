package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.internal.ads.zzbp;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicReference;

public final class zzem extends zzfk {
    private static zzfl<String> zzzf = new zzfl<>();
    private final Context zzzg;

    public zzem(zzdy zzdy, String str, String str2, zzbp.zza.C0029zza zza, int i, int i2, Context context) {
        super(zzdy, str, str2, zza, i, 29);
        this.zzzg = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcg.zza(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.google.android.gms.internal.ads.zzcg.zza(java.lang.String, boolean):byte[]
      com.google.android.gms.internal.ads.zzcg.zza(byte[], boolean):java.lang.String */
    /* access modifiers changed from: protected */
    public final void zzcx() throws IllegalAccessException, InvocationTargetException {
        this.zzzm.zzaa("E");
        AtomicReference<String> zzar = zzzf.zzar(this.zzzg.getPackageName());
        if (zzar.get() == null) {
            synchronized (zzar) {
                if (zzar.get() == null) {
                    zzar.set((String) this.zzzw.invoke(null, this.zzzg));
                }
            }
        }
        String str = zzar.get();
        synchronized (this.zzzm) {
            this.zzzm.zzaa(zzcg.zza(str.getBytes(), true));
        }
    }
}
