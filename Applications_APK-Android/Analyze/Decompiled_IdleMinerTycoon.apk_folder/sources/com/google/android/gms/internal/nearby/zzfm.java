package com.google.android.gms.internal.nearby;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "RejectConnectionRequestParamsCreator")
@SafeParcelable.Reserved({1000})
public final class zzfm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzfm> CREATOR = new zzfp();
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getResultListenerAsBinder", id = 1, type = "android.os.IBinder")
    public zzdz zzar;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getRemoteEndpointId", id = 2)
    public String zzat;

    private zzfm() {
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [android.os.IInterface] */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzfm(@android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) android.os.IBinder r3, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) java.lang.String r4) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x0004
            r3 = 0
            goto L_0x0018
        L_0x0004:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IResultListener"
            android.os.IInterface r0 = r3.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.nearby.zzdz
            if (r1 == 0) goto L_0x0012
            r3 = r0
            com.google.android.gms.internal.nearby.zzdz r3 = (com.google.android.gms.internal.nearby.zzdz) r3
            goto L_0x0018
        L_0x0012:
            com.google.android.gms.internal.nearby.zzeb r0 = new com.google.android.gms.internal.nearby.zzeb
            r0.<init>(r3)
            r3 = r0
        L_0x0018:
            r2.<init>(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.nearby.zzfm.<init>(android.os.IBinder, java.lang.String):void");
    }

    private zzfm(@Nullable zzdz zzdz, String str) {
        this.zzar = zzdz;
        this.zzat = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzfm) {
            zzfm zzfm = (zzfm) obj;
            return Objects.equal(this.zzar, zzfm.zzar) && Objects.equal(this.zzat, zzfm.zzat);
        }
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzar, this.zzat);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeIBinder(parcel, 1, this.zzar == null ? null : this.zzar.asBinder(), false);
        SafeParcelWriter.writeString(parcel, 2, this.zzat, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
