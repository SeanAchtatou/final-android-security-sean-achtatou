package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;

final class zzac extends zzau<ConnectionLifecycleCallback> {
    private final /* synthetic */ zzep zzbk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzac(zzz zzz, zzep zzep) {
        super();
        this.zzbk = zzep;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((ConnectionLifecycleCallback) obj).onDisconnected(this.zzbk.zzg());
    }
}
