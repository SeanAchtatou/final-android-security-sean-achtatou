package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.measurement.zzp;

final class zzib implements Runnable {
    private final /* synthetic */ zzp zzdi;
    private final /* synthetic */ zzn zzpg;
    private final /* synthetic */ zzhv zzrd;

    zzib(zzhv zzhv, zzn zzn, zzp zzp) {
        this.zzrd = zzhv;
        this.zzpg = zzn;
        this.zzdi = zzp;
    }

    public final void run() {
        String str;
        RemoteException e;
        try {
            zzdx zzd = this.zzrd.zzrf;
            if (zzd == null) {
                this.zzrd.zzab().zzgk().zzao("Failed to get app instance id");
                this.zzrd.zzz().zzb(this.zzdi, (String) null);
                return;
            }
            str = zzd.zzc(this.zzpg);
            if (str != null) {
                try {
                    this.zzrd.zzq().zzbg(str);
                    this.zzrd.zzac().zzlq.zzau(str);
                } catch (RemoteException e2) {
                    e = e2;
                    try {
                        this.zzrd.zzab().zzgk().zza("Failed to get app instance id", e);
                        this.zzrd.zzz().zzb(this.zzdi, str);
                    } catch (Throwable th) {
                        th = th;
                        this.zzrd.zzz().zzb(this.zzdi, str);
                        throw th;
                    }
                }
            }
            this.zzrd.zzir();
            this.zzrd.zzz().zzb(this.zzdi, str);
        } catch (RemoteException e3) {
            RemoteException remoteException = e3;
            str = null;
            e = remoteException;
            this.zzrd.zzab().zzgk().zza("Failed to get app instance id", e);
            this.zzrd.zzz().zzb(this.zzdi, str);
        } catch (Throwable th2) {
            Throwable th3 = th2;
            str = null;
            th = th3;
            this.zzrd.zzz().zzb(this.zzdi, str);
            throw th;
        }
    }
}
