package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.GamesMetadata;
import com.google.android.gms.games.internal.zze;

final class zzac extends zze.zzat<GamesMetadata.LoadGamesResult> {
    zzac(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzg(DataHolder dataHolder) {
        setResult(new zze.zzy(dataHolder));
    }
}
