package com.google.android.gms.internal.nearby;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.nearby.connection.AdvertisingOptions;

@SafeParcelable.Class(creator = "StartAdvertisingParamsCreator")
@SafeParcelable.Reserved({1000})
public final class zzfy extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzfy> CREATOR = new zzgb();
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getDurationMillis", id = 5)
    public long durationMillis;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getName", id = 3)
    public String name;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getConnectionLifecycleListenerAsBinder", id = 7, type = "android.os.IBinder")
    public zzdj zzec;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getResultListenerAsBinder", id = 1, type = "android.os.IBinder")
    public zzec zzeh;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getCallbackAsBinder", id = 2, type = "android.os.IBinder")
    public zzdd zzei;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getOptions", id = 6)
    public AdvertisingOptions zzej;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getServiceId", id = 4)
    public String zzu;

    private zzfy() {
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzfy(@android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) android.os.IBinder r15, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r16, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) java.lang.String r17, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) java.lang.String r18, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) long r19, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 6) com.google.android.gms.nearby.connection.AdvertisingOptions r21, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 7) android.os.IBinder r22) {
        /*
            r14 = this;
            r0 = r15
            r1 = r16
            r2 = r22
            r3 = 0
            if (r0 != 0) goto L_0x000a
            r6 = r3
            goto L_0x001f
        L_0x000a:
            java.lang.String r4 = "com.google.android.gms.nearby.internal.connection.IStartAdvertisingResultListener"
            android.os.IInterface r4 = r15.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.nearby.zzec
            if (r5 == 0) goto L_0x0019
            r0 = r4
            com.google.android.gms.internal.nearby.zzec r0 = (com.google.android.gms.internal.nearby.zzec) r0
            r6 = r0
            goto L_0x001f
        L_0x0019:
            com.google.android.gms.internal.nearby.zzee r4 = new com.google.android.gms.internal.nearby.zzee
            r4.<init>(r15)
            r6 = r4
        L_0x001f:
            if (r1 != 0) goto L_0x0023
            r7 = r3
            goto L_0x0037
        L_0x0023:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IAdvertisingCallback"
            android.os.IInterface r0 = r1.queryLocalInterface(r0)
            boolean r4 = r0 instanceof com.google.android.gms.internal.nearby.zzdd
            if (r4 == 0) goto L_0x0031
            com.google.android.gms.internal.nearby.zzdd r0 = (com.google.android.gms.internal.nearby.zzdd) r0
        L_0x002f:
            r7 = r0
            goto L_0x0037
        L_0x0031:
            com.google.android.gms.internal.nearby.zzdf r0 = new com.google.android.gms.internal.nearby.zzdf
            r0.<init>(r1)
            goto L_0x002f
        L_0x0037:
            if (r2 != 0) goto L_0x003b
        L_0x0039:
            r13 = r3
            goto L_0x004f
        L_0x003b:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IConnectionLifecycleListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.nearby.zzdj
            if (r1 == 0) goto L_0x0049
            r3 = r0
            com.google.android.gms.internal.nearby.zzdj r3 = (com.google.android.gms.internal.nearby.zzdj) r3
            goto L_0x0039
        L_0x0049:
            com.google.android.gms.internal.nearby.zzdl r3 = new com.google.android.gms.internal.nearby.zzdl
            r3.<init>(r2)
            goto L_0x0039
        L_0x004f:
            r5 = r14
            r8 = r17
            r9 = r18
            r10 = r19
            r12 = r21
            r5.<init>(r6, r7, r8, r9, r10, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.nearby.zzfy.<init>(android.os.IBinder, android.os.IBinder, java.lang.String, java.lang.String, long, com.google.android.gms.nearby.connection.AdvertisingOptions, android.os.IBinder):void");
    }

    private zzfy(@Nullable zzec zzec2, @Nullable zzdd zzdd, String str, String str2, long j, AdvertisingOptions advertisingOptions, @Nullable zzdj zzdj) {
        this.zzeh = zzec2;
        this.zzei = zzdd;
        this.name = str;
        this.zzu = str2;
        this.durationMillis = j;
        this.zzej = advertisingOptions;
        this.zzec = zzdj;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzfy) {
            zzfy zzfy = (zzfy) obj;
            return Objects.equal(this.zzeh, zzfy.zzeh) && Objects.equal(this.zzei, zzfy.zzei) && Objects.equal(this.name, zzfy.name) && Objects.equal(this.zzu, zzfy.zzu) && Objects.equal(Long.valueOf(this.durationMillis), Long.valueOf(zzfy.durationMillis)) && Objects.equal(this.zzej, zzfy.zzej) && Objects.equal(this.zzec, zzfy.zzec);
        }
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzeh, this.zzei, this.name, this.zzu, Long.valueOf(this.durationMillis), this.zzej, this.zzec);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 1, this.zzeh == null ? null : this.zzeh.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzei == null ? null : this.zzei.asBinder(), false);
        SafeParcelWriter.writeString(parcel, 3, this.name, false);
        SafeParcelWriter.writeString(parcel, 4, this.zzu, false);
        SafeParcelWriter.writeLong(parcel, 5, this.durationMillis);
        SafeParcelWriter.writeParcelable(parcel, 6, this.zzej, i, false);
        if (this.zzec != null) {
            iBinder = this.zzec.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 7, iBinder, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
