package com.google.android.gms.internal.ads;

import com.mintegral.msdk.base.entity.CampaignEx;

public final class zzcfh implements zzdti<zzcfl> {
    private static final zzcfh zzfup = new zzcfh();

    public static zzcfh zzajw() {
        return zzfup;
    }

    public final /* synthetic */ Object get() {
        return (zzcfl) zzdto.zza(new zzcfl(CampaignEx.JSON_KEY_PRE_CLICK, zzczs.SIGNALS, zzczs.RENDERER), "Cannot return null from a non-@Nullable @Provides method");
    }
}
