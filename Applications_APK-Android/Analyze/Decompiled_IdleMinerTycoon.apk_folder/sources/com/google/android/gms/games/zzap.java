package com.google.android.gms.games;

import android.os.Parcelable;

public class zzap implements Parcelable.Creator<PlayerEntity> {
    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v6, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v7, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v8, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: zzc */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.gms.games.PlayerEntity createFromParcel(android.os.Parcel r38) {
        /*
            r37 = this;
            r0 = r38
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.validateObjectHeader(r38)
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = -1
            r13 = r2
            r16 = r13
            r32 = r16
            r9 = r5
            r10 = r9
            r11 = r10
            r12 = r11
            r18 = r12
            r19 = r18
            r20 = r19
            r21 = r20
            r22 = r21
            r25 = r22
            r26 = r25
            r27 = r26
            r28 = r27
            r29 = r28
            r30 = r29
            r35 = r6
            r15 = 0
            r23 = 0
            r24 = 0
            r31 = 0
            r34 = 0
        L_0x0036:
            int r2 = r38.dataPosition()
            if (r2 >= r1) goto L_0x00ed
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readHeader(r38)
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.getFieldId(r2)
            switch(r3) {
                case 1: goto L_0x00e7;
                case 2: goto L_0x00e1;
                case 3: goto L_0x00d6;
                case 4: goto L_0x00cb;
                case 5: goto L_0x00c5;
                case 6: goto L_0x00bf;
                case 7: goto L_0x00b9;
                case 8: goto L_0x00b3;
                case 9: goto L_0x00ae;
                case 10: goto L_0x0047;
                case 11: goto L_0x0047;
                case 12: goto L_0x0047;
                case 13: goto L_0x0047;
                case 14: goto L_0x00a9;
                case 15: goto L_0x009e;
                case 16: goto L_0x0093;
                case 17: goto L_0x0047;
                case 18: goto L_0x008e;
                case 19: goto L_0x0089;
                case 20: goto L_0x0084;
                case 21: goto L_0x007f;
                case 22: goto L_0x0074;
                case 23: goto L_0x006f;
                case 24: goto L_0x0064;
                case 25: goto L_0x005f;
                case 26: goto L_0x005a;
                case 27: goto L_0x0055;
                case 28: goto L_0x0050;
                case 29: goto L_0x004b;
                default: goto L_0x0047;
            }
        L_0x0047:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.skipUnknownField(r0, r2)
            goto L_0x0036
        L_0x004b:
            long r35 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0036
        L_0x0050:
            boolean r34 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0036
        L_0x0055:
            long r32 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0036
        L_0x005a:
            int r31 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readInt(r0, r2)
            goto L_0x0036
        L_0x005f:
            java.lang.String r30 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x0064:
            android.os.Parcelable$Creator r3 = android.net.Uri.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r29 = r2
            android.net.Uri r29 = (android.net.Uri) r29
            goto L_0x0036
        L_0x006f:
            java.lang.String r28 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x0074:
            android.os.Parcelable$Creator r3 = android.net.Uri.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r27 = r2
            android.net.Uri r27 = (android.net.Uri) r27
            goto L_0x0036
        L_0x007f:
            java.lang.String r26 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x0084:
            java.lang.String r25 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x0089:
            boolean r24 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0036
        L_0x008e:
            boolean r23 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0036
        L_0x0093:
            android.os.Parcelable$Creator<com.google.android.gms.games.PlayerLevelInfo> r3 = com.google.android.gms.games.PlayerLevelInfo.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r22 = r2
            com.google.android.gms.games.PlayerLevelInfo r22 = (com.google.android.gms.games.PlayerLevelInfo) r22
            goto L_0x0036
        L_0x009e:
            android.os.Parcelable$Creator<com.google.android.gms.games.internal.player.MostRecentGameInfoEntity> r3 = com.google.android.gms.games.internal.player.MostRecentGameInfoEntity.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r21 = r2
            com.google.android.gms.games.internal.player.MostRecentGameInfoEntity r21 = (com.google.android.gms.games.internal.player.MostRecentGameInfoEntity) r21
            goto L_0x0036
        L_0x00a9:
            java.lang.String r20 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x00ae:
            java.lang.String r19 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x00b3:
            java.lang.String r18 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x00b9:
            long r16 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0036
        L_0x00bf:
            int r15 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readInt(r0, r2)
            goto L_0x0036
        L_0x00c5:
            long r13 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0036
        L_0x00cb:
            android.os.Parcelable$Creator r3 = android.net.Uri.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r12 = r2
            android.net.Uri r12 = (android.net.Uri) r12
            goto L_0x0036
        L_0x00d6:
            android.os.Parcelable$Creator r3 = android.net.Uri.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r11 = r2
            android.net.Uri r11 = (android.net.Uri) r11
            goto L_0x0036
        L_0x00e1:
            java.lang.String r10 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x00e7:
            java.lang.String r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createString(r0, r2)
            goto L_0x0036
        L_0x00ed:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ensureAtEnd(r0, r1)
            com.google.android.gms.games.PlayerEntity r0 = new com.google.android.gms.games.PlayerEntity
            r8 = r0
            r8.<init>(r9, r10, r11, r12, r13, r15, r16, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r34, r35)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.zzap.createFromParcel(android.os.Parcel):com.google.android.gms.games.PlayerEntity");
    }

    public /* synthetic */ Object[] newArray(int i) {
        return new PlayerEntity[i];
    }
}
