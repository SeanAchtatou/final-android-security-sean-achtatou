package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Stack;

final class zzny implements zzob {
    private final byte[] zzanf = new byte[8];
    private final Stack<zzoa> zzang = new Stack<>();
    private int zzanj;
    private int zzank;
    private long zzanl;
    private final zzoi zzazr = new zzoi();
    private zzoc zzazs;

    zzny() {
    }

    public final void zza(zzoc zzoc) {
        this.zzazs = zzoc;
    }

    public final void reset() {
        this.zzanj = 0;
        this.zzang.clear();
        this.zzazr.reset();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01a2 A[LOOP:0: B:5:0x000c->B:66:0x01a2, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00a2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00bb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0103 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0110 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0150 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0182 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0014  */
    public final boolean zzb(com.google.android.gms.internal.ads.zzno r11) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r10 = this;
            com.google.android.gms.internal.ads.zzoc r0 = r10.zzazs
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0008
            r0 = 1
            goto L_0x0009
        L_0x0008:
            r0 = 0
        L_0x0009:
            com.google.android.gms.internal.ads.zzsk.checkState(r0)
        L_0x000c:
            java.util.Stack<com.google.android.gms.internal.ads.zzoa> r0 = r10.zzang
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x003a
            long r3 = r11.getPosition()
            java.util.Stack<com.google.android.gms.internal.ads.zzoa> r0 = r10.zzang
            java.lang.Object r0 = r0.peek()
            com.google.android.gms.internal.ads.zzoa r0 = (com.google.android.gms.internal.ads.zzoa) r0
            long r5 = r0.zzanm
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x003a
            com.google.android.gms.internal.ads.zzoc r11 = r10.zzazs
            java.util.Stack<com.google.android.gms.internal.ads.zzoa> r0 = r10.zzang
            java.lang.Object r0 = r0.pop()
            com.google.android.gms.internal.ads.zzoa r0 = (com.google.android.gms.internal.ads.zzoa) r0
            int r0 = r0.zzank
            r11.zzy(r0)
            return r1
        L_0x003a:
            int r0 = r10.zzanj
            r3 = 4
            if (r0 != 0) goto L_0x0084
            com.google.android.gms.internal.ads.zzoi r0 = r10.zzazr
            long r4 = r0.zza(r11, r1, r2, r3)
            r6 = -2
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0078
            r11.zzig()
        L_0x004e:
            byte[] r0 = r10.zzanf
            r11.zzc(r0, r2, r3)
            byte[] r0 = r10.zzanf
            byte r0 = r0[r2]
            int r0 = com.google.android.gms.internal.ads.zzoi.zzaw(r0)
            r4 = -1
            if (r0 == r4) goto L_0x0074
            if (r0 > r3) goto L_0x0074
            byte[] r4 = r10.zzanf
            long r4 = com.google.android.gms.internal.ads.zzoi.zza(r4, r0, r2)
            int r4 = (int) r4
            com.google.android.gms.internal.ads.zzoc r5 = r10.zzazs
            boolean r5 = r5.zzav(r4)
            if (r5 == 0) goto L_0x0074
            r11.zzr(r0)
            long r4 = (long) r4
            goto L_0x0078
        L_0x0074:
            r11.zzr(r1)
            goto L_0x004e
        L_0x0078:
            r6 = -1
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x007f
            return r2
        L_0x007f:
            int r0 = (int) r4
            r10.zzank = r0
            r10.zzanj = r1
        L_0x0084:
            int r0 = r10.zzanj
            if (r0 != r1) goto L_0x0095
            com.google.android.gms.internal.ads.zzoi r0 = r10.zzazr
            r4 = 8
            long r4 = r0.zza(r11, r2, r1, r4)
            r10.zzanl = r4
            r0 = 2
            r10.zzanj = r0
        L_0x0095:
            com.google.android.gms.internal.ads.zzoc r0 = r10.zzazs
            int r4 = r10.zzank
            int r0 = r0.zzx(r4)
            r4 = 8
            switch(r0) {
                case 0: goto L_0x01a2;
                case 1: goto L_0x0182;
                case 2: goto L_0x0150;
                case 3: goto L_0x0110;
                case 4: goto L_0x0103;
                case 5: goto L_0x00bb;
                default: goto L_0x00a2;
            }
        L_0x00a2:
            com.google.android.gms.internal.ads.zzlm r11 = new com.google.android.gms.internal.ads.zzlm
            r1 = 32
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r1)
            java.lang.String r1 = "Invalid element type "
            r2.append(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r11.<init>(r0)
            throw r11
        L_0x00bb:
            long r6 = r10.zzanl
            r8 = 4
            int r0 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r0 == 0) goto L_0x00e5
            long r6 = r10.zzanl
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x00ca
            goto L_0x00e5
        L_0x00ca:
            com.google.android.gms.internal.ads.zzlm r11 = new com.google.android.gms.internal.ads.zzlm
            long r0 = r10.zzanl
            r2 = 40
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Invalid float size: "
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r11.<init>(r0)
            throw r11
        L_0x00e5:
            com.google.android.gms.internal.ads.zzoc r0 = r10.zzazs
            int r4 = r10.zzank
            long r5 = r10.zzanl
            int r5 = (int) r5
            long r6 = r10.zza(r11, r5)
            if (r5 != r3) goto L_0x00f9
            int r11 = (int) r6
            float r11 = java.lang.Float.intBitsToFloat(r11)
            double r5 = (double) r11
            goto L_0x00fd
        L_0x00f9:
            double r5 = java.lang.Double.longBitsToDouble(r6)
        L_0x00fd:
            r0.zza(r4, r5)
            r10.zzanj = r2
            return r1
        L_0x0103:
            com.google.android.gms.internal.ads.zzoc r0 = r10.zzazs
            int r3 = r10.zzank
            long r4 = r10.zzanl
            int r4 = (int) r4
            r0.zza(r3, r4, r11)
            r10.zzanj = r2
            return r1
        L_0x0110:
            long r3 = r10.zzanl
            r5 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x0135
            com.google.android.gms.internal.ads.zzoc r0 = r10.zzazs
            int r3 = r10.zzank
            long r4 = r10.zzanl
            int r4 = (int) r4
            if (r4 != 0) goto L_0x0125
            java.lang.String r11 = ""
            goto L_0x012f
        L_0x0125:
            byte[] r5 = new byte[r4]
            r11.readFully(r5, r2, r4)
            java.lang.String r11 = new java.lang.String
            r11.<init>(r5)
        L_0x012f:
            r0.zza(r3, r11)
            r10.zzanj = r2
            return r1
        L_0x0135:
            com.google.android.gms.internal.ads.zzlm r11 = new com.google.android.gms.internal.ads.zzlm
            long r0 = r10.zzanl
            r2 = 41
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "String element size: "
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r11.<init>(r0)
            throw r11
        L_0x0150:
            long r6 = r10.zzanl
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x0167
            com.google.android.gms.internal.ads.zzoc r0 = r10.zzazs
            int r3 = r10.zzank
            long r4 = r10.zzanl
            int r4 = (int) r4
            long r4 = r10.zza(r11, r4)
            r0.zzc(r3, r4)
            r10.zzanj = r2
            return r1
        L_0x0167:
            com.google.android.gms.internal.ads.zzlm r11 = new com.google.android.gms.internal.ads.zzlm
            long r0 = r10.zzanl
            r2 = 42
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Invalid integer size: "
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r11.<init>(r0)
            throw r11
        L_0x0182:
            long r5 = r11.getPosition()
            long r3 = r10.zzanl
            long r3 = r3 + r5
            java.util.Stack<com.google.android.gms.internal.ads.zzoa> r11 = r10.zzang
            com.google.android.gms.internal.ads.zzoa r0 = new com.google.android.gms.internal.ads.zzoa
            int r7 = r10.zzank
            r8 = 0
            r0.<init>(r7, r3)
            r11.add(r0)
            com.google.android.gms.internal.ads.zzoc r3 = r10.zzazs
            int r4 = r10.zzank
            long r7 = r10.zzanl
            r3.zzb(r4, r5, r7)
            r10.zzanj = r2
            return r1
        L_0x01a2:
            long r3 = r10.zzanl
            int r0 = (int) r3
            r11.zzr(r0)
            r10.zzanj = r2
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzny.zzb(com.google.android.gms.internal.ads.zzno):boolean");
    }

    private final long zza(zzno zzno, int i) throws IOException, InterruptedException {
        zzno.readFully(this.zzanf, 0, i);
        long j = 0;
        for (int i2 = 0; i2 < i; i2++) {
            j = (j << 8) | ((long) (this.zzanf[i2] & 255));
        }
        return j;
    }
}
