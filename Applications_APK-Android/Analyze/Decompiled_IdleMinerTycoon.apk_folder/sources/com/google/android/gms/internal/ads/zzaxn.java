package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class zzaxn extends BroadcastReceiver {
    private final /* synthetic */ zzaxi zzdvz;

    private zzaxn(zzaxi zzaxi) {
        this.zzdvz = zzaxi;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzaxi.zza(com.google.android.gms.internal.ads.zzaxi, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzaxi, int]
     candidates:
      com.google.android.gms.internal.ads.zzaxi.zza(com.google.android.gms.internal.ads.zzaxi, java.lang.String):java.lang.String
      com.google.android.gms.internal.ads.zzaxi.zza(android.content.Context, android.content.Intent):void
      com.google.android.gms.internal.ads.zzaxi.zza(android.content.Context, android.net.Uri):void
      com.google.android.gms.internal.ads.zzaxi.zza(android.content.Context, java.lang.Throwable):void
      com.google.android.gms.internal.ads.zzaxi.zza(org.json.JSONArray, java.lang.Object):void
      com.google.android.gms.internal.ads.zzaxi.zza(android.os.Bundle, org.json.JSONObject):org.json.JSONObject
      com.google.android.gms.internal.ads.zzaxi.zza(android.view.View, android.content.Context):boolean
      com.google.android.gms.internal.ads.zzaxi.zza(com.google.android.gms.internal.ads.zzaxi, boolean):boolean */
    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            boolean unused = this.zzdvz.zzxt = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            boolean unused2 = this.zzdvz.zzxt = false;
        }
    }

    /* synthetic */ zzaxn(zzaxi zzaxi, zzaxj zzaxj) {
        this(zzaxi);
    }
}
