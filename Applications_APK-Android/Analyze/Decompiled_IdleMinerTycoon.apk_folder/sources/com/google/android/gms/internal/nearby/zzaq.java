package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.Connections;

final class zzaq extends zzau<Connections.EndpointDiscoveryListener> {
    private final /* synthetic */ zzet zzbs;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaq(zzao zzao, zzet zzet) {
        super();
        this.zzbs = zzet;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((Connections.EndpointDiscoveryListener) obj).onEndpointLost(this.zzbs.zze());
    }
}
