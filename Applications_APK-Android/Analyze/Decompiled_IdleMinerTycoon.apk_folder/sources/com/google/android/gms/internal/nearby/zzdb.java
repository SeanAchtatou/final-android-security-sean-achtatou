package com.google.android.gms.internal.nearby;

public final class zzdb {
    private final zzcz zzdi = new zzcz();

    public final zzdb zzd(String str) {
        String unused = this.zzdi.zzat = str;
        return this;
    }

    public final zzcz zzf() {
        return this.zzdi;
    }
}
