package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.MetadataChangeSet;

final class zzbu extends zzca {
    private final /* synthetic */ MetadataChangeSet zzfb;
    private final /* synthetic */ zzbs zzff;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzbu(zzbs zzbs, GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet) {
        super(googleApiClient);
        this.zzff = zzbs;
        this.zzfb = metadataChangeSet;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        zzaw zzaw = (zzaw) anyClient;
        this.zzfb.zzp().zza(zzaw.getContext());
        ((zzeo) zzaw.getService()).zza(new zzy(this.zzff.getDriveId(), this.zzfb.zzp()), new zzbw(this));
    }
}
