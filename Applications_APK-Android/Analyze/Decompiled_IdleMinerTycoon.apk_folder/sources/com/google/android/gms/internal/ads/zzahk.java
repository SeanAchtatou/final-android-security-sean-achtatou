package com.google.android.gms.internal.ads;

import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.Map;

final class zzahk implements zzaho<zzbgz> {
    zzahk() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbgz zzbgz = (zzbgz) obj;
        String str = (String) map.get("action");
        if (CampaignEx.JSON_NATIVE_VIDEO_PAUSE.equals(str)) {
            zzbgz.zzlc();
        } else if (CampaignEx.JSON_NATIVE_VIDEO_RESUME.equals(str)) {
            zzbgz.zzld();
        }
    }
}
