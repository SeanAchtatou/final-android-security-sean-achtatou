package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;

public interface zzbm<R> {
    void release(@NonNull R r);
}
