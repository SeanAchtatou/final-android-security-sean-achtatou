package com.google.android.gms.games.leaderboard;

import android.os.Bundle;

public final class zza {
    private final Bundle zznz;

    public zza(Bundle bundle) {
        this.zznz = bundle == null ? new Bundle() : bundle;
    }

    public final Bundle zzdj() {
        return this.zznz;
    }
}
