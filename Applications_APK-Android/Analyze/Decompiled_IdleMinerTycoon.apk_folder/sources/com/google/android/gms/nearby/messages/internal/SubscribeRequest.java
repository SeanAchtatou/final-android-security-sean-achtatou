package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.Strategy;

@SafeParcelable.Class(creator = "SubscribeRequestCreator")
public final class SubscribeRequest extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<SubscribeRequest> CREATOR = new zzcd();
    @SafeParcelable.VersionField(id = 1)
    private final int versionCode;
    @Nullable
    @SafeParcelable.Field(id = 8)
    @Deprecated
    private final String zzff;
    @SafeParcelable.Field(id = 13)
    @Deprecated
    private final boolean zzfg;
    @Nullable
    @SafeParcelable.Field(id = 9)
    @Deprecated
    private final String zzfj;
    @SafeParcelable.Field(id = 15)
    private final boolean zzgb;
    @SafeParcelable.Field(id = 16)
    private final int zzgc;
    @SafeParcelable.Field(id = 17)
    private final int zzhf;
    @SafeParcelable.Field(getter = "getCallbackAsBinder", id = 4, type = "android.os.IBinder")
    private final zzp zzhh;
    @SafeParcelable.Field(id = 14)
    @Deprecated
    private final ClientAppContext zzhi;
    @SafeParcelable.Field(id = 3)
    private final Strategy zzit;
    @SafeParcelable.Field(id = 11)
    @Deprecated
    private final boolean zziu;
    @Nullable
    @SafeParcelable.Field(getter = "getMessageListenerAsBinder", id = 2, type = "android.os.IBinder")
    private final zzm zziy;
    @SafeParcelable.Field(id = 5)
    private final MessageFilter zziz;
    @Nullable
    @SafeParcelable.Field(id = 6)
    private final PendingIntent zzja;
    @SafeParcelable.Field(id = 7)
    @Deprecated
    private final int zzjb;
    @Nullable
    @SafeParcelable.Field(id = 10)
    private final byte[] zzjc;
    @Nullable
    @SafeParcelable.Field(getter = "getSubscribeCallbackAsBinder", id = 12, type = "android.os.IBinder")
    private final zzaa zzjd;

    /* JADX WARN: Type inference failed for: r1v15, types: [android.os.IInterface] */
    /* JADX WARN: Type inference failed for: r8v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    @com.google.android.gms.common.util.VisibleForTesting
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SubscribeRequest(@com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) int r11, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r12, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) com.google.android.gms.nearby.messages.Strategy r13, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) android.os.IBinder r14, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) com.google.android.gms.nearby.messages.MessageFilter r15, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 6) android.app.PendingIntent r16, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 7) int r17, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 8) java.lang.String r18, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 9) java.lang.String r19, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 10) byte[] r20, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 11) boolean r21, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 12) android.os.IBinder r22, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 13) boolean r23, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 14) com.google.android.gms.nearby.messages.internal.ClientAppContext r24, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 15) boolean r25, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 16) int r26, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 17) int r27) {
        /*
            r10 = this;
            r0 = r10
            r1 = r12
            r2 = r14
            r3 = r18
            r4 = r19
            r5 = r22
            r6 = r23
            r10.<init>()
            r7 = r11
            r0.versionCode = r7
            r7 = 0
            if (r1 != 0) goto L_0x0016
            r1 = r7
            goto L_0x002a
        L_0x0016:
            java.lang.String r8 = "com.google.android.gms.nearby.messages.internal.IMessageListener"
            android.os.IInterface r8 = r12.queryLocalInterface(r8)
            boolean r9 = r8 instanceof com.google.android.gms.nearby.messages.internal.zzm
            if (r9 == 0) goto L_0x0024
            r1 = r8
            com.google.android.gms.nearby.messages.internal.zzm r1 = (com.google.android.gms.nearby.messages.internal.zzm) r1
            goto L_0x002a
        L_0x0024:
            com.google.android.gms.nearby.messages.internal.zzo r8 = new com.google.android.gms.nearby.messages.internal.zzo
            r8.<init>(r12)
            r1 = r8
        L_0x002a:
            r0.zziy = r1
            r1 = r13
            r0.zzit = r1
            if (r2 != 0) goto L_0x0033
            r1 = r7
            goto L_0x0045
        L_0x0033:
            java.lang.String r1 = "com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback"
            android.os.IInterface r1 = r14.queryLocalInterface(r1)
            boolean r8 = r1 instanceof com.google.android.gms.nearby.messages.internal.zzp
            if (r8 == 0) goto L_0x0040
            com.google.android.gms.nearby.messages.internal.zzp r1 = (com.google.android.gms.nearby.messages.internal.zzp) r1
            goto L_0x0045
        L_0x0040:
            com.google.android.gms.nearby.messages.internal.zzr r1 = new com.google.android.gms.nearby.messages.internal.zzr
            r1.<init>(r14)
        L_0x0045:
            r0.zzhh = r1
            r1 = r15
            r0.zziz = r1
            r1 = r16
            r0.zzja = r1
            r1 = r17
            r0.zzjb = r1
            r0.zzff = r3
            r0.zzfj = r4
            r1 = r20
            r0.zzjc = r1
            r1 = r21
            r0.zziu = r1
            if (r5 != 0) goto L_0x0061
            goto L_0x0077
        L_0x0061:
            if (r5 != 0) goto L_0x0064
            goto L_0x0077
        L_0x0064:
            java.lang.String r1 = "com.google.android.gms.nearby.messages.internal.ISubscribeCallback"
            android.os.IInterface r1 = r5.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.nearby.messages.internal.zzaa
            if (r2 == 0) goto L_0x0072
            r7 = r1
            com.google.android.gms.nearby.messages.internal.zzaa r7 = (com.google.android.gms.nearby.messages.internal.zzaa) r7
            goto L_0x0077
        L_0x0072:
            com.google.android.gms.nearby.messages.internal.zzac r7 = new com.google.android.gms.nearby.messages.internal.zzac
            r7.<init>(r5)
        L_0x0077:
            r0.zzjd = r7
            r0.zzfg = r6
            r1 = r24
            com.google.android.gms.nearby.messages.internal.ClientAppContext r1 = com.google.android.gms.nearby.messages.internal.ClientAppContext.zza(r1, r4, r3, r6)
            r0.zzhi = r1
            r1 = r25
            r0.zzgb = r1
            r1 = r26
            r0.zzgc = r1
            r1 = r27
            r0.zzhf = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.internal.SubscribeRequest.<init>(int, android.os.IBinder, com.google.android.gms.nearby.messages.Strategy, android.os.IBinder, com.google.android.gms.nearby.messages.MessageFilter, android.app.PendingIntent, int, java.lang.String, java.lang.String, byte[], boolean, android.os.IBinder, boolean, com.google.android.gms.nearby.messages.internal.ClientAppContext, boolean, int, int):void");
    }

    public SubscribeRequest(IBinder iBinder, Strategy strategy, IBinder iBinder2, MessageFilter messageFilter, PendingIntent pendingIntent, @Nullable byte[] bArr, @Nullable IBinder iBinder3, boolean z, int i) {
        this(iBinder, strategy, iBinder2, messageFilter, null, bArr, iBinder3, z, 0, i);
    }

    public SubscribeRequest(IBinder iBinder, Strategy strategy, IBinder iBinder2, MessageFilter messageFilter, PendingIntent pendingIntent, @Nullable byte[] bArr, @Nullable IBinder iBinder3, boolean z, int i, int i2) {
        this(3, iBinder, strategy, iBinder2, messageFilter, pendingIntent, 0, null, null, bArr, false, iBinder3, false, null, z, i, i2);
    }

    public final String toString() {
        String str;
        String valueOf = String.valueOf(this.zziy);
        String valueOf2 = String.valueOf(this.zzit);
        String valueOf3 = String.valueOf(this.zzhh);
        String valueOf4 = String.valueOf(this.zziz);
        String valueOf5 = String.valueOf(this.zzja);
        if (this.zzjc == null) {
            str = null;
        } else {
            int length = this.zzjc.length;
            StringBuilder sb = new StringBuilder(19);
            sb.append("<");
            sb.append(length);
            sb.append(" bytes>");
            str = sb.toString();
        }
        String valueOf6 = String.valueOf(this.zzjd);
        boolean z = this.zzfg;
        String valueOf7 = String.valueOf(this.zzhi);
        boolean z2 = this.zzgb;
        String str2 = this.zzff;
        String str3 = this.zzfj;
        boolean z3 = this.zziu;
        int i = this.zzhf;
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 291 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length() + String.valueOf(valueOf5).length() + String.valueOf(str).length() + String.valueOf(valueOf6).length() + String.valueOf(valueOf7).length() + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb2.append("SubscribeRequest{messageListener=");
        sb2.append(valueOf);
        sb2.append(", strategy=");
        sb2.append(valueOf2);
        sb2.append(", callback=");
        sb2.append(valueOf3);
        sb2.append(", filter=");
        sb2.append(valueOf4);
        sb2.append(", pendingIntent=");
        sb2.append(valueOf5);
        sb2.append(", hint=");
        sb2.append(str);
        sb2.append(", subscribeCallback=");
        sb2.append(valueOf6);
        sb2.append(", useRealClientApiKey=");
        sb2.append(z);
        sb2.append(", clientAppContext=");
        sb2.append(valueOf7);
        sb2.append(", isDiscardPendingIntent=");
        sb2.append(z2);
        sb2.append(", zeroPartyPackageName=");
        sb2.append(str2);
        sb2.append(", realClientPackageName=");
        sb2.append(str3);
        sb2.append(", isIgnoreNearbyPermission=");
        sb2.append(z3);
        sb2.append(", callingContext=");
        sb2.append(i);
        sb2.append("}");
        return sb2.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 2, this.zziy == null ? null : this.zziy.asBinder(), false);
        SafeParcelWriter.writeParcelable(parcel, 3, this.zzit, i, false);
        SafeParcelWriter.writeIBinder(parcel, 4, this.zzhh == null ? null : this.zzhh.asBinder(), false);
        SafeParcelWriter.writeParcelable(parcel, 5, this.zziz, i, false);
        SafeParcelWriter.writeParcelable(parcel, 6, this.zzja, i, false);
        SafeParcelWriter.writeInt(parcel, 7, this.zzjb);
        SafeParcelWriter.writeString(parcel, 8, this.zzff, false);
        SafeParcelWriter.writeString(parcel, 9, this.zzfj, false);
        SafeParcelWriter.writeByteArray(parcel, 10, this.zzjc, false);
        SafeParcelWriter.writeBoolean(parcel, 11, this.zziu);
        if (this.zzjd != null) {
            iBinder = this.zzjd.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 12, iBinder, false);
        SafeParcelWriter.writeBoolean(parcel, 13, this.zzfg);
        SafeParcelWriter.writeParcelable(parcel, 14, this.zzhi, i, false);
        SafeParcelWriter.writeBoolean(parcel, 15, this.zzgb);
        SafeParcelWriter.writeInt(parcel, 16, this.zzgc);
        SafeParcelWriter.writeInt(parcel, 17, this.zzhf);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
