package com.google.android.gms.internal.drive;

import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;

public final class zzhn extends zzir<zzhn> {
    public int versionCode = 1;
    public String zzab = "";
    public long zzac = -1;
    public int zzad = -1;
    public long zzf = -1;

    public zzhn() {
        this.zzmw = null;
        this.zznf = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzhn)) {
            return false;
        }
        zzhn zzhn = (zzhn) obj;
        if (this.versionCode != zzhn.versionCode) {
            return false;
        }
        if (this.zzab == null) {
            if (zzhn.zzab != null) {
                return false;
            }
        } else if (!this.zzab.equals(zzhn.zzab)) {
            return false;
        }
        if (this.zzac == zzhn.zzac && this.zzf == zzhn.zzf && this.zzad == zzhn.zzad) {
            return (this.zzmw == null || this.zzmw.isEmpty()) ? zzhn.zzmw == null || zzhn.zzmw.isEmpty() : this.zzmw.equals(zzhn.zzmw);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((getClass().getName().hashCode() + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31) + this.versionCode) * 31) + (this.zzab == null ? 0 : this.zzab.hashCode())) * 31) + ((int) (this.zzac ^ (this.zzac >>> 32)))) * 31) + ((int) (this.zzf ^ (this.zzf >>> 32)))) * 31) + this.zzad) * 31;
        if (this.zzmw != null && !this.zzmw.isEmpty()) {
            i = this.zzmw.hashCode();
        }
        return hashCode + i;
    }

    public final /* synthetic */ zzix zza(zzio zzio) throws IOException {
        while (true) {
            int zzbd = zzio.zzbd();
            if (zzbd == 0) {
                return this;
            }
            if (zzbd == 8) {
                this.versionCode = zzio.zzbe();
            } else if (zzbd == 18) {
                this.zzab = zzio.readString();
            } else if (zzbd == 24) {
                long zzbf = zzio.zzbf();
                this.zzac = (-(zzbf & 1)) ^ (zzbf >>> 1);
            } else if (zzbd == 32) {
                long zzbf2 = zzio.zzbf();
                this.zzf = (-(zzbf2 & 1)) ^ (zzbf2 >>> 1);
            } else if (zzbd == 40) {
                this.zzad = zzio.zzbe();
            } else if (!super.zza(zzio, zzbd)) {
                return this;
            }
        }
    }

    public final void zza(zzip zzip) throws IOException {
        zzip.zzb(1, this.versionCode);
        String str = this.zzab;
        zzip.zzd(2, 2);
        zzip.zzh(str);
        zzip.zza(3, this.zzac);
        zzip.zza(4, this.zzf);
        if (this.zzad != -1) {
            zzip.zzb(5, this.zzad);
        }
        super.zza(zzip);
    }

    /* access modifiers changed from: protected */
    public final int zzaq() {
        int zzaq = super.zzaq() + zzip.zzc(1, this.versionCode) + zzip.zzo(2) + zzip.zzi(this.zzab) + zzip.zzb(3, this.zzac) + zzip.zzb(4, this.zzf);
        return this.zzad != -1 ? zzaq + zzip.zzc(5, this.zzad) : zzaq;
    }
}
