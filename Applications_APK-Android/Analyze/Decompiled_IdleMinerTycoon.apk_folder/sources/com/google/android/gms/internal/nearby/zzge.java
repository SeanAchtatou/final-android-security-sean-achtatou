package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.DiscoveryOptions;

public final class zzge {
    private final zzgc zzeo = new zzgc();

    public final zzge zza(zzdr zzdr) {
        zzdr unused = this.zzeo.zzen = zzdr;
        return this;
    }

    public final zzge zze(long j) {
        long unused = this.zzeo.durationMillis = j;
        return this;
    }

    public final zzge zze(DiscoveryOptions discoveryOptions) {
        DiscoveryOptions unused = this.zzeo.zzem = discoveryOptions;
        return this;
    }

    public final zzge zzf(zzdz zzdz) {
        zzdz unused = this.zzeo.zzar = zzdz;
        return this;
    }

    public final zzge zzk(String str) {
        String unused = this.zzeo.zzu = str;
        return this;
    }

    public final zzgc zzw() {
        return this.zzeo;
    }
}
