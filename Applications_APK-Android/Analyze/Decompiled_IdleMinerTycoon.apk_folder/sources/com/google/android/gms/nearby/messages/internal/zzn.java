package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.nearby.zzb;
import com.google.android.gms.internal.nearby.zzc;

public abstract class zzn extends zzb implements zzm {
    public zzn() {
        super("com.google.android.gms.nearby.messages.internal.IMessageListener");
    }

    /* access modifiers changed from: protected */
    public final boolean dispatchTransaction(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 4) {
            switch (i) {
                case 1:
                    zza((zzaf) zzc.zza(parcel, zzaf.CREATOR));
                    return true;
                case 2:
                    zzb((zzaf) zzc.zza(parcel, zzaf.CREATOR));
                    return true;
                default:
                    return false;
            }
        } else {
            zza(parcel.createTypedArrayList(Update.CREATOR));
            return true;
        }
    }
}
