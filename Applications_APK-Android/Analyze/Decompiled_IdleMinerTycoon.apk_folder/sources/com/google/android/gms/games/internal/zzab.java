package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.internal.zze;

final class zzab extends zze.zzat<Achievements.LoadAchievementsResult> {
    zzab(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zza(DataHolder dataHolder) {
        setResult(new zze.zzw(dataHolder));
    }
}
