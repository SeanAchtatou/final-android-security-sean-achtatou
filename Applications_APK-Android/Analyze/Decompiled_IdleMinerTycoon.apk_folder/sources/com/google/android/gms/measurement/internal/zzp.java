package com.google.android.gms.measurement.internal;

import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzbk;
import com.google.android.gms.internal.measurement.zzbs;
import com.google.android.gms.internal.measurement.zzey;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzp extends zzjh {
    zzp(zzjg zzjg) {
        super(zzjg);
    }

    /* access modifiers changed from: protected */
    public final boolean zzbk() {
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v22, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v56, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v15, resolved type: java.lang.Long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r84v0, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r89v7, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r84v1, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v85, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v37, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r42v5, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:411:0x0cf0, code lost:
        if (r0.zzkb() == false) goto L_0x0cfb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:412:0x0cf2, code lost:
        r8 = java.lang.Integer.valueOf(r0.getId());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:413:0x0cfb, code lost:
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:414:0x0cfc, code lost:
        r1.zza("Invalid property filter ID. appId, id", r4, java.lang.String.valueOf(r8));
        r15.add(java.lang.Integer.valueOf(r5));
        r7 = r14;
        r0 = r100;
        r1 = r101;
        r3 = r103;
        r4 = r104;
        r98 = r106;
        r99 = r107;
        r12 = r108;
        r97 = r109;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x035f  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0473  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0490  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x04aa  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x05b4  */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0643  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0664  */
    /* JADX WARNING: Removed duplicated region for block: B:300:0x09d7  */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x0d52  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01e1  */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.measurement.zzbs.zza> zza(java.lang.String r117, java.util.List<com.google.android.gms.internal.measurement.zzbs.zzc> r118, java.util.List<com.google.android.gms.internal.measurement.zzbs.zzk> r119) {
        /*
            r116 = this;
            r7 = r116
            r9 = r117
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r117)
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r118)
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r119)
            java.util.HashSet r15 = new java.util.HashSet
            r15.<init>()
            android.support.v4.util.ArrayMap r13 = new android.support.v4.util.ArrayMap
            r13.<init>()
            android.support.v4.util.ArrayMap r14 = new android.support.v4.util.ArrayMap
            r14.<init>()
            android.support.v4.util.ArrayMap r11 = new android.support.v4.util.ArrayMap
            r11.<init>()
            android.support.v4.util.ArrayMap r12 = new android.support.v4.util.ArrayMap
            r12.<init>()
            android.support.v4.util.ArrayMap r10 = new android.support.v4.util.ArrayMap
            r10.<init>()
            com.google.android.gms.measurement.internal.zzs r0 = r116.zzad()
            boolean r25 = r0.zzq(r9)
            com.google.android.gms.measurement.internal.zzs r0 = r116.zzad()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzak.zziq
            boolean r26 = r0.zzd(r9, r1)
            com.google.android.gms.measurement.internal.zzs r0 = r116.zzad()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzak.zziy
            boolean r27 = r0.zzd(r9, r1)
            com.google.android.gms.measurement.internal.zzs r0 = r116.zzad()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzak.zziz
            boolean r28 = r0.zzd(r9, r1)
            if (r27 != 0) goto L_0x0055
            if (r28 == 0) goto L_0x007c
        L_0x0055:
            java.util.Iterator r0 = r118.iterator()
        L_0x0059:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x007c
            java.lang.Object r1 = r0.next()
            com.google.android.gms.internal.measurement.zzbs$zzc r1 = (com.google.android.gms.internal.measurement.zzbs.zzc) r1
            java.lang.String r2 = "_s"
            java.lang.String r3 = r1.getName()
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0059
            long r0 = r1.getTimestampMillis()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r29 = r0
            goto L_0x007e
        L_0x007c:
            r29 = 0
        L_0x007e:
            r6 = 1
            r4 = 0
            if (r29 == 0) goto L_0x00c1
            if (r28 == 0) goto L_0x00c1
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            r1.zzbi()
            r1.zzo()
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r117)
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.String r2 = "current_session_count"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r0.put(r2, r3)
            android.database.sqlite.SQLiteDatabase r2 = r1.getWritableDatabase()     // Catch:{ SQLiteException -> 0x00af }
            java.lang.String r3 = "events"
            java.lang.String r5 = "app_id = ?"
            java.lang.String[] r8 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x00af }
            r8[r4] = r9     // Catch:{ SQLiteException -> 0x00af }
            r2.update(r3, r0, r5, r8)     // Catch:{ SQLiteException -> 0x00af }
            goto L_0x00c1
        L_0x00af:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgk()
            java.lang.String r2 = "Error resetting session-scoped event counts. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzef.zzam(r117)
            r1.zza(r2, r3, r0)
        L_0x00c1:
            com.google.android.gms.measurement.internal.zzx r0 = r116.zzgy()
            java.util.Map r0 = r0.zzaf(r9)
            if (r0 == 0) goto L_0x0351
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x0351
            java.util.HashSet r1 = new java.util.HashSet
            java.util.Set r2 = r0.keySet()
            r1.<init>(r2)
            if (r27 == 0) goto L_0x01d6
            if (r29 == 0) goto L_0x01d6
            com.google.android.gms.measurement.internal.zzp r2 = r116.zzgx()
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r117)
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r0)
            android.support.v4.util.ArrayMap r3 = new android.support.v4.util.ArrayMap
            r3.<init>()
            boolean r5 = r0.isEmpty()
            if (r5 != 0) goto L_0x01d7
            com.google.android.gms.measurement.internal.zzx r5 = r2.zzgy()
            java.util.Map r5 = r5.zzae(r9)
            java.util.Set r8 = r0.keySet()
            java.util.Iterator r8 = r8.iterator()
        L_0x0103:
            boolean r16 = r8.hasNext()
            if (r16 == 0) goto L_0x01d7
            java.lang.Object r16 = r8.next()
            java.lang.Integer r16 = (java.lang.Integer) r16
            int r16 = r16.intValue()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r16)
            java.lang.Object r6 = r0.get(r6)
            com.google.android.gms.internal.measurement.zzbs$zzi r6 = (com.google.android.gms.internal.measurement.zzbs.zzi) r6
            java.lang.Integer r4 = java.lang.Integer.valueOf(r16)
            java.lang.Object r4 = r5.get(r4)
            java.util.List r4 = (java.util.List) r4
            if (r4 == 0) goto L_0x01bf
            boolean r17 = r4.isEmpty()
            if (r17 == 0) goto L_0x0131
            goto L_0x01bf
        L_0x0131:
            r33 = r5
            com.google.android.gms.measurement.internal.zzjo r5 = r2.zzgw()
            r34 = r8
            java.util.List r8 = r6.zzpy()
            java.util.List r5 = r5.zza(r8, r4)
            boolean r8 = r5.isEmpty()
            if (r8 != 0) goto L_0x01ba
            com.google.android.gms.internal.measurement.zzey$zza r8 = r6.zzuj()
            com.google.android.gms.internal.measurement.zzey$zza r8 = (com.google.android.gms.internal.measurement.zzey.zza) r8
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r8 = (com.google.android.gms.internal.measurement.zzbs.zzi.zza) r8
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r8 = r8.zzqr()
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r5 = r8.zzo(r5)
            com.google.android.gms.measurement.internal.zzjo r8 = r2.zzgw()
            r35 = r2
            java.util.List r2 = r6.zzpv()
            java.util.List r2 = r8.zza(r2, r4)
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r8 = r5.zzqq()
            r8.zzn(r2)
            r2 = 0
        L_0x016d:
            int r8 = r6.zzqc()
            if (r2 >= r8) goto L_0x018b
            com.google.android.gms.internal.measurement.zzbs$zzb r8 = r6.zzae(r2)
            int r8 = r8.getIndex()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            boolean r8 = r4.contains(r8)
            if (r8 == 0) goto L_0x0188
            r5.zzaj(r2)
        L_0x0188:
            int r2 = r2 + 1
            goto L_0x016d
        L_0x018b:
            r2 = 0
        L_0x018c:
            int r8 = r6.zzqf()
            if (r2 >= r8) goto L_0x01aa
            com.google.android.gms.internal.measurement.zzbs$zzj r8 = r6.zzag(r2)
            int r8 = r8.getIndex()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            boolean r8 = r4.contains(r8)
            if (r8 == 0) goto L_0x01a7
            r5.zzak(r2)
        L_0x01a7:
            int r2 = r2 + 1
            goto L_0x018c
        L_0x01aa:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r16)
            com.google.android.gms.internal.measurement.zzgi r4 = r5.zzug()
            com.google.android.gms.internal.measurement.zzey r4 = (com.google.android.gms.internal.measurement.zzey) r4
            com.google.android.gms.internal.measurement.zzbs$zzi r4 = (com.google.android.gms.internal.measurement.zzbs.zzi) r4
            r3.put(r2, r4)
            goto L_0x01cc
        L_0x01ba:
            r5 = r33
            r8 = r34
            goto L_0x01d2
        L_0x01bf:
            r35 = r2
            r33 = r5
            r34 = r8
            java.lang.Integer r2 = java.lang.Integer.valueOf(r16)
            r3.put(r2, r6)
        L_0x01cc:
            r5 = r33
            r8 = r34
            r2 = r35
        L_0x01d2:
            r4 = 0
            r6 = 1
            goto L_0x0103
        L_0x01d6:
            r3 = r0
        L_0x01d7:
            java.util.Iterator r1 = r1.iterator()
        L_0x01db:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0351
            java.lang.Object r2 = r1.next()
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            java.lang.Object r4 = r3.get(r4)
            com.google.android.gms.internal.measurement.zzbs$zzi r4 = (com.google.android.gms.internal.measurement.zzbs.zzi) r4
            java.lang.Integer r5 = java.lang.Integer.valueOf(r2)
            java.lang.Object r5 = r14.get(r5)
            java.util.BitSet r5 = (java.util.BitSet) r5
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)
            java.lang.Object r6 = r11.get(r6)
            java.util.BitSet r6 = (java.util.BitSet) r6
            if (r25 == 0) goto L_0x026c
            android.support.v4.util.ArrayMap r8 = new android.support.v4.util.ArrayMap
            r8.<init>()
            if (r4 == 0) goto L_0x0260
            int r16 = r4.zzqc()
            if (r16 != 0) goto L_0x0219
            goto L_0x0260
        L_0x0219:
            java.util.List r16 = r4.zzqb()
            java.util.Iterator r16 = r16.iterator()
        L_0x0221:
            boolean r17 = r16.hasNext()
            if (r17 == 0) goto L_0x0260
            java.lang.Object r17 = r16.next()
            com.google.android.gms.internal.measurement.zzbs$zzb r17 = (com.google.android.gms.internal.measurement.zzbs.zzb) r17
            boolean r18 = r17.zzme()
            if (r18 == 0) goto L_0x0257
            int r18 = r17.getIndex()
            r36 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r18)
            boolean r18 = r17.zzmf()
            if (r18 == 0) goto L_0x0250
            long r17 = r17.zzmg()
            java.lang.Long r17 = java.lang.Long.valueOf(r17)
            r37 = r3
            r3 = r17
            goto L_0x0253
        L_0x0250:
            r37 = r3
            r3 = 0
        L_0x0253:
            r8.put(r1, r3)
            goto L_0x025b
        L_0x0257:
            r36 = r1
            r37 = r3
        L_0x025b:
            r1 = r36
            r3 = r37
            goto L_0x0221
        L_0x0260:
            r36 = r1
            r37 = r3
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r12.put(r1, r8)
            goto L_0x0271
        L_0x026c:
            r36 = r1
            r37 = r3
            r8 = 0
        L_0x0271:
            if (r5 != 0) goto L_0x028b
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r14.put(r1, r5)
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r11.put(r1, r6)
        L_0x028b:
            if (r4 == 0) goto L_0x02e8
            r1 = 0
        L_0x028e:
            int r3 = r4.zzpw()
            int r3 = r3 << 6
            if (r1 >= r3) goto L_0x02e8
            java.util.List r3 = r4.zzpv()
            boolean r3 = com.google.android.gms.measurement.internal.zzjo.zza(r3, r1)
            if (r3 == 0) goto L_0x02cd
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgs()
            r38 = r11
            java.lang.String r11 = "Filter already evaluated. audience ID, filter ID"
            r39 = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r2)
            r40 = r14
            java.lang.Integer r14 = java.lang.Integer.valueOf(r1)
            r3.zza(r11, r12, r14)
            r6.set(r1)
            java.util.List r3 = r4.zzpy()
            boolean r3 = com.google.android.gms.measurement.internal.zzjo.zza(r3, r1)
            if (r3 == 0) goto L_0x02d3
            r5.set(r1)
            r3 = 1
            goto L_0x02d4
        L_0x02cd:
            r38 = r11
            r39 = r12
            r40 = r14
        L_0x02d3:
            r3 = 0
        L_0x02d4:
            if (r8 == 0) goto L_0x02df
            if (r3 != 0) goto L_0x02df
            java.lang.Integer r3 = java.lang.Integer.valueOf(r1)
            r8.remove(r3)
        L_0x02df:
            int r1 = r1 + 1
            r11 = r38
            r12 = r39
            r14 = r40
            goto L_0x028e
        L_0x02e8:
            r38 = r11
            r39 = r12
            r40 = r14
            com.google.android.gms.internal.measurement.zzbs$zza$zza r1 = com.google.android.gms.internal.measurement.zzbs.zza.zzmc()
            r3 = 0
            com.google.android.gms.internal.measurement.zzbs$zza$zza r1 = r1.zzk(r3)
            if (r27 == 0) goto L_0x0307
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.Object r3 = r0.get(r3)
            com.google.android.gms.internal.measurement.zzbs$zzi r3 = (com.google.android.gms.internal.measurement.zzbs.zzi) r3
            r1.zza(r3)
            goto L_0x030a
        L_0x0307:
            r1.zza(r4)
        L_0x030a:
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r3 = com.google.android.gms.internal.measurement.zzbs.zzi.zzqh()
            java.util.List r4 = com.google.android.gms.measurement.internal.zzjo.zza(r5)
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r3 = r3.zzo(r4)
            java.util.List r4 = com.google.android.gms.measurement.internal.zzjo.zza(r6)
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r3 = r3.zzn(r4)
            if (r25 == 0) goto L_0x0333
            java.util.List r4 = zza(r8)
            r3.zzp(r4)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            android.support.v4.util.ArrayMap r5 = new android.support.v4.util.ArrayMap
            r5.<init>()
            r10.put(r4, r5)
        L_0x0333:
            r1.zza(r3)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            com.google.android.gms.internal.measurement.zzgi r1 = r1.zzug()
            com.google.android.gms.internal.measurement.zzey r1 = (com.google.android.gms.internal.measurement.zzey) r1
            com.google.android.gms.internal.measurement.zzbs$zza r1 = (com.google.android.gms.internal.measurement.zzbs.zza) r1
            r13.put(r2, r1)
            r1 = r36
            r3 = r37
            r11 = r38
            r12 = r39
            r14 = r40
            goto L_0x01db
        L_0x0351:
            r38 = r11
            r39 = r12
            r40 = r14
            boolean r0 = r118.isEmpty()
            r14 = 256(0x100, float:3.59E-43)
            if (r0 != 0) goto L_0x09c6
            android.support.v4.util.ArrayMap r12 = new android.support.v4.util.ArrayMap
            r12.<init>()
            java.util.Iterator r33 = r118.iterator()
            r34 = 0
            r1 = r34
            r0 = 0
            r8 = 0
        L_0x036e:
            boolean r3 = r33.hasNext()
            if (r3 == 0) goto L_0x09c6
            java.lang.Object r3 = r33.next()
            r6 = r3
            com.google.android.gms.internal.measurement.zzbs$zzc r6 = (com.google.android.gms.internal.measurement.zzbs.zzc) r6
            java.lang.String r4 = r6.getName()
            java.util.List r16 = r6.zzmj()
            r116.zzgw()
            java.lang.String r3 = "_eid"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzjo.zzb(r6, r3)
            r5 = r3
            java.lang.Long r5 = (java.lang.Long) r5
            if (r5 == 0) goto L_0x0393
            r3 = 1
            goto L_0x0394
        L_0x0393:
            r3 = 0
        L_0x0394:
            if (r3 == 0) goto L_0x03a0
            java.lang.String r11 = "_ep"
            boolean r11 = r4.equals(r11)
            if (r11 == 0) goto L_0x03a0
            r11 = 1
            goto L_0x03a1
        L_0x03a0:
            r11 = 0
        L_0x03a1:
            r17 = 1
            if (r11 == 0) goto L_0x04d5
            r116.zzgw()
            java.lang.String r3 = "_en"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzjo.zzb(r6, r3)
            r11 = r3
            java.lang.String r11 = (java.lang.String) r11
            boolean r3 = android.text.TextUtils.isEmpty(r11)
            if (r3 == 0) goto L_0x03c8
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgk()
            java.lang.String r4 = "Extra parameter without an event name. eventId"
            r3.zza(r4, r5)
            r41 = r15
            goto L_0x04d1
        L_0x03c8:
            if (r8 == 0) goto L_0x03dc
            if (r0 == 0) goto L_0x03dc
            long r3 = r5.longValue()
            long r19 = r0.longValue()
            int r21 = (r3 > r19 ? 1 : (r3 == r19 ? 0 : -1))
            if (r21 == 0) goto L_0x03d9
            goto L_0x03dc
        L_0x03d9:
            r19 = r0
            goto L_0x0406
        L_0x03dc:
            com.google.android.gms.measurement.internal.zzx r3 = r116.zzgy()
            android.util.Pair r3 = r3.zza(r9, r5)
            if (r3 == 0) goto L_0x04c2
            java.lang.Object r4 = r3.first
            if (r4 != 0) goto L_0x03ec
            goto L_0x04c2
        L_0x03ec:
            java.lang.Object r0 = r3.first
            com.google.android.gms.internal.measurement.zzbs$zzc r0 = (com.google.android.gms.internal.measurement.zzbs.zzc) r0
            java.lang.Object r1 = r3.second
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            r116.zzgw()
            java.lang.String r3 = "_eid"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzjo.zzb(r0, r3)
            java.lang.Long r3 = (java.lang.Long) r3
            r8 = r0
            r19 = r3
        L_0x0406:
            r0 = 0
            long r20 = r1 - r17
            int r0 = (r20 > r34 ? 1 : (r20 == r34 ? 0 : -1))
            if (r0 > 0) goto L_0x044b
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            r1.zzo()
            com.google.android.gms.measurement.internal.zzef r0 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgs()
            java.lang.String r2 = "Clearing complex main event info. appId"
            r0.zza(r2, r9)
            android.database.sqlite.SQLiteDatabase r0 = r1.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0436 }
            java.lang.String r2 = "delete from main_event_params where app_id=?"
            r4 = 1
            java.lang.String[] r3 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0434 }
            r22 = 0
            r3[r22] = r9     // Catch:{ SQLiteException -> 0x0432 }
            r0.execSQL(r2, r3)     // Catch:{ SQLiteException -> 0x0432 }
            goto L_0x0447
        L_0x0432:
            r0 = move-exception
            goto L_0x043a
        L_0x0434:
            r0 = move-exception
            goto L_0x0438
        L_0x0436:
            r0 = move-exception
            r4 = 1
        L_0x0438:
            r22 = 0
        L_0x043a:
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgk()
            java.lang.String r2 = "Error clearing complex main event"
            r1.zza(r2, r0)
        L_0x0447:
            r41 = r15
            r15 = r6
            goto L_0x0460
        L_0x044b:
            r4 = 1
            r22 = 0
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            r2 = r117
            r3 = r5
            r22 = 1
            r4 = r20
            r41 = r15
            r15 = r6
            r6 = r8
            r1.zza(r2, r3, r4, r6)
        L_0x0460:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.List r1 = r8.zzmj()
            java.util.Iterator r1 = r1.iterator()
        L_0x046d:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x048a
            java.lang.Object r2 = r1.next()
            com.google.android.gms.internal.measurement.zzbs$zze r2 = (com.google.android.gms.internal.measurement.zzbs.zze) r2
            r116.zzgw()
            java.lang.String r3 = r2.getName()
            com.google.android.gms.internal.measurement.zzbs$zze r3 = com.google.android.gms.measurement.internal.zzjo.zza(r15, r3)
            if (r3 != 0) goto L_0x046d
            r0.add(r2)
            goto L_0x046d
        L_0x048a:
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x04aa
            java.util.Iterator r1 = r16.iterator()
        L_0x0494:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x04a4
            java.lang.Object r2 = r1.next()
            com.google.android.gms.internal.measurement.zzbs$zze r2 = (com.google.android.gms.internal.measurement.zzbs.zze) r2
            r0.add(r2)
            goto L_0x0494
        L_0x04a4:
            r42 = r0
            r36 = r8
            r0 = r11
            goto L_0x04bc
        L_0x04aa:
            com.google.android.gms.measurement.internal.zzef r0 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgn()
            java.lang.String r1 = "No unique parameters in main event. eventName"
            r0.zza(r1, r11)
            r36 = r8
            r0 = r11
            r42 = r16
        L_0x04bc:
            r37 = r19
            r31 = r20
            goto L_0x0527
        L_0x04c2:
            r41 = r15
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgk()
            java.lang.String r4 = "Extra parameter without existing main event. eventName, eventId"
            r3.zza(r4, r11, r5)
        L_0x04d1:
            r15 = r41
            goto L_0x036e
        L_0x04d5:
            r41 = r15
            r15 = r6
            if (r3 == 0) goto L_0x051d
            r116.zzgw()
            java.lang.String r0 = "_epc"
            java.lang.Long r1 = java.lang.Long.valueOf(r34)
            java.lang.Object r0 = com.google.android.gms.measurement.internal.zzjo.zzb(r15, r0)
            if (r0 != 0) goto L_0x04ea
            r0 = r1
        L_0x04ea:
            java.lang.Long r0 = (java.lang.Long) r0
            long r19 = r0.longValue()
            int r0 = (r19 > r34 ? 1 : (r19 == r34 ? 0 : -1))
            if (r0 > 0) goto L_0x0504
            com.google.android.gms.measurement.internal.zzef r0 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgn()
            java.lang.String r1 = "Complex event with zero extra param count. eventName"
            r0.zza(r1, r4)
            r11 = r4
            r0 = r5
            goto L_0x0513
        L_0x0504:
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            r2 = r117
            r3 = r5
            r11 = r4
            r0 = r5
            r4 = r19
            r6 = r15
            r1.zza(r2, r3, r4, r6)
        L_0x0513:
            r37 = r0
            r0 = r11
            r36 = r15
            r42 = r16
            r31 = r19
            goto L_0x0527
        L_0x051d:
            r11 = r4
            r37 = r0
            r31 = r1
            r36 = r8
            r0 = r11
            r42 = r16
        L_0x0527:
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            java.lang.String r2 = r15.getName()
            com.google.android.gms.measurement.internal.zzae r1 = r1.zzc(r9, r2)
            if (r1 != 0) goto L_0x05b4
            com.google.android.gms.measurement.internal.zzef r1 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgn()
            java.lang.String r2 = "Event aggregate wasn't created during raw event logging. appId, event"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzef.zzam(r117)
            com.google.android.gms.measurement.internal.zzed r4 = r116.zzy()
            java.lang.String r4 = r4.zzaj(r0)
            r1.zza(r2, r3, r4)
            if (r28 == 0) goto L_0x0583
            com.google.android.gms.measurement.internal.zzae r1 = new com.google.android.gms.measurement.internal.zzae
            r5 = 0
            r8 = r1
            java.lang.String r2 = r15.getName()
            r6 = r10
            r10 = r2
            r2 = 1
            r44 = r12
            r4 = r38
            r43 = r39
            r11 = r2
            r45 = r13
            r46 = r40
            r13 = r2
            r30 = r15
            r47 = r41
            r15 = r2
            long r17 = r30.getTimestampMillis()
            r19 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 0
            r3 = r9
            r9 = r117
            r8.<init>(r9, r10, r11, r13, r15, r17, r19, r21, r22, r23, r24)
            goto L_0x0630
        L_0x0583:
            r3 = r9
            r6 = r10
            r44 = r12
            r45 = r13
            r30 = r15
            r4 = r38
            r43 = r39
            r46 = r40
            r47 = r41
            r5 = 0
            com.google.android.gms.measurement.internal.zzae r1 = new com.google.android.gms.measurement.internal.zzae
            java.lang.String r10 = r30.getName()
            r11 = 1
            r13 = 1
            long r15 = r30.getTimestampMillis()
            r17 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r8 = r1
            r9 = r117
            r8.<init>(r9, r10, r11, r13, r15, r17, r19, r20, r21, r22)
            goto L_0x0630
        L_0x05b4:
            r3 = r9
            r6 = r10
            r44 = r12
            r45 = r13
            r30 = r15
            r4 = r38
            r43 = r39
            r46 = r40
            r47 = r41
            r5 = 0
            if (r28 == 0) goto L_0x05fc
            com.google.android.gms.measurement.internal.zzae r2 = new com.google.android.gms.measurement.internal.zzae
            r48 = r2
            java.lang.String r8 = r1.zzce
            r49 = r8
            java.lang.String r8 = r1.name
            r50 = r8
            long r8 = r1.zzfg
            long r51 = r8 + r17
            long r8 = r1.zzfh
            long r53 = r8 + r17
            long r8 = r1.zzfi
            long r55 = r8 + r17
            long r8 = r1.zzfj
            r57 = r8
            long r8 = r1.zzfk
            r59 = r8
            java.lang.Long r8 = r1.zzfl
            r61 = r8
            java.lang.Long r8 = r1.zzfm
            r62 = r8
            java.lang.Long r8 = r1.zzfn
            r63 = r8
            java.lang.Boolean r1 = r1.zzfo
            r64 = r1
            r48.<init>(r49, r50, r51, r53, r55, r57, r59, r61, r62, r63, r64)
        L_0x05fa:
            r8 = r2
            goto L_0x0630
        L_0x05fc:
            com.google.android.gms.measurement.internal.zzae r2 = new com.google.android.gms.measurement.internal.zzae
            r64 = r2
            java.lang.String r8 = r1.zzce
            r65 = r8
            java.lang.String r8 = r1.name
            r66 = r8
            long r8 = r1.zzfg
            long r67 = r8 + r17
            long r8 = r1.zzfh
            long r69 = r8 + r17
            long r8 = r1.zzfi
            r71 = r8
            long r8 = r1.zzfj
            r73 = r8
            long r8 = r1.zzfk
            r75 = r8
            java.lang.Long r8 = r1.zzfl
            r77 = r8
            java.lang.Long r8 = r1.zzfm
            r78 = r8
            java.lang.Long r8 = r1.zzfn
            r79 = r8
            java.lang.Boolean r1 = r1.zzfo
            r80 = r1
            r64.<init>(r65, r66, r67, r69, r71, r73, r75, r77, r78, r79, r80)
            goto L_0x05fa
        L_0x0630:
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            r1.zza(r8)
            long r9 = r8.zzfg
            r11 = r44
            java.lang.Object r1 = r11.get(r0)
            java.util.Map r1 = (java.util.Map) r1
            if (r1 != 0) goto L_0x0655
            com.google.android.gms.measurement.internal.zzx r1 = r116.zzgy()
            java.util.Map r1 = r1.zzh(r3, r0)
            if (r1 != 0) goto L_0x0652
            android.support.v4.util.ArrayMap r1 = new android.support.v4.util.ArrayMap
            r1.<init>()
        L_0x0652:
            r11.put(r0, r1)
        L_0x0655:
            r12 = r1
            java.util.Set r1 = r12.keySet()
            java.util.Iterator r13 = r1.iterator()
        L_0x065e:
            boolean r1 = r13.hasNext()
            if (r1 == 0) goto L_0x09ad
            java.lang.Object r1 = r13.next()
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r14 = r1.intValue()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r15 = r47
            boolean r1 = r15.contains(r1)
            if (r1 == 0) goto L_0x068e
            com.google.android.gms.measurement.internal.zzef r1 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "Skipping failed audience ID"
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r1.zza(r2, r14)
            r47 = r15
            goto L_0x065e
        L_0x068e:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r2 = r46
            java.lang.Object r1 = r2.get(r1)
            java.util.BitSet r1 = (java.util.BitSet) r1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r14)
            java.lang.Object r5 = r4.get(r5)
            java.util.BitSet r5 = (java.util.BitSet) r5
            if (r25 == 0) goto L_0x06c3
            r81 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r82 = r9
            r9 = r43
            java.lang.Object r1 = r9.get(r1)
            java.util.Map r1 = (java.util.Map) r1
            java.lang.Integer r10 = java.lang.Integer.valueOf(r14)
            java.lang.Object r10 = r6.get(r10)
            java.util.Map r10 = (java.util.Map) r10
            r84 = r1
            goto L_0x06cc
        L_0x06c3:
            r81 = r1
            r82 = r9
            r9 = r43
            r10 = 0
            r84 = 0
        L_0x06cc:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r85 = r10
            r10 = r45
            java.lang.Object r1 = r10.get(r1)
            com.google.android.gms.internal.measurement.zzbs$zza r1 = (com.google.android.gms.internal.measurement.zzbs.zza) r1
            if (r1 != 0) goto L_0x073b
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            com.google.android.gms.internal.measurement.zzbs$zza$zza r5 = com.google.android.gms.internal.measurement.zzbs.zza.zzmc()
            r86 = r11
            r11 = 1
            com.google.android.gms.internal.measurement.zzbs$zza$zza r5 = r5.zzk(r11)
            com.google.android.gms.internal.measurement.zzgi r5 = r5.zzug()
            com.google.android.gms.internal.measurement.zzey r5 = (com.google.android.gms.internal.measurement.zzey) r5
            com.google.android.gms.internal.measurement.zzbs$zza r5 = (com.google.android.gms.internal.measurement.zzbs.zza) r5
            r10.put(r1, r5)
            java.util.BitSet r1 = new java.util.BitSet
            r1.<init>()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r14)
            r2.put(r5, r1)
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r14)
            r4.put(r11, r5)
            if (r25 == 0) goto L_0x0730
            android.support.v4.util.ArrayMap r11 = new android.support.v4.util.ArrayMap
            r11.<init>()
            r87 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r9.put(r1, r11)
            android.support.v4.util.ArrayMap r1 = new android.support.v4.util.ArrayMap
            r1.<init>()
            r88 = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r14)
            r6.put(r2, r1)
            r89 = r1
            r2 = r11
            goto L_0x0738
        L_0x0730:
            r87 = r1
            r88 = r2
            r2 = r84
            r89 = r85
        L_0x0738:
            r11 = r87
            goto L_0x0745
        L_0x073b:
            r88 = r2
            r86 = r11
            r11 = r81
            r2 = r84
            r89 = r85
        L_0x0745:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            java.lang.Object r1 = r12.get(r1)
            java.util.List r1 = (java.util.List) r1
            java.util.Iterator r16 = r1.iterator()
        L_0x0753:
            boolean r1 = r16.hasNext()
            if (r1 == 0) goto L_0x099c
            java.lang.Object r1 = r16.next()
            com.google.android.gms.internal.measurement.zzbk$zza r1 = (com.google.android.gms.internal.measurement.zzbk.zza) r1
            if (r28 == 0) goto L_0x0770
            if (r27 == 0) goto L_0x0770
            boolean r17 = r1.zzki()
            if (r17 == 0) goto L_0x0770
            r90 = r2
            long r2 = r8.zzfi
            r17 = r2
            goto L_0x0774
        L_0x0770:
            r90 = r2
            r17 = r82
        L_0x0774:
            com.google.android.gms.measurement.internal.zzef r2 = r116.zzab()
            r3 = 2
            boolean r2 = r2.isLoggable(r3)
            if (r2 == 0) goto L_0x07d0
            com.google.android.gms.measurement.internal.zzef r2 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()
            java.lang.String r3 = "Evaluating filter. audience, filter, event"
            r91 = r4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r14)
            boolean r19 = r1.zzkb()
            if (r19 == 0) goto L_0x07a4
            int r19 = r1.getId()
            java.lang.Integer r19 = java.lang.Integer.valueOf(r19)
            r92 = r5
            r93 = r6
            r5 = r19
            goto L_0x07a9
        L_0x07a4:
            r92 = r5
            r93 = r6
            r5 = 0
        L_0x07a9:
            com.google.android.gms.measurement.internal.zzed r6 = r116.zzy()
            r94 = r8
            java.lang.String r8 = r1.zzjz()
            java.lang.String r6 = r6.zzaj(r8)
            r2.zza(r3, r4, r5, r6)
            com.google.android.gms.measurement.internal.zzef r2 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()
            java.lang.String r3 = "Filter definition"
            com.google.android.gms.measurement.internal.zzjo r4 = r116.zzgw()
            java.lang.String r4 = r4.zza(r1)
            r2.zza(r3, r4)
            goto L_0x07d8
        L_0x07d0:
            r91 = r4
            r92 = r5
            r93 = r6
            r94 = r8
        L_0x07d8:
            boolean r2 = r1.zzkb()
            if (r2 == 0) goto L_0x0947
            int r2 = r1.getId()
            r8 = 256(0x100, float:3.59E-43)
            if (r2 <= r8) goto L_0x07e8
            goto L_0x0947
        L_0x07e8:
            if (r25 == 0) goto L_0x08c3
            boolean r2 = r1.zzkf()
            boolean r19 = r1.zzkg()
            if (r27 == 0) goto L_0x07fc
            boolean r3 = r1.zzki()
            if (r3 == 0) goto L_0x07fc
            r3 = 1
            goto L_0x07fd
        L_0x07fc:
            r3 = 0
        L_0x07fd:
            if (r2 != 0) goto L_0x0807
            if (r19 != 0) goto L_0x0807
            if (r3 == 0) goto L_0x0804
            goto L_0x0807
        L_0x0804:
            r20 = 0
            goto L_0x0809
        L_0x0807:
            r20 = 1
        L_0x0809:
            int r2 = r1.getId()
            boolean r2 = r11.get(r2)
            if (r2 == 0) goto L_0x0844
            if (r20 != 0) goto L_0x0844
            com.google.android.gms.measurement.internal.zzef r2 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()
            java.lang.String r3 = "Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r14)
            boolean r5 = r1.zzkb()
            if (r5 == 0) goto L_0x0832
            int r1 = r1.getId()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            goto L_0x0833
        L_0x0832:
            r1 = 0
        L_0x0833:
            r2.zza(r3, r4, r1)
            r2 = r90
            r4 = r91
            r5 = r92
            r6 = r93
            r8 = r94
            r3 = r117
            goto L_0x0753
        L_0x0844:
            r21 = r1
            r5 = r89
            r1 = r116
            r6 = r88
            r4 = r90
            r2 = r21
            r95 = r12
            r8 = r117
            r12 = 2
            r3 = r0
            r96 = r13
            r12 = r91
            r13 = r4
            r4 = r42
            r7 = r6
            r99 = r9
            r97 = r10
            r10 = r92
            r98 = r93
            r9 = r5
            r5 = r17
            java.lang.Boolean r1 = r1.zza(r2, r3, r4, r5)
            com.google.android.gms.measurement.internal.zzef r2 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()
            java.lang.String r3 = "Event filter result"
            if (r1 != 0) goto L_0x087c
            java.lang.String r4 = "null"
            goto L_0x087d
        L_0x087c:
            r4 = r1
        L_0x087d:
            r2.zza(r3, r4)
            if (r1 != 0) goto L_0x088b
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r15.add(r1)
            goto L_0x0984
        L_0x088b:
            int r2 = r21.getId()
            r10.set(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0984
            int r1 = r21.getId()
            r11.set(r1)
            if (r20 == 0) goto L_0x0984
            boolean r1 = r30.zzml()
            if (r1 == 0) goto L_0x0984
            if (r19 == 0) goto L_0x08b6
            int r1 = r21.getId()
            long r2 = r30.getTimestampMillis()
            zzb(r9, r1, r2)
            goto L_0x0984
        L_0x08b6:
            int r1 = r21.getId()
            long r2 = r30.getTimestampMillis()
            zza(r13, r1, r2)
            goto L_0x0984
        L_0x08c3:
            r21 = r1
            r99 = r9
            r97 = r10
            r95 = r12
            r96 = r13
            r7 = r88
            r9 = r89
            r13 = r90
            r12 = r91
            r10 = r92
            r98 = r93
            r8 = r117
            int r1 = r21.getId()
            boolean r1 = r11.get(r1)
            if (r1 == 0) goto L_0x0908
            com.google.android.gms.measurement.internal.zzef r1 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "Event filter already evaluated true. audience ID, filter ID"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r14)
            boolean r4 = r21.zzkb()
            if (r4 == 0) goto L_0x0902
            int r4 = r21.getId()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            goto L_0x0903
        L_0x0902:
            r4 = 0
        L_0x0903:
            r1.zza(r2, r3, r4)
            goto L_0x0984
        L_0x0908:
            r1 = r116
            r2 = r21
            r3 = r0
            r4 = r42
            r5 = r17
            java.lang.Boolean r1 = r1.zza(r2, r3, r4, r5)
            com.google.android.gms.measurement.internal.zzef r2 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()
            java.lang.String r3 = "Event filter result"
            if (r1 != 0) goto L_0x0924
            java.lang.String r4 = "null"
            goto L_0x0925
        L_0x0924:
            r4 = r1
        L_0x0925:
            r2.zza(r3, r4)
            if (r1 != 0) goto L_0x0932
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r15.add(r1)
            goto L_0x0984
        L_0x0932:
            int r2 = r21.getId()
            r10.set(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0984
            int r1 = r21.getId()
            r11.set(r1)
            goto L_0x0984
        L_0x0947:
            r21 = r1
            r99 = r9
            r97 = r10
            r95 = r12
            r96 = r13
            r7 = r88
            r9 = r89
            r13 = r90
            r12 = r91
            r10 = r92
            r98 = r93
            r8 = r117
            com.google.android.gms.measurement.internal.zzef r1 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgn()
            java.lang.String r2 = "Invalid event filter ID. appId, id"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzef.zzam(r117)
            boolean r4 = r21.zzkb()
            if (r4 == 0) goto L_0x097c
            int r4 = r21.getId()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            goto L_0x097d
        L_0x097c:
            r4 = 0
        L_0x097d:
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r1.zza(r2, r3, r4)
        L_0x0984:
            r88 = r7
            r3 = r8
            r89 = r9
            r5 = r10
            r4 = r12
            r2 = r13
            r8 = r94
            r12 = r95
            r13 = r96
            r10 = r97
            r6 = r98
            r9 = r99
            r7 = r116
            goto L_0x0753
        L_0x099c:
            r43 = r9
            r45 = r10
            r47 = r15
            r9 = r82
            r11 = r86
            r46 = r88
            r5 = 0
            r7 = r116
            goto L_0x065e
        L_0x09ad:
            r9 = r3
            r38 = r4
            r10 = r6
            r12 = r11
            r1 = r31
            r8 = r36
            r0 = r37
            r39 = r43
            r13 = r45
            r40 = r46
            r15 = r47
            r7 = r116
            r14 = 256(0x100, float:3.59E-43)
            goto L_0x036e
        L_0x09c6:
            r8 = r9
            r98 = r10
            r97 = r13
            r12 = r38
            r99 = r39
            r7 = r40
            boolean r0 = r119.isEmpty()
            if (r0 != 0) goto L_0x0d34
            android.support.v4.util.ArrayMap r0 = new android.support.v4.util.ArrayMap
            r0.<init>()
            java.util.Iterator r1 = r119.iterator()
        L_0x09e0:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0d34
            java.lang.Object r2 = r1.next()
            com.google.android.gms.internal.measurement.zzbs$zzk r2 = (com.google.android.gms.internal.measurement.zzbs.zzk) r2
            java.lang.String r3 = r2.getName()
            java.lang.Object r3 = r0.get(r3)
            java.util.Map r3 = (java.util.Map) r3
            if (r3 != 0) goto L_0x0a12
            com.google.android.gms.measurement.internal.zzx r3 = r116.zzgy()
            java.lang.String r4 = r2.getName()
            java.util.Map r3 = r3.zzi(r8, r4)
            if (r3 != 0) goto L_0x0a0b
            android.support.v4.util.ArrayMap r3 = new android.support.v4.util.ArrayMap
            r3.<init>()
        L_0x0a0b:
            java.lang.String r4 = r2.getName()
            r0.put(r4, r3)
        L_0x0a12:
            java.util.Set r4 = r3.keySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x0a1a:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0d2e
            java.lang.Object r5 = r4.next()
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r5 = r5.intValue()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r5)
            boolean r6 = r15.contains(r6)
            if (r6 == 0) goto L_0x0a46
            com.google.android.gms.measurement.internal.zzef r6 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgs()
            java.lang.String r9 = "Skipping failed audience ID"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r6.zza(r9, r5)
            goto L_0x0a1a
        L_0x0a46:
            java.lang.Integer r6 = java.lang.Integer.valueOf(r5)
            java.lang.Object r6 = r7.get(r6)
            java.util.BitSet r6 = (java.util.BitSet) r6
            java.lang.Integer r9 = java.lang.Integer.valueOf(r5)
            java.lang.Object r9 = r12.get(r9)
            java.util.BitSet r9 = (java.util.BitSet) r9
            if (r25 == 0) goto L_0x0a77
            java.lang.Integer r10 = java.lang.Integer.valueOf(r5)
            r11 = r99
            java.lang.Object r10 = r11.get(r10)
            java.util.Map r10 = (java.util.Map) r10
            java.lang.Integer r13 = java.lang.Integer.valueOf(r5)
            r14 = r98
            java.lang.Object r13 = r14.get(r13)
            java.util.Map r13 = (java.util.Map) r13
            r100 = r0
            goto L_0x0a7f
        L_0x0a77:
            r14 = r98
            r11 = r99
            r100 = r0
            r10 = 0
            r13 = 0
        L_0x0a7f:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r101 = r1
            r1 = r97
            java.lang.Object r0 = r1.get(r0)
            com.google.android.gms.internal.measurement.zzbs$zza r0 = (com.google.android.gms.internal.measurement.zzbs.zza) r0
            if (r0 != 0) goto L_0x0ad9
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            com.google.android.gms.internal.measurement.zzbs$zza$zza r6 = com.google.android.gms.internal.measurement.zzbs.zza.zzmc()
            r9 = 1
            com.google.android.gms.internal.measurement.zzbs$zza$zza r6 = r6.zzk(r9)
            com.google.android.gms.internal.measurement.zzgi r6 = r6.zzug()
            com.google.android.gms.internal.measurement.zzey r6 = (com.google.android.gms.internal.measurement.zzey) r6
            com.google.android.gms.internal.measurement.zzbs$zza r6 = (com.google.android.gms.internal.measurement.zzbs.zza) r6
            r1.put(r0, r6)
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r7.put(r0, r6)
            java.util.BitSet r9 = new java.util.BitSet
            r9.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r12.put(r0, r9)
            if (r25 == 0) goto L_0x0ad9
            android.support.v4.util.ArrayMap r10 = new android.support.v4.util.ArrayMap
            r10.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r11.put(r0, r10)
            android.support.v4.util.ArrayMap r13 = new android.support.v4.util.ArrayMap
            r13.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r14.put(r0, r13)
        L_0x0ad9:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.Object r0 = r3.get(r0)
            java.util.List r0 = (java.util.List) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x0ae7:
            boolean r16 = r0.hasNext()
            if (r16 == 0) goto L_0x0d1c
            java.lang.Object r16 = r0.next()
            r102 = r0
            r0 = r16
            com.google.android.gms.internal.measurement.zzbk$zzd r0 = (com.google.android.gms.internal.measurement.zzbk.zzd) r0
            r103 = r3
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            r104 = r4
            r4 = 2
            boolean r3 = r3.isLoggable(r4)
            if (r3 == 0) goto L_0x0b53
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgs()
            java.lang.String r4 = "Evaluating filter. audience, filter, property"
            r105 = r7
            java.lang.Integer r7 = java.lang.Integer.valueOf(r5)
            boolean r16 = r0.zzkb()
            if (r16 == 0) goto L_0x0b29
            int r16 = r0.getId()
            java.lang.Integer r16 = java.lang.Integer.valueOf(r16)
            r106 = r14
            r8 = r16
            goto L_0x0b2c
        L_0x0b29:
            r106 = r14
            r8 = 0
        L_0x0b2c:
            com.google.android.gms.measurement.internal.zzed r14 = r116.zzy()
            r107 = r11
            java.lang.String r11 = r0.getPropertyName()
            java.lang.String r11 = r14.zzal(r11)
            r3.zza(r4, r7, r8, r11)
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgs()
            java.lang.String r4 = "Filter definition"
            com.google.android.gms.measurement.internal.zzjo r7 = r116.zzgw()
            java.lang.String r7 = r7.zza(r0)
            r3.zza(r4, r7)
            goto L_0x0b59
        L_0x0b53:
            r105 = r7
            r107 = r11
            r106 = r14
        L_0x0b59:
            boolean r3 = r0.zzkb()
            if (r3 == 0) goto L_0x0cd6
            int r3 = r0.getId()
            r4 = 256(0x100, float:3.59E-43)
            if (r3 <= r4) goto L_0x0b69
            goto L_0x0cd6
        L_0x0b69:
            if (r25 == 0) goto L_0x0c5a
            boolean r3 = r0.zzkf()
            boolean r7 = r0.zzkg()
            if (r27 == 0) goto L_0x0b7d
            boolean r8 = r0.zzki()
            if (r8 == 0) goto L_0x0b7d
            r8 = 1
            goto L_0x0b7e
        L_0x0b7d:
            r8 = 0
        L_0x0b7e:
            if (r3 != 0) goto L_0x0b87
            if (r7 != 0) goto L_0x0b87
            if (r8 == 0) goto L_0x0b85
            goto L_0x0b87
        L_0x0b85:
            r3 = 0
            goto L_0x0b88
        L_0x0b87:
            r3 = 1
        L_0x0b88:
            int r11 = r0.getId()
            boolean r11 = r6.get(r11)
            if (r11 == 0) goto L_0x0bc3
            if (r3 != 0) goto L_0x0bc3
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgs()
            java.lang.String r7 = "Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r5)
            boolean r11 = r0.zzkb()
            if (r11 == 0) goto L_0x0bb1
            int r0 = r0.getId()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x0bb2
        L_0x0bb1:
            r0 = 0
        L_0x0bb2:
            r3.zza(r7, r8, r0)
            r0 = r102
            r3 = r103
            r4 = r104
            r7 = r105
            r14 = r106
            r11 = r107
            goto L_0x0c9c
        L_0x0bc3:
            r14 = r105
            r11 = r116
            java.lang.Boolean r16 = r11.zza(r0, r2)
            com.google.android.gms.measurement.internal.zzef r17 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r4 = r17.zzgs()
            r108 = r12
            java.lang.String r12 = "Property filter result"
            if (r16 != 0) goto L_0x0be0
            java.lang.String r17 = "null"
            r109 = r1
            r1 = r17
            goto L_0x0be4
        L_0x0be0:
            r109 = r1
            r1 = r16
        L_0x0be4:
            r4.zza(r12, r1)
            if (r16 != 0) goto L_0x0bf2
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r15.add(r0)
            goto L_0x0c8d
        L_0x0bf2:
            int r1 = r0.getId()
            r9.set(r1)
            if (r27 == 0) goto L_0x0c03
            if (r8 == 0) goto L_0x0c03
            boolean r1 = r16.booleanValue()
            if (r1 == 0) goto L_0x0c8d
        L_0x0c03:
            if (r26 == 0) goto L_0x0c21
            int r1 = r0.getId()
            boolean r1 = r6.get(r1)
            if (r1 == 0) goto L_0x0c15
            boolean r1 = r0.zzkf()
            if (r1 == 0) goto L_0x0c2c
        L_0x0c15:
            int r1 = r0.getId()
            boolean r4 = r16.booleanValue()
            r6.set(r1, r4)
            goto L_0x0c2c
        L_0x0c21:
            int r1 = r0.getId()
            boolean r4 = r16.booleanValue()
            r6.set(r1, r4)
        L_0x0c2c:
            boolean r1 = r16.booleanValue()
            if (r1 == 0) goto L_0x0c8d
            if (r3 == 0) goto L_0x0c8d
            boolean r1 = r2.zzqs()
            if (r1 == 0) goto L_0x0c8d
            long r3 = r2.zzqt()
            if (r27 == 0) goto L_0x0c48
            if (r8 == 0) goto L_0x0c48
            if (r29 == 0) goto L_0x0c48
            long r3 = r29.longValue()
        L_0x0c48:
            if (r7 == 0) goto L_0x0c52
            int r0 = r0.getId()
            zzb(r13, r0, r3)
            goto L_0x0c8d
        L_0x0c52:
            int r0 = r0.getId()
            zza(r10, r0, r3)
            goto L_0x0c8d
        L_0x0c5a:
            r109 = r1
            r108 = r12
            r14 = r105
            r11 = r116
            int r1 = r0.getId()
            boolean r1 = r6.get(r1)
            if (r1 == 0) goto L_0x0ca0
            com.google.android.gms.measurement.internal.zzef r1 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r3 = "Property filter already evaluated true. audience ID, filter ID"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r5)
            boolean r7 = r0.zzkb()
            if (r7 == 0) goto L_0x0c89
            int r0 = r0.getId()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            goto L_0x0c8a
        L_0x0c89:
            r8 = 0
        L_0x0c8a:
            r1.zza(r3, r4, r8)
        L_0x0c8d:
            r7 = r14
            r0 = r102
            r3 = r103
            r4 = r104
            r14 = r106
            r11 = r107
            r12 = r108
            r1 = r109
        L_0x0c9c:
            r8 = r117
            goto L_0x0ae7
        L_0x0ca0:
            java.lang.Boolean r1 = r11.zza(r0, r2)
            com.google.android.gms.measurement.internal.zzef r3 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgs()
            java.lang.String r4 = "Property filter result"
            if (r1 != 0) goto L_0x0cb3
            java.lang.String r7 = "null"
            goto L_0x0cb4
        L_0x0cb3:
            r7 = r1
        L_0x0cb4:
            r3.zza(r4, r7)
            if (r1 != 0) goto L_0x0cc1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r15.add(r0)
            goto L_0x0c8d
        L_0x0cc1:
            int r3 = r0.getId()
            r9.set(r3)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0c8d
            int r0 = r0.getId()
            r6.set(r0)
            goto L_0x0c8d
        L_0x0cd6:
            r109 = r1
            r108 = r12
            r14 = r105
            r11 = r116
            com.google.android.gms.measurement.internal.zzef r1 = r116.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgn()
            java.lang.String r3 = "Invalid property filter ID. appId, id"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzef.zzam(r117)
            boolean r6 = r0.zzkb()
            if (r6 == 0) goto L_0x0cfb
            int r0 = r0.getId()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            goto L_0x0cfc
        L_0x0cfb:
            r8 = 0
        L_0x0cfc:
            java.lang.String r0 = java.lang.String.valueOf(r8)
            r1.zza(r3, r4, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            r15.add(r0)
            r7 = r14
            r0 = r100
            r1 = r101
            r3 = r103
            r4 = r104
            r98 = r106
            r99 = r107
            r12 = r108
            r97 = r109
            goto L_0x0d2a
        L_0x0d1c:
            r107 = r11
            r11 = r116
            r97 = r1
            r98 = r14
            r0 = r100
            r1 = r101
            r99 = r107
        L_0x0d2a:
            r8 = r117
            goto L_0x0a1a
        L_0x0d2e:
            r11 = r116
            r8 = r117
            goto L_0x09e0
        L_0x0d34:
            r14 = r7
            r108 = r12
            r109 = r97
            r106 = r98
            r107 = r99
            r11 = r116
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Set r0 = r14.keySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x0d4c:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x1003
            java.lang.Object r0 = r2.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            boolean r3 = r15.contains(r3)
            if (r3 != 0) goto L_0x0fff
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            r4 = r109
            java.lang.Object r3 = r4.get(r3)
            com.google.android.gms.internal.measurement.zzbs$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zza) r3
            if (r3 != 0) goto L_0x0d79
            com.google.android.gms.internal.measurement.zzbs$zza$zza r3 = com.google.android.gms.internal.measurement.zzbs.zza.zzmc()
            goto L_0x0d81
        L_0x0d79:
            com.google.android.gms.internal.measurement.zzey$zza r3 = r3.zzuj()
            com.google.android.gms.internal.measurement.zzey$zza r3 = (com.google.android.gms.internal.measurement.zzey.zza) r3
            com.google.android.gms.internal.measurement.zzbs$zza$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zza.C0044zza) r3
        L_0x0d81:
            r3.zzi(r0)
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r5 = com.google.android.gms.internal.measurement.zzbs.zzi.zzqh()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)
            java.lang.Object r6 = r14.get(r6)
            java.util.BitSet r6 = (java.util.BitSet) r6
            java.util.List r6 = com.google.android.gms.measurement.internal.zzjo.zza(r6)
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r5 = r5.zzo(r6)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)
            r7 = r108
            java.lang.Object r6 = r7.get(r6)
            java.util.BitSet r6 = (java.util.BitSet) r6
            java.util.List r6 = com.google.android.gms.measurement.internal.zzjo.zza(r6)
            com.google.android.gms.internal.measurement.zzbs$zzi$zza r5 = r5.zzn(r6)
            if (r25 == 0) goto L_0x0f61
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)
            r8 = r107
            java.lang.Object r6 = r8.get(r6)
            java.util.Map r6 = (java.util.Map) r6
            java.util.List r6 = zza(r6)
            r5.zzp(r6)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)
            r9 = r106
            java.lang.Object r6 = r9.get(r6)
            java.util.Map r6 = (java.util.Map) r6
            if (r6 != 0) goto L_0x0ddb
            java.util.List r6 = java.util.Collections.emptyList()
            r110 = r2
            r111 = r7
            goto L_0x0e4c
        L_0x0ddb:
            java.util.ArrayList r10 = new java.util.ArrayList
            int r12 = r6.size()
            r10.<init>(r12)
            java.util.Set r12 = r6.keySet()
            java.util.Iterator r12 = r12.iterator()
        L_0x0dec:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x0e47
            java.lang.Object r13 = r12.next()
            java.lang.Integer r13 = (java.lang.Integer) r13
            r110 = r2
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r2 = com.google.android.gms.internal.measurement.zzbs.zzj.zzqo()
            r111 = r7
            int r7 = r13.intValue()
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r2 = r2.zzal(r7)
            java.lang.Object r7 = r6.get(r13)
            java.util.List r7 = (java.util.List) r7
            if (r7 == 0) goto L_0x0e33
            java.util.Collections.sort(r7)
            java.util.Iterator r7 = r7.iterator()
        L_0x0e17:
            boolean r13 = r7.hasNext()
            if (r13 == 0) goto L_0x0e33
            java.lang.Object r13 = r7.next()
            java.lang.Long r13 = (java.lang.Long) r13
            r112 = r6
            r113 = r7
            long r6 = r13.longValue()
            r2.zzbj(r6)
            r6 = r112
            r7 = r113
            goto L_0x0e17
        L_0x0e33:
            r112 = r6
            com.google.android.gms.internal.measurement.zzgi r2 = r2.zzug()
            com.google.android.gms.internal.measurement.zzey r2 = (com.google.android.gms.internal.measurement.zzey) r2
            com.google.android.gms.internal.measurement.zzbs$zzj r2 = (com.google.android.gms.internal.measurement.zzbs.zzj) r2
            r10.add(r2)
            r2 = r110
            r7 = r111
            r6 = r112
            goto L_0x0dec
        L_0x0e47:
            r110 = r2
            r111 = r7
            r6 = r10
        L_0x0e4c:
            if (r26 == 0) goto L_0x0e62
            boolean r2 = r3.zzlw()
            if (r2 == 0) goto L_0x0e62
            com.google.android.gms.internal.measurement.zzbs$zzi r2 = r3.zzlx()
            java.util.List r2 = r2.zzqe()
            boolean r7 = r2.isEmpty()
            if (r7 == 0) goto L_0x0e6a
        L_0x0e62:
            r114 = r8
            r115 = r9
            r16 = 1
            goto L_0x0f5d
        L_0x0e6a:
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>(r6)
            android.support.v4.util.ArrayMap r6 = new android.support.v4.util.ArrayMap
            r6.<init>()
            java.util.Iterator r2 = r2.iterator()
        L_0x0e78:
            boolean r10 = r2.hasNext()
            if (r10 == 0) goto L_0x0eaf
            java.lang.Object r10 = r2.next()
            com.google.android.gms.internal.measurement.zzbs$zzj r10 = (com.google.android.gms.internal.measurement.zzbs.zzj) r10
            boolean r12 = r10.zzme()
            if (r12 == 0) goto L_0x0eac
            int r12 = r10.zzql()
            if (r12 <= 0) goto L_0x0eac
            int r12 = r10.getIndex()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            int r13 = r10.zzql()
            r16 = 1
            int r13 = r13 + -1
            long r17 = r10.zzai(r13)
            java.lang.Long r10 = java.lang.Long.valueOf(r17)
            r6.put(r12, r10)
            goto L_0x0e78
        L_0x0eac:
            r16 = 1
            goto L_0x0e78
        L_0x0eaf:
            r16 = 1
            r2 = 0
        L_0x0eb2:
            int r10 = r7.size()
            if (r2 >= r10) goto L_0x0f18
            java.lang.Object r10 = r7.get(r2)
            com.google.android.gms.internal.measurement.zzbs$zzj r10 = (com.google.android.gms.internal.measurement.zzbs.zzj) r10
            boolean r12 = r10.zzme()
            if (r12 == 0) goto L_0x0ecd
            int r12 = r10.getIndex()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            goto L_0x0ece
        L_0x0ecd:
            r12 = 0
        L_0x0ece:
            java.lang.Object r12 = r6.remove(r12)
            java.lang.Long r12 = (java.lang.Long) r12
            if (r12 == 0) goto L_0x0f10
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            long r17 = r12.longValue()
            r114 = r8
            r8 = 0
            long r19 = r10.zzai(r8)
            int r21 = (r17 > r19 ? 1 : (r17 == r19 ? 0 : -1))
            if (r21 >= 0) goto L_0x0eed
            r13.add(r12)
        L_0x0eed:
            java.util.List r12 = r10.zzqk()
            r13.addAll(r12)
            com.google.android.gms.internal.measurement.zzey$zza r10 = r10.zzuj()
            com.google.android.gms.internal.measurement.zzey$zza r10 = (com.google.android.gms.internal.measurement.zzey.zza) r10
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r10 = (com.google.android.gms.internal.measurement.zzbs.zzj.zza) r10
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r10 = r10.zzqw()
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r10 = r10.zzr(r13)
            com.google.android.gms.internal.measurement.zzgi r10 = r10.zzug()
            com.google.android.gms.internal.measurement.zzey r10 = (com.google.android.gms.internal.measurement.zzey) r10
            com.google.android.gms.internal.measurement.zzbs$zzj r10 = (com.google.android.gms.internal.measurement.zzbs.zzj) r10
            r7.set(r2, r10)
            goto L_0x0f13
        L_0x0f10:
            r114 = r8
            r8 = 0
        L_0x0f13:
            int r2 = r2 + 1
            r8 = r114
            goto L_0x0eb2
        L_0x0f18:
            r114 = r8
            r8 = 0
            java.util.Set r2 = r6.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0f23:
            boolean r10 = r2.hasNext()
            if (r10 == 0) goto L_0x0f5a
            java.lang.Object r10 = r2.next()
            java.lang.Integer r10 = (java.lang.Integer) r10
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r12 = com.google.android.gms.internal.measurement.zzbs.zzj.zzqo()
            int r13 = r10.intValue()
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r12 = r12.zzal(r13)
            java.lang.Object r10 = r6.get(r10)
            java.lang.Long r10 = (java.lang.Long) r10
            r115 = r9
            long r8 = r10.longValue()
            com.google.android.gms.internal.measurement.zzbs$zzj$zza r8 = r12.zzbj(r8)
            com.google.android.gms.internal.measurement.zzgi r8 = r8.zzug()
            com.google.android.gms.internal.measurement.zzey r8 = (com.google.android.gms.internal.measurement.zzey) r8
            com.google.android.gms.internal.measurement.zzbs$zzj r8 = (com.google.android.gms.internal.measurement.zzbs.zzj) r8
            r7.add(r8)
            r9 = r115
            r8 = 0
            goto L_0x0f23
        L_0x0f5a:
            r115 = r9
            r6 = r7
        L_0x0f5d:
            r5.zzq(r6)
            goto L_0x0f6b
        L_0x0f61:
            r110 = r2
            r111 = r7
            r115 = r106
            r114 = r107
            r16 = 1
        L_0x0f6b:
            r3.zza(r5)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            com.google.android.gms.internal.measurement.zzgi r5 = r3.zzug()
            com.google.android.gms.internal.measurement.zzey r5 = (com.google.android.gms.internal.measurement.zzey) r5
            com.google.android.gms.internal.measurement.zzbs$zza r5 = (com.google.android.gms.internal.measurement.zzbs.zza) r5
            r4.put(r2, r5)
            com.google.android.gms.internal.measurement.zzgi r2 = r3.zzug()
            com.google.android.gms.internal.measurement.zzey r2 = (com.google.android.gms.internal.measurement.zzey) r2
            com.google.android.gms.internal.measurement.zzbs$zza r2 = (com.google.android.gms.internal.measurement.zzbs.zza) r2
            r1.add(r2)
            com.google.android.gms.measurement.internal.zzx r2 = r116.zzgy()
            com.google.android.gms.internal.measurement.zzbs$zzi r3 = r3.zzlv()
            r2.zzbi()
            r2.zzo()
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r117)
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r3)
            byte[] r3 = r3.toByteArray()
            android.content.ContentValues r5 = new android.content.ContentValues
            r5.<init>()
            java.lang.String r6 = "app_id"
            r7 = r117
            r5.put(r6, r7)
            java.lang.String r6 = "audience_id"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r5.put(r6, r0)
            java.lang.String r0 = "current_results"
            r5.put(r0, r3)
            android.database.sqlite.SQLiteDatabase r0 = r2.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0fe0 }
            java.lang.String r3 = "audience_filter_values"
            r6 = 5
            r8 = 0
            long r5 = r0.insertWithOnConflict(r3, r8, r5, r6)     // Catch:{ SQLiteException -> 0x0fde }
            r9 = -1
            int r0 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r0 != 0) goto L_0x0ff3
            com.google.android.gms.measurement.internal.zzef r0 = r2.zzab()     // Catch:{ SQLiteException -> 0x0fde }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgk()     // Catch:{ SQLiteException -> 0x0fde }
            java.lang.String r3 = "Failed to insert filter results (got -1). appId"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzef.zzam(r117)     // Catch:{ SQLiteException -> 0x0fde }
            r0.zza(r3, r5)     // Catch:{ SQLiteException -> 0x0fde }
            goto L_0x0ff3
        L_0x0fde:
            r0 = move-exception
            goto L_0x0fe2
        L_0x0fe0:
            r0 = move-exception
            r8 = 0
        L_0x0fe2:
            com.google.android.gms.measurement.internal.zzef r2 = r2.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgk()
            java.lang.String r3 = "Error storing filter results. appId"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzef.zzam(r117)
            r2.zza(r3, r5, r0)
        L_0x0ff3:
            r109 = r4
            r2 = r110
            r108 = r111
            r107 = r114
            r106 = r115
            goto L_0x0d4c
        L_0x0fff:
            r7 = r117
            goto L_0x0d4c
        L_0x1003:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzp.zza(java.lang.String, java.util.List, java.util.List):java.util.List");
    }

    private final Boolean zza(zzbk.zza zza, String str, List<zzbs.zze> list, long j) {
        Boolean bool;
        if (zza.zzkd()) {
            Boolean zza2 = zza(j, zza.zzke());
            if (zza2 == null) {
                return null;
            }
            if (!zza2.booleanValue()) {
                return false;
            }
        }
        HashSet hashSet = new HashSet();
        for (zzbk.zzb next : zza.zzkc()) {
            if (next.zzkr().isEmpty()) {
                zzab().zzgn().zza("null or empty param name in filter. event", zzy().zzaj(str));
                return null;
            }
            hashSet.add(next.zzkr());
        }
        ArrayMap arrayMap = new ArrayMap();
        for (zzbs.zze next2 : list) {
            if (hashSet.contains(next2.getName())) {
                if (next2.zzna()) {
                    arrayMap.put(next2.getName(), next2.zzna() ? Long.valueOf(next2.zznb()) : null);
                } else if (next2.zznd()) {
                    arrayMap.put(next2.getName(), next2.zznd() ? Double.valueOf(next2.zzne()) : null);
                } else if (next2.zzmx()) {
                    arrayMap.put(next2.getName(), next2.zzmy());
                } else {
                    zzab().zzgn().zza("Unknown value for param. event, param", zzy().zzaj(str), zzy().zzak(next2.getName()));
                    return null;
                }
            }
        }
        Iterator<zzbk.zzb> it = zza.zzkc().iterator();
        while (true) {
            boolean z = true;
            if (!it.hasNext()) {
                return true;
            }
            zzbk.zzb next3 = it.next();
            if (!next3.zzkp() || !next3.zzkq()) {
                z = false;
            }
            String zzkr = next3.zzkr();
            if (zzkr.isEmpty()) {
                zzab().zzgn().zza("Event has empty param name. event", zzy().zzaj(str));
                return null;
            }
            Object obj = arrayMap.get(zzkr);
            if (obj instanceof Long) {
                if (!next3.zzkn()) {
                    zzab().zzgn().zza("No number filter for long param. event, param", zzy().zzaj(str), zzy().zzak(zzkr));
                    return null;
                }
                Boolean zza3 = zza(((Long) obj).longValue(), next3.zzko());
                if (zza3 == null) {
                    return null;
                }
                if (zza3.booleanValue() == z) {
                    return false;
                }
            } else if (obj instanceof Double) {
                if (!next3.zzkn()) {
                    zzab().zzgn().zza("No number filter for double param. event, param", zzy().zzaj(str), zzy().zzak(zzkr));
                    return null;
                }
                Boolean zza4 = zza(((Double) obj).doubleValue(), next3.zzko());
                if (zza4 == null) {
                    return null;
                }
                if (zza4.booleanValue() == z) {
                    return false;
                }
            } else if (obj instanceof String) {
                if (next3.zzkl()) {
                    bool = zza((String) obj, next3.zzkm());
                } else if (next3.zzkn()) {
                    String str2 = (String) obj;
                    if (zzjo.zzbj(str2)) {
                        bool = zza(str2, next3.zzko());
                    } else {
                        zzab().zzgn().zza("Invalid param value for number filter. event, param", zzy().zzaj(str), zzy().zzak(zzkr));
                        return null;
                    }
                } else {
                    zzab().zzgn().zza("No filter for String param. event, param", zzy().zzaj(str), zzy().zzak(zzkr));
                    return null;
                }
                if (bool == null) {
                    return null;
                }
                if (bool.booleanValue() == z) {
                    return false;
                }
            } else if (obj == null) {
                zzab().zzgs().zza("Missing param for filter. event, param", zzy().zzaj(str), zzy().zzak(zzkr));
                return false;
            } else {
                zzab().zzgn().zza("Unknown param type. event, param", zzy().zzaj(str), zzy().zzak(zzkr));
                return null;
            }
        }
    }

    private final Boolean zza(zzbk.zzd zzd, zzbs.zzk zzk) {
        zzbk.zzb zzli = zzd.zzli();
        boolean zzkq = zzli.zzkq();
        if (zzk.zzna()) {
            if (zzli.zzkn()) {
                return zza(zza(zzk.zznb(), zzli.zzko()), zzkq);
            }
            zzab().zzgn().zza("No number filter for long property. property", zzy().zzal(zzk.getName()));
            return null;
        } else if (zzk.zznd()) {
            if (zzli.zzkn()) {
                return zza(zza(zzk.zzne(), zzli.zzko()), zzkq);
            }
            zzab().zzgn().zza("No number filter for double property. property", zzy().zzal(zzk.getName()));
            return null;
        } else if (!zzk.zzmx()) {
            zzab().zzgn().zza("User property has no value, property", zzy().zzal(zzk.getName()));
            return null;
        } else if (zzli.zzkl()) {
            return zza(zza(zzk.zzmy(), zzli.zzkm()), zzkq);
        } else {
            if (!zzli.zzkn()) {
                zzab().zzgn().zza("No string or number filter defined. property", zzy().zzal(zzk.getName()));
            } else if (zzjo.zzbj(zzk.zzmy())) {
                return zza(zza(zzk.zzmy(), zzli.zzko()), zzkq);
            } else {
                zzab().zzgn().zza("Invalid user property value for Numeric number filter. property, value", zzy().zzal(zzk.getName()), zzk.zzmy());
            }
            return null;
        }
    }

    @VisibleForTesting
    private static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    @VisibleForTesting
    private final Boolean zza(String str, zzbk.zze zze) {
        String str2;
        List<String> list;
        Preconditions.checkNotNull(zze);
        if (str == null || !zze.zzlk() || zze.zzll() == zzbk.zze.zza.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (zze.zzll() == zzbk.zze.zza.IN_LIST) {
            if (zze.zzlr() == 0) {
                return null;
            }
        } else if (!zze.zzlm()) {
            return null;
        }
        zzbk.zze.zza zzll = zze.zzll();
        boolean zzlp = zze.zzlp();
        if (zzlp || zzll == zzbk.zze.zza.REGEXP || zzll == zzbk.zze.zza.IN_LIST) {
            str2 = zze.zzln();
        } else {
            str2 = zze.zzln().toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        if (zze.zzlr() == 0) {
            list = null;
        } else {
            List<String> zzlq = zze.zzlq();
            if (!zzlp) {
                ArrayList arrayList = new ArrayList(zzlq.size());
                for (String upperCase : zzlq) {
                    arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
                }
                zzlq = Collections.unmodifiableList(arrayList);
            }
            list = zzlq;
        }
        return zza(str, zzll, zzlp, str3, list, zzll == zzbk.zze.zza.REGEXP ? str3 : null);
    }

    private final Boolean zza(String str, zzbk.zze.zza zza, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (zza == zzbk.zze.zza.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && zza != zzbk.zze.zza.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (zzo.zzdu[zza.ordinal()]) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    zzab().zzgn().zza("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    private final Boolean zza(long j, zzbk.zzc zzc) {
        try {
            return zza(new BigDecimal(j), zzc, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(double d, zzbk.zzc zzc) {
        try {
            return zza(new BigDecimal(d), zzc, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(String str, zzbk.zzc zzc) {
        if (!zzjo.zzbj(str)) {
            return null;
        }
        try {
            return zza(new BigDecimal(str), zzc, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        if (r2 != null) goto L_0x0088;
     */
    @com.google.android.gms.common.util.VisibleForTesting
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Boolean zza(java.math.BigDecimal r7, com.google.android.gms.internal.measurement.zzbk.zzc r8, double r9) {
        /*
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r8)
            boolean r0 = r8.zzku()
            r1 = 0
            if (r0 == 0) goto L_0x010c
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r0 = r8.zzkv()
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r2 = com.google.android.gms.internal.measurement.zzbk.zzc.zzb.UNKNOWN_COMPARISON_TYPE
            if (r0 != r2) goto L_0x0014
            goto L_0x010c
        L_0x0014:
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r0 = r8.zzkv()
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r2 = com.google.android.gms.internal.measurement.zzbk.zzc.zzb.BETWEEN
            if (r0 != r2) goto L_0x0029
            boolean r0 = r8.zzla()
            if (r0 == 0) goto L_0x0028
            boolean r0 = r8.zzlc()
            if (r0 != 0) goto L_0x0030
        L_0x0028:
            return r1
        L_0x0029:
            boolean r0 = r8.zzky()
            if (r0 != 0) goto L_0x0030
            return r1
        L_0x0030:
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r0 = r8.zzkv()
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r2 = r8.zzkv()
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r3 = com.google.android.gms.internal.measurement.zzbk.zzc.zzb.BETWEEN
            if (r2 != r3) goto L_0x0068
            java.lang.String r2 = r8.zzlb()
            boolean r2 = com.google.android.gms.measurement.internal.zzjo.zzbj(r2)
            if (r2 == 0) goto L_0x0067
            java.lang.String r2 = r8.zzld()
            boolean r2 = com.google.android.gms.measurement.internal.zzjo.zzbj(r2)
            if (r2 != 0) goto L_0x0051
            goto L_0x0067
        L_0x0051:
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0066 }
            java.lang.String r3 = r8.zzlb()     // Catch:{ NumberFormatException -> 0x0066 }
            r2.<init>(r3)     // Catch:{ NumberFormatException -> 0x0066 }
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0066 }
            java.lang.String r8 = r8.zzld()     // Catch:{ NumberFormatException -> 0x0066 }
            r3.<init>(r8)     // Catch:{ NumberFormatException -> 0x0066 }
            r8 = r2
            r2 = r1
            goto L_0x007e
        L_0x0066:
            return r1
        L_0x0067:
            return r1
        L_0x0068:
            java.lang.String r2 = r8.zzkz()
            boolean r2 = com.google.android.gms.measurement.internal.zzjo.zzbj(r2)
            if (r2 != 0) goto L_0x0073
            return r1
        L_0x0073:
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x010b }
            java.lang.String r8 = r8.zzkz()     // Catch:{ NumberFormatException -> 0x010b }
            r2.<init>(r8)     // Catch:{ NumberFormatException -> 0x010b }
            r8 = r1
            r3 = r8
        L_0x007e:
            com.google.android.gms.internal.measurement.zzbk$zzc$zzb r4 = com.google.android.gms.internal.measurement.zzbk.zzc.zzb.BETWEEN
            if (r0 != r4) goto L_0x0086
            if (r8 == 0) goto L_0x0085
            goto L_0x0088
        L_0x0085:
            return r1
        L_0x0086:
            if (r2 == 0) goto L_0x010a
        L_0x0088:
            int[] r4 = com.google.android.gms.measurement.internal.zzo.zzdv
            int r0 = r0.ordinal()
            r0 = r4[r0]
            r4 = -1
            r5 = 0
            r6 = 1
            switch(r0) {
                case 1: goto L_0x00fe;
                case 2: goto L_0x00f2;
                case 3: goto L_0x00a9;
                case 4: goto L_0x0097;
                default: goto L_0x0096;
            }
        L_0x0096:
            goto L_0x010a
        L_0x0097:
            int r8 = r7.compareTo(r8)
            if (r8 == r4) goto L_0x00a4
            int r7 = r7.compareTo(r3)
            if (r7 == r6) goto L_0x00a4
            r5 = 1
        L_0x00a4:
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r5)
            return r7
        L_0x00a9:
            r0 = 0
            int r8 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r8 == 0) goto L_0x00e6
            java.math.BigDecimal r8 = new java.math.BigDecimal
            r8.<init>(r9)
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r1 = 2
            r0.<init>(r1)
            java.math.BigDecimal r8 = r8.multiply(r0)
            java.math.BigDecimal r8 = r2.subtract(r8)
            int r8 = r7.compareTo(r8)
            if (r8 != r6) goto L_0x00e1
            java.math.BigDecimal r8 = new java.math.BigDecimal
            r8.<init>(r9)
            java.math.BigDecimal r9 = new java.math.BigDecimal
            r9.<init>(r1)
            java.math.BigDecimal r8 = r8.multiply(r9)
            java.math.BigDecimal r8 = r2.add(r8)
            int r7 = r7.compareTo(r8)
            if (r7 != r4) goto L_0x00e1
            r5 = 1
        L_0x00e1:
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r5)
            return r7
        L_0x00e6:
            int r7 = r7.compareTo(r2)
            if (r7 != 0) goto L_0x00ed
            r5 = 1
        L_0x00ed:
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r5)
            return r7
        L_0x00f2:
            int r7 = r7.compareTo(r2)
            if (r7 != r6) goto L_0x00f9
            r5 = 1
        L_0x00f9:
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r5)
            return r7
        L_0x00fe:
            int r7 = r7.compareTo(r2)
            if (r7 != r4) goto L_0x0105
            r5 = 1
        L_0x0105:
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r5)
            return r7
        L_0x010a:
            return r1
        L_0x010b:
            return r1
        L_0x010c:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzp.zza(java.math.BigDecimal, com.google.android.gms.internal.measurement.zzbk$zzc, double):java.lang.Boolean");
    }

    private static List<zzbs.zzb> zza(Map<Integer, Long> map) {
        if (map == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Integer intValue : map.keySet()) {
            int intValue2 = intValue.intValue();
            arrayList.add((zzbs.zzb) ((zzey) zzbs.zzb.zzmh().zzk(intValue2).zzae(map.get(Integer.valueOf(intValue2)).longValue()).zzug()));
        }
        return arrayList;
    }

    private static void zza(Map<Integer, Long> map, int i, long j) {
        Long l = map.get(Integer.valueOf(i));
        long j2 = j / 1000;
        if (l == null || j2 > l.longValue()) {
            map.put(Integer.valueOf(i), Long.valueOf(j2));
        }
    }

    private static void zzb(Map<Integer, List<Long>> map, int i, long j) {
        List list = map.get(Integer.valueOf(i));
        if (list == null) {
            list = new ArrayList();
            map.put(Integer.valueOf(i), list);
        }
        list.add(Long.valueOf(j / 1000));
    }
}
