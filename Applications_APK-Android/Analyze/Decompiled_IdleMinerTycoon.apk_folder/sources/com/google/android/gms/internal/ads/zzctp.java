package com.google.android.gms.internal.ads;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.helpshift.analytics.AnalyticsEventKey;
import java.util.ArrayList;
import javax.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

public final class zzctp implements zzcva<zzcto> {
    private final PackageInfo zzdlm;
    private final zzaxb zzduk;
    private final zzcxv zzfjp;
    private final zzbbl zzfqw;

    public zzctp(zzbbl zzbbl, zzcxv zzcxv, @Nullable PackageInfo packageInfo, zzaxb zzaxb) {
        this.zzfqw = zzbbl;
        this.zzfjp = zzcxv;
        this.zzdlm = packageInfo;
        this.zzduk = zzaxb;
    }

    public final zzbbh<zzcto> zzalm() {
        return this.zzfqw.zza(new zzctq(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(ArrayList arrayList, Bundle bundle) {
        String str;
        int i;
        String str2;
        JSONArray optJSONArray;
        String str3;
        bundle.putInt("native_version", 3);
        bundle.putStringArrayList("native_templates", arrayList);
        bundle.putStringArrayList("native_custom_templates", this.zzfjp.zzgld);
        if (((Boolean) zzyt.zzpe().zzd(zzacu.zzcsg)).booleanValue() && this.zzfjp.zzdgs.versionCode > 3) {
            bundle.putBoolean("enable_native_media_orientation", true);
            switch (this.zzfjp.zzdgs.zzbqd) {
                case 1:
                    str3 = "any";
                    break;
                case 2:
                    str3 = "landscape";
                    break;
                case 3:
                    str3 = "portrait";
                    break;
                case 4:
                    str3 = MessengerShareContentUtility.IMAGE_RATIO_SQUARE;
                    break;
                default:
                    str3 = "unknown";
                    break;
            }
            if (!"unknown".equals(str3)) {
                bundle.putString("native_media_orientation", str3);
            }
        }
        switch (this.zzfjp.zzdgs.zzbqc) {
            case 0:
                str = "any";
                break;
            case 1:
                str = "portrait";
                break;
            case 2:
                str = "landscape";
                break;
            default:
                str = "unknown";
                break;
        }
        if (!"unknown".equals(str)) {
            bundle.putString("native_image_orientation", str);
        }
        bundle.putBoolean("native_multiple_images", this.zzfjp.zzdgs.zzbqe);
        bundle.putBoolean("use_custom_mute", this.zzfjp.zzdgs.zzbqh);
        PackageInfo packageInfo = this.zzdlm;
        if (packageInfo == null) {
            i = 0;
        } else {
            i = packageInfo.versionCode;
        }
        if (i > this.zzduk.zzvq()) {
            this.zzduk.zzvw();
            this.zzduk.zzct(i);
        }
        JSONObject zzvv = this.zzduk.zzvv();
        String jSONArray = (zzvv == null || (optJSONArray = zzvv.optJSONArray(this.zzfjp.zzglb)) == null) ? null : optJSONArray.toString();
        if (!TextUtils.isEmpty(jSONArray)) {
            bundle.putString("native_advanced_settings", jSONArray);
        }
        if (this.zzfjp.zzglg > 1) {
            bundle.putInt("max_num_ads", this.zzfjp.zzglg);
        }
        if (this.zzfjp.zzdne != null) {
            zzaiy zzaiy = this.zzfjp.zzdne;
            switch (zzaiy.zzdbe) {
                case 1:
                    str2 = "l";
                    break;
                case 2:
                    str2 = AnalyticsEventKey.PROTOCOL;
                    break;
                default:
                    int i2 = zzaiy.zzdbe;
                    StringBuilder sb = new StringBuilder(52);
                    sb.append("Instream ad video aspect ratio ");
                    sb.append(i2);
                    sb.append(" is wrong.");
                    zzbad.zzen(sb.toString());
                    str2 = "l";
                    break;
            }
            bundle.putString("ia_var", str2);
            bundle.putBoolean("instr", true);
        }
        if (this.zzfjp.zzamn() != null) {
            bundle.putBoolean("has_delayed_banner_listener", true);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcto zzalt() throws Exception {
        ArrayList<String> arrayList = this.zzfjp.zzglc;
        if (arrayList == null) {
            return zzctr.zzghl;
        }
        if (arrayList.isEmpty()) {
            return zzcts.zzghl;
        }
        return new zzctt(this, arrayList);
    }
}
