package com.google.android.gms.base;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int buttonSize = 2130903127;
        public static final int circleCrop = 2130903163;
        public static final int colorScheme = 2130903186;
        public static final int imageAspectRatio = 2130903359;
        public static final int imageAspectRatioAdjust = 2130903360;
        public static final int scopeUris = 2130903450;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131034183;
        public static final int common_google_signin_btn_text_dark_default = 2131034184;
        public static final int common_google_signin_btn_text_dark_disabled = 2131034185;
        public static final int common_google_signin_btn_text_dark_focused = 2131034186;
        public static final int common_google_signin_btn_text_dark_pressed = 2131034187;
        public static final int common_google_signin_btn_text_light = 2131034188;
        public static final int common_google_signin_btn_text_light_default = 2131034189;
        public static final int common_google_signin_btn_text_light_disabled = 2131034190;
        public static final int common_google_signin_btn_text_light_focused = 2131034191;
        public static final int common_google_signin_btn_text_light_pressed = 2131034192;
        public static final int common_google_signin_btn_tint = 2131034193;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131165314;
        public static final int common_google_signin_btn_icon_dark = 2131165315;
        public static final int common_google_signin_btn_icon_dark_focused = 2131165316;
        public static final int common_google_signin_btn_icon_dark_normal = 2131165317;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131165318;
        public static final int common_google_signin_btn_icon_disabled = 2131165319;
        public static final int common_google_signin_btn_icon_light = 2131165320;
        public static final int common_google_signin_btn_icon_light_focused = 2131165321;
        public static final int common_google_signin_btn_icon_light_normal = 2131165322;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131165323;
        public static final int common_google_signin_btn_text_dark = 2131165324;
        public static final int common_google_signin_btn_text_dark_focused = 2131165325;
        public static final int common_google_signin_btn_text_dark_normal = 2131165326;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131165327;
        public static final int common_google_signin_btn_text_disabled = 2131165328;
        public static final int common_google_signin_btn_text_light = 2131165329;
        public static final int common_google_signin_btn_text_light_focused = 2131165330;
        public static final int common_google_signin_btn_text_light_normal = 2131165331;
        public static final int common_google_signin_btn_text_light_normal_background = 2131165332;
        public static final int googleg_disabled_color_18 = 2131165339;
        public static final int googleg_standard_color_18 = 2131165340;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131230748;
        public static final int adjust_width = 2131230749;
        public static final int auto = 2131230777;
        public static final int dark = 2131230832;
        public static final int icon_only = 2131230920;
        public static final int light = 2131230935;
        public static final int none = 2131231017;
        public static final int standard = 2131231127;
        public static final int wide = 2131231190;

        private id() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131689543;
        public static final int common_google_play_services_enable_text = 2131689544;
        public static final int common_google_play_services_enable_title = 2131689545;
        public static final int common_google_play_services_install_button = 2131689546;
        public static final int common_google_play_services_install_text = 2131689547;
        public static final int common_google_play_services_install_title = 2131689548;
        public static final int common_google_play_services_notification_channel_name = 2131689549;
        public static final int common_google_play_services_notification_ticker = 2131689550;
        public static final int common_google_play_services_unsupported_text = 2131689552;
        public static final int common_google_play_services_update_button = 2131689553;
        public static final int common_google_play_services_update_text = 2131689554;
        public static final int common_google_play_services_update_title = 2131689555;
        public static final int common_google_play_services_updating_text = 2131689556;
        public static final int common_google_play_services_wear_update_text = 2131689557;
        public static final int common_open_on_phone = 2131689558;
        public static final int common_signin_button_text = 2131689559;
        public static final int common_signin_button_text_long = 2131689560;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.fluffyfairygames.idleminertycoon.R.attr.circleCrop, com.fluffyfairygames.idleminertycoon.R.attr.imageAspectRatio, com.fluffyfairygames.idleminertycoon.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.fluffyfairygames.idleminertycoon.R.attr.buttonSize, com.fluffyfairygames.idleminertycoon.R.attr.colorScheme, com.fluffyfairygames.idleminertycoon.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
