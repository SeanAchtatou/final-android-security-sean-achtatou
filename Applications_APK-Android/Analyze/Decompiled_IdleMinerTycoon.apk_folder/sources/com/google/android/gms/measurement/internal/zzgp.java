package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

public final class zzgp extends zzg {
    @VisibleForTesting
    protected zzhj zzpu;
    private zzgk zzpv;
    private final Set<zzgn> zzpw = new CopyOnWriteArraySet();
    private boolean zzpx;
    private final AtomicReference<String> zzpy = new AtomicReference<>();
    @VisibleForTesting
    protected boolean zzpz = true;

    protected zzgp(zzfj zzfj) {
        super(zzfj);
    }

    /* access modifiers changed from: protected */
    public final boolean zzbk() {
        return false;
    }

    public final void zzif() {
        if (getContext().getApplicationContext() instanceof Application) {
            ((Application) getContext().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.zzpu);
        }
    }

    public final Boolean zzig() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) zzaa().zza(atomicReference, MTGInterstitialActivity.WEB_LOAD_TIME, "boolean test flag value", new zzgo(this, atomicReference));
    }

    public final String zzih() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) zzaa().zza(atomicReference, MTGInterstitialActivity.WEB_LOAD_TIME, "String test flag value", new zzgy(this, atomicReference));
    }

    public final Long zzii() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) zzaa().zza(atomicReference, MTGInterstitialActivity.WEB_LOAD_TIME, "long test flag value", new zzha(this, atomicReference));
    }

    public final Integer zzij() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) zzaa().zza(atomicReference, MTGInterstitialActivity.WEB_LOAD_TIME, "int test flag value", new zzhd(this, atomicReference));
    }

    public final Double zzik() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) zzaa().zza(atomicReference, MTGInterstitialActivity.WEB_LOAD_TIME, "double test flag value", new zzhc(this, atomicReference));
    }

    public final void setMeasurementEnabled(boolean z) {
        zzbi();
        zzm();
        zzaa().zza(new zzhf(this, z));
    }

    public final void zza(boolean z) {
        zzbi();
        zzm();
        zzaa().zza(new zzhe(this, z));
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzg(boolean z) {
        zzo();
        zzm();
        zzbi();
        zzab().zzgr().zza("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        zzac().setMeasurementEnabled(z);
        zzil();
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzil() {
        if (zzad().zze(zzr().zzag(), zzak.zzik)) {
            zzo();
            String zzho = zzac().zzlx.zzho();
            if (zzho != null) {
                if ("unset".equals(zzho)) {
                    zza("app", "_npa", (Object) null, zzx().currentTimeMillis());
                } else {
                    zza("app", "_npa", Long.valueOf("true".equals(zzho) ? 1 : 0), zzx().currentTimeMillis());
                }
            }
        }
        if (!this.zzj.isEnabled() || !this.zzpz) {
            zzab().zzgr().zzao("Updating Scion state (FE)");
            zzs().zzip();
            return;
        }
        zzab().zzgr().zzao("Recording app launch after enabling measurement for the first time (FE)");
        zzim();
    }

    public final void setMinimumSessionDuration(long j) {
        zzm();
        zzaa().zza(new zzhh(this, j));
    }

    public final void setSessionTimeoutDuration(long j) {
        zzm();
        zzaa().zza(new zzhg(this, j));
    }

    public final void zza(String str, String str2, Bundle bundle, boolean z) {
        logEvent(str, str2, bundle, false, true, zzx().currentTimeMillis());
    }

    public final void logEvent(String str, String str2, Bundle bundle) {
        logEvent(str, str2, bundle, true, true, zzx().currentTimeMillis());
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zza(String str, String str2, Bundle bundle) {
        zzm();
        zzo();
        zza(str, str2, zzx().currentTimeMillis(), bundle);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zza(String str, String str2, long j, Bundle bundle) {
        zzm();
        zzo();
        zza(str, str2, j, bundle, true, this.zzpv == null || zzjs.zzbq(str2), false, null);
    }

    /* JADX WARN: Type inference failed for: r14v0, types: [boolean] */
    /* JADX WARN: Type inference failed for: r14v11 */
    /* JADX WARN: Type inference failed for: r14v12 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.zzhq.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzhr, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zziw.zza(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      com.google.android.gms.measurement.internal.zziw.zza(com.google.android.gms.measurement.internal.zziw, long):void
      com.google.android.gms.measurement.internal.zziw.zza(long, boolean):void
      com.google.android.gms.measurement.internal.zziw.zza(boolean, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0143  */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r27, java.lang.String r28, long r29, android.os.Bundle r31, boolean r32, boolean r33, boolean r34, java.lang.String r35) {
        /*
            r26 = this;
            r7 = r26
            r8 = r27
            r15 = r28
            r13 = r29
            r12 = r31
            r11 = r35
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r27)
            com.google.android.gms.measurement.internal.zzs r0 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzak.zzip
            boolean r0 = r0.zze(r11, r1)
            if (r0 != 0) goto L_0x001e
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r28)
        L_0x001e:
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r31)
            r26.zzo()
            r26.zzbi()
            com.google.android.gms.measurement.internal.zzfj r0 = r7.zzj
            boolean r0 = r0.isEnabled()
            if (r0 != 0) goto L_0x003d
            com.google.android.gms.measurement.internal.zzef r0 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgr()
            java.lang.String r1 = "Event not sent since app measurement is disabled"
            r0.zzao(r1)
            return
        L_0x003d:
            com.google.android.gms.measurement.internal.zzs r0 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdy r1 = r26.zzr()
            java.lang.String r1 = r1.zzag()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzak.zzix
            boolean r0 = r0.zze(r1, r2)
            if (r0 == 0) goto L_0x006f
            com.google.android.gms.measurement.internal.zzdy r0 = r26.zzr()
            java.util.List r0 = r0.zzbh()
            if (r0 == 0) goto L_0x006f
            boolean r0 = r0.contains(r15)
            if (r0 != 0) goto L_0x006f
            com.google.android.gms.measurement.internal.zzef r0 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgr()
            java.lang.String r1 = "Dropping non-safelisted event. event name, origin"
            r0.zza(r1, r15, r8)
            return
        L_0x006f:
            boolean r0 = r7.zzpx
            r10 = 0
            r16 = 0
            r5 = 1
            if (r0 != 0) goto L_0x00ca
            r7.zzpx = r5
            com.google.android.gms.measurement.internal.zzfj r0 = r7.zzj     // Catch:{ ClassNotFoundException -> 0x00bd }
            boolean r0 = r0.zzia()     // Catch:{ ClassNotFoundException -> 0x00bd }
            if (r0 != 0) goto L_0x0090
            java.lang.String r0 = "com.google.android.gms.tagmanager.TagManagerService"
            android.content.Context r1 = r26.getContext()     // Catch:{ ClassNotFoundException -> 0x00bd }
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x00bd }
            java.lang.Class r0 = java.lang.Class.forName(r0, r5, r1)     // Catch:{ ClassNotFoundException -> 0x00bd }
            goto L_0x0096
        L_0x0090:
            java.lang.String r0 = "com.google.android.gms.tagmanager.TagManagerService"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00bd }
        L_0x0096:
            java.lang.String r1 = "initialize"
            java.lang.Class[] r2 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x00ae }
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r2[r16] = r3     // Catch:{ Exception -> 0x00ae }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x00ae }
            java.lang.Object[] r1 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00ae }
            android.content.Context r2 = r26.getContext()     // Catch:{ Exception -> 0x00ae }
            r1[r16] = r2     // Catch:{ Exception -> 0x00ae }
            r0.invoke(r10, r1)     // Catch:{ Exception -> 0x00ae }
            goto L_0x00ca
        L_0x00ae:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzef r1 = r26.zzab()     // Catch:{ ClassNotFoundException -> 0x00bd }
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgn()     // Catch:{ ClassNotFoundException -> 0x00bd }
            java.lang.String r2 = "Failed to invoke Tag Manager's initialize() method"
            r1.zza(r2, r0)     // Catch:{ ClassNotFoundException -> 0x00bd }
            goto L_0x00ca
        L_0x00bd:
            com.google.android.gms.measurement.internal.zzef r0 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgq()
            java.lang.String r1 = "Tag Manager is not found and thus will not be used"
            r0.zzao(r1)
        L_0x00ca:
            com.google.android.gms.measurement.internal.zzs r0 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdy r1 = r26.zzr()
            java.lang.String r1 = r1.zzag()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzak.zzje
            boolean r0 = r0.zze(r1, r2)
            if (r0 == 0) goto L_0x0109
            java.lang.String r0 = "_cmp"
            boolean r0 = r0.equals(r15)
            if (r0 == 0) goto L_0x0109
            java.lang.String r0 = "gclid"
            boolean r0 = r12.containsKey(r0)
            if (r0 == 0) goto L_0x0109
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_lgclid"
            java.lang.String r0 = "gclid"
            java.lang.String r4 = r12.getString(r0)
            com.google.android.gms.common.util.Clock r0 = r26.zzx()
            long r17 = r0.currentTimeMillis()
            r1 = r26
            r14 = 1
            r5 = r17
            r1.zza(r2, r3, r4, r5)
            goto L_0x010a
        L_0x0109:
            r14 = 1
        L_0x010a:
            r0 = 40
            r1 = 2
            if (r34 == 0) goto L_0x0177
            r26.zzae()
            java.lang.String r2 = "_iap"
            boolean r2 = r2.equals(r15)
            if (r2 != 0) goto L_0x0177
            com.google.android.gms.measurement.internal.zzfj r2 = r7.zzj
            com.google.android.gms.measurement.internal.zzjs r2 = r2.zzz()
            java.lang.String r3 = "event"
            boolean r3 = r2.zzp(r3, r15)
            if (r3 != 0) goto L_0x012a
        L_0x0128:
            r2 = 2
            goto L_0x0141
        L_0x012a:
            java.lang.String r3 = "event"
            java.lang.String[] r4 = com.google.android.gms.measurement.internal.zzgj.zzpn
            boolean r3 = r2.zza(r3, r4, r15)
            if (r3 != 0) goto L_0x0137
            r2 = 13
            goto L_0x0141
        L_0x0137:
            java.lang.String r3 = "event"
            boolean r2 = r2.zza(r3, r0, r15)
            if (r2 != 0) goto L_0x0140
            goto L_0x0128
        L_0x0140:
            r2 = 0
        L_0x0141:
            if (r2 == 0) goto L_0x0177
            com.google.android.gms.measurement.internal.zzef r1 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgm()
            java.lang.String r3 = "Invalid public event name. Event will not be logged (FE)"
            com.google.android.gms.measurement.internal.zzed r4 = r26.zzy()
            java.lang.String r4 = r4.zzaj(r15)
            r1.zza(r3, r4)
            com.google.android.gms.measurement.internal.zzfj r1 = r7.zzj
            r1.zzz()
            java.lang.String r0 = com.google.android.gms.measurement.internal.zzjs.zza(r15, r0, r14)
            if (r15 == 0) goto L_0x016a
            int r16 = r28.length()
            r1 = r16
            goto L_0x016b
        L_0x016a:
            r1 = 0
        L_0x016b:
            com.google.android.gms.measurement.internal.zzfj r3 = r7.zzj
            com.google.android.gms.measurement.internal.zzjs r3 = r3.zzz()
            java.lang.String r4 = "_ev"
            r3.zza(r2, r4, r0, r1)
            return
        L_0x0177:
            r26.zzae()
            com.google.android.gms.measurement.internal.zzhq r2 = r26.zzt()
            com.google.android.gms.measurement.internal.zzhr r2 = r2.zzin()
            if (r2 == 0) goto L_0x018e
            java.lang.String r3 = "_sc"
            boolean r3 = r12.containsKey(r3)
            if (r3 != 0) goto L_0x018e
            r2.zzqx = r14
        L_0x018e:
            if (r32 == 0) goto L_0x0194
            if (r34 == 0) goto L_0x0194
            r3 = 1
            goto L_0x0195
        L_0x0194:
            r3 = 0
        L_0x0195:
            com.google.android.gms.measurement.internal.zzhq.zza(r2, r12, r3)
            java.lang.String r3 = "am"
            boolean r17 = r3.equals(r8)
            boolean r3 = com.google.android.gms.measurement.internal.zzjs.zzbq(r28)
            if (r32 == 0) goto L_0x01d7
            com.google.android.gms.measurement.internal.zzgk r4 = r7.zzpv
            if (r4 == 0) goto L_0x01d7
            if (r3 != 0) goto L_0x01d7
            if (r17 != 0) goto L_0x01d7
            com.google.android.gms.measurement.internal.zzef r0 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgr()
            java.lang.String r1 = "Passing event to registered event handler (FE)"
            com.google.android.gms.measurement.internal.zzed r2 = r26.zzy()
            java.lang.String r2 = r2.zzaj(r15)
            com.google.android.gms.measurement.internal.zzed r3 = r26.zzy()
            java.lang.String r3 = r3.zzc(r12)
            r0.zza(r1, r2, r3)
            com.google.android.gms.measurement.internal.zzgk r1 = r7.zzpv
            r2 = r27
            r3 = r28
            r4 = r31
            r5 = r29
            r1.interceptEvent(r2, r3, r4, r5)
            return
        L_0x01d7:
            com.google.android.gms.measurement.internal.zzfj r3 = r7.zzj
            boolean r3 = r3.zzie()
            if (r3 != 0) goto L_0x01e0
            return
        L_0x01e0:
            com.google.android.gms.measurement.internal.zzjs r3 = r26.zzz()
            int r3 = r3.zzbl(r15)
            if (r3 == 0) goto L_0x0226
            com.google.android.gms.measurement.internal.zzef r1 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgm()
            java.lang.String r2 = "Invalid event name. Event will not be logged (FE)"
            com.google.android.gms.measurement.internal.zzed r4 = r26.zzy()
            java.lang.String r4 = r4.zzaj(r15)
            r1.zza(r2, r4)
            r26.zzz()
            java.lang.String r0 = com.google.android.gms.measurement.internal.zzjs.zza(r15, r0, r14)
            if (r15 == 0) goto L_0x020d
            int r1 = r28.length()
            goto L_0x020e
        L_0x020d:
            r1 = 0
        L_0x020e:
            com.google.android.gms.measurement.internal.zzfj r2 = r7.zzj
            com.google.android.gms.measurement.internal.zzjs r2 = r2.zzz()
            java.lang.String r4 = "_ev"
            r27 = r2
            r28 = r35
            r29 = r3
            r30 = r4
            r31 = r0
            r32 = r1
            r27.zza(r28, r29, r30, r31, r32)
            return
        L_0x0226:
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.String r3 = "_o"
            r0[r16] = r3
            java.lang.String r3 = "_sn"
            r0[r14] = r3
            java.lang.String r3 = "_sc"
            r0[r1] = r3
            r1 = 3
            java.lang.String r3 = "_si"
            r0[r1] = r3
            java.util.List r0 = com.google.android.gms.common.util.CollectionUtils.listOf(r0)
            com.google.android.gms.measurement.internal.zzjs r9 = r26.zzz()
            r1 = 1
            r3 = r10
            r10 = r35
            r5 = r11
            r11 = r28
            r12 = r31
            r13 = r0
            r6 = 1
            r14 = r34
            r4 = r15
            r15 = r1
            android.os.Bundle r15 = r9.zza(r10, r11, r12, r13, r14, r15)
            if (r15 == 0) goto L_0x0288
            java.lang.String r1 = "_sc"
            boolean r1 = r15.containsKey(r1)
            if (r1 == 0) goto L_0x0288
            java.lang.String r1 = "_si"
            boolean r1 = r15.containsKey(r1)
            if (r1 != 0) goto L_0x0268
            goto L_0x0288
        L_0x0268:
            java.lang.String r1 = "_sn"
            java.lang.String r1 = r15.getString(r1)
            java.lang.String r3 = "_sc"
            java.lang.String r3 = r15.getString(r3)
            java.lang.String r9 = "_si"
            long r9 = r15.getLong(r9)
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            com.google.android.gms.measurement.internal.zzhr r10 = new com.google.android.gms.measurement.internal.zzhr
            long r11 = r9.longValue()
            r10.<init>(r1, r3, r11)
            goto L_0x0289
        L_0x0288:
            r10 = r3
        L_0x0289:
            if (r10 != 0) goto L_0x028d
            r14 = r2
            goto L_0x028e
        L_0x028d:
            r14 = r10
        L_0x028e:
            com.google.android.gms.measurement.internal.zzs r1 = r26.zzad()
            boolean r1 = r1.zzz(r5)
            r9 = 0
            if (r1 == 0) goto L_0x02c2
            r26.zzae()
            com.google.android.gms.measurement.internal.zzhq r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzhr r1 = r1.zzin()
            if (r1 == 0) goto L_0x02c2
            java.lang.String r1 = "_ae"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x02c2
            com.google.android.gms.measurement.internal.zziw r1 = r26.zzv()
            long r1 = r1.zzjb()
            int r3 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x02c2
            com.google.android.gms.measurement.internal.zzjs r3 = r26.zzz()
            r3.zzb(r15, r1)
        L_0x02c2:
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            r13.add(r15)
            com.google.android.gms.measurement.internal.zzjs r1 = r26.zzz()
            java.security.SecureRandom r1 = r1.zzjw()
            long r11 = r1.nextLong()
            com.google.android.gms.measurement.internal.zzs r1 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdy r2 = r26.zzr()
            java.lang.String r2 = r2.zzag()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.zzak.zzid
            boolean r1 = r1.zze(r2, r3)
            if (r1 == 0) goto L_0x0376
            com.google.android.gms.measurement.internal.zzeo r1 = r26.zzac()
            com.google.android.gms.measurement.internal.zzet r1 = r1.zzma
            long r1 = r1.get()
            int r3 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x0376
            com.google.android.gms.measurement.internal.zzeo r1 = r26.zzac()
            r2 = r29
            boolean r1 = r1.zzx(r2)
            if (r1 == 0) goto L_0x0376
            com.google.android.gms.measurement.internal.zzeo r1 = r26.zzac()
            com.google.android.gms.measurement.internal.zzeq r1 = r1.zzmd
            boolean r1 = r1.get()
            if (r1 == 0) goto L_0x0376
            com.google.android.gms.measurement.internal.zzef r1 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r6 = "Current session is expired, remove the session number and Id"
            r1.zzao(r6)
            com.google.android.gms.measurement.internal.zzs r1 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdy r6 = r26.zzr()
            java.lang.String r6 = r6.zzag()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.zzak.zzhz
            boolean r1 = r1.zze(r6, r9)
            if (r1 == 0) goto L_0x034c
            java.lang.String r6 = "auto"
            java.lang.String r9 = "_sid"
            r10 = 0
            com.google.android.gms.common.util.Clock r1 = r26.zzx()
            long r21 = r1.currentTimeMillis()
            r1 = r26
            r2 = r6
            r3 = r9
            r9 = r4
            r4 = r10
            r10 = r5
            r8 = 1
            r5 = r21
            r1.zza(r2, r3, r4, r5)
            goto L_0x034f
        L_0x034c:
            r9 = r4
            r10 = r5
            r8 = 1
        L_0x034f:
            com.google.android.gms.measurement.internal.zzs r1 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdy r2 = r26.zzr()
            java.lang.String r2 = r2.zzag()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.zzak.zzia
            boolean r1 = r1.zze(r2, r3)
            if (r1 == 0) goto L_0x0379
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_sno"
            r4 = 0
            com.google.android.gms.common.util.Clock r1 = r26.zzx()
            long r5 = r1.currentTimeMillis()
            r1 = r26
            r1.zza(r2, r3, r4, r5)
            goto L_0x0379
        L_0x0376:
            r9 = r4
            r10 = r5
            r8 = 1
        L_0x0379:
            com.google.android.gms.measurement.internal.zzs r1 = r26.zzad()
            com.google.android.gms.measurement.internal.zzdy r2 = r26.zzr()
            java.lang.String r2 = r2.zzag()
            boolean r1 = r1.zzy(r2)
            if (r1 == 0) goto L_0x03b2
            java.lang.String r1 = "extend_session"
            r2 = 0
            long r1 = r15.getLong(r1, r2)
            r3 = 1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x03b2
            com.google.android.gms.measurement.internal.zzef r1 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "EXTEND_SESSION param attached: initiate a new session or extend the current active session"
            r1.zzao(r2)
            com.google.android.gms.measurement.internal.zzfj r1 = r7.zzj
            com.google.android.gms.measurement.internal.zziw r1 = r1.zzv()
            r5 = r29
            r1.zza(r5, r8)
            goto L_0x03b4
        L_0x03b2:
            r5 = r29
        L_0x03b4:
            java.util.Set r1 = r15.keySet()
            int r2 = r31.size()
            java.lang.String[] r2 = new java.lang.String[r2]
            java.lang.Object[] r1 = r1.toArray(r2)
            java.lang.String[] r1 = (java.lang.String[]) r1
            java.util.Arrays.sort(r1)
            int r2 = r1.length
            r3 = 0
            r4 = 0
        L_0x03ca:
            if (r3 >= r2) goto L_0x0466
            r8 = r1[r3]
            java.lang.Object r18 = r15.get(r8)
            r26.zzz()
            r23 = r1
            android.os.Bundle[] r1 = com.google.android.gms.measurement.internal.zzjs.zzb(r18)
            if (r1 == 0) goto L_0x0445
            r24 = r2
            int r2 = r1.length
            r15.putInt(r8, r2)
            r2 = 0
        L_0x03e4:
            int r5 = r1.length
            if (r2 >= r5) goto L_0x043a
            r5 = r1[r2]
            r6 = 1
            com.google.android.gms.measurement.internal.zzhq.zza(r14, r5, r6)
            com.google.android.gms.measurement.internal.zzjs r18 = r26.zzz()
            java.lang.String r19 = "_ep"
            r20 = 0
            r9 = r18
            r10 = r35
            r6 = r11
            r11 = r19
            r12 = r5
            r5 = r13
            r13 = r0
            r18 = r14
            r14 = r34
            r25 = r0
            r0 = r15
            r15 = r20
            android.os.Bundle r9 = r9.zza(r10, r11, r12, r13, r14, r15)
            java.lang.String r10 = "_en"
            r11 = r28
            r9.putString(r10, r11)
            java.lang.String r10 = "_eid"
            r9.putLong(r10, r6)
            java.lang.String r10 = "_gn"
            r9.putString(r10, r8)
            java.lang.String r10 = "_ll"
            int r12 = r1.length
            r9.putInt(r10, r12)
            java.lang.String r10 = "_i"
            r9.putInt(r10, r2)
            r5.add(r9)
            int r2 = r2 + 1
            r15 = r0
            r13 = r5
            r9 = r11
            r14 = r18
            r0 = r25
            r10 = r35
            r11 = r6
            r7 = r26
            goto L_0x03e4
        L_0x043a:
            r25 = r0
            r6 = r11
            r5 = r13
            r18 = r14
            r0 = r15
            r11 = r9
            int r1 = r1.length
            int r4 = r4 + r1
            goto L_0x044f
        L_0x0445:
            r25 = r0
            r24 = r2
            r6 = r11
            r5 = r13
            r18 = r14
            r0 = r15
            r11 = r9
        L_0x044f:
            int r3 = r3 + 1
            r15 = r0
            r13 = r5
            r9 = r11
            r14 = r18
            r1 = r23
            r2 = r24
            r0 = r25
            r8 = 1
            r10 = r35
            r11 = r6
            r5 = r29
            r7 = r26
            goto L_0x03ca
        L_0x0466:
            r6 = r11
            r5 = r13
            r0 = r15
            r11 = r9
            if (r4 == 0) goto L_0x0476
            java.lang.String r1 = "_eid"
            r0.putLong(r1, r6)
            java.lang.String r1 = "_epc"
            r0.putInt(r1, r4)
        L_0x0476:
            r0 = 0
        L_0x0477:
            int r1 = r5.size()
            if (r0 >= r1) goto L_0x0503
            java.lang.Object r1 = r5.get(r0)
            android.os.Bundle r1 = (android.os.Bundle) r1
            if (r0 == 0) goto L_0x0487
            r2 = 1
            goto L_0x0488
        L_0x0487:
            r2 = 0
        L_0x0488:
            if (r2 == 0) goto L_0x048d
            java.lang.String r2 = "_ep"
            goto L_0x048e
        L_0x048d:
            r2 = r11
        L_0x048e:
            java.lang.String r3 = "_o"
            r7 = r27
            r8 = 1
            r1.putString(r3, r7)
            if (r33 == 0) goto L_0x04a0
            com.google.android.gms.measurement.internal.zzjs r3 = r26.zzz()
            android.os.Bundle r1 = r3.zzg(r1)
        L_0x04a0:
            r10 = r1
            com.google.android.gms.measurement.internal.zzef r1 = r26.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgr()
            java.lang.String r3 = "Logging event (FE)"
            com.google.android.gms.measurement.internal.zzed r4 = r26.zzy()
            java.lang.String r4 = r4.zzaj(r11)
            com.google.android.gms.measurement.internal.zzed r6 = r26.zzy()
            java.lang.String r6 = r6.zzc(r10)
            r1.zza(r3, r4, r6)
            com.google.android.gms.measurement.internal.zzai r12 = new com.google.android.gms.measurement.internal.zzai
            com.google.android.gms.measurement.internal.zzah r3 = new com.google.android.gms.measurement.internal.zzah
            r3.<init>(r10)
            r1 = r12
            r4 = r27
            r13 = r5
            r5 = r29
            r1.<init>(r2, r3, r4, r5)
            com.google.android.gms.measurement.internal.zzhv r1 = r26.zzs()
            r14 = r35
            r1.zzc(r12, r14)
            if (r17 != 0) goto L_0x04fc
            r12 = r26
            java.util.Set<com.google.android.gms.measurement.internal.zzgn> r1 = r12.zzpw
            java.util.Iterator r15 = r1.iterator()
        L_0x04e1:
            boolean r1 = r15.hasNext()
            if (r1 == 0) goto L_0x04fe
            java.lang.Object r1 = r15.next()
            com.google.android.gms.measurement.internal.zzgn r1 = (com.google.android.gms.measurement.internal.zzgn) r1
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>(r10)
            r2 = r27
            r3 = r28
            r5 = r29
            r1.onEvent(r2, r3, r4, r5)
            goto L_0x04e1
        L_0x04fc:
            r12 = r26
        L_0x04fe:
            int r0 = r0 + 1
            r5 = r13
            goto L_0x0477
        L_0x0503:
            r8 = 1
            r12 = r26
            r26.zzae()
            com.google.android.gms.measurement.internal.zzhq r0 = r26.zzt()
            com.google.android.gms.measurement.internal.zzhr r0 = r0.zzin()
            if (r0 == 0) goto L_0x0522
            java.lang.String r0 = "_ae"
            boolean r0 = r0.equals(r11)
            if (r0 == 0) goto L_0x0522
            com.google.android.gms.measurement.internal.zziw r0 = r26.zzv()
            r0.zza(r8, r8)
        L_0x0522:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzgp.zza(java.lang.String, java.lang.String, long, android.os.Bundle, boolean, boolean, boolean, java.lang.String):void");
    }

    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        boolean z3;
        zzm();
        String str3 = str == null ? "app" : str;
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        if (z2) {
            if (this.zzpv != null && !zzjs.zzbq(str2)) {
                z3 = false;
                zzb(str3, str2, j, bundle2, z2, z3, !z, null);
            }
        }
        z3 = true;
        zzb(str3, str2, j, bundle2, z2, z3, !z, null);
    }

    private final void zzb(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        zzaa().zza(new zzgr(this, str, str2, j, zzjs.zzh(bundle), z, z2, z3, str3));
    }

    public final void zzb(String str, String str2, Object obj, boolean z) {
        zza(str, str2, obj, z, zzx().currentTimeMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzjs.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzjs.zza(long, java.lang.String, java.lang.String):java.net.URL
      com.google.android.gms.measurement.internal.zzjs.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, boolean):java.lang.String */
    public final void zza(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        int i = 6;
        int i2 = 0;
        if (z) {
            i = zzz().zzbm(str2);
        } else {
            zzjs zzz = zzz();
            if (zzz.zzp("user property", str2)) {
                if (!zzz.zza("user property", zzgl.zzpp, str2)) {
                    i = 15;
                } else if (zzz.zza("user property", 24, str2)) {
                    i = 0;
                }
            }
        }
        if (i != 0) {
            zzz();
            String zza = zzjs.zza(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.zzj.zzz().zza(i, "_ev", zza, i2);
        } else if (obj != null) {
            int zzc = zzz().zzc(str2, obj);
            if (zzc != 0) {
                zzz();
                String zza2 = zzjs.zza(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.zzj.zzz().zza(zzc, "_ev", zza2, i2);
                return;
            }
            Object zzd = zzz().zzd(str2, obj);
            if (zzd != null) {
                zza(str3, str2, j, zzd);
            }
        } else {
            zza(str3, str2, j, (Object) null);
        }
    }

    private final void zza(String str, String str2, long j, Object obj) {
        zzaa().zza(new zzgq(this, str, str2, obj, j));
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zza(String str, String str2, Object obj, long j) {
        Preconditions.checkNotEmpty(str);
        Preconditions.checkNotEmpty(str2);
        zzo();
        zzm();
        zzbi();
        if (zzad().zze(zzr().zzag(), zzak.zzik) && FirebaseAnalytics.UserProperty.ALLOW_AD_PERSONALIZATION_SIGNALS.equals(str2)) {
            if (obj instanceof String) {
                String str3 = (String) obj;
                if (!TextUtils.isEmpty(str3)) {
                    obj = Long.valueOf("false".equals(str3.toLowerCase(Locale.ENGLISH)) ? 1 : 0);
                    str2 = "_npa";
                    zzac().zzlx.zzau(((Long) obj).longValue() == 1 ? "true" : "false");
                }
            }
            if (obj == null) {
                str2 = "_npa";
                zzac().zzlx.zzau("unset");
            }
        }
        String str4 = str2;
        Object obj2 = obj;
        if (!this.zzj.isEnabled()) {
            zzab().zzgr().zzao("User property not set since app measurement is disabled");
        } else if (this.zzj.zzie()) {
            zzab().zzgr().zza("Setting user property (FE)", zzy().zzaj(str4), obj2);
            zzs().zzb(new zzjn(str4, j, obj2, str));
        }
    }

    public final List<zzjn> zzh(boolean z) {
        zzm();
        zzbi();
        zzab().zzgr().zzao("Fetching user attributes (FE)");
        if (zzaa().zzhp()) {
            zzab().zzgk().zzao("Cannot get all user properties from analytics worker thread");
            return Collections.emptyList();
        } else if (zzr.isMainThread()) {
            zzab().zzgk().zzao("Cannot get all user properties from main thread");
            return Collections.emptyList();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.zzj.zzaa().zza(new zzgt(this, atomicReference, z));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e) {
                    zzab().zzgn().zza("Interrupted waiting for get user properties", e);
                }
            }
            List<zzjn> list = (List) atomicReference.get();
            if (list != null) {
                return list;
            }
            zzab().zzgn().zzao("Timed out waiting for get user properties");
            return Collections.emptyList();
        }
    }

    @Nullable
    public final String zzi() {
        zzm();
        return this.zzpy.get();
    }

    @Nullable
    public final String zzy(long j) {
        if (zzaa().zzhp()) {
            zzab().zzgk().zzao("Cannot retrieve app instance id from analytics worker thread");
            return null;
        } else if (zzr.isMainThread()) {
            zzab().zzgk().zzao("Cannot retrieve app instance id from main thread");
            return null;
        } else {
            long elapsedRealtime = zzx().elapsedRealtime();
            String zzz = zzz(120000);
            long elapsedRealtime2 = zzx().elapsedRealtime() - elapsedRealtime;
            return (zzz != null || elapsedRealtime2 >= 120000) ? zzz : zzz(120000 - elapsedRealtime2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzbg(@Nullable String str) {
        this.zzpy.set(str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        zzab().zzgn().zzao("Interrupted waiting for app instance id");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x001d */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.String zzz(long r4) {
        /*
            r3 = this;
            java.util.concurrent.atomic.AtomicReference r0 = new java.util.concurrent.atomic.AtomicReference
            r0.<init>()
            monitor-enter(r0)
            com.google.android.gms.measurement.internal.zzfc r1 = r3.zzaa()     // Catch:{ all -> 0x002d }
            com.google.android.gms.measurement.internal.zzgs r2 = new com.google.android.gms.measurement.internal.zzgs     // Catch:{ all -> 0x002d }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x002d }
            r1.zza(r2)     // Catch:{ all -> 0x002d }
            r0.wait(r4)     // Catch:{ InterruptedException -> 0x001d }
            monitor-exit(r0)     // Catch:{ all -> 0x002d }
            java.lang.Object r4 = r0.get()
            java.lang.String r4 = (java.lang.String) r4
            return r4
        L_0x001d:
            com.google.android.gms.measurement.internal.zzef r4 = r3.zzab()     // Catch:{ all -> 0x002d }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgn()     // Catch:{ all -> 0x002d }
            java.lang.String r5 = "Interrupted waiting for app instance id"
            r4.zzao(r5)     // Catch:{ all -> 0x002d }
            r4 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x002d }
            return r4
        L_0x002d:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002d }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzgp.zzz(long):java.lang.String");
    }

    public final void resetAnalyticsData(long j) {
        zzbg(null);
        zzaa().zza(new zzgv(this, j));
    }

    @WorkerThread
    public final void zzim() {
        zzo();
        zzm();
        zzbi();
        if (this.zzj.zzie()) {
            zzs().zzim();
            this.zzpz = false;
            String zzhh = zzac().zzhh();
            if (!TextUtils.isEmpty(zzhh)) {
                zzw().zzbi();
                if (!zzhh.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", zzhh);
                    logEvent("auto", "_ou", bundle);
                }
            }
        }
    }

    @WorkerThread
    public final void zza(zzgk zzgk) {
        zzo();
        zzm();
        zzbi();
        if (!(zzgk == null || zzgk == this.zzpv)) {
            Preconditions.checkState(this.zzpv == null, "EventInterceptor already set.");
        }
        this.zzpv = zzgk;
    }

    public final void zza(zzgn zzgn) {
        zzm();
        zzbi();
        Preconditions.checkNotNull(zzgn);
        if (!this.zzpw.add(zzgn)) {
            zzab().zzgn().zzao("OnEventListener already registered");
        }
    }

    public final void zzb(zzgn zzgn) {
        zzm();
        zzbi();
        Preconditions.checkNotNull(zzgn);
        if (!this.zzpw.remove(zzgn)) {
            zzab().zzgn().zzao("OnEventListener had not been registered");
        }
    }

    public final void setConditionalUserProperty(Bundle bundle) {
        setConditionalUserProperty(bundle, zzx().currentTimeMillis());
    }

    public final void setConditionalUserProperty(Bundle bundle, long j) {
        Preconditions.checkNotNull(bundle);
        zzm();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            zzab().zzgn().zzao("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        zza(bundle2, j);
    }

    public final void zzd(Bundle bundle) {
        Preconditions.checkNotNull(bundle);
        Preconditions.checkNotEmpty(bundle.getString("app_id"));
        zzl();
        zza(new Bundle(bundle), zzx().currentTimeMillis());
    }

    private final void zza(Bundle bundle, long j) {
        Preconditions.checkNotNull(bundle);
        zzgg.zza(bundle, "app_id", String.class, null);
        zzgg.zza(bundle, "origin", String.class, null);
        zzgg.zza(bundle, "name", String.class, null);
        zzgg.zza(bundle, "value", Object.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME, String.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT, Long.class, 0L);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_NAME, String.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_PARAMS, Bundle.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_NAME, String.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_PARAMS, Bundle.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE, Long.class, 0L);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME, String.class, null);
        zzgg.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS, Bundle.class, null);
        Preconditions.checkNotEmpty(bundle.getString("name"));
        Preconditions.checkNotEmpty(bundle.getString("origin"));
        Preconditions.checkNotNull(bundle.get("value"));
        bundle.putLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP, j);
        String string = bundle.getString("name");
        Object obj = bundle.get("value");
        if (zzz().zzbm(string) != 0) {
            zzab().zzgk().zza("Invalid conditional user property name", zzy().zzal(string));
        } else if (zzz().zzc(string, obj) != 0) {
            zzab().zzgk().zza("Invalid conditional user property value", zzy().zzal(string), obj);
        } else {
            Object zzd = zzz().zzd(string, obj);
            if (zzd == null) {
                zzab().zzgk().zza("Unable to normalize conditional user property value", zzy().zzal(string), obj);
                return;
            }
            zzgg.zza(bundle, zzd);
            long j2 = bundle.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT);
            if (TextUtils.isEmpty(bundle.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME)) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle.getLong(AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE);
                if (j3 > 15552000000L || j3 < 1) {
                    zzab().zzgk().zza("Invalid conditional user property time to live", zzy().zzal(string), Long.valueOf(j3));
                } else {
                    zzaa().zza(new zzgx(this, bundle));
                }
            } else {
                zzab().zzgk().zza("Invalid conditional user property timeout", zzy().zzal(string), Long.valueOf(j2));
            }
        }
    }

    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        zzm();
        zza((String) null, str, str2, bundle);
    }

    public final void clearConditionalUserPropertyAs(String str, String str2, String str3, Bundle bundle) {
        Preconditions.checkNotEmpty(str);
        zzl();
        zza(str, str2, str3, bundle);
    }

    private final void zza(String str, String str2, String str3, Bundle bundle) {
        long currentTimeMillis = zzx().currentTimeMillis();
        Preconditions.checkNotEmpty(str2);
        Bundle bundle2 = new Bundle();
        if (str != null) {
            bundle2.putString("app_id", str);
        }
        bundle2.putString("name", str2);
        bundle2.putLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP, currentTimeMillis);
        if (str3 != null) {
            bundle2.putString(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME, str3);
            bundle2.putBundle(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS, bundle);
        }
        zzaa().zza(new zzgw(this, bundle2));
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zze(Bundle bundle) {
        Bundle bundle2 = bundle;
        zzo();
        zzbi();
        Preconditions.checkNotNull(bundle);
        Preconditions.checkNotEmpty(bundle2.getString("name"));
        Preconditions.checkNotEmpty(bundle2.getString("origin"));
        Preconditions.checkNotNull(bundle2.get("value"));
        if (!this.zzj.isEnabled()) {
            zzab().zzgr().zzao("Conditional property not sent since collection is disabled");
            return;
        }
        zzjn zzjn = new zzjn(bundle2.getString("name"), bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_TIMESTAMP), bundle2.get("value"), bundle2.getString("origin"));
        try {
            zzai zza = zzz().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_PARAMS), bundle2.getString("origin"), 0, true, false);
            zzai zza2 = zzz().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_PARAMS), bundle2.getString("origin"), 0, true, false);
            zzai zza3 = zzz().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS), bundle2.getString("origin"), 0, true, false);
            String string = bundle2.getString("app_id");
            String string2 = bundle2.getString("origin");
            long j = bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP);
            String string3 = bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME);
            long j2 = bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT);
            long j3 = bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE);
            zzq zzq = r3;
            zzq zzq2 = new zzq(string, string2, zzjn, j, false, string3, zza2, j2, zza, j3, zza3);
            zzs().zzd(zzq);
        } catch (IllegalArgumentException unused) {
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zzf(Bundle bundle) {
        Bundle bundle2 = bundle;
        zzo();
        zzbi();
        Preconditions.checkNotNull(bundle);
        Preconditions.checkNotEmpty(bundle2.getString("name"));
        if (!this.zzj.isEnabled()) {
            zzab().zzgr().zzao("Conditional property not cleared since collection is disabled");
            return;
        }
        zzjn zzjn = new zzjn(bundle2.getString("name"), 0, null, null);
        try {
            zzai zza = zzz().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS), bundle2.getString("origin"), bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP), true, false);
            String string = bundle2.getString("app_id");
            String string2 = bundle2.getString("origin");
            long j = bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP);
            boolean z = bundle2.getBoolean(AppMeasurementSdk.ConditionalUserProperty.ACTIVE);
            String string3 = bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME);
            long j2 = bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT);
            long j3 = bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE);
            zzq zzq = r3;
            zzq zzq2 = new zzq(string, string2, zzjn, j, z, string3, null, j2, null, j3, zza);
            zzs().zzd(zzq);
        } catch (IllegalArgumentException unused) {
        }
    }

    public final ArrayList<Bundle> zzn(String str, String str2) {
        zzm();
        return zze(null, str, str2);
    }

    public final ArrayList<Bundle> zzd(String str, String str2, String str3) {
        Preconditions.checkNotEmpty(str);
        zzl();
        return zze(str, str2, str3);
    }

    @VisibleForTesting
    private final ArrayList<Bundle> zze(String str, String str2, String str3) {
        if (zzaa().zzhp()) {
            zzab().zzgk().zzao("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (zzr.isMainThread()) {
            zzab().zzgk().zzao("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.zzj.zzaa().zza(new zzgz(this, atomicReference, str, str2, str3));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e) {
                    zzab().zzgn().zza("Interrupted waiting for get conditional user properties", str, e);
                }
            }
            List list = (List) atomicReference.get();
            if (list != null) {
                return zzjs.zzd(list);
            }
            zzab().zzgn().zza("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    public final Map<String, Object> getUserProperties(String str, String str2, boolean z) {
        zzm();
        return zzb((String) null, str, str2, z);
    }

    public final Map<String, Object> getUserPropertiesAs(String str, String str2, String str3, boolean z) {
        Preconditions.checkNotEmpty(str);
        zzl();
        return zzb(str, str2, str3, z);
    }

    @VisibleForTesting
    private final Map<String, Object> zzb(String str, String str2, String str3, boolean z) {
        if (zzaa().zzhp()) {
            zzab().zzgk().zzao("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (zzr.isMainThread()) {
            zzab().zzgk().zzao("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.zzj.zzaa().zza(new zzhb(this, atomicReference, str, str2, str3, z));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e) {
                    zzab().zzgn().zza("Interrupted waiting for get user properties", e);
                }
            }
            List<zzjn> list = (List) atomicReference.get();
            if (list == null) {
                zzab().zzgn().zzao("Timed out waiting for get user properties");
                return Collections.emptyMap();
            }
            ArrayMap arrayMap = new ArrayMap(list.size());
            for (zzjn zzjn : list) {
                arrayMap.put(zzjn.name, zzjn.getValue());
            }
            return arrayMap;
        }
    }

    @Nullable
    public final String getCurrentScreenName() {
        zzhr zzio = this.zzj.zzt().zzio();
        if (zzio != null) {
            return zzio.zzqu;
        }
        return null;
    }

    @Nullable
    public final String getCurrentScreenClass() {
        zzhr zzio = this.zzj.zzt().zzio();
        if (zzio != null) {
            return zzio.zzqv;
        }
        return null;
    }

    @Nullable
    public final String getGmpAppId() {
        if (this.zzj.zzhx() != null) {
            return this.zzj.zzhx();
        }
        try {
            return GoogleServices.getGoogleAppId();
        } catch (IllegalStateException e) {
            this.zzj.zzab().zzgk().zza("getGoogleAppId failed with exception", e);
            return null;
        }
    }

    public final /* bridge */ /* synthetic */ void zzl() {
        super.zzl();
    }

    public final /* bridge */ /* synthetic */ void zzm() {
        super.zzm();
    }

    public final /* bridge */ /* synthetic */ void zzn() {
        super.zzn();
    }

    public final /* bridge */ /* synthetic */ void zzo() {
        super.zzo();
    }

    public final /* bridge */ /* synthetic */ zza zzp() {
        return super.zzp();
    }

    public final /* bridge */ /* synthetic */ zzgp zzq() {
        return super.zzq();
    }

    public final /* bridge */ /* synthetic */ zzdy zzr() {
        return super.zzr();
    }

    public final /* bridge */ /* synthetic */ zzhv zzs() {
        return super.zzs();
    }

    public final /* bridge */ /* synthetic */ zzhq zzt() {
        return super.zzt();
    }

    public final /* bridge */ /* synthetic */ zzeb zzu() {
        return super.zzu();
    }

    public final /* bridge */ /* synthetic */ zziw zzv() {
        return super.zzv();
    }

    public final /* bridge */ /* synthetic */ zzac zzw() {
        return super.zzw();
    }

    public final /* bridge */ /* synthetic */ Clock zzx() {
        return super.zzx();
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public final /* bridge */ /* synthetic */ zzed zzy() {
        return super.zzy();
    }

    public final /* bridge */ /* synthetic */ zzjs zzz() {
        return super.zzz();
    }

    public final /* bridge */ /* synthetic */ zzfc zzaa() {
        return super.zzaa();
    }

    public final /* bridge */ /* synthetic */ zzef zzab() {
        return super.zzab();
    }

    public final /* bridge */ /* synthetic */ zzeo zzac() {
        return super.zzac();
    }

    public final /* bridge */ /* synthetic */ zzs zzad() {
        return super.zzad();
    }

    public final /* bridge */ /* synthetic */ zzr zzae() {
        return super.zzae();
    }
}
