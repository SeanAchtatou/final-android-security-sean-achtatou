package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Stack;

final class zzja implements zzjd {
    private final byte[] zzanf = new byte[8];
    private final Stack<zzjc> zzang = new Stack<>();
    private final zzjf zzanh = new zzjf();
    private zzje zzani;
    private int zzanj;
    private int zzank;
    private long zzanl;

    zzja() {
    }

    public final void zza(zzje zzje) {
        this.zzani = zzje;
    }

    public final void reset() {
        this.zzanj = 0;
        this.zzang.clear();
        this.zzanh.reset();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016e A[LOOP:0: B:5:0x000c->B:52:0x016e, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x006c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0085 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00ce A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00db A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x011c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0014  */
    public final boolean zza(com.google.android.gms.internal.ads.zzie r10) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r9 = this;
            com.google.android.gms.internal.ads.zzje r0 = r9.zzani
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0008
            r0 = 1
            goto L_0x0009
        L_0x0008:
            r0 = 0
        L_0x0009:
            com.google.android.gms.internal.ads.zzkh.checkState(r0)
        L_0x000c:
            java.util.Stack<com.google.android.gms.internal.ads.zzjc> r0 = r9.zzang
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x003a
            long r3 = r10.getPosition()
            java.util.Stack<com.google.android.gms.internal.ads.zzjc> r0 = r9.zzang
            java.lang.Object r0 = r0.peek()
            com.google.android.gms.internal.ads.zzjc r0 = (com.google.android.gms.internal.ads.zzjc) r0
            long r5 = r0.zzanm
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x003a
            com.google.android.gms.internal.ads.zzje r10 = r9.zzani
            java.util.Stack<com.google.android.gms.internal.ads.zzjc> r0 = r9.zzang
            java.lang.Object r0 = r0.pop()
            com.google.android.gms.internal.ads.zzjc r0 = (com.google.android.gms.internal.ads.zzjc) r0
            int r0 = r0.zzank
            r10.zzy(r0)
            return r2
        L_0x003a:
            int r0 = r9.zzanj
            if (r0 != 0) goto L_0x0050
            com.google.android.gms.internal.ads.zzjf r0 = r9.zzanh
            long r3 = r0.zza(r10, r2, r1)
            r5 = -1
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x004b
            return r1
        L_0x004b:
            int r0 = (int) r3
            r9.zzank = r0
            r9.zzanj = r2
        L_0x0050:
            int r0 = r9.zzanj
            if (r0 != r2) goto L_0x005f
            com.google.android.gms.internal.ads.zzjf r0 = r9.zzanh
            long r3 = r0.zza(r10, r1, r2)
            r9.zzanl = r3
            r0 = 2
            r9.zzanj = r0
        L_0x005f:
            com.google.android.gms.internal.ads.zzje r0 = r9.zzani
            int r3 = r9.zzank
            int r0 = r0.zzx(r3)
            r3 = 8
            switch(r0) {
                case 0: goto L_0x016e;
                case 1: goto L_0x014e;
                case 2: goto L_0x011c;
                case 3: goto L_0x00db;
                case 4: goto L_0x00ce;
                case 5: goto L_0x0085;
                default: goto L_0x006c;
            }
        L_0x006c:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r2 = 32
            r1.<init>(r2)
            java.lang.String r2 = "Invalid element type "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r10.<init>(r0)
            throw r10
        L_0x0085:
            long r5 = r9.zzanl
            r7 = 4
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x00af
            long r5 = r9.zzanl
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x0094
            goto L_0x00af
        L_0x0094:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            long r0 = r9.zzanl
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r3 = 40
            r2.<init>(r3)
            java.lang.String r3 = "Invalid float size: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r10.<init>(r0)
            throw r10
        L_0x00af:
            com.google.android.gms.internal.ads.zzje r0 = r9.zzani
            int r3 = r9.zzank
            long r4 = r9.zzanl
            int r4 = (int) r4
            long r5 = r9.zzc(r10, r4)
            r10 = 4
            if (r4 != r10) goto L_0x00c4
            int r10 = (int) r5
            float r10 = java.lang.Float.intBitsToFloat(r10)
            double r4 = (double) r10
            goto L_0x00c8
        L_0x00c4:
            double r4 = java.lang.Double.longBitsToDouble(r5)
        L_0x00c8:
            r0.zza(r3, r4)
            r9.zzanj = r1
            return r2
        L_0x00ce:
            com.google.android.gms.internal.ads.zzje r0 = r9.zzani
            int r3 = r9.zzank
            long r4 = r9.zzanl
            int r4 = (int) r4
            r0.zza(r3, r4, r10)
            r9.zzanj = r1
            return r2
        L_0x00db:
            long r3 = r9.zzanl
            r5 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x0101
            com.google.android.gms.internal.ads.zzje r0 = r9.zzani
            int r3 = r9.zzank
            long r4 = r9.zzanl
            int r4 = (int) r4
            byte[] r5 = new byte[r4]
            r10.readFully(r5, r1, r4)
            java.lang.String r10 = new java.lang.String
            java.lang.String r4 = "UTF-8"
            java.nio.charset.Charset r4 = java.nio.charset.Charset.forName(r4)
            r10.<init>(r5, r4)
            r0.zza(r3, r10)
            r9.zzanj = r1
            return r2
        L_0x0101:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            long r0 = r9.zzanl
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r3 = 41
            r2.<init>(r3)
            java.lang.String r3 = "String element size: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r10.<init>(r0)
            throw r10
        L_0x011c:
            long r5 = r9.zzanl
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x0133
            com.google.android.gms.internal.ads.zzje r0 = r9.zzani
            int r3 = r9.zzank
            long r4 = r9.zzanl
            int r4 = (int) r4
            long r4 = r9.zzc(r10, r4)
            r0.zzc(r3, r4)
            r9.zzanj = r1
            return r2
        L_0x0133:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            long r0 = r9.zzanl
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r3 = 42
            r2.<init>(r3)
            java.lang.String r3 = "Invalid integer size: "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r10.<init>(r0)
            throw r10
        L_0x014e:
            long r5 = r10.getPosition()
            long r3 = r9.zzanl
            long r3 = r3 + r5
            java.util.Stack<com.google.android.gms.internal.ads.zzjc> r10 = r9.zzang
            com.google.android.gms.internal.ads.zzjc r0 = new com.google.android.gms.internal.ads.zzjc
            int r7 = r9.zzank
            r8 = 0
            r0.<init>(r7, r3)
            r10.add(r0)
            com.google.android.gms.internal.ads.zzje r3 = r9.zzani
            int r4 = r9.zzank
            long r7 = r9.zzanl
            r3.zzb(r4, r5, r7)
            r9.zzanj = r1
            return r2
        L_0x016e:
            long r3 = r9.zzanl
            int r0 = (int) r3
            r10.zzr(r0)
            r9.zzanj = r1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzja.zza(com.google.android.gms.internal.ads.zzie):boolean");
    }

    private final long zzc(zzie zzie, int i) throws IOException, InterruptedException {
        zzie.readFully(this.zzanf, 0, i);
        long j = 0;
        for (int i2 = 0; i2 < i; i2++) {
            j = (j << 8) | ((long) (this.zzanf[i2] & 255));
        }
        return j;
    }
}
