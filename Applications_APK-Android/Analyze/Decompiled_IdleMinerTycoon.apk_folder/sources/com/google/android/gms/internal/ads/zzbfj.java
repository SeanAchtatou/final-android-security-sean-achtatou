package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

@zzard
final class zzbfj implements zzrv {
    private Uri uri;
    private final long zzegg;
    private long zzegi;
    private final zzrv zzehm;
    private final zzrv zzehn;

    zzbfj(zzrv zzrv, int i, zzrv zzrv2) {
        this.zzehm = zzrv;
        this.zzegg = (long) i;
        this.zzehn = zzrv2;
    }

    public final long zza(zzry zzry) throws IOException {
        zzry zzry2;
        long j;
        zzry zzry3 = zzry;
        this.uri = zzry3.uri;
        zzry zzry4 = null;
        if (zzry3.zzahv >= this.zzegg) {
            zzry2 = null;
        } else {
            long j2 = zzry3.zzahv;
            if (zzry3.zzcd != -1) {
                j = Math.min(zzry3.zzcd, this.zzegg - j2);
            } else {
                j = this.zzegg - j2;
            }
            zzry2 = new zzry(zzry3.uri, j2, j, null);
        }
        if (zzry3.zzcd == -1 || zzry3.zzahv + zzry3.zzcd > this.zzegg) {
            zzry4 = new zzry(zzry3.uri, Math.max(this.zzegg, zzry3.zzahv), zzry3.zzcd != -1 ? Math.min(zzry3.zzcd, (zzry3.zzahv + zzry3.zzcd) - this.zzegg) : -1, null);
        }
        long j3 = 0;
        long zza = zzry2 != null ? this.zzehm.zza(zzry2) : 0;
        if (zzry4 != null) {
            j3 = this.zzehn.zza(zzry4);
        }
        this.zzegi = zzry3.zzahv;
        if (zza == -1 || j3 == -1) {
            return -1;
        }
        return zza + j3;
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        if (this.zzegi < this.zzegg) {
            i3 = this.zzehm.read(bArr, i, (int) Math.min((long) i2, this.zzegg - this.zzegi));
            this.zzegi += (long) i3;
        } else {
            i3 = 0;
        }
        if (this.zzegi < this.zzegg) {
            return i3;
        }
        int read = this.zzehn.read(bArr, i + i3, i2 - i3);
        int i4 = i3 + read;
        this.zzegi += (long) read;
        return i4;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final void close() throws IOException {
        this.zzehm.close();
        this.zzehn.close();
    }
}
