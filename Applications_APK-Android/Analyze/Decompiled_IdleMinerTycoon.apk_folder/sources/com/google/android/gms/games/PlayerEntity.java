package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.MostRecentGameInfoEntity;
import com.tapjoy.TJAdUnitConstants;

@RetainForClient
@UsedByReflection("GamesClientImpl.java")
@SafeParcelable.Class(creator = "PlayerEntityCreator")
@SafeParcelable.Reserved({1000})
public final class PlayerEntity extends GamesDowngradeableSafeParcel implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new zza();
    @SafeParcelable.Field(getter = "getName", id = 21)
    private final String name;
    @Nullable
    @SafeParcelable.Field(getter = "getIconImageUrl", id = 8)
    private final String zzac;
    @Nullable
    @SafeParcelable.Field(getter = "getHiResImageUrl", id = 9)
    private final String zzad;
    @SafeParcelable.Field(getter = "getPlayerId", id = 1)
    private String zzbz;
    @SafeParcelable.Field(getter = "getRetrievedTimestamp", id = 5)
    private final long zzca;
    @SafeParcelable.Field(getter = "isInCircles", id = 6)
    private final int zzcb;
    @SafeParcelable.Field(getter = "getLastPlayedWithTimestamp", id = 7)
    private final long zzcc;
    @Nullable
    @SafeParcelable.Field(getter = "getTitle", id = 14)
    private final String zzcd;
    @Nullable
    @SafeParcelable.Field(getter = "getMostRecentGameInfo", id = 15)
    private final MostRecentGameInfoEntity zzce;
    @Nullable
    @SafeParcelable.Field(getter = "getLevelInfo", id = 16)
    private final PlayerLevelInfo zzcf;
    @SafeParcelable.Field(getter = "isProfileVisible", id = 18)
    private final boolean zzcg;
    @SafeParcelable.Field(getter = "hasDebugAccess", id = 19)
    private final boolean zzch;
    @Nullable
    @SafeParcelable.Field(getter = "getGamerTag", id = 20)
    private final String zzci;
    @Nullable
    @SafeParcelable.Field(getter = "getBannerImageLandscapeUri", id = 22)
    private final Uri zzcj;
    @Nullable
    @SafeParcelable.Field(getter = "getBannerImageLandscapeUrl", id = 23)
    private final String zzck;
    @Nullable
    @SafeParcelable.Field(getter = "getBannerImagePortraitUri", id = 24)
    private final Uri zzcl;
    @Nullable
    @SafeParcelable.Field(getter = "getBannerImagePortraitUrl", id = 25)
    private final String zzcm;
    @SafeParcelable.Field(getter = "getGamerFriendStatus", id = 26)
    private final int zzcn;
    @SafeParcelable.Field(getter = "getGamerFriendUpdateTimestamp", id = 27)
    private final long zzco;
    @SafeParcelable.Field(getter = TJAdUnitConstants.String.IS_MUTED, id = 28)
    private final boolean zzcp;
    @SafeParcelable.Field(defaultValue = "-1", getter = "getTotalUnlockedAchievement", id = 29)
    private final long zzcq;
    @SafeParcelable.Field(getter = "getDisplayName", id = 2)
    private String zzn;
    @Nullable
    @SafeParcelable.Field(getter = "getIconImageUri", id = 3)
    private final Uri zzr;
    @Nullable
    @SafeParcelable.Field(getter = "getHiResImageUri", id = 4)
    private final Uri zzs;

    static final class zza extends zzap {
        zza() {
        }

        public final PlayerEntity zzc(Parcel parcel) {
            Uri uri;
            Uri uri2;
            if (PlayerEntity.zzb(PlayerEntity.getUnparcelClientVersion()) || PlayerEntity.canUnparcelSafely(PlayerEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            long readLong = parcel.readLong();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            if (readString3 == null) {
                uri = null;
            } else {
                uri = Uri.parse(readString3);
            }
            if (readString4 == null) {
                uri2 = null;
            } else {
                uri2 = Uri.parse(readString4);
            }
            return new PlayerEntity(readString, readString2, uri, uri2, readLong, -1, -1, null, null, null, null, null, true, false, readString5, readString6, null, null, null, null, -1, -1, false, -1);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    public PlayerEntity(Player player) {
        this(player, true);
    }

    public final Player freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    private PlayerEntity(Player player, boolean z) {
        MostRecentGameInfoEntity mostRecentGameInfoEntity;
        this.zzbz = player.getPlayerId();
        this.zzn = player.getDisplayName();
        this.zzr = player.getIconImageUri();
        this.zzac = player.getIconImageUrl();
        this.zzs = player.getHiResImageUri();
        this.zzad = player.getHiResImageUrl();
        this.zzca = player.getRetrievedTimestamp();
        this.zzcb = player.zzj();
        this.zzcc = player.getLastPlayedWithTimestamp();
        this.zzcd = player.getTitle();
        this.zzcg = player.zzk();
        com.google.android.gms.games.internal.player.zza zzl = player.zzl();
        if (zzl == null) {
            mostRecentGameInfoEntity = null;
        } else {
            mostRecentGameInfoEntity = new MostRecentGameInfoEntity(zzl);
        }
        this.zzce = mostRecentGameInfoEntity;
        this.zzcf = player.getLevelInfo();
        this.zzch = player.zzi();
        this.zzci = player.zzh();
        this.name = player.getName();
        this.zzcj = player.getBannerImageLandscapeUri();
        this.zzck = player.getBannerImageLandscapeUrl();
        this.zzcl = player.getBannerImagePortraitUri();
        this.zzcm = player.getBannerImagePortraitUrl();
        this.zzcn = player.zzm();
        this.zzco = player.zzn();
        this.zzcp = player.isMuted();
        this.zzcq = player.zzo();
        Asserts.checkNotNull(this.zzbz);
        Asserts.checkNotNull(this.zzn);
        Asserts.checkState(this.zzca > 0);
    }

    @SafeParcelable.Constructor
    PlayerEntity(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) String str2, @Nullable @SafeParcelable.Param(id = 3) Uri uri, @Nullable @SafeParcelable.Param(id = 4) Uri uri2, @SafeParcelable.Param(id = 5) long j, @SafeParcelable.Param(id = 6) int i, @SafeParcelable.Param(id = 7) long j2, @Nullable @SafeParcelable.Param(id = 8) String str3, @Nullable @SafeParcelable.Param(id = 9) String str4, @Nullable @SafeParcelable.Param(id = 14) String str5, @Nullable @SafeParcelable.Param(id = 15) MostRecentGameInfoEntity mostRecentGameInfoEntity, @Nullable @SafeParcelable.Param(id = 16) PlayerLevelInfo playerLevelInfo, @SafeParcelable.Param(id = 18) boolean z, @SafeParcelable.Param(id = 19) boolean z2, @Nullable @SafeParcelable.Param(id = 20) String str6, @SafeParcelable.Param(id = 21) String str7, @Nullable @SafeParcelable.Param(id = 22) Uri uri3, @Nullable @SafeParcelable.Param(id = 23) String str8, @Nullable @SafeParcelable.Param(id = 24) Uri uri4, @Nullable @SafeParcelable.Param(id = 25) String str9, @SafeParcelable.Param(id = 26) int i2, @SafeParcelable.Param(id = 27) long j3, @SafeParcelable.Param(id = 28) boolean z3, @SafeParcelable.Param(id = 29) long j4) {
        this.zzbz = str;
        this.zzn = str2;
        this.zzr = uri;
        this.zzac = str3;
        this.zzs = uri2;
        this.zzad = str4;
        this.zzca = j;
        this.zzcb = i;
        this.zzcc = j2;
        this.zzcd = str5;
        this.zzcg = z;
        this.zzce = mostRecentGameInfoEntity;
        this.zzcf = playerLevelInfo;
        this.zzch = z2;
        this.zzci = str6;
        this.name = str7;
        this.zzcj = uri3;
        this.zzck = str8;
        this.zzcl = uri4;
        this.zzcm = str9;
        this.zzcn = i2;
        this.zzco = j3;
        this.zzcp = z3;
        this.zzcq = j4;
    }

    public final String getPlayerId() {
        return this.zzbz;
    }

    public final String getDisplayName() {
        return this.zzn;
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        DataUtils.copyStringToBuffer(this.zzn, charArrayBuffer);
    }

    @Nullable
    public final String zzh() {
        return this.zzci;
    }

    public final String getName() {
        return this.name;
    }

    public final boolean zzi() {
        return this.zzch;
    }

    public final boolean hasIconImage() {
        return getIconImageUri() != null;
    }

    @Nullable
    public final Uri getIconImageUri() {
        return this.zzr;
    }

    @Nullable
    public final String getIconImageUrl() {
        return this.zzac;
    }

    public final boolean hasHiResImage() {
        return getHiResImageUri() != null;
    }

    @Nullable
    public final Uri getHiResImageUri() {
        return this.zzs;
    }

    @Nullable
    public final String getHiResImageUrl() {
        return this.zzad;
    }

    public final long getRetrievedTimestamp() {
        return this.zzca;
    }

    public final long getLastPlayedWithTimestamp() {
        return this.zzcc;
    }

    public final int zzj() {
        return this.zzcb;
    }

    public final boolean zzk() {
        return this.zzcg;
    }

    @Nullable
    public final String getTitle() {
        return this.zzcd;
    }

    public final void getTitle(CharArrayBuffer charArrayBuffer) {
        DataUtils.copyStringToBuffer(this.zzcd, charArrayBuffer);
    }

    @Nullable
    public final PlayerLevelInfo getLevelInfo() {
        return this.zzcf;
    }

    @Nullable
    public final com.google.android.gms.games.internal.player.zza zzl() {
        return this.zzce;
    }

    @Nullable
    public final Uri getBannerImageLandscapeUri() {
        return this.zzcj;
    }

    @Nullable
    public final String getBannerImageLandscapeUrl() {
        return this.zzck;
    }

    @Nullable
    public final Uri getBannerImagePortraitUri() {
        return this.zzcl;
    }

    @Nullable
    public final String getBannerImagePortraitUrl() {
        return this.zzcm;
    }

    public final int zzm() {
        return this.zzcn;
    }

    public final long zzn() {
        return this.zzco;
    }

    public final boolean isMuted() {
        return this.zzcp;
    }

    public final long zzo() {
        return this.zzcq;
    }

    public final int hashCode() {
        return zza(this);
    }

    static int zza(Player player) {
        return Objects.hashCode(player.getPlayerId(), player.getDisplayName(), Boolean.valueOf(player.zzi()), player.getIconImageUri(), player.getHiResImageUri(), Long.valueOf(player.getRetrievedTimestamp()), player.getTitle(), player.getLevelInfo(), player.zzh(), player.getName(), player.getBannerImageLandscapeUri(), player.getBannerImagePortraitUri(), Integer.valueOf(player.zzm()), Long.valueOf(player.zzn()), Boolean.valueOf(player.isMuted()), Long.valueOf(player.zzo()));
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    static boolean zza(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return Objects.equal(player2.getPlayerId(), player.getPlayerId()) && Objects.equal(player2.getDisplayName(), player.getDisplayName()) && Objects.equal(Boolean.valueOf(player2.zzi()), Boolean.valueOf(player.zzi())) && Objects.equal(player2.getIconImageUri(), player.getIconImageUri()) && Objects.equal(player2.getHiResImageUri(), player.getHiResImageUri()) && Objects.equal(Long.valueOf(player2.getRetrievedTimestamp()), Long.valueOf(player.getRetrievedTimestamp())) && Objects.equal(player2.getTitle(), player.getTitle()) && Objects.equal(player2.getLevelInfo(), player.getLevelInfo()) && Objects.equal(player2.zzh(), player.zzh()) && Objects.equal(player2.getName(), player.getName()) && Objects.equal(player2.getBannerImageLandscapeUri(), player.getBannerImageLandscapeUri()) && Objects.equal(player2.getBannerImagePortraitUri(), player.getBannerImagePortraitUri()) && Objects.equal(Integer.valueOf(player2.zzm()), Integer.valueOf(player.zzm())) && Objects.equal(Long.valueOf(player2.zzn()), Long.valueOf(player.zzn())) && Objects.equal(Boolean.valueOf(player2.isMuted()), Boolean.valueOf(player.isMuted())) && Objects.equal(Long.valueOf(player2.zzo()), Long.valueOf(player.zzo()));
    }

    public final String toString() {
        return zzb(this);
    }

    static String zzb(Player player) {
        return Objects.toStringHelper(player).add("PlayerId", player.getPlayerId()).add("DisplayName", player.getDisplayName()).add("HasDebugAccess", Boolean.valueOf(player.zzi())).add("IconImageUri", player.getIconImageUri()).add("IconImageUrl", player.getIconImageUrl()).add("HiResImageUri", player.getHiResImageUri()).add("HiResImageUrl", player.getHiResImageUrl()).add("RetrievedTimestamp", Long.valueOf(player.getRetrievedTimestamp())).add("Title", player.getTitle()).add("LevelInfo", player.getLevelInfo()).add("GamerTag", player.zzh()).add("Name", player.getName()).add("BannerImageLandscapeUri", player.getBannerImageLandscapeUri()).add("BannerImageLandscapeUrl", player.getBannerImageLandscapeUrl()).add("BannerImagePortraitUri", player.getBannerImagePortraitUri()).add("BannerImagePortraitUrl", player.getBannerImagePortraitUrl()).add("GamerFriendStatus", Integer.valueOf(player.zzm())).add("GamerFriendUpdateTimestamp", Long.valueOf(player.zzn())).add("IsMuted", Boolean.valueOf(player.isMuted())).add("totalUnlockedAchievement", Long.valueOf(player.zzo())).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        if (!shouldDowngrade()) {
            int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeString(parcel, 1, getPlayerId(), false);
            SafeParcelWriter.writeString(parcel, 2, getDisplayName(), false);
            SafeParcelWriter.writeParcelable(parcel, 3, getIconImageUri(), i, false);
            SafeParcelWriter.writeParcelable(parcel, 4, getHiResImageUri(), i, false);
            SafeParcelWriter.writeLong(parcel, 5, getRetrievedTimestamp());
            SafeParcelWriter.writeInt(parcel, 6, this.zzcb);
            SafeParcelWriter.writeLong(parcel, 7, getLastPlayedWithTimestamp());
            SafeParcelWriter.writeString(parcel, 8, getIconImageUrl(), false);
            SafeParcelWriter.writeString(parcel, 9, getHiResImageUrl(), false);
            SafeParcelWriter.writeString(parcel, 14, getTitle(), false);
            SafeParcelWriter.writeParcelable(parcel, 15, this.zzce, i, false);
            SafeParcelWriter.writeParcelable(parcel, 16, getLevelInfo(), i, false);
            SafeParcelWriter.writeBoolean(parcel, 18, this.zzcg);
            SafeParcelWriter.writeBoolean(parcel, 19, this.zzch);
            SafeParcelWriter.writeString(parcel, 20, this.zzci, false);
            SafeParcelWriter.writeString(parcel, 21, this.name, false);
            SafeParcelWriter.writeParcelable(parcel, 22, getBannerImageLandscapeUri(), i, false);
            SafeParcelWriter.writeString(parcel, 23, getBannerImageLandscapeUrl(), false);
            SafeParcelWriter.writeParcelable(parcel, 24, getBannerImagePortraitUri(), i, false);
            SafeParcelWriter.writeString(parcel, 25, getBannerImagePortraitUrl(), false);
            SafeParcelWriter.writeInt(parcel, 26, this.zzcn);
            SafeParcelWriter.writeLong(parcel, 27, this.zzco);
            SafeParcelWriter.writeBoolean(parcel, 28, this.zzcp);
            SafeParcelWriter.writeLong(parcel, 29, this.zzcq);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
            return;
        }
        parcel.writeString(this.zzbz);
        parcel.writeString(this.zzn);
        String str = null;
        parcel.writeString(this.zzr == null ? null : this.zzr.toString());
        if (this.zzs != null) {
            str = this.zzs.toString();
        }
        parcel.writeString(str);
        parcel.writeLong(this.zzca);
    }
}
