package com.google.android.gms.games;

import com.google.android.gms.games.Games;
import java.util.Collections;
import java.util.List;

final class zzj extends Games.zzb {
    zzj() {
        super(null);
    }

    public final /* synthetic */ List getImpliedScopes(Object obj) {
        return Collections.singletonList(Games.zzam);
    }
}
