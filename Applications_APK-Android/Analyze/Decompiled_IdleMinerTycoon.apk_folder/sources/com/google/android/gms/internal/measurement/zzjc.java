package com.google.android.gms.internal.measurement;

public final class zzjc implements zziz {
    private static final zzcm<Boolean> zzapl = new zzct(zzcn.zzdh("com.google.android.gms.measurement")).zzb("measurement.sdk.collection.validate_param_names_alphabetical", false);

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: com.google.android.gms.internal.measurement.zzcm.get():java.lang.Object in method: com.google.android.gms.internal.measurement.zzjc.zzxg():boolean, dex: classes2.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: com.google.android.gms.internal.measurement.zzcm.get():java.lang.Object
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public final boolean zzxg() {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.zzcm<java.lang.Boolean> r0 = com.google.android.gms.internal.measurement.zzjc.zzapl
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzjc.zzxg():boolean");
    }
}
