package com.google.android.gms.internal.nearby;

public final class zzfs {
    private final zzfq zzed = new zzfq();

    public final zzfs zza(zzdj zzdj) {
        zzdj unused = this.zzed.zzec = zzdj;
        return this;
    }

    public final zzfs zza(zzdm zzdm) {
        zzdm unused = this.zzed.zzeb = zzdm;
        return this;
    }

    public final zzfs zzb(zzdg zzdg) {
        zzdg unused = this.zzed.zzas = zzdg;
        return this;
    }

    public final zzfs zzc(byte[] bArr) {
        byte[] unused = this.zzed.zzau = bArr;
        return this;
    }

    public final zzfs zzd(zzdz zzdz) {
        zzdz unused = this.zzed.zzar = zzdz;
        return this;
    }

    public final zzfs zzg(String str) {
        String unused = this.zzed.name = str;
        return this;
    }

    public final zzfs zzh(String str) {
        String unused = this.zzed.zzat = str;
        return this;
    }

    public final zzfq zzt() {
        return this.zzed;
    }
}
