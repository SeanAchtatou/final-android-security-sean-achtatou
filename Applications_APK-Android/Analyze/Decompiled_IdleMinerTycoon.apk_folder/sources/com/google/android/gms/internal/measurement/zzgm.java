package com.google.android.gms.internal.measurement;

import com.google.android.gms.drive.DriveFile;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

final class zzgm<T> implements zzgx<T> {
    private static final int[] zzakh = new int[0];
    private static final Unsafe zzaki = zzhv.zzwv();
    private final int[] zzakj;
    private final Object[] zzakk;
    private final int zzakl;
    private final int zzakm;
    private final zzgi zzakn;
    private final boolean zzako;
    private final boolean zzakp;
    private final boolean zzakq;
    private final boolean zzakr;
    private final int[] zzaks;
    private final int zzakt;
    private final int zzaku;
    private final zzgq zzakv;
    private final zzfs zzakw;
    private final zzhp<?, ?> zzakx;
    private final zzen<?> zzaky;
    private final zzgb zzakz;

    private zzgm(int[] iArr, Object[] objArr, int i, int i2, zzgi zzgi, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzgq zzgq, zzfs zzfs, zzhp<?, ?> zzhp, zzen<?> zzen, zzgb zzgb) {
        this.zzakj = iArr;
        this.zzakk = objArr;
        this.zzakl = i;
        this.zzakm = i2;
        this.zzakp = zzgi instanceof zzey;
        this.zzakq = z;
        this.zzako = zzen != null && zzen.zze(zzgi);
        this.zzakr = false;
        this.zzaks = iArr2;
        this.zzakt = i3;
        this.zzaku = i4;
        this.zzakv = zzgq;
        this.zzakw = zzfs;
        this.zzakx = zzhp;
        this.zzaky = zzen;
        this.zzakn = zzgi;
        this.zzakz = zzgb;
    }

    private static boolean zzcc(int i) {
        return (i & DriveFile.MODE_WRITE_ONLY) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:170:0x037f  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x03ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static <T> com.google.android.gms.internal.measurement.zzgm<T> zza(java.lang.Class<T> r37, com.google.android.gms.internal.measurement.zzgg r38, com.google.android.gms.internal.measurement.zzgq r39, com.google.android.gms.internal.measurement.zzfs r40, com.google.android.gms.internal.measurement.zzhp<?, ?> r41, com.google.android.gms.internal.measurement.zzen<?> r42, com.google.android.gms.internal.measurement.zzgb r43) {
        /*
            r0 = r38
            boolean r1 = r0 instanceof com.google.android.gms.internal.measurement.zzgv
            if (r1 == 0) goto L_0x0441
            com.google.android.gms.internal.measurement.zzgv r0 = (com.google.android.gms.internal.measurement.zzgv) r0
            int r1 = r0.zzvr()
            int r2 = com.google.android.gms.internal.measurement.zzey.zzd.zzaim
            r3 = 0
            r4 = 1
            if (r1 != r2) goto L_0x0014
            r11 = 1
            goto L_0x0015
        L_0x0014:
            r11 = 0
        L_0x0015:
            java.lang.String r1 = r0.zzvz()
            int r2 = r1.length()
            char r5 = r1.charAt(r3)
            r7 = 55296(0xd800, float:7.7486E-41)
            if (r5 < r7) goto L_0x003f
            r5 = r5 & 8191(0x1fff, float:1.1478E-41)
            r8 = r5
            r5 = 1
            r9 = 13
        L_0x002c:
            int r10 = r5 + 1
            char r5 = r1.charAt(r5)
            if (r5 < r7) goto L_0x003c
            r5 = r5 & 8191(0x1fff, float:1.1478E-41)
            int r5 = r5 << r9
            r8 = r8 | r5
            int r9 = r9 + 13
            r5 = r10
            goto L_0x002c
        L_0x003c:
            int r5 = r5 << r9
            r5 = r5 | r8
            goto L_0x0040
        L_0x003f:
            r10 = 1
        L_0x0040:
            int r8 = r10 + 1
            char r9 = r1.charAt(r10)
            if (r9 < r7) goto L_0x005f
            r9 = r9 & 8191(0x1fff, float:1.1478E-41)
            r10 = 13
        L_0x004c:
            int r12 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x005c
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r8 = r8 << r10
            r9 = r9 | r8
            int r10 = r10 + 13
            r8 = r12
            goto L_0x004c
        L_0x005c:
            int r8 = r8 << r10
            r9 = r9 | r8
            goto L_0x0060
        L_0x005f:
            r12 = r8
        L_0x0060:
            if (r9 != 0) goto L_0x006e
            int[] r8 = com.google.android.gms.internal.measurement.zzgm.zzakh
            r16 = r8
            r8 = 0
            r9 = 0
            r10 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            goto L_0x01a1
        L_0x006e:
            int r8 = r12 + 1
            char r9 = r1.charAt(r12)
            if (r9 < r7) goto L_0x008e
            r9 = r9 & 8191(0x1fff, float:1.1478E-41)
            r10 = 13
        L_0x007a:
            int r12 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x008a
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r8 = r8 << r10
            r9 = r9 | r8
            int r10 = r10 + 13
            r8 = r12
            goto L_0x007a
        L_0x008a:
            int r8 = r8 << r10
            r8 = r8 | r9
            r9 = r8
            goto L_0x008f
        L_0x008e:
            r12 = r8
        L_0x008f:
            int r8 = r12 + 1
            char r10 = r1.charAt(r12)
            if (r10 < r7) goto L_0x00ae
            r10 = r10 & 8191(0x1fff, float:1.1478E-41)
            r12 = 13
        L_0x009b:
            int r13 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x00ab
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r8 = r8 << r12
            r10 = r10 | r8
            int r12 = r12 + 13
            r8 = r13
            goto L_0x009b
        L_0x00ab:
            int r8 = r8 << r12
            r10 = r10 | r8
            goto L_0x00af
        L_0x00ae:
            r13 = r8
        L_0x00af:
            int r8 = r13 + 1
            char r12 = r1.charAt(r13)
            if (r12 < r7) goto L_0x00cf
            r12 = r12 & 8191(0x1fff, float:1.1478E-41)
            r13 = 13
        L_0x00bb:
            int r14 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x00cb
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r8 = r8 << r13
            r12 = r12 | r8
            int r13 = r13 + 13
            r8 = r14
            goto L_0x00bb
        L_0x00cb:
            int r8 = r8 << r13
            r8 = r8 | r12
            r12 = r8
            goto L_0x00d0
        L_0x00cf:
            r14 = r8
        L_0x00d0:
            int r8 = r14 + 1
            char r13 = r1.charAt(r14)
            if (r13 < r7) goto L_0x00f0
            r13 = r13 & 8191(0x1fff, float:1.1478E-41)
            r14 = 13
        L_0x00dc:
            int r15 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x00ec
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r8 = r8 << r14
            r13 = r13 | r8
            int r14 = r14 + 13
            r8 = r15
            goto L_0x00dc
        L_0x00ec:
            int r8 = r8 << r14
            r8 = r8 | r13
            r13 = r8
            goto L_0x00f1
        L_0x00f0:
            r15 = r8
        L_0x00f1:
            int r8 = r15 + 1
            char r14 = r1.charAt(r15)
            if (r14 < r7) goto L_0x0113
            r14 = r14 & 8191(0x1fff, float:1.1478E-41)
            r15 = 13
        L_0x00fd:
            int r16 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x010e
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r8 = r8 << r15
            r14 = r14 | r8
            int r15 = r15 + 13
            r8 = r16
            goto L_0x00fd
        L_0x010e:
            int r8 = r8 << r15
            r8 = r8 | r14
            r14 = r8
            r8 = r16
        L_0x0113:
            int r15 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r7) goto L_0x0136
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            r16 = 13
        L_0x011f:
            int r17 = r15 + 1
            char r15 = r1.charAt(r15)
            if (r15 < r7) goto L_0x0131
            r15 = r15 & 8191(0x1fff, float:1.1478E-41)
            int r15 = r15 << r16
            r8 = r8 | r15
            int r16 = r16 + 13
            r15 = r17
            goto L_0x011f
        L_0x0131:
            int r15 = r15 << r16
            r8 = r8 | r15
            r15 = r17
        L_0x0136:
            int r16 = r15 + 1
            char r15 = r1.charAt(r15)
            if (r15 < r7) goto L_0x0162
            r15 = r15 & 8191(0x1fff, float:1.1478E-41)
            r17 = 13
            r36 = r16
            r16 = r15
            r15 = r36
        L_0x0148:
            int r18 = r15 + 1
            char r15 = r1.charAt(r15)
            if (r15 < r7) goto L_0x015b
            r15 = r15 & 8191(0x1fff, float:1.1478E-41)
            int r15 = r15 << r17
            r16 = r16 | r15
            int r17 = r17 + 13
            r15 = r18
            goto L_0x0148
        L_0x015b:
            int r15 = r15 << r17
            r15 = r16 | r15
            r3 = r18
            goto L_0x0164
        L_0x0162:
            r3 = r16
        L_0x0164:
            int r16 = r3 + 1
            char r3 = r1.charAt(r3)
            if (r3 < r7) goto L_0x018f
            r3 = r3 & 8191(0x1fff, float:1.1478E-41)
            r17 = 13
            r36 = r16
            r16 = r3
            r3 = r36
        L_0x0176:
            int r18 = r3 + 1
            char r3 = r1.charAt(r3)
            if (r3 < r7) goto L_0x0189
            r3 = r3 & 8191(0x1fff, float:1.1478E-41)
            int r3 = r3 << r17
            r16 = r16 | r3
            int r17 = r17 + 13
            r3 = r18
            goto L_0x0176
        L_0x0189:
            int r3 = r3 << r17
            r3 = r16 | r3
            r16 = r18
        L_0x018f:
            int r17 = r3 + r8
            int r15 = r17 + r15
            int[] r15 = new int[r15]
            int r17 = r9 << 1
            int r10 = r17 + r10
            r36 = r15
            r15 = r10
            r10 = r12
            r12 = r16
            r16 = r36
        L_0x01a1:
            sun.misc.Unsafe r6 = com.google.android.gms.internal.measurement.zzgm.zzaki
            java.lang.Object[] r17 = r0.zzwa()
            com.google.android.gms.internal.measurement.zzgi r18 = r0.zzvt()
            java.lang.Class r7 = r18.getClass()
            r22 = r12
            int r12 = r14 * 3
            int[] r12 = new int[r12]
            int r14 = r14 << r4
            java.lang.Object[] r14 = new java.lang.Object[r14]
            int r18 = r3 + r8
            r20 = r15
            r23 = r18
            r8 = r22
            r15 = 0
            r19 = 0
            r22 = r3
        L_0x01c5:
            if (r8 >= r2) goto L_0x0413
            int r24 = r8 + 1
            char r8 = r1.charAt(r8)
            r4 = 55296(0xd800, float:7.7486E-41)
            if (r8 < r4) goto L_0x01f9
            r8 = r8 & 8191(0x1fff, float:1.1478E-41)
            r25 = 13
            r36 = r24
            r24 = r8
            r8 = r36
        L_0x01dc:
            int r26 = r8 + 1
            char r8 = r1.charAt(r8)
            if (r8 < r4) goto L_0x01f2
            r4 = r8 & 8191(0x1fff, float:1.1478E-41)
            int r4 = r4 << r25
            r24 = r24 | r4
            int r25 = r25 + 13
            r8 = r26
            r4 = 55296(0xd800, float:7.7486E-41)
            goto L_0x01dc
        L_0x01f2:
            int r4 = r8 << r25
            r8 = r24 | r4
            r4 = r26
            goto L_0x01fb
        L_0x01f9:
            r4 = r24
        L_0x01fb:
            int r24 = r4 + 1
            char r4 = r1.charAt(r4)
            r27 = r2
            r2 = 55296(0xd800, float:7.7486E-41)
            if (r4 < r2) goto L_0x022f
            r4 = r4 & 8191(0x1fff, float:1.1478E-41)
            r25 = 13
            r36 = r24
            r24 = r4
            r4 = r36
        L_0x0212:
            int r26 = r4 + 1
            char r4 = r1.charAt(r4)
            if (r4 < r2) goto L_0x0228
            r2 = r4 & 8191(0x1fff, float:1.1478E-41)
            int r2 = r2 << r25
            r24 = r24 | r2
            int r25 = r25 + 13
            r4 = r26
            r2 = 55296(0xd800, float:7.7486E-41)
            goto L_0x0212
        L_0x0228:
            int r2 = r4 << r25
            r4 = r24 | r2
            r2 = r26
            goto L_0x0231
        L_0x022f:
            r2 = r24
        L_0x0231:
            r28 = r3
            r3 = r4 & 255(0xff, float:3.57E-43)
            r29 = r11
            r11 = r4 & 1024(0x400, float:1.435E-42)
            if (r11 == 0) goto L_0x0240
            int r11 = r15 + 1
            r16[r15] = r19
            r15 = r11
        L_0x0240:
            r11 = 51
            r30 = r15
            if (r3 < r11) goto L_0x02e4
            int r11 = r2 + 1
            char r2 = r1.charAt(r2)
            r15 = 55296(0xd800, float:7.7486E-41)
            if (r2 < r15) goto L_0x026f
            r2 = r2 & 8191(0x1fff, float:1.1478E-41)
            r24 = 13
        L_0x0255:
            int r25 = r11 + 1
            char r11 = r1.charAt(r11)
            if (r11 < r15) goto L_0x026a
            r11 = r11 & 8191(0x1fff, float:1.1478E-41)
            int r11 = r11 << r24
            r2 = r2 | r11
            int r24 = r24 + 13
            r11 = r25
            r15 = 55296(0xd800, float:7.7486E-41)
            goto L_0x0255
        L_0x026a:
            int r11 = r11 << r24
            r2 = r2 | r11
            r11 = r25
        L_0x026f:
            int r15 = r3 + -51
            r31 = r11
            r11 = 9
            if (r15 == r11) goto L_0x0296
            r11 = 17
            if (r15 != r11) goto L_0x027c
            goto L_0x0296
        L_0x027c:
            r11 = 12
            if (r15 != r11) goto L_0x0292
            r11 = r5 & 1
            r15 = 1
            if (r11 != r15) goto L_0x0292
            int r11 = r19 / 3
            int r11 = r11 << r15
            int r11 = r11 + r15
            int r15 = r20 + 1
            r20 = r17[r20]
            r14[r11] = r20
            r24 = r15
            goto L_0x0294
        L_0x0292:
            r24 = r20
        L_0x0294:
            r15 = 1
            goto L_0x02a1
        L_0x0296:
            int r11 = r19 / 3
            r15 = 1
            int r11 = r11 << r15
            int r11 = r11 + r15
            int r24 = r20 + 1
            r20 = r17[r20]
            r14[r11] = r20
        L_0x02a1:
            int r2 = r2 << r15
            r11 = r17[r2]
            boolean r15 = r11 instanceof java.lang.reflect.Field
            if (r15 == 0) goto L_0x02ad
            java.lang.reflect.Field r11 = (java.lang.reflect.Field) r11
        L_0x02aa:
            r32 = r10
            goto L_0x02b6
        L_0x02ad:
            java.lang.String r11 = (java.lang.String) r11
            java.lang.reflect.Field r11 = zza(r7, r11)
            r17[r2] = r11
            goto L_0x02aa
        L_0x02b6:
            long r10 = r6.objectFieldOffset(r11)
            int r10 = (int) r10
            int r2 = r2 + 1
            r11 = r17[r2]
            boolean r15 = r11 instanceof java.lang.reflect.Field
            if (r15 == 0) goto L_0x02c8
            java.lang.reflect.Field r11 = (java.lang.reflect.Field) r11
        L_0x02c5:
            r33 = r10
            goto L_0x02d1
        L_0x02c8:
            java.lang.String r11 = (java.lang.String) r11
            java.lang.reflect.Field r11 = zza(r7, r11)
            r17[r2] = r11
            goto L_0x02c5
        L_0x02d1:
            long r10 = r6.objectFieldOffset(r11)
            int r2 = (int) r10
            r34 = r13
            r35 = r14
            r20 = r24
            r13 = r31
            r11 = r33
            r14 = r2
            r2 = 0
            goto L_0x03dd
        L_0x02e4:
            r32 = r10
            int r10 = r20 + 1
            r11 = r17[r20]
            java.lang.String r11 = (java.lang.String) r11
            java.lang.reflect.Field r11 = zza(r7, r11)
            r15 = 9
            if (r3 == r15) goto L_0x0366
            r15 = 17
            if (r3 != r15) goto L_0x02fa
            goto L_0x0366
        L_0x02fa:
            r15 = 27
            if (r3 == r15) goto L_0x0354
            r15 = 49
            if (r3 != r15) goto L_0x0303
            goto L_0x0354
        L_0x0303:
            r15 = 12
            if (r3 == r15) goto L_0x0342
            r15 = 30
            if (r3 == r15) goto L_0x0342
            r15 = 44
            if (r3 != r15) goto L_0x0310
            goto L_0x0342
        L_0x0310:
            r15 = 50
            if (r3 != r15) goto L_0x033e
            int r15 = r22 + 1
            r16[r22] = r19
            int r20 = r19 / 3
            r22 = 1
            int r20 = r20 << 1
            int r22 = r10 + 1
            r10 = r17[r10]
            r14[r20] = r10
            r10 = r4 & 2048(0x800, float:2.87E-42)
            if (r10 == 0) goto L_0x0335
            int r20 = r20 + 1
            int r10 = r22 + 1
            r22 = r17[r22]
            r14[r20] = r22
            r34 = r13
            r35 = r14
            goto L_0x033b
        L_0x0335:
            r34 = r13
            r35 = r14
            r10 = r22
        L_0x033b:
            r22 = r15
            goto L_0x0375
        L_0x033e:
            r34 = r13
            r13 = 1
            goto L_0x0373
        L_0x0342:
            r15 = r5 & 1
            r34 = r13
            r13 = 1
            if (r15 != r13) goto L_0x0373
            int r15 = r19 / 3
            int r15 = r15 << r13
            int r15 = r15 + r13
            int r20 = r10 + 1
            r10 = r17[r10]
            r14[r15] = r10
            goto L_0x0361
        L_0x0354:
            r34 = r13
            r13 = 1
            int r15 = r19 / 3
            int r15 = r15 << r13
            int r15 = r15 + r13
            int r20 = r10 + 1
            r10 = r17[r10]
            r14[r15] = r10
        L_0x0361:
            r35 = r14
            r10 = r20
            goto L_0x0375
        L_0x0366:
            r34 = r13
            r13 = 1
            int r15 = r19 / 3
            int r15 = r15 << r13
            int r15 = r15 + r13
            java.lang.Class r20 = r11.getType()
            r14[r15] = r20
        L_0x0373:
            r35 = r14
        L_0x0375:
            long r13 = r6.objectFieldOffset(r11)
            int r11 = (int) r13
            r13 = r5 & 1
            r14 = 1
            if (r13 != r14) goto L_0x03c7
            r13 = 17
            if (r3 > r13) goto L_0x03c7
            int r13 = r2 + 1
            char r2 = r1.charAt(r2)
            r14 = 55296(0xd800, float:7.7486E-41)
            if (r2 < r14) goto L_0x03a7
            r2 = r2 & 8191(0x1fff, float:1.1478E-41)
            r15 = 13
        L_0x0392:
            int r20 = r13 + 1
            char r13 = r1.charAt(r13)
            if (r13 < r14) goto L_0x03a3
            r13 = r13 & 8191(0x1fff, float:1.1478E-41)
            int r13 = r13 << r15
            r2 = r2 | r13
            int r15 = r15 + 13
            r13 = r20
            goto L_0x0392
        L_0x03a3:
            int r13 = r13 << r15
            r2 = r2 | r13
            r13 = r20
        L_0x03a7:
            r15 = 1
            int r20 = r9 << 1
            int r21 = r2 / 32
            int r20 = r20 + r21
            r14 = r17[r20]
            boolean r15 = r14 instanceof java.lang.reflect.Field
            if (r15 == 0) goto L_0x03b7
            java.lang.reflect.Field r14 = (java.lang.reflect.Field) r14
            goto L_0x03bf
        L_0x03b7:
            java.lang.String r14 = (java.lang.String) r14
            java.lang.reflect.Field r14 = zza(r7, r14)
            r17[r20] = r14
        L_0x03bf:
            long r14 = r6.objectFieldOffset(r14)
            int r14 = (int) r14
            int r2 = r2 % 32
            goto L_0x03ca
        L_0x03c7:
            r13 = r2
            r2 = 0
            r14 = 0
        L_0x03ca:
            r15 = 18
            if (r3 < r15) goto L_0x03db
            r15 = 49
            if (r3 > r15) goto L_0x03db
            int r15 = r23 + 1
            r16[r23] = r11
            r20 = r10
            r23 = r15
            goto L_0x03dd
        L_0x03db:
            r20 = r10
        L_0x03dd:
            int r10 = r19 + 1
            r12[r19] = r8
            int r8 = r10 + 1
            r15 = r4 & 512(0x200, float:7.175E-43)
            if (r15 == 0) goto L_0x03ea
            r15 = 536870912(0x20000000, float:1.0842022E-19)
            goto L_0x03eb
        L_0x03ea:
            r15 = 0
        L_0x03eb:
            r4 = r4 & 256(0x100, float:3.59E-43)
            if (r4 == 0) goto L_0x03f2
            r4 = 268435456(0x10000000, float:2.5243549E-29)
            goto L_0x03f3
        L_0x03f2:
            r4 = 0
        L_0x03f3:
            r4 = r4 | r15
            int r3 = r3 << 20
            r3 = r3 | r4
            r3 = r3 | r11
            r12[r10] = r3
            int r19 = r8 + 1
            int r2 = r2 << 20
            r2 = r2 | r14
            r12[r8] = r2
            r8 = r13
            r2 = r27
            r3 = r28
            r11 = r29
            r15 = r30
            r10 = r32
            r13 = r34
            r14 = r35
            r4 = 1
            goto L_0x01c5
        L_0x0413:
            r28 = r3
            r32 = r10
            r29 = r11
            r34 = r13
            r35 = r14
            com.google.android.gms.internal.measurement.zzgm r1 = new com.google.android.gms.internal.measurement.zzgm
            com.google.android.gms.internal.measurement.zzgi r10 = r0.zzvt()
            r0 = 0
            r5 = r1
            r6 = r12
            r7 = r35
            r8 = r32
            r9 = r34
            r12 = r0
            r13 = r16
            r14 = r28
            r15 = r18
            r16 = r39
            r17 = r40
            r18 = r41
            r19 = r42
            r20 = r43
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            return r1
        L_0x0441:
            com.google.android.gms.internal.measurement.zzhm r0 = (com.google.android.gms.internal.measurement.zzhm) r0
            int r0 = r0.zzvr()
            int r1 = com.google.android.gms.internal.measurement.zzey.zzd.zzaim
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Class, com.google.android.gms.internal.measurement.zzgg, com.google.android.gms.internal.measurement.zzgq, com.google.android.gms.internal.measurement.zzfs, com.google.android.gms.internal.measurement.zzhp, com.google.android.gms.internal.measurement.zzen, com.google.android.gms.internal.measurement.zzgb):com.google.android.gms.internal.measurement.zzgm");
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T newInstance() {
        return this.zzakv.newInstance(this.zzakn);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.measurement.zzgz.zzd(com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6), com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.measurement.zzgz.zzd(com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6), com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.measurement.zzgz.zzd(com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6), com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.measurement.zzgz.zzd(com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6), com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzm(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzm(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6) == com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.zzhv.zzn(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.zzhv.zzn(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.zzhv.zzo(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.zzhv.zzo(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.measurement.zzgz.zzd(com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6), com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.zzakj
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.zzca(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.zzcb(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r4)
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzgz.zzd(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.zzgz.zzd(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.zzgz.zzd(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzgz.zzd(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzgz.zzd(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzgz.zzd(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzhv.zzp(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzgz.zzd(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.measurement.zzhv.zzm(r10, r6)
            boolean r5 = com.google.android.gms.internal.measurement.zzhv.zzm(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzhv.zzk(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzhv.zzk(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzhv.zzl(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzhv.zzl(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.measurement.zzhv.zzn(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.measurement.zzhv.zzn(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.measurement.zzhv.zzo(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.measurement.zzhv.zzo(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.measurement.zzhp<?, ?> r0 = r9.zzakx
            java.lang.Object r0 = r0.zzx(r10)
            com.google.android.gms.internal.measurement.zzhp<?, ?> r2 = r9.zzakx
            java.lang.Object r2 = r2.zzx(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.zzako
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.measurement.zzen<?> r0 = r9.zzaky
            com.google.android.gms.internal.measurement.zzeo r10 = r0.zzh(r10)
            com.google.android.gms.internal.measurement.zzen<?> r0 = r9.zzaky
            com.google.android.gms.internal.measurement.zzeo r11 = r0.zzh(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.equals(java.lang.Object, java.lang.Object):boolean");
    }

    public final int hashCode(T t) {
        int length = this.zzakj.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2 += 3) {
            int zzca = zzca(i2);
            int i3 = this.zzakj[i2];
            long j = (long) (1048575 & zzca);
            int i4 = 37;
            switch ((zzca & 267386880) >>> 20) {
                case 0:
                    i = (i * 53) + zzez.zzbx(Double.doubleToLongBits(zzhv.zzo(t, j)));
                    break;
                case 1:
                    i = (i * 53) + Float.floatToIntBits(zzhv.zzn(t, j));
                    break;
                case 2:
                    i = (i * 53) + zzez.zzbx(zzhv.zzl(t, j));
                    break;
                case 3:
                    i = (i * 53) + zzez.zzbx(zzhv.zzl(t, j));
                    break;
                case 4:
                    i = (i * 53) + zzhv.zzk(t, j);
                    break;
                case 5:
                    i = (i * 53) + zzez.zzbx(zzhv.zzl(t, j));
                    break;
                case 6:
                    i = (i * 53) + zzhv.zzk(t, j);
                    break;
                case 7:
                    i = (i * 53) + zzez.zzs(zzhv.zzm(t, j));
                    break;
                case 8:
                    i = (i * 53) + ((String) zzhv.zzp(t, j)).hashCode();
                    break;
                case 9:
                    Object zzp = zzhv.zzp(t, j);
                    if (zzp != null) {
                        i4 = zzp.hashCode();
                    }
                    i = (i * 53) + i4;
                    break;
                case 10:
                    i = (i * 53) + zzhv.zzp(t, j).hashCode();
                    break;
                case 11:
                    i = (i * 53) + zzhv.zzk(t, j);
                    break;
                case 12:
                    i = (i * 53) + zzhv.zzk(t, j);
                    break;
                case 13:
                    i = (i * 53) + zzhv.zzk(t, j);
                    break;
                case 14:
                    i = (i * 53) + zzez.zzbx(zzhv.zzl(t, j));
                    break;
                case 15:
                    i = (i * 53) + zzhv.zzk(t, j);
                    break;
                case 16:
                    i = (i * 53) + zzez.zzbx(zzhv.zzl(t, j));
                    break;
                case 17:
                    Object zzp2 = zzhv.zzp(t, j);
                    if (zzp2 != null) {
                        i4 = zzp2.hashCode();
                    }
                    i = (i * 53) + i4;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = (i * 53) + zzhv.zzp(t, j).hashCode();
                    break;
                case 50:
                    i = (i * 53) + zzhv.zzp(t, j).hashCode();
                    break;
                case 51:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzbx(Double.doubleToLongBits(zzf(t, j)));
                        break;
                    }
                case 52:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Float.floatToIntBits(zzg(t, j));
                        break;
                    }
                case 53:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzbx(zzi(t, j));
                        break;
                    }
                case 54:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzbx(zzi(t, j));
                        break;
                    }
                case 55:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 56:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzbx(zzi(t, j));
                        break;
                    }
                case 57:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 58:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzs(zzj(t, j));
                        break;
                    }
                case 59:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + ((String) zzhv.zzp(t, j)).hashCode();
                        break;
                    }
                case 60:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzhv.zzp(t, j).hashCode();
                        break;
                    }
                case 61:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzhv.zzp(t, j).hashCode();
                        break;
                    }
                case 62:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 63:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 64:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 65:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzbx(zzi(t, j));
                        break;
                    }
                case 66:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 67:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzez.zzbx(zzi(t, j));
                        break;
                    }
                case 68:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzhv.zzp(t, j).hashCode();
                        break;
                    }
            }
        }
        int hashCode = (i * 53) + this.zzakx.zzx(t).hashCode();
        return this.zzako ? (hashCode * 53) + this.zzaky.zzh(t).hashCode() : hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzhv.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhv.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzhv.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void */
    public final void zzc(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzakj.length; i += 3) {
                int zzca = zzca(i);
                long j = (long) (1048575 & zzca);
                int i2 = this.zzakj[i];
                switch ((zzca & 267386880) >>> 20) {
                    case 0:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza(t, j, zzhv.zzo(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 1:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzn(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 2:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzl(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 3:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzl(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 4:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zzb(t, j, zzhv.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 5:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzl(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 6:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zzb(t, j, zzhv.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 7:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzm(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 8:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza(t, j, zzhv.zzp(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 9:
                        zza(t, t2, i);
                        break;
                    case 10:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza(t, j, zzhv.zzp(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 11:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zzb(t, j, zzhv.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 12:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zzb(t, j, zzhv.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 13:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zzb(t, j, zzhv.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 14:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzl(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 15:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zzb(t, j, zzhv.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 16:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhv.zza((Object) t, j, zzhv.zzl(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 17:
                        zza(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.zzakw.zza(t, t2, j);
                        break;
                    case 50:
                        zzgz.zza(this.zzakz, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzhv.zza(t, j, zzhv.zzp(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 60:
                        zzb(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzhv.zza(t, j, zzhv.zzp(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 68:
                        zzb(t, t2, i);
                        break;
                }
            }
            if (!this.zzakq) {
                zzgz.zza(this.zzakx, t, t2);
                if (this.zzako) {
                    zzgz.zza(this.zzaky, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    private final void zza(T t, T t2, int i) {
        long zzca = (long) (zzca(i) & 1048575);
        if (zza(t2, i)) {
            Object zzp = zzhv.zzp(t, zzca);
            Object zzp2 = zzhv.zzp(t2, zzca);
            if (zzp != null && zzp2 != null) {
                zzhv.zza(t, zzca, zzez.zza(zzp, zzp2));
                zzb(t, i);
            } else if (zzp2 != null) {
                zzhv.zza(t, zzca, zzp2);
                zzb(t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzca = zzca(i);
        int i2 = this.zzakj[i];
        long j = (long) (zzca & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzhv.zzp(t, j);
            Object zzp2 = zzhv.zzp(t2, j);
            if (zzp != null && zzp2 != null) {
                zzhv.zza(t, j, zzez.zza(zzp, zzp2));
                zzb(t, i2, i);
            } else if (zzp2 != null) {
                zzhv.zza(t, j, zzp2);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzee.zzh(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzee.zzh(int, int):int
      com.google.android.gms.internal.measurement.zzee.zzh(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzee.zzc(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzee.zzc(int, com.google.android.gms.internal.measurement.zzdp):int
      com.google.android.gms.internal.measurement.zzee.zzc(int, com.google.android.gms.internal.measurement.zzgi):int
      com.google.android.gms.internal.measurement.zzee.zzc(int, java.lang.String):int
      com.google.android.gms.internal.measurement.zzee.zzc(int, int):void
      com.google.android.gms.internal.measurement.zzee.zzc(int, long):void
      com.google.android.gms.internal.measurement.zzee.zzc(int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzee.zzg(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzee.zzg(int, int):int
      com.google.android.gms.internal.measurement.zzee.zzg(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzee.zzb(int, float):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzee.zzb(int, double):int
      com.google.android.gms.internal.measurement.zzee.zzb(int, com.google.android.gms.internal.measurement.zzfn):int
      com.google.android.gms.internal.measurement.zzee.zzb(com.google.android.gms.internal.measurement.zzgi, com.google.android.gms.internal.measurement.zzgx):int
      com.google.android.gms.internal.measurement.zzee.zzb(int, int):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, long):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, com.google.android.gms.internal.measurement.zzdp):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, com.google.android.gms.internal.measurement.zzgi):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, java.lang.String):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, boolean):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, float):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzee.zzb(int, double):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzee.zzb(int, float):int
      com.google.android.gms.internal.measurement.zzee.zzb(int, com.google.android.gms.internal.measurement.zzfn):int
      com.google.android.gms.internal.measurement.zzee.zzb(com.google.android.gms.internal.measurement.zzgi, com.google.android.gms.internal.measurement.zzgx):int
      com.google.android.gms.internal.measurement.zzee.zzb(int, int):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, long):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, com.google.android.gms.internal.measurement.zzdp):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, com.google.android.gms.internal.measurement.zzgi):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, java.lang.String):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, boolean):void
      com.google.android.gms.internal.measurement.zzee.zzb(int, double):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgz.zzd(int, java.util.List<com.google.android.gms.internal.measurement.zzdp>):int
     arg types: [int, java.util.List<?>]
     candidates:
      com.google.android.gms.internal.measurement.zzgz.zzd(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgz.zzd(int, java.util.List<com.google.android.gms.internal.measurement.zzdp>):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgm.zza(com.google.android.gms.internal.measurement.zzhp, java.lang.Object):int
     arg types: [com.google.android.gms.internal.measurement.zzhp<?, ?>, T]
     candidates:
      com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, int):boolean
      com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzim):void
      com.google.android.gms.internal.measurement.zzgx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzim):void
      com.google.android.gms.internal.measurement.zzgm.zza(com.google.android.gms.internal.measurement.zzhp, java.lang.Object):int */
    public final int zzt(T t) {
        int i;
        int i2;
        long j;
        T t2 = t;
        int i3 = 267386880;
        int i4 = 1048575;
        int i5 = 1;
        if (this.zzakq) {
            Unsafe unsafe = zzaki;
            int i6 = 0;
            int i7 = 0;
            while (i6 < this.zzakj.length) {
                int zzca = zzca(i6);
                int i8 = (zzca & i3) >>> 20;
                int i9 = this.zzakj[i6];
                long j2 = (long) (zzca & 1048575);
                int i10 = (i8 < zzet.DOUBLE_LIST_PACKED.id() || i8 > zzet.SINT64_LIST_PACKED.id()) ? 0 : this.zzakj[i6 + 2] & 1048575;
                switch (i8) {
                    case 0:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzb(i9, 0.0d);
                            break;
                        }
                    case 1:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzb(i9, 0.0f);
                            break;
                        }
                    case 2:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzd(i9, zzhv.zzl(t2, j2));
                            break;
                        }
                    case 3:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zze(i9, zzhv.zzl(t2, j2));
                            break;
                        }
                    case 4:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzg(i9, zzhv.zzk(t2, j2));
                            break;
                        }
                    case 5:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzg(i9, 0L);
                            break;
                        }
                    case 6:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzj(i9, 0);
                            break;
                        }
                    case 7:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzc(i9, true);
                            break;
                        }
                    case 8:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            Object zzp = zzhv.zzp(t2, j2);
                            if (!(zzp instanceof zzdp)) {
                                i7 += zzee.zzc(i9, (String) zzp);
                                break;
                            } else {
                                i7 += zzee.zzc(i9, (zzdp) zzp);
                                break;
                            }
                        }
                    case 9:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzgz.zzc(i9, zzhv.zzp(t2, j2), zzbx(i6));
                            break;
                        }
                    case 10:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzc(i9, (zzdp) zzhv.zzp(t2, j2));
                            break;
                        }
                    case 11:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzh(i9, zzhv.zzk(t2, j2));
                            break;
                        }
                    case 12:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzl(i9, zzhv.zzk(t2, j2));
                            break;
                        }
                    case 13:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzk(i9, 0);
                            break;
                        }
                    case 14:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzh(i9, 0L);
                            break;
                        }
                    case 15:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzi(i9, zzhv.zzk(t2, j2));
                            break;
                        }
                    case 16:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzf(i9, zzhv.zzl(t2, j2));
                            break;
                        }
                    case 17:
                        if (!zza(t2, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzc(i9, (zzgi) zzhv.zzp(t2, j2), zzbx(i6));
                            break;
                        }
                    case 18:
                        i7 += zzgz.zzw(i9, zze(t2, j2), false);
                        break;
                    case 19:
                        i7 += zzgz.zzv(i9, zze(t2, j2), false);
                        break;
                    case 20:
                        i7 += zzgz.zzo(i9, zze(t2, j2), false);
                        break;
                    case 21:
                        i7 += zzgz.zzp(i9, zze(t2, j2), false);
                        break;
                    case 22:
                        i7 += zzgz.zzs(i9, zze(t2, j2), false);
                        break;
                    case 23:
                        i7 += zzgz.zzw(i9, zze(t2, j2), false);
                        break;
                    case 24:
                        i7 += zzgz.zzv(i9, zze(t2, j2), false);
                        break;
                    case 25:
                        i7 += zzgz.zzx(i9, zze(t2, j2), false);
                        break;
                    case 26:
                        i7 += zzgz.zzc(i9, zze(t2, j2));
                        break;
                    case 27:
                        i7 += zzgz.zzc(i9, zze(t2, j2), zzbx(i6));
                        break;
                    case 28:
                        i7 += zzgz.zzd(i9, (List<zzdp>) zze(t2, j2));
                        break;
                    case 29:
                        i7 += zzgz.zzt(i9, zze(t2, j2), false);
                        break;
                    case 30:
                        i7 += zzgz.zzr(i9, zze(t2, j2), false);
                        break;
                    case 31:
                        i7 += zzgz.zzv(i9, zze(t2, j2), false);
                        break;
                    case 32:
                        i7 += zzgz.zzw(i9, zze(t2, j2), false);
                        break;
                    case 33:
                        i7 += zzgz.zzu(i9, zze(t2, j2), false);
                        break;
                    case 34:
                        i7 += zzgz.zzq(i9, zze(t2, j2), false);
                        break;
                    case 35:
                        int zzac = zzgz.zzac((List) unsafe.getObject(t2, j2));
                        if (zzac > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzac);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzac) + zzac;
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        int zzab = zzgz.zzab((List) unsafe.getObject(t2, j2));
                        if (zzab > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzab);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzab) + zzab;
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        int zzu = zzgz.zzu((List) unsafe.getObject(t2, j2));
                        if (zzu > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzu);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzu) + zzu;
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        int zzv = zzgz.zzv((List) unsafe.getObject(t2, j2));
                        if (zzv > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzv);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzv) + zzv;
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        int zzy = zzgz.zzy((List) unsafe.getObject(t2, j2));
                        if (zzy > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzy);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzy) + zzy;
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        int zzac2 = zzgz.zzac((List) unsafe.getObject(t2, j2));
                        if (zzac2 > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzac2);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzac2) + zzac2;
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        int zzab2 = zzgz.zzab((List) unsafe.getObject(t2, j2));
                        if (zzab2 > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzab2);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzab2) + zzab2;
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        int zzad = zzgz.zzad((List) unsafe.getObject(t2, j2));
                        if (zzad > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzad);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzad) + zzad;
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        int zzz = zzgz.zzz((List) unsafe.getObject(t2, j2));
                        if (zzz > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzz);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzz) + zzz;
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        int zzx = zzgz.zzx((List) unsafe.getObject(t2, j2));
                        if (zzx > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzx);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzx) + zzx;
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        int zzab3 = zzgz.zzab((List) unsafe.getObject(t2, j2));
                        if (zzab3 > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzab3);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzab3) + zzab3;
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        int zzac3 = zzgz.zzac((List) unsafe.getObject(t2, j2));
                        if (zzac3 > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzac3);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzac3) + zzac3;
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        int zzaa = zzgz.zzaa((List) unsafe.getObject(t2, j2));
                        if (zzaa > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzaa);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzaa) + zzaa;
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        int zzw = zzgz.zzw((List) unsafe.getObject(t2, j2));
                        if (zzw > 0) {
                            if (this.zzakr) {
                                unsafe.putInt(t2, (long) i10, zzw);
                            }
                            i7 += zzee.zzbi(i9) + zzee.zzbk(zzw) + zzw;
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i7 += zzgz.zzd(i9, zze(t2, j2), zzbx(i6));
                        break;
                    case 50:
                        i7 += this.zzakz.zzb(i9, zzhv.zzp(t2, j2), zzby(i6));
                        break;
                    case 51:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzb(i9, 0.0d);
                            break;
                        }
                    case 52:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzb(i9, 0.0f);
                            break;
                        }
                    case 53:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzd(i9, zzi(t2, j2));
                            break;
                        }
                    case 54:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zze(i9, zzi(t2, j2));
                            break;
                        }
                    case 55:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzg(i9, zzh(t2, j2));
                            break;
                        }
                    case 56:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzg(i9, 0L);
                            break;
                        }
                    case 57:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzj(i9, 0);
                            break;
                        }
                    case 58:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzc(i9, true);
                            break;
                        }
                    case 59:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            Object zzp2 = zzhv.zzp(t2, j2);
                            if (!(zzp2 instanceof zzdp)) {
                                i7 += zzee.zzc(i9, (String) zzp2);
                                break;
                            } else {
                                i7 += zzee.zzc(i9, (zzdp) zzp2);
                                break;
                            }
                        }
                    case 60:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzgz.zzc(i9, zzhv.zzp(t2, j2), zzbx(i6));
                            break;
                        }
                    case 61:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzc(i9, (zzdp) zzhv.zzp(t2, j2));
                            break;
                        }
                    case 62:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzh(i9, zzh(t2, j2));
                            break;
                        }
                    case 63:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzl(i9, zzh(t2, j2));
                            break;
                        }
                    case 64:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzk(i9, 0);
                            break;
                        }
                    case 65:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzh(i9, 0L);
                            break;
                        }
                    case 66:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzi(i9, zzh(t2, j2));
                            break;
                        }
                    case 67:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzf(i9, zzi(t2, j2));
                            break;
                        }
                    case 68:
                        if (!zza(t2, i9, i6)) {
                            break;
                        } else {
                            i7 += zzee.zzc(i9, (zzgi) zzhv.zzp(t2, j2), zzbx(i6));
                            break;
                        }
                }
                i6 += 3;
                i3 = 267386880;
            }
            return i7 + zza((zzhp) this.zzakx, (Object) t2);
        }
        Unsafe unsafe2 = zzaki;
        int i11 = 0;
        int i12 = 0;
        int i13 = -1;
        int i14 = 0;
        while (i11 < this.zzakj.length) {
            int zzca2 = zzca(i11);
            int i15 = this.zzakj[i11];
            int i16 = (zzca2 & 267386880) >>> 20;
            if (i16 <= 17) {
                i2 = this.zzakj[i11 + 2];
                int i17 = i2 & i4;
                i = i5 << (i2 >>> 20);
                if (i17 != i13) {
                    i14 = unsafe2.getInt(t2, (long) i17);
                    i13 = i17;
                }
            } else {
                i2 = (!this.zzakr || i16 < zzet.DOUBLE_LIST_PACKED.id() || i16 > zzet.SINT64_LIST_PACKED.id()) ? 0 : this.zzakj[i11 + 2] & i4;
                i = 0;
            }
            long j3 = (long) (zzca2 & i4);
            switch (i16) {
                case 0:
                    j = 0;
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzb(i15, 0.0d);
                        continue;
                        i11 += 3;
                        i4 = 1048575;
                        i5 = 1;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzb(i15, 0.0f);
                        break;
                    }
                    break;
                case 2:
                    j = 0;
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzd(i15, unsafe2.getLong(t2, j3));
                        break;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i14 & i) != 0) {
                        i12 += zzee.zze(i15, unsafe2.getLong(t2, j3));
                        break;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzg(i15, unsafe2.getInt(t2, j3));
                        break;
                    }
                    break;
                case 5:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzg(i15, 0L);
                        j = 0;
                        break;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 6:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzj(i15, 0);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 7:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzc(i15, true);
                        j = 0;
                        i11 += 3;
                        i4 = 1048575;
                        i5 = 1;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 8:
                    if ((i14 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        i12 = object instanceof zzdp ? i12 + zzee.zzc(i15, (zzdp) object) : i12 + zzee.zzc(i15, (String) object);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 9:
                    if ((i14 & i) != 0) {
                        i12 += zzgz.zzc(i15, unsafe2.getObject(t2, j3), zzbx(i11));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 10:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzc(i15, (zzdp) unsafe2.getObject(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 11:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzh(i15, unsafe2.getInt(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 12:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzl(i15, unsafe2.getInt(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 13:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzk(i15, 0);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 14:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzh(i15, 0L);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 15:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzi(i15, unsafe2.getInt(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 16:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzf(i15, unsafe2.getLong(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 17:
                    if ((i14 & i) != 0) {
                        i12 += zzee.zzc(i15, (zzgi) unsafe2.getObject(t2, j3), zzbx(i11));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 18:
                    i12 += zzgz.zzw(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 19:
                    i12 += zzgz.zzv(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 20:
                    i12 += zzgz.zzo(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 21:
                    i12 += zzgz.zzp(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 22:
                    i12 += zzgz.zzs(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 23:
                    i12 += zzgz.zzw(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 24:
                    i12 += zzgz.zzv(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 25:
                    i12 += zzgz.zzx(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 26:
                    i12 += zzgz.zzc(i15, (List) unsafe2.getObject(t2, j3));
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 27:
                    i12 += zzgz.zzc(i15, (List<?>) ((List) unsafe2.getObject(t2, j3)), zzbx(i11));
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 28:
                    i12 += zzgz.zzd(i15, (List) unsafe2.getObject(t2, j3));
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 29:
                    i12 += zzgz.zzt(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 30:
                    i12 += zzgz.zzr(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 31:
                    i12 += zzgz.zzv(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 32:
                    i12 += zzgz.zzw(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 33:
                    i12 += zzgz.zzu(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 34:
                    i12 += zzgz.zzq(i15, (List) unsafe2.getObject(t2, j3), false);
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 35:
                    int zzac4 = zzgz.zzac((List) unsafe2.getObject(t2, j3));
                    if (zzac4 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzac4);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzac4) + zzac4;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 36:
                    int zzab4 = zzgz.zzab((List) unsafe2.getObject(t2, j3));
                    if (zzab4 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzab4);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzab4) + zzab4;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 37:
                    int zzu2 = zzgz.zzu((List) unsafe2.getObject(t2, j3));
                    if (zzu2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzu2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzu2) + zzu2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 38:
                    int zzv2 = zzgz.zzv((List) unsafe2.getObject(t2, j3));
                    if (zzv2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzv2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzv2) + zzv2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 39:
                    int zzy2 = zzgz.zzy((List) unsafe2.getObject(t2, j3));
                    if (zzy2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzy2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzy2) + zzy2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 40:
                    int zzac5 = zzgz.zzac((List) unsafe2.getObject(t2, j3));
                    if (zzac5 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzac5);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzac5) + zzac5;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 41:
                    int zzab5 = zzgz.zzab((List) unsafe2.getObject(t2, j3));
                    if (zzab5 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzab5);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzab5) + zzab5;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 42:
                    int zzad2 = zzgz.zzad((List) unsafe2.getObject(t2, j3));
                    if (zzad2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzad2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzad2) + zzad2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 43:
                    int zzz2 = zzgz.zzz((List) unsafe2.getObject(t2, j3));
                    if (zzz2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzz2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzz2) + zzz2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 44:
                    int zzx2 = zzgz.zzx((List) unsafe2.getObject(t2, j3));
                    if (zzx2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzx2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzx2) + zzx2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 45:
                    int zzab6 = zzgz.zzab((List) unsafe2.getObject(t2, j3));
                    if (zzab6 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzab6);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzab6) + zzab6;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 46:
                    int zzac6 = zzgz.zzac((List) unsafe2.getObject(t2, j3));
                    if (zzac6 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzac6);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzac6) + zzac6;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 47:
                    int zzaa2 = zzgz.zzaa((List) unsafe2.getObject(t2, j3));
                    if (zzaa2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzaa2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzaa2) + zzaa2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 48:
                    int zzw2 = zzgz.zzw((List) unsafe2.getObject(t2, j3));
                    if (zzw2 > 0) {
                        if (this.zzakr) {
                            unsafe2.putInt(t2, (long) i2, zzw2);
                        }
                        i12 += zzee.zzbi(i15) + zzee.zzbk(zzw2) + zzw2;
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 49:
                    i12 += zzgz.zzd(i15, (List) unsafe2.getObject(t2, j3), zzbx(i11));
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 50:
                    i12 += this.zzakz.zzb(i15, unsafe2.getObject(t2, j3), zzby(i11));
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 51:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzb(i15, 0.0d);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 52:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzb(i15, 0.0f);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 53:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzd(i15, zzi(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 54:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zze(i15, zzi(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 55:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzg(i15, zzh(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 56:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzg(i15, 0L);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 57:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzj(i15, 0);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 58:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzc(i15, true);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 59:
                    if (zza(t2, i15, i11)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        i12 = object2 instanceof zzdp ? i12 + zzee.zzc(i15, (zzdp) object2) : i12 + zzee.zzc(i15, (String) object2);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 60:
                    if (zza(t2, i15, i11)) {
                        i12 += zzgz.zzc(i15, unsafe2.getObject(t2, j3), zzbx(i11));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 61:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzc(i15, (zzdp) unsafe2.getObject(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 62:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzh(i15, zzh(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 63:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzl(i15, zzh(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 64:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzk(i15, 0);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 65:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzh(i15, 0L);
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 66:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzi(i15, zzh(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 67:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzf(i15, zzi(t2, j3));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                case 68:
                    if (zza(t2, i15, i11)) {
                        i12 += zzee.zzc(i15, (zzgi) unsafe2.getObject(t2, j3), zzbx(i11));
                    }
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
                default:
                    j = 0;
                    i11 += 3;
                    i4 = 1048575;
                    i5 = 1;
            }
            i11 += 3;
            i4 = 1048575;
            i5 = 1;
        }
        int zza = i12 + zza((zzhp) this.zzakx, (Object) t2);
        if (!this.zzako) {
            return zza;
        }
        zzeo<?> zzh = this.zzaky.zzh(t2);
        int i18 = 0;
        for (int i19 = 0; i19 < zzh.zzaex.zzwh(); i19++) {
            Map.Entry<FieldDescriptorType, Object> zzcf = zzh.zzaex.zzcf(i19);
            i18 += zzeo.zzb((zzeq) zzcf.getKey(), zzcf.getValue());
        }
        for (Map.Entry next : zzh.zzaex.zzwi()) {
            i18 += zzeo.zzb((zzeq) next.getKey(), next.getValue());
        }
        return zza + i18;
    }

    private static <UT, UB> int zza(zzhp<UT, UB> zzhp, T t) {
        return zzhp.zzt(zzhp.zzx(t));
    }

    private static List<?> zze(Object obj, long j) {
        return (List) zzhv.zzp(obj, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgz.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zzim, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zzim, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgz.zzb(int, java.util.List<?>, com.google.android.gms.internal.measurement.zzim, com.google.android.gms.internal.measurement.zzgx):void
      com.google.android.gms.internal.measurement.zzgz.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zzim, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgz.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zzim, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zzim, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgz.zza(int, int, java.lang.Object, com.google.android.gms.internal.measurement.zzhp):UB
      com.google.android.gms.internal.measurement.zzgz.zza(int, java.util.List<?>, com.google.android.gms.internal.measurement.zzim, com.google.android.gms.internal.measurement.zzgx):void
      com.google.android.gms.internal.measurement.zzgz.zza(com.google.android.gms.internal.measurement.zzgb, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.zzgz.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zzim, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0553  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a2b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r14, com.google.android.gms.internal.measurement.zzim r15) throws java.io.IOException {
        /*
            r13 = this;
            int r0 = r15.zztk()
            int r1 = com.google.android.gms.internal.measurement.zzey.zzd.zzaip
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x0529
            com.google.android.gms.internal.measurement.zzhp<?, ?> r0 = r13.zzakx
            zza(r0, r14, r15)
            boolean r0 = r13.zzako
            if (r0 == 0) goto L_0x0032
            com.google.android.gms.internal.measurement.zzen<?> r0 = r13.zzaky
            com.google.android.gms.internal.measurement.zzeo r0 = r0.zzh(r14)
            com.google.android.gms.internal.measurement.zzhc<FieldDescriptorType, java.lang.Object> r1 = r0.zzaex
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0032
            java.util.Iterator r0 = r0.descendingIterator()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0034
        L_0x0032:
            r0 = r3
            r1 = r0
        L_0x0034:
            int[] r7 = r13.zzakj
            int r7 = r7.length
            int r7 = r7 + -3
        L_0x0039:
            if (r7 < 0) goto L_0x0511
            int r8 = r13.zzca(r7)
            int[] r9 = r13.zzakj
            r9 = r9[r7]
        L_0x0043:
            if (r1 == 0) goto L_0x0061
            com.google.android.gms.internal.measurement.zzen<?> r10 = r13.zzaky
            int r10 = r10.zza(r1)
            if (r10 <= r9) goto L_0x0061
            com.google.android.gms.internal.measurement.zzen<?> r10 = r13.zzaky
            r10.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x005f
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0043
        L_0x005f:
            r1 = r3
            goto L_0x0043
        L_0x0061:
            r10 = r8 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04fe;
                case 1: goto L_0x04ee;
                case 2: goto L_0x04de;
                case 3: goto L_0x04ce;
                case 4: goto L_0x04be;
                case 5: goto L_0x04ae;
                case 6: goto L_0x049e;
                case 7: goto L_0x048d;
                case 8: goto L_0x047c;
                case 9: goto L_0x0467;
                case 10: goto L_0x0454;
                case 11: goto L_0x0443;
                case 12: goto L_0x0432;
                case 13: goto L_0x0421;
                case 14: goto L_0x0410;
                case 15: goto L_0x03ff;
                case 16: goto L_0x03ee;
                case 17: goto L_0x03d9;
                case 18: goto L_0x03c8;
                case 19: goto L_0x03b7;
                case 20: goto L_0x03a6;
                case 21: goto L_0x0395;
                case 22: goto L_0x0384;
                case 23: goto L_0x0373;
                case 24: goto L_0x0362;
                case 25: goto L_0x0351;
                case 26: goto L_0x0340;
                case 27: goto L_0x032b;
                case 28: goto L_0x031a;
                case 29: goto L_0x0309;
                case 30: goto L_0x02f8;
                case 31: goto L_0x02e7;
                case 32: goto L_0x02d6;
                case 33: goto L_0x02c5;
                case 34: goto L_0x02b4;
                case 35: goto L_0x02a3;
                case 36: goto L_0x0292;
                case 37: goto L_0x0281;
                case 38: goto L_0x0270;
                case 39: goto L_0x025f;
                case 40: goto L_0x024e;
                case 41: goto L_0x023d;
                case 42: goto L_0x022c;
                case 43: goto L_0x021b;
                case 44: goto L_0x020a;
                case 45: goto L_0x01f9;
                case 46: goto L_0x01e8;
                case 47: goto L_0x01d7;
                case 48: goto L_0x01c6;
                case 49: goto L_0x01b1;
                case 50: goto L_0x01a6;
                case 51: goto L_0x0195;
                case 52: goto L_0x0184;
                case 53: goto L_0x0173;
                case 54: goto L_0x0162;
                case 55: goto L_0x0151;
                case 56: goto L_0x0140;
                case 57: goto L_0x012f;
                case 58: goto L_0x011e;
                case 59: goto L_0x010d;
                case 60: goto L_0x00f8;
                case 61: goto L_0x00e5;
                case 62: goto L_0x00d4;
                case 63: goto L_0x00c3;
                case 64: goto L_0x00b2;
                case 65: goto L_0x00a1;
                case 66: goto L_0x0090;
                case 67: goto L_0x007f;
                case 68: goto L_0x006a;
                default: goto L_0x0068;
            }
        L_0x0068:
            goto L_0x050d
        L_0x006a:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            com.google.android.gms.internal.measurement.zzgx r10 = r13.zzbx(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050d
        L_0x007f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050d
        L_0x0090:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zze(r9, r8)
            goto L_0x050d
        L_0x00a1:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzj(r9, r10)
            goto L_0x050d
        L_0x00b2:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzm(r9, r8)
            goto L_0x050d
        L_0x00c3:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzn(r9, r8)
            goto L_0x050d
        L_0x00d4:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzd(r9, r8)
            goto L_0x050d
        L_0x00e5:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            com.google.android.gms.internal.measurement.zzdp r8 = (com.google.android.gms.internal.measurement.zzdp) r8
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x00f8:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            com.google.android.gms.internal.measurement.zzgx r10 = r13.zzbx(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050d
        L_0x010d:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050d
        L_0x011e:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = zzj(r14, r10)
            r15.zzb(r9, r8)
            goto L_0x050d
        L_0x012f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x0140:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzc(r9, r10)
            goto L_0x050d
        L_0x0151:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzh(r14, r10)
            r15.zzc(r9, r8)
            goto L_0x050d
        L_0x0162:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x0173:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzi(r14, r10)
            r15.zzi(r9, r10)
            goto L_0x050d
        L_0x0184:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = zzg(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0195:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = zzf(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x01a6:
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            r13.zza(r15, r9, r8, r7)
            goto L_0x050d
        L_0x01b1:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgx r10 = r13.zzbx(r7)
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r8, r15, r10)
            goto L_0x050d
        L_0x01c6:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zze(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01d7:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzj(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01e8:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzg(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01f9:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzl(r9, r8, r15, r4)
            goto L_0x050d
        L_0x020a:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzm(r9, r8, r15, r4)
            goto L_0x050d
        L_0x021b:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzi(r9, r8, r15, r4)
            goto L_0x050d
        L_0x022c:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzn(r9, r8, r15, r4)
            goto L_0x050d
        L_0x023d:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzk(r9, r8, r15, r4)
            goto L_0x050d
        L_0x024e:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzf(r9, r8, r15, r4)
            goto L_0x050d
        L_0x025f:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzh(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0270:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzd(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0281:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzc(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0292:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r8, r15, r4)
            goto L_0x050d
        L_0x02a3:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r8, r15, r4)
            goto L_0x050d
        L_0x02b4:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zze(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02c5:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzj(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02d6:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzg(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02e7:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzl(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02f8:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzm(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0309:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzi(r9, r8, r15, r5)
            goto L_0x050d
        L_0x031a:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r8, r15)
            goto L_0x050d
        L_0x032b:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgx r10 = r13.zzbx(r7)
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r8, r15, r10)
            goto L_0x050d
        L_0x0340:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r8, r15)
            goto L_0x050d
        L_0x0351:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzn(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0362:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzk(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0373:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzf(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0384:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzh(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0395:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzd(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03a6:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzc(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03b7:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03c8:
            int[] r9 = r13.zzakj
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03d9:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            com.google.android.gms.internal.measurement.zzgx r10 = r13.zzbx(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050d
        L_0x03ee:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050d
        L_0x03ff:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r10)
            r15.zze(r9, r8)
            goto L_0x050d
        L_0x0410:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r10)
            r15.zzj(r9, r10)
            goto L_0x050d
        L_0x0421:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r10)
            r15.zzm(r9, r8)
            goto L_0x050d
        L_0x0432:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r10)
            r15.zzn(r9, r8)
            goto L_0x050d
        L_0x0443:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r10)
            r15.zzd(r9, r8)
            goto L_0x050d
        L_0x0454:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            com.google.android.gms.internal.measurement.zzdp r8 = (com.google.android.gms.internal.measurement.zzdp) r8
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0467:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            com.google.android.gms.internal.measurement.zzgx r10 = r13.zzbx(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050d
        L_0x047c:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050d
        L_0x048d:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = com.google.android.gms.internal.measurement.zzhv.zzm(r14, r10)
            r15.zzb(r9, r8)
            goto L_0x050d
        L_0x049e:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x04ae:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r10)
            r15.zzc(r9, r10)
            goto L_0x050d
        L_0x04be:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r10)
            r15.zzc(r9, r8)
            goto L_0x050d
        L_0x04ce:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x04de:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r10)
            r15.zzi(r9, r10)
            goto L_0x050d
        L_0x04ee:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = com.google.android.gms.internal.measurement.zzhv.zzn(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x04fe:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = com.google.android.gms.internal.measurement.zzhv.zzo(r14, r10)
            r15.zza(r9, r10)
        L_0x050d:
            int r7 = r7 + -3
            goto L_0x0039
        L_0x0511:
            if (r1 == 0) goto L_0x0528
            com.google.android.gms.internal.measurement.zzen<?> r14 = r13.zzaky
            r14.zza(r15, r1)
            boolean r14 = r0.hasNext()
            if (r14 == 0) goto L_0x0526
            java.lang.Object r14 = r0.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            r1 = r14
            goto L_0x0511
        L_0x0526:
            r1 = r3
            goto L_0x0511
        L_0x0528:
            return
        L_0x0529:
            boolean r0 = r13.zzakq
            if (r0 == 0) goto L_0x0a46
            boolean r0 = r13.zzako
            if (r0 == 0) goto L_0x054a
            com.google.android.gms.internal.measurement.zzen<?> r0 = r13.zzaky
            com.google.android.gms.internal.measurement.zzeo r0 = r0.zzh(r14)
            com.google.android.gms.internal.measurement.zzhc<FieldDescriptorType, java.lang.Object> r1 = r0.zzaex
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x054a
            java.util.Iterator r0 = r0.iterator()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x054c
        L_0x054a:
            r0 = r3
            r1 = r0
        L_0x054c:
            int[] r7 = r13.zzakj
            int r7 = r7.length
            r8 = r1
            r1 = 0
        L_0x0551:
            if (r1 >= r7) goto L_0x0a29
            int r9 = r13.zzca(r1)
            int[] r10 = r13.zzakj
            r10 = r10[r1]
        L_0x055b:
            if (r8 == 0) goto L_0x0579
            com.google.android.gms.internal.measurement.zzen<?> r11 = r13.zzaky
            int r11 = r11.zza(r8)
            if (r11 > r10) goto L_0x0579
            com.google.android.gms.internal.measurement.zzen<?> r11 = r13.zzaky
            r11.zza(r15, r8)
            boolean r8 = r0.hasNext()
            if (r8 == 0) goto L_0x0577
            java.lang.Object r8 = r0.next()
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8
            goto L_0x055b
        L_0x0577:
            r8 = r3
            goto L_0x055b
        L_0x0579:
            r11 = r9 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0a16;
                case 1: goto L_0x0a06;
                case 2: goto L_0x09f6;
                case 3: goto L_0x09e6;
                case 4: goto L_0x09d6;
                case 5: goto L_0x09c6;
                case 6: goto L_0x09b6;
                case 7: goto L_0x09a5;
                case 8: goto L_0x0994;
                case 9: goto L_0x097f;
                case 10: goto L_0x096c;
                case 11: goto L_0x095b;
                case 12: goto L_0x094a;
                case 13: goto L_0x0939;
                case 14: goto L_0x0928;
                case 15: goto L_0x0917;
                case 16: goto L_0x0906;
                case 17: goto L_0x08f1;
                case 18: goto L_0x08e0;
                case 19: goto L_0x08cf;
                case 20: goto L_0x08be;
                case 21: goto L_0x08ad;
                case 22: goto L_0x089c;
                case 23: goto L_0x088b;
                case 24: goto L_0x087a;
                case 25: goto L_0x0869;
                case 26: goto L_0x0858;
                case 27: goto L_0x0843;
                case 28: goto L_0x0832;
                case 29: goto L_0x0821;
                case 30: goto L_0x0810;
                case 31: goto L_0x07ff;
                case 32: goto L_0x07ee;
                case 33: goto L_0x07dd;
                case 34: goto L_0x07cc;
                case 35: goto L_0x07bb;
                case 36: goto L_0x07aa;
                case 37: goto L_0x0799;
                case 38: goto L_0x0788;
                case 39: goto L_0x0777;
                case 40: goto L_0x0766;
                case 41: goto L_0x0755;
                case 42: goto L_0x0744;
                case 43: goto L_0x0733;
                case 44: goto L_0x0722;
                case 45: goto L_0x0711;
                case 46: goto L_0x0700;
                case 47: goto L_0x06ef;
                case 48: goto L_0x06de;
                case 49: goto L_0x06c9;
                case 50: goto L_0x06be;
                case 51: goto L_0x06ad;
                case 52: goto L_0x069c;
                case 53: goto L_0x068b;
                case 54: goto L_0x067a;
                case 55: goto L_0x0669;
                case 56: goto L_0x0658;
                case 57: goto L_0x0647;
                case 58: goto L_0x0636;
                case 59: goto L_0x0625;
                case 60: goto L_0x0610;
                case 61: goto L_0x05fd;
                case 62: goto L_0x05ec;
                case 63: goto L_0x05db;
                case 64: goto L_0x05ca;
                case 65: goto L_0x05b9;
                case 66: goto L_0x05a8;
                case 67: goto L_0x0597;
                case 68: goto L_0x0582;
                default: goto L_0x0580;
            }
        L_0x0580:
            goto L_0x0a25
        L_0x0582:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            com.google.android.gms.internal.measurement.zzgx r11 = r13.zzbx(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a25
        L_0x0597:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a25
        L_0x05a8:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zze(r10, r9)
            goto L_0x0a25
        L_0x05b9:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzj(r10, r11)
            goto L_0x0a25
        L_0x05ca:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzm(r10, r9)
            goto L_0x0a25
        L_0x05db:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzn(r10, r9)
            goto L_0x0a25
        L_0x05ec:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzd(r10, r9)
            goto L_0x0a25
        L_0x05fd:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            com.google.android.gms.internal.measurement.zzdp r9 = (com.google.android.gms.internal.measurement.zzdp) r9
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0610:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            com.google.android.gms.internal.measurement.zzgx r11 = r13.zzbx(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a25
        L_0x0625:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a25
        L_0x0636:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = zzj(r14, r11)
            r15.zzb(r10, r9)
            goto L_0x0a25
        L_0x0647:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a25
        L_0x0658:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzc(r10, r11)
            goto L_0x0a25
        L_0x0669:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzh(r14, r11)
            r15.zzc(r10, r9)
            goto L_0x0a25
        L_0x067a:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x068b:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzi(r14, r11)
            r15.zzi(r10, r11)
            goto L_0x0a25
        L_0x069c:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = zzg(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x06ad:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = zzf(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x06be:
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            r13.zza(r15, r10, r9, r1)
            goto L_0x0a25
        L_0x06c9:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgx r11 = r13.zzbx(r1)
            com.google.android.gms.internal.measurement.zzgz.zzb(r10, r9, r15, r11)
            goto L_0x0a25
        L_0x06de:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zze(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x06ef:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzj(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0700:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzg(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0711:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzl(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0722:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzm(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0733:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzi(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0744:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzn(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0755:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzk(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0766:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzf(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0777:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzh(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0788:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzd(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0799:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzc(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07aa:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzb(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07bb:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zza(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07cc:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zze(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07dd:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzj(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07ee:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzg(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07ff:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzl(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0810:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzm(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0821:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzi(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0832:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzb(r10, r9, r15)
            goto L_0x0a25
        L_0x0843:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgx r11 = r13.zzbx(r1)
            com.google.android.gms.internal.measurement.zzgz.zza(r10, r9, r15, r11)
            goto L_0x0a25
        L_0x0858:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zza(r10, r9, r15)
            goto L_0x0a25
        L_0x0869:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzn(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x087a:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzk(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x088b:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzf(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x089c:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzh(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08ad:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzd(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08be:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzc(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08cf:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zzb(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08e0:
            int[] r10 = r13.zzakj
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzgz.zza(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08f1:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            com.google.android.gms.internal.measurement.zzgx r11 = r13.zzbx(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a25
        L_0x0906:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a25
        L_0x0917:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r11)
            r15.zze(r10, r9)
            goto L_0x0a25
        L_0x0928:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r11)
            r15.zzj(r10, r11)
            goto L_0x0a25
        L_0x0939:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r11)
            r15.zzm(r10, r9)
            goto L_0x0a25
        L_0x094a:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r11)
            r15.zzn(r10, r9)
            goto L_0x0a25
        L_0x095b:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r11)
            r15.zzd(r10, r9)
            goto L_0x0a25
        L_0x096c:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            com.google.android.gms.internal.measurement.zzdp r9 = (com.google.android.gms.internal.measurement.zzdp) r9
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x097f:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            com.google.android.gms.internal.measurement.zzgx r11 = r13.zzbx(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a25
        L_0x0994:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a25
        L_0x09a5:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = com.google.android.gms.internal.measurement.zzhv.zzm(r14, r11)
            r15.zzb(r10, r9)
            goto L_0x0a25
        L_0x09b6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a25
        L_0x09c6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r11)
            r15.zzc(r10, r11)
            goto L_0x0a25
        L_0x09d6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzhv.zzk(r14, r11)
            r15.zzc(r10, r9)
            goto L_0x0a25
        L_0x09e6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x09f6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzhv.zzl(r14, r11)
            r15.zzi(r10, r11)
            goto L_0x0a25
        L_0x0a06:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = com.google.android.gms.internal.measurement.zzhv.zzn(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0a16:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = com.google.android.gms.internal.measurement.zzhv.zzo(r14, r11)
            r15.zza(r10, r11)
        L_0x0a25:
            int r1 = r1 + 3
            goto L_0x0551
        L_0x0a29:
            if (r8 == 0) goto L_0x0a40
            com.google.android.gms.internal.measurement.zzen<?> r1 = r13.zzaky
            r1.zza(r15, r8)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0a3e
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            r8 = r1
            goto L_0x0a29
        L_0x0a3e:
            r8 = r3
            goto L_0x0a29
        L_0x0a40:
            com.google.android.gms.internal.measurement.zzhp<?, ?> r0 = r13.zzakx
            zza(r0, r14, r15)
            return
        L_0x0a46:
            r13.zzb(r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzim):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgz.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zzim, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zzim, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgz.zzb(int, java.util.List<?>, com.google.android.gms.internal.measurement.zzim, com.google.android.gms.internal.measurement.zzgx):void
      com.google.android.gms.internal.measurement.zzgz.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zzim, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgz.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zzim, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zzim, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgz.zza(int, int, java.lang.Object, com.google.android.gms.internal.measurement.zzhp):UB
      com.google.android.gms.internal.measurement.zzgz.zza(int, java.util.List<?>, com.google.android.gms.internal.measurement.zzim, com.google.android.gms.internal.measurement.zzgx):void
      com.google.android.gms.internal.measurement.zzgz.zza(com.google.android.gms.internal.measurement.zzgb, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.zzgz.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zzim, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0529  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(T r20, com.google.android.gms.internal.measurement.zzim r21) throws java.io.IOException {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            r2 = r21
            boolean r3 = r0.zzako
            if (r3 == 0) goto L_0x0023
            com.google.android.gms.internal.measurement.zzen<?> r3 = r0.zzaky
            com.google.android.gms.internal.measurement.zzeo r3 = r3.zzh(r1)
            com.google.android.gms.internal.measurement.zzhc<FieldDescriptorType, java.lang.Object> r5 = r3.zzaex
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.iterator()
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x0025
        L_0x0023:
            r3 = 0
            r5 = 0
        L_0x0025:
            r6 = -1
            int[] r7 = r0.zzakj
            int r7 = r7.length
            sun.misc.Unsafe r8 = com.google.android.gms.internal.measurement.zzgm.zzaki
            r10 = r5
            r5 = 0
            r11 = 0
        L_0x002e:
            if (r5 >= r7) goto L_0x0527
            int r12 = r0.zzca(r5)
            int[] r13 = r0.zzakj
            r13 = r13[r5]
            r14 = 267386880(0xff00000, float:2.3665827E-29)
            r14 = r14 & r12
            int r14 = r14 >>> 20
            boolean r15 = r0.zzakq
            r16 = 1048575(0xfffff, float:1.469367E-39)
            if (r15 != 0) goto L_0x0063
            r15 = 17
            if (r14 > r15) goto L_0x0063
            int[] r15 = r0.zzakj
            int r17 = r5 + 2
            r15 = r15[r17]
            r9 = r15 & r16
            if (r9 == r6) goto L_0x005b
            r18 = r5
            long r4 = (long) r9
            int r11 = r8.getInt(r1, r4)
            r6 = r9
            goto L_0x005d
        L_0x005b:
            r18 = r5
        L_0x005d:
            int r4 = r15 >>> 20
            r5 = 1
            int r9 = r5 << r4
            goto L_0x0066
        L_0x0063:
            r18 = r5
            r9 = 0
        L_0x0066:
            if (r10 == 0) goto L_0x0085
            com.google.android.gms.internal.measurement.zzen<?> r4 = r0.zzaky
            int r4 = r4.zza(r10)
            if (r4 > r13) goto L_0x0085
            com.google.android.gms.internal.measurement.zzen<?> r4 = r0.zzaky
            r4.zza(r2, r10)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0083
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r10 = r4
            goto L_0x0066
        L_0x0083:
            r10 = 0
            goto L_0x0066
        L_0x0085:
            r4 = r12 & r16
            long r4 = (long) r4
            switch(r14) {
                case 0: goto L_0x0516;
                case 1: goto L_0x0508;
                case 2: goto L_0x04fa;
                case 3: goto L_0x04ec;
                case 4: goto L_0x04de;
                case 5: goto L_0x04d0;
                case 6: goto L_0x04c2;
                case 7: goto L_0x04b4;
                case 8: goto L_0x04a5;
                case 9: goto L_0x0492;
                case 10: goto L_0x0481;
                case 11: goto L_0x0472;
                case 12: goto L_0x0463;
                case 13: goto L_0x0454;
                case 14: goto L_0x0445;
                case 15: goto L_0x0436;
                case 16: goto L_0x0427;
                case 17: goto L_0x0414;
                case 18: goto L_0x0402;
                case 19: goto L_0x03f0;
                case 20: goto L_0x03de;
                case 21: goto L_0x03cc;
                case 22: goto L_0x03ba;
                case 23: goto L_0x03a8;
                case 24: goto L_0x0396;
                case 25: goto L_0x0384;
                case 26: goto L_0x0373;
                case 27: goto L_0x035e;
                case 28: goto L_0x034d;
                case 29: goto L_0x033b;
                case 30: goto L_0x0329;
                case 31: goto L_0x0317;
                case 32: goto L_0x0305;
                case 33: goto L_0x02f3;
                case 34: goto L_0x02e1;
                case 35: goto L_0x02cf;
                case 36: goto L_0x02bd;
                case 37: goto L_0x02ab;
                case 38: goto L_0x0299;
                case 39: goto L_0x0287;
                case 40: goto L_0x0275;
                case 41: goto L_0x0263;
                case 42: goto L_0x0251;
                case 43: goto L_0x023f;
                case 44: goto L_0x022d;
                case 45: goto L_0x021b;
                case 46: goto L_0x0209;
                case 47: goto L_0x01f7;
                case 48: goto L_0x01e5;
                case 49: goto L_0x01d0;
                case 50: goto L_0x01c5;
                case 51: goto L_0x01b4;
                case 52: goto L_0x01a3;
                case 53: goto L_0x0192;
                case 54: goto L_0x0181;
                case 55: goto L_0x0170;
                case 56: goto L_0x015f;
                case 57: goto L_0x014e;
                case 58: goto L_0x013d;
                case 59: goto L_0x012c;
                case 60: goto L_0x0117;
                case 61: goto L_0x0104;
                case 62: goto L_0x00f4;
                case 63: goto L_0x00e4;
                case 64: goto L_0x00d4;
                case 65: goto L_0x00c4;
                case 66: goto L_0x00b4;
                case 67: goto L_0x00a4;
                case 68: goto L_0x0090;
                default: goto L_0x008b;
            }
        L_0x008b:
            r12 = r18
        L_0x008d:
            r14 = 0
            goto L_0x0523
        L_0x0090:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            java.lang.Object r4 = r8.getObject(r1, r4)
            com.google.android.gms.internal.measurement.zzgx r5 = r0.zzbx(r12)
            r2.zzb(r13, r4, r5)
            goto L_0x008d
        L_0x00a4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            long r4 = zzi(r1, r4)
            r2.zzb(r13, r4)
            goto L_0x008d
        L_0x00b4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            int r4 = zzh(r1, r4)
            r2.zze(r13, r4)
            goto L_0x008d
        L_0x00c4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            long r4 = zzi(r1, r4)
            r2.zzj(r13, r4)
            goto L_0x008d
        L_0x00d4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            int r4 = zzh(r1, r4)
            r2.zzm(r13, r4)
            goto L_0x008d
        L_0x00e4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            int r4 = zzh(r1, r4)
            r2.zzn(r13, r4)
            goto L_0x008d
        L_0x00f4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            int r4 = zzh(r1, r4)
            r2.zzd(r13, r4)
            goto L_0x008d
        L_0x0104:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            java.lang.Object r4 = r8.getObject(r1, r4)
            com.google.android.gms.internal.measurement.zzdp r4 = (com.google.android.gms.internal.measurement.zzdp) r4
            r2.zza(r13, r4)
            goto L_0x008d
        L_0x0117:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            java.lang.Object r4 = r8.getObject(r1, r4)
            com.google.android.gms.internal.measurement.zzgx r5 = r0.zzbx(r12)
            r2.zza(r13, r4, r5)
            goto L_0x008d
        L_0x012c:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            java.lang.Object r4 = r8.getObject(r1, r4)
            zza(r13, r4, r2)
            goto L_0x008d
        L_0x013d:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            boolean r4 = zzj(r1, r4)
            r2.zzb(r13, r4)
            goto L_0x008d
        L_0x014e:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            int r4 = zzh(r1, r4)
            r2.zzf(r13, r4)
            goto L_0x008d
        L_0x015f:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            long r4 = zzi(r1, r4)
            r2.zzc(r13, r4)
            goto L_0x008d
        L_0x0170:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            int r4 = zzh(r1, r4)
            r2.zzc(r13, r4)
            goto L_0x008d
        L_0x0181:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            long r4 = zzi(r1, r4)
            r2.zza(r13, r4)
            goto L_0x008d
        L_0x0192:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            long r4 = zzi(r1, r4)
            r2.zzi(r13, r4)
            goto L_0x008d
        L_0x01a3:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            float r4 = zzg(r1, r4)
            r2.zza(r13, r4)
            goto L_0x008d
        L_0x01b4:
            r12 = r18
            boolean r9 = r0.zza(r1, r13, r12)
            if (r9 == 0) goto L_0x008d
            double r4 = zzf(r1, r4)
            r2.zza(r13, r4)
            goto L_0x008d
        L_0x01c5:
            r12 = r18
            java.lang.Object r4 = r8.getObject(r1, r4)
            r0.zza(r2, r13, r4, r12)
            goto L_0x008d
        L_0x01d0:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgx r5 = r0.zzbx(r12)
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r4, r2, r5)
            goto L_0x008d
        L_0x01e5:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            r13 = 1
            com.google.android.gms.internal.measurement.zzgz.zze(r9, r4, r2, r13)
            goto L_0x008d
        L_0x01f7:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzj(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0209:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzg(r9, r4, r2, r13)
            goto L_0x008d
        L_0x021b:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzl(r9, r4, r2, r13)
            goto L_0x008d
        L_0x022d:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzm(r9, r4, r2, r13)
            goto L_0x008d
        L_0x023f:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzi(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0251:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzn(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0263:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzk(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0275:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzf(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0287:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzh(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0299:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzd(r9, r4, r2, r13)
            goto L_0x008d
        L_0x02ab:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzc(r9, r4, r2, r13)
            goto L_0x008d
        L_0x02bd:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r4, r2, r13)
            goto L_0x008d
        L_0x02cf:
            r12 = r18
            r13 = 1
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r4, r2, r13)
            goto L_0x008d
        L_0x02e1:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            r13 = 0
            com.google.android.gms.internal.measurement.zzgz.zze(r9, r4, r2, r13)
            goto L_0x008d
        L_0x02f3:
            r12 = r18
            r13 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzj(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0305:
            r12 = r18
            r13 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzg(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0317:
            r12 = r18
            r13 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzl(r9, r4, r2, r13)
            goto L_0x008d
        L_0x0329:
            r12 = r18
            r13 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzm(r9, r4, r2, r13)
            goto L_0x008d
        L_0x033b:
            r12 = r18
            r13 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzi(r9, r4, r2, r13)
            goto L_0x008d
        L_0x034d:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r4, r2)
            goto L_0x008d
        L_0x035e:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgx r5 = r0.zzbx(r12)
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r4, r2, r5)
            goto L_0x008d
        L_0x0373:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r4, r2)
            goto L_0x008d
        L_0x0384:
            r12 = r18
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            r14 = 0
            com.google.android.gms.internal.measurement.zzgz.zzn(r9, r4, r2, r14)
            goto L_0x0523
        L_0x0396:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzk(r9, r4, r2, r14)
            goto L_0x0523
        L_0x03a8:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzf(r9, r4, r2, r14)
            goto L_0x0523
        L_0x03ba:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzh(r9, r4, r2, r14)
            goto L_0x0523
        L_0x03cc:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzd(r9, r4, r2, r14)
            goto L_0x0523
        L_0x03de:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzc(r9, r4, r2, r14)
            goto L_0x0523
        L_0x03f0:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zzb(r9, r4, r2, r14)
            goto L_0x0523
        L_0x0402:
            r12 = r18
            r14 = 0
            int[] r9 = r0.zzakj
            r9 = r9[r12]
            java.lang.Object r4 = r8.getObject(r1, r4)
            java.util.List r4 = (java.util.List) r4
            com.google.android.gms.internal.measurement.zzgz.zza(r9, r4, r2, r14)
            goto L_0x0523
        L_0x0414:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            java.lang.Object r4 = r8.getObject(r1, r4)
            com.google.android.gms.internal.measurement.zzgx r5 = r0.zzbx(r12)
            r2.zzb(r13, r4, r5)
            goto L_0x0523
        L_0x0427:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            long r4 = r8.getLong(r1, r4)
            r2.zzb(r13, r4)
            goto L_0x0523
        L_0x0436:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            int r4 = r8.getInt(r1, r4)
            r2.zze(r13, r4)
            goto L_0x0523
        L_0x0445:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            long r4 = r8.getLong(r1, r4)
            r2.zzj(r13, r4)
            goto L_0x0523
        L_0x0454:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            int r4 = r8.getInt(r1, r4)
            r2.zzm(r13, r4)
            goto L_0x0523
        L_0x0463:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            int r4 = r8.getInt(r1, r4)
            r2.zzn(r13, r4)
            goto L_0x0523
        L_0x0472:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            int r4 = r8.getInt(r1, r4)
            r2.zzd(r13, r4)
            goto L_0x0523
        L_0x0481:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            java.lang.Object r4 = r8.getObject(r1, r4)
            com.google.android.gms.internal.measurement.zzdp r4 = (com.google.android.gms.internal.measurement.zzdp) r4
            r2.zza(r13, r4)
            goto L_0x0523
        L_0x0492:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            java.lang.Object r4 = r8.getObject(r1, r4)
            com.google.android.gms.internal.measurement.zzgx r5 = r0.zzbx(r12)
            r2.zza(r13, r4, r5)
            goto L_0x0523
        L_0x04a5:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            java.lang.Object r4 = r8.getObject(r1, r4)
            zza(r13, r4, r2)
            goto L_0x0523
        L_0x04b4:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            boolean r4 = com.google.android.gms.internal.measurement.zzhv.zzm(r1, r4)
            r2.zzb(r13, r4)
            goto L_0x0523
        L_0x04c2:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            int r4 = r8.getInt(r1, r4)
            r2.zzf(r13, r4)
            goto L_0x0523
        L_0x04d0:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            long r4 = r8.getLong(r1, r4)
            r2.zzc(r13, r4)
            goto L_0x0523
        L_0x04de:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            int r4 = r8.getInt(r1, r4)
            r2.zzc(r13, r4)
            goto L_0x0523
        L_0x04ec:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            long r4 = r8.getLong(r1, r4)
            r2.zza(r13, r4)
            goto L_0x0523
        L_0x04fa:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            long r4 = r8.getLong(r1, r4)
            r2.zzi(r13, r4)
            goto L_0x0523
        L_0x0508:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            float r4 = com.google.android.gms.internal.measurement.zzhv.zzn(r1, r4)
            r2.zza(r13, r4)
            goto L_0x0523
        L_0x0516:
            r12 = r18
            r14 = 0
            r9 = r9 & r11
            if (r9 == 0) goto L_0x0523
            double r4 = com.google.android.gms.internal.measurement.zzhv.zzo(r1, r4)
            r2.zza(r13, r4)
        L_0x0523:
            int r5 = r12 + 3
            goto L_0x002e
        L_0x0527:
            if (r10 == 0) goto L_0x053e
            com.google.android.gms.internal.measurement.zzen<?> r4 = r0.zzaky
            r4.zza(r2, r10)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x053c
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r10 = r4
            goto L_0x0527
        L_0x053c:
            r10 = 0
            goto L_0x0527
        L_0x053e:
            com.google.android.gms.internal.measurement.zzhp<?, ?> r3 = r0.zzakx
            zza(r3, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zzim):void");
    }

    private final <K, V> void zza(zzim zzim, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzim.zza(i, this.zzakz.zzr(zzby(i2)), this.zzakz.zzn(obj));
        }
    }

    private static <UT, UB> void zza(zzhp<UT, UB> zzhp, T t, zzim zzim) throws IOException {
        zzhp.zza(zzhp.zzx(t), zzim);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public final void zza(T r13, com.google.android.gms.internal.measurement.zzgy r14, com.google.android.gms.internal.measurement.zzel r15) throws java.io.IOException {
        /*
            r12 = this;
            if (r15 == 0) goto L_0x05e0
            com.google.android.gms.internal.measurement.zzhp<?, ?> r7 = r12.zzakx
            com.google.android.gms.internal.measurement.zzen<?> r8 = r12.zzaky
            r9 = 0
            r0 = r9
            r10 = r0
        L_0x0009:
            int r1 = r14.zzsy()     // Catch:{ all -> 0x05c8 }
            int r2 = r12.zzcd(r1)     // Catch:{ all -> 0x05c8 }
            if (r2 >= 0) goto L_0x0079
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r1 != r2) goto L_0x002f
            int r14 = r12.zzakt
        L_0x001a:
            int r15 = r12.zzaku
            if (r14 >= r15) goto L_0x0029
            int[] r15 = r12.zzaks
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x001a
        L_0x0029:
            if (r10 == 0) goto L_0x002e
            r7.zzf(r13, r10)
        L_0x002e:
            return
        L_0x002f:
            boolean r2 = r12.zzako     // Catch:{ all -> 0x05c8 }
            if (r2 != 0) goto L_0x0035
            r2 = r9
            goto L_0x003c
        L_0x0035:
            com.google.android.gms.internal.measurement.zzgi r2 = r12.zzakn     // Catch:{ all -> 0x05c8 }
            java.lang.Object r1 = r8.zza(r15, r2, r1)     // Catch:{ all -> 0x05c8 }
            r2 = r1
        L_0x003c:
            if (r2 == 0) goto L_0x0052
            if (r0 != 0) goto L_0x0044
            com.google.android.gms.internal.measurement.zzeo r0 = r8.zzi(r13)     // Catch:{ all -> 0x05c8 }
        L_0x0044:
            r11 = r0
            r0 = r8
            r1 = r14
            r3 = r15
            r4 = r11
            r5 = r10
            r6 = r7
            java.lang.Object r0 = r0.zza(r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x05c8 }
            r10 = r0
            r0 = r11
            goto L_0x0009
        L_0x0052:
            r7.zza(r14)     // Catch:{ all -> 0x05c8 }
            if (r10 != 0) goto L_0x005c
            java.lang.Object r1 = r7.zzy(r13)     // Catch:{ all -> 0x05c8 }
            r10 = r1
        L_0x005c:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ all -> 0x05c8 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzakt
        L_0x0064:
            int r15 = r12.zzaku
            if (r14 >= r15) goto L_0x0073
            int[] r15 = r12.zzaks
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x0064
        L_0x0073:
            if (r10 == 0) goto L_0x0078
            r7.zzf(r13, r10)
        L_0x0078:
            return
        L_0x0079:
            int r3 = r12.zzca(r2)     // Catch:{ all -> 0x05c8 }
            r4 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r3
            int r4 = r4 >>> 20
            r5 = 1048575(0xfffff, float:1.469367E-39)
            switch(r4) {
                case 0: goto L_0x0574;
                case 1: goto L_0x0565;
                case 2: goto L_0x0556;
                case 3: goto L_0x0547;
                case 4: goto L_0x0538;
                case 5: goto L_0x0529;
                case 6: goto L_0x051a;
                case 7: goto L_0x050b;
                case 8: goto L_0x0503;
                case 9: goto L_0x04d2;
                case 10: goto L_0x04c3;
                case 11: goto L_0x04b4;
                case 12: goto L_0x0492;
                case 13: goto L_0x0483;
                case 14: goto L_0x0474;
                case 15: goto L_0x0465;
                case 16: goto L_0x0456;
                case 17: goto L_0x0425;
                case 18: goto L_0x0417;
                case 19: goto L_0x0409;
                case 20: goto L_0x03fb;
                case 21: goto L_0x03ed;
                case 22: goto L_0x03df;
                case 23: goto L_0x03d1;
                case 24: goto L_0x03c3;
                case 25: goto L_0x03b5;
                case 26: goto L_0x0393;
                case 27: goto L_0x0381;
                case 28: goto L_0x0373;
                case 29: goto L_0x0365;
                case 30: goto L_0x034f;
                case 31: goto L_0x0341;
                case 32: goto L_0x0333;
                case 33: goto L_0x0325;
                case 34: goto L_0x0317;
                case 35: goto L_0x0309;
                case 36: goto L_0x02fb;
                case 37: goto L_0x02ed;
                case 38: goto L_0x02df;
                case 39: goto L_0x02d1;
                case 40: goto L_0x02c3;
                case 41: goto L_0x02b5;
                case 42: goto L_0x02a7;
                case 43: goto L_0x0299;
                case 44: goto L_0x0284;
                case 45: goto L_0x0276;
                case 46: goto L_0x0268;
                case 47: goto L_0x025a;
                case 48: goto L_0x024c;
                case 49: goto L_0x023a;
                case 50: goto L_0x01f8;
                case 51: goto L_0x01e6;
                case 52: goto L_0x01d4;
                case 53: goto L_0x01c2;
                case 54: goto L_0x01b0;
                case 55: goto L_0x019e;
                case 56: goto L_0x018c;
                case 57: goto L_0x017a;
                case 58: goto L_0x0168;
                case 59: goto L_0x0160;
                case 60: goto L_0x012f;
                case 61: goto L_0x0121;
                case 62: goto L_0x010f;
                case 63: goto L_0x00ea;
                case 64: goto L_0x00d8;
                case 65: goto L_0x00c6;
                case 66: goto L_0x00b4;
                case 67: goto L_0x00a2;
                case 68: goto L_0x0090;
                default: goto L_0x0088;
            }
        L_0x0088:
            if (r10 != 0) goto L_0x0584
            java.lang.Object r1 = r7.zzwp()     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0583
        L_0x0090:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r5 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r5 = r14.zzb(r5, r15)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x00a2:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsu()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x00b4:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            int r5 = r14.zzst()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x00c6:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzss()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x00d8:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            int r5 = r14.zzsr()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x00ea:
            int r4 = r14.zzsq()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfe r6 = r12.zzbz(r2)     // Catch:{ zzfh -> 0x05a1 }
            if (r6 == 0) goto L_0x0101
            boolean r6 = r6.zzg(r4)     // Catch:{ zzfh -> 0x05a1 }
            if (r6 == 0) goto L_0x00fb
            goto L_0x0101
        L_0x00fb:
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzgz.zza(r1, r4, r10, r7)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0362
        L_0x0101:
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r5, r3)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x010f:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            int r5 = r14.zzsp()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0121:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzdp r5 = r14.zzso()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x012f:
            boolean r4 = r12.zza(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            if (r4 == 0) goto L_0x014b
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzhv.zzp(r13, r3)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r6 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r6 = r14.zza(r6, r15)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzez.zza(r5, r6)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x015b
        L_0x014b:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r5 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r5 = r14.zza(r5, r15)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
        L_0x015b:
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0160:
            r12.zza(r13, r3, r14)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0168:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            boolean r5 = r14.zzsm()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x017a:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            int r5 = r14.zzsl()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x018c:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsk()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x019e:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            int r5 = r14.zzsj()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x01b0:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsh()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x01c2:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsi()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x01d4:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            float r5 = r14.readFloat()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Float r5 = java.lang.Float.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x01e6:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            double r5 = r14.readDouble()     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Double r5 = java.lang.Double.valueOf(r5)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x01f8:
            java.lang.Object r1 = r12.zzby(r2)     // Catch:{ zzfh -> 0x05a1 }
            int r2 = r12.zzca(r2)     // Catch:{ zzfh -> 0x05a1 }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzhv.zzp(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            if (r4 != 0) goto L_0x0212
            com.google.android.gms.internal.measurement.zzgb r4 = r12.zzakz     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r4 = r4.zzq(r1)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r2, r4)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0229
        L_0x0212:
            com.google.android.gms.internal.measurement.zzgb r5 = r12.zzakz     // Catch:{ zzfh -> 0x05a1 }
            boolean r5 = r5.zzo(r4)     // Catch:{ zzfh -> 0x05a1 }
            if (r5 == 0) goto L_0x0229
            com.google.android.gms.internal.measurement.zzgb r5 = r12.zzakz     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r5 = r5.zzq(r1)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgb r6 = r12.zzakz     // Catch:{ zzfh -> 0x05a1 }
            r6.zzb(r5, r4)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r2, r5)     // Catch:{ zzfh -> 0x05a1 }
            r4 = r5
        L_0x0229:
            com.google.android.gms.internal.measurement.zzgb r2 = r12.zzakz     // Catch:{ zzfh -> 0x05a1 }
            java.util.Map r2 = r2.zzm(r4)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgb r3 = r12.zzakz     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfz r1 = r3.zzr(r1)     // Catch:{ zzfh -> 0x05a1 }
            r14.zza(r2, r1, r15)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x023a:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r1 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfs r2 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r2 = r2.zza(r13, r3)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzb(r2, r1, r15)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x024c:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzt(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x025a:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzs(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0268:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzr(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0276:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzq(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0284:
            com.google.android.gms.internal.measurement.zzfs r4 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r3 = r4.zza(r13, r5)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzp(r3)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfe r2 = r12.zzbz(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzgz.zza(r1, r3, r2, r10, r7)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0362
        L_0x0299:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzo(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02a7:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzl(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02b5:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzk(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02c3:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzj(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02d1:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzi(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02df:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzg(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02ed:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzh(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x02fb:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzf(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0309:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zze(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0317:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzt(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0325:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzs(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0333:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzr(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0341:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzq(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x034f:
            com.google.android.gms.internal.measurement.zzfs r4 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r3 = r4.zza(r13, r5)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzp(r3)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfe r2 = r12.zzbz(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzgz.zza(r1, r3, r2, r10, r7)     // Catch:{ zzfh -> 0x05a1 }
        L_0x0362:
            r10 = r1
            goto L_0x0009
        L_0x0365:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzo(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0373:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzn(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0381:
            com.google.android.gms.internal.measurement.zzgx r1 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfs r4 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r2 = r4.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zza(r2, r1, r15)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0393:
            boolean r1 = zzcc(r3)     // Catch:{ zzfh -> 0x05a1 }
            if (r1 == 0) goto L_0x03a7
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzm(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03a7:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.readStringList(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03b5:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzl(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03c3:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzk(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03d1:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzj(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03df:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzi(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03ed:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzg(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x03fb:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzh(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0409:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zzf(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0417:
            com.google.android.gms.internal.measurement.zzfs r1 = r12.zzakw     // Catch:{ zzfh -> 0x05a1 }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfh -> 0x05a1 }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            r14.zze(r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0425:
            boolean r1 = r12.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            if (r1 == 0) goto L_0x0443
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzhv.zzp(r13, r3)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r2 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r2 = r14.zzb(r2, r15)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzez.zza(r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0443:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r1 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = r14.zzb(r1, r15)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0456:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsu()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0465:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            int r1 = r14.zzst()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zzb(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0474:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzss()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0483:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            int r1 = r14.zzsr()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zzb(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0492:
            int r4 = r14.zzsq()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzfe r6 = r12.zzbz(r2)     // Catch:{ zzfh -> 0x05a1 }
            if (r6 == 0) goto L_0x04a9
            boolean r6 = r6.zzg(r4)     // Catch:{ zzfh -> 0x05a1 }
            if (r6 == 0) goto L_0x04a3
            goto L_0x04a9
        L_0x04a3:
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzgz.zza(r1, r4, r10, r7)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0362
        L_0x04a9:
            r1 = r3 & r5
            long r5 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zzb(r13, r5, r4)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x04b4:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            int r1 = r14.zzsp()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zzb(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x04c3:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzdp r1 = r14.zzso()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x04d2:
            boolean r1 = r12.zza(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            if (r1 == 0) goto L_0x04f0
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzhv.zzp(r13, r3)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r2 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r2 = r14.zza(r2, r15)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzez.zza(r1, r2)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x04f0:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzgx r1 = r12.zzbx(r2)     // Catch:{ zzfh -> 0x05a1 }
            java.lang.Object r1 = r14.zza(r1, r15)     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0503:
            r12.zza(r13, r3, r14)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x050b:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            boolean r1 = r14.zzsm()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x051a:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            int r1 = r14.zzsl()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zzb(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0529:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsk()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0538:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            int r1 = r14.zzsj()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zzb(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0547:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsh()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0556:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            long r5 = r14.zzsi()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0565:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            float r1 = r14.readFloat()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r1)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0574:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfh -> 0x05a1 }
            double r5 = r14.readDouble()     // Catch:{ zzfh -> 0x05a1 }
            com.google.android.gms.internal.measurement.zzhv.zza(r13, r3, r5)     // Catch:{ zzfh -> 0x05a1 }
            r12.zzb(r13, r2)     // Catch:{ zzfh -> 0x05a1 }
            goto L_0x0009
        L_0x0583:
            r10 = r1
        L_0x0584:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ zzfh -> 0x05a1 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzakt
        L_0x058c:
            int r15 = r12.zzaku
            if (r14 >= r15) goto L_0x059b
            int[] r15 = r12.zzaks
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x058c
        L_0x059b:
            if (r10 == 0) goto L_0x05a0
            r7.zzf(r13, r10)
        L_0x05a0:
            return
        L_0x05a1:
            r7.zza(r14)     // Catch:{ all -> 0x05c8 }
            if (r10 != 0) goto L_0x05ab
            java.lang.Object r1 = r7.zzy(r13)     // Catch:{ all -> 0x05c8 }
            r10 = r1
        L_0x05ab:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ all -> 0x05c8 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzakt
        L_0x05b3:
            int r15 = r12.zzaku
            if (r14 >= r15) goto L_0x05c2
            int[] r15 = r12.zzaks
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x05b3
        L_0x05c2:
            if (r10 == 0) goto L_0x05c7
            r7.zzf(r13, r10)
        L_0x05c7:
            return
        L_0x05c8:
            r14 = move-exception
            int r15 = r12.zzakt
        L_0x05cb:
            int r0 = r12.zzaku
            if (r15 >= r0) goto L_0x05da
            int[] r0 = r12.zzaks
            r0 = r0[r15]
            java.lang.Object r10 = r12.zza(r13, r0, r10, r7)
            int r15 = r15 + 1
            goto L_0x05cb
        L_0x05da:
            if (r10 == 0) goto L_0x05df
            r7.zzf(r13, r10)
        L_0x05df:
            throw r14
        L_0x05e0:
            java.lang.NullPointerException r13 = new java.lang.NullPointerException
            r13.<init>()
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzgy, com.google.android.gms.internal.measurement.zzel):void");
    }

    private static zzhs zzu(Object obj) {
        zzey zzey = (zzey) obj;
        zzhs zzhs = zzey.zzahz;
        if (zzhs != zzhs.zzwq()) {
            return zzhs;
        }
        zzhs zzwr = zzhs.zzwr();
        zzey.zzahz = zzwr;
        return zzwr;
    }

    private static int zza(byte[] bArr, int i, int i2, zzig zzig, Class<?> cls, zzdk zzdk) throws IOException {
        switch (zzgl.zzaee[zzig.ordinal()]) {
            case 1:
                int zzb = zzdl.zzb(bArr, i, zzdk);
                zzdk.zzadc = Boolean.valueOf(zzdk.zzadb != 0);
                return zzb;
            case 2:
                return zzdl.zze(bArr, i, zzdk);
            case 3:
                zzdk.zzadc = Double.valueOf(zzdl.zzc(bArr, i));
                return i + 8;
            case 4:
            case 5:
                zzdk.zzadc = Integer.valueOf(zzdl.zza(bArr, i));
                return i + 4;
            case 6:
            case 7:
                zzdk.zzadc = Long.valueOf(zzdl.zzb(bArr, i));
                return i + 8;
            case 8:
                zzdk.zzadc = Float.valueOf(zzdl.zzd(bArr, i));
                return i + 4;
            case 9:
            case 10:
            case 11:
                int zza = zzdl.zza(bArr, i, zzdk);
                zzdk.zzadc = Integer.valueOf(zzdk.zzada);
                return zza;
            case 12:
            case 13:
                int zzb2 = zzdl.zzb(bArr, i, zzdk);
                zzdk.zzadc = Long.valueOf(zzdk.zzadb);
                return zzb2;
            case 14:
                return zzdl.zza(zzgt.zzvy().zzf(cls), bArr, i, i2, zzdk);
            case 15:
                int zza2 = zzdl.zza(bArr, i, zzdk);
                zzdk.zzadc = Integer.valueOf(zzeb.zzaz(zzdk.zzada));
                return zza2;
            case 16:
                int zzb3 = zzdl.zzb(bArr, i, zzdk);
                zzdk.zzadc = Long.valueOf(zzeb.zzbm(zzdk.zzadb));
                return zzb3;
            case 17:
                return zzdl.zzd(bArr, i, zzdk);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x023f  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0433 A[SYNTHETIC] */
    private final int zza(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, com.google.android.gms.internal.measurement.zzdk r30) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = com.google.android.gms.internal.measurement.zzgm.zzaki
            java.lang.Object r11 = r11.getObject(r1, r9)
            com.google.android.gms.internal.measurement.zzff r11 = (com.google.android.gms.internal.measurement.zzff) r11
            boolean r12 = r11.zzrx()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.google.android.gms.internal.measurement.zzff r11 = r11.zzap(r12)
            sun.misc.Unsafe r12 = com.google.android.gms.internal.measurement.zzgm.zzaki
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r14 = 0
            r10 = 2
            switch(r27) {
                case 18: goto L_0x03f2;
                case 19: goto L_0x03b2;
                case 20: goto L_0x0371;
                case 21: goto L_0x0371;
                case 22: goto L_0x0357;
                case 23: goto L_0x0316;
                case 24: goto L_0x02d5;
                case 25: goto L_0x027e;
                case 26: goto L_0x01c4;
                case 27: goto L_0x01aa;
                case 28: goto L_0x0151;
                case 29: goto L_0x0357;
                case 30: goto L_0x0119;
                case 31: goto L_0x02d5;
                case 32: goto L_0x0316;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03f2;
                case 36: goto L_0x03b2;
                case 37: goto L_0x0371;
                case 38: goto L_0x0371;
                case 39: goto L_0x0357;
                case 40: goto L_0x0316;
                case 41: goto L_0x02d5;
                case 42: goto L_0x027e;
                case 43: goto L_0x0357;
                case 44: goto L_0x0119;
                case 45: goto L_0x02d5;
                case 46: goto L_0x0316;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x003f;
                default: goto L_0x003d;
            }
        L_0x003d:
            goto L_0x0432
        L_0x003f:
            r1 = 3
            if (r6 != r1) goto L_0x0432
            com.google.android.gms.internal.measurement.zzgx r1 = r0.zzbx(r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.zzadc
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0432
            int r8 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r9 = r7.zzada
            if (r2 != r9) goto L_0x0432
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.zzadc
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r10) goto L_0x00a3
            com.google.android.gms.internal.measurement.zzfw r11 = (com.google.android.gms.internal.measurement.zzfw) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r1, r7)
            long r4 = r7.zzadb
            long r4 = com.google.android.gms.internal.measurement.zzeb.zzbm(r4)
            r11.zzby(r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0433
        L_0x009e:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0432
            com.google.android.gms.internal.measurement.zzfw r11 = (com.google.android.gms.internal.measurement.zzfw) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r4, r7)
            long r8 = r7.zzadb
            long r8 = com.google.android.gms.internal.measurement.zzeb.zzbm(r8)
            r11.zzby(r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r4, r7)
            long r8 = r7.zzadb
            long r8 = com.google.android.gms.internal.measurement.zzeb.zzbm(r8)
            r11.zzby(r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r10) goto L_0x00f0
            com.google.android.gms.internal.measurement.zzfa r11 = (com.google.android.gms.internal.measurement.zzfa) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r4 = r7.zzada
            int r4 = com.google.android.gms.internal.measurement.zzeb.zzaz(r4)
            r11.zzbu(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0433
        L_0x00eb:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0432
            com.google.android.gms.internal.measurement.zzfa r11 = (com.google.android.gms.internal.measurement.zzfa) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            int r4 = com.google.android.gms.internal.measurement.zzeb.zzaz(r4)
            r11.zzbu(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            int r4 = com.google.android.gms.internal.measurement.zzeb.zzaz(r4)
            r11.zzbu(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r10) goto L_0x0120
            int r2 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r11, r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0432
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = com.google.android.gms.internal.measurement.zzdl.zza(r2, r3, r4, r5, r6, r7)
        L_0x0131:
            com.google.android.gms.internal.measurement.zzey r1 = (com.google.android.gms.internal.measurement.zzey) r1
            com.google.android.gms.internal.measurement.zzhs r3 = r1.zzahz
            com.google.android.gms.internal.measurement.zzhs r4 = com.google.android.gms.internal.measurement.zzhs.zzwq()
            if (r3 != r4) goto L_0x013c
            r3 = 0
        L_0x013c:
            com.google.android.gms.internal.measurement.zzfe r4 = r0.zzbz(r8)
            com.google.android.gms.internal.measurement.zzhp<?, ?> r5 = r0.zzakx
            r6 = r22
            java.lang.Object r3 = com.google.android.gms.internal.measurement.zzgz.zza(r6, r11, r4, r3, r5)
            com.google.android.gms.internal.measurement.zzhs r3 = (com.google.android.gms.internal.measurement.zzhs) r3
            if (r3 == 0) goto L_0x014e
            r1.zzahz = r3
        L_0x014e:
            r1 = r2
            goto L_0x0433
        L_0x0151:
            if (r6 != r10) goto L_0x0432
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            if (r4 < 0) goto L_0x01a5
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x01a0
            if (r4 != 0) goto L_0x0167
            com.google.android.gms.internal.measurement.zzdp r4 = com.google.android.gms.internal.measurement.zzdp.zzadh
            r11.add(r4)
            goto L_0x016f
        L_0x0167:
            com.google.android.gms.internal.measurement.zzdp r6 = com.google.android.gms.internal.measurement.zzdp.zzb(r3, r1, r4)
            r11.add(r6)
            int r1 = r1 + r4
        L_0x016f:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            if (r4 < 0) goto L_0x019b
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0196
            if (r4 != 0) goto L_0x018d
            com.google.android.gms.internal.measurement.zzdp r4 = com.google.android.gms.internal.measurement.zzdp.zzadh
            r11.add(r4)
            goto L_0x016f
        L_0x018d:
            com.google.android.gms.internal.measurement.zzdp r6 = com.google.android.gms.internal.measurement.zzdp.zzb(r3, r1, r4)
            r11.add(r6)
            int r1 = r1 + r4
            goto L_0x016f
        L_0x0196:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x019b:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzuu()
            throw r1
        L_0x01a0:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x01a5:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzuu()
            throw r1
        L_0x01aa:
            if (r6 != r10) goto L_0x0432
            com.google.android.gms.internal.measurement.zzgx r1 = r0.zzbx(r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0433
        L_0x01c4:
            if (r6 != r10) goto L_0x0432
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            int r1 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r1 != 0) goto L_0x021a
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            if (r4 < 0) goto L_0x0215
            if (r4 != 0) goto L_0x01df
            java.lang.String r4 = ""
            r11.add(r4)
            goto L_0x01ea
        L_0x01df:
            java.lang.String r6 = new java.lang.String
            java.nio.charset.Charset r8 = com.google.android.gms.internal.measurement.zzez.UTF_8
            r6.<init>(r3, r1, r4, r8)
            r11.add(r6)
            int r1 = r1 + r4
        L_0x01ea:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            if (r4 < 0) goto L_0x0210
            if (r4 != 0) goto L_0x0204
            java.lang.String r4 = ""
            r11.add(r4)
            goto L_0x01ea
        L_0x0204:
            java.lang.String r6 = new java.lang.String
            java.nio.charset.Charset r8 = com.google.android.gms.internal.measurement.zzez.UTF_8
            r6.<init>(r3, r1, r4, r8)
            r11.add(r6)
            int r1 = r1 + r4
            goto L_0x01ea
        L_0x0210:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzuu()
            throw r1
        L_0x0215:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzuu()
            throw r1
        L_0x021a:
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            if (r4 < 0) goto L_0x0279
            if (r4 != 0) goto L_0x022a
            java.lang.String r4 = ""
            r11.add(r4)
            goto L_0x023d
        L_0x022a:
            int r6 = r1 + r4
            boolean r8 = com.google.android.gms.internal.measurement.zzhy.zzf(r3, r1, r6)
            if (r8 == 0) goto L_0x0274
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.measurement.zzez.UTF_8
            r8.<init>(r3, r1, r4, r9)
            r11.add(r8)
        L_0x023c:
            r1 = r6
        L_0x023d:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            if (r4 < 0) goto L_0x026f
            if (r4 != 0) goto L_0x0257
            java.lang.String r4 = ""
            r11.add(r4)
            goto L_0x023d
        L_0x0257:
            int r6 = r1 + r4
            boolean r8 = com.google.android.gms.internal.measurement.zzhy.zzf(r3, r1, r6)
            if (r8 == 0) goto L_0x026a
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.measurement.zzez.UTF_8
            r8.<init>(r3, r1, r4, r9)
            r11.add(r8)
            goto L_0x023c
        L_0x026a:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzvb()
            throw r1
        L_0x026f:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzuu()
            throw r1
        L_0x0274:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzvb()
            throw r1
        L_0x0279:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzuu()
            throw r1
        L_0x027e:
            r1 = 0
            if (r6 != r10) goto L_0x02a6
            com.google.android.gms.internal.measurement.zzdn r11 = (com.google.android.gms.internal.measurement.zzdn) r11
            int r2 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r4 = r7.zzada
            int r4 = r4 + r2
        L_0x028a:
            if (r2 >= r4) goto L_0x029d
            int r2 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r2, r7)
            long r5 = r7.zzadb
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0298
            r5 = 1
            goto L_0x0299
        L_0x0298:
            r5 = 0
        L_0x0299:
            r11.addBoolean(r5)
            goto L_0x028a
        L_0x029d:
            if (r2 != r4) goto L_0x02a1
            goto L_0x014e
        L_0x02a1:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x02a6:
            if (r6 != 0) goto L_0x0432
            com.google.android.gms.internal.measurement.zzdn r11 = (com.google.android.gms.internal.measurement.zzdn) r11
            int r4 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r4, r7)
            long r8 = r7.zzadb
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02b6
            r6 = 1
            goto L_0x02b7
        L_0x02b6:
            r6 = 0
        L_0x02b7:
            r11.addBoolean(r6)
        L_0x02ba:
            if (r4 >= r5) goto L_0x0432
            int r6 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r8 = r7.zzada
            if (r2 != r8) goto L_0x0432
            int r4 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r6, r7)
            long r8 = r7.zzadb
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02d0
            r6 = 1
            goto L_0x02d1
        L_0x02d0:
            r6 = 0
        L_0x02d1:
            r11.addBoolean(r6)
            goto L_0x02ba
        L_0x02d5:
            if (r6 != r10) goto L_0x02f5
            com.google.android.gms.internal.measurement.zzfa r11 = (com.google.android.gms.internal.measurement.zzfa) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x02e0:
            if (r1 >= r2) goto L_0x02ec
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1)
            r11.zzbu(r4)
            int r1 = r1 + 4
            goto L_0x02e0
        L_0x02ec:
            if (r1 != r2) goto L_0x02f0
            goto L_0x0433
        L_0x02f0:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x02f5:
            if (r6 != r9) goto L_0x0432
            com.google.android.gms.internal.measurement.zzfa r11 = (com.google.android.gms.internal.measurement.zzfa) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r18, r19)
            r11.zzbu(r1)
            int r1 = r4 + 4
        L_0x0302:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4)
            r11.zzbu(r1)
            int r1 = r4 + 4
            goto L_0x0302
        L_0x0316:
            if (r6 != r10) goto L_0x0336
            com.google.android.gms.internal.measurement.zzfw r11 = (com.google.android.gms.internal.measurement.zzfw) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x0321:
            if (r1 >= r2) goto L_0x032d
            long r4 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r1)
            r11.zzby(r4)
            int r1 = r1 + 8
            goto L_0x0321
        L_0x032d:
            if (r1 != r2) goto L_0x0331
            goto L_0x0433
        L_0x0331:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x0336:
            if (r6 != r13) goto L_0x0432
            com.google.android.gms.internal.measurement.zzfw r11 = (com.google.android.gms.internal.measurement.zzfw) r11
            long r8 = com.google.android.gms.internal.measurement.zzdl.zzb(r18, r19)
            r11.zzby(r8)
            int r1 = r4 + 8
        L_0x0343:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            long r8 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r4)
            r11.zzby(r8)
            int r1 = r4 + 8
            goto L_0x0343
        L_0x0357:
            if (r6 != r10) goto L_0x035f
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r11, r7)
            goto L_0x0433
        L_0x035f:
            if (r6 != 0) goto L_0x0432
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r21, r22, r23, r24, r25, r26)
            goto L_0x0433
        L_0x0371:
            if (r6 != r10) goto L_0x0391
            com.google.android.gms.internal.measurement.zzfw r11 = (com.google.android.gms.internal.measurement.zzfw) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x037c:
            if (r1 >= r2) goto L_0x0388
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r1, r7)
            long r4 = r7.zzadb
            r11.zzby(r4)
            goto L_0x037c
        L_0x0388:
            if (r1 != r2) goto L_0x038c
            goto L_0x0433
        L_0x038c:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x0391:
            if (r6 != 0) goto L_0x0432
            com.google.android.gms.internal.measurement.zzfw r11 = (com.google.android.gms.internal.measurement.zzfw) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r4, r7)
            long r8 = r7.zzadb
            r11.zzby(r8)
        L_0x039e:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r3, r4, r7)
            long r8 = r7.zzadb
            r11.zzby(r8)
            goto L_0x039e
        L_0x03b2:
            if (r6 != r10) goto L_0x03d1
            com.google.android.gms.internal.measurement.zzeu r11 = (com.google.android.gms.internal.measurement.zzeu) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x03bd:
            if (r1 >= r2) goto L_0x03c9
            float r4 = com.google.android.gms.internal.measurement.zzdl.zzd(r3, r1)
            r11.zzc(r4)
            int r1 = r1 + 4
            goto L_0x03bd
        L_0x03c9:
            if (r1 != r2) goto L_0x03cc
            goto L_0x0433
        L_0x03cc:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x03d1:
            if (r6 != r9) goto L_0x0432
            com.google.android.gms.internal.measurement.zzeu r11 = (com.google.android.gms.internal.measurement.zzeu) r11
            float r1 = com.google.android.gms.internal.measurement.zzdl.zzd(r18, r19)
            r11.zzc(r1)
            int r1 = r4 + 4
        L_0x03de:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            float r1 = com.google.android.gms.internal.measurement.zzdl.zzd(r3, r4)
            r11.zzc(r1)
            int r1 = r4 + 4
            goto L_0x03de
        L_0x03f2:
            if (r6 != r10) goto L_0x0411
            com.google.android.gms.internal.measurement.zzeh r11 = (com.google.android.gms.internal.measurement.zzeh) r11
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r4, r7)
            int r2 = r7.zzada
            int r2 = r2 + r1
        L_0x03fd:
            if (r1 >= r2) goto L_0x0409
            double r4 = com.google.android.gms.internal.measurement.zzdl.zzc(r3, r1)
            r11.zzf(r4)
            int r1 = r1 + 8
            goto L_0x03fd
        L_0x0409:
            if (r1 != r2) goto L_0x040c
            goto L_0x0433
        L_0x040c:
            com.google.android.gms.internal.measurement.zzfi r1 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r1
        L_0x0411:
            if (r6 != r13) goto L_0x0432
            com.google.android.gms.internal.measurement.zzeh r11 = (com.google.android.gms.internal.measurement.zzeh) r11
            double r8 = com.google.android.gms.internal.measurement.zzdl.zzc(r18, r19)
            r11.zzf(r8)
            int r1 = r4 + 8
        L_0x041e:
            if (r1 >= r5) goto L_0x0433
            int r4 = com.google.android.gms.internal.measurement.zzdl.zza(r3, r1, r7)
            int r6 = r7.zzada
            if (r2 != r6) goto L_0x0433
            double r8 = com.google.android.gms.internal.measurement.zzdl.zzc(r3, r4)
            r11.zzf(r8)
            int r1 = r4 + 8
            goto L_0x041e
        L_0x0432:
            r1 = r4
        L_0x0433:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.measurement.zzdk):int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final <K, V> int zza(T r8, byte[] r9, int r10, int r11, int r12, long r13, com.google.android.gms.internal.measurement.zzdk r15) throws java.io.IOException {
        /*
            r7 = this;
            sun.misc.Unsafe r0 = com.google.android.gms.internal.measurement.zzgm.zzaki
            java.lang.Object r12 = r7.zzby(r12)
            java.lang.Object r1 = r0.getObject(r8, r13)
            com.google.android.gms.internal.measurement.zzgb r2 = r7.zzakz
            boolean r2 = r2.zzo(r1)
            if (r2 == 0) goto L_0x0021
            com.google.android.gms.internal.measurement.zzgb r2 = r7.zzakz
            java.lang.Object r2 = r2.zzq(r12)
            com.google.android.gms.internal.measurement.zzgb r3 = r7.zzakz
            r3.zzb(r2, r1)
            r0.putObject(r8, r13, r2)
            r1 = r2
        L_0x0021:
            com.google.android.gms.internal.measurement.zzgb r8 = r7.zzakz
            com.google.android.gms.internal.measurement.zzfz r8 = r8.zzr(r12)
            com.google.android.gms.internal.measurement.zzgb r12 = r7.zzakz
            java.util.Map r12 = r12.zzm(r1)
            int r10 = com.google.android.gms.internal.measurement.zzdl.zza(r9, r10, r15)
            int r13 = r15.zzada
            if (r13 < 0) goto L_0x0094
            int r14 = r11 - r10
            if (r13 > r14) goto L_0x0094
            int r13 = r13 + r10
            K r14 = r8.zzakc
            V r0 = r8.zzaba
        L_0x003e:
            if (r10 >= r13) goto L_0x0089
            int r1 = r10 + 1
            byte r10 = r9[r10]
            if (r10 >= 0) goto L_0x004c
            int r1 = com.google.android.gms.internal.measurement.zzdl.zza(r10, r9, r1, r15)
            int r10 = r15.zzada
        L_0x004c:
            r2 = r1
            int r1 = r10 >>> 3
            r3 = r10 & 7
            switch(r1) {
                case 1: goto L_0x006f;
                case 2: goto L_0x0055;
                default: goto L_0x0054;
            }
        L_0x0054:
            goto L_0x0084
        L_0x0055:
            com.google.android.gms.internal.measurement.zzig r1 = r8.zzakd
            int r1 = r1.zzxa()
            if (r3 != r1) goto L_0x0084
            com.google.android.gms.internal.measurement.zzig r4 = r8.zzakd
            V r10 = r8.zzaba
            java.lang.Class r5 = r10.getClass()
            r1 = r9
            r3 = r11
            r6 = r15
            int r10 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r0 = r15.zzadc
            goto L_0x003e
        L_0x006f:
            com.google.android.gms.internal.measurement.zzig r1 = r8.zzakb
            int r1 = r1.zzxa()
            if (r3 != r1) goto L_0x0084
            com.google.android.gms.internal.measurement.zzig r4 = r8.zzakb
            r5 = 0
            r1 = r9
            r3 = r11
            r6 = r15
            int r10 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r14 = r15.zzadc
            goto L_0x003e
        L_0x0084:
            int r10 = com.google.android.gms.internal.measurement.zzdl.zza(r10, r9, r2, r11, r15)
            goto L_0x003e
        L_0x0089:
            if (r10 != r13) goto L_0x008f
            r12.put(r14, r0)
            return r13
        L_0x008f:
            com.google.android.gms.internal.measurement.zzfi r8 = com.google.android.gms.internal.measurement.zzfi.zzva()
            throw r8
        L_0x0094:
            com.google.android.gms.internal.measurement.zzfi r8 = com.google.android.gms.internal.measurement.zzfi.zzut()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, byte[], int, int, int, long, com.google.android.gms.internal.measurement.zzdk):int");
    }

    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzdk zzdk) throws IOException {
        int i9;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i;
        int i11 = i3;
        int i12 = i4;
        int i13 = i5;
        long j2 = j;
        int i14 = i8;
        zzdk zzdk2 = zzdk;
        Unsafe unsafe = zzaki;
        long j3 = (long) (this.zzakj[i14 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Double.valueOf(zzdl.zzc(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 52:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Float.valueOf(zzdl.zzd(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 53:
            case 54:
                if (i13 == 0) {
                    i9 = zzdl.zzb(bArr2, i10, zzdk2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzdk2.zzadb));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 55:
            case 62:
                if (i13 == 0) {
                    i9 = zzdl.zza(bArr2, i10, zzdk2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzdk2.zzada));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 56:
            case 65:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Long.valueOf(zzdl.zzb(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 57:
            case 64:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Integer.valueOf(zzdl.zza(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 58:
                if (i13 == 0) {
                    i9 = zzdl.zzb(bArr2, i10, zzdk2);
                    unsafe.putObject(t2, j2, Boolean.valueOf(zzdk2.zzadb != 0));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 59:
                if (i13 == 2) {
                    int zza = zzdl.zza(bArr2, i10, zzdk2);
                    int i15 = zzdk2.zzada;
                    if (i15 == 0) {
                        unsafe.putObject(t2, j2, "");
                    } else if ((i6 & DriveFile.MODE_WRITE_ONLY) == 0 || zzhy.zzf(bArr2, zza, zza + i15)) {
                        unsafe.putObject(t2, j2, new String(bArr2, zza, i15, zzez.UTF_8));
                        zza += i15;
                    } else {
                        throw zzfi.zzvb();
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza;
                }
                return i10;
            case 60:
                if (i13 == 2) {
                    int zza2 = zzdl.zza(zzbx(i14), bArr2, i10, i2, zzdk2);
                    Object object = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j2, zzdk2.zzadc);
                    } else {
                        unsafe.putObject(t2, j2, zzez.zza(object, zzdk2.zzadc));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza2;
                }
                return i10;
            case 61:
                if (i13 == 2) {
                    i9 = zzdl.zze(bArr2, i10, zzdk2);
                    unsafe.putObject(t2, j2, zzdk2.zzadc);
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 63:
                if (i13 == 0) {
                    int zza3 = zzdl.zza(bArr2, i10, zzdk2);
                    int i16 = zzdk2.zzada;
                    zzfe zzbz = zzbz(i14);
                    if (zzbz == null || zzbz.zzg(i16)) {
                        unsafe.putObject(t2, j2, Integer.valueOf(i16));
                        i9 = zza3;
                        unsafe.putInt(t2, j3, i12);
                        return i9;
                    }
                    zzu(t).zzb(i11, Long.valueOf((long) i16));
                    return zza3;
                }
                return i10;
            case 66:
                if (i13 == 0) {
                    i9 = zzdl.zza(bArr2, i10, zzdk2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzeb.zzaz(zzdk2.zzada)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 67:
                if (i13 == 0) {
                    i9 = zzdl.zzb(bArr2, i10, zzdk2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzeb.zzbm(zzdk2.zzadb)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 68:
                if (i13 == 3) {
                    i9 = zzdl.zza(zzbx(i14), bArr, i, i2, (i11 & -8) | 4, zzdk);
                    Object object2 = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object2 == null) {
                        unsafe.putObject(t2, j2, zzdk2.zzadc);
                    } else {
                        unsafe.putObject(t2, j2, zzez.zza(object2, zzdk2.zzadc));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            default:
                return i10;
        }
    }

    private final zzgx zzbx(int i) {
        int i2 = (i / 3) << 1;
        zzgx zzgx = (zzgx) this.zzakk[i2];
        if (zzgx != null) {
            return zzgx;
        }
        zzgx zzf = zzgt.zzvy().zzf((Class) this.zzakk[i2 + 1]);
        this.zzakk[i2] = zzf;
        return zzf;
    }

    private final Object zzby(int i) {
        return this.zzakk[(i / 3) << 1];
    }

    private final zzfe zzbz(int i) {
        return (zzfe) this.zzakk[((i / 3) << 1) + 1];
    }

    /* JADX WARN: Type inference failed for: r35v0, types: [int] */
    /* JADX WARN: Type inference failed for: r3v27, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0354, code lost:
        if (r0 == r15) goto L_0x0356;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0358, code lost:
        r12 = r32;
        r2 = r18;
        r6 = r19;
        r7 = r24;
        r1 = r25;
        r10 = r28;
        r3 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x039f, code lost:
        if (r0 == r15) goto L_0x0356;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(java.lang.Object r31, byte[] r32, int r33, int r34, int r35, com.google.android.gms.internal.measurement.zzdk r36) throws java.io.IOException {
        /*
            r30 = this;
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            r9 = r36
            sun.misc.Unsafe r10 = com.google.android.gms.internal.measurement.zzgm.zzaki
            r16 = 0
            r0 = r33
            r1 = -1
            r2 = 0
            r3 = 0
            r6 = 0
            r7 = -1
        L_0x0017:
            if (r0 >= r13) goto L_0x045d
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0028
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r12, r3, r9)
            int r3 = r9.zzada
            r4 = r0
            r5 = r3
            goto L_0x002a
        L_0x0028:
            r5 = r0
            r4 = r3
        L_0x002a:
            int r3 = r5 >>> 3
            r0 = r5 & 7
            r8 = 3
            if (r3 <= r1) goto L_0x0039
            int r2 = r2 / r8
            int r1 = r15.zzp(r3, r2)
        L_0x0036:
            r2 = r1
            r1 = -1
            goto L_0x003e
        L_0x0039:
            int r1 = r15.zzcd(r3)
            goto L_0x0036
        L_0x003e:
            if (r2 != r1) goto L_0x004d
            r25 = r3
            r2 = r4
            r19 = r6
            r28 = r10
            r0 = r11
            r18 = 0
            r6 = r5
            goto L_0x03c7
        L_0x004d:
            int[] r1 = r15.zzakj
            int r18 = r2 + 1
            r1 = r1[r18]
            r18 = 267386880(0xff00000, float:2.3665827E-29)
            r18 = r1 & r18
            int r8 = r18 >>> 20
            r18 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r5
            r5 = r1 & r18
            long r11 = (long) r5
            r5 = 17
            r21 = r1
            if (r8 > r5) goto L_0x02c4
            int[] r5 = r15.zzakj
            int r22 = r2 + 2
            r5 = r5[r22]
            int r22 = r5 >>> 20
            r1 = 1
            int r22 = r1 << r22
            r5 = r5 & r18
            if (r5 == r7) goto L_0x008a
            r13 = -1
            if (r7 == r13) goto L_0x0080
            r24 = r2
            long r1 = (long) r7
            r10.putInt(r14, r1, r6)
            goto L_0x0082
        L_0x0080:
            r24 = r2
        L_0x0082:
            long r1 = (long) r5
            int r1 = r10.getInt(r14, r1)
            r6 = r1
            r7 = r5
            goto L_0x008d
        L_0x008a:
            r24 = r2
            r13 = -1
        L_0x008d:
            r1 = 5
            switch(r8) {
                case 0: goto L_0x0296;
                case 1: goto L_0x027c;
                case 2: goto L_0x025d;
                case 3: goto L_0x025d;
                case 4: goto L_0x0243;
                case 5: goto L_0x021d;
                case 6: goto L_0x01fa;
                case 7: goto L_0x01d6;
                case 8: goto L_0x01af;
                case 9: goto L_0x0176;
                case 10: goto L_0x015a;
                case 11: goto L_0x0243;
                case 12: goto L_0x0127;
                case 13: goto L_0x01fa;
                case 14: goto L_0x021d;
                case 15: goto L_0x010b;
                case 16: goto L_0x00e8;
                case 17: goto L_0x009e;
                default: goto L_0x0091;
            }
        L_0x0091:
            r25 = r3
            r11 = r4
            r13 = r20
            r8 = r24
        L_0x0098:
            r12 = r32
        L_0x009a:
            r17 = -1
            goto L_0x02ba
        L_0x009e:
            r2 = 3
            if (r0 != r2) goto L_0x00e0
            int r0 = r3 << 3
            r5 = r0 | 4
            r2 = r24
            com.google.android.gms.internal.measurement.zzgx r0 = r15.zzbx(r2)
            r1 = r32
            r8 = r2
            r2 = r4
            r4 = r3
            r3 = r34
            r25 = r4
            r4 = r5
            r13 = r20
            r5 = r36
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r1, r2, r3, r4, r5)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x00c7
            java.lang.Object r1 = r9.zzadc
            r10.putObject(r14, r11, r1)
            goto L_0x00d4
        L_0x00c7:
            java.lang.Object r1 = r10.getObject(r14, r11)
            java.lang.Object r2 = r9.zzadc
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzez.zza(r1, r2)
            r10.putObject(r14, r11, r1)
        L_0x00d4:
            r6 = r6 | r22
            r2 = r8
            r3 = r13
            r1 = r25
            r11 = r35
            r12 = r32
            goto L_0x02b6
        L_0x00e0:
            r25 = r3
            r13 = r20
            r8 = r24
            r11 = r4
            goto L_0x0098
        L_0x00e8:
            r25 = r3
            r13 = r20
            r8 = r24
            if (r0 != 0) goto L_0x0107
            r2 = r11
            r12 = r32
            int r11 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r4, r9)
            long r0 = r9.zzadb
            long r4 = com.google.android.gms.internal.measurement.zzeb.zzbm(r0)
            r0 = r10
            r1 = r31
            r0.putLong(r1, r2, r4)
            r6 = r6 | r22
            goto L_0x0279
        L_0x0107:
            r12 = r32
            goto L_0x0173
        L_0x010b:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r12 = r32
            if (r0 != 0) goto L_0x0173
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r4, r9)
            int r1 = r9.zzada
            int r1 = com.google.android.gms.internal.measurement.zzeb.zzaz(r1)
            r10.putInt(r14, r2, r1)
            r6 = r6 | r22
            goto L_0x02b0
        L_0x0127:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r12 = r32
            if (r0 != 0) goto L_0x0173
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r4, r9)
            int r1 = r9.zzada
            com.google.android.gms.internal.measurement.zzfe r4 = r15.zzbz(r8)
            if (r4 == 0) goto L_0x0153
            boolean r4 = r4.zzg(r1)
            if (r4 == 0) goto L_0x0145
            goto L_0x0153
        L_0x0145:
            com.google.android.gms.internal.measurement.zzhs r2 = zzu(r31)
            long r3 = (long) r1
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            r2.zzb(r13, r1)
            goto L_0x02b0
        L_0x0153:
            r10.putInt(r14, r2, r1)
            r6 = r6 | r22
            goto L_0x02b0
        L_0x015a:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r1 = 2
            r12 = r32
            if (r0 != r1) goto L_0x0173
            int r0 = com.google.android.gms.internal.measurement.zzdl.zze(r12, r4, r9)
            java.lang.Object r1 = r9.zzadc
            r10.putObject(r14, r2, r1)
            r6 = r6 | r22
            goto L_0x02b0
        L_0x0173:
            r11 = r4
            goto L_0x009a
        L_0x0176:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r1 = 2
            r12 = r32
            if (r0 != r1) goto L_0x01a9
            com.google.android.gms.internal.measurement.zzgx r0 = r15.zzbx(r8)
            r11 = r34
            r17 = -1
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r12, r4, r11, r9)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x0198
            java.lang.Object r1 = r9.zzadc
            r10.putObject(r14, r2, r1)
            goto L_0x01a5
        L_0x0198:
            java.lang.Object r1 = r10.getObject(r14, r2)
            java.lang.Object r4 = r9.zzadc
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzez.zza(r1, r4)
            r10.putObject(r14, r2, r1)
        L_0x01a5:
            r6 = r6 | r22
            goto L_0x0214
        L_0x01a9:
            r11 = r34
            r17 = -1
            goto L_0x0240
        L_0x01af:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r1 = 2
            r11 = r34
            r12 = r32
            r17 = -1
            if (r0 != r1) goto L_0x0240
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r21 & r0
            if (r0 != 0) goto L_0x01ca
            int r0 = com.google.android.gms.internal.measurement.zzdl.zzc(r12, r4, r9)
            goto L_0x01ce
        L_0x01ca:
            int r0 = com.google.android.gms.internal.measurement.zzdl.zzd(r12, r4, r9)
        L_0x01ce:
            java.lang.Object r1 = r9.zzadc
            r10.putObject(r14, r2, r1)
            r6 = r6 | r22
            goto L_0x0214
        L_0x01d6:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r11 = r34
            r12 = r32
            r17 = -1
            if (r0 != 0) goto L_0x0240
            int r0 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r4, r9)
            long r4 = r9.zzadb
            r18 = 0
            int r1 = (r4 > r18 ? 1 : (r4 == r18 ? 0 : -1))
            if (r1 == 0) goto L_0x01f3
            r1 = 1
            goto L_0x01f4
        L_0x01f3:
            r1 = 0
        L_0x01f4:
            com.google.android.gms.internal.measurement.zzhv.zza(r14, r2, r1)
            r6 = r6 | r22
            goto L_0x0214
        L_0x01fa:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r11 = r34
            r12 = r32
            r17 = -1
            if (r0 != r1) goto L_0x0240
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r4)
            r10.putInt(r14, r2, r0)
            int r0 = r4 + 4
            r6 = r6 | r22
        L_0x0214:
            r2 = r8
            r3 = r13
            r1 = r25
            r13 = r11
            r11 = r35
            goto L_0x0017
        L_0x021d:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r1 = 1
            r11 = r34
            r12 = r32
            r17 = -1
            if (r0 != r1) goto L_0x0240
            long r18 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r4)
            r0 = r10
            r1 = r31
            r11 = r4
            r4 = r18
            r0.putLong(r1, r2, r4)
            int r0 = r11 + 8
            r6 = r6 | r22
            goto L_0x02b0
        L_0x0240:
            r11 = r4
            goto L_0x02ba
        L_0x0243:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r12 = r32
            r17 = -1
            r11 = r4
            if (r0 != 0) goto L_0x02ba
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r11, r9)
            int r1 = r9.zzada
            r10.putInt(r14, r2, r1)
            r6 = r6 | r22
            goto L_0x02b0
        L_0x025d:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r12 = r32
            r17 = -1
            r11 = r4
            if (r0 != 0) goto L_0x02ba
            int r11 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r11, r9)
            long r4 = r9.zzadb
            r0 = r10
            r1 = r31
            r0.putLong(r1, r2, r4)
            r6 = r6 | r22
        L_0x0279:
            r2 = r8
            r0 = r11
            goto L_0x02b1
        L_0x027c:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r12 = r32
            r17 = -1
            r11 = r4
            if (r0 != r1) goto L_0x02ba
            float r0 = com.google.android.gms.internal.measurement.zzdl.zzd(r12, r11)
            com.google.android.gms.internal.measurement.zzhv.zza(r14, r2, r0)
            int r0 = r11 + 4
            r6 = r6 | r22
            goto L_0x02b0
        L_0x0296:
            r25 = r3
            r2 = r11
            r13 = r20
            r8 = r24
            r1 = 1
            r12 = r32
            r17 = -1
            r11 = r4
            if (r0 != r1) goto L_0x02ba
            double r0 = com.google.android.gms.internal.measurement.zzdl.zzc(r12, r11)
            com.google.android.gms.internal.measurement.zzhv.zza(r14, r2, r0)
            int r0 = r11 + 8
            r6 = r6 | r22
        L_0x02b0:
            r2 = r8
        L_0x02b1:
            r3 = r13
        L_0x02b2:
            r1 = r25
            r11 = r35
        L_0x02b6:
            r13 = r34
            goto L_0x0017
        L_0x02ba:
            r19 = r6
            r18 = r8
            r28 = r10
            r2 = r11
            r6 = r13
            goto L_0x03a7
        L_0x02c4:
            r5 = r2
            r25 = r3
            r2 = r11
            r13 = r20
            r12 = r32
            r17 = -1
            r11 = r4
            r1 = 27
            if (r8 != r1) goto L_0x031d
            r1 = 2
            if (r0 != r1) goto L_0x0310
            java.lang.Object r0 = r10.getObject(r14, r2)
            com.google.android.gms.internal.measurement.zzff r0 = (com.google.android.gms.internal.measurement.zzff) r0
            boolean r1 = r0.zzrx()
            if (r1 != 0) goto L_0x02f4
            int r1 = r0.size()
            if (r1 != 0) goto L_0x02eb
            r1 = 10
            goto L_0x02ed
        L_0x02eb:
            int r1 = r1 << 1
        L_0x02ed:
            com.google.android.gms.internal.measurement.zzff r0 = r0.zzap(r1)
            r10.putObject(r14, r2, r0)
        L_0x02f4:
            r8 = r0
            com.google.android.gms.internal.measurement.zzgx r0 = r15.zzbx(r5)
            r1 = r13
            r2 = r32
            r3 = r11
            r4 = r34
            r18 = r5
            r5 = r8
            r19 = r6
            r6 = r36
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r1, r2, r3, r4, r5, r6)
            r3 = r13
            r2 = r18
            r6 = r19
            goto L_0x02b2
        L_0x0310:
            r18 = r5
            r19 = r6
            r24 = r7
            r28 = r10
            r15 = r11
            r29 = r13
            goto L_0x03a2
        L_0x031d:
            r18 = r5
            r19 = r6
            r1 = 49
            if (r8 > r1) goto L_0x0372
            r1 = r21
            long r5 = (long) r1
            r4 = r0
            r0 = r30
            r1 = r31
            r20 = r2
            r2 = r32
            r3 = r11
            r26 = r4
            r4 = r34
            r22 = r5
            r5 = r13
            r6 = r25
            r24 = r7
            r7 = r26
            r27 = r8
            r8 = r18
            r28 = r10
            r9 = r22
            r15 = r11
            r11 = r27
            r29 = r13
            r12 = r20
            r14 = r36
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0358
        L_0x0356:
            r2 = r0
            goto L_0x03a3
        L_0x0358:
            r12 = r32
            r2 = r18
            r6 = r19
            r7 = r24
            r1 = r25
            r10 = r28
            r3 = r29
        L_0x0366:
            r9 = r36
            r11 = r35
            r13 = r34
            r14 = r31
            r15 = r30
            goto L_0x0017
        L_0x0372:
            r26 = r0
            r24 = r7
            r27 = r8
            r28 = r10
            r15 = r11
            r29 = r13
            r1 = r21
            r20 = r2
            r0 = 50
            r9 = r27
            if (r9 != r0) goto L_0x03aa
            r7 = r26
            r0 = 2
            if (r7 != r0) goto L_0x03a2
            r0 = r30
            r1 = r31
            r2 = r32
            r3 = r15
            r4 = r34
            r5 = r18
            r6 = r20
            r8 = r36
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x0358
            goto L_0x0356
        L_0x03a2:
            r2 = r15
        L_0x03a3:
            r7 = r24
            r6 = r29
        L_0x03a7:
            r0 = r35
            goto L_0x03c7
        L_0x03aa:
            r7 = r26
            r0 = r30
            r8 = r1
            r1 = r31
            r2 = r32
            r3 = r15
            r4 = r34
            r5 = r29
            r6 = r25
            r10 = r20
            r12 = r18
            r13 = r36
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x044b
            goto L_0x0356
        L_0x03c7:
            if (r6 != r0) goto L_0x03d7
            if (r0 != 0) goto L_0x03cc
            goto L_0x03d7
        L_0x03cc:
            r9 = r0
            r3 = r6
            r0 = r19
            r1 = -1
            r8 = r30
            r12 = r31
            goto L_0x046a
        L_0x03d7:
            r9 = r0
            r8 = r30
            boolean r0 = r8.zzako
            if (r0 == 0) goto L_0x0424
            r10 = r36
            com.google.android.gms.internal.measurement.zzel r0 = r10.zzadd
            com.google.android.gms.internal.measurement.zzel r1 = com.google.android.gms.internal.measurement.zzel.zztp()
            if (r0 == r1) goto L_0x0421
            com.google.android.gms.internal.measurement.zzgi r0 = r8.zzakn
            com.google.android.gms.internal.measurement.zzel r1 = r10.zzadd
            r11 = r25
            com.google.android.gms.internal.measurement.zzey$zze r0 = r1.zza(r0, r11)
            if (r0 != 0) goto L_0x0411
            com.google.android.gms.internal.measurement.zzhs r4 = zzu(r31)
            r0 = r6
            r1 = r32
            r3 = r34
            r5 = r36
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r1, r2, r3, r4, r5)
            r12 = r32
            r3 = r6
            r15 = r8
            r1 = r11
            r2 = r18
            r6 = r19
            r13 = r34
            r14 = r31
            goto L_0x0445
        L_0x0411:
            r12 = r31
            r0 = r12
            com.google.android.gms.internal.measurement.zzey$zzb r0 = (com.google.android.gms.internal.measurement.zzey.zzb) r0
            r0.zzuq()
            com.google.android.gms.internal.measurement.zzeo<java.lang.Object> r0 = r0.zzaic
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x0421:
            r11 = r25
            goto L_0x0428
        L_0x0424:
            r11 = r25
            r10 = r36
        L_0x0428:
            r12 = r31
            com.google.android.gms.internal.measurement.zzhs r4 = zzu(r31)
            r0 = r6
            r1 = r32
            r3 = r34
            r5 = r36
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r1, r2, r3, r4, r5)
            r3 = r6
            r15 = r8
            r1 = r11
            r14 = r12
            r2 = r18
            r6 = r19
            r13 = r34
            r12 = r32
        L_0x0445:
            r11 = r9
            r9 = r10
            r10 = r28
            goto L_0x0017
        L_0x044b:
            r11 = r25
            r6 = r29
            r12 = r32
            r3 = r6
            r1 = r11
            r2 = r18
            r6 = r19
            r7 = r24
            r10 = r28
            goto L_0x0366
        L_0x045d:
            r19 = r6
            r24 = r7
            r28 = r10
            r9 = r11
            r12 = r14
            r8 = r15
            r2 = r0
            r0 = r19
            r1 = -1
        L_0x046a:
            if (r7 == r1) goto L_0x0472
            long r4 = (long) r7
            r1 = r28
            r1.putInt(r12, r4, r0)
        L_0x0472:
            r0 = 0
            int r1 = r8.zzakt
        L_0x0475:
            int r4 = r8.zzaku
            if (r1 >= r4) goto L_0x0488
            int[] r4 = r8.zzaks
            r4 = r4[r1]
            com.google.android.gms.internal.measurement.zzhp<?, ?> r5 = r8.zzakx
            java.lang.Object r0 = r8.zza(r12, r4, r0, r5)
            com.google.android.gms.internal.measurement.zzhs r0 = (com.google.android.gms.internal.measurement.zzhs) r0
            int r1 = r1 + 1
            goto L_0x0475
        L_0x0488:
            if (r0 == 0) goto L_0x048f
            com.google.android.gms.internal.measurement.zzhp<?, ?> r1 = r8.zzakx
            r1.zzf(r12, r0)
        L_0x048f:
            if (r9 != 0) goto L_0x049b
            r0 = r34
            if (r2 != r0) goto L_0x0496
            goto L_0x04a1
        L_0x0496:
            com.google.android.gms.internal.measurement.zzfi r0 = com.google.android.gms.internal.measurement.zzfi.zzva()
            throw r0
        L_0x049b:
            r0 = r34
            if (r2 > r0) goto L_0x04a2
            if (r3 != r9) goto L_0x04a2
        L_0x04a1:
            return r2
        L_0x04a2:
            com.google.android.gms.internal.measurement.zzfi r0 = com.google.android.gms.internal.measurement.zzfi.zzva()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, byte[], int, int, int, com.google.android.gms.internal.measurement.zzdk):int");
    }

    /* JADX WARN: Type inference failed for: r3v13, types: [int] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhv.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzhv.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzhv.zza(java.lang.Object, long, float):void */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01e4, code lost:
        if (r0 == r15) goto L_0x01e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0212, code lost:
        if (r0 == r15) goto L_0x01e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0231, code lost:
        if (r0 == r15) goto L_0x01e6;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r29, byte[] r30, int r31, int r32, com.google.android.gms.internal.measurement.zzdk r33) throws java.io.IOException {
        /*
            r28 = this;
            r15 = r28
            r14 = r29
            r12 = r30
            r13 = r32
            r11 = r33
            boolean r0 = r15.zzakq
            if (r0 == 0) goto L_0x0260
            sun.misc.Unsafe r9 = com.google.android.gms.internal.measurement.zzgm.zzaki
            r10 = -1
            r16 = 0
            r0 = r31
            r1 = -1
            r2 = 0
        L_0x0017:
            if (r0 >= r13) goto L_0x0257
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0029
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r12, r3, r11)
            int r3 = r11.zzada
            r8 = r0
            r17 = r3
            goto L_0x002c
        L_0x0029:
            r17 = r0
            r8 = r3
        L_0x002c:
            int r7 = r17 >>> 3
            r6 = r17 & 7
            if (r7 <= r1) goto L_0x003a
            int r2 = r2 / 3
            int r0 = r15.zzp(r7, r2)
        L_0x0038:
            r4 = r0
            goto L_0x003f
        L_0x003a:
            int r0 = r15.zzcd(r7)
            goto L_0x0038
        L_0x003f:
            if (r4 != r10) goto L_0x004c
            r25 = r7
            r2 = r8
            r18 = r9
            r19 = 0
            r27 = -1
            goto L_0x0234
        L_0x004c:
            int[] r0 = r15.zzakj
            int r1 = r4 + 1
            r5 = r0[r1]
            r0 = 267386880(0xff00000, float:2.3665827E-29)
            r0 = r0 & r5
            int r3 = r0 >>> 20
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r5
            long r1 = (long) r0
            r0 = 17
            r10 = 2
            if (r3 > r0) goto L_0x0175
            r0 = 1
            switch(r3) {
                case 0: goto L_0x015a;
                case 1: goto L_0x014b;
                case 2: goto L_0x0139;
                case 3: goto L_0x0139;
                case 4: goto L_0x012b;
                case 5: goto L_0x0119;
                case 6: goto L_0x0108;
                case 7: goto L_0x00f2;
                case 8: goto L_0x00db;
                case 9: goto L_0x00ba;
                case 10: goto L_0x00ad;
                case 11: goto L_0x012b;
                case 12: goto L_0x009e;
                case 13: goto L_0x0108;
                case 14: goto L_0x0119;
                case 15: goto L_0x008b;
                case 16: goto L_0x0070;
                default: goto L_0x0065;
            }
        L_0x0065:
            r19 = r4
            r25 = r7
            r15 = r8
            r18 = r9
        L_0x006c:
            r27 = -1
            goto L_0x0215
        L_0x0070:
            if (r6 != 0) goto L_0x0065
            int r6 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r8, r11)
            r20 = r1
            long r0 = r11.zzadb
            long r22 = com.google.android.gms.internal.measurement.zzeb.zzbm(r0)
            r0 = r9
            r2 = r20
            r1 = r29
            r10 = r4
            r4 = r22
            r0.putLong(r1, r2, r4)
            goto L_0x0149
        L_0x008b:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x016c
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r8, r11)
            int r1 = r11.zzada
            int r1 = com.google.android.gms.internal.measurement.zzeb.zzaz(r1)
            r9.putInt(r14, r2, r1)
            goto L_0x0167
        L_0x009e:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x016c
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r8, r11)
            int r1 = r11.zzada
            r9.putInt(r14, r2, r1)
            goto L_0x0167
        L_0x00ad:
            r2 = r1
            if (r6 != r10) goto L_0x0065
            int r0 = com.google.android.gms.internal.measurement.zzdl.zze(r12, r8, r11)
            java.lang.Object r1 = r11.zzadc
            r9.putObject(r14, r2, r1)
            goto L_0x0115
        L_0x00ba:
            r2 = r1
            if (r6 != r10) goto L_0x0065
            com.google.android.gms.internal.measurement.zzgx r0 = r15.zzbx(r4)
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r12, r8, r13, r11)
            java.lang.Object r1 = r9.getObject(r14, r2)
            if (r1 != 0) goto L_0x00d1
            java.lang.Object r1 = r11.zzadc
            r9.putObject(r14, r2, r1)
            goto L_0x0115
        L_0x00d1:
            java.lang.Object r5 = r11.zzadc
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzez.zza(r1, r5)
            r9.putObject(r14, r2, r1)
            goto L_0x0115
        L_0x00db:
            r2 = r1
            if (r6 != r10) goto L_0x0065
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r0 & r5
            if (r0 != 0) goto L_0x00e8
            int r0 = com.google.android.gms.internal.measurement.zzdl.zzc(r12, r8, r11)
            goto L_0x00ec
        L_0x00e8:
            int r0 = com.google.android.gms.internal.measurement.zzdl.zzd(r12, r8, r11)
        L_0x00ec:
            java.lang.Object r1 = r11.zzadc
            r9.putObject(r14, r2, r1)
            goto L_0x0115
        L_0x00f2:
            r2 = r1
            if (r6 != 0) goto L_0x0065
            int r1 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r8, r11)
            long r5 = r11.zzadb
            r19 = 0
            int r8 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r8 == 0) goto L_0x0102
            goto L_0x0103
        L_0x0102:
            r0 = 0
        L_0x0103:
            com.google.android.gms.internal.measurement.zzhv.zza(r14, r2, r0)
            r0 = r1
            goto L_0x0115
        L_0x0108:
            r2 = r1
            r0 = 5
            if (r6 != r0) goto L_0x0065
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r8)
            r9.putInt(r14, r2, r0)
            int r0 = r8 + 4
        L_0x0115:
            r2 = r4
            r1 = r7
            goto L_0x0169
        L_0x0119:
            r2 = r1
            if (r6 != r0) goto L_0x0065
            long r5 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r8)
            r0 = r9
            r1 = r29
            r10 = r4
            r4 = r5
            r0.putLong(r1, r2, r4)
            int r0 = r8 + 8
            goto L_0x0167
        L_0x012b:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x016c
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r12, r8, r11)
            int r1 = r11.zzada
            r9.putInt(r14, r2, r1)
            goto L_0x0167
        L_0x0139:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x016c
            int r6 = com.google.android.gms.internal.measurement.zzdl.zzb(r12, r8, r11)
            long r4 = r11.zzadb
            r0 = r9
            r1 = r29
            r0.putLong(r1, r2, r4)
        L_0x0149:
            r0 = r6
            goto L_0x0167
        L_0x014b:
            r2 = r1
            r10 = r4
            r0 = 5
            if (r6 != r0) goto L_0x016c
            float r0 = com.google.android.gms.internal.measurement.zzdl.zzd(r12, r8)
            com.google.android.gms.internal.measurement.zzhv.zza(r14, r2, r0)
            int r0 = r8 + 4
            goto L_0x0167
        L_0x015a:
            r2 = r1
            r10 = r4
            if (r6 != r0) goto L_0x016c
            double r0 = com.google.android.gms.internal.measurement.zzdl.zzc(r12, r8)
            com.google.android.gms.internal.measurement.zzhv.zza(r14, r2, r0)
            int r0 = r8 + 8
        L_0x0167:
            r1 = r7
            r2 = r10
        L_0x0169:
            r10 = -1
            goto L_0x0017
        L_0x016c:
            r25 = r7
            r15 = r8
            r18 = r9
            r19 = r10
            goto L_0x006c
        L_0x0175:
            r0 = 27
            if (r3 != r0) goto L_0x01b1
            if (r6 != r10) goto L_0x0065
            java.lang.Object r0 = r9.getObject(r14, r1)
            com.google.android.gms.internal.measurement.zzff r0 = (com.google.android.gms.internal.measurement.zzff) r0
            boolean r3 = r0.zzrx()
            if (r3 != 0) goto L_0x0199
            int r3 = r0.size()
            if (r3 != 0) goto L_0x0190
            r3 = 10
            goto L_0x0192
        L_0x0190:
            int r3 = r3 << 1
        L_0x0192:
            com.google.android.gms.internal.measurement.zzff r0 = r0.zzap(r3)
            r9.putObject(r14, r1, r0)
        L_0x0199:
            r5 = r0
            com.google.android.gms.internal.measurement.zzgx r0 = r15.zzbx(r4)
            r1 = r17
            r2 = r30
            r3 = r8
            r19 = r4
            r4 = r32
            r6 = r33
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r1, r2, r3, r4, r5, r6)
            r1 = r7
            r2 = r19
            goto L_0x0169
        L_0x01b1:
            r19 = r4
            r0 = 49
            if (r3 > r0) goto L_0x01e8
            long r4 = (long) r5
            r0 = r28
            r20 = r1
            r1 = r29
            r2 = r30
            r10 = r3
            r3 = r8
            r22 = r4
            r4 = r32
            r5 = r17
            r24 = r6
            r6 = r7
            r25 = r7
            r7 = r24
            r15 = r8
            r8 = r19
            r18 = r9
            r26 = r10
            r27 = -1
            r9 = r22
            r11 = r26
            r12 = r20
            r14 = r33
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0244
        L_0x01e6:
            r2 = r0
            goto L_0x0234
        L_0x01e8:
            r20 = r1
            r26 = r3
            r24 = r6
            r25 = r7
            r15 = r8
            r18 = r9
            r27 = -1
            r0 = 50
            r9 = r26
            if (r9 != r0) goto L_0x0217
            r7 = r24
            if (r7 != r10) goto L_0x0215
            r0 = r28
            r1 = r29
            r2 = r30
            r3 = r15
            r4 = r32
            r5 = r19
            r6 = r20
            r8 = r33
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x0244
            goto L_0x01e6
        L_0x0215:
            r2 = r15
            goto L_0x0234
        L_0x0217:
            r7 = r24
            r0 = r28
            r1 = r29
            r2 = r30
            r3 = r15
            r4 = r32
            r8 = r5
            r5 = r17
            r6 = r25
            r10 = r20
            r12 = r19
            r13 = r33
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0244
            goto L_0x01e6
        L_0x0234:
            com.google.android.gms.internal.measurement.zzhs r4 = zzu(r29)
            r0 = r17
            r1 = r30
            r3 = r32
            r5 = r33
            int r0 = com.google.android.gms.internal.measurement.zzdl.zza(r0, r1, r2, r3, r4, r5)
        L_0x0244:
            r14 = r29
            r12 = r30
            r11 = r33
            r9 = r18
            r2 = r19
            r1 = r25
            r10 = -1
            r13 = r32
            r15 = r28
            goto L_0x0017
        L_0x0257:
            r4 = r13
            if (r0 != r4) goto L_0x025b
            return
        L_0x025b:
            com.google.android.gms.internal.measurement.zzfi r0 = com.google.android.gms.internal.measurement.zzfi.zzva()
            throw r0
        L_0x0260:
            r4 = r13
            r5 = 0
            r0 = r28
            r1 = r29
            r2 = r30
            r3 = r31
            r4 = r32
            r6 = r33
            r0.zza(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.measurement.zzdk):void");
    }

    public final void zzj(T t) {
        for (int i = this.zzakt; i < this.zzaku; i++) {
            long zzca = (long) (zzca(this.zzaks[i]) & 1048575);
            Object zzp = zzhv.zzp(t, zzca);
            if (zzp != null) {
                zzhv.zza(t, zzca, this.zzakz.zzp(zzp));
            }
        }
        int length = this.zzaks.length;
        for (int i2 = this.zzaku; i2 < length; i2++) {
            this.zzakw.zzb(t, (long) this.zzaks[i2]);
        }
        this.zzakx.zzj(t);
        if (this.zzako) {
            this.zzaky.zzj(t);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzhp<UT, UB> zzhp) {
        zzfe zzbz;
        int i2 = this.zzakj[i];
        Object zzp = zzhv.zzp(obj, (long) (zzca(i) & 1048575));
        if (zzp == null || (zzbz = zzbz(i)) == null) {
            return ub;
        }
        return zza(i, i2, this.zzakz.zzm(zzp), zzbz, ub, zzhp);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map map, zzfe zzfe, Object obj, zzhp zzhp) {
        zzfz<?, ?> zzr = this.zzakz.zzr(zzby(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (!zzfe.zzg(((Integer) entry.getValue()).intValue())) {
                if (obj == null) {
                    obj = zzhp.zzwp();
                }
                zzdx zzas = zzdp.zzas(zzga.zza(zzr, entry.getKey(), entry.getValue()));
                try {
                    zzga.zza(zzas.zzsf(), zzr, entry.getKey(), entry.getValue());
                    zzhp.zza(obj, i2, zzas.zzse());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0104, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzv(T r14) {
        /*
            r13 = this;
            r0 = 0
            r1 = -1
            r1 = 0
            r2 = -1
            r3 = 0
        L_0x0005:
            int r4 = r13.zzakt
            r5 = 1
            if (r1 >= r4) goto L_0x0108
            int[] r4 = r13.zzaks
            r4 = r4[r1]
            int[] r6 = r13.zzakj
            r6 = r6[r4]
            int r7 = r13.zzca(r4)
            boolean r8 = r13.zzakq
            r9 = 1048575(0xfffff, float:1.469367E-39)
            if (r8 != 0) goto L_0x0035
            int[] r8 = r13.zzakj
            int r10 = r4 + 2
            r8 = r8[r10]
            r10 = r8 & r9
            int r8 = r8 >>> 20
            int r8 = r5 << r8
            if (r10 == r2) goto L_0x0036
            sun.misc.Unsafe r2 = com.google.android.gms.internal.measurement.zzgm.zzaki
            long r11 = (long) r10
            int r2 = r2.getInt(r14, r11)
            r3 = r2
            r2 = r10
            goto L_0x0036
        L_0x0035:
            r8 = 0
        L_0x0036:
            r10 = 268435456(0x10000000, float:2.5243549E-29)
            r10 = r10 & r7
            if (r10 == 0) goto L_0x003d
            r10 = 1
            goto L_0x003e
        L_0x003d:
            r10 = 0
        L_0x003e:
            if (r10 == 0) goto L_0x0047
            boolean r10 = r13.zza(r14, r4, r3, r8)
            if (r10 != 0) goto L_0x0047
            return r0
        L_0x0047:
            r10 = 267386880(0xff00000, float:2.3665827E-29)
            r10 = r10 & r7
            int r10 = r10 >>> 20
            r11 = 9
            if (r10 == r11) goto L_0x00f3
            r11 = 17
            if (r10 == r11) goto L_0x00f3
            r8 = 27
            if (r10 == r8) goto L_0x00c7
            r8 = 60
            if (r10 == r8) goto L_0x00b6
            r8 = 68
            if (r10 == r8) goto L_0x00b6
            switch(r10) {
                case 49: goto L_0x00c7;
                case 50: goto L_0x0065;
                default: goto L_0x0063;
            }
        L_0x0063:
            goto L_0x0104
        L_0x0065:
            com.google.android.gms.internal.measurement.zzgb r6 = r13.zzakz
            r7 = r7 & r9
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r7)
            java.util.Map r6 = r6.zzn(r7)
            boolean r7 = r6.isEmpty()
            if (r7 != 0) goto L_0x00b3
            java.lang.Object r4 = r13.zzby(r4)
            com.google.android.gms.internal.measurement.zzgb r7 = r13.zzakz
            com.google.android.gms.internal.measurement.zzfz r4 = r7.zzr(r4)
            com.google.android.gms.internal.measurement.zzig r4 = r4.zzakd
            com.google.android.gms.internal.measurement.zzij r4 = r4.zzwz()
            com.google.android.gms.internal.measurement.zzij r7 = com.google.android.gms.internal.measurement.zzij.MESSAGE
            if (r4 != r7) goto L_0x00b3
            r4 = 0
            java.util.Collection r6 = r6.values()
            java.util.Iterator r6 = r6.iterator()
        L_0x0094:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x00b3
            java.lang.Object r7 = r6.next()
            if (r4 != 0) goto L_0x00ac
            com.google.android.gms.internal.measurement.zzgt r4 = com.google.android.gms.internal.measurement.zzgt.zzvy()
            java.lang.Class r8 = r7.getClass()
            com.google.android.gms.internal.measurement.zzgx r4 = r4.zzf(r8)
        L_0x00ac:
            boolean r7 = r4.zzv(r7)
            if (r7 != 0) goto L_0x0094
            r5 = 0
        L_0x00b3:
            if (r5 != 0) goto L_0x0104
            return r0
        L_0x00b6:
            boolean r5 = r13.zza(r14, r6, r4)
            if (r5 == 0) goto L_0x0104
            com.google.android.gms.internal.measurement.zzgx r4 = r13.zzbx(r4)
            boolean r4 = zza(r14, r7, r4)
            if (r4 != 0) goto L_0x0104
            return r0
        L_0x00c7:
            r6 = r7 & r9
            long r6 = (long) r6
            java.lang.Object r6 = com.google.android.gms.internal.measurement.zzhv.zzp(r14, r6)
            java.util.List r6 = (java.util.List) r6
            boolean r7 = r6.isEmpty()
            if (r7 != 0) goto L_0x00f0
            com.google.android.gms.internal.measurement.zzgx r4 = r13.zzbx(r4)
            r7 = 0
        L_0x00db:
            int r8 = r6.size()
            if (r7 >= r8) goto L_0x00f0
            java.lang.Object r8 = r6.get(r7)
            boolean r8 = r4.zzv(r8)
            if (r8 != 0) goto L_0x00ed
            r5 = 0
            goto L_0x00f0
        L_0x00ed:
            int r7 = r7 + 1
            goto L_0x00db
        L_0x00f0:
            if (r5 != 0) goto L_0x0104
            return r0
        L_0x00f3:
            boolean r5 = r13.zza(r14, r4, r3, r8)
            if (r5 == 0) goto L_0x0104
            com.google.android.gms.internal.measurement.zzgx r4 = r13.zzbx(r4)
            boolean r4 = zza(r14, r7, r4)
            if (r4 != 0) goto L_0x0104
            return r0
        L_0x0104:
            int r1 = r1 + 1
            goto L_0x0005
        L_0x0108:
            boolean r1 = r13.zzako
            if (r1 == 0) goto L_0x0119
            com.google.android.gms.internal.measurement.zzen<?> r1 = r13.zzaky
            com.google.android.gms.internal.measurement.zzeo r14 = r1.zzh(r14)
            boolean r14 = r14.isInitialized()
            if (r14 != 0) goto L_0x0119
            return r0
        L_0x0119:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgm.zzv(java.lang.Object):boolean");
    }

    private static boolean zza(Object obj, int i, zzgx zzgx) {
        return zzgx.zzv(zzhv.zzp(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzim zzim) throws IOException {
        if (obj instanceof String) {
            zzim.zzb(i, (String) obj);
        } else {
            zzim.zza(i, (zzdp) obj);
        }
    }

    private final void zza(Object obj, int i, zzgy zzgy) throws IOException {
        if (zzcc(i)) {
            zzhv.zza(obj, (long) (i & 1048575), zzgy.zzsn());
        } else if (this.zzakp) {
            zzhv.zza(obj, (long) (i & 1048575), zzgy.readString());
        } else {
            zzhv.zza(obj, (long) (i & 1048575), zzgy.zzso());
        }
    }

    private final int zzca(int i) {
        return this.zzakj[i + 1];
    }

    private final int zzcb(int i) {
        return this.zzakj[i + 2];
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzhv.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzhv.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzhv.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzhv.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzhv.zzp(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3) {
        if (this.zzakq) {
            return zza(t, i);
        }
        return (i2 & i3) != 0;
    }

    private final boolean zza(T t, int i) {
        if (this.zzakq) {
            int zzca = zzca(i);
            long j = (long) (zzca & 1048575);
            switch ((zzca & 267386880) >>> 20) {
                case 0:
                    return zzhv.zzo(t, j) != 0.0d;
                case 1:
                    return zzhv.zzn(t, j) != 0.0f;
                case 2:
                    return zzhv.zzl(t, j) != 0;
                case 3:
                    return zzhv.zzl(t, j) != 0;
                case 4:
                    return zzhv.zzk(t, j) != 0;
                case 5:
                    return zzhv.zzl(t, j) != 0;
                case 6:
                    return zzhv.zzk(t, j) != 0;
                case 7:
                    return zzhv.zzm(t, j);
                case 8:
                    Object zzp = zzhv.zzp(t, j);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzdp) {
                        return !zzdp.zzadh.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzhv.zzp(t, j) != null;
                case 10:
                    return !zzdp.zzadh.equals(zzhv.zzp(t, j));
                case 11:
                    return zzhv.zzk(t, j) != 0;
                case 12:
                    return zzhv.zzk(t, j) != 0;
                case 13:
                    return zzhv.zzk(t, j) != 0;
                case 14:
                    return zzhv.zzl(t, j) != 0;
                case 15:
                    return zzhv.zzk(t, j) != 0;
                case 16:
                    return zzhv.zzl(t, j) != 0;
                case 17:
                    return zzhv.zzp(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zzcb = zzcb(i);
            return (zzhv.zzk(t, (long) (zzcb & 1048575)) & (1 << (zzcb >>> 20))) != 0;
        }
    }

    private final void zzb(T t, int i) {
        if (!this.zzakq) {
            int zzcb = zzcb(i);
            long j = (long) (zzcb & 1048575);
            zzhv.zzb(t, j, zzhv.zzk(t, j) | (1 << (zzcb >>> 20)));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzhv.zzk(t, (long) (zzcb(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzhv.zzb(t, (long) (zzcb(i2) & 1048575), i);
    }

    private final int zzcd(int i) {
        if (i < this.zzakl || i > this.zzakm) {
            return -1;
        }
        return zzq(i, 0);
    }

    private final int zzp(int i, int i2) {
        if (i < this.zzakl || i > this.zzakm) {
            return -1;
        }
        return zzq(i, i2);
    }

    private final int zzq(int i, int i2) {
        int length = (this.zzakj.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzakj[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
