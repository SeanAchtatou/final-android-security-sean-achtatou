package com.google.android.gms.nearby.messages.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzaz extends RegisterListenerMethod<zzah, T> {
    private final /* synthetic */ zzak zzia;
    private final /* synthetic */ zzbd zzid;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaz(zzak zzak, ListenerHolder listenerHolder, zzbd zzbd) {
        super(listenerHolder);
        this.zzia = zzak;
        this.zzid = zzbd;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void registerListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        this.zzid.zza((zzah) anyClient, this.zzia.zza(taskCompletionSource));
    }
}
