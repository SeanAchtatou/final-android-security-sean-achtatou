package com.google.android.gms.measurement.internal;

import android.os.Bundle;

final class zzht implements Runnable {
    private final /* synthetic */ zzhq zzqz;
    private final /* synthetic */ boolean zzra;
    private final /* synthetic */ zzhr zzrb;
    private final /* synthetic */ zzhr zzrc;

    zzht(zzhq zzhq, boolean z, zzhr zzhr, zzhr zzhr2) {
        this.zzqz = zzhq;
        this.zzra = z;
        this.zzrb = zzhr;
        this.zzrc = zzhr2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, int]
     candidates:
      com.google.android.gms.measurement.internal.zzhq.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzhr, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.zzhq.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzhr, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, boolean):void */
    public final void run() {
        boolean z;
        boolean z2 = false;
        if (this.zzqz.zzad().zzz(this.zzqz.zzr().zzag())) {
            z = this.zzra && this.zzqz.zzqo != null;
            if (z) {
                this.zzqz.zza(this.zzqz.zzqo, true);
            }
        } else {
            if (this.zzra && this.zzqz.zzqo != null) {
                this.zzqz.zza(this.zzqz.zzqo, true);
            }
            z = false;
        }
        if (this.zzrb == null || this.zzrb.zzqw != this.zzrc.zzqw || !zzjs.zzs(this.zzrb.zzqv, this.zzrc.zzqv) || !zzjs.zzs(this.zzrb.zzqu, this.zzrc.zzqu)) {
            z2 = true;
        }
        if (z2) {
            Bundle bundle = new Bundle();
            zzhq.zza(this.zzrc, bundle, true);
            if (this.zzrb != null) {
                if (this.zzrb.zzqu != null) {
                    bundle.putString("_pn", this.zzrb.zzqu);
                }
                bundle.putString("_pc", this.zzrb.zzqv);
                bundle.putLong("_pi", this.zzrb.zzqw);
            }
            if (this.zzqz.zzad().zzz(this.zzqz.zzr().zzag()) && z) {
                long zzjb = this.zzqz.zzv().zzjb();
                if (zzjb > 0) {
                    this.zzqz.zzz().zzb(bundle, zzjb);
                }
            }
            this.zzqz.zzq().zza("auto", "_vs", bundle);
        }
        this.zzqz.zzqo = this.zzrc;
        this.zzqz.zzs().zza(this.zzrc);
    }
}
