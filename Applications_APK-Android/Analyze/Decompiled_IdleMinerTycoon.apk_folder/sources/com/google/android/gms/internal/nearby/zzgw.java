package com.google.android.gms.internal.nearby;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.internal.Update;
import com.google.android.gms.nearby.messages.internal.zzaf;
import com.google.android.gms.nearby.messages.internal.zzn;
import java.util.Collections;
import java.util.List;

public final class zzgw extends zzn {
    private final ListenerHolder<MessageListener> zzjj;

    public zzgw(ListenerHolder<MessageListener> listenerHolder) {
        this.zzjj = listenerHolder;
    }

    public static void zza(Intent intent, MessageListener messageListener) {
        Bundle bundleExtra = intent.getBundleExtra("com.google.android.gms.nearby.messages.UPDATES");
        zza(bundleExtra == null ? Collections.emptyList() : bundleExtra.getParcelableArrayList("com.google.android.gms.nearby.messages.UPDATES"), messageListener);
    }

    public static void zza(Iterable<Update> iterable, MessageListener messageListener) {
        for (Update next : iterable) {
            if (next.zzg(1)) {
                messageListener.onFound(next.zzhk);
            }
            if (next.zzg(2)) {
                messageListener.onLost(next.zzhk);
            }
            if (next.zzg(4)) {
                messageListener.onDistanceChanged(next.zzhk, next.zzjf);
            }
            if (next.zzg(8)) {
                messageListener.onBleSignalChanged(next.zzhk, next.zzjg);
            }
            if (next.zzg(16)) {
                Message message = next.zzhk;
                zzgs zzgs = next.zzjh;
            }
        }
    }

    public final void zza(zzaf zzaf) {
    }

    public final void zza(List<Update> list) throws RemoteException {
        this.zzjj.notifyListener(new zzgx(this, list));
    }

    public final void zzb(zzaf zzaf) {
    }
}
