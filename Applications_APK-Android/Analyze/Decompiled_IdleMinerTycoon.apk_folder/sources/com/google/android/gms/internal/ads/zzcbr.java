package com.google.android.gms.internal.ads;

import com.facebook.appevents.AppEventsConstants;
import com.ironsource.sdk.ISNAdView.ISNAdViewConstants;
import java.util.HashMap;

final /* synthetic */ class zzcbr implements zzue {
    private final zzbgz zzemf;

    zzcbr(zzbgz zzbgz) {
        this.zzemf = zzbgz;
    }

    public final void zza(zzud zzud) {
        zzbgz zzbgz = this.zzemf;
        HashMap hashMap = new HashMap();
        hashMap.put(ISNAdViewConstants.IS_VISIBLE_KEY, zzud.zzbtk ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        zzbgz.zza("onAdVisibilityChanged", hashMap);
    }
}
