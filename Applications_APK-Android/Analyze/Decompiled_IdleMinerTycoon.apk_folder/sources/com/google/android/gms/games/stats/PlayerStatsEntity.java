package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.zzd;

@UsedByReflection("GamesClientImpl.java")
@SafeParcelable.Class(creator = "PlayerStatsEntityCreator")
@SafeParcelable.Reserved({1000})
public class PlayerStatsEntity extends zzd implements PlayerStats {
    public static final Parcelable.Creator<PlayerStatsEntity> CREATOR = new zza();
    @SafeParcelable.Field(getter = "getAverageSessionLength", id = 1)
    private final float zzsi;
    @SafeParcelable.Field(getter = "getChurnProbability", id = 2)
    private final float zzsj;
    @SafeParcelable.Field(getter = "getDaysSinceLastPlayed", id = 3)
    private final int zzsk;
    @SafeParcelable.Field(getter = "getNumberOfPurchases", id = 4)
    private final int zzsl;
    @SafeParcelable.Field(getter = "getNumberOfSessions", id = 5)
    private final int zzsm;
    @SafeParcelable.Field(getter = "getSessionPercentile", id = 6)
    private final float zzsn;
    @SafeParcelable.Field(getter = "getSpendPercentile", id = 7)
    private final float zzso;
    @SafeParcelable.Field(getter = "getRawValues", id = 8)
    private final Bundle zzsp;
    @SafeParcelable.Field(getter = "getSpendProbability", id = 9)
    private final float zzsq;
    @SafeParcelable.Field(getter = "getHighSpenderProbability", id = 10)
    private final float zzsr;
    @SafeParcelable.Field(getter = "getTotalSpendNext28Days", id = 11)
    private final float zzss;

    public PlayerStatsEntity(PlayerStats playerStats) {
        this.zzsi = playerStats.getAverageSessionLength();
        this.zzsj = playerStats.getChurnProbability();
        this.zzsk = playerStats.getDaysSinceLastPlayed();
        this.zzsl = playerStats.getNumberOfPurchases();
        this.zzsm = playerStats.getNumberOfSessions();
        this.zzsn = playerStats.getSessionPercentile();
        this.zzso = playerStats.getSpendPercentile();
        this.zzsq = playerStats.getSpendProbability();
        this.zzsr = playerStats.getHighSpenderProbability();
        this.zzss = playerStats.getTotalSpendNext28Days();
        this.zzsp = playerStats.zzdu();
    }

    public /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public boolean isDataValid() {
        return true;
    }

    @SafeParcelable.Constructor
    PlayerStatsEntity(@SafeParcelable.Param(id = 1) float f, @SafeParcelable.Param(id = 2) float f2, @SafeParcelable.Param(id = 3) int i, @SafeParcelable.Param(id = 4) int i2, @SafeParcelable.Param(id = 5) int i3, @SafeParcelable.Param(id = 6) float f3, @SafeParcelable.Param(id = 7) float f4, @SafeParcelable.Param(id = 8) Bundle bundle, @SafeParcelable.Param(id = 9) float f5, @SafeParcelable.Param(id = 10) float f6, @SafeParcelable.Param(id = 11) float f7) {
        this.zzsi = f;
        this.zzsj = f2;
        this.zzsk = i;
        this.zzsl = i2;
        this.zzsm = i3;
        this.zzsn = f3;
        this.zzso = f4;
        this.zzsp = bundle;
        this.zzsq = f5;
        this.zzsr = f6;
        this.zzss = f7;
    }

    public float getAverageSessionLength() {
        return this.zzsi;
    }

    public float getChurnProbability() {
        return this.zzsj;
    }

    public int getDaysSinceLastPlayed() {
        return this.zzsk;
    }

    public int getNumberOfPurchases() {
        return this.zzsl;
    }

    public int getNumberOfSessions() {
        return this.zzsm;
    }

    public float getSessionPercentile() {
        return this.zzsn;
    }

    public float getSpendPercentile() {
        return this.zzso;
    }

    public final Bundle zzdu() {
        return this.zzsp;
    }

    public float getSpendProbability() {
        return this.zzsq;
    }

    public float getHighSpenderProbability() {
        return this.zzsr;
    }

    public float getTotalSpendNext28Days() {
        return this.zzss;
    }

    public int hashCode() {
        return zza(this);
    }

    static int zza(PlayerStats playerStats) {
        return Objects.hashCode(Float.valueOf(playerStats.getAverageSessionLength()), Float.valueOf(playerStats.getChurnProbability()), Integer.valueOf(playerStats.getDaysSinceLastPlayed()), Integer.valueOf(playerStats.getNumberOfPurchases()), Integer.valueOf(playerStats.getNumberOfSessions()), Float.valueOf(playerStats.getSessionPercentile()), Float.valueOf(playerStats.getSpendPercentile()), Float.valueOf(playerStats.getSpendProbability()), Float.valueOf(playerStats.getHighSpenderProbability()), Float.valueOf(playerStats.getTotalSpendNext28Days()));
    }

    public boolean equals(Object obj) {
        return zza(this, obj);
    }

    static boolean zza(PlayerStats playerStats, Object obj) {
        if (!(obj instanceof PlayerStats)) {
            return false;
        }
        if (playerStats == obj) {
            return true;
        }
        PlayerStats playerStats2 = (PlayerStats) obj;
        return Objects.equal(Float.valueOf(playerStats2.getAverageSessionLength()), Float.valueOf(playerStats.getAverageSessionLength())) && Objects.equal(Float.valueOf(playerStats2.getChurnProbability()), Float.valueOf(playerStats.getChurnProbability())) && Objects.equal(Integer.valueOf(playerStats2.getDaysSinceLastPlayed()), Integer.valueOf(playerStats.getDaysSinceLastPlayed())) && Objects.equal(Integer.valueOf(playerStats2.getNumberOfPurchases()), Integer.valueOf(playerStats.getNumberOfPurchases())) && Objects.equal(Integer.valueOf(playerStats2.getNumberOfSessions()), Integer.valueOf(playerStats.getNumberOfSessions())) && Objects.equal(Float.valueOf(playerStats2.getSessionPercentile()), Float.valueOf(playerStats.getSessionPercentile())) && Objects.equal(Float.valueOf(playerStats2.getSpendPercentile()), Float.valueOf(playerStats.getSpendPercentile())) && Objects.equal(Float.valueOf(playerStats2.getSpendProbability()), Float.valueOf(playerStats.getSpendProbability())) && Objects.equal(Float.valueOf(playerStats2.getHighSpenderProbability()), Float.valueOf(playerStats.getHighSpenderProbability())) && Objects.equal(Float.valueOf(playerStats2.getTotalSpendNext28Days()), Float.valueOf(playerStats.getTotalSpendNext28Days()));
    }

    public String toString() {
        return zzb(this);
    }

    static String zzb(PlayerStats playerStats) {
        return Objects.toStringHelper(playerStats).add("AverageSessionLength", Float.valueOf(playerStats.getAverageSessionLength())).add("ChurnProbability", Float.valueOf(playerStats.getChurnProbability())).add("DaysSinceLastPlayed", Integer.valueOf(playerStats.getDaysSinceLastPlayed())).add("NumberOfPurchases", Integer.valueOf(playerStats.getNumberOfPurchases())).add("NumberOfSessions", Integer.valueOf(playerStats.getNumberOfSessions())).add("SessionPercentile", Float.valueOf(playerStats.getSessionPercentile())).add("SpendPercentile", Float.valueOf(playerStats.getSpendPercentile())).add("SpendProbability", Float.valueOf(playerStats.getSpendProbability())).add("HighSpenderProbability", Float.valueOf(playerStats.getHighSpenderProbability())).add("TotalSpendNext28Days", Float.valueOf(playerStats.getTotalSpendNext28Days())).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeFloat(parcel, 1, getAverageSessionLength());
        SafeParcelWriter.writeFloat(parcel, 2, getChurnProbability());
        SafeParcelWriter.writeInt(parcel, 3, getDaysSinceLastPlayed());
        SafeParcelWriter.writeInt(parcel, 4, getNumberOfPurchases());
        SafeParcelWriter.writeInt(parcel, 5, getNumberOfSessions());
        SafeParcelWriter.writeFloat(parcel, 6, getSessionPercentile());
        SafeParcelWriter.writeFloat(parcel, 7, getSpendPercentile());
        SafeParcelWriter.writeBundle(parcel, 8, this.zzsp, false);
        SafeParcelWriter.writeFloat(parcel, 9, getSpendProbability());
        SafeParcelWriter.writeFloat(parcel, 10, getHighSpenderProbability());
        SafeParcelWriter.writeFloat(parcel, 11, getTotalSpendNext28Days());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
