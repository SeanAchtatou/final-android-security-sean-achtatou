package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;

final class zzh extends zze.zzat<TurnBasedMultiplayer.CancelMatchResult> {
    zzh(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzc(int i, String str) {
        setResult(new zze.zzc(GamesStatusCodes.zza(i), str));
    }
}
