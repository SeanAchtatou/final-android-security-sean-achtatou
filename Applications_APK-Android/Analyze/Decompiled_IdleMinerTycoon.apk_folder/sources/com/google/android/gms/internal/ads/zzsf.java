package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsh;
import java.io.IOException;

public interface zzsf<T extends zzsh> {
    int zza(zzsh zzsh, long j, long j2, IOException iOException);

    void zza(T t, long j, long j2);

    void zza(zzsh zzsh, long j, long j2, boolean z);
}
