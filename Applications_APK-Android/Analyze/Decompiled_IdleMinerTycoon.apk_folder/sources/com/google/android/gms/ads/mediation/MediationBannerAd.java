package com.google.android.gms.ads.mediation;

import android.support.annotation.NonNull;
import android.view.View;

public interface MediationBannerAd {
    @NonNull
    View getView();
}
