package com.google.android.gms.games;

import android.support.annotation.NonNull;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zzbm;

final class zzay implements zzbm<Players.LoadPlayersResult> {
    zzay() {
    }

    public final /* synthetic */ void release(@NonNull Object obj) {
        Players.LoadPlayersResult loadPlayersResult = (Players.LoadPlayersResult) obj;
        if (loadPlayersResult.getPlayers() != null) {
            loadPlayersResult.getPlayers().release();
        }
    }
}
