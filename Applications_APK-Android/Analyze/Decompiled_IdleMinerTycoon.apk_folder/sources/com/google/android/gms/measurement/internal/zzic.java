package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.measurement.zzp;

final class zzic implements Runnable {
    private final /* synthetic */ zzp zzdi;
    private final /* synthetic */ zzai zzdm;
    private final /* synthetic */ String zzdn;
    private final /* synthetic */ zzhv zzrd;

    zzic(zzhv zzhv, zzai zzai, String str, zzp zzp) {
        this.zzrd = zzhv;
        this.zzdm = zzai;
        this.zzdn = str;
        this.zzdi = zzp;
    }

    public final void run() {
        byte[] bArr = null;
        try {
            zzdx zzd = this.zzrd.zzrf;
            if (zzd == null) {
                this.zzrd.zzab().zzgk().zzao("Discarding data. Failed to send event to service to bundle");
                this.zzrd.zzz().zza(this.zzdi, (byte[]) null);
                return;
            }
            byte[] zza = zzd.zza(this.zzdm, this.zzdn);
            try {
                this.zzrd.zzir();
                this.zzrd.zzz().zza(this.zzdi, zza);
            } catch (RemoteException e) {
                byte[] bArr2 = zza;
                e = e;
                bArr = bArr2;
                try {
                    this.zzrd.zzab().zzgk().zza("Failed to send event to the service to bundle", e);
                    this.zzrd.zzz().zza(this.zzdi, bArr);
                } catch (Throwable th) {
                    th = th;
                    this.zzrd.zzz().zza(this.zzdi, bArr);
                    throw th;
                }
            } catch (Throwable th2) {
                byte[] bArr3 = zza;
                th = th2;
                bArr = bArr3;
                this.zzrd.zzz().zza(this.zzdi, bArr);
                throw th;
            }
        } catch (RemoteException e2) {
            e = e2;
            this.zzrd.zzab().zzgk().zza("Failed to send event to the service to bundle", e);
            this.zzrd.zzz().zza(this.zzdi, bArr);
        }
    }
}
