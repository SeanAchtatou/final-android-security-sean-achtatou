package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.quest.Quests;

final class zzl extends zze.zzat<Quests.ClaimMilestoneResult> {
    private final /* synthetic */ String zzha;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzl(BaseImplementation.ResultHolder resultHolder, String str) {
        super(resultHolder);
        this.zzha = str;
    }

    public final void zzai(DataHolder dataHolder) {
        setResult(new zze.zzg(dataHolder, this.zzha));
    }
}
