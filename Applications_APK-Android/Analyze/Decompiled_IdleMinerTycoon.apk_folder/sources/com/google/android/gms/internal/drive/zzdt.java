package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.MetadataChangeSet;

final class zzdt extends zzea {
    private final /* synthetic */ MetadataChangeSet zzfb;
    private final /* synthetic */ zzdp zzgo;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdt(zzdp zzdp, GoogleApiClient googleApiClient, MetadataChangeSet metadataChangeSet) {
        super(zzdp, googleApiClient, null);
        this.zzgo = zzdp;
        this.zzfb = metadataChangeSet;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        zzaw zzaw = (zzaw) anyClient;
        this.zzfb.zzp().zza(zzaw.getContext());
        ((zzeo) zzaw.getService()).zza(new zzgz(this.zzgo.zzk, this.zzfb.zzp()), new zzdy(this));
    }
}
