package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.measurement.zzbs;
import com.google.android.gms.internal.measurement.zzbw;
import com.google.android.gms.internal.measurement.zzey;
import com.google.android.gms.internal.measurement.zzx;
import com.ironsource.eventsmodule.DataBaseEventsStorage;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzjg implements zzgh {
    private static volatile zzjg zzsn;
    private boolean zzdh;
    private final zzfj zzj;
    private zzfd zzso;
    private zzej zzsp;
    private zzx zzsq;
    private zzem zzsr;
    private zzjc zzss;
    private zzp zzst;
    private final zzjo zzsu;
    private zzhp zzsv;
    private boolean zzsw;
    private boolean zzsx;
    @VisibleForTesting
    private long zzsy;
    private List<Runnable> zzsz;
    private int zzta;
    private int zztb;
    private boolean zztc;
    private boolean zztd;
    private boolean zzte;
    private FileLock zztf;
    private FileChannel zztg;
    private List<Long> zzth;
    private List<Long> zzti;
    private long zztj;

    class zza implements zzz {
        zzbs.zzg zztn;
        List<Long> zzto;
        List<zzbs.zzc> zztp;
        private long zztq;

        private zza() {
        }

        public final void zzb(zzbs.zzg zzg) {
            Preconditions.checkNotNull(zzg);
            this.zztn = zzg;
        }

        public final boolean zza(long j, zzbs.zzc zzc) {
            Preconditions.checkNotNull(zzc);
            if (this.zztp == null) {
                this.zztp = new ArrayList();
            }
            if (this.zzto == null) {
                this.zzto = new ArrayList();
            }
            if (this.zztp.size() > 0 && zza(this.zztp.get(0)) != zza(zzc)) {
                return false;
            }
            long zzuk = this.zztq + ((long) zzc.zzuk());
            if (zzuk >= ((long) Math.max(0, zzak.zzgn.get(null).intValue()))) {
                return false;
            }
            this.zztq = zzuk;
            this.zztp.add(zzc);
            this.zzto.add(Long.valueOf(j));
            if (this.zztp.size() >= Math.max(1, zzak.zzgo.get(null).intValue())) {
                return false;
            }
            return true;
        }

        private static long zza(zzbs.zzc zzc) {
            return ((zzc.getTimestampMillis() / 1000) / 60) / 60;
        }

        /* synthetic */ zza(zzjg zzjg, zzjj zzjj) {
            this();
        }
    }

    public static zzjg zzm(Context context) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(context.getApplicationContext());
        if (zzsn == null) {
            synchronized (zzjg.class) {
                if (zzsn == null) {
                    zzsn = new zzjg(new zzjm(context));
                }
            }
        }
        return zzsn;
    }

    private zzjg(zzjm zzjm) {
        this(zzjm, null);
    }

    private zzjg(zzjm zzjm, zzfj zzfj) {
        this.zzdh = false;
        Preconditions.checkNotNull(zzjm);
        this.zzj = zzfj.zza(zzjm.zzob, (zzx) null);
        this.zztj = -1;
        zzjo zzjo = new zzjo(this);
        zzjo.initialize();
        this.zzsu = zzjo;
        zzej zzej = new zzej(this);
        zzej.initialize();
        this.zzsp = zzej;
        zzfd zzfd = new zzfd(this);
        zzfd.initialize();
        this.zzso = zzfd;
        this.zzj.zzaa().zza(new zzjj(this, zzjm));
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zza(zzjm zzjm) {
        this.zzj.zzaa().zzo();
        zzx zzx = new zzx(this);
        zzx.initialize();
        this.zzsq = zzx;
        this.zzj.zzad().zza(this.zzso);
        zzp zzp = new zzp(this);
        zzp.initialize();
        this.zzst = zzp;
        zzhp zzhp = new zzhp(this);
        zzhp.initialize();
        this.zzsv = zzhp;
        zzjc zzjc = new zzjc(this);
        zzjc.initialize();
        this.zzss = zzjc;
        this.zzsr = new zzem(this);
        if (this.zzta != this.zztb) {
            this.zzj.zzab().zzgk().zza("Not all upload components initialized", Integer.valueOf(this.zzta), Integer.valueOf(this.zztb));
        }
        this.zzdh = true;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public final void start() {
        this.zzj.zzaa().zzo();
        zzgy().zzca();
        if (this.zzj.zzac().zzlj.get() == 0) {
            this.zzj.zzac().zzlj.set(this.zzj.zzx().currentTimeMillis());
        }
        zzjn();
    }

    public final zzr zzae() {
        return this.zzj.zzae();
    }

    public final zzs zzad() {
        return this.zzj.zzad();
    }

    public final zzef zzab() {
        return this.zzj.zzab();
    }

    public final zzfc zzaa() {
        return this.zzj.zzaa();
    }

    public final zzfd zzgz() {
        zza(this.zzso);
        return this.zzso;
    }

    public final zzej zzjf() {
        zza(this.zzsp);
        return this.zzsp;
    }

    public final zzx zzgy() {
        zza(this.zzsq);
        return this.zzsq;
    }

    private final zzem zzjg() {
        if (this.zzsr != null) {
            return this.zzsr;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    private final zzjc zzjh() {
        zza(this.zzss);
        return this.zzss;
    }

    public final zzp zzgx() {
        zza(this.zzst);
        return this.zzst;
    }

    public final zzhp zzji() {
        zza(this.zzsv);
        return this.zzsv;
    }

    public final zzjo zzgw() {
        zza(this.zzsu);
        return this.zzsu;
    }

    public final zzed zzy() {
        return this.zzj.zzy();
    }

    public final Context getContext() {
        return this.zzj.getContext();
    }

    public final Clock zzx() {
        return this.zzj.zzx();
    }

    public final zzjs zzz() {
        return this.zzj.zzz();
    }

    @WorkerThread
    private final void zzo() {
        this.zzj.zzaa().zzo();
    }

    /* access modifiers changed from: package-private */
    public final void zzjj() {
        if (!this.zzdh) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    private static void zza(zzjh zzjh) {
        if (zzjh == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!zzjh.isInitialized()) {
            String valueOf = String.valueOf(zzjh.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public final void zze(zzn zzn) {
        zzo();
        zzjj();
        Preconditions.checkNotEmpty(zzn.packageName);
        zzg(zzn);
    }

    private final long zzjk() {
        long currentTimeMillis = this.zzj.zzx().currentTimeMillis();
        zzeo zzac = this.zzj.zzac();
        zzac.zzbi();
        zzac.zzo();
        long j = zzac.zzln.get();
        if (j == 0) {
            j = 1 + ((long) zzac.zzz().zzjw().nextInt(86400000));
            zzac.zzln.set(j);
        }
        return ((((currentTimeMillis + j) / 1000) / 60) / 60) / 24;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>]
     candidates:
      com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
      com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void */
    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzd(zzai zzai, String str) {
        zzai zzai2 = zzai;
        String str2 = str;
        zzf zzab = zzgy().zzab(str2);
        if (zzab == null || TextUtils.isEmpty(zzab.zzal())) {
            this.zzj.zzab().zzgr().zza("No app data available; dropping event", str2);
            return;
        }
        Boolean zzc = zzc(zzab);
        if (zzc == null) {
            if (!"_ui".equals(zzai2.name)) {
                this.zzj.zzab().zzgn().zza("Could not find package. appId", zzef.zzam(str));
            }
        } else if (!zzc.booleanValue()) {
            this.zzj.zzab().zzgk().zza("App version does not match; dropping event. appId", zzef.zzam(str));
            return;
        }
        zzn zzn = r2;
        zzf zzf = zzab;
        zzn zzn2 = new zzn(str, zzab.getGmpAppId(), zzab.zzal(), zzab.zzam(), zzab.zzan(), zzab.zzao(), zzab.zzap(), (String) null, zzab.isMeasurementEnabled(), false, zzf.getFirebaseInstanceId(), zzf.zzbd(), 0L, 0, zzf.zzbe(), zzf.zzbf(), false, zzf.zzah(), zzf.zzbg(), zzf.zzaq(), zzf.zzbh());
        zzc(zzai2, zzn);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzc(zzai zzai, zzn zzn) {
        List<zzq> list;
        List<zzq> list2;
        List<zzq> list3;
        zzai zzai2 = zzai;
        zzn zzn2 = zzn;
        Preconditions.checkNotNull(zzn);
        Preconditions.checkNotEmpty(zzn2.packageName);
        zzo();
        zzjj();
        String str = zzn2.packageName;
        long j = zzai2.zzfu;
        if (zzgw().zze(zzai2, zzn2)) {
            if (!zzn2.zzcq) {
                zzg(zzn2);
                return;
            }
            if (this.zzj.zzad().zze(str, zzak.zzix) && zzn2.zzcw != null) {
                if (zzn2.zzcw.contains(zzai2.name)) {
                    Bundle zzcv = zzai2.zzfq.zzcv();
                    zzcv.putLong("ga_safelisted", 1);
                    zzai2 = new zzai(zzai2.name, new zzah(zzcv), zzai2.origin, zzai2.zzfu);
                } else {
                    this.zzj.zzab().zzgr().zza("Dropping non-safelisted event. appId, event name, origin", str, zzai2.name, zzai2.origin);
                    return;
                }
            }
            zzgy().beginTransaction();
            try {
                zzx zzgy = zzgy();
                Preconditions.checkNotEmpty(str);
                zzgy.zzo();
                zzgy.zzbi();
                if (j < 0) {
                    zzgy.zzab().zzgn().zza("Invalid time querying timed out conditional properties", zzef.zzam(str), Long.valueOf(j));
                    list = Collections.emptyList();
                } else {
                    list = zzgy.zzb("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j)});
                }
                for (zzq zzq : list) {
                    if (zzq != null) {
                        this.zzj.zzab().zzgr().zza("User property timed out", zzq.packageName, this.zzj.zzy().zzal(zzq.zzdw.name), zzq.zzdw.getValue());
                        if (zzq.zzdx != null) {
                            zzd(new zzai(zzq.zzdx, j), zzn2);
                        }
                        zzgy().zzg(str, zzq.zzdw.name);
                    }
                }
                zzx zzgy2 = zzgy();
                Preconditions.checkNotEmpty(str);
                zzgy2.zzo();
                zzgy2.zzbi();
                if (j < 0) {
                    zzgy2.zzab().zzgn().zza("Invalid time querying expired conditional properties", zzef.zzam(str), Long.valueOf(j));
                    list2 = Collections.emptyList();
                } else {
                    list2 = zzgy2.zzb("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (zzq zzq2 : list2) {
                    if (zzq2 != null) {
                        this.zzj.zzab().zzgr().zza("User property expired", zzq2.packageName, this.zzj.zzy().zzal(zzq2.zzdw.name), zzq2.zzdw.getValue());
                        zzgy().zzd(str, zzq2.zzdw.name);
                        if (zzq2.zzdz != null) {
                            arrayList.add(zzq2.zzdz);
                        }
                        zzgy().zzg(str, zzq2.zzdw.name);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i = 0;
                while (i < size) {
                    Object obj = arrayList2.get(i);
                    i++;
                    zzd(new zzai((zzai) obj, j), zzn2);
                }
                zzx zzgy3 = zzgy();
                String str2 = zzai2.name;
                Preconditions.checkNotEmpty(str);
                Preconditions.checkNotEmpty(str2);
                zzgy3.zzo();
                zzgy3.zzbi();
                if (j < 0) {
                    zzgy3.zzab().zzgn().zza("Invalid time querying triggered conditional properties", zzef.zzam(str), zzgy3.zzy().zzaj(str2), Long.valueOf(j));
                    list3 = Collections.emptyList();
                } else {
                    list3 = zzgy3.zzb("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j)});
                }
                ArrayList arrayList3 = new ArrayList(list3.size());
                for (zzq zzq3 : list3) {
                    if (zzq3 != null) {
                        zzjn zzjn = zzq3.zzdw;
                        zzjp zzjp = r4;
                        zzjp zzjp2 = new zzjp(zzq3.packageName, zzq3.origin, zzjn.name, j, zzjn.getValue());
                        if (zzgy().zza(zzjp)) {
                            this.zzj.zzab().zzgr().zza("User property triggered", zzq3.packageName, this.zzj.zzy().zzal(zzjp.name), zzjp.value);
                        } else {
                            this.zzj.zzab().zzgk().zza("Too many active user properties, ignoring", zzef.zzam(zzq3.packageName), this.zzj.zzy().zzal(zzjp.name), zzjp.value);
                        }
                        if (zzq3.zzdy != null) {
                            arrayList3.add(zzq3.zzdy);
                        }
                        zzq3.zzdw = new zzjn(zzjp);
                        zzq3.active = true;
                        zzgy().zza(zzq3);
                    }
                }
                zzd(zzai2, zzn2);
                ArrayList arrayList4 = arrayList3;
                int size2 = arrayList4.size();
                int i2 = 0;
                while (i2 < size2) {
                    Object obj2 = arrayList4.get(i2);
                    i2++;
                    zzd(new zzai((zzai) obj2, j), zzn2);
                }
                zzgy().setTransactionSuccessful();
            } finally {
                zzgy().endTransaction();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzjs.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.zzjs.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.zzjs.zza(long, java.lang.String, java.lang.String):java.net.URL
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzjs.zza(android.os.Bundle, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0337  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0855 A[Catch:{ IOException -> 0x0858, all -> 0x08ca }] */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x0885 A[Catch:{ IOException -> 0x0858, all -> 0x08ca }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0283 A[Catch:{ IOException -> 0x0858, all -> 0x08ca }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02bb A[Catch:{ IOException -> 0x0858, all -> 0x08ca }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x030a A[Catch:{ IOException -> 0x0858, all -> 0x08ca }] */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzd(com.google.android.gms.measurement.internal.zzai r24, com.google.android.gms.measurement.internal.zzn r25) {
        /*
            r23 = this;
            r1 = r23
            r2 = r24
            r3 = r25
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r25)
            java.lang.String r4 = r3.packageName
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r4)
            long r4 = java.lang.System.nanoTime()
            r23.zzo()
            r23.zzjj()
            java.lang.String r15 = r3.packageName
            com.google.android.gms.measurement.internal.zzjo r6 = r23.zzgw()
            boolean r6 = r6.zze(r2, r3)
            if (r6 != 0) goto L_0x0025
            return
        L_0x0025:
            boolean r6 = r3.zzcq
            if (r6 != 0) goto L_0x002d
            r1.zzg(r3)
            return
        L_0x002d:
            com.google.android.gms.measurement.internal.zzfd r6 = r23.zzgz()
            java.lang.String r7 = r2.name
            boolean r6 = r6.zzk(r15, r7)
            r14 = 0
            r13 = 0
            r12 = 1
            if (r6 == 0) goto L_0x00d7
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj
            com.google.android.gms.measurement.internal.zzef r3 = r3.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgn()
            java.lang.String r4 = "Dropping blacklisted event. appId"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzef.zzam(r15)
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj
            com.google.android.gms.measurement.internal.zzed r6 = r6.zzy()
            java.lang.String r7 = r2.name
            java.lang.String r6 = r6.zzaj(r7)
            r3.zza(r4, r5, r6)
            com.google.android.gms.measurement.internal.zzfd r3 = r23.zzgz()
            boolean r3 = r3.zzbc(r15)
            if (r3 != 0) goto L_0x006f
            com.google.android.gms.measurement.internal.zzfd r3 = r23.zzgz()
            boolean r3 = r3.zzbd(r15)
            if (r3 == 0) goto L_0x0070
        L_0x006f:
            r13 = 1
        L_0x0070:
            if (r13 != 0) goto L_0x008d
            java.lang.String r3 = "_err"
            java.lang.String r4 = r2.name
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x008d
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj
            com.google.android.gms.measurement.internal.zzjs r6 = r3.zzz()
            r8 = 11
            java.lang.String r9 = "_ev"
            java.lang.String r10 = r2.name
            r11 = 0
            r7 = r15
            r6.zza(r7, r8, r9, r10, r11)
        L_0x008d:
            if (r13 == 0) goto L_0x00d6
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            com.google.android.gms.measurement.internal.zzf r2 = r2.zzab(r15)
            if (r2 == 0) goto L_0x00d6
            long r3 = r2.zzat()
            long r5 = r2.zzas()
            long r3 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj
            com.google.android.gms.common.util.Clock r5 = r5.zzx()
            long r5 = r5.currentTimeMillis()
            long r5 = r5 - r3
            long r3 = java.lang.Math.abs(r5)
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r5 = com.google.android.gms.measurement.internal.zzak.zzhe
            java.lang.Object r5 = r5.get(r14)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00d6
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj
            com.google.android.gms.measurement.internal.zzef r3 = r3.zzab()
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgr()
            java.lang.String r4 = "Fetching config for blacklisted app"
            r3.zzao(r4)
            r1.zzb(r2)
        L_0x00d6:
            return
        L_0x00d7:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()
            r10 = 2
            boolean r6 = r6.isLoggable(r10)
            if (r6 == 0) goto L_0x00fd
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgs()
            java.lang.String r7 = "Logging event"
            com.google.android.gms.measurement.internal.zzfj r8 = r1.zzj
            com.google.android.gms.measurement.internal.zzed r8 = r8.zzy()
            java.lang.String r8 = r8.zzb(r2)
            r6.zza(r7, r8)
        L_0x00fd:
            com.google.android.gms.measurement.internal.zzx r6 = r23.zzgy()
            r6.beginTransaction()
            r1.zzg(r3)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "_iap"
            java.lang.String r7 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x08ca }
            if (r6 != 0) goto L_0x0122
            java.lang.String r6 = "ecommerce_purchase"
            java.lang.String r7 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x011c
            goto L_0x0122
        L_0x011c:
            r16 = 1
            r17 = 2
            goto L_0x02ca
        L_0x0122:
            com.google.android.gms.measurement.internal.zzah r6 = r2.zzfq     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = "currency"
            java.lang.String r6 = r6.getString(r7)     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = "ecommerce_purchase"
            java.lang.String r8 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r7 = r7.equals(r8)     // Catch:{ all -> 0x08ca }
            if (r7 == 0) goto L_0x018e
            com.google.android.gms.measurement.internal.zzah r7 = r2.zzfq     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = "value"
            java.lang.Double r7 = r7.zzah(r8)     // Catch:{ all -> 0x08ca }
            double r7 = r7.doubleValue()     // Catch:{ all -> 0x08ca }
            r16 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r7 = r7 * r16
            r18 = 0
            int r9 = (r7 > r18 ? 1 : (r7 == r18 ? 0 : -1))
            if (r9 != 0) goto L_0x015f
            com.google.android.gms.measurement.internal.zzah r7 = r2.zzfq     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = "value"
            java.lang.Long r7 = r7.getLong(r8)     // Catch:{ all -> 0x08ca }
            long r7 = r7.longValue()     // Catch:{ all -> 0x08ca }
            double r7 = (double) r7
            java.lang.Double.isNaN(r7)
            double r7 = r7 * r16
        L_0x015f:
            r16 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r9 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r9 > 0) goto L_0x0170
            r16 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r9 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r9 < 0) goto L_0x0170
            long r7 = java.lang.Math.round(r7)     // Catch:{ all -> 0x08ca }
            goto L_0x019a
        L_0x0170:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgn()     // Catch:{ all -> 0x08ca }
            java.lang.String r9 = "Data lost. Currency value is too big. appId"
            java.lang.Object r11 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            java.lang.Double r7 = java.lang.Double.valueOf(r7)     // Catch:{ all -> 0x08ca }
            r6.zza(r9, r11, r7)     // Catch:{ all -> 0x08ca }
            r6 = 0
            r16 = 1
            r17 = 2
            goto L_0x02b9
        L_0x018e:
            com.google.android.gms.measurement.internal.zzah r7 = r2.zzfq     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = "value"
            java.lang.Long r7 = r7.getLong(r8)     // Catch:{ all -> 0x08ca }
            long r7 = r7.longValue()     // Catch:{ all -> 0x08ca }
        L_0x019a:
            boolean r9 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08ca }
            if (r9 != 0) goto L_0x02b4
            java.util.Locale r9 = java.util.Locale.US     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r6.toUpperCase(r9)     // Catch:{ all -> 0x08ca }
            java.lang.String r9 = "[A-Z]{3}"
            boolean r9 = r6.matches(r9)     // Catch:{ all -> 0x08ca }
            if (r9 == 0) goto L_0x02b4
            java.lang.String r9 = "_ltv_"
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x08ca }
            int r11 = r6.length()     // Catch:{ all -> 0x08ca }
            if (r11 == 0) goto L_0x01c4
            java.lang.String r6 = r9.concat(r6)     // Catch:{ all -> 0x08ca }
        L_0x01c2:
            r9 = r6
            goto L_0x01ca
        L_0x01c4:
            java.lang.String r6 = new java.lang.String     // Catch:{ all -> 0x08ca }
            r6.<init>(r9)     // Catch:{ all -> 0x08ca }
            goto L_0x01c2
        L_0x01ca:
            com.google.android.gms.measurement.internal.zzx r6 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjp r6 = r6.zze(r15, r9)     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x020d
            java.lang.Object r11 = r6.value     // Catch:{ all -> 0x08ca }
            boolean r11 = r11 instanceof java.lang.Long     // Catch:{ all -> 0x08ca }
            if (r11 != 0) goto L_0x01db
            goto L_0x020d
        L_0x01db:
            java.lang.Object r6 = r6.value     // Catch:{ all -> 0x08ca }
            java.lang.Long r6 = (java.lang.Long) r6     // Catch:{ all -> 0x08ca }
            long r16 = r6.longValue()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjp r18 = new com.google.android.gms.measurement.internal.zzjp     // Catch:{ all -> 0x08ca }
            java.lang.String r11 = r2.origin     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.common.util.Clock r6 = r6.zzx()     // Catch:{ all -> 0x08ca }
            long r19 = r6.currentTimeMillis()     // Catch:{ all -> 0x08ca }
            r6 = 0
            long r16 = r16 + r7
            java.lang.Long r16 = java.lang.Long.valueOf(r16)     // Catch:{ all -> 0x08ca }
            r6 = r18
            r7 = r15
            r8 = r11
            r14 = 2
            r10 = r19
            r14 = 1
            r12 = r16
            r6.<init>(r7, r8, r9, r10, r12)     // Catch:{ all -> 0x08ca }
            r14 = r18
            r16 = 1
            r17 = 2
            goto L_0x0279
        L_0x020d:
            r14 = 1
            com.google.android.gms.measurement.internal.zzx r6 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r10 = r10.zzad()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r11 = com.google.android.gms.measurement.internal.zzak.zzhj     // Catch:{ all -> 0x08ca }
            int r10 = r10.zzb(r15, r11)     // Catch:{ all -> 0x08ca }
            int r10 = r10 - r14
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r15)     // Catch:{ all -> 0x08ca }
            r6.zzo()     // Catch:{ all -> 0x08ca }
            r6.zzbi()     // Catch:{ all -> 0x08ca }
            android.database.sqlite.SQLiteDatabase r11 = r6.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r12 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);"
            r14 = 3
            java.lang.String[] r14 = new java.lang.String[r14]     // Catch:{ SQLiteException -> 0x0247 }
            r14[r13] = r15     // Catch:{ SQLiteException -> 0x0247 }
            r16 = 1
            r14[r16] = r15     // Catch:{ SQLiteException -> 0x0245 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ SQLiteException -> 0x0245 }
            r17 = 2
            r14[r17] = r10     // Catch:{ SQLiteException -> 0x0243 }
            r11.execSQL(r12, r14)     // Catch:{ SQLiteException -> 0x0243 }
            goto L_0x025e
        L_0x0243:
            r0 = move-exception
            goto L_0x024c
        L_0x0245:
            r0 = move-exception
            goto L_0x024a
        L_0x0247:
            r0 = move-exception
            r16 = 1
        L_0x024a:
            r17 = 2
        L_0x024c:
            r10 = r0
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r11 = "Error pruning currencies. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            r6.zza(r11, r12, r10)     // Catch:{ all -> 0x08ca }
        L_0x025e:
            com.google.android.gms.measurement.internal.zzjp r14 = new com.google.android.gms.measurement.internal.zzjp     // Catch:{ all -> 0x08ca }
            java.lang.String r10 = r2.origin     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.common.util.Clock r6 = r6.zzx()     // Catch:{ all -> 0x08ca }
            long r11 = r6.currentTimeMillis()     // Catch:{ all -> 0x08ca }
            java.lang.Long r18 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x08ca }
            r6 = r14
            r7 = r15
            r8 = r10
            r10 = r11
            r12 = r18
            r6.<init>(r7, r8, r9, r10, r12)     // Catch:{ all -> 0x08ca }
        L_0x0279:
            com.google.android.gms.measurement.internal.zzx r6 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            boolean r6 = r6.zza(r14)     // Catch:{ all -> 0x08ca }
            if (r6 != 0) goto L_0x02b8
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r9 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzed r9 = r9.zzy()     // Catch:{ all -> 0x08ca }
            java.lang.String r10 = r14.name     // Catch:{ all -> 0x08ca }
            java.lang.String r9 = r9.zzal(r10)     // Catch:{ all -> 0x08ca }
            java.lang.Object r10 = r14.value     // Catch:{ all -> 0x08ca }
            r6.zza(r7, r8, r9, r10)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r6 = r6.zzz()     // Catch:{ all -> 0x08ca }
            r8 = 9
            r9 = 0
            r10 = 0
            r11 = 0
            r7 = r15
            r6.zza(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x08ca }
            goto L_0x02b8
        L_0x02b4:
            r16 = 1
            r17 = 2
        L_0x02b8:
            r6 = 1
        L_0x02b9:
            if (r6 != 0) goto L_0x02ca
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            r2.endTransaction()
            return
        L_0x02ca:
            java.lang.String r6 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r18 = com.google.android.gms.measurement.internal.zzjs.zzbk(r6)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "_err"
            java.lang.String r7 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r19 = r6.equals(r7)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r6 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            long r7 = r23.zzjk()     // Catch:{ all -> 0x08ca }
            r10 = 1
            r12 = 0
            r14 = 0
            r9 = r15
            r11 = r18
            r13 = r19
            r21 = r4
            r4 = 1
            r5 = 0
            com.google.android.gms.measurement.internal.zzw r6 = r6.zza(r7, r9, r10, r11, r12, r13, r14)     // Catch:{ all -> 0x08ca }
            long r7 = r6.zzeg     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r9 = com.google.android.gms.measurement.internal.zzak.zzgp     // Catch:{ all -> 0x08ca }
            java.lang.Object r9 = r9.get(r5)     // Catch:{ all -> 0x08ca }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x08ca }
            int r9 = r9.intValue()     // Catch:{ all -> 0x08ca }
            long r9 = (long) r9     // Catch:{ all -> 0x08ca }
            long r7 = r7 - r9
            r13 = 0
            int r9 = (r7 > r13 ? 1 : (r7 == r13 ? 0 : -1))
            r10 = 1000(0x3e8, double:4.94E-321)
            r16 = 1
            if (r9 <= 0) goto L_0x0337
            long r7 = r7 % r10
            int r2 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r2 != 0) goto L_0x0328
            com.google.android.gms.measurement.internal.zzfj r2 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r2 = r2.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r3 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            long r5 = r6.zzeg     // Catch:{ all -> 0x08ca }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x08ca }
            r2.zza(r3, r4, r5)     // Catch:{ all -> 0x08ca }
        L_0x0328:
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            r2.endTransaction()
            return
        L_0x0337:
            if (r18 == 0) goto L_0x038b
            long r7 = r6.zzef     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r9 = com.google.android.gms.measurement.internal.zzak.zzgr     // Catch:{ all -> 0x08ca }
            java.lang.Object r9 = r9.get(r5)     // Catch:{ all -> 0x08ca }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x08ca }
            int r9 = r9.intValue()     // Catch:{ all -> 0x08ca }
            long r4 = (long) r9     // Catch:{ all -> 0x08ca }
            long r7 = r7 - r4
            int r4 = (r7 > r13 ? 1 : (r7 == r13 ? 0 : -1))
            if (r4 <= 0) goto L_0x038b
            long r7 = r7 % r10
            int r3 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r3 != 0) goto L_0x036b
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r3 = r3.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            long r6 = r6.zzef     // Catch:{ all -> 0x08ca }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x08ca }
            r3.zza(r4, r5, r6)     // Catch:{ all -> 0x08ca }
        L_0x036b:
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r6 = r3.zzz()     // Catch:{ all -> 0x08ca }
            r8 = 16
            java.lang.String r9 = "_ev"
            java.lang.String r10 = r2.name     // Catch:{ all -> 0x08ca }
            r11 = 0
            r7 = r15
            r6.zza(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            r2.endTransaction()
            return
        L_0x038b:
            if (r19 == 0) goto L_0x03db
            long r4 = r6.zzei     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r7 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r7 = r7.zzad()     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = r3.packageName     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r9 = com.google.android.gms.measurement.internal.zzak.zzgq     // Catch:{ all -> 0x08ca }
            int r7 = r7.zzb(r8, r9)     // Catch:{ all -> 0x08ca }
            r8 = 1000000(0xf4240, float:1.401298E-39)
            int r7 = java.lang.Math.min(r8, r7)     // Catch:{ all -> 0x08ca }
            r11 = 0
            int r7 = java.lang.Math.max(r11, r7)     // Catch:{ all -> 0x08ca }
            long r7 = (long) r7     // Catch:{ all -> 0x08ca }
            long r4 = r4 - r7
            int r7 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
            if (r7 <= 0) goto L_0x03dc
            int r2 = (r4 > r16 ? 1 : (r4 == r16 ? 0 : -1))
            if (r2 != 0) goto L_0x03cc
            com.google.android.gms.measurement.internal.zzfj r2 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r2 = r2.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r3 = "Too many error events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            long r5 = r6.zzei     // Catch:{ all -> 0x08ca }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x08ca }
            r2.zza(r3, r4, r5)     // Catch:{ all -> 0x08ca }
        L_0x03cc:
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            r2.endTransaction()
            return
        L_0x03db:
            r11 = 0
        L_0x03dc:
            com.google.android.gms.measurement.internal.zzah r4 = r2.zzfq     // Catch:{ all -> 0x08ca }
            android.os.Bundle r4 = r4.zzcv()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r5 = r5.zzz()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "_o"
            java.lang.String r7 = r2.origin     // Catch:{ all -> 0x08ca }
            r5.zza(r4, r6, r7)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r5 = r5.zzz()     // Catch:{ all -> 0x08ca }
            boolean r5 = r5.zzbr(r15)     // Catch:{ all -> 0x08ca }
            if (r5 == 0) goto L_0x0419
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r5 = r5.zzz()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "_dbg"
            java.lang.Long r7 = java.lang.Long.valueOf(r16)     // Catch:{ all -> 0x08ca }
            r5.zza(r4, r6, r7)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r5 = r5.zzz()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "_r"
            java.lang.Long r7 = java.lang.Long.valueOf(r16)     // Catch:{ all -> 0x08ca }
            r5.zza(r4, r6, r7)     // Catch:{ all -> 0x08ca }
        L_0x0419:
            java.lang.String r5 = "_s"
            java.lang.String r6 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x08ca }
            if (r5 == 0) goto L_0x0452
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r5 = r5.zzad()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            boolean r5 = r5.zzw(r6)     // Catch:{ all -> 0x08ca }
            if (r5 == 0) goto L_0x0452
            com.google.android.gms.measurement.internal.zzx r5 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = "_sno"
            com.google.android.gms.measurement.internal.zzjp r5 = r5.zze(r6, r7)     // Catch:{ all -> 0x08ca }
            if (r5 == 0) goto L_0x0452
            java.lang.Object r6 = r5.value     // Catch:{ all -> 0x08ca }
            boolean r6 = r6 instanceof java.lang.Long     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x0452
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r6 = r6.zzz()     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = "_sno"
            java.lang.Object r5 = r5.value     // Catch:{ all -> 0x08ca }
            r6.zza(r4, r7, r5)     // Catch:{ all -> 0x08ca }
        L_0x0452:
            java.lang.String r5 = "_s"
            java.lang.String r6 = r2.name     // Catch:{ all -> 0x08ca }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x08ca }
            if (r5 == 0) goto L_0x0485
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r5 = r5.zzad()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzak.zzif     // Catch:{ all -> 0x08ca }
            boolean r5 = r5.zze(r6, r7)     // Catch:{ all -> 0x08ca }
            if (r5 == 0) goto L_0x0485
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r5 = r5.zzad()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            boolean r5 = r5.zzw(r6)     // Catch:{ all -> 0x08ca }
            if (r5 != 0) goto L_0x0485
            com.google.android.gms.measurement.internal.zzjn r5 = new com.google.android.gms.measurement.internal.zzjn     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "_sno"
            r7 = 0
            r5.<init>(r6, r13, r7)     // Catch:{ all -> 0x08ca }
            r1.zzc(r5, r3)     // Catch:{ all -> 0x08ca }
        L_0x0485:
            com.google.android.gms.measurement.internal.zzx r5 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            long r5 = r5.zzac(r15)     // Catch:{ all -> 0x08ca }
            int r7 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r7 <= 0) goto L_0x04a8
            com.google.android.gms.measurement.internal.zzfj r7 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r7 = r7.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r7 = r7.zzgn()     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzef.zzam(r15)     // Catch:{ all -> 0x08ca }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x08ca }
            r7.zza(r8, r9, r5)     // Catch:{ all -> 0x08ca }
        L_0x04a8:
            com.google.android.gms.measurement.internal.zzaf r5 = new com.google.android.gms.measurement.internal.zzaf     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r7 = r1.zzj     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = r2.origin     // Catch:{ all -> 0x08ca }
            java.lang.String r10 = r2.name     // Catch:{ all -> 0x08ca }
            long r11 = r2.zzfu     // Catch:{ all -> 0x08ca }
            r16 = 0
            r6 = r5
            r9 = r15
            r2 = 0
            r13 = r16
            r2 = r15
            r15 = r4
            r6.<init>(r7, r8, r9, r10, r11, r13, r15)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r4 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r5.name     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzae r4 = r4.zzc(r2, r6)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x0530
            com.google.android.gms.measurement.internal.zzx r4 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            long r6 = r4.zzag(r2)     // Catch:{ all -> 0x08ca }
            r8 = 500(0x1f4, double:2.47E-321)
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 < 0) goto L_0x0516
            if (r18 == 0) goto L_0x0516
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r3 = r3.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzef.zzam(r2)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r7 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzed r7 = r7.zzy()     // Catch:{ all -> 0x08ca }
            java.lang.String r5 = r5.name     // Catch:{ all -> 0x08ca }
            java.lang.String r5 = r7.zzaj(r5)     // Catch:{ all -> 0x08ca }
            r7 = 500(0x1f4, float:7.0E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x08ca }
            r3.zza(r4, r6, r5, r7)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r6 = r3.zzz()     // Catch:{ all -> 0x08ca }
            r8 = 8
            r9 = 0
            r10 = 0
            r11 = 0
            r7 = r2
            r6.zza(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            r2.endTransaction()
            return
        L_0x0516:
            com.google.android.gms.measurement.internal.zzae r4 = new com.google.android.gms.measurement.internal.zzae     // Catch:{ all -> 0x08ca }
            java.lang.String r8 = r5.name     // Catch:{ all -> 0x08ca }
            r9 = 0
            r11 = 0
            long r13 = r5.timestamp     // Catch:{ all -> 0x08ca }
            r15 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r6 = r4
            r7 = r2
            r6.<init>(r7, r8, r9, r11, r13, r15, r17, r18, r19, r20)     // Catch:{ all -> 0x08ca }
            goto L_0x053e
        L_0x0530:
            com.google.android.gms.measurement.internal.zzfj r2 = r1.zzj     // Catch:{ all -> 0x08ca }
            long r6 = r4.zzfj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzaf r5 = r5.zza(r2, r6)     // Catch:{ all -> 0x08ca }
            long r6 = r5.timestamp     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzae r4 = r4.zzw(r6)     // Catch:{ all -> 0x08ca }
        L_0x053e:
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r2.zza(r4)     // Catch:{ all -> 0x08ca }
            r23.zzo()     // Catch:{ all -> 0x08ca }
            r23.zzjj()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r5)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r25)     // Catch:{ all -> 0x08ca }
            java.lang.String r2 = r5.zzce     // Catch:{ all -> 0x08ca }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r2)     // Catch:{ all -> 0x08ca }
            java.lang.String r2 = r5.zzce     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = r3.packageName     // Catch:{ all -> 0x08ca }
            boolean r2 = r2.equals(r4)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.common.internal.Preconditions.checkArgument(r2)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r2 = com.google.android.gms.internal.measurement.zzbs.zzg.zzpr()     // Catch:{ all -> 0x08ca }
            r11 = 1
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r2 = r2.zzp(r11)     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = "android"
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r2 = r2.zzcc(r4)     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = r3.packageName     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x057d
            java.lang.String r4 = r3.packageName     // Catch:{ all -> 0x08ca }
            r2.zzch(r4)     // Catch:{ all -> 0x08ca }
        L_0x057d:
            java.lang.String r4 = r3.zzco     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x058a
            java.lang.String r4 = r3.zzco     // Catch:{ all -> 0x08ca }
            r2.zzcg(r4)     // Catch:{ all -> 0x08ca }
        L_0x058a:
            java.lang.String r4 = r3.zzcm     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x0597
            java.lang.String r4 = r3.zzcm     // Catch:{ all -> 0x08ca }
            r2.zzci(r4)     // Catch:{ all -> 0x08ca }
        L_0x0597:
            long r6 = r3.zzcn     // Catch:{ all -> 0x08ca }
            r8 = -2147483648(0xffffffff80000000, double:NaN)
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 == 0) goto L_0x05a6
            long r6 = r3.zzcn     // Catch:{ all -> 0x08ca }
            int r4 = (int) r6     // Catch:{ all -> 0x08ca }
            r2.zzv(r4)     // Catch:{ all -> 0x08ca }
        L_0x05a6:
            long r6 = r3.zzr     // Catch:{ all -> 0x08ca }
            r2.zzas(r6)     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = r3.zzcg     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x05b8
            java.lang.String r4 = r3.zzcg     // Catch:{ all -> 0x08ca }
            r2.zzcm(r4)     // Catch:{ all -> 0x08ca }
        L_0x05b8:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzak.zzit     // Catch:{ all -> 0x08ca }
            boolean r4 = r4.zza(r6)     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x05de
            java.lang.String r4 = r2.getGmpAppId()     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x05eb
            java.lang.String r4 = r3.zzcu     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x05eb
            java.lang.String r4 = r3.zzcu     // Catch:{ all -> 0x08ca }
            r2.zzcq(r4)     // Catch:{ all -> 0x08ca }
            goto L_0x05eb
        L_0x05de:
            java.lang.String r4 = r3.zzcu     // Catch:{ all -> 0x08ca }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x05eb
            java.lang.String r4 = r3.zzcu     // Catch:{ all -> 0x08ca }
            r2.zzcq(r4)     // Catch:{ all -> 0x08ca }
        L_0x05eb:
            long r6 = r3.zzcp     // Catch:{ all -> 0x08ca }
            r8 = 0
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 == 0) goto L_0x05f8
            long r6 = r3.zzcp     // Catch:{ all -> 0x08ca }
            r2.zzau(r6)     // Catch:{ all -> 0x08ca }
        L_0x05f8:
            long r6 = r3.zzs     // Catch:{ all -> 0x08ca }
            r2.zzax(r6)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzak.zzin     // Catch:{ all -> 0x08ca }
            boolean r4 = r4.zze(r6, r7)     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x061a
            com.google.android.gms.measurement.internal.zzjo r4 = r23.zzgw()     // Catch:{ all -> 0x08ca }
            java.util.List r4 = r4.zzju()     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x061a
            r2.zzd(r4)     // Catch:{ all -> 0x08ca }
        L_0x061a:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeo r4 = r4.zzac()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            android.util.Pair r4 = r4.zzap(r6)     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x064d
            java.lang.Object r6 = r4.first     // Catch:{ all -> 0x08ca }
            java.lang.CharSequence r6 = (java.lang.CharSequence) r6     // Catch:{ all -> 0x08ca }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08ca }
            if (r6 != 0) goto L_0x064d
            boolean r6 = r3.zzcs     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x06af
            java.lang.Object r6 = r4.first     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x08ca }
            r2.zzcj(r6)     // Catch:{ all -> 0x08ca }
            java.lang.Object r6 = r4.second     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x06af
            java.lang.Object r4 = r4.second     // Catch:{ all -> 0x08ca }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ all -> 0x08ca }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x08ca }
            r2.zzm(r4)     // Catch:{ all -> 0x08ca }
            goto L_0x06af
        L_0x064d:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzac r4 = r4.zzw()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            android.content.Context r6 = r6.getContext()     // Catch:{ all -> 0x08ca }
            boolean r4 = r4.zzj(r6)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x06af
            boolean r4 = r3.zzct     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x06af
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            android.content.Context r4 = r4.getContext()     // Catch:{ all -> 0x08ca }
            android.content.ContentResolver r4 = r4.getContentResolver()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "android_id"
            java.lang.String r4 = android.provider.Settings.Secure.getString(r4, r6)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x068f
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r4 = r4.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgn()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "null secure ID. appId"
            java.lang.String r7 = r2.zzag()     // Catch:{ all -> 0x08ca }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzef.zzam(r7)     // Catch:{ all -> 0x08ca }
            r4.zza(r6, r7)     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = "null"
            goto L_0x06ac
        L_0x068f:
            boolean r6 = r4.isEmpty()     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x06ac
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgn()     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = "empty secure ID. appId"
            java.lang.String r10 = r2.zzag()     // Catch:{ all -> 0x08ca }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.zzef.zzam(r10)     // Catch:{ all -> 0x08ca }
            r6.zza(r7, r10)     // Catch:{ all -> 0x08ca }
        L_0x06ac:
            r2.zzco(r4)     // Catch:{ all -> 0x08ca }
        L_0x06af:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzac r4 = r4.zzw()     // Catch:{ all -> 0x08ca }
            r4.zzbi()     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r4 = r2.zzce(r4)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzac r6 = r6.zzw()     // Catch:{ all -> 0x08ca }
            r6.zzbi()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r4 = r4.zzcd(r6)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzac r6 = r6.zzw()     // Catch:{ all -> 0x08ca }
            long r6 = r6.zzcq()     // Catch:{ all -> 0x08ca }
            int r6 = (int) r6     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r4 = r4.zzt(r6)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzac r6 = r6.zzw()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r6.zzcr()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r4 = r4.zzcf(r6)     // Catch:{ all -> 0x08ca }
            long r6 = r3.zzcr     // Catch:{ all -> 0x08ca }
            r4.zzaw(r6)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            boolean r4 = r4.isEnabled()     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x070a
            boolean r4 = com.google.android.gms.measurement.internal.zzs.zzbv()     // Catch:{ all -> 0x08ca }
            if (r4 == 0) goto L_0x070a
            r2.zzag()     // Catch:{ all -> 0x08ca }
            r4 = 0
            boolean r6 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x08ca }
            if (r6 != 0) goto L_0x070a
            r2.zzcp(r4)     // Catch:{ all -> 0x08ca }
        L_0x070a:
            com.google.android.gms.measurement.internal.zzx r4 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.packageName     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzf r4 = r4.zzab(r6)     // Catch:{ all -> 0x08ca }
            if (r4 != 0) goto L_0x077d
            com.google.android.gms.measurement.internal.zzf r4 = new com.google.android.gms.measurement.internal.zzf     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = r3.packageName     // Catch:{ all -> 0x08ca }
            r4.<init>(r6, r7)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjs r6 = r6.zzz()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r6.zzjy()     // Catch:{ all -> 0x08ca }
            r4.zza(r6)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.zzci     // Catch:{ all -> 0x08ca }
            r4.zze(r6)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.zzcg     // Catch:{ all -> 0x08ca }
            r4.zzb(r6)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeo r6 = r6.zzac()     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = r3.packageName     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r6.zzaq(r7)     // Catch:{ all -> 0x08ca }
            r4.zzd(r6)     // Catch:{ all -> 0x08ca }
            r4.zzk(r8)     // Catch:{ all -> 0x08ca }
            r4.zze(r8)     // Catch:{ all -> 0x08ca }
            r4.zzf(r8)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.zzcm     // Catch:{ all -> 0x08ca }
            r4.zzf(r6)     // Catch:{ all -> 0x08ca }
            long r6 = r3.zzcn     // Catch:{ all -> 0x08ca }
            r4.zzg(r6)     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = r3.zzco     // Catch:{ all -> 0x08ca }
            r4.zzg(r6)     // Catch:{ all -> 0x08ca }
            long r6 = r3.zzr     // Catch:{ all -> 0x08ca }
            r4.zzh(r6)     // Catch:{ all -> 0x08ca }
            long r6 = r3.zzcp     // Catch:{ all -> 0x08ca }
            r4.zzi(r6)     // Catch:{ all -> 0x08ca }
            boolean r6 = r3.zzcq     // Catch:{ all -> 0x08ca }
            r4.setMeasurementEnabled(r6)     // Catch:{ all -> 0x08ca }
            long r6 = r3.zzcr     // Catch:{ all -> 0x08ca }
            r4.zzt(r6)     // Catch:{ all -> 0x08ca }
            long r6 = r3.zzs     // Catch:{ all -> 0x08ca }
            r4.zzj(r6)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r6 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r6.zza(r4)     // Catch:{ all -> 0x08ca }
        L_0x077d:
            java.lang.String r6 = r4.getAppInstanceId()     // Catch:{ all -> 0x08ca }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08ca }
            if (r6 != 0) goto L_0x078e
            java.lang.String r6 = r4.getAppInstanceId()     // Catch:{ all -> 0x08ca }
            r2.zzck(r6)     // Catch:{ all -> 0x08ca }
        L_0x078e:
            java.lang.String r6 = r4.getFirebaseInstanceId()     // Catch:{ all -> 0x08ca }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08ca }
            if (r6 != 0) goto L_0x079f
            java.lang.String r4 = r4.getFirebaseInstanceId()     // Catch:{ all -> 0x08ca }
            r2.zzcn(r4)     // Catch:{ all -> 0x08ca }
        L_0x079f:
            com.google.android.gms.measurement.internal.zzx r4 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            java.lang.String r3 = r3.packageName     // Catch:{ all -> 0x08ca }
            java.util.List r3 = r4.zzaa(r3)     // Catch:{ all -> 0x08ca }
            r4 = 0
        L_0x07aa:
            int r6 = r3.size()     // Catch:{ all -> 0x08ca }
            if (r4 >= r6) goto L_0x07e1
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r6 = com.google.android.gms.internal.measurement.zzbs.zzk.zzqu()     // Catch:{ all -> 0x08ca }
            java.lang.Object r7 = r3.get(r4)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjp r7 = (com.google.android.gms.measurement.internal.zzjp) r7     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = r7.name     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r6 = r6.zzdb(r7)     // Catch:{ all -> 0x08ca }
            java.lang.Object r7 = r3.get(r4)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjp r7 = (com.google.android.gms.measurement.internal.zzjp) r7     // Catch:{ all -> 0x08ca }
            long r12 = r7.zztr     // Catch:{ all -> 0x08ca }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r6 = r6.zzbk(r12)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjo r7 = r23.zzgw()     // Catch:{ all -> 0x08ca }
            java.lang.Object r10 = r3.get(r4)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzjp r10 = (com.google.android.gms.measurement.internal.zzjp) r10     // Catch:{ all -> 0x08ca }
            java.lang.Object r10 = r10.value     // Catch:{ all -> 0x08ca }
            r7.zza(r6, r10)     // Catch:{ all -> 0x08ca }
            r2.zza(r6)     // Catch:{ all -> 0x08ca }
            int r4 = r4 + 1
            goto L_0x07aa
        L_0x07e1:
            com.google.android.gms.measurement.internal.zzx r3 = r23.zzgy()     // Catch:{ IOException -> 0x0858 }
            com.google.android.gms.internal.measurement.zzgi r4 = r2.zzug()     // Catch:{ IOException -> 0x0858 }
            com.google.android.gms.internal.measurement.zzey r4 = (com.google.android.gms.internal.measurement.zzey) r4     // Catch:{ IOException -> 0x0858 }
            com.google.android.gms.internal.measurement.zzbs$zzg r4 = (com.google.android.gms.internal.measurement.zzbs.zzg) r4     // Catch:{ IOException -> 0x0858 }
            long r3 = r3.zza(r4)     // Catch:{ IOException -> 0x0858 }
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzah r6 = r5.zzfq     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x084e
            com.google.android.gms.measurement.internal.zzah r6 = r5.zzfq     // Catch:{ all -> 0x08ca }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x08ca }
        L_0x07ff:
            boolean r7 = r6.hasNext()     // Catch:{ all -> 0x08ca }
            if (r7 == 0) goto L_0x0814
            java.lang.Object r7 = r6.next()     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x08ca }
            java.lang.String r10 = "_r"
            boolean r7 = r10.equals(r7)     // Catch:{ all -> 0x08ca }
            if (r7 == 0) goto L_0x07ff
            goto L_0x084f
        L_0x0814:
            com.google.android.gms.measurement.internal.zzfd r6 = r23.zzgz()     // Catch:{ all -> 0x08ca }
            java.lang.String r7 = r5.zzce     // Catch:{ all -> 0x08ca }
            java.lang.String r10 = r5.name     // Catch:{ all -> 0x08ca }
            boolean r6 = r6.zzl(r7, r10)     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzx r12 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            long r13 = r23.zzjk()     // Catch:{ all -> 0x08ca }
            java.lang.String r15 = r5.zzce     // Catch:{ all -> 0x08ca }
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            com.google.android.gms.measurement.internal.zzw r7 = r12.zza(r13, r15, r16, r17, r18, r19, r20)     // Catch:{ all -> 0x08ca }
            if (r6 == 0) goto L_0x084e
            long r6 = r7.zzej     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzs r10 = r10.zzad()     // Catch:{ all -> 0x08ca }
            java.lang.String r12 = r5.zzce     // Catch:{ all -> 0x08ca }
            int r10 = r10.zzi(r12)     // Catch:{ all -> 0x08ca }
            long r12 = (long) r10     // Catch:{ all -> 0x08ca }
            int r10 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r10 >= 0) goto L_0x084e
            goto L_0x084f
        L_0x084e:
            r11 = 0
        L_0x084f:
            boolean r2 = r2.zza(r5, r3, r11)     // Catch:{ all -> 0x08ca }
            if (r2 == 0) goto L_0x0871
            r1.zzsy = r8     // Catch:{ all -> 0x08ca }
            goto L_0x0871
        L_0x0858:
            r0 = move-exception
            r3 = r0
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r4 = r4.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgk()     // Catch:{ all -> 0x08ca }
            java.lang.String r6 = "Data loss. Failed to insert raw event metadata. appId"
            java.lang.String r2 = r2.zzag()     // Catch:{ all -> 0x08ca }
            java.lang.Object r2 = com.google.android.gms.measurement.internal.zzef.zzam(r2)     // Catch:{ all -> 0x08ca }
            r4.zza(r6, r2, r3)     // Catch:{ all -> 0x08ca }
        L_0x0871:
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()     // Catch:{ all -> 0x08ca }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzfj r2 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r2 = r2.zzab()     // Catch:{ all -> 0x08ca }
            r3 = 2
            boolean r2 = r2.isLoggable(r3)     // Catch:{ all -> 0x08ca }
            if (r2 == 0) goto L_0x089e
            com.google.android.gms.measurement.internal.zzfj r2 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzef r2 = r2.zzab()     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()     // Catch:{ all -> 0x08ca }
            java.lang.String r3 = "Event recorded"
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x08ca }
            com.google.android.gms.measurement.internal.zzed r4 = r4.zzy()     // Catch:{ all -> 0x08ca }
            java.lang.String r4 = r4.zza(r5)     // Catch:{ all -> 0x08ca }
            r2.zza(r3, r4)     // Catch:{ all -> 0x08ca }
        L_0x089e:
            com.google.android.gms.measurement.internal.zzx r2 = r23.zzgy()
            r2.endTransaction()
            r23.zzjn()
            com.google.android.gms.measurement.internal.zzfj r2 = r1.zzj
            com.google.android.gms.measurement.internal.zzef r2 = r2.zzab()
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgs()
            java.lang.String r3 = "Background event processing time, ms"
            long r4 = java.lang.System.nanoTime()
            long r4 = r4 - r21
            r6 = 500000(0x7a120, double:2.47033E-318)
            long r4 = r4 + r6
            r6 = 1000000(0xf4240, double:4.940656E-318)
            long r4 = r4 / r6
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r2.zza(r3, r4)
            return
        L_0x08ca:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.measurement.internal.zzx r3 = r23.zzgy()
            r3.endTransaction()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzjg.zzd(com.google.android.gms.measurement.internal.zzai, com.google.android.gms.measurement.internal.zzn):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:91|92) */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        r1.zzj.zzab().zzgk().zza("Failed to parse upload URL. Not uploading. appId", com.google.android.gms.measurement.internal.zzef.zzam(r5), r9);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x02c4 */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzjl() {
        /*
            r17 = this;
            r1 = r17
            r17.zzo()
            r17.zzjj()
            r0 = 1
            r1.zzte = r0
            r2 = 0
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x0301 }
            r3.zzae()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzhv r3 = r3.zzs()     // Catch:{ all -> 0x0301 }
            java.lang.Boolean r3 = r3.zzit()     // Catch:{ all -> 0x0301 }
            if (r3 != 0) goto L_0x0032
            com.google.android.gms.measurement.internal.zzfj r0 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r0 = r0.zzab()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgn()     // Catch:{ all -> 0x0301 }
            java.lang.String r3 = "Upload data called on the client side before use of service was decided"
            r0.zzao(r3)     // Catch:{ all -> 0x0301 }
            r1.zzte = r2
            r17.zzjo()
            return
        L_0x0032:
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x0301 }
            if (r3 == 0) goto L_0x004d
            com.google.android.gms.measurement.internal.zzfj r0 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r0 = r0.zzab()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgk()     // Catch:{ all -> 0x0301 }
            java.lang.String r3 = "Upload called in the client side when service should be used"
            r0.zzao(r3)     // Catch:{ all -> 0x0301 }
            r1.zzte = r2
            r17.zzjo()
            return
        L_0x004d:
            long r3 = r1.zzsy     // Catch:{ all -> 0x0301 }
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x005e
            r17.zzjn()     // Catch:{ all -> 0x0301 }
            r1.zzte = r2
            r17.zzjo()
            return
        L_0x005e:
            r17.zzo()     // Catch:{ all -> 0x0301 }
            java.util.List<java.lang.Long> r3 = r1.zzth     // Catch:{ all -> 0x0301 }
            if (r3 == 0) goto L_0x0067
            r3 = 1
            goto L_0x0068
        L_0x0067:
            r3 = 0
        L_0x0068:
            if (r3 == 0) goto L_0x007f
            com.google.android.gms.measurement.internal.zzfj r0 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r0 = r0.zzab()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgs()     // Catch:{ all -> 0x0301 }
            java.lang.String r3 = "Uploading requested multiple times"
            r0.zzao(r3)     // Catch:{ all -> 0x0301 }
            r1.zzte = r2
            r17.zzjo()
            return
        L_0x007f:
            com.google.android.gms.measurement.internal.zzej r3 = r17.zzjf()     // Catch:{ all -> 0x0301 }
            boolean r3 = r3.zzgv()     // Catch:{ all -> 0x0301 }
            if (r3 != 0) goto L_0x00a1
            com.google.android.gms.measurement.internal.zzfj r0 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r0 = r0.zzab()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgs()     // Catch:{ all -> 0x0301 }
            java.lang.String r3 = "Network not connected, ignoring upload request"
            r0.zzao(r3)     // Catch:{ all -> 0x0301 }
            r17.zzjn()     // Catch:{ all -> 0x0301 }
            r1.zzte = r2
            r17.zzjo()
            return
        L_0x00a1:
            com.google.android.gms.measurement.internal.zzfj r3 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.common.util.Clock r3 = r3.zzx()     // Catch:{ all -> 0x0301 }
            long r3 = r3.currentTimeMillis()     // Catch:{ all -> 0x0301 }
            long r7 = com.google.android.gms.measurement.internal.zzs.zzbt()     // Catch:{ all -> 0x0301 }
            r9 = 0
            long r7 = r3 - r7
            r9 = 0
            r1.zzd(r9, r7)     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzfj r7 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeo r7 = r7.zzac()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzet r7 = r7.zzlj     // Catch:{ all -> 0x0301 }
            long r7 = r7.get()     // Catch:{ all -> 0x0301 }
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x00e0
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r5 = r5.zzab()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgr()     // Catch:{ all -> 0x0301 }
            java.lang.String r6 = "Uploading events. Elapsed time since last upload attempt (ms)"
            r10 = 0
            long r7 = r3 - r7
            long r7 = java.lang.Math.abs(r7)     // Catch:{ all -> 0x0301 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0301 }
            r5.zza(r6, r7)     // Catch:{ all -> 0x0301 }
        L_0x00e0:
            com.google.android.gms.measurement.internal.zzx r5 = r17.zzgy()     // Catch:{ all -> 0x0301 }
            java.lang.String r5 = r5.zzby()     // Catch:{ all -> 0x0301 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0301 }
            r7 = -1
            if (r6 != 0) goto L_0x02d8
            long r10 = r1.zztj     // Catch:{ all -> 0x0301 }
            int r6 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x0100
            com.google.android.gms.measurement.internal.zzx r6 = r17.zzgy()     // Catch:{ all -> 0x0301 }
            long r6 = r6.zzcf()     // Catch:{ all -> 0x0301 }
            r1.zztj = r6     // Catch:{ all -> 0x0301 }
        L_0x0100:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzs r6 = r6.zzad()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.zzak.zzgl     // Catch:{ all -> 0x0301 }
            int r6 = r6.zzb(r5, r7)     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzfj r7 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzs r7 = r7.zzad()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r8 = com.google.android.gms.measurement.internal.zzak.zzgm     // Catch:{ all -> 0x0301 }
            int r7 = r7.zzb(r5, r8)     // Catch:{ all -> 0x0301 }
            int r7 = java.lang.Math.max(r2, r7)     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzx r8 = r17.zzgy()     // Catch:{ all -> 0x0301 }
            java.util.List r6 = r8.zza(r5, r6, r7)     // Catch:{ all -> 0x0301 }
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x0301 }
            if (r7 != 0) goto L_0x02fb
            java.util.Iterator r7 = r6.iterator()     // Catch:{ all -> 0x0301 }
        L_0x012e:
            boolean r8 = r7.hasNext()     // Catch:{ all -> 0x0301 }
            if (r8 == 0) goto L_0x014d
            java.lang.Object r8 = r7.next()     // Catch:{ all -> 0x0301 }
            android.util.Pair r8 = (android.util.Pair) r8     // Catch:{ all -> 0x0301 }
            java.lang.Object r8 = r8.first     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = (com.google.android.gms.internal.measurement.zzbs.zzg) r8     // Catch:{ all -> 0x0301 }
            java.lang.String r10 = r8.zzot()     // Catch:{ all -> 0x0301 }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x0301 }
            if (r10 != 0) goto L_0x012e
            java.lang.String r7 = r8.zzot()     // Catch:{ all -> 0x0301 }
            goto L_0x014e
        L_0x014d:
            r7 = r9
        L_0x014e:
            if (r7 == 0) goto L_0x017d
            r8 = 0
        L_0x0151:
            int r10 = r6.size()     // Catch:{ all -> 0x0301 }
            if (r8 >= r10) goto L_0x017d
            java.lang.Object r10 = r6.get(r8)     // Catch:{ all -> 0x0301 }
            android.util.Pair r10 = (android.util.Pair) r10     // Catch:{ all -> 0x0301 }
            java.lang.Object r10 = r10.first     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = (com.google.android.gms.internal.measurement.zzbs.zzg) r10     // Catch:{ all -> 0x0301 }
            java.lang.String r11 = r10.zzot()     // Catch:{ all -> 0x0301 }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x0301 }
            if (r11 != 0) goto L_0x017a
            java.lang.String r10 = r10.zzot()     // Catch:{ all -> 0x0301 }
            boolean r10 = r10.equals(r7)     // Catch:{ all -> 0x0301 }
            if (r10 != 0) goto L_0x017a
            java.util.List r6 = r6.subList(r2, r8)     // Catch:{ all -> 0x0301 }
            goto L_0x017d
        L_0x017a:
            int r8 = r8 + 1
            goto L_0x0151
        L_0x017d:
            com.google.android.gms.internal.measurement.zzbs$zzf$zza r7 = com.google.android.gms.internal.measurement.zzbs.zzf.zznj()     // Catch:{ all -> 0x0301 }
            int r8 = r6.size()     // Catch:{ all -> 0x0301 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x0301 }
            int r11 = r6.size()     // Catch:{ all -> 0x0301 }
            r10.<init>(r11)     // Catch:{ all -> 0x0301 }
            boolean r11 = com.google.android.gms.measurement.internal.zzs.zzbv()     // Catch:{ all -> 0x0301 }
            if (r11 == 0) goto L_0x01a2
            com.google.android.gms.measurement.internal.zzfj r11 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzs r11 = r11.zzad()     // Catch:{ all -> 0x0301 }
            boolean r11 = r11.zzl(r5)     // Catch:{ all -> 0x0301 }
            if (r11 == 0) goto L_0x01a2
            r11 = 1
            goto L_0x01a3
        L_0x01a2:
            r11 = 0
        L_0x01a3:
            r12 = 0
        L_0x01a4:
            if (r12 >= r8) goto L_0x020f
            java.lang.Object r13 = r6.get(r12)     // Catch:{ all -> 0x0301 }
            android.util.Pair r13 = (android.util.Pair) r13     // Catch:{ all -> 0x0301 }
            java.lang.Object r13 = r13.first     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = (com.google.android.gms.internal.measurement.zzbs.zzg) r13     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzey$zza r13 = r13.zzuj()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzey$zza r13 = (com.google.android.gms.internal.measurement.zzey.zza) r13     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r13 = (com.google.android.gms.internal.measurement.zzbs.zzg.zza) r13     // Catch:{ all -> 0x0301 }
            java.lang.Object r14 = r6.get(r12)     // Catch:{ all -> 0x0301 }
            android.util.Pair r14 = (android.util.Pair) r14     // Catch:{ all -> 0x0301 }
            java.lang.Object r14 = r14.second     // Catch:{ all -> 0x0301 }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x0301 }
            r10.add(r14)     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzfj r14 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzs r14 = r14.zzad()     // Catch:{ all -> 0x0301 }
            long r14 = r14.zzao()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r14 = r13.zzat(r14)     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r14 = r14.zzan(r3)     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzfj r15 = r1.zzj     // Catch:{ all -> 0x0301 }
            r15.zzae()     // Catch:{ all -> 0x0301 }
            r14.zzn(r2)     // Catch:{ all -> 0x0301 }
            if (r11 != 0) goto L_0x01e4
            r13.zznw()     // Catch:{ all -> 0x0301 }
        L_0x01e4:
            com.google.android.gms.measurement.internal.zzfj r14 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzs r14 = r14.zzad()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.zzak.zzis     // Catch:{ all -> 0x0301 }
            boolean r14 = r14.zze(r5, r15)     // Catch:{ all -> 0x0301 }
            if (r14 == 0) goto L_0x0209
            com.google.android.gms.internal.measurement.zzgi r14 = r13.zzug()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzey r14 = (com.google.android.gms.internal.measurement.zzey) r14     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = (com.google.android.gms.internal.measurement.zzbs.zzg) r14     // Catch:{ all -> 0x0301 }
            byte[] r14 = r14.toByteArray()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzjo r15 = r17.zzgw()     // Catch:{ all -> 0x0301 }
            long r14 = r15.zza(r14)     // Catch:{ all -> 0x0301 }
            r13.zzay(r14)     // Catch:{ all -> 0x0301 }
        L_0x0209:
            r7.zza(r13)     // Catch:{ all -> 0x0301 }
            int r12 = r12 + 1
            goto L_0x01a4
        L_0x020f:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x0301 }
            r11 = 2
            boolean r6 = r6.isLoggable(r11)     // Catch:{ all -> 0x0301 }
            if (r6 == 0) goto L_0x022d
            com.google.android.gms.measurement.internal.zzjo r6 = r17.zzgw()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzgi r11 = r7.zzug()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzey r11 = (com.google.android.gms.internal.measurement.zzey) r11     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzf r11 = (com.google.android.gms.internal.measurement.zzbs.zzf) r11     // Catch:{ all -> 0x0301 }
            java.lang.String r6 = r6.zza(r11)     // Catch:{ all -> 0x0301 }
            goto L_0x022e
        L_0x022d:
            r6 = r9
        L_0x022e:
            r17.zzgw()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzgi r11 = r7.zzug()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzey r11 = (com.google.android.gms.internal.measurement.zzey) r11     // Catch:{ all -> 0x0301 }
            com.google.android.gms.internal.measurement.zzbs$zzf r11 = (com.google.android.gms.internal.measurement.zzbs.zzf) r11     // Catch:{ all -> 0x0301 }
            byte[] r14 = r11.toByteArray()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzdu<java.lang.String> r11 = com.google.android.gms.measurement.internal.zzak.zzgv     // Catch:{ all -> 0x0301 }
            java.lang.Object r9 = r11.get(r9)     // Catch:{ all -> 0x0301 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0301 }
            java.net.URL r13 = new java.net.URL     // Catch:{ MalformedURLException -> 0x02c4 }
            r13.<init>(r9)     // Catch:{ MalformedURLException -> 0x02c4 }
            boolean r11 = r10.isEmpty()     // Catch:{ MalformedURLException -> 0x02c4 }
            r11 = r11 ^ r0
            com.google.android.gms.common.internal.Preconditions.checkArgument(r11)     // Catch:{ MalformedURLException -> 0x02c4 }
            java.util.List<java.lang.Long> r11 = r1.zzth     // Catch:{ MalformedURLException -> 0x02c4 }
            if (r11 == 0) goto L_0x0266
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzef r10 = r10.zzab()     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzeh r10 = r10.zzgk()     // Catch:{ MalformedURLException -> 0x02c4 }
            java.lang.String r11 = "Set uploading progress before finishing the previous upload"
            r10.zzao(r11)     // Catch:{ MalformedURLException -> 0x02c4 }
            goto L_0x026d
        L_0x0266:
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ MalformedURLException -> 0x02c4 }
            r11.<init>(r10)     // Catch:{ MalformedURLException -> 0x02c4 }
            r1.zzth = r11     // Catch:{ MalformedURLException -> 0x02c4 }
        L_0x026d:
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzeo r10 = r10.zzac()     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzet r10 = r10.zzlk     // Catch:{ MalformedURLException -> 0x02c4 }
            r10.set(r3)     // Catch:{ MalformedURLException -> 0x02c4 }
            java.lang.String r3 = "?"
            if (r8 <= 0) goto L_0x0284
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = r7.zzo(r2)     // Catch:{ MalformedURLException -> 0x02c4 }
            java.lang.String r3 = r3.zzag()     // Catch:{ MalformedURLException -> 0x02c4 }
        L_0x0284:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzef r4 = r4.zzab()     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgs()     // Catch:{ MalformedURLException -> 0x02c4 }
            java.lang.String r7 = "Uploading data. app, uncompressed size, data"
            int r8 = r14.length     // Catch:{ MalformedURLException -> 0x02c4 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ MalformedURLException -> 0x02c4 }
            r4.zza(r7, r3, r8, r6)     // Catch:{ MalformedURLException -> 0x02c4 }
            r1.zztd = r0     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzej r11 = r17.zzjf()     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzji r0 = new com.google.android.gms.measurement.internal.zzji     // Catch:{ MalformedURLException -> 0x02c4 }
            r0.<init>(r1, r5)     // Catch:{ MalformedURLException -> 0x02c4 }
            r11.zzo()     // Catch:{ MalformedURLException -> 0x02c4 }
            r11.zzbi()     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r13)     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r14)     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r0)     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzfc r3 = r11.zzaa()     // Catch:{ MalformedURLException -> 0x02c4 }
            com.google.android.gms.measurement.internal.zzen r4 = new com.google.android.gms.measurement.internal.zzen     // Catch:{ MalformedURLException -> 0x02c4 }
            r15 = 0
            r10 = r4
            r12 = r5
            r16 = r0
            r10.<init>(r11, r12, r13, r14, r15, r16)     // Catch:{ MalformedURLException -> 0x02c4 }
            r3.zzb(r4)     // Catch:{ MalformedURLException -> 0x02c4 }
            goto L_0x02fb
        L_0x02c4:
            com.google.android.gms.measurement.internal.zzfj r0 = r1.zzj     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzef r0 = r0.zzab()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgk()     // Catch:{ all -> 0x0301 }
            java.lang.String r3 = "Failed to parse upload URL. Not uploading. appId"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzef.zzam(r5)     // Catch:{ all -> 0x0301 }
            r0.zza(r3, r4, r9)     // Catch:{ all -> 0x0301 }
            goto L_0x02fb
        L_0x02d8:
            r1.zztj = r7     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzx r0 = r17.zzgy()     // Catch:{ all -> 0x0301 }
            long r5 = com.google.android.gms.measurement.internal.zzs.zzbt()     // Catch:{ all -> 0x0301 }
            r7 = 0
            long r3 = r3 - r5
            java.lang.String r0 = r0.zzu(r3)     // Catch:{ all -> 0x0301 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0301 }
            if (r3 != 0) goto L_0x02fb
            com.google.android.gms.measurement.internal.zzx r3 = r17.zzgy()     // Catch:{ all -> 0x0301 }
            com.google.android.gms.measurement.internal.zzf r0 = r3.zzab(r0)     // Catch:{ all -> 0x0301 }
            if (r0 == 0) goto L_0x02fb
            r1.zzb(r0)     // Catch:{ all -> 0x0301 }
        L_0x02fb:
            r1.zzte = r2
            r17.zzjo()
            return
        L_0x0301:
            r0 = move-exception
            r1.zzte = r2
            r17.zzjo()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzjg.zzjl():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzjg.zza(com.google.android.gms.internal.measurement.zzbs$zzg$zza, long, boolean):void
     arg types: [com.google.android.gms.internal.measurement.zzbs$zzg$zza, long, int]
     candidates:
      com.google.android.gms.measurement.internal.zzjg.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, int, java.lang.String):void
      com.google.android.gms.measurement.internal.zzjg.zza(com.google.android.gms.internal.measurement.zzbs$zzg$zza, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzjo.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void
     arg types: [com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.zzjo.zza(boolean, boolean, boolean):java.lang.String
      com.google.android.gms.measurement.internal.zzjo.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.zzbk$zzb):void
      com.google.android.gms.measurement.internal.zzjo.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0257, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0258, code lost:
        r8 = r3;
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r8 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        r7 = null;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:464:0x0c3d, code lost:
        if (r6 != r11) goto L_0x0c3f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:573:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[ExcHandler: all (th java.lang.Throwable), PHI: r3 
      PHI: (r3v31 android.database.Cursor) = (r3v26 android.database.Cursor), (r3v26 android.database.Cursor), (r3v26 android.database.Cursor), (r3v34 android.database.Cursor), (r3v34 android.database.Cursor), (r3v34 android.database.Cursor), (r3v34 android.database.Cursor), (r3v0 android.database.Cursor), (r3v0 android.database.Cursor) binds: [B:47:0x00e2, B:53:0x00ef, B:54:?, B:24:0x0080, B:30:0x008d, B:32:0x0091, B:33:?, B:9:0x0031, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0279 A[SYNTHETIC, Splitter:B:132:0x0279] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0280 A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x028e A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x03b9 A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x03bb A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x03be A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x03bf A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x05e0 A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x060d A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x06ad A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:296:0x075b A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:297:0x0775 A[Catch:{ IOException -> 0x022f, all -> 0x0efa }] */
    /* JADX WARNING: Removed duplicated region for block: B:471:0x0c5b A[Catch:{ all -> 0x0cfc }] */
    /* JADX WARNING: Removed duplicated region for block: B:475:0x0ca6 A[Catch:{ all -> 0x0cfc }] */
    /* JADX WARNING: Removed duplicated region for block: B:564:0x0edc  */
    /* JADX WARNING: Removed duplicated region for block: B:572:0x0ef3 A[SYNTHETIC, Splitter:B:572:0x0ef3] */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zzd(java.lang.String r66, long r67) {
        /*
            r65 = this;
            r1 = r65
            com.google.android.gms.measurement.internal.zzx r2 = r65.zzgy()
            r2.beginTransaction()
            com.google.android.gms.measurement.internal.zzjg$zza r2 = new com.google.android.gms.measurement.internal.zzjg$zza     // Catch:{ all -> 0x0efa }
            r3 = 0
            r2.<init>(r1, r3)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzx r4 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            long r5 = r1.zztj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r2)     // Catch:{ all -> 0x0efa }
            r4.zzo()     // Catch:{ all -> 0x0efa }
            r4.zzbi()     // Catch:{ all -> 0x0efa }
            r8 = -1
            r10 = 2
            r11 = 0
            r12 = 1
            android.database.sqlite.SQLiteDatabase r15 = r4.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            boolean r13 = android.text.TextUtils.isEmpty(r3)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            if (r13 == 0) goto L_0x00a0
            int r13 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r13 == 0) goto L_0x004b
            java.lang.String[] r13 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            java.lang.String r14 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            r13[r11] = r14     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            java.lang.String r14 = java.lang.String.valueOf(r67)     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            r13[r12] = r14     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            goto L_0x0053
        L_0x0040:
            r0 = move-exception
            r5 = r1
            r8 = r3
            goto L_0x025f
        L_0x0045:
            r0 = move-exception
            r7 = r3
            r8 = r7
        L_0x0048:
            r3 = r0
            goto L_0x0266
        L_0x004b:
            java.lang.String[] r13 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r14 = java.lang.String.valueOf(r67)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r13[r11] = r14     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
        L_0x0053:
            int r14 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r14 == 0) goto L_0x005a
            java.lang.String r14 = "rowid <= ? and "
            goto L_0x005c
        L_0x005a:
            java.lang.String r14 = ""
        L_0x005c:
            java.lang.String r16 = java.lang.String.valueOf(r14)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            int r7 = r16.length()     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            int r7 = r7 + 148
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r3.<init>(r7)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r7 = "select app_id, metadata_fingerprint from raw_events where "
            r3.append(r7)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r3.append(r14)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r7 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            r3.append(r7)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            android.database.Cursor r3 = r15.rawQuery(r3, r13)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            boolean r7 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            if (r7 != 0) goto L_0x008d
            if (r3 == 0) goto L_0x027c
            r3.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x008d:
            java.lang.String r7 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            java.lang.String r13 = r3.getString(r12)     // Catch:{ SQLiteException -> 0x009d, all -> 0x0040 }
            r3.close()     // Catch:{ SQLiteException -> 0x009d, all -> 0x0040 }
            r22 = r3
            r3 = r7
            r7 = r13
            goto L_0x00fa
        L_0x009d:
            r0 = move-exception
            r8 = r3
            goto L_0x0048
        L_0x00a0:
            int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x00b0
            java.lang.String[] r3 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r7 = 0
            r3[r11] = r7     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r3[r12] = r7     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            goto L_0x00b5
        L_0x00b0:
            java.lang.String[] r3 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r7 = 0
            r3[r11] = r7     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
        L_0x00b5:
            int r7 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r7 == 0) goto L_0x00bc
            java.lang.String r7 = " and rowid <= ?"
            goto L_0x00be
        L_0x00bc:
            java.lang.String r7 = ""
        L_0x00be:
            java.lang.String r13 = java.lang.String.valueOf(r7)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            int r13 = r13.length()     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            int r13 = r13 + 84
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r14.<init>(r13)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r13 = "select metadata_fingerprint from raw_events where app_id = ?"
            r14.append(r13)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            r14.append(r7)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r7 = " order by rowid limit 1;"
            r14.append(r7)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            java.lang.String r7 = r14.toString()     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            android.database.Cursor r3 = r15.rawQuery(r7, r3)     // Catch:{ SQLiteException -> 0x0262, all -> 0x025c }
            boolean r7 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            if (r7 != 0) goto L_0x00ef
            if (r3 == 0) goto L_0x027c
            r3.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x00ef:
            java.lang.String r13 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            r3.close()     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            r22 = r3
            r7 = r13
            r3 = 0
        L_0x00fa:
            java.lang.String r14 = "raw_events_metadata"
            java.lang.String[] r13 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0251, all -> 0x024c }
            java.lang.String r16 = "metadata"
            r13[r11] = r16     // Catch:{ SQLiteException -> 0x0251, all -> 0x024c }
            java.lang.String r16 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r8 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0251, all -> 0x024c }
            r8[r11] = r3     // Catch:{ SQLiteException -> 0x0251, all -> 0x024c }
            r8[r12] = r7     // Catch:{ SQLiteException -> 0x0251, all -> 0x024c }
            r18 = 0
            r19 = 0
            java.lang.String r20 = "rowid"
            java.lang.String r21 = "2"
            r9 = r13
            r13 = r15
            r23 = r15
            r15 = r9
            r17 = r8
            android.database.Cursor r8 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x0251, all -> 0x024c }
            boolean r9 = r8.moveToFirst()     // Catch:{ SQLiteException -> 0x0248 }
            if (r9 != 0) goto L_0x013b
            com.google.android.gms.measurement.internal.zzef r5 = r4.zzab()     // Catch:{ SQLiteException -> 0x0248 }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgk()     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r6 = "Raw event metadata record is missing. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzef.zzam(r3)     // Catch:{ SQLiteException -> 0x0248 }
            r5.zza(r6, r7)     // Catch:{ SQLiteException -> 0x0248 }
            if (r8 == 0) goto L_0x027c
            r8.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x013b:
            byte[] r9 = r8.getBlob(r11)     // Catch:{ SQLiteException -> 0x0248 }
            com.google.android.gms.internal.measurement.zzel r13 = com.google.android.gms.internal.measurement.zzel.zztq()     // Catch:{ IOException -> 0x022f }
            com.google.android.gms.internal.measurement.zzbs$zzg r9 = com.google.android.gms.internal.measurement.zzbs.zzg.zzd(r9, r13)     // Catch:{ IOException -> 0x022f }
            boolean r13 = r8.moveToNext()     // Catch:{ SQLiteException -> 0x0248 }
            if (r13 == 0) goto L_0x015e
            com.google.android.gms.measurement.internal.zzef r13 = r4.zzab()     // Catch:{ SQLiteException -> 0x0248 }
            com.google.android.gms.measurement.internal.zzeh r13 = r13.zzgn()     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r14 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r15 = com.google.android.gms.measurement.internal.zzef.zzam(r3)     // Catch:{ SQLiteException -> 0x0248 }
            r13.zza(r14, r15)     // Catch:{ SQLiteException -> 0x0248 }
        L_0x015e:
            r8.close()     // Catch:{ SQLiteException -> 0x0248 }
            r2.zzb(r9)     // Catch:{ SQLiteException -> 0x0248 }
            r13 = -1
            int r9 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r9 == 0) goto L_0x017e
            java.lang.String r9 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r13 = 3
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x0248 }
            r14[r11] = r3     // Catch:{ SQLiteException -> 0x0248 }
            r14[r12] = r7     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0248 }
            r14[r10] = r5     // Catch:{ SQLiteException -> 0x0248 }
            r16 = r9
            r17 = r14
            goto L_0x018a
        L_0x017e:
            java.lang.String r5 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r6 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0248 }
            r6[r11] = r3     // Catch:{ SQLiteException -> 0x0248 }
            r6[r12] = r7     // Catch:{ SQLiteException -> 0x0248 }
            r16 = r5
            r17 = r6
        L_0x018a:
            java.lang.String r14 = "raw_events"
            r5 = 4
            java.lang.String[] r15 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r5 = "rowid"
            r15[r11] = r5     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r5 = "name"
            r15[r12] = r5     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r5 = "timestamp"
            r15[r10] = r5     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r5 = "data"
            r6 = 3
            r15[r6] = r5     // Catch:{ SQLiteException -> 0x0248 }
            r18 = 0
            r19 = 0
            java.lang.String r20 = "rowid"
            r21 = 0
            r13 = r23
            android.database.Cursor r5 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x0248 }
            boolean r6 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            if (r6 != 0) goto L_0x01cc
            com.google.android.gms.measurement.internal.zzef r6 = r4.zzab()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgn()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            java.lang.String r7 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzef.zzam(r3)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            r6.zza(r7, r8)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            if (r5 == 0) goto L_0x027c
            r5.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x01cc:
            long r6 = r5.getLong(r11)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            r8 = 3
            byte[] r9 = r5.getBlob(r8)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r8 = com.google.android.gms.internal.measurement.zzbs.zzc.zzmq()     // Catch:{ IOException -> 0x0207 }
            com.google.android.gms.internal.measurement.zzel r13 = com.google.android.gms.internal.measurement.zzel.zztq()     // Catch:{ IOException -> 0x0207 }
            com.google.android.gms.internal.measurement.zzdh r8 = r8.zzf(r9, r13)     // Catch:{ IOException -> 0x0207 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r8 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r8     // Catch:{ IOException -> 0x0207 }
            java.lang.String r9 = r5.getString(r12)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r9 = r8.zzbx(r9)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            long r13 = r5.getLong(r10)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            r9.zzag(r13)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.internal.measurement.zzgi r8 = r8.zzug()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.internal.measurement.zzey r8 = (com.google.android.gms.internal.measurement.zzey) r8     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.internal.measurement.zzbs$zzc r8 = (com.google.android.gms.internal.measurement.zzbs.zzc) r8     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            boolean r6 = r2.zza(r6, r8)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            if (r6 != 0) goto L_0x021a
            if (r5 == 0) goto L_0x027c
            r5.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x0207:
            r0 = move-exception
            r6 = r0
            com.google.android.gms.measurement.internal.zzef r7 = r4.zzab()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            com.google.android.gms.measurement.internal.zzeh r7 = r7.zzgk()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            java.lang.String r8 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzef.zzam(r3)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            r7.zza(r8, r9, r6)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
        L_0x021a:
            boolean r6 = r5.moveToNext()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0226 }
            if (r6 != 0) goto L_0x01cc
            if (r5 == 0) goto L_0x027c
            r5.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x0226:
            r0 = move-exception
            r8 = r5
            goto L_0x0eee
        L_0x022a:
            r0 = move-exception
            r7 = r3
            r8 = r5
            goto L_0x0048
        L_0x022f:
            r0 = move-exception
            r5 = r0
            com.google.android.gms.measurement.internal.zzef r6 = r4.zzab()     // Catch:{ SQLiteException -> 0x0248 }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgk()     // Catch:{ SQLiteException -> 0x0248 }
            java.lang.String r7 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzef.zzam(r3)     // Catch:{ SQLiteException -> 0x0248 }
            r6.zza(r7, r9, r5)     // Catch:{ SQLiteException -> 0x0248 }
            if (r8 == 0) goto L_0x027c
            r8.close()     // Catch:{ all -> 0x0efa }
            goto L_0x027c
        L_0x0248:
            r0 = move-exception
            r7 = r3
            goto L_0x0048
        L_0x024c:
            r0 = move-exception
            r5 = r1
            r8 = r22
            goto L_0x025f
        L_0x0251:
            r0 = move-exception
            r7 = r3
            r8 = r22
            goto L_0x0048
        L_0x0257:
            r0 = move-exception
            r8 = r3
            r7 = 0
            goto L_0x0048
        L_0x025c:
            r0 = move-exception
            r5 = r1
            r8 = 0
        L_0x025f:
            r1 = r0
            goto L_0x0ef1
        L_0x0262:
            r0 = move-exception
            r3 = r0
            r7 = 0
            r8 = 0
        L_0x0266:
            com.google.android.gms.measurement.internal.zzef r4 = r4.zzab()     // Catch:{ all -> 0x0eed }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgk()     // Catch:{ all -> 0x0eed }
            java.lang.String r5 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzef.zzam(r7)     // Catch:{ all -> 0x0eed }
            r4.zza(r5, r6, r3)     // Catch:{ all -> 0x0eed }
            if (r8 == 0) goto L_0x027c
            r8.close()     // Catch:{ all -> 0x0efa }
        L_0x027c:
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r3 = r2.zztp     // Catch:{ all -> 0x0efa }
            if (r3 == 0) goto L_0x028b
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r3 = r2.zztp     // Catch:{ all -> 0x0efa }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0efa }
            if (r3 == 0) goto L_0x0289
            goto L_0x028b
        L_0x0289:
            r3 = 0
            goto L_0x028c
        L_0x028b:
            r3 = 1
        L_0x028c:
            if (r3 != 0) goto L_0x0edc
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = r2.zztn     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r3 = r3.zzuj()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r3 = (com.google.android.gms.internal.measurement.zzey.zza) r3     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zzg.zza) r3     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r3 = r3.zznn()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r5.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzak.zzii     // Catch:{ all -> 0x0efa }
            boolean r4 = r4.zze(r5, r6)     // Catch:{ all -> 0x0efa }
            r8 = 0
            r9 = 0
            r10 = -1
            r11 = -1
            r13 = 0
            r15 = 0
            r16 = 0
            r17 = 0
        L_0x02b9:
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r12 = r2.zztp     // Catch:{ all -> 0x0efa }
            int r12 = r12.size()     // Catch:{ all -> 0x0efa }
            if (r8 >= r12) goto L_0x07c9
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r12 = r2.zztp     // Catch:{ all -> 0x0efa }
            java.lang.Object r12 = r12.get(r8)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r12 = (com.google.android.gms.internal.measurement.zzbs.zzc) r12     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r12 = r12.zzuj()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r12 = (com.google.android.gms.internal.measurement.zzey.zza) r12     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r12 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r12     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfd r7 = r65.zzgz()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r5.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r7.zzk(r5, r6)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x0361
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r5 = r5.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgn()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "Dropping blacklisted raw event. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = r7.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzef.zzam(r7)     // Catch:{ all -> 0x0efa }
            r24 = r9
            com.google.android.gms.measurement.internal.zzfj r9 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzed r9 = r9.zzy()     // Catch:{ all -> 0x0efa }
            r25 = r15
            java.lang.String r15 = r12.getName()     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = r9.zzaj(r15)     // Catch:{ all -> 0x0efa }
            r5.zza(r6, r7, r9)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfd r5 = r65.zzgz()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r6.zzag()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.zzbc(r6)     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x0333
            com.google.android.gms.measurement.internal.zzfd r5 = r65.zzgz()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r6.zzag()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.zzbd(r6)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x0331
            goto L_0x0333
        L_0x0331:
            r5 = 0
            goto L_0x0334
        L_0x0333:
            r5 = 1
        L_0x0334:
            if (r5 != 0) goto L_0x035b
            java.lang.String r5 = "_err"
            java.lang.String r6 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x035b
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzjs r26 = r5.zzz()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r27 = r5.zzag()     // Catch:{ all -> 0x0efa }
            r28 = 11
            java.lang.String r29 = "_ev"
            java.lang.String r30 = r12.getName()     // Catch:{ all -> 0x0efa }
            r31 = 0
            r26.zza(r27, r28, r29, r30, r31)     // Catch:{ all -> 0x0efa }
        L_0x035b:
            r15 = r25
            r6 = -1
            r9 = 3
            goto L_0x07c3
        L_0x0361:
            r24 = r9
            r25 = r15
            com.google.android.gms.measurement.internal.zzfd r5 = r65.zzgz()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r6.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.zzl(r6, r7)     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x03c5
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r12.getName()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r6)     // Catch:{ all -> 0x0efa }
            int r7 = r6.hashCode()     // Catch:{ all -> 0x0efa }
            r9 = 94660(0x171c4, float:1.32647E-40)
            if (r7 == r9) goto L_0x03ab
            r9 = 95025(0x17331, float:1.33158E-40)
            if (r7 == r9) goto L_0x03a1
            r9 = 95027(0x17333, float:1.33161E-40)
            if (r7 == r9) goto L_0x0397
            goto L_0x03b5
        L_0x0397:
            java.lang.String r7 = "_ui"
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x03b5
            r6 = 1
            goto L_0x03b6
        L_0x03a1:
            java.lang.String r7 = "_ug"
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x03b5
            r6 = 2
            goto L_0x03b6
        L_0x03ab:
            java.lang.String r7 = "_in"
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x03b5
            r6 = 0
            goto L_0x03b6
        L_0x03b5:
            r6 = -1
        L_0x03b6:
            switch(r6) {
                case 0: goto L_0x03bb;
                case 1: goto L_0x03bb;
                case 2: goto L_0x03bb;
                default: goto L_0x03b9;
            }     // Catch:{ all -> 0x0efa }
        L_0x03b9:
            r6 = 0
            goto L_0x03bc
        L_0x03bb:
            r6 = 1
        L_0x03bc:
            if (r6 == 0) goto L_0x03bf
            goto L_0x03c5
        L_0x03bf:
            r32 = r8
            r33 = r13
            goto L_0x05ba
        L_0x03c5:
            r6 = 0
            r7 = 0
            r9 = 0
        L_0x03c8:
            int r15 = r12.zzmk()     // Catch:{ all -> 0x0efa }
            if (r6 >= r15) goto L_0x0438
            java.lang.String r15 = "_c"
            com.google.android.gms.internal.measurement.zzbs$zze r18 = r12.zzl(r6)     // Catch:{ all -> 0x0efa }
            r32 = r8
            java.lang.String r8 = r18.getName()     // Catch:{ all -> 0x0efa }
            boolean r8 = r15.equals(r8)     // Catch:{ all -> 0x0efa }
            if (r8 == 0) goto L_0x0401
            com.google.android.gms.internal.measurement.zzbs$zze r7 = r12.zzl(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r7 = r7.zzuj()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r7 = (com.google.android.gms.internal.measurement.zzey.zza) r7     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r7 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r7     // Catch:{ all -> 0x0efa }
            r33 = r13
            r13 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r7 = r7.zzam(r13)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r7 = r7.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r7 = (com.google.android.gms.internal.measurement.zzey) r7     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r7 = (com.google.android.gms.internal.measurement.zzbs.zze) r7     // Catch:{ all -> 0x0efa }
            r12.zza(r6, r7)     // Catch:{ all -> 0x0efa }
            r7 = 1
            goto L_0x0431
        L_0x0401:
            r33 = r13
            java.lang.String r8 = "_r"
            com.google.android.gms.internal.measurement.zzbs$zze r13 = r12.zzl(r6)     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r13.getName()     // Catch:{ all -> 0x0efa }
            boolean r8 = r8.equals(r13)     // Catch:{ all -> 0x0efa }
            if (r8 == 0) goto L_0x0431
            com.google.android.gms.internal.measurement.zzbs$zze r8 = r12.zzl(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r8 = r8.zzuj()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r8 = (com.google.android.gms.internal.measurement.zzey.zza) r8     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r8 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r8     // Catch:{ all -> 0x0efa }
            r13 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r8 = r8.zzam(r13)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r8 = r8.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r8 = (com.google.android.gms.internal.measurement.zzey) r8     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r8 = (com.google.android.gms.internal.measurement.zzbs.zze) r8     // Catch:{ all -> 0x0efa }
            r12.zza(r6, r8)     // Catch:{ all -> 0x0efa }
            r9 = 1
        L_0x0431:
            int r6 = r6 + 1
            r8 = r32
            r13 = r33
            goto L_0x03c8
        L_0x0438:
            r32 = r8
            r33 = r13
            if (r7 != 0) goto L_0x0470
            if (r5 == 0) goto L_0x0470
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgs()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "Marking event as conversion"
            com.google.android.gms.measurement.internal.zzfj r8 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzed r8 = r8.zzy()     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r12.getName()     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = r8.zzaj(r13)     // Catch:{ all -> 0x0efa }
            r6.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = com.google.android.gms.internal.measurement.zzbs.zze.zzng()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_c"
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zzbz(r7)     // Catch:{ all -> 0x0efa }
            r7 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zzam(r7)     // Catch:{ all -> 0x0efa }
            r12.zza(r6)     // Catch:{ all -> 0x0efa }
        L_0x0470:
            if (r9 != 0) goto L_0x04a2
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgs()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "Marking event as real-time"
            com.google.android.gms.measurement.internal.zzfj r8 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzed r8 = r8.zzy()     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = r12.getName()     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = r8.zzaj(r9)     // Catch:{ all -> 0x0efa }
            r6.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = com.google.android.gms.internal.measurement.zzbs.zze.zzng()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_r"
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zzbz(r7)     // Catch:{ all -> 0x0efa }
            r7 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zzam(r7)     // Catch:{ all -> 0x0efa }
            r12.zza(r6)     // Catch:{ all -> 0x0efa }
        L_0x04a2:
            com.google.android.gms.measurement.internal.zzx r35 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            long r36 = r65.zzjk()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r38 = r6.zzag()     // Catch:{ all -> 0x0efa }
            r39 = 0
            r40 = 0
            r41 = 0
            r42 = 0
            r43 = 1
            com.google.android.gms.measurement.internal.zzw r6 = r35.zza(r36, r38, r39, r40, r41, r42, r43)     // Catch:{ all -> 0x0efa }
            long r6 = r6.zzej     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfj r8 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r8 = r8.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r9 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = r9.zzag()     // Catch:{ all -> 0x0efa }
            int r8 = r8.zzi(r9)     // Catch:{ all -> 0x0efa }
            long r8 = (long) r8     // Catch:{ all -> 0x0efa }
            int r13 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r13 <= 0) goto L_0x04dd
            java.lang.String r6 = "_r"
            zza(r12, r6)     // Catch:{ all -> 0x0efa }
            r9 = r24
            goto L_0x04de
        L_0x04dd:
            r9 = 1
        L_0x04de:
            java.lang.String r6 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r6 = com.google.android.gms.measurement.internal.zzjs.zzbk(r6)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x05b6
            if (r5 == 0) goto L_0x05b6
            com.google.android.gms.measurement.internal.zzx r35 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            long r36 = r65.zzjk()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r38 = r6.zzag()     // Catch:{ all -> 0x0efa }
            r39 = 0
            r40 = 0
            r41 = 1
            r42 = 0
            r43 = 0
            com.google.android.gms.measurement.internal.zzw r6 = r35.zza(r36, r38, r39, r40, r41, r42, r43)     // Catch:{ all -> 0x0efa }
            long r6 = r6.zzeh     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfj r8 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r8 = r8.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r13.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r14 = com.google.android.gms.measurement.internal.zzak.zzgs     // Catch:{ all -> 0x0efa }
            int r8 = r8.zzb(r13, r14)     // Catch:{ all -> 0x0efa }
            long r13 = (long) r8     // Catch:{ all -> 0x0efa }
            int r8 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r8 <= 0) goto L_0x05b6
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgn()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "Too many conversions. Not logging as conversion. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = r8.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzef.zzam(r8)     // Catch:{ all -> 0x0efa }
            r6.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            r6 = 0
            r7 = 0
            r8 = 0
            r13 = -1
        L_0x053c:
            int r14 = r12.zzmk()     // Catch:{ all -> 0x0efa }
            if (r6 >= r14) goto L_0x0570
            com.google.android.gms.internal.measurement.zzbs$zze r14 = r12.zzl(r6)     // Catch:{ all -> 0x0efa }
            java.lang.String r15 = "_c"
            r44 = r9
            java.lang.String r9 = r14.getName()     // Catch:{ all -> 0x0efa }
            boolean r9 = r15.equals(r9)     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x055e
            com.google.android.gms.internal.measurement.zzey$zza r8 = r14.zzuj()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r8 = (com.google.android.gms.internal.measurement.zzey.zza) r8     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r8 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r8     // Catch:{ all -> 0x0efa }
            r13 = r6
            goto L_0x056b
        L_0x055e:
            java.lang.String r9 = "_err"
            java.lang.String r14 = r14.getName()     // Catch:{ all -> 0x0efa }
            boolean r9 = r9.equals(r14)     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x056b
            r7 = 1
        L_0x056b:
            int r6 = r6 + 1
            r9 = r44
            goto L_0x053c
        L_0x0570:
            r44 = r9
            if (r7 == 0) goto L_0x057a
            if (r8 == 0) goto L_0x057a
            r12.zzm(r13)     // Catch:{ all -> 0x0efa }
            goto L_0x05b8
        L_0x057a:
            if (r8 == 0) goto L_0x059c
            java.lang.Object r6 = r8.clone()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r6 = (com.google.android.gms.internal.measurement.zzey.zza) r6     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r6     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_err"
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zzbz(r7)     // Catch:{ all -> 0x0efa }
            r7 = 10
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zzam(r7)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r6 = r6.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r6 = (com.google.android.gms.internal.measurement.zzey) r6     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r6 = (com.google.android.gms.internal.measurement.zzbs.zze) r6     // Catch:{ all -> 0x0efa }
            r12.zza(r13, r6)     // Catch:{ all -> 0x0efa }
            goto L_0x05b8
        L_0x059c:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r6 = r6.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r6 = r6.zzgk()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "Did not find conversion parameter. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = r8.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzef.zzam(r8)     // Catch:{ all -> 0x0efa }
            r6.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            goto L_0x05b8
        L_0x05b6:
            r44 = r9
        L_0x05b8:
            r24 = r44
        L_0x05ba:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r6 = r6.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = r7.zzag()     // Catch:{ all -> 0x0efa }
            boolean r6 = r6.zzs(r7)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x0697
            if (r5 == 0) goto L_0x0697
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0efa }
            java.util.List r6 = r12.zzmj()     // Catch:{ all -> 0x0efa }
            r5.<init>(r6)     // Catch:{ all -> 0x0efa }
            r6 = 0
            r7 = -1
            r8 = -1
        L_0x05da:
            int r9 = r5.size()     // Catch:{ all -> 0x0efa }
            if (r6 >= r9) goto L_0x060a
            java.lang.String r9 = "value"
            java.lang.Object r13 = r5.get(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r13 = (com.google.android.gms.internal.measurement.zzbs.zze) r13     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r13.getName()     // Catch:{ all -> 0x0efa }
            boolean r9 = r9.equals(r13)     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x05f4
            r7 = r6
            goto L_0x0607
        L_0x05f4:
            java.lang.String r9 = "currency"
            java.lang.Object r13 = r5.get(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r13 = (com.google.android.gms.internal.measurement.zzbs.zze) r13     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r13.getName()     // Catch:{ all -> 0x0efa }
            boolean r9 = r9.equals(r13)     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0607
            r8 = r6
        L_0x0607:
            int r6 = r6 + 1
            goto L_0x05da
        L_0x060a:
            r6 = -1
            if (r7 == r6) goto L_0x0698
            java.lang.Object r6 = r5.get(r7)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r6 = (com.google.android.gms.internal.measurement.zzbs.zze) r6     // Catch:{ all -> 0x0efa }
            boolean r6 = r6.zzna()     // Catch:{ all -> 0x0efa }
            if (r6 != 0) goto L_0x0644
            java.lang.Object r6 = r5.get(r7)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r6 = (com.google.android.gms.internal.measurement.zzbs.zze) r6     // Catch:{ all -> 0x0efa }
            boolean r6 = r6.zznd()     // Catch:{ all -> 0x0efa }
            if (r6 != 0) goto L_0x0644
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r5 = r5.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgp()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "Value must be specified with a numeric type."
            r5.zzao(r6)     // Catch:{ all -> 0x0efa }
            r12.zzm(r7)     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = "_c"
            zza(r12, r5)     // Catch:{ all -> 0x0efa }
            r5 = 18
            java.lang.String r6 = "value"
            zza(r12, r5, r6)     // Catch:{ all -> 0x0efa }
            goto L_0x0697
        L_0x0644:
            r6 = -1
            if (r8 != r6) goto L_0x064a
            r5 = 1
            r9 = 3
            goto L_0x0676
        L_0x064a:
            java.lang.Object r5 = r5.get(r8)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r5 = (com.google.android.gms.internal.measurement.zzbs.zze) r5     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r5.zzmy()     // Catch:{ all -> 0x0efa }
            int r8 = r5.length()     // Catch:{ all -> 0x0efa }
            r9 = 3
            if (r8 == r9) goto L_0x065d
        L_0x065b:
            r5 = 1
            goto L_0x0676
        L_0x065d:
            r8 = 0
        L_0x065e:
            int r13 = r5.length()     // Catch:{ all -> 0x0efa }
            if (r8 >= r13) goto L_0x0675
            int r13 = r5.codePointAt(r8)     // Catch:{ all -> 0x0efa }
            boolean r14 = java.lang.Character.isLetter(r13)     // Catch:{ all -> 0x0efa }
            if (r14 != 0) goto L_0x066f
            goto L_0x065b
        L_0x066f:
            int r13 = java.lang.Character.charCount(r13)     // Catch:{ all -> 0x0efa }
            int r8 = r8 + r13
            goto L_0x065e
        L_0x0675:
            r5 = 0
        L_0x0676:
            if (r5 == 0) goto L_0x0699
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r5 = r5.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgp()     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = "Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter."
            r5.zzao(r8)     // Catch:{ all -> 0x0efa }
            r12.zzm(r7)     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = "_c"
            zza(r12, r5)     // Catch:{ all -> 0x0efa }
            r5 = 19
            java.lang.String r7 = "currency"
            zza(r12, r5, r7)     // Catch:{ all -> 0x0efa }
            goto L_0x0699
        L_0x0697:
            r6 = -1
        L_0x0698:
            r9 = 3
        L_0x0699:
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r5 = r5.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = r7.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r8 = com.google.android.gms.measurement.internal.zzak.zzih     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.zze(r7, r8)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x0747
            java.lang.String r5 = "_e"
            java.lang.String r7 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.equals(r7)     // Catch:{ all -> 0x0efa }
            r7 = 1000(0x3e8, double:4.94E-321)
            if (r5 == 0) goto L_0x06fd
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r5 = r12.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r5 = (com.google.android.gms.internal.measurement.zzey) r5     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r5 = (com.google.android.gms.internal.measurement.zzbs.zzc) r5     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = "_fr"
            com.google.android.gms.internal.measurement.zzbs$zze r5 = com.google.android.gms.measurement.internal.zzjo.zza(r5, r13)     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x0747
            if (r17 == 0) goto L_0x06f8
            long r13 = r17.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            long r15 = r12.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            r5 = 0
            long r13 = r13 - r15
            long r13 = java.lang.Math.abs(r13)     // Catch:{ all -> 0x0efa }
            int r5 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
            if (r5 > 0) goto L_0x06f8
            java.lang.Object r5 = r17.clone()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r5 = (com.google.android.gms.internal.measurement.zzey.zza) r5     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r5 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r5     // Catch:{ all -> 0x0efa }
            boolean r7 = r1.zza(r12, r5)     // Catch:{ all -> 0x0efa }
            if (r7 == 0) goto L_0x06f8
            r3.zza(r11, r5)     // Catch:{ all -> 0x0efa }
        L_0x06f3:
            r16 = 0
            r17 = 0
            goto L_0x0747
        L_0x06f8:
            r16 = r12
            r10 = r25
            goto L_0x0747
        L_0x06fd:
            java.lang.String r5 = "_vs"
            java.lang.String r13 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.equals(r13)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x0747
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r5 = r12.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r5 = (com.google.android.gms.internal.measurement.zzey) r5     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r5 = (com.google.android.gms.internal.measurement.zzbs.zzc) r5     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = "_et"
            com.google.android.gms.internal.measurement.zzbs$zze r5 = com.google.android.gms.measurement.internal.zzjo.zza(r5, r13)     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x0747
            if (r16 == 0) goto L_0x0743
            long r13 = r16.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            long r17 = r12.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            r5 = 0
            long r13 = r13 - r17
            long r13 = java.lang.Math.abs(r13)     // Catch:{ all -> 0x0efa }
            int r5 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
            if (r5 > 0) goto L_0x0743
            java.lang.Object r5 = r16.clone()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey$zza r5 = (com.google.android.gms.internal.measurement.zzey.zza) r5     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r5 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r5     // Catch:{ all -> 0x0efa }
            boolean r7 = r1.zza(r5, r12)     // Catch:{ all -> 0x0efa }
            if (r7 == 0) goto L_0x0743
            r3.zza(r10, r5)     // Catch:{ all -> 0x0efa }
            goto L_0x06f3
        L_0x0743:
            r17 = r12
            r11 = r25
        L_0x0747:
            if (r4 != 0) goto L_0x07ad
            java.lang.String r5 = "_e"
            java.lang.String r7 = r12.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.equals(r7)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x07ad
            int r5 = r12.zzmk()     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x0775
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r5 = r5.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgn()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "Engagement event does not contain any parameters. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = r8.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzef.zzam(r8)     // Catch:{ all -> 0x0efa }
            r5.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            goto L_0x07ad
        L_0x0775:
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r5 = r12.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r5 = (com.google.android.gms.internal.measurement.zzey) r5     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r5 = (com.google.android.gms.internal.measurement.zzbs.zzc) r5     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_et"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzjo.zzb(r5, r7)     // Catch:{ all -> 0x0efa }
            java.lang.Long r5 = (java.lang.Long) r5     // Catch:{ all -> 0x0efa }
            if (r5 != 0) goto L_0x07a4
            com.google.android.gms.measurement.internal.zzfj r5 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r5 = r5.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgn()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "Engagement event does not include duration. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r8 = r8.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzef.zzam(r8)     // Catch:{ all -> 0x0efa }
            r5.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            goto L_0x07ad
        L_0x07a4:
            long r7 = r5.longValue()     // Catch:{ all -> 0x0efa }
            r5 = 0
            long r13 = r33 + r7
            r33 = r13
        L_0x07ad:
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r5 = r2.zztp     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r7 = r12.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r7 = (com.google.android.gms.internal.measurement.zzey) r7     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r7 = (com.google.android.gms.internal.measurement.zzbs.zzc) r7     // Catch:{ all -> 0x0efa }
            r8 = r32
            r5.set(r8, r7)     // Catch:{ all -> 0x0efa }
            int r15 = r25 + 1
            r3.zza(r12)     // Catch:{ all -> 0x0efa }
            r13 = r33
        L_0x07c3:
            int r8 = r8 + 1
            r9 = r24
            goto L_0x02b9
        L_0x07c9:
            r24 = r9
            r33 = r13
            r25 = r15
            if (r4 == 0) goto L_0x082b
            r5 = r25
            r13 = r33
            r4 = 0
        L_0x07d6:
            if (r4 >= r5) goto L_0x082d
            com.google.android.gms.internal.measurement.zzbs$zzc r6 = r3.zzq(r4)     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_e"
            java.lang.String r8 = r6.getName()     // Catch:{ all -> 0x0efa }
            boolean r7 = r7.equals(r8)     // Catch:{ all -> 0x0efa }
            if (r7 == 0) goto L_0x07fb
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_fr"
            com.google.android.gms.internal.measurement.zzbs$zze r7 = com.google.android.gms.measurement.internal.zzjo.zza(r6, r7)     // Catch:{ all -> 0x0efa }
            if (r7 == 0) goto L_0x07fb
            r3.zzr(r4)     // Catch:{ all -> 0x0efa }
            int r5 = r5 + -1
            int r4 = r4 + -1
            goto L_0x0828
        L_0x07fb:
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = "_et"
            com.google.android.gms.internal.measurement.zzbs$zze r6 = com.google.android.gms.measurement.internal.zzjo.zza(r6, r7)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x0828
            boolean r7 = r6.zzna()     // Catch:{ all -> 0x0efa }
            if (r7 == 0) goto L_0x0815
            long r6 = r6.zznb()     // Catch:{ all -> 0x0efa }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0efa }
            goto L_0x0816
        L_0x0815:
            r6 = 0
        L_0x0816:
            if (r6 == 0) goto L_0x0828
            long r7 = r6.longValue()     // Catch:{ all -> 0x0efa }
            r9 = 0
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 <= 0) goto L_0x0828
            long r6 = r6.longValue()     // Catch:{ all -> 0x0efa }
            r8 = 0
            long r13 = r13 + r6
        L_0x0828:
            r6 = 1
            int r4 = r4 + r6
            goto L_0x07d6
        L_0x082b:
            r13 = r33
        L_0x082d:
            r4 = 0
            r1.zza(r3, r13, r4)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r3.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzak.zzja     // Catch:{ all -> 0x0efa }
            boolean r4 = r4.zze(r5, r6)     // Catch:{ all -> 0x0efa }
            if (r4 == 0) goto L_0x087a
            java.util.List r4 = r3.zznl()     // Catch:{ all -> 0x0efa }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0efa }
        L_0x084b:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x0865
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r5 = (com.google.android.gms.internal.measurement.zzbs.zzc) r5     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "_s"
            java.lang.String r5 = r5.getName()     // Catch:{ all -> 0x0efa }
            boolean r5 = r6.equals(r5)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x084b
            r4 = 1
            goto L_0x0866
        L_0x0865:
            r4 = 0
        L_0x0866:
            if (r4 == 0) goto L_0x0875
            com.google.android.gms.measurement.internal.zzx r4 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r3.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "_se"
            r4.zzd(r5, r6)     // Catch:{ all -> 0x0efa }
        L_0x0875:
            r4 = 1
            r1.zza(r3, r13, r4)     // Catch:{ all -> 0x0efa }
            goto L_0x0899
        L_0x087a:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r3.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzak.zzjb     // Catch:{ all -> 0x0efa }
            boolean r4 = r4.zze(r5, r6)     // Catch:{ all -> 0x0efa }
            if (r4 == 0) goto L_0x0899
            com.google.android.gms.measurement.internal.zzx r4 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r3.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "_se"
            r4.zzd(r5, r6)     // Catch:{ all -> 0x0efa }
        L_0x0899:
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r3.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzak.zzij     // Catch:{ all -> 0x0efa }
            boolean r4 = r4.zze(r5, r6)     // Catch:{ all -> 0x0efa }
            if (r4 == 0) goto L_0x093e
            com.google.android.gms.measurement.internal.zzjo r4 = r65.zzgw()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r5 = r4.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgs()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "Checking account type status for ad personalization signals"
            r5.zzao(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfd r5 = r4.zzgz()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r3.zzag()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.zzba(r6)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x093e
            com.google.android.gms.measurement.internal.zzx r5 = r4.zzgy()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = r3.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzf r5 = r5.zzab(r6)     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x093e
            boolean r5 = r5.zzbe()     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x093e
            com.google.android.gms.measurement.internal.zzac r5 = r4.zzw()     // Catch:{ all -> 0x0efa }
            boolean r5 = r5.zzcu()     // Catch:{ all -> 0x0efa }
            if (r5 == 0) goto L_0x093e
            com.google.android.gms.measurement.internal.zzef r5 = r4.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r5 = r5.zzgr()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "Turning off ad personalization due to account type"
            r5.zzao(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r5 = com.google.android.gms.internal.measurement.zzbs.zzk.zzqu()     // Catch:{ all -> 0x0efa }
            java.lang.String r6 = "_npa"
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r5 = r5.zzdb(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzac r4 = r4.zzw()     // Catch:{ all -> 0x0efa }
            long r6 = r4.zzcs()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r4 = r5.zzbk(r6)     // Catch:{ all -> 0x0efa }
            r5 = 1
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r4 = r4.zzbl(r5)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r4 = r4.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r4 = (com.google.android.gms.internal.measurement.zzey) r4     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzk r4 = (com.google.android.gms.internal.measurement.zzbs.zzk) r4     // Catch:{ all -> 0x0efa }
            r5 = 0
        L_0x091a:
            int r6 = r3.zznp()     // Catch:{ all -> 0x0efa }
            if (r5 >= r6) goto L_0x0938
            java.lang.String r6 = "_npa"
            com.google.android.gms.internal.measurement.zzbs$zzk r7 = r3.zzs(r5)     // Catch:{ all -> 0x0efa }
            java.lang.String r7 = r7.getName()     // Catch:{ all -> 0x0efa }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x0efa }
            if (r6 == 0) goto L_0x0935
            r3.zza(r5, r4)     // Catch:{ all -> 0x0efa }
            r5 = 1
            goto L_0x0939
        L_0x0935:
            int r5 = r5 + 1
            goto L_0x091a
        L_0x0938:
            r5 = 0
        L_0x0939:
            if (r5 != 0) goto L_0x093e
            r3.zza(r4)     // Catch:{ all -> 0x0efa }
        L_0x093e:
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r4 = r3.zznv()     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r3.zzag()     // Catch:{ all -> 0x0efa }
            java.util.List r6 = r3.zzno()     // Catch:{ all -> 0x0efa }
            java.util.List r7 = r3.zznl()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r5)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzp r8 = r65.zzgx()     // Catch:{ all -> 0x0efa }
            java.util.List r5 = r8.zza(r5, r7, r6)     // Catch:{ all -> 0x0efa }
            r4.zzc(r5)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfj r4 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r4 = r4.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r5 = r5.zzag()     // Catch:{ all -> 0x0efa }
            boolean r4 = r4.zzm(r5)     // Catch:{ all -> 0x0efa }
            if (r4 == 0) goto L_0x0d02
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x0cfc }
            r4.<init>()     // Catch:{ all -> 0x0cfc }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0cfc }
            r5.<init>()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzjs r6 = r6.zzz()     // Catch:{ all -> 0x0cfc }
            java.security.SecureRandom r6 = r6.zzjw()     // Catch:{ all -> 0x0cfc }
            r7 = 0
        L_0x0983:
            int r8 = r3.zznm()     // Catch:{ all -> 0x0cfc }
            if (r7 >= r8) goto L_0x0cc7
            com.google.android.gms.internal.measurement.zzbs$zzc r8 = r3.zzq(r7)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzey$zza r8 = r8.zzuj()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzey$zza r8 = (com.google.android.gms.internal.measurement.zzey.zza) r8     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r8 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r8     // Catch:{ all -> 0x0cfc }
            java.lang.String r9 = r8.getName()     // Catch:{ all -> 0x0cfc }
            java.lang.String r10 = "_ep"
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x0cfc }
            if (r9 == 0) goto L_0x0a16
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r9 = r8.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r9 = (com.google.android.gms.internal.measurement.zzey) r9     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0efa }
            java.lang.String r10 = "_en"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzjo.zzb(r9, r10)     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0efa }
            java.lang.Object r10 = r4.get(r9)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzae r10 = (com.google.android.gms.measurement.internal.zzae) r10     // Catch:{ all -> 0x0efa }
            if (r10 != 0) goto L_0x09cd
            com.google.android.gms.measurement.internal.zzx r10 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r11 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r11 = r11.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzae r10 = r10.zzc(r11, r9)     // Catch:{ all -> 0x0efa }
            r4.put(r9, r10)     // Catch:{ all -> 0x0efa }
        L_0x09cd:
            java.lang.Long r9 = r10.zzfm     // Catch:{ all -> 0x0efa }
            if (r9 != 0) goto L_0x0a0c
            java.lang.Long r9 = r10.zzfn     // Catch:{ all -> 0x0efa }
            long r11 = r9.longValue()     // Catch:{ all -> 0x0efa }
            r13 = 1
            int r9 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r9 <= 0) goto L_0x09e7
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = "_sr"
            java.lang.Long r11 = r10.zzfn     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzjo.zza(r8, r9, r11)     // Catch:{ all -> 0x0efa }
        L_0x09e7:
            java.lang.Boolean r9 = r10.zzfo     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0a01
            java.lang.Boolean r9 = r10.zzfo     // Catch:{ all -> 0x0efa }
            boolean r9 = r9.booleanValue()     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0a01
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = "_efs"
            r10 = 1
            java.lang.Long r12 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzjo.zza(r8, r9, r12)     // Catch:{ all -> 0x0efa }
        L_0x0a01:
            com.google.android.gms.internal.measurement.zzgi r9 = r8.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r9 = (com.google.android.gms.internal.measurement.zzey) r9     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0efa }
            r5.add(r9)     // Catch:{ all -> 0x0efa }
        L_0x0a0c:
            r3.zza(r7, r8)     // Catch:{ all -> 0x0efa }
        L_0x0a0f:
            r64 = r2
            r62 = r6
            r1 = r7
            goto L_0x0cbd
        L_0x0a16:
            com.google.android.gms.measurement.internal.zzfd r9 = r65.zzgz()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zztn     // Catch:{ all -> 0x0cfc }
            java.lang.String r10 = r10.zzag()     // Catch:{ all -> 0x0cfc }
            long r9 = r9.zzbb(r10)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzfj r11 = r1.zzj     // Catch:{ all -> 0x0cfc }
            r11.zzz()     // Catch:{ all -> 0x0cfc }
            long r11 = r8.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            long r11 = com.google.android.gms.measurement.internal.zzjs.zzc(r11, r9)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzgi r13 = r8.zzug()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzey r13 = (com.google.android.gms.internal.measurement.zzey) r13     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzbs$zzc r13 = (com.google.android.gms.internal.measurement.zzbs.zzc) r13     // Catch:{ all -> 0x0cfc }
            java.lang.String r14 = "_dbg"
            r45 = r9
            r15 = 1
            java.lang.Long r9 = java.lang.Long.valueOf(r15)     // Catch:{ all -> 0x0cfc }
            boolean r10 = android.text.TextUtils.isEmpty(r14)     // Catch:{ all -> 0x0cfc }
            if (r10 != 0) goto L_0x0a9e
            if (r9 != 0) goto L_0x0a4c
            goto L_0x0a9e
        L_0x0a4c:
            java.util.List r10 = r13.zzmj()     // Catch:{ all -> 0x0efa }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ all -> 0x0efa }
        L_0x0a54:
            boolean r13 = r10.hasNext()     // Catch:{ all -> 0x0efa }
            if (r13 == 0) goto L_0x0a9e
            java.lang.Object r13 = r10.next()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zze r13 = (com.google.android.gms.internal.measurement.zzbs.zze) r13     // Catch:{ all -> 0x0efa }
            java.lang.String r15 = r13.getName()     // Catch:{ all -> 0x0efa }
            boolean r15 = r14.equals(r15)     // Catch:{ all -> 0x0efa }
            if (r15 == 0) goto L_0x0a54
            boolean r10 = r9 instanceof java.lang.Long     // Catch:{ all -> 0x0efa }
            if (r10 == 0) goto L_0x0a7c
            long r14 = r13.zznb()     // Catch:{ all -> 0x0efa }
            java.lang.Long r10 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x0efa }
            boolean r10 = r9.equals(r10)     // Catch:{ all -> 0x0efa }
            if (r10 != 0) goto L_0x0a9c
        L_0x0a7c:
            boolean r10 = r9 instanceof java.lang.String     // Catch:{ all -> 0x0efa }
            if (r10 == 0) goto L_0x0a8a
            java.lang.String r10 = r13.zzmy()     // Catch:{ all -> 0x0efa }
            boolean r10 = r9.equals(r10)     // Catch:{ all -> 0x0efa }
            if (r10 != 0) goto L_0x0a9c
        L_0x0a8a:
            boolean r10 = r9 instanceof java.lang.Double     // Catch:{ all -> 0x0efa }
            if (r10 == 0) goto L_0x0a9e
            double r13 = r13.zzne()     // Catch:{ all -> 0x0efa }
            java.lang.Double r10 = java.lang.Double.valueOf(r13)     // Catch:{ all -> 0x0efa }
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0a9e
        L_0x0a9c:
            r9 = 1
            goto L_0x0a9f
        L_0x0a9e:
            r9 = 0
        L_0x0a9f:
            if (r9 != 0) goto L_0x0ab4
            com.google.android.gms.measurement.internal.zzfd r9 = r65.zzgz()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r10 = r10.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r8.getName()     // Catch:{ all -> 0x0efa }
            int r9 = r9.zzm(r10, r13)     // Catch:{ all -> 0x0efa }
            goto L_0x0ab5
        L_0x0ab4:
            r9 = 1
        L_0x0ab5:
            if (r9 > 0) goto L_0x0ade
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r10 = r10.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r10 = r10.zzgn()     // Catch:{ all -> 0x0efa }
            java.lang.String r11 = "Sample rate must be positive. event, rate"
            java.lang.String r12 = r8.getName()     // Catch:{ all -> 0x0efa }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0efa }
            r10.zza(r11, r12, r9)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r9 = r8.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r9 = (com.google.android.gms.internal.measurement.zzey) r9     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0efa }
            r5.add(r9)     // Catch:{ all -> 0x0efa }
            r3.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            goto L_0x0a0f
        L_0x0ade:
            java.lang.String r10 = r8.getName()     // Catch:{ all -> 0x0cfc }
            java.lang.Object r10 = r4.get(r10)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzae r10 = (com.google.android.gms.measurement.internal.zzae) r10     // Catch:{ all -> 0x0cfc }
            if (r10 != 0) goto L_0x0b74
            com.google.android.gms.measurement.internal.zzx r10 = r65.zzgy()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r13.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r14 = r8.getName()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzae r10 = r10.zzc(r13, r14)     // Catch:{ all -> 0x0efa }
            if (r10 != 0) goto L_0x0b74
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzef r10 = r10.zzab()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzeh r10 = r10.zzgn()     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = "Event being bundled has no eventAggregate. appId, eventName"
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r14 = r14.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r15 = r8.getName()     // Catch:{ all -> 0x0efa }
            r10.zza(r13, r14, r15)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzfj r10 = r1.zzj     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzs r10 = r10.zzad()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = r13.zzag()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.zzak.zziz     // Catch:{ all -> 0x0efa }
            boolean r10 = r10.zze(r13, r14)     // Catch:{ all -> 0x0efa }
            if (r10 == 0) goto L_0x0b51
            com.google.android.gms.measurement.internal.zzae r10 = new com.google.android.gms.measurement.internal.zzae     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r26 = r13.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r27 = r8.getName()     // Catch:{ all -> 0x0efa }
            r28 = 1
            r30 = 1
            r32 = 1
            long r34 = r8.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            r36 = 0
            r38 = 0
            r39 = 0
            r40 = 0
            r41 = 0
            r25 = r10
            r25.<init>(r26, r27, r28, r30, r32, r34, r36, r38, r39, r40, r41)     // Catch:{ all -> 0x0efa }
            goto L_0x0b74
        L_0x0b51:
            com.google.android.gms.measurement.internal.zzae r10 = new com.google.android.gms.measurement.internal.zzae     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = r2.zztn     // Catch:{ all -> 0x0efa }
            java.lang.String r48 = r13.zzag()     // Catch:{ all -> 0x0efa }
            java.lang.String r49 = r8.getName()     // Catch:{ all -> 0x0efa }
            r50 = 1
            r52 = 1
            long r54 = r8.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            r56 = 0
            r58 = 0
            r59 = 0
            r60 = 0
            r61 = 0
            r47 = r10
            r47.<init>(r48, r49, r50, r52, r54, r56, r58, r59, r60, r61)     // Catch:{ all -> 0x0efa }
        L_0x0b74:
            r65.zzgw()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzgi r13 = r8.zzug()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzey r13 = (com.google.android.gms.internal.measurement.zzey) r13     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzbs$zzc r13 = (com.google.android.gms.internal.measurement.zzbs.zzc) r13     // Catch:{ all -> 0x0cfc }
            java.lang.String r14 = "_eid"
            java.lang.Object r13 = com.google.android.gms.measurement.internal.zzjo.zzb(r13, r14)     // Catch:{ all -> 0x0cfc }
            java.lang.Long r13 = (java.lang.Long) r13     // Catch:{ all -> 0x0cfc }
            if (r13 == 0) goto L_0x0b8b
            r14 = 1
            goto L_0x0b8c
        L_0x0b8b:
            r14 = 0
        L_0x0b8c:
            java.lang.Boolean r14 = java.lang.Boolean.valueOf(r14)     // Catch:{ all -> 0x0cfc }
            r15 = 1
            if (r9 != r15) goto L_0x0bc1
            com.google.android.gms.internal.measurement.zzgi r9 = r8.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r9 = (com.google.android.gms.internal.measurement.zzey) r9     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0efa }
            r5.add(r9)     // Catch:{ all -> 0x0efa }
            boolean r9 = r14.booleanValue()     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0bbc
            java.lang.Long r9 = r10.zzfm     // Catch:{ all -> 0x0efa }
            if (r9 != 0) goto L_0x0bb0
            java.lang.Long r9 = r10.zzfn     // Catch:{ all -> 0x0efa }
            if (r9 != 0) goto L_0x0bb0
            java.lang.Boolean r9 = r10.zzfo     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0bbc
        L_0x0bb0:
            r9 = 0
            com.google.android.gms.measurement.internal.zzae r10 = r10.zza(r9, r9, r9)     // Catch:{ all -> 0x0efa }
            java.lang.String r9 = r8.getName()     // Catch:{ all -> 0x0efa }
            r4.put(r9, r10)     // Catch:{ all -> 0x0efa }
        L_0x0bbc:
            r3.zza(r7, r8)     // Catch:{ all -> 0x0efa }
            goto L_0x0a0f
        L_0x0bc1:
            int r15 = r6.nextInt(r9)     // Catch:{ all -> 0x0cfc }
            if (r15 != 0) goto L_0x0c07
            r65.zzgw()     // Catch:{ all -> 0x0efa }
            java.lang.String r13 = "_sr"
            r62 = r6
            r63 = r7
            long r6 = (long) r9     // Catch:{ all -> 0x0efa }
            java.lang.Long r9 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzjo.zza(r8, r13, r9)     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzgi r9 = r8.zzug()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzey r9 = (com.google.android.gms.internal.measurement.zzey) r9     // Catch:{ all -> 0x0efa }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0efa }
            r5.add(r9)     // Catch:{ all -> 0x0efa }
            boolean r9 = r14.booleanValue()     // Catch:{ all -> 0x0efa }
            if (r9 == 0) goto L_0x0bf2
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0efa }
            r7 = 0
            com.google.android.gms.measurement.internal.zzae r10 = r10.zza(r7, r6, r7)     // Catch:{ all -> 0x0efa }
        L_0x0bf2:
            java.lang.String r6 = r8.getName()     // Catch:{ all -> 0x0efa }
            long r13 = r8.getTimestampMillis()     // Catch:{ all -> 0x0efa }
            com.google.android.gms.measurement.internal.zzae r7 = r10.zza(r13, r11)     // Catch:{ all -> 0x0efa }
            r4.put(r6, r7)     // Catch:{ all -> 0x0efa }
            r64 = r2
        L_0x0c03:
            r1 = r63
            goto L_0x0cba
        L_0x0c07:
            r62 = r6
            r63 = r7
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzs r6 = r6.zzad()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = r2.zztn     // Catch:{ all -> 0x0cfc }
            java.lang.String r7 = r7.zzag()     // Catch:{ all -> 0x0cfc }
            boolean r6 = r6.zzu(r7)     // Catch:{ all -> 0x0cfc }
            if (r6 == 0) goto L_0x0c43
            java.lang.Long r6 = r10.zzfl     // Catch:{ all -> 0x0cfc }
            if (r6 == 0) goto L_0x0c2a
            java.lang.Long r6 = r10.zzfl     // Catch:{ all -> 0x0efa }
            long r6 = r6.longValue()     // Catch:{ all -> 0x0efa }
            r64 = r2
            goto L_0x0c3b
        L_0x0c2a:
            com.google.android.gms.measurement.internal.zzfj r6 = r1.zzj     // Catch:{ all -> 0x0cfc }
            r6.zzz()     // Catch:{ all -> 0x0cfc }
            long r6 = r8.zzmm()     // Catch:{ all -> 0x0cfc }
            r64 = r2
            r1 = r45
            long r6 = com.google.android.gms.measurement.internal.zzjs.zzc(r6, r1)     // Catch:{ all -> 0x0cfc }
        L_0x0c3b:
            int r1 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r1 == 0) goto L_0x0c41
        L_0x0c3f:
            r1 = 1
            goto L_0x0c59
        L_0x0c41:
            r1 = 0
            goto L_0x0c59
        L_0x0c43:
            r64 = r2
            long r1 = r10.zzfk     // Catch:{ all -> 0x0cfc }
            long r6 = r8.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            r15 = 0
            long r6 = r6 - r1
            long r1 = java.lang.Math.abs(r6)     // Catch:{ all -> 0x0cfc }
            r6 = 86400000(0x5265c00, double:4.2687272E-316)
            int r15 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r15 < 0) goto L_0x0c41
            goto L_0x0c3f
        L_0x0c59:
            if (r1 == 0) goto L_0x0ca6
            r65.zzgw()     // Catch:{ all -> 0x0cfc }
            java.lang.String r1 = "_efs"
            r6 = 1
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzjo.zza(r8, r1, r2)     // Catch:{ all -> 0x0cfc }
            r65.zzgw()     // Catch:{ all -> 0x0cfc }
            java.lang.String r1 = "_sr"
            long r6 = (long) r9     // Catch:{ all -> 0x0cfc }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzjo.zza(r8, r1, r2)     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzgi r1 = r8.zzug()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzey r1 = (com.google.android.gms.internal.measurement.zzey) r1     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.internal.measurement.zzbs$zzc r1 = (com.google.android.gms.internal.measurement.zzbs.zzc) r1     // Catch:{ all -> 0x0cfc }
            r5.add(r1)     // Catch:{ all -> 0x0cfc }
            boolean r1 = r14.booleanValue()     // Catch:{ all -> 0x0cfc }
            if (r1 == 0) goto L_0x0c95
            java.lang.Long r1 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0cfc }
            r2 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x0cfc }
            r2 = 0
            com.google.android.gms.measurement.internal.zzae r10 = r10.zza(r2, r1, r6)     // Catch:{ all -> 0x0cfc }
        L_0x0c95:
            java.lang.String r1 = r8.getName()     // Catch:{ all -> 0x0cfc }
            long r6 = r8.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzae r2 = r10.zza(r6, r11)     // Catch:{ all -> 0x0cfc }
            r4.put(r1, r2)     // Catch:{ all -> 0x0cfc }
            goto L_0x0c03
        L_0x0ca6:
            boolean r1 = r14.booleanValue()     // Catch:{ all -> 0x0cfc }
            if (r1 == 0) goto L_0x0c03
            java.lang.String r1 = r8.getName()     // Catch:{ all -> 0x0cfc }
            r2 = 0
            com.google.android.gms.measurement.internal.zzae r6 = r10.zza(r13, r2, r2)     // Catch:{ all -> 0x0cfc }
            r4.put(r1, r6)     // Catch:{ all -> 0x0cfc }
            goto L_0x0c03
        L_0x0cba:
            r3.zza(r1, r8)     // Catch:{ all -> 0x0cfc }
        L_0x0cbd:
            int r7 = r1 + 1
            r6 = r62
            r2 = r64
            r1 = r65
            goto L_0x0983
        L_0x0cc7:
            r64 = r2
            int r1 = r5.size()     // Catch:{ all -> 0x0cfc }
            int r2 = r3.zznm()     // Catch:{ all -> 0x0cfc }
            if (r1 >= r2) goto L_0x0cda
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r1 = r3.zznn()     // Catch:{ all -> 0x0cfc }
            r1.zza(r5)     // Catch:{ all -> 0x0cfc }
        L_0x0cda:
            java.util.Set r1 = r4.entrySet()     // Catch:{ all -> 0x0cfc }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0cfc }
        L_0x0ce2:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0cfc }
            if (r2 == 0) goto L_0x0d04
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x0cfc }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzx r4 = r65.zzgy()     // Catch:{ all -> 0x0cfc }
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x0cfc }
            com.google.android.gms.measurement.internal.zzae r2 = (com.google.android.gms.measurement.internal.zzae) r2     // Catch:{ all -> 0x0cfc }
            r4.zza(r2)     // Catch:{ all -> 0x0cfc }
            goto L_0x0ce2
        L_0x0cfc:
            r0 = move-exception
            r1 = r0
            r5 = r65
            goto L_0x0efd
        L_0x0d02:
            r64 = r2
        L_0x0d04:
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r1 = r3.zzao(r1)     // Catch:{ all -> 0x0ed8 }
            r4 = -9223372036854775808
            r1.zzap(r4)     // Catch:{ all -> 0x0ed8 }
            r1 = 0
        L_0x0d13:
            int r2 = r3.zznm()     // Catch:{ all -> 0x0ed8 }
            if (r1 >= r2) goto L_0x0d46
            com.google.android.gms.internal.measurement.zzbs$zzc r2 = r3.zzq(r1)     // Catch:{ all -> 0x0cfc }
            long r4 = r2.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            long r6 = r3.zznq()     // Catch:{ all -> 0x0cfc }
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x0d30
            long r4 = r2.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            r3.zzao(r4)     // Catch:{ all -> 0x0cfc }
        L_0x0d30:
            long r4 = r2.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            long r6 = r3.zznr()     // Catch:{ all -> 0x0cfc }
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 <= 0) goto L_0x0d43
            long r4 = r2.getTimestampMillis()     // Catch:{ all -> 0x0cfc }
            r3.zzap(r4)     // Catch:{ all -> 0x0cfc }
        L_0x0d43:
            int r1 = r1 + 1
            goto L_0x0d13
        L_0x0d46:
            r1 = r64
            com.google.android.gms.internal.measurement.zzbs$zzg r2 = r1.zztn     // Catch:{ all -> 0x0ed8 }
            java.lang.String r2 = r2.zzag()     // Catch:{ all -> 0x0ed8 }
            com.google.android.gms.measurement.internal.zzx r4 = r65.zzgy()     // Catch:{ all -> 0x0ed8 }
            com.google.android.gms.measurement.internal.zzf r4 = r4.zzab(r2)     // Catch:{ all -> 0x0ed8 }
            if (r4 != 0) goto L_0x0d74
            r5 = r65
            com.google.android.gms.measurement.internal.zzfj r4 = r5.zzj     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzef r4 = r4.zzab()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgk()     // Catch:{ all -> 0x0ef7 }
            java.lang.String r6 = "Bundling raw events w/o app info. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = r1.zztn     // Catch:{ all -> 0x0ef7 }
            java.lang.String r7 = r7.zzag()     // Catch:{ all -> 0x0ef7 }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzef.zzam(r7)     // Catch:{ all -> 0x0ef7 }
            r4.zza(r6, r7)     // Catch:{ all -> 0x0ef7 }
            goto L_0x0dd1
        L_0x0d74:
            r5 = r65
            int r6 = r3.zznm()     // Catch:{ all -> 0x0ef7 }
            if (r6 <= 0) goto L_0x0dd1
            long r6 = r4.zzak()     // Catch:{ all -> 0x0ef7 }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0d8a
            r3.zzar(r6)     // Catch:{ all -> 0x0ef7 }
            goto L_0x0d8d
        L_0x0d8a:
            r3.zznt()     // Catch:{ all -> 0x0ef7 }
        L_0x0d8d:
            long r8 = r4.zzaj()     // Catch:{ all -> 0x0ef7 }
            r10 = 0
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x0d98
            goto L_0x0d99
        L_0x0d98:
            r6 = r8
        L_0x0d99:
            int r8 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x0da1
            r3.zzaq(r6)     // Catch:{ all -> 0x0ef7 }
            goto L_0x0da4
        L_0x0da1:
            r3.zzns()     // Catch:{ all -> 0x0ef7 }
        L_0x0da4:
            r4.zzau()     // Catch:{ all -> 0x0ef7 }
            long r6 = r4.zzar()     // Catch:{ all -> 0x0ef7 }
            int r6 = (int) r6     // Catch:{ all -> 0x0ef7 }
            r3.zzu(r6)     // Catch:{ all -> 0x0ef7 }
            long r6 = r3.zznq()     // Catch:{ all -> 0x0ef7 }
            r4.zze(r6)     // Catch:{ all -> 0x0ef7 }
            long r6 = r3.zznr()     // Catch:{ all -> 0x0ef7 }
            r4.zzf(r6)     // Catch:{ all -> 0x0ef7 }
            java.lang.String r6 = r4.zzbc()     // Catch:{ all -> 0x0ef7 }
            if (r6 == 0) goto L_0x0dc7
            r3.zzcl(r6)     // Catch:{ all -> 0x0ef7 }
            goto L_0x0dca
        L_0x0dc7:
            r3.zznu()     // Catch:{ all -> 0x0ef7 }
        L_0x0dca:
            com.google.android.gms.measurement.internal.zzx r6 = r65.zzgy()     // Catch:{ all -> 0x0ef7 }
            r6.zza(r4)     // Catch:{ all -> 0x0ef7 }
        L_0x0dd1:
            int r4 = r3.zznm()     // Catch:{ all -> 0x0ef7 }
            if (r4 <= 0) goto L_0x0e37
            com.google.android.gms.measurement.internal.zzfj r4 = r5.zzj     // Catch:{ all -> 0x0ef7 }
            r4.zzae()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzfd r4 = r65.zzgz()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r1.zztn     // Catch:{ all -> 0x0ef7 }
            java.lang.String r6 = r6.zzag()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.internal.measurement.zzbw r4 = r4.zzaw(r6)     // Catch:{ all -> 0x0ef7 }
            if (r4 == 0) goto L_0x0dfb
            java.lang.Long r6 = r4.zzzk     // Catch:{ all -> 0x0ef7 }
            if (r6 != 0) goto L_0x0df1
            goto L_0x0dfb
        L_0x0df1:
            java.lang.Long r4 = r4.zzzk     // Catch:{ all -> 0x0ef7 }
            long r6 = r4.longValue()     // Catch:{ all -> 0x0ef7 }
            r3.zzav(r6)     // Catch:{ all -> 0x0ef7 }
            goto L_0x0e26
        L_0x0dfb:
            com.google.android.gms.internal.measurement.zzbs$zzg r4 = r1.zztn     // Catch:{ all -> 0x0ef7 }
            java.lang.String r4 = r4.getGmpAppId()     // Catch:{ all -> 0x0ef7 }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0ef7 }
            if (r4 == 0) goto L_0x0e0d
            r6 = -1
            r3.zzav(r6)     // Catch:{ all -> 0x0ef7 }
            goto L_0x0e26
        L_0x0e0d:
            com.google.android.gms.measurement.internal.zzfj r4 = r5.zzj     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzef r4 = r4.zzab()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgn()     // Catch:{ all -> 0x0ef7 }
            java.lang.String r6 = "Did not find measurement config or missing version info. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = r1.zztn     // Catch:{ all -> 0x0ef7 }
            java.lang.String r7 = r7.zzag()     // Catch:{ all -> 0x0ef7 }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzef.zzam(r7)     // Catch:{ all -> 0x0ef7 }
            r4.zza(r6, r7)     // Catch:{ all -> 0x0ef7 }
        L_0x0e26:
            com.google.android.gms.measurement.internal.zzx r4 = r65.zzgy()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.internal.measurement.zzgi r3 = r3.zzug()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.internal.measurement.zzey r3 = (com.google.android.gms.internal.measurement.zzey) r3     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = (com.google.android.gms.internal.measurement.zzbs.zzg) r3     // Catch:{ all -> 0x0ef7 }
            r11 = r24
            r4.zza(r3, r11)     // Catch:{ all -> 0x0ef7 }
        L_0x0e37:
            com.google.android.gms.measurement.internal.zzx r3 = r65.zzgy()     // Catch:{ all -> 0x0ef7 }
            java.util.List<java.lang.Long> r1 = r1.zzto     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r1)     // Catch:{ all -> 0x0ef7 }
            r3.zzo()     // Catch:{ all -> 0x0ef7 }
            r3.zzbi()     // Catch:{ all -> 0x0ef7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0ef7 }
            java.lang.String r6 = "rowid in ("
            r4.<init>(r6)     // Catch:{ all -> 0x0ef7 }
            r6 = 0
        L_0x0e4e:
            int r7 = r1.size()     // Catch:{ all -> 0x0ef7 }
            if (r6 >= r7) goto L_0x0e6b
            if (r6 == 0) goto L_0x0e5b
            java.lang.String r7 = ","
            r4.append(r7)     // Catch:{ all -> 0x0ef7 }
        L_0x0e5b:
            java.lang.Object r7 = r1.get(r6)     // Catch:{ all -> 0x0ef7 }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0ef7 }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0ef7 }
            r4.append(r7)     // Catch:{ all -> 0x0ef7 }
            int r6 = r6 + 1
            goto L_0x0e4e
        L_0x0e6b:
            java.lang.String r6 = ")"
            r4.append(r6)     // Catch:{ all -> 0x0ef7 }
            android.database.sqlite.SQLiteDatabase r6 = r3.getWritableDatabase()     // Catch:{ all -> 0x0ef7 }
            java.lang.String r7 = "raw_events"
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0ef7 }
            r8 = 0
            int r4 = r6.delete(r7, r4, r8)     // Catch:{ all -> 0x0ef7 }
            int r6 = r1.size()     // Catch:{ all -> 0x0ef7 }
            if (r4 == r6) goto L_0x0e9e
            com.google.android.gms.measurement.internal.zzef r3 = r3.zzab()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzeh r3 = r3.zzgk()     // Catch:{ all -> 0x0ef7 }
            java.lang.String r6 = "Deleted fewer rows from raw events table than expected"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0ef7 }
            int r1 = r1.size()     // Catch:{ all -> 0x0ef7 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0ef7 }
            r3.zza(r6, r4, r1)     // Catch:{ all -> 0x0ef7 }
        L_0x0e9e:
            com.google.android.gms.measurement.internal.zzx r1 = r65.zzgy()     // Catch:{ all -> 0x0ef7 }
            android.database.sqlite.SQLiteDatabase r3 = r1.getWritableDatabase()     // Catch:{ all -> 0x0ef7 }
            java.lang.String r4 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0eb5 }
            r7 = 0
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0eb5 }
            r7 = 1
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0eb5 }
            r3.execSQL(r4, r6)     // Catch:{ SQLiteException -> 0x0eb5 }
            goto L_0x0ec8
        L_0x0eb5:
            r0 = move-exception
            r3 = r0
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgk()     // Catch:{ all -> 0x0ef7 }
            java.lang.String r4 = "Failed to remove unused event metadata. appId"
            java.lang.Object r2 = com.google.android.gms.measurement.internal.zzef.zzam(r2)     // Catch:{ all -> 0x0ef7 }
            r1.zza(r4, r2, r3)     // Catch:{ all -> 0x0ef7 }
        L_0x0ec8:
            com.google.android.gms.measurement.internal.zzx r1 = r65.zzgy()     // Catch:{ all -> 0x0ef7 }
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzx r1 = r65.zzgy()
            r1.endTransaction()
            r1 = 1
            return r1
        L_0x0ed8:
            r0 = move-exception
            r5 = r65
            goto L_0x0efc
        L_0x0edc:
            r5 = r1
            com.google.android.gms.measurement.internal.zzx r1 = r65.zzgy()     // Catch:{ all -> 0x0ef7 }
            r1.setTransactionSuccessful()     // Catch:{ all -> 0x0ef7 }
            com.google.android.gms.measurement.internal.zzx r1 = r65.zzgy()
            r1.endTransaction()
            r1 = 0
            return r1
        L_0x0eed:
            r0 = move-exception
        L_0x0eee:
            r5 = r1
            goto L_0x025f
        L_0x0ef1:
            if (r8 == 0) goto L_0x0ef9
            r8.close()     // Catch:{ all -> 0x0ef7 }
            goto L_0x0ef9
        L_0x0ef7:
            r0 = move-exception
            goto L_0x0efc
        L_0x0ef9:
            throw r1     // Catch:{ all -> 0x0ef7 }
        L_0x0efa:
            r0 = move-exception
            r5 = r1
        L_0x0efc:
            r1 = r0
        L_0x0efd:
            com.google.android.gms.measurement.internal.zzx r2 = r65.zzgy()
            r2.endTransaction()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzjg.zzd(java.lang.String, long):boolean");
    }

    @VisibleForTesting
    private final void zza(zzbs.zzg.zza zza2, long j, boolean z) {
        zzjp zzjp;
        String str = "_lte";
        if (z) {
            str = "_se";
        }
        zzjp zze = zzgy().zze(zza2.zzag(), str);
        if (zze == null || zze.value == null) {
            zzjp = new zzjp(zza2.zzag(), "auto", str, this.zzj.zzx().currentTimeMillis(), Long.valueOf(j));
        } else {
            zzjp = new zzjp(zza2.zzag(), "auto", str, this.zzj.zzx().currentTimeMillis(), Long.valueOf(((Long) zze.value).longValue() + j));
        }
        zzbs.zzk zzk = (zzbs.zzk) ((zzey) zzbs.zzk.zzqu().zzdb(str).zzbk(this.zzj.zzx().currentTimeMillis()).zzbl(((Long) zzjp.value).longValue()).zzug());
        boolean z2 = false;
        int i = 0;
        while (true) {
            if (i >= zza2.zznp()) {
                break;
            } else if (str.equals(zza2.zzs(i).getName())) {
                zza2.zza(i, zzk);
                z2 = true;
                break;
            } else {
                i++;
            }
        }
        if (!z2) {
            zza2.zza(zzk);
        }
        if (j > 0) {
            zzgy().zza(zzjp);
            String str2 = "lifetime";
            if (z) {
                str2 = "session-scoped";
            }
            this.zzj.zzab().zzgr().zza("Updated engagement user property. scope, value", str2, zzjp.value);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzjo.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void
     arg types: [com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.zzjo.zza(boolean, boolean, boolean):java.lang.String
      com.google.android.gms.measurement.internal.zzjo.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.zzbk$zzb):void
      com.google.android.gms.measurement.internal.zzjo.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void */
    private final boolean zza(zzbs.zzc.zza zza2, zzbs.zzc.zza zza3) {
        String str;
        Preconditions.checkArgument("_e".equals(zza2.getName()));
        zzgw();
        zzbs.zze zza4 = zzjo.zza((zzbs.zzc) ((zzey) zza2.zzug()), "_sc");
        String str2 = null;
        if (zza4 == null) {
            str = null;
        } else {
            str = zza4.zzmy();
        }
        zzgw();
        zzbs.zze zza5 = zzjo.zza((zzbs.zzc) ((zzey) zza3.zzug()), "_pc");
        if (zza5 != null) {
            str2 = zza5.zzmy();
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        zzgw();
        zzbs.zze zza6 = zzjo.zza((zzbs.zzc) ((zzey) zza2.zzug()), "_et");
        if (!zza6.zzna() || zza6.zznb() <= 0) {
            return true;
        }
        long zznb = zza6.zznb();
        zzgw();
        zzbs.zze zza7 = zzjo.zza((zzbs.zzc) ((zzey) zza3.zzug()), "_et");
        if (zza7 != null && zza7.zznb() > 0) {
            zznb += zza7.zznb();
        }
        zzgw();
        zzjo.zza(zza3, "_et", Long.valueOf(zznb));
        zzgw();
        zzjo.zza(zza2, "_fr", (Object) 1L);
        return true;
    }

    @VisibleForTesting
    private static void zza(zzbs.zzc.zza zza2, @NonNull String str) {
        List<zzbs.zze> zzmj = zza2.zzmj();
        for (int i = 0; i < zzmj.size(); i++) {
            if (str.equals(zzmj.get(i).getName())) {
                zza2.zzm(i);
                return;
            }
        }
    }

    @VisibleForTesting
    private static void zza(zzbs.zzc.zza zza2, int i, String str) {
        List<zzbs.zze> zzmj = zza2.zzmj();
        int i2 = 0;
        while (i2 < zzmj.size()) {
            if (!"_err".equals(zzmj.get(i2).getName())) {
                i2++;
            } else {
                return;
            }
        }
        zza2.zza((zzbs.zze) ((zzey) zzbs.zze.zzng().zzbz("_err").zzam(Long.valueOf((long) i).longValue()).zzug())).zza((zzbs.zze) ((zzey) zzbs.zze.zzng().zzbz("_ev").zzca(str).zzug()));
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    @WorkerThread
    @VisibleForTesting
    public final void zza(int i, Throwable th, byte[] bArr, String str) {
        zzx zzgy;
        zzo();
        zzjj();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.zztd = false;
                zzjo();
                throw th2;
            }
        }
        List<Long> list = this.zzth;
        this.zzth = null;
        boolean z = true;
        if ((i == 200 || i == 204) && th == null) {
            try {
                this.zzj.zzac().zzlj.set(this.zzj.zzx().currentTimeMillis());
                this.zzj.zzac().zzlk.set(0);
                zzjn();
                this.zzj.zzab().zzgs().zza("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                zzgy().beginTransaction();
                try {
                    for (Long next : list) {
                        try {
                            zzgy = zzgy();
                            long longValue = next.longValue();
                            zzgy.zzo();
                            zzgy.zzbi();
                            if (zzgy.getWritableDatabase().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e) {
                            zzgy.zzab().zzgk().zza("Failed to delete a bundle in a queue table", e);
                            throw e;
                        } catch (SQLiteException e2) {
                            if (this.zzti == null || !this.zzti.contains(next)) {
                                throw e2;
                            }
                        }
                    }
                    zzgy().setTransactionSuccessful();
                    zzgy().endTransaction();
                    this.zzti = null;
                    if (!zzjf().zzgv() || !zzjm()) {
                        this.zztj = -1;
                        zzjn();
                    } else {
                        zzjl();
                    }
                    this.zzsy = 0;
                } catch (Throwable th3) {
                    zzgy().endTransaction();
                    throw th3;
                }
            } catch (SQLiteException e3) {
                this.zzj.zzab().zzgk().zza("Database error while trying to delete uploaded bundles", e3);
                this.zzsy = this.zzj.zzx().elapsedRealtime();
                this.zzj.zzab().zzgs().zza("Disable upload, time", Long.valueOf(this.zzsy));
            }
        } else {
            this.zzj.zzab().zzgs().zza("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            this.zzj.zzac().zzlk.set(this.zzj.zzx().currentTimeMillis());
            if (i != 503) {
                if (i != 429) {
                    z = false;
                }
            }
            if (z) {
                this.zzj.zzac().zzll.set(this.zzj.zzx().currentTimeMillis());
            }
            zzgy().zzb(list);
            zzjn();
        }
        this.zztd = false;
        zzjo();
    }

    private final boolean zzjm() {
        zzo();
        zzjj();
        return zzgy().zzcd() || !TextUtils.isEmpty(zzgy().zzby());
    }

    @WorkerThread
    private final void zzb(zzf zzf) {
        zzo();
        if (!TextUtils.isEmpty(zzf.getGmpAppId()) || (zzs.zzbx() && !TextUtils.isEmpty(zzf.zzah()))) {
            zzs zzad = this.zzj.zzad();
            Uri.Builder builder = new Uri.Builder();
            String gmpAppId = zzf.getGmpAppId();
            if (TextUtils.isEmpty(gmpAppId) && zzs.zzbx()) {
                gmpAppId = zzf.zzah();
            }
            ArrayMap arrayMap = null;
            Uri.Builder encodedAuthority = builder.scheme(zzak.zzgj.get(null)).encodedAuthority(zzak.zzgk.get(null));
            String valueOf = String.valueOf(gmpAppId);
            encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", zzf.getAppInstanceId()).appendQueryParameter(TapjoyConstants.TJC_PLATFORM, "android").appendQueryParameter("gmp_version", String.valueOf(zzad.zzao()));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.zzj.zzab().zzgs().zza("Fetching remote configuration", zzf.zzag());
                zzbw zzaw = zzgz().zzaw(zzf.zzag());
                String zzax = zzgz().zzax(zzf.zzag());
                if (zzaw != null && !TextUtils.isEmpty(zzax)) {
                    arrayMap = new ArrayMap();
                    arrayMap.put("If-Modified-Since", zzax);
                }
                this.zztc = true;
                zzej zzjf = zzjf();
                String zzag = zzf.zzag();
                zzjl zzjl = new zzjl(this);
                zzjf.zzo();
                zzjf.zzbi();
                Preconditions.checkNotNull(url);
                Preconditions.checkNotNull(zzjl);
                zzjf.zzaa().zzb(new zzen(zzjf, zzag, url, null, arrayMap, zzjl));
            } catch (MalformedURLException unused) {
                this.zzj.zzab().zzgk().zza("Failed to parse config URL. Not fetching. appId", zzef.zzam(zzf.zzag()), uri);
            }
        } else {
            zzb(zzf.zzag(), 204, null, null, null);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x013e A[Catch:{ all -> 0x0191, all -> 0x000f }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x014e A[Catch:{ all -> 0x0191, all -> 0x000f }] */
    @android.support.annotation.WorkerThread
    @com.google.android.gms.common.util.VisibleForTesting
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzb(java.lang.String r7, int r8, java.lang.Throwable r9, byte[] r10, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r11) {
        /*
            r6 = this;
            r6.zzo()
            r6.zzjj()
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r7)
            r0 = 0
            if (r10 != 0) goto L_0x0012
            byte[] r10 = new byte[r0]     // Catch:{ all -> 0x000f }
            goto L_0x0012
        L_0x000f:
            r7 = move-exception
            goto L_0x019a
        L_0x0012:
            com.google.android.gms.measurement.internal.zzfj r1 = r6.zzj     // Catch:{ all -> 0x000f }
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()     // Catch:{ all -> 0x000f }
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()     // Catch:{ all -> 0x000f }
            java.lang.String r2 = "onConfigFetched. Response size"
            int r3 = r10.length     // Catch:{ all -> 0x000f }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x000f }
            r1.zza(r2, r3)     // Catch:{ all -> 0x000f }
            com.google.android.gms.measurement.internal.zzx r1 = r6.zzgy()     // Catch:{ all -> 0x000f }
            r1.beginTransaction()     // Catch:{ all -> 0x000f }
            com.google.android.gms.measurement.internal.zzx r1 = r6.zzgy()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzf r1 = r1.zzab(r7)     // Catch:{ all -> 0x0191 }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 1
            r4 = 304(0x130, float:4.26E-43)
            if (r8 == r2) goto L_0x0042
            r2 = 204(0xcc, float:2.86E-43)
            if (r8 == r2) goto L_0x0042
            if (r8 != r4) goto L_0x0046
        L_0x0042:
            if (r9 != 0) goto L_0x0046
            r2 = 1
            goto L_0x0047
        L_0x0046:
            r2 = 0
        L_0x0047:
            if (r1 != 0) goto L_0x005e
            com.google.android.gms.measurement.internal.zzfj r8 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzef r8 = r8.zzab()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzeh r8 = r8.zzgn()     // Catch:{ all -> 0x0191 }
            java.lang.String r9 = "App does not exist in onConfigFetched. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzef.zzam(r7)     // Catch:{ all -> 0x0191 }
            r8.zza(r9, r7)     // Catch:{ all -> 0x0191 }
            goto L_0x017d
        L_0x005e:
            r5 = 404(0x194, float:5.66E-43)
            if (r2 != 0) goto L_0x00ce
            if (r8 != r5) goto L_0x0065
            goto L_0x00ce
        L_0x0065:
            com.google.android.gms.measurement.internal.zzfj r10 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.common.util.Clock r10 = r10.zzx()     // Catch:{ all -> 0x0191 }
            long r10 = r10.currentTimeMillis()     // Catch:{ all -> 0x0191 }
            r1.zzm(r10)     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzx r10 = r6.zzgy()     // Catch:{ all -> 0x0191 }
            r10.zza(r1)     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzfj r10 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzef r10 = r10.zzab()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzeh r10 = r10.zzgs()     // Catch:{ all -> 0x0191 }
            java.lang.String r11 = "Fetching config failed. code, error"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0191 }
            r10.zza(r11, r1, r9)     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzfd r9 = r6.zzgz()     // Catch:{ all -> 0x0191 }
            r9.zzay(r7)     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzfj r7 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzeo r7 = r7.zzac()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzet r7 = r7.zzlk     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzfj r9 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.common.util.Clock r9 = r9.zzx()     // Catch:{ all -> 0x0191 }
            long r9 = r9.currentTimeMillis()     // Catch:{ all -> 0x0191 }
            r7.set(r9)     // Catch:{ all -> 0x0191 }
            r7 = 503(0x1f7, float:7.05E-43)
            if (r8 == r7) goto L_0x00b2
            r7 = 429(0x1ad, float:6.01E-43)
            if (r8 != r7) goto L_0x00b1
            goto L_0x00b2
        L_0x00b1:
            r3 = 0
        L_0x00b2:
            if (r3 == 0) goto L_0x00c9
            com.google.android.gms.measurement.internal.zzfj r7 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzeo r7 = r7.zzac()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzet r7 = r7.zzll     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzfj r8 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.common.util.Clock r8 = r8.zzx()     // Catch:{ all -> 0x0191 }
            long r8 = r8.currentTimeMillis()     // Catch:{ all -> 0x0191 }
            r7.set(r8)     // Catch:{ all -> 0x0191 }
        L_0x00c9:
            r6.zzjn()     // Catch:{ all -> 0x0191 }
            goto L_0x017d
        L_0x00ce:
            r9 = 0
            if (r11 == 0) goto L_0x00da
            java.lang.String r2 = "Last-Modified"
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x0191 }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x0191 }
            goto L_0x00db
        L_0x00da:
            r11 = r9
        L_0x00db:
            if (r11 == 0) goto L_0x00ea
            int r2 = r11.size()     // Catch:{ all -> 0x0191 }
            if (r2 <= 0) goto L_0x00ea
            java.lang.Object r11 = r11.get(r0)     // Catch:{ all -> 0x0191 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0191 }
            goto L_0x00eb
        L_0x00ea:
            r11 = r9
        L_0x00eb:
            if (r8 == r5) goto L_0x0107
            if (r8 != r4) goto L_0x00f0
            goto L_0x0107
        L_0x00f0:
            com.google.android.gms.measurement.internal.zzfd r9 = r6.zzgz()     // Catch:{ all -> 0x0191 }
            boolean r9 = r9.zza(r7, r10, r11)     // Catch:{ all -> 0x0191 }
            if (r9 != 0) goto L_0x0128
            com.google.android.gms.measurement.internal.zzx r7 = r6.zzgy()     // Catch:{ all -> 0x000f }
            r7.endTransaction()     // Catch:{ all -> 0x000f }
            r6.zztc = r0
            r6.zzjo()
            return
        L_0x0107:
            com.google.android.gms.measurement.internal.zzfd r11 = r6.zzgz()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.internal.measurement.zzbw r11 = r11.zzaw(r7)     // Catch:{ all -> 0x0191 }
            if (r11 != 0) goto L_0x0128
            com.google.android.gms.measurement.internal.zzfd r11 = r6.zzgz()     // Catch:{ all -> 0x0191 }
            boolean r9 = r11.zza(r7, r9, r9)     // Catch:{ all -> 0x0191 }
            if (r9 != 0) goto L_0x0128
            com.google.android.gms.measurement.internal.zzx r7 = r6.zzgy()     // Catch:{ all -> 0x000f }
            r7.endTransaction()     // Catch:{ all -> 0x000f }
            r6.zztc = r0
            r6.zzjo()
            return
        L_0x0128:
            com.google.android.gms.measurement.internal.zzfj r9 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.common.util.Clock r9 = r9.zzx()     // Catch:{ all -> 0x0191 }
            long r2 = r9.currentTimeMillis()     // Catch:{ all -> 0x0191 }
            r1.zzl(r2)     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzx r9 = r6.zzgy()     // Catch:{ all -> 0x0191 }
            r9.zza(r1)     // Catch:{ all -> 0x0191 }
            if (r8 != r5) goto L_0x014e
            com.google.android.gms.measurement.internal.zzfj r8 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzef r8 = r8.zzab()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzeh r8 = r8.zzgp()     // Catch:{ all -> 0x0191 }
            java.lang.String r9 = "Config not found. Using empty config. appId"
            r8.zza(r9, r7)     // Catch:{ all -> 0x0191 }
            goto L_0x0166
        L_0x014e:
            com.google.android.gms.measurement.internal.zzfj r7 = r6.zzj     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzef r7 = r7.zzab()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzeh r7 = r7.zzgs()     // Catch:{ all -> 0x0191 }
            java.lang.String r9 = "Successfully fetched config. Got network response. code, size"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0191 }
            int r10 = r10.length     // Catch:{ all -> 0x0191 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0191 }
            r7.zza(r9, r8, r10)     // Catch:{ all -> 0x0191 }
        L_0x0166:
            com.google.android.gms.measurement.internal.zzej r7 = r6.zzjf()     // Catch:{ all -> 0x0191 }
            boolean r7 = r7.zzgv()     // Catch:{ all -> 0x0191 }
            if (r7 == 0) goto L_0x017a
            boolean r7 = r6.zzjm()     // Catch:{ all -> 0x0191 }
            if (r7 == 0) goto L_0x017a
            r6.zzjl()     // Catch:{ all -> 0x0191 }
            goto L_0x017d
        L_0x017a:
            r6.zzjn()     // Catch:{ all -> 0x0191 }
        L_0x017d:
            com.google.android.gms.measurement.internal.zzx r7 = r6.zzgy()     // Catch:{ all -> 0x0191 }
            r7.setTransactionSuccessful()     // Catch:{ all -> 0x0191 }
            com.google.android.gms.measurement.internal.zzx r7 = r6.zzgy()     // Catch:{ all -> 0x000f }
            r7.endTransaction()     // Catch:{ all -> 0x000f }
            r6.zztc = r0
            r6.zzjo()
            return
        L_0x0191:
            r7 = move-exception
            com.google.android.gms.measurement.internal.zzx r8 = r6.zzgy()     // Catch:{ all -> 0x000f }
            r8.endTransaction()     // Catch:{ all -> 0x000f }
            throw r7     // Catch:{ all -> 0x000f }
        L_0x019a:
            r6.zztc = r0
            r6.zzjo()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzjg.zzb(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c1  */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzjn() {
        /*
            r21 = this;
            r0 = r21
            r21.zzo()
            r21.zzjj()
            boolean r1 = r21.zzjr()
            if (r1 != 0) goto L_0x001d
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzs r1 = r1.zzad()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzak.zzim
            boolean r1 = r1.zza(r2)
            if (r1 != 0) goto L_0x001d
            return
        L_0x001d:
            long r1 = r0.zzsy
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0062
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.common.util.Clock r1 = r1.zzx()
            long r1 = r1.elapsedRealtime()
            r5 = 3600000(0x36ee80, double:1.7786363E-317)
            long r7 = r0.zzsy
            long r1 = r1 - r7
            long r1 = java.lang.Math.abs(r1)
            long r5 = r5 - r1
            int r1 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0060
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "Upload has been suspended. Will update scheduling later in approximately ms"
            java.lang.Long r3 = java.lang.Long.valueOf(r5)
            r1.zza(r2, r3)
            com.google.android.gms.measurement.internal.zzem r1 = r21.zzjg()
            r1.unregister()
            com.google.android.gms.measurement.internal.zzjc r1 = r21.zzjh()
            r1.cancel()
            return
        L_0x0060:
            r0.zzsy = r3
        L_0x0062:
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            boolean r1 = r1.zzie()
            if (r1 == 0) goto L_0x026c
            boolean r1 = r21.zzjm()
            if (r1 != 0) goto L_0x0072
            goto L_0x026c
        L_0x0072:
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.common.util.Clock r1 = r1.zzx()
            long r1 = r1.currentTimeMillis()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r5 = com.google.android.gms.measurement.internal.zzak.zzhf
            r6 = 0
            java.lang.Object r5 = r5.get(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r7 = r5.longValue()
            long r7 = java.lang.Math.max(r3, r7)
            com.google.android.gms.measurement.internal.zzx r5 = r21.zzgy()
            boolean r5 = r5.zzce()
            if (r5 != 0) goto L_0x00a4
            com.google.android.gms.measurement.internal.zzx r5 = r21.zzgy()
            boolean r5 = r5.zzbz()
            if (r5 == 0) goto L_0x00a2
            goto L_0x00a4
        L_0x00a2:
            r5 = 0
            goto L_0x00a5
        L_0x00a4:
            r5 = 1
        L_0x00a5:
            if (r5 == 0) goto L_0x00e1
            com.google.android.gms.measurement.internal.zzfj r10 = r0.zzj
            com.google.android.gms.measurement.internal.zzs r10 = r10.zzad()
            java.lang.String r10 = r10.zzbu()
            boolean r11 = android.text.TextUtils.isEmpty(r10)
            if (r11 != 0) goto L_0x00d0
            java.lang.String r11 = ".none."
            boolean r10 = r11.equals(r10)
            if (r10 != 0) goto L_0x00d0
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r10 = com.google.android.gms.measurement.internal.zzak.zzha
            java.lang.Object r10 = r10.get(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00f1
        L_0x00d0:
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r10 = com.google.android.gms.measurement.internal.zzak.zzgz
            java.lang.Object r10 = r10.get(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00f1
        L_0x00e1:
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r10 = com.google.android.gms.measurement.internal.zzak.zzgy
            java.lang.Object r10 = r10.get(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
        L_0x00f1:
            com.google.android.gms.measurement.internal.zzfj r12 = r0.zzj
            com.google.android.gms.measurement.internal.zzeo r12 = r12.zzac()
            com.google.android.gms.measurement.internal.zzet r12 = r12.zzlj
            long r12 = r12.get()
            com.google.android.gms.measurement.internal.zzfj r14 = r0.zzj
            com.google.android.gms.measurement.internal.zzeo r14 = r14.zzac()
            com.google.android.gms.measurement.internal.zzet r14 = r14.zzlk
            long r14 = r14.get()
            com.google.android.gms.measurement.internal.zzx r16 = r21.zzgy()
            r17 = r10
            long r9 = r16.zzcb()
            com.google.android.gms.measurement.internal.zzx r11 = r21.zzgy()
            r19 = r7
            long r6 = r11.zzcc()
            long r6 = java.lang.Math.max(r9, r6)
            int r8 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r8 != 0) goto L_0x0128
        L_0x0125:
            r8 = r3
            goto L_0x019f
        L_0x0128:
            r8 = 0
            long r6 = r6 - r1
            long r6 = java.lang.Math.abs(r6)
            long r6 = r1 - r6
            long r12 = r12 - r1
            long r8 = java.lang.Math.abs(r12)
            long r8 = r1 - r8
            long r14 = r14 - r1
            long r10 = java.lang.Math.abs(r14)
            long r1 = r1 - r10
            long r8 = java.lang.Math.max(r8, r1)
            long r10 = r6 + r19
            if (r5 == 0) goto L_0x014f
            int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x014f
            long r10 = java.lang.Math.min(r6, r8)
            long r10 = r10 + r17
        L_0x014f:
            com.google.android.gms.measurement.internal.zzjo r5 = r21.zzgw()
            r12 = r17
            boolean r5 = r5.zzb(r8, r12)
            if (r5 != 0) goto L_0x015d
            long r8 = r8 + r12
            goto L_0x015e
        L_0x015d:
            r8 = r10
        L_0x015e:
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x019f
            int r5 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x019f
            r5 = 0
        L_0x0167:
            r6 = 20
            com.google.android.gms.measurement.internal.zzdu<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.zzak.zzhh
            r10 = 0
            java.lang.Object r7 = r7.get(r10)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            r11 = 0
            int r7 = java.lang.Math.max(r11, r7)
            int r6 = java.lang.Math.min(r6, r7)
            if (r5 >= r6) goto L_0x0125
            r6 = 1
            long r6 = r6 << r5
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r12 = com.google.android.gms.measurement.internal.zzak.zzhg
            java.lang.Object r12 = r12.get(r10)
            java.lang.Long r12 = (java.lang.Long) r12
            long r12 = r12.longValue()
            long r12 = java.lang.Math.max(r3, r12)
            long r12 = r12 * r6
            long r8 = r8 + r12
            int r6 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x019c
            goto L_0x019f
        L_0x019c:
            int r5 = r5 + 1
            goto L_0x0167
        L_0x019f:
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x01c1
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "Next upload time is 0"
            r1.zzao(r2)
            com.google.android.gms.measurement.internal.zzem r1 = r21.zzjg()
            r1.unregister()
            com.google.android.gms.measurement.internal.zzjc r1 = r21.zzjh()
            r1.cancel()
            return
        L_0x01c1:
            com.google.android.gms.measurement.internal.zzej r1 = r21.zzjf()
            boolean r1 = r1.zzgv()
            if (r1 != 0) goto L_0x01e9
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "No network"
            r1.zzao(r2)
            com.google.android.gms.measurement.internal.zzem r1 = r21.zzjg()
            r1.zzha()
            com.google.android.gms.measurement.internal.zzjc r1 = r21.zzjh()
            r1.cancel()
            return
        L_0x01e9:
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzeo r1 = r1.zzac()
            com.google.android.gms.measurement.internal.zzet r1 = r1.zzll
            long r1 = r1.get()
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r5 = com.google.android.gms.measurement.internal.zzak.zzgw
            r6 = 0
            java.lang.Object r5 = r5.get(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            long r5 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.zzjo r7 = r21.zzgw()
            boolean r7 = r7.zzb(r1, r5)
            if (r7 != 0) goto L_0x0215
            long r1 = r1 + r5
            long r8 = java.lang.Math.max(r8, r1)
        L_0x0215:
            com.google.android.gms.measurement.internal.zzem r1 = r21.zzjg()
            r1.unregister()
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.common.util.Clock r1 = r1.zzx()
            long r1 = r1.currentTimeMillis()
            long r8 = r8 - r1
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 > 0) goto L_0x0251
            com.google.android.gms.measurement.internal.zzdu<java.lang.Long> r1 = com.google.android.gms.measurement.internal.zzak.zzhb
            r2 = 0
            java.lang.Object r1 = r1.get(r2)
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            long r8 = java.lang.Math.max(r3, r1)
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzeo r1 = r1.zzac()
            com.google.android.gms.measurement.internal.zzet r1 = r1.zzlj
            com.google.android.gms.measurement.internal.zzfj r2 = r0.zzj
            com.google.android.gms.common.util.Clock r2 = r2.zzx()
            long r2 = r2.currentTimeMillis()
            r1.set(r2)
        L_0x0251:
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "Upload scheduled in approximately ms"
            java.lang.Long r3 = java.lang.Long.valueOf(r8)
            r1.zza(r2, r3)
            com.google.android.gms.measurement.internal.zzjc r1 = r21.zzjh()
            r1.zzv(r8)
            return
        L_0x026c:
            com.google.android.gms.measurement.internal.zzfj r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzef r1 = r1.zzab()
            com.google.android.gms.measurement.internal.zzeh r1 = r1.zzgs()
            java.lang.String r2 = "Nothing to upload or uploading impossible"
            r1.zzao(r2)
            com.google.android.gms.measurement.internal.zzem r1 = r21.zzjg()
            r1.unregister()
            com.google.android.gms.measurement.internal.zzjc r1 = r21.zzjh()
            r1.cancel()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzjg.zzjn():void");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzf(Runnable runnable) {
        zzo();
        if (this.zzsz == null) {
            this.zzsz = new ArrayList();
        }
        this.zzsz.add(runnable);
    }

    @WorkerThread
    private final void zzjo() {
        zzo();
        if (this.zztc || this.zztd || this.zzte) {
            this.zzj.zzab().zzgs().zza("Not stopping services. fetch, network, upload", Boolean.valueOf(this.zztc), Boolean.valueOf(this.zztd), Boolean.valueOf(this.zzte));
            return;
        }
        this.zzj.zzab().zzgs().zzao("Stopping uploading service(s)");
        if (this.zzsz != null) {
            for (Runnable run : this.zzsz) {
                run.run();
            }
            this.zzsz.clear();
        }
    }

    @WorkerThread
    private final Boolean zzc(zzf zzf) {
        try {
            if (zzf.zzam() != -2147483648L) {
                if (zzf.zzam() == ((long) Wrappers.packageManager(this.zzj.getContext()).getPackageInfo(zzf.zzag(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = Wrappers.packageManager(this.zzj.getContext()).getPackageInfo(zzf.zzag(), 0).versionName;
                if (zzf.zzal() != null && zzf.zzal().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final boolean zzjp() {
        zzo();
        if (!this.zzj.zzad().zza(zzak.zzjh) || this.zztf == null || !this.zztf.isValid()) {
            try {
                this.zztg = new RandomAccessFile(new File(this.zzj.getContext().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
                this.zztf = this.zztg.tryLock();
                if (this.zztf != null) {
                    this.zzj.zzab().zzgs().zzao("Storage concurrent access okay");
                    return true;
                }
                this.zzj.zzab().zzgk().zzao("Storage concurrent data access panic");
                return false;
            } catch (FileNotFoundException e) {
                this.zzj.zzab().zzgk().zza("Failed to acquire storage lock", e);
                return false;
            } catch (IOException e2) {
                this.zzj.zzab().zzgk().zza("Failed to access storage lock file", e2);
                return false;
            } catch (OverlappingFileLockException e3) {
                this.zzj.zzab().zzgn().zza("Storage lock already acquired", e3);
                return false;
            }
        } else {
            this.zzj.zzab().zzgs().zzao("Storage concurrent access okay");
            return true;
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final int zza(FileChannel fileChannel) {
        zzo();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.zzj.zzab().zzgk().zzao("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.zzj.zzab().zzgn().zza("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e) {
            this.zzj.zzab().zzgk().zza("Failed to read from channel", e);
            return 0;
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final boolean zza(int i, FileChannel fileChannel) {
        zzo();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.zzj.zzab().zzgk().zzao("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.zzj.zzab().zzgk().zza("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e) {
            this.zzj.zzab().zzgk().zza("Failed to write to channel", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzjq() {
        zzo();
        zzjj();
        if (!this.zzsx) {
            this.zzsx = true;
            zzo();
            zzjj();
            if ((this.zzj.zzad().zza(zzak.zzim) || zzjr()) && zzjp()) {
                int zza2 = zza(this.zztg);
                int zzgf = this.zzj.zzr().zzgf();
                zzo();
                if (zza2 > zzgf) {
                    this.zzj.zzab().zzgk().zza("Panic: can't downgrade version. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzgf));
                } else if (zza2 < zzgf) {
                    if (zza(zzgf, this.zztg)) {
                        this.zzj.zzab().zzgs().zza("Storage version upgraded. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzgf));
                    } else {
                        this.zzj.zzab().zzgk().zza("Storage version upgrade failed. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzgf));
                    }
                }
            }
        }
        if (!this.zzsw && !this.zzj.zzad().zza(zzak.zzim)) {
            this.zzj.zzab().zzgq().zzao("This instance being marked as an uploader");
            this.zzsw = true;
            zzjn();
        }
    }

    @WorkerThread
    private final boolean zzjr() {
        zzo();
        zzjj();
        return this.zzsw;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    @VisibleForTesting
    public final void zzd(zzn zzn) {
        if (this.zzth != null) {
            this.zzti = new ArrayList();
            this.zzti.addAll(this.zzth);
        }
        zzx zzgy = zzgy();
        String str = zzn.packageName;
        Preconditions.checkNotEmpty(str);
        zzgy.zzo();
        zzgy.zzbi();
        try {
            SQLiteDatabase writableDatabase = zzgy.getWritableDatabase();
            String[] strArr = {str};
            int delete = writableDatabase.delete("apps", "app_id=?", strArr) + 0 + writableDatabase.delete(DataBaseEventsStorage.EventEntry.TABLE_NAME, "app_id=?", strArr) + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("conditional_properties", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("queue", "app_id=?", strArr) + writableDatabase.delete("audience_filter_values", "app_id=?", strArr) + writableDatabase.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                zzgy.zzab().zzgs().zza("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzgy.zzab().zzgk().zza("Error resetting analytics data. appId, error", zzef.zzam(str), e);
        }
        zzn zza2 = zza(this.zzj.getContext(), zzn.packageName, zzn.zzcg, zzn.zzcq, zzn.zzcs, zzn.zzct, zzn.zzdr, zzn.zzcu);
        if (zzn.zzcq) {
            zzf(zza2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, int, long, int, boolean, boolean, int, java.lang.String, ?[OBJECT, ARRAY], int, ?[OBJECT, ARRAY]]
     candidates:
      com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
      com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void */
    private final zzn zza(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j, String str3) {
        String str4;
        int i;
        String str5 = str;
        String str6 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.zzj.zzab().zzgk().zzao("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str6 = packageManager.getInstallerPackageName(str5);
        } catch (IllegalArgumentException unused) {
            this.zzj.zzab().zzgk().zza("Error retrieving installer package name. appId", zzef.zzam(str));
        }
        if (str6 == null) {
            str6 = "manual_install";
        } else if ("com.android.vending".equals(str6)) {
            str6 = "";
        }
        String str7 = str6;
        try {
            PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo(str5, 0);
            if (packageInfo != null) {
                CharSequence applicationLabel = Wrappers.packageManager(context).getApplicationLabel(str5);
                if (!TextUtils.isEmpty(applicationLabel)) {
                    String charSequence = applicationLabel.toString();
                }
                str4 = packageInfo.versionName;
                i = packageInfo.versionCode;
            } else {
                str4 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                i = Integer.MIN_VALUE;
            }
            this.zzj.zzae();
            return new zzn(str, str2, str4, (long) i, str7, this.zzj.zzad().zzao(), this.zzj.zzz().zzc(context, str5), (String) null, z, false, "", 0L, this.zzj.zzad().zzr(str5) ? j : 0, 0, z2, z3, false, str3, (Boolean) null, 0L, (List<String>) null);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.zzj.zzab().zzgk().zza("Error retrieving newly installed package info. appId, appName", zzef.zzam(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzjs.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzjs.zza(long, java.lang.String, java.lang.String):java.net.URL
      com.google.android.gms.measurement.internal.zzjs.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzjs.zza(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzb(zzjn zzjn, zzn zzn) {
        zzae zzc;
        zzo();
        zzjj();
        if (TextUtils.isEmpty(zzn.zzcg) && TextUtils.isEmpty(zzn.zzcu)) {
            return;
        }
        if (!zzn.zzcq) {
            zzg(zzn);
            return;
        }
        int zzbm = this.zzj.zzz().zzbm(zzjn.name);
        if (zzbm != 0) {
            this.zzj.zzz();
            this.zzj.zzz().zza(zzn.packageName, zzbm, "_ev", zzjs.zza(zzjn.name, 24, true), zzjn.name != null ? zzjn.name.length() : 0);
            return;
        }
        int zzc2 = this.zzj.zzz().zzc(zzjn.name, zzjn.getValue());
        if (zzc2 != 0) {
            this.zzj.zzz();
            String zza2 = zzjs.zza(zzjn.name, 24, true);
            Object value = zzjn.getValue();
            this.zzj.zzz().zza(zzn.packageName, zzc2, "_ev", zza2, (value == null || (!(value instanceof String) && !(value instanceof CharSequence))) ? 0 : String.valueOf(value).length());
            return;
        }
        Object zzd = this.zzj.zzz().zzd(zzjn.name, zzjn.getValue());
        if (zzd != null) {
            if ("_sid".equals(zzjn.name) && this.zzj.zzad().zzw(zzn.packageName)) {
                long j = zzjn.zztr;
                String str = zzjn.origin;
                long j2 = 0;
                zzjp zze = zzgy().zze(zzn.packageName, "_sno");
                if (zze == null || !(zze.value instanceof Long)) {
                    if (zze != null) {
                        this.zzj.zzab().zzgn().zza("Retrieved last session number from database does not contain a valid (long) value", zze.value);
                    }
                    if (this.zzj.zzad().zze(zzn.packageName, zzak.zzie) && (zzc = zzgy().zzc(zzn.packageName, "_s")) != null) {
                        j2 = zzc.zzfg;
                        this.zzj.zzab().zzgs().zza("Backfill the session number. Last used session number", Long.valueOf(j2));
                    }
                } else {
                    j2 = ((Long) zze.value).longValue();
                }
                zzb(new zzjn("_sno", j, Long.valueOf(j2 + 1), str), zzn);
            }
            zzjp zzjp = new zzjp(zzn.packageName, zzjn.origin, zzjn.name, zzjn.zztr, zzd);
            this.zzj.zzab().zzgr().zza("Setting user property", this.zzj.zzy().zzal(zzjp.name), zzd);
            zzgy().beginTransaction();
            try {
                zzg(zzn);
                boolean zza3 = zzgy().zza(zzjp);
                zzgy().setTransactionSuccessful();
                if (zza3) {
                    this.zzj.zzab().zzgr().zza("User property set", this.zzj.zzy().zzal(zzjp.name), zzjp.value);
                } else {
                    this.zzj.zzab().zzgk().zza("Too many unique user properties are set. Ignoring user property", this.zzj.zzy().zzal(zzjp.name), zzjp.value);
                    this.zzj.zzz().zza(zzn.packageName, 9, (String) null, (String) null, 0);
                }
            } finally {
                zzgy().endTransaction();
            }
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzc(zzjn zzjn, zzn zzn) {
        zzo();
        zzjj();
        if (TextUtils.isEmpty(zzn.zzcg) && TextUtils.isEmpty(zzn.zzcu)) {
            return;
        }
        if (!zzn.zzcq) {
            zzg(zzn);
        } else if (!this.zzj.zzad().zze(zzn.packageName, zzak.zzij)) {
            this.zzj.zzab().zzgr().zza("Removing user property", this.zzj.zzy().zzal(zzjn.name));
            zzgy().beginTransaction();
            try {
                zzg(zzn);
                zzgy().zzd(zzn.packageName, zzjn.name);
                zzgy().setTransactionSuccessful();
                this.zzj.zzab().zzgr().zza("User property removed", this.zzj.zzy().zzal(zzjn.name));
            } finally {
                zzgy().endTransaction();
            }
        } else if (!"_npa".equals(zzjn.name) || zzn.zzcv == null) {
            this.zzj.zzab().zzgr().zza("Removing user property", this.zzj.zzy().zzal(zzjn.name));
            zzgy().beginTransaction();
            try {
                zzg(zzn);
                zzgy().zzd(zzn.packageName, zzjn.name);
                zzgy().setTransactionSuccessful();
                this.zzj.zzab().zzgr().zza("User property removed", this.zzj.zzy().zzal(zzjn.name));
            } finally {
                zzgy().endTransaction();
            }
        } else {
            this.zzj.zzab().zzgr().zzao("Falling back to manifest metadata value for ad personalization");
            zzb(new zzjn("_npa", this.zzj.zzx().currentTimeMillis(), Long.valueOf(zzn.zzcv.booleanValue() ? 1 : 0), "auto"), zzn);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzjh zzjh) {
        this.zzta++;
    }

    /* access modifiers changed from: package-private */
    public final void zzjs() {
        this.zztb++;
    }

    /* access modifiers changed from: package-private */
    public final zzfj zzjt() {
        return this.zzj;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzf(zzn zzn) {
        int i;
        zzf zzab;
        zzae zzae;
        long j;
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        boolean z;
        zzx zzgy;
        String zzag;
        zzjp zze;
        zzn zzn2 = zzn;
        zzo();
        zzjj();
        Preconditions.checkNotNull(zzn);
        Preconditions.checkNotEmpty(zzn2.packageName);
        if (!TextUtils.isEmpty(zzn2.zzcg) || !TextUtils.isEmpty(zzn2.zzcu)) {
            zzf zzab2 = zzgy().zzab(zzn2.packageName);
            if (zzab2 != null && TextUtils.isEmpty(zzab2.getGmpAppId()) && !TextUtils.isEmpty(zzn2.zzcg)) {
                zzab2.zzl(0);
                zzgy().zza(zzab2);
                zzgz().zzaz(zzn2.packageName);
            }
            if (!zzn2.zzcq) {
                zzg(zzn);
                return;
            }
            long j2 = zzn2.zzdr;
            if (j2 == 0) {
                j2 = this.zzj.zzx().currentTimeMillis();
            }
            if (this.zzj.zzad().zze(zzn2.packageName, zzak.zzij)) {
                this.zzj.zzw().zzct();
            }
            int i2 = zzn2.zzds;
            if (i2 == 0 || i2 == 1) {
                i = i2;
            } else {
                this.zzj.zzab().zzgn().zza("Incorrect app type, assuming installed app. appId, appType", zzef.zzam(zzn2.packageName), Integer.valueOf(i2));
                i = 0;
            }
            zzgy().beginTransaction();
            try {
                if (this.zzj.zzad().zze(zzn2.packageName, zzak.zzij) && ((zze = zzgy().zze(zzn2.packageName, "_npa")) == null || "auto".equals(zze.origin))) {
                    if (zzn2.zzcv != null) {
                        zzjn zzjn = r7;
                        zzjn zzjn2 = new zzjn("_npa", j2, Long.valueOf(zzn2.zzcv.booleanValue() ? 1 : 0), "auto");
                        if (zze == null || !zze.value.equals(zzjn.zzts)) {
                            zzb(zzjn, zzn2);
                        }
                    } else if (zze != null) {
                        zzc(new zzjn("_npa", j2, null, "auto"), zzn2);
                    }
                }
                zzab = zzgy().zzab(zzn2.packageName);
                if (zzab != null) {
                    this.zzj.zzz();
                    if (zzjs.zza(zzn2.zzcg, zzab.getGmpAppId(), zzn2.zzcu, zzab.zzah())) {
                        this.zzj.zzab().zzgn().zza("New GMP App Id passed in. Removing cached database data. appId", zzef.zzam(zzab.zzag()));
                        zzgy = zzgy();
                        zzag = zzab.zzag();
                        zzgy.zzbi();
                        zzgy.zzo();
                        Preconditions.checkNotEmpty(zzag);
                        SQLiteDatabase writableDatabase = zzgy.getWritableDatabase();
                        String[] strArr = {zzag};
                        int delete = writableDatabase.delete(DataBaseEventsStorage.EventEntry.TABLE_NAME, "app_id=?", strArr) + 0 + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("conditional_properties", "app_id=?", strArr) + writableDatabase.delete("apps", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("event_filters", "app_id=?", strArr) + writableDatabase.delete("property_filters", "app_id=?", strArr) + writableDatabase.delete("audience_filter_values", "app_id=?", strArr);
                        if (delete > 0) {
                            zzgy.zzab().zzgs().zza("Deleted application data. app, records", zzag, Integer.valueOf(delete));
                        }
                        zzab = null;
                    }
                }
            } catch (SQLiteException e) {
                zzgy.zzab().zzgk().zza("Error deleting application data. appId, error", zzef.zzam(zzag), e);
            } catch (Throwable th) {
                zzgy().endTransaction();
                throw th;
            }
            if (zzab != null) {
                if (zzab.zzam() != -2147483648L) {
                    if (zzab.zzam() != zzn2.zzcn) {
                        Bundle bundle = new Bundle();
                        bundle.putString("_pv", zzab.zzal());
                        zzc(new zzai("_au", new zzah(bundle), "auto", j2), zzn2);
                    }
                } else if (zzab.zzal() != null && !zzab.zzal().equals(zzn2.zzcm)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_pv", zzab.zzal());
                    zzc(new zzai("_au", new zzah(bundle2), "auto", j2), zzn2);
                }
            }
            zzg(zzn);
            if (i == 0) {
                zzae = zzgy().zzc(zzn2.packageName, "_f");
            } else {
                zzae = i == 1 ? zzgy().zzc(zzn2.packageName, "_v") : null;
            }
            if (zzae == null) {
                long j3 = ((j2 / 3600000) + 1) * 3600000;
                if (i == 0) {
                    j = 1;
                    zzb(new zzjn("_fot", j2, Long.valueOf(j3), "auto"), zzn2);
                    if (this.zzj.zzad().zzt(zzn2.zzcg)) {
                        zzo();
                        this.zzj.zzht().zzat(zzn2.packageName);
                    }
                    zzo();
                    zzjj();
                    Bundle bundle3 = new Bundle();
                    bundle3.putLong("_c", 1);
                    bundle3.putLong("_r", 1);
                    bundle3.putLong("_uwa", 0);
                    bundle3.putLong("_pfo", 0);
                    bundle3.putLong("_sys", 0);
                    bundle3.putLong("_sysu", 0);
                    if (this.zzj.zzad().zzz(zzn2.packageName)) {
                        bundle3.putLong("_et", 1);
                    }
                    if (zzn2.zzdt) {
                        bundle3.putLong("_dac", 1);
                    }
                    if (this.zzj.getContext().getPackageManager() == null) {
                        this.zzj.zzab().zzgk().zza("PackageManager is null, first open report might be inaccurate. appId", zzef.zzam(zzn2.packageName));
                    } else {
                        try {
                            packageInfo = Wrappers.packageManager(this.zzj.getContext()).getPackageInfo(zzn2.packageName, 0);
                        } catch (PackageManager.NameNotFoundException e2) {
                            this.zzj.zzab().zzgk().zza("Package info is null, first open report might be inaccurate. appId", zzef.zzam(zzn2.packageName), e2);
                            packageInfo = null;
                        }
                        if (!(packageInfo == null || packageInfo.firstInstallTime == 0)) {
                            if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                bundle3.putLong("_uwa", 1);
                                z = false;
                            } else {
                                z = true;
                            }
                            zzb(new zzjn("_fi", j2, Long.valueOf(z ? 1 : 0), "auto"), zzn2);
                        }
                        try {
                            applicationInfo = Wrappers.packageManager(this.zzj.getContext()).getApplicationInfo(zzn2.packageName, 0);
                        } catch (PackageManager.NameNotFoundException e3) {
                            this.zzj.zzab().zzgk().zza("Application info is null, first open report might be inaccurate. appId", zzef.zzam(zzn2.packageName), e3);
                            applicationInfo = null;
                        }
                        if (applicationInfo != null) {
                            if ((applicationInfo.flags & 1) != 0) {
                                bundle3.putLong("_sys", 1);
                            }
                            if ((applicationInfo.flags & 128) != 0) {
                                bundle3.putLong("_sysu", 1);
                            }
                        }
                    }
                    zzx zzgy2 = zzgy();
                    String str = zzn2.packageName;
                    Preconditions.checkNotEmpty(str);
                    zzgy2.zzo();
                    zzgy2.zzbi();
                    long zzj2 = zzgy2.zzj(str, "first_open_count");
                    if (zzj2 >= 0) {
                        bundle3.putLong("_pfo", zzj2);
                    }
                    zzc(new zzai("_f", new zzah(bundle3), "auto", j2), zzn2);
                } else {
                    j = 1;
                    if (i == 1) {
                        zzb(new zzjn("_fvt", j2, Long.valueOf(j3), "auto"), zzn2);
                        zzo();
                        zzjj();
                        Bundle bundle4 = new Bundle();
                        bundle4.putLong("_c", 1);
                        bundle4.putLong("_r", 1);
                        if (this.zzj.zzad().zzz(zzn2.packageName)) {
                            bundle4.putLong("_et", 1);
                        }
                        if (zzn2.zzdt) {
                            bundle4.putLong("_dac", 1);
                        }
                        zzc(new zzai("_v", new zzah(bundle4), "auto", j2), zzn2);
                    }
                }
                if (!this.zzj.zzad().zze(zzn2.packageName, zzak.zzii)) {
                    Bundle bundle5 = new Bundle();
                    bundle5.putLong("_et", j);
                    if (this.zzj.zzad().zzz(zzn2.packageName)) {
                        bundle5.putLong("_fr", j);
                    }
                    zzc(new zzai("_e", new zzah(bundle5), "auto", j2), zzn2);
                }
            } else if (zzn2.zzdq) {
                zzc(new zzai("_cd", new zzah(new Bundle()), "auto", j2), zzn2);
            }
            zzgy().setTransactionSuccessful();
            zzgy().endTransaction();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>]
     candidates:
      com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
      com.google.android.gms.measurement.internal.zzn.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void */
    @WorkerThread
    private final zzn zzbi(String str) {
        String str2 = str;
        zzf zzab = zzgy().zzab(str2);
        if (zzab == null || TextUtils.isEmpty(zzab.zzal())) {
            this.zzj.zzab().zzgr().zza("No app data available; dropping", str2);
            return null;
        }
        Boolean zzc = zzc(zzab);
        if (zzc == null || zzc.booleanValue()) {
            zzf zzf = zzab;
            return new zzn(str, zzab.getGmpAppId(), zzab.zzal(), zzab.zzam(), zzab.zzan(), zzab.zzao(), zzab.zzap(), (String) null, zzab.isMeasurementEnabled(), false, zzab.getFirebaseInstanceId(), zzf.zzbd(), 0L, 0, zzf.zzbe(), zzf.zzbf(), false, zzf.zzah(), zzf.zzbg(), zzf.zzaq(), zzf.zzbh());
        }
        this.zzj.zzab().zzgk().zza("App version does not match; dropping. appId", zzef.zzam(str));
        return null;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zze(zzq zzq) {
        zzn zzbi = zzbi(zzq.packageName);
        if (zzbi != null) {
            zzb(zzq, zzbi);
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzb(zzq zzq, zzn zzn) {
        Preconditions.checkNotNull(zzq);
        Preconditions.checkNotEmpty(zzq.packageName);
        Preconditions.checkNotNull(zzq.origin);
        Preconditions.checkNotNull(zzq.zzdw);
        Preconditions.checkNotEmpty(zzq.zzdw.name);
        zzo();
        zzjj();
        if (TextUtils.isEmpty(zzn.zzcg) && TextUtils.isEmpty(zzn.zzcu)) {
            return;
        }
        if (!zzn.zzcq) {
            zzg(zzn);
            return;
        }
        zzq zzq2 = new zzq(zzq);
        boolean z = false;
        zzq2.active = false;
        zzgy().beginTransaction();
        try {
            zzq zzf = zzgy().zzf(zzq2.packageName, zzq2.zzdw.name);
            if (zzf != null && !zzf.origin.equals(zzq2.origin)) {
                this.zzj.zzab().zzgn().zza("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.zzj.zzy().zzal(zzq2.zzdw.name), zzq2.origin, zzf.origin);
            }
            if (zzf != null && zzf.active) {
                zzq2.origin = zzf.origin;
                zzq2.creationTimestamp = zzf.creationTimestamp;
                zzq2.triggerTimeout = zzf.triggerTimeout;
                zzq2.triggerEventName = zzf.triggerEventName;
                zzq2.zzdy = zzf.zzdy;
                zzq2.active = zzf.active;
                zzq2.zzdw = new zzjn(zzq2.zzdw.name, zzf.zzdw.zztr, zzq2.zzdw.getValue(), zzf.zzdw.origin);
            } else if (TextUtils.isEmpty(zzq2.triggerEventName)) {
                zzq2.zzdw = new zzjn(zzq2.zzdw.name, zzq2.creationTimestamp, zzq2.zzdw.getValue(), zzq2.zzdw.origin);
                zzq2.active = true;
                z = true;
            }
            if (zzq2.active) {
                zzjn zzjn = zzq2.zzdw;
                zzjp zzjp = new zzjp(zzq2.packageName, zzq2.origin, zzjn.name, zzjn.zztr, zzjn.getValue());
                if (zzgy().zza(zzjp)) {
                    this.zzj.zzab().zzgr().zza("User property updated immediately", zzq2.packageName, this.zzj.zzy().zzal(zzjp.name), zzjp.value);
                } else {
                    this.zzj.zzab().zzgk().zza("(2)Too many active user properties, ignoring", zzef.zzam(zzq2.packageName), this.zzj.zzy().zzal(zzjp.name), zzjp.value);
                }
                if (z && zzq2.zzdy != null) {
                    zzd(new zzai(zzq2.zzdy, zzq2.creationTimestamp), zzn);
                }
            }
            if (zzgy().zza(zzq2)) {
                this.zzj.zzab().zzgr().zza("Conditional property added", zzq2.packageName, this.zzj.zzy().zzal(zzq2.zzdw.name), zzq2.zzdw.getValue());
            } else {
                this.zzj.zzab().zzgk().zza("Too many conditional properties, ignoring", zzef.zzam(zzq2.packageName), this.zzj.zzy().zzal(zzq2.zzdw.name), zzq2.zzdw.getValue());
            }
            zzgy().setTransactionSuccessful();
        } finally {
            zzgy().endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzf(zzq zzq) {
        zzn zzbi = zzbi(zzq.packageName);
        if (zzbi != null) {
            zzc(zzq, zzbi);
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public final void zzc(zzq zzq, zzn zzn) {
        Preconditions.checkNotNull(zzq);
        Preconditions.checkNotEmpty(zzq.packageName);
        Preconditions.checkNotNull(zzq.zzdw);
        Preconditions.checkNotEmpty(zzq.zzdw.name);
        zzo();
        zzjj();
        if (TextUtils.isEmpty(zzn.zzcg) && TextUtils.isEmpty(zzn.zzcu)) {
            return;
        }
        if (!zzn.zzcq) {
            zzg(zzn);
            return;
        }
        zzgy().beginTransaction();
        try {
            zzg(zzn);
            zzq zzf = zzgy().zzf(zzq.packageName, zzq.zzdw.name);
            if (zzf != null) {
                this.zzj.zzab().zzgr().zza("Removing conditional user property", zzq.packageName, this.zzj.zzy().zzal(zzq.zzdw.name));
                zzgy().zzg(zzq.packageName, zzq.zzdw.name);
                if (zzf.active) {
                    zzgy().zzd(zzq.packageName, zzq.zzdw.name);
                }
                if (zzq.zzdz != null) {
                    Bundle bundle = null;
                    if (zzq.zzdz.zzfq != null) {
                        bundle = zzq.zzdz.zzfq.zzcv();
                    }
                    Bundle bundle2 = bundle;
                    zzd(this.zzj.zzz().zza(zzq.packageName, zzq.zzdz.name, bundle2, zzf.origin, zzq.zzdz.zzfu, true, false), zzn);
                }
            } else {
                this.zzj.zzab().zzgn().zza("Conditional user property doesn't exist", zzef.zzam(zzq.packageName), this.zzj.zzy().zzal(zzq.zzdw.name));
            }
            zzgy().setTransactionSuccessful();
        } finally {
            zzgy().endTransaction();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x018e  */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.measurement.internal.zzf zzg(com.google.android.gms.measurement.internal.zzn r11) {
        /*
            r10 = this;
            r10.zzo()
            r10.zzjj()
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r11)
            java.lang.String r0 = r11.packageName
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r0)
            com.google.android.gms.measurement.internal.zzx r0 = r10.zzgy()
            java.lang.String r1 = r11.packageName
            com.google.android.gms.measurement.internal.zzf r0 = r0.zzab(r1)
            com.google.android.gms.measurement.internal.zzfj r1 = r10.zzj
            com.google.android.gms.measurement.internal.zzeo r1 = r1.zzac()
            java.lang.String r2 = r11.packageName
            java.lang.String r1 = r1.zzaq(r2)
            r2 = 1
            if (r0 != 0) goto L_0x0042
            com.google.android.gms.measurement.internal.zzf r0 = new com.google.android.gms.measurement.internal.zzf
            com.google.android.gms.measurement.internal.zzfj r3 = r10.zzj
            java.lang.String r4 = r11.packageName
            r0.<init>(r3, r4)
            com.google.android.gms.measurement.internal.zzfj r3 = r10.zzj
            com.google.android.gms.measurement.internal.zzjs r3 = r3.zzz()
            java.lang.String r3 = r3.zzjy()
            r0.zza(r3)
            r0.zzd(r1)
        L_0x0040:
            r1 = 1
            goto L_0x005e
        L_0x0042:
            java.lang.String r3 = r0.zzai()
            boolean r3 = r1.equals(r3)
            if (r3 != 0) goto L_0x005d
            r0.zzd(r1)
            com.google.android.gms.measurement.internal.zzfj r1 = r10.zzj
            com.google.android.gms.measurement.internal.zzjs r1 = r1.zzz()
            java.lang.String r1 = r1.zzjy()
            r0.zza(r1)
            goto L_0x0040
        L_0x005d:
            r1 = 0
        L_0x005e:
            java.lang.String r3 = r11.zzcg
            java.lang.String r4 = r0.getGmpAppId()
            boolean r3 = android.text.TextUtils.equals(r3, r4)
            if (r3 != 0) goto L_0x0070
            java.lang.String r1 = r11.zzcg
            r0.zzb(r1)
            r1 = 1
        L_0x0070:
            java.lang.String r3 = r11.zzcu
            java.lang.String r4 = r0.zzah()
            boolean r3 = android.text.TextUtils.equals(r3, r4)
            if (r3 != 0) goto L_0x0082
            java.lang.String r1 = r11.zzcu
            r0.zzc(r1)
            r1 = 1
        L_0x0082:
            java.lang.String r3 = r11.zzci
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x009c
            java.lang.String r3 = r11.zzci
            java.lang.String r4 = r0.getFirebaseInstanceId()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x009c
            java.lang.String r1 = r11.zzci
            r0.zze(r1)
            r1 = 1
        L_0x009c:
            long r3 = r11.zzr
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00b4
            long r3 = r11.zzr
            long r7 = r0.zzao()
            int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x00b4
            long r3 = r11.zzr
            r0.zzh(r3)
            r1 = 1
        L_0x00b4:
            java.lang.String r3 = r11.zzcm
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x00ce
            java.lang.String r3 = r11.zzcm
            java.lang.String r4 = r0.zzal()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x00ce
            java.lang.String r1 = r11.zzcm
            r0.zzf(r1)
            r1 = 1
        L_0x00ce:
            long r3 = r11.zzcn
            long r7 = r0.zzam()
            int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x00de
            long r3 = r11.zzcn
            r0.zzg(r3)
            r1 = 1
        L_0x00de:
            java.lang.String r3 = r11.zzco
            if (r3 == 0) goto L_0x00f4
            java.lang.String r3 = r11.zzco
            java.lang.String r4 = r0.zzan()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x00f4
            java.lang.String r1 = r11.zzco
            r0.zzg(r1)
            r1 = 1
        L_0x00f4:
            long r3 = r11.zzcp
            long r7 = r0.zzap()
            int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x0104
            long r3 = r11.zzcp
            r0.zzi(r3)
            r1 = 1
        L_0x0104:
            boolean r3 = r11.zzcq
            boolean r4 = r0.isMeasurementEnabled()
            if (r3 == r4) goto L_0x0112
            boolean r1 = r11.zzcq
            r0.setMeasurementEnabled(r1)
            r1 = 1
        L_0x0112:
            java.lang.String r3 = r11.zzdp
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x012c
            java.lang.String r3 = r11.zzdp
            java.lang.String r4 = r0.zzbb()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x012c
            java.lang.String r1 = r11.zzdp
            r0.zzh(r1)
            r1 = 1
        L_0x012c:
            long r3 = r11.zzcr
            long r7 = r0.zzbd()
            int r9 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x013c
            long r3 = r11.zzcr
            r0.zzt(r3)
            r1 = 1
        L_0x013c:
            boolean r3 = r11.zzcs
            boolean r4 = r0.zzbe()
            if (r3 == r4) goto L_0x014a
            boolean r1 = r11.zzcs
            r0.zzb(r1)
            r1 = 1
        L_0x014a:
            boolean r3 = r11.zzct
            boolean r4 = r0.zzbf()
            if (r3 == r4) goto L_0x0158
            boolean r1 = r11.zzct
            r0.zzc(r1)
            r1 = 1
        L_0x0158:
            com.google.android.gms.measurement.internal.zzfj r3 = r10.zzj
            com.google.android.gms.measurement.internal.zzs r3 = r3.zzad()
            java.lang.String r4 = r11.packageName
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzak.zzij
            boolean r3 = r3.zze(r4, r7)
            if (r3 == 0) goto L_0x0176
            java.lang.Boolean r3 = r11.zzcv
            java.lang.Boolean r4 = r0.zzbg()
            if (r3 == r4) goto L_0x0176
            java.lang.Boolean r1 = r11.zzcv
            r0.zza(r1)
            r1 = 1
        L_0x0176:
            long r3 = r11.zzs
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x018c
            long r3 = r11.zzs
            long r5 = r0.zzaq()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x018c
            long r3 = r11.zzs
            r0.zzj(r3)
            r1 = 1
        L_0x018c:
            if (r1 == 0) goto L_0x0195
            com.google.android.gms.measurement.internal.zzx r11 = r10.zzgy()
            r11.zza(r0)
        L_0x0195:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzjg.zzg(com.google.android.gms.measurement.internal.zzn):com.google.android.gms.measurement.internal.zzf");
    }

    /* access modifiers changed from: package-private */
    public final String zzh(zzn zzn) {
        try {
            return (String) this.zzj.zzaa().zza(new zzjk(this, zzn)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            this.zzj.zzab().zzgk().zza("Failed to get app instance id. appId", zzef.zzam(zzn.packageName), e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzj(boolean z) {
        zzjn();
    }
}
