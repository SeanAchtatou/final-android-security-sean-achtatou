package com.google.android.gms.internal.ads;

public final class zzix {
    private final int id;
    public final int type;
    private final long zzack;
    public final zzhj zzame;
    public final int zzamf;
    private final zziy[] zzamz;
    public final long zzcs;

    public zzix(int i, int i2, long j, long j2, zzhj zzhj, zziy[] zziyArr, int i3) {
        this.id = i;
        this.type = i2;
        this.zzcs = j;
        this.zzack = j2;
        this.zzame = zzhj;
        this.zzamz = zziyArr;
        this.zzamf = i3;
    }
}
