package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.snapshot.Snapshots;

final class zzm extends zze.zzat<Snapshots.CommitSnapshotResult> {
    zzm(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zzah(DataHolder dataHolder) {
        setResult(new zze.zzh(dataHolder));
    }
}
