package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.zzd;

@UsedByReflection("GamesClientImpl.java")
@SafeParcelable.Class(creator = "AppContentConditionEntityCreator")
@SafeParcelable.Reserved({1000})
public final class AppContentConditionEntity extends zzd implements zzg {
    public static final Parcelable.Creator<AppContentConditionEntity> CREATOR = new zzh();
    @SafeParcelable.Field(getter = "getDefaultValue", id = 1)
    private final String zzgf;
    @SafeParcelable.Field(getter = "getExpectedValue", id = 2)
    private final String zzgg;
    @SafeParcelable.Field(getter = "getPredicate", id = 3)
    private final String zzgh;
    @SafeParcelable.Field(getter = "getPredicateParameters", id = 4)
    private final Bundle zzgi;

    @SafeParcelable.Constructor
    AppContentConditionEntity(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) String str2, @SafeParcelable.Param(id = 3) String str3, @SafeParcelable.Param(id = 4) Bundle bundle) {
        this.zzgf = str;
        this.zzgg = str2;
        this.zzgh = str3;
        this.zzgi = bundle;
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String zzam() {
        return this.zzgf;
    }

    public final String zzan() {
        return this.zzgg;
    }

    public final String zzao() {
        return this.zzgh;
    }

    public final Bundle zzap() {
        return this.zzgi;
    }

    public final int hashCode() {
        return Objects.hashCode(zzam(), zzan(), zzao(), zzap());
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzg)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zzg zzg = (zzg) obj;
        if (!Objects.equal(zzg.zzam(), zzam()) || !Objects.equal(zzg.zzan(), zzan()) || !Objects.equal(zzg.zzao(), zzao()) || !Objects.equal(zzg.zzap(), zzap())) {
            return false;
        }
        return true;
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("DefaultValue", zzam()).add("ExpectedValue", zzan()).add("Predicate", zzao()).add("PredicateParameters", zzap()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zzgf, false);
        SafeParcelWriter.writeString(parcel, 2, this.zzgg, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzgh, false);
        SafeParcelWriter.writeBundle(parcel, 4, this.zzgi, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
