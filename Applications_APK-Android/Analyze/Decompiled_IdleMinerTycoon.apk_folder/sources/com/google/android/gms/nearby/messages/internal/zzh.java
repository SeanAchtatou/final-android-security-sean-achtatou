package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "GetPermissionStatusRequestCreator")
@Deprecated
public final class zzh extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzh> CREATOR = new zzi();
    @SafeParcelable.VersionField(id = 1)
    private final int versionCode;
    @Nullable
    @SafeParcelable.Field(id = 3)
    @Deprecated
    private final String zzff;
    @SafeParcelable.Field(getter = "getCallbackAsBinder", id = 2, type = "android.os.IBinder")
    private final zzp zzhh;
    @Nullable
    @SafeParcelable.Field(id = 4)
    @Deprecated
    private final ClientAppContext zzhi;

    /* JADX WARN: Type inference failed for: r0v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzh(@com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) int r3, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r4, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) java.lang.String r5, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) com.google.android.gms.nearby.messages.internal.ClientAppContext r6) {
        /*
            r2 = this;
            r2.<init>()
            r2.versionCode = r3
            r3 = 0
            if (r4 != 0) goto L_0x000a
            r4 = r3
            goto L_0x001e
        L_0x000a:
            java.lang.String r0 = "com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.nearby.messages.internal.zzp
            if (r1 == 0) goto L_0x0018
            r4 = r0
            com.google.android.gms.nearby.messages.internal.zzp r4 = (com.google.android.gms.nearby.messages.internal.zzp) r4
            goto L_0x001e
        L_0x0018:
            com.google.android.gms.nearby.messages.internal.zzr r0 = new com.google.android.gms.nearby.messages.internal.zzr
            r0.<init>(r4)
            r4 = r0
        L_0x001e:
            r2.zzhh = r4
            r2.zzff = r5
            r4 = 0
            com.google.android.gms.nearby.messages.internal.ClientAppContext r3 = com.google.android.gms.nearby.messages.internal.ClientAppContext.zza(r6, r3, r5, r4)
            r2.zzhi = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.internal.zzh.<init>(int, android.os.IBinder, java.lang.String, com.google.android.gms.nearby.messages.internal.ClientAppContext):void");
    }

    zzh(IBinder iBinder) {
        this(1, iBinder, null, null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzhh.asBinder(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zzff, false);
        SafeParcelWriter.writeParcelable(parcel, 4, this.zzhi, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
