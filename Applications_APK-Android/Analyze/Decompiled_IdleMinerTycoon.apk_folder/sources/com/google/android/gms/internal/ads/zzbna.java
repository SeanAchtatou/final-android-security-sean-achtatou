package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzbna {
    @Nullable
    public static JSONObject zza(zzcxm zzcxm) {
        try {
            return new JSONObject(zzcxm.zzdnr);
        } catch (JSONException unused) {
            return null;
        }
    }
}
