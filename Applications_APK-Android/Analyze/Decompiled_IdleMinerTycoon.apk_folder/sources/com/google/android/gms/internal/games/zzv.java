package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzv extends zzy {
    private final /* synthetic */ boolean zzjz;
    private final /* synthetic */ String[] zzkc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzv(zzu zzu, GoogleApiClient googleApiClient, boolean z, String[] strArr) {
        super(googleApiClient, null);
        this.zzjz = z;
        this.zzkc = strArr;
    }

    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzjz, this.zzkc);
    }
}
