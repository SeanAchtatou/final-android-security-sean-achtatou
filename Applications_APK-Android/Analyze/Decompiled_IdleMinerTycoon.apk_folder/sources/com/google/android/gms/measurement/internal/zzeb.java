package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;

public final class zzeb extends zzg {
    private final zzea zzjv = new zzea(this, getContext(), "google_app_measurement_local.db");
    private boolean zzjw;

    zzeb(zzfj zzfj) {
        super(zzfj);
    }

    /* access modifiers changed from: protected */
    public final boolean zzbk() {
        return false;
    }

    @WorkerThread
    public final void resetAnalyticsData() {
        zzm();
        zzo();
        try {
            int delete = getWritableDatabase().delete("messages", null, null) + 0;
            if (delete > 0) {
                zzab().zzgs().zza("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzab().zzgk().zza("Error resetting local analytics data. error", e);
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:73:0x0106 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:64:0x00f4 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [int, boolean] */
    /* JADX WARN: Type inference failed for: r7v0 */
    /* JADX WARN: Type inference failed for: r7v1 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r7v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r7v5 */
    /* JADX WARN: Type inference failed for: r7v6 */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00cb A[SYNTHETIC, Splitter:B:49:0x00cb] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x011c  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x011f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x011f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x011f A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zza(int r18, byte[] r19) {
        /*
            r17 = this;
            r1 = r17
            r17.zzm()
            r17.zzo()
            boolean r0 = r1.zzjw
            r2 = 0
            if (r0 == 0) goto L_0x000e
            return r2
        L_0x000e:
            android.content.ContentValues r3 = new android.content.ContentValues
            r3.<init>()
            java.lang.String r0 = "type"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r18)
            r3.put(r0, r4)
            java.lang.String r0 = "entry"
            r4 = r19
            r3.put(r0, r4)
            r4 = 5
            r5 = 0
            r6 = 5
        L_0x0026:
            if (r5 >= r4) goto L_0x0132
            r7 = 0
            r8 = 1
            android.database.sqlite.SQLiteDatabase r9 = r17.getWritableDatabase()     // Catch:{ SQLiteFullException -> 0x0104, SQLiteDatabaseLockedException -> 0x00f2, SQLiteException -> 0x00c7, all -> 0x00c3 }
            if (r9 != 0) goto L_0x0040
            r1.zzjw = r8     // Catch:{ SQLiteFullException -> 0x003d, SQLiteDatabaseLockedException -> 0x00f3, SQLiteException -> 0x0038 }
            if (r9 == 0) goto L_0x0037
            r9.close()
        L_0x0037:
            return r2
        L_0x0038:
            r0 = move-exception
            r12 = r7
        L_0x003a:
            r7 = r9
            goto L_0x00c9
        L_0x003d:
            r0 = move-exception
            goto L_0x0106
        L_0x0040:
            r9.beginTransaction()     // Catch:{ SQLiteFullException -> 0x003d, SQLiteDatabaseLockedException -> 0x00f3, SQLiteException -> 0x0038 }
            r10 = 0
            java.lang.String r0 = "select count(1) from messages"
            android.database.Cursor r12 = r9.rawQuery(r0, r7)     // Catch:{ SQLiteFullException -> 0x003d, SQLiteDatabaseLockedException -> 0x00f3, SQLiteException -> 0x0038 }
            if (r12 == 0) goto L_0x0061
            boolean r0 = r12.moveToFirst()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            if (r0 == 0) goto L_0x0061
            long r10 = r12.getLong(r2)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            goto L_0x0061
        L_0x0058:
            r0 = move-exception
            goto L_0x0127
        L_0x005b:
            r0 = move-exception
            goto L_0x003a
        L_0x005d:
            r0 = move-exception
            r7 = r12
            goto L_0x0106
        L_0x0061:
            r13 = 100000(0x186a0, double:4.94066E-319)
            int r0 = (r10 > r13 ? 1 : (r10 == r13 ? 0 : -1))
            if (r0 < 0) goto L_0x00ab
            com.google.android.gms.measurement.internal.zzef r0 = r17.zzab()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgk()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            java.lang.String r15 = "Data loss, local db full"
            r0.zzao(r15)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            r0 = 0
            long r13 = r13 - r10
            r10 = 1
            long r13 = r13 + r10
            java.lang.String r0 = "messages"
            java.lang.String r10 = "rowid in (select rowid from messages order by rowid asc limit ?)"
            java.lang.String[] r11 = new java.lang.String[r8]     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            java.lang.String r15 = java.lang.Long.toString(r13)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            r11[r2] = r15     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            int r0 = r9.delete(r0, r10, r11)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            long r10 = (long) r0     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            int r0 = (r10 > r13 ? 1 : (r10 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x00ab
            com.google.android.gms.measurement.internal.zzef r0 = r17.zzab()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgk()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            java.lang.String r15 = "Different delete count than expected in local db. expected, received, difference"
            java.lang.Long r4 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            java.lang.Long r2 = java.lang.Long.valueOf(r10)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            r16 = 0
            long r13 = r13 - r10
            java.lang.Long r10 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            r0.zza(r15, r4, r2, r10)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
        L_0x00ab:
            java.lang.String r0 = "messages"
            r9.insertOrThrow(r0, r7, r3)     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            r9.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            r9.endTransaction()     // Catch:{ SQLiteFullException -> 0x005d, SQLiteDatabaseLockedException -> 0x00c1, SQLiteException -> 0x005b, all -> 0x0058 }
            if (r12 == 0) goto L_0x00bb
            r12.close()
        L_0x00bb:
            if (r9 == 0) goto L_0x00c0
            r9.close()
        L_0x00c0:
            return r8
        L_0x00c1:
            r7 = r12
            goto L_0x00f3
        L_0x00c3:
            r0 = move-exception
            r9 = r7
            r12 = r9
            goto L_0x0127
        L_0x00c7:
            r0 = move-exception
            r12 = r7
        L_0x00c9:
            if (r7 == 0) goto L_0x00d8
            boolean r2 = r7.inTransaction()     // Catch:{ all -> 0x00d5 }
            if (r2 == 0) goto L_0x00d8
            r7.endTransaction()     // Catch:{ all -> 0x00d5 }
            goto L_0x00d8
        L_0x00d5:
            r0 = move-exception
            r9 = r7
            goto L_0x0127
        L_0x00d8:
            com.google.android.gms.measurement.internal.zzef r2 = r17.zzab()     // Catch:{ all -> 0x00d5 }
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgk()     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = "Error writing entry to local database"
            r2.zza(r4, r0)     // Catch:{ all -> 0x00d5 }
            r1.zzjw = r8     // Catch:{ all -> 0x00d5 }
            if (r12 == 0) goto L_0x00ec
            r12.close()
        L_0x00ec:
            if (r7 == 0) goto L_0x011f
            r7.close()
            goto L_0x011f
        L_0x00f2:
            r9 = r7
        L_0x00f3:
            long r10 = (long) r6
            android.os.SystemClock.sleep(r10)     // Catch:{ all -> 0x0125 }
            int r6 = r6 + 20
            if (r7 == 0) goto L_0x00fe
            r7.close()
        L_0x00fe:
            if (r9 == 0) goto L_0x011f
            r9.close()
            goto L_0x011f
        L_0x0104:
            r0 = move-exception
            r9 = r7
        L_0x0106:
            com.google.android.gms.measurement.internal.zzef r2 = r17.zzab()     // Catch:{ all -> 0x0125 }
            com.google.android.gms.measurement.internal.zzeh r2 = r2.zzgk()     // Catch:{ all -> 0x0125 }
            java.lang.String r4 = "Error writing entry to local database"
            r2.zza(r4, r0)     // Catch:{ all -> 0x0125 }
            r1.zzjw = r8     // Catch:{ all -> 0x0125 }
            if (r7 == 0) goto L_0x011a
            r7.close()
        L_0x011a:
            if (r9 == 0) goto L_0x011f
            r9.close()
        L_0x011f:
            int r5 = r5 + 1
            r2 = 0
            r4 = 5
            goto L_0x0026
        L_0x0125:
            r0 = move-exception
            r12 = r7
        L_0x0127:
            if (r12 == 0) goto L_0x012c
            r12.close()
        L_0x012c:
            if (r9 == 0) goto L_0x0131
            r9.close()
        L_0x0131:
            throw r0
        L_0x0132:
            com.google.android.gms.measurement.internal.zzef r0 = r17.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgn()
            java.lang.String r2 = "Failed to write entry to local database"
            r0.zzao(r2)
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzeb.zza(int, byte[]):boolean");
    }

    public final boolean zza(zzai zzai) {
        Parcel obtain = Parcel.obtain();
        zzai.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(0, marshall);
        }
        zzab().zzgn().zzao("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public final boolean zza(zzjn zzjn) {
        Parcel obtain = Parcel.obtain();
        zzjn.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(1, marshall);
        }
        zzab().zzgn().zzao("User property too long for local database. Sending directly to service");
        return false;
    }

    public final boolean zzc(zzq zzq) {
        zzz();
        byte[] zza = zzjs.zza(zzq);
        if (zza.length <= 131072) {
            return zza(2, zza);
        }
        zzab().zzgn().zzao("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:101|102|103|104) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:86|87|88|89) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:73|74|75|76|211) */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0172, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        zzab().zzgk().zzao("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        r10.recycle();
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x018c, code lost:
        r10.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x018f, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01b0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01b3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x01f9, code lost:
        r11 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0202, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0207, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x020c, code lost:
        r11 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x020f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0214, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0215, code lost:
        r11 = r15;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0218, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0219, code lost:
        r11 = r15;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x021e, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0220, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0221, code lost:
        r11 = r15;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0236, code lost:
        if (r22.inTransaction() != false) goto L_0x0238;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0238, code lost:
        r22.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x024c, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0251, code lost:
        r22.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x025f, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0264, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0281, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0286, code lost:
        r22.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0030, code lost:
        r9 = null;
        r22 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x028e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0291, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0296, code lost:
        r22.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0035, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0036, code lost:
        r9 = null;
        r22 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        r11 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        r9 = null;
        r22 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x010c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        zzab().zzgk().zzao("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r10.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x011f, code lost:
        r10.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0122, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x013c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        zzab().zzgk().zzao("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r10.recycle();
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0155, code lost:
        r10.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0158, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:101:0x0174 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x010e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:86:0x013e */
    /* JADX WARNING: Removed duplicated region for block: B:135:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:59:0x00dc] */
    /* JADX WARNING: Removed duplicated region for block: B:143:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), PHI: r22 
      PHI: (r22v11 android.database.sqlite.SQLiteDatabase) = (r22v12 android.database.sqlite.SQLiteDatabase), (r22v19 android.database.sqlite.SQLiteDatabase) binds: [B:57:0x00be, B:42:0x009b] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:42:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0232 A[SYNTHETIC, Splitter:B:160:0x0232] */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0251  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0264  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x0281  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0286  */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0291  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0296  */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x0289 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x0289 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x0289 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0027] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable> zzc(int r25) {
        /*
            r24 = this;
            r1 = r24
            r24.zzo()
            r24.zzm()
            boolean r0 = r1.zzjw
            r2 = 0
            if (r0 == 0) goto L_0x000e
            return r2
        L_0x000e:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            boolean r0 = r24.zzcg()
            if (r0 != 0) goto L_0x001a
            return r3
        L_0x001a:
            r4 = 5
            r5 = 0
            r6 = 0
            r7 = 5
        L_0x001e:
            if (r6 >= r4) goto L_0x029a
            r8 = 1
            android.database.sqlite.SQLiteDatabase r15 = r24.getWritableDatabase()     // Catch:{ SQLiteFullException -> 0x026c, SQLiteDatabaseLockedException -> 0x0255, SQLiteException -> 0x022c, all -> 0x0226 }
            if (r15 != 0) goto L_0x0044
            r1.zzjw = r8     // Catch:{ SQLiteFullException -> 0x003e, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0035, all -> 0x002f }
            if (r15 == 0) goto L_0x002e
            r15.close()
        L_0x002e:
            return r2
        L_0x002f:
            r0 = move-exception
            r9 = r2
            r22 = r15
            goto L_0x028f
        L_0x0035:
            r0 = move-exception
            r9 = r2
            r22 = r15
            goto L_0x0230
        L_0x003b:
            r11 = r15
            goto L_0x021e
        L_0x003e:
            r0 = move-exception
            r9 = r2
            r22 = r15
            goto L_0x0270
        L_0x0044:
            r15.beginTransaction()     // Catch:{ SQLiteFullException -> 0x0220, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0218, all -> 0x0214 }
            com.google.android.gms.measurement.internal.zzs r0 = r24.zzad()     // Catch:{ SQLiteFullException -> 0x0220, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0218, all -> 0x0214 }
            com.google.android.gms.measurement.internal.zzdu<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.zzak.zzjd     // Catch:{ SQLiteFullException -> 0x0220, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0218, all -> 0x0214 }
            boolean r0 = r0.zza(r9)     // Catch:{ SQLiteFullException -> 0x0220, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0218, all -> 0x0214 }
            r9 = 100
            r18 = -1
            r14 = 3
            r13 = 2
            if (r0 == 0) goto L_0x00b8
            long r10 = zza(r15)     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x00ac, all -> 0x00a6 }
            int r0 = (r10 > r18 ? 1 : (r10 == r18 ? 0 : -1))
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = "rowid<?"
            java.lang.String[] r12 = new java.lang.String[r8]     // Catch:{ SQLiteFullException -> 0x003e, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0035, all -> 0x002f }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ SQLiteFullException -> 0x003e, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0035, all -> 0x002f }
            r12[r5] = r10     // Catch:{ SQLiteFullException -> 0x003e, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x0035, all -> 0x002f }
            r23 = r12
            r12 = r0
            r0 = r23
            goto L_0x0073
        L_0x0071:
            r0 = r2
            r12 = r0
        L_0x0073:
            java.lang.String r10 = "messages"
            java.lang.String[] r11 = new java.lang.String[r14]     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x00ac, all -> 0x00a6 }
            java.lang.String r16 = "rowid"
            r11[r5] = r16     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x00ac, all -> 0x00a6 }
            java.lang.String r16 = "type"
            r11[r8] = r16     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x00ac, all -> 0x00a6 }
            java.lang.String r16 = "entry"
            r11[r13] = r16     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x00ac, all -> 0x00a6 }
            r16 = 0
            r17 = 0
            java.lang.String r20 = "rowid asc"
            java.lang.String r21 = java.lang.Integer.toString(r9)     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x003b, SQLiteException -> 0x00ac, all -> 0x00a6 }
            r9 = r15
            r4 = 2
            r13 = r0
            r0 = 3
            r14 = r16
            r22 = r15
            r15 = r17
            r16 = r20
            r17 = r21
            android.database.Cursor r9 = r9.query(r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ SQLiteFullException -> 0x00a4, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x00a2, all -> 0x00a0 }
            goto L_0x00dc
        L_0x00a0:
            r0 = move-exception
            goto L_0x00a9
        L_0x00a2:
            r0 = move-exception
            goto L_0x00af
        L_0x00a4:
            r0 = move-exception
            goto L_0x00b5
        L_0x00a6:
            r0 = move-exception
            r22 = r15
        L_0x00a9:
            r9 = r2
            goto L_0x028f
        L_0x00ac:
            r0 = move-exception
            r22 = r15
        L_0x00af:
            r9 = r2
            goto L_0x0230
        L_0x00b2:
            r0 = move-exception
            r22 = r15
        L_0x00b5:
            r9 = r2
            goto L_0x0270
        L_0x00b8:
            r22 = r15
            r0 = 3
            r4 = 2
            java.lang.String r10 = "messages"
            java.lang.String[] r11 = new java.lang.String[r0]     // Catch:{ SQLiteFullException -> 0x020f, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x0207, all -> 0x0202 }
            java.lang.String r12 = "rowid"
            r11[r5] = r12     // Catch:{ SQLiteFullException -> 0x020f, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x0207, all -> 0x0202 }
            java.lang.String r12 = "type"
            r11[r8] = r12     // Catch:{ SQLiteFullException -> 0x020f, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x0207, all -> 0x0202 }
            java.lang.String r12 = "entry"
            r11[r4] = r12     // Catch:{ SQLiteFullException -> 0x020f, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x0207, all -> 0x0202 }
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            java.lang.String r16 = "rowid asc"
            java.lang.String r17 = java.lang.Integer.toString(r9)     // Catch:{ SQLiteFullException -> 0x020f, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x0207, all -> 0x0202 }
            r9 = r22
            android.database.Cursor r9 = r9.query(r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ SQLiteFullException -> 0x020f, SQLiteDatabaseLockedException -> 0x020c, SQLiteException -> 0x0207, all -> 0x0202 }
        L_0x00dc:
            boolean r10 = r9.moveToNext()     // Catch:{ SQLiteFullException -> 0x01fd, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01f5, all -> 0x01f0 }
            if (r10 == 0) goto L_0x01b6
            long r18 = r9.getLong(r5)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            int r10 = r9.getInt(r8)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            byte[] r11 = r9.getBlob(r4)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            if (r10 != 0) goto L_0x0123
            android.os.Parcel r10 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            int r12 = r11.length     // Catch:{ ParseException -> 0x010e }
            r10.unmarshall(r11, r5, r12)     // Catch:{ ParseException -> 0x010e }
            r10.setDataPosition(r5)     // Catch:{ ParseException -> 0x010e }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzai> r11 = com.google.android.gms.measurement.internal.zzai.CREATOR     // Catch:{ ParseException -> 0x010e }
            java.lang.Object r11 = r11.createFromParcel(r10)     // Catch:{ ParseException -> 0x010e }
            com.google.android.gms.measurement.internal.zzai r11 = (com.google.android.gms.measurement.internal.zzai) r11     // Catch:{ ParseException -> 0x010e }
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            if (r11 == 0) goto L_0x00dc
            r3.add(r11)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x00dc
        L_0x010c:
            r0 = move-exception
            goto L_0x011f
        L_0x010e:
            com.google.android.gms.measurement.internal.zzef r11 = r24.zzab()     // Catch:{ all -> 0x010c }
            com.google.android.gms.measurement.internal.zzeh r11 = r11.zzgk()     // Catch:{ all -> 0x010c }
            java.lang.String r12 = "Failed to load event from local database"
            r11.zzao(r12)     // Catch:{ all -> 0x010c }
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x00dc
        L_0x011f:
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            throw r0     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
        L_0x0123:
            if (r10 != r8) goto L_0x0159
            android.os.Parcel r10 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            int r12 = r11.length     // Catch:{ ParseException -> 0x013e }
            r10.unmarshall(r11, r5, r12)     // Catch:{ ParseException -> 0x013e }
            r10.setDataPosition(r5)     // Catch:{ ParseException -> 0x013e }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzjn> r11 = com.google.android.gms.measurement.internal.zzjn.CREATOR     // Catch:{ ParseException -> 0x013e }
            java.lang.Object r11 = r11.createFromParcel(r10)     // Catch:{ ParseException -> 0x013e }
            com.google.android.gms.measurement.internal.zzjn r11 = (com.google.android.gms.measurement.internal.zzjn) r11     // Catch:{ ParseException -> 0x013e }
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x014f
        L_0x013c:
            r0 = move-exception
            goto L_0x0155
        L_0x013e:
            com.google.android.gms.measurement.internal.zzef r11 = r24.zzab()     // Catch:{ all -> 0x013c }
            com.google.android.gms.measurement.internal.zzeh r11 = r11.zzgk()     // Catch:{ all -> 0x013c }
            java.lang.String r12 = "Failed to load user property from local database"
            r11.zzao(r12)     // Catch:{ all -> 0x013c }
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            r11 = r2
        L_0x014f:
            if (r11 == 0) goto L_0x00dc
            r3.add(r11)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x00dc
        L_0x0155:
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            throw r0     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
        L_0x0159:
            if (r10 != r4) goto L_0x0190
            android.os.Parcel r10 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            int r12 = r11.length     // Catch:{ ParseException -> 0x0174 }
            r10.unmarshall(r11, r5, r12)     // Catch:{ ParseException -> 0x0174 }
            r10.setDataPosition(r5)     // Catch:{ ParseException -> 0x0174 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzq> r11 = com.google.android.gms.measurement.internal.zzq.CREATOR     // Catch:{ ParseException -> 0x0174 }
            java.lang.Object r11 = r11.createFromParcel(r10)     // Catch:{ ParseException -> 0x0174 }
            com.google.android.gms.measurement.internal.zzq r11 = (com.google.android.gms.measurement.internal.zzq) r11     // Catch:{ ParseException -> 0x0174 }
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x0185
        L_0x0172:
            r0 = move-exception
            goto L_0x018c
        L_0x0174:
            com.google.android.gms.measurement.internal.zzef r11 = r24.zzab()     // Catch:{ all -> 0x0172 }
            com.google.android.gms.measurement.internal.zzeh r11 = r11.zzgk()     // Catch:{ all -> 0x0172 }
            java.lang.String r12 = "Failed to load user property from local database"
            r11.zzao(r12)     // Catch:{ all -> 0x0172 }
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            r11 = r2
        L_0x0185:
            if (r11 == 0) goto L_0x00dc
            r3.add(r11)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x00dc
        L_0x018c:
            r10.recycle()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            throw r0     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
        L_0x0190:
            if (r10 != r0) goto L_0x01a1
            com.google.android.gms.measurement.internal.zzef r10 = r24.zzab()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            com.google.android.gms.measurement.internal.zzeh r10 = r10.zzgn()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            java.lang.String r11 = "Skipping app launch break"
            r10.zzao(r11)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x00dc
        L_0x01a1:
            com.google.android.gms.measurement.internal.zzef r10 = r24.zzab()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            com.google.android.gms.measurement.internal.zzeh r10 = r10.zzgk()     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            java.lang.String r11 = "Unknown record type in local database"
            r10.zzao(r11)     // Catch:{ SQLiteFullException -> 0x01b3, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01b0 }
            goto L_0x00dc
        L_0x01b0:
            r0 = move-exception
            goto L_0x0230
        L_0x01b3:
            r0 = move-exception
            goto L_0x0270
        L_0x01b6:
            java.lang.String r0 = "messages"
            java.lang.String r4 = "rowid <= ?"
            java.lang.String[] r10 = new java.lang.String[r8]     // Catch:{ SQLiteFullException -> 0x01fd, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01f5, all -> 0x01f0 }
            java.lang.String r11 = java.lang.Long.toString(r18)     // Catch:{ SQLiteFullException -> 0x01fd, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01f5, all -> 0x01f0 }
            r10[r5] = r11     // Catch:{ SQLiteFullException -> 0x01fd, SQLiteDatabaseLockedException -> 0x01f9, SQLiteException -> 0x01f5, all -> 0x01f0 }
            r11 = r22
            int r0 = r11.delete(r0, r4, r10)     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
            int r4 = r3.size()     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
            if (r0 >= r4) goto L_0x01db
            com.google.android.gms.measurement.internal.zzef r0 = r24.zzab()     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgk()     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
            java.lang.String r4 = "Fewer entries removed from local database than expected"
            r0.zzao(r4)     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
        L_0x01db:
            r11.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
            r11.endTransaction()     // Catch:{ SQLiteFullException -> 0x01ee, SQLiteDatabaseLockedException -> 0x0257, SQLiteException -> 0x01ec }
            if (r9 == 0) goto L_0x01e6
            r9.close()
        L_0x01e6:
            if (r11 == 0) goto L_0x01eb
            r11.close()
        L_0x01eb:
            return r3
        L_0x01ec:
            r0 = move-exception
            goto L_0x021b
        L_0x01ee:
            r0 = move-exception
            goto L_0x0223
        L_0x01f0:
            r0 = move-exception
            r11 = r22
            goto L_0x028f
        L_0x01f5:
            r0 = move-exception
            r11 = r22
            goto L_0x0230
        L_0x01f9:
            r11 = r22
            goto L_0x0257
        L_0x01fd:
            r0 = move-exception
            r11 = r22
            goto L_0x0270
        L_0x0202:
            r0 = move-exception
            r11 = r22
            goto L_0x00a9
        L_0x0207:
            r0 = move-exception
            r11 = r22
            goto L_0x00af
        L_0x020c:
            r11 = r22
            goto L_0x021e
        L_0x020f:
            r0 = move-exception
            r11 = r22
            goto L_0x00b5
        L_0x0214:
            r0 = move-exception
            r11 = r15
            r9 = r2
            goto L_0x0269
        L_0x0218:
            r0 = move-exception
            r11 = r15
            r9 = r2
        L_0x021b:
            r22 = r11
            goto L_0x0230
        L_0x021e:
            r9 = r2
            goto L_0x0257
        L_0x0220:
            r0 = move-exception
            r11 = r15
            r9 = r2
        L_0x0223:
            r22 = r11
            goto L_0x0270
        L_0x0226:
            r0 = move-exception
            r9 = r2
            r22 = r9
            goto L_0x028f
        L_0x022c:
            r0 = move-exception
            r9 = r2
            r22 = r9
        L_0x0230:
            if (r22 == 0) goto L_0x023b
            boolean r4 = r22.inTransaction()     // Catch:{ all -> 0x028e }
            if (r4 == 0) goto L_0x023b
            r22.endTransaction()     // Catch:{ all -> 0x028e }
        L_0x023b:
            com.google.android.gms.measurement.internal.zzef r4 = r24.zzab()     // Catch:{ all -> 0x028e }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgk()     // Catch:{ all -> 0x028e }
            java.lang.String r10 = "Error reading entries from local database"
            r4.zza(r10, r0)     // Catch:{ all -> 0x028e }
            r1.zzjw = r8     // Catch:{ all -> 0x028e }
            if (r9 == 0) goto L_0x024f
            r9.close()
        L_0x024f:
            if (r22 == 0) goto L_0x0289
            r22.close()
            goto L_0x0289
        L_0x0255:
            r9 = r2
            r11 = r9
        L_0x0257:
            long r12 = (long) r7
            android.os.SystemClock.sleep(r12)     // Catch:{ all -> 0x0268 }
            int r7 = r7 + 20
            if (r9 == 0) goto L_0x0262
            r9.close()
        L_0x0262:
            if (r11 == 0) goto L_0x0289
            r11.close()
            goto L_0x0289
        L_0x0268:
            r0 = move-exception
        L_0x0269:
            r22 = r11
            goto L_0x028f
        L_0x026c:
            r0 = move-exception
            r9 = r2
            r22 = r9
        L_0x0270:
            com.google.android.gms.measurement.internal.zzef r4 = r24.zzab()     // Catch:{ all -> 0x028e }
            com.google.android.gms.measurement.internal.zzeh r4 = r4.zzgk()     // Catch:{ all -> 0x028e }
            java.lang.String r10 = "Error reading entries from local database"
            r4.zza(r10, r0)     // Catch:{ all -> 0x028e }
            r1.zzjw = r8     // Catch:{ all -> 0x028e }
            if (r9 == 0) goto L_0x0284
            r9.close()
        L_0x0284:
            if (r22 == 0) goto L_0x0289
            r22.close()
        L_0x0289:
            int r6 = r6 + 1
            r4 = 5
            goto L_0x001e
        L_0x028e:
            r0 = move-exception
        L_0x028f:
            if (r9 == 0) goto L_0x0294
            r9.close()
        L_0x0294:
            if (r22 == 0) goto L_0x0299
            r22.close()
        L_0x0299:
            throw r0
        L_0x029a:
            com.google.android.gms.measurement.internal.zzef r0 = r24.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgn()
            java.lang.String r3 = "Failed to read events from database in reasonable time"
            r0.zzao(r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzeb.zzc(int):java.util.List");
    }

    @WorkerThread
    public final boolean zzgh() {
        return zza(3, new byte[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0056 A[SYNTHETIC, Splitter:B:31:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0098 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0098 A[SYNTHETIC] */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzgi() {
        /*
            r11 = this;
            r11.zzo()
            r11.zzm()
            boolean r0 = r11.zzjw
            r1 = 0
            if (r0 == 0) goto L_0x000c
            return r1
        L_0x000c:
            boolean r0 = r11.zzcg()
            if (r0 != 0) goto L_0x0013
            return r1
        L_0x0013:
            r0 = 5
            r2 = 0
            r3 = 5
        L_0x0016:
            if (r2 >= r0) goto L_0x00a3
            r4 = 0
            r5 = 1
            android.database.sqlite.SQLiteDatabase r6 = r11.getWritableDatabase()     // Catch:{ SQLiteFullException -> 0x0080, SQLiteDatabaseLockedException -> 0x0074, SQLiteException -> 0x0050 }
            if (r6 != 0) goto L_0x002c
            r11.zzjw = r5     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            if (r6 == 0) goto L_0x0027
            r6.close()
        L_0x0027:
            return r1
        L_0x0028:
            r4 = move-exception
            goto L_0x0054
        L_0x002a:
            r4 = move-exception
            goto L_0x0084
        L_0x002c:
            r6.beginTransaction()     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            java.lang.String r4 = "messages"
            java.lang.String r7 = "type == ?"
            java.lang.String[] r8 = new java.lang.String[r5]     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            r9 = 3
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            r8[r1] = r9     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            r6.delete(r4, r7, r8)     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            r6.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            r6.endTransaction()     // Catch:{ SQLiteFullException -> 0x002a, SQLiteDatabaseLockedException -> 0x004b, SQLiteException -> 0x0028 }
            if (r6 == 0) goto L_0x004a
            r6.close()
        L_0x004a:
            return r5
        L_0x004b:
            r4 = r6
            goto L_0x0074
        L_0x004d:
            r0 = move-exception
            r6 = r4
            goto L_0x009d
        L_0x0050:
            r6 = move-exception
            r10 = r6
            r6 = r4
            r4 = r10
        L_0x0054:
            if (r6 == 0) goto L_0x005f
            boolean r7 = r6.inTransaction()     // Catch:{ all -> 0x009c }
            if (r7 == 0) goto L_0x005f
            r6.endTransaction()     // Catch:{ all -> 0x009c }
        L_0x005f:
            com.google.android.gms.measurement.internal.zzef r7 = r11.zzab()     // Catch:{ all -> 0x009c }
            com.google.android.gms.measurement.internal.zzeh r7 = r7.zzgk()     // Catch:{ all -> 0x009c }
            java.lang.String r8 = "Error deleting app launch break from local database"
            r7.zza(r8, r4)     // Catch:{ all -> 0x009c }
            r11.zzjw = r5     // Catch:{ all -> 0x009c }
            if (r6 == 0) goto L_0x0098
            r6.close()
            goto L_0x0098
        L_0x0074:
            long r5 = (long) r3
            android.os.SystemClock.sleep(r5)     // Catch:{ all -> 0x004d }
            int r3 = r3 + 20
            if (r4 == 0) goto L_0x0098
            r4.close()
            goto L_0x0098
        L_0x0080:
            r6 = move-exception
            r10 = r6
            r6 = r4
            r4 = r10
        L_0x0084:
            com.google.android.gms.measurement.internal.zzef r7 = r11.zzab()     // Catch:{ all -> 0x009c }
            com.google.android.gms.measurement.internal.zzeh r7 = r7.zzgk()     // Catch:{ all -> 0x009c }
            java.lang.String r8 = "Error deleting app launch break from local database"
            r7.zza(r8, r4)     // Catch:{ all -> 0x009c }
            r11.zzjw = r5     // Catch:{ all -> 0x009c }
            if (r6 == 0) goto L_0x0098
            r6.close()
        L_0x0098:
            int r2 = r2 + 1
            goto L_0x0016
        L_0x009c:
            r0 = move-exception
        L_0x009d:
            if (r6 == 0) goto L_0x00a2
            r6.close()
        L_0x00a2:
            throw r0
        L_0x00a3:
            com.google.android.gms.measurement.internal.zzef r0 = r11.zzab()
            com.google.android.gms.measurement.internal.zzeh r0 = r0.zzgn()
            java.lang.String r2 = "Error deleting app launch break from local database in reasonable time"
            r0.zzao(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzeb.zzgi():boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long zza(android.database.sqlite.SQLiteDatabase r12) {
        /*
            r0 = 0
            java.lang.String r2 = "messages"
            r1 = 1
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ all -> 0x003b }
            java.lang.String r4 = "rowid"
            r10 = 0
            r3[r10] = r4     // Catch:{ all -> 0x003b }
            java.lang.String r4 = "type=?"
            java.lang.String[] r5 = new java.lang.String[r1]     // Catch:{ all -> 0x003b }
            java.lang.String r1 = "3"
            r5[r10] = r1     // Catch:{ all -> 0x003b }
            r6 = 0
            r7 = 0
            java.lang.String r8 = "rowid desc"
            java.lang.String r9 = "1"
            r1 = r12
            android.database.Cursor r12 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x003b }
            boolean r0 = r12.moveToFirst()     // Catch:{ all -> 0x0036 }
            if (r0 == 0) goto L_0x002e
            long r0 = r12.getLong(r10)     // Catch:{ all -> 0x0036 }
            if (r12 == 0) goto L_0x002d
            r12.close()
        L_0x002d:
            return r0
        L_0x002e:
            if (r12 == 0) goto L_0x0033
            r12.close()
        L_0x0033:
            r0 = -1
            return r0
        L_0x0036:
            r0 = move-exception
            r11 = r0
            r0 = r12
            r12 = r11
            goto L_0x003c
        L_0x003b:
            r12 = move-exception
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()
        L_0x0041:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzeb.zza(android.database.sqlite.SQLiteDatabase):long");
    }

    @WorkerThread
    @VisibleForTesting
    private final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        if (this.zzjw) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.zzjv.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.zzjw = true;
        return null;
    }

    @VisibleForTesting
    private final boolean zzcg() {
        return getContext().getDatabasePath("google_app_measurement_local.db").exists();
    }

    public final /* bridge */ /* synthetic */ void zzl() {
        super.zzl();
    }

    public final /* bridge */ /* synthetic */ void zzm() {
        super.zzm();
    }

    public final /* bridge */ /* synthetic */ void zzn() {
        super.zzn();
    }

    public final /* bridge */ /* synthetic */ void zzo() {
        super.zzo();
    }

    public final /* bridge */ /* synthetic */ zza zzp() {
        return super.zzp();
    }

    public final /* bridge */ /* synthetic */ zzgp zzq() {
        return super.zzq();
    }

    public final /* bridge */ /* synthetic */ zzdy zzr() {
        return super.zzr();
    }

    public final /* bridge */ /* synthetic */ zzhv zzs() {
        return super.zzs();
    }

    public final /* bridge */ /* synthetic */ zzhq zzt() {
        return super.zzt();
    }

    public final /* bridge */ /* synthetic */ zzeb zzu() {
        return super.zzu();
    }

    public final /* bridge */ /* synthetic */ zziw zzv() {
        return super.zzv();
    }

    public final /* bridge */ /* synthetic */ zzac zzw() {
        return super.zzw();
    }

    public final /* bridge */ /* synthetic */ Clock zzx() {
        return super.zzx();
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public final /* bridge */ /* synthetic */ zzed zzy() {
        return super.zzy();
    }

    public final /* bridge */ /* synthetic */ zzjs zzz() {
        return super.zzz();
    }

    public final /* bridge */ /* synthetic */ zzfc zzaa() {
        return super.zzaa();
    }

    public final /* bridge */ /* synthetic */ zzef zzab() {
        return super.zzab();
    }

    public final /* bridge */ /* synthetic */ zzeo zzac() {
        return super.zzac();
    }

    public final /* bridge */ /* synthetic */ zzs zzad() {
        return super.zzad();
    }

    public final /* bridge */ /* synthetic */ zzr zzae() {
        return super.zzae();
    }
}
