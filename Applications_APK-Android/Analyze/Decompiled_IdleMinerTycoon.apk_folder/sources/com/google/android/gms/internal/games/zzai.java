package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzai extends zzaj {
    private final /* synthetic */ int zzkd;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzai(zzah zzah, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient, null);
        this.zzkd = i;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzkd);
    }
}
