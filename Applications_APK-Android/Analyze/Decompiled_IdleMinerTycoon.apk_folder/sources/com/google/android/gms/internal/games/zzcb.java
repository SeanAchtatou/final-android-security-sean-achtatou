package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzcb extends zzcf {
    private final /* synthetic */ String[] zzko;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcb(zzbz zzbz, GoogleApiClient googleApiClient, String[] strArr) {
        super(googleApiClient, null);
        this.zzko = strArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zzb(this, this.zzko);
    }
}
