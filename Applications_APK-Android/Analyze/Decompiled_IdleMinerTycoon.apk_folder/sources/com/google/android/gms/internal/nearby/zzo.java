package com.google.android.gms.internal.nearby;

public final class zzo {
    private final zzm zzaw = new zzm();

    public final zzo zza(zzdg zzdg) {
        zzdg unused = this.zzaw.zzas = zzdg;
        return this;
    }

    public final zzo zza(zzdw zzdw) {
        zzdw unused = this.zzaw.zzav = zzdw;
        return this;
    }

    public final zzo zza(zzdz zzdz) {
        zzdz unused = this.zzaw.zzar = zzdz;
        return this;
    }

    public final zzo zza(String str) {
        String unused = this.zzaw.zzat = str;
        return this;
    }

    public final zzo zza(byte[] bArr) {
        byte[] unused = this.zzaw.zzau = bArr;
        return this;
    }

    public final zzm zzb() {
        return this.zzaw;
    }
}
