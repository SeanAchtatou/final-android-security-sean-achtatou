package com.google.android.gms.internal.nearby;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Arrays;

@SafeParcelable.Class(creator = "SendConnectionRequestParamsCreator")
@SafeParcelable.Reserved({1000})
public final class zzfq extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzfq> CREATOR = new zzft();
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getName", id = 4)
    public String name;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getResultListenerAsBinder", id = 1, type = "android.os.IBinder")
    public zzdz zzar;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getConnectionEventListenerAsBinder", id = 2, type = "android.os.IBinder")
    public zzdg zzas;
    /* access modifiers changed from: private */
    @SafeParcelable.Field(getter = "getRemoteEndpointId", id = 5)
    public String zzat;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getHandshakeData", id = 6)
    public byte[] zzau;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getConnectionResponseListenerAsBinder", id = 3, type = "android.os.IBinder")
    public zzdm zzeb;
    /* access modifiers changed from: private */
    @Nullable
    @SafeParcelable.Field(getter = "getConnectionLifecycleListenerAsBinder", id = 7, type = "android.os.IBinder")
    public zzdj zzec;

    private zzfq() {
    }

    /* JADX WARN: Type inference failed for: r5v2, types: [android.os.IInterface] */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzfq(@android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) android.os.IBinder r15, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r16, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) android.os.IBinder r17, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) java.lang.String r18, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) java.lang.String r19, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 6) byte[] r20, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 7) android.os.IBinder r21) {
        /*
            r14 = this;
            r0 = r15
            r1 = r16
            r2 = r17
            r3 = r21
            r4 = 0
            if (r0 != 0) goto L_0x000c
            r7 = r4
            goto L_0x0021
        L_0x000c:
            java.lang.String r5 = "com.google.android.gms.nearby.internal.connection.IResultListener"
            android.os.IInterface r5 = r15.queryLocalInterface(r5)
            boolean r6 = r5 instanceof com.google.android.gms.internal.nearby.zzdz
            if (r6 == 0) goto L_0x001b
            r0 = r5
            com.google.android.gms.internal.nearby.zzdz r0 = (com.google.android.gms.internal.nearby.zzdz) r0
            r7 = r0
            goto L_0x0021
        L_0x001b:
            com.google.android.gms.internal.nearby.zzeb r5 = new com.google.android.gms.internal.nearby.zzeb
            r5.<init>(r15)
            r7 = r5
        L_0x0021:
            if (r1 != 0) goto L_0x0025
            r8 = r4
            goto L_0x0039
        L_0x0025:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IConnectionEventListener"
            android.os.IInterface r0 = r1.queryLocalInterface(r0)
            boolean r5 = r0 instanceof com.google.android.gms.internal.nearby.zzdg
            if (r5 == 0) goto L_0x0033
            com.google.android.gms.internal.nearby.zzdg r0 = (com.google.android.gms.internal.nearby.zzdg) r0
        L_0x0031:
            r8 = r0
            goto L_0x0039
        L_0x0033:
            com.google.android.gms.internal.nearby.zzdi r0 = new com.google.android.gms.internal.nearby.zzdi
            r0.<init>(r1)
            goto L_0x0031
        L_0x0039:
            if (r2 != 0) goto L_0x003d
            r9 = r4
            goto L_0x0051
        L_0x003d:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IConnectionResponseListener"
            android.os.IInterface r0 = r2.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.nearby.zzdm
            if (r1 == 0) goto L_0x004b
            com.google.android.gms.internal.nearby.zzdm r0 = (com.google.android.gms.internal.nearby.zzdm) r0
        L_0x0049:
            r9 = r0
            goto L_0x0051
        L_0x004b:
            com.google.android.gms.internal.nearby.zzdo r0 = new com.google.android.gms.internal.nearby.zzdo
            r0.<init>(r2)
            goto L_0x0049
        L_0x0051:
            if (r3 != 0) goto L_0x0055
        L_0x0053:
            r13 = r4
            goto L_0x0069
        L_0x0055:
            java.lang.String r0 = "com.google.android.gms.nearby.internal.connection.IConnectionLifecycleListener"
            android.os.IInterface r0 = r3.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.nearby.zzdj
            if (r1 == 0) goto L_0x0063
            r4 = r0
            com.google.android.gms.internal.nearby.zzdj r4 = (com.google.android.gms.internal.nearby.zzdj) r4
            goto L_0x0053
        L_0x0063:
            com.google.android.gms.internal.nearby.zzdl r4 = new com.google.android.gms.internal.nearby.zzdl
            r4.<init>(r3)
            goto L_0x0053
        L_0x0069:
            r6 = r14
            r10 = r18
            r11 = r19
            r12 = r20
            r6.<init>(r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.nearby.zzfq.<init>(android.os.IBinder, android.os.IBinder, android.os.IBinder, java.lang.String, java.lang.String, byte[], android.os.IBinder):void");
    }

    private zzfq(@Nullable zzdz zzdz, @Nullable zzdg zzdg, @Nullable zzdm zzdm, @Nullable String str, String str2, @Nullable byte[] bArr, @Nullable zzdj zzdj) {
        this.zzar = zzdz;
        this.zzas = zzdg;
        this.zzeb = zzdm;
        this.name = str;
        this.zzat = str2;
        this.zzau = bArr;
        this.zzec = zzdj;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof zzfq) {
            zzfq zzfq = (zzfq) obj;
            return Objects.equal(this.zzar, zzfq.zzar) && Objects.equal(this.zzas, zzfq.zzas) && Objects.equal(this.zzeb, zzfq.zzeb) && Objects.equal(this.name, zzfq.name) && Objects.equal(this.zzat, zzfq.zzat) && Arrays.equals(this.zzau, zzfq.zzau) && Objects.equal(this.zzec, zzfq.zzec);
        }
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzar, this.zzas, this.zzeb, this.name, this.zzat, Integer.valueOf(Arrays.hashCode(this.zzau)), this.zzec);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 1, this.zzar == null ? null : this.zzar.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzas == null ? null : this.zzas.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zzeb == null ? null : this.zzeb.asBinder(), false);
        SafeParcelWriter.writeString(parcel, 4, this.name, false);
        SafeParcelWriter.writeString(parcel, 5, this.zzat, false);
        SafeParcelWriter.writeByteArray(parcel, 6, this.zzau, false);
        if (this.zzec != null) {
            iBinder = this.zzec.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 7, iBinder, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
