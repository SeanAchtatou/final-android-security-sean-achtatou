package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "UnsubscribeRequestCreator")
public final class zzcg extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzcg> CREATOR = new zzch();
    @SafeParcelable.VersionField(id = 1)
    private final int versionCode;
    @Nullable
    @SafeParcelable.Field(id = 6)
    @Deprecated
    private final String zzff;
    @SafeParcelable.Field(id = 8)
    @Deprecated
    private final boolean zzfg;
    @Nullable
    @SafeParcelable.Field(id = 7)
    @Deprecated
    private final String zzfj;
    @SafeParcelable.Field(getter = "getCallbackAsBinder", id = 3, type = "android.os.IBinder")
    private final zzp zzhh;
    @Nullable
    @SafeParcelable.Field(id = 9)
    @Deprecated
    private final ClientAppContext zzhi;
    @Nullable
    @SafeParcelable.Field(getter = "getMessageListenerAsBinder", id = 2, type = "android.os.IBinder")
    private final zzm zziy;
    @Nullable
    @SafeParcelable.Field(id = 4)
    private final PendingIntent zzja;
    @SafeParcelable.Field(id = 5)
    @Deprecated
    private final int zzjb;

    /* JADX WARN: Type inference failed for: r0v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @android.support.annotation.VisibleForTesting
    @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzcg(@com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 1) int r3, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 2) android.os.IBinder r4, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 3) android.os.IBinder r5, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 4) android.app.PendingIntent r6, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 5) int r7, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 6) java.lang.String r8, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 7) java.lang.String r9, @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 8) boolean r10, @android.support.annotation.Nullable @com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param(id = 9) com.google.android.gms.nearby.messages.internal.ClientAppContext r11) {
        /*
            r2 = this;
            r2.<init>()
            r2.versionCode = r3
            r3 = 0
            if (r4 != 0) goto L_0x000a
            r4 = r3
            goto L_0x001e
        L_0x000a:
            java.lang.String r0 = "com.google.android.gms.nearby.messages.internal.IMessageListener"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.nearby.messages.internal.zzm
            if (r1 == 0) goto L_0x0018
            r4 = r0
            com.google.android.gms.nearby.messages.internal.zzm r4 = (com.google.android.gms.nearby.messages.internal.zzm) r4
            goto L_0x001e
        L_0x0018:
            com.google.android.gms.nearby.messages.internal.zzo r0 = new com.google.android.gms.nearby.messages.internal.zzo
            r0.<init>(r4)
            r4 = r0
        L_0x001e:
            r2.zziy = r4
            if (r5 != 0) goto L_0x0023
            goto L_0x0035
        L_0x0023:
            java.lang.String r3 = "com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback"
            android.os.IInterface r3 = r5.queryLocalInterface(r3)
            boolean r4 = r3 instanceof com.google.android.gms.nearby.messages.internal.zzp
            if (r4 == 0) goto L_0x0030
            com.google.android.gms.nearby.messages.internal.zzp r3 = (com.google.android.gms.nearby.messages.internal.zzp) r3
            goto L_0x0035
        L_0x0030:
            com.google.android.gms.nearby.messages.internal.zzr r3 = new com.google.android.gms.nearby.messages.internal.zzr
            r3.<init>(r5)
        L_0x0035:
            r2.zzhh = r3
            r2.zzja = r6
            r2.zzjb = r7
            r2.zzff = r8
            r2.zzfj = r9
            r2.zzfg = r10
            com.google.android.gms.nearby.messages.internal.ClientAppContext r3 = com.google.android.gms.nearby.messages.internal.ClientAppContext.zza(r11, r9, r8, r10)
            r2.zzhi = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.internal.zzcg.<init>(int, android.os.IBinder, android.os.IBinder, android.app.PendingIntent, int, java.lang.String, java.lang.String, boolean, com.google.android.gms.nearby.messages.internal.ClientAppContext):void");
    }

    @VisibleForTesting
    public zzcg(IBinder iBinder, IBinder iBinder2, @Nullable PendingIntent pendingIntent) {
        this(1, iBinder, iBinder2, pendingIntent, 0, null, null, false, null);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zziy == null ? null : this.zziy.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zzhh.asBinder(), false);
        SafeParcelWriter.writeParcelable(parcel, 4, this.zzja, i, false);
        SafeParcelWriter.writeInt(parcel, 5, this.zzjb);
        SafeParcelWriter.writeString(parcel, 6, this.zzff, false);
        SafeParcelWriter.writeString(parcel, 7, this.zzfj, false);
        SafeParcelWriter.writeBoolean(parcel, 8, this.zzfg);
        SafeParcelWriter.writeParcelable(parcel, 9, this.zzhi, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
