package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class zzclx implements zzclw<zzbyn> {
    private final zzbbl zzfqw;
    private final zzbxo zzgav;
    private final zzcaq zzgaw;

    public zzclx(zzbxo zzbxo, zzbbl zzbbl, zzcaq zzcaq) {
        this.zzgav = zzbxo;
        this.zzfqw = zzbbl;
        this.zzgaw = zzcaq;
    }

    public final boolean zza(zzcxu zzcxu, zzcxm zzcxm) {
        return (zzcxm.zzgke == null || zzcxm.zzgke.zzfmo == null) ? false : true;
    }

    public final zzbbh<List<zzbbh<zzbyn>>> zzb(zzcxu zzcxu, zzcxm zzcxm) {
        zzbbh<zzccj> zzamr = this.zzgav.zzadc().zzamr();
        this.zzgav.zzadc().zza(zzamr);
        return zzbar.zza(zzbar.zza(zzamr, new zzcly(this, zzcxm), this.zzfqw), new zzclz(this, zzcxu, zzcxm), this.zzfqw);
    }

    private final zzbbh<zzbyn> zzb(zzcxu zzcxu, zzcxm zzcxm, JSONObject jSONObject) {
        zzbbh<zzccj> zzamr = this.zzgav.zzadc().zzamr();
        zzbbh<zzbyt> zza = this.zzgaw.zza(zzcxu, zzcxm, jSONObject);
        return zzbar.zza(zzamr, zza).zza(new zzcmc(this, zza, zzamr, zzcxu, zzcxm, jSONObject), this.zzfqw);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbyn zza(zzbbh zzbbh, zzbbh zzbbh2, zzcxu zzcxu, zzcxm zzcxm, JSONObject jSONObject) throws Exception {
        zzbyt zzbyt = (zzbyt) zzbbh.get();
        zzccj zzccj = (zzccj) zzbbh2.get();
        zzbyv zza = this.zzgav.zza(new zzbpr(zzcxu, zzcxm, null), new zzbzf(zzbyt), new zzbyc(jSONObject, zzccj));
        zza.zzadk().zzaji();
        zza.zzadl().zzb(zzccj);
        zza.zzadm().zzk(zzbyt.zzahz());
        return zza.zzadj();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbbh zza(zzcxu zzcxu, zzcxm zzcxm, JSONArray jSONArray) throws Exception {
        if (jSONArray.length() == 0) {
            return zzbar.zzd(new zzcgm(3));
        }
        if (zzcxu.zzgkx.zzfjp.zzglg <= 1) {
            return zzbar.zza(zzb(zzcxu, zzcxm, jSONArray.getJSONObject(0)), zzcmb.zzdrn, this.zzfqw);
        }
        int length = jSONArray.length();
        this.zzgav.zzadc().zzdq(Math.min(length, zzcxu.zzgkx.zzfjp.zzglg));
        ArrayList arrayList = new ArrayList(zzcxu.zzgkx.zzfjp.zzglg);
        for (int i = 0; i < zzcxu.zzgkx.zzfjp.zzglg; i++) {
            if (i < length) {
                arrayList.add(zzb(zzcxu, zzcxm, jSONArray.getJSONObject(i)));
            } else {
                arrayList.add(zzbar.zzd(new zzcgm(3)));
            }
        }
        return zzbar.zzm(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazc.zza(java.lang.String, java.lang.Object):org.json.JSONObject
     arg types: [java.lang.String, boolean]
     candidates:
      com.google.android.gms.internal.ads.zzazc.zza(org.json.JSONArray, java.util.List<java.lang.String>):java.util.List<java.lang.String>
      com.google.android.gms.internal.ads.zzazc.zza(org.json.JSONObject, java.lang.String[]):org.json.JSONObject
      com.google.android.gms.internal.ads.zzazc.zza(android.util.JsonWriter, java.lang.Object):void
      com.google.android.gms.internal.ads.zzazc.zza(android.util.JsonWriter, org.json.JSONArray):void
      com.google.android.gms.internal.ads.zzazc.zza(android.util.JsonWriter, org.json.JSONObject):void
      com.google.android.gms.internal.ads.zzazc.zza(java.lang.String, java.lang.Object):org.json.JSONObject */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbbh zza(zzcxm zzcxm, zzccj zzccj) throws Exception {
        JSONObject zza = zzazc.zza("isNonagon", (Object) true);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("response", zzcxm.zzgke.zzfmo);
        jSONObject.put("sdk_params", zza);
        return zzbar.zza(zzccj.zzc("google.afma.nativeAds.preProcessJson", jSONObject), zzcma.zzbqz, this.zzfqw);
    }
}
