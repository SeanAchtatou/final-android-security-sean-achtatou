package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;
import android.view.View;

public final class zzbwc implements zzdti<View> {
    private final zzbvz zzflo;

    private zzbwc(zzbvz zzbvz) {
        this.zzflo = zzbvz;
    }

    public static zzbwc zza(zzbvz zzbvz) {
        return new zzbwc(zzbvz);
    }

    @Nullable
    public final /* synthetic */ Object get() {
        return this.zzflo.zzahb();
    }
}
