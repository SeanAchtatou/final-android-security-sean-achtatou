package com.google.android.gms.games.appcontent;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.zzd;

@UsedByReflection("GamesClientImpl.java")
@SafeParcelable.Class(creator = "AppContentTupleEntityCreator")
@SafeParcelable.Reserved({1000})
public final class AppContentTupleEntity extends zzd implements zzk {
    public static final Parcelable.Creator<AppContentTupleEntity> CREATOR = new zzl();
    @SafeParcelable.Field(getter = "getName", id = 1)
    private final String name;
    @SafeParcelable.Field(getter = "getValue", id = 2)
    private final String value;

    @SafeParcelable.Constructor
    AppContentTupleEntity(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) String str2) {
        this.name = str;
        this.value = str2;
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String getName() {
        return this.name;
    }

    public final String getValue() {
        return this.value;
    }

    public final int hashCode() {
        return Objects.hashCode(getName(), getValue());
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzk)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zzk zzk = (zzk) obj;
        if (!Objects.equal(zzk.getName(), getName()) || !Objects.equal(zzk.getValue(), getValue())) {
            return false;
        }
        return true;
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("Name", getName()).add("Value", getValue()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.name, false);
        SafeParcelWriter.writeString(parcel, 2, this.value, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
