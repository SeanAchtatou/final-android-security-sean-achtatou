package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzas extends zzaz {
    private final /* synthetic */ String zzbr;
    private final /* synthetic */ long zzbu;
    private final /* synthetic */ String zzbv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzas(zzal zzal, GoogleApiClient googleApiClient, String str, long j, String str2) {
        super(googleApiClient);
        this.zzbr = str;
        this.zzbu = j;
        this.zzbv = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzbr, this.zzbu, this.zzbv);
    }
}
