package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdj extends zzds {
    private final /* synthetic */ int zzld;
    private final /* synthetic */ int[] zzle;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdj(zzcz zzcz, GoogleApiClient googleApiClient, int i, int[] iArr) {
        super(googleApiClient, null);
        this.zzld = i;
        this.zzle = iArr;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzld, this.zzle);
    }
}
