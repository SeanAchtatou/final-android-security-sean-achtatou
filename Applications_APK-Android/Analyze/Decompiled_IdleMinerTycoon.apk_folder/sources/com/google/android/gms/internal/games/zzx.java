package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzx extends zzaa {
    private final /* synthetic */ String zzk;
    private final /* synthetic */ int zzl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzx(zzu zzu, GoogleApiClient googleApiClient, String str, int i) {
        super(googleApiClient, null);
        this.zzk = str;
        this.zzl = i;
    }

    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this.zzk, this.zzl);
    }
}
