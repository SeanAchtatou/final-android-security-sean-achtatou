package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@zzard
public final class zzawf implements zzawe {
    @Nullable
    public final zzbbh<String> zzdq(String str) {
        return zzbar.zzm(null);
    }

    @Nullable
    public final zzbbh<String> zzdr(String str) {
        return zzbar.zzm(null);
    }
}
