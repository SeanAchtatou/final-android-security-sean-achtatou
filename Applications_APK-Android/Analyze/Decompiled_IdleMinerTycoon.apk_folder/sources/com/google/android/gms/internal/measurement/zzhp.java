package com.google.android.gms.internal.measurement;

import java.io.IOException;

abstract class zzhp<T, B> {
    zzhp() {
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, long j);

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, zzdp zzdp);

    /* access modifiers changed from: package-private */
    public abstract void zza(B b, int i, T t);

    /* access modifiers changed from: package-private */
    public abstract void zza(T t, zzim zzim) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzgy zzgy);

    /* access modifiers changed from: package-private */
    public abstract void zzb(B b, int i, long j);

    /* access modifiers changed from: package-private */
    public abstract void zzc(B b, int i, int i2);

    /* access modifiers changed from: package-private */
    public abstract void zzc(T t, zzim zzim) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zze(Object obj, T t);

    /* access modifiers changed from: package-private */
    public abstract void zzf(Object obj, B b);

    /* access modifiers changed from: package-private */
    public abstract T zzg(T t, T t2);

    /* access modifiers changed from: package-private */
    public abstract void zzj(Object obj);

    /* access modifiers changed from: package-private */
    public abstract T zzp(B b);

    /* access modifiers changed from: package-private */
    public abstract int zzt(T t);

    /* access modifiers changed from: package-private */
    public abstract B zzwp();

    /* access modifiers changed from: package-private */
    public abstract T zzx(Object obj);

    /* access modifiers changed from: package-private */
    public abstract B zzy(Object obj);

    /* access modifiers changed from: package-private */
    public abstract int zzz(T t);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzdp):void
     arg types: [B, int, com.google.android.gms.internal.measurement.zzdp]
     candidates:
      com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, long):void
      com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzdp):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(B r7, com.google.android.gms.internal.measurement.zzgy r8) throws java.io.IOException {
        /*
            r6 = this;
            int r0 = r8.getTag()
            int r1 = r0 >>> 3
            r0 = r0 & 7
            r2 = 1
            switch(r0) {
                case 0: goto L_0x0055;
                case 1: goto L_0x004d;
                case 2: goto L_0x0045;
                case 3: goto L_0x001b;
                case 4: goto L_0x0019;
                case 5: goto L_0x0011;
                default: goto L_0x000c;
            }
        L_0x000c:
            com.google.android.gms.internal.measurement.zzfh r7 = com.google.android.gms.internal.measurement.zzfi.zzuy()
            throw r7
        L_0x0011:
            int r8 = r8.zzsl()
            r6.zzc(r7, r1, r8)
            return r2
        L_0x0019:
            r7 = 0
            return r7
        L_0x001b:
            java.lang.Object r0 = r6.zzwp()
            int r3 = r1 << 3
            r3 = r3 | 4
        L_0x0023:
            int r4 = r8.zzsy()
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L_0x0032
            boolean r4 = r6.zza(r0, r8)
            if (r4 != 0) goto L_0x0023
        L_0x0032:
            int r8 = r8.getTag()
            if (r3 != r8) goto L_0x0040
            java.lang.Object r8 = r6.zzp(r0)
            r6.zza(r7, r1, r8)
            return r2
        L_0x0040:
            com.google.android.gms.internal.measurement.zzfi r7 = com.google.android.gms.internal.measurement.zzfi.zzux()
            throw r7
        L_0x0045:
            com.google.android.gms.internal.measurement.zzdp r8 = r8.zzso()
            r6.zza(r7, r1, r8)
            return r2
        L_0x004d:
            long r3 = r8.zzsk()
            r6.zzb(r7, r1, r3)
            return r2
        L_0x0055:
            long r3 = r8.zzsi()
            r6.zza(r7, r1, r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzhp.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzgy):boolean");
    }
}
