package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzbq;
import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;

public final class zzbw extends zziq<zzbw> {
    public String zzcg = null;
    public Long zzzk = null;
    private Integer zzzl = null;
    public zzbq.zza[] zzzm = new zzbq.zza[0];
    public zzbx[] zzzn = zzbx.zzrc();
    public zzbv[] zzzo = zzbv.zzqx();
    private String zzzp = null;
    public Boolean zzzq = null;

    public zzbw() {
        this.zzaoo = null;
        this.zzaow = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbw)) {
            return false;
        }
        zzbw zzbw = (zzbw) obj;
        if (this.zzzk == null) {
            if (zzbw.zzzk != null) {
                return false;
            }
        } else if (!this.zzzk.equals(zzbw.zzzk)) {
            return false;
        }
        if (this.zzcg == null) {
            if (zzbw.zzcg != null) {
                return false;
            }
        } else if (!this.zzcg.equals(zzbw.zzcg)) {
            return false;
        }
        if (this.zzzl == null) {
            if (zzbw.zzzl != null) {
                return false;
            }
        } else if (!this.zzzl.equals(zzbw.zzzl)) {
            return false;
        }
        if (!zziu.equals(this.zzzm, zzbw.zzzm) || !zziu.equals(this.zzzn, zzbw.zzzn) || !zziu.equals(this.zzzo, zzbw.zzzo)) {
            return false;
        }
        if (this.zzzp == null) {
            if (zzbw.zzzp != null) {
                return false;
            }
        } else if (!this.zzzp.equals(zzbw.zzzp)) {
            return false;
        }
        if (this.zzzq == null) {
            if (zzbw.zzzq != null) {
                return false;
            }
        } else if (!this.zzzq.equals(zzbw.zzzq)) {
            return false;
        }
        if (this.zzaoo == null || this.zzaoo.isEmpty()) {
            return zzbw.zzaoo == null || zzbw.zzaoo.isEmpty();
        }
        return this.zzaoo.equals(zzbw.zzaoo);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((((((getClass().getName().hashCode() + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31) + (this.zzzk == null ? 0 : this.zzzk.hashCode())) * 31) + (this.zzcg == null ? 0 : this.zzcg.hashCode())) * 31) + (this.zzzl == null ? 0 : this.zzzl.hashCode())) * 31) + zziu.hashCode(this.zzzm)) * 31) + zziu.hashCode(this.zzzn)) * 31) + zziu.hashCode(this.zzzo)) * 31) + (this.zzzp == null ? 0 : this.zzzp.hashCode())) * 31) + (this.zzzq == null ? 0 : this.zzzq.hashCode())) * 31;
        if (this.zzaoo != null && !this.zzaoo.isEmpty()) {
            i = this.zzaoo.hashCode();
        }
        return hashCode + i;
    }

    public final void zza(zzio zzio) throws IOException {
        if (this.zzzk != null) {
            long longValue = this.zzzk.longValue();
            zzio.zzb(1, 0);
            zzio.zzbz(longValue);
        }
        if (this.zzcg != null) {
            zzio.zzb(2, this.zzcg);
        }
        if (this.zzzl != null) {
            zzio.zzc(3, this.zzzl.intValue());
        }
        if (this.zzzm != null && this.zzzm.length > 0) {
            for (zzbq.zza zza : this.zzzm) {
                if (zza != null) {
                    zzio.zze(4, zza);
                }
            }
        }
        if (this.zzzn != null && this.zzzn.length > 0) {
            for (zzbx zzbx : this.zzzn) {
                if (zzbx != null) {
                    zzio.zza(5, zzbx);
                }
            }
        }
        if (this.zzzo != null && this.zzzo.length > 0) {
            for (zzbv zzbv : this.zzzo) {
                if (zzbv != null) {
                    zzio.zza(6, zzbv);
                }
            }
        }
        if (this.zzzp != null) {
            zzio.zzb(7, this.zzzp);
        }
        if (this.zzzq != null) {
            zzio.zzb(8, this.zzzq.booleanValue());
        }
        super.zza(zzio);
    }

    /* access modifiers changed from: protected */
    public final int zzqy() {
        int zzqy = super.zzqy();
        if (this.zzzk != null) {
            long longValue = this.zzzk.longValue();
            zzqy += zzio.zzbi(1) + ((-128 & longValue) == 0 ? 1 : (-16384 & longValue) == 0 ? 2 : (-2097152 & longValue) == 0 ? 3 : (-268435456 & longValue) == 0 ? 4 : (-34359738368L & longValue) == 0 ? 5 : (-4398046511104L & longValue) == 0 ? 6 : (-562949953421312L & longValue) == 0 ? 7 : (-72057594037927936L & longValue) == 0 ? 8 : (longValue & Long.MIN_VALUE) == 0 ? 9 : 10);
        }
        if (this.zzcg != null) {
            zzqy += zzio.zzc(2, this.zzcg);
        }
        if (this.zzzl != null) {
            zzqy += zzio.zzg(3, this.zzzl.intValue());
        }
        if (this.zzzm != null && this.zzzm.length > 0) {
            int i = zzqy;
            for (zzbq.zza zza : this.zzzm) {
                if (zza != null) {
                    i += zzee.zzc(4, zza);
                }
            }
            zzqy = i;
        }
        if (this.zzzn != null && this.zzzn.length > 0) {
            int i2 = zzqy;
            for (zzbx zzbx : this.zzzn) {
                if (zzbx != null) {
                    i2 += zzio.zzb(5, zzbx);
                }
            }
            zzqy = i2;
        }
        if (this.zzzo != null && this.zzzo.length > 0) {
            for (zzbv zzbv : this.zzzo) {
                if (zzbv != null) {
                    zzqy += zzio.zzb(6, zzbv);
                }
            }
        }
        if (this.zzzp != null) {
            zzqy += zzio.zzc(7, this.zzzp);
        }
        if (this.zzzq == null) {
            return zzqy;
        }
        this.zzzq.booleanValue();
        return zzqy + zzio.zzbi(8) + 1;
    }

    public final /* synthetic */ zziw zza(zzil zzil) throws IOException {
        while (true) {
            int zzsg = zzil.zzsg();
            if (zzsg == 0) {
                return this;
            }
            if (zzsg == 8) {
                this.zzzk = Long.valueOf(zzil.zztb());
            } else if (zzsg == 18) {
                this.zzcg = zzil.readString();
            } else if (zzsg == 24) {
                this.zzzl = Integer.valueOf(zzil.zzta());
            } else if (zzsg == 34) {
                int zzb = zzix.zzb(zzil, 34);
                int length = this.zzzm == null ? 0 : this.zzzm.length;
                zzbq.zza[] zzaArr = new zzbq.zza[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzzm, 0, zzaArr, 0, length);
                }
                while (length < zzaArr.length - 1) {
                    zzaArr[length] = (zzbq.zza) zzil.zza(zzbq.zza.zzkj());
                    zzil.zzsg();
                    length++;
                }
                zzaArr[length] = (zzbq.zza) zzil.zza(zzbq.zza.zzkj());
                this.zzzm = zzaArr;
            } else if (zzsg == 42) {
                int zzb2 = zzix.zzb(zzil, 42);
                int length2 = this.zzzn == null ? 0 : this.zzzn.length;
                zzbx[] zzbxArr = new zzbx[(zzb2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.zzzn, 0, zzbxArr, 0, length2);
                }
                while (length2 < zzbxArr.length - 1) {
                    zzbxArr[length2] = new zzbx();
                    zzil.zza(zzbxArr[length2]);
                    zzil.zzsg();
                    length2++;
                }
                zzbxArr[length2] = new zzbx();
                zzil.zza(zzbxArr[length2]);
                this.zzzn = zzbxArr;
            } else if (zzsg == 50) {
                int zzb3 = zzix.zzb(zzil, 50);
                int length3 = this.zzzo == null ? 0 : this.zzzo.length;
                zzbv[] zzbvArr = new zzbv[(zzb3 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.zzzo, 0, zzbvArr, 0, length3);
                }
                while (length3 < zzbvArr.length - 1) {
                    zzbvArr[length3] = new zzbv();
                    zzil.zza(zzbvArr[length3]);
                    zzil.zzsg();
                    length3++;
                }
                zzbvArr[length3] = new zzbv();
                zzil.zza(zzbvArr[length3]);
                this.zzzo = zzbvArr;
            } else if (zzsg == 58) {
                this.zzzp = zzil.readString();
            } else if (zzsg == 64) {
                this.zzzq = Boolean.valueOf(zzil.zzsm());
            } else if (!super.zza(zzil, zzsg)) {
                return this;
            }
        }
    }
}
