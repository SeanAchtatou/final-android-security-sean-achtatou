package com.google.android.gms.internal.measurement;

import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;

public final class zzbx extends zziq<zzbx> {
    private static volatile zzbx[] zzzr;
    public String name = null;
    public Boolean zzzs = null;
    public Boolean zzzt = null;
    public Integer zzzu = null;

    public static zzbx[] zzrc() {
        if (zzzr == null) {
            synchronized (zziu.zzaov) {
                if (zzzr == null) {
                    zzzr = new zzbx[0];
                }
            }
        }
        return zzzr;
    }

    public zzbx() {
        this.zzaoo = null;
        this.zzaow = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbx)) {
            return false;
        }
        zzbx zzbx = (zzbx) obj;
        if (this.name == null) {
            if (zzbx.name != null) {
                return false;
            }
        } else if (!this.name.equals(zzbx.name)) {
            return false;
        }
        if (this.zzzs == null) {
            if (zzbx.zzzs != null) {
                return false;
            }
        } else if (!this.zzzs.equals(zzbx.zzzs)) {
            return false;
        }
        if (this.zzzt == null) {
            if (zzbx.zzzt != null) {
                return false;
            }
        } else if (!this.zzzt.equals(zzbx.zzzt)) {
            return false;
        }
        if (this.zzzu == null) {
            if (zzbx.zzzu != null) {
                return false;
            }
        } else if (!this.zzzu.equals(zzbx.zzzu)) {
            return false;
        }
        if (this.zzaoo == null || this.zzaoo.isEmpty()) {
            return zzbx.zzaoo == null || zzbx.zzaoo.isEmpty();
        }
        return this.zzaoo.equals(zzbx.zzaoo);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((getClass().getName().hashCode() + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31) + (this.name == null ? 0 : this.name.hashCode())) * 31) + (this.zzzs == null ? 0 : this.zzzs.hashCode())) * 31) + (this.zzzt == null ? 0 : this.zzzt.hashCode())) * 31) + (this.zzzu == null ? 0 : this.zzzu.hashCode())) * 31;
        if (this.zzaoo != null && !this.zzaoo.isEmpty()) {
            i = this.zzaoo.hashCode();
        }
        return hashCode + i;
    }

    public final void zza(zzio zzio) throws IOException {
        if (this.name != null) {
            zzio.zzb(1, this.name);
        }
        if (this.zzzs != null) {
            zzio.zzb(2, this.zzzs.booleanValue());
        }
        if (this.zzzt != null) {
            zzio.zzb(3, this.zzzt.booleanValue());
        }
        if (this.zzzu != null) {
            zzio.zzc(4, this.zzzu.intValue());
        }
        super.zza(zzio);
    }

    /* access modifiers changed from: protected */
    public final int zzqy() {
        int zzqy = super.zzqy();
        if (this.name != null) {
            zzqy += zzio.zzc(1, this.name);
        }
        if (this.zzzs != null) {
            this.zzzs.booleanValue();
            zzqy += zzio.zzbi(2) + 1;
        }
        if (this.zzzt != null) {
            this.zzzt.booleanValue();
            zzqy += zzio.zzbi(3) + 1;
        }
        return this.zzzu != null ? zzqy + zzio.zzg(4, this.zzzu.intValue()) : zzqy;
    }

    public final /* synthetic */ zziw zza(zzil zzil) throws IOException {
        while (true) {
            int zzsg = zzil.zzsg();
            if (zzsg == 0) {
                return this;
            }
            if (zzsg == 10) {
                this.name = zzil.readString();
            } else if (zzsg == 16) {
                this.zzzs = Boolean.valueOf(zzil.zzsm());
            } else if (zzsg == 24) {
                this.zzzt = Boolean.valueOf(zzil.zzsm());
            } else if (zzsg == 32) {
                this.zzzu = Integer.valueOf(zzil.zzta());
            } else if (!super.zza(zzil, zzsg)) {
                return this;
            }
        }
    }
}
