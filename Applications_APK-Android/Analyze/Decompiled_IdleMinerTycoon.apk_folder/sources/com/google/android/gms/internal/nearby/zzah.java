package com.google.android.gms.internal.nearby;

import com.google.android.gms.nearby.connection.Connections;

final class zzah extends zzau<Connections.ConnectionRequestListener> {
    private final /* synthetic */ zzej zzbn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzah(zzag zzag, zzej zzej) {
        super();
        this.zzbn = zzej;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((Connections.ConnectionRequestListener) obj).onConnectionRequest(this.zzbn.zzg(), this.zzbn.zzh(), this.zzbn.zzj());
    }
}
