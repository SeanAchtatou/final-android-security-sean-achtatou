package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzbk;
import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;

public final class zzbv extends zziq<zzbv> {
    private static volatile zzbv[] zzze;
    public Integer zzzf = null;
    public zzbk.zzd[] zzzg = new zzbk.zzd[0];
    public zzbk.zza[] zzzh = new zzbk.zza[0];
    private Boolean zzzi = null;
    private Boolean zzzj = null;

    public static zzbv[] zzqx() {
        if (zzze == null) {
            synchronized (zziu.zzaov) {
                if (zzze == null) {
                    zzze = new zzbv[0];
                }
            }
        }
        return zzze;
    }

    public zzbv() {
        this.zzaoo = null;
        this.zzaow = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbv)) {
            return false;
        }
        zzbv zzbv = (zzbv) obj;
        if (this.zzzf == null) {
            if (zzbv.zzzf != null) {
                return false;
            }
        } else if (!this.zzzf.equals(zzbv.zzzf)) {
            return false;
        }
        if (!zziu.equals(this.zzzg, zzbv.zzzg) || !zziu.equals(this.zzzh, zzbv.zzzh)) {
            return false;
        }
        if (this.zzzi == null) {
            if (zzbv.zzzi != null) {
                return false;
            }
        } else if (!this.zzzi.equals(zzbv.zzzi)) {
            return false;
        }
        if (this.zzzj == null) {
            if (zzbv.zzzj != null) {
                return false;
            }
        } else if (!this.zzzj.equals(zzbv.zzzj)) {
            return false;
        }
        if (this.zzaoo == null || this.zzaoo.isEmpty()) {
            return zzbv.zzaoo == null || zzbv.zzaoo.isEmpty();
        }
        return this.zzaoo.equals(zzbv.zzaoo);
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((getClass().getName().hashCode() + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31) + (this.zzzf == null ? 0 : this.zzzf.hashCode())) * 31) + zziu.hashCode(this.zzzg)) * 31) + zziu.hashCode(this.zzzh)) * 31) + (this.zzzi == null ? 0 : this.zzzi.hashCode())) * 31) + (this.zzzj == null ? 0 : this.zzzj.hashCode())) * 31;
        if (this.zzaoo != null && !this.zzaoo.isEmpty()) {
            i = this.zzaoo.hashCode();
        }
        return hashCode + i;
    }

    public final void zza(zzio zzio) throws IOException {
        if (this.zzzf != null) {
            zzio.zzc(1, this.zzzf.intValue());
        }
        if (this.zzzg != null && this.zzzg.length > 0) {
            for (zzbk.zzd zzd : this.zzzg) {
                if (zzd != null) {
                    zzio.zze(2, zzd);
                }
            }
        }
        if (this.zzzh != null && this.zzzh.length > 0) {
            for (zzbk.zza zza : this.zzzh) {
                if (zza != null) {
                    zzio.zze(3, zza);
                }
            }
        }
        if (this.zzzi != null) {
            zzio.zzb(4, this.zzzi.booleanValue());
        }
        if (this.zzzj != null) {
            zzio.zzb(5, this.zzzj.booleanValue());
        }
        super.zza(zzio);
    }

    /* access modifiers changed from: protected */
    public final int zzqy() {
        int zzqy = super.zzqy();
        if (this.zzzf != null) {
            zzqy += zzio.zzg(1, this.zzzf.intValue());
        }
        if (this.zzzg != null && this.zzzg.length > 0) {
            int i = zzqy;
            for (zzbk.zzd zzd : this.zzzg) {
                if (zzd != null) {
                    i += zzee.zzc(2, zzd);
                }
            }
            zzqy = i;
        }
        if (this.zzzh != null && this.zzzh.length > 0) {
            for (zzbk.zza zza : this.zzzh) {
                if (zza != null) {
                    zzqy += zzee.zzc(3, zza);
                }
            }
        }
        if (this.zzzi != null) {
            this.zzzi.booleanValue();
            zzqy += zzio.zzbi(4) + 1;
        }
        if (this.zzzj == null) {
            return zzqy;
        }
        this.zzzj.booleanValue();
        return zzqy + zzio.zzbi(5) + 1;
    }

    public final /* synthetic */ zziw zza(zzil zzil) throws IOException {
        while (true) {
            int zzsg = zzil.zzsg();
            if (zzsg == 0) {
                return this;
            }
            if (zzsg == 8) {
                this.zzzf = Integer.valueOf(zzil.zzta());
            } else if (zzsg == 18) {
                int zzb = zzix.zzb(zzil, 18);
                int length = this.zzzg == null ? 0 : this.zzzg.length;
                zzbk.zzd[] zzdArr = new zzbk.zzd[(zzb + length)];
                if (length != 0) {
                    System.arraycopy(this.zzzg, 0, zzdArr, 0, length);
                }
                while (length < zzdArr.length - 1) {
                    zzdArr[length] = (zzbk.zzd) zzil.zza(zzbk.zzd.zzkj());
                    zzil.zzsg();
                    length++;
                }
                zzdArr[length] = (zzbk.zzd) zzil.zza(zzbk.zzd.zzkj());
                this.zzzg = zzdArr;
            } else if (zzsg == 26) {
                int zzb2 = zzix.zzb(zzil, 26);
                int length2 = this.zzzh == null ? 0 : this.zzzh.length;
                zzbk.zza[] zzaArr = new zzbk.zza[(zzb2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.zzzh, 0, zzaArr, 0, length2);
                }
                while (length2 < zzaArr.length - 1) {
                    zzaArr[length2] = (zzbk.zza) zzil.zza(zzbk.zza.zzkj());
                    zzil.zzsg();
                    length2++;
                }
                zzaArr[length2] = (zzbk.zza) zzil.zza(zzbk.zza.zzkj());
                this.zzzh = zzaArr;
            } else if (zzsg == 32) {
                this.zzzi = Boolean.valueOf(zzil.zzsm());
            } else if (zzsg == 40) {
                this.zzzj = Boolean.valueOf(zzil.zzsm());
            } else if (!super.zza(zzil, zzsg)) {
                return this;
            }
        }
    }
}
