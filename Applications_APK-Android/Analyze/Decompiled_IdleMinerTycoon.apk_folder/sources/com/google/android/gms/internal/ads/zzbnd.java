package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;
import android.view.ViewGroup;

public final class zzbnd implements zzdti<ViewGroup> {
    private final zzbnc zzfgv;

    public zzbnd(zzbnc zzbnc) {
        this.zzfgv = zzbnc;
    }

    @Nullable
    public final /* synthetic */ Object get() {
        return this.zzfgv.zzafh();
    }
}
