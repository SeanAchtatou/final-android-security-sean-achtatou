package com.google.android.gms.nearby.messages.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.common.api.internal.ListenerHolder;

interface zzbd {
    void zza(zzah zzah, ListenerHolder<BaseImplementation.ResultHolder<Status>> listenerHolder) throws RemoteException;
}
