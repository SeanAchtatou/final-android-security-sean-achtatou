package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.zzu;

@SafeParcelable.Class(creator = "OnListParentsResponseCreator")
@SafeParcelable.Reserved({1})
public final class zzfp extends zzu {
    public static final Parcelable.Creator<zzfp> CREATOR = new zzfq();
    @SafeParcelable.Field(id = 2)
    final DataHolder zzht;

    @SafeParcelable.Constructor
    public zzfp(@SafeParcelable.Param(id = 2) DataHolder dataHolder) {
        this.zzht = dataHolder;
    }

    /* access modifiers changed from: protected */
    public final void zza(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzht, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final DataHolder zzam() {
        return this.zzht;
    }
}
