package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.snapshot.SnapshotContents;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;

final class zzcm extends zzct {
    private final /* synthetic */ SnapshotMetadataChange zzku;
    private final /* synthetic */ String zzkw;
    private final /* synthetic */ String zzkx;
    private final /* synthetic */ SnapshotContents zzky;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcm(zzch zzch, GoogleApiClient googleApiClient, String str, String str2, SnapshotMetadataChange snapshotMetadataChange, SnapshotContents snapshotContents) {
        super(googleApiClient, null);
        this.zzkw = str;
        this.zzkx = str2;
        this.zzku = snapshotMetadataChange;
        this.zzky = snapshotContents;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zza(this, this.zzkw, this.zzkx, this.zzku, this.zzky);
    }
}
