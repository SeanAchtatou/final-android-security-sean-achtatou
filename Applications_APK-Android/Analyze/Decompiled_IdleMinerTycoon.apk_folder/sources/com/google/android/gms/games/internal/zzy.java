package com.google.android.gms.games.internal;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.internal.zze;

final class zzy extends zze.zzat<Games.GetServerAuthCodeResult> {
    zzy(BaseImplementation.ResultHolder resultHolder) {
        super(resultHolder);
    }

    public final void zza(int i, String str) {
        setResult(new zze.zzm(GamesStatusCodes.zza(i), str));
    }
}
