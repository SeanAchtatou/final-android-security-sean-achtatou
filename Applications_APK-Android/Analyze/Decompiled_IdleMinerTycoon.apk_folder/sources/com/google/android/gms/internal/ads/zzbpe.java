package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;

public interface zzbpe<AdT> {
    @Nullable
    zzcjv<AdT> zze(int i, String str);
}
