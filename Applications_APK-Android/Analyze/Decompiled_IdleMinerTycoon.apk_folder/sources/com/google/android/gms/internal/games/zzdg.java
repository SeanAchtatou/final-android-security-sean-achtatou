package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.internal.zze;

final class zzdg extends zzdo {
    private final /* synthetic */ String zzey;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdg(zzcz zzcz, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzey = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zze) anyClient).zze(this, this.zzey);
    }
}
