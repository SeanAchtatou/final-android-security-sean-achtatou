package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.stats.Stats;

abstract class zzcx extends Games.zza<Stats.LoadPlayerStatsResult> {
    private zzcx(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzcy(this, status);
    }

    /* synthetic */ zzcx(GoogleApiClient googleApiClient, zzcw zzcw) {
        this(googleApiClient);
    }
}
