package com.google.android.gms.internal.ads;

import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.ads.internal.zzk;
import com.unity.purchasing.googleplay.Consts;
import org.json.JSONException;
import org.json.JSONObject;

final class zzcis implements zzalm<zzcir> {
    zzcis() {
    }

    public final /* synthetic */ JSONObject zzj(Object obj) throws JSONException {
        zzcir zzcir = (zzcir) obj;
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        jSONObject2.put("base_url", zzcir.zzfxu.zztu());
        jSONObject2.put("signals", zzcir.zzfxt);
        jSONObject3.put("body", zzcir.zzfxs.zzdnh);
        jSONObject3.put("headers", zzk.zzlg().zzi(zzcir.zzfxs.zzab));
        jSONObject3.put(Consts.INAPP_RESPONSE_CODE, zzcir.zzfxs.zzfya);
        jSONObject3.put("latency", zzcir.zzfxs.zzfyb);
        jSONObject.put(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jSONObject2);
        jSONObject.put("response", jSONObject3);
        jSONObject.put("flags", zzcir.zzfxu.zztx());
        return jSONObject;
    }
}
