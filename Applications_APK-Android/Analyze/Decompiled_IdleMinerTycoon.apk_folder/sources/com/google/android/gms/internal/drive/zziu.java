package com.google.android.gms.internal.drive;

import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class zziu implements Cloneable {
    private Object value;
    private zzis<?, ?> zznc;
    private List<zziz> zznd = new ArrayList();

    zziu() {
    }

    private final byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzaq()];
        zza(zzip.zzb(bArr));
        return bArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: zzbj */
    public final zziu clone() {
        Object clone;
        zziu zziu = new zziu();
        try {
            zziu.zznc = this.zznc;
            if (this.zznd == null) {
                zziu.zznd = null;
            } else {
                zziu.zznd.addAll(this.zznd);
            }
            if (this.value != null) {
                if (this.value instanceof zzix) {
                    clone = (zzix) ((zzix) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    clone = ((byte[]) this.value).clone();
                } else {
                    int i = 0;
                    if (this.value instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.value;
                        byte[][] bArr2 = new byte[bArr.length][];
                        zziu.value = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.value instanceof boolean[]) {
                        clone = ((boolean[]) this.value).clone();
                    } else if (this.value instanceof int[]) {
                        clone = ((int[]) this.value).clone();
                    } else if (this.value instanceof long[]) {
                        clone = ((long[]) this.value).clone();
                    } else if (this.value instanceof float[]) {
                        clone = ((float[]) this.value).clone();
                    } else if (this.value instanceof double[]) {
                        clone = ((double[]) this.value).clone();
                    } else if (this.value instanceof zzix[]) {
                        zzix[] zzixArr = (zzix[]) this.value;
                        zzix[] zzixArr2 = new zzix[zzixArr.length];
                        zziu.value = zzixArr2;
                        while (i < zzixArr.length) {
                            zzixArr2[i] = (zzix) zzixArr[i].clone();
                            i++;
                        }
                    }
                }
                zziu.value = clone;
            }
            return zziu;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zziu)) {
            return false;
        }
        zziu zziu = (zziu) obj;
        if (this.value == null || zziu.value == null) {
            if (this.zznd != null && zziu.zznd != null) {
                return this.zznd.equals(zziu.zznd);
            }
            try {
                return Arrays.equals(toByteArray(), zziu.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.zznc != zziu.zznc) {
            return false;
        } else {
            return !this.zznc.zzmx.isArray() ? this.value.equals(zziu.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zziu.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zziu.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zziu.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zziu.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zziu.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zziu.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zziu.value);
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(toByteArray()) + IronSourceError.ERROR_NON_EXISTENT_INSTANCE;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzip zzip) throws IOException {
        if (this.value == null) {
            for (zziz next : this.zznd) {
                zzip.zzp(next.tag);
                zzip.zzc(next.zzng);
            }
            return;
        }
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void zza(zziz zziz) throws IOException {
        if (this.zznd != null) {
            this.zznd.add(zziz);
        } else if (this.value instanceof zzix) {
            byte[] bArr = zziz.zzng;
            zzio zza = zzio.zza(bArr, 0, bArr.length);
            int zzbe = zza.zzbe();
            if (zzbe == bArr.length - zzip.zzm(zzbe)) {
                zzix zza2 = ((zzix) this.value).zza(zza);
                this.zznc = this.zznc;
                this.value = zza2;
                this.zznd = null;
                return;
            }
            throw zziw.zzbk();
        } else if (this.value instanceof zzix[]) {
            Collections.singletonList(zziz);
            throw new NoSuchMethodError();
        } else {
            Collections.singletonList(zziz);
            throw new NoSuchMethodError();
        }
    }

    /* access modifiers changed from: package-private */
    public final int zzaq() {
        if (this.value == null) {
            int i = 0;
            for (zziz next : this.zznd) {
                i += zzip.zzq(next.tag) + 0 + next.zzng.length;
            }
            return i;
        }
        throw new NoSuchMethodError();
    }
}
