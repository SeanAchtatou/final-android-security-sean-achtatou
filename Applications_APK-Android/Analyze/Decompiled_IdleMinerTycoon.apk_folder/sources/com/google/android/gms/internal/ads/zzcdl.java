package com.google.android.gms.internal.ads;

import com.ironsource.sdk.constants.Constants;

public final class zzcdl implements zzdti<String> {
    private static final zzcdl zzftf = new zzcdl();

    public static zzcdl zzajl() {
        return zzftf;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdto.zza(Constants.CONVERT_REWARDED, "Cannot return null from a non-@Nullable @Provides method");
    }
}
