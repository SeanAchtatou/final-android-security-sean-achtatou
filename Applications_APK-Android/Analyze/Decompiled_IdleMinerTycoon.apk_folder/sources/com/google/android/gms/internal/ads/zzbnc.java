package com.google.android.gms.internal.ads;

import android.support.annotation.Nullable;
import android.view.ViewGroup;

public class zzbnc {
    private final ViewGroup zzfgu;

    public zzbnc(@Nullable ViewGroup viewGroup) {
        this.zzfgu = viewGroup;
    }

    @Nullable
    public final ViewGroup zzafh() {
        return this.zzfgu;
    }
}
