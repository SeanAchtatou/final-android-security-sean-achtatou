package com.google.android.gms.games.internal;

import android.support.annotation.NonNull;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;

public interface zzbl<Data> {
    ApiException zza(@NonNull Status status, @NonNull Data data);
}
