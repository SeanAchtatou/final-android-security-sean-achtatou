package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;

public abstract class zzczf<E> {
    /* access modifiers changed from: private */
    public static final zzbbh<?> zzgmh = zzbar.zzm(null);
    /* access modifiers changed from: private */
    public final ScheduledExecutorService zzfkf;
    /* access modifiers changed from: private */
    public final zzbbl zzfqw;
    /* access modifiers changed from: private */
    public final zzczr<E> zzgmi;

    public zzczf(zzbbl zzbbl, ScheduledExecutorService scheduledExecutorService, zzczr<E> zzczr) {
        this.zzfqw = zzbbl;
        this.zzfkf = scheduledExecutorService;
        this.zzgmi = zzczr;
    }

    /* access modifiers changed from: protected */
    public abstract String zzw(E e);

    public final zzczj zzv(E e) {
        return new zzczj(this, e);
    }

    public final <I> zzczl<I> zza(Object obj, zzbbh zzbbh) {
        return new zzczl(this, obj, zzbbh, Collections.singletonList(zzbbh), zzbbh);
    }

    public final zzczh zza(Object obj, zzbbh<?>... zzbbhArr) {
        return new zzczh(this, obj, Arrays.asList(zzbbhArr));
    }
}
