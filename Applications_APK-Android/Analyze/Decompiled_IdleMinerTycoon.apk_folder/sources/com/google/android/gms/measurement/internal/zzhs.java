package com.google.android.gms.measurement.internal;

final class zzhs implements Runnable {
    private final /* synthetic */ zzhr zzqy;
    private final /* synthetic */ zzhq zzqz;

    zzhs(zzhq zzhq, zzhr zzhr) {
        this.zzqz = zzhq;
        this.zzqy = zzhr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, int]
     candidates:
      com.google.android.gms.measurement.internal.zzhq.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzhr, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhr, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.zzhq.zza(com.google.android.gms.measurement.internal.zzhq, com.google.android.gms.measurement.internal.zzhr, boolean):void */
    public final void run() {
        this.zzqz.zza(this.zzqy, false);
        this.zzqz.zzqo = null;
        this.zzqz.zzs().zza((zzhr) null);
    }
}
