package com.google.android.gms.internal.drive;

import android.content.IntentSender;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzep extends zza implements zzeo {
    zzep(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.drive.internal.IDriveService");
    }

    public final IntentSender zza(zzgg zzgg) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgg);
        Parcel transactAndReadException = transactAndReadException(10, obtainAndWriteInterfaceToken);
        IntentSender intentSender = (IntentSender) zzc.zza(transactAndReadException, IntentSender.CREATOR);
        transactAndReadException.recycle();
        return intentSender;
    }

    public final IntentSender zza(zzu zzu) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzu);
        Parcel transactAndReadException = transactAndReadException(11, obtainAndWriteInterfaceToken);
        IntentSender intentSender = (IntentSender) zzc.zza(transactAndReadException, IntentSender.CREATOR);
        transactAndReadException.recycle();
        return intentSender;
    }

    public final zzec zza(zzgd zzgd, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgd);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        Parcel transactAndReadException = transactAndReadException(7, obtainAndWriteInterfaceToken);
        zzec zzec = (zzec) zzc.zza(transactAndReadException, zzec.CREATOR);
        transactAndReadException.recycle();
        return zzec;
    }

    public final void zza(zzab zzab, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzab);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(24, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzad zzad) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzad);
        transactAndReadExceptionReturnVoid(16, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzek zzek, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzek);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(1, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(9, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzex zzex, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzex);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(13, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgk zzgk, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgk);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(2, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgm zzgm, zzes zzes, String str, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgm);
        zzc.zza(obtainAndWriteInterfaceToken, zzes);
        obtainAndWriteInterfaceToken.writeString(null);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(15, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgo zzgo, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgo);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(36, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgq zzgq, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgq);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(28, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgv zzgv, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgv);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(17, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgx zzgx, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgx);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(38, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzgz zzgz, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzgz);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(3, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzj zzj, zzes zzes, String str, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzj);
        zzc.zza(obtainAndWriteInterfaceToken, zzes);
        obtainAndWriteInterfaceToken.writeString(null);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(14, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzm zzm, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzm);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(18, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzo zzo, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzo);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(8, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzr zzr, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzr);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(4, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzw zzw, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzw);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(5, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzy zzy, zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzy);
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(6, obtainAndWriteInterfaceToken);
    }

    public final void zzb(zzeq zzeq) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzc.zza(obtainAndWriteInterfaceToken, zzeq);
        transactAndReadExceptionReturnVoid(35, obtainAndWriteInterfaceToken);
    }
}
