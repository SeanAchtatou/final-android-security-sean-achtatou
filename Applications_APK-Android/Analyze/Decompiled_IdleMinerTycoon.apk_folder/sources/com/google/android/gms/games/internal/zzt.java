package com.google.android.gms.games.internal;

import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.video.Videos;

final /* synthetic */ class zzt implements zze.zzap {
    private final int zzhc;

    zzt(int i) {
        this.zzhc = i;
    }

    public final void accept(Object obj) {
        ((Videos.CaptureOverlayStateListener) obj).onCaptureOverlayStateChanged(this.zzhc);
    }
}
