package com.google.android.gms.internal.nearby;

import com.google.android.gms.common.api.internal.ListenerHolder;

public abstract class zzha<T> implements ListenerHolder.Notifier<T> {
    public void onNotifyListenerFailed() {
    }
}
