package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "PopupLocationInfoParcelableCreator")
@SafeParcelable.Reserved({1000})
public final class zzbw extends zzd {
    public static final Parcelable.Creator<zzbw> CREATOR = new zzbx();
    @SafeParcelable.Field(getter = "getInfoBundle", id = 1)
    private final Bundle zzjt;
    @SafeParcelable.Field(getter = "getWindowToken", id = 2)
    private final IBinder zzju;

    public zzbw(zzca zzca) {
        this.zzjt = zzca.zzcs();
        this.zzju = zzca.zzju;
    }

    @SafeParcelable.Constructor
    zzbw(@SafeParcelable.Param(id = 1) Bundle bundle, @SafeParcelable.Param(id = 2) IBinder iBinder) {
        this.zzjt = bundle;
        this.zzju = iBinder;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBundle(parcel, 1, this.zzjt, false);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzju, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
