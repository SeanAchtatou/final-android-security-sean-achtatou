package com.google.android.gms.internal.measurement;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzgt {
    private static final zzgt zzalc = new zzgt();
    private final zzha zzald = new zzfv();
    private final ConcurrentMap<Class<?>, zzgx<?>> zzale = new ConcurrentHashMap();

    public static zzgt zzvy() {
        return zzalc;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzez.zza(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.zzez.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.zzez.zza(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzez.zza(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.zzgx<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.zzez.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.zzez.zza(java.lang.Object, java.lang.String):T */
    public final <T> zzgx<T> zzf(Class<T> cls) {
        zzez.zza((Object) cls, "messageType");
        zzgx<T> zzgx = this.zzale.get(cls);
        if (zzgx != null) {
            return zzgx;
        }
        zzgx<T> zze = this.zzald.zze(cls);
        zzez.zza((Object) cls, "messageType");
        zzez.zza((Object) zze, "schema");
        zzgx<T> putIfAbsent = this.zzale.putIfAbsent(cls, zze);
        return putIfAbsent != null ? putIfAbsent : zze;
    }

    public final <T> zzgx<T> zzw(T t) {
        return zzf(t.getClass());
    }

    private zzgt() {
    }
}
