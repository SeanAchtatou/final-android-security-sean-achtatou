package com.google.android.gms.internal.ads;

import android.os.Bundle;
import im.getsocial.sdk.consts.LanguageCodes;

final /* synthetic */ class zzcrm implements zzcuz {
    private final String zzddy;

    zzcrm(String str) {
        this.zzddy = str;
    }

    public final void zzt(Object obj) {
        ((Bundle) obj).putString(LanguageCodes.MALAY, this.zzddy);
    }
}
