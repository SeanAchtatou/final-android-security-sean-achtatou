package com.google.android.gms.internal.nearby;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

final class zzcf extends zzcy {
    private final /* synthetic */ String zzcv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcf(zzca zzca, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient, null);
        this.zzcv = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzx) anyClient).disconnectFromEndpoint(this.zzcv);
    }
}
