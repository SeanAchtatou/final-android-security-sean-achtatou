package com.google.android.gms.drive.metadata;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.tapjoy.TJAdUnitConstants;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class zza<T> implements MetadataField<T> {
    private final String fieldName;
    private final Set<String> zzig;
    private final Set<String> zzih;
    private final int zzii;

    protected zza(String str, int i) {
        this.fieldName = (String) Preconditions.checkNotNull(str, "fieldName");
        this.zzig = Collections.singleton(str);
        this.zzih = Collections.emptySet();
        this.zzii = i;
    }

    protected zza(String str, Collection<String> collection, Collection<String> collection2, int i) {
        this.fieldName = (String) Preconditions.checkNotNull(str, "fieldName");
        this.zzig = Collections.unmodifiableSet(new HashSet(collection));
        this.zzih = Collections.unmodifiableSet(new HashSet(collection2));
        this.zzii = i;
    }

    public final String getName() {
        return this.fieldName;
    }

    public String toString() {
        return this.fieldName;
    }

    public final T zza(Bundle bundle) {
        Preconditions.checkNotNull(bundle, TJAdUnitConstants.String.BUNDLE);
        if (bundle.get(this.fieldName) != null) {
            return zzb(bundle);
        }
        return null;
    }

    public final T zza(DataHolder dataHolder, int i, int i2) {
        if (zzb(dataHolder, i, i2)) {
            return zzc(dataHolder, i, i2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract void zza(Bundle bundle, T t);

    public final void zza(DataHolder dataHolder, MetadataBundle metadataBundle, int i, int i2) {
        Preconditions.checkNotNull(dataHolder, "dataHolder");
        Preconditions.checkNotNull(metadataBundle, TJAdUnitConstants.String.BUNDLE);
        if (zzb(dataHolder, i, i2)) {
            metadataBundle.zzb(this, zzc(dataHolder, i, i2));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.drive.metadata.zza.zza(android.os.Bundle, java.lang.Object):void
     arg types: [android.os.Bundle, T]
     candidates:
      com.google.android.gms.drive.metadata.zza.zza(java.lang.Object, android.os.Bundle):void
      com.google.android.gms.drive.metadata.MetadataField.zza(java.lang.Object, android.os.Bundle):void
      com.google.android.gms.drive.metadata.zza.zza(android.os.Bundle, java.lang.Object):void */
    public final void zza(T t, Bundle bundle) {
        Preconditions.checkNotNull(bundle, TJAdUnitConstants.String.BUNDLE);
        if (t == null) {
            bundle.putString(this.fieldName, null);
        } else {
            zza(bundle, (Object) t);
        }
    }

    public final Collection<String> zzar() {
        return this.zzig;
    }

    /* access modifiers changed from: protected */
    public abstract T zzb(Bundle bundle);

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean zzb(com.google.android.gms.common.data.DataHolder r4, int r5, int r6) {
        /*
            r3 = this;
            java.util.Set<java.lang.String> r0 = r3.zzig
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0020
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r4.hasColumn(r1)
            if (r2 == 0) goto L_0x001e
            boolean r1 = r4.hasNull(r1, r5, r6)
            if (r1 == 0) goto L_0x0006
        L_0x001e:
            r4 = 0
            return r4
        L_0x0020:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.metadata.zza.zzb(com.google.android.gms.common.data.DataHolder, int, int):boolean");
    }

    /* access modifiers changed from: protected */
    public abstract T zzc(DataHolder dataHolder, int i, int i2);
}
