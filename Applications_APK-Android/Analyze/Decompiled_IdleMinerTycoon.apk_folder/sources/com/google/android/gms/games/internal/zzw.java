package com.google.android.gms.games.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.internal.zze;
import java.util.ArrayList;

final class zzw extends zze.zzv<T> {
    private final /* synthetic */ DataHolder zzhf;
    private final /* synthetic */ zze.zzav zzhg;
    private final /* synthetic */ ArrayList zzhh;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzw(zze.zzav zzav, DataHolder dataHolder, ArrayList arrayList) {
        super(null);
        this.zzhg = zzav;
        this.zzhf = dataHolder;
        this.zzhh = arrayList;
    }

    public final void notifyListener(T t) {
        this.zzhg.zza(t, zze.zzay(this.zzhf), this.zzhh);
    }
}
