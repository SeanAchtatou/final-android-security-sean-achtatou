package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzey;
import com.google.android.gms.internal.measurement.zzey.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzey<MessageType extends zzey<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzdf<MessageType, BuilderType> {
    private static Map<Object, zzey<?, ?>> zzaib = new ConcurrentHashMap();
    protected zzhs zzahz = zzhs.zzwq();
    private int zzaia = -1;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class zzd {
        public static final int zzaid = 1;
        public static final int zzaie = 2;
        public static final int zzaif = 3;
        public static final int zzaig = 4;
        public static final int zzaih = 5;
        public static final int zzaii = 6;
        public static final int zzaij = 7;
        private static final /* synthetic */ int[] zzaik = {zzaid, zzaie, zzaif, zzaig, zzaih, zzaii, zzaij};
        public static final int zzail = 1;
        public static final int zzaim = 2;
        private static final /* synthetic */ int[] zzain = {zzail, zzaim};
        public static final int zzaio = 1;
        public static final int zzaip = 2;
        private static final /* synthetic */ int[] zzaiq = {zzaio, zzaip};

        public static int[] zzur() {
            return (int[]) zzaik.clone();
        }
    }

    public static class zze<ContainingType extends zzgi, Type> extends zzek<ContainingType, Type> {
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    public static abstract class zzb<MessageType extends zzb<MessageType, BuilderType>, BuilderType> extends zzey<MessageType, BuilderType> implements zzgk {
        protected zzeo<Object> zzaic = zzeo.zztr();

        /* access modifiers changed from: package-private */
        public final zzeo<Object> zzuq() {
            if (this.zzaic.isImmutable()) {
                this.zzaic = (zzeo) this.zzaic.clone();
            }
            return this.zzaic;
        }
    }

    public static class zzc<T extends zzey<T, ?>> extends zzdg<T> {
        private final T zzahw;

        public zzc(T t) {
            this.zzahw = t;
        }

        public final /* synthetic */ Object zzc(zzeb zzeb, zzel zzel) throws zzfi {
            return zzey.zza(this.zzahw, zzeb, zzel);
        }
    }

    public String toString() {
        return zzgj.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzact != 0) {
            return this.zzact;
        }
        this.zzact = zzgt.zzvy().zzw(this).hashCode(this);
        return this.zzact;
    }

    public static abstract class zza<MessageType extends zzey<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzdh<MessageType, BuilderType> {
        private final MessageType zzahw;
        protected MessageType zzahx;
        private boolean zzahy = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zza(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzahw = r3
                int r0 = com.google.android.gms.internal.measurement.zzey.zzd.zzaig
                r1 = 0
                java.lang.Object r3 = r3.zza(r0, r1, r1)
                com.google.android.gms.internal.measurement.zzey r3 = (com.google.android.gms.internal.measurement.zzey) r3
                r2.zzahx = r3
                r3 = 0
                r2.zzahy = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzey.zza.<init>(com.google.android.gms.internal.measurement.zzey):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected final void zzuc() {
            /*
                r3 = this;
                boolean r0 = r3.zzahy
                if (r0 == 0) goto L_0x0019
                MessageType r0 = r3.zzahx
                int r1 = com.google.android.gms.internal.measurement.zzey.zzd.zzaig
                r2 = 0
                java.lang.Object r0 = r0.zza(r1, r2, r2)
                com.google.android.gms.internal.measurement.zzey r0 = (com.google.android.gms.internal.measurement.zzey) r0
                MessageType r1 = r3.zzahx
                zza(r0, r1)
                r3.zzahx = r0
                r0 = 0
                r3.zzahy = r0
            L_0x0019:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzey.zza.zzuc():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.zzey.zza(com.google.android.gms.internal.measurement.zzey, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.measurement.zzey.zza(java.lang.Class, com.google.android.gms.internal.measurement.zzey):void
          com.google.android.gms.internal.measurement.zzdf.zza(java.lang.Iterable, java.util.List):void
          com.google.android.gms.internal.measurement.zzey.zza(com.google.android.gms.internal.measurement.zzey, boolean):boolean */
        public final boolean isInitialized() {
            return zzey.zza((zzey) this.zzahx, false);
        }

        /* renamed from: zzud */
        public MessageType zzuf() {
            if (this.zzahy) {
                return this.zzahx;
            }
            this.zzahx.zzry();
            this.zzahy = true;
            return this.zzahx;
        }

        /* renamed from: zzue */
        public final MessageType zzug() {
            MessageType messagetype = (zzey) zzuf();
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzhq(messagetype);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.zzey.zza.zza(com.google.android.gms.internal.measurement.zzey, com.google.android.gms.internal.measurement.zzey):void
         arg types: [MessageType, MessageType]
         candidates:
          com.google.android.gms.internal.measurement.zzey.zza.zza(com.google.android.gms.internal.measurement.zzeb, com.google.android.gms.internal.measurement.zzel):com.google.android.gms.internal.measurement.zzdh
          com.google.android.gms.internal.measurement.zzdh.zza(com.google.android.gms.internal.measurement.zzeb, com.google.android.gms.internal.measurement.zzel):BuilderType
          com.google.android.gms.internal.measurement.zzey.zza.zza(com.google.android.gms.internal.measurement.zzey, com.google.android.gms.internal.measurement.zzey):void */
        public final BuilderType zza(MessageType messagetype) {
            zzuc();
            zza((zzey) this.zzahx, (zzey) messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzgt.zzvy().zzw(messagetype).zzc(messagetype, messagetype2);
        }

        private final BuilderType zzb(byte[] bArr, int i, int i2, zzel zzel) throws zzfi {
            zzuc();
            try {
                zzgt.zzvy().zzw(this.zzahx).zza(this.zzahx, bArr, 0, i2 + 0, new zzdk(zzel));
                return this;
            } catch (zzfi e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw zzfi.zzut();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: zzb */
        public final BuilderType zza(zzeb zzeb, zzel zzel) throws IOException {
            zzuc();
            try {
                zzgt.zzvy().zzw(this.zzahx).zza(this.zzahx, zzec.zza(zzeb), zzel);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }

        public final /* synthetic */ zzdh zza(byte[] bArr, int i, int i2, zzel zzel) throws zzfi {
            return zzb(bArr, 0, i2, zzel);
        }

        public final /* synthetic */ zzdh zzru() {
            return (zza) clone();
        }

        public final /* synthetic */ zzgi zzuh() {
            return this.zzahw;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zza zza = (zza) ((zzey) this.zzahw).zza(zzd.zzaih, (Object) null, (Object) null);
            zza.zza((zzey) zzuf());
            return zza;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzey) zza(zzd.zzaii, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return zzgt.zzvy().zzw(this).equals(this, (zzey) obj);
    }

    /* access modifiers changed from: protected */
    public final void zzry() {
        zzgt.zzvy().zzw(this).zzj(this);
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzey<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> BuilderType zzui() {
        return (zza) zza(zzd.zzaih, (Object) null, (Object) null);
    }

    public final boolean isInitialized() {
        return zza(this, Boolean.TRUE.booleanValue());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: BuilderType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final BuilderType zzuj() {
        /*
            r2 = this;
            int r0 = com.google.android.gms.internal.measurement.zzey.zzd.zzaih
            r1 = 0
            java.lang.Object r0 = r2.zza(r0, r1, r1)
            com.google.android.gms.internal.measurement.zzey$zza r0 = (com.google.android.gms.internal.measurement.zzey.zza) r0
            r0.zza(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzey.zzuj():com.google.android.gms.internal.measurement.zzey$zza");
    }

    /* access modifiers changed from: package-private */
    public final int zzrt() {
        return this.zzaia;
    }

    /* access modifiers changed from: package-private */
    public final void zzam(int i) {
        this.zzaia = i;
    }

    public final void zzb(zzee zzee) throws IOException {
        zzgt.zzvy().zzf(getClass()).zza(this, zzei.zza(zzee));
    }

    public final int zzuk() {
        if (this.zzaia == -1) {
            this.zzaia = zzgt.zzvy().zzw(this).zzt(this);
        }
        return this.zzaia;
    }

    static <T extends zzey<?, ?>> T zzd(Class<T> cls) {
        T t = (zzey) zzaib.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzey) zzaib.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzey) ((zzey) zzhv.zzh(cls)).zza(zzd.zzaii, (Object) null, (Object) null);
            if (t != null) {
                zzaib.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzey<?, ?>> void zza(Class<T> cls, T t) {
        zzaib.put(cls, t);
    }

    protected static Object zza(zzgi zzgi, String str, Object[] objArr) {
        return new zzgv(zzgi, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static final <T extends com.google.android.gms.internal.measurement.zzey<T, ?>> boolean zza(T r3, boolean r4) {
        /*
            int r0 = com.google.android.gms.internal.measurement.zzey.zzd.zzaid
            r1 = 0
            java.lang.Object r0 = r3.zza(r0, r1, r1)
            java.lang.Byte r0 = (java.lang.Byte) r0
            byte r0 = r0.byteValue()
            r2 = 1
            if (r0 != r2) goto L_0x0011
            return r2
        L_0x0011:
            if (r0 != 0) goto L_0x0015
            r3 = 0
            return r3
        L_0x0015:
            com.google.android.gms.internal.measurement.zzgt r0 = com.google.android.gms.internal.measurement.zzgt.zzvy()
            com.google.android.gms.internal.measurement.zzgx r0 = r0.zzw(r3)
            boolean r0 = r0.zzv(r3)
            if (r4 == 0) goto L_0x002d
            int r4 = com.google.android.gms.internal.measurement.zzey.zzd.zzaie
            if (r0 == 0) goto L_0x0029
            r2 = r3
            goto L_0x002a
        L_0x0029:
            r2 = r1
        L_0x002a:
            r3.zza(r4, r2, r1)
        L_0x002d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzey.zza(com.google.android.gms.internal.measurement.zzey, boolean):boolean");
    }

    protected static zzfd zzul() {
        return zzfa.zzus();
    }

    protected static zzfg zzum() {
        return zzfw.zzvk();
    }

    protected static zzfg zza(zzfg zzfg) {
        int size = zzfg.size();
        return zzfg.zzbv(size == 0 ? 10 : size << 1);
    }

    protected static <E> zzff<E> zzun() {
        return zzgw.zzwb();
    }

    protected static <E> zzff<E> zza(zzff<E> zzff) {
        int size = zzff.size();
        return zzff.zzap(size == 0 ? 10 : size << 1);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static <T extends com.google.android.gms.internal.measurement.zzey<T, ?>> T zza(T r2, com.google.android.gms.internal.measurement.zzeb r3, com.google.android.gms.internal.measurement.zzel r4) throws com.google.android.gms.internal.measurement.zzfi {
        /*
            int r0 = com.google.android.gms.internal.measurement.zzey.zzd.zzaig
            r1 = 0
            java.lang.Object r2 = r2.zza(r0, r1, r1)
            com.google.android.gms.internal.measurement.zzey r2 = (com.google.android.gms.internal.measurement.zzey) r2
            com.google.android.gms.internal.measurement.zzgt r0 = com.google.android.gms.internal.measurement.zzgt.zzvy()     // Catch:{ IOException -> 0x002d, RuntimeException -> 0x001c }
            com.google.android.gms.internal.measurement.zzgx r0 = r0.zzw(r2)     // Catch:{ IOException -> 0x002d, RuntimeException -> 0x001c }
            com.google.android.gms.internal.measurement.zzec r3 = com.google.android.gms.internal.measurement.zzec.zza(r3)     // Catch:{ IOException -> 0x002d, RuntimeException -> 0x001c }
            r0.zza(r2, r3, r4)     // Catch:{ IOException -> 0x002d, RuntimeException -> 0x001c }
            r2.zzry()     // Catch:{ IOException -> 0x002d, RuntimeException -> 0x001c }
            return r2
        L_0x001c:
            r2 = move-exception
            java.lang.Throwable r3 = r2.getCause()
            boolean r3 = r3 instanceof com.google.android.gms.internal.measurement.zzfi
            if (r3 == 0) goto L_0x002c
            java.lang.Throwable r2 = r2.getCause()
            com.google.android.gms.internal.measurement.zzfi r2 = (com.google.android.gms.internal.measurement.zzfi) r2
            throw r2
        L_0x002c:
            throw r2
        L_0x002d:
            r3 = move-exception
            java.lang.Throwable r4 = r3.getCause()
            boolean r4 = r4 instanceof com.google.android.gms.internal.measurement.zzfi
            if (r4 == 0) goto L_0x003d
            java.lang.Throwable r2 = r3.getCause()
            com.google.android.gms.internal.measurement.zzfi r2 = (com.google.android.gms.internal.measurement.zzfi) r2
            throw r2
        L_0x003d:
            com.google.android.gms.internal.measurement.zzfi r4 = new com.google.android.gms.internal.measurement.zzfi
            java.lang.String r3 = r3.getMessage()
            r4.<init>(r3)
            com.google.android.gms.internal.measurement.zzfi r2 = r4.zzg(r2)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzey.zza(com.google.android.gms.internal.measurement.zzey, com.google.android.gms.internal.measurement.zzeb, com.google.android.gms.internal.measurement.zzel):com.google.android.gms.internal.measurement.zzey");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T extends com.google.android.gms.internal.measurement.zzey<T, ?>> T zza(T r6, byte[] r7, int r8, int r9, com.google.android.gms.internal.measurement.zzel r10) throws com.google.android.gms.internal.measurement.zzfi {
        /*
            int r8 = com.google.android.gms.internal.measurement.zzey.zzd.zzaig
            r0 = 0
            java.lang.Object r6 = r6.zza(r8, r0, r0)
            com.google.android.gms.internal.measurement.zzey r6 = (com.google.android.gms.internal.measurement.zzey) r6
            com.google.android.gms.internal.measurement.zzgt r8 = com.google.android.gms.internal.measurement.zzgt.zzvy()     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            com.google.android.gms.internal.measurement.zzgx r0 = r8.zzw(r6)     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r3 = 0
            com.google.android.gms.internal.measurement.zzdk r5 = new com.google.android.gms.internal.measurement.zzdk     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r5.<init>(r10)     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r1 = r6
            r2 = r7
            r4 = r9
            r0.zza(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r6.zzry()     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            int r7 = r6.zzact     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            if (r7 != 0) goto L_0x0025
            return r6
        L_0x0025:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r7.<init>()     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            throw r7     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
        L_0x002b:
            com.google.android.gms.internal.measurement.zzfi r7 = com.google.android.gms.internal.measurement.zzfi.zzut()
            com.google.android.gms.internal.measurement.zzfi r6 = r7.zzg(r6)
            throw r6
        L_0x0034:
            r7 = move-exception
            java.lang.Throwable r8 = r7.getCause()
            boolean r8 = r8 instanceof com.google.android.gms.internal.measurement.zzfi
            if (r8 == 0) goto L_0x0044
            java.lang.Throwable r6 = r7.getCause()
            com.google.android.gms.internal.measurement.zzfi r6 = (com.google.android.gms.internal.measurement.zzfi) r6
            throw r6
        L_0x0044:
            com.google.android.gms.internal.measurement.zzfi r8 = new com.google.android.gms.internal.measurement.zzfi
            java.lang.String r7 = r7.getMessage()
            r8.<init>(r7)
            com.google.android.gms.internal.measurement.zzfi r6 = r8.zzg(r6)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzey.zza(com.google.android.gms.internal.measurement.zzey, byte[], int, int, com.google.android.gms.internal.measurement.zzel):com.google.android.gms.internal.measurement.zzey");
    }

    protected static <T extends zzey<T, ?>> T zza(T t, byte[] bArr, zzel zzel) throws zzfi {
        T zza2 = zza(t, bArr, 0, bArr.length, zzel);
        if (zza2 == null || zza2.isInitialized()) {
            return zza2;
        }
        throw new zzfi(new zzhq(zza2).getMessage()).zzg(zza2);
    }

    public final /* synthetic */ zzgh zzuo() {
        zza zza2 = (zza) zza(zzd.zzaih, (Object) null, (Object) null);
        zza2.zza(this);
        return zza2;
    }

    public final /* synthetic */ zzgh zzup() {
        return (zza) zza(zzd.zzaih, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzgi zzuh() {
        return (zzey) zza(zzd.zzaii, (Object) null, (Object) null);
    }
}
