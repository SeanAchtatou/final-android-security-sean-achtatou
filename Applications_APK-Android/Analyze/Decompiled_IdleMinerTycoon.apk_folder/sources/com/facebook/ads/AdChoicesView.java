package com.facebook.ads;

import android.content.Context;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.dynamicloading.DynamicLoaderFactory;
import com.facebook.ads.internal.util.common.Preconditions;

@Keep
@Deprecated
public class AdChoicesView extends RelativeLayout {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.ads.AdChoicesView.<init>(android.content.Context, com.facebook.ads.NativeAdBase, boolean):void
     arg types: [android.content.Context, com.facebook.ads.NativeAdBase, int]
     candidates:
      com.facebook.ads.AdChoicesView.<init>(android.content.Context, com.facebook.ads.NativeAdBase, com.facebook.ads.NativeAdLayout):void
      com.facebook.ads.AdChoicesView.<init>(android.content.Context, com.facebook.ads.NativeAdBase, boolean):void */
    @Deprecated
    public AdChoicesView(Context context, NativeAdBase nativeAdBase) {
        this((Context) Preconditions.checkNotNull(context, "Context can not be null."), nativeAdBase, false);
    }

    @Deprecated
    public AdChoicesView(Context context, NativeAdBase nativeAdBase, @Nullable NativeAdLayout nativeAdLayout) {
        this((Context) Preconditions.checkNotNull(context, "Context can not be null."), nativeAdBase, false, nativeAdLayout);
    }

    @Deprecated
    public AdChoicesView(Context context, NativeAdBase nativeAdBase, boolean z) {
        this((Context) Preconditions.checkNotNull(context, "Context can not be null."), nativeAdBase, z, null);
    }

    @Deprecated
    public AdChoicesView(Context context, NativeAdBase nativeAdBase, boolean z, @Nullable NativeAdLayout nativeAdLayout) {
        super((Context) Preconditions.checkNotNull(context, "Context can not be null."));
        DynamicLoaderFactory.makeLoader(context).createAdChoicesViewApi(this, context, nativeAdBase).initialize(z, nativeAdLayout);
    }
}
