package com.facebook.ads;

import android.support.annotation.Keep;
import java.util.EnumSet;

@Keep
public enum CacheFlag {
    NONE,
    ICON,
    IMAGE,
    VIDEO;
    
    public static final EnumSet<CacheFlag> ALL = EnumSet.allOf(CacheFlag.class);
}
