package com.facebook.ads.internal.dynamicloading;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import com.facebook.ads.internal.api.BuildConfigApi;
import com.helpshift.logger.model.LogDatabaseTable;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class DexLoadErrorReporter {
    private static final String LOGGING_URL = "https://www.facebook.com/adnw_logging/";
    public static final double SAMPLING = 0.1d;
    private static final AtomicBoolean sAlreadyReported = new AtomicBoolean();

    @SuppressLint({"CatchGeneralException"})
    public static void reportDexLoadingIssue(final Context context, final String str, double d) {
        if (!sAlreadyReported.get() && Math.random() < d) {
            sAlreadyReported.set(true);
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:45:0x017d A[SYNTHETIC, Splitter:B:45:0x017d] */
                /* JADX WARNING: Removed duplicated region for block: B:50:0x018b A[SYNTHETIC, Splitter:B:50:0x018b] */
                /* JADX WARNING: Removed duplicated region for block: B:58:0x01a0 A[SYNTHETIC, Splitter:B:58:0x01a0] */
                /* JADX WARNING: Removed duplicated region for block: B:63:0x01ae A[SYNTHETIC, Splitter:B:63:0x01ae] */
                /* JADX WARNING: Removed duplicated region for block: B:68:0x01bc  */
                /* JADX WARNING: Removed duplicated region for block: B:73:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r12 = this;
                        super.run()
                        r0 = 0
                        java.net.URL r1 = new java.net.URL     // Catch:{ Throwable -> 0x0170, all -> 0x016b }
                        java.lang.String r2 = "https://www.facebook.com/adnw_logging/"
                        r1.<init>(r2)     // Catch:{ Throwable -> 0x0170, all -> 0x016b }
                        java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Throwable -> 0x0170, all -> 0x016b }
                        java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ Throwable -> 0x0170, all -> 0x016b }
                        java.lang.String r2 = "POST"
                        r1.setRequestMethod(r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r2 = "Content-Type"
                        java.lang.String r3 = "application/x-www-form-urlencoded;charset=UTF-8"
                        r1.setRequestProperty(r2, r3)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r2 = "Accept"
                        java.lang.String r3 = "application/json"
                        r1.setRequestProperty(r2, r3)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r2 = "Accept-Charset"
                        java.lang.String r3 = "UTF-8"
                        r1.setRequestProperty(r2, r3)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r2 = "user-agent"
                        java.lang.String r3 = "[FBAN/AudienceNetworkForAndroid;FBSN/Android]"
                        r1.setRequestProperty(r2, r3)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r2 = 1
                        r1.setDoOutput(r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r1.setDoInput(r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r1.connect()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.util.UUID r2 = java.util.UUID.randomUUID()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r3.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r4 = "attempt"
                        java.lang.String r5 = "0"
                        r3.put(r4, r5)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        android.content.Context r4 = r3     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        com.facebook.ads.internal.dynamicloading.DexLoadErrorReporter.addEnvFields(r4, r3, r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r4.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r5 = "subtype"
                        java.lang.String r6 = "generic"
                        r4.put(r5, r6)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r5 = "subtype_code"
                        java.lang.String r6 = "1320"
                        r4.put(r5, r6)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r5 = "caught_exception"
                        java.lang.String r6 = "1"
                        r4.put(r5, r6)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r5 = "stacktrace"
                        java.lang.String r6 = r4     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r4.put(r5, r6)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r5.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "id"
                        java.util.UUID r7 = java.util.UUID.randomUUID()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r5.put(r6, r7)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "type"
                        java.lang.String r7 = "debug"
                        r5.put(r6, r7)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "session_time"
                        java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r7.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r8 = ""
                        r7.append(r8)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r10 = 1000(0x3e8, double:4.94E-321)
                        long r8 = r8 / r10
                        r7.append(r8)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r5.put(r6, r7)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "time"
                        java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r7.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r8 = ""
                        r7.append(r8)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        long r8 = r8 / r10
                        r7.append(r8)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r5.put(r6, r7)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "session_id"
                        r5.put(r6, r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "data"
                        r5.put(r6, r4)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r6 = "attempt"
                        java.lang.String r7 = "0"
                        r5.put(r6, r7)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        android.content.Context r6 = r3     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        com.facebook.ads.internal.dynamicloading.DexLoadErrorReporter.addEnvFields(r6, r4, r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r2.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r2.put(r5)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r4.<init>()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r5 = "data"
                        r4.put(r5, r3)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r3 = "events"
                        r4.put(r3, r2)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.String r2 = r4.toString()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.io.OutputStream r4 = r1.getOutputStream()     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        r3.<init>(r4)     // Catch:{ Throwable -> 0x0168, all -> 0x0165 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        r4.<init>()     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        java.lang.String r5 = "payload="
                        r4.append(r5)     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        java.lang.String r5 = "UTF-8"
                        java.lang.String r2 = java.net.URLEncoder.encode(r2, r5)     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        r4.append(r2)     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        java.lang.String r2 = r4.toString()     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        r3.writeBytes(r2)     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        r3.flush()     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        r2 = 16384(0x4000, float:2.2959E-41)
                        byte[] r2 = new byte[r2]     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        r4.<init>()     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                        java.io.InputStream r5 = r1.getInputStream()     // Catch:{ Throwable -> 0x0161, all -> 0x015d }
                    L_0x012b:
                        int r0 = r5.read(r2)     // Catch:{ Throwable -> 0x015a, all -> 0x0157 }
                        r6 = -1
                        if (r0 == r6) goto L_0x0137
                        r6 = 0
                        r4.write(r2, r6, r0)     // Catch:{ Throwable -> 0x015a, all -> 0x0157 }
                        goto L_0x012b
                    L_0x0137:
                        r4.flush()     // Catch:{ Throwable -> 0x015a, all -> 0x0157 }
                        r3.close()     // Catch:{ Exception -> 0x013e }
                        goto L_0x0146
                    L_0x013e:
                        r0 = move-exception
                        java.lang.String r2 = "FBAudienceNetwork"
                        java.lang.String r3 = "Can't close connection."
                        android.util.Log.e(r2, r3, r0)
                    L_0x0146:
                        if (r5 == 0) goto L_0x0154
                        r5.close()     // Catch:{ Exception -> 0x014c }
                        goto L_0x0154
                    L_0x014c:
                        r0 = move-exception
                        java.lang.String r2 = "FBAudienceNetwork"
                        java.lang.String r3 = "Can't close connection."
                        android.util.Log.e(r2, r3, r0)
                    L_0x0154:
                        if (r1 == 0) goto L_0x019c
                        goto L_0x0199
                    L_0x0157:
                        r0 = move-exception
                        r2 = r0
                        goto L_0x015f
                    L_0x015a:
                        r0 = move-exception
                        r2 = r0
                        goto L_0x0163
                    L_0x015d:
                        r2 = move-exception
                        r5 = r0
                    L_0x015f:
                        r0 = r3
                        goto L_0x019e
                    L_0x0161:
                        r2 = move-exception
                        r5 = r0
                    L_0x0163:
                        r0 = r3
                        goto L_0x0174
                    L_0x0165:
                        r2 = move-exception
                        r5 = r0
                        goto L_0x019e
                    L_0x0168:
                        r2 = move-exception
                        r5 = r0
                        goto L_0x0174
                    L_0x016b:
                        r1 = move-exception
                        r5 = r0
                        r2 = r1
                        r1 = r5
                        goto L_0x019e
                    L_0x0170:
                        r1 = move-exception
                        r5 = r0
                        r2 = r1
                        r1 = r5
                    L_0x0174:
                        java.lang.String r3 = "FBAudienceNetwork"
                        java.lang.String r4 = "Can't send error."
                        android.util.Log.e(r3, r4, r2)     // Catch:{ all -> 0x019d }
                        if (r0 == 0) goto L_0x0189
                        r0.close()     // Catch:{ Exception -> 0x0181 }
                        goto L_0x0189
                    L_0x0181:
                        r0 = move-exception
                        java.lang.String r2 = "FBAudienceNetwork"
                        java.lang.String r3 = "Can't close connection."
                        android.util.Log.e(r2, r3, r0)
                    L_0x0189:
                        if (r5 == 0) goto L_0x0197
                        r5.close()     // Catch:{ Exception -> 0x018f }
                        goto L_0x0197
                    L_0x018f:
                        r0 = move-exception
                        java.lang.String r2 = "FBAudienceNetwork"
                        java.lang.String r3 = "Can't close connection."
                        android.util.Log.e(r2, r3, r0)
                    L_0x0197:
                        if (r1 == 0) goto L_0x019c
                    L_0x0199:
                        r1.disconnect()
                    L_0x019c:
                        return
                    L_0x019d:
                        r2 = move-exception
                    L_0x019e:
                        if (r0 == 0) goto L_0x01ac
                        r0.close()     // Catch:{ Exception -> 0x01a4 }
                        goto L_0x01ac
                    L_0x01a4:
                        r0 = move-exception
                        java.lang.String r3 = "FBAudienceNetwork"
                        java.lang.String r4 = "Can't close connection."
                        android.util.Log.e(r3, r4, r0)
                    L_0x01ac:
                        if (r5 == 0) goto L_0x01ba
                        r5.close()     // Catch:{ Exception -> 0x01b2 }
                        goto L_0x01ba
                    L_0x01b2:
                        r0 = move-exception
                        java.lang.String r3 = "FBAudienceNetwork"
                        java.lang.String r4 = "Can't close connection."
                        android.util.Log.e(r3, r4, r0)
                    L_0x01ba:
                        if (r1 == 0) goto L_0x01bf
                        r1.disconnect()
                    L_0x01bf:
                        throw r2
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.dynamicloading.DexLoadErrorReporter.AnonymousClass1.run():void");
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public static void addEnvFields(Context context, JSONObject jSONObject, String str) throws JSONException, PackageManager.NameNotFoundException {
        String packageName = context.getPackageName();
        jSONObject.put("APPBUILD", context.getPackageManager().getPackageInfo(packageName, 0).versionCode);
        jSONObject.put("APPNAME", context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(packageName, 0)));
        jSONObject.put("APPVERS", context.getPackageManager().getPackageInfo(packageName, 0).versionName);
        jSONObject.put("OSVERS", Build.VERSION.RELEASE);
        jSONObject.put("SDK", "android");
        jSONObject.put("SESSION_ID", str);
        jSONObject.put("MODEL", Build.MODEL);
        jSONObject.put("BUNDLE", packageName);
        jSONObject.put(LogDatabaseTable.LogTableColumns.SDK_VERSION, BuildConfigApi.getVersionName(context));
        jSONObject.put("OS", "Android");
    }
}
