package com.facebook.unity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.appevents.internal.ActivityLifecycleTracker;
import com.facebook.appevents.internal.AutomaticAnalyticsLogger;
import com.facebook.applinks.AppLinkData;
import com.facebook.internal.BundleJSONConverter;
import com.facebook.internal.InternalSettings;
import com.facebook.internal.Utility;
import com.facebook.login.LoginManager;
import com.facebook.share.widget.ShareDialog;
import com.facebook.unity.FBUnityLoginActivity;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Currency;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;

public class FB {
    static final String FB_UNITY_OBJECT = "UnityFacebookSDKPlugin";
    static ShareDialog.Mode ShareDialogMode = ShareDialog.Mode.AUTOMATIC;
    static final String TAG = "com.facebook.unity.FB";
    private static AtomicBoolean activateAppCalled = new AtomicBoolean();
    private static AppEventsLogger appEventsLogger;
    private static Intent clearedIntent;
    private static Intent intent;

    private static AppEventsLogger getAppEventsLogger() {
        if (appEventsLogger == null) {
            appEventsLogger = AppEventsLogger.newLogger(getUnityActivity().getApplicationContext());
        }
        return appEventsLogger;
    }

    public static Activity getUnityActivity() {
        return UnityReflection.GetUnityActivity();
    }

    @UnityCallable
    public static void Init(String str) {
        final String str2;
        String str3 = TAG;
        Log.v(str3, "Init(" + str + ")");
        StringBuilder sb = new StringBuilder();
        sb.append("couldn't parse init params: ");
        sb.append(str);
        UnityParams parse = UnityParams.parse(str, sb.toString());
        if (parse.hasString("appId").booleanValue()) {
            str2 = parse.getString("appId");
        } else {
            str2 = Utility.getMetadataApplicationId(getUnityActivity());
        }
        FacebookSdk.setApplicationId(str2);
        FacebookSdk.sdkInitialize(getUnityActivity(), new FacebookSdk.InitializeCallback() {
            public void onInitialized() {
                UnityMessage unityMessage = new UnityMessage("OnInitComplete");
                AccessToken currentAccessToken = AccessToken.getCurrentAccessToken();
                if (currentAccessToken != null) {
                    FBLogin.addLoginParametersToMessage(unityMessage, currentAccessToken, null);
                } else {
                    unityMessage.put("key_hash", FB.getKeyHash());
                }
                if (FacebookSdk.getAutoLogAppEventsEnabled()) {
                    FB.ActivateApp(str2);
                }
                unityMessage.send();
            }
        });
    }

    @UnityCallable
    public static void LoginWithReadPermissions(String str) {
        String str2 = TAG;
        Log.v(str2, "LoginWithReadPermissions(" + str + ")");
        Intent intent2 = new Intent(getUnityActivity(), FBUnityLoginActivity.class);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_PARAMS, str);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_TYPE, FBUnityLoginActivity.LoginType.READ);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void LoginWithPublishPermissions(String str) {
        String str2 = TAG;
        Log.v(str2, "LoginWithPublishPermissions(" + str + ")");
        Intent intent2 = new Intent(getUnityActivity(), FBUnityLoginActivity.class);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_PARAMS, str);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_TYPE, FBUnityLoginActivity.LoginType.PUBLISH);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void Logout(String str) {
        String str2 = TAG;
        Log.v(str2, "Logout(" + str + ")");
        LoginManager.getInstance().logOut();
        UnityMessage unityMessage = new UnityMessage("OnLogoutComplete");
        unityMessage.put("did_complete", true);
        unityMessage.send();
    }

    @UnityCallable
    public static void loginForTVWithReadPermissions(String str) {
        String str2 = TAG;
        Log.v(str2, "loginForTVWithReadPermissions(" + str + ")");
        Intent intent2 = new Intent(getUnityActivity(), FBUnityLoginActivity.class);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_PARAMS, str);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_TYPE, FBUnityLoginActivity.LoginType.TV_READ);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void LoginForTVWithPublishPermissions(String str) {
        String str2 = TAG;
        Log.v(str2, "LoginForTVWithPublishPermissions(" + str + ")");
        Intent intent2 = new Intent(getUnityActivity(), FBUnityLoginActivity.class);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_PARAMS, str);
        intent2.putExtra(FBUnityLoginActivity.LOGIN_TYPE, FBUnityLoginActivity.LoginType.TV_PUBLISH);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void AppRequest(String str) {
        String str2 = TAG;
        Log.v(str2, "AppRequest(" + str + ")");
        Intent intent2 = new Intent(getUnityActivity(), FBUnityGameRequestActivity.class);
        intent2.putExtra(FBUnityGameRequestActivity.GAME_REQUEST_PARAMS, UnityParams.parse(str).getStringParams());
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void GameGroupCreate(String str) {
        String str2 = TAG;
        Log.v(str2, "GameGroupCreate(" + str + ")");
        Bundle stringParams = UnityParams.parse(str).getStringParams();
        Intent intent2 = new Intent(getUnityActivity(), FBUnityCreateGameGroupActivity.class);
        intent2.putExtra(FBUnityCreateGameGroupActivity.CREATE_GAME_GROUP_PARAMS, stringParams);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void GameGroupJoin(String str) {
        String str2 = TAG;
        Log.v(str2, "GameGroupJoin(" + str + ")");
        Bundle stringParams = UnityParams.parse(str).getStringParams();
        Intent intent2 = new Intent(getUnityActivity(), FBUnityJoinGameGroupActivity.class);
        intent2.putExtra(FBUnityJoinGameGroupActivity.JOIN_GAME_GROUP_PARAMS, stringParams);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void ShareLink(String str) {
        String str2 = TAG;
        Log.v(str2, "ShareLink(" + str + ")");
        Bundle stringParams = UnityParams.parse(str).getStringParams();
        Intent intent2 = new Intent(getUnityActivity(), FBUnityDialogsActivity.class);
        intent2.putExtra(FBUnityDialogsActivity.DIALOG_TYPE, ShareDialogMode);
        intent2.putExtra(FBUnityDialogsActivity.SHARE_DIALOG_PARAMS, stringParams);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void FeedShare(String str) {
        String str2 = TAG;
        Log.v(str2, "FeedShare(" + str + ")");
        Bundle stringParams = UnityParams.parse(str).getStringParams();
        Intent intent2 = new Intent(getUnityActivity(), FBUnityDialogsActivity.class);
        intent2.putExtra(FBUnityDialogsActivity.DIALOG_TYPE, ShareDialog.Mode.FEED);
        intent2.putExtra(FBUnityDialogsActivity.FEED_DIALOG_PARAMS, stringParams);
        getUnityActivity().startActivity(intent2);
    }

    @UnityCallable
    public static void SetUserID(String str) {
        String str2 = TAG;
        Log.v(str2, "SetUserID(" + str + ")");
        AppEventsLogger.setUserID(str);
    }

    @UnityCallable
    public static String GetUserID() {
        return AppEventsLogger.getUserID();
    }

    @UnityCallable
    public static void UpdateUserProperties(String str) {
        String str2 = TAG;
        Log.v(str2, "UpdateUserProperties(" + str + ")");
        AppEventsLogger.updateUserProperties(UnityParams.parse(str).getStringParams(), null);
    }

    public static void SetIntent(Intent intent2) {
        intent = intent2;
    }

    public static void SetLimitEventUsage(String str) {
        String str2 = TAG;
        Log.v(str2, "SetLimitEventUsage(" + str + ")");
        FacebookSdk.setLimitEventAndDataUsage(getUnityActivity().getApplicationContext(), Boolean.valueOf(str).booleanValue());
    }

    @UnityCallable
    public static void LogAppEvent(String str) {
        String str2 = TAG;
        Log.v(str2, "LogAppEvent(" + str + ")");
        UnityParams parse = UnityParams.parse(str);
        Bundle bundle = new Bundle();
        if (parse.has("parameters")) {
            bundle = parse.getParamsObject("parameters").getStringParams();
        }
        if (parse.has("logPurchase")) {
            getAppEventsLogger().logPurchase(new BigDecimal(parse.getDouble("logPurchase")), Currency.getInstance(parse.getString("currency")), bundle);
        } else if (!parse.hasString("logEvent").booleanValue()) {
            String str3 = TAG;
            Log.e(str3, "couldn't logPurchase or logEvent params: " + str);
        } else if (parse.has("valueToSum")) {
            getAppEventsLogger().logEvent(parse.getString("logEvent"), parse.getDouble("valueToSum"), bundle);
        } else {
            getAppEventsLogger().logEvent(parse.getString("logEvent"), bundle);
        }
    }

    @UnityCallable
    public static boolean IsImplicitPurchaseLoggingEnabled() {
        return AutomaticAnalyticsLogger.isImplicitPurchaseLoggingEnabled();
    }

    @UnityCallable
    public static void SetShareDialogMode(String str) {
        String str2 = TAG;
        Log.v(str2, "SetShareDialogMode(" + str + ")");
        if (str.equalsIgnoreCase("NATIVE")) {
            ShareDialogMode = ShareDialog.Mode.NATIVE;
        } else if (str.equalsIgnoreCase("WEB")) {
            ShareDialogMode = ShareDialog.Mode.WEB;
        } else if (str.equalsIgnoreCase("FEED")) {
            ShareDialogMode = ShareDialog.Mode.FEED;
        } else {
            ShareDialogMode = ShareDialog.Mode.AUTOMATIC;
        }
    }

    @UnityCallable
    public static void SetAutoLogAppEventsEnabled(String str) {
        String str2 = TAG;
        Log.v(str2, "SetAutoLogAppEventsEnabled(" + str + ")");
        FacebookSdk.setAutoLogAppEventsEnabled(Boolean.valueOf(str).booleanValue());
    }

    @UnityCallable
    public static void SetAdvertiserIDCollectionEnabled(String str) {
        String str2 = TAG;
        Log.v(str2, "SetAdvertiserIDCollectionEnabled(" + str + ")");
        FacebookSdk.setAdvertiserIDCollectionEnabled(Boolean.valueOf(str).booleanValue());
    }

    @UnityCallable
    public static String GetSdkVersion() {
        return FacebookSdk.getSdkVersion();
    }

    @UnityCallable
    public static void SetUserAgentSuffix(String str) {
        String str2 = TAG;
        Log.v(str2, "SetUserAgentSuffix(" + str + ")");
        InternalSettings.setCustomUserAgent(str);
    }

    @UnityCallable
    public static void FetchDeferredAppLinkData(String str) {
        LogMethodCall("FetchDeferredAppLinkData", str);
        UnityParams parse = UnityParams.parse(str);
        final UnityMessage unityMessage = new UnityMessage("OnFetchDeferredAppLinkComplete");
        if (parse.hasString(Constants.CALLBACK_ID_KEY).booleanValue()) {
            unityMessage.put(Constants.CALLBACK_ID_KEY, parse.getString(Constants.CALLBACK_ID_KEY));
        }
        AppLinkData.fetchDeferredAppLinkData(getUnityActivity(), new AppLinkData.CompletionHandler() {
            public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                FB.addAppLinkToMessage(unityMessage, appLinkData);
                unityMessage.send();
            }
        });
    }

    @UnityCallable
    public static void GetAppLink(String str) {
        String str2 = TAG;
        Log.v(str2, "GetAppLink(" + str + ")");
        UnityMessage createWithCallbackFromParams = UnityMessage.createWithCallbackFromParams("OnGetAppLinkComplete", UnityParams.parse(str));
        if (intent == null) {
            createWithCallbackFromParams.put("did_complete", true);
            createWithCallbackFromParams.send();
        } else if (intent == clearedIntent) {
            createWithCallbackFromParams.put("did_complete", true);
            createWithCallbackFromParams.send();
        } else {
            AppLinkData createFromAlApplinkData = AppLinkData.createFromAlApplinkData(intent);
            if (createFromAlApplinkData != null) {
                addAppLinkToMessage(createWithCallbackFromParams, createFromAlApplinkData);
                createWithCallbackFromParams.put("url", intent.getDataString());
            } else if (intent.getData() != null) {
                createWithCallbackFromParams.put("url", intent.getDataString());
            } else {
                createWithCallbackFromParams.put("did_complete", true);
            }
            createWithCallbackFromParams.send();
        }
    }

    @UnityCallable
    public static void ClearAppLink() {
        Log.v(TAG, "ClearAppLink");
        clearedIntent = intent;
    }

    @UnityCallable
    public static void RefreshCurrentAccessToken(String str) {
        LogMethodCall("RefreshCurrentAccessToken", str);
        UnityParams parse = UnityParams.parse(str);
        final UnityMessage unityMessage = new UnityMessage("OnRefreshCurrentAccessTokenComplete");
        if (parse.hasString(Constants.CALLBACK_ID_KEY).booleanValue()) {
            unityMessage.put(Constants.CALLBACK_ID_KEY, parse.getString(Constants.CALLBACK_ID_KEY));
        }
        AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
            public void OnTokenRefreshed(AccessToken accessToken) {
                FBLogin.addLoginParametersToMessage(unityMessage, accessToken, null);
                unityMessage.send();
            }

            public void OnTokenRefreshFailed(FacebookException facebookException) {
                unityMessage.sendError(facebookException.getMessage());
            }
        });
    }

    @TargetApi(8)
    public static String getKeyHash() {
        try {
            Activity unityActivity = getUnityActivity();
            if (unityActivity == null) {
                return "";
            }
            Signature[] signatureArr = unityActivity.getPackageManager().getPackageInfo(unityActivity.getPackageName(), 64).signatures;
            if (signatureArr.length <= 0) {
                return "";
            }
            Signature signature = signatureArr[0];
            MessageDigest instance = MessageDigest.getInstance("SHA");
            instance.update(signature.toByteArray());
            String encodeToString = Base64.encodeToString(instance.digest(), 0);
            String str = TAG;
            Log.d(str, "KeyHash: " + encodeToString);
            return encodeToString;
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException unused) {
            return "";
        }
    }

    @UnityCallable
    public static void ActivateApp() {
        AppEventsLogger.activateApp(getUnityActivity());
    }

    /* access modifiers changed from: private */
    public static void ActivateApp(String str) {
        if (!activateAppCalled.compareAndSet(false, true)) {
            Log.w(TAG, "Activite app only needs to be called once");
            return;
        }
        final Activity unityActivity = getUnityActivity();
        if (str != null) {
            AppEventsLogger.activateApp(unityActivity.getApplication(), str);
        } else {
            AppEventsLogger.activateApp(unityActivity.getApplication());
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                ActivityLifecycleTracker.onActivityCreated(unityActivity);
                ActivityLifecycleTracker.onActivityResumed(unityActivity);
            }
        });
    }

    private static void startActivity(Class<?> cls, String str) {
        Intent intent2 = new Intent(getUnityActivity(), cls);
        intent2.putExtra(BaseActivity.ACTIVITY_PARAMS, UnityParams.parse(str).getStringParams());
        getUnityActivity().startActivity(intent2);
    }

    private static void LogMethodCall(String str, String str2) {
        Log.v(TAG, String.format(Locale.ROOT, "%s(%s)", str, str2));
    }

    /* access modifiers changed from: private */
    public static void addAppLinkToMessage(UnityMessage unityMessage, AppLinkData appLinkData) {
        if (appLinkData == null) {
            unityMessage.put("did_complete", true);
            return;
        }
        unityMessage.put("ref", appLinkData.getRef());
        unityMessage.put("target_url", appLinkData.getTargetUri().toString());
        try {
            if (appLinkData.getArgumentBundle() != null) {
                unityMessage.put("extras", BundleJSONConverter.convertToJSON(appLinkData.getArgumentBundle()).toString());
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
    }
}
