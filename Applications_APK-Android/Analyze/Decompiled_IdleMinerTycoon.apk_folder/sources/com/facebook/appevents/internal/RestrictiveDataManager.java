package com.facebook.appevents.internal;

import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.util.Log;
import com.facebook.appevents.AppEvent;
import com.facebook.internal.Utility;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public final class RestrictiveDataManager {
    private static final String TAG = RestrictiveDataManager.class.getCanonicalName();
    private static boolean enabled = false;
    private static Set<String> restrictiveEvents = new HashSet();
    private static List<RestrictiveParam> restrictiveParams = new ArrayList();

    public static void enable() {
        enabled = true;
    }

    public static synchronized void updateFromSetting(String str) {
        synchronized (RestrictiveDataManager.class) {
            if (enabled) {
                try {
                    if (!str.isEmpty()) {
                        JSONObject jSONObject = new JSONObject(str);
                        restrictiveParams.clear();
                        restrictiveEvents.clear();
                        Iterator<String> keys = jSONObject.keys();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            JSONObject jSONObject2 = jSONObject.getJSONObject(next);
                            if (jSONObject2 != null) {
                                if (jSONObject2.optBoolean("is_deprecated_event")) {
                                    restrictiveEvents.add(next);
                                } else {
                                    JSONObject optJSONObject = jSONObject.getJSONObject(next).optJSONObject("restrictive_param");
                                    if (optJSONObject != null) {
                                        restrictiveParams.add(new RestrictiveParam(next, Utility.convertJSONObjectToStringMap(optJSONObject)));
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.w(TAG, "updateRulesFromSetting failed", e);
                } catch (Exception e2) {
                    Log.w(TAG, "updateFromSetting failed", e2);
                }
            }
        }
    }

    public static void processEvents(List<AppEvent> list) {
        if (enabled) {
            Iterator<AppEvent> it = list.iterator();
            while (it.hasNext()) {
                if (isDeprecatedEvent(it.next().getName())) {
                    it.remove();
                }
            }
        }
    }

    public static void processParameters(Map<String, String> map, String str) {
        if (enabled) {
            HashMap hashMap = new HashMap();
            Iterator it = new ArrayList(map.keySet()).iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next();
                String matchedRuleType = getMatchedRuleType(str, str2);
                if (matchedRuleType != null) {
                    hashMap.put(str2, matchedRuleType);
                    map.remove(str2);
                }
            }
            if (hashMap.size() > 0) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    for (Map.Entry entry : hashMap.entrySet()) {
                        jSONObject.put((String) entry.getKey(), entry.getValue());
                    }
                    map.put("_restrictedParams", jSONObject.toString());
                } catch (JSONException e) {
                    Log.w(TAG, "processParameters failed", e);
                }
            }
        }
    }

    private static boolean isDeprecatedEvent(String str) {
        return restrictiveEvents.contains(str);
    }

    @Nullable
    private static String getMatchedRuleType(String str, String str2) {
        try {
            Iterator it = new ArrayList(restrictiveParams).iterator();
            while (it.hasNext()) {
                RestrictiveParam restrictiveParam = (RestrictiveParam) it.next();
                if (restrictiveParam != null) {
                    if (str.equals(restrictiveParam.eventName)) {
                        for (String next : restrictiveParam.params.keySet()) {
                            if (str2.equals(next)) {
                                return restrictiveParam.params.get(next);
                            }
                        }
                        continue;
                    } else {
                        continue;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            Log.w(TAG, "getMatchedRuleType failed", e);
            return null;
        }
    }

    static class RestrictiveParam {
        String eventName;
        Map<String, String> params;

        RestrictiveParam(String str, Map<String, String> map) {
            this.eventName = str;
            this.params = map;
        }
    }
}
