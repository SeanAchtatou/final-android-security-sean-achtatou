package com.adcolony.sdk;

import com.appsflyer.share.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class v {
    static final SimpleDateFormat l = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSSZ", Locale.US);
    static final String m = "message";
    static final String n = "timestamp";
    /* access modifiers changed from: private */
    public Date a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public r c;
    protected String o;

    v() {
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar) {
        this.c = rVar;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        switch (this.b) {
            case -1:
                return "Fatal";
            case 0:
                return "Error";
            case 1:
                return "Warn";
            case 2:
                return "Info";
            case 3:
                return "Debug";
            default:
                return "UNKNOWN LOG LEVEL";
        }
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return l.format(this.a);
    }

    /* access modifiers changed from: package-private */
    public r f() {
        return this.c;
    }

    public String toString() {
        return e() + " " + b() + Constants.URL_PATH_DELIMITER + f().d() + ": " + d();
    }

    static class a {
        protected v b = new v();

        a() {
        }

        /* access modifiers changed from: package-private */
        public a a(int i) {
            int unused = this.b.b = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(r rVar) {
            r unused = this.b.c = rVar;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.b.o = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(Date date) {
            Date unused = this.b.a = date;
            return this;
        }

        /* access modifiers changed from: package-private */
        public v a() {
            if (this.b.a == null) {
                Date unused = this.b.a = new Date(System.currentTimeMillis());
            }
            return this.b;
        }
    }
}
