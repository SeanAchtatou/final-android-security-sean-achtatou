package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.u;
import com.appsflyer.share.Constants;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.vungle.warren.model.Advertisement;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

class l implements Runnable {
    String a = "";
    String b = "";
    boolean c;
    int d;
    int e;
    private HttpURLConnection f;
    private InputStream g;
    private x h;
    private a i;
    private final int j = 4096;
    private final int k = 299;
    private String l;
    private int m = 0;
    private boolean n = false;
    private Map<String, List<String>> o;
    private String p = "";
    private String q = "";

    interface a {
        void a(l lVar, x xVar, Map<String, List<String>> map);
    }

    l(x xVar, a aVar) {
        this.h = xVar;
        this.i = aVar;
    }

    public void run() {
        boolean z = false;
        this.c = false;
        try {
            if (b()) {
                this.c = c();
                if (this.h.d().equals("WebServices.post") && this.e != 200) {
                    this.c = false;
                }
            }
        } catch (MalformedURLException e2) {
            new u.a().a("MalformedURLException: ").a(e2.toString()).a(u.h);
            this.c = true;
        } catch (OutOfMemoryError unused) {
            u.a a2 = new u.a().a("Out of memory error - disabling AdColony. (").a(this.d).a(Constants.URL_PATH_DELIMITER).a(this.m);
            a2.a("): " + this.a).a(u.g);
            a.a().a(true);
        } catch (IOException e3) {
            new u.a().a("Download of ").a(this.a).a(" failed: ").a(e3.toString()).a(u.f);
            this.e = this.e == 0 ? 504 : this.e;
        } catch (IllegalStateException e4) {
            new u.a().a("okhttp error: ").a(e4.toString()).a(u.g);
            e4.printStackTrace();
        } catch (Exception e5) {
            new u.a().a("Exception: ").a(e5.toString()).a(u.g);
            e5.printStackTrace();
        }
        z = true;
        if (this.c) {
            new u.a().a("Downloaded ").a(this.a).a(u.d);
        }
        if (z) {
            if (this.h.d().equals("WebServices.download")) {
                a(this.q, this.p);
            }
            this.i.a(this, this.h, this.o);
        }
    }

    private boolean b() throws IOException {
        JSONObject c2 = this.h.c();
        String b2 = s.b(c2, FirebaseAnalytics.Param.CONTENT_TYPE);
        String b3 = s.b(c2, "content");
        boolean d2 = s.d(c2, "no_redirect");
        this.a = s.b(c2, "url");
        this.p = s.b(c2, "filepath");
        this.q = a.a().o().f() + this.p.substring(this.p.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1);
        this.l = s.b(c2, "encoding");
        this.m = s.a(c2, "max_size", 0);
        this.n = this.m != 0;
        this.d = 0;
        this.g = null;
        this.f = null;
        this.o = null;
        if (!this.a.startsWith(Advertisement.FILE_SCHEME)) {
            this.f = (HttpURLConnection) new URL(this.a).openConnection();
            this.f.setInstanceFollowRedirects(!d2);
            this.f.setRequestProperty(HttpRequest.HEADER_ACCEPT_CHARSET, "UTF-8");
            String I = a.a().m().I();
            if (I != null && !I.equals("")) {
                this.f.setRequestProperty("User-Agent", I);
            }
            if (!b2.equals("")) {
                this.f.setRequestProperty(HttpRequest.HEADER_CONTENT_TYPE, b2);
            }
            if (this.h.d().equals("WebServices.post")) {
                this.f.setDoOutput(true);
                this.f.setFixedLengthStreamingMode(b3.getBytes("UTF-8").length);
                new PrintStream(this.f.getOutputStream()).print(b3);
            }
        } else if (this.a.startsWith("file:///android_asset/")) {
            Context c3 = a.c();
            if (c3 != null) {
                this.g = c3.getAssets().open(this.a.substring("file:///android_asset/".length()));
            }
        } else {
            this.g = new FileInputStream(this.a.substring(Advertisement.FILE_SCHEME.length()));
        }
        if (this.f == null && this.g == null) {
            return false;
        }
        return true;
    }

    private boolean c() throws Exception {
        OutputStream outputStream;
        String d2 = this.h.d();
        if (this.g != null) {
            outputStream = this.p.length() == 0 ? new ByteArrayOutputStream(4096) : new FileOutputStream(new File(this.p).getAbsolutePath());
        } else if (d2.equals("WebServices.download")) {
            this.g = this.f.getInputStream();
            outputStream = new FileOutputStream(this.q);
        } else if (d2.equals("WebServices.get")) {
            this.g = this.f.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else if (d2.equals("WebServices.post")) {
            this.f.connect();
            this.g = (this.f.getResponseCode() < 200 || this.f.getResponseCode() > 299) ? this.f.getErrorStream() : this.f.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else {
            outputStream = null;
        }
        if (this.f != null) {
            this.e = this.f.getResponseCode();
            this.o = this.f.getHeaderFields();
        }
        return a(this.g, outputStream);
    }

    private boolean a(InputStream inputStream, OutputStream outputStream) throws Exception {
        BufferedInputStream bufferedInputStream;
        Exception e2;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = bufferedInputStream.read(bArr, 0, 4096);
                    if (read != -1) {
                        this.d += read;
                        if (this.n) {
                            if (this.d > this.m) {
                                throw new Exception("Data exceeds expected maximum (" + this.d + Constants.URL_PATH_DELIMITER + this.m + "): " + this.f.getURL().toString());
                            }
                        }
                        outputStream.write(bArr, 0, read);
                    } else {
                        String str = "UTF-8";
                        if (this.l != null && !this.l.isEmpty()) {
                            str = this.l;
                        }
                        if (outputStream instanceof ByteArrayOutputStream) {
                            this.b = ((ByteArrayOutputStream) outputStream).toString(str);
                        }
                        if (outputStream != null) {
                            outputStream.close();
                        }
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        bufferedInputStream.close();
                        return true;
                    }
                }
            } catch (Exception e3) {
                e2 = e3;
                try {
                    throw e2;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (Exception e4) {
            Exception exc = e4;
            bufferedInputStream = null;
            e2 = exc;
            throw e2;
        } catch (Throwable th2) {
            Throwable th3 = th2;
            bufferedInputStream = null;
            th = th3;
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (bufferedInputStream != null) {
                bufferedInputStream.close();
            }
            throw th;
        }
    }

    private void a(String str, String str2) {
        try {
            String substring = str2.substring(0, str2.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1);
            if (str2 != null && !"".equals(str2) && !substring.equals(a.a().o().f()) && !new File(str).renameTo(new File(str2))) {
                u.a a2 = new u.a().a("Moving of ");
                if (str == null) {
                    str = "temp folder's asset file";
                }
                a2.a(str).a(" failed.").a(u.f);
            }
        } catch (Exception e2) {
            new u.a().a("Exception: ").a(e2.toString()).a(u.g);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public x a() {
        return this.h;
    }
}
