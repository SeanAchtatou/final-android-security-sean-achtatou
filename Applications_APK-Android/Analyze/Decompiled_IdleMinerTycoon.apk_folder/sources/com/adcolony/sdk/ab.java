package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.adcolony.sdk.u;
import com.ironsource.sdk.constants.Constants;

class ab {
    ab() {
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"MissingPermission"})
    public boolean a() {
        NetworkInfo networkInfo;
        Context c = a.c();
        if (c == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) c.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo != null && networkInfo.getType() == 1) {
                return true;
            }
            return false;
        } catch (SecurityException e) {
            new u.a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(u.g);
            return false;
        } catch (Exception e2) {
            new u.a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_wifi(): ").a(e2.toString()).a(u.h);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"MissingPermission"})
    public boolean b() {
        NetworkInfo networkInfo;
        Context c = a.c();
        if (c == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) c.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo == null) {
                return false;
            }
            int type = networkInfo.getType();
            if (type == 0 || type >= 2) {
                return true;
            }
            return false;
        } catch (SecurityException e) {
            new u.a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(u.g);
            return false;
        } catch (Exception e2) {
            new u.a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_mobile(): ").a(e2.toString()).a(u.h);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public String c() {
        if (a()) {
            return "wifi";
        }
        return b() ? "cell" : Constants.ParametersKeys.ORIENTATION_NONE;
    }
}
