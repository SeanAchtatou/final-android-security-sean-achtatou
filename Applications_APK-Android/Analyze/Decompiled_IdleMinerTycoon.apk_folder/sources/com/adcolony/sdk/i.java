package com.adcolony.sdk;

import com.adcolony.sdk.u;
import org.json.JSONObject;

class i implements z {
    i() {
        a.a("CustomMessage.controller_send", this);
    }

    public void a(x xVar) {
        JSONObject c = xVar.c();
        final String b = s.b(c, "type");
        final String b2 = s.b(c, "message");
        ak.a(new Runnable() {
            public void run() {
                new u.a().a("Received custom message ").a(b2).a(" of type ").a(b).a(u.d);
                try {
                    AdColonyCustomMessageListener adColonyCustomMessageListener = a.a().A().get(b);
                    if (adColonyCustomMessageListener != null) {
                        adColonyCustomMessageListener.onAdColonyCustomMessage(new AdColonyCustomMessage(b, b2));
                    }
                } catch (RuntimeException unused) {
                }
            }
        });
    }
}
