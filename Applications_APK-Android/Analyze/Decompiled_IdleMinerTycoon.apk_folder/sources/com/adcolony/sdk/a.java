package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.u;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {
    static boolean a;
    static boolean b;
    private static WeakReference<Context> c;
    /* access modifiers changed from: private */
    public static h d;

    a() {
    }

    static void a(final Context context, AdColonyAppOptions adColonyAppOptions, boolean z) {
        a(context);
        b = true;
        if (d == null) {
            d = new h();
            d.a(adColonyAppOptions, z);
        } else {
            d.a(adColonyAppOptions);
        }
        ak.b.execute(new Runnable() {
            public void run() {
                a.d.a(context, (x) null);
            }
        });
        new u.a().a("Configuring AdColony").a(u.c);
        d.b(false);
        d.k().d(true);
        d.k().e(true);
        d.k().f(false);
        d.f = true;
        d.k().a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.h.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
     arg types: [com.adcolony.sdk.AdColonyAppOptions, int]
     candidates:
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, com.iab.omid.library.adcolony.adsession.Partner):com.iab.omid.library.adcolony.adsession.Partner
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, com.adcolony.sdk.x):void
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, boolean):boolean
      com.adcolony.sdk.h.a(boolean, boolean):boolean
      com.adcolony.sdk.h.a(android.content.Context, com.adcolony.sdk.x):boolean
      com.adcolony.sdk.h.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void */
    static h a() {
        if (!b()) {
            Context c2 = c();
            if (c2 == null) {
                return new h();
            }
            d = new h();
            JSONObject c3 = s.c(c2.getFilesDir().getAbsolutePath() + "/adc3/AppInfo");
            JSONArray g = s.g(c3, "zoneIds");
            d.a(new AdColonyAppOptions().a(s.b(c3, "appId")).a(s.a(g)), false);
        }
        return d;
    }

    static boolean b() {
        return d != null;
    }

    static void a(Context context) {
        if (context == null) {
            c.clear();
        } else {
            c = new WeakReference<>(context);
        }
    }

    static Context c() {
        if (c == null) {
            return null;
        }
        return c.get();
    }

    static boolean d() {
        return (c == null || c.get() == null) ? false : true;
    }

    static boolean e() {
        return a;
    }

    static void a(String str, z zVar) {
        a().q().a(str, zVar);
    }

    static z a(String str, z zVar, boolean z) {
        a().q().a(str, zVar);
        return zVar;
    }

    static void b(String str, z zVar) {
        a().q().b(str, zVar);
    }

    static void f() {
        a().q().b();
    }

    static void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = s.a();
        }
        s.a(jSONObject, "m_type", str);
        a().q().a(jSONObject);
    }

    static void a(String str) {
        try {
            x xVar = new x("CustomMessage.send", 0);
            xVar.c().put("message", str);
            xVar.b();
        } catch (JSONException e) {
            new u.a().a("JSON error from ADC.java's send_custom_message(): ").a(e.toString()).a(u.h);
        }
    }
}
