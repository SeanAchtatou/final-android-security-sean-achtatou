package com.adcolony.sdk;

import android.annotation.SuppressLint;
import com.adcolony.sdk.u;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"UseSparseArrays"})
class y {
    private ArrayList<aa> a = new ArrayList<>();
    private HashMap<Integer, aa> b = new HashMap<>();
    private int c = 2;
    private HashMap<String, ArrayList<z>> d = new HashMap<>();
    private JSONArray e = s.b();
    private int f = 1;

    static /* synthetic */ int a(y yVar) {
        int i = yVar.f;
        yVar.f = i + 1;
        return i;
    }

    y() {
    }

    /* access modifiers changed from: package-private */
    public void a(String str, z zVar) {
        ArrayList arrayList = this.d.get(str);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.d.put(str, arrayList);
        }
        arrayList.add(zVar);
    }

    /* access modifiers changed from: package-private */
    public void b(String str, z zVar) {
        synchronized (this.d) {
            ArrayList arrayList = this.d.get(str);
            if (arrayList != null) {
                arrayList.remove(zVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        r0 = com.adcolony.sdk.a.c();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r2 = this;
            com.adcolony.sdk.h r0 = com.adcolony.sdk.a.a()
            boolean r1 = r0.g()
            if (r1 != 0) goto L_0x0020
            boolean r0 = r0.h()
            if (r0 != 0) goto L_0x0020
            android.content.Context r0 = com.adcolony.sdk.a.c()
            if (r0 != 0) goto L_0x0017
            goto L_0x0020
        L_0x0017:
            com.adcolony.sdk.y$1 r1 = new com.adcolony.sdk.y$1
            r1.<init>(r0)
            com.adcolony.sdk.ak.a(r1)
            return
        L_0x0020:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.y.a():void");
    }

    /* access modifiers changed from: package-private */
    public aa a(aa aaVar) {
        synchronized (this.a) {
            int b2 = aaVar.b();
            if (b2 <= 0) {
                b2 = aaVar.a();
            }
            this.a.add(aaVar);
            this.b.put(Integer.valueOf(b2), aaVar);
        }
        return aaVar;
    }

    /* access modifiers changed from: package-private */
    public aa a(int i) {
        synchronized (this.a) {
            aa aaVar = this.b.get(Integer.valueOf(i));
            if (aaVar == null) {
                return null;
            }
            this.a.remove(aaVar);
            this.b.remove(Integer.valueOf(i));
            aaVar.c();
            return aaVar;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        synchronized (this.a) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                this.a.get(size).d();
            }
        }
        JSONArray jSONArray = null;
        if (this.e.length() > 0) {
            jSONArray = this.e;
            this.e = s.b();
        }
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    final JSONObject jSONObject = jSONArray.getJSONObject(i);
                    final String string = jSONObject.getString("m_type");
                    if (jSONObject.getInt("m_origin") >= 2) {
                        ak.a(new Runnable() {
                            public void run() {
                                y.this.a(string, jSONObject);
                            }
                        });
                    } else {
                        a(string, jSONObject);
                    }
                } catch (JSONException e2) {
                    new u.a().a("JSON error from message dispatcher's updateModules(): ").a(e2.toString()).a(u.h);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject) {
        synchronized (this.d) {
            ArrayList arrayList = this.d.get(str);
            if (arrayList != null) {
                x xVar = new x(jSONObject);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    try {
                        ((z) it.next()).a(xVar);
                    } catch (RuntimeException e2) {
                        new u.a().a(e2).a(u.h);
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        try {
            if (!jSONObject.has("m_id")) {
                int i = this.f;
                this.f = i + 1;
                jSONObject.put("m_id", i);
            }
            if (!jSONObject.has("m_origin")) {
                jSONObject.put("m_origin", 0);
            }
            int i2 = jSONObject.getInt("m_target");
            if (i2 == 0) {
                synchronized (this) {
                    this.e.put(jSONObject);
                }
                return;
            }
            aa aaVar = this.b.get(Integer.valueOf(i2));
            if (aaVar != null) {
                aaVar.a(jSONObject);
            }
        } catch (JSONException e2) {
            new u.a().a("JSON error in ADCMessageDispatcher's sendMessage(): ").a(e2.toString()).a(u.h);
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<aa> c() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        int i = this.c;
        this.c = i + 1;
        return i;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, aa> e() {
        return this.b;
    }
}
