package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.adcolony.sdk.l;
import com.adcolony.sdk.u;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.iab.omid.library.adcolony.adsession.Partner;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

class h implements l.a {
    private static volatile String H = "";
    static final String a = "026ae9c9824b3e483fa6c71fa88f57ae27816141";
    static final String b = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5";
    static String e = "https://adc3-launch.adcolony.com/v4/launch";
    private HashMap<String, AdColonyZone> A = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, am> B = new HashMap<>();
    private String C;
    private String D;
    private String E;
    private String F;
    private String G = "";
    private boolean I;
    /* access modifiers changed from: private */
    public boolean J;
    private boolean K;
    /* access modifiers changed from: private */
    public boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    /* access modifiers changed from: private */
    public boolean Q;
    /* access modifiers changed from: private */
    public boolean R;
    /* access modifiers changed from: private */
    public boolean S;
    private int T;
    /* access modifiers changed from: private */
    public int U = 1;
    private final int V = 120;
    private Application.ActivityLifecycleCallbacks W;
    /* access modifiers changed from: private */
    public Partner X = null;
    j c;
    ab d;
    boolean f;
    private i g;
    /* access modifiers changed from: private */
    public y h;
    /* access modifiers changed from: private */
    public m i;
    /* access modifiers changed from: private */
    public af j;
    private d k;
    /* access modifiers changed from: private */
    public k l;
    private p m;
    private ai n;
    /* access modifiers changed from: private */
    public ag o;
    private w p;
    private c q;
    private AdColonyAdView r;
    private AdColonyInterstitial s;
    /* access modifiers changed from: private */
    public AdColonyRewardListener t;
    private HashMap<String, AdColonyCustomMessageListener> u = new HashMap<>();
    /* access modifiers changed from: private */
    public AdColonyAppOptions v;
    /* access modifiers changed from: private */
    public x w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public x y;
    private JSONObject z;

    h() {
    }

    /* access modifiers changed from: package-private */
    public Context a() {
        return a.c();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0115  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.adcolony.sdk.AdColonyAppOptions r4, boolean r5) {
        /*
            r3 = this;
            r3.K = r5
            r3.v = r4
            com.adcolony.sdk.y r0 = new com.adcolony.sdk.y
            r0.<init>()
            r3.h = r0
            com.adcolony.sdk.i r0 = new com.adcolony.sdk.i
            r0.<init>()
            r3.g = r0
            com.adcolony.sdk.m r0 = new com.adcolony.sdk.m
            r0.<init>()
            r3.i = r0
            com.adcolony.sdk.m r0 = r3.i
            r0.a()
            com.adcolony.sdk.af r0 = new com.adcolony.sdk.af
            r0.<init>()
            r3.j = r0
            com.adcolony.sdk.af r0 = r3.j
            r0.a()
            com.adcolony.sdk.d r0 = new com.adcolony.sdk.d
            r0.<init>()
            r3.k = r0
            com.adcolony.sdk.d r0 = r3.k
            r0.a()
            com.adcolony.sdk.k r0 = new com.adcolony.sdk.k
            r0.<init>()
            r3.l = r0
            com.adcolony.sdk.p r0 = new com.adcolony.sdk.p
            r0.<init>()
            r3.m = r0
            com.adcolony.sdk.p r0 = r3.m
            r0.a()
            com.adcolony.sdk.w r0 = new com.adcolony.sdk.w
            r0.<init>()
            r3.p = r0
            com.adcolony.sdk.w r0 = r3.p
            com.adcolony.sdk.w.c()
            com.adcolony.sdk.ag r0 = new com.adcolony.sdk.ag
            r0.<init>()
            r3.o = r0
            com.adcolony.sdk.ag r0 = r3.o
            r0.a()
            com.adcolony.sdk.ai r0 = new com.adcolony.sdk.ai
            r0.<init>()
            r3.n = r0
            com.adcolony.sdk.ai r0 = r3.n
            r0.a()
            com.adcolony.sdk.j r0 = new com.adcolony.sdk.j
            r0.<init>()
            r3.c = r0
            com.adcolony.sdk.j r0 = r3.c
            r0.e()
            com.adcolony.sdk.ab r0 = new com.adcolony.sdk.ab
            r0.<init>()
            r3.d = r0
            com.adcolony.sdk.ab r0 = r3.d
            java.lang.String r0 = r0.c()
            r3.C = r0
            android.content.Context r0 = com.adcolony.sdk.a.c()
            com.adcolony.sdk.AdColony.a(r0, r4)
            r4 = 0
            r0 = 1
            if (r5 != 0) goto L_0x013f
            java.io.File r5 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.adcolony.sdk.ag r2 = r3.o
            java.lang.String r2 = r2.g()
            r1.append(r2)
            java.lang.String r2 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r5.<init>(r1)
            boolean r5 = r5.exists()
            r3.N = r5
            java.io.File r5 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.adcolony.sdk.ag r2 = r3.o
            java.lang.String r2 = r2.g()
            r1.append(r2)
            java.lang.String r2 = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r5.<init>(r1)
            boolean r5 = r5.exists()
            r3.O = r5
            boolean r5 = r3.N
            if (r5 == 0) goto L_0x010e
            boolean r5 = r3.O
            if (r5 == 0) goto L_0x010e
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.adcolony.sdk.ag r1 = r3.o
            java.lang.String r1 = r1.g()
            r5.append(r1)
            java.lang.String r1 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            org.json.JSONObject r5 = com.adcolony.sdk.s.c(r5)
            java.lang.String r1 = "sdkVersion"
            java.lang.String r5 = com.adcolony.sdk.s.b(r5, r1)
            com.adcolony.sdk.j r1 = r3.c
            java.lang.String r1 = r1.H()
            boolean r5 = r5.equals(r1)
            if (r5 == 0) goto L_0x010e
            r5 = 1
            goto L_0x010f
        L_0x010e:
            r5 = 0
        L_0x010f:
            r3.M = r5
            boolean r5 = r3.N
            if (r5 == 0) goto L_0x0137
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.adcolony.sdk.ag r1 = r3.o
            java.lang.String r1 = r1.g()
            r5.append(r1)
            java.lang.String r1 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            org.json.JSONObject r5 = com.adcolony.sdk.s.c(r5)
            r3.z = r5
            org.json.JSONObject r5 = r3.z
            r3.b(r5)
        L_0x0137:
            boolean r5 = r3.M
            r3.e(r5)
            r3.I()
        L_0x013f:
            java.lang.String r5 = "Module.load"
            com.adcolony.sdk.h$1 r1 = new com.adcolony.sdk.h$1
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "Module.unload"
            com.adcolony.sdk.h$12 r1 = new com.adcolony.sdk.h$12
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "AdColony.on_configured"
            com.adcolony.sdk.h$14 r1 = new com.adcolony.sdk.h$14
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "AdColony.get_app_info"
            com.adcolony.sdk.h$15 r1 = new com.adcolony.sdk.h$15
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "AdColony.v4vc_reward"
            com.adcolony.sdk.h$16 r1 = new com.adcolony.sdk.h$16
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "AdColony.zone_info"
            com.adcolony.sdk.h$17 r1 = new com.adcolony.sdk.h$17
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "AdColony.probe_launch_server"
            com.adcolony.sdk.h$18 r1 = new com.adcolony.sdk.h$18
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "Crypto.sha1"
            com.adcolony.sdk.h$19 r1 = new com.adcolony.sdk.h$19
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "Crypto.crc32"
            com.adcolony.sdk.h$20 r1 = new com.adcolony.sdk.h$20
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "Crypto.uuid"
            com.adcolony.sdk.h$2 r1 = new com.adcolony.sdk.h$2
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "Device.query_advertiser_info"
            com.adcolony.sdk.h$3 r1 = new com.adcolony.sdk.h$3
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            java.lang.String r5 = "AdColony.controller_version"
            com.adcolony.sdk.h$4 r1 = new com.adcolony.sdk.h$4
            r1.<init>()
            com.adcolony.sdk.a.a(r5, r1)
            com.adcolony.sdk.ag r5 = r3.o
            int r5 = com.adcolony.sdk.ak.a(r5)
            if (r5 != r0) goto L_0x01c1
            r1 = 1
            goto L_0x01c2
        L_0x01c1:
            r1 = 0
        L_0x01c2:
            r3.Q = r1
            r1 = 2
            if (r5 != r1) goto L_0x01c8
            r4 = 1
        L_0x01c8:
            r3.R = r4
            com.adcolony.sdk.h$5 r4 = new com.adcolony.sdk.h$5
            r4.<init>()
            com.adcolony.sdk.ak.a(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.h.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void");
    }

    /* access modifiers changed from: private */
    public void E() {
        JSONObject a2 = s.a();
        s.a(a2, "type", "AdColony.on_configuration_completed");
        JSONArray jSONArray = new JSONArray();
        for (String put : f().keySet()) {
            jSONArray.put(put);
        }
        JSONObject a3 = s.a();
        s.a(a3, "zone_ids", jSONArray);
        s.a(a2, "message", a3);
        new x("CustomMessage.controller_send", 0, a2).b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.h.a(boolean, boolean):boolean
     arg types: [boolean, int]
     candidates:
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, com.iab.omid.library.adcolony.adsession.Partner):com.iab.omid.library.adcolony.adsession.Partner
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, com.adcolony.sdk.x):void
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, boolean):boolean
      com.adcolony.sdk.h.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.h.a(android.content.Context, com.adcolony.sdk.x):boolean
      com.adcolony.sdk.h.a(boolean, boolean):boolean */
    private boolean e(boolean z2) {
        return a(z2, false);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.G;
    }

    /* access modifiers changed from: package-private */
    public JSONObject c() {
        return this.z;
    }

    /* access modifiers changed from: private */
    public boolean a(boolean z2, boolean z3) {
        if (!a.d()) {
            return false;
        }
        this.P = z3;
        this.M = z2;
        if (z2 && !z3 && !H()) {
            return false;
        }
        F();
        return true;
    }

    /* access modifiers changed from: private */
    public void F() {
        new Thread(new Runnable() {
            public void run() {
                JSONObject a2 = s.a();
                s.a(a2, "url", h.e);
                s.a(a2, FirebaseAnalytics.Param.CONTENT_TYPE, "application/json");
                s.a(a2, "content", h.this.m().L().toString());
                new u.a().a("Launch: ").a(h.this.m().L().toString()).a(u.b);
                new u.a().a("Saving Launch to ").a(h.this.o.g()).a(h.a).a(u.d);
                h.this.i.a(new l(new x("WebServices.post", 0, a2), h.this));
            }
        }).start();
    }

    private boolean a(JSONObject jSONObject) {
        if (!this.M) {
            new u.a().a("Non-standard launch. Downloading new controller.").a(u.f);
            return true;
        } else if (this.z != null && s.b(s.f(this.z, "controller"), "sha1").equals(s.b(s.f(jSONObject, "controller"), "sha1"))) {
            return false;
        } else {
            new u.a().a("Controller sha1 does not match, downloading new controller.").a(u.f);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void f(x xVar) {
        a(s.c(xVar.c(), "id"));
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.K = z2;
    }

    /* access modifiers changed from: private */
    public void g(x xVar) {
        JSONObject jSONObject = this.v.d;
        s.a(jSONObject, "app_id", this.v.a);
        s.a(jSONObject, "zone_ids", this.v.c);
        JSONObject a2 = s.a();
        s.a(a2, "options", jSONObject);
        xVar.a(a2).b();
    }

    /* access modifiers changed from: package-private */
    public boolean a(final x xVar) {
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        try {
            int c3 = xVar.c().has("id") ? s.c(xVar.c(), "id") : 0;
            if (c3 <= 0) {
                c3 = this.h.d();
            }
            a(c3);
            final boolean d2 = s.d(xVar.c(), "is_display_module");
            ak.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.am.a(boolean, com.adcolony.sdk.x):void
                 arg types: [int, com.adcolony.sdk.x]
                 candidates:
                  com.adcolony.sdk.am.a(java.lang.String, java.lang.String):java.lang.String
                  com.adcolony.sdk.am.a(com.adcolony.sdk.am, org.json.JSONArray):org.json.JSONArray
                  com.adcolony.sdk.am.a(com.adcolony.sdk.am, java.lang.String):void
                  com.adcolony.sdk.am.a(org.json.JSONObject, java.lang.String):void
                  com.adcolony.sdk.am.a(com.adcolony.sdk.am, boolean):boolean
                  com.adcolony.sdk.am.a(boolean, com.adcolony.sdk.x):void */
                public void run() {
                    am amVar = new am(c2.getApplicationContext(), h.this.h.d(), d2);
                    amVar.a(true, xVar);
                    h.this.B.put(Integer.valueOf(amVar.a()), amVar);
                }
            });
            return true;
        } catch (RuntimeException e2) {
            u.a aVar = new u.a();
            aVar.a(e2.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(u.g);
            AdColony.disable();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(x xVar) {
        this.w = xVar;
    }

    /* access modifiers changed from: package-private */
    public void c(x xVar) {
        this.y = xVar;
    }

    private void G() {
        if (a.a().k().e()) {
            this.T++;
            int i2 = 120;
            if (this.U * this.T <= 120) {
                i2 = this.T * this.U;
            }
            this.U = i2;
            ak.a(new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (a.a().k().e()) {
                                h.this.F();
                            }
                        }
                    }, (long) (h.this.U * 1000));
                }
            });
            return;
        }
        new u.a().a("Max launch server download attempts hit, or AdColony is no longer").a(" active.").a(u.f);
    }

    public void a(l lVar, x xVar, Map<String, List<String>> map) {
        if (lVar.a.equals(e)) {
            if (lVar.c) {
                new u.a().a("Launch: ").a(lVar.b).a(u.b);
                JSONObject a2 = s.a(lVar.b, "Parsing launch response");
                s.a(a2, GeneralPropertiesWorker.SDK_VERSION, m().H());
                s.h(a2, this.o.g() + a);
                if (c(a2)) {
                    if (a(a2)) {
                        new u.a().a("Controller missing or out of date. Downloading controller").a(u.d);
                        JSONObject a3 = s.a();
                        s.a(a3, "url", this.D);
                        s.a(a3, "filepath", this.o.g() + b);
                        this.i.a(new l(new x("WebServices.download", 0, a3), this));
                    }
                    this.z = a2;
                } else if (!this.M) {
                    new u.a().a("Incomplete or disabled launch server response. ").a("Disabling AdColony until next launch.").a(u.g);
                    a(true);
                }
            } else {
                G();
            }
        } else if (!lVar.a.equals(this.D)) {
        } else {
            if (!b(this.E) && !am.a) {
                new u.a().a("Downloaded controller sha1 does not match, retrying.").a(u.e);
                G();
            } else if (!this.M && !this.P) {
                ak.a(new Runnable() {
                    public void run() {
                        boolean l = h.this.H();
                        u.a aVar = new u.a();
                        aVar.a("Loaded library. Success=" + l).a(u.b);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean H() {
        this.h.a();
        return true;
    }

    private boolean b(String str) {
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        File file = new File(c2.getFilesDir().getAbsolutePath() + "/adc3/" + b);
        if (file.exists()) {
            return ak.a(str, file);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context, x xVar) {
        boolean z2;
        if (context == null) {
            return false;
        }
        String str = "";
        AdvertisingIdClient.Info info = null;
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (NoClassDefFoundError unused) {
            new u.a().a("Google Play Services ads dependencies are missing. Collecting ").a("Android ID instead of Advertising ID.").a(u.e);
            return false;
        } catch (NoSuchMethodError unused2) {
            new u.a().a("Google Play Services is out of date, please update to GPS 4.0+. ").a("Collecting Android ID instead of Advertising ID.").a(u.e);
        } catch (Exception e2) {
            e2.printStackTrace();
            if (!Build.MANUFACTURER.equals("Amazon")) {
                new u.a().a("Advertising ID is not available. Collecting Android ID instead of").a(" Advertising ID.").a(u.e);
                return false;
            }
            str = m().g();
            z2 = m().h();
        }
        z2 = false;
        if (!Build.MANUFACTURER.equals("Amazon") && info == null) {
            return false;
        }
        if (!Build.MANUFACTURER.equals("Amazon")) {
            str = info.getId();
            z2 = info.isLimitAdTrackingEnabled();
        }
        m().a(str);
        w.l.g.put("advertisingId", m().c());
        m().b(z2);
        m().a(true);
        if (xVar != null) {
            JSONObject a2 = s.a();
            s.a(a2, "advertiser_id", m().c());
            s.b(a2, "limit_ad_tracking", m().i());
            xVar.a(a2).b();
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.h.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, com.iab.omid.library.adcolony.adsession.Partner):com.iab.omid.library.adcolony.adsession.Partner
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, com.adcolony.sdk.x):void
      com.adcolony.sdk.h.a(com.adcolony.sdk.h, boolean):boolean
      com.adcolony.sdk.h.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.h.a(android.content.Context, com.adcolony.sdk.x):boolean
      com.adcolony.sdk.h.a(boolean, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(AdColonyAppOptions adColonyAppOptions) {
        synchronized (this.k.c()) {
            for (Map.Entry<String, AdColonyInterstitial> value : this.k.c().entrySet()) {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) value.getValue();
                AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
                adColonyInterstitial.a(true);
                if (listener != null) {
                    listener.onExpiring(adColonyInterstitial);
                }
            }
            this.k.c().clear();
        }
        this.L = false;
        a(1);
        this.A.clear();
        this.v = adColonyAppOptions;
        this.h.a();
        a(true, true);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        aa a2 = this.h.a(i2);
        final am remove = this.B.remove(Integer.valueOf(i2));
        boolean z2 = false;
        if (a2 == null) {
            return false;
        }
        if (remove != null && remove.l()) {
            z2 = true;
        }
        AnonymousClass10 r2 = new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.adcolony.sdk.h.d(com.adcolony.sdk.h, boolean):boolean
             arg types: [com.adcolony.sdk.h, int]
             candidates:
              com.adcolony.sdk.h.d(com.adcolony.sdk.h, com.adcolony.sdk.x):com.adcolony.sdk.x
              com.adcolony.sdk.h.d(com.adcolony.sdk.h, boolean):boolean */
            public void run() {
                if (remove != null && remove.m()) {
                    remove.loadUrl("about:blank");
                    remove.clearCache(true);
                    remove.removeAllViews();
                    remove.a(true);
                    remove.destroy();
                }
                if (h.this.y != null) {
                    h.this.y.b();
                    x unused = h.this.y = (x) null;
                    boolean unused2 = h.this.x = false;
                }
            }
        };
        if (z2) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            new Handler().postDelayed(r2, 1000);
        } else {
            r2.run();
        }
        return true;
    }

    private void b(JSONObject jSONObject) {
        if (!am.a) {
            JSONObject f2 = s.f(jSONObject, "logging");
            w.k = s.a(f2, "send_level", 1);
            w.a = s.d(f2, "log_private");
            w.i = s.a(f2, "print_level", 3);
            this.p.a(s.g(f2, "modules"));
        }
        m().a(s.f(jSONObject, "metadata"));
        this.G = s.b(s.f(jSONObject, "controller"), "version");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        new java.io.File(r3.o.g() + com.adcolony.sdk.h.a).delete();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0047 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(org.json.JSONObject r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0014
            com.adcolony.sdk.u$a r4 = new com.adcolony.sdk.u$a
            r4.<init>()
            java.lang.String r1 = "Launch response verification failed - response is null or unknown"
            com.adcolony.sdk.u$a r4 = r4.a(r1)
            com.adcolony.sdk.u r1 = com.adcolony.sdk.u.d
            r4.a(r1)
            return r0
        L_0x0014:
            java.lang.String r1 = "controller"
            org.json.JSONObject r1 = com.adcolony.sdk.s.f(r4, r1)     // Catch:{ Exception -> 0x0047 }
            java.lang.String r2 = "url"
            java.lang.String r2 = com.adcolony.sdk.s.b(r1, r2)     // Catch:{ Exception -> 0x0047 }
            r3.D = r2     // Catch:{ Exception -> 0x0047 }
            java.lang.String r2 = "sha1"
            java.lang.String r1 = com.adcolony.sdk.s.b(r1, r2)     // Catch:{ Exception -> 0x0047 }
            r3.E = r1     // Catch:{ Exception -> 0x0047 }
            java.lang.String r1 = "status"
            java.lang.String r1 = com.adcolony.sdk.s.b(r4, r1)     // Catch:{ Exception -> 0x0047 }
            r3.F = r1     // Catch:{ Exception -> 0x0047 }
            java.lang.String r1 = "pie"
            java.lang.String r1 = com.adcolony.sdk.s.b(r4, r1)     // Catch:{ Exception -> 0x0047 }
            com.adcolony.sdk.h.H = r1     // Catch:{ Exception -> 0x0047 }
            boolean r1 = com.adcolony.sdk.AdColonyEventTracker.b()     // Catch:{ Exception -> 0x0047 }
            if (r1 == 0) goto L_0x0043
            com.adcolony.sdk.AdColonyEventTracker.a()     // Catch:{ Exception -> 0x0047 }
        L_0x0043:
            r3.b(r4)     // Catch:{ Exception -> 0x0047 }
            goto L_0x0066
        L_0x0047:
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0066 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0066 }
            r1.<init>()     // Catch:{ Exception -> 0x0066 }
            com.adcolony.sdk.ag r2 = r3.o     // Catch:{ Exception -> 0x0066 }
            java.lang.String r2 = r2.g()     // Catch:{ Exception -> 0x0066 }
            r1.append(r2)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r2 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r1.append(r2)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0066 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0066 }
            r4.delete()     // Catch:{ Exception -> 0x0066 }
        L_0x0066:
            java.lang.String r4 = r3.F
            java.lang.String r1 = "disable"
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00ad
            boolean r4 = com.adcolony.sdk.am.a
            if (r4 != 0) goto L_0x00ad
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0093 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093 }
            r1.<init>()     // Catch:{ Exception -> 0x0093 }
            com.adcolony.sdk.ag r2 = r3.o     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = r2.g()     // Catch:{ Exception -> 0x0093 }
            r1.append(r2)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5"
            r1.append(r2)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0093 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0093 }
            r4.delete()     // Catch:{ Exception -> 0x0093 }
        L_0x0093:
            com.adcolony.sdk.u$a r4 = new com.adcolony.sdk.u$a
            r4.<init>()
            java.lang.String r1 = "Launch server response with disabled status. Disabling AdColony "
            com.adcolony.sdk.u$a r4 = r4.a(r1)
            java.lang.String r1 = "until next launch."
            com.adcolony.sdk.u$a r4 = r4.a(r1)
            com.adcolony.sdk.u r1 = com.adcolony.sdk.u.f
            r4.a(r1)
            com.adcolony.sdk.AdColony.disable()
            return r0
        L_0x00ad:
            java.lang.String r4 = r3.D
            java.lang.String r1 = ""
            boolean r4 = r4.equals(r1)
            if (r4 != 0) goto L_0x00c1
            java.lang.String r4 = r3.F
            java.lang.String r1 = ""
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00dc
        L_0x00c1:
            boolean r4 = com.adcolony.sdk.am.a
            if (r4 != 0) goto L_0x00dc
            com.adcolony.sdk.u$a r4 = new com.adcolony.sdk.u$a
            r4.<init>()
            java.lang.String r1 = "Missing controller status or URL. Disabling AdColony until next "
            com.adcolony.sdk.u$a r4 = r4.a(r1)
            java.lang.String r1 = "launch."
            com.adcolony.sdk.u$a r4 = r4.a(r1)
            com.adcolony.sdk.u r1 = com.adcolony.sdk.u.g
            r4.a(r1)
            return r0
        L_0x00dc:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.h.c(org.json.JSONObject):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean d(final x xVar) {
        if (this.t == null) {
            return false;
        }
        ak.a(new Runnable() {
            public void run() {
                h.this.t.onReward(new AdColonyReward(xVar));
            }
        });
        return true;
    }

    /* access modifiers changed from: package-private */
    public void e(x xVar) {
        AdColonyZone adColonyZone;
        if (this.K) {
            new u.a().a("AdColony is disabled. Ignoring zone_info message.").a(u.f);
            return;
        }
        String b2 = s.b(xVar.c(), "zone_id");
        if (this.A.containsKey(b2)) {
            adColonyZone = this.A.get(b2);
        } else {
            AdColonyZone adColonyZone2 = new AdColonyZone(b2);
            this.A.put(b2, adColonyZone2);
            adColonyZone = adColonyZone2;
        }
        adColonyZone.a(xVar);
    }

    private void I() {
        Application application;
        Context c2 = a.c();
        if (c2 != null && this.W == null && Build.VERSION.SDK_INT > 14) {
            this.W = new Application.ActivityLifecycleCallbacks() {
                public void onActivityDestroyed(Activity activity) {
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                }

                public void onActivityStarted(Activity activity) {
                }

                public void onActivityStopped(Activity activity) {
                }

                public void onActivityResumed(Activity activity) {
                    a.b = true;
                    a.a(activity);
                    Context c = a.c();
                    if (c == null || !h.this.j.c() || !(c instanceof b) || ((b) c).g) {
                        new u.a().a("onActivityResumed() Activity Lifecycle Callback").a(u.d);
                        a.a(activity);
                        if (h.this.w != null) {
                            h.this.w.a(h.this.w.c()).b();
                            x unused = h.this.w = (x) null;
                        }
                        boolean unused2 = h.this.J = false;
                        h.this.j.d(true);
                        h.this.j.e(true);
                        h.this.j.f(false);
                        if (h.this.f && !h.this.j.e()) {
                            h.this.j.a(true);
                        }
                        h.this.l.a();
                        if (w.l == null || w.l.d == null || w.l.d.isShutdown() || w.l.d.isTerminated()) {
                            AdColony.a(activity, a.a().v);
                            return;
                        }
                        return;
                    }
                    new u.a().a("Ignoring onActivityResumed").a(u.d);
                }

                public void onActivityPaused(Activity activity) {
                    a.b = false;
                    h.this.j.d(false);
                    h.this.j.e(true);
                    a.a().m().K();
                }

                public void onActivityCreated(Activity activity, Bundle bundle) {
                    if (!h.this.j.e()) {
                        h.this.j.a(true);
                    }
                    a.a(activity);
                }
            };
            if (c2 instanceof Application) {
                application = (Application) c2;
            } else {
                application = ((Activity) c2).getApplication();
            }
            application.registerActivityLifecycleCallbacks(this.W);
        }
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions d() {
        if (this.v == null) {
            this.v = new AdColonyAppOptions();
        }
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.v != null;
    }

    /* access modifiers changed from: package-private */
    public void b(@NonNull AdColonyAppOptions adColonyAppOptions) {
        this.v = adColonyAppOptions;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyZone> f() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.J = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.J;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.K;
    }

    /* access modifiers changed from: package-private */
    public AdColonyRewardListener i() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyRewardListener adColonyRewardListener) {
        this.t = adColonyRewardListener;
    }

    /* access modifiers changed from: package-private */
    public p j() {
        if (this.m == null) {
            this.m = new p();
            this.m.a();
        }
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public af k() {
        if (this.j == null) {
            this.j = new af();
            this.j.a();
        }
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public d l() {
        if (this.k == null) {
            this.k = new d();
            this.k.a();
        }
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public j m() {
        if (this.c == null) {
            this.c = new j();
            this.c.e();
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public ai n() {
        if (this.n == null) {
            this.n = new ai();
            this.n.a();
        }
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public ag o() {
        if (this.o == null) {
            this.o = new ag();
            this.o.a();
        }
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public ab p() {
        if (this.d == null) {
            this.d = new ab();
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public y q() {
        if (this.h == null) {
            this.h = new y();
            this.h.a();
        }
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public k r() {
        if (this.l == null) {
            this.l = new k();
        }
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public m s() {
        if (this.i == null) {
            this.i = new m();
        }
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public c t() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        this.q = cVar;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAdView u() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAdView adColonyAdView) {
        this.r = adColonyAdView;
    }

    /* access modifiers changed from: package-private */
    public AdColonyInterstitial v() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.s = adColonyInterstitial;
    }

    /* access modifiers changed from: package-private */
    public String w() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.C = str;
    }

    /* access modifiers changed from: package-private */
    public boolean x() {
        return this.I;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        this.I = z2;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, am> y() {
        return this.B;
    }

    /* access modifiers changed from: package-private */
    public boolean z() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z2) {
        this.x = z2;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyCustomMessageListener> A() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public boolean B() {
        return this.L;
    }

    static String C() {
        return H;
    }

    /* access modifiers changed from: package-private */
    public Partner D() {
        return this.X;
    }
}
