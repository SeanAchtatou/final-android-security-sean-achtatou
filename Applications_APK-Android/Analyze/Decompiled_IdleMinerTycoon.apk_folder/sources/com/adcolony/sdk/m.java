package com.adcolony.sdk;

import com.adcolony.sdk.l;
import com.adcolony.sdk.u;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

class m implements l.a {
    private BlockingQueue<Runnable> a = new LinkedBlockingQueue();
    private ThreadPoolExecutor b = new ThreadPoolExecutor(4, 16, 60, TimeUnit.SECONDS, this.a);
    private LinkedList<l> c = new LinkedList<>();
    private String d = a.a().m().I();

    m() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("WebServices.download", new z() {
            public void a(x xVar) {
                m.this.a(new l(xVar, m.this));
            }
        });
        a.a("WebServices.get", new z() {
            public void a(x xVar) {
                m.this.a(new l(xVar, m.this));
            }
        });
        a.a("WebServices.post", new z() {
            public void a(x xVar) {
                m.this.a(new l(xVar, m.this));
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(l lVar) {
        if (this.d == null || this.d.equals("")) {
            this.c.push(lVar);
            return;
        }
        try {
            this.b.execute(lVar);
        } catch (RejectedExecutionException unused) {
            u.a a2 = new u.a().a("RejectedExecutionException: ThreadPoolExecutor unable to ");
            a2.a("execute download for url " + lVar.a).a(u.h);
            a(lVar, lVar.a(), null);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.d = str;
        while (!this.c.isEmpty()) {
            a(this.c.removeLast());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b.setCorePoolSize(i);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b.getCorePoolSize();
    }

    public void a(l lVar, x xVar, Map<String, List<String>> map) {
        JSONObject a2 = s.a();
        s.a(a2, "url", lVar.a);
        s.b(a2, "success", lVar.c);
        s.b(a2, "status", lVar.e);
        s.a(a2, "body", lVar.b);
        s.b(a2, "size", lVar.d);
        if (map != null) {
            JSONObject a3 = s.a();
            for (Map.Entry next : map.entrySet()) {
                String obj = ((List) next.getValue()).toString();
                String substring = obj.substring(1, obj.length() - 1);
                if (next.getKey() != null) {
                    s.a(a3, (String) next.getKey(), substring);
                }
            }
            s.a(a2, "headers", a3);
        }
        xVar.a(a2).b();
    }
}
