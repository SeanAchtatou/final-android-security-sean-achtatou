package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.FloatRange;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.adcolony.sdk.u;
import com.iab.omid.library.adcolony.adsession.AdSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class c extends FrameLayout {
    private AdSession A;
    boolean a;
    boolean b;
    Context c;
    VideoView d;
    private HashMap<Integer, al> e;
    private HashMap<Integer, aj> f;
    private HashMap<Integer, am> g;
    private HashMap<Integer, n> h;
    private HashMap<Integer, q> i;
    private HashMap<Integer, Boolean> j;
    private HashMap<Integer, View> k;
    private int l;
    private int m;
    private int n;
    private int o;
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public float q = 0.0f;
    /* access modifiers changed from: private */
    public double r = 0.0d;
    /* access modifiers changed from: private */
    public long s = 0;
    /* access modifiers changed from: private */
    public int t = 0;
    /* access modifiers changed from: private */
    public int u = 0;
    private ArrayList<z> v;
    private ArrayList<String> w;
    private boolean x;
    private boolean y;
    private boolean z;

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    private c(Context context) {
        super(context);
    }

    c(Context context, String str) {
        super(context);
        this.c = context;
        this.p = str;
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
    }

    /* access modifiers changed from: package-private */
    public boolean a(x xVar) {
        JSONObject c2 = xVar.c();
        return s.c(c2, "container_id") == this.n && s.b(c2, "ad_session_id").equals(this.p);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$3, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$4, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$5, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$6, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$7, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$8, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.c$9, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* access modifiers changed from: package-private */
    public void b(x xVar) {
        int i2;
        this.e = new HashMap<>();
        this.f = new HashMap<>();
        this.g = new HashMap<>();
        this.h = new HashMap<>();
        this.i = new HashMap<>();
        this.j = new HashMap<>();
        this.k = new HashMap<>();
        this.v = new ArrayList<>();
        this.w = new ArrayList<>();
        JSONObject c2 = xVar.c();
        if (s.d(c2, "transparent")) {
            setBackgroundColor(0);
        }
        this.n = s.c(c2, "id");
        this.l = s.c(c2, "width");
        this.m = s.c(c2, "height");
        this.o = s.c(c2, "module_id");
        this.b = s.d(c2, "viewability_enabled");
        this.x = this.n == 1;
        h a2 = a.a();
        if (this.l == 0 && this.m == 0) {
            this.l = a2.m().s();
            if (a2.d().getMultiWindowEnabled()) {
                i2 = a2.m().t() - ak.c(a.c());
            } else {
                i2 = a2.m().t();
            }
            this.m = i2;
        } else {
            setLayoutParams(new FrameLayout.LayoutParams(this.l, this.m));
        }
        this.v.add(a.a("VideoView.create", (z) new z() {
            public void a(x xVar) {
                if (c.this.a(xVar)) {
                    c.this.a(c.this.e(xVar));
                }
            }
        }, true));
        this.v.add(a.a("VideoView.destroy", (z) new z() {
            public void a(x xVar) {
                if (c.this.a(xVar)) {
                    c.this.f(xVar);
                }
            }
        }, true));
        this.v.add(a.a("WebView.create", (z) new z() {
            public void a(final x xVar) {
                if (c.this.a(xVar)) {
                    ak.a(new Runnable() {
                        public void run() {
                            c.this.a(c.this.g(xVar));
                        }
                    });
                }
            }
        }, true));
        this.v.add(a.a("WebView.destroy", (z) new z() {
            public void a(final x xVar) {
                if (c.this.a(xVar)) {
                    ak.a(new Runnable() {
                        public void run() {
                            c.this.h(xVar);
                        }
                    });
                }
            }
        }, true));
        this.v.add(a.a("TextView.create", (z) new z() {
            public void a(x xVar) {
                if (c.this.a(xVar)) {
                    c.this.a(c.this.i(xVar));
                }
            }
        }, true));
        this.v.add(a.a("TextView.destroy", (z) new z() {
            public void a(x xVar) {
                if (c.this.a(xVar)) {
                    c.this.j(xVar);
                }
            }
        }, true));
        this.v.add(a.a("ImageView.create", (z) new z() {
            public void a(x xVar) {
                if (c.this.a(xVar)) {
                    c.this.a(c.this.c(xVar));
                }
            }
        }, true));
        this.v.add(a.a("ImageView.destroy", (z) new z() {
            public void a(x xVar) {
                if (c.this.a(xVar)) {
                    c.this.d(xVar);
                }
            }
        }, true));
        this.w.add("VideoView.create");
        this.w.add("VideoView.destroy");
        this.w.add("WebView.create");
        this.w.add("WebView.destroy");
        this.w.add("TextView.create");
        this.w.add("TextView.destroy");
        this.w.add("ImageView.create");
        this.w.add("ImageView.destroy");
        this.d = new VideoView(this.c);
        this.d.setVisibility(8);
        addView(this.d);
        setClipToPadding(false);
        if (this.b) {
            d(s.d(xVar.c(), "advanced_viewability"));
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        h a2 = a.a();
        d l2 = a2.l();
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = s.a();
        s.b(a3, "view_id", -1);
        s.a(a3, "ad_session_id", this.p);
        s.b(a3, "container_x", x2);
        s.b(a3, "container_y", y2);
        s.b(a3, "view_x", x2);
        s.b(a3, "view_y", y2);
        s.b(a3, "id", this.n);
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.o, a3).b();
                break;
            case 1:
                if (!this.x) {
                    a2.a(l2.e().get(this.p));
                }
                new x("AdContainer.on_touch_ended", this.o, a3).b();
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.o, a3).b();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.o, a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", (int) motionEvent.getX(action2));
                s.b(a3, "container_y", (int) motionEvent.getY(action2));
                s.b(a3, "view_x", (int) motionEvent.getX(action2));
                s.b(a3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.o, a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", (int) motionEvent.getX(action3));
                s.b(a3, "container_y", (int) motionEvent.getY(action3));
                s.b(a3, "view_x", (int) motionEvent.getX(action3));
                s.b(a3, "view_y", (int) motionEvent.getY(action3));
                s.b(a3, "x", (int) motionEvent.getX(action3));
                s.b(a3, "y", (int) motionEvent.getY(action3));
                if (!this.x) {
                    a2.a(l2.e().get(this.p));
                }
                new x("AdContainer.on_touch_ended", this.o, a3).b();
                break;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public q c(x xVar) {
        int c2 = s.c(xVar.c(), "id");
        q qVar = new q(this.c, xVar, c2, this);
        qVar.a();
        this.i.put(Integer.valueOf(c2), qVar);
        this.k.put(Integer.valueOf(c2), qVar);
        return qVar;
    }

    /* access modifiers changed from: package-private */
    public boolean d(x xVar) {
        int c2 = s.c(xVar.c(), "id");
        View remove = this.k.remove(Integer.valueOf(c2));
        q remove2 = this.i.remove(Integer.valueOf(c2));
        if (remove == null || remove2 == null) {
            d l2 = a.a().l();
            String d2 = xVar.d();
            l2.a(d2, "" + c2);
            return false;
        }
        removeView(remove2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public al e(x xVar) {
        int c2 = s.c(xVar.c(), "id");
        al alVar = new al(this.c, xVar, c2, this);
        alVar.b();
        this.e.put(Integer.valueOf(c2), alVar);
        this.k.put(Integer.valueOf(c2), alVar);
        return alVar;
    }

    /* access modifiers changed from: package-private */
    public boolean f(x xVar) {
        int c2 = s.c(xVar.c(), "id");
        View remove = this.k.remove(Integer.valueOf(c2));
        al remove2 = this.e.remove(Integer.valueOf(c2));
        if (remove == null || remove2 == null) {
            d l2 = a.a().l();
            String d2 = xVar.d();
            l2.a(d2, "" + c2);
            return false;
        }
        if (remove2.h()) {
            remove2.d();
        }
        remove2.a();
        removeView(remove2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public am g(x xVar) {
        am amVar;
        JSONObject c2 = xVar.c();
        int c3 = s.c(c2, "id");
        boolean d2 = s.d(c2, "is_module");
        h a2 = a.a();
        if (d2) {
            amVar = a2.y().get(Integer.valueOf(s.c(c2, "module_id")));
            if (amVar == null) {
                new u.a().a("Module WebView created with invalid id").a(u.g);
                return null;
            }
            amVar.a(xVar, c3, this);
        } else {
            try {
                amVar = new am(this.c, xVar, c3, a2.q().d(), this);
            } catch (RuntimeException e2) {
                u.a aVar = new u.a();
                aVar.a(e2.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(u.g);
                AdColony.disable();
                return null;
            }
        }
        this.g.put(Integer.valueOf(c3), amVar);
        this.k.put(Integer.valueOf(c3), amVar);
        JSONObject a3 = s.a();
        s.b(a3, "module_id", amVar.a());
        s.b(a3, "mraid_module_id", amVar.b());
        xVar.a(a3).b();
        return amVar;
    }

    /* access modifiers changed from: package-private */
    public boolean h(x xVar) {
        int c2 = s.c(xVar.c(), "id");
        h a2 = a.a();
        View remove = this.k.remove(Integer.valueOf(c2));
        am remove2 = this.g.remove(Integer.valueOf(c2));
        if (remove2 == null || remove == null) {
            d l2 = a2.l();
            String d2 = xVar.d();
            l2.a(d2, "" + c2);
            return false;
        }
        a2.q().a(remove2.a());
        removeView(remove2);
        return true;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"InlinedApi"})
    public View i(x xVar) {
        JSONObject c2 = xVar.c();
        int c3 = s.c(c2, "id");
        if (s.d(c2, "editable")) {
            n nVar = new n(this.c, xVar, c3, this);
            nVar.a();
            this.h.put(Integer.valueOf(c3), nVar);
            this.k.put(Integer.valueOf(c3), nVar);
            this.j.put(Integer.valueOf(c3), true);
            return nVar;
        } else if (!s.d(c2, "button")) {
            aj ajVar = new aj(this.c, xVar, c3, this);
            ajVar.a();
            this.f.put(Integer.valueOf(c3), ajVar);
            this.k.put(Integer.valueOf(c3), ajVar);
            this.j.put(Integer.valueOf(c3), false);
            return ajVar;
        } else {
            aj ajVar2 = new aj(this.c, 16974145, xVar, c3, this);
            ajVar2.a();
            this.f.put(Integer.valueOf(c3), ajVar2);
            this.k.put(Integer.valueOf(c3), ajVar2);
            this.j.put(Integer.valueOf(c3), false);
            return ajVar2;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean j(x xVar) {
        TextView textView;
        int c2 = s.c(xVar.c(), "id");
        View remove = this.k.remove(Integer.valueOf(c2));
        if (this.j.remove(Integer.valueOf(this.n)).booleanValue()) {
            textView = this.h.remove(Integer.valueOf(c2));
        } else {
            textView = this.f.remove(Integer.valueOf(c2));
        }
        if (remove == null || textView == null) {
            d l2 = a.a().l();
            String d2 = xVar.d();
            l2.a(d2, "" + c2);
            return false;
        }
        removeView(textView);
        return true;
    }

    private void d(final boolean z2) {
        final AnonymousClass10 r0 = new Runnable() {
            public void run() {
                am webView;
                double d;
                if (c.this.s == 0) {
                    long unused = c.this.s = System.currentTimeMillis();
                }
                View view = (View) c.this.getParent();
                AdColonyAdView adColonyAdView = a.a().l().e().get(c.this.p);
                if (adColonyAdView == null) {
                    webView = null;
                } else {
                    webView = adColonyAdView.getWebView();
                }
                am amVar = webView;
                Context c = a.c();
                boolean z = false;
                float a2 = ao.a(view, c, true, z2, true, adColonyAdView != null);
                if (c == null) {
                    d = 0.0d;
                } else {
                    d = ak.b(ak.a(c));
                }
                int a3 = ak.a(amVar);
                int b2 = ak.b(amVar);
                if (!(a3 == c.this.t && b2 == c.this.u)) {
                    z = true;
                }
                if (z) {
                    int unused2 = c.this.t = a3;
                    int unused3 = c.this.u = b2;
                    c.this.a(a3, b2, amVar);
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (c.this.s + 200 < currentTimeMillis) {
                    long unused4 = c.this.s = currentTimeMillis;
                    if (!(c.this.q == a2 && c.this.r == d && !z)) {
                        c.this.a(a2, d);
                    }
                    float unused5 = c.this.q = a2;
                    double unused6 = c.this.r = d;
                }
            }
        };
        new Thread(new Runnable() {
            public void run() {
                while (!c.this.a) {
                    ak.a(r0);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException unused) {
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, am amVar) {
        float r2 = a.a().m().r();
        if (amVar != null) {
            JSONObject a2 = s.a();
            s.b(a2, "app_orientation", ak.j(ak.h()));
            s.b(a2, "width", (int) (((float) amVar.s()) / r2));
            s.b(a2, "height", (int) (((float) amVar.t()) / r2));
            s.b(a2, "x", i2);
            s.b(a2, "y", i3);
            s.a(a2, "ad_session_id", this.p);
            new x("MRAID.on_size_change", this.o, a2).b();
        }
    }

    /* access modifiers changed from: private */
    public void a(@FloatRange(from = 0.0d, to = 100.0d) float f2, @FloatRange(from = 0.0d, to = 1.0d) double d2) {
        JSONObject a2 = s.a();
        s.b(a2, "id", this.n);
        s.a(a2, "ad_session_id", this.p);
        s.a(a2, "exposure", (double) f2);
        s.a(a2, "volume", d2);
        new x("AdContainer.on_exposure_change", this.o, a2).b();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        JSONObject a2 = s.a();
        s.a(a2, "id", this.p);
        new x("AdSession.on_error", this.o, a2).b();
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, al> e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, aj> f() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, am> g() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, n> h() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, q> i() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, Boolean> j() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, View> k() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<z> l() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> m() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public int n() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.m = i2;
    }

    /* access modifiers changed from: package-private */
    public int o() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        this.l = i2;
    }

    /* access modifiers changed from: package-private */
    public boolean p() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.x = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.z = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean r() {
        return this.y;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        this.y = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(AdSession adSession) {
        this.A = adSession;
        a(this.k);
    }

    /* access modifiers changed from: package-private */
    public void a(Map map) {
        if (this.A != null && map != null) {
            for (Map.Entry value : map.entrySet()) {
                this.A.addFriendlyObstruction((View) value.getValue());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        if (this.A != null && view != null) {
            this.A.addFriendlyObstruction(view);
        }
    }
}
