package com.adcolony.sdk;

class u {
    static u a = new u(3, false);
    static u b = new u(3, true);
    static u c = new u(2, false);
    static u d = new u(2, true);
    static u e = new u(1, false);
    static u f = new u(1, true);
    static u g = new u(0, false);
    static u h = new u(0, true);
    private int i;
    private boolean j;

    u(int i2, boolean z) {
        this.i = i2;
        this.j = z;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        w.a(this.i, str, this.j);
    }

    static class a {
        StringBuilder a = new StringBuilder();

        a() {
        }

        /* access modifiers changed from: package-private */
        public a a(char c) {
            if (c != 10) {
                this.a.append(c);
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.a.append(str);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(Object obj) {
            if (obj != null) {
                this.a.append(obj.toString());
            } else {
                this.a.append("null");
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(double d) {
            ak.a(d, 2, this.a);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(int i) {
            this.a.append(i);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(boolean z) {
            this.a.append(z);
            return this;
        }

        /* access modifiers changed from: package-private */
        public void a(u uVar) {
            uVar.a(this.a.toString());
        }
    }
}
