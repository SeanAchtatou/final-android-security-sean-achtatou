package com.adcolony.sdk;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewParent;

public class AdColonyAdViewActivity extends b {
    AdColonyAdView m;

    public AdColonyAdViewActivity() {
        AdColonyAdView adColonyAdView;
        if (!a.b()) {
            adColonyAdView = null;
        } else {
            adColonyAdView = a.a().u();
        }
        this.m = adColonyAdView;
    }

    public /* bridge */ /* synthetic */ void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    public /* bridge */ /* synthetic */ void onPause() {
        super.onPause();
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    public /* bridge */ /* synthetic */ void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void onCreate(Bundle bundle) {
        if (!a.b() || this.m == null) {
            a.a().a((AdColonyAdView) null);
            finish();
            return;
        }
        this.d = this.m.getOrientation();
        super.onCreate(bundle);
        this.m.b();
        AdColonyAdViewListener listener = this.m.getListener();
        if (listener != null) {
            listener.onOpened(this.m);
        }
    }

    public void onBackPressed() {
        b();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        ViewParent parent = this.c.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.c);
        }
        this.m.c();
        a.a().a((AdColonyAdView) null);
        finish();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.m.b();
    }
}
