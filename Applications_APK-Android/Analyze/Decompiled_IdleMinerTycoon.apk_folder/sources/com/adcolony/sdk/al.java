package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import com.adcolony.sdk.u;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

@TargetApi(14)
class al extends TextureView implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener, TextureView.SurfaceTextureListener {
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    /* access modifiers changed from: private */
    public boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private String F;
    /* access modifiers changed from: private */
    public String G;
    private FileInputStream H;
    private x I;
    /* access modifiers changed from: private */
    public c J;
    private Surface K;
    private SurfaceTexture L;
    /* access modifiers changed from: private */
    public RectF M = new RectF();
    /* access modifiers changed from: private */
    public a N;
    private ProgressBar O;
    /* access modifiers changed from: private */
    public MediaPlayer P;
    /* access modifiers changed from: private */
    public JSONObject Q = s.a();
    private ExecutorService R = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public x S;
    private float a;
    private float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    private float e;
    private float f;
    /* access modifiers changed from: private */
    public int g;
    private boolean h = true;
    /* access modifiers changed from: private */
    public Paint i = new Paint();
    /* access modifiers changed from: private */
    public Paint j = new Paint(1);
    private int k;
    private int l;
    private int m;
    private int n;
    /* access modifiers changed from: private */
    public int o;
    private int p;
    private int q;
    private int r;
    /* access modifiers changed from: private */
    public double s;
    /* access modifiers changed from: private */
    public double t;
    /* access modifiers changed from: private */
    public long u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public boolean y;
    private boolean z;

    private al(Context context) {
        super(context);
    }

    al(Context context, x xVar, int i2, c cVar) {
        super(context);
        this.J = cVar;
        this.I = xVar;
        this.o = i2;
        setSurfaceTextureListener(this);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.L != null) {
            this.A = true;
        }
        this.R.shutdown();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.al$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.al$2, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.al$3, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.al$4, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.al$5, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.al$6, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* access modifiers changed from: package-private */
    public void b() {
        Context c2;
        JSONObject c3 = this.I.c();
        this.G = s.b(c3, "ad_session_id");
        this.k = s.c(c3, "x");
        this.l = s.c(c3, "y");
        this.m = s.c(c3, "width");
        this.n = s.c(c3, "height");
        this.C = s.d(c3, "enable_timer");
        this.E = s.d(c3, "enable_progress");
        this.F = s.b(c3, "filepath");
        this.p = s.c(c3, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_WIDTH);
        this.q = s.c(c3, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_HEIGHT);
        this.f = a.a().m().r();
        new u.a().a("Original video dimensions = ").a(this.p).a("x").a(this.q).a(u.b);
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.m, this.n);
        layoutParams.setMargins(this.k, this.l, 0, 0);
        layoutParams.gravity = 0;
        this.J.addView(this, layoutParams);
        if (this.E && (c2 = a.c()) != null) {
            this.O = new ProgressBar(c2);
            this.J.addView(this.O, new FrameLayout.LayoutParams((int) (this.f * 100.0f), (int) (this.f * 100.0f), 17));
        }
        this.P = new MediaPlayer();
        this.z = false;
        try {
            if (!this.F.startsWith("http")) {
                this.H = new FileInputStream(this.F);
                this.P.setDataSource(this.H.getFD());
            } else {
                this.B = true;
                this.P.setDataSource(this.F);
            }
            this.P.setOnErrorListener(this);
            this.P.setOnPreparedListener(this);
            this.P.setOnCompletionListener(this);
            this.P.prepareAsync();
        } catch (IOException e2) {
            new u.a().a("Failed to create/prepare MediaPlayer: ").a(e2.toString()).a(u.g);
            l();
        }
        this.J.l().add(a.a("VideoView.play", (z) new z() {
            public void a(x xVar) {
                if (al.this.a(xVar)) {
                    al.this.e();
                }
            }
        }, true));
        this.J.l().add(a.a("VideoView.set_bounds", (z) new z() {
            public void a(x xVar) {
                if (al.this.a(xVar)) {
                    al.this.b(xVar);
                }
            }
        }, true));
        this.J.l().add(a.a("VideoView.set_visible", (z) new z() {
            public void a(x xVar) {
                if (al.this.a(xVar)) {
                    al.this.c(xVar);
                }
            }
        }, true));
        this.J.l().add(a.a("VideoView.pause", (z) new z() {
            public void a(x xVar) {
                if (al.this.a(xVar)) {
                    al.this.f();
                }
            }
        }, true));
        this.J.l().add(a.a("VideoView.seek_to_time", (z) new z() {
            public void a(x xVar) {
                if (al.this.a(xVar)) {
                    boolean unused = al.this.e(xVar);
                }
            }
        }, true));
        this.J.l().add(a.a("VideoView.set_volume", (z) new z() {
            public void a(x xVar) {
                if (al.this.a(xVar)) {
                    boolean unused = al.this.d(xVar);
                }
            }
        }, true));
        this.J.m().add("VideoView.play");
        this.J.m().add("VideoView.set_bounds");
        this.J.m().add("VideoView.set_visible");
        this.J.m().add("VideoView.pause");
        this.J.m().add("VideoView.seek_to_time");
        this.J.m().add("VideoView.set_volume");
    }

    /* access modifiers changed from: private */
    public boolean a(x xVar) {
        JSONObject c2 = xVar.c();
        return s.c(c2, "id") == this.o && s.c(c2, "container_id") == this.J.d() && s.b(c2, "ad_session_id").equals(this.J.b());
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (surfaceTexture == null || this.A) {
            new u.a().a("Null texture provided by system's onSurfaceTextureAvailable or ").a("MediaPlayer has been destroyed.").a(u.h);
            return;
        }
        this.K = new Surface(surfaceTexture);
        try {
            this.P.setSurface(this.K);
        } catch (IllegalStateException unused) {
            new u.a().a("IllegalStateException thrown when calling MediaPlayer.setSurface()").a(u.g);
            l();
        }
        this.L = surfaceTexture;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.L = surfaceTexture;
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.L = surfaceTexture;
        if (!this.A) {
            return false;
        }
        surfaceTexture.release();
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.L = surfaceTexture;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        h a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = s.a();
        s.b(a3, "view_id", this.o);
        s.a(a3, "ad_session_id", this.G);
        s.b(a3, "container_x", this.k + x2);
        s.b(a3, "container_y", this.l + y2);
        s.b(a3, "view_x", x2);
        s.b(a3, "view_y", y2);
        s.b(a3, "id", this.J.d());
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.J.c(), a3).b();
                break;
            case 1:
                if (!this.J.p()) {
                    a2.a(l2.e().get(this.G));
                }
                new x("AdContainer.on_touch_ended", this.J.c(), a3).b();
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.J.c(), a3).b();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.J.c(), a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", ((int) motionEvent.getX(action2)) + this.k);
                s.b(a3, "container_y", ((int) motionEvent.getY(action2)) + this.l);
                s.b(a3, "view_x", (int) motionEvent.getX(action2));
                s.b(a3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.J.c(), a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", ((int) motionEvent.getX(action3)) + this.k);
                s.b(a3, "container_y", ((int) motionEvent.getY(action3)) + this.l);
                s.b(a3, "view_x", (int) motionEvent.getX(action3));
                s.b(a3, "view_y", (int) motionEvent.getY(action3));
                if (!this.J.p()) {
                    a2.a(l2.e().get(this.G));
                }
                new x("AdContainer.on_touch_ended", this.J.c(), a3).b();
                break;
        }
        return true;
    }

    private void k() {
        double d2 = (double) this.m;
        double d3 = (double) this.p;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        double d5 = (double) this.n;
        double d6 = (double) this.q;
        Double.isNaN(d5);
        Double.isNaN(d6);
        double d7 = d5 / d6;
        if (d4 > d7) {
            d4 = d7;
        }
        double d8 = (double) this.p;
        Double.isNaN(d8);
        int i2 = (int) (d8 * d4);
        double d9 = (double) this.q;
        Double.isNaN(d9);
        int i3 = (int) (d9 * d4);
        new u.a().a("setMeasuredDimension to ").a(i2).a(" by ").a(i3).a(u.d);
        setMeasuredDimension(i2, i3);
        if (this.B) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
            layoutParams.width = i2;
            layoutParams.height = i3;
            layoutParams.gravity = 17;
            layoutParams.setMargins(0, 0, 0, 0);
            setLayoutParams(layoutParams);
        }
    }

    public void onMeasure(int i2, int i3) {
        k();
    }

    /* access modifiers changed from: private */
    public void l() {
        JSONObject a2 = s.a();
        s.a(a2, "id", this.G);
        new x("AdSession.on_error", this.J.c(), a2).b();
        this.v = true;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        l();
        u.a aVar = new u.a();
        aVar.a("MediaPlayer error: " + i2 + "," + i3).a(u.g);
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.z = true;
        if (this.E) {
            this.J.removeView(this.O);
        }
        if (this.B) {
            this.p = mediaPlayer.getVideoWidth();
            this.q = mediaPlayer.getVideoHeight();
            k();
            new u.a().a("MediaPlayer getVideoWidth = ").a(mediaPlayer.getVideoWidth()).a(u.d);
            new u.a().a("MediaPlayer getVideoHeight = ").a(mediaPlayer.getVideoHeight()).a(u.d);
        }
        JSONObject a2 = s.a();
        s.b(a2, "id", this.o);
        s.b(a2, "container_id", this.J.d());
        s.a(a2, "ad_session_id", this.G);
        new u.a().a("ADCVideoView is prepared").a(u.b);
        new x("VideoView.on_ready", this.J.c(), a2).b();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.v = true;
        this.s = this.t;
        s.b(this.Q, "id", this.o);
        s.b(this.Q, "container_id", this.J.d());
        s.a(this.Q, "ad_session_id", this.G);
        s.a(this.Q, "elapsed", this.s);
        s.a(this.Q, IronSourceConstants.EVENTS_DURATION, this.t);
        new x("VideoView.on_progress", this.J.c(), this.Q).b();
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        if (this.R != null && !this.R.isShutdown()) {
            try {
                this.R.submit(new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
                     arg types: [org.json.JSONObject, java.lang.String, int]
                     candidates:
                      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
                      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                    public void run() {
                        try {
                            Thread.sleep(150);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (al.this.S != null) {
                            JSONObject a2 = s.a();
                            s.b(a2, "id", al.this.o);
                            s.a(a2, "ad_session_id", al.this.G);
                            s.b(a2, "success", true);
                            al.this.S.a(a2).b();
                            x unused = al.this.S = null;
                        }
                    }
                });
            } catch (RejectedExecutionException unused) {
                l();
            }
        }
    }

    private void m() {
        try {
            this.R.submit(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.al.a(com.adcolony.sdk.al, long):long
                 arg types: [com.adcolony.sdk.al, int]
                 candidates:
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, double):double
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, com.adcolony.sdk.al$a):com.adcolony.sdk.al$a
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, com.adcolony.sdk.x):boolean
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, boolean):boolean
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, long):long */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.al.a(com.adcolony.sdk.al, boolean):boolean
                 arg types: [com.adcolony.sdk.al, int]
                 candidates:
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, double):double
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, long):long
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, com.adcolony.sdk.al$a):com.adcolony.sdk.al$a
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, com.adcolony.sdk.x):boolean
                  com.adcolony.sdk.al.a(com.adcolony.sdk.al, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.al.b(com.adcolony.sdk.al, boolean):boolean
                 arg types: [com.adcolony.sdk.al, int]
                 candidates:
                  com.adcolony.sdk.al.b(com.adcolony.sdk.al, double):double
                  com.adcolony.sdk.al.b(com.adcolony.sdk.al, com.adcolony.sdk.x):void
                  com.adcolony.sdk.al.b(com.adcolony.sdk.al, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.al.c(com.adcolony.sdk.al, boolean):boolean
                 arg types: [com.adcolony.sdk.al, int]
                 candidates:
                  com.adcolony.sdk.al.c(com.adcolony.sdk.al, com.adcolony.sdk.x):void
                  com.adcolony.sdk.al.c(com.adcolony.sdk.al, boolean):boolean */
                public void run() {
                    long unused = al.this.u = 0L;
                    while (!al.this.v && !al.this.y && a.d()) {
                        Context c = a.c();
                        if (!al.this.v && !al.this.A && c != null && (c instanceof Activity)) {
                            if (al.this.P.isPlaying()) {
                                if (al.this.u == 0 && a.b) {
                                    long unused2 = al.this.u = System.currentTimeMillis();
                                }
                                boolean unused3 = al.this.x = true;
                                al alVar = al.this;
                                double currentPosition = (double) al.this.P.getCurrentPosition();
                                Double.isNaN(currentPosition);
                                double unused4 = alVar.s = currentPosition / 1000.0d;
                                al alVar2 = al.this;
                                double duration = (double) al.this.P.getDuration();
                                Double.isNaN(duration);
                                double unused5 = alVar2.t = duration / 1000.0d;
                                if (System.currentTimeMillis() - al.this.u > 1000 && !al.this.D && a.b) {
                                    if (al.this.s == 0.0d) {
                                        new u.a().a("getCurrentPosition() not working, firing ").a("AdSession.on_error").a(u.h);
                                        al.this.l();
                                    } else {
                                        boolean unused6 = al.this.D = true;
                                    }
                                }
                                if (al.this.C) {
                                    al.this.c();
                                }
                            }
                            if (al.this.x && !al.this.v && !al.this.y) {
                                s.b(al.this.Q, "id", al.this.o);
                                s.b(al.this.Q, "container_id", al.this.J.d());
                                s.a(al.this.Q, "ad_session_id", al.this.G);
                                s.a(al.this.Q, "elapsed", al.this.s);
                                s.a(al.this.Q, IronSourceConstants.EVENTS_DURATION, al.this.t);
                                new x("VideoView.on_progress", al.this.J.c(), al.this.Q).b();
                            }
                            if (al.this.w || ((Activity) c).isFinishing()) {
                                boolean unused7 = al.this.w = false;
                                al.this.d();
                                return;
                            }
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException unused8) {
                                al.this.l();
                                new u.a().a("InterruptedException in ADCVideoView's update thread.").a(u.g);
                            }
                        } else {
                            return;
                        }
                    }
                    if (al.this.w) {
                        al.this.d();
                    }
                }
            });
        } catch (RejectedExecutionException unused) {
            l();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, int, ?]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    /* access modifiers changed from: package-private */
    public void c() {
        if (this.h) {
            this.e = (float) (360.0d / this.t);
            this.j.setColor(-3355444);
            this.j.setShadowLayer((float) ((int) (this.f * 2.0f)), 0.0f, 0.0f, (int) ViewCompat.MEASURED_STATE_MASK);
            this.j.setTextAlign(Paint.Align.CENTER);
            this.j.setLinearText(true);
            this.j.setTextSize(this.f * 12.0f);
            this.i.setStyle(Paint.Style.STROKE);
            float f2 = 6.0f;
            if (this.f * 2.0f <= 6.0f) {
                f2 = this.f * 2.0f;
            }
            float f3 = 4.0f;
            if (f2 >= 4.0f) {
                f3 = f2;
            }
            this.i.setStrokeWidth(f3);
            this.i.setShadowLayer((float) ((int) (this.f * 3.0f)), 0.0f, 0.0f, (int) ViewCompat.MEASURED_STATE_MASK);
            this.i.setColor(-3355444);
            Rect rect = new Rect();
            this.j.getTextBounds("0123456789", 0, 9, rect);
            this.c = (float) rect.height();
            final Context c2 = a.c();
            if (c2 != null) {
                ak.a(new Runnable() {
                    public void run() {
                        a unused = al.this.N = new a(c2);
                        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) (al.this.c * 4.0f), (int) (al.this.c * 4.0f));
                        layoutParams.setMargins(0, al.this.J.n() - ((int) (al.this.c * 4.0f)), 0, 0);
                        layoutParams.gravity = 0;
                        al.this.J.addView(al.this.N, layoutParams);
                    }
                });
            }
            this.h = false;
        }
        this.g = (int) (this.t - this.s);
        this.a = (float) ((int) this.c);
        this.b = (float) ((int) (this.c * 3.0f));
        this.M.set(this.a - (this.c / 2.0f), this.b - (this.c * 2.0f), this.a + (this.c * 2.0f), this.b + (this.c / 2.0f));
        double d2 = (double) this.e;
        Double.isNaN(d2);
        this.d = (float) (d2 * (this.t - this.s));
    }

    /* access modifiers changed from: package-private */
    public void d() {
        new u.a().a("MediaPlayer stopped and released.").a(u.d);
        try {
            if (!this.v && this.z && this.P.isPlaying()) {
                this.P.stop();
            }
        } catch (IllegalStateException unused) {
            new u.a().a("Caught IllegalStateException when calling stop on MediaPlayer").a(u.f);
        }
        if (this.O != null) {
            this.J.removeView(this.O);
        }
        this.v = true;
        this.z = false;
        this.P.release();
    }

    /* access modifiers changed from: private */
    public void b(x xVar) {
        JSONObject c2 = xVar.c();
        this.k = s.c(c2, "x");
        this.l = s.c(c2, "y");
        this.m = s.c(c2, "width");
        this.n = s.c(c2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.k, this.l, 0, 0);
        layoutParams.width = this.m;
        layoutParams.height = this.n;
        setLayoutParams(layoutParams);
        if (this.C && this.N != null) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams((int) (this.c * 4.0f), (int) (this.c * 4.0f));
            layoutParams2.setMargins(0, this.J.n() - ((int) (this.c * 4.0f)), 0, 0);
            layoutParams2.gravity = 0;
            this.N.setLayoutParams(layoutParams2);
        }
    }

    /* access modifiers changed from: private */
    public void c(x xVar) {
        if (s.d(xVar.c(), TJAdUnitConstants.String.VISIBLE)) {
            setVisibility(0);
            if (this.C && this.N != null) {
                this.N.setVisibility(0);
                return;
            }
            return;
        }
        setVisibility(4);
        if (this.C && this.N != null) {
            this.N.setVisibility(4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean d(x xVar) {
        boolean z2 = false;
        if (!this.z) {
            return false;
        }
        float e2 = (float) s.e(xVar.c(), "volume");
        AdColonyInterstitial v2 = a.a().v();
        if (v2 != null) {
            if (((double) e2) <= 0.0d) {
                z2 = true;
            }
            v2.b(z2);
        }
        this.P.setVolume(e2, e2);
        JSONObject a2 = s.a();
        s.b(a2, "success", true);
        xVar.a(a2).b();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        if (!this.z) {
            return false;
        }
        if (!this.y && a.b) {
            this.P.start();
            m();
            new u.a().a("MediaPlayer is prepared - ADCVideoView play() called.").a(u.b);
        } else if (!this.v && a.b) {
            this.P.start();
            this.y = false;
            if (!this.R.isShutdown()) {
                m();
            }
            if (this.N != null) {
                this.N.invalidate();
            }
        }
        setWillNotDraw(false);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (!this.z) {
            new u.a().a("ADCVideoView pause() called while MediaPlayer is not prepared.").a(u.f);
            return false;
        } else if (!this.x) {
            new u.a().a("Ignoring ADCVideoView pause due to invalid MediaPlayer state.").a(u.d);
            return false;
        } else {
            this.r = this.P.getCurrentPosition();
            this.t = (double) this.P.getDuration();
            this.P.pause();
            this.y = true;
            new u.a().a("Video view paused").a(u.b);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean e(x xVar) {
        if (!this.z) {
            return false;
        }
        if (this.v) {
            this.v = false;
        }
        this.S = xVar;
        int c2 = s.c(xVar.c(), LocationConst.TIME);
        int duration = this.P.getDuration() / 1000;
        this.P.setOnSeekCompleteListener(this);
        this.P.seekTo(c2 * 1000);
        if (duration == c2) {
            this.v = true;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.w = true;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.P != null;
    }

    /* access modifiers changed from: package-private */
    public MediaPlayer i() {
        return this.P;
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        return this.v;
    }

    private class a extends View {
        a(Context context) {
            super(context);
            setWillNotDraw(false);
            try {
                getClass().getMethod("setLayerType", Integer.TYPE, Paint.class).invoke(this, 1, null);
            } catch (Exception unused) {
            }
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawArc(al.this.M, 270.0f, al.this.d, false, al.this.i);
            float centerX = al.this.M.centerX();
            double centerY = (double) al.this.M.centerY();
            double d = (double) al.this.j.getFontMetrics().bottom;
            Double.isNaN(d);
            Double.isNaN(centerY);
            canvas.drawText("" + al.this.g, centerX, (float) (centerY + (d * 1.35d)), al.this.j);
            invalidate();
        }
    }
}
