package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class q extends ImageView {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private boolean h;
    private String i;
    private String j;
    private x k;
    private c l;

    private q(Context context) {
        super(context);
    }

    q(Context context, x xVar, int i2, c cVar) {
        super(context);
        this.a = i2;
        this.k = xVar;
        this.l = cVar;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        h a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        JSONObject a3 = s.a();
        s.b(a3, "view_id", this.a);
        s.a(a3, "ad_session_id", this.j);
        s.b(a3, "container_x", this.b + x);
        s.b(a3, "container_y", this.c + y);
        s.b(a3, "view_x", x);
        s.b(a3, "view_y", y);
        s.b(a3, "id", this.l.getId());
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.l.c(), a3).b();
                break;
            case 1:
                if (!this.l.p()) {
                    a2.a(l2.e().get(this.j));
                }
                if (x > 0 && x < this.d && y > 0 && y < this.e) {
                    new x("AdContainer.on_touch_ended", this.l.c(), a3).b();
                    break;
                } else {
                    new x("AdContainer.on_touch_cancelled", this.l.c(), a3).b();
                    break;
                }
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.l.c(), a3).b();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.l.c(), a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", ((int) motionEvent.getX(action2)) + this.b);
                s.b(a3, "container_y", ((int) motionEvent.getY(action2)) + this.c);
                s.b(a3, "view_x", (int) motionEvent.getX(action2));
                s.b(a3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.l.c(), a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                int x2 = (int) motionEvent.getX(action3);
                int y2 = (int) motionEvent.getY(action3);
                s.b(a3, "container_x", ((int) motionEvent.getX(action3)) + this.b);
                s.b(a3, "container_y", ((int) motionEvent.getY(action3)) + this.c);
                s.b(a3, "view_x", (int) motionEvent.getX(action3));
                s.b(a3, "view_y", (int) motionEvent.getY(action3));
                if (!this.l.p()) {
                    a2.a(l2.e().get(this.j));
                }
                if (x2 > 0 && x2 < this.d && y2 > 0 && y2 < this.e) {
                    new x("AdContainer.on_touch_ended", this.l.c(), a3).b();
                    break;
                } else {
                    new x("AdContainer.on_touch_cancelled", this.l.c(), a3).b();
                    break;
                }
                break;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean a(x xVar) {
        JSONObject c2 = xVar.c();
        return s.c(c2, "id") == this.a && s.c(c2, "container_id") == this.l.d() && s.b(c2, "ad_session_id").equals(this.l.b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.q$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.q$2, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.q$3, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* access modifiers changed from: package-private */
    public void a() {
        JSONObject c2 = this.k.c();
        this.j = s.b(c2, "ad_session_id");
        this.b = s.c(c2, "x");
        this.c = s.c(c2, "y");
        this.d = s.c(c2, "width");
        this.e = s.c(c2, "height");
        this.i = s.b(c2, "filepath");
        this.f = s.d(c2, "dpi");
        this.g = s.d(c2, "invert_y");
        this.h = s.d(c2, "wrap_content");
        setImageURI(Uri.fromFile(new File(this.i)));
        if (this.f) {
            float r = (((float) this.e) * a.a().m().r()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * r);
            this.d = (int) (((float) getDrawable().getIntrinsicWidth()) * r);
            this.b -= this.d;
            this.c = this.g ? this.c + this.e : this.c - this.e;
        }
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = this.h ? new FrameLayout.LayoutParams(-2, -2) : new FrameLayout.LayoutParams(this.d, this.e);
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.gravity = 0;
        this.l.addView(this, layoutParams);
        this.l.l().add(a.a("ImageView.set_visible", (z) new z() {
            public void a(x xVar) {
                if (q.this.a(xVar)) {
                    q.this.d(xVar);
                }
            }
        }, true));
        this.l.l().add(a.a("ImageView.set_bounds", (z) new z() {
            public void a(x xVar) {
                if (q.this.a(xVar)) {
                    q.this.b(xVar);
                }
            }
        }, true));
        this.l.l().add(a.a("ImageView.set_image", (z) new z() {
            public void a(x xVar) {
                if (q.this.a(xVar)) {
                    q.this.c(xVar);
                }
            }
        }, true));
        this.l.m().add("ImageView.set_visible");
        this.l.m().add("ImageView.set_bounds");
        this.l.m().add("ImageView.set_image");
    }

    /* access modifiers changed from: private */
    public void b(x xVar) {
        JSONObject c2 = xVar.c();
        this.b = s.c(c2, "x");
        this.c = s.c(c2, "y");
        this.d = s.c(c2, "width");
        this.e = s.c(c2, "height");
        if (this.f) {
            float r = (((float) this.e) * a.a().m().r()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * r);
            this.d = (int) (((float) getDrawable().getIntrinsicWidth()) * r);
            this.b -= this.d;
            this.c -= this.e;
        }
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.width = this.d;
        layoutParams.height = this.e;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void c(x xVar) {
        this.i = s.b(xVar.c(), "filepath");
        setImageURI(Uri.fromFile(new File(this.i)));
    }

    /* access modifiers changed from: private */
    public void d(x xVar) {
        if (s.d(xVar.c(), TJAdUnitConstants.String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    /* access modifiers changed from: package-private */
    public int[] b() {
        return new int[]{this.b, this.c, this.d, this.e};
    }
}
