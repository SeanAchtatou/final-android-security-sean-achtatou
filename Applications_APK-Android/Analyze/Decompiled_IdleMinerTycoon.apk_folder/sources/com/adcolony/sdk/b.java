package com.adcolony.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.adcolony.sdk.u;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

class b extends Activity {
    static final int a = 0;
    static final int b = 1;
    c c;
    int d = -1;
    String e;
    int f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    boolean l;

    b() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.b$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!a.b() || a.a().t() == null) {
            finish();
            return;
        }
        h a2 = a.a();
        this.i = false;
        this.c = a2.t();
        this.c.b(false);
        if (ak.g()) {
            this.c.b(true);
        }
        this.e = this.c.b();
        this.f = this.c.c();
        this.j = a2.d().getMultiWindowEnabled();
        if (this.j) {
            getWindow().addFlags(2048);
            getWindow().clearFlags(1024);
        } else {
            getWindow().addFlags(1024);
            getWindow().clearFlags(2048);
        }
        requestWindowFeature(1);
        getWindow().getDecorView().setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        if (a2.d().getKeepScreenOn()) {
            getWindow().addFlags(128);
        }
        ViewParent parent = this.c.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.c);
        }
        setContentView(this.c);
        this.c.l().add(a.a("AdSession.finish_fullscreen_ad", (z) new z() {
            public void a(x xVar) {
                b.this.a(xVar);
            }
        }, true));
        this.c.m().add("AdSession.finish_fullscreen_ad");
        a(this.d);
        if (!this.c.r()) {
            JSONObject a3 = s.a();
            s.a(a3, "id", this.c.b());
            s.b(a3, "screen_width", this.c.o());
            s.b(a3, "screen_height", this.c.n());
            new u.a().a("AdSession.on_fullscreen_ad_started").a(u.b);
            new x("AdSession.on_fullscreen_ad_started", this.c.c(), a3).b();
            this.c.c(true);
            return;
        }
        a();
    }

    public void onPause() {
        super.onPause();
        a(this.h);
        this.h = false;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        Iterator<Map.Entry<Integer, al>> it = this.c.e().entrySet().iterator();
        while (it.hasNext() && !isFinishing()) {
            al alVar = (al) it.next().getValue();
            if (!alVar.j() && alVar.i().isPlaying()) {
                alVar.f();
            }
        }
        AdColonyInterstitial v = a.a().v();
        if (v != null && v.g() && v.h().e() != null && z && this.k) {
            v.h().a(CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
        }
    }

    public void onResume() {
        super.onResume();
        a();
        b(this.h);
        this.h = true;
        this.l = true;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        for (Map.Entry<Integer, al> value : this.c.e().entrySet()) {
            al alVar = (al) value.getValue();
            if (!alVar.j() && !alVar.i().isPlaying() && !a.a().r().c()) {
                alVar.e();
            }
        }
        AdColonyInterstitial v = a.a().v();
        if (v != null && v.g() && v.h().e() != null) {
            if ((!z || (z && !this.k)) && this.l) {
                v.h().a(CampaignEx.JSON_NATIVE_VIDEO_RESUME);
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (z && this.h) {
            a.a().k().c(true);
            b(this.h);
            this.k = true;
        } else if (!z && this.h) {
            new u.a().a("Activity is active but window does not have focus, pausing.").a(u.d);
            a.a().k().b(true);
            a(this.h);
            this.k = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (a.b() && this.c != null && !this.g) {
            if ((Build.VERSION.SDK_INT < 24 || !ak.g()) && !this.c.q()) {
                JSONObject a2 = s.a();
                s.a(a2, "id", this.c.b());
                new x("AdSession.on_error", this.c.c(), a2).b();
                this.i = true;
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this instanceof AdColonyInterstitialActivity) {
            a();
        } else {
            ((AdColonyAdViewActivity) this).c();
        }
    }

    public void onBackPressed() {
        JSONObject a2 = s.a();
        s.a(a2, "id", this.c.b());
        new x("AdSession.on_back_button", this.c.c(), a2).b();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        h a2 = a.a();
        if (this.c == null) {
            this.c = a2.t();
        }
        if (this.c != null) {
            this.c.b(false);
            if (ak.g()) {
                this.c.b(true);
            }
            int s = a2.m().s();
            int t = this.j ? a2.m().t() - ak.c(a.c()) : a2.m().t();
            if (s > 0 && t > 0) {
                JSONObject a3 = s.a();
                JSONObject a4 = s.a();
                float r = a2.m().r();
                s.b(a4, "width", (int) (((float) s) / r));
                s.b(a4, "height", (int) (((float) t) / r));
                s.b(a4, "app_orientation", ak.j(ak.h()));
                s.b(a4, "x", 0);
                s.b(a4, "y", 0);
                s.a(a4, "ad_session_id", this.c.b());
                s.b(a3, "screen_width", s);
                s.b(a3, "screen_height", t);
                s.a(a3, "ad_session_id", this.c.b());
                s.b(a3, "id", this.c.d());
                this.c.setLayoutParams(new FrameLayout.LayoutParams(s, t));
                this.c.b(s);
                this.c.a(t);
                new x("MRAID.on_size_change", this.c.c(), a4).b();
                new x("AdContainer.on_orientation_change", this.c.c(), a3).b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(x xVar) {
        int c2 = s.c(xVar.c(), "status");
        if ((c2 == 5 || c2 == 0 || c2 == 6 || c2 == 1) && !this.g) {
            h a2 = a.a();
            k r = a2.r();
            a2.b(xVar);
            if (r.b() != null) {
                r.b().dismiss();
                r.a((AlertDialog) null);
            }
            if (!this.i) {
                finish();
            }
            this.g = true;
            ((ViewGroup) getWindow().getDecorView()).removeAllViews();
            a2.c(false);
            JSONObject a3 = s.a();
            s.a(a3, "id", this.c.b());
            new x("AdSession.on_close", this.c.c(), a3).b();
            a2.a((c) null);
            a2.a((AdColonyInterstitial) null);
            a2.a((AdColonyAdView) null);
            a.a().l().c().remove(this.c.b());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        switch (i2) {
            case 0:
                setRequestedOrientation(7);
                break;
            case 1:
                setRequestedOrientation(6);
                break;
            default:
                setRequestedOrientation(4);
                break;
        }
        this.d = i2;
    }
}
