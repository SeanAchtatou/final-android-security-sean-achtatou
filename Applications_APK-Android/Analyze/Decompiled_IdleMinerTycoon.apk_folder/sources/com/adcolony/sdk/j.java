package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.security.NetworkSecurityPolicy;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.adcolony.sdk.u;
import com.facebook.internal.NativeProtocol;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TapjoyConstants;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

class j {
    static final String a = "Production";
    private static int g = 2;
    private String b = "";
    /* access modifiers changed from: private */
    public String c;
    private boolean d;
    private boolean e;
    private JSONObject f = s.a();
    private String h = "android";
    private String i = "android_native";
    private String j = "";

    /* access modifiers changed from: package-private */
    public String H() {
        return "4.1.2";
    }

    /* access modifiers changed from: package-private */
    public String z() {
        return "";
    }

    j() {
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        this.f = jSONObject;
    }

    /* access modifiers changed from: package-private */
    public JSONObject a() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"HardwareIds"})
    public String b() {
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        return Settings.Secure.getString(c2.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return System.getProperty("os.arch").toLowerCase(Locale.ENGLISH);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.d = false;
        a.a("Device.get_info", new z() {
            public void a(final x xVar) {
                ak.a(new Runnable() {
                    public void run() {
                        try {
                            if (j.this.u() < 14) {
                                new a(xVar, false).execute(new Void[0]);
                            } else {
                                new a(xVar, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                            }
                        } catch (RuntimeException unused) {
                            new u.a().a("Error retrieving device info, disabling AdColony.").a(u.h);
                            AdColony.disable();
                        }
                    }
                });
            }
        });
        a.a("Device.application_exists", new z() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
             arg types: [org.json.JSONObject, java.lang.String, int]
             candidates:
              com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
              com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
            public void a(x xVar) {
                JSONObject a2 = s.a();
                s.b(a2, IronSourceConstants.EVENTS_RESULT, ak.a(s.b(xVar.c(), "name")));
                s.b(a2, "success", true);
                xVar.a(a2).b();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return w() ? "tablet" : "phone";
    }

    /* access modifiers changed from: package-private */
    public String g() {
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        return Settings.Secure.getString(c2.getContentResolver(), "advertising_id");
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        try {
            if (Settings.Secure.getInt(c2.getContentResolver(), "limit_ad_tracking") != 0) {
                return true;
            }
            return false;
        } catch (Settings.SettingNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.e = z;
    }

    /* access modifiers changed from: package-private */
    public String j() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.j = str;
    }

    /* access modifiers changed from: package-private */
    public String k() {
        String str;
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        TelephonyManager telephonyManager = (TelephonyManager) c2.getSystemService("phone");
        if (telephonyManager == null) {
            str = "";
        } else {
            str = telephonyManager.getNetworkOperatorName();
        }
        return str.length() == 0 ? "unknown" : str;
    }

    /* access modifiers changed from: package-private */
    public int l() {
        ActivityManager activityManager;
        Context c2 = a.c();
        if (c2 == null || (activityManager = (ActivityManager) c2.getSystemService("activity")) == null) {
            return 0;
        }
        return activityManager.getMemoryClass();
    }

    /* access modifiers changed from: package-private */
    public String m() {
        TelephonyManager telephonyManager;
        Context c2 = a.c();
        if (c2 == null || (telephonyManager = (TelephonyManager) c2.getSystemService("phone")) == null) {
            return "";
        }
        return telephonyManager.getSimCountryIso();
    }

    /* access modifiers changed from: package-private */
    public String n() {
        return TimeZone.getDefault().getID();
    }

    /* access modifiers changed from: package-private */
    public int o() {
        return TimeZone.getDefault().getOffset(15) / 60000;
    }

    /* access modifiers changed from: package-private */
    public int p() {
        TimeZone timeZone = TimeZone.getDefault();
        if (!timeZone.inDaylightTime(new Date())) {
            return 0;
        }
        return timeZone.getDSTSavings() / 60000;
    }

    /* access modifiers changed from: package-private */
    public long q() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.totalMemory() - runtime.freeMemory()) / ((long) 1048576);
    }

    /* access modifiers changed from: package-private */
    public float r() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0.0f;
        }
        return c2.getResources().getDisplayMetrics().density;
    }

    /* access modifiers changed from: package-private */
    public int s() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) c2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.widthPixels;
    }

    /* access modifiers changed from: package-private */
    public int t() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) c2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.heightPixels;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return Build.VERSION.SDK_INT;
    }

    /* access modifiers changed from: package-private */
    public double v() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0.0d;
        }
        try {
            Intent registerReceiver = c2.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver == null) {
                return 0.0d;
            }
            int intExtra = registerReceiver.getIntExtra("level", -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra < 0 || intExtra2 < 0) {
                return 0.0d;
            }
            double d2 = (double) intExtra;
            double d3 = (double) intExtra2;
            Double.isNaN(d2);
            Double.isNaN(d3);
            return d2 / d3;
        } catch (IllegalArgumentException unused) {
            return 0.0d;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean w() {
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        DisplayMetrics displayMetrics = c2.getResources().getDisplayMetrics();
        float f2 = ((float) displayMetrics.widthPixels) / displayMetrics.xdpi;
        float f3 = ((float) displayMetrics.heightPixels) / displayMetrics.ydpi;
        if (Math.sqrt((double) ((f2 * f2) + (f3 * f3))) >= 6.0d) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public String x() {
        return Locale.getDefault().getLanguage();
    }

    /* access modifiers changed from: package-private */
    public String y() {
        return Locale.getDefault().getCountry();
    }

    /* access modifiers changed from: package-private */
    public boolean A() {
        int i2;
        Context c2 = a.c();
        if (c2 == null || Build.VERSION.SDK_INT < 29 || (i2 = c2.getResources().getConfiguration().uiMode & 48) == 16 || i2 != 32) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public String B() {
        return Build.MANUFACTURER;
    }

    /* access modifiers changed from: package-private */
    public String C() {
        return Build.MODEL;
    }

    /* access modifiers changed from: package-private */
    public String D() {
        return Build.VERSION.RELEASE;
    }

    /* access modifiers changed from: package-private */
    public boolean E() {
        return Build.VERSION.SDK_INT < 23 || NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
    }

    /* access modifiers changed from: package-private */
    public int F() {
        Context c2 = a.c();
        if (c2 == null) {
            return 2;
        }
        switch (c2.getResources().getConfiguration().orientation) {
            case 1:
                return 0;
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    /* access modifiers changed from: package-private */
    public int G() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) c2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.densityDpi;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = com.adcolony.sdk.a.c();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String I() {
        /*
            r2 = this;
            java.lang.String r0 = r2.c
            if (r0 != 0) goto L_0x0012
            android.content.Context r0 = com.adcolony.sdk.a.c()
            if (r0 == 0) goto L_0x0012
            com.adcolony.sdk.j$3 r1 = new com.adcolony.sdk.j$3
            r1.<init>(r0)
            com.adcolony.sdk.ak.a(r1)
        L_0x0012:
            java.lang.String r0 = r2.c
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.j.I():java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public String J() {
        Context c2 = a.c();
        return c2 == null ? "unknown" : c2.getPackageName();
    }

    /* access modifiers changed from: package-private */
    public boolean K() {
        if (!a.d()) {
            return false;
        }
        int F = F();
        switch (F) {
            case 0:
                if (g == 1) {
                    new u.a().a("Sending device info update").a(u.d);
                    g = F;
                    if (u() < 14) {
                        new a(null, true).execute(new Void[0]);
                    } else {
                        new a(null, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                    return true;
                }
                break;
            case 1:
                if (g == 0) {
                    new u.a().a("Sending device info update").a(u.d);
                    g = F;
                    if (u() < 14) {
                        new a(null, true).execute(new Void[0]);
                    } else {
                        new a(null, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                    return true;
                }
                break;
        }
        return false;
    }

    private static class a extends AsyncTask<Void, Void, JSONObject> {
        private x a;
        private boolean b;

        a(x xVar, boolean z) {
            this.a = xVar;
            this.b = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JSONObject doInBackground(Void... voidArr) {
            if (Build.VERSION.SDK_INT < 14) {
                return null;
            }
            return a.a().m().L();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(JSONObject jSONObject) {
            if (this.b) {
                new x("Device.update_info", 1, jSONObject).b();
            } else {
                this.a.a(jSONObject).b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject L() {
        JSONObject a2 = s.a();
        h a3 = a.a();
        s.a(a2, TapjoyConstants.TJC_CARRIER_NAME, k());
        s.a(a2, "data_path", a.a().o().e());
        s.b(a2, "device_api", u());
        s.b(a2, "display_width", s());
        s.b(a2, "display_height", t());
        s.b(a2, "screen_width", s());
        s.b(a2, "screen_height", t());
        s.b(a2, "display_dpi", G());
        s.a(a2, TapjoyConstants.TJC_DEVICE_TYPE_NAME, f());
        s.a(a2, "locale_language_code", x());
        s.a(a2, "ln", x());
        s.a(a2, "locale_country_code", y());
        s.a(a2, "locale", y());
        s.a(a2, TapjoyConstants.TJC_DEVICE_MAC_ADDRESS, z());
        s.a(a2, "manufacturer", B());
        s.a(a2, "device_brand", B());
        s.a(a2, "media_path", a.a().o().d());
        s.a(a2, "temp_storage_path", a.a().o().f());
        s.b(a2, "memory_class", l());
        s.b(a2, "network_speed", 20);
        s.a(a2, "memory_used_mb", q());
        s.a(a2, "model", C());
        s.a(a2, "device_model", C());
        s.a(a2, TapjoyConstants.TJC_SDK_TYPE, this.i);
        s.a(a2, "sdk_version", H());
        s.a(a2, "network_type", a3.d.c());
        s.a(a2, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, D());
        s.a(a2, "os_name", this.h);
        s.a(a2, TapjoyConstants.TJC_PLATFORM, this.h);
        s.a(a2, "arch", d());
        s.a(a2, "user_id", s.b(a3.d().d, "user_id"));
        s.a(a2, "app_id", a3.d().a);
        s.a(a2, "app_bundle_name", ak.d());
        s.a(a2, "app_bundle_version", ak.b());
        s.a(a2, "battery_level", v());
        s.a(a2, "cell_service_country_code", m());
        s.a(a2, "timezone_ietf", n());
        s.b(a2, "timezone_gmt_m", o());
        s.b(a2, "timezone_dst_m", p());
        s.a(a2, "launch_metadata", a());
        s.a(a2, "controller_version", a3.b());
        g = F();
        s.b(a2, "current_orientation", g);
        s.b(a2, "cleartext_permitted", E());
        s.a(a2, "density", (double) r());
        s.b(a2, "dark_mode", A());
        JSONArray b2 = s.b();
        if (ak.a("com.android.vending")) {
            b2.put("google");
        }
        if (ak.a("com.amazon.venezia")) {
            b2.put("amazon");
        }
        s.a(a2, "available_stores", b2);
        s.a(a2, NativeProtocol.RESULT_ARGS_PERMISSIONS, ak.d(a.c()));
        int i2 = 40;
        while (!this.d && i2 > 0) {
            try {
                Thread.sleep(50);
                i2--;
            } catch (Exception unused) {
            }
        }
        s.a(a2, "advertiser_id", c());
        s.b(a2, "limit_tracking", i());
        if (c() == null || c().equals("")) {
            s.a(a2, "android_id_sha1", ak.c(b()));
        }
        return a2;
    }
}
