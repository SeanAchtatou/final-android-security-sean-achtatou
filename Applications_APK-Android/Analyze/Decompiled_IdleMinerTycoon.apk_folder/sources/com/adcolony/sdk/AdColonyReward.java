package com.adcolony.sdk;

import com.mintegral.msdk.base.entity.CampaignEx;
import org.json.JSONObject;

public class AdColonyReward {
    private int a;
    private String b;
    private String c;
    private boolean d;

    AdColonyReward(x xVar) {
        JSONObject c2 = xVar.c();
        this.a = s.c(c2, CampaignEx.JSON_KEY_REWARD_AMOUNT);
        this.b = s.b(c2, CampaignEx.JSON_KEY_REWARD_NAME);
        this.d = s.d(c2, "success");
        this.c = s.b(c2, "zone_id");
    }

    public int getRewardAmount() {
        return this.a;
    }

    public String getRewardName() {
        return this.b;
    }

    public String getZoneID() {
        return this.c;
    }

    public boolean success() {
        return this.d;
    }
}
