package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import com.adcolony.sdk.ak;
import com.adcolony.sdk.u;
import com.appsflyer.ServerParameters;
import com.helpshift.configuration.domainmodel.SDKConfigurationDM;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

public class AdColony {
    static ExecutorService a = Executors.newSingleThreadExecutor();

    public static boolean disable() {
        if (!a.e()) {
            return false;
        }
        Context c = a.c();
        if (c != null && (c instanceof b)) {
            ((Activity) c).finish();
        }
        final h a2 = a.a();
        for (final AdColonyInterstitial next : a2.l().c().values()) {
            ak.a(new Runnable() {
                public void run() {
                    AdColonyInterstitialListener listener = next.getListener();
                    next.a(true);
                    if (listener != null) {
                        listener.onExpiring(next);
                    }
                }
            });
        }
        ak.a(new Runnable() {
            public void run() {
                ArrayList arrayList = new ArrayList();
                Iterator<aa> it = a2.q().c().iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next());
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    aa aaVar = (aa) it2.next();
                    a2.a(aaVar.a());
                    if (aaVar instanceof am) {
                        am amVar = (am) aaVar;
                        if (!amVar.m()) {
                            amVar.loadUrl("about:blank");
                            amVar.clearCache(true);
                            amVar.removeAllViews();
                            amVar.a(true);
                        }
                    }
                }
            }
        });
        a.a().a(true);
        return true;
    }

    public static boolean configure(Activity activity, @NonNull String str, @NonNull String... strArr) {
        return a(activity, null, str, strArr);
    }

    public static boolean configure(Activity activity, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        return a(activity, adColonyAppOptions, str, strArr);
    }

    public static boolean configure(Application application, @NonNull String str, @NonNull String... strArr) {
        return configure(application, (AdColonyAppOptions) null, str, strArr);
    }

    public static boolean configure(Application application, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        return a(application, adColonyAppOptions, str, strArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
     arg types: [android.content.Context, com.adcolony.sdk.AdColonyAppOptions, int]
     candidates:
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray
     arg types: [org.json.JSONArray, java.lang.String[], int]
     candidates:
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, int):int
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, double):boolean
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, long):boolean
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, java.lang.String):boolean
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, org.json.JSONArray):boolean
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, org.json.JSONObject):boolean
      com.adcolony.sdk.s.a(org.json.JSONObject, java.lang.String, boolean):boolean
      com.adcolony.sdk.s.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray */
    private static boolean a(Context context, AdColonyAppOptions adColonyAppOptions, @NonNull String str, @NonNull String... strArr) {
        if (ad.a(0, null)) {
            new u.a().a("Cannot configure AdColony; configuration mechanism requires 5 ").a("seconds between attempts.").a(u.e);
            return false;
        }
        if (context == null) {
            context = a.c();
        }
        if (context == null) {
            new u.a().a("Ignoring call to AdColony.configure() as the provided Activity or ").a("Application context is null and we do not currently hold a ").a("reference to either for our use.").a(u.e);
            return false;
        }
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (adColonyAppOptions == null) {
            adColonyAppOptions = new AdColonyAppOptions();
        }
        if (a.b() && !s.d(a.a().d().d(), "reconfigurable")) {
            h a2 = a.a();
            if (!a2.d().a().equals(str)) {
                new u.a().a("Ignoring call to AdColony.configure() as the app id does not ").a("match what was used during the initial configuration.").a(u.e);
                return false;
            } else if (ak.a(strArr, a2.d().b())) {
                new u.a().a("Ignoring call to AdColony.configure() as the same zone ids ").a("were used during the previous configuration.").a(u.e);
                return true;
            }
        }
        adColonyAppOptions.a(str);
        adColonyAppOptions.a(strArr);
        adColonyAppOptions.f();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:SSS", Locale.US);
        long currentTimeMillis = System.currentTimeMillis();
        String format = simpleDateFormat.format(new Date(currentTimeMillis));
        boolean z = true;
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && !strArr[i].equals("")) {
                z = false;
            }
        }
        if (str.equals("") || z) {
            new u.a().a("AdColony.configure() called with an empty app or zone id String.").a(u.g);
            return false;
        }
        a.a = true;
        if (Build.VERSION.SDK_INT < 14) {
            new u.a().a("The minimum API level for the AdColony SDK is 14.").a(u.e);
            a.a(context, adColonyAppOptions, true);
        } else {
            a.a(context, adColonyAppOptions, false);
        }
        String str2 = a.a().o().c() + "/adc3/AppInfo";
        JSONObject a3 = s.a();
        if (new File(str2).exists()) {
            a3 = s.c(str2);
        }
        JSONObject a4 = s.a();
        if (s.b(a3, "appId").equals(str)) {
            s.a(a4, "zoneIds", s.a(s.g(a3, "zoneIds"), strArr, true));
            s.a(a4, "appId", str);
        } else {
            s.a(a4, "zoneIds", s.a(strArr));
            s.a(a4, "appId", str);
        }
        s.h(a4, str2);
        new u.a().a("Configure: Total Time (ms): ").a("" + (System.currentTimeMillis() - currentTimeMillis)).a(" and started at " + format).a(u.f);
        return true;
    }

    public static AdColonyZone getZone(@NonNull String str) {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.getZone() as AdColony has not yet been ").a("configured.").a(u.e);
            return null;
        }
        HashMap<String, AdColonyZone> f = a.a().f();
        if (f.containsKey(str)) {
            return f.get(str);
        }
        AdColonyZone adColonyZone = new AdColonyZone(str);
        a.a().f().put(str, adColonyZone);
        return adColonyZone;
    }

    public static boolean notifyIAPComplete(@NonNull String str, @NonNull String str2) {
        return notifyIAPComplete(str, str2, null, 0.0d);
    }

    public static boolean notifyIAPComplete(@NonNull String str, @NonNull String str2, String str3, @FloatRange(from = 0.0d) double d) {
        if (!a.e()) {
            new u.a().a("Ignoring call to notifyIAPComplete as AdColony has not yet been ").a("configured.").a(u.e);
            return false;
        } else if (!ak.d(str) || !ak.d(str2)) {
            new u.a().a("Ignoring call to notifyIAPComplete as one of the passed Strings ").a("is greater than ").a(128).a(" characters.").a(u.e);
            return false;
        } else {
            if (str3 != null && str3.length() > 3) {
                new u.a().a("You are trying to report an IAP event with a currency String ").a("containing more than 3 characters.").a(u.e);
            }
            final double d2 = d;
            final String str4 = str3;
            final String str5 = str;
            final String str6 = str2;
            a.execute(new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = s.a();
                    if (d2 >= 0.0d) {
                        s.a(a2, "price", d2);
                    }
                    if (str4 != null && str4.length() <= 3) {
                        s.a(a2, "currency_code", str4);
                    }
                    s.a(a2, "product_id", str5);
                    s.a(a2, "transaction_id", str6);
                    new x("AdColony.on_iap_report", 1, a2).b();
                }
            });
            return true;
        }
    }

    public static boolean requestAdView(@NonNull String str, @NonNull AdColonyAdViewListener adColonyAdViewListener, @NonNull AdColonyAdSize adColonyAdSize) {
        return requestAdView(str, adColonyAdViewListener, adColonyAdSize, null);
    }

    public static boolean requestAdView(@NonNull final String str, @NonNull final AdColonyAdViewListener adColonyAdViewListener, @NonNull final AdColonyAdSize adColonyAdSize, final AdColonyAdOptions adColonyAdOptions) {
        if (!a.e()) {
            new u.a().a("Ignoring call to requestAdView as AdColony has not yet been").a(" configured.").a(u.e);
            a(adColonyAdViewListener, str);
            return false;
        } else if (adColonyAdSize.getHeight() <= 0 || adColonyAdSize.getWidth() <= 0) {
            new u.a().a("Ignoring call to requestAdView as you've provided an AdColonyAdSize").a(" object with an invalid width or height.").a(u.e);
            return false;
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("zone_id", str);
            if (ad.a(1, bundle)) {
                a(adColonyAdViewListener, str);
                return false;
            }
            try {
                a.execute(new Runnable() {
                    public void run() {
                        h a2 = a.a();
                        if (a2.g() || a2.h()) {
                            AdColony.b();
                            AdColony.a(adColonyAdViewListener, str);
                        }
                        if (!AdColony.a() && a.d()) {
                            AdColony.a(adColonyAdViewListener, str);
                        }
                        if (a2.f().get(str) == null) {
                            new AdColonyZone(str);
                            new u.a().a("Zone info for ").a(str).a(" doesn't exist in hashmap").a(u.b);
                        }
                        a2.l().a(str, adColonyAdViewListener, adColonyAdSize, adColonyAdOptions);
                    }
                });
                return true;
            } catch (RejectedExecutionException unused) {
                a(adColonyAdViewListener, str);
                return false;
            }
        }
    }

    public static boolean setAppOptions(@NonNull final AdColonyAppOptions adColonyAppOptions) {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.setAppOptions() as AdColony has not yet").a(" been configured.").a(u.e);
            return false;
        }
        a.a().b(adColonyAppOptions);
        adColonyAppOptions.f();
        try {
            a.execute(new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = s.a();
                    s.a(a2, "options", adColonyAppOptions.d());
                    new x("Options.set_options", 1, a2).b();
                }
            });
            return true;
        } catch (RejectedExecutionException unused) {
            return false;
        }
    }

    public static AdColonyAppOptions getAppOptions() {
        if (!a.e()) {
            return null;
        }
        return a.a().d();
    }

    public static boolean setRewardListener(@NonNull AdColonyRewardListener adColonyRewardListener) {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.setRewardListener() as AdColony has not").a(" yet been configured.").a(u.e);
            return false;
        }
        a.a().a(adColonyRewardListener);
        return true;
    }

    public static boolean removeRewardListener() {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.removeRewardListener() as AdColony has ").a("not yet been configured.").a(u.e);
            return false;
        }
        a.a().a((AdColonyRewardListener) null);
        return true;
    }

    public static String getSDKVersion() {
        if (!a.e()) {
            return "";
        }
        return a.a().m().H();
    }

    public static AdColonyRewardListener getRewardListener() {
        if (!a.e()) {
            return null;
        }
        return a.a().i();
    }

    public static boolean addCustomMessageListener(@NonNull AdColonyCustomMessageListener adColonyCustomMessageListener, final String str) {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.addCustomMessageListener as AdColony ").a("has not yet been configured.").a(u.e);
            return false;
        } else if (!ak.d(str)) {
            new u.a().a("Ignoring call to AdColony.addCustomMessageListener.").a(u.e);
            return false;
        } else {
            try {
                a.a().A().put(str, adColonyCustomMessageListener);
                a.execute(new Runnable() {
                    public void run() {
                        AdColony.a();
                        JSONObject a2 = s.a();
                        s.a(a2, "type", str);
                        new x("CustomMessage.register", 1, a2).b();
                    }
                });
                return true;
            } catch (RejectedExecutionException unused) {
                return false;
            }
        }
    }

    public static AdColonyCustomMessageListener getCustomMessageListener(@NonNull String str) {
        if (!a.e()) {
            return null;
        }
        return a.a().A().get(str);
    }

    public static boolean removeCustomMessageListener(@NonNull final String str) {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.removeCustomMessageListener as AdColony").a(" has not yet been configured.").a(u.e);
            return false;
        }
        a.a().A().remove(str);
        a.execute(new Runnable() {
            public void run() {
                AdColony.a();
                JSONObject a2 = s.a();
                s.a(a2, "type", str);
                new x("CustomMessage.unregister", 1, a2).b();
            }
        });
        return true;
    }

    public static boolean clearCustomMessageListeners() {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.clearCustomMessageListeners as AdColony").a(" has not yet been configured.").a(u.e);
            return false;
        }
        a.a().A().clear();
        a.execute(new Runnable() {
            public void run() {
                AdColony.a();
                for (String a : a.a().A().keySet()) {
                    JSONObject a2 = s.a();
                    s.a(a2, "type", a);
                    new x("CustomMessage.unregister", 1, a2).b();
                }
            }
        });
        return true;
    }

    public static boolean requestInterstitial(@NonNull String str, @NonNull AdColonyInterstitialListener adColonyInterstitialListener) {
        return requestInterstitial(str, adColonyInterstitialListener, null);
    }

    public static boolean requestInterstitial(@NonNull final String str, @NonNull final AdColonyInterstitialListener adColonyInterstitialListener, final AdColonyAdOptions adColonyAdOptions) {
        if (!a.e()) {
            new u.a().a("Ignoring call to AdColony.requestInterstitial as AdColony has not").a(" yet been configured.").a(u.e);
            adColonyInterstitialListener.onRequestNotFilled(new AdColonyZone(str));
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("zone_id", str);
        if (ad.a(1, bundle)) {
            AdColonyZone adColonyZone = a.a().f().get(str);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(str);
                u.a a2 = new u.a().a("Zone info for ");
                a2.a(str + " doesn't exist in hashmap").a(u.b);
            }
            adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
            return false;
        }
        try {
            a.execute(new Runnable() {
                public void run() {
                    h a2 = a.a();
                    if (a2.g() || a2.h()) {
                        AdColony.b();
                        AdColony.a(adColonyInterstitialListener, str);
                    } else if (AdColony.a() || !a.d()) {
                        final AdColonyZone adColonyZone = a2.f().get(str);
                        if (adColonyZone == null) {
                            adColonyZone = new AdColonyZone(str);
                            u.a a3 = new u.a().a("Zone info for ");
                            a3.a(str + " doesn't exist in hashmap").a(u.b);
                        }
                        if (adColonyZone.getZoneType() == 2 || adColonyZone.getZoneType() == 1) {
                            ak.a(new Runnable() {
                                public void run() {
                                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                                }
                            });
                        } else {
                            a2.l().a(str, adColonyInterstitialListener, adColonyAdOptions);
                        }
                    } else {
                        AdColony.a(adColonyInterstitialListener, str);
                    }
                }
            });
            return true;
        } catch (RejectedExecutionException unused) {
            a(adColonyInterstitialListener, str);
            return false;
        }
    }

    static boolean a() {
        ak.a aVar = new ak.a(15.0d);
        h a2 = a.a();
        while (!a2.B() && !aVar.b()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException unused) {
            }
        }
        return a2.B();
    }

    static boolean a(final AdColonyInterstitialListener adColonyInterstitialListener, final String str) {
        if (adColonyInterstitialListener == null || !a.d()) {
            return false;
        }
        ak.a(new Runnable() {
            public void run() {
                AdColonyZone adColonyZone = a.a().f().get(str);
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(str);
                }
                adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
            }
        });
        return false;
    }

    static boolean a(final AdColonyAdViewListener adColonyAdViewListener, final String str) {
        if (adColonyAdViewListener == null || !a.d()) {
            return false;
        }
        ak.a(new Runnable() {
            public void run() {
                AdColonyZone adColonyZone;
                if (!a.b()) {
                    adColonyZone = null;
                } else {
                    adColonyZone = a.a().f().get(str);
                }
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(str);
                }
                adColonyAdViewListener.onRequestNotFilled(adColonyZone);
            }
        });
        return false;
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions) {
        if (adColonyAppOptions != null && context != null) {
            String b = ak.b(context);
            String b2 = ak.b();
            int c = ak.c();
            String k = a.a().m().k();
            String str = Constants.ParametersKeys.ORIENTATION_NONE;
            if (a.a().p().a()) {
                str = "wifi";
            } else if (a.a().p().b()) {
                str = TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("sessionId", "unknown");
            hashMap.put(ServerParameters.ADVERTISING_ID_PARAM, "unknown");
            hashMap.put("countryLocale", Locale.getDefault().getDisplayLanguage() + " (" + Locale.getDefault().getDisplayCountry() + ")");
            hashMap.put("countryLocalShort", a.a().m().y());
            hashMap.put("manufacturer", a.a().m().B());
            hashMap.put("model", a.a().m().C());
            hashMap.put("osVersion", a.a().m().D());
            hashMap.put("carrierName", k);
            hashMap.put("networkType", str);
            hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
            hashMap.put("appName", b);
            hashMap.put(Constants.RequestParameters.APPLICATION_VERSION_NAME, b2);
            hashMap.put("appBuildNumber", Integer.valueOf(c));
            hashMap.put("appId", "" + adColonyAppOptions.a());
            hashMap.put("apiLevel", Integer.valueOf(Build.VERSION.SDK_INT));
            hashMap.put(GeneralPropertiesWorker.SDK_VERSION, a.a().m().H());
            hashMap.put("controllerVersion", "unknown");
            hashMap.put("zoneIds", adColonyAppOptions.c());
            JSONObject mediationInfo = adColonyAppOptions.getMediationInfo();
            JSONObject pluginInfo = adColonyAppOptions.getPluginInfo();
            if (!s.b(mediationInfo, "mediation_network").equals("")) {
                hashMap.put("mediationNetwork", s.b(mediationInfo, "mediation_network"));
                hashMap.put("mediationNetworkVersion", s.b(mediationInfo, "mediation_network_version"));
            }
            if (!s.b(pluginInfo, TapjoyConstants.TJC_PLUGIN).equals("")) {
                hashMap.put(TapjoyConstants.TJC_PLUGIN, s.b(pluginInfo, TapjoyConstants.TJC_PLUGIN));
                hashMap.put(SDKConfigurationDM.PLUGIN_VERSION, s.b(pluginInfo, "plugin_version"));
            }
            w.a(hashMap);
        }
    }

    static void b() {
        new u.a().a("The AdColony API is not available while AdColony is disabled.").a(u.g);
    }
}
