package com.adcolony.sdk;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings;
import com.adcolony.sdk.u;
import org.json.JSONObject;

class g extends ContentObserver {
    private AudioManager a;
    private AdColonyInterstitial b;

    public boolean deliverSelfNotifications() {
        return false;
    }

    public g(Handler handler, AdColonyInterstitial adColonyInterstitial) {
        super(handler);
        Context c = a.c();
        if (c != null) {
            this.a = (AudioManager) c.getSystemService("audio");
            this.b = adColonyInterstitial;
            c.getApplicationContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this);
        }
    }

    public void onChange(boolean z) {
        if (this.a != null && this.b != null && this.b.d() != null) {
            double streamVolume = (double) ((((float) this.a.getStreamVolume(3)) / 15.0f) * 100.0f);
            JSONObject a2 = s.a();
            s.a(a2, "audio_percentage", streamVolume);
            s.a(a2, "ad_session_id", this.b.d().b());
            s.b(a2, "id", this.b.d().d());
            new x("AdContainer.on_audio_change", this.b.d().c(), a2).b();
            new u.a().a("Volume changed to ").a(streamVolume).a(u.d);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Context c = a.c();
        if (c != null) {
            c.getApplicationContext().getContentResolver().unregisterContentObserver(this);
        }
        this.b = null;
        this.a = null;
    }
}
