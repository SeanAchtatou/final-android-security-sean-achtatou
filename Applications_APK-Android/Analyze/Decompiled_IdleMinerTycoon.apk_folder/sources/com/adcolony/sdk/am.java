package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adcolony.sdk.u;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.iab.omid.library.adcolony.ScriptInjector;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TJAdUnitConstants;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
class am extends WebView implements aa {
    static boolean a = false;
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    private boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    /* access modifiers changed from: private */
    public JSONArray H = s.b();
    /* access modifiers changed from: private */
    public JSONObject I = s.a();
    private JSONObject J = s.a();
    /* access modifiers changed from: private */
    public c K;
    /* access modifiers changed from: private */
    public x L;
    private ImageView M;
    /* access modifiers changed from: private */
    public final Object N = new Object();
    /* access modifiers changed from: private */
    public String b;
    private String c;
    private String d = "";
    private String e = "";
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g = "";
    private String h = "";
    private String i = "";
    /* access modifiers changed from: private */
    public String j = "";
    private String k = "";
    /* access modifiers changed from: private */
    public int l;
    private int m;
    private int n;
    private int o;
    private int p;
    /* access modifiers changed from: private */
    public int q;
    private int r;
    /* access modifiers changed from: private */
    public int s;
    private int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    private int w;
    private int x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    public void c() {
    }

    private am(Context context) {
        super(context);
    }

    am(Context context, x xVar, int i2, int i3, c cVar) {
        super(context);
        this.L = xVar;
        a(xVar, i2, i3, cVar);
        e();
    }

    am(Context context, int i2, boolean z2) {
        super(context);
        this.u = i2;
        this.A = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.C) {
            new u.a().a("Ignoring call to execute_js as WebView has been destroyed.").a(u.b);
        } else if (Build.VERSION.SDK_INT >= 19) {
            evaluateJavascript(str, null);
        } else {
            loadUrl("javascript:" + str);
        }
    }

    public int a() {
        return this.u;
    }

    public int b() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public boolean a(x xVar) {
        JSONObject c2 = xVar.c();
        return s.c(c2, "id") == this.l && s.c(c2, "container_id") == this.K.d() && s.b(c2, "ad_session_id").equals(this.K.b());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        AdColonyAdView adColonyAdView;
        if (motionEvent.getAction() == 1) {
            if (this.f == null) {
                adColonyAdView = null;
            } else {
                adColonyAdView = a.a().l().e().get(this.f);
            }
            if (adColonyAdView != null && !adColonyAdView.getUserInteraction()) {
                JSONObject a2 = s.a();
                s.a(a2, "ad_session_id", this.f);
                new x("WebView.on_first_click", 1, a2).b();
                adColonyAdView.setUserInteraction(true);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.am.a(boolean, com.adcolony.sdk.x):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.adcolony.sdk.am.a(java.lang.String, java.lang.String):java.lang.String
      com.adcolony.sdk.am.a(com.adcolony.sdk.am, org.json.JSONArray):org.json.JSONArray
      com.adcolony.sdk.am.a(com.adcolony.sdk.am, java.lang.String):void
      com.adcolony.sdk.am.a(org.json.JSONObject, java.lang.String):void
      com.adcolony.sdk.am.a(com.adcolony.sdk.am, boolean):boolean
      com.adcolony.sdk.am.a(boolean, com.adcolony.sdk.x):void */
    /* access modifiers changed from: package-private */
    public void e() {
        a(false, (x) null);
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"AddJavascriptInterface"})
    public void a(boolean z2, x xVar) {
        WebViewClient webViewClient;
        String str;
        String str2;
        if (this.L == null) {
            this.L = xVar;
        }
        final JSONObject c2 = this.L.c();
        this.z = z2;
        this.A = s.d(c2, "is_display_module");
        if (z2) {
            String b2 = s.b(c2, "filepath");
            this.k = s.b(c2, "interstitial_html");
            this.h = s.b(c2, "mraid_filepath");
            this.e = s.b(c2, "base_url");
            this.c = b2;
            this.J = s.f(c2, "iab");
            if (a && this.u == 1) {
                this.c = "android_asset/ADCController.js";
            }
            if (this.k.equals("")) {
                str2 = "file:///" + this.c;
            } else {
                str2 = "";
            }
            this.b = str2;
            this.I = s.f(c2, TJAdUnitConstants.String.VIDEO_INFO);
            this.f = s.b(c2, "ad_session_id");
            this.y = true;
        }
        setFocusable(true);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        if (Build.VERSION.SDK_INT >= 19) {
            setWebContentsDebuggingEnabled(true);
        }
        setWebChromeClient(new WebChromeClient() {
            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                new u.a().a("JS Alert: ").a(str2).a(u.d);
                jsResult.confirm();
                return true;
            }

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                String str;
                ConsoleMessage.MessageLevel messageLevel = consoleMessage.messageLevel();
                String message = consoleMessage.message();
                boolean z = false;
                boolean z2 = messageLevel == ConsoleMessage.MessageLevel.ERROR;
                if (consoleMessage.message().contains("Viewport argument key \"shrink-to-fit\" not recognized and ignored") || consoleMessage.message().contains("Viewport target-densitydpi is not supported.")) {
                    z = true;
                }
                if (message.contains("ADC3_update is not defined") || message.contains("NativeLayer.dispatch_messages is not a function")) {
                    am.this.a(c2, "Unable to communicate with AdColony. Please ensure that you have added an exception for our Javascript interface in your ProGuard configuration and that you do not have a faulty proxy enabled on your device.");
                }
                if (!z && (messageLevel == ConsoleMessage.MessageLevel.WARNING || z2)) {
                    AdColonyInterstitial adColonyInterstitial = null;
                    if (am.this.f != null) {
                        adColonyInterstitial = a.a().l().c().get(am.this.f);
                    }
                    if (adColonyInterstitial == null) {
                        str = "unknown";
                    } else {
                        str = adColonyInterstitial.b();
                    }
                    new u.a().a("onConsoleMessage: ").a(consoleMessage.message()).a(" with ad id: ").a(str).a(z2 ? u.h : u.f);
                }
                return true;
            }
        });
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setGeolocationEnabled(true);
        settings.setUseWideViewPort(true);
        if (Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setAllowFileAccess(true);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            webViewClient = new a() {
                @RequiresApi(api = 24)
                public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                    if (!am.this.B || !webResourceRequest.isForMainFrame()) {
                        return false;
                    }
                    Uri url = webResourceRequest.getUrl();
                    ak.a(new Intent("android.intent.action.VIEW", url));
                    JSONObject a2 = s.a();
                    s.a(a2, "url", url.toString());
                    s.a(a2, "ad_session_id", am.this.f);
                    new x("WebView.redirect_detected", am.this.K.c(), a2).b();
                    ai n = a.a().n();
                    n.b(am.this.f);
                    n.a(am.this.f);
                    return true;
                }

                @RequiresApi(api = 23)
                public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                    am.this.a(webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
                }

                @RequiresApi(api = 23)
                public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                    if (!webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                        return null;
                    }
                    try {
                        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(am.this.g.getBytes("UTF-8")));
                    } catch (UnsupportedEncodingException unused) {
                        new u.a().a("UTF-8 not supported.").a(u.h);
                        return null;
                    }
                }
            };
        } else {
            webViewClient = Build.VERSION.SDK_INT >= 21 ? new a() {
                @RequiresApi(api = 21)
                public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                    if (!webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                        return null;
                    }
                    try {
                        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(am.this.g.getBytes("UTF-8")));
                    } catch (UnsupportedEncodingException unused) {
                        new u.a().a("UTF-8 not supported.").a(u.h);
                        return null;
                    }
                }
            } : new a();
        }
        addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void push_messages(String str) {
                am.this.b(str);
            }

            @JavascriptInterface
            public void dispatch_messages(String str) {
                am.this.b(str);
            }

            @JavascriptInterface
            public String pull_messages() {
                String str = "[]";
                synchronized (am.this.N) {
                    if (am.this.H.length() > 0) {
                        if (am.this.y) {
                            str = am.this.H.toString();
                        }
                        JSONArray unused = am.this.H = s.b();
                    }
                }
                return str;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.adcolony.sdk.am.a(com.adcolony.sdk.am, boolean):boolean
             arg types: [com.adcolony.sdk.am, int]
             candidates:
              com.adcolony.sdk.am.a(java.lang.String, java.lang.String):java.lang.String
              com.adcolony.sdk.am.a(com.adcolony.sdk.am, org.json.JSONArray):org.json.JSONArray
              com.adcolony.sdk.am.a(com.adcolony.sdk.am, java.lang.String):void
              com.adcolony.sdk.am.a(org.json.JSONObject, java.lang.String):void
              com.adcolony.sdk.am.a(boolean, com.adcolony.sdk.x):void
              com.adcolony.sdk.am.a(com.adcolony.sdk.am, boolean):boolean */
            @JavascriptInterface
            public void enable_reverse_messaging() {
                boolean unused = am.this.D = true;
            }
        }, "NativeLayer");
        setWebViewClient(webViewClient);
        if (this.A) {
            try {
                if (this.k.equals("")) {
                    FileInputStream fileInputStream = new FileInputStream(this.c);
                    StringBuilder sb = new StringBuilder(fileInputStream.available());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr, 0, 1024);
                        if (read < 0) {
                            break;
                        }
                        sb.append(new String(bArr, 0, read));
                    }
                    if (this.c.contains(".html")) {
                        str = sb.toString();
                    } else {
                        str = "<html><script>" + sb.toString() + "</script></html>";
                    }
                } else {
                    str = this.k.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", "script src=\"file://" + this.h + "\"");
                }
                String b3 = s.b(s.f(c2, TJAdUnitConstants.String.VIDEO_INFO), "metadata");
                loadDataWithBaseURL(this.b.equals("") ? this.e : this.b, a(str, s.b(s.a(b3), "iab_filepath")).replaceFirst("var\\s*ADC_DEVICE_INFO\\s*=\\s*null\\s*;", Matcher.quoteReplacement("var ADC_DEVICE_INFO = " + b3 + ";")), "text/html", null, null);
            } catch (IOException e2) {
                a(e2);
                return;
            } catch (IllegalArgumentException e3) {
                a(e3);
                return;
            } catch (IndexOutOfBoundsException e4) {
                a(e4);
                return;
            }
        } else if (!this.b.startsWith("http") && !this.b.startsWith(Constants.ParametersKeys.FILE)) {
            loadDataWithBaseURL(this.e, this.b, "text/html", null, null);
        } else if (this.b.contains(".html") || !this.b.startsWith(Constants.ParametersKeys.FILE)) {
            loadUrl(this.b);
        } else {
            loadDataWithBaseURL(this.b, "<html><script src=\"" + this.b + "\"></script></html>", "text/html", null, null);
        }
        if (!z2) {
            f();
            g();
        }
        if (z2 || this.y) {
            a.a().q().a(this);
        }
        if (!this.d.equals("")) {
            a(this.d);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.p.a(java.lang.String, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, int]
     candidates:
      com.adcolony.sdk.p.a(com.adcolony.sdk.p, com.adcolony.sdk.x):boolean
      com.adcolony.sdk.p.a(com.adcolony.sdk.x, java.io.File):boolean
      com.adcolony.sdk.p.a(java.lang.String, boolean):java.lang.StringBuilder */
    private String a(String str, String str2) {
        ac acVar;
        d l2 = a.a().l();
        AdColonyInterstitial adColonyInterstitial = l2.c().get(this.f);
        AdColonyAdViewListener adColonyAdViewListener = l2.d().get(this.f);
        if (adColonyInterstitial != null && this.J.length() > 0 && !s.b(this.J, "ad_type").equals("video")) {
            adColonyInterstitial.a(this.J);
        } else if (adColonyAdViewListener != null && this.J.length() > 0) {
            adColonyAdViewListener.a(new ac(this.J, this.f));
        }
        if (adColonyInterstitial == null) {
            acVar = null;
        } else {
            acVar = adColonyInterstitial.h();
        }
        if (acVar == null && adColonyAdViewListener != null) {
            acVar = adColonyAdViewListener.c();
        }
        if (acVar != null && acVar.c() == 2) {
            this.E = true;
            if (!str2.equals("")) {
                try {
                    return ScriptInjector.injectScriptContentIntoHtml(a.a().j().a(str2, false).toString(), str);
                } catch (IOException e2) {
                    a(e2);
                }
            }
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        JSONArray b2 = s.b(str);
        for (int i2 = 0; i2 < b2.length(); i2++) {
            a.a().q().a(s.d(b2, i2));
        }
    }

    private boolean a(Exception exc) {
        AdColonyInterstitialListener listener;
        new u.a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(s.b(this.I, "metadata")).a(u.h);
        AdColonyInterstitial remove = a.a().l().c().remove(s.b(this.I, "ad_session_id"));
        if (remove == null || (listener = remove.getListener()) == null) {
            return false;
        }
        listener.onExpiring(remove);
        remove.a(true);
        return true;
    }

    private void b(Exception exc) {
        new u.a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(s.b(this.I, "metadata")).a(u.h);
        JSONObject a2 = s.a();
        s.a(a2, "id", this.f);
        new x("AdSession.on_error", this.K.c(), a2).b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.am$7, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.am$8, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.am$9, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.am$10, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* access modifiers changed from: package-private */
    public void f() {
        this.K.l().add(a.a("WebView.set_visible", (z) new z() {
            public void a(final x xVar) {
                if (am.this.a(xVar)) {
                    ak.a(new Runnable() {
                        public void run() {
                            am.this.c(xVar);
                        }
                    });
                }
            }
        }, true));
        this.K.l().add(a.a("WebView.set_bounds", (z) new z() {
            public void a(final x xVar) {
                if (am.this.a(xVar)) {
                    ak.a(new Runnable() {
                        public void run() {
                            am.this.b(xVar);
                        }
                    });
                }
            }
        }, true));
        this.K.l().add(a.a("WebView.execute_js", (z) new z() {
            public void a(final x xVar) {
                if (am.this.a(xVar)) {
                    ak.a(new Runnable() {
                        public void run() {
                            am.this.a(s.b(xVar.c(), "custom_js"));
                        }
                    });
                }
            }
        }, true));
        this.K.l().add(a.a("WebView.set_transparent", (z) new z() {
            public void a(final x xVar) {
                if (am.this.a(xVar)) {
                    ak.a(new Runnable() {
                        public void run() {
                            am.this.b(s.d(xVar.c(), "transparent"));
                        }
                    });
                }
            }
        }, true));
        this.K.m().add("WebView.set_visible");
        this.K.m().add("WebView.set_bounds");
        this.K.m().add("WebView.execute_js");
        this.K.m().add("WebView.set_transparent");
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        setBackgroundColor(z2 ? 0 : -1);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.q, this.s);
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.gravity = 0;
        this.K.addView(this, layoutParams);
        if (!this.i.equals("") && !this.j.equals("")) {
            w();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(x xVar, int i2, c cVar) {
        a(xVar, i2, -1, cVar);
        g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.p.a(java.lang.String, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, int]
     candidates:
      com.adcolony.sdk.p.a(com.adcolony.sdk.p, com.adcolony.sdk.x):boolean
      com.adcolony.sdk.p.a(com.adcolony.sdk.x, java.io.File):boolean
      com.adcolony.sdk.p.a(java.lang.String, boolean):java.lang.StringBuilder */
    /* access modifiers changed from: package-private */
    public void a(x xVar, int i2, int i3, c cVar) {
        JSONObject c2 = xVar.c();
        this.b = s.b(c2, "url");
        if (this.b.equals("")) {
            this.b = s.b(c2, "data");
        }
        this.e = s.b(c2, "base_url");
        this.d = s.b(c2, "custom_js");
        this.f = s.b(c2, "ad_session_id");
        this.I = s.f(c2, TJAdUnitConstants.String.VIDEO_INFO);
        this.h = s.b(c2, "mraid_filepath");
        this.v = s.d(c2, "use_mraid_module") ? a.a().q().d() : this.v;
        this.i = s.b(c2, "ad_choices_filepath");
        this.j = s.b(c2, "ad_choices_url");
        this.F = s.d(c2, "disable_ad_choices");
        this.G = s.d(c2, "ad_choices_snap_to_webview");
        this.w = s.c(c2, "ad_choices_width");
        this.x = s.c(c2, "ad_choices_height");
        if (this.J.length() == 0) {
            this.J = s.f(c2, "iab");
        }
        boolean z2 = false;
        if (!this.A && !this.h.equals("")) {
            if (this.v > 0) {
                this.b = a(this.b.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", "script src=\"file://" + this.h + "\""), s.b(s.f(this.I, DeviceRequestsHelper.DEVICE_INFO_PARAM), "iab_filepath"));
            } else {
                try {
                    this.g = a.a().j().a(this.h, false).toString();
                    this.g = this.g.replaceFirst("bridge.os_name\\s*=\\s*\"\"\\s*;", "bridge.os_name = \"\";\nvar ADC_DEVICE_INFO = " + this.I.toString() + ";\n");
                } catch (IOException e2) {
                    b(e2);
                } catch (IllegalArgumentException e3) {
                    b(e3);
                } catch (IndexOutOfBoundsException e4) {
                    b(e4);
                }
            }
        }
        this.l = i2;
        this.K = cVar;
        if (i3 >= 0) {
            this.u = i3;
        } else {
            f();
        }
        this.q = s.c(c2, "width");
        this.s = s.c(c2, "height");
        this.m = s.c(c2, "x");
        this.o = s.c(c2, "y");
        this.r = this.q;
        this.t = this.s;
        this.p = this.o;
        this.n = this.m;
        if (s.d(c2, "enable_messages") || this.z) {
            z2 = true;
        }
        this.y = z2;
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void b(x xVar) {
        JSONObject c2 = xVar.c();
        this.m = s.c(c2, "x");
        this.o = s.c(c2, "y");
        this.q = s.c(c2, "width");
        this.s = s.c(c2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.width = this.q;
        layoutParams.height = this.s;
        setLayoutParams(layoutParams);
        if (this.z) {
            JSONObject a2 = s.a();
            s.b(a2, "success", true);
            s.b(a2, "id", this.u);
            xVar.a(a2).b();
        }
        h();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.M != null) {
            int s2 = a.a().m().s();
            int t2 = a.a().m().t();
            if (this.G) {
                s2 = this.m + this.q;
            }
            if (this.G) {
                t2 = this.o + this.s;
            }
            float r2 = a.a().m().r();
            int i2 = (int) (((float) this.w) * r2);
            int i3 = (int) (((float) this.x) * r2);
            this.M.setLayoutParams(new AbsoluteLayout.LayoutParams(i2, i3, s2 - i2, t2 - i3));
        }
    }

    private void w() {
        Context c2 = a.c();
        if (c2 != null && this.K != null && !this.F) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(-1);
            gradientDrawable.setShape(1);
            this.M = new ImageView(c2);
            this.M.setImageURI(Uri.fromFile(new File(this.i)));
            this.M.setBackground(gradientDrawable);
            this.M.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ak.a(new Intent("android.intent.action.VIEW", Uri.parse(am.this.j)));
                    a.a().n().a(am.this.f);
                }
            });
            h();
            addView(this.M);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.M != null) {
            this.K.a(this.M);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void c(x xVar) {
        if (s.d(xVar.c(), TJAdUnitConstants.String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
        if (this.z) {
            JSONObject a2 = s.a();
            s.b(a2, "success", true);
            s.b(a2, "id", this.u);
            xVar.a(a2).b();
        }
    }

    public void a(JSONObject jSONObject) {
        synchronized (this.N) {
            this.H.put(jSONObject);
        }
    }

    public void d() {
        if (a.d() && this.B && !this.D) {
            j();
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        ak.a(new Runnable() {
            public void run() {
                String str = "";
                synchronized (am.this.N) {
                    if (am.this.H.length() > 0) {
                        if (am.this.y) {
                            str = am.this.H.toString();
                        }
                        JSONArray unused = am.this.H = s.b();
                    }
                }
                if (am.this.y) {
                    am amVar = am.this;
                    amVar.a("NativeLayer.dispatch_messages(ADC3_update(" + str + "));");
                }
            }
        });
    }

    private class a extends WebViewClient {
        private a() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!am.this.B) {
                return false;
            }
            ak.a(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            ai n = a.a().n();
            n.b(am.this.f);
            n.a(am.this.f);
            JSONObject a = s.a();
            s.a(a, "url", str);
            s.a(a, "ad_session_id", am.this.f);
            new x("WebView.redirect_detected", am.this.K.c(), a).b();
            return true;
        }

        public void onLoadResource(WebView webView, String str) {
            if (str.equals(am.this.b)) {
                am.this.a("if (typeof(CN) != 'undefined' && CN.div) {\n  if (typeof(cn_dispatch_on_touch_begin) != 'undefined') CN.div.removeEventListener('mousedown',  cn_dispatch_on_touch_begin, true);\n  if (typeof(cn_dispatch_on_touch_end) != 'undefined')   CN.div.removeEventListener('mouseup',  cn_dispatch_on_touch_end, true);\n  if (typeof(cn_dispatch_on_touch_move) != 'undefined')  CN.div.removeEventListener('mousemove',  cn_dispatch_on_touch_move, true);\n}\n");
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            boolean unused = am.this.B = false;
            new u.a().a("onPageStarted with URL = ").a(str).a(u.d);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
         arg types: [org.json.JSONObject, java.lang.String, int]
         candidates:
          com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
          com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
        public void onPageFinished(WebView webView, String str) {
            JSONObject a = s.a();
            s.b(a, "id", am.this.l);
            s.a(a, "url", str);
            new u.a().a("onPageFinished called with URL = ").a(str).a(u.b);
            if (am.this.K == null) {
                new x("WebView.on_load", am.this.u, a).b();
            } else {
                s.a(a, "ad_session_id", am.this.f);
                s.b(a, "container_id", am.this.K.d());
                new x("WebView.on_load", am.this.K.c(), a).b();
            }
            if ((am.this.y || am.this.z) && !am.this.B) {
                int m = am.this.v > 0 ? am.this.v : am.this.u;
                if (am.this.v > 0) {
                    float r = a.a().m().r();
                    s.b(am.this.I, "app_orientation", ak.j(ak.h()));
                    s.b(am.this.I, "x", ak.a(am.this));
                    s.b(am.this.I, "y", ak.b(am.this));
                    s.b(am.this.I, "width", (int) (((float) am.this.q) / r));
                    s.b(am.this.I, "height", (int) (((float) am.this.s) / r));
                    s.a(am.this.I, "ad_session_id", am.this.f);
                }
                am amVar = am.this;
                amVar.a("ADC3_init(" + m + "," + am.this.I.toString() + ");");
                boolean unused = am.this.B = true;
            }
            if (!am.this.z) {
                return;
            }
            if (am.this.u != 1 || am.this.v > 0) {
                JSONObject a2 = s.a();
                s.b(a2, "success", true);
                s.b(a2, "id", am.this.u);
                am.this.L.a(a2).b();
            }
        }

        @TargetApi(11)
        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (Build.VERSION.SDK_INT >= 21 || !str.endsWith("mraid.js")) {
                return null;
            }
            try {
                return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(am.this.g.getBytes("UTF-8")));
            } catch (UnsupportedEncodingException unused) {
                new u.a().a("UTF-8 not supported.").a(u.h);
                return null;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            if (Build.VERSION.SDK_INT < 23) {
                am.this.a(i, str, str2);
            }
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            if (Build.VERSION.SDK_INT < 26) {
                return super.onRenderProcessGone(webView, renderProcessGoneDetail);
            }
            if (!renderProcessGoneDetail.didCrash()) {
                return true;
            }
            am.this.a(s.a(), "An error occurred while rendering the ad. Ad closing.");
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, String str) {
        Context c2 = a.c();
        if (c2 != null && (c2 instanceof b)) {
            x xVar = new x("AdSession.finish_fullscreen_ad", 0);
            s.b(jSONObject, "status", 1);
            new u.a().a(str).a(u.g);
            ((b) c2).a(xVar);
        } else if (this.u == 1) {
            new u.a().a("Unable to communicate with controller, disabling AdColony.").a(u.g);
            AdColony.disable();
        } else if (this.v > 0) {
            this.y = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        ak.a(new Runnable() {
            public void run() {
                ac acVar;
                int i;
                try {
                    d l = a.a().l();
                    AdColonyInterstitial adColonyInterstitial = l.c().get(am.this.f);
                    AdColonyAdView adColonyAdView = l.e().get(am.this.f);
                    if (adColonyInterstitial == null) {
                        acVar = null;
                    } else {
                        acVar = adColonyInterstitial.h();
                    }
                    if (acVar == null && adColonyAdView != null) {
                        acVar = adColonyAdView.getOmidManager();
                    }
                    if (acVar == null) {
                        i = -1;
                    } else {
                        i = acVar.c();
                    }
                    if (acVar != null && i == 2) {
                        acVar.a(am.this);
                        acVar.a(am.this.K);
                    }
                } catch (IllegalArgumentException unused) {
                    new u.a().a("IllegalArgumentException when creating omid session").a(u.h);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, String str2) {
        if (this.K != null) {
            JSONObject a2 = s.a();
            s.b(a2, "id", this.l);
            s.a(a2, "ad_session_id", this.f);
            s.b(a2, "container_id", this.K.d());
            s.b(a2, "code", i2);
            s.a(a2, "error", str);
            s.a(a2, "url", str2);
            new x("WebView.on_error", this.K.c(), a2).b();
        }
        new u.a().a("onReceivedError: ").a(str).a(u.h);
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        return this.E;
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.C = z2;
    }

    /* access modifiers changed from: package-private */
    public int o() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public int p() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int q() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public int r() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public int s() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public int t() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int v() {
        return this.o;
    }
}
