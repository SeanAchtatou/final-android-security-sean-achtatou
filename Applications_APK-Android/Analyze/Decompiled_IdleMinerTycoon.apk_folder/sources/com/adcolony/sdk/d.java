package com.adcolony.sdk;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import com.adcolony.sdk.u;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class d {
    private HashMap<String, c> a;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdColonyInterstitial> b;
    private HashMap<String, AdColonyAdViewListener> c;
    /* access modifiers changed from: private */
    public HashMap<String, AdColonyAdView> d;

    d() {
    }

    /* access modifiers changed from: private */
    public boolean c(x xVar) {
        final String b2 = s.b(xVar.c(), "id");
        final AdColonyAdViewListener remove = this.c.remove(b2);
        if (remove == null) {
            a(xVar.d(), b2);
            return false;
        }
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        final x xVar2 = xVar;
        ak.a(new Runnable() {
            public void run() {
                AdColonyAdView adColonyAdView = new AdColonyAdView(c2, xVar2, remove);
                d.this.d.put(b2, adColonyAdView);
                adColonyAdView.setOmidManager(remove.c());
                adColonyAdView.a();
                remove.a((ac) null);
                remove.onRequestFilled(adColonyAdView);
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    public boolean d(x xVar) {
        String b2 = s.b(xVar.c(), "id");
        final AdColonyAdViewListener remove = this.c.remove(b2);
        if (remove == null) {
            a(xVar.d(), b2);
            return false;
        }
        ak.a(new Runnable() {
            public void run() {
                String a2 = remove.a();
                AdColonyZone adColonyZone = a.a().f().get(a2);
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(a2);
                    adColonyZone.b(6);
                }
                remove.onRequestNotFilled(adColonyZone);
            }
        });
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.a = new HashMap<>();
        this.b = new ConcurrentHashMap<>();
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        a.a("AdContainer.create", new z() {
            public void a(final x xVar) {
                ak.a(new Runnable() {
                    public void run() {
                        boolean unused = d.this.i(xVar);
                    }
                });
            }
        });
        a.a("AdContainer.destroy", new z() {
            public void a(final x xVar) {
                ak.a(new Runnable() {
                    public void run() {
                        boolean unused = d.this.j(xVar);
                    }
                });
            }
        });
        a.a("AdContainer.move_view_to_index", new z() {
            public void a(x xVar) {
                boolean unused = d.this.k(xVar);
            }
        });
        a.a("AdContainer.move_view_to_front", new z() {
            public void a(x xVar) {
                boolean unused = d.this.l(xVar);
            }
        });
        a.a("AdSession.finish_fullscreen_ad", new z() {
            public void a(x xVar) {
                boolean unused = d.this.h(xVar);
            }
        });
        a.a("AdSession.start_fullscreen_ad", new z() {
            public void a(x xVar) {
                boolean unused = d.this.g(xVar);
            }
        });
        a.a("AdSession.ad_view_available", new z() {
            public void a(x xVar) {
                boolean unused = d.this.c(xVar);
            }
        });
        a.a("AdSession.ad_view_unavailable", new z() {
            public void a(x xVar) {
                boolean unused = d.this.d(xVar);
            }
        });
        a.a("AdSession.expiring", new z() {
            public void a(x xVar) {
                d.this.a(xVar);
            }
        });
        a.a("AdSession.audio_stopped", new z() {
            public void a(final x xVar) {
                ak.a(new Runnable() {
                    public void run() {
                        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(s.b(xVar.c(), "id"));
                        if (adColonyInterstitial != null && adColonyInterstitial.getListener() != null) {
                            adColonyInterstitial.getListener().onAudioStopped(adColonyInterstitial);
                        }
                    }
                });
            }
        });
        a.a("AdSession.audio_started", new z() {
            public void a(final x xVar) {
                ak.a(new Runnable() {
                    public void run() {
                        AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(s.b(xVar.c(), "id"));
                        if (adColonyInterstitial != null && adColonyInterstitial.getListener() != null) {
                            adColonyInterstitial.getListener().onAudioStarted(adColonyInterstitial);
                        }
                    }
                });
            }
        });
        a.a("AdSession.interstitial_available", new z() {
            public void a(x xVar) {
                boolean unused = d.this.f(xVar);
            }
        });
        a.a("AdSession.interstitial_unavailable", new z() {
            public void a(x xVar) {
                d.this.b(xVar);
            }
        });
        a.a("AdSession.has_audio", new z() {
            public void a(x xVar) {
                boolean unused = d.this.e(xVar);
            }
        });
        a.a("WebView.prepare", new z() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
             arg types: [org.json.JSONObject, java.lang.String, int]
             candidates:
              com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
              com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
            public void a(x xVar) {
                JSONObject a2 = s.a();
                s.b(a2, "success", true);
                xVar.a(a2).b();
            }
        });
        a.a("AdSession.expanded", new z() {
            public void a(final x xVar) {
                ak.a(new Runnable() {
                    public void run() {
                        xVar.a(xVar.c()).b();
                    }
                });
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean a(x xVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject c2 = xVar.c();
        String b2 = s.b(c2, "id");
        if (s.c(c2, "type") != 0) {
            return true;
        }
        final AdColonyInterstitial remove = this.b.remove(b2);
        if (remove == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = remove.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.d(), b2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            ak.a(new Runnable() {
                public void run() {
                    remove.a(true);
                    adColonyInterstitialListener.onExpiring(remove);
                    k r = a.a().r();
                    if (r.b() != null) {
                        r.b().dismiss();
                        r.a((AlertDialog) null);
                    }
                }
            });
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean e(x xVar) {
        String b2 = s.b(xVar.c(), "id");
        JSONObject a2 = s.a();
        s.a(a2, "id", b2);
        Context c2 = a.c();
        if (c2 == null) {
            s.b(a2, "has_audio", false);
            xVar.a(a2).b();
            return false;
        }
        boolean a3 = ak.a(ak.a(c2));
        double b3 = ak.b(ak.a(c2));
        s.b(a2, "has_audio", a3);
        s.a(a2, "volume", b3);
        xVar.a(a2).b();
        return a3;
    }

    /* access modifiers changed from: private */
    public boolean f(final x xVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        String b2 = s.b(xVar.c(), "id");
        final AdColonyInterstitial adColonyInterstitial = this.b.get(b2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.d(), b2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            ak.a(new Runnable() {
                public void run() {
                    if (adColonyInterstitial.h() == null) {
                        adColonyInterstitial.a(s.f(xVar.c(), "iab"));
                    }
                    adColonyInterstitial.a(s.b(xVar.c(), "ad_id"));
                    adColonyInterstitial.b(s.b(xVar.c(), CampaignEx.JSON_KEY_CREATIVE_ID));
                    ac h = adColonyInterstitial.h();
                    if (!(h == null || h.c() == 2)) {
                        try {
                            h.b();
                        } catch (IllegalArgumentException unused) {
                            new u.a().a("IllegalArgumentException when creating omid session").a(u.h);
                        }
                    }
                    adColonyInterstitialListener.onRequestFilled(adColonyInterstitial);
                }
            });
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(x xVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        String b2 = s.b(xVar.c(), "id");
        final AdColonyInterstitial remove = this.b.remove(b2);
        if (remove == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = remove.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.d(), b2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            ak.a(new Runnable() {
                public void run() {
                    AdColonyZone adColonyZone = a.a().f().get(remove.getZoneID());
                    if (adColonyZone == null) {
                        adColonyZone = new AdColonyZone(remove.getZoneID());
                        adColonyZone.b(6);
                    }
                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                }
            });
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, AdColonyAdViewListener adColonyAdViewListener, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
        String e = ak.e();
        JSONObject a2 = s.a();
        float r = a.a().m().r();
        s.a(a2, "zone_id", str);
        s.b(a2, "type", 1);
        s.b(a2, "width_pixels", (int) (((float) adColonyAdSize.getWidth()) * r));
        s.b(a2, "height_pixels", (int) (((float) adColonyAdSize.getHeight()) * r));
        s.b(a2, "width", adColonyAdSize.getWidth());
        s.b(a2, "height", adColonyAdSize.getHeight());
        s.a(a2, "id", e);
        adColonyAdViewListener.a(str);
        adColonyAdViewListener.a(adColonyAdSize);
        if (!(adColonyAdOptions == null || adColonyAdOptions.d == null)) {
            s.a(a2, "options", adColonyAdOptions.d);
        }
        this.c.put(e, adColonyAdViewListener);
        new x("AdSession.on_request", 1, a2).b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(String str, AdColonyInterstitialListener adColonyInterstitialListener, AdColonyAdOptions adColonyAdOptions) {
        String e = ak.e();
        h a2 = a.a();
        JSONObject a3 = s.a();
        s.a(a3, "zone_id", str);
        s.b(a3, "fullscreen", true);
        s.b(a3, "width", a2.m().s());
        s.b(a3, "height", a2.m().t());
        s.b(a3, "type", 0);
        s.a(a3, "id", e);
        new u.a().a("AdSession request with id = ").a(e).a(u.b);
        AdColonyInterstitial adColonyInterstitial = new AdColonyInterstitial(e, adColonyInterstitialListener, str);
        this.b.put(e, adColonyInterstitial);
        if (!(adColonyAdOptions == null || adColonyAdOptions.d == null)) {
            adColonyInterstitial.a(adColonyAdOptions);
            s.a(a3, "options", adColonyAdOptions.d);
        }
        new u.a().a("Requesting AdColony interstitial advertisement.").a(u.a);
        new x("AdSession.on_request", 1, a3).b();
    }

    /* access modifiers changed from: private */
    public boolean g(x xVar) {
        JSONObject c2 = xVar.c();
        String b2 = s.b(c2, "id");
        AdColonyInterstitial adColonyInterstitial = this.b.get(b2);
        AdColonyAdView adColonyAdView = this.d.get(b2);
        int a2 = s.a(c2, "orientation", -1);
        boolean z = adColonyAdView != null;
        if (adColonyInterstitial != null || z) {
            JSONObject a3 = s.a();
            s.a(a3, "id", b2);
            if (adColonyInterstitial != null) {
                adColonyInterstitial.a(s.c(a3, "module_id"));
                if (a2 == 0 || a2 == 1) {
                    adColonyInterstitial.b(a2);
                }
                adColonyInterstitial.a();
            }
            return true;
        }
        a(xVar.d(), b2);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean h(x xVar) {
        final AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject c2 = xVar.c();
        int c3 = s.c(c2, "status");
        if (c3 == 5 || c3 == 1 || c3 == 0 || c3 == 6) {
            return false;
        }
        String b2 = s.b(c2, "id");
        final AdColonyInterstitial remove = this.b.remove(b2);
        if (remove == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = remove.getListener();
        }
        if (adColonyInterstitialListener == null) {
            a(xVar.d(), b2);
            return false;
        }
        ak.a(new Runnable() {
            public void run() {
                a.a().c(false);
                adColonyInterstitialListener.onClosed(remove);
            }
        });
        remove.a((c) null);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean i(x xVar) {
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        JSONObject c3 = xVar.c();
        String b2 = s.b(c3, "ad_session_id");
        c cVar = new c(c2.getApplicationContext(), b2);
        cVar.b(xVar);
        this.a.put(b2, cVar);
        if (s.c(c3, "width") != 0) {
            cVar.a(false);
        } else if (this.b.get(b2) == null) {
            a(xVar.d(), b2);
            return false;
        } else {
            this.b.get(b2).a(cVar);
        }
        JSONObject a2 = s.a();
        s.b(a2, "success", true);
        xVar.a(a2).b();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean j(x xVar) {
        String b2 = s.b(xVar.c(), "ad_session_id");
        c cVar = this.a.get(b2);
        if (cVar == null) {
            a(xVar.d(), b2);
            return false;
        }
        a(cVar);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(final c cVar) {
        ak.a(new Runnable() {
            public void run() {
                for (int i = 0; i < cVar.l().size(); i++) {
                    a.b(cVar.m().get(i), cVar.l().get(i));
                }
                cVar.m().clear();
                cVar.l().clear();
                cVar.removeAllViews();
                cVar.d = null;
                cVar.c = null;
                new u.a().a("Destroying container tied to ad_session_id = ").a(cVar.b()).a(u.d);
                for (am next : cVar.g().values()) {
                    if (!next.m()) {
                        int b2 = next.b();
                        if (b2 <= 0) {
                            b2 = next.a();
                        }
                        a.a().a(b2);
                        next.loadUrl("about:blank");
                        next.clearCache(true);
                        next.removeAllViews();
                        next.a(true);
                    }
                }
                new u.a().a("Stopping and releasing all media players associated with ").a("VideoViews tied to ad_session_id = ").a(cVar.b()).a(u.d);
                for (al next2 : cVar.e().values()) {
                    next2.d();
                    next2.g();
                }
                cVar.e().clear();
                cVar.f().clear();
                cVar.g().clear();
                cVar.i().clear();
                cVar.k().clear();
                cVar.h().clear();
                cVar.j().clear();
                cVar.a = true;
            }
        });
        AdColonyAdView adColonyAdView = this.d.get(cVar.b());
        if (adColonyAdView == null || adColonyAdView.d()) {
            new u.a().a("Removing ad 4").a(u.b);
            this.a.remove(cVar.b());
            cVar.c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        new u.a().a("Message '").a(str).a("' sent with invalid id: ").a(str2).a(u.g);
    }

    /* access modifiers changed from: private */
    public boolean k(x xVar) {
        JSONObject c2 = xVar.c();
        String d2 = xVar.d();
        String b2 = s.b(c2, "ad_session_id");
        int c3 = s.c(c2, "view_id");
        c cVar = this.a.get(b2);
        View view = cVar.k().get(Integer.valueOf(c3));
        if (cVar == null) {
            a(d2, b2);
            return false;
        } else if (view == null) {
            a(d2, "" + c3);
            return false;
        } else {
            view.bringToFront();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean l(x xVar) {
        JSONObject c2 = xVar.c();
        String d2 = xVar.d();
        String b2 = s.b(c2, "ad_session_id");
        int c3 = s.c(c2, "view_id");
        c cVar = this.a.get(b2);
        if (cVar == null) {
            a(d2, b2);
            return false;
        }
        View view = cVar.k().get(Integer.valueOf(c3));
        if (view == null) {
            a(d2, "" + c3);
            return false;
        }
        cVar.removeView(view);
        cVar.addView(view, view.getLayoutParams());
        return true;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, c> b() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public ConcurrentHashMap<String, AdColonyInterstitial> c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyAdViewListener> d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyAdView> e() {
        return this.d;
    }
}
