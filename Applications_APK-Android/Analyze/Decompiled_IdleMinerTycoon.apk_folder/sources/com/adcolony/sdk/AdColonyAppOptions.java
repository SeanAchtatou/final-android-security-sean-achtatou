package com.adcolony.sdk;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.Log;
import com.tapjoy.TapjoyConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyAppOptions {
    public static final String ADMARVEL = "AdMarvel";
    public static final String ADMOB = "AdMob";
    public static final String ADOBEAIR = "Adobe AIR";
    public static final String AERSERVE = "AerServe";
    public static final int ALL = 2;
    public static final String APPODEAL = "Appodeal";
    public static final String COCOS2DX = "Cocos2d-x";
    public static final String CORONA = "Corona";
    public static final String FUSEPOWERED = "Fuse Powered";
    public static final String FYBER = "Fyber";
    public static final String IRONSOURCE = "ironSource";
    public static final int LANDSCAPE = 1;
    public static final String MOPUB = "MoPub";
    public static final int PORTRAIT = 0;
    @Deprecated
    public static final int SENSOR = 2;
    public static final String UNITY = "Unity";
    String a = "";
    String[] b;
    JSONArray c = s.b();
    JSONObject d = s.a();
    AdColonyUserMetadata e;

    public AdColonyAppOptions() {
        setOriginStore("google");
        if (a.b()) {
            h a2 = a.a();
            if (a2.e()) {
                a(a2.d().a);
                a(a2.d().b);
            }
        }
    }

    public AdColonyAppOptions setGDPRRequired(boolean z) {
        setOption("gdpr_required", z);
        return this;
    }

    public boolean getGDPRRequired() {
        return s.d(this.d, "gdpr_required");
    }

    public AdColonyAppOptions setGDPRConsentString(@NonNull String str) {
        s.a(this.d, "consent_string", str);
        return this;
    }

    public String getGDPRConsentString() {
        return s.b(this.d, "consent_string");
    }

    public AdColonyAppOptions setAppVersion(@NonNull String str) {
        if (ak.d(str)) {
            setOption(TapjoyConstants.TJC_APP_VERSION_NAME, str);
        }
        return this;
    }

    public String getAppVersion() {
        return s.b(this.d, TapjoyConstants.TJC_APP_VERSION_NAME);
    }

    public AdColonyAppOptions setUserID(@NonNull String str) {
        if (ak.d(str)) {
            setOption("user_id", str);
        }
        return this;
    }

    public String getUserID() {
        return s.b(this.d, "user_id");
    }

    public AdColonyAppOptions setOption(@NonNull String str, boolean z) {
        if (ak.d(str)) {
            s.b(this.d, str, z);
        }
        return this;
    }

    public Object getOption(@NonNull String str) {
        return s.a(this.d, str);
    }

    public AdColonyAppOptions setOption(@NonNull String str, double d2) {
        if (ak.d(str)) {
            s.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAppOptions setOption(@NonNull String str, @NonNull String str2) {
        if (str != null && ak.d(str) && ak.d(str2)) {
            s.a(this.d, str, str2);
        }
        return this;
    }

    public AdColonyAppOptions setOriginStore(@NonNull String str) {
        if (ak.d(str)) {
            setOption("origin_store", str);
        }
        return this;
    }

    public String getOriginStore() {
        return s.b(this.d, "origin_store");
    }

    public AdColonyAppOptions setRequestedAdOrientation(@IntRange(from = 0, to = 2) int i) {
        setOption("orientation", (double) i);
        return this;
    }

    public int getRequestedAdOrientation() {
        return s.a(this.d, "orientation", -1);
    }

    public AdColonyAppOptions setAppOrientation(@IntRange(from = 0, to = 2) int i) {
        setOption("app_orientation", (double) i);
        return this;
    }

    public int getAppOrientation() {
        return s.a(this.d, "app_orientation", -1);
    }

    public AdColonyAppOptions setUserMetadata(@NonNull AdColonyUserMetadata adColonyUserMetadata) {
        this.e = adColonyUserMetadata;
        s.a(this.d, "user_metadata", adColonyUserMetadata.c);
        return this;
    }

    public AdColonyAppOptions setTestModeEnabled(boolean z) {
        s.b(this.d, "test_mode", z);
        return this;
    }

    public boolean getTestModeEnabled() {
        return s.d(this.d, "test_mode");
    }

    public AdColonyAppOptions setMultiWindowEnabled(boolean z) {
        s.b(this.d, "multi_window_enabled", z);
        return this;
    }

    public boolean getMultiWindowEnabled() {
        return s.d(this.d, "multi_window_enabled");
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.e;
    }

    public AdColonyAppOptions setMediationNetwork(@NonNull String str, @NonNull String str2) {
        if (ak.d(str) && ak.d(str2)) {
            s.a(this.d, "mediation_network", str);
            s.a(this.d, "mediation_network_version", str2);
        }
        return this;
    }

    public JSONObject getMediationInfo() {
        JSONObject a2 = s.a();
        s.a(a2, "name", s.b(this.d, "mediation_network"));
        s.a(a2, "version", s.b(this.d, "mediation_network_version"));
        return a2;
    }

    public AdColonyAppOptions setPlugin(@NonNull String str, @NonNull String str2) {
        if (ak.d(str) && ak.d(str2)) {
            s.a(this.d, TapjoyConstants.TJC_PLUGIN, str);
            s.a(this.d, "plugin_version", str2);
        }
        return this;
    }

    public JSONObject getPluginInfo() {
        JSONObject a2 = s.a();
        s.a(a2, "name", s.b(this.d, TapjoyConstants.TJC_PLUGIN));
        s.a(a2, "version", s.b(this.d, "plugin_version"));
        return a2;
    }

    public AdColonyAppOptions setKeepScreenOn(boolean z) {
        s.b(this.d, "keep_screen_on", z);
        return this;
    }

    public boolean getKeepScreenOn() {
        return s.d(this.d, "keep_screen_on");
    }

    public static AdColonyAppOptions getMoPubAppOptions(@NonNull String str) {
        AdColonyAppOptions mediationNetwork = new AdColonyAppOptions().setMediationNetwork(MOPUB, "1.0");
        if (str == null || str.isEmpty()) {
            return mediationNetwork;
        }
        String[] split = str.split(",");
        int length = split.length;
        int i = 0;
        while (i < length) {
            String[] split2 = split[i].split(":");
            if (split2.length == 2) {
                String str2 = split2[0];
                char c2 = 65535;
                int hashCode = str2.hashCode();
                if (hashCode != 109770977) {
                    if (hashCode == 351608024 && str2.equals("version")) {
                        c2 = 1;
                    }
                } else if (str2.equals("store")) {
                    c2 = 0;
                }
                switch (c2) {
                    case 0:
                        mediationNetwork.setOriginStore(split2[1]);
                        break;
                    case 1:
                        mediationNetwork.setAppVersion(split2[1]);
                        break;
                    default:
                        Log.e("AdColonyMoPub", "AdColony client options in wrong format - please check your MoPub dashboard");
                        return mediationNetwork;
                }
                i++;
            } else {
                Log.e("AdColonyMoPub", "AdColony client options not recognized - please check your MoPub dashboard");
                return null;
            }
        }
        return mediationNetwork;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions a(String str) {
        if (str == null) {
            return this;
        }
        this.a = str;
        s.a(this.d, "app_id", str);
        return this;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions a(String... strArr) {
        if (strArr == null) {
            return this;
        }
        this.b = strArr;
        this.c = s.b();
        for (String a2 : strArr) {
            s.a(this.c, a2);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public String[] b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public JSONArray c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public JSONObject d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        setOption("bundle_id", a.a().m().J());
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (s.i(this.d, "use_forced_controller")) {
            am.a = s.d(this.d, "use_forced_controller");
        }
        if (s.i(this.d, "use_staging_launch_server") && s.d(this.d, "use_staging_launch_server")) {
            h.e = "https://adc3-launch-staging.adcolony.com/v4/launch";
        }
    }
}
