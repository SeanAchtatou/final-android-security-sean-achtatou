package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import com.adcolony.sdk.u;
import com.google.android.gms.drive.DriveFile;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ai {
    ai() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("System.open_store", new z() {
            public void a(x xVar) {
                ai.this.b(xVar);
            }
        });
        a.a("System.save_screenshot", new z() {
            public void a(x xVar) {
                ai.this.c(xVar);
            }
        });
        a.a("System.telephone", new z() {
            public void a(x xVar) {
                ai.this.d(xVar);
            }
        });
        a.a("System.sms", new z() {
            public void a(x xVar) {
                ai.this.e(xVar);
            }
        });
        a.a("System.vibrate", new z() {
            public void a(x xVar) {
                ai.this.f(xVar);
            }
        });
        a.a("System.open_browser", new z() {
            public void a(x xVar) {
                ai.this.g(xVar);
            }
        });
        a.a("System.mail", new z() {
            public void a(x xVar) {
                ai.this.h(xVar);
            }
        });
        a.a("System.launch_app", new z() {
            public void a(x xVar) {
                ai.this.i(xVar);
            }
        });
        a.a("System.create_calendar_event", new z() {
            public void a(x xVar) {
                ai.this.j(xVar);
            }
        });
        a.a("System.check_app_presence", new z() {
            public void a(x xVar) {
                ai.this.k(xVar);
            }
        });
        a.a("System.check_social_presence", new z() {
            public void a(x xVar) {
                ai.this.l(xVar);
            }
        });
        a.a("System.social_post", new z() {
            public void a(x xVar) {
                ai.this.m(xVar);
            }
        });
        a.a("System.make_in_app_purchase", new z() {
            public void a(x xVar) {
                boolean unused = ai.this.q(xVar);
            }
        });
        a.a("System.close", new z() {
            public void a(x xVar) {
                boolean unused = ai.this.p(xVar);
            }
        });
        a.a("System.expand", new z() {
            public void a(x xVar) {
                ai.this.a(xVar);
            }
        });
        a.a("System.use_custom_close", new z() {
            public void a(x xVar) {
                boolean unused = ai.this.o(xVar);
            }
        });
        a.a("System.set_orientation_properties", new z() {
            public void a(x xVar) {
                boolean unused = ai.this.n(xVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean n(x xVar) {
        JSONObject c = xVar.c();
        String b = s.b(c, "ad_session_id");
        int c2 = s.c(c, "orientation");
        d l = a.a().l();
        AdColonyAdView adColonyAdView = l.e().get(b);
        AdColonyInterstitial adColonyInterstitial = l.c().get(b);
        Context c3 = a.c();
        if (adColonyAdView != null) {
            adColonyAdView.setOrientation(c2);
        } else if (adColonyInterstitial != null) {
            adColonyInterstitial.b(c2);
        }
        if (adColonyInterstitial == null && adColonyAdView == null) {
            new u.a().a("Invalid ad session id sent with set orientation properties message: ").a(b).a(u.h);
            return false;
        } else if (!(c3 instanceof b)) {
            return true;
        } else {
            ((b) c3).a(adColonyAdView == null ? adColonyInterstitial.e() : adColonyAdView.getOrientation());
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(x xVar) {
        JSONObject c = xVar.c();
        Context c2 = a.c();
        if (c2 == null || !a.b()) {
            return false;
        }
        String b = s.b(c, "ad_session_id");
        h a = a.a();
        AdColonyAdView adColonyAdView = a.l().e().get(b);
        if (adColonyAdView == null || ((!adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) || a.u() == adColonyAdView)) {
            return false;
        }
        adColonyAdView.setExpandMessage(xVar);
        adColonyAdView.setExpandedWidth(s.c(c, "width"));
        adColonyAdView.setExpandedHeight(s.c(c, "height"));
        adColonyAdView.setOrientation(s.a(c, "orientation", -1));
        adColonyAdView.setNoCloseButton(s.d(c, "use_custom_close"));
        a.a(adColonyAdView);
        a.a(adColonyAdView.getContainer());
        Intent intent = new Intent(c2, AdColonyAdViewActivity.class);
        if (c2 instanceof Application) {
            intent.addFlags(DriveFile.MODE_READ_ONLY);
        }
        c(b);
        b(b);
        c2.startActivity(intent);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean o(x xVar) {
        AdColonyAdView adColonyAdView = a.a().l().e().get(s.b(xVar.c(), "ad_session_id"));
        if (adColonyAdView == null) {
            return false;
        }
        adColonyAdView.setNoCloseButton(s.d(xVar.c(), "use_custom_close"));
        return true;
    }

    /* access modifiers changed from: private */
    public boolean p(x xVar) {
        String b = s.b(xVar.c(), "ad_session_id");
        Activity activity = a.c() instanceof Activity ? (Activity) a.c() : null;
        boolean z = activity instanceof AdColonyAdViewActivity;
        if (!(activity instanceof b)) {
            return false;
        }
        if (z) {
            ((AdColonyAdViewActivity) activity).b();
            return true;
        }
        JSONObject a = s.a();
        s.a(a, "id", b);
        new x("AdSession.on_request_close", ((b) activity).f, a).b();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean q(x xVar) {
        JSONObject c = xVar.c();
        d l = a.a().l();
        String b = s.b(c, "ad_session_id");
        AdColonyInterstitial adColonyInterstitial = l.c().get(b);
        AdColonyAdView adColonyAdView = l.e().get(b);
        if ((adColonyInterstitial == null || adColonyInterstitial.getListener() == null || adColonyInterstitial.d() == null) && (adColonyAdView == null || adColonyAdView.getListener() == null)) {
            return false;
        }
        if (adColonyAdView == null) {
            new x("AdUnit.make_in_app_purchase", adColonyInterstitial.d().c()).b();
        }
        b(b);
        c(b);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean b(x xVar) {
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        String b = s.b(c, "product_id");
        String b2 = s.b(c, "ad_session_id");
        if (b.equals("")) {
            b = s.b(c, "handle");
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(b));
        d(b);
        if (ak.a(intent)) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b2);
            b(b2);
            c(b2);
            return true;
        }
        ak.a("Unable to open.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        com.adcolony.sdk.ak.a("Error saving screenshot.", 0);
        com.adcolony.sdk.s.b(r2, "success", false);
        r11.a(r2).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e1, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e2, code lost:
        com.adcolony.sdk.ak.a("Error saving screenshot.", 0);
        com.adcolony.sdk.s.b(r2, "success", false);
        r11.a(r2).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00f3, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x00ab */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(final com.adcolony.sdk.x r11) {
        /*
            r10 = this;
            android.content.Context r0 = com.adcolony.sdk.a.c()
            r1 = 0
            if (r0 == 0) goto L_0x0120
            boolean r2 = r0 instanceof android.app.Activity
            if (r2 != 0) goto L_0x000d
            goto L_0x0120
        L_0x000d:
            java.lang.String r2 = "android.permission.WRITE_EXTERNAL_STORAGE"
            int r2 = android.support.v4.app.ActivityCompat.checkSelfPermission(r0, r2)     // Catch:{ NoClassDefFoundError -> 0x010a }
            if (r2 != 0) goto L_0x00f4
            org.json.JSONObject r2 = r11.c()     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r3 = "ad_session_id"
            java.lang.String r2 = com.adcolony.sdk.s.b(r2, r3)     // Catch:{ NoClassDefFoundError -> 0x010a }
            r10.b(r2)     // Catch:{ NoClassDefFoundError -> 0x010a }
            org.json.JSONObject r2 = com.adcolony.sdk.s.a()     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NoClassDefFoundError -> 0x010a }
            r3.<init>()     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r4 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x010a }
            r3.append(r4)     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r4 = "/Pictures/AdColony_Screenshots/AdColony_Screenshot_"
            r3.append(r4)     // Catch:{ NoClassDefFoundError -> 0x010a }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ NoClassDefFoundError -> 0x010a }
            r3.append(r4)     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r4 = ".jpg"
            r3.append(r4)     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r3 = r3.toString()     // Catch:{ NoClassDefFoundError -> 0x010a }
            r4 = r0
            android.app.Activity r4 = (android.app.Activity) r4     // Catch:{ NoClassDefFoundError -> 0x010a }
            android.view.Window r4 = r4.getWindow()     // Catch:{ NoClassDefFoundError -> 0x010a }
            android.view.View r4 = r4.getDecorView()     // Catch:{ NoClassDefFoundError -> 0x010a }
            android.view.View r4 = r4.getRootView()     // Catch:{ NoClassDefFoundError -> 0x010a }
            r5 = 1
            r4.setDrawingCacheEnabled(r5)     // Catch:{ NoClassDefFoundError -> 0x010a }
            android.graphics.Bitmap r6 = r4.getDrawingCache()     // Catch:{ NoClassDefFoundError -> 0x010a }
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r6)     // Catch:{ NoClassDefFoundError -> 0x010a }
            r4.setDrawingCacheEnabled(r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x00ab }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ab }
            r7.<init>()     // Catch:{ Exception -> 0x00ab }
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00ab }
            java.lang.String r8 = r8.getPath()     // Catch:{ Exception -> 0x00ab }
            r7.append(r8)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r8 = "/Pictures"
            r7.append(r8)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00ab }
            r4.<init>(r7)     // Catch:{ Exception -> 0x00ab }
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00ab }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ab }
            r8.<init>()     // Catch:{ Exception -> 0x00ab }
            java.io.File r9 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00ab }
            java.lang.String r9 = r9.getPath()     // Catch:{ Exception -> 0x00ab }
            r8.append(r9)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r9 = "/Pictures/AdColony_Screenshots"
            r8.append(r9)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00ab }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00ab }
            r4.mkdirs()     // Catch:{ Exception -> 0x00ab }
            r7.mkdirs()     // Catch:{ Exception -> 0x00ab }
        L_0x00ab:
            java.io.File r4 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r4.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r7.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r8 = 90
            r6.compress(r4, r8, r7)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r7.flush()     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r7.close()     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            java.lang.String[] r4 = new java.lang.String[r5]     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r4[r1] = r3     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r3 = 0
            com.adcolony.sdk.ai$10 r6 = new com.adcolony.sdk.ai$10     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            r6.<init>(r2, r11)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            android.media.MediaScannerConnection.scanFile(r0, r4, r3, r6)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d0 }
            return r5
        L_0x00d0:
            java.lang.String r0 = "Error saving screenshot."
            com.adcolony.sdk.ak.a(r0, r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r0 = "success"
            com.adcolony.sdk.s.b(r2, r0, r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            com.adcolony.sdk.x r0 = r11.a(r2)     // Catch:{ NoClassDefFoundError -> 0x010a }
            r0.b()     // Catch:{ NoClassDefFoundError -> 0x010a }
            return r1
        L_0x00e2:
            java.lang.String r0 = "Error saving screenshot."
            com.adcolony.sdk.ak.a(r0, r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r0 = "success"
            com.adcolony.sdk.s.b(r2, r0, r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            com.adcolony.sdk.x r0 = r11.a(r2)     // Catch:{ NoClassDefFoundError -> 0x010a }
            r0.b()     // Catch:{ NoClassDefFoundError -> 0x010a }
            return r1
        L_0x00f4:
            java.lang.String r0 = "Error saving screenshot."
            com.adcolony.sdk.ak.a(r0, r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            org.json.JSONObject r0 = r11.c()     // Catch:{ NoClassDefFoundError -> 0x010a }
            java.lang.String r2 = "success"
            com.adcolony.sdk.s.b(r0, r2, r1)     // Catch:{ NoClassDefFoundError -> 0x010a }
            com.adcolony.sdk.x r0 = r11.a(r0)     // Catch:{ NoClassDefFoundError -> 0x010a }
            r0.b()     // Catch:{ NoClassDefFoundError -> 0x010a }
            return r1
        L_0x010a:
            java.lang.String r0 = "Error saving screenshot."
            com.adcolony.sdk.ak.a(r0, r1)
            org.json.JSONObject r0 = r11.c()
            java.lang.String r2 = "success"
            com.adcolony.sdk.s.b(r0, r2, r1)
            com.adcolony.sdk.x r11 = r11.a(r0)
            r11.b()
            return r1
        L_0x0120:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.ai.c(com.adcolony.sdk.x):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean d(x xVar) {
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        Intent intent = new Intent("android.intent.action.DIAL");
        Intent data = intent.setData(Uri.parse("tel:" + s.b(c, "phone_number")));
        String b = s.b(c, "ad_session_id");
        if (ak.a(data)) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        ak.a("Failed to dial number.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean e(x xVar) {
        JSONObject c = xVar.c();
        JSONObject a = s.a();
        String b = s.b(c, "ad_session_id");
        JSONArray g = s.g(c, "recipients");
        String str = "";
        for (int i = 0; i < g.length(); i++) {
            if (i != 0) {
                str = str + ";";
            }
            str = str + s.c(g, i);
        }
        if (ak.a(new Intent("android.intent.action.VIEW", Uri.parse("smsto:" + str)).putExtra("sms_body", s.b(c, "body")))) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        ak.a("Failed to create sms.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean f(x xVar) {
        Context c = a.c();
        if (c == null) {
            return false;
        }
        int a = s.a(xVar.c(), "length_ms", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL);
        JSONObject a2 = s.a();
        JSONArray d = ak.d(c);
        boolean z = false;
        for (int i = 0; i < d.length(); i++) {
            if (s.c(d, i).equals("android.permission.VIBRATE")) {
                z = true;
            }
        }
        if (!z) {
            new u.a().a("No vibrate permission detected.").a(u.e);
            s.b(a2, "success", false);
            xVar.a(a2).b();
            return false;
        }
        try {
            ((Vibrator) c.getSystemService("vibrator")).vibrate((long) a);
            s.b(a2, "success", false);
            xVar.a(a2).b();
            return true;
        } catch (Exception unused) {
            new u.a().a("Vibrate command failed.").a(u.e);
            s.b(a2, "success", false);
            xVar.a(a2).b();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean g(x xVar) {
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        String b = s.b(c, "url");
        String b2 = s.b(c, "ad_session_id");
        AdColonyAdView adColonyAdView = a.a().l().e().get(b2);
        if (adColonyAdView != null && !adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) {
            return false;
        }
        if (b.startsWith("browser")) {
            b = b.replaceFirst("browser", "http");
        }
        if (b.startsWith("safari")) {
            b = b.replaceFirst("safari", "http");
        }
        d(b);
        if (ak.a(new Intent("android.intent.action.VIEW", Uri.parse(b)))) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b2);
            b(b2);
            c(b2);
            return true;
        }
        ak.a("Failed to launch browser.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean h(x xVar) {
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        JSONArray g = s.g(c, "recipients");
        boolean d = s.d(c, TJAdUnitConstants.String.HTML);
        String b = s.b(c, "subject");
        String b2 = s.b(c, "body");
        String b3 = s.b(c, "ad_session_id");
        String[] strArr = new String[g.length()];
        for (int i = 0; i < g.length(); i++) {
            strArr[i] = s.c(g, i);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        if (!d) {
            intent.setType("plain/text");
        }
        intent.putExtra("android.intent.extra.SUBJECT", b).putExtra("android.intent.extra.TEXT", b2).putExtra("android.intent.extra.EMAIL", strArr);
        if (ak.a(intent)) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b3);
            b(b3);
            c(b3);
            return true;
        }
        ak.a("Failed to send email.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean i(x xVar) {
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        String b = s.b(c, "ad_session_id");
        if (s.d(c, CampaignEx.JSON_KEY_DEEP_LINK_URL)) {
            return b(xVar);
        }
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        if (ak.a(c2.getPackageManager().getLaunchIntentForPackage(s.b(c, "handle")))) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        ak.a("Failed to launch external application.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean j(x xVar) {
        Intent intent;
        x xVar2 = xVar;
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        String str = "";
        String str2 = "";
        String b = s.b(c, "ad_session_id");
        JSONObject f = s.f(c, "params");
        JSONObject f2 = s.f(f, "recurrence");
        JSONArray b2 = s.b();
        JSONArray b3 = s.b();
        JSONArray b4 = s.b();
        String b5 = s.b(f, "description");
        s.b(f, FirebaseAnalytics.Param.LOCATION);
        String b6 = s.b(f, "start");
        String b7 = s.b(f, "end");
        String b8 = s.b(f, "summary");
        if (f2 != null && f2.length() > 0) {
            str2 = s.b(f2, "expires");
            str = s.b(f2, "frequency").toUpperCase(Locale.getDefault());
            b2 = s.g(f2, "daysInWeek");
            b3 = s.g(f2, "daysInMonth");
            b4 = s.g(f2, "daysInYear");
        }
        if (b8.equals("")) {
            b8 = b5;
        }
        Date h = ak.h(b6);
        Date h2 = ak.h(b7);
        Date h3 = ak.h(str2);
        if (h == null || h2 == null) {
            ak.a("Unable to create Calendar Event", 0);
            s.b(a, "success", false);
            xVar2.a(a).b();
            return false;
        }
        long time = h.getTime();
        long time2 = h2.getTime();
        long j = 0;
        long time3 = h3 != null ? (h3.getTime() - h.getTime()) / 1000 : 0;
        if (str.equals("DAILY")) {
            j = (time3 / 86400) + 1;
        } else if (str.equals("WEEKLY")) {
            j = (time3 / 604800) + 1;
        } else if (str.equals("MONTHLY")) {
            j = (time3 / 2629800) + 1;
        } else if (str.equals("YEARLY")) {
            j = (time3 / 31557600) + 1;
        }
        long j2 = j;
        if (f2 == null || f2.length() <= 0) {
            intent = new Intent("android.intent.action.EDIT").setType("vnd.android.cursor.item/event").putExtra("title", b8).putExtra("description", b5).putExtra("beginTime", time).putExtra("endTime", time2);
        } else {
            String str3 = "FREQ=" + str + ";COUNT=" + j2;
            try {
                if (b2.length() != 0) {
                    str3 = str3 + ";BYDAY=" + ak.a(b2);
                }
                if (b3.length() != 0) {
                    str3 = str3 + ";BYMONTHDAY=" + ak.b(b3);
                }
                if (b4.length() != 0) {
                    str3 = str3 + ";BYYEARDAY=" + ak.b(b4);
                }
            } catch (JSONException unused) {
            }
            intent = new Intent("android.intent.action.EDIT").setType("vnd.android.cursor.item/event").putExtra("title", b8).putExtra("description", b5).putExtra("beginTime", time).putExtra("endTime", time2).putExtra("rrule", str3);
        }
        if (ak.a(intent)) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        ak.a("Unable to create Calendar Event.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean k(x xVar) {
        JSONObject a = s.a();
        String b = s.b(xVar.c(), "name");
        boolean a2 = ak.a(b);
        s.b(a, "success", true);
        s.b(a, IronSourceConstants.EVENTS_RESULT, a2);
        s.a(a, "name", b);
        s.a(a, NotificationCompat.CATEGORY_SERVICE, b);
        xVar.a(a).b();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean l(x xVar) {
        return k(xVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.ak.a(android.content.Intent, boolean):boolean
     arg types: [android.content.Intent, int]
     candidates:
      com.adcolony.sdk.ak.a(double, int):java.lang.String
      com.adcolony.sdk.ak.a(java.lang.String, int):boolean
      com.adcolony.sdk.ak.a(java.lang.String, java.io.File):boolean
      com.adcolony.sdk.ak.a(java.lang.String[], java.lang.String[]):boolean
      com.adcolony.sdk.ak.a(android.content.Intent, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean m(x xVar) {
        JSONObject a = s.a();
        JSONObject c = xVar.c();
        Intent type = new Intent("android.intent.action.SEND").setType("text/plain");
        Intent putExtra = type.putExtra("android.intent.extra.TEXT", s.b(c, "text") + " " + s.b(c, "url"));
        String b = s.b(c, "ad_session_id");
        if (ak.a(putExtra, true)) {
            s.b(a, "success", true);
            xVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        ak.a("Unable to create social post.", 0);
        s.b(a, "success", false);
        xVar.a(a).b();
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        d l = a.a().l();
        AdColonyInterstitial adColonyInterstitial = l.c().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = l.e().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (adColonyAdView != null && listener != null) {
                listener.onLeftApplication(adColonyAdView);
                return;
            }
            return;
        }
        adColonyInterstitial.getListener().onLeftApplication(adColonyInterstitial);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        d l = a.a().l();
        AdColonyInterstitial adColonyInterstitial = l.c().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = l.e().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (adColonyAdView != null && listener != null) {
                listener.onClicked(adColonyAdView);
                return;
            }
            return;
        }
        adColonyInterstitial.getListener().onClicked(adColonyInterstitial);
    }

    private boolean c(@NonNull String str) {
        if (a.a().l().e().get(str) == null) {
            return false;
        }
        JSONObject a = s.a();
        s.a(a, "ad_session_id", str);
        new x("MRAID.on_event", 1, a).b();
        return true;
    }

    private void d(final String str) {
        ak.b.execute(new Runnable() {
            public void run() {
                JSONObject a2 = s.a();
                s.a(a2, "type", "open_hook");
                s.a(a2, "message", str);
                new x("CustomMessage.controller_send", 0, a2).b();
            }
        });
    }
}
