package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import com.adcolony.sdk.u;
import org.json.JSONObject;

class k {
    static AlertDialog a;
    /* access modifiers changed from: private */
    public x b;
    /* access modifiers changed from: private */
    public AlertDialog c;
    /* access modifiers changed from: private */
    public boolean d;

    k() {
        a.a("Alert.show", new z() {
            public void a(x xVar) {
                if (!a.d() || !(a.c() instanceof Activity)) {
                    new u.a().a("Missing Activity reference, can't build AlertDialog.").a(u.g);
                } else if (s.d(xVar.c(), "on_resume")) {
                    x unused = k.this.b = xVar;
                } else {
                    k.this.a(xVar);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"InlinedApi"})
    public void a(final x xVar) {
        Context c2 = a.c();
        if (c2 != null) {
            final AlertDialog.Builder builder = Build.VERSION.SDK_INT >= 21 ? new AlertDialog.Builder(c2, 16974374) : new AlertDialog.Builder(c2, 16974126);
            JSONObject c3 = xVar.c();
            String b2 = s.b(c3, "message");
            String b3 = s.b(c3, "title");
            String b4 = s.b(c3, "positive");
            String b5 = s.b(c3, "negative");
            builder.setMessage(b2);
            builder.setTitle(b3);
            builder.setPositiveButton(b4, new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
                 arg types: [org.json.JSONObject, java.lang.String, int]
                 candidates:
                  com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
                  com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean
                 arg types: [com.adcolony.sdk.k, int]
                 candidates:
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, com.adcolony.sdk.x):com.adcolony.sdk.x
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean */
                public void onClick(DialogInterface dialogInterface, int i) {
                    AlertDialog unused = k.this.c = (AlertDialog) null;
                    dialogInterface.dismiss();
                    JSONObject a2 = s.a();
                    s.b(a2, "positive", true);
                    boolean unused2 = k.this.d = false;
                    xVar.a(a2).b();
                }
            });
            if (!b5.equals("")) {
                builder.setNegativeButton(b5, new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
                     arg types: [org.json.JSONObject, java.lang.String, int]
                     candidates:
                      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
                      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean
                     arg types: [com.adcolony.sdk.k, int]
                     candidates:
                      com.adcolony.sdk.k.a(com.adcolony.sdk.k, android.app.AlertDialog):android.app.AlertDialog
                      com.adcolony.sdk.k.a(com.adcolony.sdk.k, com.adcolony.sdk.x):com.adcolony.sdk.x
                      com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog unused = k.this.c = (AlertDialog) null;
                        dialogInterface.dismiss();
                        JSONObject a2 = s.a();
                        s.b(a2, "positive", false);
                        boolean unused2 = k.this.d = false;
                        xVar.a(a2).b();
                    }
                });
            }
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean
                 arg types: [com.adcolony.sdk.k, int]
                 candidates:
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, com.adcolony.sdk.x):com.adcolony.sdk.x
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
                 arg types: [org.json.JSONObject, java.lang.String, int]
                 candidates:
                  com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
                  com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                public void onCancel(DialogInterface dialogInterface) {
                    AlertDialog unused = k.this.c = (AlertDialog) null;
                    boolean unused2 = k.this.d = false;
                    JSONObject a2 = s.a();
                    s.b(a2, "positive", false);
                    xVar.a(a2).b();
                }
            });
            ak.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean
                 arg types: [com.adcolony.sdk.k, int]
                 candidates:
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, com.adcolony.sdk.x):com.adcolony.sdk.x
                  com.adcolony.sdk.k.a(com.adcolony.sdk.k, boolean):boolean */
                public void run() {
                    boolean unused = k.this.d = true;
                    AlertDialog unused2 = k.this.c = builder.show();
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.b != null) {
            a(this.b);
            this.b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public AlertDialog b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(AlertDialog alertDialog) {
        this.c = alertDialog;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.d;
    }
}
