package com.adcolony.sdk;

import com.adcolony.sdk.u;
import org.json.JSONException;
import org.json.JSONObject;

class x {
    private String a;
    private JSONObject b;

    x(JSONObject jSONObject) {
        try {
            this.b = jSONObject;
            this.a = jSONObject.getString("m_type");
        } catch (JSONException e) {
            new u.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.h);
        }
    }

    x(String str, int i) {
        try {
            this.a = str;
            this.b = new JSONObject();
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new u.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.h);
        }
    }

    x(String str, int i, String str2) {
        try {
            this.a = str;
            this.b = s.a(str2);
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new u.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.h);
        }
    }

    x(String str, int i, JSONObject jSONObject) {
        try {
            this.a = str;
            this.b = jSONObject == null ? new JSONObject() : jSONObject;
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new u.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(u.h);
        }
    }

    /* access modifiers changed from: package-private */
    public x a() {
        return a((JSONObject) null);
    }

    /* access modifiers changed from: package-private */
    public x a(String str) {
        return a(s.a(str));
    }

    /* access modifiers changed from: package-private */
    public x a(JSONObject jSONObject) {
        try {
            x xVar = new x("reply", this.b.getInt("m_origin"), jSONObject);
            xVar.b.put("m_id", this.b.getInt("m_id"));
            return xVar;
        } catch (JSONException e) {
            new u.a().a("JSON error in ADCMessage's createReply(): ").a(e.toString()).a(u.h);
            return new x("JSONException", 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.a(this.a, this.b);
    }

    /* access modifiers changed from: package-private */
    public JSONObject c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.a = str;
    }
}
