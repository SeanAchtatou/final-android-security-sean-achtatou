package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.ak;
import com.adcolony.sdk.u;
import com.facebook.login.widget.ToolTipPopup;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executors;
import org.json.JSONObject;

class af implements Runnable {
    private final long a = 30000;
    private final int b = 17;
    private final int c = 15000;
    private final int d = 1000;
    private long e;
    private long f;
    private long g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;
    private boolean m = true;
    private boolean n = true;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public boolean s;
    private boolean t;

    af() {
    }

    public void a() {
        a.a("SessionInfo.stopped", new z() {
            public void a(x xVar) {
                boolean unused = af.this.s = true;
            }
        });
    }

    public void run() {
        long j2;
        while (!this.r) {
            this.h = System.currentTimeMillis();
            a.f();
            if (this.f >= 30000) {
                break;
            }
            if (!this.m) {
                if (this.o && !this.n) {
                    this.o = false;
                    f();
                }
                long j3 = this.f;
                if (this.l == 0) {
                    j2 = 0;
                } else {
                    j2 = System.currentTimeMillis() - this.l;
                }
                this.f = j3 + j2;
                this.l = System.currentTimeMillis();
            } else {
                if (this.o && this.n) {
                    this.o = false;
                    g();
                }
                this.f = 0;
                this.l = 0;
            }
            this.g = 17;
            a(this.g);
            this.i = System.currentTimeMillis() - this.h;
            if (this.i > 0 && this.i < ToolTipPopup.DEFAULT_POPUP_DISPLAY_TIME) {
                this.e += this.i;
            }
            h a2 = a.a();
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.k > MTGInterstitialActivity.WEB_LOAD_TIME) {
                this.k = currentTimeMillis;
            }
            if (a.d() && currentTimeMillis - this.j > 1000) {
                this.j = currentTimeMillis;
                String c2 = a2.p().c();
                if (!c2.equals(a2.w())) {
                    a2.a(c2);
                    JSONObject a3 = s.a();
                    s.a(a3, "network_type", a2.w());
                    new x("Network.on_status_change", 1, a3).b();
                }
            }
        }
        new u.a().a("AdColony session ending, releasing Context.").a(u.c);
        a.a().b(true);
        a.a((Context) null);
        this.q = true;
        this.t = true;
        b();
        ak.a aVar = new ak.a(10.0d);
        while (!this.s && !aVar.b() && this.t) {
            a.f();
            a(100);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (!this.p) {
            if (this.q) {
                a.a().b(false);
                this.q = false;
            }
            this.e = 0;
            this.f = 0;
            this.p = true;
            this.m = true;
            this.s = false;
            new Thread(this).start();
            if (z) {
                JSONObject a2 = s.a();
                s.a(a2, "id", ak.e());
                new x("SessionInfo.on_start", 1, a2).b();
                am amVar = (am) a.a().q().e().get(1);
                if (amVar != null) {
                    amVar.j();
                }
            }
            if (AdColony.a.isShutdown()) {
                AdColony.a = Executors.newSingleThreadExecutor();
            }
            w.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.p = false;
        this.m = false;
        if (w.l != null) {
            w.l.a();
        }
        JSONObject a2 = s.a();
        double d2 = (double) this.e;
        Double.isNaN(d2);
        s.a(a2, "session_length", d2 / 1000.0d);
        new x("SessionInfo.on_stop", 1, a2).b();
        a.f();
        AdColony.a.shutdown();
    }

    private void f() {
        b(false);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        ArrayList<aa> c2 = a.a().q().c();
        synchronized (c2) {
            Iterator<aa> it = c2.iterator();
            while (it.hasNext()) {
                JSONObject a2 = s.a();
                s.b(a2, "from_window_focus", z);
                new x("SessionInfo.on_pause", it.next().a(), a2).b();
            }
        }
        this.n = true;
        a.f();
    }

    private void g() {
        c(false);
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        ArrayList<aa> c2 = a.a().q().c();
        synchronized (c2) {
            Iterator<aa> it = c2.iterator();
            while (it.hasNext()) {
                JSONObject a2 = s.a();
                s.b(a2, "from_window_focus", z);
                new x("SessionInfo.on_resume", it.next().a(), a2).b();
            }
        }
        w.a();
        this.n = false;
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: package-private */
    public void e(boolean z) {
        this.o = z;
    }

    /* access modifiers changed from: package-private */
    public void f(boolean z) {
        this.t = z;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.p;
    }

    private void a(long j2) {
        try {
            Thread.sleep(j2);
        } catch (InterruptedException unused) {
        }
    }
}
