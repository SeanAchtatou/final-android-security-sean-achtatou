package com.adcolony.sdk;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adcolony.sdk.u;
import java.io.File;
import org.json.JSONObject;

public class AdColonyAdView extends FrameLayout {
    /* access modifiers changed from: private */
    public c a = a.a().l().b().get(this.d);
    private AdColonyAdViewListener b;
    private AdColonyAdSize c;
    /* access modifiers changed from: private */
    public String d;
    private String e;
    private String f;
    private ImageView g;
    private ac h;
    private x i;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private JSONObject t;

    AdColonyAdView(Context context, x xVar, AdColonyAdViewListener adColonyAdViewListener) {
        super(context);
        this.b = adColonyAdViewListener;
        this.e = adColonyAdViewListener.a();
        JSONObject c2 = xVar.c();
        this.t = c2;
        this.d = s.b(c2, "id");
        this.f = s.b(c2, "close_button_filepath");
        this.j = s.d(c2, "trusted_demand_source");
        this.n = s.d(c2, "close_button_snap_to_webview");
        this.r = s.c(c2, "close_button_width");
        this.s = s.c(c2, "close_button_height");
        this.c = adColonyAdViewListener.b();
        setLayoutParams(new FrameLayout.LayoutParams(this.a.o(), this.a.n()));
        setBackgroundColor(0);
        addView(this.a);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.h != null) {
            getWebView().k();
        }
    }

    public boolean destroy() {
        if (this.k) {
            new u.a().a("Ignoring duplicate call to destroy().").a(u.e);
            return false;
        }
        this.k = true;
        if (!(this.h == null || this.h.e() == null)) {
            this.h.a();
        }
        ak.a(new Runnable() {
            public void run() {
                Context c = a.c();
                if (c instanceof AdColonyAdViewActivity) {
                    ((AdColonyAdViewActivity) c).b();
                }
                d l = a.a().l();
                l.e().remove(AdColonyAdView.this.d);
                l.a(AdColonyAdView.this.a);
                JSONObject a2 = s.a();
                s.a(a2, "id", AdColonyAdView.this.d);
                new x("AdSession.on_ad_view_destroyed", 1, a2).b();
            }
        });
        return true;
    }

    public String getZoneId() {
        return this.e;
    }

    public void setListener(AdColonyAdViewListener adColonyAdViewListener) {
        this.b = adColonyAdViewListener;
    }

    public AdColonyAdViewListener getListener() {
        return this.b;
    }

    public AdColonyAdSize getAdSize() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean b() {
        if (this.j || this.m) {
            j m2 = a.a().m();
            int s2 = m2.s();
            int t2 = m2.t();
            int i2 = this.p > 0 ? this.p : s2;
            int i3 = this.q > 0 ? this.q : t2;
            int i4 = (s2 - i2) / 2;
            int i5 = (t2 - i3) / 2;
            this.a.setLayoutParams(new FrameLayout.LayoutParams(s2, t2));
            am webView = getWebView();
            if (webView != null) {
                x xVar = new x("WebView.set_bounds", 0);
                JSONObject a2 = s.a();
                s.b(a2, "x", i4);
                s.b(a2, "y", i5);
                s.b(a2, "width", i2);
                s.b(a2, "height", i3);
                xVar.b(a2);
                webView.b(xVar);
                float r2 = m2.r();
                JSONObject a3 = s.a();
                s.b(a3, "app_orientation", ak.j(ak.h()));
                s.b(a3, "width", (int) (((float) i2) / r2));
                s.b(a3, "height", (int) (((float) i3) / r2));
                s.b(a3, "x", ak.a(webView));
                s.b(a3, "y", ak.b(webView));
                s.a(a3, "ad_session_id", this.d);
                new x("MRAID.on_size_change", this.a.c(), a3).b();
            }
            if (this.g != null) {
                this.a.removeView(this.g);
            }
            final Context c2 = a.c();
            if (!(c2 == null || this.l || webView == null)) {
                float r3 = a.a().m().r();
                int i6 = (int) (((float) this.r) * r3);
                int i7 = (int) (((float) this.s) * r3);
                if (this.n) {
                    s2 = webView.u() + webView.s();
                }
                int v = this.n ? webView.v() : 0;
                this.g = new ImageView(c2.getApplicationContext());
                this.g.setImageURI(Uri.fromFile(new File(this.f)));
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i6, i7);
                layoutParams.setMargins(s2 - i6, v, 0, 0);
                this.g.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (c2 instanceof AdColonyAdViewActivity) {
                            ((AdColonyAdViewActivity) c2).b();
                        }
                    }
                });
                this.a.addView(this.g, layoutParams);
            }
            if (this.i != null) {
                JSONObject a4 = s.a();
                s.b(a4, "success", true);
                this.i.a(a4).b();
                this.i = null;
            }
            return true;
        }
        if (this.i != null) {
            JSONObject a5 = s.a();
            s.b(a5, "success", false);
            this.i.a(a5).b();
            this.i = null;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.j || this.m) {
            float r2 = a.a().m().r();
            this.a.setLayoutParams(new FrameLayout.LayoutParams((int) (((float) this.c.getWidth()) * r2), (int) (((float) this.c.getHeight()) * r2)));
            am webView = getWebView();
            if (webView != null) {
                x xVar = new x("WebView.set_bounds", 0);
                JSONObject a2 = s.a();
                s.b(a2, "x", webView.o());
                s.b(a2, "y", webView.p());
                s.b(a2, "width", webView.q());
                s.b(a2, "height", webView.r());
                xVar.b(a2);
                webView.b(xVar);
                JSONObject a3 = s.a();
                s.a(a3, "ad_session_id", this.d);
                new x("MRAID.on_close", this.a.c(), a3).b();
            }
            if (this.g != null) {
                this.a.removeView(this.g);
            }
            addView(this.a);
            if (this.b != null) {
                this.b.onClosed(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ac getOmidManager() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void setOmidManager(ac acVar) {
        this.h = acVar;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public String getAdSessionId() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public c getContainer() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void setNoCloseButton(boolean z) {
        this.l = this.j && z;
    }

    /* access modifiers changed from: package-private */
    public void setUserInteraction(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: package-private */
    public boolean getUserInteraction() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public boolean getTrustedDemandSource() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void setExpandedWidth(int i2) {
        this.p = (int) (((float) i2) * a.a().m().r());
    }

    /* access modifiers changed from: package-private */
    public void setExpandedHeight(int i2) {
        this.q = (int) (((float) i2) * a.a().m().r());
    }

    /* access modifiers changed from: package-private */
    public int getOrientation() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public void setOrientation(int i2) {
        this.o = i2;
    }

    /* access modifiers changed from: package-private */
    public am getWebView() {
        if (this.a == null) {
            return null;
        }
        return this.a.g().get(2);
    }

    /* access modifiers changed from: package-private */
    public void setExpandMessage(x xVar) {
        this.i = xVar;
    }
}
