package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.tapjoy.TJAdUnitConstants;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class n extends EditText {
    private c A;
    private x B;
    private final int a = 0;
    private final int b = 1;
    private final int c = 2;
    private final int d = 3;
    private final int e = 1;
    private final int f = 2;
    private final int g = 3;
    private final int h = 0;
    private final int i = 1;
    private final int j = 2;
    private final int k = 1;
    private final int l = 2;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private String z;

    /* access modifiers changed from: package-private */
    public int a(boolean z2, int i2) {
        switch (i2) {
            case 0:
                return z2 ? 1 : 16;
            case 1:
                if (z2) {
                    return GravityCompat.START;
                }
                return 48;
            case 2:
                if (z2) {
                    return GravityCompat.END;
                }
                return 80;
            default:
                return 17;
        }
    }

    private n(Context context) {
        super(context);
    }

    n(Context context, x xVar, int i2, c cVar) {
        super(context);
        this.m = i2;
        this.B = xVar;
        this.A = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(x xVar) {
        JSONObject c2 = xVar.c();
        this.u = s.c(c2, "x");
        this.v = s.c(c2, "y");
        setGravity(a(true, this.u) | a(false, this.v));
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        h a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = s.a();
        s.b(a3, "view_id", this.m);
        s.a(a3, "ad_session_id", this.w);
        s.b(a3, "container_x", this.n + x2);
        s.b(a3, "container_y", this.o + y2);
        s.b(a3, "view_x", x2);
        s.b(a3, "view_y", y2);
        s.b(a3, "id", this.A.d());
        switch (action) {
            case 0:
                new x("AdContainer.on_touch_began", this.A.c(), a3).b();
                break;
            case 1:
                if (!this.A.p()) {
                    a2.a(l2.e().get(this.w));
                }
                new x("AdContainer.on_touch_ended", this.A.c(), a3).b();
                break;
            case 2:
                new x("AdContainer.on_touch_moved", this.A.c(), a3).b();
                break;
            case 3:
                new x("AdContainer.on_touch_cancelled", this.A.c(), a3).b();
                break;
            case 5:
                int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", ((int) motionEvent.getX(action2)) + this.n);
                s.b(a3, "container_y", ((int) motionEvent.getY(action2)) + this.o);
                s.b(a3, "view_x", (int) motionEvent.getX(action2));
                s.b(a3, "view_y", (int) motionEvent.getY(action2));
                new x("AdContainer.on_touch_began", this.A.c(), a3).b();
                break;
            case 6:
                int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                s.b(a3, "container_x", ((int) motionEvent.getX(action3)) + this.n);
                s.b(a3, "container_y", ((int) motionEvent.getY(action3)) + this.o);
                s.b(a3, "view_x", (int) motionEvent.getX(action3));
                s.b(a3, "view_y", (int) motionEvent.getY(action3));
                if (!this.A.p()) {
                    a2.a(l2.e().get(this.w));
                }
                new x("AdContainer.on_touch_ended", this.A.c(), a3).b();
                break;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(x xVar) {
        JSONObject c2 = xVar.c();
        return s.c(c2, "id") == this.m && s.c(c2, "container_id") == this.A.d() && s.b(c2, "ad_session_id").equals(this.A.b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$3, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$4, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$5, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$6, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$7, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$8, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$9, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$10, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z
     arg types: [java.lang.String, com.adcolony.sdk.n$2, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.z, boolean):com.adcolony.sdk.z */
    /* access modifiers changed from: package-private */
    public void a() {
        JSONObject c2 = this.B.c();
        this.w = s.b(c2, "ad_session_id");
        this.n = s.c(c2, "x");
        this.o = s.c(c2, "y");
        this.p = s.c(c2, "width");
        this.q = s.c(c2, "height");
        this.s = s.c(c2, "font_family");
        this.r = s.c(c2, "font_style");
        this.t = s.c(c2, "font_size");
        this.x = s.b(c2, "background_color");
        this.y = s.b(c2, "font_color");
        this.z = s.b(c2, "text");
        this.u = s.c(c2, "align_x");
        this.v = s.c(c2, "align_y");
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.p, this.q);
        layoutParams.setMargins(this.n, this.o, 0, 0);
        layoutParams.gravity = 0;
        this.A.addView(this, layoutParams);
        switch (this.s) {
            case 0:
                setTypeface(Typeface.DEFAULT);
                break;
            case 1:
                setTypeface(Typeface.SERIF);
                break;
            case 2:
                setTypeface(Typeface.SANS_SERIF);
                break;
            case 3:
                setTypeface(Typeface.MONOSPACE);
                break;
        }
        switch (this.r) {
            case 0:
                setTypeface(getTypeface(), 0);
                break;
            case 1:
                setTypeface(getTypeface(), 1);
                break;
            case 2:
                setTypeface(getTypeface(), 2);
                break;
            case 3:
                setTypeface(getTypeface(), 3);
                break;
        }
        setText(this.z);
        setTextSize((float) this.t);
        setGravity(a(true, this.u) | a(false, this.v));
        if (!this.x.equals("")) {
            setBackgroundColor(ak.g(this.x));
        }
        if (!this.y.equals("")) {
            setTextColor(ak.g(this.y));
        }
        this.A.l().add(a.a("TextView.set_visible", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.k(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_bounds", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.d(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_font_color", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.f(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_background_color", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.e(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_typeface", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.j(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_font_size", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.g(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_font_style", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.h(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.get_text", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.c(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.set_text", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.i(xVar);
                }
            }
        }, true));
        this.A.l().add(a.a("TextView.align", (z) new z() {
            public void a(x xVar) {
                if (n.this.b(xVar)) {
                    n.this.a(xVar);
                }
            }
        }, true));
        this.A.m().add("TextView.set_visible");
        this.A.m().add("TextView.set_bounds");
        this.A.m().add("TextView.set_font_color");
        this.A.m().add("TextView.set_background_color");
        this.A.m().add("TextView.set_typeface");
        this.A.m().add("TextView.set_font_size");
        this.A.m().add("TextView.set_font_style");
        this.A.m().add("TextView.get_text");
        this.A.m().add("TextView.set_text");
        this.A.m().add("TextView.align");
    }

    /* access modifiers changed from: package-private */
    public void c(x xVar) {
        JSONObject a2 = s.a();
        s.a(a2, "text", getText().toString());
        xVar.a(a2).b();
    }

    /* access modifiers changed from: package-private */
    public void d(x xVar) {
        JSONObject c2 = xVar.c();
        this.n = s.c(c2, "x");
        this.o = s.c(c2, "y");
        this.p = s.c(c2, "width");
        this.q = s.c(c2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.n, this.o, 0, 0);
        layoutParams.width = this.p;
        layoutParams.height = this.q;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void e(x xVar) {
        this.x = s.b(xVar.c(), "background_color");
        setBackgroundColor(ak.g(this.x));
    }

    /* access modifiers changed from: package-private */
    public void f(x xVar) {
        this.y = s.b(xVar.c(), "font_color");
        setTextColor(ak.g(this.y));
    }

    /* access modifiers changed from: package-private */
    public void g(x xVar) {
        this.t = s.c(xVar.c(), "font_size");
        setTextSize((float) this.t);
    }

    /* access modifiers changed from: package-private */
    public void h(x xVar) {
        int c2 = s.c(xVar.c(), "font_style");
        this.r = c2;
        switch (c2) {
            case 0:
                setTypeface(getTypeface(), 0);
                return;
            case 1:
                setTypeface(getTypeface(), 1);
                return;
            case 2:
                setTypeface(getTypeface(), 2);
                return;
            case 3:
                setTypeface(getTypeface(), 3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void i(x xVar) {
        this.z = s.b(xVar.c(), "text");
        setText(this.z);
    }

    /* access modifiers changed from: package-private */
    public void j(x xVar) {
        int c2 = s.c(xVar.c(), "font_family");
        this.s = c2;
        switch (c2) {
            case 0:
                setTypeface(Typeface.DEFAULT);
                return;
            case 1:
                setTypeface(Typeface.SERIF);
                return;
            case 2:
                setTypeface(Typeface.SANS_SERIF);
                return;
            case 3:
                setTypeface(Typeface.MONOSPACE);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void k(x xVar) {
        if (s.d(xVar.c(), TJAdUnitConstants.String.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }
}
