package com.adcolony.sdk;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.adcolony.sdk.u;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyInterstitialActivity extends b {
    AdColonyInterstitial m;
    private g n;

    public AdColonyInterstitialActivity() {
        AdColonyInterstitial adColonyInterstitial;
        if (!a.b()) {
            adColonyInterstitial = null;
        } else {
            adColonyInterstitial = a.a().v();
        }
        this.m = adColonyInterstitial;
    }

    public /* bridge */ /* synthetic */ void onBackPressed() {
        super.onBackPressed();
    }

    public /* bridge */ /* synthetic */ void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public /* bridge */ /* synthetic */ void onDestroy() {
        super.onDestroy();
    }

    public /* bridge */ /* synthetic */ void onPause() {
        super.onPause();
    }

    public /* bridge */ /* synthetic */ void onResume() {
        super.onResume();
    }

    public /* bridge */ /* synthetic */ void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void onCreate(Bundle bundle) {
        this.d = this.m == null ? 0 : this.m.e();
        super.onCreate(bundle);
        if (a.b() && this.m != null) {
            ac h = this.m.h();
            if (h != null) {
                h.a(this.m.d());
            }
            this.n = new g(new Handler(Looper.getMainLooper()), this.m);
            if (this.m.getListener() != null) {
                this.m.getListener().onOpened(this.m);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(x xVar) {
        super.a(xVar);
        d l = a.a().l();
        JSONObject f = s.f(xVar.c(), "v4iap");
        JSONArray g = s.g(f, "product_ids");
        if (!(f == null || this.m == null || this.m.getListener() == null || g.length() <= 0)) {
            this.m.getListener().onIAPEvent(this.m, s.c(g, 0), s.c(f, "engagement_type"));
        }
        l.a(this.c);
        if (this.m != null) {
            l.c().remove(this.m.f());
        }
        if (!(this.m == null || this.m.getListener() == null)) {
            this.m.getListener().onClosed(this.m);
            this.m.a((c) null);
            this.m.setListener(null);
            this.m = null;
        }
        if (this.n != null) {
            this.n.a();
            this.n = null;
        }
        new u.a().a("finish_ad call finished").a(u.d);
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.m = adColonyInterstitial;
    }
}
