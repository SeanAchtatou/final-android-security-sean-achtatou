package com.adcolony.sdk;

import android.support.annotation.NonNull;
import org.json.JSONObject;

public class AdColonyAdOptions {
    boolean a;
    boolean b;
    AdColonyUserMetadata c;
    JSONObject d = s.a();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    public AdColonyAdOptions enableConfirmationDialog(boolean z) {
        this.a = z;
        s.b(this.d, "confirmation_enabled", true);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.s.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    public AdColonyAdOptions enableResultsDialog(boolean z) {
        this.b = z;
        s.b(this.d, "results_enabled", true);
        return this;
    }

    public AdColonyAdOptions setOption(@NonNull String str, boolean z) {
        if (ak.d(str)) {
            s.b(this.d, str, z);
        }
        return this;
    }

    public Object getOption(@NonNull String str) {
        return s.a(this.d, str);
    }

    public AdColonyAdOptions setOption(@NonNull String str, double d2) {
        if (ak.d(str)) {
            s.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAdOptions setOption(@NonNull String str, @NonNull String str2) {
        if (str != null && ak.d(str) && ak.d(str2)) {
            s.a(this.d, str, str2);
        }
        return this;
    }

    public AdColonyAdOptions setUserMetadata(@NonNull AdColonyUserMetadata adColonyUserMetadata) {
        this.c = adColonyUserMetadata;
        s.a(this.d, "user_metadata", adColonyUserMetadata.c);
        return this;
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.c;
    }
}
