package com.adcolony.sdk;

import com.adcolony.sdk.v;
import java.util.Date;
import org.json.JSONObject;

class o extends v {
    static final r a = new r("adcolony_fatal_reports", "4.1.2", "Production");
    static final String b = "sourceFile";
    static final String c = "lineNumber";
    static final String d = "methodName";
    static final String e = "stackTrace";
    static final String f = "isAdActive";
    static final String g = "activeAdId";
    static final String h = "active_creative_ad_id";
    static final String i = "listOfCachedAds";
    static final String j = "listOfCreativeAdIds";
    static final String k = "adCacheSize";
    /* access modifiers changed from: private */
    public JSONObject p;

    o() {
    }

    /* access modifiers changed from: package-private */
    public o a(JSONObject jSONObject) {
        a aVar = new a();
        aVar.a(jSONObject);
        aVar.a(s.b(jSONObject, "message"));
        try {
            aVar.a(new Date(Long.parseLong(s.b(jSONObject, "timestamp"))));
        } catch (NumberFormatException unused) {
        }
        aVar.a(a);
        aVar.a(-1);
        return (o) aVar.a();
    }

    /* access modifiers changed from: package-private */
    public JSONObject a() {
        return this.p;
    }

    private class a extends v.a {
        a() {
            this.b = new o();
        }

        /* access modifiers changed from: package-private */
        public a a(JSONObject jSONObject) {
            JSONObject unused = ((o) this.b).p = jSONObject;
            return this;
        }

        /* access modifiers changed from: package-private */
        public v.a a(Date date) {
            s.a(((o) this.b).p, "timestamp", v.l.format(date));
            return super.a(date);
        }
    }
}
