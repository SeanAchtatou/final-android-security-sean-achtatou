package com.unity3d.services.core.properties;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import com.unity3d.services.core.log.DeviceLog;
import java.io.ByteArrayInputStream;
import java.lang.ref.WeakReference;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;

public class ClientProperties {
    private static final X500Principal DEBUG_CERT = new X500Principal("CN=Android Debug,O=Android,C=US");
    private static WeakReference<Activity> _activity;
    private static Application _application;
    private static Context _applicationContext;
    private static String _gameId;

    public static Activity getActivity() {
        return _activity.get();
    }

    public static void setActivity(Activity activity) {
        _activity = new WeakReference<>(activity);
    }

    public static Context getApplicationContext() {
        return _applicationContext;
    }

    public static void setApplicationContext(Context context) {
        _applicationContext = context;
    }

    public static Application getApplication() {
        return _application;
    }

    public static void setApplication(Application application) {
        _application = application;
    }

    public static String getGameId() {
        return _gameId;
    }

    public static void setGameId(String str) {
        _gameId = str;
    }

    public static String getAppName() {
        return _applicationContext.getPackageName();
    }

    public static String getAppVersion() {
        String packageName = getApplicationContext().getPackageName();
        PackageManager packageManager = getApplicationContext().getPackageManager();
        try {
            if (packageManager.getPackageInfo(packageName, 0).versionName == null) {
                return "FakeVersionName";
            }
            return packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            DeviceLog.exception("Error getting package info", e);
            return null;
        }
    }

    public static boolean isAppDebuggable() {
        boolean z;
        int i = 0;
        if (getApplicationContext() == null) {
            return false;
        }
        PackageManager packageManager = getApplicationContext().getPackageManager();
        String packageName = getApplicationContext().getPackageName();
        boolean z2 = true;
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);
            int i2 = applicationInfo.flags & 2;
            applicationInfo.flags = i2;
            if (i2 == 0) {
                z2 = false;
            }
            z = z2;
            z2 = false;
        } catch (PackageManager.NameNotFoundException e) {
            DeviceLog.exception("Could not find name", e);
            z = false;
        }
        if (!z2) {
            return z;
        }
        try {
            Signature[] signatureArr = packageManager.getPackageInfo(packageName, 64).signatures;
            int length = signatureArr.length;
            while (i < length) {
                boolean equals = ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(signatureArr[i].toByteArray()))).getSubjectX500Principal().equals(DEBUG_CERT);
                if (equals) {
                    return equals;
                }
                i++;
                z = equals;
            }
            return z;
        } catch (PackageManager.NameNotFoundException e2) {
            DeviceLog.exception("Could not find name", e2);
            return z;
        } catch (CertificateException e3) {
            DeviceLog.exception("Certificate exception", e3);
            return z;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:6|7|8|9|(2:10|11)|16|17|22|18|4) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0025 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x003e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONArray areClassesPresent(org.json.JSONArray r7) {
        /*
            if (r7 != 0) goto L_0x0008
            org.json.JSONArray r7 = new org.json.JSONArray
            r7.<init>()
            return r7
        L_0x0008:
            org.json.JSONArray r0 = new org.json.JSONArray
            r0.<init>()
            r1 = 0
            r2 = 0
        L_0x000f:
            int r3 = r7.length()
            if (r2 >= r3) goto L_0x0044
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            java.lang.String r4 = ""
            java.lang.Object r5 = r7.get(r2)     // Catch:{ Exception -> 0x0025 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0025 }
            r4 = r5
        L_0x0025:
            java.lang.Class.forName(r4)     // Catch:{ Exception -> 0x0034 }
            java.lang.String r5 = "class"
            r3.put(r5, r4)     // Catch:{ Exception -> 0x0034 }
            java.lang.String r5 = "found"
            r6 = 1
            r3.put(r5, r6)     // Catch:{ Exception -> 0x0034 }
            goto L_0x003e
        L_0x0034:
            java.lang.String r5 = "class"
            r3.put(r5, r4)     // Catch:{ Exception -> 0x003e }
            java.lang.String r4 = "found"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x003e }
        L_0x003e:
            r0.put(r3)     // Catch:{ Exception -> 0x0041 }
        L_0x0041:
            int r2 = r2 + 1
            goto L_0x000f
        L_0x0044:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unity3d.services.core.properties.ClientProperties.areClassesPresent(org.json.JSONArray):org.json.JSONArray");
    }
}
