package com.unity3d.services.banners;

import android.content.Context;
import android.content.res.Resources;
import com.unity3d.services.core.misc.ViewUtilities;

public class UnityBannerSize {
    private int height;
    private int width;

    public UnityBannerSize(int i, int i2) {
        this.width = i;
        this.height = i2;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public static UnityBannerSize getDynamicSize(Context context) {
        return new UnityBannerSize(BannerSize.access$000(BannerSize.BANNER_SIZE_DYNAMIC, context), BannerSize.access$100(BannerSize.BANNER_SIZE_DYNAMIC, context));
    }

    private enum BannerSize {
        BANNER_SIZE_STANDARD,
        BANNER_SIZE_LEADERBOARD,
        BANNER_SIZE_IAB_STANDARD,
        BANNER_SIZE_DYNAMIC;
        
        private static final int IAB_STANDARD_HEIGHT = 60;
        private static final int IAB_STANDARD_WIDTH = 468;
        private static final int LEADERBOARD_HEIGHT = 90;
        private static final int LEADERBOARD_WIDTH = 728;
        private static final int STANDARD_HEIGHT = 50;
        private static final int STANDARD_WIDTH = 320;

        private BannerSize getNonDynamicSize(Context context) {
            if (this != BANNER_SIZE_DYNAMIC) {
                return this;
            }
            int round = Math.round(ViewUtilities.dpFromPx(context, (float) Resources.getSystem().getDisplayMetrics().widthPixels));
            if (round >= LEADERBOARD_WIDTH) {
                return BANNER_SIZE_LEADERBOARD;
            }
            if (round >= IAB_STANDARD_WIDTH) {
                return BANNER_SIZE_IAB_STANDARD;
            }
            return BANNER_SIZE_STANDARD;
        }

        private int getWidth(Context context) {
            switch (getNonDynamicSize(context)) {
                case BANNER_SIZE_STANDARD:
                    return STANDARD_WIDTH;
                case BANNER_SIZE_LEADERBOARD:
                    return LEADERBOARD_WIDTH;
                case BANNER_SIZE_IAB_STANDARD:
                    return IAB_STANDARD_WIDTH;
                default:
                    return STANDARD_WIDTH;
            }
        }

        private int getHeight(Context context) {
            switch (getNonDynamicSize(context)) {
                case BANNER_SIZE_STANDARD:
                    return 50;
                case BANNER_SIZE_LEADERBOARD:
                    return 90;
                case BANNER_SIZE_IAB_STANDARD:
                    return 60;
                default:
                    return 50;
            }
        }
    }
}
