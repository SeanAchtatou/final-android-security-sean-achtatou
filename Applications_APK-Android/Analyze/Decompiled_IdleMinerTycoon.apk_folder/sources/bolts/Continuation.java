package bolts;

public interface Continuation<TTaskResult, TContinuationResult> {
    TContinuationResult then(Task task) throws Exception;
}
