package im.getsocial.sdk;

public interface GlobalErrorListener {
    void onError(GetSocialException getSocialException);
}
