package im.getsocial.sdk.iap.a.b;

/* compiled from: PurchaseReceipt */
public final class jjbQypPegg {
    public final String acquisition;
    public final byte[] attribution;
    public final byte[] getsocial;
    public final boolean mobile;
    public final boolean retention;

    jjbQypPegg(byte[] bArr, byte[] bArr2, String str, boolean z, boolean z2) {
        this.getsocial = bArr;
        this.attribution = bArr2 == null ? new byte[0] : bArr2;
        this.acquisition = str == null ? "" : str;
        this.mobile = z;
        this.retention = z2;
    }

    public static C0012jjbQypPegg getsocial() {
        return new C0012jjbQypPegg();
    }

    /* renamed from: im.getsocial.sdk.iap.a.b.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: PurchaseReceipt */
    public static final class C0012jjbQypPegg {
        private String acquisition;
        private byte[] attribution;
        private byte[] getsocial;
        private boolean mobile;
        private boolean retention;

        C0012jjbQypPegg() {
        }

        public final C0012jjbQypPegg getsocial(byte[] bArr) {
            this.getsocial = (byte[]) bArr.clone();
            return this;
        }

        public final C0012jjbQypPegg attribution(byte[] bArr) {
            this.attribution = bArr == null ? null : (byte[]) bArr.clone();
            return this;
        }

        public final C0012jjbQypPegg getsocial(String str) {
            this.acquisition = str;
            return this;
        }

        public final C0012jjbQypPegg getsocial(boolean z) {
            this.mobile = z;
            return this;
        }

        public final C0012jjbQypPegg attribution(boolean z) {
            this.retention = z;
            return this;
        }

        public final jjbQypPegg getsocial() {
            return new jjbQypPegg(this.getsocial, this.attribution, this.acquisition, this.mobile, this.retention);
        }
    }
}
