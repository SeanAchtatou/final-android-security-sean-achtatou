package im.getsocial.sdk.iap;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;

public class PurchaseData {
    /* access modifiers changed from: private */
    public String acquisition;
    /* access modifiers changed from: private */
    public ProductType attribution;
    /* access modifiers changed from: private */
    public long cat = 0;
    /* access modifiers changed from: private */
    public long dau;
    /* access modifiers changed from: private */
    public String getsocial;
    /* access modifiers changed from: private */
    public String mau;
    /* access modifiers changed from: private */
    public float mobile;
    /* access modifiers changed from: private */
    public String retention;

    public enum ProductType {
        ITEM("item"),
        SUBSCRIPTION("subscription");
        
        private final String _productType;

        private ProductType(String str) {
            this._productType = str;
        }

        public final String toString() {
            return this._productType.toLowerCase();
        }
    }

    PurchaseData() {
    }

    public String getProductId() {
        return this.getsocial;
    }

    public ProductType getProductType() {
        return this.attribution;
    }

    public String getProductTitle() {
        return this.acquisition;
    }

    public float getPrice() {
        return this.mobile;
    }

    public String getPriceCurrency() {
        return this.retention;
    }

    public long getPurchaseDate() {
        return this.dau;
    }

    public String getPurchaseId() {
        return this.mau;
    }

    public long getValidationResult() {
        return this.cat;
    }

    public boolean canSend() {
        return jjbQypPegg.attribution(this.getsocial) && jjbQypPegg.getsocial(this.attribution) && jjbQypPegg.attribution(this.acquisition) && jjbQypPegg.attribution(this.retention) && jjbQypPegg.attribution(this.getsocial);
    }

    public static class Builder {
        PurchaseData getsocial = new PurchaseData();

        public String getProductId() {
            return this.getsocial.getsocial;
        }

        public Builder withProductId(String str) {
            String unused = this.getsocial.getsocial = str;
            return this;
        }

        public Builder withProductType(ProductType productType) {
            ProductType unused = this.getsocial.attribution = productType;
            return this;
        }

        public Builder withPrice(float f) {
            float unused = this.getsocial.mobile = f;
            return this;
        }

        public Builder withPriceCurrency(String str) {
            String unused = this.getsocial.retention = str;
            return this;
        }

        public Builder withPurchaseDate(long j) {
            long unused = this.getsocial.dau = j;
            return this;
        }

        public Builder withProductTitle(String str) {
            String unused = this.getsocial.acquisition = str;
            return this;
        }

        public Builder withPurchaseId(String str) {
            String unused = this.getsocial.mau = str;
            return this;
        }

        public Builder withValidationResult(long j) {
            long unused = this.getsocial.cat = j;
            return this;
        }

        public PurchaseData build() {
            return this.getsocial;
        }
    }
}
