package im.getsocial.sdk.usermanagement;

import android.graphics.Bitmap;
import java.util.Map;

/* compiled from: UserUpdateAccessHelper */
public class XdbacJlTDQ {
    private final UserUpdate getsocial;

    public XdbacJlTDQ(UserUpdate userUpdate) {
        this.getsocial = userUpdate;
    }

    public final String getsocial() {
        return this.getsocial.getsocial;
    }

    public final String attribution() {
        return this.getsocial.attribution;
    }

    public final Map<String, String> acquisition() {
        return this.getsocial.mobile;
    }

    public final Map<String, String> mobile() {
        return this.getsocial.retention;
    }

    public final Map<String, String> retention() {
        return this.getsocial.dau;
    }

    public final Map<String, String> dau() {
        return this.getsocial.mau;
    }

    public final Bitmap mau() {
        return this.getsocial.acquisition;
    }
}
