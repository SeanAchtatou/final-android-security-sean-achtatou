package im.getsocial.sdk.usermanagement.a.b;

import com.ironsource.sdk.constants.Constants;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT;
import im.getsocial.sdk.usermanagement.upgqDBbsrL;
import java.util.Map;

/* compiled from: CommonUserManagementFunctions */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static void getsocial(AuthIdentity authIdentity) {
        upgqDBbsrL upgqdbbsrl = new upgqDBbsrL(authIdentity);
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(authIdentity), "AuthIdentity cannot be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(upgqdbbsrl.getsocial()), "ProviderId cannot be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(upgqdbbsrl.acquisition()), "AccessToken cannot be null or empty");
        if (!"facebook".equals(upgqdbbsrl.getsocial())) {
            boolean attribution = im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(upgqdbbsrl.attribution());
            jjbQypPegg.C0021jjbQypPegg.getsocial(attribution, "User ID cannot be null or empty for provider " + upgqdbbsrl.getsocial());
        }
    }

    public static void getsocial(pdwpUtZXDT pdwputzxdt) {
        getsocial(pdwputzxdt.mobile(), "Public Property");
        getsocial(pdwputzxdt.retention(), "Private Property");
        getsocial(pdwputzxdt.dau(), "Internal Public Property");
        getsocial(pdwputzxdt.mau(), "internal Private Property");
        getsocial(pdwputzxdt.attribution(), 2048);
        getsocial(pdwputzxdt.getsocial(), 32);
    }

    private static void getsocial(String str, int i) {
        if (str != null) {
            boolean z = str.length() <= i;
            jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Avatar URL length should be less or equal " + i + ":" + str);
        }
    }

    private static void getsocial(Map<String, String> map, String str) {
        for (Map.Entry next : map.entrySet()) {
            String str2 = (String) next.getKey();
            String str3 = (String) next.getValue();
            boolean z = im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str2);
            jjbQypPegg.C0021jjbQypPegg.getsocial(z, str + " can not have null key: " + str2 + Constants.RequestParameters.EQUAL + str3);
            boolean z2 = im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str3);
            jjbQypPegg.C0021jjbQypPegg.getsocial(z2, str + " can not have null value: " + str2 + Constants.RequestParameters.EQUAL + str3);
            boolean z3 = false;
            boolean z4 = str2.length() <= 64;
            jjbQypPegg.C0021jjbQypPegg.getsocial(z4, str + " key length should be less or equal 64" + ":" + str2);
            if (str3.length() <= 1024) {
                z3 = true;
            }
            jjbQypPegg.C0021jjbQypPegg.getsocial(z3, str + " value length should be less or equal 1024" + ":" + str3);
        }
    }
}
