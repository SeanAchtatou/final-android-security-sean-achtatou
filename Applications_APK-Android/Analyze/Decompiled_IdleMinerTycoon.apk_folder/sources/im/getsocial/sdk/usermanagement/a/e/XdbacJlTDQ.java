package im.getsocial.sdk.usermanagement.a.e;

import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.i.jjbQypPegg;
import im.getsocial.sdk.internal.c.k.upgqDBbsrL;
import im.getsocial.sdk.internal.c.vkXhnjhKGp;
import im.getsocial.sdk.usermanagement.OnUserChangedListener;

/* compiled from: FireUserChangeListener */
public final class XdbacJlTDQ implements upgqDBbsrL {
    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    vkXhnjhKGp attribution;
    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    jjbQypPegg getsocial;

    public XdbacJlTDQ() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial() {
        final OnUserChangedListener acquisition = this.getsocial.acquisition();
        if (acquisition != null) {
            this.attribution.getsocial(new Runnable() {
                public void run() {
                    acquisition.onUserChanged();
                }
            });
        }
    }
}
