package im.getsocial.sdk.usermanagement;

/* compiled from: IdentityAccessHelper */
public class upgqDBbsrL {
    private final AuthIdentity getsocial;

    public upgqDBbsrL(AuthIdentity authIdentity) {
        this.getsocial = authIdentity;
    }

    public final String getsocial() {
        return this.getsocial.acquisition();
    }

    public final String attribution() {
        return this.getsocial.getsocial();
    }

    public final String acquisition() {
        return this.getsocial.attribution();
    }
}
