package im.getsocial.sdk.usermanagement.a.a;

import im.getsocial.sdk.internal.c.JbBdMtJmlU;

/* compiled from: AuthRequest */
public class jjbQypPegg {
    private String acquisition;
    private String attribution;
    private String getsocial;
    private JbBdMtJmlU mobile;
    private String retention;

    public final String getsocial() {
        return this.getsocial;
    }

    public final void getsocial(String str) {
        this.getsocial = str;
    }

    public final String attribution() {
        return this.attribution;
    }

    public final void attribution(String str) {
        this.attribution = str;
    }

    public final String acquisition() {
        return this.acquisition;
    }

    public final void acquisition(String str) {
        this.acquisition = str;
    }

    public final JbBdMtJmlU mobile() {
        return this.mobile;
    }

    public final void getsocial(JbBdMtJmlU jbBdMtJmlU) {
        this.mobile = jbBdMtJmlU;
    }

    public final String retention() {
        return this.retention;
    }

    public final void mobile(String str) {
        this.retention = str;
    }
}
