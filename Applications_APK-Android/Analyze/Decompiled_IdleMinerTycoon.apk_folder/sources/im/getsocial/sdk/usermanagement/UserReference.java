package im.getsocial.sdk.usermanagement;

public class UserReference {
    protected String acquisition = null;
    protected String attribution = null;
    protected String getsocial = null;

    public String getId() {
        return this.getsocial;
    }

    public String getDisplayName() {
        return this.attribution;
    }

    public String getAvatarUrl() {
        return this.acquisition;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserReference)) {
            return false;
        }
        UserReference userReference = (UserReference) obj;
        if (!this.getsocial.equals(userReference.getsocial)) {
            return false;
        }
        if (this.attribution == null ? userReference.attribution != null : !this.attribution.equals(userReference.attribution)) {
            return false;
        }
        if (this.acquisition != null) {
            return this.acquisition.equals(userReference.acquisition);
        }
        return userReference.acquisition == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial.hashCode() * 31) + (this.attribution != null ? this.attribution.hashCode() : 0)) * 31;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "UserReference{_userId='" + this.getsocial + '\'' + ", _displayName='" + this.attribution + '\'' + ", _avatarUrl='" + this.acquisition + '\'' + '}';
    }

    public static class Builder {
        UserReference getsocial = new UserReference();

        public Builder(String str) {
            this.getsocial.getsocial = str;
        }

        public Builder setAvatarUrl(String str) {
            this.getsocial.acquisition = str;
            return this;
        }

        public Builder setDisplayName(String str) {
            this.getsocial.attribution = str;
            return this;
        }

        public UserReference build() {
            return this.getsocial;
        }
    }
}
