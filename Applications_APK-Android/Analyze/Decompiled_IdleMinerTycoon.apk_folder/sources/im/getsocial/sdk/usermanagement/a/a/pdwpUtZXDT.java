package im.getsocial.sdk.usermanagement.a.a;

import im.getsocial.sdk.internal.k.a.upgqDBbsrL;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UserUpdateInternal */
public class pdwpUtZXDT {
    private upgqDBbsrL acquisition;
    private String attribution;
    private Map<String, String> dau = new HashMap();
    private String getsocial;
    private Map<String, String> mau = new HashMap();
    private Map<String, String> mobile = new HashMap();
    private Map<String, String> retention = new HashMap();

    public final pdwpUtZXDT getsocial(String str) {
        this.getsocial = str;
        return this;
    }

    public final pdwpUtZXDT attribution(String str) {
        this.attribution = str;
        return this;
    }

    public final pdwpUtZXDT getsocial(upgqDBbsrL upgqdbbsrl) {
        this.acquisition = upgqdbbsrl;
        return this;
    }

    public final pdwpUtZXDT getsocial(Map<String, String> map) {
        this.mobile = new HashMap(map);
        return this;
    }

    public final pdwpUtZXDT attribution(Map<String, String> map) {
        this.dau = new HashMap(map);
        return this;
    }

    public final pdwpUtZXDT acquisition(Map<String, String> map) {
        this.retention = new HashMap(map);
        return this;
    }

    public final pdwpUtZXDT mobile(Map<String, String> map) {
        this.mau = new HashMap(map);
        return this;
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final String attribution() {
        return this.attribution;
    }

    public final upgqDBbsrL acquisition() {
        return this.acquisition;
    }

    public final Map<String, String> mobile() {
        return this.mobile;
    }

    public final Map<String, String> retention() {
        return this.retention;
    }

    public final Map<String, String> dau() {
        return this.dau;
    }

    public final Map<String, String> mau() {
        return this.mau;
    }
}
