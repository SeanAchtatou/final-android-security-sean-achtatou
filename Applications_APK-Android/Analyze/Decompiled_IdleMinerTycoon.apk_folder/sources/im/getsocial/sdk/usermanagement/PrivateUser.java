package im.getsocial.sdk.usermanagement;

import im.getsocial.sdk.internal.c.l.upgqDBbsrL;
import java.util.Collections;
import java.util.Map;

public class PrivateUser extends PublicUser {
    Map<String, String> getsocial = null;
    /* access modifiers changed from: private */
    public String organic = null;
    /* access modifiers changed from: private */
    public Map<String, String> viral = null;

    PrivateUser() {
    }

    public static Builder builder(String str) {
        return new Builder(str);
    }

    public boolean hasPrivateProperty(String str) {
        return this.viral.containsKey(str);
    }

    public String getPrivateProperty(String str) {
        return this.viral.get(str);
    }

    public Map<String, String> getAllPrivateProperties() {
        return Collections.unmodifiableMap(this.viral);
    }

    public String getPassword() {
        return this.organic;
    }

    public int hashCode() {
        return (this.viral.hashCode() * 31) + this.organic.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PrivateUser privateUser = (PrivateUser) obj;
        if (super.equals(privateUser) && this.viral.equals(privateUser.viral)) {
            return this.organic.equals(privateUser.organic);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void getsocial(PrivateUser privateUser) {
        getsocial((PublicUser) privateUser);
        privateUser.viral = upgqDBbsrL.getsocial(this.viral);
        privateUser.getsocial = upgqDBbsrL.getsocial(this.getsocial);
        privateUser.organic = this.organic;
    }

    public static class Builder {
        protected final PrivateUser getsocial = new PrivateUser();

        public Builder(String str) {
            this.getsocial.attribution = str;
        }

        public Builder setAvatarUrl(String str) {
            this.getsocial.mobile = str;
            return this;
        }

        public Builder setDisplayName(String str) {
            this.getsocial.acquisition = str;
            return this;
        }

        public Builder setIdentities(Map<String, String> map) {
            this.getsocial.retention = map;
            return this;
        }

        public Builder setPublicProperties(Map<String, String> map) {
            this.getsocial.dau = map;
            return this;
        }

        public Builder setPassword(String str) {
            String unused = this.getsocial.organic = str;
            return this;
        }

        public Builder setPrivateProperties(Map<String, String> map) {
            Map unused = this.getsocial.viral = map;
            return this;
        }

        public PrivateUser build() {
            PrivateUser privateUser = new PrivateUser();
            this.getsocial.getsocial(privateUser);
            return privateUser;
        }
    }
}
