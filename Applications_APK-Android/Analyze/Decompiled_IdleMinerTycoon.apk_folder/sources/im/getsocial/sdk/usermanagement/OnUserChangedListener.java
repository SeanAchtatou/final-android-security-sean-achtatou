package im.getsocial.sdk.usermanagement;

public interface OnUserChangedListener {
    void onUserChanged();
}
