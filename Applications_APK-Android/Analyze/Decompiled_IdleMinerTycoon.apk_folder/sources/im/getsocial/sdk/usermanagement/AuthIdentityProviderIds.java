package im.getsocial.sdk.usermanagement;

public final class AuthIdentityProviderIds {
    public static final String FACEBOOK = "facebook";
    public static final String GOOGLE_PLAY = "googleplay";
    public static final String GOOGLE_PLUS = "googleplus";

    private AuthIdentityProviderIds() {
    }
}
