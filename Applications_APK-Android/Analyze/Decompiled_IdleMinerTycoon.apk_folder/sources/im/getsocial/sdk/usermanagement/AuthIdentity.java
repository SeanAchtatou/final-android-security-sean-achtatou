package im.getsocial.sdk.usermanagement;

public final class AuthIdentity {
    private final String acquisition;
    private final String attribution;
    private final String getsocial;

    private AuthIdentity(String str, String str2, String str3) {
        String str4;
        if (str == null) {
            str4 = null;
        } else {
            str4 = str.toLowerCase();
        }
        this.getsocial = str4;
        this.attribution = str2;
        this.acquisition = str3;
    }

    public static AuthIdentity createFacebookIdentity(String str) {
        return getsocial("facebook", null, str);
    }

    public static AuthIdentity createCustomIdentity(String str, String str2, String str3) {
        return getsocial(str, str2, str3);
    }

    private static AuthIdentity getsocial(String str, String str2, String str3) {
        return new AuthIdentity(str, str2, str3);
    }

    /* access modifiers changed from: package-private */
    public final String getsocial() {
        return this.attribution;
    }

    /* access modifiers changed from: package-private */
    public final String attribution() {
        return this.acquisition;
    }

    /* access modifiers changed from: package-private */
    public final String acquisition() {
        return this.getsocial;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AuthIdentity)) {
            return false;
        }
        AuthIdentity authIdentity = (AuthIdentity) obj;
        if (!this.getsocial.equals(authIdentity.getsocial)) {
            return false;
        }
        if (this.attribution == null ? authIdentity.attribution == null : this.attribution.equals(authIdentity.attribution)) {
            return this.acquisition.equals(authIdentity.acquisition);
        }
        return false;
    }

    public final int hashCode() {
        return (((this.getsocial.hashCode() * 31) + (this.attribution != null ? this.attribution.hashCode() : 0)) * 31) + this.acquisition.hashCode();
    }

    public final String toString() {
        return "AuthIdentity: [\nProvider Name: " + this.getsocial + ",\nUser ID: " + this.attribution + ",\nAccessToken: " + this.acquisition + ",\n];";
    }
}
