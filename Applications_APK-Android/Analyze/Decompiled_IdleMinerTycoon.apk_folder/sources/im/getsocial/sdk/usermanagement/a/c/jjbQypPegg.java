package im.getsocial.sdk.usermanagement.a.c;

import im.getsocial.sdk.internal.c.b.KluUZYuxme;
import im.getsocial.sdk.internal.c.b.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.PrivateUser;
import im.getsocial.sdk.usermanagement.a.a.cjrhisSQCL;

/* compiled from: UserManagementUserRepo */
public class jjbQypPegg implements KluUZYuxme {
    private C0026jjbQypPegg attribution;
    private PrivateUser getsocial;

    public final qdyNCsqjKt attribution() {
        return qdyNCsqjKt.USER;
    }

    public final PrivateUser getsocial() {
        return this.getsocial;
    }

    public final void getsocial(PrivateUser privateUser) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(privateUser), "Missing required parameter: privateUser");
        this.getsocial = privateUser;
    }

    public final void getsocial(AuthIdentity authIdentity, cjrhisSQCL cjrhissqcl) {
        this.attribution = new C0026jjbQypPegg(authIdentity, cjrhissqcl);
    }

    public final void acquisition() {
        this.attribution = null;
    }

    public final cjrhisSQCL getsocial(AuthIdentity authIdentity) {
        if (this.attribution != null && this.attribution.getsocial.equals(authIdentity)) {
            return this.attribution.attribution;
        }
        return null;
    }

    /* renamed from: im.getsocial.sdk.usermanagement.a.c.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: UserManagementUserRepo */
    private static final class C0026jjbQypPegg {
        final cjrhisSQCL attribution;
        final AuthIdentity getsocial;

        C0026jjbQypPegg(AuthIdentity authIdentity, cjrhisSQCL cjrhissqcl) {
            this.getsocial = authIdentity;
            this.attribution = cjrhissqcl;
        }
    }
}
