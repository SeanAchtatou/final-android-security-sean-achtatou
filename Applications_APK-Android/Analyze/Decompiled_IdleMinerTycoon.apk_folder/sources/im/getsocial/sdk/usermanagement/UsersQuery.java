package im.getsocial.sdk.usermanagement;

public final class UsersQuery {
    private int attribution = 20;
    private final String getsocial;

    private UsersQuery(String str) {
        this.getsocial = str;
    }

    public static UsersQuery usersByDisplayName(String str) {
        return new UsersQuery(str);
    }

    public final UsersQuery withLimit(int i) {
        this.attribution = i;
        return this;
    }

    public final String getQuery() {
        return this.getsocial;
    }

    public final int getLimit() {
        return this.attribution;
    }
}
