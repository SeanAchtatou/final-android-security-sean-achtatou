package im.getsocial.sdk.usermanagement.a.d;

import im.getsocial.sdk.internal.c.EmkjBpiUfq;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.KSZKMmRWhZ;
import im.getsocial.sdk.internal.f.a.CyDeXbQkhA;
import im.getsocial.sdk.internal.f.a.FvojpKUsoc;
import im.getsocial.sdk.internal.f.a.GiuMozhmHE;
import im.getsocial.sdk.internal.f.a.KkSvQPDhNi;
import im.getsocial.sdk.internal.f.a.RbduBRVrSj;
import im.getsocial.sdk.internal.f.a.SIWJGMbZKs;
import im.getsocial.sdk.internal.f.a.eTZqdsudqh;
import im.getsocial.sdk.internal.f.a.gnmRLOtbDV;
import im.getsocial.sdk.internal.f.a.iqXBPEYHZB;
import im.getsocial.sdk.internal.f.a.jMsobIMeui;
import im.getsocial.sdk.internal.f.a.ruWsnwUPKh;
import im.getsocial.sdk.internal.f.a.xEJQVTBATI;
import im.getsocial.sdk.internal.m.QCXFOjcJkE;
import im.getsocial.sdk.pushnotifications.a.b.pdwpUtZXDT;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.PrivateUser;
import im.getsocial.sdk.usermanagement.PublicUser;
import im.getsocial.sdk.usermanagement.UserReference;
import im.getsocial.sdk.usermanagement.UsersQuery;
import im.getsocial.sdk.usermanagement.a.a.upgqDBbsrL;
import im.getsocial.sdk.usermanagement.cjrhisSQCL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ThriftyUserManagementConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static RbduBRVrSj getsocial(im.getsocial.sdk.usermanagement.a.a.jjbQypPegg jjbqyppegg) {
        RbduBRVrSj rbduBRVrSj = new RbduBRVrSj();
        rbduBRVrSj.attribution = jjbqyppegg.attribution();
        rbduBRVrSj.getsocial = jjbqyppegg.getsocial();
        rbduBRVrSj.acquisition = jjbqyppegg.acquisition();
        rbduBRVrSj.mobile = getsocial(jjbqyppegg.mobile());
        rbduBRVrSj.retention = jjbqyppegg.retention();
        return rbduBRVrSj;
    }

    public static eTZqdsudqh getsocial(JbBdMtJmlU jbBdMtJmlU) {
        eTZqdsudqh etzqdsudqh = new eTZqdsudqh();
        etzqdsudqh.attribution = jbBdMtJmlU.attribution();
        etzqdsudqh.retention = jbBdMtJmlU.acquisition();
        etzqdsudqh.acquisition = jbBdMtJmlU.retention();
        etzqdsudqh.mobile = jbBdMtJmlU.dau();
        etzqdsudqh.dau = SIWJGMbZKs.valueOf(jbBdMtJmlU.cat());
        etzqdsudqh.mau = jbBdMtJmlU.viral();
        etzqdsudqh.viral = jbBdMtJmlU.organic();
        etzqdsudqh.cat = jbBdMtJmlU.mau();
        etzqdsudqh.organic = jbBdMtJmlU.growth();
        etzqdsudqh.getsocial = jbBdMtJmlU.unity();
        etzqdsudqh.growth = jbBdMtJmlU.android();
        etzqdsudqh.f484android = getsocial(jbBdMtJmlU.getsocial());
        etzqdsudqh.ios = jbBdMtJmlU.mobile();
        etzqdsudqh.unity = jbBdMtJmlU.referral();
        etzqdsudqh.connect = jbBdMtJmlU.engage();
        etzqdsudqh.engage = jbBdMtJmlU.marketing();
        etzqdsudqh.referral = jbBdMtJmlU.invites();
        etzqdsudqh.marketing = jbBdMtJmlU.feeds();
        etzqdsudqh.invites = null;
        etzqdsudqh.feeds = jbBdMtJmlU.social().getsocial();
        etzqdsudqh.sharing = jbBdMtJmlU.social().attribution();
        etzqdsudqh.social = Boolean.valueOf(jbBdMtJmlU.a());
        return etzqdsudqh;
    }

    public static KkSvQPDhNi getsocial(im.getsocial.sdk.internal.c.KkSvQPDhNi kkSvQPDhNi) {
        switch (kkSvQPDhNi) {
            case ANDROID:
                return KkSvQPDhNi.ANDROID;
            case IOS:
                return KkSvQPDhNi.IOS;
            default:
                return KkSvQPDhNi.OTHER;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, long):long
     arg types: [java.lang.Long, int]
     candidates:
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, int):int
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.String, long):long
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.util.List, im.getsocial.sdk.internal.m.QCXFOjcJkE$jjbQypPegg):java.util.List<R>
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, int):int
     arg types: [java.lang.Long, int]
     candidates:
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, long):long
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.String, long):long
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.util.List, im.getsocial.sdk.internal.m.QCXFOjcJkE$jjbQypPegg):java.util.List<R>
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, int):int */
    public static upgqDBbsrL getsocial(CyDeXbQkhA cyDeXbQkhA) {
        upgqDBbsrL upgqdbbsrl = new upgqDBbsrL();
        upgqdbbsrl.getsocial(cyDeXbQkhA.getsocial);
        upgqdbbsrl.getsocial(QCXFOjcJkE.attribution(cyDeXbQkhA.acquisition));
        upgqdbbsrl.getsocial(getsocial(cyDeXbQkhA.attribution));
        ruWsnwUPKh ruwsnwupkh = cyDeXbQkhA.mobile;
        boolean z = false;
        boolean z2 = ruwsnwupkh != null;
        if (z2 && QCXFOjcJkE.getsocial(ruwsnwupkh.f486android)) {
            z = true;
        }
        upgqdbbsrl.getsocial(new pdwpUtZXDT(z, (!z2 || ruwsnwupkh.organic == null) ? "" : ruwsnwupkh.organic));
        upgqdbbsrl.getsocial(QCXFOjcJkE.getsocial(cyDeXbQkhA.retention));
        jMsobIMeui jmsobimeui = cyDeXbQkhA.dau;
        upgqdbbsrl.getsocial(new KSZKMmRWhZ(jmsobimeui.getsocial, jmsobimeui.attribution, jmsobimeui.acquisition));
        EmkjBpiUfq emkjBpiUfq = new EmkjBpiUfq(cyDeXbQkhA.mau, QCXFOjcJkE.getsocial((Number) cyDeXbQkhA.viral, 10485760L));
        xEJQVTBATI xejqvtbati = cyDeXbQkhA.cat;
        if (xejqvtbati != null) {
            emkjBpiUfq.getsocial("WIFI", QCXFOjcJkE.getsocial((Number) xejqvtbati.getsocial, 512000));
            emkjBpiUfq.getsocial("LTE", QCXFOjcJkE.getsocial((Number) xejqvtbati.attribution, 512000));
            emkjBpiUfq.getsocial("3G", QCXFOjcJkE.getsocial((Number) xejqvtbati.acquisition, 512000));
            emkjBpiUfq.getsocial("OTHER", QCXFOjcJkE.getsocial((Number) xejqvtbati.mobile, 512000));
        }
        upgqdbbsrl.getsocial(emkjBpiUfq);
        return upgqdbbsrl;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.usermanagement.pdwpUtZXDT.getsocial(im.getsocial.sdk.usermanagement.PublicUser, java.util.Map<java.lang.String, java.lang.String>):void
     arg types: [im.getsocial.sdk.usermanagement.PrivateUser, java.util.Map<java.lang.String, java.lang.String>]
     candidates:
      im.getsocial.sdk.usermanagement.cjrhisSQCL.getsocial(im.getsocial.sdk.usermanagement.PrivateUser, java.util.Map<java.lang.String, java.lang.String>):void
      im.getsocial.sdk.usermanagement.pdwpUtZXDT.getsocial(im.getsocial.sdk.usermanagement.PublicUser, boolean):void
      im.getsocial.sdk.usermanagement.pdwpUtZXDT.getsocial(im.getsocial.sdk.usermanagement.PublicUser, java.util.Map<java.lang.String, java.lang.String>):void */
    public static PrivateUser getsocial(iqXBPEYHZB iqxbpeyhzb) {
        PrivateUser.Builder builder = new PrivateUser.Builder(iqxbpeyhzb.getsocial);
        builder.setPassword(iqxbpeyhzb.attribution);
        builder.setDisplayName(iqxbpeyhzb.acquisition);
        builder.setAvatarUrl(iqxbpeyhzb.mobile);
        builder.setIdentities(getsocial(iqxbpeyhzb.cat));
        builder.setPublicProperties(iqxbpeyhzb.retention);
        builder.setPrivateProperties(iqxbpeyhzb.dau);
        PrivateUser build = builder.build();
        cjrhisSQCL.getsocial(build, iqxbpeyhzb.viral);
        cjrhisSQCL.getsocial((PublicUser) build, iqxbpeyhzb.mau);
        return build;
    }

    public static Map<String, String> getsocial(List<im.getsocial.sdk.internal.f.a.JbBdMtJmlU> list) {
        HashMap hashMap = new HashMap();
        if (list != null) {
            for (im.getsocial.sdk.internal.f.a.JbBdMtJmlU next : list) {
                hashMap.put(next.getsocial, next.attribution);
            }
        }
        return hashMap;
    }

    public static im.getsocial.sdk.internal.f.a.JbBdMtJmlU getsocial(AuthIdentity authIdentity) {
        im.getsocial.sdk.usermanagement.upgqDBbsrL upgqdbbsrl = new im.getsocial.sdk.usermanagement.upgqDBbsrL(authIdentity);
        im.getsocial.sdk.internal.f.a.JbBdMtJmlU jbBdMtJmlU = new im.getsocial.sdk.internal.f.a.JbBdMtJmlU();
        jbBdMtJmlU.acquisition = upgqdbbsrl.acquisition();
        jbBdMtJmlU.getsocial = upgqdbbsrl.getsocial();
        jbBdMtJmlU.attribution = upgqdbbsrl.attribution();
        return jbBdMtJmlU;
    }

    public static PublicUser getsocial(FvojpKUsoc fvojpKUsoc) {
        PublicUser build = new PublicUser.Builder(fvojpKUsoc.getsocial).setAvatarUrl(fvojpKUsoc.acquisition).setDisplayName(fvojpKUsoc.attribution).setIdentities(getsocial(fvojpKUsoc.mobile)).setPublicProperties(fvojpKUsoc.retention).build();
        im.getsocial.sdk.usermanagement.pdwpUtZXDT.getsocial(build, fvojpKUsoc.dau);
        return build;
    }

    public static UserReference getsocial(gnmRLOtbDV gnmrlotbdv) {
        return new UserReference.Builder(gnmrlotbdv.getsocial).setAvatarUrl(gnmrlotbdv.acquisition).setDisplayName(gnmrlotbdv.attribution).build();
    }

    public static iqXBPEYHZB getsocial(im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT pdwputzxdt) {
        iqXBPEYHZB iqxbpeyhzb = new iqXBPEYHZB();
        iqxbpeyhzb.acquisition = pdwputzxdt.getsocial();
        iqxbpeyhzb.mobile = pdwputzxdt.attribution();
        iqxbpeyhzb.retention = pdwputzxdt.mobile();
        iqxbpeyhzb.dau = pdwputzxdt.retention();
        iqxbpeyhzb.mau = pdwputzxdt.dau();
        iqxbpeyhzb.viral = pdwputzxdt.mau();
        return iqxbpeyhzb;
    }

    public static GiuMozhmHE getsocial(UsersQuery usersQuery) {
        GiuMozhmHE giuMozhmHE = new GiuMozhmHE();
        giuMozhmHE.attribution = usersQuery.getQuery();
        giuMozhmHE.getsocial = Integer.valueOf(usersQuery.getLimit());
        return giuMozhmHE;
    }
}
