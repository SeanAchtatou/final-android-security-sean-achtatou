package im.getsocial.sdk.usermanagement.a.e;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.m.qZypgoeblR;
import im.getsocial.sdk.usermanagement.AddAuthIdentityCallback;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.ConflictUser;
import im.getsocial.sdk.usermanagement.a.a.cjrhisSQCL;

/* compiled from: AddAuthIdentityUseCase */
public final class jjbQypPegg extends im.getsocial.sdk.internal.c.k.jjbQypPegg {
    @XdbacJlTDQ
    im.getsocial.sdk.usermanagement.a.c.jjbQypPegg getsocial;

    public final void getsocial(final AuthIdentity authIdentity, final AddAuthIdentityCallback addAuthIdentityCallback) {
        im.getsocial.sdk.usermanagement.a.b.jjbQypPegg.getsocial(authIdentity);
        getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.getsocial.getsocial(jjbQypPegg.this.getsocial().getsocial(authIdentity));
            }
        }, new CompletionCallback() {
            public void onSuccess() {
                addAuthIdentityCallback.onComplete();
            }

            public void onFailure(GetSocialException getSocialException) {
                if (qZypgoeblR.getsocial(getSocialException, 101)) {
                    jjbQypPegg.attribution.attribution("AddUserIdentityUseCase let's fetch conflict user");
                    jjbQypPegg.this.getsocial(new jjbQypPegg.C0020jjbQypPegg<ConflictUser>(authIdentity) {
                        public final /* synthetic */ Object getsocial() {
                            return im.getsocial.sdk.usermanagement.jjbQypPegg.getsocial(jjbQypPegg.this.getsocial().attribution(r3));
                        }
                    }, new Callback<ConflictUser>(authIdentity, addAuthIdentityCallback) {
                        public /* synthetic */ void onSuccess(Object obj) {
                            ConflictUser conflictUser = (ConflictUser) obj;
                            jjbQypPegg.this.getsocial.getsocial(r3, new cjrhisSQCL(conflictUser.getId(), conflictUser.getPassword()));
                            r4.onConflict(conflictUser);
                        }

                        public void onFailure(GetSocialException getSocialException) {
                            if (getSocialException.getErrorCode() == 204) {
                                getSocialException = new GetSocialException(ErrorCode.USERID_TOKEN_MISMATCH, "UserId and token do not match");
                            }
                            r4.onFailure(getSocialException);
                        }
                    });
                    return;
                }
                addAuthIdentityCallback.onFailure(getSocialException);
            }
        });
    }
}
