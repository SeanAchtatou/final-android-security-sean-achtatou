package im.getsocial.sdk.usermanagement;

import android.graphics.Bitmap;
import java.util.HashMap;
import java.util.Map;

public class UserUpdate {
    Bitmap acquisition;
    String attribution;
    final Map<String, String> dau = new HashMap();
    String getsocial;
    final Map<String, String> mau = new HashMap();
    final Map<String, String> mobile = new HashMap();
    final Map<String, String> retention = new HashMap();

    UserUpdate() {
    }

    public static Builder createBuilder() {
        return new Builder((byte) 0);
    }

    public static class Builder {
        UserUpdate getsocial;

        /* synthetic */ Builder(byte b) {
            this();
        }

        private Builder() {
            this.getsocial = new UserUpdate();
        }

        public Builder updateDisplayName(String str) {
            this.getsocial.getsocial = str;
            return this;
        }

        public Builder updateAvatarUrl(String str) {
            this.getsocial.attribution = str;
            this.getsocial.acquisition = null;
            return this;
        }

        public Builder updateAvatar(Bitmap bitmap) {
            this.getsocial.acquisition = bitmap;
            this.getsocial.attribution = null;
            return this;
        }

        public Builder setPublicProperty(String str, String str2) {
            this.getsocial.mobile.put(str, str2);
            return this;
        }

        public Builder removePublicProperty(String str) {
            this.getsocial.mobile.put(str, "");
            return this;
        }

        public Builder setPrivateProperty(String str, String str2) {
            this.getsocial.retention.put(str, str2);
            return this;
        }

        public Builder removePrivateProperty(String str) {
            this.getsocial.retention.put(str, "");
            return this;
        }

        public UserUpdate build() {
            UserUpdate userUpdate = new UserUpdate();
            userUpdate.getsocial = this.getsocial.getsocial;
            userUpdate.attribution = this.getsocial.attribution;
            userUpdate.acquisition = this.getsocial.acquisition;
            userUpdate.mobile.putAll(this.getsocial.mobile);
            userUpdate.retention.putAll(this.getsocial.retention);
            userUpdate.dau.putAll(this.getsocial.dau);
            userUpdate.mau.putAll(this.getsocial.mau);
            return userUpdate;
        }
    }
}
