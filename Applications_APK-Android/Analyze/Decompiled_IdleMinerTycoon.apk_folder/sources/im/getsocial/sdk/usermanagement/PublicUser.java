package im.getsocial.sdk.usermanagement;

import im.getsocial.sdk.internal.c.l.upgqDBbsrL;
import java.util.Collections;
import java.util.Map;

public class PublicUser {
    /* access modifiers changed from: protected */
    public String acquisition = null;
    /* access modifiers changed from: protected */
    public String attribution = null;
    boolean cat;
    /* access modifiers changed from: protected */
    public Map<String, String> dau = null;
    Map<String, String> mau = null;
    /* access modifiers changed from: protected */
    public String mobile = null;
    /* access modifiers changed from: protected */
    public Map<String, String> retention = null;

    protected PublicUser() {
    }

    @Deprecated
    public Map<String, String> getIdentities() {
        return this.retention;
    }

    public Map<String, String> getAuthIdentities() {
        return this.retention;
    }

    public boolean hasPublicProperty(String str) {
        return this.dau.containsKey(str);
    }

    public String getPublicProperty(String str) {
        return this.dau.get(str);
    }

    public String getId() {
        return this.attribution;
    }

    public Map<String, String> getAllPublicProperties() {
        return Collections.unmodifiableMap(this.dau);
    }

    public String getDisplayName() {
        return this.acquisition;
    }

    public String getAvatarUrl() {
        return this.mobile;
    }

    /* access modifiers changed from: protected */
    public final void getsocial(PublicUser publicUser) {
        publicUser.attribution = this.attribution;
        publicUser.acquisition = this.acquisition;
        publicUser.mobile = this.mobile;
        publicUser.retention = upgqDBbsrL.getsocial(this.retention);
        publicUser.dau = upgqDBbsrL.getsocial(this.dau);
        publicUser.mau = upgqDBbsrL.getsocial(this.mau);
        publicUser.cat = this.cat;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PublicUser publicUser = (PublicUser) obj;
        if (!this.attribution.equals(publicUser.attribution) || this.cat != publicUser.cat) {
            return false;
        }
        if (this.acquisition == null ? publicUser.acquisition != null : !this.acquisition.equals(publicUser.acquisition)) {
            return false;
        }
        if (this.mobile == null ? publicUser.mobile != null : !this.mobile.equals(publicUser.mobile)) {
            return false;
        }
        if (this.retention.equals(publicUser.retention) && this.dau.equals(publicUser.dau)) {
            return this.mau.equals(publicUser.mau);
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.attribution.hashCode() * 31) + (this.acquisition != null ? this.acquisition.hashCode() : 0)) * 31;
        if (this.mobile != null) {
            i = this.mobile.hashCode();
        }
        return ((((((((hashCode + i) * 31) + this.retention.hashCode()) * 31) + this.dau.hashCode()) * 31) + this.mau.hashCode()) * 31) + (this.cat ? 1 : 0);
    }

    public static class Builder {
        PublicUser getsocial = new PublicUser();

        public Builder(String str) {
            this.getsocial.attribution = str;
        }

        public Builder setAvatarUrl(String str) {
            this.getsocial.mobile = str;
            return this;
        }

        public Builder setDisplayName(String str) {
            this.getsocial.acquisition = str;
            return this;
        }

        public Builder setIdentities(Map<String, String> map) {
            this.getsocial.retention = map;
            return this;
        }

        public Builder setPublicProperties(Map<String, String> map) {
            this.getsocial.dau = map;
            return this;
        }

        public PublicUser build() {
            PublicUser publicUser = new PublicUser();
            this.getsocial.getsocial(publicUser);
            return publicUser;
        }
    }
}
