package im.getsocial.sdk.pushnotifications;

public final class SendNotificationPlaceholders {

    public static final class CustomText {
        public static final String RECEIVER_DISPLAY_NAME = "[RECEIVER_DISPLAY_NAME]";
        public static final String SENDER_DISPLAY_NAME = "[SENDER_DISPLAY_NAME]";
    }

    public static final class Receivers {
        public static final String FRIENDS = "friends";
        public static final String REFERRED_USERS = "referred_users";
        public static final String REFERRER = "referrer";
    }
}
