package im.getsocial.sdk.pushnotifications;

import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.media.MediaAttachment;
import im.getsocial.sdk.media.jjbQypPegg;
import im.getsocial.sdk.pushnotifications.a.b.upgqDBbsrL;
import java.util.List;
import java.util.Map;

public final class NotificationContent {
    private final upgqDBbsrL getsocial = new upgqDBbsrL();

    private NotificationContent() {
    }

    public static NotificationContent notificationWithText(String str) {
        return new NotificationContent().withText(str);
    }

    public static NotificationContent notificationFromTemplate(String str) {
        return new NotificationContent().withTemplateName(str);
    }

    public final NotificationContent withAction(Action action) {
        this.getsocial.mau = action;
        return this;
    }

    public final NotificationContent withTitle(String str) {
        this.getsocial.attribution = str;
        return this;
    }

    public final NotificationContent withText(String str) {
        this.getsocial.getsocial = str;
        return this;
    }

    public final NotificationContent withTemplateName(String str) {
        this.getsocial.mobile = str;
        return this;
    }

    public final NotificationContent addTemplatePlaceholder(String str, String str2) {
        this.getsocial.dau.put(str, str2);
        return this;
    }

    public final NotificationContent addTemplatePlaceholders(Map<String, String> map) {
        this.getsocial.dau.putAll(map);
        return this;
    }

    public final NotificationContent withMediaAttachment(MediaAttachment mediaAttachment) {
        this.getsocial.acquisition = jjbQypPegg.getsocial(mediaAttachment);
        return this;
    }

    public final NotificationContent addActionButton(ActionButton actionButton) {
        this.getsocial.cat.add(actionButton);
        return this;
    }

    public final NotificationContent addActionButtons(List<ActionButton> list) {
        this.getsocial.cat.addAll(list);
        return this;
    }

    public final NotificationContent withCustomization(NotificationCustomization notificationCustomization) {
        this.getsocial.retention = notificationCustomization;
        return this;
    }

    /* access modifiers changed from: package-private */
    public final upgqDBbsrL getsocial() {
        return this.getsocial;
    }
}
