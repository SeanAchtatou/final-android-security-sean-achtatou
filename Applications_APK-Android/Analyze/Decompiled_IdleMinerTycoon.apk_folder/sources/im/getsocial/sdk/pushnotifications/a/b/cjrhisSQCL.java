package im.getsocial.sdk.pushnotifications.a.b;

import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NotificationQueryData */
public final class cjrhisSQCL {
    public String[] acquisition;
    public String[] attribution;
    public String dau;
    public int getsocial;
    public final List<String> mobile = new ArrayList();
    public NotificationsQuery.Filter retention;

    public cjrhisSQCL(List<String> list) {
        this.mobile.addAll(list);
    }
}
