package im.getsocial.sdk.pushnotifications;

public interface NotificationListener {
    boolean onNotificationReceived(Notification notification, boolean z);
}
