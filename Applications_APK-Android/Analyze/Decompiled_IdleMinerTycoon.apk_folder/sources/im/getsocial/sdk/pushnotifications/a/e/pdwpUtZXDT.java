package im.getsocial.sdk.pushnotifications.a.e;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.widget.RemoteViews;
import com.appsflyer.share.Constants;
import com.onesignal.OneSignalDbContract;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.PushNotificationsReceiver;
import im.getsocial.sdk.R;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;
import im.getsocial.sdk.internal.m.cjrhisSQCL;
import im.getsocial.sdk.internal.m.zoToeBNOjF;
import im.getsocial.sdk.pushnotifications.Notification;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ReceiveNotificationStrategy */
class pdwpUtZXDT extends cjrhisSQCL {
    private static final Collection<cjrhisSQCL> cat = Arrays.asList(cjrhisSQCL.PAUSED, cjrhisSQCL.NOT_STARTED);
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL mau = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(pdwpUtZXDT.class);
    private static final Set<String> viral = new HashSet();
    @XdbacJlTDQ
    zoToeBNOjF acquisition;
    @XdbacJlTDQ
    sqEuGXwfLT attribution;
    @XdbacJlTDQ
    qdyNCsqjKt dau;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.g.jjbQypPegg getsocial;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.d.jjbQypPegg mobile;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.upgqDBbsrL retention;

    /* compiled from: ReceiveNotificationStrategy */
    interface upgqDBbsrL {
        void getsocial(Bitmap bitmap);
    }

    pdwpUtZXDT() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(final Context context, Intent intent) {
        Intent intent2;
        im.getsocial.sdk.pushnotifications.a.b.jjbQypPegg jjbqyppegg = getsocial(intent);
        boolean z = true;
        if (jjbqyppegg == null || jjbqyppegg.getsocial()) {
            mau.getsocial("Received notification:\n%s", jjbqyppegg);
            return;
        }
        final im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF zotoebnojf = (im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF) jjbqyppegg;
        mau.getsocial("Received notification:\n%s", zotoebnojf.organic());
        if (viral.add(zotoebnojf.android())) {
            this.getsocial.getsocial("push_notification_received", zotoebnojf.attribution());
            if (!zotoebnojf.acquisition(this.dau.attribution("user_id"))) {
                mau.mobile("Notification received for another user, won't show up.");
                return;
            }
            boolean contains = cat.contains(this.acquisition.getsocial());
            boolean attribution2 = attribution();
            mau.getsocial("App in background: %b, show notifications in foreground: %b", Boolean.valueOf(contains), Boolean.valueOf(attribution2));
            if (contains || attribution2) {
                mau.attribution("Show notification");
                if (Build.VERSION.SDK_INT >= 26) {
                    intent2 = new Intent(context, PushNotificationsReceiver.class).setAction("im.getsocial.sdk.intent.RECEIVE");
                } else {
                    intent2 = new Intent("im.getsocial.sdk.intent.RECEIVE");
                }
                Intent data = intent2.setData(Uri.parse("getSocialNotificationId://" + zotoebnojf.android()));
                im.getsocial.sdk.pushnotifications.a.cjrhisSQCL.getsocial(data, zotoebnojf);
                final jjbQypPegg jjbqyppegg2 = new jjbQypPegg(this, (byte) 0);
                jjbqyppegg2.getsocial = (int) (System.currentTimeMillis() % 2147483647L);
                int i = this.attribution.getsocial("im.getsocial.sdk.NotificationIcon", 0);
                if (i == 0) {
                    i = context.getResources().getIdentifier("getsocial_notification_icon", "drawable", context.getApplicationInfo().packageName);
                }
                if (i == 0) {
                    i = context.getApplicationInfo().icon;
                }
                jjbqyppegg2.attribution = i;
                jjbqyppegg2.acquisition = getsocial(context, "im.getsocial.sdk.LargeNotificationIcon");
                final PendingIntent broadcast = PendingIntent.getBroadcast(context, jjbqyppegg2.getsocial, data, 134217728);
                Notification acquisition2 = zotoebnojf.acquisition();
                jjbqyppegg2.mobile = im.getsocial.sdk.internal.c.l.ztWNWCuZiM.getsocial(acquisition2.getTitle()) ? context.getString(context.getApplicationInfo().labelRes) : acquisition2.getTitle();
                jjbqyppegg2.retention = acquisition2.getText();
                jjbqyppegg2.mau = acquisition2.getId();
                if (acquisition2.getCustomization() != null) {
                    jjbqyppegg2.organic = acquisition2.getCustomization().getTitleColor();
                    jjbqyppegg2.growth = acquisition2.getCustomization().getTextColor();
                    jjbqyppegg2.cat = acquisition2.getCustomization().getBackgroundImageConfiguration();
                }
                if (!(Build.VERSION.SDK_INT >= 24) || (jjbqyppegg2.cat == null && this.attribution.getsocial("im.getsocial.sdk.NotificationBackgroundImage", 0) == 0)) {
                    z = false;
                }
                jjbqyppegg2.f493android = z;
                if (!jjbqyppegg2.f493android) {
                    getsocial(acquisition2.getImageUrl(), new upgqDBbsrL() {
                        public final void getsocial(Bitmap bitmap) {
                            jjbqyppegg2.dau = bitmap;
                            pdwpUtZXDT.this.getsocial(context, broadcast, jjbqyppegg2);
                        }
                    });
                } else if (jjbqyppegg2.cat == null || !jjbqyppegg2.cat.contains("drawable")) {
                    getsocial(jjbqyppegg2.cat, new upgqDBbsrL() {
                        public final void getsocial(Bitmap bitmap) {
                            jjbqyppegg2.viral = bitmap;
                            pdwpUtZXDT.this.getsocial(context, broadcast, jjbqyppegg2);
                        }
                    });
                } else {
                    getsocial(context, broadcast, jjbqyppegg2);
                }
            } else {
                mau.attribution("Pass notification to listener");
                this.mobile.getsocial(new Runnable() {
                    public void run() {
                        im.getsocial.sdk.internal.cjrhisSQCL.getsocial(pdwpUtZXDT.this.retention, zotoebnojf);
                    }
                });
            }
        } else {
            mau.getsocial("Notification %s already in received: %s", zotoebnojf.android(), viral);
        }
    }

    private void getsocial(String str, final upgqDBbsrL upgqdbbsrl) {
        if (str == null) {
            upgqdbbsrl.getsocial(null);
        } else {
            im.getsocial.sdk.internal.g.jjbQypPegg.getsocial(str).getsocial(new Callback<Bitmap>() {
                public /* synthetic */ void onSuccess(Object obj) {
                    upgqdbbsrl.getsocial((Bitmap) obj);
                }

                public void onFailure(GetSocialException getSocialException) {
                    upgqdbbsrl.getsocial(null);
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, int):int
      im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, boolean):boolean */
    private boolean attribution() {
        return this.attribution.getsocial("im.getsocial.sdk.ShowNotificationInForeground", false);
    }

    private Bitmap getsocial(Context context, String str) {
        int i = this.attribution.getsocial(str, 0);
        if (i == 0) {
            return null;
        }
        return BitmapFactory.decodeResource(context.getResources(), i);
    }

    /* access modifiers changed from: private */
    public int attribution(Context context, String str) {
        int i = this.attribution.getsocial(str, 0);
        return (Build.VERSION.SDK_INT < 23 || i == 0) ? ViewCompat.MEASURED_STATE_MASK : context.getResources().getColor(i, null);
    }

    private static im.getsocial.sdk.pushnotifications.a.b.jjbQypPegg getsocial(Intent intent) {
        try {
            if (intent.hasExtra("gs_data")) {
                return im.getsocial.sdk.pushnotifications.a.b.jjbQypPegg.getsocial(intent.getStringExtra("gs_data"));
            }
            return im.getsocial.sdk.pushnotifications.a.cjrhisSQCL.attribution(intent);
        } catch (Exception unused) {
            return null;
        }
    }

    /* compiled from: ReceiveNotificationStrategy */
    private class jjbQypPegg {
        Bitmap acquisition;

        /* renamed from: android  reason: collision with root package name */
        boolean f493android;
        int attribution;
        String cat;
        Bitmap dau;
        int getsocial;
        String growth;
        String mau;
        CharSequence mobile;
        String organic;
        CharSequence retention;
        Bitmap viral;

        private jjbQypPegg() {
        }

        /* synthetic */ jjbQypPegg(pdwpUtZXDT pdwputzxdt, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        public final int getsocial(Context context) {
            if (this.organic == null || this.organic.length() == 0) {
                return pdwpUtZXDT.this.attribution(context, "im.getsocial.sdk.NotificationTitleColor");
            }
            try {
                return Color.parseColor(this.organic);
            } catch (IllegalArgumentException e) {
                pdwpUtZXDT.mau.acquisition(e);
                return -1;
            }
        }

        /* access modifiers changed from: package-private */
        public final int attribution(Context context) {
            if (this.growth == null || this.growth.length() == 0) {
                return pdwpUtZXDT.this.attribution(context, "im.getsocial.sdk.NotificationMessageColor");
            }
            try {
                return Color.parseColor(this.growth);
            } catch (IllegalArgumentException e) {
                pdwpUtZXDT.mau.acquisition(e);
                return -1;
            }
        }
    }

    /* access modifiers changed from: private */
    public void getsocial(Context context, PendingIntent pendingIntent, jjbQypPegg jjbqyppegg) {
        android.app.Notification notification;
        Bitmap bitmap;
        Notification.Builder contentIntent = (Build.VERSION.SDK_INT >= 26 ? new Notification.Builder(context, "getsocial_channel_id") : new Notification.Builder(context)).setSmallIcon(jjbqyppegg.attribution).setLargeIcon(jjbqyppegg.acquisition).setTicker(jjbqyppegg.retention).setWhen(System.currentTimeMillis()).setSound(RingtoneManager.getDefaultUri(2)).setAutoCancel(true).setContentTitle(jjbqyppegg.mobile).setContentText(jjbqyppegg.retention).setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT < 16) {
            notification = contentIntent.getNotification();
        } else {
            if (Build.VERSION.SDK_INT < 26 && attribution()) {
                contentIntent.setPriority(1);
            }
            if (jjbqyppegg.dau == null) {
                contentIntent.setStyle(new Notification.BigTextStyle().bigText(jjbqyppegg.retention).setBigContentTitle(jjbqyppegg.mobile));
            } else {
                contentIntent.setStyle(new Notification.BigPictureStyle().bigPicture(jjbqyppegg.dau));
            }
            if (jjbqyppegg.f493android && Build.VERSION.SDK_INT >= 24) {
                RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.custom_notification_layout);
                remoteViews.setTextViewText(R.id.notification_title, jjbqyppegg.mobile);
                remoteViews.setTextColor(R.id.notification_title, jjbqyppegg.getsocial(context));
                remoteViews.setTextViewText(R.id.notification_message, jjbqyppegg.retention);
                remoteViews.setTextColor(R.id.notification_message, jjbqyppegg.attribution(context));
                int i = R.id.notification_background;
                if (jjbqyppegg.viral == null) {
                    pdwpUtZXDT pdwputzxdt = pdwpUtZXDT.this;
                    String str = jjbqyppegg.cat;
                    if (str == null || !str.contains("drawable")) {
                        bitmap = pdwputzxdt.getsocial(context, "im.getsocial.sdk.NotificationBackgroundImage");
                    } else {
                        int identifier = context.getResources().getIdentifier(str.substring(str.indexOf(Constants.URL_PATH_DELIMITER) + 1), "drawable", context.getApplicationInfo().packageName);
                        if (identifier == 0) {
                            bitmap = null;
                        } else {
                            bitmap = BitmapFactory.decodeResource(context.getResources(), identifier);
                        }
                    }
                } else {
                    bitmap = jjbqyppegg.viral;
                }
                remoteViews.setImageViewBitmap(i, bitmap);
                contentIntent.setCustomContentView(remoteViews);
                contentIntent.setStyle(null);
            }
            notification = contentIntent.build();
        }
        int i2 = jjbqyppegg.getsocial;
        String str2 = jjbqyppegg.mau;
        mau.getsocial("Save notification to local storage(systemId=%s, getsocialId=%s)", String.valueOf(i2), str2);
        this.dau.getsocial("push_notification_id_" + str2, i2);
        ((NotificationManager) context.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME)).notify(jjbqyppegg.getsocial, notification);
    }
}
