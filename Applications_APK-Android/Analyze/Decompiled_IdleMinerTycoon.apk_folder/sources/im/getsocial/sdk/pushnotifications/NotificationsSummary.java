package im.getsocial.sdk.pushnotifications;

public class NotificationsSummary {
    private final int getsocial;

    public NotificationsSummary(int i) {
        this.getsocial = i;
    }

    public int getSuccessfullySentCount() {
        return this.getsocial;
    }
}
