package im.getsocial.sdk.pushnotifications.a.g;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.pushnotifications.NotificationStatus;
import im.getsocial.sdk.pushnotifications.a.d.upgqDBbsrL;
import java.util.Arrays;
import java.util.List;

/* compiled from: SetNotificationsStatusUseCase */
public final class qZypgoeblR extends jjbQypPegg {
    @XdbacJlTDQ
    upgqDBbsrL dau;
    @XdbacJlTDQ
    im.getsocial.sdk.pushnotifications.a.a.jjbQypPegg getsocial;

    public qZypgoeblR() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(final List<String> list, final String str, CompletionCallback completionCallback) {
        if (Arrays.asList(NotificationStatus.CONSUMED, NotificationStatus.IGNORED).contains(str)) {
            this.getsocial.getsocial(list);
        }
        im.getsocial.sdk.pushnotifications.a.upgqDBbsrL mobile = this.dau.mobile();
        if (mobile != null) {
            mobile.getsocial(list, str);
        }
        getsocial(new Runnable() {
            public void run() {
                qZypgoeblR.this.getsocial().getsocial(list, str);
            }
        }, completionCallback);
    }
}
