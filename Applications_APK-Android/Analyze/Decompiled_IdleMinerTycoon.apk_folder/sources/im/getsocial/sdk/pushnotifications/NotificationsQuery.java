package im.getsocial.sdk.pushnotifications;

import im.getsocial.sdk.pushnotifications.a.b.cjrhisSQCL;
import java.util.Arrays;

public final class NotificationsQuery {
    public static final int DEFAULT_LIMIT = 20;
    private static final String[] attribution = new String[0];
    final cjrhisSQCL getsocial;

    public enum Filter {
        NO_FILTER,
        OLDER,
        NEWER
    }

    private NotificationsQuery(String... strArr) {
        this.getsocial = new cjrhisSQCL(Arrays.asList(strArr));
        this.getsocial.retention = Filter.NO_FILTER;
        this.getsocial.getsocial = 20;
        this.getsocial.attribution = attribution;
    }

    @Deprecated
    public static NotificationsQuery readAndUnread() {
        return new NotificationsQuery(NotificationStatus.READ, NotificationStatus.UNREAD);
    }

    @Deprecated
    public static NotificationsQuery read() {
        return new NotificationsQuery(NotificationStatus.READ);
    }

    @Deprecated
    public static NotificationsQuery unread() {
        return new NotificationsQuery(NotificationStatus.UNREAD);
    }

    public static NotificationsQuery withStatuses(String... strArr) {
        return new NotificationsQuery(strArr);
    }

    public static NotificationsQuery withAllStatuses() {
        return withStatuses(new String[0]);
    }

    public final NotificationsQuery withLimit(int i) {
        this.getsocial.getsocial = i;
        return this;
    }

    public final NotificationsQuery withFilter(Filter filter, String str) {
        this.getsocial.retention = filter;
        this.getsocial.dau = str;
        return this;
    }

    public final NotificationsQuery withActions(String... strArr) {
        this.getsocial.acquisition = (String[]) strArr.clone();
        return this;
    }

    public final NotificationsQuery ofAllTypes() {
        this.getsocial.attribution = attribution;
        return this;
    }

    public final NotificationsQuery ofTypes(String... strArr) {
        this.getsocial.attribution = (String[]) strArr.clone();
        return this;
    }
}
