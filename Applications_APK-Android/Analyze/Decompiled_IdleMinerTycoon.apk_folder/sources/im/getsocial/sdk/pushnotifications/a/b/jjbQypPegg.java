package im.getsocial.sdk.pushnotifications.a.b;

import com.helpshift.analytics.AnalyticsEventKey;
import im.getsocial.a.a.a.cjrhisSQCL;
import im.getsocial.a.a.pdwpUtZXDT;

/* compiled from: NotificationContainer */
public abstract class jjbQypPegg {
    public abstract boolean getsocial();

    public static jjbQypPegg getsocial(String str) {
        return getsocial((pdwpUtZXDT) new cjrhisSQCL().getsocial(str, (im.getsocial.a.a.a.jjbQypPegg) null), null, null);
    }

    private static pdwpUtZXDT getsocial(pdwpUtZXDT pdwputzxdt) {
        pdwpUtZXDT pdwputzxdt2;
        if (pdwputzxdt.containsKey("gs_data")) {
            return (pdwpUtZXDT) pdwputzxdt.get("gs_data");
        }
        for (Object next : pdwputzxdt.values()) {
            if ((next instanceof pdwpUtZXDT) && (pdwputzxdt2 = getsocial((pdwpUtZXDT) next)) != null) {
                return pdwputzxdt2;
            }
        }
        return null;
    }

    public static jjbQypPegg attribution(String str) {
        pdwpUtZXDT pdwputzxdt = getsocial((pdwpUtZXDT) new cjrhisSQCL().getsocial(str, (im.getsocial.a.a.a.jjbQypPegg) null));
        if (pdwputzxdt != null) {
            return getsocial(pdwputzxdt, null, null);
        }
        throw new im.getsocial.a.a.a.pdwpUtZXDT(2);
    }

    private static jjbQypPegg getsocial(pdwpUtZXDT pdwputzxdt, String str, String str2) {
        Object obj = pdwputzxdt.get(AnalyticsEventKey.SEARCH_QUERY);
        return (obj instanceof Number) && ((Number) obj).intValue() != 0 ? new XdbacJlTDQ() : new zoToeBNOjF(pdwputzxdt, str, str2);
    }
}
