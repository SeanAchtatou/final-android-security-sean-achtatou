package im.getsocial.sdk.pushnotifications.a;

import android.app.NotificationChannel;
import android.os.Build;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;
import im.getsocial.sdk.internal.j.a.jjbQypPegg;
import im.getsocial.sdk.sharedl10n.Localization;

/* compiled from: PushNotificationsChannelCreator */
public final class pdwpUtZXDT {
    @XdbacJlTDQ
    sqEuGXwfLT attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.m.XdbacJlTDQ getsocial;

    private pdwpUtZXDT() {
        ztWNWCuZiM.getsocial(this);
    }

    public static void getsocial() {
        pdwpUtZXDT pdwputzxdt = new pdwpUtZXDT();
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("getsocial_channel_id", pdwputzxdt.getsocial("im.getsocial.sdk.NotificationsChannelName", Localization.forLanguage(jjbQypPegg.getsocial()).NotificationsChannelName), 3);
            String str = pdwputzxdt.getsocial("im.getsocial.sdk.NotificationsChannelDescription", null);
            if (str != null) {
                notificationChannel.setDescription(str);
            }
            pdwputzxdt.getsocial.getsocial().createNotificationChannel(notificationChannel);
        }
    }

    private String getsocial(String str, String str2) {
        int i = this.attribution.getsocial(str, 0);
        if (i == 0) {
            return str2;
        }
        return this.getsocial.attribution().getString(i);
    }
}
