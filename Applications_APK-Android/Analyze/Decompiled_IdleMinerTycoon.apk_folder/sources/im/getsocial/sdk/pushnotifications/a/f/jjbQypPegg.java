package im.getsocial.sdk.pushnotifications.a.f;

import im.getsocial.sdk.internal.f.a.CJZnJxRuoc;
import im.getsocial.sdk.internal.f.a.XdbacJlTDQ;
import im.getsocial.sdk.internal.f.a.YgeTlQwUNa;
import im.getsocial.sdk.internal.f.a.nGNJgptECj;
import im.getsocial.sdk.internal.f.a.rFvvVpjzZH;
import im.getsocial.sdk.internal.f.a.sdizKTglGl;
import im.getsocial.sdk.internal.f.a.xlPHPMtUBa;
import im.getsocial.sdk.internal.k.a.cjrhisSQCL;
import im.getsocial.sdk.internal.m.QCXFOjcJkE;
import im.getsocial.sdk.pushnotifications.ActionButton;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationCustomization;
import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import im.getsocial.sdk.pushnotifications.NotificationsSummary;
import im.getsocial.sdk.pushnotifications.a.b.upgqDBbsrL;
import im.getsocial.sdk.usermanagement.UserReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: PushNotificationsThriftyConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, int):int
     arg types: [java.lang.Integer, int]
     candidates:
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, long):long
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.String, long):long
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.util.List, im.getsocial.sdk.internal.m.QCXFOjcJkE$jjbQypPegg):java.util.List<R>
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, int):int */
    public static Notification getsocial(nGNJgptECj ngnjgptecj) {
        NotificationCustomization notificationCustomization;
        Notification.Builder withType = Notification.builder(ngnjgptecj.getsocial).withAction(im.getsocial.sdk.actions.a.a.jjbQypPegg.getsocial(ngnjgptecj.f485android)).withActionType(QCXFOjcJkE.getsocial((Number) ngnjgptecj.retention, 0)).withCreatedAt(QCXFOjcJkE.attribution(ngnjgptecj.attribution)).withStatus(ngnjgptecj.unity).withText(ngnjgptecj.mau).withTitle(ngnjgptecj.cat).withType(ngnjgptecj.ios);
        xlPHPMtUBa xlphpmtuba = ngnjgptecj.viral;
        Notification.Builder addActionButtons = withType.withSender(new UserReference.Builder(xlphpmtuba.getsocial).setAvatarUrl(xlphpmtuba.acquisition).setDisplayName(xlphpmtuba.attribution).build()).addActionButtons(QCXFOjcJkE.getsocial(ngnjgptecj.connect, new QCXFOjcJkE.jjbQypPegg<XdbacJlTDQ, ActionButton>() {
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return jjbQypPegg.getsocial((XdbacJlTDQ) obj);
            }
        }));
        CJZnJxRuoc cJZnJxRuoc = ngnjgptecj.referral;
        if (cJZnJxRuoc == null) {
            notificationCustomization = null;
        } else {
            notificationCustomization = NotificationCustomization.withBackgroundImageConfiguration(cJZnJxRuoc.acquisition).withTextColor(cJZnJxRuoc.retention).withTitleColor(cJZnJxRuoc.mobile);
        }
        Notification.Builder withNotificationCustomization = addActionButtons.withNotificationCustomization(notificationCustomization);
        if (ngnjgptecj.referral != null) {
            withNotificationCustomization.withImageUrl(ngnjgptecj.referral.getsocial);
            withNotificationCustomization.withVideoUrl(ngnjgptecj.referral.attribution);
        }
        return withNotificationCustomization.build();
    }

    public static rFvvVpjzZH getsocial(upgqDBbsrL upgqdbbsrl) {
        CJZnJxRuoc cJZnJxRuoc;
        rFvvVpjzZH rfvvvpjzzh = new rFvvVpjzZH();
        rfvvvpjzzh.attribution = upgqdbbsrl.getsocial;
        rfvvvpjzzh.acquisition = upgqdbbsrl.attribution;
        if (upgqdbbsrl.acquisition != null) {
            cJZnJxRuoc = new CJZnJxRuoc();
            cJZnJxRuoc.getsocial = upgqdbbsrl.acquisition.getsocial(cjrhisSQCL.IMAGE);
            cJZnJxRuoc.attribution = upgqdbbsrl.acquisition.getsocial(cjrhisSQCL.VIDEO);
        } else {
            cJZnJxRuoc = null;
        }
        rfvvvpjzzh.mobile = upgqdbbsrl.mobile;
        rfvvvpjzzh.retention = upgqdbbsrl.dau;
        rfvvvpjzzh.dau = im.getsocial.sdk.actions.a.a.jjbQypPegg.getsocial(upgqdbbsrl.mau);
        rfvvvpjzzh.mau = QCXFOjcJkE.getsocial(upgqdbbsrl.cat, new QCXFOjcJkE.jjbQypPegg<ActionButton, XdbacJlTDQ>() {
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return jjbQypPegg.getsocial((ActionButton) obj);
            }
        });
        NotificationCustomization notificationCustomization = upgqdbbsrl.retention;
        if (notificationCustomization != null) {
            if (notificationCustomization.getBackgroundImageConfiguration() != null && notificationCustomization.getBackgroundImageConfiguration().length() > 0) {
                cJZnJxRuoc = getsocial(cJZnJxRuoc);
                cJZnJxRuoc.acquisition = notificationCustomization.getBackgroundImageConfiguration();
            }
            if (notificationCustomization.getTitleColor() != null && notificationCustomization.getTitleColor().length() > 0) {
                cJZnJxRuoc = getsocial(cJZnJxRuoc);
                cJZnJxRuoc.mobile = notificationCustomization.getTitleColor();
            }
            if (notificationCustomization.getTextColor() != null && notificationCustomization.getTextColor().length() > 0) {
                cJZnJxRuoc = getsocial(cJZnJxRuoc);
                cJZnJxRuoc.retention = notificationCustomization.getTextColor();
            }
        }
        rfvvvpjzzh.cat = cJZnJxRuoc;
        return rfvvvpjzzh;
    }

    static XdbacJlTDQ getsocial(ActionButton actionButton) {
        XdbacJlTDQ xdbacJlTDQ = new XdbacJlTDQ();
        xdbacJlTDQ.getsocial = actionButton.getTitle();
        xdbacJlTDQ.attribution = actionButton.getId();
        return xdbacJlTDQ;
    }

    public static NotificationsSummary getsocial(sdizKTglGl sdizktglgl) {
        return new NotificationsSummary(QCXFOjcJkE.getsocial(sdizktglgl.getsocial));
    }

    public static YgeTlQwUNa getsocial(im.getsocial.sdk.pushnotifications.a.b.cjrhisSQCL cjrhissqcl) {
        YgeTlQwUNa ygeTlQwUNa = new YgeTlQwUNa();
        String str = null;
        ygeTlQwUNa.attribution = cjrhissqcl.retention == NotificationsQuery.Filter.NEWER ? cjrhissqcl.dau : null;
        if (cjrhissqcl.retention == NotificationsQuery.Filter.OLDER) {
            str = cjrhissqcl.dau;
        }
        ygeTlQwUNa.acquisition = str;
        ygeTlQwUNa.getsocial = Integer.valueOf(cjrhissqcl.getsocial);
        ygeTlQwUNa.mobile = getsocial(cjrhissqcl.attribution);
        ygeTlQwUNa.retention = cjrhissqcl.mobile;
        ygeTlQwUNa.dau = getsocial(cjrhissqcl.acquisition);
        return ygeTlQwUNa;
    }

    private static CJZnJxRuoc getsocial(CJZnJxRuoc cJZnJxRuoc) {
        return cJZnJxRuoc != null ? cJZnJxRuoc : new CJZnJxRuoc();
    }

    static ActionButton getsocial(XdbacJlTDQ xdbacJlTDQ) {
        return ActionButton.create(xdbacJlTDQ.getsocial, xdbacJlTDQ.attribution);
    }

    private static <T> List<T> getsocial(Object... objArr) {
        return objArr == null ? Collections.emptyList() : Arrays.asList(objArr);
    }
}
