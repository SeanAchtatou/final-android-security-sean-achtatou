package im.getsocial.sdk.pushnotifications.a.e;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import com.google.android.gms.drive.DriveFile;
import im.getsocial.sdk.internal.a.g.jjbQypPegg;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF;
import im.getsocial.sdk.pushnotifications.a.cjrhisSQCL;
import java.util.List;

/* compiled from: NotificationClickStrategy */
class upgqDBbsrL extends cjrhisSQCL {
    @XdbacJlTDQ
    qdyNCsqjKt attribution;
    @XdbacJlTDQ
    jjbQypPegg getsocial;

    upgqDBbsrL() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(Context context, Intent intent) {
        Intent intent2;
        zoToeBNOjF zotoebnojf = cjrhisSQCL.getsocial(intent);
        if (zotoebnojf != null) {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent("getsocial.intent.action.NOTIFICATION_RECEIVE").setPackage(context.getPackageName()), 65536);
            if (queryIntentActivities.isEmpty()) {
                intent2 = null;
            } else {
                intent2 = new Intent().setClassName(context, queryIntentActivities.get(0).activityInfo.name);
            }
            if (intent2 == null) {
                intent2 = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            }
            if (intent2 != null) {
                cjrhisSQCL.getsocial(this.attribution, zotoebnojf);
                intent2.addFlags(DriveFile.MODE_READ_ONLY);
                context.startActivity(intent2);
            }
            this.getsocial.getsocial("push_notification_clicked", zotoebnojf.attribution());
        }
    }
}
