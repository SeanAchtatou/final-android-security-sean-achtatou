package im.getsocial.sdk.pushnotifications.a.d;

import im.getsocial.sdk.internal.c.b.KluUZYuxme;
import im.getsocial.sdk.internal.c.b.qdyNCsqjKt;
import im.getsocial.sdk.pushnotifications.NotificationListener;
import im.getsocial.sdk.pushnotifications.PushTokenListener;
import im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/* compiled from: PushNotificationsStateAppRepo */
public class upgqDBbsrL implements KluUZYuxme {
    private static final long getsocial = TimeUnit.SECONDS.toMillis(5);
    private jjbQypPegg<zoToeBNOjF> acquisition;
    private final Object attribution = new Object();
    private PushTokenListener dau;
    private String mau;
    private NotificationListener mobile;
    private im.getsocial.sdk.pushnotifications.a.upgqDBbsrL retention;

    /* renamed from: im.getsocial.sdk.pushnotifications.a.d.upgqDBbsrL$upgqDBbsrL  reason: collision with other inner class name */
    /* compiled from: PushNotificationsStateAppRepo */
    public enum C0025upgqDBbsrL {
        REGISTERED,
        NOT_REGISTERED
    }

    public final zoToeBNOjF getsocial() {
        if (this.acquisition == null) {
            return null;
        }
        zoToeBNOjF zotoebnojf = this.acquisition.getsocial();
        if (this.acquisition.attribution()) {
            zotoebnojf.viral();
        }
        this.acquisition = null;
        return zotoebnojf;
    }

    public final void getsocial(zoToeBNOjF zotoebnojf) {
        if (zotoebnojf == null) {
            this.acquisition = null;
        } else {
            this.acquisition = new jjbQypPegg<>(zotoebnojf, getsocial);
        }
    }

    public final void getsocial(NotificationListener notificationListener) {
        this.mobile = notificationListener;
    }

    public final void getsocial(PushTokenListener pushTokenListener) {
        synchronized (this.attribution) {
            this.dau = pushTokenListener;
            retention();
        }
    }

    public final void getsocial(String str) {
        synchronized (this.attribution) {
            this.mau = str;
            retention();
        }
    }

    private void retention() {
        if (this.dau != null && this.mau != null) {
            this.dau.onTokenReady(this.mau);
            this.dau = null;
        }
    }

    public final NotificationListener acquisition() {
        return this.mobile;
    }

    public final qdyNCsqjKt attribution() {
        return qdyNCsqjKt.APP;
    }

    public final im.getsocial.sdk.pushnotifications.a.upgqDBbsrL mobile() {
        return this.retention;
    }

    public final void getsocial(im.getsocial.sdk.pushnotifications.a.upgqDBbsrL upgqdbbsrl) {
        this.retention = upgqdbbsrl;
    }

    /* compiled from: PushNotificationsStateAppRepo */
    private static class jjbQypPegg<T> {
        private final long attribution;
        private final T getsocial;

        jjbQypPegg(T t, long j) {
            this.getsocial = t;
            this.attribution = acquisition() + j;
        }

        private static long acquisition() {
            return new Date().getTime();
        }

        public final T getsocial() {
            return this.getsocial;
        }

        public final boolean attribution() {
            return this.attribution < acquisition();
        }
    }
}
