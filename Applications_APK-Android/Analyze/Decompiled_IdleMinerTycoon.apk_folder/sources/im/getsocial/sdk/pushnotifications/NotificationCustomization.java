package im.getsocial.sdk.pushnotifications;

public class NotificationCustomization {
    private String acquisition;
    private String attribution;
    private String getsocial;

    public String getBackgroundImageConfiguration() {
        return this.getsocial;
    }

    public String getTitleColor() {
        return this.attribution;
    }

    public String getTextColor() {
        return this.acquisition;
    }

    public static NotificationCustomization withBackgroundImageConfiguration(String str) {
        NotificationCustomization notificationCustomization = new NotificationCustomization();
        notificationCustomization.getsocial = str;
        return notificationCustomization;
    }

    public NotificationCustomization withTitleColor(String str) {
        this.attribution = str;
        return this;
    }

    public NotificationCustomization withTextColor(String str) {
        this.acquisition = str;
        return this;
    }

    public String toString() {
        return "NotificationCustomization{_backgroundImageConfiguration='" + this.getsocial + '\'' + ", _titleColor='" + this.attribution + '\'' + ", _textColor='" + this.acquisition + '\'';
    }
}
