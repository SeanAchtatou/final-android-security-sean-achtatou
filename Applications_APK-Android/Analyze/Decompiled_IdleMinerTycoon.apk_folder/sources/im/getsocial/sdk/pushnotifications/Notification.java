package im.getsocial.sdk.pushnotifications;

import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.usermanagement.UserReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Notification {
    /* access modifiers changed from: private */
    public String acquisition;
    /* access modifiers changed from: private */

    /* renamed from: android  reason: collision with root package name */
    public UserReference f492android;
    /* access modifiers changed from: private */
    public String attribution;
    /* access modifiers changed from: private */
    public int cat;
    /* access modifiers changed from: private */
    public String dau;
    /* access modifiers changed from: private */
    public final String getsocial;
    /* access modifiers changed from: private */
    public String growth;
    /* access modifiers changed from: private */
    public NotificationCustomization ios;
    /* access modifiers changed from: private */
    public Action mau;
    /* access modifiers changed from: private */
    public long mobile;
    /* access modifiers changed from: private */
    public String organic;
    /* access modifiers changed from: private */
    public String retention;
    /* access modifiers changed from: private */
    public final List<ActionButton> viral = new ArrayList();

    public static class NotificationType {
        public static final String COMMENT = "comment";
        public static final String COMMENTED_IN_SAME_THREAD = "related_comment";
        public static final String DIRECT = "direct";
        public static final String INVITE_ACCEPTED = "invite_accept";
        public static final String LIKE_ACTIVITY = "activity_like";
        public static final String LIKE_COMMENT = "comment_like";
        public static final String MENTION_IN_ACTIVITY = "activity_mention";
        public static final String MENTION_IN_COMMENT = "comment_mention";
        public static final String NEW_FRIENDSHIP = "friends_add";
        public static final String REPLY_TO_COMMENT = "comment_reply";
        public static final String SDK = "custom";
        public static final String TARGETING = "targeting";
    }

    Notification(String str) {
        this.getsocial = str;
    }

    public static Builder builder(String str) {
        return new Builder(str);
    }

    public String getTitle() {
        return this.retention;
    }

    public String getText() {
        return this.dau;
    }

    public Action getAction() {
        return this.mau;
    }

    public List<ActionButton> getActionButtons() {
        return this.viral;
    }

    @Deprecated
    public int getActionType() {
        return this.cat;
    }

    @Deprecated
    public Map<String, String> getActionData() {
        return this.mau.getData();
    }

    public String getId() {
        return this.getsocial;
    }

    public boolean wasRead() {
        return NotificationStatus.READ.equals(this.attribution);
    }

    public String getStatus() {
        return this.attribution;
    }

    public String getType() {
        return this.acquisition;
    }

    public long getCreatedAt() {
        return this.mobile;
    }

    public String getImageUrl() {
        return this.organic;
    }

    public String getVideoUrl() {
        return this.growth;
    }

    public UserReference getSender() {
        return this.f492android;
    }

    public NotificationCustomization getCustomization() {
        return this.ios;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Notification{, _id='");
        sb.append(this.getsocial);
        sb.append('\'');
        sb.append(", _status='");
        sb.append(this.attribution);
        sb.append('\'');
        sb.append(", _type=");
        sb.append(this.acquisition);
        sb.append(", _createdAt=");
        sb.append(this.mobile);
        sb.append(", _title='");
        sb.append(this.retention);
        sb.append('\'');
        sb.append(", _text='");
        sb.append(this.dau);
        sb.append('\'');
        sb.append(", _action=");
        sb.append(this.mau);
        sb.append(", _actionType=");
        sb.append(this.cat);
        sb.append(", _actionButtons=");
        sb.append(this.viral);
        sb.append(", _imageUrl='");
        sb.append(this.organic);
        sb.append('\'');
        sb.append(", _videoUrl='");
        sb.append(this.growth);
        sb.append('\'');
        sb.append(", _sender='");
        sb.append(this.f492android);
        sb.append('\'');
        sb.append(", _notificationCustomization='");
        sb.append(this.ios == null ? "null" : this.ios.toString());
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Deprecated
    public static class ActionType {
        public static final int CUSTOM = 0;
        public static final int OPEN_ACTIVITY = 2;
        public static final int OPEN_INVITES = 3;
        public static final int OPEN_PROFILE = 1;
        public static final int OPEN_URL = 4;

        private ActionType() {
        }
    }

    @Deprecated
    public static final class Key {

        public static final class OpenActivity {
            public static final String ACTIVITY_ID = "$activity_id";
            public static final String COMMENT_ID = "$comment_id";
            public static final String FEED_NAME = "$feed_name";

            private OpenActivity() {
            }
        }

        public static final class OpenProfile {
            public static final String USER_ID = "$user_id";

            private OpenProfile() {
            }
        }

        public static final class OpenUrl {
            public static final String URL = "$url";

            private OpenUrl() {
            }
        }

        private Key() {
        }
    }

    public static final class Builder {
        private final Notification getsocial;

        Builder(String str) {
            this.getsocial = new Notification(str);
        }

        public final Builder withStatus(String str) {
            String unused = this.getsocial.attribution = str;
            return this;
        }

        public final Builder withType(String str) {
            String unused = this.getsocial.acquisition = str;
            return this;
        }

        public final Builder withCreatedAt(long j) {
            long unused = this.getsocial.mobile = j;
            return this;
        }

        public final Builder withTitle(String str) {
            String unused = this.getsocial.retention = str;
            return this;
        }

        public final Builder withText(String str) {
            String unused = this.getsocial.dau = str;
            return this;
        }

        public final Builder withAction(Action action) {
            Action unused = this.getsocial.mau = action;
            return this;
        }

        @Deprecated
        public final Builder withActionType(int i) {
            int unused = this.getsocial.cat = i;
            return this;
        }

        public final Builder addActionButtons(List<ActionButton> list) {
            this.getsocial.viral.addAll(list);
            return this;
        }

        public final Builder addActionButton(ActionButton actionButton) {
            this.getsocial.viral.add(actionButton);
            return this;
        }

        public final Builder withImageUrl(String str) {
            String unused = this.getsocial.organic = str;
            return this;
        }

        public final Builder withVideoUrl(String str) {
            String unused = this.getsocial.growth = str;
            return this;
        }

        public final Builder withSender(UserReference userReference) {
            UserReference unused = this.getsocial.f492android = userReference;
            return this;
        }

        public final Builder withNotificationCustomization(NotificationCustomization notificationCustomization) {
            NotificationCustomization unused = this.getsocial.ios = notificationCustomization;
            return this;
        }

        public final Notification build() {
            Notification notification = this.getsocial;
            Notification notification2 = new Notification(notification.getsocial);
            String unused = notification2.dau = notification.dau;
            notification2.viral.addAll(notification.viral);
            String unused2 = notification2.attribution = notification.attribution;
            String unused3 = notification2.retention = notification.retention;
            int unused4 = notification2.cat = notification.cat;
            String unused5 = notification2.growth = notification.growth;
            Action unused6 = notification2.mau = notification.mau;
            String unused7 = notification2.organic = notification.organic;
            String unused8 = notification2.acquisition = notification.acquisition;
            long unused9 = notification2.mobile = notification.mobile;
            UserReference unused10 = notification2.f492android = notification.f492android;
            NotificationCustomization unused11 = notification2.ios = notification.ios;
            return notification2;
        }
    }
}
