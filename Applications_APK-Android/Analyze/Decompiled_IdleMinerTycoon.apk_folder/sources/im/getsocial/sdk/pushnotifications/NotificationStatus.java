package im.getsocial.sdk.pushnotifications;

public final class NotificationStatus {
    public static final String CONSUMED = "consumed";
    public static final String IGNORED = "ignored";
    public static final String READ = "read";
    public static final String UNREAD = "unread";

    private NotificationStatus() {
    }
}
