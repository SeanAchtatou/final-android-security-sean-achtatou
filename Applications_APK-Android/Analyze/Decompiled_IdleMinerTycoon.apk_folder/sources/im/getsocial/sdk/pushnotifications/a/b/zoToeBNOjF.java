package im.getsocial.sdk.pushnotifications.a.b;

import com.helpshift.analytics.AnalyticsEventKey;
import com.mintegral.msdk.base.b.d;
import com.mintegral.msdk.base.entity.CampaignEx;
import im.getsocial.a.a.pdwpUtZXDT;
import im.getsocial.a.a.upgqDBbsrL;
import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.actions.ActionTypes;
import im.getsocial.sdk.pushnotifications.ActionButton;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationCustomization;
import im.getsocial.sdk.pushnotifications.NotificationStatus;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: VisibleNotificationContainer */
public final class zoToeBNOjF extends jjbQypPegg {
    private static final List<String> getsocial = Arrays.asList(ActionTypes.OPEN_ACTIVITY, ActionTypes.OPEN_PROFILE, ActionTypes.OPEN_INVITES, ActionTypes.ADD_FRIEND, ActionTypes.CLAIM_PROMO_CODE);
    private final Notification acquisition;
    private final String attribution;
    private boolean cat = false;
    private boolean dau = false;
    private boolean mau = false;
    private final Map<String, String> mobile;
    private final String retention;

    private static String mobile(String str) {
        return str == null ? "" : str;
    }

    public final boolean getsocial() {
        return false;
    }

    zoToeBNOjF(pdwpUtZXDT pdwputzxdt, String str, String str2) {
        HashMap hashMap;
        int i;
        HashMap hashMap2;
        Number number;
        pdwpUtZXDT pdwputzxdt2 = pdwputzxdt;
        String str3 = str;
        String str4 = str2;
        pdwpUtZXDT pdwputzxdt3 = new pdwpUtZXDT(pdwputzxdt2);
        if (str3 != null) {
            pdwputzxdt3.put("text", str3);
        }
        if (str4 != null) {
            pdwputzxdt3.put("title", str4);
        }
        this.attribution = pdwpUtZXDT.getsocial(pdwputzxdt3);
        str3 = str3 == null ? (String) pdwputzxdt2.get("text") : str3;
        str4 = str4 == null ? (String) pdwputzxdt2.get("title") : str4;
        String str5 = (String) pdwputzxdt2.get("i_url");
        String str6 = (String) pdwputzxdt2.get("v_url");
        String str7 = (String) pdwputzxdt2.get("bgi");
        String str8 = (String) pdwputzxdt2.get("t_color");
        String str9 = (String) pdwputzxdt2.get("m_color");
        this.retention = (String) pdwputzxdt2.get("uid");
        Object obj = pdwputzxdt2.get("ap");
        if (obj instanceof pdwpUtZXDT) {
            hashMap = new HashMap((pdwpUtZXDT) obj);
        } else {
            hashMap = new HashMap();
        }
        this.mobile = hashMap;
        upgqDBbsrL upgqdbbsrl = (upgqDBbsrL) pdwputzxdt2.get("a");
        boolean z = upgqdbbsrl == null || upgqdbbsrl.isEmpty();
        if (!z && (number = (Number) ((pdwpUtZXDT) upgqdbbsrl.get(0)).get("t")) != null) {
            i = number.intValue();
        } else {
            i = 0;
        }
        String str10 = z ? "custom" : (String) ((pdwpUtZXDT) upgqdbbsrl.get(0)).get(AnalyticsEventKey.FAQ_SEARCH_RESULT_COUNT);
        if (z) {
            hashMap2 = new HashMap();
        } else {
            hashMap2 = new HashMap((pdwpUtZXDT) ((pdwpUtZXDT) upgqdbbsrl.get(0)).get(d.b));
        }
        this.acquisition = Notification.builder((String) pdwputzxdt2.get("id")).withStatus(NotificationStatus.UNREAD).withTitle(mobile(str4)).withText(mobile(str3)).withAction(Action.builder(str10).addActionData(hashMap2).build()).withImageUrl(str5).withVideoUrl(str6).withActionType(i).withCreatedAt(((Number) pdwputzxdt2.get(CampaignEx.JSON_KEY_ST_TS)).longValue()).withType((String) pdwputzxdt2.get("type")).addActionButtons(getsocial((upgqDBbsrL) pdwputzxdt2.get("ab"))).withNotificationCustomization(NotificationCustomization.withBackgroundImageConfiguration(str7).withTitleColor(str8).withTextColor(str9)).build();
    }

    private static List<ActionButton> getsocial(upgqDBbsrL<pdwpUtZXDT<String, Object>> upgqdbbsrl) {
        ArrayList arrayList = new ArrayList();
        if (upgqdbbsrl != null) {
            Iterator<E> it = upgqdbbsrl.iterator();
            while (it.hasNext()) {
                pdwpUtZXDT pdwputzxdt = (pdwpUtZXDT) it.next();
                try {
                    arrayList.add(ActionButton.create((String) pdwputzxdt.get("title"), (String) pdwputzxdt.get("id")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public final Map<String, String> attribution() {
        return this.mobile;
    }

    public final Notification acquisition() {
        return this.acquisition;
    }

    public final boolean mobile() {
        return this.dau;
    }

    public final void retention() {
        this.dau = true;
    }

    public final boolean dau() {
        return this.mau;
    }

    public final void mau() {
        this.mau = true;
    }

    public final boolean cat() {
        return this.cat;
    }

    public final void viral() {
        this.cat = true;
    }

    public final String organic() {
        return this.attribution;
    }

    public final boolean growth() {
        return getsocial.contains(this.acquisition.getAction().getType());
    }

    public final boolean acquisition(String str) {
        return this.retention.equals(str);
    }

    public final String android() {
        return this.mobile.get("g_nid");
    }
}
