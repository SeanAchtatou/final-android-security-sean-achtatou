package im.getsocial.sdk.pushnotifications.a.g;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.pushnotifications.NotificationStatus;
import im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF;
import java.util.Collections;

/* compiled from: MarkNotificationAsReadAndInvokeListenerUseCase */
final class XdbacJlTDQ extends jjbQypPegg {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(XdbacJlTDQ.class);

    XdbacJlTDQ() {
    }

    public final void getsocial(final zoToeBNOjF zotoebnojf) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(zotoebnojf), "Notification can not be null");
        getsocial(new Runnable() {
            public void run() {
                XdbacJlTDQ.this.getsocial().getsocial(Collections.singletonList(zotoebnojf.acquisition().getId()), NotificationStatus.READ);
            }
        }, new CompletionCallback() {
            public void onSuccess() {
                new cjrhisSQCL().getsocial(zotoebnojf);
            }

            public void onFailure(GetSocialException getSocialException) {
                cjrhisSQCL attribution2 = XdbacJlTDQ.getsocial;
                attribution2.mobile("Failed to mark notification as read: " + getSocialException.getMessage());
                XdbacJlTDQ.getsocial.attribution(getSocialException);
                new cjrhisSQCL().getsocial(zotoebnojf);
            }
        });
    }
}
