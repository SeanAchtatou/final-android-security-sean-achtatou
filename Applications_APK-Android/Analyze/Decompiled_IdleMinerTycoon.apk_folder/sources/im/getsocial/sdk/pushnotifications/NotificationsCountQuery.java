package im.getsocial.sdk.pushnotifications;

import im.getsocial.sdk.pushnotifications.a.b.cjrhisSQCL;
import java.util.Arrays;

public final class NotificationsCountQuery {
    private static final String[] attribution = new String[0];
    final cjrhisSQCL getsocial;

    public enum Filter {
        NO_FILTER,
        OLDER,
        NEWER
    }

    private NotificationsCountQuery(String... strArr) {
        this.getsocial = new cjrhisSQCL(Arrays.asList(strArr));
        this.getsocial.attribution = attribution;
    }

    @Deprecated
    public static NotificationsCountQuery readAndUnread() {
        return new NotificationsCountQuery(NotificationStatus.UNREAD, NotificationStatus.READ);
    }

    @Deprecated
    public static NotificationsCountQuery read() {
        return new NotificationsCountQuery(NotificationStatus.READ);
    }

    @Deprecated
    public static NotificationsCountQuery unread() {
        return new NotificationsCountQuery(NotificationStatus.UNREAD);
    }

    public static NotificationsCountQuery withStatuses(String... strArr) {
        return new NotificationsCountQuery(strArr);
    }

    public static NotificationsCountQuery withAllStatuses() {
        return withStatuses(new String[0]);
    }

    public final NotificationsCountQuery ofAllTypes() {
        this.getsocial.attribution = attribution;
        return this;
    }

    public final NotificationsCountQuery withActions(String... strArr) {
        this.getsocial.acquisition = (String[]) strArr.clone();
        return this;
    }

    public final NotificationsCountQuery ofTypes(String... strArr) {
        this.getsocial.attribution = (String[]) strArr.clone();
        return this;
    }
}
