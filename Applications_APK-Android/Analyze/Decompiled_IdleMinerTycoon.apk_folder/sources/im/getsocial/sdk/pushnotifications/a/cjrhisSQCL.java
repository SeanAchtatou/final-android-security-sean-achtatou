package im.getsocial.sdk.pushnotifications.a;

import android.content.Intent;
import android.os.Bundle;
import im.getsocial.a.a.a.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.a.b.jjbQypPegg;
import im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF;

/* compiled from: PushNotificationAccessHelper */
public final class cjrhisSQCL {
    private static final im.getsocial.sdk.internal.c.f.cjrhisSQCL attribution = upgqDBbsrL.getsocial(cjrhisSQCL.class);
    private static final String getsocial = Notification.class.getName();

    private cjrhisSQCL() {
    }

    public static void getsocial(Intent intent, zoToeBNOjF zotoebnojf) {
        intent.putExtra(getsocial, zotoebnojf.organic());
    }

    public static zoToeBNOjF getsocial(Intent intent) {
        if (!intent.hasExtra(getsocial)) {
            return null;
        }
        String stringExtra = intent.getStringExtra(getsocial);
        intent.removeExtra(getsocial);
        return getsocial(stringExtra);
    }

    public static void getsocial(qdyNCsqjKt qdyncsqjkt, zoToeBNOjF zotoebnojf) {
        qdyncsqjkt.getsocial(getsocial, zotoebnojf.organic());
    }

    public static zoToeBNOjF getsocial(qdyNCsqjKt qdyncsqjkt) {
        if (!qdyncsqjkt.getsocial(getsocial)) {
            return null;
        }
        String attribution2 = qdyncsqjkt.attribution(getsocial);
        qdyncsqjkt.retention(getsocial);
        return getsocial(attribution2);
    }

    private static zoToeBNOjF getsocial(String str) {
        try {
            return (zoToeBNOjF) jjbQypPegg.getsocial(str);
        } catch (pdwpUtZXDT unused) {
            return null;
        }
    }

    public static jjbQypPegg attribution(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return null;
        }
        for (String next : extras.keySet()) {
            Object obj = extras.get(next);
            if (obj instanceof String) {
                String str = (String) obj;
                try {
                    return jjbQypPegg.attribution(str);
                } catch (pdwpUtZXDT unused) {
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = attribution;
                    cjrhissqcl.attribution(next + " does not contain GetSocialNotification, it is " + str);
                }
            }
        }
        throw new pdwpUtZXDT(2, "Can not find GetSocial data in " + extras);
    }
}
