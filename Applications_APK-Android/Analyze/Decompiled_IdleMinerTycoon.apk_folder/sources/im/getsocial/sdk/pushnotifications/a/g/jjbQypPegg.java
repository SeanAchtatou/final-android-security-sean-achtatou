package im.getsocial.sdk.pushnotifications.a.g;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.pushnotifications.NotificationsCountQuery;

/* compiled from: GetNotificationsCountUseCase */
public final class jjbQypPegg extends im.getsocial.sdk.internal.c.k.jjbQypPegg {
    public final void getsocial(final NotificationsCountQuery notificationsCountQuery, Callback<Integer> callback) {
        getsocial(new jjbQypPegg.C0020jjbQypPegg<Integer>() {
            public final /* bridge */ /* synthetic */ Object getsocial() {
                return jjbQypPegg.this.getsocial().getsocial(notificationsCountQuery);
            }
        }, callback);
    }
}
