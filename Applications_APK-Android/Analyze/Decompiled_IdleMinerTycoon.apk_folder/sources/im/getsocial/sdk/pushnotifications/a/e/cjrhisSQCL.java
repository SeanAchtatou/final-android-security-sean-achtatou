package im.getsocial.sdk.pushnotifications.a.e;

import android.content.Context;
import android.content.Intent;

/* compiled from: NotificationStrategy */
public abstract class cjrhisSQCL {
    public abstract void getsocial(Context context, Intent intent);

    public static cjrhisSQCL getsocial(String str) {
        if (str == null) {
            return new jjbQypPegg();
        }
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != 366519424) {
            if (hashCode != 1736128796) {
                if (hashCode == 1824844730 && str.equals("im.getsocial.sdk.intent.RECEIVE")) {
                    c = 2;
                }
            } else if (str.equals("com.google.android.c2dm.intent.REGISTRATION")) {
                c = 1;
            }
        } else if (str.equals("com.google.android.c2dm.intent.RECEIVE")) {
            c = 0;
        }
        switch (c) {
            case 0:
                return new pdwpUtZXDT();
            case 1:
                return new XdbacJlTDQ();
            case 2:
                return new upgqDBbsrL();
            default:
                return new jjbQypPegg();
        }
    }
}
