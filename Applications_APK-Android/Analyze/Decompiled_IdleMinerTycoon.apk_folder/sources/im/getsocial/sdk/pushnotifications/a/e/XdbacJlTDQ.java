package im.getsocial.sdk.pushnotifications.a.e;

import android.content.Context;
import android.content.Intent;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;

/* compiled from: RegisterForNotificationStrategy */
class XdbacJlTDQ extends cjrhisSQCL {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(XdbacJlTDQ.class);

    XdbacJlTDQ() {
    }

    public final void getsocial(Context context, Intent intent) {
        if (intent.hasExtra("error")) {
            String stringExtra = intent.getStringExtra("error");
            getsocial.getsocial("GCM error: %s", stringExtra);
        } else if (intent.hasExtra("registration_id")) {
            String stringExtra2 = intent.getStringExtra("registration_id");
            getsocial.getsocial("GCM registration id received: [%s]", stringExtra2);
        } else if (intent.hasExtra("unregistered")) {
            getsocial.attribution("GCM unregistered successfully");
        }
    }
}
