package im.getsocial.sdk.pushnotifications.a.g;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.k.upgqDBbsrL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.vkXhnjhKGp;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationListener;
import im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF;

/* compiled from: InvokeNotificationListenersAndMarkAsSentUseCase */
final class cjrhisSQCL implements upgqDBbsrL {
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL acquisition = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(cjrhisSQCL.class);
    @XdbacJlTDQ
    vkXhnjhKGp attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.pushnotifications.a.d.upgqDBbsrL getsocial;

    cjrhisSQCL() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(final zoToeBNOjF zotoebnojf) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial(zotoebnojf), "Notification can not be null");
        if (zotoebnojf.dau() || zotoebnojf.cat()) {
            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = acquisition;
            cjrhissqcl.attribution("Won't notify listeners, notification state: alreadySent = " + zotoebnojf.dau() + ", expired = " + zotoebnojf.cat());
            return;
        }
        zotoebnojf.mau();
        this.attribution.getsocial(new Runnable() {
            public void run() {
                NotificationListener acquisition = cjrhisSQCL.this.getsocial.acquisition();
                Notification acquisition2 = zotoebnojf.acquisition();
                boolean mobile = zotoebnojf.mobile();
                cjrhisSQCL.acquisition.getsocial("Invoke listener with notification %s (wasClicked=%b)", acquisition2, Boolean.valueOf(mobile));
                if (acquisition == null) {
                    cjrhisSQCL.acquisition.attribution("No custom notification listener set");
                } else if (acquisition.onNotificationReceived(acquisition2, mobile)) {
                    cjrhisSQCL.acquisition.attribution("Notification handled by developer");
                    return;
                }
                if (mobile) {
                    cjrhisSQCL.acquisition.attribution("Clicked notification not handled by developer, process by GetSocial");
                    new im.getsocial.sdk.actions.a.b.jjbQypPegg().getsocial(acquisition2.getAction());
                    return;
                }
                cjrhisSQCL.acquisition.attribution("Notification not clicked, doing nothing");
            }
        });
    }
}
