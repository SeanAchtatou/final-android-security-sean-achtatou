package im.getsocial.sdk.pushnotifications;

public interface PushTokenListener {
    void onTokenReady(String str);
}
