package im.getsocial.sdk.pushnotifications;

public final class ActionButton {
    public static final String CONSUME_ACTION = "consume";
    public static final String IGNORE_ACTION = "ignore";
    private final String attribution;
    private final String getsocial;

    private ActionButton(String str, String str2) {
        this.getsocial = str;
        this.attribution = str2;
    }

    public static ActionButton create(String str, String str2) {
        return new ActionButton(str, str2);
    }

    public final String getTitle() {
        return this.getsocial;
    }

    public final String getId() {
        return this.attribution;
    }
}
