package im.getsocial.sdk;

public final class ErrorCode {
    public static final int ACTION_DENIED = 201;
    public static final int APP_SIGNATURE_MISMATCH = 210;
    public static final int CONNECTION_TIMEOUT = 701;
    public static final int GENERIC_COMMUNICATION_ERROR = 1;
    public static final int ILLEGAL_ARGUMENT = 204;
    public static final int ILLEGAL_STATE = 205;
    public static final int INVITE_CANCELLED = 100;
    public static final int MEDIAUPLOAD_FAILED = 800;
    public static final int MEDIAUPLOAD_FILE_SIZE_OVER_LIMIT = 803;
    public static final int MEDIAUPLOAD_RESOURCE_NOT_READY = 801;
    public static final int NOT_FOUND = 207;
    public static final int NO_INTERNET = 702;
    public static final int NO_REFERRER_MATCH = 102;
    public static final int NULL_POINTER = 206;
    public static final int OOM = 103;
    public static final int PLATFORM_DISABLED = 209;
    public static final int SDK_INITIALIZATION_FAILED = 203;
    public static final int SDK_NOT_INITIALIZED = 202;
    public static final int TRANSPORT_CLOSED = 703;
    public static final int UNCAUGHT_EXCEPTION = 42;
    public static final int UNKNOWN = 0;
    public static final int USERID_TOKEN_MISMATCH = 211;
    public static final int USER_CONFLICT = 101;
    public static final int USER_IS_BANNED = 208;
}
