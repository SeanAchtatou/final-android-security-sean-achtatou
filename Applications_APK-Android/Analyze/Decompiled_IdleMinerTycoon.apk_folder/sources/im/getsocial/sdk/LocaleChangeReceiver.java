package im.getsocial.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import im.getsocial.sdk.pushnotifications.a.pdwpUtZXDT;

public final class LocaleChangeReceiver extends BroadcastReceiver {
    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.LOCALE_CHANGED".equals(intent.getAction())) {
            pdwpUtZXDT.getsocial();
        }
    }
}
