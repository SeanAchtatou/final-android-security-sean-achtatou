package im.getsocial.sdk.internal.h.a;

import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;

/* compiled from: BillingClientStateListenerProxy */
public abstract class jjbQypPegg implements BillingClientStateListener {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(jjbQypPegg.class);

    public abstract void onSetupFinished(XdbacJlTDQ xdbacJlTDQ);

    public void onBillingSetupFinished(BillingResult billingResult) {
        onSetupFinished(new XdbacJlTDQ(billingResult));
    }

    public void onBillingSetupFinished(int i) {
        onSetupFinished(new XdbacJlTDQ(i));
    }

    public void onBillingServiceDisconnected() {
        getsocial.attribution("Billing client disconnected");
    }
}
