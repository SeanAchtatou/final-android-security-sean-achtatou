package im.getsocial.sdk.internal.c.l;

/* compiled from: TextUtils */
public final class ztWNWCuZiM {
    private ztWNWCuZiM() {
    }

    public static boolean getsocial(String str) {
        return str == null || str.isEmpty() || str.trim().isEmpty();
    }
}
