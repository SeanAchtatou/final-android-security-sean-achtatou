package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THTagsQuery */
public final class NgDaxXOnCs {
    public String acquisition;
    public String attribution;
    public Integer getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof NgDaxXOnCs)) {
            return false;
        }
        NgDaxXOnCs ngDaxXOnCs = (NgDaxXOnCs) obj;
        return (this.getsocial == ngDaxXOnCs.getsocial || (this.getsocial != null && this.getsocial.equals(ngDaxXOnCs.getsocial))) && (this.attribution == ngDaxXOnCs.attribution || (this.attribution != null && this.attribution.equals(ngDaxXOnCs.attribution))) && (this.acquisition == ngDaxXOnCs.acquisition || (this.acquisition != null && this.acquisition.equals(ngDaxXOnCs.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THTagsQuery{limit=" + this.getsocial + ", name=" + this.attribution + ", feedId=" + this.acquisition + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, NgDaxXOnCs ngDaxXOnCs) {
        if (ngDaxXOnCs.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 8);
            zotoebnojf.getsocial(ngDaxXOnCs.getsocial.intValue());
        }
        if (ngDaxXOnCs.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(ngDaxXOnCs.attribution);
        }
        if (ngDaxXOnCs.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(ngDaxXOnCs.acquisition);
        }
        zotoebnojf.getsocial();
    }
}
