package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: THPostAuthor */
public final class xlPHPMtUBa {
    public String acquisition;
    public String attribution;
    public Boolean dau;
    public String getsocial;
    public Map<String, String> mau;
    public List<JbBdMtJmlU> mobile;
    public Boolean retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof xlPHPMtUBa)) {
            return false;
        }
        xlPHPMtUBa xlphpmtuba = (xlPHPMtUBa) obj;
        return (this.getsocial == xlphpmtuba.getsocial || (this.getsocial != null && this.getsocial.equals(xlphpmtuba.getsocial))) && (this.attribution == xlphpmtuba.attribution || (this.attribution != null && this.attribution.equals(xlphpmtuba.attribution))) && ((this.acquisition == xlphpmtuba.acquisition || (this.acquisition != null && this.acquisition.equals(xlphpmtuba.acquisition))) && ((this.mobile == xlphpmtuba.mobile || (this.mobile != null && this.mobile.equals(xlphpmtuba.mobile))) && ((this.retention == xlphpmtuba.retention || (this.retention != null && this.retention.equals(xlphpmtuba.retention))) && ((this.dau == xlphpmtuba.dau || (this.dau != null && this.dau.equals(xlphpmtuba.dau))) && (this.mau == xlphpmtuba.mau || (this.mau != null && this.mau.equals(xlphpmtuba.mau)))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035;
        if (this.mau != null) {
            i = this.mau.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THPostAuthor{id=" + this.getsocial + ", displayName=" + this.attribution + ", avatarUrl=" + this.acquisition + ", identities=" + this.mobile + ", verified=" + this.retention + ", isApp=" + this.dau + ", publicProperties=" + this.mau + "}";
    }

    public static xlPHPMtUBa getsocial(zoToeBNOjF zotoebnojf) {
        xlPHPMtUBa xlphpmtuba = new xlPHPMtUBa();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xlphpmtuba.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xlphpmtuba.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xlphpmtuba.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention2 = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention2.attribution);
                            while (i < retention2.attribution) {
                                arrayList.add(JbBdMtJmlU.getsocial(zotoebnojf));
                                i++;
                            }
                            xlphpmtuba.mobile = arrayList;
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xlphpmtuba.retention = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xlphpmtuba.dau = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            xlphpmtuba.mau = hashMap;
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return xlphpmtuba;
            }
        }
    }
}
