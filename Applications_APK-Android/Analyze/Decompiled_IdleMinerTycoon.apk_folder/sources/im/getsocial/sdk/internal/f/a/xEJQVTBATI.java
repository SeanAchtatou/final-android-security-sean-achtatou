package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THUploadChunkSize */
public final class xEJQVTBATI {
    public Long acquisition;
    public Long attribution;
    public Long getsocial;
    public Long mobile;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof xEJQVTBATI)) {
            return false;
        }
        xEJQVTBATI xejqvtbati = (xEJQVTBATI) obj;
        return (this.getsocial == xejqvtbati.getsocial || (this.getsocial != null && this.getsocial.equals(xejqvtbati.getsocial))) && (this.attribution == xejqvtbati.attribution || (this.attribution != null && this.attribution.equals(xejqvtbati.attribution))) && ((this.acquisition == xejqvtbati.acquisition || (this.acquisition != null && this.acquisition.equals(xejqvtbati.acquisition))) && (this.mobile == xejqvtbati.mobile || (this.mobile != null && this.mobile.equals(xejqvtbati.mobile))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035;
        if (this.mobile != null) {
            i = this.mobile.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THUploadChunkSize{wifi=" + this.getsocial + ", lte=" + this.attribution + ", tg=" + this.acquisition + ", other=" + this.mobile + "}";
    }

    public static xEJQVTBATI getsocial(zoToeBNOjF zotoebnojf) {
        xEJQVTBATI xejqvtbati = new xEJQVTBATI();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xejqvtbati.getsocial = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xejqvtbati.attribution = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xejqvtbati.acquisition = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xejqvtbati.mobile = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return xejqvtbati;
            }
        }
    }
}
