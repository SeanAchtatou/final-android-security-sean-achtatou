package im.getsocial.sdk.internal.m;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.internal.view.SupportMenu;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import com.appsflyer.share.Constants;
import im.getsocial.sdk.sharedl10n.Localization;
import im.getsocial.sdk.sharedl10n.generated.LanguageStrings;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* compiled from: Utilities */
public class iFpupLCESp {
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL attribution = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(iFpupLCESp.class);
    private static SimpleDateFormat getsocial = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());

    /* compiled from: Utilities */
    public interface cjrhisSQCL {
        void getsocial(Uri uri);
    }

    /* compiled from: Utilities */
    public interface jjbQypPegg {
        void getsocial(String str);
    }

    private iFpupLCESp() {
    }

    public static String getsocial(String str) {
        if (str == null) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "UTF-8").replace("-", "%2D");
        } catch (UnsupportedEncodingException e) {
            attribution.acquisition(e);
            return "";
        }
    }

    public static byte[] getsocial(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static Uri getsocial(Context context, Bitmap bitmap, boolean z, String str) {
        String str2 = fOrCGNYyfk.getsocial(context);
        if (str2 != null && attribution(context, bitmap, z, str)) {
            return new Uri.Builder().scheme("content").authority(str2).path(fOrCGNYyfk.getsocial(str)).build();
        }
        return null;
    }

    public static void getsocial(final Context context, String str, final cjrhisSQCL cjrhissqcl) {
        String str2;
        final String str3;
        if (str.endsWith("gif")) {
            str3 = "getsocial-smartinvite-tempgif.gif";
            str2 = "smart-invite-gif.gif";
        } else if (str.endsWith("mp4")) {
            str3 = "getsocial-smartinvite-tempvideo.mp4";
            str2 = "smart-invite-video.mp4";
        } else {
            attribution.mobile("Invalid invite resource format.");
            return;
        }
        try {
            new upgqDBbsrL(new jjbQypPegg() {
                public final void getsocial(String str) {
                    cjrhissqcl.getsocial(new Uri.Builder().scheme("content").authority(fOrCGNYyfk.getsocial(context)).path(fOrCGNYyfk.getsocial(str3)).build());
                }
            }).execute(context, str, str3, str2);
        } catch (Exception e) {
            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl2 = attribution;
            cjrhissqcl2.mobile("Could not writeRemoteResourceFileToDisk contents of url, returning null. error: " + e.getMessage());
        }
    }

    public static void getsocial(Context context, String str, String str2, jjbQypPegg jjbqyppegg) {
        try {
            new upgqDBbsrL(jjbqyppegg).execute(context, str, str2 + str.substring(str.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1) + ".mp4");
        } catch (Exception e) {
            attribution.mobile("Could not download contents of url, returning null. error: " + e.getMessage());
        }
    }

    /* compiled from: Utilities */
    static class upgqDBbsrL extends AsyncTask<Object, Integer, String> {
        private final jjbQypPegg getsocial;

        /* access modifiers changed from: protected */
        public /* synthetic */ void onPostExecute(Object obj) {
            this.getsocial.getsocial((String) obj);
        }

        upgqDBbsrL(jjbQypPegg jjbqyppegg) {
            this.getsocial = jjbqyppegg;
        }

        /* access modifiers changed from: protected */
        public /* synthetic */ Object doInBackground(Object[] objArr) {
            Context context = (Context) objArr[0];
            if (fOrCGNYyfk.getsocial(context) == null) {
                return null;
            }
            String str = (String) objArr[2];
            File file = new File(context.getCacheDir(), str);
            if (file.exists() && !file.delete()) {
                im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = iFpupLCESp.attribution;
                cjrhissqcl.attribution("Failed to delete file " + file.getPath());
                return null;
            } else if (!iFpupLCESp.attribution(file, (String) objArr[1])) {
                return null;
            } else {
                return String.format("file://%s/%s", context.getCacheDir(), str);
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean attribution(File file, String str) {
        InputStream inputStream;
        FileOutputStream fileOutputStream = null;
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(true);
            httpURLConnection.connect();
            inputStream = httpURLConnection.getInputStream();
            try {
                if (!file.createNewFile()) {
                    attribution.attribution("Couldn't create new file " + file.getPath());
                    getsocial((Closeable) null);
                    getsocial(inputStream);
                    return false;
                }
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    byte[] bArr = new byte[256];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (-1 != read) {
                            fileOutputStream2.write(bArr, 0, read);
                        } else {
                            getsocial(fileOutputStream2);
                            getsocial(inputStream);
                            return true;
                        }
                    }
                } catch (IOException e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    try {
                        attribution.mobile("Could not save url content to the cache directory, returning null. error: " + e.getMessage());
                        getsocial(fileOutputStream);
                        getsocial(inputStream);
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        getsocial(fileOutputStream);
                        getsocial(inputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    getsocial(fileOutputStream);
                    getsocial(inputStream);
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                attribution.mobile("Could not save url content to the cache directory, returning null. error: " + e.getMessage());
                getsocial(fileOutputStream);
                getsocial(inputStream);
                return false;
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            attribution.mobile("Could not save url content to the cache directory, returning null. error: " + e.getMessage());
            getsocial(fileOutputStream);
            getsocial(inputStream);
            return false;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            getsocial(fileOutputStream);
            getsocial(inputStream);
            throw th;
        }
    }

    private static boolean attribution(Context context, Bitmap bitmap, boolean z, String str) {
        Bitmap.CompressFormat compressFormat;
        File file = new File(context.getCacheDir(), str);
        if (!file.exists() || file.delete()) {
            FileOutputStream fileOutputStream = null;
            try {
                if (!file.createNewFile()) {
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = attribution;
                    cjrhissqcl.attribution("Couldn't create the new file " + file.getPath());
                    getsocial((Closeable) null);
                    return false;
                }
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                if (z) {
                    try {
                        compressFormat = Bitmap.CompressFormat.PNG;
                    } catch (IOException e) {
                        e = e;
                        fileOutputStream = fileOutputStream2;
                        try {
                            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl2 = attribution;
                            cjrhissqcl2.mobile("Could not save image to the cache directory, returning null. error: " + e.getMessage());
                            getsocial(fileOutputStream);
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            getsocial(fileOutputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = fileOutputStream2;
                        getsocial(fileOutputStream);
                        throw th;
                    }
                } else {
                    compressFormat = Bitmap.CompressFormat.JPEG;
                }
                bitmap.compress(compressFormat, 100, fileOutputStream2);
                getsocial(fileOutputStream2);
                return true;
            } catch (IOException e2) {
                e = e2;
                im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl22 = attribution;
                cjrhissqcl22.mobile("Could not save image to the cache directory, returning null. error: " + e.getMessage());
                getsocial(fileOutputStream);
                return false;
            }
        } else {
            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl3 = attribution;
            cjrhissqcl3.attribution("Couldn't delete the old file " + file.getPath());
            return false;
        }
    }

    public static void getsocial(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = attribution;
                cjrhissqcl.attribution("Failed to close the stream: " + e.getMessage());
            }
        }
    }

    public static Bitmap attribution(String str) {
        int i = str.length() > 40 ? 400 : 200;
        int i2 = i + 30;
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-1);
        TextPaint textPaint = new TextPaint(1);
        textPaint.setTextSize(25.0f);
        textPaint.setColor((int) SupportMenu.CATEGORY_MASK);
        textPaint.setAntiAlias(true);
        StaticLayout staticLayout = new StaticLayout(str, textPaint, i, str.length() > 40 ? Layout.Alignment.ALIGN_NORMAL : Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
        RectF rectF = new RectF();
        rectF.bottom = (float) (staticLayout.getHeight() + 30);
        rectF.right = (float) i2;
        rectF.left = 0.0f;
        rectF.top = 0.0f;
        Bitmap createBitmap = Bitmap.createBitmap(i2, staticLayout.getHeight() + 30, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawRoundRect(rectF, 15.0f, 15.0f, paint);
        canvas.save();
        canvas.translate(15.0f, 15.0f);
        staticLayout.draw(canvas);
        return createBitmap;
    }

    public static String getsocial(long j, Localization localization) {
        synchronized (iFpupLCESp.class) {
            long millis = TimeUnit.SECONDS.toMillis(1);
            long millis2 = TimeUnit.SECONDS.toMillis(30);
            long millis3 = TimeUnit.MINUTES.toMillis(1);
            long millis4 = TimeUnit.HOURS.toMillis(1);
            long millis5 = TimeUnit.DAYS.toMillis(1);
            long millis6 = TimeUnit.DAYS.toMillis(7);
            long j2 = 1000 * j;
            long millis7 = TimeUnit.DAYS.toMillis(21);
            long currentTimeMillis = System.currentTimeMillis() - j2;
            LanguageStrings strings = localization.strings();
            if (currentTimeMillis < millis2) {
                String str = strings.TimestampJustNow;
                return str;
            } else if (currentTimeMillis < millis3) {
                String str2 = strings.TimestampSeconds;
                Object[] objArr = new Object[1];
                double d = (double) currentTimeMillis;
                double d2 = (double) millis;
                Double.isNaN(d);
                Double.isNaN(d2);
                objArr[0] = Double.valueOf(Math.floor(d / d2));
                String format = String.format(str2, objArr);
                return format;
            } else if (currentTimeMillis < millis4) {
                String str3 = strings.TimestampMinutes;
                Object[] objArr2 = new Object[1];
                double d3 = (double) currentTimeMillis;
                double d4 = (double) millis3;
                Double.isNaN(d3);
                Double.isNaN(d4);
                objArr2[0] = Double.valueOf(Math.floor(d3 / d4));
                String format2 = String.format(str3, objArr2);
                return format2;
            } else if (currentTimeMillis < millis5) {
                String str4 = strings.TimestampHours;
                Object[] objArr3 = new Object[1];
                double d5 = (double) currentTimeMillis;
                double d6 = (double) millis4;
                Double.isNaN(d5);
                Double.isNaN(d6);
                objArr3[0] = Double.valueOf(Math.floor(d5 / d6));
                String format3 = String.format(str4, objArr3);
                return format3;
            } else if (currentTimeMillis < millis6) {
                String str5 = strings.TimestampDays;
                Object[] objArr4 = new Object[1];
                double d7 = (double) currentTimeMillis;
                double d8 = (double) millis5;
                Double.isNaN(d7);
                Double.isNaN(d8);
                objArr4[0] = Double.valueOf(Math.floor(d7 / d8));
                String format4 = String.format(str5, objArr4);
                return format4;
            } else if (currentTimeMillis < millis7) {
                String str6 = strings.TimestampWeeks;
                Object[] objArr5 = new Object[1];
                double d9 = (double) currentTimeMillis;
                double d10 = (double) millis6;
                Double.isNaN(d9);
                Double.isNaN(d10);
                objArr5[0] = Double.valueOf(Math.floor(d9 / d10));
                String format5 = String.format(str6, objArr5);
                return format5;
            } else {
                String format6 = getsocial.format(Long.valueOf(j2));
                return format6;
            }
        }
    }

    public static String attribution(long j, Localization localization) {
        long millis = TimeUnit.MINUTES.toMillis(1);
        long millis2 = TimeUnit.HOURS.toMillis(1);
        long millis3 = TimeUnit.DAYS.toMillis(1);
        long millis4 = TimeUnit.DAYS.toMillis(7);
        long currentTimeMillis = System.currentTimeMillis() - (j * 1000);
        LanguageStrings strings = localization.strings();
        if (currentTimeMillis < millis2) {
            String str = strings.TimestampMinutes;
            double d = (double) currentTimeMillis;
            double d2 = (double) millis;
            Double.isNaN(d);
            Double.isNaN(d2);
            return String.format(str, Double.valueOf(Math.floor(d / d2)));
        } else if (currentTimeMillis < millis3) {
            String str2 = strings.TimestampHours;
            double d3 = (double) currentTimeMillis;
            double d4 = (double) millis2;
            Double.isNaN(d3);
            Double.isNaN(d4);
            return String.format(str2, Double.valueOf(Math.floor(d3 / d4)));
        } else if (currentTimeMillis < millis4) {
            String str3 = strings.TimestampDays;
            double d5 = (double) currentTimeMillis;
            double d6 = (double) millis3;
            Double.isNaN(d5);
            Double.isNaN(d6);
            return String.format(str3, Double.valueOf(Math.floor(d5 / d6)));
        } else {
            String str4 = strings.TimestampWeeks;
            double d7 = (double) currentTimeMillis;
            double d8 = (double) millis4;
            Double.isNaN(d7);
            Double.isNaN(d8);
            return String.format(str4, Double.valueOf(Math.floor(d7 / d8)));
        }
    }

    public static String acquisition(String str) {
        if (str.contains(Constants.URL_PATH_DELIMITER)) {
            return "Posted at " + str;
        }
        return "Posted " + str + " ago";
    }
}
