package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THReceiptItem */
public final class ZWjsSaCmFq {
    public Long acquisition;
    public String attribution;
    public String getsocial;
    public Long mobile;
    public Long retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof ZWjsSaCmFq)) {
            return false;
        }
        ZWjsSaCmFq zWjsSaCmFq = (ZWjsSaCmFq) obj;
        return (this.getsocial == zWjsSaCmFq.getsocial || (this.getsocial != null && this.getsocial.equals(zWjsSaCmFq.getsocial))) && (this.attribution == zWjsSaCmFq.attribution || (this.attribution != null && this.attribution.equals(zWjsSaCmFq.attribution))) && ((this.acquisition == zWjsSaCmFq.acquisition || (this.acquisition != null && this.acquisition.equals(zWjsSaCmFq.acquisition))) && ((this.mobile == zWjsSaCmFq.mobile || (this.mobile != null && this.mobile.equals(zWjsSaCmFq.mobile))) && (this.retention == zWjsSaCmFq.retention || (this.retention != null && this.retention.equals(zWjsSaCmFq.retention)))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035;
        if (this.retention != null) {
            i = this.retention.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THReceiptItem{productID=" + this.getsocial + ", transactionID=" + this.attribution + ", purchaseDate=" + this.acquisition + ", quantity=" + this.mobile + ", isValid=" + this.retention + "}";
    }

    public static ZWjsSaCmFq getsocial(zoToeBNOjF zotoebnojf) {
        ZWjsSaCmFq zWjsSaCmFq = new ZWjsSaCmFq();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            zWjsSaCmFq.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            zWjsSaCmFq.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            zWjsSaCmFq.acquisition = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            zWjsSaCmFq.mobile = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            zWjsSaCmFq.retention = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return zWjsSaCmFq;
            }
        }
    }
}
