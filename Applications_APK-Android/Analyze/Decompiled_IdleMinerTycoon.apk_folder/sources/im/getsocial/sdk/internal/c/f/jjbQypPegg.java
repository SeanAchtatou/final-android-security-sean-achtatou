package im.getsocial.sdk.internal.c.f;

import com.ironsource.sdk.constants.Constants;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import java.io.PrintStream;
import java.util.Locale;

/* compiled from: ConsoleLogPrinter */
public class jjbQypPegg implements zoToeBNOjF {
    public final void getsocial(cjrhisSQCL.jjbQypPegg jjbqyppegg, String str, String str2, Object... objArr) {
        PrintStream printStream = jjbqyppegg == cjrhisSQCL.jjbQypPegg.ERROR ? System.err : System.out;
        printStream.println(Constants.RequestParameters.LEFT_BRACKETS + jjbqyppegg.name() + "] " + str + ": " + String.format(Locale.ENGLISH, str2, objArr));
    }
}
