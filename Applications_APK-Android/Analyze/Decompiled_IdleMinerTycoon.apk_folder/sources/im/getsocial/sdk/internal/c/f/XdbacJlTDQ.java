package im.getsocial.sdk.internal.c.f;

import android.util.Log;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import java.util.Locale;

/* compiled from: LogcatPrinter */
public class XdbacJlTDQ implements zoToeBNOjF {
    public final void getsocial(cjrhisSQCL.jjbQypPegg jjbqyppegg, String str, String str2, Object... objArr) {
        String format = String.format(Locale.ENGLISH, str2, objArr);
        switch (jjbqyppegg) {
            case OFF:
                return;
            case ERROR:
                Log.d(str, format);
                return;
            case WARN:
                Log.w(str, format);
                return;
            case INFO:
                Log.i(str, format);
                return;
            case DEBUG:
                Log.d(str, format);
                return;
            default:
                Log.v(str, format);
                return;
        }
    }
}
