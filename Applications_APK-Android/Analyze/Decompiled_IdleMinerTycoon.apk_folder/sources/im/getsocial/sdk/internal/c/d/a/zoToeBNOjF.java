package im.getsocial.sdk.internal.c.d.a;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;
import im.getsocial.sdk.invites.InviteCallback;

/* compiled from: InviteCallbackAdapter */
class zoToeBNOjF implements jjbQypPegg.C0016jjbQypPegg {
    private final InviteCallback getsocial;

    zoToeBNOjF(InviteCallback inviteCallback) {
        this.getsocial = inviteCallback;
    }

    public final void getsocial(GetSocialException getSocialException) {
        if (this.getsocial != null) {
            this.getsocial.onError(getSocialException);
        }
    }
}
