package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.HashMap;
import java.util.Map;

/* compiled from: THInviteContent */
public final class QhisXzMgay {
    public Map<String, String> acquisition;
    public Map<String, String> attribution;
    public String getsocial;
    public String mobile;
    public String retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof QhisXzMgay)) {
            return false;
        }
        QhisXzMgay qhisXzMgay = (QhisXzMgay) obj;
        return (this.getsocial == qhisXzMgay.getsocial || (this.getsocial != null && this.getsocial.equals(qhisXzMgay.getsocial))) && (this.attribution == qhisXzMgay.attribution || (this.attribution != null && this.attribution.equals(qhisXzMgay.attribution))) && ((this.acquisition == qhisXzMgay.acquisition || (this.acquisition != null && this.acquisition.equals(qhisXzMgay.acquisition))) && ((this.mobile == qhisXzMgay.mobile || (this.mobile != null && this.mobile.equals(qhisXzMgay.mobile))) && (this.retention == qhisXzMgay.retention || (this.retention != null && this.retention.equals(qhisXzMgay.retention)))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035;
        if (this.retention != null) {
            i = this.retention.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THInviteContent{imageUrl=" + this.getsocial + ", subject=" + this.attribution + ", text=" + this.acquisition + ", gifUrl=" + this.mobile + ", videoUrl=" + this.retention + "}";
    }

    public static QhisXzMgay getsocial(zoToeBNOjF zotoebnojf) {
        QhisXzMgay qhisXzMgay = new QhisXzMgay();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            qhisXzMgay.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            qhisXzMgay.attribution = hashMap;
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile3 = zotoebnojf.mobile();
                            HashMap hashMap2 = new HashMap(mobile3.acquisition);
                            while (i < mobile3.acquisition) {
                                hashMap2.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            qhisXzMgay.acquisition = hashMap2;
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            qhisXzMgay.mobile = zotoebnojf.ios();
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            qhisXzMgay.retention = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return qhisXzMgay;
            }
        }
    }
}
