package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THCreateTokenResponse */
public final class sqEuGXwfLT {
    public String acquisition;
    public String attribution;
    public String getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof sqEuGXwfLT)) {
            return false;
        }
        sqEuGXwfLT sqeugxwflt = (sqEuGXwfLT) obj;
        return (this.getsocial == sqeugxwflt.getsocial || (this.getsocial != null && this.getsocial.equals(sqeugxwflt.getsocial))) && (this.attribution == sqeugxwflt.attribution || (this.attribution != null && this.attribution.equals(sqeugxwflt.attribution))) && (this.acquisition == sqeugxwflt.acquisition || (this.acquisition != null && this.acquisition.equals(sqeugxwflt.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THCreateTokenResponse{url=" + this.getsocial + ", token=" + this.attribution + ", copycode=" + this.acquisition + "}";
    }

    public static sqEuGXwfLT getsocial(zoToeBNOjF zotoebnojf) {
        sqEuGXwfLT sqeugxwflt = new sqEuGXwfLT();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sqeugxwflt.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sqeugxwflt.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sqeugxwflt.acquisition = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return sqeugxwflt;
            }
        }
    }
}
