package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;
import java.util.Map;

/* compiled from: THAnalyticsBaseEvent */
public final class HptYHntaqF {
    public String acquisition;
    public Long attribution;
    public Boolean dau;
    public Map<String, String> getsocial;
    public IbawHMWljm mau;
    public String mobile;
    public Long retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof HptYHntaqF)) {
            return false;
        }
        HptYHntaqF hptYHntaqF = (HptYHntaqF) obj;
        return (this.getsocial == hptYHntaqF.getsocial || (this.getsocial != null && this.getsocial.equals(hptYHntaqF.getsocial))) && (this.attribution == hptYHntaqF.attribution || (this.attribution != null && this.attribution.equals(hptYHntaqF.attribution))) && ((this.acquisition == hptYHntaqF.acquisition || (this.acquisition != null && this.acquisition.equals(hptYHntaqF.acquisition))) && ((this.mobile == hptYHntaqF.mobile || (this.mobile != null && this.mobile.equals(hptYHntaqF.mobile))) && ((this.retention == hptYHntaqF.retention || (this.retention != null && this.retention.equals(hptYHntaqF.retention))) && ((this.dau == hptYHntaqF.dau || (this.dau != null && this.dau.equals(hptYHntaqF.dau))) && (this.mau == hptYHntaqF.mau || (this.mau != null && this.mau.equals(hptYHntaqF.mau)))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035;
        if (this.mau != null) {
            i = this.mau.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THAnalyticsBaseEvent{customProperties=" + this.getsocial + ", deviceTime=" + this.attribution + ", name=" + this.acquisition + ", id=" + this.mobile + ", retryCount=" + this.retention + ", isCustom=" + this.dau + ", deviceTimeType=" + this.mau + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, HptYHntaqF hptYHntaqF) {
        if (hptYHntaqF.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, hptYHntaqF.getsocial.size());
            for (Map.Entry next : hptYHntaqF.getsocial.entrySet()) {
                zotoebnojf.getsocial((String) next.getKey());
                zotoebnojf.getsocial((String) next.getValue());
            }
        }
        if (hptYHntaqF.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 10);
            zotoebnojf.getsocial(hptYHntaqF.attribution.longValue());
        }
        if (hptYHntaqF.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(hptYHntaqF.acquisition);
        }
        if (hptYHntaqF.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(hptYHntaqF.mobile);
        }
        if (hptYHntaqF.retention != null) {
            zotoebnojf.getsocial(5, (byte) 10);
            zotoebnojf.getsocial(hptYHntaqF.retention.longValue());
        }
        if (hptYHntaqF.dau != null) {
            zotoebnojf.getsocial(6, (byte) 2);
            zotoebnojf.getsocial(hptYHntaqF.dau.booleanValue());
        }
        if (hptYHntaqF.mau != null) {
            zotoebnojf.getsocial(7, (byte) 8);
            zotoebnojf.getsocial(hptYHntaqF.mau.value);
        }
        zotoebnojf.getsocial();
    }
}
