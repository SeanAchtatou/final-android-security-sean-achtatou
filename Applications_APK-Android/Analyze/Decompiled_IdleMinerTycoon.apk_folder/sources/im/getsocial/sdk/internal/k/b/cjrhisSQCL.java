package im.getsocial.sdk.internal.k.b;

import com.ironsource.sdk.constants.Constants;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.QCXFOjcJkE;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.k.a.pdwpUtZXDT;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/* compiled from: UploadMediaFunc */
public class cjrhisSQCL {
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL acquisition = upgqDBbsrL.getsocial(cjrhisSQCL.class);
    private static final Map<String, pdwpUtZXDT> mobile = new HashMap();
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.i.upgqDBbsrL attribution;
    @XdbacJlTDQ
    QCXFOjcJkE getsocial;

    public cjrhisSQCL() {
        ztWNWCuZiM.getsocial(this);
    }

    private static String getsocial(im.getsocial.sdk.internal.k.a.upgqDBbsrL upgqdbbsrl) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(upgqdbbsrl.getsocial());
            return new BigInteger(1, instance.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = acquisition;
            cjrhissqcl.attribution("Could not calculate hash for media content, error: " + e.getMessage());
            return null;
        }
    }

    public final pdwpUtZXDT getsocial(im.getsocial.sdk.internal.k.a.upgqDBbsrL upgqdbbsrl, im.getsocial.sdk.internal.k.a.XdbacJlTDQ xdbacJlTDQ) {
        if (upgqdbbsrl == null) {
            return null;
        }
        long attribution2 = this.attribution.dau().attribution();
        long length = (long) upgqdbbsrl.getsocial().length;
        if (length <= attribution2) {
            final String str = getsocial(upgqdbbsrl);
            if (str != null && mobile.containsKey(str)) {
                return mobile.get(str);
            }
            CountDownLatch countDownLatch = new CountDownLatch(1);
            pdwpUtZXDT[] pdwputzxdtArr = new pdwpUtZXDT[1];
            final pdwpUtZXDT[] pdwputzxdtArr2 = pdwputzxdtArr;
            final CountDownLatch countDownLatch2 = countDownLatch;
            final im.getsocial.sdk.internal.k.a.upgqDBbsrL upgqdbbsrl2 = upgqdbbsrl;
            this.getsocial.getsocial(upgqdbbsrl, xdbacJlTDQ, new Callback<pdwpUtZXDT>() {
                public /* synthetic */ void onSuccess(Object obj) {
                    pdwpUtZXDT pdwputzxdt = (pdwpUtZXDT) obj;
                    cjrhisSQCL.getsocial(str, pdwputzxdt);
                    pdwputzxdtArr2[0] = pdwputzxdt;
                    countDownLatch2.countDown();
                }

                public void onFailure(GetSocialException getSocialException) {
                    cjrhisSQCL.acquisition.attribution("Failed to upload media: %s, error: %s", upgqdbbsrl2, getSocialException.getMessage());
                    pdwputzxdtArr2[0] = null;
                    countDownLatch2.countDown();
                }
            });
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return pdwputzxdtArr[0];
        }
        throw new GetSocialException(ErrorCode.MEDIAUPLOAD_FILE_SIZE_OVER_LIMIT, "The file you want to upload is too large [" + length + "], limit is [" + attribution2 + Constants.RequestParameters.RIGHT_BRACKETS);
    }

    static /* synthetic */ void getsocial(String str, pdwpUtZXDT pdwputzxdt) {
        if (str != null) {
            mobile.put(str, pdwputzxdt);
        }
    }
}
