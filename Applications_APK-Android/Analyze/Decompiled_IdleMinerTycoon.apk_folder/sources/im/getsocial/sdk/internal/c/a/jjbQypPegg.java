package im.getsocial.sdk.internal.c.a;

import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.activities.ReportingReason;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.iap.a.b.upgqDBbsrL;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.KkSvQPDhNi;
import im.getsocial.sdk.internal.c.KluUZYuxme;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.ReferralData;
import im.getsocial.sdk.invites.ReferredUser;
import im.getsocial.sdk.invites.a.a.cjrhisSQCL;
import im.getsocial.sdk.invites.a.b.XdbacJlTDQ;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationsCountQuery;
import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import im.getsocial.sdk.pushnotifications.NotificationsSummary;
import im.getsocial.sdk.socialgraph.SuggestedFriend;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.PrivateUser;
import im.getsocial.sdk.usermanagement.PublicUser;
import im.getsocial.sdk.usermanagement.UserReference;
import im.getsocial.sdk.usermanagement.UsersQuery;
import im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT;
import java.util.List;
import java.util.Map;

/* compiled from: SyncCommunicationLayer */
public interface jjbQypPegg {
    Integer acquisition(String str);

    Integer acquisition(String str, List<String> list);

    List<UserReference> acquisition();

    ActivityPost attribution(String str, im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg);

    PrivateUser attribution(AuthIdentity authIdentity);

    PublicUser attribution(String str);

    Integer attribution();

    Integer attribution(String str, List<String> list);

    List<SuggestedFriend> attribution(int i, int i2);

    List<ActivityPost> attribution(String str, ActivitiesQuery activitiesQuery);

    void attribution(List<String> list);

    PromoCode cat(String str);

    List<ActivityPost> dau(String str);

    ActivityPost getsocial(String str, im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg);

    ActivityPost getsocial(String str, boolean z);

    upgqDBbsrL getsocial(im.getsocial.sdk.iap.a.b.jjbQypPegg jjbqyppegg);

    im.getsocial.sdk.internal.i.b.upgqDBbsrL getsocial(im.getsocial.sdk.internal.i.b.jjbQypPegg jjbqyppegg);

    ReferralData getsocial(KluUZYuxme kluUZYuxme, boolean z, Map<cjrhisSQCL, Map<String, String>> map, Map<String, String> map2);

    XdbacJlTDQ getsocial(String str, LinkParams linkParams);

    PromoCode getsocial(PromoCodeBuilder promoCodeBuilder);

    NotificationsSummary getsocial(List<String> list, im.getsocial.sdk.pushnotifications.a.b.upgqDBbsrL upgqdbbsrl);

    PrivateUser getsocial(AuthIdentity authIdentity);

    PrivateUser getsocial(pdwpUtZXDT pdwputzxdt);

    PrivateUser getsocial(String str);

    im.getsocial.sdk.usermanagement.a.a.upgqDBbsrL getsocial(im.getsocial.sdk.usermanagement.a.a.jjbQypPegg jjbqyppegg);

    Integer getsocial(NotificationsCountQuery notificationsCountQuery);

    Integer getsocial(List<String> list);

    List<ReferredUser> getsocial();

    List<PublicUser> getsocial(int i, int i2);

    List<String> getsocial(TagsQuery tagsQuery);

    List<Notification> getsocial(NotificationsQuery notificationsQuery);

    List<UserReference> getsocial(UsersQuery usersQuery);

    List<PublicUser> getsocial(String str, int i, int i2);

    List<ActivityPost> getsocial(String str, ActivitiesQuery activitiesQuery);

    Map<String, PublicUser> getsocial(String str, List<String> list);

    void getsocial(JbBdMtJmlU jbBdMtJmlU);

    void getsocial(JbBdMtJmlU jbBdMtJmlU, List<im.getsocial.sdk.internal.a.b.jjbQypPegg> list);

    void getsocial(String str, ReportingReason reportingReason);

    void getsocial(String str, String str2, KkSvQPDhNi kkSvQPDhNi, Boolean bool);

    void getsocial(List<String> list, String str);

    void getsocial(boolean z);

    ActivityPost mau(String str);

    Boolean mobile();

    Integer mobile(String str);

    Integer mobile(String str, List<String> list);

    Boolean retention(String str);

    PromoCode viral(String str);
}
