package im.getsocial.sdk.internal.c.c;

import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;

/* compiled from: GetSocialInitializationException */
public class upgqDBbsrL extends GetSocialException {
    public upgqDBbsrL(String str) {
        super(ErrorCode.SDK_INITIALIZATION_FAILED, str);
    }
}
