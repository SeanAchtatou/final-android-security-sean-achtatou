package im.getsocial.sdk.internal.j;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;

/* compiled from: LanguageCodeValidator */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static String getsocial(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "Language code cannot be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(str.length() >= 2 && str.length() <= 7, "Invalid language code length, can't be shorter than 2 and longer than 7");
        if (upgqDBbsrL.getsocial(str)) {
            return str;
        }
        String attribution = upgqDBbsrL.attribution(str);
        if (attribution == null) {
            attribution = str.substring(0, 2);
            if (!upgqDBbsrL.getsocial(attribution)) {
                throw new IllegalArgumentException("Language code " + str + " is not valid");
            }
        }
        return attribution;
    }
}
