package im.getsocial.sdk.internal.a.e;

import im.getsocial.sdk.internal.f.a.HptYHntaqF;
import im.getsocial.sdk.internal.f.a.IbawHMWljm;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ThriftyAnalyticsConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static HptYHntaqF getsocial(im.getsocial.sdk.internal.a.b.jjbQypPegg jjbqyppegg) {
        IbawHMWljm ibawHMWljm;
        HptYHntaqF hptYHntaqF = new HptYHntaqF();
        Map<String, String> attribution = jjbqyppegg.attribution();
        HashMap hashMap = new HashMap();
        for (Map.Entry next : attribution.entrySet()) {
            if (next.getValue() != null) {
                hashMap.put(next.getKey(), next.getValue());
            }
        }
        hptYHntaqF.getsocial = hashMap;
        hptYHntaqF.attribution = Long.valueOf(jjbqyppegg.acquisition());
        hptYHntaqF.acquisition = jjbqyppegg.getsocial();
        hptYHntaqF.mobile = jjbqyppegg.mobile();
        hptYHntaqF.retention = Long.valueOf(jjbqyppegg.retention());
        hptYHntaqF.dau = Boolean.valueOf(jjbqyppegg.mau());
        switch (jjbqyppegg.cat()) {
            case SERVER_TIME:
                ibawHMWljm = IbawHMWljm.SERVER_TIME;
                break;
            case DEVICE_UPTIME:
                ibawHMWljm = IbawHMWljm.DEVICE_UPTIME;
                break;
            default:
                ibawHMWljm = IbawHMWljm.LOCAL_TIME;
                break;
        }
        hptYHntaqF.mau = ibawHMWljm;
        return hptYHntaqF;
    }
}
