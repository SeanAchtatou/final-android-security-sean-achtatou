package im.getsocial.sdk.internal.m;

import android.content.Context;
import com.google.android.gms.common.wrappers.InstantApps;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;

/* compiled from: InstantAppHelper */
public final class KluUZYuxme {
    private static final cjrhisSQCL attribution = upgqDBbsrL.getsocial(KluUZYuxme.class);
    @XdbacJlTDQ
    Context getsocial;

    private KluUZYuxme() {
        ztWNWCuZiM.getsocial(this);
    }

    public static boolean getsocial() {
        return new KluUZYuxme().attribution();
    }

    private boolean attribution() {
        try {
            boolean isInstantApp = InstantApps.isInstantApp(this.getsocial);
            cjrhisSQCL cjrhissqcl = attribution;
            cjrhissqcl.attribution("Is InstantApp:" + isInstantApp);
            return isInstantApp;
        } catch (Throwable th) {
            cjrhisSQCL cjrhissqcl2 = attribution;
            cjrhissqcl2.attribution("InstantApps is not available, error: " + th);
            return false;
        }
    }
}
