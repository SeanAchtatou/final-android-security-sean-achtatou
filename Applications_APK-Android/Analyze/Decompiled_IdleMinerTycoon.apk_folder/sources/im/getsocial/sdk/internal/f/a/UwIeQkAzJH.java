package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;
import java.util.List;

/* compiled from: THNotificationsSetStatusParams */
public final class UwIeQkAzJH {
    public String attribution;
    public List<String> getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof UwIeQkAzJH)) {
            return false;
        }
        UwIeQkAzJH uwIeQkAzJH = (UwIeQkAzJH) obj;
        return (this.getsocial == uwIeQkAzJH.getsocial || (this.getsocial != null && this.getsocial.equals(uwIeQkAzJH.getsocial))) && (this.attribution == uwIeQkAzJH.attribution || (this.attribution != null && this.attribution.equals(uwIeQkAzJH.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035 * -2128831035 * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THNotificationsSetStatusParams{ids=" + this.getsocial + ", isRead=" + ((Object) null) + ", appId=" + ((String) null) + ", status=" + this.attribution + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, UwIeQkAzJH uwIeQkAzJH) {
        if (uwIeQkAzJH.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 15);
            zotoebnojf.getsocial((byte) 11, uwIeQkAzJH.getsocial.size());
            for (String str : uwIeQkAzJH.getsocial) {
                zotoebnojf.getsocial(str);
            }
        }
        if (uwIeQkAzJH.attribution != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(uwIeQkAzJH.attribution);
        }
        zotoebnojf.getsocial();
    }
}
