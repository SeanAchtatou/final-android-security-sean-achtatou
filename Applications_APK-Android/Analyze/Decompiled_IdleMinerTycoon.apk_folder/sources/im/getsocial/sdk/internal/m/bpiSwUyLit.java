package im.getsocial.sdk.internal.m;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import im.getsocial.sdk.internal.c.fOrCGNYyfk;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.File;
import java.util.Calendar;
import java.util.Locale;

/* compiled from: SystemInformation */
public final class bpiSwUyLit {
    private static final String[] acquisition = {"com.devadvance.rootcloak", "com.devadvance.rootcloakplus", "de.robv.android.xposed.installer", "com.saurik.substrate", "com.zachspong.temprootremovejb", "com.amphoras.hidemyroot", "com.amphoras.hidemyrootadfree", "com.formyhm.hiderootPremium", "com.formyhm.hideroot"};
    private static final String[] attribution = {"com.koushikdutta.rommanager", "com.koushikdutta.rommanager.license", "com.dimonvideo.luckypatcher", "com.chelpus.lackypatch", "com.ramdroid.appquarantine", "com.ramdroid.appquarantinepro", "com.android.vending.billing.InAppBillingService.COIN", "com.chelpus.luckypatcher"};
    private static final String[] getsocial = {"com.noshufou.android.su", "com.noshufou.android.su.elite", "eu.chainfire.supersu", "com.koushikdutta.superuser", "com.thirdparty.superuser", "com.yellowes.su", "com.topjohnwu.magisk"};
    private static final String[] mobile = {"/data/local/", "/data/local/bin/", "/data/local/xbin/", "/sbin/", "/su/bin/", "/system/bin/", "/system/bin/.ext/", "/system/bin/failsafe/", "/system/sd/xbin/", "/system/usr/we-need-root/", "/system/xbin/", "/cache", "/data", "/dev"};
    private static final String[] retention = {"su", "magisk", "busybox"};

    public static String getsocial() {
        return "ANDROID";
    }

    private static String getsocial(String str) {
        return str == null ? "" : str;
    }

    private bpiSwUyLit() {
    }

    public static String attribution() {
        return getsocial(Build.VERSION.RELEASE);
    }

    public static String mobile(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    public static String retention(Context context) {
        return context.getApplicationInfo().packageName;
    }

    public static String dau(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
            return "";
        }
    }

    public static String mau(Context context) {
        int i;
        try {
            i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            i = 0;
        }
        return Integer.toString(i);
    }

    public static String acquisition() {
        return Calendar.getInstance().getTimeZone().getID();
    }

    public static long mobile() {
        return System.currentTimeMillis() / 1000;
    }

    public static long retention() {
        return SystemClock.elapsedRealtime() / 1000;
    }

    public static String dau() {
        return Locale.getDefault().getLanguage() + "-" + Locale.getDefault().getCountry();
    }

    public static String cat(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        return getsocial(telephonyManager.getNetworkOperatorName());
    }

    public static String mau() {
        return getsocial(Build.MANUFACTURER);
    }

    public static String cat() {
        return getsocial(Build.MODEL);
    }

    public static String getsocial(sqEuGXwfLT sqeugxwflt) {
        String str = sqeugxwflt.getsocial("im.getsocial.sdk.Runtime");
        return str == null ? "NATIVE" : str;
    }

    public static String attribution(sqEuGXwfLT sqeugxwflt) {
        return getsocial(sqeugxwflt.getsocial("im.getsocial.sdk.RuntimeVersion"));
    }

    public static String acquisition(sqEuGXwfLT sqeugxwflt) {
        return getsocial(sqeugxwflt.getsocial("im.getsocial.sdk.WrapperVersion"));
    }

    public static String viral(Context context) {
        return upgqDBbsrL.getsocial(context);
    }

    public static boolean organic(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            if (packageManager.getPackageInfo(context.getPackageName(), 0).firstInstallTime == packageManager.getPackageInfo(context.getPackageName(), 0).lastUpdateTime) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static fOrCGNYyfk getsocial(XdbacJlTDQ xdbacJlTDQ) {
        NetworkInfo networkInfo;
        ConnectivityManager acquisition2 = xdbacJlTDQ.acquisition();
        if (acquisition2 == null) {
            networkInfo = null;
        } else {
            networkInfo = acquisition2.getActiveNetworkInfo();
        }
        if (networkInfo == null) {
            return new fOrCGNYyfk();
        }
        return new fOrCGNYyfk(networkInfo.getTypeName(), networkInfo.getSubtypeName());
    }

    private static boolean getsocial(PackageManager packageManager, String... strArr) {
        for (String str : strArr) {
            if (getsocial(packageManager, str)) {
                return true;
            }
        }
        return false;
    }

    private static boolean getsocial(PackageManager packageManager, String str) {
        try {
            packageManager.getPackageInfo(str, 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static int getsocial(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int attribution(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static float acquisition(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static boolean growth(Context context) {
        boolean z;
        boolean z2;
        String str = getsocial(Build.MODEL);
        boolean z3 = str.startsWith("sdk") || CommonUtils.GOOGLE_SDK.equals(str) || str.contains("Emulator") || str.contains("Android SDK");
        String str2 = Build.TAGS;
        if (!z3 && str2 != null && str2.contains("test-keys")) {
            return true;
        }
        PackageManager packageManager = context.getPackageManager();
        if (!(getsocial(packageManager, getsocial) || getsocial(packageManager, attribution) || getsocial(packageManager, acquisition))) {
            if (!z3) {
                String[] strArr = retention;
                int i = 0;
                while (true) {
                    if (i >= 3) {
                        z = false;
                        break;
                    }
                    String str3 = strArr[i];
                    String[] strArr2 = mobile;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= 14) {
                            z2 = false;
                            break;
                        } else if (new File(strArr2[i2], str3).exists()) {
                            z2 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (z2) {
                        z = true;
                        break;
                    }
                    i++;
                }
                if (z) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
}
