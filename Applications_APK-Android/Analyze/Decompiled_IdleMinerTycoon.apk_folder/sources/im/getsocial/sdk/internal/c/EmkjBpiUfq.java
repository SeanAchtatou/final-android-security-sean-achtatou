package im.getsocial.sdk.internal.c;

import java.util.HashMap;
import java.util.Map;

/* compiled from: MediaUploadSettings */
public class EmkjBpiUfq {
    private final Map<String, Integer> acquisition = new HashMap();
    private final long attribution;
    private final String getsocial;

    public EmkjBpiUfq(String str, long j) {
        this.getsocial = str;
        this.attribution = j;
        this.acquisition.put("WIFI", 512000);
        this.acquisition.put("LTE", 512000);
        this.acquisition.put("3G", 512000);
        this.acquisition.put("OTHER", 512000);
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final int getsocial(String str) {
        return this.acquisition.get(str).intValue();
    }

    public final void getsocial(String str, int i) {
        this.acquisition.put(str, Integer.valueOf(i));
    }

    public final long attribution() {
        return this.attribution;
    }
}
