package im.getsocial.sdk.internal.c;

import android.os.Build;
import im.getsocial.sdk.internal.c.b.cjrhisSQCL;
import im.getsocial.sdk.internal.c.b.pdwpUtZXDT;
import java.util.HashMap;
import java.util.Map;

/* compiled from: DeviceInfoComponentProvider */
public final class qZypgoeblR implements cjrhisSQCL<Map> {
    public final /* synthetic */ Object getsocial(pdwpUtZXDT pdwputzxdt) {
        HashMap hashMap = new HashMap();
        hashMap.put("PRODUCT", Build.PRODUCT);
        hashMap.put("MANUFACTURER", Build.MANUFACTURER);
        hashMap.put("DEVICE", Build.DEVICE);
        hashMap.put("MODEL", Build.MODEL);
        hashMap.put("HARDWARE", Build.HARDWARE);
        hashMap.put("FINGERPRINT", Build.FINGERPRINT);
        return hashMap;
    }
}
