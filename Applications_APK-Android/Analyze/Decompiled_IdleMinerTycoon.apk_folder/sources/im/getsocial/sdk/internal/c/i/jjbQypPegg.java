package im.getsocial.sdk.internal.c.i;

import im.getsocial.sdk.internal.c.QWVUXapsSm;
import im.getsocial.sdk.internal.c.b.KluUZYuxme;
import im.getsocial.sdk.internal.c.b.qdyNCsqjKt;
import im.getsocial.sdk.usermanagement.OnUserChangedListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/* compiled from: CoreAppRepo */
public class jjbQypPegg implements KluUZYuxme {
    private final LinkedList<Runnable> acquisition = new LinkedList<>();
    private final Map<qdyNCsqjKt, QWVUXapsSm> attribution = new HashMap();
    private final Object getsocial = new Object();
    private Runnable mobile;
    private OnUserChangedListener retention;

    public jjbQypPegg() {
        mobile();
    }

    public final QWVUXapsSm getsocial(qdyNCsqjKt qdyncsqjkt) {
        QWVUXapsSm qWVUXapsSm;
        synchronized (this.getsocial) {
            qWVUXapsSm = this.attribution.get(qdyncsqjkt);
        }
        return qWVUXapsSm;
    }

    public final void attribution(qdyNCsqjKt qdyncsqjkt) {
        getsocial(qdyncsqjkt, QWVUXapsSm.INITIALIZING);
    }

    public final void acquisition(qdyNCsqjKt qdyncsqjkt) {
        getsocial(qdyncsqjkt, QWVUXapsSm.INITIALIZED);
        retention(qdyncsqjkt);
    }

    public final void mobile(qdyNCsqjKt qdyncsqjkt) {
        getsocial(qdyncsqjkt, QWVUXapsSm.UNINITIALIZED);
        retention(qdyncsqjkt);
    }

    private void mobile() {
        synchronized (this.getsocial) {
            for (qdyNCsqjKt put : qdyNCsqjKt.values()) {
                this.attribution.put(put, QWVUXapsSm.UNINITIALIZED);
            }
        }
    }

    private void getsocial(qdyNCsqjKt qdyncsqjkt, QWVUXapsSm qWVUXapsSm) {
        synchronized (this.getsocial) {
            this.attribution.put(qdyncsqjkt, qWVUXapsSm);
        }
    }

    private void retention(qdyNCsqjKt qdyncsqjkt) {
        if (qdyncsqjkt == qdyNCsqjKt.SESSION) {
            Iterator<Runnable> it = this.acquisition.iterator();
            while (it.hasNext()) {
                try {
                    it.next().run();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            this.acquisition.clear();
        }
    }

    public final void getsocial(Runnable runnable) {
        this.acquisition.add(runnable);
    }

    public final qdyNCsqjKt attribution() {
        qdyNCsqjKt qdyncsqjkt;
        synchronized (this.getsocial) {
            qdyncsqjkt = qdyNCsqjKt.APP;
        }
        return qdyncsqjkt;
    }

    public final void attribution(Runnable runnable) {
        this.mobile = runnable;
    }

    public final Runnable getsocial() {
        return this.mobile;
    }

    public final void getsocial(OnUserChangedListener onUserChangedListener) {
        this.retention = onUserChangedListener;
    }

    public final OnUserChangedListener acquisition() {
        return this.retention;
    }
}
