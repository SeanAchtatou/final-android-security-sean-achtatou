package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.ArrayList;
import java.util.List;

/* compiled from: THActivityPost */
public final class ztWNWCuZiM {
    public xlPHPMtUBa acquisition;
    public QCXFOjcJkE attribution;
    public Integer cat;
    public Integer dau;
    public String getsocial;
    public List<ofLJAxfaCe> growth;
    public Integer mau;
    public Integer mobile;
    public String organic;
    public Integer retention;
    public Boolean viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof ztWNWCuZiM)) {
            return false;
        }
        ztWNWCuZiM ztwnwcuzim = (ztWNWCuZiM) obj;
        return (this.getsocial == ztwnwcuzim.getsocial || (this.getsocial != null && this.getsocial.equals(ztwnwcuzim.getsocial))) && (this.attribution == ztwnwcuzim.attribution || (this.attribution != null && this.attribution.equals(ztwnwcuzim.attribution))) && ((this.acquisition == ztwnwcuzim.acquisition || (this.acquisition != null && this.acquisition.equals(ztwnwcuzim.acquisition))) && ((this.mobile == ztwnwcuzim.mobile || (this.mobile != null && this.mobile.equals(ztwnwcuzim.mobile))) && ((this.retention == ztwnwcuzim.retention || (this.retention != null && this.retention.equals(ztwnwcuzim.retention))) && ((this.dau == ztwnwcuzim.dau || (this.dau != null && this.dau.equals(ztwnwcuzim.dau))) && ((this.mau == ztwnwcuzim.mau || (this.mau != null && this.mau.equals(ztwnwcuzim.mau))) && ((this.cat == ztwnwcuzim.cat || (this.cat != null && this.cat.equals(ztwnwcuzim.cat))) && ((this.viral == ztwnwcuzim.viral || (this.viral != null && this.viral.equals(ztwnwcuzim.viral))) && ((this.organic == ztwnwcuzim.organic || (this.organic != null && this.organic.equals(ztwnwcuzim.organic))) && (this.growth == ztwnwcuzim.growth || (this.growth != null && this.growth.equals(ztwnwcuzim.growth)))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035;
        if (this.growth != null) {
            i = this.growth.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THActivityPost{id=" + this.getsocial + ", content=" + this.attribution + ", author=" + this.acquisition + ", createdAt=" + this.mobile + ", stickyStart=" + this.retention + ", stickyEnd=" + this.dau + ", commentsCount=" + this.mau + ", likesCount=" + this.cat + ", likedByMe=" + this.viral + ", feedId=" + this.organic + ", mentions=" + this.growth + "}";
    }

    public static ztWNWCuZiM getsocial(zoToeBNOjF zotoebnojf) {
        ztWNWCuZiM ztwnwcuzim = new ztWNWCuZiM();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.attribution = QCXFOjcJkE.getsocial(zotoebnojf);
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.acquisition = xlPHPMtUBa.getsocial(zotoebnojf);
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.mobile = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.retention = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.dau = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.mau = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.cat = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.viral = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ztwnwcuzim.organic = zotoebnojf.ios();
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention2 = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention2.attribution);
                            for (int i = 0; i < retention2.attribution; i++) {
                                arrayList.add(ofLJAxfaCe.getsocial(zotoebnojf));
                            }
                            ztwnwcuzim.growth = arrayList;
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return ztwnwcuzim;
            }
        }
    }
}
