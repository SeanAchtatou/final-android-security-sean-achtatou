package im.getsocial.sdk.internal.c;

/* compiled from: DeviceNetworkInformation */
public class fOrCGNYyfk {
    private final String attribution;
    private final String getsocial;

    public fOrCGNYyfk() {
        this("NO_NETWORK", null);
    }

    public fOrCGNYyfk(String str, String str2) {
        this.getsocial = str;
        this.attribution = str2;
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final String attribution() {
        return this.attribution;
    }

    public final String acquisition() {
        if (this.attribution == null) {
            return this.getsocial;
        }
        return String.format("%s/%s", this.getsocial, this.attribution);
    }

    public final boolean mobile() {
        return "WIFI".equalsIgnoreCase(this.getsocial);
    }

    public final boolean retention() {
        return "LTE".equalsIgnoreCase(this.attribution);
    }

    public final boolean dau() {
        return "3G".equalsIgnoreCase(this.attribution);
    }
}
