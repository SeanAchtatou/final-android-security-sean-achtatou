package im.getsocial.sdk.internal.g;

import android.graphics.Bitmap;

/* compiled from: ResizeWithKeepingAspectRatioTransformation */
public class cjrhisSQCL extends upgqDBbsrL {
    public cjrhisSQCL(int i, int i2) {
        super(i, i2);
    }

    public final Bitmap getsocial(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            this.attribution = (int) (((float) this.attribution) / (((float) width) / ((float) height)));
        }
        if (height > width) {
            this.getsocial = (int) (((float) this.getsocial) / (((float) height) / ((float) width)));
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, this.getsocial, this.attribution, true);
        if (createScaledBitmap != bitmap) {
            bitmap.recycle();
        }
        return createScaledBitmap;
    }

    public final int getsocial() {
        return this.getsocial;
    }

    public final int attribution() {
        return this.attribution;
    }
}
