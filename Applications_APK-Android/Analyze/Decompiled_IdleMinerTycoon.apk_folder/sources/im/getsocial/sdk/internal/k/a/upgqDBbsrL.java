package im.getsocial.sdk.internal.k.a;

/* compiled from: MediaContent */
public final class upgqDBbsrL {
    private final byte[] attribution;
    private final cjrhisSQCL getsocial;

    private upgqDBbsrL(byte[] bArr, cjrhisSQCL cjrhissqcl) {
        this.attribution = (byte[]) bArr.clone();
        this.getsocial = cjrhissqcl;
    }

    public static upgqDBbsrL getsocial(byte[] bArr) {
        return getsocial(bArr, cjrhisSQCL.IMAGE);
    }

    public static upgqDBbsrL attribution(byte[] bArr) {
        return getsocial(bArr, cjrhisSQCL.VIDEO);
    }

    public final byte[] getsocial() {
        return this.attribution;
    }

    public final cjrhisSQCL attribution() {
        return this.getsocial;
    }

    private static upgqDBbsrL getsocial(byte[] bArr, cjrhisSQCL cjrhissqcl) {
        if (bArr != null && bArr.length > 0) {
            return new upgqDBbsrL(bArr, cjrhissqcl);
        }
        return null;
    }
}
