package im.getsocial.sdk.internal.c.l;

import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;

/* compiled from: SdkUtils */
public final class XdbacJlTDQ {
    private XdbacJlTDQ() {
    }

    public static boolean getsocial(JbBdMtJmlU jbBdMtJmlU) {
        return "UNITY".equalsIgnoreCase(jbBdMtJmlU.cat());
    }

    public static boolean getsocial(qdyNCsqjKt qdyncsqjkt) {
        boolean z = !qdyncsqjkt.getsocial("first_app_open");
        if (z) {
            qdyncsqjkt.getsocial("first_app_open", System.currentTimeMillis());
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, int):int
      im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, boolean):boolean */
    public static boolean getsocial(sqEuGXwfLT sqeugxwflt) {
        return sqeugxwflt.getsocial("im.getsocial.sdk.AutoRegisterForPush", true);
    }
}
