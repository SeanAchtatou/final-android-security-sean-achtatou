package im.getsocial.sdk.internal.b;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.GlobalErrorListener;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;

/* compiled from: SafeGlobalErrorListener */
public final class jjbQypPegg implements GlobalErrorListener {
    /* access modifiers changed from: private */
    public final GlobalErrorListener attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.d.jjbQypPegg getsocial;

    public jjbQypPegg(GlobalErrorListener globalErrorListener) {
        ztWNWCuZiM.getsocial(this);
        this.attribution = globalErrorListener;
    }

    public final void onError(final GetSocialException getSocialException) {
        this.getsocial.attribution(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.onError(getSocialException);
            }
        });
    }
}
