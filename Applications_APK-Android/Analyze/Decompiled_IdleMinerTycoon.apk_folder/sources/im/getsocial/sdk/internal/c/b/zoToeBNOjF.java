package im.getsocial.sdk.internal.c.b;

import java.lang.reflect.Field;

/* compiled from: Injector */
public class zoToeBNOjF {
    private final pdwpUtZXDT getsocial;

    public zoToeBNOjF(pdwpUtZXDT pdwputzxdt) {
        this.getsocial = pdwputzxdt;
    }

    public void getsocial(Object obj) {
        getsocial(obj, obj.getClass());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
     arg types: [java.lang.Class<?>, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object */
    private void getsocial(Object obj, Class cls) {
        Object obj2;
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                if (field.getAnnotation(XdbacJlTDQ.class) != null) {
                    boolean z = field.getAnnotation(qZypgoeblR.class) != null;
                    boolean isAccessible = field.isAccessible();
                    field.setAccessible(true);
                    try {
                        if (field.getType() == pdwpUtZXDT.class) {
                            obj2 = this.getsocial;
                        } else {
                            obj2 = this.getsocial.getsocial((Class) field.getType(), (KSZKMmRWhZ) field.getAnnotation(KSZKMmRWhZ.class));
                        }
                        if (obj2 != null) {
                            field.set(obj, obj2);
                        }
                    } catch (RuntimeException e) {
                        if (!z) {
                            throw e;
                        }
                    } catch (IllegalAccessException e2) {
                        throw new RuntimeException(e2);
                    } catch (Throwable th) {
                        field.setAccessible(isAccessible);
                        throw th;
                    }
                    field.setAccessible(isAccessible);
                }
            }
            cls = cls.getSuperclass();
        }
    }
}
