package im.getsocial.sdk.internal.d;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import im.getsocial.sdk.GetSocial;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;
import im.getsocial.sdk.pushnotifications.a.pdwpUtZXDT;

/* compiled from: SetUpFallback */
public class jjbQypPegg {
    private static final cjrhisSQCL attribution = upgqDBbsrL.getsocial(jjbQypPegg.class);
    @XdbacJlTDQ
    sqEuGXwfLT getsocial;

    public final boolean getsocial(Object obj) {
        Application application;
        attribution.mobile("GetSocial setup didn't happen! Check if you have AutoInitSdkContentProvider declared in your manifest!");
        try {
            application = (Application) Class.forName("android.app.ActivityThread").getMethod("currentApplication", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            attribution.attribution("Failed to get application using reflection.");
            application = new Application();
        }
        try {
            getsocial(application, false);
        } catch (Exception e) {
            attribution.acquisition(e);
            attribution.getsocial("Failed to make fallback setup, exception: %s", e);
        }
        if (this.getsocial == null) {
            return false;
        }
        ztWNWCuZiM.getsocial(obj);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, int):int
      im.getsocial.sdk.internal.c.sqEuGXwfLT.getsocial(java.lang.String, boolean):boolean */
    public final void getsocial(final Application application, boolean z) {
        im.getsocial.sdk.internal.c.b.jjbQypPegg.getsocial(application);
        ztWNWCuZiM.getsocial(this);
        attribution.attribution("Register activity lifecycle callbacks");
        im.getsocial.sdk.jjbQypPegg.getsocial(application);
        getsocial(application);
        if (this.getsocial.getsocial("im.getsocial.sdk.AutoInitSdk", true)) {
            if (z) {
                application.registerActivityLifecycleCallbacks(new im.getsocial.sdk.internal.m.jjbQypPegg() {
                    public void onActivityCreated(Activity activity, Bundle bundle) {
                        GetSocial.init();
                        application.unregisterActivityLifecycleCallbacks(this);
                    }
                });
            } else {
                GetSocial.init();
            }
        }
        pdwpUtZXDT.getsocial();
    }

    private static void getsocial(Application application) {
        try {
            im.getsocial.sdk.internal.l.jjbQypPegg.getsocial().getsocial(application);
        } catch (Exception e) {
            cjrhisSQCL cjrhissqcl = attribution;
            cjrhissqcl.attribution("UI framework can not be found: " + e.getMessage());
        }
    }
}
