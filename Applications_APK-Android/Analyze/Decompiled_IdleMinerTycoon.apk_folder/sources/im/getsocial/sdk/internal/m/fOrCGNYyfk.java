package im.getsocial.sdk.internal.m;

import android.content.Context;
import android.content.pm.ProviderInfo;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.invites.ImageContentProvider;

/* compiled from: ImageContract */
public final class fOrCGNYyfk {
    private static cjrhisSQCL getsocial = upgqDBbsrL.getsocial(fOrCGNYyfk.class);

    private fOrCGNYyfk() {
    }

    public static String getsocial(Context context) {
        ProviderInfo providerInfo = im.getsocial.sdk.invites.a.k.upgqDBbsrL.getsocial(context);
        if (providerInfo != null) {
            return providerInfo.authority;
        }
        getsocial.attribution("Can not create media content URI, %s is not found in the AndroidManifest.xml", ImageContentProvider.class.getSimpleName());
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0045 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0047 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getsocial(java.lang.String r2) {
        /*
            int r0 = r2.hashCode()
            r1 = -2075480191(0xffffffff844aaf81, float:-2.3825573E-36)
            if (r0 == r1) goto L_0x0037
            r1 = -1124559883(0xffffffffbcf893f5, float:-0.030343989)
            if (r0 == r1) goto L_0x002d
            r1 = 1086552613(0x40c37a25, float:6.10866)
            if (r0 == r1) goto L_0x0023
            r1 = 2118523743(0x7e461b5f, float:6.5832316E37)
            if (r0 == r1) goto L_0x0019
            goto L_0x0041
        L_0x0019:
            java.lang.String r0 = "getsocial-smartinvite-tempsticker.png"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0041
            r2 = 2
            goto L_0x0042
        L_0x0023:
            java.lang.String r0 = "getsocial-smartinvite-tempvideo.mp4"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0041
            r2 = 3
            goto L_0x0042
        L_0x002d:
            java.lang.String r0 = "getsocial-smartinvite-tempimage.jpg"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0041
            r2 = 0
            goto L_0x0042
        L_0x0037:
            java.lang.String r0 = "getsocial-smartinvite-tempgif.gif"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0041
            r2 = 1
            goto L_0x0042
        L_0x0041:
            r2 = -1
        L_0x0042:
            switch(r2) {
                case 0: goto L_0x0050;
                case 1: goto L_0x004d;
                case 2: goto L_0x004a;
                case 3: goto L_0x0047;
                default: goto L_0x0045;
            }
        L_0x0045:
            r2 = 0
            return r2
        L_0x0047:
            java.lang.String r2 = "smart-invite-video.mp4"
            return r2
        L_0x004a:
            java.lang.String r2 = "smart-invite-sticker.png"
            return r2
        L_0x004d:
            java.lang.String r2 = "smart-invite-gif.gif"
            return r2
        L_0x0050:
            java.lang.String r2 = "smart-invite.jpg"
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: im.getsocial.sdk.internal.m.fOrCGNYyfk.getsocial(java.lang.String):java.lang.String");
    }
}
