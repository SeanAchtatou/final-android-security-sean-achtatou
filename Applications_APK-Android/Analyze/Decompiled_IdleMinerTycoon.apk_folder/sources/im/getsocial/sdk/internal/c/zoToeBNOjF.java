package im.getsocial.sdk.internal.c;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;

/* compiled from: AppId */
public class zoToeBNOjF {
    private final String getsocial;

    public zoToeBNOjF(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial((Object) str), "GetSocial appId may not be null.");
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial(str), "GetSocial appId may not be empty.");
        boolean z = false;
        if (str.length() == 40) {
            this.getsocial = getsocial(str);
            return;
        }
        if (str.length() <= 20 ? true : z) {
            this.getsocial = str;
            return;
        }
        throw new IllegalArgumentException("GetSocial appId must be less than or equal to 20 symbols or exactly 40 symbols.");
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public int hashCode() {
        return this.getsocial.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.getsocial.equals(((zoToeBNOjF) obj).getsocial);
    }

    private static String getsocial(String str) {
        String substring = str.substring(20);
        while (substring.charAt(0) == '0') {
            substring = substring.substring(1);
        }
        return substring;
    }
}
