package im.getsocial.sdk.internal.c.b;

import com.ironsource.sdk.constants.Constants;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: RepositoryPool */
public class jMsobIMeui implements ruWsnwUPKh {
    private final Object acquisition = new Object();
    protected EnumSet<qdyNCsqjKt> attribution;
    protected final Map<Class<? extends KluUZYuxme>, KluUZYuxme> getsocial = new HashMap();

    public jMsobIMeui(EnumSet<qdyNCsqjKt> enumSet) {
        this.attribution = enumSet;
    }

    public final <T extends KluUZYuxme> T getsocial(Class cls) {
        T t;
        synchronized (this.acquisition) {
            KluUZYuxme kluUZYuxme = this.getsocial.get(cls);
            if (kluUZYuxme == null) {
                try {
                    if (cls.getConstructors().length == 0) {
                        throw new RuntimeException("Can't instantiate [" + cls + "]. It must not have a private constructor.");
                    } else if (cls.getConstructors().length > 1) {
                        throw new RuntimeException("Can't instantiate [" + cls + "]. It must not have multiple constructors");
                    } else if (cls.getConstructors()[0].getParameterTypes().length <= 0) {
                        kluUZYuxme = (KluUZYuxme) cls.newInstance();
                        if (!(true ^ this.attribution.contains(qdyNCsqjKt.APP))) {
                            if (kluUZYuxme.attribution() != qdyNCsqjKt.APP) {
                                throw new RuntimeException("Can not get '" + cls.getName() + "' from this RepositoryPool. Scope '" + kluUZYuxme.attribution() + "' not initialised.");
                            }
                        }
                        if (this.attribution.contains(kluUZYuxme.attribution())) {
                            this.getsocial.put(cls, kluUZYuxme);
                        } else {
                            throw new RuntimeException("Can not get '" + cls.getName() + "' from this RepositoryPool. Valid scopes are " + this.attribution.toString());
                        }
                    } else {
                        throw new RuntimeException("Can't instantiate [" + cls + "]. Constructor must have no arguments.");
                    }
                } catch (InstantiationException e) {
                    throw new RuntimeException("Can't instantiate [" + cls + "]. Repository can have only one constructor without parameters.", e);
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException("IllegalAccessException on [" + cls + Constants.RequestParameters.RIGHT_BRACKETS, e2);
                }
            }
            t = (KluUZYuxme) cls.cast(kluUZYuxme);
        }
        return t;
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(jMsobIMeui jmsobimeui) {
        synchronized (this.acquisition) {
            EnumSet<qdyNCsqjKt> enumSet = jmsobimeui.attribution;
            if (!enumSet.contains(qdyNCsqjKt.APP)) {
                Iterator<Class<? extends KluUZYuxme>> it = this.getsocial.keySet().iterator();
                while (it.hasNext()) {
                    if (enumSet.contains(this.getsocial.get(it.next()).attribution())) {
                        it.remove();
                    }
                }
                for (Map.Entry next : jmsobimeui.getsocial.entrySet()) {
                    this.getsocial.put(next.getKey(), next.getValue());
                }
            } else {
                throw new RuntimeException("Can not commitRepositoryTransaction with Application scope.");
            }
        }
    }
}
