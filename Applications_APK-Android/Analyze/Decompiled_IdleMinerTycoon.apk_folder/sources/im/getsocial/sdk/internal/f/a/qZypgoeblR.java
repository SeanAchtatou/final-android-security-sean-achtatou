package im.getsocial.sdk.internal.f.a;

/* compiled from: THAnalyticsEvent */
public final class qZypgoeblR {
    public final int hashCode() {
        return 2071361763;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && (obj instanceof qZypgoeblR);
    }

    public final String toString() {
        return "THAnalyticsEvent{superProperties=" + ((Object) null) + ", customProperties=" + ((Object) null) + ", deviceTime=" + ((Object) null) + ", name=" + ((String) null) + "}";
    }
}
