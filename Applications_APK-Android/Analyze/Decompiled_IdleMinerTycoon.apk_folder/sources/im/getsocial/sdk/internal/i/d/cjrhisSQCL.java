package im.getsocial.sdk.internal.i.d;

import im.getsocial.sdk.internal.c.HptYHntaqF;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;
import im.getsocial.sdk.internal.c.ztWNWCuZiM;

/* compiled from: InitSdkUseCase */
public final class cjrhisSQCL extends jjbQypPegg {
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL engage = upgqDBbsrL.getsocial(cjrhisSQCL.class);
    @XdbacJlTDQ

    /* renamed from: android  reason: collision with root package name */
    qdyNCsqjKt f489android;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.j.c.jjbQypPegg cat;
    @XdbacJlTDQ
    pdwpUtZXDT connect;
    @XdbacJlTDQ
    JbBdMtJmlU dau;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.b.pdwpUtZXDT getsocial;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.a.jjbQypPegg growth;
    @XdbacJlTDQ
    ztWNWCuZiM ios;
    @XdbacJlTDQ
    sqEuGXwfLT mau;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.j.b.jjbQypPegg organic;
    @XdbacJlTDQ
    HptYHntaqF unity;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.i.jjbQypPegg viral;

    public cjrhisSQCL() {
        im.getsocial.sdk.internal.c.b.ztWNWCuZiM.getsocial(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        r6 = new im.getsocial.sdk.internal.c.b.jMsobIMeui(java.util.EnumSet.of(im.getsocial.sdk.internal.c.b.qdyNCsqjKt.SESSION, im.getsocial.sdk.internal.c.b.qdyNCsqjKt.USER));
        r10.cat.getsocial(new im.getsocial.sdk.internal.i.d.cjrhisSQCL.AnonymousClass1(r10));
        r9 = im.getsocial.sdk.internal.c.l.XdbacJlTDQ.getsocial(r10.f489android);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006a, code lost:
        if (r10.dau.sharing() == false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006c, code lost:
        if (r9 == false) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006e, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0071, code lost:
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0073, code lost:
        r5 = r11;
        r8 = r12;
        getsocial(new im.getsocial.sdk.internal.i.d.cjrhisSQCL.AnonymousClass2(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void getsocial(java.lang.String r11, im.getsocial.sdk.CompletionCallback r12) {
        /*
            r10 = this;
            im.getsocial.sdk.internal.c.f.cjrhisSQCL r0 = im.getsocial.sdk.internal.i.d.cjrhisSQCL.engage
            java.lang.String r1 = "InitSdkUseCase.execute called"
            r0.attribution(r1)
            boolean r0 = im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(r12)
            java.lang.String r1 = "Can not execute InitSdkUseCase with null callback"
            im.getsocial.sdk.internal.c.l.jjbQypPegg.C0021jjbQypPegg.getsocial(r0, r1)
            im.getsocial.sdk.internal.c.i.jjbQypPegg r0 = r10.viral
            monitor-enter(r0)
            int[] r1 = im.getsocial.sdk.internal.i.d.cjrhisSQCL.AnonymousClass4.getsocial     // Catch:{ all -> 0x0080 }
            im.getsocial.sdk.internal.c.i.jjbQypPegg r2 = r10.viral     // Catch:{ all -> 0x0080 }
            im.getsocial.sdk.internal.c.b.qdyNCsqjKt r3 = im.getsocial.sdk.internal.c.b.qdyNCsqjKt.SESSION     // Catch:{ all -> 0x0080 }
            im.getsocial.sdk.internal.c.QWVUXapsSm r2 = r2.getsocial(r3)     // Catch:{ all -> 0x0080 }
            int r2 = r2.ordinal()     // Catch:{ all -> 0x0080 }
            r1 = r1[r2]     // Catch:{ all -> 0x0080 }
            switch(r1) {
                case 1: goto L_0x0035;
                case 2: goto L_0x0029;
                default: goto L_0x0026;
            }     // Catch:{ all -> 0x0080 }
        L_0x0026:
            im.getsocial.sdk.internal.c.i.jjbQypPegg r1 = r10.viral     // Catch:{ all -> 0x0080 }
            goto L_0x003a
        L_0x0029:
            im.getsocial.sdk.internal.c.c.upgqDBbsrL r11 = new im.getsocial.sdk.internal.c.c.upgqDBbsrL     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = "GetSocial initialization has already started."
            r11.<init>(r1)     // Catch:{ all -> 0x0080 }
            r12.onFailure(r11)     // Catch:{ all -> 0x0080 }
            monitor-exit(r0)     // Catch:{ all -> 0x0080 }
            return
        L_0x0035:
            r12.onSuccess()     // Catch:{ all -> 0x0080 }
            monitor-exit(r0)     // Catch:{ all -> 0x0080 }
            return
        L_0x003a:
            im.getsocial.sdk.internal.c.b.qdyNCsqjKt r2 = im.getsocial.sdk.internal.c.b.qdyNCsqjKt.SESSION     // Catch:{ all -> 0x0080 }
            r1.attribution(r2)     // Catch:{ all -> 0x0080 }
            im.getsocial.sdk.internal.c.i.jjbQypPegg r1 = r10.viral     // Catch:{ all -> 0x0080 }
            im.getsocial.sdk.internal.c.b.qdyNCsqjKt r2 = im.getsocial.sdk.internal.c.b.qdyNCsqjKt.USER     // Catch:{ all -> 0x0080 }
            r1.attribution(r2)     // Catch:{ all -> 0x0080 }
            monitor-exit(r0)     // Catch:{ all -> 0x0080 }
            im.getsocial.sdk.internal.c.b.jMsobIMeui r6 = new im.getsocial.sdk.internal.c.b.jMsobIMeui
            im.getsocial.sdk.internal.c.b.qdyNCsqjKt r0 = im.getsocial.sdk.internal.c.b.qdyNCsqjKt.SESSION
            im.getsocial.sdk.internal.c.b.qdyNCsqjKt r1 = im.getsocial.sdk.internal.c.b.qdyNCsqjKt.USER
            java.util.EnumSet r0 = java.util.EnumSet.of(r0, r1)
            r6.<init>(r0)
            im.getsocial.sdk.internal.c.j.c.jjbQypPegg r0 = r10.cat
            im.getsocial.sdk.internal.i.d.cjrhisSQCL$1 r1 = new im.getsocial.sdk.internal.i.d.cjrhisSQCL$1
            r1.<init>(r6)
            r0.getsocial(r1)
            im.getsocial.sdk.internal.c.qdyNCsqjKt r0 = r10.f489android
            boolean r9 = im.getsocial.sdk.internal.c.l.XdbacJlTDQ.getsocial(r0)
            im.getsocial.sdk.internal.c.JbBdMtJmlU r0 = r10.dau
            boolean r0 = r0.sharing()
            if (r0 == 0) goto L_0x0071
            if (r9 == 0) goto L_0x0071
            r0 = 1
            r7 = 1
            goto L_0x0073
        L_0x0071:
            r0 = 0
            r7 = 0
        L_0x0073:
            im.getsocial.sdk.internal.i.d.cjrhisSQCL$2 r0 = new im.getsocial.sdk.internal.i.d.cjrhisSQCL$2
            r3 = r0
            r4 = r10
            r5 = r11
            r8 = r12
            r3.<init>(r5, r6, r7, r8, r9)
            r10.getsocial(r0)
            return
        L_0x0080:
            r11 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0080 }
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: im.getsocial.sdk.internal.i.d.cjrhisSQCL.getsocial(java.lang.String, im.getsocial.sdk.CompletionCallback):void");
    }

    static /* synthetic */ boolean getsocial(cjrhisSQCL cjrhissqcl) {
        return !cjrhissqcl.mau.getsocial("im.getsocial.sdk.DisableFacebookReferralCheck", false);
    }
}
