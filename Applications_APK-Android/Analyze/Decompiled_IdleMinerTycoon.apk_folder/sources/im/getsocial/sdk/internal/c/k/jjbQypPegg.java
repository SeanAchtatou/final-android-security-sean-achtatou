package im.getsocial.sdk.internal.c.k;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.e.a.pdwpUtZXDT;

/* compiled from: AsyncUseCase */
public abstract class jjbQypPegg implements upgqDBbsrL {
    protected static final cjrhisSQCL attribution = upgqDBbsrL.getsocial(upgqDBbsrL.class);
    @XdbacJlTDQ
    pdwpUtZXDT acquisition;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.e.a.upgqDBbsrL mobile;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.a.jjbQypPegg retention;

    /* renamed from: im.getsocial.sdk.internal.c.k.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: AsyncUseCase */
    protected interface C0020jjbQypPegg<T> {
        T getsocial();
    }

    public jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    /* access modifiers changed from: protected */
    public final im.getsocial.sdk.internal.c.a.jjbQypPegg getsocial() {
        return this.retention;
    }

    /* access modifiers changed from: protected */
    public final void getsocial(final Runnable runnable, final CompletionCallback completionCallback) {
        acquisition(new Runnable() {
            public void run() {
                try {
                    runnable.run();
                    jjbQypPegg.this.mobile.getsocial(new Runnable() {
                        public void run() {
                            completionCallback.onSuccess();
                        }
                    });
                } catch (Exception e) {
                    cjrhisSQCL cjrhissqcl = jjbQypPegg.attribution;
                    cjrhissqcl.attribution(jjbQypPegg.this.getClass().getSimpleName() + " execution failed");
                    jjbQypPegg.attribution.getsocial(e);
                    final GetSocialException getSocialException = im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(e);
                    jjbQypPegg.this.mobile.getsocial(new Runnable() {
                        public void run() {
                            completionCallback.onFailure(getSocialException);
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void getsocial(final Runnable runnable) {
        acquisition(new Runnable() {
            public void run() {
                try {
                    runnable.run();
                } catch (Exception e) {
                    cjrhisSQCL cjrhissqcl = jjbQypPegg.attribution;
                    cjrhissqcl.attribution(jjbQypPegg.this.getClass().getSimpleName() + " execution failed");
                    jjbQypPegg.attribution.getsocial(e);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final <T> void getsocial(final C0020jjbQypPegg jjbqyppegg, final Callback callback) {
        acquisition(new Runnable() {
            public void run() {
                try {
                    final Object obj = jjbqyppegg.getsocial();
                    jjbQypPegg.this.mobile.getsocial(new Runnable() {
                        public void run() {
                            callback.onSuccess(obj);
                        }
                    });
                } catch (Exception e) {
                    cjrhisSQCL cjrhissqcl = jjbQypPegg.attribution;
                    cjrhissqcl.attribution(jjbQypPegg.this.getClass().getSimpleName() + " execution failed");
                    jjbQypPegg.attribution.getsocial(e);
                    final GetSocialException getSocialException = im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(e);
                    jjbQypPegg.this.mobile.getsocial(new Runnable() {
                        public void run() {
                            callback.onFailure(getSocialException);
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void attribution(Runnable runnable) {
        this.mobile.getsocial(runnable);
    }

    private void acquisition(Runnable runnable) {
        this.acquisition.getsocial().getsocial(runnable);
    }
}
