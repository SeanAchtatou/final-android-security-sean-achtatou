package im.getsocial.sdk.internal.c;

import android.content.Context;
import android.content.SharedPreferences;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;

/* compiled from: SharedPreferencesLocalStorage */
public class wWemqSpYTx implements qdyNCsqjKt {
    private final SharedPreferences getsocial;

    @XdbacJlTDQ
    public wWemqSpYTx(Context context) {
        this.getsocial = context.getSharedPreferences("getsocial", 0);
    }

    public final boolean getsocial(String str) {
        return this.getsocial.contains(str);
    }

    public final String attribution(String str) {
        return this.getsocial.getString(str, null);
    }

    public final void getsocial(String str, String str2) {
        this.getsocial.edit().putString(str, str2).apply();
    }

    public final long acquisition(String str) {
        return this.getsocial.getLong(str, 0);
    }

    public final void getsocial(String str, long j) {
        this.getsocial.edit().putLong(str, j).apply();
    }

    public final int mobile(String str) {
        return this.getsocial.getInt(str, 0);
    }

    public final void getsocial(String str, int i) {
        this.getsocial.edit().putInt(str, i).apply();
    }

    public final void retention(String str) {
        this.getsocial.edit().remove(str).apply();
    }
}
