package im.getsocial.sdk.internal.e.a;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: BackgroundScheduler */
class jjbQypPegg extends upgqDBbsrL {
    private final ExecutorService getsocial = Executors.newSingleThreadExecutor();

    jjbQypPegg() {
    }

    public final void getsocial(Runnable runnable) {
        if (this.getsocial.isShutdown()) {
            new Thread(runnable).start();
        } else {
            this.getsocial.execute(runnable);
        }
    }
}
