package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import im.getsocial.b.a.ztWNWCuZiM;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: THPublicUser */
public final class FvojpKUsoc {
    public String acquisition;

    /* renamed from: android  reason: collision with root package name */
    public String f482android;
    public String attribution;
    public String cat;
    public Map<String, String> dau;
    public String getsocial;
    public Boolean growth;
    public String mau;
    public List<JbBdMtJmlU> mobile;
    public Boolean organic;
    public Map<String, String> retention;
    public KkSvQPDhNi viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof FvojpKUsoc)) {
            return false;
        }
        FvojpKUsoc fvojpKUsoc = (FvojpKUsoc) obj;
        return (this.getsocial == fvojpKUsoc.getsocial || (this.getsocial != null && this.getsocial.equals(fvojpKUsoc.getsocial))) && (this.attribution == fvojpKUsoc.attribution || (this.attribution != null && this.attribution.equals(fvojpKUsoc.attribution))) && ((this.acquisition == fvojpKUsoc.acquisition || (this.acquisition != null && this.acquisition.equals(fvojpKUsoc.acquisition))) && ((this.mobile == fvojpKUsoc.mobile || (this.mobile != null && this.mobile.equals(fvojpKUsoc.mobile))) && ((this.retention == fvojpKUsoc.retention || (this.retention != null && this.retention.equals(fvojpKUsoc.retention))) && ((this.dau == fvojpKUsoc.dau || (this.dau != null && this.dau.equals(fvojpKUsoc.dau))) && ((this.mau == fvojpKUsoc.mau || (this.mau != null && this.mau.equals(fvojpKUsoc.mau))) && ((this.cat == fvojpKUsoc.cat || (this.cat != null && this.cat.equals(fvojpKUsoc.cat))) && ((this.viral == fvojpKUsoc.viral || (this.viral != null && this.viral.equals(fvojpKUsoc.viral))) && ((this.organic == fvojpKUsoc.organic || (this.organic != null && this.organic.equals(fvojpKUsoc.organic))) && ((this.growth == fvojpKUsoc.growth || (this.growth != null && this.growth.equals(fvojpKUsoc.growth))) && (this.f482android == fvojpKUsoc.f482android || (this.f482android != null && this.f482android.equals(fvojpKUsoc.f482android))))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035) ^ (this.growth == null ? 0 : this.growth.hashCode())) * -2128831035;
        if (this.f482android != null) {
            i = this.f482android.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THPublicUser{id=" + this.getsocial + ", displayName=" + this.attribution + ", avatarUrl=" + this.acquisition + ", identities=" + this.mobile + ", publicProperties=" + this.retention + ", internalPublicProperties=" + this.dau + ", installDate=" + this.mau + ", installProvider=" + this.cat + ", installPlatform=" + this.viral + ", reinstall=" + this.organic + ", installSuspicious=" + this.growth + ", installPlatformStr=" + this.f482android + "}";
    }

    public static FvojpKUsoc getsocial(zoToeBNOjF zotoebnojf) {
        FvojpKUsoc fvojpKUsoc = new FvojpKUsoc();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention2 = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention2.attribution);
                            while (i < retention2.attribution) {
                                arrayList.add(JbBdMtJmlU.getsocial(zotoebnojf));
                                i++;
                            }
                            fvojpKUsoc.mobile = arrayList;
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            fvojpKUsoc.retention = hashMap;
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile3 = zotoebnojf.mobile();
                            HashMap hashMap2 = new HashMap(mobile3.acquisition);
                            while (i < mobile3.acquisition) {
                                hashMap2.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            fvojpKUsoc.dau = hashMap2;
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.mau = zotoebnojf.ios();
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.cat = zotoebnojf.ios();
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            int organic2 = zotoebnojf.organic();
                            KkSvQPDhNi findByValue = KkSvQPDhNi.findByValue(organic2);
                            if (findByValue != null) {
                                fvojpKUsoc.viral = findByValue;
                                break;
                            } else {
                                ztWNWCuZiM.jjbQypPegg jjbqyppegg = ztWNWCuZiM.jjbQypPegg.PROTOCOL_ERROR;
                                throw new ztWNWCuZiM(jjbqyppegg, "Unexpected value for enum-type THDeviceOs: " + organic2);
                            }
                        }
                    case 10:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.organic = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.growth = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 12:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            fvojpKUsoc.f482android = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return fvojpKUsoc;
            }
        }
    }
}
