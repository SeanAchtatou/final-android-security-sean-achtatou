package im.getsocial.sdk.internal.c;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;

/* compiled from: MetaDataReaderImpl */
public class rFvvVpjzZH implements sqEuGXwfLT {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(rFvvVpjzZH.class);
    private final Context attribution;

    @XdbacJlTDQ
    rFvvVpjzZH(Context context) {
        this.attribution = context;
    }

    public final String getsocial(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial((Object) str), "Key could not be null");
        return getsocial().getString(str);
    }

    public final boolean getsocial(String str, boolean z) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial((Object) str), "Key could not be null");
        return getsocial().getBoolean(str, z);
    }

    public final int getsocial(String str, int i) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial((Object) str), "Key could not be null");
        return getsocial().getInt(str, 0);
    }

    private Bundle getsocial() {
        try {
            ApplicationInfo applicationInfo = this.attribution.getPackageManager().getApplicationInfo(this.attribution.getPackageName(), 128);
            return applicationInfo.metaData == null ? Bundle.EMPTY : applicationInfo.metaData;
        } catch (PackageManager.NameNotFoundException e) {
            cjrhisSQCL cjrhissqcl = getsocial;
            cjrhissqcl.retention("Failed to load meta-data, NameNotFound: " + e.getMessage());
            return Bundle.EMPTY;
        }
    }
}
