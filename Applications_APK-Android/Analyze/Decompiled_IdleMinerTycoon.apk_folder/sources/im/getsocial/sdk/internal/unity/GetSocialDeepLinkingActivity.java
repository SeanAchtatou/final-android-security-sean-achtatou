package im.getsocial.sdk.internal.unity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;

public class GetSocialDeepLinkingActivity extends Activity {
    private static cjrhisSQCL getsocial = upgqDBbsrL.getsocial(GetSocialDeepLinkingActivity.class);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        onResume();
        startActivity(new Intent(this, getsocial()));
        onPause();
        finish();
    }

    private Class<?> getsocial() {
        try {
            return Class.forName(getPackageManager().getLaunchIntentForPackage(getPackageName()).getComponent().getClassName());
        } catch (Exception e) {
            cjrhisSQCL cjrhissqcl = getsocial;
            cjrhissqcl.retention("Unable to find Main Activity Class, error: " + e.getMessage());
            return null;
        }
    }
}
