package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THIdentity */
public final class JbBdMtJmlU {
    public String acquisition;
    public String attribution;
    public String getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof JbBdMtJmlU)) {
            return false;
        }
        JbBdMtJmlU jbBdMtJmlU = (JbBdMtJmlU) obj;
        return (this.getsocial == jbBdMtJmlU.getsocial || (this.getsocial != null && this.getsocial.equals(jbBdMtJmlU.getsocial))) && (this.attribution == jbBdMtJmlU.attribution || (this.attribution != null && this.attribution.equals(jbBdMtJmlU.attribution))) && (this.acquisition == jbBdMtJmlU.acquisition || (this.acquisition != null && this.acquisition.equals(jbBdMtJmlU.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THIdentity{provider=" + this.getsocial + ", providerId=" + this.attribution + ", accessToken=" + this.acquisition + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, JbBdMtJmlU jbBdMtJmlU) {
        if (jbBdMtJmlU.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(jbBdMtJmlU.getsocial);
        }
        if (jbBdMtJmlU.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(jbBdMtJmlU.attribution);
        }
        if (jbBdMtJmlU.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(jbBdMtJmlU.acquisition);
        }
        zotoebnojf.getsocial();
    }

    public static JbBdMtJmlU getsocial(zoToeBNOjF zotoebnojf) {
        JbBdMtJmlU jbBdMtJmlU = new JbBdMtJmlU();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            jbBdMtJmlU.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            jbBdMtJmlU.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            jbBdMtJmlU.acquisition = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return jbBdMtJmlU;
            }
        }
    }
}
