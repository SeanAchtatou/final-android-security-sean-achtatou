package im.getsocial.sdk.internal.c.h;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: Reachability */
public class cjrhisSQCL {
    private static final long getsocial = TimeUnit.SECONDS.toMillis(1);
    private final ArrayList<jjbQypPegg> acquisition = new ArrayList<>();
    private final Object attribution = new Object();
    private volatile boolean dau;
    private final jjbQypPegg mobile;
    private ScheduledExecutorService retention = null;

    /* compiled from: Reachability */
    public interface jjbQypPegg {
        void getsocial(boolean z);
    }

    @XdbacJlTDQ
    protected cjrhisSQCL(jjbQypPegg jjbqyppegg) {
        this.mobile = jjbqyppegg;
    }

    public final void getsocial() {
        synchronized (this.attribution) {
            attribution();
            this.retention = Executors.newScheduledThreadPool(1);
            this.retention.scheduleAtFixedRate(new upgqDBbsrL(this, (byte) 0), 0, getsocial, TimeUnit.MILLISECONDS);
        }
    }

    public final void attribution() {
        synchronized (this.attribution) {
            if (this.retention != null) {
                this.retention.shutdown();
                this.retention = null;
            }
        }
    }

    public final void getsocial(jjbQypPegg jjbqyppegg) {
        this.acquisition.add(jjbqyppegg);
    }

    public final void attribution(jjbQypPegg jjbqyppegg) {
        this.acquisition.remove(jjbqyppegg);
    }

    /* compiled from: Reachability */
    private final class upgqDBbsrL implements Runnable {
        private upgqDBbsrL() {
        }

        /* synthetic */ upgqDBbsrL(cjrhisSQCL cjrhissqcl, byte b) {
            this();
        }

        public final void run() {
            try {
                cjrhisSQCL.getsocial(cjrhisSQCL.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static /* synthetic */ void getsocial(cjrhisSQCL cjrhissqcl) {
        try {
            boolean z = cjrhissqcl.mobile.getsocial();
            if (cjrhissqcl.dau != z) {
                cjrhissqcl.dau = z;
                Iterator<jjbQypPegg> it = cjrhissqcl.acquisition.iterator();
                while (it.hasNext()) {
                    it.next().getsocial(cjrhissqcl.dau);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
