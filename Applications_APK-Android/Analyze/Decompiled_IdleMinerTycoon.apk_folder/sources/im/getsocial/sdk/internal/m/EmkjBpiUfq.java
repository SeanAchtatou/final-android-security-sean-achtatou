package im.getsocial.sdk.internal.m;

import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/* compiled from: SystemProperty */
public final class EmkjBpiUfq {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(EmkjBpiUfq.class);

    private EmkjBpiUfq() {
    }

    public static String getsocial(String str) {
        BufferedReader bufferedReader;
        InputStreamReader inputStreamReader;
        BufferedReader bufferedReader2;
        InputStreamReader inputStreamReader2;
        InputStreamReader inputStreamReader3 = null;
        try {
            inputStreamReader = new InputStreamReader(Runtime.getRuntime().exec(new String[]{"getprop", str}).getInputStream(), Charset.defaultCharset());
            try {
                bufferedReader = new BufferedReader(inputStreamReader);
                try {
                    String readLine = bufferedReader.readLine();
                    getsocial(inputStreamReader);
                    getsocial(bufferedReader);
                    return readLine;
                } catch (IOException unused) {
                    getsocial(inputStreamReader);
                    getsocial(bufferedReader);
                    return null;
                } catch (Throwable th) {
                    inputStreamReader2 = inputStreamReader;
                    bufferedReader2 = bufferedReader;
                    th = th;
                    inputStreamReader3 = inputStreamReader2;
                    getsocial(inputStreamReader3);
                    getsocial(bufferedReader2);
                    throw th;
                }
            } catch (IOException unused2) {
                bufferedReader = null;
                getsocial(inputStreamReader);
                getsocial(bufferedReader);
                return null;
            } catch (Throwable th2) {
                th = th2;
                inputStreamReader2 = inputStreamReader;
                bufferedReader2 = null;
                inputStreamReader3 = inputStreamReader2;
                getsocial(inputStreamReader3);
                getsocial(bufferedReader2);
                throw th;
            }
        } catch (IOException unused3) {
            bufferedReader = null;
            inputStreamReader = null;
            getsocial(inputStreamReader);
            getsocial(bufferedReader);
            return null;
        } catch (Throwable th3) {
            th = th3;
            bufferedReader2 = null;
            getsocial(inputStreamReader3);
            getsocial(bufferedReader2);
            throw th;
        }
    }

    private static void getsocial(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("Failed to close the stream: " + e.getMessage());
            }
        }
    }
}
