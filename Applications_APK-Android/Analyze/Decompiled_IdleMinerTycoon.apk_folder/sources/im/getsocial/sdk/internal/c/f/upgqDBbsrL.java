package im.getsocial.sdk.internal.c.f;

import im.getsocial.sdk.internal.a.g.jjbQypPegg;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.qZypgoeblR;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.util.Arrays;

/* compiled from: GsLog */
public final class upgqDBbsrL implements cjrhisSQCL {
    private final boolean acquisition;
    private final String attribution;
    @XdbacJlTDQ
    @qZypgoeblR
    jjbQypPegg getsocial;

    private upgqDBbsrL(String str) {
        this(str, true);
    }

    private upgqDBbsrL(String str, boolean z) {
        this.attribution = "GetSocial_" + str;
        this.acquisition = z;
    }

    public static cjrhisSQCL getsocial(Class cls) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(cls), "Can not create log with null or empty tag");
        return new upgqDBbsrL(cls.getSimpleName(), !Arrays.asList(im.getsocial.sdk.internal.a.c.upgqDBbsrL.class, im.getsocial.sdk.internal.a.h.jjbQypPegg.class, im.getsocial.sdk.internal.a.h.upgqDBbsrL.class).contains(cls));
    }

    public static cjrhisSQCL getsocial(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(str) && im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(str), "Can not create log with null or empty tag");
        return new upgqDBbsrL(str);
    }

    public final void attribution(String str) {
        getsocial(cjrhisSQCL.jjbQypPegg.DEBUG, str);
    }

    public final void getsocial(Throwable th) {
        getsocial(cjrhisSQCL.jjbQypPegg.DEBUG, th);
    }

    public final void getsocial(String str, Object... objArr) {
        getsocial(cjrhisSQCL.jjbQypPegg.DEBUG, str, objArr);
    }

    public final void acquisition(String str) {
        getsocial(cjrhisSQCL.jjbQypPegg.INFO, str);
    }

    public final void mobile(String str) {
        getsocial(cjrhisSQCL.jjbQypPegg.WARN, str);
    }

    public final void attribution(String str, Object... objArr) {
        getsocial(cjrhisSQCL.jjbQypPegg.WARN, str, objArr);
    }

    public final void attribution(Throwable th) {
        getsocial(cjrhisSQCL.jjbQypPegg.WARN, th);
    }

    public final void retention(String str) {
        getsocial(cjrhisSQCL.jjbQypPegg.ERROR, str);
    }

    public final void acquisition(String str, Object... objArr) {
        getsocial(cjrhisSQCL.jjbQypPegg.ERROR, str, objArr);
    }

    public final void acquisition(Throwable th) {
        getsocial(cjrhisSQCL.jjbQypPegg.ERROR, th);
    }

    private void getsocial(cjrhisSQCL.jjbQypPegg jjbqyppegg, String str) {
        if (str == null) {
            str = "null";
        }
        if (getsocial(jjbqyppegg)) {
            try {
                pdwpUtZXDT.attribution().getsocial(jjbqyppegg, this.attribution, str, new Object[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dau(str);
    }

    private void getsocial(cjrhisSQCL.jjbQypPegg jjbqyppegg, String str, Object... objArr) {
        if (str == null) {
            str = "null";
        }
        if (getsocial(jjbqyppegg)) {
            try {
                pdwpUtZXDT.attribution().getsocial(jjbqyppegg, this.attribution, str, objArr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dau(String.format(str, objArr));
    }

    private void getsocial(cjrhisSQCL.jjbQypPegg jjbqyppegg, Throwable th) {
        String str;
        if (th == null) {
            str = null;
        } else {
            str = th.getMessage() + 10 + pdwpUtZXDT.getsocial(th);
        }
        getsocial(jjbqyppegg, str);
    }

    private static boolean getsocial(cjrhisSQCL.jjbQypPegg jjbqyppegg) {
        return jjbqyppegg.value() <= pdwpUtZXDT.getsocial().value();
    }

    private void dau(String str) {
        if (this.acquisition) {
            if (this.getsocial == null) {
                ztWNWCuZiM.getsocial(this);
            }
            if (this.getsocial != null) {
                this.getsocial.getsocial(str);
            }
        }
    }
}
