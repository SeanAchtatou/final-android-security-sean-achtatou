package im.getsocial.sdk.internal.c.h;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import im.getsocial.sdk.internal.m.XdbacJlTDQ;

/* compiled from: InternetConnectivityImpl */
public class upgqDBbsrL implements jjbQypPegg {
    private final XdbacJlTDQ getsocial;

    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    upgqDBbsrL(XdbacJlTDQ xdbacJlTDQ) {
        this.getsocial = xdbacJlTDQ;
    }

    public final boolean getsocial() {
        NetworkInfo networkInfo;
        ConnectivityManager acquisition = this.getsocial.acquisition();
        if (acquisition == null) {
            networkInfo = null;
        } else {
            networkInfo = acquisition.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isConnected();
    }
}
