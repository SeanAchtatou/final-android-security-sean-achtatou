package im.getsocial.sdk.internal.f.a;

/* compiled from: THReportStatus */
public enum JQrJMKopAa {
    OPEN(0),
    CLOSED(1);
    
    public final int value;

    private JQrJMKopAa(int i) {
        this.value = i;
    }

    public static JQrJMKopAa findByValue(int i) {
        switch (i) {
            case 0:
                return OPEN;
            case 1:
                return CLOSED;
            default:
                return null;
        }
    }
}
