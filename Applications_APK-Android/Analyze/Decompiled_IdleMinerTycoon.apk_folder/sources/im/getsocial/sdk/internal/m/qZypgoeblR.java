package im.getsocial.sdk.internal.m;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.a.a.jjbQypPegg;

/* compiled from: GetSocialExceptionUtil */
public final class qZypgoeblR {
    private qZypgoeblR() {
    }

    public static boolean getsocial(GetSocialException getSocialException, int i) {
        if (getSocialException instanceof jjbQypPegg) {
            return ((jjbQypPegg) getSocialException).getsocial(101);
        }
        return getSocialException.getErrorCode() == 101;
    }
}
