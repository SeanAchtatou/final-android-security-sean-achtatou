package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THSdkAuthRequest */
public final class RbduBRVrSj {
    public String acquisition;
    public String attribution;
    public String getsocial;
    public eTZqdsudqh mobile;
    public String retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof RbduBRVrSj)) {
            return false;
        }
        RbduBRVrSj rbduBRVrSj = (RbduBRVrSj) obj;
        return (this.getsocial == rbduBRVrSj.getsocial || (this.getsocial != null && this.getsocial.equals(rbduBRVrSj.getsocial))) && (this.attribution == rbduBRVrSj.attribution || (this.attribution != null && this.attribution.equals(rbduBRVrSj.attribution))) && ((this.acquisition == rbduBRVrSj.acquisition || (this.acquisition != null && this.acquisition.equals(rbduBRVrSj.acquisition))) && ((this.mobile == rbduBRVrSj.mobile || (this.mobile != null && this.mobile.equals(rbduBRVrSj.mobile))) && (this.retention == rbduBRVrSj.retention || (this.retention != null && this.retention.equals(rbduBRVrSj.retention)))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035;
        if (this.retention != null) {
            i = this.retention.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THSdkAuthRequest{appId=" + this.getsocial + ", userId=" + this.attribution + ", password=" + this.acquisition + ", sessionProperties=" + this.mobile + ", appSignatureFingerprint=" + this.retention + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, RbduBRVrSj rbduBRVrSj) {
        if (rbduBRVrSj.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(rbduBRVrSj.getsocial);
        }
        if (rbduBRVrSj.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(rbduBRVrSj.attribution);
        }
        if (rbduBRVrSj.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(rbduBRVrSj.acquisition);
        }
        if (rbduBRVrSj.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 12);
            eTZqdsudqh.getsocial(zotoebnojf, rbduBRVrSj.mobile);
        }
        if (rbduBRVrSj.retention != null) {
            zotoebnojf.getsocial(5, (byte) 11);
            zotoebnojf.getsocial(rbduBRVrSj.retention);
        }
        zotoebnojf.getsocial();
    }
}
