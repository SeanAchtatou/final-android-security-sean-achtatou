package im.getsocial.sdk.internal.g.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/* compiled from: DiskCache */
public class upgqDBbsrL implements jjbQypPegg {
    private static final cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);
    private final Context attribution;

    public upgqDBbsrL(Context context) {
        this.attribution = context;
    }

    public final void getsocial() {
        File file = new File(this.attribution.getCacheDir() + "/getsocial_images/");
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                getsocial(listFiles);
            }
            if (!file.delete()) {
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("Failed to delete " + file.getAbsolutePath());
            }
        }
    }

    private static void getsocial(File... fileArr) {
        for (File file : fileArr) {
            if (!file.delete()) {
                getsocial.attribution("Failed to delete " + file.getName());
            }
        }
    }

    public final boolean getsocial(String str) {
        return acquisition(str).exists();
    }

    public final Bitmap attribution(String str) {
        File acquisition = acquisition(str);
        if (acquisition.exists()) {
            return BitmapFactory.decodeFile(acquisition.getPath());
        }
        return null;
    }

    public final void getsocial(String str, Bitmap bitmap) {
        File file = new File(this.attribution.getCacheDir(), "/getsocial_images/");
        if (file.exists() || file.mkdir()) {
            File acquisition = acquisition(str);
            if (acquisition.exists() && !acquisition.delete()) {
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("Couldn't delete the old file " + acquisition.getPath());
            }
            FileOutputStream fileOutputStream = null;
            try {
                if (!acquisition.createNewFile()) {
                    cjrhisSQCL cjrhissqcl2 = getsocial;
                    cjrhissqcl2.attribution("Couldn't create the new file " + acquisition.getPath());
                }
                FileOutputStream fileOutputStream2 = new FileOutputStream(acquisition);
                try {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream2);
                    getsocial(fileOutputStream2);
                } catch (IOException e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    try {
                        cjrhisSQCL cjrhissqcl3 = getsocial;
                        cjrhissqcl3.mobile("Could not save image to the cache directory, returning null. error: " + e.getMessage());
                        getsocial(fileOutputStream);
                    } catch (Throwable th) {
                        th = th;
                        getsocial(fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    getsocial(fileOutputStream);
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                cjrhisSQCL cjrhissqcl32 = getsocial;
                cjrhissqcl32.mobile("Could not save image to the cache directory, returning null. error: " + e.getMessage());
                getsocial(fileOutputStream);
            }
        }
    }

    private File acquisition(String str) {
        return new File(this.attribution.getCacheDir() + "/getsocial_images/", String.valueOf(str.hashCode()));
    }

    private static void getsocial(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("Failed to close the stream: " + e.getMessage());
            }
        }
    }
}
