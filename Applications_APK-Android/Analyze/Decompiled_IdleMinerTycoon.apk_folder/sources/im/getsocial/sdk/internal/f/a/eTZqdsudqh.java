package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THSuperProperties */
public final class eTZqdsudqh {
    public String acquisition;

    /* renamed from: android  reason: collision with root package name */
    public KkSvQPDhNi f484android;
    public String attribution;
    public String cat;
    public String connect;
    public SIWJGMbZKs dau;
    public String engage;
    public String feeds;
    public String getsocial;
    public String growth;
    public String invites;
    public String ios;
    public String marketing;
    public String mau;
    public String mobile;
    public String organic;
    public String referral;
    public String retention;
    public String sharing;
    public Boolean social;
    public String unity;
    public String viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof eTZqdsudqh)) {
            return false;
        }
        eTZqdsudqh etzqdsudqh = (eTZqdsudqh) obj;
        return (this.getsocial == etzqdsudqh.getsocial || (this.getsocial != null && this.getsocial.equals(etzqdsudqh.getsocial))) && (this.attribution == etzqdsudqh.attribution || (this.attribution != null && this.attribution.equals(etzqdsudqh.attribution))) && ((this.acquisition == etzqdsudqh.acquisition || (this.acquisition != null && this.acquisition.equals(etzqdsudqh.acquisition))) && ((this.mobile == etzqdsudqh.mobile || (this.mobile != null && this.mobile.equals(etzqdsudqh.mobile))) && ((this.retention == etzqdsudqh.retention || (this.retention != null && this.retention.equals(etzqdsudqh.retention))) && ((this.dau == etzqdsudqh.dau || (this.dau != null && this.dau.equals(etzqdsudqh.dau))) && ((this.mau == etzqdsudqh.mau || (this.mau != null && this.mau.equals(etzqdsudqh.mau))) && ((this.cat == etzqdsudqh.cat || (this.cat != null && this.cat.equals(etzqdsudqh.cat))) && ((this.viral == etzqdsudqh.viral || (this.viral != null && this.viral.equals(etzqdsudqh.viral))) && ((this.organic == etzqdsudqh.organic || (this.organic != null && this.organic.equals(etzqdsudqh.organic))) && ((this.growth == etzqdsudqh.growth || (this.growth != null && this.growth.equals(etzqdsudqh.growth))) && ((this.f484android == etzqdsudqh.f484android || (this.f484android != null && this.f484android.equals(etzqdsudqh.f484android))) && ((this.ios == etzqdsudqh.ios || (this.ios != null && this.ios.equals(etzqdsudqh.ios))) && ((this.unity == etzqdsudqh.unity || (this.unity != null && this.unity.equals(etzqdsudqh.unity))) && ((this.connect == etzqdsudqh.connect || (this.connect != null && this.connect.equals(etzqdsudqh.connect))) && ((this.engage == etzqdsudqh.engage || (this.engage != null && this.engage.equals(etzqdsudqh.engage))) && ((this.referral == etzqdsudqh.referral || (this.referral != null && this.referral.equals(etzqdsudqh.referral))) && ((this.marketing == etzqdsudqh.marketing || (this.marketing != null && this.marketing.equals(etzqdsudqh.marketing))) && ((this.invites == etzqdsudqh.invites || (this.invites != null && this.invites.equals(etzqdsudqh.invites))) && ((this.feeds == etzqdsudqh.feeds || (this.feeds != null && this.feeds.equals(etzqdsudqh.feeds))) && ((this.sharing == etzqdsudqh.sharing || (this.sharing != null && this.sharing.equals(etzqdsudqh.sharing))) && (this.social == etzqdsudqh.social || (this.social != null && this.social.equals(etzqdsudqh.social))))))))))))))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((((((((((((((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ -1141910377) * -2128831035) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035) ^ (this.growth == null ? 0 : this.growth.hashCode())) * -2128831035) ^ (this.f484android == null ? 0 : this.f484android.hashCode())) * -2128831035) ^ (this.ios == null ? 0 : this.ios.hashCode())) * -2128831035) ^ (this.unity == null ? 0 : this.unity.hashCode())) * -2128831035) ^ (this.connect == null ? 0 : this.connect.hashCode())) * -2128831035) ^ (this.engage == null ? 0 : this.engage.hashCode())) * -2128831035) ^ (this.referral == null ? 0 : this.referral.hashCode())) * -2128831035) ^ (this.marketing == null ? 0 : this.marketing.hashCode())) * -2128831035) ^ (this.invites == null ? 0 : this.invites.hashCode())) * -2128831035) ^ (this.feeds == null ? 0 : this.feeds.hashCode())) * -2128831035) ^ (this.sharing == null ? 0 : this.sharing.hashCode())) * -2128831035;
        if (this.social != null) {
            i = this.social.hashCode();
        }
        return (hashCode ^ i) * -2128831035 * -2128831035 * -2128831035;
    }

    public final String toString() {
        return "THSuperProperties{appId=" + ((String) null) + ", appDeveloperId=" + ((String) null) + ", appDeveloperName=" + ((String) null) + ", city=" + ((String) null) + ", country=" + ((String) null) + ", region=" + ((String) null) + ", userId=" + ((String) null) + ", localTime=" + this.getsocial + ", ip=" + ((String) null) + ", appName=" + this.attribution + ", appVersionPublic=" + this.acquisition + ", appVersionInternal=" + this.mobile + ", appPackageName=" + this.retention + ", sdkRuntime=" + this.dau + ", sdkRuntimeVersion=" + this.mau + ", sdkVersion=" + this.cat + ", sdkWrapperVersion=" + this.viral + ", sdkLanguage=" + this.organic + ", deviceTimezone=" + this.growth + ", deviceOs=" + this.f484android + ", deviceOsVersion=" + this.ios + ", deviceCarrier=" + this.unity + ", deviceLanguage=" + this.connect + ", deviceManufacturer=" + this.engage + ", deviceModel=" + this.referral + ", deviceIdfa=" + this.marketing + ", deviceIdfv=" + this.invites + ", deviceNetworkType=" + this.feeds + ", deviceNetworkSubType=" + this.sharing + ", deviceJailbroken=" + this.social + ", isTestDevice=" + ((Object) null) + ", isEmulator=" + ((Object) null) + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, eTZqdsudqh etzqdsudqh) {
        if (etzqdsudqh.getsocial != null) {
            zotoebnojf.getsocial(8, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.getsocial);
        }
        if (etzqdsudqh.attribution != null) {
            zotoebnojf.getsocial(101, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.attribution);
        }
        if (etzqdsudqh.acquisition != null) {
            zotoebnojf.getsocial(102, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.acquisition);
        }
        if (etzqdsudqh.mobile != null) {
            zotoebnojf.getsocial(103, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.mobile);
        }
        if (etzqdsudqh.retention != null) {
            zotoebnojf.getsocial(104, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.retention);
        }
        if (etzqdsudqh.dau != null) {
            zotoebnojf.getsocial(105, (byte) 8);
            zotoebnojf.getsocial(etzqdsudqh.dau.value);
        }
        if (etzqdsudqh.mau != null) {
            zotoebnojf.getsocial(106, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.mau);
        }
        if (etzqdsudqh.cat != null) {
            zotoebnojf.getsocial(107, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.cat);
        }
        if (etzqdsudqh.viral != null) {
            zotoebnojf.getsocial(108, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.viral);
        }
        if (etzqdsudqh.organic != null) {
            zotoebnojf.getsocial(109, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.organic);
        }
        if (etzqdsudqh.growth != null) {
            zotoebnojf.getsocial(110, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.growth);
        }
        if (etzqdsudqh.f484android != null) {
            zotoebnojf.getsocial(112, (byte) 8);
            zotoebnojf.getsocial(etzqdsudqh.f484android.value);
        }
        if (etzqdsudqh.ios != null) {
            zotoebnojf.getsocial(113, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.ios);
        }
        if (etzqdsudqh.unity != null) {
            zotoebnojf.getsocial(114, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.unity);
        }
        if (etzqdsudqh.connect != null) {
            zotoebnojf.getsocial(115, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.connect);
        }
        if (etzqdsudqh.engage != null) {
            zotoebnojf.getsocial(116, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.engage);
        }
        if (etzqdsudqh.referral != null) {
            zotoebnojf.getsocial(117, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.referral);
        }
        if (etzqdsudqh.marketing != null) {
            zotoebnojf.getsocial(118, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.marketing);
        }
        if (etzqdsudqh.invites != null) {
            zotoebnojf.getsocial(119, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.invites);
        }
        if (etzqdsudqh.feeds != null) {
            zotoebnojf.getsocial(120, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.feeds);
        }
        if (etzqdsudqh.sharing != null) {
            zotoebnojf.getsocial(121, (byte) 11);
            zotoebnojf.getsocial(etzqdsudqh.sharing);
        }
        if (etzqdsudqh.social != null) {
            zotoebnojf.getsocial(122, (byte) 2);
            zotoebnojf.getsocial(etzqdsudqh.social.booleanValue());
        }
        zotoebnojf.getsocial();
    }
}
