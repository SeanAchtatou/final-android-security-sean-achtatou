package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.ArrayList;
import java.util.List;

/* compiled from: THInviteProviders */
public final class JWvbLzaedN {
    public List<SKUqohGtGQ> attribution;
    public QhisXzMgay getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof JWvbLzaedN)) {
            return false;
        }
        JWvbLzaedN jWvbLzaedN = (JWvbLzaedN) obj;
        return (this.getsocial == jWvbLzaedN.getsocial || (this.getsocial != null && this.getsocial.equals(jWvbLzaedN.getsocial))) && (this.attribution == jWvbLzaedN.attribution || (this.attribution != null && this.attribution.equals(jWvbLzaedN.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THInviteProviders{defaultInviteContent=" + this.getsocial + ", providers=" + this.attribution + "}";
    }

    public static JWvbLzaedN getsocial(zoToeBNOjF zotoebnojf) {
        JWvbLzaedN jWvbLzaedN = new JWvbLzaedN();
        while (true) {
            upgqDBbsrL acquisition = zotoebnojf.acquisition();
            if (acquisition.attribution != 0) {
                switch (acquisition.acquisition) {
                    case 1:
                        if (acquisition.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            jWvbLzaedN.getsocial = QhisXzMgay.getsocial(zotoebnojf);
                            break;
                        }
                    case 2:
                        if (acquisition.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention.attribution);
                            for (int i = 0; i < retention.attribution; i++) {
                                arrayList.add(SKUqohGtGQ.getsocial(zotoebnojf));
                            }
                            jWvbLzaedN.attribution = arrayList;
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                        break;
                }
            } else {
                return jWvbLzaedN;
            }
        }
    }
}
