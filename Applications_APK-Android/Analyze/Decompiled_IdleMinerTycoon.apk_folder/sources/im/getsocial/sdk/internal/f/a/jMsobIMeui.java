package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.HashMap;
import java.util.Map;

/* compiled from: THApplicationInfo */
public final class jMsobIMeui {
    public Map<String, String> acquisition;
    public String attribution;
    public String getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof jMsobIMeui)) {
            return false;
        }
        jMsobIMeui jmsobimeui = (jMsobIMeui) obj;
        return (this.getsocial == jmsobimeui.getsocial || (this.getsocial != null && this.getsocial.equals(jmsobimeui.getsocial))) && (this.attribution == jmsobimeui.attribution || (this.attribution != null && this.attribution.equals(jmsobimeui.attribution))) && (this.acquisition == jmsobimeui.acquisition || (this.acquisition != null && this.acquisition.equals(jmsobimeui.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THApplicationInfo{name=" + this.getsocial + ", iconUrl=" + this.attribution + ", properties=" + this.acquisition + "}";
    }

    public static jMsobIMeui getsocial(zoToeBNOjF zotoebnojf) {
        jMsobIMeui jmsobimeui = new jMsobIMeui();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            jmsobimeui.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            jmsobimeui.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile.acquisition);
                            for (int i = 0; i < mobile.acquisition; i++) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                            }
                            jmsobimeui.acquisition = hashMap;
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return jmsobimeui;
            }
        }
    }
}
