package im.getsocial.sdk.internal.c;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.gms.drive.DriveFile;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;

/* compiled from: AndroidLinkHandler */
public class cjrhisSQCL implements jMsobIMeui {
    private final jjbQypPegg attribution;
    /* access modifiers changed from: private */
    public final Context getsocial;

    @XdbacJlTDQ
    cjrhisSQCL(Context context, jjbQypPegg jjbqyppegg) {
        this.getsocial = context;
        this.attribution = jjbqyppegg;
    }

    public final void getsocial(final String str) {
        this.attribution.getsocial(new Runnable() {
            public void run() {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(DriveFile.MODE_READ_ONLY);
                cjrhisSQCL.this.getsocial.startActivity(intent);
            }
        });
    }
}
