package im.getsocial.sdk.internal.c.d.a;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;
import im.getsocial.sdk.invites.FetchReferralDataCallback;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.usermanagement.AddAuthIdentityCallback;

/* compiled from: Adapters */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static jjbQypPegg.C0016jjbQypPegg getsocial(Callback callback) {
        return new cjrhisSQCL(callback);
    }

    public static jjbQypPegg.C0016jjbQypPegg getsocial(InviteCallback inviteCallback) {
        return new zoToeBNOjF(inviteCallback);
    }

    public static jjbQypPegg.C0016jjbQypPegg getsocial(CompletionCallback completionCallback) {
        return new pdwpUtZXDT(completionCallback);
    }

    public static jjbQypPegg.C0016jjbQypPegg getsocial(AddAuthIdentityCallback addAuthIdentityCallback) {
        return new upgqDBbsrL(addAuthIdentityCallback);
    }

    public static jjbQypPegg.C0016jjbQypPegg getsocial(FetchReferralDataCallback fetchReferralDataCallback) {
        return new XdbacJlTDQ(fetchReferralDataCallback);
    }
}
