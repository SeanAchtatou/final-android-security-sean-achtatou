package im.getsocial.sdk.internal.c;

import android.content.Context;
import im.getsocial.sdk.GetSocial;
import im.getsocial.sdk.internal.m.XdbacJlTDQ;
import im.getsocial.sdk.internal.m.bpiSwUyLit;

/* compiled from: SuperPropertiesImpl */
public class QhisXzMgay implements JbBdMtJmlU {
    private final sqEuGXwfLT acquisition;
    private final Context attribution;
    private final XdbacJlTDQ getsocial;

    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    QhisXzMgay(XdbacJlTDQ xdbacJlTDQ, Context context, sqEuGXwfLT sqeugxwflt) {
        this.getsocial = xdbacJlTDQ;
        this.attribution = context;
        this.acquisition = sqeugxwflt;
    }

    public final String attribution() {
        return bpiSwUyLit.mobile(this.attribution);
    }

    public final String acquisition() {
        return bpiSwUyLit.retention(this.attribution);
    }

    public final KkSvQPDhNi getsocial() {
        return KkSvQPDhNi.ANDROID;
    }

    public final String mobile() {
        return bpiSwUyLit.attribution();
    }

    public final String retention() {
        return bpiSwUyLit.dau(this.attribution);
    }

    public final String dau() {
        return bpiSwUyLit.mau(this.attribution);
    }

    public final String mau() {
        return GetSocial.getSdkVersion();
    }

    public final String cat() {
        return bpiSwUyLit.getsocial(this.acquisition);
    }

    public final String viral() {
        return bpiSwUyLit.attribution(this.acquisition);
    }

    public final String organic() {
        return bpiSwUyLit.acquisition(this.acquisition);
    }

    public final String growth() {
        return GetSocial.getLanguage();
    }

    public final String android() {
        return bpiSwUyLit.acquisition();
    }

    public final long ios() {
        return bpiSwUyLit.mobile();
    }

    public final String unity() {
        return Long.toString(bpiSwUyLit.mobile());
    }

    public final long connect() {
        return bpiSwUyLit.retention();
    }

    public final String engage() {
        return bpiSwUyLit.dau();
    }

    public final String referral() {
        return bpiSwUyLit.cat(this.attribution);
    }

    public final String marketing() {
        return bpiSwUyLit.mau();
    }

    public final String invites() {
        return bpiSwUyLit.cat();
    }

    public final String feeds() {
        return bpiSwUyLit.viral(this.attribution);
    }

    public final boolean sharing() {
        return bpiSwUyLit.organic(this.attribution);
    }

    public final fOrCGNYyfk social() {
        return bpiSwUyLit.getsocial(this.getsocial);
    }

    public final boolean a() {
        return bpiSwUyLit.growth(this.attribution);
    }
}
