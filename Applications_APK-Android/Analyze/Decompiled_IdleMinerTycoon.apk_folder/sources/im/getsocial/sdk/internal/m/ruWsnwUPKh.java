package im.getsocial.sdk.internal.m;

import android.os.Handler;
import android.os.Looper;
import im.getsocial.sdk.internal.e.a.upgqDBbsrL;

/* compiled from: LooperScheduler */
class ruWsnwUPKh extends upgqDBbsrL {
    private final Handler getsocial;

    ruWsnwUPKh(Looper looper) {
        this(new Handler(looper));
    }

    private ruWsnwUPKh(Handler handler) {
        this.getsocial = handler;
    }

    public final void getsocial(Runnable runnable) {
        this.getsocial.post(runnable);
    }
}
