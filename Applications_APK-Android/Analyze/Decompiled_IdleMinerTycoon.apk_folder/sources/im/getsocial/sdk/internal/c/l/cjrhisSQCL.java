package im.getsocial.sdk.internal.c.l;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/* compiled from: Interfaces */
public final class cjrhisSQCL {
    private cjrhisSQCL() {
    }

    public static <T> T getsocial(Class<T> cls) {
        return cls.cast(Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new InvocationHandler() {
            public final Object invoke(Object obj, Method method, Object... objArr) {
                return null;
            }
        }));
    }

    public static <T> T getsocial(Class<T> cls, T t) {
        return t == null ? getsocial(cls) : t;
    }
}
