package im.getsocial.sdk.internal;

import com.helpshift.support.search.storage.TableSearchToken;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.activities.ReportingReason;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.a.g.jjbQypPegg;
import im.getsocial.sdk.internal.c.KCGqEGAizh;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.e.jjbQypPegg;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.vkXhnjhKGp;
import im.getsocial.sdk.invites.FetchReferralDataCallback;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.ReferralData;
import im.getsocial.sdk.invites.ReferredUser;
import im.getsocial.sdk.invites.a.k.KSZKMmRWhZ;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationListener;
import im.getsocial.sdk.pushnotifications.NotificationsCountQuery;
import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import im.getsocial.sdk.pushnotifications.NotificationsSummary;
import im.getsocial.sdk.socialgraph.SuggestedFriend;
import im.getsocial.sdk.socialgraph.a.c.HptYHntaqF;
import im.getsocial.sdk.socialgraph.a.c.fOrCGNYyfk;
import im.getsocial.sdk.socialgraph.a.c.qZypgoeblR;
import im.getsocial.sdk.usermanagement.AddAuthIdentityCallback;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.OnUserChangedListener;
import im.getsocial.sdk.usermanagement.PrivateUser;
import im.getsocial.sdk.usermanagement.PublicUser;
import im.getsocial.sdk.usermanagement.UserReference;
import im.getsocial.sdk.usermanagement.UsersQuery;
import im.getsocial.sdk.usermanagement.a.e.zoToeBNOjF;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* compiled from: GetSocialShared */
public class upgqDBbsrL {
    private static final long engage = TimeUnit.SECONDS.convert(1, TimeUnit.MINUTES);
    /* access modifiers changed from: private */
    public static final cjrhisSQCL referral = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial("GetSocialShared");
    @XdbacJlTDQ
    KSZKMmRWhZ acquisition;
    @XdbacJlTDQ

    /* renamed from: android  reason: collision with root package name */
    jjbQypPegg f490android;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.j.b.jjbQypPegg attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.a.jjbQypPegg cat;
    @XdbacJlTDQ
    im.getsocial.sdk.iap.a.d.jjbQypPegg connect;
    @XdbacJlTDQ
    vkXhnjhKGp dau;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.i.jjbQypPegg getsocial;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.f.cjrhisSQCL growth;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.e.jjbQypPegg ios;
    @XdbacJlTDQ
    pdwpUtZXDT mau;
    @XdbacJlTDQ
    im.getsocial.sdk.pushnotifications.a.d.upgqDBbsrL mobile;
    @XdbacJlTDQ
    KCGqEGAizh organic;
    @XdbacJlTDQ
    qdyNCsqjKt retention;
    @XdbacJlTDQ
    im.getsocial.sdk.invites.a.g.jjbQypPegg unity;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.h.jjbQypPegg viral;

    public upgqDBbsrL() {
        referral.attribution("GetSocialShared created");
        ztWNWCuZiM.getsocial(this);
        referral.attribution("GetSocialShared injected");
        getsocial("facebook", new im.getsocial.sdk.invites.a.e.jjbQypPegg(new im.getsocial.sdk.invites.a.e.a.upgqDBbsrL()));
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(Runnable runnable) {
        cjrhisSQCL cjrhissqcl = referral;
        StringBuilder sb = new StringBuilder("GetSocialShared.whenInitialized called, session init at moment: ");
        sb.append(getsocial() ? "true" : "false");
        cjrhissqcl.attribution(sb.toString());
        if (getsocial()) {
            runnable.run();
        } else {
            this.getsocial.attribution(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean getsocial() {
        referral.attribution("GetSocialShared.isSessionInitialized called");
        boolean z = this.organic.getsocial();
        cjrhisSQCL cjrhissqcl = referral;
        StringBuilder sb = new StringBuilder("Session initialized at moment: ");
        sb.append(z ? "true" : "false");
        cjrhissqcl.attribution(sb.toString());
        return z;
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(final String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.init(" + str + ")");
        this.ios.getsocial(new jjbQypPegg.C0017jjbQypPegg() {
            public final void getsocial(CompletionCallback completionCallback) {
                new im.getsocial.sdk.internal.i.d.cjrhisSQCL().getsocial(str, upgqDBbsrL.attribution(upgqDBbsrL.this, upgqDBbsrL.getsocial(upgqDBbsrL.this, completionCallback)));
            }
        });
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.resetUser(" + completionCallback + ")");
        viral("resetUser");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new im.getsocial.sdk.internal.i.d.pdwpUtZXDT().getsocial(completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setLanguage(" + str + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "Can not set null or empty language");
        viral("setLanguage");
        new im.getsocial.sdk.internal.j.c.jjbQypPegg().getsocial(str);
    }

    /* access modifiers changed from: package-private */
    public final boolean acquisition(String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.isInviteChannelAvailable(" + str + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "ChannelId should be one of the constants defined in InviteChannelIds class");
        for (InviteChannel channelId : attribution()) {
            if (str.equals(channelId.getChannelId())) {
                cjrhisSQCL cjrhissqcl2 = referral;
                cjrhissqcl2.attribution("GetSocialShared.isInviteChannelAvailable(" + str + ") -> true");
                return true;
            }
        }
        cjrhisSQCL cjrhissqcl3 = referral;
        cjrhissqcl3.attribution("GetSocialShared.isInviteChannelAvailable(" + str + ") -> false");
        return false;
    }

    /* access modifiers changed from: package-private */
    public final List<InviteChannel> attribution() {
        referral.attribution("GetSocialShared.getInviteChannels()");
        cat("getInviteChannels");
        List<InviteChannel> list = new im.getsocial.sdk.invites.a.j.upgqDBbsrL().getsocial();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getInviteChannels() -> " + list);
        return list;
    }

    static void getsocial(String str, im.getsocial.sdk.invites.a.pdwpUtZXDT pdwputzxdt) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.registerInviteChannelPlugin(" + str + "," + pdwputzxdt + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Can not call registerInviteChannelPlugin with null channelId");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(pdwputzxdt), "Can not call registerInviteChannelPlugin with null sharedInviteProviderPlugin");
        new im.getsocial.sdk.invites.a.j.pdwpUtZXDT().getsocial(str, pdwputzxdt);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, im.getsocial.sdk.invites.a.b.pdwpUtZXDT pdwputzxdt, LinkParams linkParams, InviteCallback inviteCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.sendInvite(" + str + "," + pdwputzxdt + "," + linkParams + "," + inviteCallback + ")");
        viral("sendInvite");
        new im.getsocial.sdk.invites.a.j.XdbacJlTDQ().getsocial(str, pdwputzxdt, linkParams, inviteCallback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(FetchReferralDataCallback fetchReferralDataCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getReferralData(" + fetchReferralDataCallback + ")");
        cat("getReferralData");
        this.acquisition.getsocial(fetchReferralDataCallback);
    }

    /* access modifiers changed from: package-private */
    public final void acquisition() {
        referral.attribution("GetSocialShared.clearReferralData()");
        this.unity.getsocial((ReferralData) null);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(Callback<List<ReferredUser>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getReferredUsers(" + callback + ")");
        cat("getReferredUsers");
        new im.getsocial.sdk.invites.a.j.cjrhisSQCL().getsocial(callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(LinkParams linkParams, Callback<String> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.createInviteLink(" + linkParams + TableSearchToken.COMMA_SEP + callback + ")");
        cat("createInviteLink");
        new im.getsocial.sdk.invites.a.j.jjbQypPegg().getsocial(linkParams, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(OnUserChangedListener onUserChangedListener) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setOnUserChangedListener(" + onUserChangedListener + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(onUserChangedListener), "OnUserChangedListener can not be null");
        this.getsocial.getsocial(onUserChangedListener);
    }

    /* access modifiers changed from: package-private */
    public final void mobile() {
        referral.attribution("GetSocialShared.removeOnUserChangedListener()");
        this.getsocial.getsocial((OnUserChangedListener) null);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT pdwputzxdt, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setUserDetails(" + pdwputzxdt + "," + completionCallback + ")");
        viral("setUserDetails");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(pdwputzxdt), "UserUpdate can not be null");
        new im.getsocial.sdk.usermanagement.a.e.ztWNWCuZiM().getsocial(pdwputzxdt, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final String retention() {
        referral.attribution("GetSocialShared.getUserId()");
        cat("getUserId");
        String id = marketing().getId();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getUserId() -> " + id);
        return id;
    }

    /* access modifiers changed from: package-private */
    public final String dau() {
        referral.attribution("GetSocialShared.getDisplayName()");
        cat("getDisplayName");
        String displayName = marketing().getDisplayName();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getDisplayName() -> " + displayName);
        return displayName;
    }

    /* access modifiers changed from: package-private */
    public final String mau() {
        referral.attribution("GetSocialShared.getAvatarUrl()");
        cat("getAvatarUrl");
        String avatarUrl = marketing().getAvatarUrl();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getAvatarUrl() -> " + avatarUrl);
        return avatarUrl;
    }

    /* access modifiers changed from: package-private */
    public final String mobile(String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getPrivateProperty(" + str + ")");
        cat("getPrivateProperty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Key can not be null");
        String privateProperty = marketing().getPrivateProperty(str);
        cjrhisSQCL cjrhissqcl2 = referral;
        cjrhissqcl2.attribution("GetSocialShared.getPrivateProperty(" + str + ") -> " + privateProperty);
        return privateProperty;
    }

    /* access modifiers changed from: package-private */
    public final String retention(String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getPublicProperty(" + str + ")");
        cat("getPublicProperty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Key can not be null");
        String publicProperty = marketing().getPublicProperty(str);
        cjrhisSQCL cjrhissqcl2 = referral;
        cjrhissqcl2.attribution("GetSocialShared.getPublicProperty(" + str + ") -> " + publicProperty);
        return publicProperty;
    }

    /* access modifiers changed from: package-private */
    public final boolean dau(String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.hasPrivateProperty(" + str + ")");
        cat("hasPrivateProperty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Key can not be null");
        boolean hasPrivateProperty = marketing().hasPrivateProperty(str);
        cjrhisSQCL cjrhissqcl2 = referral;
        cjrhissqcl2.attribution("GetSocialShared.hasPrivateProperty(" + str + ") -> " + hasPrivateProperty);
        return hasPrivateProperty;
    }

    /* access modifiers changed from: package-private */
    public final boolean mau(String str) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.hasPublicProperty(" + str + ")");
        cat("hasPublicProperty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Key can not be null");
        boolean hasPublicProperty = marketing().hasPublicProperty(str);
        cjrhisSQCL cjrhissqcl2 = referral;
        cjrhissqcl2.attribution("GetSocialShared.hasPublicProperty(" + str + ") -> " + hasPublicProperty);
        return hasPublicProperty;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> cat() {
        referral.attribution("GetSocialShared.getAllPublicProperties()");
        cat("getAllPublicProperties");
        Map<String, String> allPublicProperties = marketing().getAllPublicProperties();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getAllPublicProperties() -> " + allPublicProperties);
        return allPublicProperties;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> viral() {
        referral.attribution("GetSocialShared.getAllPrivateProperties()");
        cat("getAllPrivateProperties");
        Map<String, String> allPrivateProperties = marketing().getAllPrivateProperties();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getAllPrivateProperties() -> " + allPrivateProperties);
        return allPrivateProperties;
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(AuthIdentity authIdentity, AddAuthIdentityCallback addAuthIdentityCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.addAuthIdentity(" + authIdentity + TableSearchToken.COMMA_SEP + addAuthIdentityCallback + ")");
        viral("addAuthIdentity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(authIdentity), "AuthIdentity can not be null");
        new im.getsocial.sdk.usermanagement.a.e.jjbQypPegg().getsocial(authIdentity, addAuthIdentityCallback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.removeAuthIdentity(" + str + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("removeAuthIdentity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "ProviderId can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new zoToeBNOjF().getsocial(str, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final boolean organic() {
        referral.attribution("GetSocialShared.isAnonymous()");
        cat("isAnonymous");
        boolean isEmpty = marketing().getAuthIdentities().isEmpty();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.isAnonymous() -> " + isEmpty);
        return isEmpty;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> growth() {
        referral.attribution("GetSocialShared.getAuthIdentities()");
        cat("getAuthIdentities");
        Map<String, String> authIdentities = marketing().getAuthIdentities();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getAuthIdentities() -> " + authIdentities);
        return authIdentities;
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(AuthIdentity authIdentity, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.switchUser(" + authIdentity + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("switchUser");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(authIdentity), "AuthIdentity can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new im.getsocial.sdk.usermanagement.a.e.KSZKMmRWhZ().getsocial(authIdentity, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, String str2, final Callback<PublicUser> callback) {
        AnonymousClass4 r6;
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getUserByAuthIdentity(" + str + TableSearchToken.COMMA_SEP + str2 + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getUserByAuthIdentity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Provider id can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str2), "User id can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        im.getsocial.sdk.usermanagement.a.e.cjrhisSQCL cjrhissqcl2 = new im.getsocial.sdk.usermanagement.a.e.cjrhisSQCL();
        List asList = Arrays.asList(str2);
        if (callback == null) {
            r6 = null;
        } else {
            r6 = new Callback<Map<String, PublicUser>>() {
                public /* synthetic */ void onSuccess(Object obj) {
                    Map map = (Map) obj;
                    int size = map.size();
                    if (size == 1) {
                        callback.onSuccess(map.values().iterator().next());
                    } else {
                        callback.onFailure(size == 0 ? new GetSocialException(204, "No GetSocial User found for provided arguments") : new GetSocialException(ErrorCode.ILLEGAL_STATE, "API returned unexpected amount of responses"));
                    }
                }

                public void onFailure(GetSocialException getSocialException) {
                    callback.onFailure(getSocialException);
                }
            };
        }
        cjrhissqcl2.getsocial(str, asList, r6);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, List<String> list, Callback<Map<String, PublicUser>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getUsersByAuthIdentities(" + str + TableSearchToken.COMMA_SEP + list + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getUsersByAuthIdentities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Provider id can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "List of user ids can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.usermanagement.a.e.cjrhisSQCL().getsocial(str, list, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, Callback<PublicUser> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getUserById(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getUserById");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "UserId can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.usermanagement.a.e.upgqDBbsrL().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(UsersQuery usersQuery, Callback<List<UserReference>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.findUsers(" + usersQuery + TableSearchToken.COMMA_SEP + callback + ")");
        viral("findUsers");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(usersQuery), "Query can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        boolean z = false;
        jjbQypPegg.C0021jjbQypPegg.getsocial(usersQuery.getLimit() > 0, "Limit can not be less than or equal 0");
        if (usersQuery.getLimit() <= 20) {
            z = true;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Limit can not be greater that 20");
        new im.getsocial.sdk.usermanagement.a.e.pdwpUtZXDT().getsocial(usersQuery, callback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(String str, Callback<Integer> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.addFriend(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        viral("addFriend");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "UserID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.socialgraph.a.c.jjbQypPegg().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(String str, List<String> list, Callback<Integer> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.addFriendsByAuthIdentities(" + str + TableSearchToken.COMMA_SEP + list + TableSearchToken.COMMA_SEP + callback + ")");
        viral("addFriendsByAuthIdentities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "Provider id can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "User ids list can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.socialgraph.a.c.upgqDBbsrL().getsocial(str, list, callback);
    }

    /* access modifiers changed from: package-private */
    public final void acquisition(String str, Callback<Integer> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.removeFriend(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        viral("removeFriend");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "UserID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.socialgraph.a.c.KSZKMmRWhZ().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void acquisition(String str, List<String> list, Callback<Integer> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.removeFriendsByAuthIdentities(" + str + TableSearchToken.COMMA_SEP + list + TableSearchToken.COMMA_SEP + callback + ")");
        viral("removeFriendsByAuthIdentities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Provider id can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "User ids list can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new HptYHntaqF().getsocial(str, list, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(List<String> list, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setFriends(" + list + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("setFriends");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "User ids list can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new fOrCGNYyfk().getsocial(list, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, List<String> list, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setFriendsByAuthIdentities(" + str + TableSearchToken.COMMA_SEP + list + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("setFriendsByAuthIdentities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Provider id can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "User ids list can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new qZypgoeblR().getsocial(str, list, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void mobile(String str, Callback<Boolean> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.isFriend(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        viral("isFriend");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "UserID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.socialgraph.a.c.ztWNWCuZiM().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(Callback<Integer> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getFriendsCount(" + callback + ")");
        viral("getFriendsCount");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.socialgraph.a.c.cjrhisSQCL().getsocial(callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(int i, int i2, Callback<List<PublicUser>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getFriends(" + i + ", , " + i2 + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getFriends");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        boolean z = false;
        jjbQypPegg.C0021jjbQypPegg.getsocial(i2 > 0, "Limit can not be less ot equal zero");
        if (i >= 0) {
            z = true;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Offset can not be less than zero");
        new im.getsocial.sdk.socialgraph.a.c.XdbacJlTDQ().getsocial(i, i2, callback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(int i, int i2, Callback<List<SuggestedFriend>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getSuggestedFriends(" + i + ", , " + i2 + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getSuggestedFriends");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        boolean z = false;
        jjbQypPegg.C0021jjbQypPegg.getsocial(i2 > 0, "Limit can not be less ot equal zero");
        if (i >= 0) {
            z = true;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Offset can not be less than zero");
        new im.getsocial.sdk.socialgraph.a.c.zoToeBNOjF().getsocial(i, i2, callback);
    }

    /* access modifiers changed from: package-private */
    public final void acquisition(Callback<List<UserReference>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getFriendsReferences(" + callback + ")");
        viral("getFriendsReferences");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.socialgraph.a.c.pdwpUtZXDT().getsocial(callback);
    }

    /* access modifiers changed from: package-private */
    public final void retention(String str, Callback<List<ActivityPost>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getStickyActivities(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getStickyActivities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "Feed can not be null or empty");
        new im.getsocial.sdk.activities.a.d.XdbacJlTDQ().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(ActivitiesQuery activitiesQuery, Callback<List<ActivityPost>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getActivities(" + activitiesQuery + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getActivities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(activitiesQuery), "Query can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        im.getsocial.sdk.activities.jjbQypPegg.getsocial(activitiesQuery).organic();
        new im.getsocial.sdk.activities.a.d.upgqDBbsrL().getsocial(activitiesQuery, callback);
    }

    /* access modifiers changed from: package-private */
    public final void dau(String str, Callback<ActivityPost> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getActivity(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getActivity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Activity ID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.pdwpUtZXDT().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg, Callback<ActivityPost> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.postActivity(" + str + TableSearchToken.COMMA_SEP + jjbqyppegg + TableSearchToken.COMMA_SEP + callback + ")");
        viral("postActivity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.ztWNWCuZiM().getsocial(str, jjbqyppegg, callback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(String str, im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg, Callback<ActivityPost> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.postComment(" + str + TableSearchToken.COMMA_SEP + jjbqyppegg + TableSearchToken.COMMA_SEP + callback + ")");
        viral("postComment");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.KSZKMmRWhZ().getsocial(str, jjbqyppegg, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, boolean z, Callback<ActivityPost> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.likeActivity(" + str + TableSearchToken.COMMA_SEP + z + TableSearchToken.COMMA_SEP + callback + ")");
        viral("likeActivity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Activity ID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.zoToeBNOjF().getsocial(str, z, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, int i, int i2, Callback<List<PublicUser>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getActivityLikers(" + str + TableSearchToken.COMMA_SEP + i + TableSearchToken.COMMA_SEP + i2 + TableSearchToken.COMMA_SEP + callback + ")");
        viral("getActivityLikers");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Activity ID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        boolean z = false;
        jjbQypPegg.C0021jjbQypPegg.getsocial(i >= 0, "Offset can not be less then 0");
        if (i2 > 0) {
            z = true;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Limit can not be equal or less then 0");
        new im.getsocial.sdk.activities.a.d.cjrhisSQCL().getsocial(str, i, i2, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, ReportingReason reportingReason, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.reportActivity(" + str + TableSearchToken.COMMA_SEP + reportingReason + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("reportActivity");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Activity ID can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(reportingReason), "ReportingReason can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.fOrCGNYyfk().getsocial(str, reportingReason, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void attribution(List<String> list, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.removeActivities(" + list + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("removeActivities");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((List) list), "Activity IDs can not be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.qZypgoeblR().getsocial(list, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(TagsQuery tagsQuery, Callback<List<String>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.findTags(" + tagsQuery + TableSearchToken.COMMA_SEP + callback + ")");
        viral("findTags");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(tagsQuery), "Tags query can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.activities.a.d.jjbQypPegg().getsocial(tagsQuery, callback);
    }

    /* access modifiers changed from: package-private */
    public final void android() {
        referral.attribution("GetSocialShared.registerForPushNotifications()");
        viral("registerForPushNotifications");
        new im.getsocial.sdk.pushnotifications.a.g.ztWNWCuZiM().getsocial();
    }

    /* access modifiers changed from: package-private */
    public final void attribution(String str, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.registerOnPushServer(" + str + TableSearchToken.COMMA_SEP + completionCallback + ")");
        viral("registerOnPushServer");
        new im.getsocial.sdk.pushnotifications.a.g.KSZKMmRWhZ().getsocial(str, completionCallback);
    }

    static void getsocial(im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF zotoebnojf) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.processPendingPushNotification(" + zotoebnojf.organic() + ")");
        new im.getsocial.sdk.pushnotifications.a.g.zoToeBNOjF().getsocial(zotoebnojf);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(NotificationListener notificationListener) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setNotificationListener(" + notificationListener + ")");
        jjbQypPegg.cjrhisSQCL.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(notificationListener), "Can not set null NotificationListener");
        this.mobile.getsocial(notificationListener);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(NotificationsQuery notificationsQuery, Callback<List<Notification>> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getNotifications(" + notificationsQuery + TableSearchToken.COMMA_SEP + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(notificationsQuery), "Query can not be null");
        viral("getNotifications");
        new im.getsocial.sdk.pushnotifications.a.g.upgqDBbsrL().getsocial(notificationsQuery, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(NotificationsCountQuery notificationsCountQuery, Callback<Integer> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getNotificationsCount(" + notificationsCountQuery + TableSearchToken.COMMA_SEP + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(notificationsCountQuery), "Query can not be null");
        viral("getNotificationsCount");
        new im.getsocial.sdk.pushnotifications.a.g.jjbQypPegg().getsocial(notificationsCountQuery, callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(List<String> list, String str, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setNotificationsStatus(" + list + TableSearchToken.COMMA_SEP + str + TableSearchToken.COMMA_SEP + completionCallback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "Notifications list can not be null");
        viral("setNotificationsStatus");
        new im.getsocial.sdk.pushnotifications.a.g.qZypgoeblR().getsocial(list, str, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(boolean z, CompletionCallback completionCallback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.setPushNotificationsEnabled(" + z + TableSearchToken.COMMA_SEP + completionCallback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "Callback can not be null");
        viral("setPushNotificationsEnabled");
        new im.getsocial.sdk.pushnotifications.a.g.fOrCGNYyfk().getsocial(z, completionCallback);
    }

    /* access modifiers changed from: package-private */
    public final void mobile(Callback<Boolean> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.isPushNotificationsEnabled(" + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        viral("isPushNotificationsEnabled");
        new im.getsocial.sdk.pushnotifications.a.g.pdwpUtZXDT().getsocial(callback);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(List<String> list, im.getsocial.sdk.pushnotifications.a.b.upgqDBbsrL upgqdbbsrl, Callback<NotificationsSummary> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.sendNotification(" + list + TableSearchToken.COMMA_SEP + upgqdbbsrl + TableSearchToken.COMMA_SEP + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((List) list), "User list can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(upgqdbbsrl), "Notification content can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        viral("sendNotification");
        new im.getsocial.sdk.pushnotifications.a.g.HptYHntaqF().getsocial(list, upgqdbbsrl, callback);
    }

    static void getsocial(Action action) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.processAction(" + action + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(action), "Action can not be null");
        new im.getsocial.sdk.actions.a.b.jjbQypPegg().getsocial(action);
    }

    static void getsocial(PromoCodeBuilder promoCodeBuilder, Callback<PromoCode> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.createPromoCode(" + promoCodeBuilder + TableSearchToken.COMMA_SEP + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(promoCodeBuilder), "PromoCodeBuilder can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.promocodes.a.b.upgqDBbsrL().getsocial(promoCodeBuilder, callback);
    }

    static void mau(String str, Callback<PromoCode> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.getPromoCode(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "PromoCode can not be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.promocodes.a.b.cjrhisSQCL().getsocial(str, callback);
    }

    static void cat(String str, Callback<PromoCode> callback) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.claimPromoCode(" + str + TableSearchToken.COMMA_SEP + callback + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "PromoCode can not be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Callback can not be null");
        new im.getsocial.sdk.promocodes.a.b.jjbQypPegg().getsocial(str, callback);
    }

    /* access modifiers changed from: package-private */
    public final void ios() {
        referral.attribution("GetSocialShared.trackAppSessionEvent()");
        long acquisition2 = this.retention.acquisition("application_did_become_active_event_timestamp");
        long acquisition3 = this.retention.acquisition("application_did_become_inactive_event_timestamp");
        if (acquisition2 == 0) {
            acquisition3 = 0;
        }
        referral.attribution("GetSocialShared.trackAppSessionEvent -> becomeActive: " + acquisition2 + ", becomeInactive: " + acquisition3);
        boolean z = false;
        if (acquisition3 == 0) {
            referral.attribution("GetSocialShared.trackAppSessionEvent -> Track app session start");
            this.f490android.getsocial("app_session_start", (Map<String, String>) null);
        } else {
            if (this.growth.attribution() - acquisition3 > engage) {
                z = true;
            }
            if (z) {
                referral.attribution("GetSocialShared.trackAppSessionEvent -> Track app session end");
                this.f490android.getsocial(acquisition2, acquisition3);
                referral.attribution("GetSocialShared.trackAppSessionEvent -> Track app session start");
                this.f490android.getsocial("app_session_start", (Map<String, String>) null);
                this.retention.retention("application_did_become_inactive_event_timestamp");
            }
        }
        long attribution2 = this.growth.attribution();
        referral.attribution("GetSocialShared.trackAppSessionEvent -> Set app session start time: " + attribution2);
        this.retention.getsocial("application_did_become_active_event_timestamp", attribution2);
    }

    /* access modifiers changed from: package-private */
    public final void unity() {
        referral.attribution("GetSocialShared.applicationDidBecomeInactive()");
        long attribution2 = this.growth.attribution();
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.trackAppSessionEvent -> Set app session end time: " + attribution2);
        this.retention.getsocial("application_did_become_inactive_event_timestamp", attribution2);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(PurchaseData purchaseData) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.trackPurchaseEvent(" + purchaseData + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(purchaseData), "PurchaseData cannot be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(this.connect.getsocial() ^ true, "trackPurchaseEvent can be called only if automatic purchase tracking is disabled on dashboard.");
        this.f490android.getsocial(purchaseData);
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str, Map<String, String> map) {
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.trackCustomEvent(" + str + TableSearchToken.COMMA_SEP + map + ")");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "EventName cannot be null");
        this.f490android.attribution(str, map);
    }

    private void cat(String str) {
        boolean z = getsocial();
        jjbQypPegg.cjrhisSQCL.getsocial(z, "GetSocial SDK should be initialized before calling " + str);
    }

    /* access modifiers changed from: package-private */
    public final void connect() {
        referral.attribution("GetSocialShared.handleOnStartUnityEvent()");
        if (this.dau instanceof im.getsocial.sdk.internal.m.qdyNCsqjKt) {
            ((im.getsocial.sdk.internal.m.qdyNCsqjKt) this.dau).getsocial();
        } else {
            referral.mobile("GetSocialShared.handleOnStartUnityEvent() called on non-Unity app, that's not OK.");
        }
    }

    private PrivateUser marketing() {
        return ((im.getsocial.sdk.usermanagement.a.c.jjbQypPegg) this.mau.getsocial(im.getsocial.sdk.usermanagement.a.c.jjbQypPegg.class)).getsocial();
    }

    /* access modifiers changed from: package-private */
    public final void engage() {
        referral.attribution("GetSocialShared.onResume()");
        InviteCallback acquisition2 = this.unity.acquisition();
        if (acquisition2 == null) {
            referral.attribution("GetSocialShared.onResume -> No pending callback to notify");
            return;
        }
        cjrhisSQCL cjrhissqcl = referral;
        cjrhissqcl.attribution("GetSocialShared.onResume() -> notify pending invite callback: " + acquisition2);
        this.unity.getsocial((InviteCallback) null);
        acquisition2.onComplete();
    }

    private void viral(String str) {
        boolean z = this.viral.getsocial();
        jjbQypPegg.upgqDBbsrL.getsocial(z, "Can not call " + str + " when internet connection is off");
        boolean z2 = getsocial();
        jjbQypPegg.cjrhisSQCL.getsocial(z2, "GetSocial SDK should be initialized before calling " + str);
    }

    static /* synthetic */ CompletionCallback getsocial(upgqDBbsrL upgqdbbsrl, final CompletionCallback completionCallback) {
        return new CompletionCallback() {
            public void onSuccess() {
                upgqDBbsrL.getsocial(upgqDBbsrL.this);
                completionCallback.onSuccess();
            }

            public void onFailure(GetSocialException getSocialException) {
                completionCallback.onFailure(getSocialException);
            }
        };
    }

    static /* synthetic */ CompletionCallback attribution(upgqDBbsrL upgqdbbsrl, final CompletionCallback completionCallback) {
        return new CompletionCallback() {
            public void onSuccess() {
                upgqDBbsrL.referral.attribution("GetSocialShared.onInitialized()");
                Runnable runnable = upgqDBbsrL.this.getsocial.getsocial();
                if (runnable == null) {
                    upgqDBbsrL.referral.attribution("GetSocialShared.onInitialized() -> No onInitialize listener to notify");
                } else {
                    cjrhisSQCL referral = upgqDBbsrL.referral;
                    referral.attribution("GetSocialShared.onInitialized() -> Notify onInitialize listener: " + runnable);
                    runnable.run();
                    upgqDBbsrL.this.getsocial.attribution((Runnable) null);
                }
                completionCallback.onSuccess();
            }

            public void onFailure(GetSocialException getSocialException) {
                completionCallback.onFailure(getSocialException);
            }
        };
    }

    static /* synthetic */ void getsocial(upgqDBbsrL upgqdbbsrl) {
        referral.attribution("GetSocialShared.processPendingPushNotificationIfNeeded()");
        im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF zotoebnojf = upgqdbbsrl.mobile.getsocial();
        if (zotoebnojf == null) {
            referral.attribution("GetSocialShared.processPendingPushNotificationIfNeeded -> No notification to process.");
        } else {
            getsocial(zotoebnojf);
        }
    }
}
