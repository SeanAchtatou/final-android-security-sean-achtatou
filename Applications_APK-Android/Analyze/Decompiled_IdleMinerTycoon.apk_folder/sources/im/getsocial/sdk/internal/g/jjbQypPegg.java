package im.getsocial.sdk.internal.g;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.google.android.gms.nearby.messages.Strategy;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.g.e.upgqDBbsrL;
import java.lang.ref.WeakReference;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: ImageLoader */
public final class jjbQypPegg {
    private static upgqDBbsrL acquisition = new im.getsocial.sdk.internal.g.e.jjbQypPegg();
    private static Map<WeakReference<ImageView>, String> attribution = new HashMap();
    /* access modifiers changed from: private */
    public static final cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(jjbQypPegg.class);
    /* access modifiers changed from: private */
    public static im.getsocial.sdk.internal.g.b.jjbQypPegg mobile = new im.getsocial.sdk.internal.g.b.cjrhisSQCL();
    private static im.getsocial.sdk.internal.g.d.jjbQypPegg retention = new im.getsocial.sdk.internal.g.d.upgqDBbsrL();

    /* renamed from: android  reason: collision with root package name */
    private int f488android = 0;
    /* access modifiers changed from: private */
    public upgqDBbsrL cat;
    /* access modifiers changed from: private */
    public im.getsocial.sdk.internal.g.a.jjbQypPegg dau = new im.getsocial.sdk.internal.g.a.upgqDBbsrL();
    private int growth = 0;
    /* access modifiers changed from: private */
    public Callback<Bitmap> ios;
    /* access modifiers changed from: private */
    public boolean mau;
    /* access modifiers changed from: private */
    public final String organic;
    /* access modifiers changed from: private */
    public im.getsocial.sdk.internal.g.b.jjbQypPegg unity;
    private final Set<pdwpUtZXDT> viral = new HashSet();

    private static boolean getsocial(ImageView imageView, ImageView imageView2) {
        return imageView == imageView2;
    }

    private jjbQypPegg(String str) {
        this.organic = str;
    }

    private jjbQypPegg(String str, int i, int i2) {
        this.organic = str;
        this.growth = i;
        this.f488android = i2;
    }

    public static void getsocial(ImageView imageView) {
        attribution(new WeakReference(imageView));
    }

    public static jjbQypPegg getsocial(String str) {
        return new jjbQypPegg(str);
    }

    public static jjbQypPegg getsocial(String str, int i, int i2) {
        return new jjbQypPegg(str, i, i2);
    }

    public final jjbQypPegg getsocial(int i, int i2) {
        this.cat = new cjrhisSQCL(i, i2);
        return this;
    }

    public final jjbQypPegg getsocial(pdwpUtZXDT pdwputzxdt) {
        this.viral.add(pdwputzxdt);
        return this;
    }

    public final jjbQypPegg getsocial(im.getsocial.sdk.internal.g.b.jjbQypPegg jjbqyppegg) {
        this.unity = jjbqyppegg;
        return this;
    }

    public final void getsocial(Callback<Bitmap> callback) {
        this.ios = callback;
        if (!acquisition() || !mobile.getsocial(this.organic)) {
            if (this.cat != null) {
                this.growth = this.cat.getsocial();
                this.f488android = this.cat.attribution();
            }
            acquisition.getsocial(this.organic, this.growth, this.f488android, new Callback<Bitmap>() {
                public /* synthetic */ void onSuccess(Object obj) {
                    Bitmap bitmap = (Bitmap) obj;
                    Bitmap bitmap2 = jjbQypPegg.getsocial(jjbQypPegg.this, bitmap) ? jjbQypPegg.this.cat.getsocial(bitmap) : bitmap;
                    if (jjbQypPegg.acquisition()) {
                        jjbQypPegg.mobile.getsocial(jjbQypPegg.this.organic, bitmap2);
                    }
                    if (jjbQypPegg.this.unity != null) {
                        jjbQypPegg.this.unity.getsocial(jjbQypPegg.this.organic, bitmap);
                    }
                    jjbQypPegg.this.getsocial(bitmap2);
                }

                public void onFailure(GetSocialException getSocialException) {
                    if (getSocialException.getErrorCode() == 103) {
                        jjbQypPegg.mobile.getsocial();
                    }
                    jjbQypPegg.this.getsocial(getSocialException);
                }
            });
            return;
        }
        getsocial(mobile.attribution(this.organic));
    }

    public final void getsocial(ImageView imageView, final Drawable drawable) {
        final WeakReference weakReference = new WeakReference(imageView);
        if (!attribution(weakReference, this.organic)) {
            String str = this.organic;
            attribution(weakReference);
            attribution.put(weakReference, str);
            if (!acquisition() || !mobile.getsocial(this.organic)) {
                if (this.unity == null || !this.unity.getsocial(this.organic)) {
                    imageView.setImageDrawable(drawable);
                } else {
                    imageView.setImageBitmap(this.unity.attribution(this.organic));
                    this.mau = true;
                }
                retention.getsocial(new Runnable() {
                    public void run() {
                        if (jjbQypPegg.attribution(weakReference, jjbQypPegg.this.organic)) {
                            jjbQypPegg.this.getsocial(new Callback<Bitmap>() {
                                public /* synthetic */ void onSuccess(Object obj) {
                                    Bitmap bitmap = (Bitmap) obj;
                                    if (!jjbQypPegg.this.mau || !bitmap.sameAs(jjbQypPegg.this.unity.attribution(jjbQypPegg.this.organic))) {
                                        ImageView imageView = (ImageView) weakReference.get();
                                        if (!jjbQypPegg.attribution(weakReference, jjbQypPegg.this.organic)) {
                                            return;
                                        }
                                        if (jjbQypPegg.this.mau) {
                                            imageView.setImageBitmap(bitmap);
                                            return;
                                        }
                                        im.getsocial.sdk.internal.g.a.jjbQypPegg unused = jjbQypPegg.this.dau;
                                        Animation loadAnimation = AnimationUtils.loadAnimation(imageView.getContext(), 17432576);
                                        loadAnimation.setDuration(200);
                                        imageView.setImageBitmap(bitmap);
                                        imageView.startAnimation(loadAnimation);
                                    }
                                }

                                public void onFailure(GetSocialException getSocialException) {
                                    ImageView imageView = (ImageView) weakReference.get();
                                    if (jjbQypPegg.attribution(weakReference, jjbQypPegg.this.organic)) {
                                        cjrhisSQCL cjrhissqcl = jjbQypPegg.getsocial;
                                        cjrhissqcl.attribution("Failed to load " + URLDecoder.decode(jjbQypPegg.this.organic) + " into " + imageView);
                                        jjbQypPegg.attribution(weakReference);
                                    }
                                }
                            });
                        }
                    }
                }, Strategy.TTL_SECONDS_DEFAULT);
                return;
            }
            getsocial(new Callback<Bitmap>() {
                public /* synthetic */ void onSuccess(Object obj) {
                    Bitmap bitmap = (Bitmap) obj;
                    ImageView imageView = (ImageView) weakReference.get();
                    if (jjbQypPegg.attribution(weakReference, jjbQypPegg.this.organic)) {
                        imageView.setImageBitmap(bitmap);
                    }
                }

                public void onFailure(GetSocialException getSocialException) {
                    ImageView imageView = (ImageView) weakReference.get();
                    if (jjbQypPegg.attribution(weakReference, jjbQypPegg.this.organic)) {
                        cjrhisSQCL cjrhissqcl = jjbQypPegg.getsocial;
                        cjrhissqcl.attribution("Failed to load " + jjbQypPegg.this.organic + " into " + imageView);
                        jjbQypPegg.attribution(weakReference);
                        imageView.setImageDrawable(drawable);
                    }
                }
            });
        }
    }

    public final void attribution(ImageView imageView) {
        getsocial(imageView, (Drawable) null);
    }

    /* access modifiers changed from: private */
    public static boolean acquisition() {
        return mobile != null;
    }

    /* access modifiers changed from: private */
    public void getsocial(Bitmap bitmap) {
        try {
            final Bitmap bitmap2 = bitmap;
            boolean z = true;
            for (pdwpUtZXDT pdwputzxdt : this.viral) {
                bitmap2 = pdwputzxdt.getsocial(bitmap2);
                if (!z || !acquisition()) {
                    bitmap.recycle();
                } else {
                    z = false;
                }
            }
            getsocial(new Runnable() {
                public void run() {
                    jjbQypPegg.this.ios.onSuccess(bitmap2);
                    Callback unused = jjbQypPegg.this.ios = null;
                }
            });
        } catch (Exception e) {
            getsocial(e);
        } catch (OutOfMemoryError e2) {
            mobile.getsocial();
            getsocial(new GetSocialException(103, e2.getMessage()));
        }
    }

    /* access modifiers changed from: private */
    public void getsocial(final Exception exc) {
        getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.ios.onFailure(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(exc));
                Callback unused = jjbQypPegg.this.ios = null;
            }
        });
    }

    private static void getsocial(Runnable runnable) {
        retention.getsocial(runnable);
    }

    /* access modifiers changed from: private */
    public static void attribution(WeakReference<ImageView> weakReference) {
        ImageView imageView = weakReference.get();
        if (imageView != null) {
            Iterator<Map.Entry<WeakReference<ImageView>, String>> it = attribution.entrySet().iterator();
            while (it.hasNext()) {
                ImageView imageView2 = (ImageView) ((WeakReference) it.next().getKey()).get();
                if (imageView2 == null) {
                    it.remove();
                } else if (getsocial(imageView2, imageView)) {
                    it.remove();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean attribution(WeakReference<ImageView> weakReference, String str) {
        ImageView imageView = weakReference.get();
        if (imageView == null) {
            return false;
        }
        Iterator<Map.Entry<WeakReference<ImageView>, String>> it = attribution.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            ImageView imageView2 = (ImageView) ((WeakReference) next.getKey()).get();
            String str2 = (String) next.getValue();
            if (imageView2 == null) {
                it.remove();
            } else if (getsocial(imageView2, imageView) && str.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ boolean getsocial(jjbQypPegg jjbqyppegg, Bitmap bitmap) {
        return (jjbqyppegg.cat == null || bitmap.getWidth() == jjbqyppegg.cat.getsocial() || bitmap.getHeight() == jjbqyppegg.cat.attribution()) ? false : true;
    }
}
