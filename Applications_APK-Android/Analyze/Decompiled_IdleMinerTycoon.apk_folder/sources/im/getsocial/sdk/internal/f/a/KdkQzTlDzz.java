package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;
import java.util.Map;

/* compiled from: THProcessAppOpenRequest */
public final class KdkQzTlDzz {
    public Map<KluUZYuxme, Map<fOrCGNYyfk, String>> acquisition;
    public Boolean attribution;
    public wWemqSpYTx getsocial;
    public Map<String, String> mobile;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof KdkQzTlDzz)) {
            return false;
        }
        KdkQzTlDzz kdkQzTlDzz = (KdkQzTlDzz) obj;
        return (this.getsocial == kdkQzTlDzz.getsocial || (this.getsocial != null && this.getsocial.equals(kdkQzTlDzz.getsocial))) && (this.attribution == kdkQzTlDzz.attribution || (this.attribution != null && this.attribution.equals(kdkQzTlDzz.attribution))) && ((this.acquisition == kdkQzTlDzz.acquisition || (this.acquisition != null && this.acquisition.equals(kdkQzTlDzz.acquisition))) && (this.mobile == kdkQzTlDzz.mobile || (this.mobile != null && this.mobile.equals(kdkQzTlDzz.mobile))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) * -2128831035) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035;
        if (this.mobile != null) {
            i = this.mobile.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THProcessAppOpenRequest{fingerprint=" + this.getsocial + ", referrer=" + ((String) null) + ", deepLinkUrl=" + ((String) null) + ", isNewInstall=" + this.attribution + ", referrers=" + ((Object) null) + ", referrerData=" + this.acquisition + ", deviceInfo=" + this.mobile + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, KdkQzTlDzz kdkQzTlDzz) {
        if (kdkQzTlDzz.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 12);
            wWemqSpYTx.getsocial(zotoebnojf, kdkQzTlDzz.getsocial);
        }
        if (kdkQzTlDzz.attribution != null) {
            zotoebnojf.getsocial(4, (byte) 2);
            zotoebnojf.getsocial(kdkQzTlDzz.attribution.booleanValue());
        }
        if (kdkQzTlDzz.acquisition != null) {
            zotoebnojf.getsocial(6, (byte) 13);
            zotoebnojf.getsocial((byte) 8, (byte) 13, kdkQzTlDzz.acquisition.size());
            for (Map.Entry next : kdkQzTlDzz.acquisition.entrySet()) {
                Map map = (Map) next.getValue();
                zotoebnojf.getsocial(((KluUZYuxme) next.getKey()).value);
                zotoebnojf.getsocial((byte) 8, (byte) 11, map.size());
                for (Map.Entry entry : map.entrySet()) {
                    zotoebnojf.getsocial(((fOrCGNYyfk) entry.getKey()).value);
                    zotoebnojf.getsocial((String) entry.getValue());
                }
            }
        }
        if (kdkQzTlDzz.mobile != null) {
            zotoebnojf.getsocial(7, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, kdkQzTlDzz.mobile.size());
            for (Map.Entry next2 : kdkQzTlDzz.mobile.entrySet()) {
                zotoebnojf.getsocial((String) next2.getKey());
                zotoebnojf.getsocial((String) next2.getValue());
            }
        }
        zotoebnojf.getsocial();
    }
}
