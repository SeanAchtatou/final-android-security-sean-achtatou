package im.getsocial.sdk.internal.f.a;

/* compiled from: THInviteType */
public enum DvmrLquonW {
    SMART_INVITE(1),
    MARKETING_CAMPAIGN(2);
    
    public final int value;

    private DvmrLquonW(int i) {
        this.value = i;
    }

    public static DvmrLquonW findByValue(int i) {
        switch (i) {
            case 1:
                return SMART_INVITE;
            case 2:
                return MARKETING_CAMPAIGN;
            default:
                return null;
        }
    }
}
