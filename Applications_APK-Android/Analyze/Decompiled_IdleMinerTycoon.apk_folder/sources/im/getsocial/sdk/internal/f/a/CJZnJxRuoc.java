package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THNotificationTemplateMedia */
public final class CJZnJxRuoc {
    public String acquisition;
    public String attribution;
    public String getsocial;
    public String mobile;
    public String retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof CJZnJxRuoc)) {
            return false;
        }
        CJZnJxRuoc cJZnJxRuoc = (CJZnJxRuoc) obj;
        return (this.getsocial == cJZnJxRuoc.getsocial || (this.getsocial != null && this.getsocial.equals(cJZnJxRuoc.getsocial))) && (this.attribution == cJZnJxRuoc.attribution || (this.attribution != null && this.attribution.equals(cJZnJxRuoc.attribution))) && ((this.acquisition == cJZnJxRuoc.acquisition || (this.acquisition != null && this.acquisition.equals(cJZnJxRuoc.acquisition))) && ((this.mobile == cJZnJxRuoc.mobile || (this.mobile != null && this.mobile.equals(cJZnJxRuoc.mobile))) && (this.retention == cJZnJxRuoc.retention || (this.retention != null && this.retention.equals(cJZnJxRuoc.retention)))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035;
        if (this.retention != null) {
            i = this.retention.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THNotificationTemplateMedia{image=" + this.getsocial + ", video=" + this.attribution + ", backgroundImage=" + this.acquisition + ", titleColor=" + this.mobile + ", textColor=" + this.retention + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, CJZnJxRuoc cJZnJxRuoc) {
        if (cJZnJxRuoc.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(cJZnJxRuoc.getsocial);
        }
        if (cJZnJxRuoc.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(cJZnJxRuoc.attribution);
        }
        if (cJZnJxRuoc.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(cJZnJxRuoc.acquisition);
        }
        if (cJZnJxRuoc.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(cJZnJxRuoc.mobile);
        }
        if (cJZnJxRuoc.retention != null) {
            zotoebnojf.getsocial(5, (byte) 11);
            zotoebnojf.getsocial(cJZnJxRuoc.retention);
        }
        zotoebnojf.getsocial();
    }

    public static CJZnJxRuoc getsocial(zoToeBNOjF zotoebnojf) {
        CJZnJxRuoc cJZnJxRuoc = new CJZnJxRuoc();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cJZnJxRuoc.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cJZnJxRuoc.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cJZnJxRuoc.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cJZnJxRuoc.mobile = zotoebnojf.ios();
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cJZnJxRuoc.retention = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return cJZnJxRuoc;
            }
        }
    }
}
