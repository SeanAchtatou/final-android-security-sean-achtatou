package im.getsocial.sdk.internal.h;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import com.android.vending.billing.IInAppBillingService;
import com.unity.purchasing.googleplay.IabHelper;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: PurchaseDataCollectorBillingService */
public class pdwpUtZXDT implements jjbQypPegg {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL acquisition = upgqDBbsrL.getsocial(pdwpUtZXDT.class);
    private static final BigDecimal attribution = new BigDecimal(1000000);
    private final upgqDBbsrL cat;
    private final Map<String, im.getsocial.a.a.pdwpUtZXDT> dau;
    @XdbacJlTDQ
    Context getsocial;
    /* access modifiers changed from: private */
    public boolean mau;
    /* access modifiers changed from: private */
    public IInAppBillingService mobile;
    /* access modifiers changed from: private */
    public boolean retention = false;

    /* compiled from: PurchaseDataCollectorBillingService */
    interface jjbQypPegg {
        void attribution();

        void getsocial();
    }

    public final void getsocial() {
    }

    public pdwpUtZXDT(upgqDBbsrL upgqdbbsrl) {
        ztWNWCuZiM.getsocial(this);
        this.cat = upgqdbbsrl;
        this.dau = new HashMap();
    }

    public static boolean acquisition() {
        try {
            Class.forName("com.android.vending.billing.IInAppBillingService");
            return true;
        } catch (Throwable th) {
            acquisition.attribution("Failed to find Billing Service class.");
            acquisition.getsocial(th);
            return false;
        }
    }

    private void getsocial(List<String> list, String str) {
        if (list != null) {
            for (String str2 : list) {
                im.getsocial.a.a.pdwpUtZXDT pdwputzxdt = (im.getsocial.a.a.pdwpUtZXDT) new im.getsocial.a.a.a.cjrhisSQCL().getsocial(str2, (im.getsocial.a.a.a.jjbQypPegg) null);
                this.cat.getsocial(((String) pdwputzxdt.get("productId")).getBytes("UTF-8"), ((String) pdwputzxdt.get("purchaseToken")).getBytes("UTF-8"), str, true);
            }
        }
    }

    private void getsocial(List<String> list, List<PurchaseData.Builder> list2, String str, String str2) {
        try {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(IabHelper.GET_SKU_DETAILS_ITEM_LIST, new ArrayList(list));
            Bundle skuDetails = this.mobile.getSkuDetails(3, this.getsocial.getPackageName(), str, bundle);
            int i = skuDetails.getInt("RESPONSE_CODE");
            cjrhisSQCL cjrhissqcl = acquisition;
            cjrhissqcl.attribution("Getting product details response: " + i);
            if (i == 0) {
                getsocial(skuDetails.getStringArrayList(IabHelper.RESPONSE_GET_SKU_DETAILS_LIST));
            }
            for (PurchaseData.Builder next : list2) {
                im.getsocial.a.a.pdwpUtZXDT pdwputzxdt = this.dau.get(next.getProductId());
                String str3 = (String) pdwputzxdt.get("price_currency_code");
                String str4 = (String) pdwputzxdt.get("title");
                Long l = (Long) pdwputzxdt.get("price_amount_micros");
                float f = 0.0f;
                if (l != null) {
                    f = new BigDecimal(l.longValue()).divide(attribution).floatValue();
                }
                this.cat.getsocial(next.withPrice(f).withProductTitle(str4).withPriceCurrency(str3).withProductType("subs".equalsIgnoreCase(str) ? PurchaseData.ProductType.SUBSCRIPTION : PurchaseData.ProductType.ITEM).build(), str, str2);
            }
        } catch (Throwable th) {
            cjrhisSQCL cjrhissqcl2 = acquisition;
            cjrhissqcl2.attribution("Error while getting product details: " + th.getMessage());
            acquisition.getsocial(th);
        }
    }

    private void getsocial(List<String> list) {
        if (list != null) {
            for (String str : list) {
                im.getsocial.a.a.pdwpUtZXDT pdwputzxdt = (im.getsocial.a.a.pdwpUtZXDT) new im.getsocial.a.a.a.cjrhisSQCL().getsocial(str, (im.getsocial.a.a.a.jjbQypPegg) null);
                this.dau.put((String) pdwputzxdt.get("productId"), pdwputzxdt);
            }
        }
    }

    public final void getsocial(List<PurchaseData.Builder> list, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        for (PurchaseData.Builder next : list) {
            if (!this.dau.containsKey(next.getProductId())) {
                arrayList.add(next.getProductId());
            }
        }
        getsocial(arrayList, list, str, str2);
    }

    public final void attribution() {
        if (!this.mau) {
            this.mau = true;
            acquisition.attribution("Checking purchase history");
            final AnonymousClass1 r1 = new jjbQypPegg() {
                public final void getsocial() {
                    new Thread(new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, boolean):boolean
                         arg types: [im.getsocial.sdk.internal.h.pdwpUtZXDT, int]
                         candidates:
                          im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, com.android.vending.billing.IInAppBillingService):com.android.vending.billing.IInAppBillingService
                          im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, java.lang.String):void
                          im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(java.util.List<java.lang.String>, java.lang.String):void
                          im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, boolean):boolean */
                        public void run() {
                            pdwpUtZXDT.getsocial(pdwpUtZXDT.this, "inapp");
                            pdwpUtZXDT.getsocial(pdwpUtZXDT.this, "subs");
                            boolean unused = pdwpUtZXDT.this.mau = false;
                        }
                    }).start();
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, boolean):boolean
                 arg types: [im.getsocial.sdk.internal.h.pdwpUtZXDT, int]
                 candidates:
                  im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, com.android.vending.billing.IInAppBillingService):com.android.vending.billing.IInAppBillingService
                  im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, java.lang.String):void
                  im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(java.util.List<java.lang.String>, java.lang.String):void
                  im.getsocial.sdk.internal.h.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.h.pdwpUtZXDT, boolean):boolean */
                public final void attribution() {
                    pdwpUtZXDT.acquisition.attribution("Could not connect to billing service");
                    boolean unused = pdwpUtZXDT.this.mau = false;
                }
            };
            if (this.retention) {
                r1.getsocial();
                return;
            }
            AnonymousClass2 r2 = new ServiceConnection() {
                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                    try {
                        pdwpUtZXDT.acquisition.attribution("Billing service connected");
                        boolean unused = pdwpUtZXDT.this.retention = true;
                        IInAppBillingService unused2 = pdwpUtZXDT.this.mobile = IInAppBillingService.Stub.asInterface(iBinder);
                        r1.getsocial();
                    } catch (Throwable th) {
                        pdwpUtZXDT.acquisition.getsocial(th);
                    }
                }

                public void onServiceDisconnected(ComponentName componentName) {
                    pdwpUtZXDT.acquisition.attribution("Billing service disconnected");
                    boolean unused = pdwpUtZXDT.this.retention = false;
                    r1.attribution();
                }
            };
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            this.getsocial.bindService(intent, r2, 1);
        }
    }

    static /* synthetic */ void getsocial(pdwpUtZXDT pdwputzxdt, String str) {
        cjrhisSQCL cjrhissqcl = acquisition;
        cjrhissqcl.attribution("Reading purchase history, product type: " + str);
        try {
            Bundle purchaseHistory = pdwputzxdt.mobile.getPurchaseHistory(6, pdwputzxdt.getsocial.getPackageName(), str, null, Bundle.EMPTY);
            if (purchaseHistory.getInt("RESPONSE_CODE") == 0) {
                pdwputzxdt.getsocial(purchaseHistory.getStringArrayList(IabHelper.RESPONSE_INAPP_PURCHASE_DATA_LIST), str);
            }
        } catch (Throwable th) {
            cjrhisSQCL cjrhissqcl2 = acquisition;
            cjrhissqcl2.attribution("Error while getting purchase history: " + th);
            acquisition.getsocial(th);
        }
    }
}
