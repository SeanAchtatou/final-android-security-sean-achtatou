package im.getsocial.sdk.internal.a.b;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AnalyticsEvent */
public final class jjbQypPegg implements Serializable {
    private long acquisition;
    private final Map<String, String> attribution;
    private long cat;
    private final boolean dau;
    private final String getsocial;
    private C0013jjbQypPegg mau;
    private final String mobile;
    private long retention;

    /* renamed from: im.getsocial.sdk.internal.a.b.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: AnalyticsEvent */
    public enum C0013jjbQypPegg {
        SERVER_TIME,
        DEVICE_UPTIME,
        LOCAL_TIME,
        PENDING
    }

    private jjbQypPegg(String str, Map<String, String> map, long j, String str2, long j2, boolean z, C0013jjbQypPegg jjbqyppegg) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Can not create AnalyticsEvent with null name");
        this.getsocial = str;
        this.attribution = map == null ? new HashMap<>() : map;
        this.acquisition = j;
        this.mobile = str2;
        this.retention = j2;
        this.dau = z;
        this.mau = jjbqyppegg;
    }

    public static jjbQypPegg getsocial(String str, Map<String, String> map, long j, String str2, long j2, boolean z, C0013jjbQypPegg jjbqyppegg) {
        return new jjbQypPegg(str, map, j, str2, j2, z, jjbqyppegg);
    }

    public static jjbQypPegg getsocial(String str, Map<String, String> map, long j, String str2, boolean z, C0013jjbQypPegg jjbqyppegg) {
        return new jjbQypPegg(str, map, j, str2, 0, z, jjbqyppegg);
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final Map<String, String> attribution() {
        return this.attribution;
    }

    public final long acquisition() {
        return this.acquisition;
    }

    public final String mobile() {
        return this.mobile;
    }

    public final long retention() {
        return this.retention;
    }

    public final void dau() {
        this.retention++;
    }

    public final boolean mau() {
        return this.dau;
    }

    public final C0013jjbQypPegg cat() {
        return this.mau;
    }

    public final long viral() {
        return this.cat;
    }

    public final void getsocial(long j) {
        this.cat = j;
    }

    public final void attribution(long j) {
        this.acquisition = j;
    }

    public final void getsocial(C0013jjbQypPegg jjbqyppegg) {
        this.mau = jjbqyppegg;
    }

    public final String toString() {
        return "AnalyticsEvent{_name='" + this.getsocial + '\'' + ", _properties=" + this.attribution + ", _timestamp=" + this.acquisition + ", _uniqueId=" + this.mobile + ", _retryCount=" + this.retention + ", _customEvent=" + this.dau + ", _eventTimeType=" + this.mau + '}';
    }
}
