package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.ArrayList;
import java.util.List;

/* compiled from: THReceipt */
public final class icjTFWWVFN {
    public String attribution;
    public List<ZWjsSaCmFq> getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof icjTFWWVFN)) {
            return false;
        }
        icjTFWWVFN icjtfwwvfn = (icjTFWWVFN) obj;
        return (this.getsocial == icjtfwwvfn.getsocial || (this.getsocial != null && this.getsocial.equals(icjtfwwvfn.getsocial))) && (this.attribution == icjtfwwvfn.attribution || (this.attribution != null && this.attribution.equals(icjtfwwvfn.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THReceipt{items=" + this.getsocial + ", checkpoint=" + this.attribution + "}";
    }

    public static icjTFWWVFN getsocial(zoToeBNOjF zotoebnojf) {
        icjTFWWVFN icjtfwwvfn = new icjTFWWVFN();
        while (true) {
            upgqDBbsrL acquisition = zotoebnojf.acquisition();
            if (acquisition.attribution != 0) {
                switch (acquisition.acquisition) {
                    case 1:
                        if (acquisition.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention.attribution);
                            for (int i = 0; i < retention.attribution; i++) {
                                arrayList.add(ZWjsSaCmFq.getsocial(zotoebnojf));
                            }
                            icjtfwwvfn.getsocial = arrayList;
                            break;
                        }
                    case 2:
                        if (acquisition.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            icjtfwwvfn.attribution = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                        break;
                }
            } else {
                return icjtfwwvfn;
            }
        }
    }
}
