package im.getsocial.sdk.internal.c.b.a;

import im.getsocial.sdk.internal.c.b.KSZKMmRWhZ;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;

/* compiled from: JavaInstanceLoader */
public final class upgqDBbsrL implements jjbQypPegg {
    private static final cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);

    private static <T> T getsocial(Constructor<?> constructor, Object... objArr) {
        boolean isAccessible = constructor.isAccessible();
        constructor.setAccessible(true);
        try {
            Object newInstance = constructor.newInstance(objArr);
            constructor.setAccessible(isAccessible);
            return newInstance;
        } catch (Exception e) {
            getsocial.getsocial(e);
            throw new RuntimeException("Can not instantiate [" + constructor.getDeclaringClass() + "] :" + e);
        } catch (Throwable th) {
            constructor.setAccessible(isAccessible);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
     arg types: [java.lang.Class<?>, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object */
    private static Object[] getsocial(Constructor<?> constructor, pdwpUtZXDT pdwputzxdt) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
        Object[] objArr = new Object[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            KSZKMmRWhZ kSZKMmRWhZ = null;
            for (Annotation annotation : parameterAnnotations[i]) {
                if (annotation instanceof KSZKMmRWhZ) {
                    kSZKMmRWhZ = (KSZKMmRWhZ) annotation;
                }
            }
            objArr[i] = pdwputzxdt.getsocial((Class) parameterTypes[i], kSZKMmRWhZ);
        }
        return objArr;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [java.lang.Class, java.lang.Class<T>] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T> java.lang.reflect.Constructor<?> getsocial(java.lang.Class<T> r1) {
        /*
            r0 = 0
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x0008 }
            java.lang.reflect.Constructor r1 = r1.getConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x0008 }
            return r1
        L_0x0008:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: im.getsocial.sdk.internal.c.b.a.upgqDBbsrL.getsocial(java.lang.Class):java.lang.reflect.Constructor");
    }

    public final <T> T getsocial(Class<T> cls, pdwpUtZXDT pdwputzxdt) {
        Constructor<?> constructor = null;
        for (Constructor<?> constructor2 : cls.getDeclaredConstructors()) {
            if (constructor2.getAnnotation(XdbacJlTDQ.class) != null) {
                if (constructor == null) {
                    constructor = constructor2;
                } else {
                    throw new RuntimeException("Can't instantiate [" + cls + "]. It must not have multiple injectable constructors");
                }
            }
        }
        if (constructor == null) {
            constructor = getsocial(cls);
        }
        if (constructor != null) {
            return getsocial(constructor, getsocial(constructor, pdwputzxdt));
        }
        throw new RuntimeException("Can't instantiate [" + cls + "]. It must have exactly one injectable constructor.");
    }
}
