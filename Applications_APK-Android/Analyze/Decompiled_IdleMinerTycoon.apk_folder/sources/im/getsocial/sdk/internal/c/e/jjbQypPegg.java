package im.getsocial.sdk.internal.c.e;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.KCGqEGAizh;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.h.cjrhisSQCL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: SdkInitializer */
public final class jjbQypPegg {
    private static final ScheduledExecutorService attribution = Executors.newSingleThreadScheduledExecutor();
    /* access modifiers changed from: private */
    public static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(jjbQypPegg.class);
    private final im.getsocial.sdk.internal.c.h.jjbQypPegg acquisition;
    private final im.getsocial.sdk.internal.c.h.cjrhisSQCL mobile;
    private final KCGqEGAizh retention;

    /* renamed from: im.getsocial.sdk.internal.c.e.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: SdkInitializer */
    public interface C0017jjbQypPegg {
        void getsocial(CompletionCallback completionCallback);
    }

    @XdbacJlTDQ
    jjbQypPegg(im.getsocial.sdk.internal.c.h.jjbQypPegg jjbqyppegg, im.getsocial.sdk.internal.c.h.cjrhisSQCL cjrhissqcl, KCGqEGAizh kCGqEGAizh) {
        this.acquisition = jjbqyppegg;
        this.mobile = cjrhissqcl;
        this.retention = kCGqEGAizh;
    }

    public final void getsocial(final C0017jjbQypPegg jjbqyppegg) {
        if (this.retention.getsocial()) {
            return;
        }
        if (this.acquisition.getsocial()) {
            attribution(jjbqyppegg);
            return;
        }
        this.mobile.getsocial(new cjrhisSQCL.jjbQypPegg() {
            public final void getsocial(boolean z) {
                if (z) {
                    jjbQypPegg.getsocial(jjbQypPegg.this, this);
                    jjbQypPegg.this.attribution(jjbqyppegg);
                }
            }
        });
        this.mobile.getsocial();
    }

    /* access modifiers changed from: private */
    public void attribution(final C0017jjbQypPegg jjbqyppegg) {
        if (!this.retention.getsocial()) {
            jjbqyppegg.getsocial(new CompletionCallback() {
                public void onSuccess() {
                    jjbQypPegg.getsocial.attribution("GetSocial initialization successful");
                }

                public void onFailure(GetSocialException getSocialException) {
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = jjbQypPegg.getsocial;
                    cjrhissqcl.retention("GetSocial initialization failed, exception:\n" + getSocialException);
                    if (210 != getSocialException.getErrorCode()) {
                        jjbQypPegg.getsocial.attribution("Initialization failed, retry in 10 seconds.");
                        jjbQypPegg.attribution.schedule(new Runnable(jjbqyppegg) {
                            public void run() {
                                jjbQypPegg.this.getsocial(r5);
                            }
                        }, 10, TimeUnit.SECONDS);
                    }
                }
            });
        }
    }

    static /* synthetic */ void getsocial(jjbQypPegg jjbqyppegg, cjrhisSQCL.jjbQypPegg jjbqyppegg2) {
        jjbqyppegg.mobile.attribution();
        jjbqyppegg.mobile.attribution(jjbqyppegg2);
    }
}
