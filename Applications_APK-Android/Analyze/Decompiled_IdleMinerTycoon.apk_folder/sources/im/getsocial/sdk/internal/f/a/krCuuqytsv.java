package im.getsocial.sdk.internal.f.a;

/* compiled from: THReportingReason */
public enum krCuuqytsv {
    SPAM(0),
    INAPPROPRIATE_CONTENT(1);
    
    public final int value;

    private krCuuqytsv(int i) {
        this.value = i;
    }

    public static krCuuqytsv findByValue(int i) {
        switch (i) {
            case 0:
                return SPAM;
            case 1:
                return INAPPROPRIATE_CONTENT;
            default:
                return null;
        }
    }
}
