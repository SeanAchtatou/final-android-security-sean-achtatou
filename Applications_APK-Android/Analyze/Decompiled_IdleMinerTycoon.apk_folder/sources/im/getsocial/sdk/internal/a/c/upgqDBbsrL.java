package im.getsocial.sdk.internal.a.c;

import im.getsocial.a.a.pdwpUtZXDT;
import im.getsocial.sdk.internal.a.b.jjbQypPegg;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: AnalyticsQueueComponentImpl */
public class upgqDBbsrL implements jjbQypPegg {
    private static final long acquisition = TimeUnit.DAYS.toMillis(7);
    private static final cjrhisSQCL attribution = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);
    private static final ScheduledExecutorService getsocial = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture dau;
    /* access modifiers changed from: private */
    public final qdyNCsqjKt mobile;
    private final List<jjbQypPegg> retention;

    @XdbacJlTDQ
    upgqDBbsrL(qdyNCsqjKt qdyncsqjkt) {
        Object obj;
        this.mobile = qdyncsqjkt;
        if (this.mobile.getsocial("analytics_queue")) {
            obj = getsocial(this.mobile.attribution("analytics_queue"));
        } else {
            obj = new ArrayList();
        }
        this.retention = new CopyOnWriteArrayList((Collection) obj);
    }

    public final void getsocial(jjbQypPegg jjbqyppegg) {
        this.retention.add(jjbqyppegg);
        mobile();
    }

    public final void getsocial(List<jjbQypPegg> list) {
        cjrhisSQCL cjrhissqcl = attribution;
        cjrhissqcl.attribution("Queue analytics events: " + list);
        this.retention.addAll(list);
        mobile();
    }

    public final List<jjbQypPegg> getsocial() {
        return new ArrayList(this.retention);
    }

    public final void attribution() {
        attribution.attribution("Clear queue");
        this.retention.clear();
        mobile();
    }

    private void mobile() {
        if (this.dau != null) {
            this.dau.cancel(false);
        }
        this.dau = getsocial.schedule(new Runnable() {
            public void run() {
                upgqDBbsrL.this.mobile.getsocial("analytics_queue", upgqDBbsrL.this.acquisition());
            }
        }, 100, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: package-private */
    public final String acquisition() {
        pdwpUtZXDT pdwputzxdt = new pdwpUtZXDT();
        pdwputzxdt.put("queue_userid", retention());
        im.getsocial.a.a.upgqDBbsrL upgqdbbsrl = new im.getsocial.a.a.upgqDBbsrL();
        for (jjbQypPegg next : this.retention) {
            if (!"sdk_log".equals(next.getsocial())) {
                long acquisition2 = next.acquisition();
                String str = next.getsocial();
                if (System.currentTimeMillis() - acquisition2 <= acquisition || str.equals("inapp_purchase")) {
                    pdwpUtZXDT pdwputzxdt2 = new pdwpUtZXDT();
                    pdwputzxdt2.put("name", str);
                    pdwputzxdt2.put("timestamp", Long.valueOf(acquisition2));
                    pdwputzxdt2.put("unique_id", next.mobile());
                    pdwputzxdt2.put("retry_count", Long.valueOf(next.retention()));
                    pdwputzxdt2.put("custom_event", Boolean.valueOf(next.mau()));
                    pdwputzxdt2.put("properties", new pdwpUtZXDT(next.attribution()));
                    pdwputzxdt2.put("time_type", next.cat().name());
                    pdwputzxdt2.put("system_uptime", Long.valueOf(next.viral()));
                    upgqdbbsrl.add(pdwputzxdt2);
                }
            }
        }
        pdwputzxdt.put("queue", upgqdbbsrl);
        return pdwpUtZXDT.getsocial(pdwputzxdt);
    }

    private List<jjbQypPegg> getsocial(String str) {
        im.getsocial.a.a.upgqDBbsrL upgqdbbsrl;
        ArrayList arrayList = new ArrayList();
        try {
            Object obj = new im.getsocial.a.a.a.cjrhisSQCL().getsocial(str, (im.getsocial.a.a.a.jjbQypPegg) null);
            String str2 = "";
            if (obj instanceof im.getsocial.a.a.upgqDBbsrL) {
                str2 = EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR;
                upgqdbbsrl = (im.getsocial.a.a.upgqDBbsrL) obj;
            } else {
                String str3 = ((pdwpUtZXDT) obj).containsKey("queue_userid") ? (String) ((pdwpUtZXDT) obj).get("queue_userid") : null;
                if (str3 == null || retention().equals(str3)) {
                    upgqdbbsrl = (im.getsocial.a.a.upgqDBbsrL) ((pdwpUtZXDT) obj).get("queue");
                } else {
                    attribution.attribution("Persisted analytics queue belongs to a different user, deleting it");
                    this.mobile.retention("analytics_queue");
                    return arrayList;
                }
            }
            Iterator it = upgqdbbsrl.iterator();
            while (it.hasNext()) {
                pdwpUtZXDT pdwputzxdt = (pdwpUtZXDT) it.next();
                Map map = pdwputzxdt.containsKey("properties") ? (Map) getsocial(pdwputzxdt, "properties", str2) : null;
                String str4 = pdwputzxdt.containsKey("unique_id") ? (String) getsocial(pdwputzxdt, "unique_id", str2) : null;
                if (str4 == null) {
                    str4 = UUID.randomUUID().toString();
                }
                String str5 = str4;
                long longValue = pdwputzxdt.containsKey("retry_count") ? ((Long) getsocial(pdwputzxdt, "retry_count", str2)).longValue() : 0;
                jjbQypPegg.C0013jjbQypPegg jjbqyppegg = jjbQypPegg.C0013jjbQypPegg.SERVER_TIME;
                String str6 = (String) getsocial(pdwputzxdt, "time_type", str2);
                if (str6 != null) {
                    jjbqyppegg = jjbQypPegg.C0013jjbQypPegg.valueOf(str6);
                }
                jjbQypPegg.C0013jjbQypPegg jjbqyppegg2 = jjbqyppegg;
                Long l = (Long) getsocial(pdwputzxdt, "system_uptime", str2);
                if (l == null) {
                    l = 0L;
                }
                jjbQypPegg jjbqyppegg3 = jjbQypPegg.getsocial((String) getsocial(pdwputzxdt, "name", str2), map, ((Long) getsocial(pdwputzxdt, "timestamp", str2)).longValue(), str5, longValue, pdwputzxdt.containsKey("custom_event") ? ((Boolean) getsocial(pdwputzxdt, "custom_event", str2)).booleanValue() : false, jjbqyppegg2);
                jjbqyppegg3.getsocial(l.longValue());
                arrayList.add(jjbqyppegg3);
            }
        } catch (Exception e) {
            attribution.retention("Persisted analytics queue uses old format, deleting it, error: ");
            attribution.acquisition(e);
            this.mobile.retention("analytics_queue");
        }
        return arrayList;
    }

    private String retention() {
        return this.mobile.attribution("user_id");
    }

    private static Object getsocial(pdwpUtZXDT pdwputzxdt, String str, String str2) {
        return pdwputzxdt.get(str2 + str);
    }
}
