package im.getsocial.sdk.internal.j;

import im.getsocial.sdk.consts.LanguageCodes;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: LocalizationUtils */
public final class upgqDBbsrL {
    private static final Map<String, List<String>> attribution;
    static final String[] getsocial = {LanguageCodes.BENGALI, LanguageCodes.BHOJPURI, LanguageCodes.CHINESE_SIMPLIFIED, LanguageCodes.CHINESE_TRADITIONAL, LanguageCodes.DANISH, LanguageCodes.DUTCH, "en", LanguageCodes.FRENCH, LanguageCodes.GERMAN, LanguageCodes.GUJARATI, LanguageCodes.HINDI, LanguageCodes.ICELANDIC, "id", LanguageCodes.ITALIAN, LanguageCodes.JAPANESE, LanguageCodes.KANNADA, LanguageCodes.KOREAN, LanguageCodes.LATIN_AMERICAN_SPANISH, LanguageCodes.MALAY, LanguageCodes.MALAYALAM, LanguageCodes.MARATHI, LanguageCodes.NORWEGIAN, LanguageCodes.POLISH, LanguageCodes.PORTUGUESE, LanguageCodes.PORTUGUESE_BRAZILLIAN, LanguageCodes.PUNJABI, LanguageCodes.RUSSIAN, LanguageCodes.SPANISH, LanguageCodes.SWEDISH, LanguageCodes.TAGALOG, LanguageCodes.TAMIL, LanguageCodes.TELUGU, LanguageCodes.THAI, LanguageCodes.TURKISH, LanguageCodes.UKRAINIAN, LanguageCodes.VIETNAMESE};

    static {
        HashMap hashMap = new HashMap();
        attribution = hashMap;
        hashMap.put(LanguageCodes.CHINESE_SIMPLIFIED, Arrays.asList("zh", "zh-CN", "zh-SG"));
        attribution.put(LanguageCodes.CHINESE_TRADITIONAL, Arrays.asList("zh-TW", "zh-HK"));
    }

    private upgqDBbsrL() {
    }

    public static boolean getsocial(String str) {
        return Arrays.asList(getsocial).contains(str);
    }

    public static String attribution(String str) {
        for (Map.Entry next : attribution.entrySet()) {
            if (((List) next.getValue()).contains(str)) {
                return (String) next.getKey();
            }
        }
        return null;
    }
}
