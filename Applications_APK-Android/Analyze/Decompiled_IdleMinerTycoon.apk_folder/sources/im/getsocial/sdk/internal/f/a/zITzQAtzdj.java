package im.getsocial.sdk.internal.f.a;

/* compiled from: THReportedActivityPost */
public final class zITzQAtzdj {
    public final int hashCode() {
        return 1176649135;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && (obj instanceof zITzQAtzdj);
    }

    public final String toString() {
        return "THReportedActivityPost{activity=" + ((Object) null) + ", reportsCount=" + ((Object) null) + ", reportStatus=" + ((Object) null) + ", authorBanExpiry=" + ((Object) null) + ", deletedAt=" + ((Object) null) + "}";
    }
}
