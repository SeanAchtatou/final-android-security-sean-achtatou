package im.getsocial.sdk.internal.c.j.a;

import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.a.a.jjbQypPegg;
import im.getsocial.sdk.internal.f.a.KCGqEGAizh;
import im.getsocial.sdk.internal.f.a.rWfbqYooCV;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: GetSocialThriftyExceptionAdapter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static GetSocialException getsocial(KCGqEGAizh kCGqEGAizh) {
        List<rWfbqYooCV> list = kCGqEGAizh.getsocial;
        ArrayList arrayList = new ArrayList();
        Iterator<rWfbqYooCV> it = list.iterator();
        while (true) {
            int i = 0;
            if (!it.hasNext()) {
                return new im.getsocial.sdk.internal.c.a.a.jjbQypPegg(((jjbQypPegg.C0015jjbQypPegg) arrayList.get(0)).attribution(), ((jjbQypPegg.C0015jjbQypPegg) arrayList.get(0)).getsocial(), arrayList);
            }
            rWfbqYooCV next = it.next();
            switch (next.getsocial) {
                case EMUnauthorized:
                    i = ErrorCode.ILLEGAL_STATE;
                    break;
                case EMResourceAlreadyExists:
                case IdentityAlreadyExists:
                    i = 101;
                    break;
                case InvalidSession:
                    i = ErrorCode.SDK_NOT_INITIALIZED;
                    break;
                case SIErrProcessAppOpenNoMatch:
                    i = 102;
                    break;
                case PlatformNotEnabled:
                    i = ErrorCode.PLATFORM_DISABLED;
                    break;
                case AppSignatureMismatch:
                    i = ErrorCode.APP_SIGNATURE_MISMATCH;
                    break;
                case MissingFields:
                case EMFieldCannotBeNull:
                case InvalidUserOrPassword:
                case EMFieldHasInvalidLength:
                case EMInvalidProperties:
                case EMInvalidEnumGiven:
                case EMFieldMismatch:
                case EMNotFound:
                case AFOlderXorNewer:
                case AFInvalidNewer:
                case AFInvalidOlder:
                case AFInvalidImageUrl:
                case AFInvalidLanguage:
                    i = 204;
                    break;
                case AFActivityNotFound:
                case AFAuthorActivityNotFound:
                    i = ErrorCode.NOT_FOUND;
                    break;
                case AFBanForbidden:
                    i = ErrorCode.USER_IS_BANNED;
                    break;
            }
            arrayList.add(new jjbQypPegg.C0015jjbQypPegg(i, next.attribution, next.acquisition));
        }
    }
}
