package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;
import java.util.Map;

/* compiled from: THCreateTokenRequest */
public final class iFpupLCESp {
    public Map<String, String> attribution;
    public String getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof iFpupLCESp)) {
            return false;
        }
        iFpupLCESp ifpuplcesp = (iFpupLCESp) obj;
        return (this.getsocial == ifpuplcesp.getsocial || (this.getsocial != null && this.getsocial.equals(ifpuplcesp.getsocial))) && (this.attribution == ifpuplcesp.attribution || (this.attribution != null && this.attribution.equals(ifpuplcesp.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035 * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THCreateTokenRequest{providerId=" + this.getsocial + ", type=" + ((Object) null) + ", linkParams=" + this.attribution + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, iFpupLCESp ifpuplcesp) {
        if (ifpuplcesp.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(ifpuplcesp.getsocial);
        }
        if (ifpuplcesp.attribution != null) {
            zotoebnojf.getsocial(3, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, ifpuplcesp.attribution.size());
            for (Map.Entry next : ifpuplcesp.attribution.entrySet()) {
                zotoebnojf.getsocial((String) next.getKey());
                zotoebnojf.getsocial((String) next.getValue());
            }
        }
        zotoebnojf.getsocial();
    }
}
