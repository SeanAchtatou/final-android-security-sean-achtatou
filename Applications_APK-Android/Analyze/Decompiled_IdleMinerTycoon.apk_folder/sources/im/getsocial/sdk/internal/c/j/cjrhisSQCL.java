package im.getsocial.sdk.internal.c.j;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: HadesConfigurationProviderImpl */
public class cjrhisSQCL implements upgqDBbsrL {
    private static Map<jjbQypPegg, jjbQypPegg> attribution;
    @XdbacJlTDQ
    qdyNCsqjKt getsocial;

    public cjrhisSQCL() {
        ztWNWCuZiM.getsocial(this);
    }

    private static Map<jjbQypPegg, jjbQypPegg> attribution() {
        Map<jjbQypPegg, jjbQypPegg> map;
        synchronized (jjbQypPegg.class) {
            if (attribution == null) {
                HashMap hashMap = new HashMap();
                hashMap.put(jjbQypPegg.PRODUCTION, new jjbQypPegg("hades.getsocial.im/sdk", true));
                hashMap.put(jjbQypPegg.TESTING_SSL, new jjbQypPegg("hades.testing.getsocial.im/sdk", true));
                hashMap.put(jjbQypPegg.TESTING, new jjbQypPegg("hades.testing.getsocial.im/sdk", false));
                attribution = Collections.unmodifiableMap(hashMap);
            }
            map = attribution;
        }
        return map;
    }

    public final jjbQypPegg getsocial() {
        return attribution().get(jjbQypPegg.findByValue(this.getsocial.getsocial("hades_configuration") ? this.getsocial.mobile("hades_configuration") : 0));
    }

    /* compiled from: HadesConfigurationProviderImpl */
    public enum jjbQypPegg {
        PRODUCTION(0),
        TESTING_SSL(1),
        TESTING(2);
        
        private final int _value;

        private jjbQypPegg(int i) {
            this._value = i;
        }

        public static jjbQypPegg findByValue(int i) {
            switch (i) {
                case 0:
                    return PRODUCTION;
                case 1:
                    return TESTING_SSL;
                case 2:
                    return TESTING;
                default:
                    return PRODUCTION;
            }
        }

        public final int getValue() {
            return this._value;
        }
    }
}
