package im.getsocial.sdk.internal.c.d.a;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;

/* compiled from: CompletionCallbackAdapter */
class pdwpUtZXDT implements jjbQypPegg.C0016jjbQypPegg {
    private final CompletionCallback getsocial;

    pdwpUtZXDT(CompletionCallback completionCallback) {
        this.getsocial = completionCallback;
    }

    public final void getsocial(GetSocialException getSocialException) {
        if (this.getsocial != null) {
            this.getsocial.onFailure(getSocialException);
        }
    }
}
