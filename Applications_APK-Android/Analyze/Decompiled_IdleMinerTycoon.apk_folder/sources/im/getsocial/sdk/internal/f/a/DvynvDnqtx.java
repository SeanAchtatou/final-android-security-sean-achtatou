package im.getsocial.sdk.internal.f.a;

/* compiled from: THListNotificationsTextsQuery */
public final class DvynvDnqtx {
    public final int hashCode() {
        return 1174645723;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && (obj instanceof DvynvDnqtx);
    }

    public final String toString() {
        return "THListNotificationsTextsQuery{messageType=" + ((Object) null) + ", language=" + ((String) null) + "}";
    }
}
