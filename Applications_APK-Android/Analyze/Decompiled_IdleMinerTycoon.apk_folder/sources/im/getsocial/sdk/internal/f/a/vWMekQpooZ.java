package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THSdkAuthRequestAllInOne */
public final class vWMekQpooZ {
    public KdkQzTlDzz attribution;
    public RbduBRVrSj getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof vWMekQpooZ)) {
            return false;
        }
        vWMekQpooZ vwmekqpooz = (vWMekQpooZ) obj;
        return (this.getsocial == vwmekqpooz.getsocial || (this.getsocial != null && this.getsocial.equals(vwmekqpooz.getsocial))) && (this.attribution == vwmekqpooz.attribution || (this.attribution != null && this.attribution.equals(vwmekqpooz.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THSdkAuthRequestAllInOne{sdkAuthRequest=" + this.getsocial + ", processAppOpenRequest=" + this.attribution + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, vWMekQpooZ vwmekqpooz) {
        if (vwmekqpooz.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 12);
            RbduBRVrSj.getsocial(zotoebnojf, vwmekqpooz.getsocial);
        }
        if (vwmekqpooz.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 12);
            KdkQzTlDzz.getsocial(zotoebnojf, vwmekqpooz.attribution);
        }
        zotoebnojf.getsocial();
    }
}
