package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THUserReference */
public final class gnmRLOtbDV {
    public String acquisition;
    public String attribution;
    public String getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof gnmRLOtbDV)) {
            return false;
        }
        gnmRLOtbDV gnmrlotbdv = (gnmRLOtbDV) obj;
        return (this.getsocial == gnmrlotbdv.getsocial || (this.getsocial != null && this.getsocial.equals(gnmrlotbdv.getsocial))) && (this.attribution == gnmrlotbdv.attribution || (this.attribution != null && this.attribution.equals(gnmrlotbdv.attribution))) && (this.acquisition == gnmrlotbdv.acquisition || (this.acquisition != null && this.acquisition.equals(gnmrlotbdv.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THUserReference{id=" + this.getsocial + ", displayName=" + this.attribution + ", avatarUrl=" + this.acquisition + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, gnmRLOtbDV gnmrlotbdv) {
        if (gnmrlotbdv.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(gnmrlotbdv.getsocial);
        }
        if (gnmrlotbdv.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(gnmrlotbdv.attribution);
        }
        if (gnmrlotbdv.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(gnmrlotbdv.acquisition);
        }
        zotoebnojf.getsocial();
    }

    public static gnmRLOtbDV getsocial(zoToeBNOjF zotoebnojf) {
        gnmRLOtbDV gnmrlotbdv = new gnmRLOtbDV();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            gnmrlotbdv.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            gnmrlotbdv.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            gnmrlotbdv.acquisition = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return gnmrlotbdv;
            }
        }
    }
}
