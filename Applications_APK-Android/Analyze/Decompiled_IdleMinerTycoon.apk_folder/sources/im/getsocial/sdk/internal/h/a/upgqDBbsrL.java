package im.getsocial.sdk.internal.h.a;

import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PurchaseHistoryResponseListenerProxy */
public abstract class upgqDBbsrL implements PurchaseHistoryResponseListener {
    /* access modifiers changed from: protected */
    public abstract void getsocial(XdbacJlTDQ xdbacJlTDQ, List<cjrhisSQCL> list);

    public void onPurchaseHistoryResponse(BillingResult billingResult, List<PurchaseHistoryRecord> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (PurchaseHistoryRecord cjrhissqcl : list) {
                arrayList.add(new cjrhisSQCL(cjrhissqcl));
            }
        }
        getsocial(new XdbacJlTDQ(billingResult), arrayList);
    }

    public void onPurchaseHistoryResponse(int i, List<Purchase> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (Purchase cjrhissqcl : list) {
                arrayList.add(new cjrhisSQCL(cjrhissqcl));
            }
        }
        getsocial(new XdbacJlTDQ(i), arrayList);
    }
}
