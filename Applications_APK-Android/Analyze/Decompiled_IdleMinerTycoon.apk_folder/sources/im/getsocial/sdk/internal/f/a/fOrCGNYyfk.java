package im.getsocial.sdk.internal.f.a;

/* compiled from: THAppOpenKey */
public enum fOrCGNYyfk {
    Url(0),
    InstallBeginTimestamp(1),
    ReferrerClickTimestamp(2),
    Value(3);
    
    public final int value;

    private fOrCGNYyfk(int i) {
        this.value = i;
    }

    public static fOrCGNYyfk findByValue(int i) {
        switch (i) {
            case 0:
                return Url;
            case 1:
                return InstallBeginTimestamp;
            case 2:
                return ReferrerClickTimestamp;
            case 3:
                return Value;
            default:
                return null;
        }
    }
}
