package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;
import java.util.List;

/* compiled from: THNotificationsQuery */
public final class YgeTlQwUNa {
    public String acquisition;
    public String attribution;
    public List<String> dau;
    public Integer getsocial;
    public List<String> mobile;
    public List<String> retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof YgeTlQwUNa)) {
            return false;
        }
        YgeTlQwUNa ygeTlQwUNa = (YgeTlQwUNa) obj;
        return (this.getsocial == ygeTlQwUNa.getsocial || (this.getsocial != null && this.getsocial.equals(ygeTlQwUNa.getsocial))) && (this.attribution == ygeTlQwUNa.attribution || (this.attribution != null && this.attribution.equals(ygeTlQwUNa.attribution))) && ((this.acquisition == ygeTlQwUNa.acquisition || (this.acquisition != null && this.acquisition.equals(ygeTlQwUNa.acquisition))) && ((this.mobile == ygeTlQwUNa.mobile || (this.mobile != null && this.mobile.equals(ygeTlQwUNa.mobile))) && ((this.retention == ygeTlQwUNa.retention || (this.retention != null && this.retention.equals(ygeTlQwUNa.retention))) && (this.dau == ygeTlQwUNa.dau || (this.dau != null && this.dau.equals(ygeTlQwUNa.dau))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) * -2128831035) * -2128831035) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035;
        if (this.dau != null) {
            i = this.dau.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THNotificationsQuery{limit=" + this.getsocial + ", newer=" + this.attribution + ", older=" + this.acquisition + ", type=" + ((Object) null) + ", isRead=" + ((Object) null) + ", appId=" + ((String) null) + ", types=" + this.mobile + ", statuses=" + this.retention + ", actions=" + this.dau + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, YgeTlQwUNa ygeTlQwUNa) {
        if (ygeTlQwUNa.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 8);
            zotoebnojf.getsocial(ygeTlQwUNa.getsocial.intValue());
        }
        if (ygeTlQwUNa.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(ygeTlQwUNa.attribution);
        }
        if (ygeTlQwUNa.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(ygeTlQwUNa.acquisition);
        }
        if (ygeTlQwUNa.mobile != null) {
            zotoebnojf.getsocial(7, (byte) 15);
            zotoebnojf.getsocial((byte) 11, ygeTlQwUNa.mobile.size());
            for (String str : ygeTlQwUNa.mobile) {
                zotoebnojf.getsocial(str);
            }
        }
        if (ygeTlQwUNa.retention != null) {
            zotoebnojf.getsocial(8, (byte) 15);
            zotoebnojf.getsocial((byte) 11, ygeTlQwUNa.retention.size());
            for (String str2 : ygeTlQwUNa.retention) {
                zotoebnojf.getsocial(str2);
            }
        }
        if (ygeTlQwUNa.dau != null) {
            zotoebnojf.getsocial(9, (byte) 15);
            zotoebnojf.getsocial((byte) 11, ygeTlQwUNa.dau.size());
            for (String str3 : ygeTlQwUNa.dau) {
                zotoebnojf.getsocial(str3);
            }
        }
        zotoebnojf.getsocial();
    }
}
