package im.getsocial.sdk.internal.k.a;

import java.util.HashMap;
import java.util.Map;

/* compiled from: MediaAttachmentInternal */
public final class jjbQypPegg {
    private final upgqDBbsrL attribution;
    private final Map<cjrhisSQCL, String> getsocial;

    private jjbQypPegg(cjrhisSQCL cjrhissqcl, String str) {
        this(null);
        this.getsocial.put(cjrhissqcl, str);
    }

    private jjbQypPegg(upgqDBbsrL upgqdbbsrl) {
        this.getsocial = new HashMap();
        this.attribution = upgqdbbsrl;
    }

    public static jjbQypPegg getsocial(byte[] bArr) {
        return new jjbQypPegg(upgqDBbsrL.getsocial(bArr));
    }

    public static jjbQypPegg attribution(byte[] bArr) {
        return new jjbQypPegg(upgqDBbsrL.attribution(bArr));
    }

    public static jjbQypPegg getsocial(String str) {
        return new jjbQypPegg(cjrhisSQCL.IMAGE, str);
    }

    public static jjbQypPegg attribution(String str) {
        return new jjbQypPegg(cjrhisSQCL.VIDEO, str);
    }

    public final String getsocial(cjrhisSQCL cjrhissqcl) {
        return this.getsocial.get(cjrhissqcl);
    }

    public final void getsocial(cjrhisSQCL cjrhissqcl, String str) {
        this.getsocial.put(cjrhissqcl, str);
    }

    public final upgqDBbsrL getsocial() {
        return this.attribution;
    }
}
