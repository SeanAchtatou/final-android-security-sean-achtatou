package im.getsocial.sdk.internal.c;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.stats.CodePackage;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.onesignal.OneSignalDbContract;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: AndroidPushRegistrator */
public class XdbacJlTDQ implements im.getsocial.sdk.pushnotifications.a.a.jjbQypPegg {
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(XdbacJlTDQ.class);
    private final im.getsocial.sdk.internal.c.d.jjbQypPegg acquisition;
    /* access modifiers changed from: private */
    public final im.getsocial.sdk.internal.upgqDBbsrL attribution;
    private final Context mobile;
    private final qdyNCsqjKt retention;

    /* compiled from: AndroidPushRegistrator */
    private interface pdwpUtZXDT {
        String getsocial();
    }

    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    XdbacJlTDQ(im.getsocial.sdk.internal.upgqDBbsrL upgqdbbsrl, im.getsocial.sdk.internal.c.d.jjbQypPegg jjbqyppegg, Context context, qdyNCsqjKt qdyncsqjkt) {
        this.attribution = upgqdbbsrl;
        this.acquisition = jjbqyppegg;
        this.mobile = context;
        this.retention = qdyncsqjkt;
    }

    public final void getsocial() {
        new Thread(new Runnable() {
            public void run() {
                XdbacJlTDQ.getsocial(XdbacJlTDQ.this);
            }
        }).start();
    }

    public final void getsocial(List<String> list) {
        NotificationManager notificationManager = (NotificationManager) this.mobile.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME);
        if (notificationManager != null) {
            for (String next : list) {
                qdyNCsqjKt qdyncsqjkt = this.retention;
                int mobile2 = qdyncsqjkt.mobile("push_notification_id_" + next);
                if (mobile2 != 0) {
                    notificationManager.cancel(mobile2);
                    this.retention.retention(next);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static String acquisition() {
        List<pdwpUtZXDT> asList = Arrays.asList(new upgqDBbsrL(), new cjrhisSQCL());
        ArrayList<Throwable> arrayList = new ArrayList<>();
        for (pdwpUtZXDT pdwputzxdt : asList) {
            try {
                return pdwputzxdt.getsocial();
            } catch (Throwable th) {
                arrayList.add(th);
            }
        }
        getsocial.attribution("Failed to obtain push token, exceptions: ");
        for (Throwable th2 : arrayList) {
            getsocial.getsocial(th2);
        }
        return null;
    }

    /* compiled from: AndroidPushRegistrator */
    private static final class upgqDBbsrL implements pdwpUtZXDT {
        @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
        im.getsocial.sdk.pushnotifications.a.d.jjbQypPegg getsocial;

        @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
        upgqDBbsrL() {
            ztWNWCuZiM.getsocial(this);
        }

        @SuppressLint({"MissingFirebaseInstanceTokenRefresh"})
        public final String getsocial() {
            return FirebaseInstanceId.getInstance().getToken(this.getsocial.getsocial().getsocial(), FirebaseMessaging.INSTANCE_ID_SCOPE);
        }
    }

    /* compiled from: AndroidPushRegistrator */
    private static final class cjrhisSQCL implements pdwpUtZXDT {
        @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
        im.getsocial.sdk.pushnotifications.a.d.jjbQypPegg attribution;
        @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
        Context getsocial;

        @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
        cjrhisSQCL() {
            ztWNWCuZiM.getsocial(this);
        }

        public final String getsocial() {
            return InstanceID.getInstance(this.getsocial).getToken(this.attribution.getsocial().getsocial(), CodePackage.GCM);
        }
    }

    /* compiled from: AndroidPushRegistrator */
    private static abstract class jjbQypPegg<V> implements Callable<V> {
        private final int acquisition;
        private final float attribution;
        private final long getsocial;

        public abstract boolean getsocial(V v);

        jjbQypPegg(long j, float f, int i) {
            this.getsocial = j;
            this.attribution = f;
            this.acquisition = i;
        }

        public final V attribution(V v) {
            for (int i = 0; i < this.acquisition; i++) {
                V v2 = getsocial();
                if (getsocial(v2)) {
                    return v2;
                }
                try {
                    double d = (double) this.getsocial;
                    double pow = Math.pow((double) this.attribution, (double) i);
                    Double.isNaN(d);
                    Thread.sleep((long) (d * pow));
                } catch (InterruptedException unused) {
                }
            }
            return null;
        }

        private V getsocial() {
            try {
                return call();
            } catch (Exception unused) {
                return null;
            }
        }
    }

    static /* synthetic */ void getsocial(XdbacJlTDQ xdbacJlTDQ) {
        final String str = (String) new jjbQypPegg<String>(1000, 2.0f, 3) {
            public final /* bridge */ /* synthetic */ boolean getsocial(Object obj) {
                return !im.getsocial.sdk.internal.c.l.ztWNWCuZiM.getsocial((String) obj);
            }

            public /* synthetic */ Object call() {
                return XdbacJlTDQ.acquisition();
            }
        }.attribution(null);
        if (str == null) {
            getsocial.mobile("Failed to obtain push token");
            return;
        }
        final AnonymousClass2 r1 = new CompletionCallback() {
            public void onSuccess() {
                XdbacJlTDQ.getsocial.attribution("Successfully register for a push notifications.");
            }

            public void onFailure(GetSocialException getSocialException) {
                XdbacJlTDQ.getsocial.attribution("Failed to register for a push notifications. Error: %s", getSocialException.getLocalizedMessage());
            }
        };
        xdbacJlTDQ.acquisition.getsocial(new Runnable() {
            public void run() {
                im.getsocial.sdk.internal.cjrhisSQCL.getsocial(XdbacJlTDQ.this.attribution, str, r1);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(r1));
    }
}
