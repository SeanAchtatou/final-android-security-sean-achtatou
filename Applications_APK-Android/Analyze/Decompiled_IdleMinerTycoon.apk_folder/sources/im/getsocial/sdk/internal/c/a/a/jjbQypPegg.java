package im.getsocial.sdk.internal.c.a.a;

import im.getsocial.sdk.GetSocialException;
import java.util.List;
import java.util.Map;

/* compiled from: GetSocialApiException */
public class jjbQypPegg extends GetSocialException {
    private final List<C0015jjbQypPegg> getsocial;

    public jjbQypPegg(int i, String str, List<C0015jjbQypPegg> list) {
        super(i, str);
        this.getsocial = list;
    }

    public final boolean getsocial(int i) {
        for (C0015jjbQypPegg attribution : this.getsocial) {
            if (attribution.attribution() == i) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GetSocialApiException{ _errorCode={ ");
        sb.append(getErrorCode());
        sb.append(" },_errorMessage={ ");
        sb.append(getMessage());
        sb.append(" },_apiErrors={ ");
        for (C0015jjbQypPegg append : this.getsocial) {
            sb.append(append);
        }
        sb.append(" }}");
        return sb.toString();
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.getsocial == null ? 0 : this.getsocial.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        jjbQypPegg jjbqyppegg = (jjbQypPegg) obj;
        if (this.getsocial == null) {
            return jjbqyppegg.getsocial == null;
        }
        return this.getsocial.equals(jjbqyppegg.getsocial);
    }

    /* renamed from: im.getsocial.sdk.internal.c.a.a.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: GetSocialApiException */
    public static class C0015jjbQypPegg {
        private final Map<String, String> acquisition;
        private final String attribution;
        private final int getsocial;

        public C0015jjbQypPegg(int i, String str, Map<String, String> map) {
            this.getsocial = i;
            this.attribution = str;
            this.acquisition = map;
        }

        public String toString() {
            return "ApiError{_errorCode=" + this.getsocial + ", _message='" + this.attribution + '\'' + ", _context=" + this.acquisition + '}';
        }

        public final String getsocial() {
            return this.attribution;
        }

        public final int attribution() {
            return this.getsocial;
        }
    }
}
