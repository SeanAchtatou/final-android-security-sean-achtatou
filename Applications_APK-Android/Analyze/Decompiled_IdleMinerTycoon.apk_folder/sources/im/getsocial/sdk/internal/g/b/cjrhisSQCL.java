package im.getsocial.sdk.internal.g.b;

import android.graphics.Bitmap;
import android.util.LruCache;

/* compiled from: InMemoryCache */
public class cjrhisSQCL implements jjbQypPegg {
    private LruCache<String, Bitmap> getsocial = new LruCache<String, Bitmap>(((int) (((float) Runtime.getRuntime().maxMemory()) / 1024.0f)) / 8) {
        /* access modifiers changed from: protected */
        public /* synthetic */ int sizeOf(Object obj, Object obj2) {
            return ((Bitmap) obj2).getByteCount() / 1024;
        }
    };

    public final void getsocial() {
        this.getsocial.evictAll();
    }

    public final boolean getsocial(String str) {
        return this.getsocial.get(str) != null;
    }

    public final Bitmap attribution(String str) {
        return this.getsocial.get(str);
    }

    public final void getsocial(String str, Bitmap bitmap) {
        this.getsocial.put(str, bitmap);
    }
}
