package im.getsocial.sdk.internal.i.c;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.zoToeBNOjF;

/* compiled from: ValidateAppIdFunc */
public final class jjbQypPegg {
    private final upgqDBbsrL attribution = upgqDBbsrL.getsocial(this.getsocial);
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.l.pdwpUtZXDT getsocial;

    jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    private zoToeBNOjF attribution(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), this.attribution.mobile());
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(str), this.attribution.retention());
        try {
            return new zoToeBNOjF(str);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage() + "\n" + this.attribution.dau());
        }
    }

    /* compiled from: ValidateAppIdFunc */
    private static abstract class upgqDBbsrL {
        /* access modifiers changed from: protected */
        public abstract String acquisition();

        /* access modifiers changed from: protected */
        public abstract String attribution();

        /* access modifiers changed from: protected */
        public abstract String getsocial();

        private upgqDBbsrL() {
        }

        /* synthetic */ upgqDBbsrL(byte b) {
            this();
        }

        static upgqDBbsrL getsocial(im.getsocial.sdk.internal.c.l.pdwpUtZXDT pdwputzxdt) {
            switch (pdwputzxdt) {
                case ANDROID:
                    return new C0023jjbQypPegg((byte) 0);
                case IOS:
                    return new pdwpUtZXDT((byte) 0);
                default:
                    return new cjrhisSQCL((byte) 0);
            }
        }

        /* access modifiers changed from: package-private */
        public final String mobile() {
            return attribution() + "\n" + acquisition() + "\nCheck the documentation for more information.";
        }

        /* access modifiers changed from: package-private */
        public final String retention() {
            return "Your APP_ID is empty. " + dau();
        }

        /* access modifiers changed from: private */
        public String dau() {
            return getsocial() + "\n" + acquisition() + "\nCheck the documentation for more information.";
        }
    }

    /* renamed from: im.getsocial.sdk.internal.i.c.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: ValidateAppIdFunc */
    private static final class C0023jjbQypPegg extends upgqDBbsrL {
        /* access modifiers changed from: protected */
        public final String acquisition() {
            return "<meta-data\n\t\tandroid:name=\"im.getsocial.sdk.AppId\"\n\t\tandroid:value=\"YOUR_APP_ID\" />";
        }

        /* access modifiers changed from: protected */
        public final String attribution() {
            return "You need to add <meta-data> tag into your Application tag in AndroidManifest.xml:";
        }

        /* access modifiers changed from: protected */
        public final String getsocial() {
            return "Check that you have set up <meta-data> tag correctly in your AndroidManifest.xml:";
        }

        private C0023jjbQypPegg() {
            super((byte) 0);
        }

        /* synthetic */ C0023jjbQypPegg(byte b) {
            this();
        }
    }

    /* compiled from: ValidateAppIdFunc */
    private static final class pdwpUtZXDT extends upgqDBbsrL {
        /* access modifiers changed from: protected */
        public final String acquisition() {
            return "\t<key>im.getsocial.sdk.AppId</key>\n\t<string>YOUR_APP_ID</string>";
        }

        /* access modifiers changed from: protected */
        public final String attribution() {
            return "You need to add AppId key to application Info.plist file:";
        }

        /* access modifiers changed from: protected */
        public final String getsocial() {
            return "Check you have correctly setup AppId key in your application Info.plist file:";
        }

        private pdwpUtZXDT() {
            super((byte) 0);
        }

        /* synthetic */ pdwpUtZXDT(byte b) {
            this();
        }
    }

    /* compiled from: ValidateAppIdFunc */
    private static final class cjrhisSQCL extends upgqDBbsrL {
        /* access modifiers changed from: protected */
        public final String acquisition() {
            return "";
        }

        /* access modifiers changed from: protected */
        public final String attribution() {
            return "AppId is null. ";
        }

        /* access modifiers changed from: protected */
        public final String getsocial() {
            return "AppId is not valid.";
        }

        private cjrhisSQCL() {
            super((byte) 0);
        }

        /* synthetic */ cjrhisSQCL(byte b) {
            this();
        }
    }

    public static zoToeBNOjF getsocial(String str) {
        return new jjbQypPegg().attribution(str);
    }
}
