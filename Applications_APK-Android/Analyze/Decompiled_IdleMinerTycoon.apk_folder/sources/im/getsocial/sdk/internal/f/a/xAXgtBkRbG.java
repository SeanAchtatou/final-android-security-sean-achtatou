package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.HashMap;
import java.util.Map;

/* compiled from: THPromoCode */
public final class xAXgtBkRbG {
    public gnmRLOtbDV acquisition;

    /* renamed from: android  reason: collision with root package name */
    public Boolean f487android;
    public String attribution;
    public Long cat;
    public Integer dau;
    public String getsocial;
    public Boolean growth;
    public Boolean ios;
    public Integer mau;
    public String mobile;
    public Long organic;
    public Map<String, String> retention;
    public Boolean unity;
    public Long viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof xAXgtBkRbG)) {
            return false;
        }
        xAXgtBkRbG xaxgtbkrbg = (xAXgtBkRbG) obj;
        return (this.getsocial == xaxgtbkrbg.getsocial || (this.getsocial != null && this.getsocial.equals(xaxgtbkrbg.getsocial))) && (this.attribution == xaxgtbkrbg.attribution || (this.attribution != null && this.attribution.equals(xaxgtbkrbg.attribution))) && ((this.acquisition == xaxgtbkrbg.acquisition || (this.acquisition != null && this.acquisition.equals(xaxgtbkrbg.acquisition))) && ((this.mobile == xaxgtbkrbg.mobile || (this.mobile != null && this.mobile.equals(xaxgtbkrbg.mobile))) && ((this.retention == xaxgtbkrbg.retention || (this.retention != null && this.retention.equals(xaxgtbkrbg.retention))) && ((this.dau == xaxgtbkrbg.dau || (this.dau != null && this.dau.equals(xaxgtbkrbg.dau))) && ((this.mau == xaxgtbkrbg.mau || (this.mau != null && this.mau.equals(xaxgtbkrbg.mau))) && ((this.cat == xaxgtbkrbg.cat || (this.cat != null && this.cat.equals(xaxgtbkrbg.cat))) && ((this.viral == xaxgtbkrbg.viral || (this.viral != null && this.viral.equals(xaxgtbkrbg.viral))) && ((this.organic == xaxgtbkrbg.organic || (this.organic != null && this.organic.equals(xaxgtbkrbg.organic))) && ((this.growth == xaxgtbkrbg.growth || (this.growth != null && this.growth.equals(xaxgtbkrbg.growth))) && ((this.f487android == xaxgtbkrbg.f487android || (this.f487android != null && this.f487android.equals(xaxgtbkrbg.f487android))) && ((this.ios == xaxgtbkrbg.ios || (this.ios != null && this.ios.equals(xaxgtbkrbg.ios))) && (this.unity == xaxgtbkrbg.unity || (this.unity != null && this.unity.equals(xaxgtbkrbg.unity))))))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035) ^ (this.growth == null ? 0 : this.growth.hashCode())) * -2128831035) ^ (this.f487android == null ? 0 : this.f487android.hashCode())) * -2128831035) ^ (this.ios == null ? 0 : this.ios.hashCode())) * -2128831035;
        if (this.unity != null) {
            i = this.unity.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THPromoCode{id=" + this.getsocial + ", appId=" + this.attribution + ", creator=" + this.acquisition + ", code=" + this.mobile + ", customData=" + this.retention + ", maxClaims=" + this.dau + ", numClaims=" + this.mau + ", validFrom=" + this.cat + ", validUntil=" + this.viral + ", createdAt=" + this.organic + ", makeFriends=" + this.growth + ", makeReferrer=" + this.f487android + ", enabled=" + this.ios + ", claimable=" + this.unity + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, xAXgtBkRbG xaxgtbkrbg) {
        if (xaxgtbkrbg.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(xaxgtbkrbg.getsocial);
        }
        if (xaxgtbkrbg.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(xaxgtbkrbg.attribution);
        }
        if (xaxgtbkrbg.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 12);
            gnmRLOtbDV.getsocial(zotoebnojf, xaxgtbkrbg.acquisition);
        }
        if (xaxgtbkrbg.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(xaxgtbkrbg.mobile);
        }
        if (xaxgtbkrbg.retention != null) {
            zotoebnojf.getsocial(5, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, xaxgtbkrbg.retention.size());
            for (Map.Entry next : xaxgtbkrbg.retention.entrySet()) {
                zotoebnojf.getsocial((String) next.getKey());
                zotoebnojf.getsocial((String) next.getValue());
            }
        }
        if (xaxgtbkrbg.dau != null) {
            zotoebnojf.getsocial(6, (byte) 8);
            zotoebnojf.getsocial(xaxgtbkrbg.dau.intValue());
        }
        if (xaxgtbkrbg.mau != null) {
            zotoebnojf.getsocial(7, (byte) 8);
            zotoebnojf.getsocial(xaxgtbkrbg.mau.intValue());
        }
        if (xaxgtbkrbg.cat != null) {
            zotoebnojf.getsocial(8, (byte) 10);
            zotoebnojf.getsocial(xaxgtbkrbg.cat.longValue());
        }
        if (xaxgtbkrbg.viral != null) {
            zotoebnojf.getsocial(9, (byte) 10);
            zotoebnojf.getsocial(xaxgtbkrbg.viral.longValue());
        }
        if (xaxgtbkrbg.organic != null) {
            zotoebnojf.getsocial(10, (byte) 10);
            zotoebnojf.getsocial(xaxgtbkrbg.organic.longValue());
        }
        if (xaxgtbkrbg.growth != null) {
            zotoebnojf.getsocial(11, (byte) 2);
            zotoebnojf.getsocial(xaxgtbkrbg.growth.booleanValue());
        }
        if (xaxgtbkrbg.f487android != null) {
            zotoebnojf.getsocial(12, (byte) 2);
            zotoebnojf.getsocial(xaxgtbkrbg.f487android.booleanValue());
        }
        if (xaxgtbkrbg.ios != null) {
            zotoebnojf.getsocial(13, (byte) 2);
            zotoebnojf.getsocial(xaxgtbkrbg.ios.booleanValue());
        }
        if (xaxgtbkrbg.unity != null) {
            zotoebnojf.getsocial(14, (byte) 2);
            zotoebnojf.getsocial(xaxgtbkrbg.unity.booleanValue());
        }
        zotoebnojf.getsocial();
    }

    public static xAXgtBkRbG getsocial(zoToeBNOjF zotoebnojf) {
        xAXgtBkRbG xaxgtbkrbg = new xAXgtBkRbG();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.acquisition = gnmRLOtbDV.getsocial(zotoebnojf);
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.mobile = zotoebnojf.ios();
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            for (int i = 0; i < mobile2.acquisition; i++) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                            }
                            xaxgtbkrbg.retention = hashMap;
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.dau = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.mau = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.cat = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.viral = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.organic = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.growth = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 12:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.f487android = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 13:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.ios = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 14:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            xaxgtbkrbg.unity = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return xaxgtbkrbg;
            }
        }
    }
}
