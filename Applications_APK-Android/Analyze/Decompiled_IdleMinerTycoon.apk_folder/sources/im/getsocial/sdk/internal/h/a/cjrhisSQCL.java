package im.getsocial.sdk.internal.h.a;

import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;

/* compiled from: PurchaseProduct */
public final class cjrhisSQCL {
    public final String _json;
    public final String _signature;

    private cjrhisSQCL(String str, String str2) {
        this._signature = str;
        this._json = str2;
    }

    cjrhisSQCL(PurchaseHistoryRecord purchaseHistoryRecord) {
        this(purchaseHistoryRecord.getSignature(), purchaseHistoryRecord.getOriginalJson());
    }

    cjrhisSQCL(Purchase purchase) {
        this(purchase.getSignature(), purchase.getOriginalJson());
    }
}
