package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THActionButton */
public final class XdbacJlTDQ {
    public String attribution;
    public String getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof XdbacJlTDQ)) {
            return false;
        }
        XdbacJlTDQ xdbacJlTDQ = (XdbacJlTDQ) obj;
        return (this.getsocial == xdbacJlTDQ.getsocial || (this.getsocial != null && this.getsocial.equals(xdbacJlTDQ.getsocial))) && (this.attribution == xdbacJlTDQ.attribution || (this.attribution != null && this.attribution.equals(xdbacJlTDQ.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THActionButton{title=" + this.getsocial + ", actionId=" + this.attribution + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, XdbacJlTDQ xdbacJlTDQ) {
        if (xdbacJlTDQ.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(xdbacJlTDQ.getsocial);
        }
        if (xdbacJlTDQ.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(xdbacJlTDQ.attribution);
        }
        zotoebnojf.getsocial();
    }

    public static XdbacJlTDQ getsocial(zoToeBNOjF zotoebnojf) {
        XdbacJlTDQ xdbacJlTDQ = new XdbacJlTDQ();
        while (true) {
            upgqDBbsrL acquisition = zotoebnojf.acquisition();
            if (acquisition.attribution != 0) {
                switch (acquisition.acquisition) {
                    case 1:
                        if (acquisition.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            xdbacJlTDQ.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            xdbacJlTDQ.attribution = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                        break;
                }
            } else {
                return xdbacJlTDQ;
            }
        }
    }
}
