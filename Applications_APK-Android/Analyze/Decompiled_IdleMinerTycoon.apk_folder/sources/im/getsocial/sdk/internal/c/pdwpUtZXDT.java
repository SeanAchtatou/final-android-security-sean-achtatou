package im.getsocial.sdk.internal.c;

import android.content.Context;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.a.g.jjbQypPegg;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.h.upgqDBbsrL;
import java.util.List;

/* compiled from: AndroidPurchaseDataCollectorImpl */
public class pdwpUtZXDT implements IbawHMWljm, upgqDBbsrL {
    private static int cat = -1;
    /* access modifiers changed from: private */
    public static final cjrhisSQCL dau = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(pdwpUtZXDT.class);
    @XdbacJlTDQ
    qdyNCsqjKt acquisition;
    @XdbacJlTDQ
    jjbQypPegg attribution;
    @XdbacJlTDQ
    Context getsocial;
    /* access modifiers changed from: private */
    public im.getsocial.sdk.internal.h.jjbQypPegg mau;
    @XdbacJlTDQ
    im.getsocial.sdk.iap.a.d.jjbQypPegg mobile;
    @XdbacJlTDQ
    im.getsocial.sdk.iap.a.a.jjbQypPegg retention;

    public pdwpUtZXDT() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial() {
        if (mobile()) {
            this.mau.getsocial();
        }
    }

    public final void attribution() {
        if (mobile()) {
            this.mau.attribution();
        }
    }

    private boolean mobile() {
        im.getsocial.sdk.internal.h.jjbQypPegg jjbqyppegg;
        if (cat == -1) {
            cat = this.getsocial.checkCallingOrSelfPermission("com.android.vending.BILLING");
        }
        if (this.mau == null) {
            if (im.getsocial.sdk.internal.h.cjrhisSQCL.acquisition()) {
                dau.attribution("Using Billing Library to track purchases.");
                jjbqyppegg = new im.getsocial.sdk.internal.h.cjrhisSQCL(this);
            } else if (im.getsocial.sdk.internal.h.pdwpUtZXDT.acquisition()) {
                dau.attribution("Using Billing Service to track purchases.");
                jjbqyppegg = new im.getsocial.sdk.internal.h.pdwpUtZXDT(this);
            } else {
                dau.attribution("None of purchase collectors can be used.");
                jjbqyppegg = null;
            }
            this.mau = jjbqyppegg;
        }
        if (cat == -1) {
            dau.attribution("Purchase tracking is disabled because PackageManager denied permission.");
            return false;
        } else if (this.mau == null) {
            dau.attribution("Purchase tracking is disabled because there is no available tracker");
            return false;
        } else if (this.mobile.getsocial()) {
            return true;
        } else {
            dau.attribution("Purchase tracking is not enabled on Dashboard");
            return false;
        }
    }

    public final void getsocial(byte[] bArr, byte[] bArr2, final String str, boolean z) {
        im.getsocial.sdk.iap.a.a.jjbQypPegg.getsocial(im.getsocial.sdk.iap.a.b.jjbQypPegg.getsocial().getsocial(this.acquisition.attribution("subs".equalsIgnoreCase(str) ? "last_iap_checkpoint_subscription" : "last_iap_checkpoint")).getsocial(bArr).attribution(bArr2).getsocial("subs".equalsIgnoreCase(str)).attribution(z).getsocial(), new Callback<im.getsocial.sdk.iap.a.b.upgqDBbsrL>() {
            public /* synthetic */ void onSuccess(Object obj) {
                im.getsocial.sdk.iap.a.b.upgqDBbsrL upgqdbbsrl = (im.getsocial.sdk.iap.a.b.upgqDBbsrL) obj;
                List<PurchaseData.Builder> list = upgqdbbsrl.attribution;
                String str = upgqdbbsrl.getsocial;
                if (list.isEmpty()) {
                    pdwpUtZXDT.dau.attribution("No purchase to track");
                    pdwpUtZXDT.this.getsocial(str, str);
                    return;
                }
                pdwpUtZXDT.this.mau.getsocial(list, "subs".equalsIgnoreCase(str) ? "subs" : "inapp", str);
            }

            public void onFailure(GetSocialException getSocialException) {
                cjrhisSQCL acquisition = pdwpUtZXDT.dau;
                acquisition.attribution("Could not validate raw purchases, error: " + getSocialException.getMessage());
            }
        });
    }

    /* access modifiers changed from: private */
    public void getsocial(String str, String str2) {
        cjrhisSQCL cjrhissqcl = dau;
        cjrhissqcl.attribution("Updating checkpoint for product type: " + str);
        this.acquisition.getsocial("subs".equalsIgnoreCase(str) ? "last_iap_checkpoint_subscription" : "last_iap_checkpoint", str2);
    }

    public final void getsocial(PurchaseData purchaseData, String str, String str2) {
        this.attribution.attribution(purchaseData);
        getsocial(str, str2);
    }
}
