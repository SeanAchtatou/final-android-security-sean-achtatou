package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THPushTarget */
public final class BpPZzHFMaU {
    public KkSvQPDhNi acquisition;
    public String attribution;
    public String getsocial;
    public Boolean mobile;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof BpPZzHFMaU)) {
            return false;
        }
        BpPZzHFMaU bpPZzHFMaU = (BpPZzHFMaU) obj;
        return (this.getsocial == bpPZzHFMaU.getsocial || (this.getsocial != null && this.getsocial.equals(bpPZzHFMaU.getsocial))) && (this.attribution == bpPZzHFMaU.attribution || (this.attribution != null && this.attribution.equals(bpPZzHFMaU.attribution))) && ((this.acquisition == bpPZzHFMaU.acquisition || (this.acquisition != null && this.acquisition.equals(bpPZzHFMaU.acquisition))) && (this.mobile == bpPZzHFMaU.mobile || (this.mobile != null && this.mobile.equals(bpPZzHFMaU.mobile))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035;
        if (this.mobile != null) {
            i = this.mobile.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THPushTarget{token=" + this.getsocial + ", language=" + this.attribution + ", platformId=" + this.acquisition + ", iosSandbox=" + this.mobile + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, BpPZzHFMaU bpPZzHFMaU) {
        if (bpPZzHFMaU.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(bpPZzHFMaU.getsocial);
        }
        if (bpPZzHFMaU.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(bpPZzHFMaU.attribution);
        }
        if (bpPZzHFMaU.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 8);
            zotoebnojf.getsocial(bpPZzHFMaU.acquisition.value);
        }
        if (bpPZzHFMaU.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 2);
            zotoebnojf.getsocial(bpPZzHFMaU.mobile.booleanValue());
        }
        zotoebnojf.getsocial();
    }
}
