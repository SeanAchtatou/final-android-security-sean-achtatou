package im.getsocial.sdk.internal.f.a;

import com.facebook.ads.AdError;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.ironsource.mediationsdk.logger.IronSourceError;

/* compiled from: THErrorCode */
public enum QWVUXapsSm {
    UnknownError(0),
    MissingFields(1),
    InvalidSession(2),
    IdentityAlreadyExists(3),
    InvalidUserOrPassword(4),
    PasswordsDontMatch(5),
    InvalidToken(6),
    PlatformNotEnabled(7),
    AppSignatureMismatch(8),
    MissingEmailAddress(9),
    RateLimited(10),
    InvalidIAPReceipt(100),
    IAPNotSupported(101),
    EMFieldCannotBeNull(1000),
    EMFieldHasInvalidLength(1010),
    EMInvalidProperties(1020),
    EMInvalidEnumGiven(IronSourceError.ERROR_RV_LOAD_FAIL_UNEXPECTED),
    EMInvalidData(1060),
    EMOther(1099),
    EMResourceAlreadyExists(5000),
    EMNotAuthenticated(5040),
    EMFieldMismatch(5050),
    EMUnauthorized(5060),
    EMNotFound(5070),
    EMFieldMustBeUnique(5080),
    EMNeedsSDK6Migration(5090),
    EMFacebookError(5100),
    EMInvalidVATNumber(5110),
    AFOlderXorNewer(6001),
    AFInvalidNewer(6002),
    AFInvalidOlder(6003),
    AFInvalidLanguage(GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_INVALID_OPERATION),
    AFInvalidUser(6005),
    AFInvalidImageUrl(6006),
    AFNotEnoughPermissions(6007),
    AFActivityNotFound(6008),
    AFAuthorActivityNotFound(6009),
    AFRelatedActivityNotFound(6010),
    AFBanForbidden(6011),
    AFInvalidVideoUrl(6012),
    AFStickyDatesMissing(6013),
    AFStickyDatesBadRange(6014),
    AFStickyComment(6015),
    AFStickyPostsFeed(6016),
    SGInvalidParam(7001),
    SGInvalidData(7002),
    SGInvalidJson(7003),
    SGRequiredParam(7004),
    SGUnathorized(7005),
    SGNotFound(AdError.API_NOT_SUPPORTED),
    SGGraphLoop(7007),
    SGMethodNotAllowed(7008),
    SGInvalidProvider(7009),
    SGProviderError(7010),
    SIErrBadRequest(8001),
    SIErrUnknownError(8002),
    SIErrInvalidApp(8003),
    SIErrResourceNotFound(ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED),
    SIErrCampaignAlreadyExists(ConnectionsStatusCodes.API_CONNECTION_FAILED_ALREADY_IN_USE),
    SIErrMissingCampaignID(8051),
    SIErrMarketingTokenExists(8052),
    SIErrMissingMarketingLinkFields(8053),
    SIErrInvalidCampaignID(8054),
    SIErrProcessAppOpenNoMatch(8055),
    SIErrInsufficientPermissions(8056),
    SIErrInvalidToken(8057),
    SIErrPromoCodeExists(8058),
    SIErrPromoCodeExpired(8059),
    SIErrPromoCodeNotYetValid(8060),
    SIErrPromoCodeInvalid(8061),
    SIErrPromoCodeDisabled(8062),
    SIErrPromoCodeReachedClaimCount(8063),
    SIErrPromoCodeAlreadyClaimed(8064),
    SIErrPromoCodeInvalidArguments(8065),
    SIErrPromoCodeCantDelete(8066),
    IrisInvalidPlatfromCreds(GamesStatusCodes.STATUS_VIDEO_UNSUPPORTED),
    IrisIOSCertProblem(GamesStatusCodes.STATUS_VIDEO_PERMISSION_ERROR),
    IrisRegWrongPlatform(GamesStatusCodes.STATUS_VIDEO_STORAGE_ERROR),
    IrisNotValidInput(GamesStatusCodes.STATUS_VIDEO_UNEXPECTED_CAPTURE_ERROR),
    IrisIOSCertValidationProblem(9005),
    IrisIOSSandboxCertForProd(GamesStatusCodes.STATUS_VIDEO_ALREADY_CAPTURING),
    IrisIOSCertBadBundleID(9007),
    IrisTNDuplicateName(9011),
    IrisTNPayloadTooBig(9012),
    IrisTNInProgress(9013),
    IrisInvalidLanguage(9021),
    IrisDefaultLangTextIsMissing(9022),
    IrisDuplicateTemplate(9023),
    IrisReservedNameTemplate(9024),
    IrisNotificationTemplateNotFound(9025),
    IrisSenderDoesNotExist(9026),
    IrisNotificationTargetUsersEmpty(9027),
    IrisNotificationNoText(9028),
    IrisSendPNUsersLimit(9029),
    TalosRequestError(GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED),
    MercuryStripeError(110001);
    
    public final int value;

    private QWVUXapsSm(int i) {
        this.value = i;
    }

    public static QWVUXapsSm findByValue(int i) {
        switch (i) {
            case 0:
                return UnknownError;
            case 1:
                return MissingFields;
            case 2:
                return InvalidSession;
            case 3:
                return IdentityAlreadyExists;
            case 4:
                return InvalidUserOrPassword;
            case 5:
                return PasswordsDontMatch;
            case 6:
                return InvalidToken;
            case 7:
                return PlatformNotEnabled;
            case 8:
                return AppSignatureMismatch;
            case 9:
                return MissingEmailAddress;
            case 10:
                return RateLimited;
            default:
                switch (i) {
                    case 100:
                        return InvalidIAPReceipt;
                    case 101:
                        return IAPNotSupported;
                    default:
                        switch (i) {
                            case 6001:
                                return AFOlderXorNewer;
                            case 6002:
                                return AFInvalidNewer;
                            case 6003:
                                return AFInvalidOlder;
                            case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_INVALID_OPERATION:
                                return AFInvalidLanguage;
                            case 6005:
                                return AFInvalidUser;
                            case 6006:
                                return AFInvalidImageUrl;
                            case 6007:
                                return AFNotEnoughPermissions;
                            case 6008:
                                return AFActivityNotFound;
                            case 6009:
                                return AFAuthorActivityNotFound;
                            case 6010:
                                return AFRelatedActivityNotFound;
                            case 6011:
                                return AFBanForbidden;
                            case 6012:
                                return AFInvalidVideoUrl;
                            case 6013:
                                return AFStickyDatesMissing;
                            case 6014:
                                return AFStickyDatesBadRange;
                            case 6015:
                                return AFStickyComment;
                            case 6016:
                                return AFStickyPostsFeed;
                            default:
                                switch (i) {
                                    case 7001:
                                        return SGInvalidParam;
                                    case 7002:
                                        return SGInvalidData;
                                    case 7003:
                                        return SGInvalidJson;
                                    case 7004:
                                        return SGRequiredParam;
                                    case 7005:
                                        return SGUnathorized;
                                    case AdError.API_NOT_SUPPORTED:
                                        return SGNotFound;
                                    case 7007:
                                        return SGGraphLoop;
                                    case 7008:
                                        return SGMethodNotAllowed;
                                    case 7009:
                                        return SGInvalidProvider;
                                    case 7010:
                                        return SGProviderError;
                                    default:
                                        switch (i) {
                                            case 8001:
                                                return SIErrBadRequest;
                                            case 8002:
                                                return SIErrUnknownError;
                                            case 8003:
                                                return SIErrInvalidApp;
                                            case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED /*8004*/:
                                                return SIErrResourceNotFound;
                                            default:
                                                switch (i) {
                                                    case ConnectionsStatusCodes.API_CONNECTION_FAILED_ALREADY_IN_USE /*8050*/:
                                                        return SIErrCampaignAlreadyExists;
                                                    case 8051:
                                                        return SIErrMissingCampaignID;
                                                    case 8052:
                                                        return SIErrMarketingTokenExists;
                                                    case 8053:
                                                        return SIErrMissingMarketingLinkFields;
                                                    case 8054:
                                                        return SIErrInvalidCampaignID;
                                                    case 8055:
                                                        return SIErrProcessAppOpenNoMatch;
                                                    case 8056:
                                                        return SIErrInsufficientPermissions;
                                                    case 8057:
                                                        return SIErrInvalidToken;
                                                    case 8058:
                                                        return SIErrPromoCodeExists;
                                                    case 8059:
                                                        return SIErrPromoCodeExpired;
                                                    case 8060:
                                                        return SIErrPromoCodeNotYetValid;
                                                    case 8061:
                                                        return SIErrPromoCodeInvalid;
                                                    case 8062:
                                                        return SIErrPromoCodeDisabled;
                                                    case 8063:
                                                        return SIErrPromoCodeReachedClaimCount;
                                                    case 8064:
                                                        return SIErrPromoCodeAlreadyClaimed;
                                                    case 8065:
                                                        return SIErrPromoCodeInvalidArguments;
                                                    case 8066:
                                                        return SIErrPromoCodeCantDelete;
                                                    default:
                                                        switch (i) {
                                                            case GamesStatusCodes.STATUS_VIDEO_UNSUPPORTED:
                                                                return IrisInvalidPlatfromCreds;
                                                            case GamesStatusCodes.STATUS_VIDEO_PERMISSION_ERROR:
                                                                return IrisIOSCertProblem;
                                                            case GamesStatusCodes.STATUS_VIDEO_STORAGE_ERROR:
                                                                return IrisRegWrongPlatform;
                                                            case GamesStatusCodes.STATUS_VIDEO_UNEXPECTED_CAPTURE_ERROR:
                                                                return IrisNotValidInput;
                                                            case 9005:
                                                                return IrisIOSCertValidationProblem;
                                                            case GamesStatusCodes.STATUS_VIDEO_ALREADY_CAPTURING:
                                                                return IrisIOSSandboxCertForProd;
                                                            case 9007:
                                                                return IrisIOSCertBadBundleID;
                                                            default:
                                                                switch (i) {
                                                                    case 9011:
                                                                        return IrisTNDuplicateName;
                                                                    case 9012:
                                                                        return IrisTNPayloadTooBig;
                                                                    case 9013:
                                                                        return IrisTNInProgress;
                                                                    default:
                                                                        switch (i) {
                                                                            case 9021:
                                                                                return IrisInvalidLanguage;
                                                                            case 9022:
                                                                                return IrisDefaultLangTextIsMissing;
                                                                            case 9023:
                                                                                return IrisDuplicateTemplate;
                                                                            case 9024:
                                                                                return IrisReservedNameTemplate;
                                                                            case 9025:
                                                                                return IrisNotificationTemplateNotFound;
                                                                            case 9026:
                                                                                return IrisSenderDoesNotExist;
                                                                            case 9027:
                                                                                return IrisNotificationTargetUsersEmpty;
                                                                            case 9028:
                                                                                return IrisNotificationNoText;
                                                                            case 9029:
                                                                                return IrisSendPNUsersLimit;
                                                                            default:
                                                                                switch (i) {
                                                                                    case 1000:
                                                                                        return EMFieldCannotBeNull;
                                                                                    case 1010:
                                                                                        return EMFieldHasInvalidLength;
                                                                                    case 1020:
                                                                                        return EMInvalidProperties;
                                                                                    case IronSourceError.ERROR_RV_LOAD_FAIL_UNEXPECTED /*1030*/:
                                                                                        return EMInvalidEnumGiven;
                                                                                    case 1060:
                                                                                        return EMInvalidData;
                                                                                    case 1099:
                                                                                        return EMOther;
                                                                                    case 5000:
                                                                                        return EMResourceAlreadyExists;
                                                                                    case 5040:
                                                                                        return EMNotAuthenticated;
                                                                                    case 5050:
                                                                                        return EMFieldMismatch;
                                                                                    case 5060:
                                                                                        return EMUnauthorized;
                                                                                    case 5070:
                                                                                        return EMNotFound;
                                                                                    case 5080:
                                                                                        return EMFieldMustBeUnique;
                                                                                    case 5090:
                                                                                        return EMNeedsSDK6Migration;
                                                                                    case 5100:
                                                                                        return EMFacebookError;
                                                                                    case 5110:
                                                                                        return EMInvalidVATNumber;
                                                                                    case GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED:
                                                                                        return TalosRequestError;
                                                                                    case 110001:
                                                                                        return MercuryStripeError;
                                                                                    default:
                                                                                        return null;
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
    }
}
