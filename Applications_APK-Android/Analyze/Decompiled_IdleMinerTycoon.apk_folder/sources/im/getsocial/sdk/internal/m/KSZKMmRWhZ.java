package im.getsocial.sdk.internal.m;

import android.content.Context;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;

/* compiled from: DebugModeHelper */
public final class KSZKMmRWhZ {
    private KSZKMmRWhZ() {
    }

    public static void getsocial(Context context) {
        String str = EmkjBpiUfq.getsocial("debug.getsocial.sdk.app");
        if (jjbQypPegg.attribution(str) && context.getApplicationInfo().packageName.equalsIgnoreCase(str)) {
            pdwpUtZXDT.getsocial(cjrhisSQCL.jjbQypPegg.ALL);
        }
    }
}
