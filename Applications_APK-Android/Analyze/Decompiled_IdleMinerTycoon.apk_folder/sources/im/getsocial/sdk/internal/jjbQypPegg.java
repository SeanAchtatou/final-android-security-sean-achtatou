package im.getsocial.sdk.internal;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GlobalErrorListener;
import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.activities.ActivityPostContent;
import im.getsocial.sdk.activities.ReportingReason;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.b.upgqDBbsrL;
import im.getsocial.sdk.internal.c.IbawHMWljm;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.m.KluUZYuxme;
import im.getsocial.sdk.internal.m.iFpupLCESp;
import im.getsocial.sdk.internal.m.zoToeBNOjF;
import im.getsocial.sdk.invites.FetchReferralDataCallback;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelIds;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InviteContent;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.ReferredUser;
import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.e.cjrhisSQCL;
import im.getsocial.sdk.invites.a.e.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.k.KSZKMmRWhZ;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationContent;
import im.getsocial.sdk.pushnotifications.NotificationListener;
import im.getsocial.sdk.pushnotifications.NotificationsCountQuery;
import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import im.getsocial.sdk.pushnotifications.NotificationsSummary;
import im.getsocial.sdk.pushnotifications.PushTokenListener;
import im.getsocial.sdk.socialgraph.SuggestedFriend;
import im.getsocial.sdk.usermanagement.AddAuthIdentityCallback;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.OnUserChangedListener;
import im.getsocial.sdk.usermanagement.PublicUser;
import im.getsocial.sdk.usermanagement.UserReference;
import im.getsocial.sdk.usermanagement.UserUpdate;
import im.getsocial.sdk.usermanagement.UsersQuery;
import java.lang.Thread;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/* compiled from: GetSocialInternal */
public final class jjbQypPegg {
    @XdbacJlTDQ
    qdyNCsqjKt acquisition;
    @XdbacJlTDQ
    upgqDBbsrL attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.g.jjbQypPegg cat;
    @XdbacJlTDQ
    KSZKMmRWhZ dau;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.d.jjbQypPegg getsocial;
    @XdbacJlTDQ
    zoToeBNOjF mau;
    @XdbacJlTDQ
    im.getsocial.sdk.invites.a.k.jjbQypPegg mobile;
    private final Set<Integer> organic = new HashSet();
    @XdbacJlTDQ
    upgqDBbsrL retention;
    @XdbacJlTDQ
    IbawHMWljm viral;

    public jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
        if (this.getsocial == null && !new im.getsocial.sdk.internal.d.jjbQypPegg().getsocial(this)) {
            this.getsocial = new im.getsocial.sdk.internal.c.d.upgqDBbsrL();
        }
        getsocial("email", new im.getsocial.sdk.invites.a.e.upgqDBbsrL());
        getsocial(InviteChannelIds.SMS, new im.getsocial.sdk.invites.a.e.KSZKMmRWhZ());
        getsocial(InviteChannelIds.FACEBOOK_MESSENGER, new cjrhisSQCL());
        getsocial("generic", new im.getsocial.sdk.invites.a.e.XdbacJlTDQ());
        getsocial(InviteChannelIds.FACEBOOK_STORIES, new pdwpUtZXDT());
        getsocial(InviteChannelIds.INSTAGRAM_STORIES, new im.getsocial.sdk.invites.a.e.zoToeBNOjF());
    }

    public final void getsocial(final String str) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                if (!KluUZYuxme.getsocial()) {
                    jjbQypPegg.cjrhisSQCL.getsocial(jjbQypPegg.this.mobile.attribution() != -1, "GetSocial INSTALL_REFERRER receiver is not configured properly. Check the errors above for details.");
                }
                jjbQypPegg.this.attribution.getsocial(str);
            }
        });
    }

    public final void getsocial(final Runnable runnable) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(runnable), "Can not call WhenInitialize with null runnable");
                jjbQypPegg.this.attribution.getsocial((Runnable) jjbQypPegg.this.retention.getsocial(Runnable.class, runnable));
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T
     arg types: [im.getsocial.sdk.internal.jjbQypPegg$23, boolean]
     candidates:
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(im.getsocial.sdk.GetSocialException, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.lang.Runnable, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T */
    public final boolean getsocial() {
        return ((Boolean) this.getsocial.getsocial((Callable) new Callable<Boolean>() {
            public /* synthetic */ Object call() {
                return Boolean.valueOf(jjbQypPegg.this.attribution.getsocial());
            }
        }, (Object) false)).booleanValue();
    }

    public final void getsocial(final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final boolean getsocial(final GlobalErrorListener globalErrorListener) {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.getsocial.getsocial(globalErrorListener);
            }
        });
    }

    public final boolean attribution() {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.getsocial.getsocial();
            }
        });
    }

    public final String acquisition() {
        return (String) this.getsocial.getsocial(new Callable<String>() {
            public /* synthetic */ Object call() {
                String str = jjbQypPegg.this.attribution.attribution.getsocial();
                return str == null ? "en" : str;
            }
        }, "en");
    }

    public final boolean attribution(final String str) {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.attribution(str);
            }
        });
    }

    public final void getsocial(final FetchReferralDataCallback fetchReferralDataCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(fetchReferralDataCallback), "GetReferralData method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(fetchReferralDataCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(fetchReferralDataCallback));
    }

    public final void mobile() {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.acquisition();
            }
        });
    }

    public final void getsocial(final Callback<List<ReferredUser>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "GetReferredUsers method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final boolean getsocial(final String str, final InviteChannelPlugin inviteChannelPlugin) {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                upgqDBbsrL.getsocial(str, new im.getsocial.sdk.invites.a.e.ztWNWCuZiM(inviteChannelPlugin));
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T
     arg types: [im.getsocial.sdk.internal.jjbQypPegg$5, boolean]
     candidates:
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(im.getsocial.sdk.GetSocialException, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.lang.Runnable, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T */
    public final boolean acquisition(final String str) {
        return ((Boolean) this.getsocial.getsocial((Callable) new Callable<Boolean>() {
            public /* synthetic */ Object call() {
                return Boolean.valueOf(jjbQypPegg.this.attribution.acquisition(str));
            }
        }, (Object) false)).booleanValue();
    }

    public final List<InviteChannel> retention() {
        return (List) this.getsocial.getsocial(new Callable<List<InviteChannel>>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.attribution();
            }
        }, Collections.emptyList());
    }

    public final void getsocial(String str, InviteContent inviteContent, LinkParams linkParams, InviteCallback inviteCallback) {
        final InviteCallback inviteCallback2 = inviteCallback;
        final LinkParams linkParams2 = linkParams;
        final InviteContent inviteContent2 = inviteContent;
        final String str2 = str;
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                LinkParams linkParams;
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(inviteCallback2), "sendInvite method can not be called without a callback.");
                im.getsocial.sdk.invites.a.b.pdwpUtZXDT pdwputzxdt = null;
                if (linkParams2 != null) {
                    linkParams = new LinkParams(linkParams2);
                    jjbQypPegg.getsocial(jjbQypPegg.this, linkParams);
                } else {
                    linkParams = null;
                }
                if (inviteContent2 != null) {
                    im.getsocial.sdk.internal.k.a.jjbQypPegg jjbqyppegg = im.getsocial.sdk.media.jjbQypPegg.getsocial(inviteContent2.getMediaAttachment());
                    pdwpUtZXDT.jjbQypPegg jjbqyppegg2 = im.getsocial.sdk.invites.a.b.pdwpUtZXDT.getsocial().attribution(inviteContent2.getText()).getsocial(inviteContent2.getSubject());
                    if (jjbqyppegg != null) {
                        jjbqyppegg2.acquisition(jjbqyppegg.getsocial(im.getsocial.sdk.internal.k.a.cjrhisSQCL.IMAGE));
                        jjbqyppegg2.mobile(jjbqyppegg.getsocial(im.getsocial.sdk.internal.k.a.cjrhisSQCL.VIDEO));
                        im.getsocial.sdk.internal.k.a.upgqDBbsrL upgqdbbsrl = jjbqyppegg.getsocial();
                        if (upgqdbbsrl != null) {
                            if (upgqdbbsrl.attribution() == im.getsocial.sdk.internal.k.a.cjrhisSQCL.IMAGE) {
                                jjbqyppegg2.getsocial(upgqdbbsrl.getsocial());
                            } else if (upgqdbbsrl.attribution() == im.getsocial.sdk.internal.k.a.cjrhisSQCL.VIDEO) {
                                jjbqyppegg2.attribution(upgqdbbsrl.getsocial());
                            }
                        }
                    }
                    pdwputzxdt = jjbqyppegg2.getsocial();
                }
                jjbQypPegg.this.attribution.getsocial(str2, pdwputzxdt, linkParams, inviteCallback2);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(inviteCallback));
    }

    public final void getsocial(final LinkParams linkParams, final Callback<String> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                LinkParams linkParams;
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "CreateInviteLink method can not be called without a callback.");
                if (linkParams != null) {
                    linkParams = new LinkParams(linkParams);
                    jjbQypPegg.getsocial(jjbQypPegg.this, linkParams);
                } else {
                    linkParams = null;
                }
                jjbQypPegg.this.attribution.getsocial(linkParams, callback);
            }
        });
    }

    public final void getsocial(final String str, final Callback<List<ActivityPost>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "GetStickyActivities method can not be called without a callback.");
                jjbQypPegg.this.attribution.retention(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final ActivitiesQuery activitiesQuery, final Callback<List<ActivityPost>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "GetActivities method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(activitiesQuery, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void attribution(final String str, final Callback<ActivityPost> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "GetActivity method can not be called without a callback.");
                jjbQypPegg.this.attribution.dau(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final String str, final ActivityPostContent activityPostContent, final Callback<ActivityPost> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "PostActivity method can not be called without a callback.");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Feed name can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(activityPostContent), "Activity content can not be null");
                jjbQypPegg.this.attribution.getsocial(str, im.getsocial.sdk.activities.upgqDBbsrL.getsocial(activityPostContent), callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void attribution(final String str, final ActivityPostContent activityPostContent, final Callback<ActivityPost> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "PostComment method can not be called without a callback.");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Activity ID can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(activityPostContent), "Comment content can not be null");
                jjbQypPegg.this.attribution.attribution(str, im.getsocial.sdk.activities.upgqDBbsrL.getsocial(activityPostContent), callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final String str, final boolean z, final Callback<ActivityPost> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "LikeActivity method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(str, z, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(String str, int i, int i2, Callback<List<PublicUser>> callback) {
        final Callback<List<PublicUser>> callback2 = callback;
        final String str2 = str;
        final int i3 = i;
        final int i4 = i2;
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback2), "GetActivityLikers method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(str2, i3, i4, callback2);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final String str, final ReportingReason reportingReason, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(str, reportingReason, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final List<String> list, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.attribution(list, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final TagsQuery tagsQuery, final Callback<List<String>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(tagsQuery, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final boolean getsocial(final OnUserChangedListener onUserChangedListener) {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(onUserChangedListener), "OnUserChangedListener can not be null");
                jjbQypPegg.this.attribution.getsocial((OnUserChangedListener) jjbQypPegg.this.retention.getsocial(OnUserChangedListener.class, onUserChangedListener));
            }
        });
    }

    public final boolean dau() {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.mobile();
            }
        });
    }

    public final void getsocial(final UserUpdate userUpdate, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "setUserDetails method can not be called without a callback.");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(userUpdate), "setUserDetails method can not be called without UserUpdate.");
                im.getsocial.sdk.usermanagement.XdbacJlTDQ xdbacJlTDQ = new im.getsocial.sdk.usermanagement.XdbacJlTDQ(userUpdate);
                jjbQypPegg.this.attribution.getsocial(new im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT().getsocial(xdbacJlTDQ.getsocial()).attribution(xdbacJlTDQ.attribution()).getsocial(xdbacJlTDQ.acquisition()).acquisition(xdbacJlTDQ.mobile()).attribution(xdbacJlTDQ.retention()).mobile(xdbacJlTDQ.dau()).getsocial(xdbacJlTDQ.mau() != null ? im.getsocial.sdk.internal.k.a.upgqDBbsrL.getsocial(iFpupLCESp.getsocial(xdbacJlTDQ.mau())) : null), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final String str, final String str2, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Public property key can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str2), "Public property value can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "setUserDetails method can not be called without a callback.");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().setPublicProperty(str, str2).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void attribution(final String str, final String str2, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Private property key can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str2), "Private property value can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "setUserDetails method can not be called without a callback.");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().setPrivateProperty(str, str2).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final String str, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Public property key can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "setUserDetails method can not be called without a callback.");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().removePublicProperty(str).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void attribution(final String str, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Private property key can not be null");
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "setUserDetails method can not be called without a callback.");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().removePrivateProperty(str).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final String mobile(final String str) {
        return (String) this.getsocial.getsocial(new Callable<String>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.mobile(str);
            }
        }, (Object) null);
    }

    public final String retention(final String str) {
        return (String) this.getsocial.getsocial(new Callable<String>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.retention(str);
            }
        }, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T
     arg types: [im.getsocial.sdk.internal.jjbQypPegg$30, boolean]
     candidates:
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(im.getsocial.sdk.GetSocialException, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.lang.Runnable, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T */
    public final boolean dau(final String str) {
        return ((Boolean) this.getsocial.getsocial((Callable) new Callable<Boolean>() {
            public /* synthetic */ Object call() {
                return Boolean.valueOf(jjbQypPegg.this.attribution.dau(str));
            }
        }, (Object) false)).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T
     arg types: [im.getsocial.sdk.internal.jjbQypPegg$31, boolean]
     candidates:
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(im.getsocial.sdk.GetSocialException, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.lang.Runnable, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T */
    public final boolean mau(final String str) {
        return ((Boolean) this.getsocial.getsocial((Callable) new Callable<Boolean>() {
            public /* synthetic */ Object call() {
                return Boolean.valueOf(jjbQypPegg.this.attribution.mau(str));
            }
        }, (Object) false)).booleanValue();
    }

    public final Map<String, String> mau() {
        return (Map) this.getsocial.getsocial(new Callable<Map<String, String>>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.cat();
            }
        }, Collections.emptyMap());
    }

    public final Map<String, String> cat() {
        return (Map) this.getsocial.getsocial(new Callable<Map<String, String>>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.viral();
            }
        }, Collections.emptyMap());
    }

    public final String viral() {
        return (String) this.getsocial.getsocial(new Callable<String>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.retention();
            }
        }, (Object) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T
     arg types: [im.getsocial.sdk.internal.jjbQypPegg$36, boolean]
     candidates:
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(im.getsocial.sdk.GetSocialException, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.lang.Runnable, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T */
    public final boolean organic() {
        return ((Boolean) this.getsocial.getsocial((Callable) new Callable<Boolean>() {
            public /* synthetic */ Object call() {
                return Boolean.valueOf(jjbQypPegg.this.attribution.organic());
            }
        }, (Object) true)).booleanValue();
    }

    public final void getsocial(final AuthIdentity authIdentity, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "SwitchUser method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(authIdentity, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final AuthIdentity authIdentity, final AddAuthIdentityCallback addAuthIdentityCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(addAuthIdentityCallback), "AddAuthIdentity method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(authIdentity, addAuthIdentityCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(addAuthIdentityCallback));
    }

    public final void acquisition(final String str, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(completionCallback), "RemoveAuthIdentity method can not be called without a callback.");
                jjbQypPegg.this.attribution.getsocial(str, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final String growth() {
        return (String) this.getsocial.getsocial(new Callable<String>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.dau();
            }
        }, (Object) null);
    }

    public final String android() {
        return (String) this.getsocial.getsocial(new Callable<String>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.mau();
            }
        }, (Object) null);
    }

    public final void mobile(final String str, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "User display name can not be null");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().updateDisplayName(str).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void retention(final String str, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "User avatar url can not be null");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().updateAvatarUrl(str).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final Bitmap bitmap, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(bitmap), "User avatar can not be null");
                jjbQypPegg.this.getsocial(UserUpdate.createBuilder().updateAvatar(bitmap).build(), completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final Map<String, String> ios() {
        return (Map) this.getsocial.getsocial(new Callable<Map<String, String>>() {
            public /* synthetic */ Object call() {
                return jjbQypPegg.this.attribution.growth();
            }
        }, new HashMap());
    }

    public final void acquisition(final String str, final Callback<PublicUser> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final String str, final String str2, final Callback<PublicUser> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(str, str2, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final String str, final List<String> list, final Callback<Map<String, PublicUser>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(str, list, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final UsersQuery usersQuery, final Callback<List<UserReference>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(usersQuery, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void mobile(final String str, final Callback<Integer> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.attribution(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void attribution(final String str, final List<String> list, final Callback<Integer> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.attribution(str, list, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void retention(final String str, final Callback<Integer> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.acquisition(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void acquisition(final String str, final List<String> list, final Callback<Integer> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.acquisition(str, list, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void attribution(final List<String> list, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(list, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final String str, final List<String> list, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(str, list, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void dau(final String str, final Callback<Boolean> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.mobile(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void attribution(final Callback<Integer> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.attribution(callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final int i, final int i2, final Callback<List<PublicUser>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(i, i2, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void attribution(final int i, final int i2, final Callback<List<SuggestedFriend>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.attribution(i, i2, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void acquisition(final Callback<List<UserReference>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.acquisition(callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void unity() {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.android();
            }
        });
    }

    public final void getsocial(final NotificationListener notificationListener) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial((NotificationListener) jjbQypPegg.this.retention.getsocial(NotificationListener.class, notificationListener, false));
            }
        });
    }

    public final void getsocial(final PushTokenListener pushTokenListener) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                upgqDBbsrL upgqdbbsrl = jjbQypPegg.this.attribution;
                PushTokenListener pushTokenListener = (PushTokenListener) jjbQypPegg.this.retention.getsocial(PushTokenListener.class, pushTokenListener, false);
                jjbQypPegg.cjrhisSQCL.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(pushTokenListener), "Can not set null NotificationListener");
                upgqdbbsrl.mobile.getsocial(pushTokenListener);
            }
        });
    }

    public final void getsocial(final NotificationsQuery notificationsQuery, final Callback<List<Notification>> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(notificationsQuery, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final NotificationsCountQuery notificationsCountQuery, final Callback<Integer> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(notificationsCountQuery, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final List<String> list, final String str, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(list, str, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void getsocial(final List<String> list, final NotificationContent notificationContent, final Callback<NotificationsSummary> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(list, im.getsocial.sdk.pushnotifications.jjbQypPegg.getsocial(notificationContent), callback);
            }
        });
    }

    public final void getsocial(final boolean z, final CompletionCallback completionCallback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(z, completionCallback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(completionCallback));
    }

    public final void mobile(final Callback<Boolean> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.mobile(callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void getsocial(final Action action) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                upgqDBbsrL.getsocial(action);
            }
        });
    }

    public final void getsocial(final PromoCodeBuilder promoCodeBuilder, final Callback<PromoCode> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                upgqDBbsrL.getsocial(promoCodeBuilder, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void mau(final String str, final Callback<PromoCode> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                upgqDBbsrL.mau(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final void cat(final String str, final Callback<PromoCode> callback) {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                upgqDBbsrL.cat(str, callback);
            }
        }, im.getsocial.sdk.internal.c.d.a.jjbQypPegg.getsocial(callback));
    }

    public final boolean getsocial(final PurchaseData purchaseData) {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(purchaseData);
            }
        });
    }

    public final boolean getsocial(final String str, final Map<String, String> map) {
        return this.getsocial.getsocial(new Runnable() {
            public void run() {
                jjbQypPegg.this.attribution.getsocial(str, map);
            }
        });
    }

    public final void getsocial(Application application) {
        application.registerActivityLifecycleCallbacks(new im.getsocial.sdk.internal.m.jjbQypPegg() {
            private final Runnable acquisition = new Runnable() {
                public void run() {
                    if (jjbQypPegg.this.mau.getsocial() == im.getsocial.sdk.internal.m.cjrhisSQCL.PAUSING) {
                        jjbQypPegg.this.mau.getsocial(im.getsocial.sdk.internal.m.cjrhisSQCL.PAUSED);
                        jjbQypPegg.this.getsocial.getsocial(new Runnable() {
                            public void run() {
                                jjbQypPegg.this.attribution.unity();
                            }
                        });
                    }
                }
            };
            private final Handler attribution = new Handler(Looper.getMainLooper());

            public void onActivityCreated(Activity activity, Bundle bundle) {
                jjbQypPegg.this.dau.getsocial(jjbQypPegg.getsocial(jjbQypPegg.this, activity.getIntent()));
            }

            public void onActivityResumed(Activity activity) {
                im.getsocial.sdk.internal.m.cjrhisSQCL cjrhissqcl = jjbQypPegg.this.mau.getsocial();
                boolean z = cjrhissqcl == im.getsocial.sdk.internal.m.cjrhisSQCL.PAUSED || cjrhissqcl == im.getsocial.sdk.internal.m.cjrhisSQCL.NOT_STARTED;
                jjbQypPegg.this.mau.getsocial(im.getsocial.sdk.internal.m.cjrhisSQCL.RESUMED);
                jjbQypPegg.this.getsocial.getsocial(new Runnable(activity.getIntent()) {
                    public void run() {
                        jjbQypPegg.this.dau.getsocial(jjbQypPegg.getsocial(jjbQypPegg.this, r3));
                        jjbQypPegg.this.attribution.engage();
                        im.getsocial.sdk.pushnotifications.a.b.zoToeBNOjF zotoebnojf = im.getsocial.sdk.pushnotifications.a.cjrhisSQCL.getsocial(jjbQypPegg.this.acquisition);
                        if (zotoebnojf != null) {
                            zotoebnojf.retention();
                            upgqDBbsrL.getsocial(zotoebnojf);
                        }
                    }
                });
                this.attribution.removeCallbacks(this.acquisition);
                if (z) {
                    jjbQypPegg.this.getsocial.getsocial(new Runnable() {
                        public void run() {
                            jjbQypPegg.this.attribution.ios();
                            if (jjbQypPegg.this.getsocial()) {
                                jjbQypPegg.this.viral.attribution();
                            }
                        }
                    });
                }
            }

            public void onActivityPaused(Activity activity) {
                jjbQypPegg.this.dau.getsocial();
                jjbQypPegg.this.mau.getsocial(im.getsocial.sdk.internal.m.cjrhisSQCL.PAUSING);
                this.attribution.postDelayed(this.acquisition, 500);
            }
        });
        final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable th) {
                jjbQypPegg.this.cat.getsocial(th);
                if (defaultUncaughtExceptionHandler != null) {
                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                }
            }
        });
    }

    public final void connect() {
        this.getsocial.getsocial(new Runnable() {
            public void run() {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        jjbQypPegg.this.getsocial.getsocial(new Runnable() {
                            public void run() {
                                jjbQypPegg.this.attribution.connect();
                            }
                        });
                    }
                }, 100);
            }
        });
    }

    static /* synthetic */ void getsocial(jjbQypPegg jjbqyppegg, LinkParams linkParams) {
        Object obj = linkParams.get(LinkParams.KEY_CUSTOM_IMAGE);
        if (obj instanceof Bitmap) {
            linkParams.put(LinkParams.KEY_CUSTOM_IMAGE, iFpupLCESp.getsocial((Bitmap) obj));
        }
    }

    static /* synthetic */ String getsocial(jjbQypPegg jjbqyppegg, Intent intent) {
        if (intent != null && jjbqyppegg.organic.add(Integer.valueOf(intent.hashCode()))) {
            return intent.getDataString();
        }
        return null;
    }
}
