package im.getsocial.sdk.internal.c.j;

import com.helpshift.common.domain.network.NetworkConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: HttpTransport */
public class pdwpUtZXDT extends im.getsocial.b.a.c.jjbQypPegg {
    private OutputStream acquisition;
    private InputStream attribution;
    private final int cat;
    private final String dau;
    private HttpURLConnection getsocial;
    private final int mau;
    private final String mobile;
    private final String retention;

    /* compiled from: HttpTransport */
    public static class jjbQypPegg {
        /* access modifiers changed from: private */
        public boolean acquisition;
        /* access modifiers changed from: private */
        public int attribution = 0;
        /* access modifiers changed from: private */
        public final String getsocial;
        /* access modifiers changed from: private */
        public int mobile;
        /* access modifiers changed from: private */
        public String retention = "GetSocialSDK/HttpTransport";

        public jjbQypPegg(String str) {
            if (str == null || str.length() == 0) {
                throw new NullPointerException("host");
            }
            this.getsocial = str;
            this.attribution = 0;
        }

        public final jjbQypPegg getsocial(int i) {
            this.attribution = NetworkConstants.UPLOAD_CONNECT_TIMEOUT;
            return this;
        }

        public final jjbQypPegg attribution(int i) {
            this.mobile = NetworkConstants.UPLOAD_CONNECT_TIMEOUT;
            return this;
        }

        public final jjbQypPegg getsocial(boolean z) {
            this.acquisition = z;
            return this;
        }

        public final jjbQypPegg getsocial(String str) {
            this.retention = str;
            return this;
        }

        public final pdwpUtZXDT getsocial() {
            return new pdwpUtZXDT(this);
        }
    }

    pdwpUtZXDT(jjbQypPegg jjbqyppegg) {
        this.mobile = jjbqyppegg.acquisition ? "https://" : "http://";
        this.dau = jjbqyppegg.retention;
        this.retention = jjbqyppegg.getsocial;
        this.mau = jjbqyppegg.attribution;
        this.cat = jjbqyppegg.mobile;
    }

    public final int getsocial(byte[] bArr, int i, int i2) {
        return this.attribution.read(bArr, i, i2);
    }

    public final void attribution(byte[] bArr, int i, int i2) {
        this.acquisition.write(bArr, 0, i2);
    }

    public final void getsocial() {
        this.acquisition.flush();
        this.attribution = new BufferedInputStream(this.getsocial.getInputStream());
        this.acquisition.close();
        this.acquisition = null;
    }

    public void close() {
        if (this.attribution != null) {
            this.attribution.close();
            this.attribution = null;
        }
        if (this.getsocial != null) {
            this.getsocial.disconnect();
            this.getsocial = null;
        }
    }

    public final void attribution() {
        this.getsocial = (HttpURLConnection) new URL(this.mobile + this.retention).openConnection();
        this.getsocial.setConnectTimeout(this.mau);
        this.getsocial.setReadTimeout(this.cat);
        this.getsocial.setRequestProperty("Connection", "Keep-Alive");
        this.getsocial.setRequestMethod(HttpRequest.METHOD_POST);
        this.getsocial.setRequestProperty(HttpRequest.HEADER_CONTENT_TYPE, "application/x-thrift");
        this.getsocial.setRequestProperty("Accept", "application/x-thrift");
        this.getsocial.setRequestProperty("User-Agent", this.dau);
        this.getsocial.setDoOutput(true);
        this.getsocial.setUseCaches(false);
        this.acquisition = this.getsocial.getOutputStream();
    }
}
