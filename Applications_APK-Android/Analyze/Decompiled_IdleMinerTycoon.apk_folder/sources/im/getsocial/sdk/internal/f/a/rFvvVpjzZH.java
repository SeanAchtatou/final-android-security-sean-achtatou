package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;
import java.util.List;
import java.util.Map;

/* compiled from: THCustomNotification */
public final class rFvvVpjzZH {
    public String acquisition;
    public String attribution;
    public CJZnJxRuoc cat;
    public pdwpUtZXDT dau;
    public List<String> getsocial;
    public List<XdbacJlTDQ> mau;
    public String mobile;
    public Map<String, String> retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof rFvvVpjzZH)) {
            return false;
        }
        rFvvVpjzZH rfvvvpjzzh = (rFvvVpjzZH) obj;
        return (this.getsocial == rfvvvpjzzh.getsocial || (this.getsocial != null && this.getsocial.equals(rfvvvpjzzh.getsocial))) && (this.attribution == rfvvvpjzzh.attribution || (this.attribution != null && this.attribution.equals(rfvvvpjzzh.attribution))) && ((this.acquisition == rfvvvpjzzh.acquisition || (this.acquisition != null && this.acquisition.equals(rfvvvpjzzh.acquisition))) && ((this.mobile == rfvvvpjzzh.mobile || (this.mobile != null && this.mobile.equals(rfvvvpjzzh.mobile))) && ((this.retention == rfvvvpjzzh.retention || (this.retention != null && this.retention.equals(rfvvvpjzzh.retention))) && ((this.dau == rfvvvpjzzh.dau || (this.dau != null && this.dau.equals(rfvvvpjzzh.dau))) && ((this.mau == rfvvvpjzzh.mau || (this.mau != null && this.mau.equals(rfvvvpjzzh.mau))) && (this.cat == rfvvvpjzzh.cat || (this.cat != null && this.cat.equals(rfvvvpjzzh.cat))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) * -2128831035) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) * -2128831035) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035;
        if (this.cat != null) {
            i = this.cat.hashCode();
        }
        return (hashCode ^ i) * -2128831035 * -2128831035;
    }

    public final String toString() {
        return "THCustomNotification{userIds=" + this.getsocial + ", action=" + ((Object) null) + ", actionData=" + ((Object) null) + ", text=" + this.attribution + ", title=" + this.acquisition + ", image=" + ((String) null) + ", video=" + ((String) null) + ", templateName=" + this.mobile + ", templateData=" + this.retention + ", newAction=" + this.dau + ", actionButtons=" + this.mau + ", media=" + this.cat + ", platformMedia=" + ((Object) null) + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, rFvvVpjzZH rfvvvpjzzh) {
        if (rfvvvpjzzh.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 15);
            zotoebnojf.getsocial((byte) 11, rfvvvpjzzh.getsocial.size());
            for (String str : rfvvvpjzzh.getsocial) {
                zotoebnojf.getsocial(str);
            }
        }
        if (rfvvvpjzzh.attribution != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(rfvvvpjzzh.attribution);
        }
        if (rfvvvpjzzh.acquisition != null) {
            zotoebnojf.getsocial(5, (byte) 11);
            zotoebnojf.getsocial(rfvvvpjzzh.acquisition);
        }
        if (rfvvvpjzzh.mobile != null) {
            zotoebnojf.getsocial(8, (byte) 11);
            zotoebnojf.getsocial(rfvvvpjzzh.mobile);
        }
        if (rfvvvpjzzh.retention != null) {
            zotoebnojf.getsocial(9, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, rfvvvpjzzh.retention.size());
            for (Map.Entry next : rfvvvpjzzh.retention.entrySet()) {
                zotoebnojf.getsocial((String) next.getKey());
                zotoebnojf.getsocial((String) next.getValue());
            }
        }
        if (rfvvvpjzzh.dau != null) {
            zotoebnojf.getsocial(10, (byte) 12);
            pdwpUtZXDT.getsocial(zotoebnojf, rfvvvpjzzh.dau);
        }
        if (rfvvvpjzzh.mau != null) {
            zotoebnojf.getsocial(11, (byte) 15);
            zotoebnojf.getsocial((byte) 12, rfvvvpjzzh.mau.size());
            for (XdbacJlTDQ xdbacJlTDQ : rfvvvpjzzh.mau) {
                XdbacJlTDQ.getsocial(zotoebnojf, xdbacJlTDQ);
            }
        }
        if (rfvvvpjzzh.cat != null) {
            zotoebnojf.getsocial(12, (byte) 12);
            CJZnJxRuoc.getsocial(zotoebnojf, rfvvvpjzzh.cat);
        }
        zotoebnojf.getsocial();
    }
}
