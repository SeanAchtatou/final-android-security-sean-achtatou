package im.getsocial.sdk.internal.f.a;

/* compiled from: THDeviceTimeType */
public enum IbawHMWljm {
    SERVER_TIME(0),
    DEVICE_UPTIME(1),
    LOCAL_TIME(2);
    
    public final int value;

    private IbawHMWljm(int i) {
        this.value = i;
    }

    public static IbawHMWljm findByValue(int i) {
        switch (i) {
            case 0:
                return SERVER_TIME;
            case 1:
                return DEVICE_UPTIME;
            case 2:
                return LOCAL_TIME;
            default:
                return null;
        }
    }
}
