package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THSdkAuthResponseAllInOne */
public final class VuXsWfriFX {
    public JWvbLzaedN acquisition;
    public lgAVmIefSo attribution;
    public CyDeXbQkhA getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof VuXsWfriFX)) {
            return false;
        }
        VuXsWfriFX vuXsWfriFX = (VuXsWfriFX) obj;
        return (this.getsocial == vuXsWfriFX.getsocial || (this.getsocial != null && this.getsocial.equals(vuXsWfriFX.getsocial))) && (this.attribution == vuXsWfriFX.attribution || (this.attribution != null && this.attribution.equals(vuXsWfriFX.attribution))) && (this.acquisition == vuXsWfriFX.acquisition || (this.acquisition != null && this.acquisition.equals(vuXsWfriFX.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THSdkAuthResponseAllInOne{sdkAuthResponse=" + this.getsocial + ", tokenInfo=" + this.attribution + ", inviteProviders=" + this.acquisition + "}";
    }

    public static VuXsWfriFX getsocial(zoToeBNOjF zotoebnojf) {
        VuXsWfriFX vuXsWfriFX = new VuXsWfriFX();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vuXsWfriFX.getsocial = CyDeXbQkhA.getsocial(zotoebnojf);
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vuXsWfriFX.attribution = lgAVmIefSo.getsocial(zotoebnojf);
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vuXsWfriFX.acquisition = JWvbLzaedN.getsocial(zotoebnojf);
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return vuXsWfriFX;
            }
        }
    }
}
