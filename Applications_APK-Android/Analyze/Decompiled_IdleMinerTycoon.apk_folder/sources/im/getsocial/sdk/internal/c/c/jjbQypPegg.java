package im.getsocial.sdk.internal.c.c;

import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import java.net.SocketTimeoutException;

/* compiled from: GetSocialExceptionAdapter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static GetSocialException getsocial(Throwable th) {
        int i;
        if (th instanceof GetSocialException) {
            return (GetSocialException) th;
        }
        if (th instanceof NullPointerException) {
            i = ErrorCode.NULL_POINTER;
        } else if (th instanceof IllegalArgumentException) {
            i = 204;
        } else if (th instanceof IllegalStateException) {
            i = ErrorCode.ILLEGAL_STATE;
        } else if (th instanceof SocketTimeoutException) {
            i = ErrorCode.CONNECTION_TIMEOUT;
        } else if (th instanceof im.getsocial.sdk.internal.c.a.a.jjbQypPegg) {
            i = ((im.getsocial.sdk.internal.c.a.a.jjbQypPegg) th).getErrorCode();
        } else {
            i = 0;
        }
        return new GetSocialException(i, th.getMessage(), th);
    }
}
