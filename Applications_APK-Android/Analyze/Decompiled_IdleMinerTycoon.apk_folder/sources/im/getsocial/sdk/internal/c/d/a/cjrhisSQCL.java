package im.getsocial.sdk.internal.c.d.a;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;

/* compiled from: CallbackAdapter */
class cjrhisSQCL implements jjbQypPegg.C0016jjbQypPegg {
    private final Callback getsocial;

    cjrhisSQCL(Callback callback) {
        this.getsocial = callback;
    }

    public final void getsocial(GetSocialException getSocialException) {
        if (this.getsocial != null) {
            this.getsocial.onFailure(getSocialException);
        }
    }
}
