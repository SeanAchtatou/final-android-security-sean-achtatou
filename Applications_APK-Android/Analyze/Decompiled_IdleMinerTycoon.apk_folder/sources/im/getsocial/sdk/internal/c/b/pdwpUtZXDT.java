package im.getsocial.sdk.internal.c.b;

import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.io.Serializable;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/* compiled from: ComponentResolver */
public class pdwpUtZXDT implements ruWsnwUPKh {
    private static final Set<im.getsocial.sdk.internal.c.b.a.jjbQypPegg> attribution;
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(pdwpUtZXDT.class);
    private final Object acquisition = new Object();
    private final jMsobIMeui dau = new jMsobIMeui(EnumSet.allOf(qdyNCsqjKt.class));
    private final Map<String, cjrhisSQCL> mobile = new HashMap();
    private final Map<String, Object> retention = new HashMap();

    static {
        TreeSet treeSet = new TreeSet(new jjbQypPegg((byte) 0));
        attribution = treeSet;
        treeSet.add(new im.getsocial.sdk.internal.c.b.a.upgqDBbsrL());
    }

    private static String attribution(Class cls, String str) {
        return cls.getName() + "#" + str;
    }

    public final <T extends KluUZYuxme> T getsocial(Class cls) {
        return this.dau.getsocial(cls);
    }

    public final void getsocial(jMsobIMeui jmsobimeui) {
        this.dau.getsocial(jmsobimeui);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, im.getsocial.sdk.internal.c.b.pdwpUtZXDT$1]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    public final <T> void getsocial(Class cls, final Class cls2) {
        getsocial(cls, (cjrhisSQCL) new cjrhisSQCL<T>() {
            public final T getsocial(pdwpUtZXDT pdwputzxdt) {
                return pdwputzxdt.mobile(cls2);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.pdwpUtZXDT$2]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    public final <T> void getsocial(Class cls, String str, final Class cls2) {
        getsocial(cls, str, (cjrhisSQCL) new cjrhisSQCL<T>() {
            public final T getsocial(pdwpUtZXDT pdwputzxdt) {
                return pdwputzxdt.mobile(cls2);
            }
        });
    }

    public final <T> void getsocial(Class cls, cjrhisSQCL cjrhissqcl) {
        getsocial(cls.getName(), cjrhissqcl);
    }

    public final <T> void getsocial(Class cls, String str, cjrhisSQCL cjrhissqcl) {
        getsocial(attribution(cls, str), cjrhissqcl);
    }

    private <T> void getsocial(String str, cjrhisSQCL cjrhissqcl) {
        synchronized (this.acquisition) {
            jjbQypPegg.cjrhisSQCL.getsocial(!acquisition(str), "Component already registered for '" + str + "'");
            jjbQypPegg.cjrhisSQCL.getsocial(mobile(str) ^ true, "Component can't be registered after injecting the test component");
            this.mobile.put(str, cjrhissqcl);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, im.getsocial.sdk.internal.c.b.pdwpUtZXDT$3]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    public final <T> void getsocial(Class cls, final Object obj) {
        getsocial(cls, (cjrhisSQCL) new cjrhisSQCL<T>() {
            public final T getsocial(pdwpUtZXDT pdwputzxdt) {
                return obj;
            }
        });
    }

    /* renamed from: im.getsocial.sdk.internal.c.b.pdwpUtZXDT$4  reason: invalid class name */
    /* compiled from: ComponentResolver */
    class AnonymousClass4 implements cjrhisSQCL {
        final /* synthetic */ Object getsocial;

        public final Object getsocial(pdwpUtZXDT pdwputzxdt) {
            return this.getsocial;
        }
    }

    public final <T> void getsocial(Class cls, String str, final Object obj) {
        this.mobile.put(attribution(cls, str), new cjrhisSQCL() {
            public final Object getsocial(pdwpUtZXDT pdwputzxdt) {
                return obj;
            }
        });
    }

    /* renamed from: im.getsocial.sdk.internal.c.b.pdwpUtZXDT$6  reason: invalid class name */
    /* compiled from: ComponentResolver */
    class AnonymousClass6 implements cjrhisSQCL {
        final /* synthetic */ pdwpUtZXDT attribution;
        final /* synthetic */ Class getsocial;

        public final Object getsocial(pdwpUtZXDT pdwputzxdt) {
            return this.attribution.mobile(this.getsocial);
        }
    }

    public final <T> T attribution(Class<T> cls) {
        return getsocial(cls.getName());
    }

    private <T> T getsocial(String str) {
        T t;
        synchronized (this.acquisition) {
            if (mobile(str)) {
                t = this.retention.get(str);
            } else if (acquisition(str)) {
                T t2 = this.mobile.get(str).getsocial(this);
                this.retention.put(str, t2);
                t = t2;
            } else {
                throw new RuntimeException("Component is not registered for '" + str + "'");
            }
        }
        return t;
    }

    /* access modifiers changed from: private */
    public <T> T mobile(Class<? extends T> cls) {
        Iterator<im.getsocial.sdk.internal.c.b.a.jjbQypPegg> it = attribution.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            try {
                return it.next().getsocial(cls, this);
            } catch (Throwable th) {
                if (!th.getClass().getSimpleName().equals("ReflectionStrippedError")) {
                    getsocial.attribution(th);
                    break;
                }
            }
        }
        throw new RuntimeException("Failed to instantiate instance of class " + cls);
    }

    public final boolean acquisition(Class cls) {
        return attribution(cls.getName());
    }

    public final boolean getsocial(Class cls, String str) {
        return attribution(attribution(cls, str));
    }

    private boolean attribution(String str) {
        return !acquisition(str) && !mobile(str);
    }

    private boolean acquisition(String str) {
        return this.mobile.containsKey(str);
    }

    private boolean mobile(String str) {
        return this.retention.containsKey(str);
    }

    /* compiled from: ComponentResolver */
    private static final class jjbQypPegg implements Serializable, Comparator<im.getsocial.sdk.internal.c.b.a.jjbQypPegg> {
        private jjbQypPegg() {
        }

        /* synthetic */ jjbQypPegg(byte b) {
            this();
        }

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            boolean z = ((im.getsocial.sdk.internal.c.b.a.jjbQypPegg) obj) instanceof im.getsocial.sdk.internal.c.b.a.upgqDBbsrL;
            if (z == (((im.getsocial.sdk.internal.c.b.a.jjbQypPegg) obj2) instanceof im.getsocial.sdk.internal.c.b.a.upgqDBbsrL)) {
                return 0;
            }
            return z ? -1 : 1;
        }
    }

    public final Object getsocial(Class cls, KSZKMmRWhZ kSZKMmRWhZ) {
        if (kSZKMmRWhZ != null) {
            return getsocial(attribution(cls, kSZKMmRWhZ.getsocial()));
        }
        if (KluUZYuxme.class.isAssignableFrom(cls)) {
            return getsocial(cls);
        }
        return attribution(cls);
    }
}
