package im.getsocial.sdk.internal.g.e;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.m.iFpupLCESp;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* compiled from: HttpUrlBitmapLoader */
public class jjbQypPegg implements upgqDBbsrL {
    private static final Executor getsocial = Executors.newFixedThreadPool(20);

    public final void getsocial(String str, int i, int i2, Callback<Bitmap> callback) {
        final String str2 = str;
        final int i3 = i;
        final int i4 = i2;
        final Callback<Bitmap> callback2 = callback;
        getsocial.execute(new Runnable() {
            public void run() {
                ByteArrayOutputStream byteArrayOutputStream;
                InputStream inputStream;
                IOException e;
                OutOfMemoryError e2;
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setUseCaches(true);
                    httpURLConnection.connect();
                    inputStream = httpURLConnection.getInputStream();
                    try {
                        byteArrayOutputStream = new ByteArrayOutputStream();
                        try {
                            jjbQypPegg.getsocial(inputStream, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            options.inPreferredConfig = null;
                            BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                            options.inSampleSize = jjbQypPegg.getsocial(jjbQypPegg.this, options, i3, i4);
                            options.inJustDecodeBounds = false;
                            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                            if (decodeByteArray == null) {
                                callback2.onFailure(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(new Exception("Failed to create image")));
                            } else {
                                callback2.onSuccess(decodeByteArray);
                            }
                        } catch (IOException e3) {
                            e = e3;
                            callback2.onFailure(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(e));
                            iFpupLCESp.getsocial(inputStream);
                            iFpupLCESp.getsocial(byteArrayOutputStream);
                        } catch (OutOfMemoryError e4) {
                            e2 = e4;
                            callback2.onFailure(new GetSocialException(103, e2.getMessage()));
                            iFpupLCESp.getsocial(inputStream);
                            iFpupLCESp.getsocial(byteArrayOutputStream);
                        }
                    } catch (IOException e5) {
                        byteArrayOutputStream = null;
                        e = e5;
                        callback2.onFailure(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(e));
                        iFpupLCESp.getsocial(inputStream);
                        iFpupLCESp.getsocial(byteArrayOutputStream);
                    } catch (OutOfMemoryError e6) {
                        byteArrayOutputStream = null;
                        e2 = e6;
                        callback2.onFailure(new GetSocialException(103, e2.getMessage()));
                        iFpupLCESp.getsocial(inputStream);
                        iFpupLCESp.getsocial(byteArrayOutputStream);
                    } catch (Throwable th) {
                        byteArrayOutputStream = null;
                        th = th;
                        iFpupLCESp.getsocial(inputStream);
                        iFpupLCESp.getsocial(byteArrayOutputStream);
                        throw th;
                    }
                } catch (IOException e7) {
                    byteArrayOutputStream = null;
                    e = e7;
                    inputStream = null;
                    callback2.onFailure(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(e));
                    iFpupLCESp.getsocial(inputStream);
                    iFpupLCESp.getsocial(byteArrayOutputStream);
                } catch (OutOfMemoryError e8) {
                    byteArrayOutputStream = null;
                    e2 = e8;
                    inputStream = null;
                    callback2.onFailure(new GetSocialException(103, e2.getMessage()));
                    iFpupLCESp.getsocial(inputStream);
                    iFpupLCESp.getsocial(byteArrayOutputStream);
                } catch (Throwable th2) {
                    th = th2;
                    iFpupLCESp.getsocial(inputStream);
                    iFpupLCESp.getsocial(byteArrayOutputStream);
                    throw th;
                }
                iFpupLCESp.getsocial(inputStream);
                iFpupLCESp.getsocial(byteArrayOutputStream);
            }
        });
    }

    static /* synthetic */ void getsocial(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[256];
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 != read) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    static /* synthetic */ int getsocial(jjbQypPegg jjbqyppegg, BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        int i5 = 1;
        if (!((i4 <= i2 && i3 <= i) || i == 0 || i2 == 0)) {
            int i6 = i3 / 2;
            int i7 = i4 / 2;
            while (i7 / i5 >= i2 && i6 / i5 >= i) {
                i5 <<= 1;
            }
        }
        return i5;
    }
}
