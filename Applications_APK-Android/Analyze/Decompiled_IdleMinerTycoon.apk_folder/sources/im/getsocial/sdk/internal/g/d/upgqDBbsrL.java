package im.getsocial.sdk.internal.g.d;

import android.os.Handler;
import android.os.Looper;

/* compiled from: MainThreadExecutor */
public class upgqDBbsrL implements jjbQypPegg {
    public final void getsocial(Runnable runnable) {
        getsocial().post(runnable);
    }

    public final void getsocial(Runnable runnable, int i) {
        getsocial().postDelayed(runnable, 300);
    }

    private static Handler getsocial() {
        return new Handler(Looper.getMainLooper());
    }
}
