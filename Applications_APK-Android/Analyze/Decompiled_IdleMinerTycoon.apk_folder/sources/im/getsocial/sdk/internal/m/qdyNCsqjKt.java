package im.getsocial.sdk.internal.m;

import im.getsocial.sdk.internal.c.vkXhnjhKGp;
import java.util.LinkedList;
import java.util.Queue;

/* compiled from: PendingUiThreadHandler */
public final class qdyNCsqjKt implements vkXhnjhKGp {
    private final vkXhnjhKGp acquisition;
    private final Object attribution = new Object();
    private boolean getsocial;
    private final Queue<Runnable> mobile = new LinkedList();

    public qdyNCsqjKt(vkXhnjhKGp vkxhnjhkgp) {
        this.acquisition = vkxhnjhkgp;
    }

    public final void getsocial() {
        synchronized (this.attribution) {
            this.getsocial = true;
            for (Runnable runnable : this.mobile) {
                this.acquisition.getsocial(runnable);
            }
            this.mobile.clear();
        }
    }

    public final void getsocial(Runnable runnable) {
        synchronized (this.attribution) {
            if (this.getsocial) {
                this.acquisition.getsocial(runnable);
            } else {
                this.mobile.add(runnable);
            }
        }
    }
}
