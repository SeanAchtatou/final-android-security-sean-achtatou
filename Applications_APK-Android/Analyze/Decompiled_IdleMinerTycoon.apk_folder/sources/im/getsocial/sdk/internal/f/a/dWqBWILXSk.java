package im.getsocial.sdk.internal.f.a;

/* compiled from: THUploadParams */
public final class dWqBWILXSk {
    public final int hashCode() {
        return 2071361763;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && (obj instanceof dWqBWILXSk);
    }

    public final String toString() {
        return "THUploadParams{ids=" + ((Object) null) + ", purpose=" + ((Object) null) + ", appId=" + ((String) null) + ", typ=" + ((Object) null) + "}";
    }
}
