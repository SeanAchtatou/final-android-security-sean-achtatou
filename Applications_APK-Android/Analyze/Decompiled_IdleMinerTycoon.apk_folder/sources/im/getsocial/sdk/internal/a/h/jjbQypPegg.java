package im.getsocial.sdk.internal.a.h;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.a.b.jjbQypPegg;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.k.upgqDBbsrL;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SendQueuedAnalyticsEventsUseCase */
public final class jjbQypPegg implements upgqDBbsrL {
    private static final cjrhisSQCL retention = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(jjbQypPegg.class);
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.j.c.jjbQypPegg acquisition;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.c.jjbQypPegg attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.a.jjbQypPegg getsocial;
    @XdbacJlTDQ
    JbBdMtJmlU mobile;

    public jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.KSZKMmRWhZ.getsocial(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.KSZKMmRWhZ.getsocial(java.lang.String, int):int
      im.getsocial.sdk.internal.c.KSZKMmRWhZ.getsocial(java.lang.String, boolean):boolean */
    public final void getsocial(boolean z) {
        List<im.getsocial.sdk.internal.a.b.jjbQypPegg> list = this.attribution.getsocial();
        im.getsocial.sdk.internal.c.i.upgqDBbsrL attribution2 = this.acquisition.attribution();
        int i = 0;
        if (!((attribution2 == null || attribution2.retention() == null) ? false : attribution2.retention().getsocial("remote_logs", false))) {
            ArrayList arrayList = new ArrayList();
            for (im.getsocial.sdk.internal.a.b.jjbQypPegg next : list) {
                if ("sdk_log".equals(next.getsocial())) {
                    arrayList.add(next);
                }
            }
            list.removeAll(arrayList);
        }
        if (!list.isEmpty()) {
            for (im.getsocial.sdk.internal.a.b.jjbQypPegg next2 : list) {
                long connect = this.mobile.connect();
                if (next2.cat() == jjbQypPegg.C0013jjbQypPegg.PENDING) {
                    long viral = connect - next2.viral();
                    if (viral > 0) {
                        next2.attribution(next2.acquisition() + viral);
                        next2.getsocial(jjbQypPegg.C0013jjbQypPegg.DEVICE_UPTIME);
                    } else {
                        next2.getsocial(jjbQypPegg.C0013jjbQypPegg.LOCAL_TIME);
                    }
                }
            }
            this.attribution.attribution();
            try {
                this.getsocial.getsocial(this.mobile, list);
            } catch (GetSocialException e) {
                retention.attribution("Failed to send analytics queue");
                retention.getsocial(e);
                if (z) {
                    im.getsocial.sdk.internal.c.i.upgqDBbsrL attribution3 = this.acquisition.attribution();
                    if (!(attribution3 == null || attribution3.retention() == null)) {
                        i = attribution3.retention().getsocial("retry_count", 0);
                    }
                    getsocial(list, e, i);
                }
            }
        }
    }

    private void getsocial(List<im.getsocial.sdk.internal.a.b.jjbQypPegg> list, GetSocialException getSocialException, int i) {
        if (getSocialException.getErrorCode() == 701) {
            retention.attribution("Timeout exception, queue events again");
            for (im.getsocial.sdk.internal.a.b.jjbQypPegg next : list) {
                if (next.retention() <= ((long) i)) {
                    next.dau();
                    this.attribution.getsocial(next);
                }
            }
            return;
        }
        this.attribution.getsocial(list);
    }
}
