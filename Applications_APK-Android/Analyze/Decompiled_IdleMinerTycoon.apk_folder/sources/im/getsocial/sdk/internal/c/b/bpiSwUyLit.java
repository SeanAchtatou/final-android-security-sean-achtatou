package im.getsocial.sdk.internal.c.b;

import im.getsocial.sdk.actions.ActionListener;
import im.getsocial.sdk.internal.a.f.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.KCGqEGAizh;
import im.getsocial.sdk.internal.c.a.jjbQypPegg;
import im.getsocial.sdk.internal.c.j.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.j.b.upgqDBbsrL;
import im.getsocial.sdk.internal.c.j.cjrhisSQCL;
import im.getsocial.sdk.internal.c.j.ztWNWCuZiM;
import im.getsocial.sdk.invites.a.k.KSZKMmRWhZ;

/* compiled from: SharedComponentHelper */
public final class bpiSwUyLit {
    private bpiSwUyLit() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.j.ztWNWCuZiM]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    public static void getsocial(pdwpUtZXDT pdwputzxdt) {
        getsocial(pdwputzxdt, jjbQypPegg.class, XdbacJlTDQ.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.j.b.jjbQypPegg.class, upgqDBbsrL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.j.c.jjbQypPegg.class, im.getsocial.sdk.internal.c.j.c.jjbQypPegg.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.a.c.jjbQypPegg.class, im.getsocial.sdk.internal.a.c.upgqDBbsrL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.j.upgqDBbsrL.class, cjrhisSQCL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.upgqDBbsrL.class, im.getsocial.sdk.internal.upgqDBbsrL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.a.a.jjbQypPegg.class, im.getsocial.sdk.internal.a.a.upgqDBbsrL.class);
        getsocial(pdwputzxdt, KCGqEGAizh.class, im.getsocial.sdk.internal.i.a.jjbQypPegg.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.a.f.cjrhisSQCL.class, pdwpUtZXDT.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.a.g.jjbQypPegg.class, im.getsocial.sdk.internal.a.g.jjbQypPegg.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.invites.a.h.jjbQypPegg.class, im.getsocial.sdk.invites.a.h.jjbQypPegg.class);
        getsocial(pdwputzxdt, KSZKMmRWhZ.class, KSZKMmRWhZ.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.h.cjrhisSQCL.class, im.getsocial.sdk.internal.c.h.cjrhisSQCL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.e.jjbQypPegg.class, im.getsocial.sdk.internal.c.e.jjbQypPegg.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.iap.a.a.jjbQypPegg.class, im.getsocial.sdk.iap.a.a.jjbQypPegg.class);
        if (pdwputzxdt.acquisition(im.getsocial.sdk.internal.e.a.pdwpUtZXDT.class)) {
            pdwputzxdt.getsocial(im.getsocial.sdk.internal.e.a.pdwpUtZXDT.class, new im.getsocial.sdk.internal.e.a.pdwpUtZXDT() {
                public final im.getsocial.sdk.internal.e.a.upgqDBbsrL getsocial() {
                    return im.getsocial.sdk.internal.e.a.cjrhisSQCL.getsocial();
                }
            });
        }
        if (pdwputzxdt.getsocial(String.class, "UserAgent")) {
            pdwputzxdt.getsocial(String.class, "UserAgent", (cjrhisSQCL) new ztWNWCuZiM());
        }
        if (pdwputzxdt.getsocial(ActionListener.class, "core_action_listener")) {
            pdwputzxdt.getsocial(ActionListener.class, "core_action_listener", im.getsocial.sdk.actions.a.jjbQypPegg.class);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
     arg types: [java.lang.Class<T>, java.lang.Class<? extends T>]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void */
    private static <T> void getsocial(pdwpUtZXDT pdwputzxdt, Class<T> cls, Class<? extends T> cls2) {
        if (pdwputzxdt.acquisition(cls)) {
            pdwputzxdt.getsocial((Class) cls, (Class) cls2);
        }
    }
}
