package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import im.getsocial.b.a.ztWNWCuZiM;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: THInviteProperties */
public final class vkXhnjhKGp {
    public String acquisition;
    public List<qdyNCsqjKt> attribution;
    public String cat;
    public String dau;
    public String getsocial;
    public String mau;
    public String mobile;
    public Map<String, String> organic;
    public String retention;
    public String viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof vkXhnjhKGp)) {
            return false;
        }
        vkXhnjhKGp vkxhnjhkgp = (vkXhnjhKGp) obj;
        return (this.getsocial == vkxhnjhkgp.getsocial || (this.getsocial != null && this.getsocial.equals(vkxhnjhkgp.getsocial))) && (this.attribution == vkxhnjhkgp.attribution || (this.attribution != null && this.attribution.equals(vkxhnjhkgp.attribution))) && ((this.acquisition == vkxhnjhkgp.acquisition || (this.acquisition != null && this.acquisition.equals(vkxhnjhkgp.acquisition))) && ((this.mobile == vkxhnjhkgp.mobile || (this.mobile != null && this.mobile.equals(vkxhnjhkgp.mobile))) && ((this.retention == vkxhnjhkgp.retention || (this.retention != null && this.retention.equals(vkxhnjhkgp.retention))) && ((this.dau == vkxhnjhkgp.dau || (this.dau != null && this.dau.equals(vkxhnjhkgp.dau))) && ((this.mau == vkxhnjhkgp.mau || (this.mau != null && this.mau.equals(vkxhnjhkgp.mau))) && ((this.cat == vkxhnjhkgp.cat || (this.cat != null && this.cat.equals(vkxhnjhkgp.cat))) && ((this.viral == vkxhnjhkgp.viral || (this.viral != null && this.viral.equals(vkxhnjhkgp.viral))) && (this.organic == vkxhnjhkgp.organic || (this.organic != null && this.organic.equals(vkxhnjhkgp.organic))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035;
        if (this.organic != null) {
            i = this.organic.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THInviteProperties{action=" + this.getsocial + ", availableFields=" + this.attribution + ", contentType=" + this.acquisition + ", className=" + this.mobile + ", data=" + this.retention + ", packageName=" + this.dau + ", uti=" + this.mau + ", urlScheme=" + this.cat + ", imageExtension=" + this.viral + ", annotations=" + this.organic + "}";
    }

    public static vkXhnjhKGp getsocial(zoToeBNOjF zotoebnojf) {
        vkXhnjhKGp vkxhnjhkgp = new vkXhnjhKGp();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention2 = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention2.attribution);
                            while (i < retention2.attribution) {
                                int organic2 = zotoebnojf.organic();
                                qdyNCsqjKt findByValue = qdyNCsqjKt.findByValue(organic2);
                                if (findByValue != null) {
                                    arrayList.add(findByValue);
                                    i++;
                                } else {
                                    ztWNWCuZiM.jjbQypPegg jjbqyppegg = ztWNWCuZiM.jjbQypPegg.PROTOCOL_ERROR;
                                    throw new ztWNWCuZiM(jjbqyppegg, "Unexpected value for enum-type THAvailableField: " + organic2);
                                }
                            }
                            vkxhnjhkgp.attribution = arrayList;
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.mobile = zotoebnojf.ios();
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.retention = zotoebnojf.ios();
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.dau = zotoebnojf.ios();
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.mau = zotoebnojf.ios();
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.cat = zotoebnojf.ios();
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            vkxhnjhkgp.viral = zotoebnojf.ios();
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            vkxhnjhkgp.organic = hashMap;
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return vkxhnjhkgp;
            }
        }
    }
}
