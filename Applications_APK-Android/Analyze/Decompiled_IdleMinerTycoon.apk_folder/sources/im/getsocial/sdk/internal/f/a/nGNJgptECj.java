package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: THNotification */
public final class nGNJgptECj {
    public Integer acquisition;

    /* renamed from: android  reason: collision with root package name */
    public pdwpUtZXDT f485android;
    public Integer attribution;
    public String cat;
    public List<XdbacJlTDQ> connect;
    public Map<String, String> dau;
    public Integer engage;
    public String getsocial;
    public String growth;
    public String ios;
    public String mau;
    public Boolean mobile;
    public String organic;
    public CJZnJxRuoc referral;
    public Integer retention;
    public String unity;
    public xlPHPMtUBa viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof nGNJgptECj)) {
            return false;
        }
        nGNJgptECj ngnjgptecj = (nGNJgptECj) obj;
        return (this.getsocial == ngnjgptecj.getsocial || (this.getsocial != null && this.getsocial.equals(ngnjgptecj.getsocial))) && (this.attribution == ngnjgptecj.attribution || (this.attribution != null && this.attribution.equals(ngnjgptecj.attribution))) && ((this.acquisition == ngnjgptecj.acquisition || (this.acquisition != null && this.acquisition.equals(ngnjgptecj.acquisition))) && ((this.mobile == ngnjgptecj.mobile || (this.mobile != null && this.mobile.equals(ngnjgptecj.mobile))) && ((this.retention == ngnjgptecj.retention || (this.retention != null && this.retention.equals(ngnjgptecj.retention))) && ((this.dau == ngnjgptecj.dau || (this.dau != null && this.dau.equals(ngnjgptecj.dau))) && ((this.mau == ngnjgptecj.mau || (this.mau != null && this.mau.equals(ngnjgptecj.mau))) && ((this.cat == ngnjgptecj.cat || (this.cat != null && this.cat.equals(ngnjgptecj.cat))) && ((this.viral == ngnjgptecj.viral || (this.viral != null && this.viral.equals(ngnjgptecj.viral))) && ((this.organic == ngnjgptecj.organic || (this.organic != null && this.organic.equals(ngnjgptecj.organic))) && ((this.growth == ngnjgptecj.growth || (this.growth != null && this.growth.equals(ngnjgptecj.growth))) && ((this.f485android == ngnjgptecj.f485android || (this.f485android != null && this.f485android.equals(ngnjgptecj.f485android))) && ((this.ios == ngnjgptecj.ios || (this.ios != null && this.ios.equals(ngnjgptecj.ios))) && ((this.unity == ngnjgptecj.unity || (this.unity != null && this.unity.equals(ngnjgptecj.unity))) && ((this.connect == ngnjgptecj.connect || (this.connect != null && this.connect.equals(ngnjgptecj.connect))) && ((this.engage == ngnjgptecj.engage || (this.engage != null && this.engage.equals(ngnjgptecj.engage))) && (this.referral == ngnjgptecj.referral || (this.referral != null && this.referral.equals(ngnjgptecj.referral)))))))))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035) ^ (this.growth == null ? 0 : this.growth.hashCode())) * -2128831035) ^ (this.f485android == null ? 0 : this.f485android.hashCode())) * -2128831035) ^ (this.ios == null ? 0 : this.ios.hashCode())) * -2128831035) ^ (this.unity == null ? 0 : this.unity.hashCode())) * -2128831035) ^ (this.connect == null ? 0 : this.connect.hashCode())) * -2128831035) ^ (this.engage == null ? 0 : this.engage.hashCode())) * -2128831035;
        if (this.referral != null) {
            i = this.referral.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THNotification{id=" + this.getsocial + ", createdAt=" + this.attribution + ", type=" + this.acquisition + ", isRead=" + this.mobile + ", actionType=" + this.retention + ", actionsArgs=" + this.dau + ", text=" + this.mau + ", title=" + this.cat + ", origin=" + this.viral + ", image=" + this.organic + ", video=" + this.growth + ", action=" + this.f485android + ", typeStr=" + this.ios + ", status=" + this.unity + ", actionButtons=" + this.connect + ", expiration=" + this.engage + ", media=" + this.referral + "}";
    }

    public static nGNJgptECj getsocial(zoToeBNOjF zotoebnojf) {
        nGNJgptECj ngnjgptecj = new nGNJgptECj();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.attribution = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.acquisition = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.mobile = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.retention = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            ngnjgptecj.dau = hashMap;
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.mau = zotoebnojf.ios();
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.cat = zotoebnojf.ios();
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.viral = xlPHPMtUBa.getsocial(zotoebnojf);
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.organic = zotoebnojf.ios();
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.growth = zotoebnojf.ios();
                            break;
                        }
                    case 12:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.f485android = pdwpUtZXDT.getsocial(zotoebnojf);
                            break;
                        }
                    case 13:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.ios = zotoebnojf.ios();
                            break;
                        }
                    case 14:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.unity = zotoebnojf.ios();
                            break;
                        }
                    case 15:
                        if (acquisition2.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention2 = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention2.attribution);
                            while (i < retention2.attribution) {
                                arrayList.add(XdbacJlTDQ.getsocial(zotoebnojf));
                                i++;
                            }
                            ngnjgptecj.connect = arrayList;
                            break;
                        }
                    case 16:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.engage = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 17:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ngnjgptecj.referral = CJZnJxRuoc.getsocial(zotoebnojf);
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return ngnjgptecj;
            }
        }
    }
}
