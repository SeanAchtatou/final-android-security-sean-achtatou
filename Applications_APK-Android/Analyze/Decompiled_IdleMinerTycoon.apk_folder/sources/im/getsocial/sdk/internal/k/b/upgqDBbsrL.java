package im.getsocial.sdk.internal.k.b;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import java.util.concurrent.CountDownLatch;

/* compiled from: DownloadMediaFunc */
public final class upgqDBbsrL {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL attribution = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);
    @XdbacJlTDQ
    im.getsocial.sdk.internal.k.upgqDBbsrL getsocial;

    public upgqDBbsrL() {
        ztWNWCuZiM.getsocial(this);
    }

    public final byte[] getsocial(final String str) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final byte[][] bArr = new byte[1][];
        this.getsocial.getsocial(str, new Callback<byte[]>() {
            public /* synthetic */ void onSuccess(Object obj) {
                bArr[0] = (byte[]) obj;
                countDownLatch.countDown();
            }

            public void onFailure(GetSocialException getSocialException) {
                upgqDBbsrL.attribution.attribution("Failed to download data from %s, error: %s", str, getSocialException.getMessage());
                bArr[0] = null;
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return bArr[0];
    }
}
