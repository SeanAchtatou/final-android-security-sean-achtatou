package im.getsocial.sdk.internal.f.a;

/* compiled from: THAvailableField */
public enum qdyNCsqjKt {
    ExtraSubject(0),
    ExtraText(1),
    ExtraStream(2),
    ExtraGif(3),
    ExtraVideo(4),
    ExtraTest(-1000);
    
    public final int value;

    private qdyNCsqjKt(int i) {
        this.value = i;
    }

    public static qdyNCsqjKt findByValue(int i) {
        if (i == -1000) {
            return ExtraTest;
        }
        switch (i) {
            case 0:
                return ExtraSubject;
            case 1:
                return ExtraText;
            case 2:
                return ExtraStream;
            case 3:
                return ExtraGif;
            case 4:
                return ExtraVideo;
            default:
                return null;
        }
    }
}
