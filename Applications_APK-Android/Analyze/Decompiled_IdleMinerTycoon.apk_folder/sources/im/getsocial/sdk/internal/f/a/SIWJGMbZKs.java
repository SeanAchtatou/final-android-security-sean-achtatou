package im.getsocial.sdk.internal.f.a;

/* compiled from: THSdkRuntime */
public enum SIWJGMbZKs {
    NATIVE(0),
    UNITY(1),
    MARMALADE(2),
    CORDOVA(3),
    REACTNATIVE(4),
    API(5);
    
    public final int value;

    private SIWJGMbZKs(int i) {
        this.value = i;
    }

    public static SIWJGMbZKs findByValue(int i) {
        switch (i) {
            case 0:
                return NATIVE;
            case 1:
                return UNITY;
            case 2:
                return MARMALADE;
            case 3:
                return CORDOVA;
            case 4:
                return REACTNATIVE;
            case 5:
                return API;
            default:
                return null;
        }
    }
}
