package im.getsocial.sdk.internal.m;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ThriftyConverterBase */
public final class QCXFOjcJkE {

    /* compiled from: ThriftyConverterBase */
    public interface jjbQypPegg<T, R> {
        R getsocial(T t);
    }

    private QCXFOjcJkE() {
    }

    public static boolean getsocial(Boolean bool) {
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public static int getsocial(Number number) {
        return getsocial(number, 0);
    }

    public static int getsocial(Number number, int i) {
        return number == null ? i : number.intValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, long):long
     arg types: [java.lang.Number, int]
     candidates:
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, int):int
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.String, long):long
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.util.List, im.getsocial.sdk.internal.m.QCXFOjcJkE$jjbQypPegg):java.util.List<R>
      im.getsocial.sdk.internal.m.QCXFOjcJkE.getsocial(java.lang.Number, long):long */
    public static long attribution(Number number) {
        return getsocial(number, 0L);
    }

    public static long getsocial(Number number, long j) {
        return number == null ? j : number.longValue();
    }

    public static long getsocial(String str) {
        return getsocial(str, 0);
    }

    private static long getsocial(String str, long j) {
        if (str == null) {
            return 0;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public static <T, R> List<R> getsocial(List list, jjbQypPegg jjbqyppegg) {
        if (list == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            arrayList.add(jjbqyppegg.getsocial(obj));
        }
        return arrayList;
    }
}
