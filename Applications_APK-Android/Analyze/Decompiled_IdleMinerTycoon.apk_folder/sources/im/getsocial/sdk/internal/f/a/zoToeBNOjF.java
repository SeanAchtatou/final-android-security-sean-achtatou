package im.getsocial.sdk.internal.f.a;

import java.util.List;

/* compiled from: THActivitiesQuery */
public final class zoToeBNOjF {
    public String acquisition;
    public String attribution;
    public List<String> dau;
    public Integer getsocial;
    public String mobile;
    public Boolean retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof zoToeBNOjF)) {
            return false;
        }
        zoToeBNOjF zotoebnojf = (zoToeBNOjF) obj;
        return (this.getsocial == zotoebnojf.getsocial || (this.getsocial != null && this.getsocial.equals(zotoebnojf.getsocial))) && (this.attribution == zotoebnojf.attribution || (this.attribution != null && this.attribution.equals(zotoebnojf.attribution))) && ((this.acquisition == zotoebnojf.acquisition || (this.acquisition != null && this.acquisition.equals(zotoebnojf.acquisition))) && ((this.mobile == zotoebnojf.mobile || (this.mobile != null && this.mobile.equals(zotoebnojf.mobile))) && ((this.retention == zotoebnojf.retention || (this.retention != null && this.retention.equals(zotoebnojf.retention))) && (this.dau == zotoebnojf.dau || (this.dau != null && this.dau.equals(zotoebnojf.dau))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035;
        if (this.dau != null) {
            i = this.dau.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THActivitiesQuery{limit=" + this.getsocial + ", beforeId=" + this.attribution + ", afterId=" + this.acquisition + ", userId=" + this.mobile + ", friendsFeed=" + this.retention + ", tags=" + this.dau + "}";
    }

    public static void getsocial(im.getsocial.b.a.a.zoToeBNOjF zotoebnojf, zoToeBNOjF zotoebnojf2) {
        if (zotoebnojf2.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 8);
            zotoebnojf.getsocial(zotoebnojf2.getsocial.intValue());
        }
        if (zotoebnojf2.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(zotoebnojf2.attribution);
        }
        if (zotoebnojf2.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(zotoebnojf2.acquisition);
        }
        if (zotoebnojf2.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(zotoebnojf2.mobile);
        }
        if (zotoebnojf2.retention != null) {
            zotoebnojf.getsocial(5, (byte) 2);
            zotoebnojf.getsocial(zotoebnojf2.retention.booleanValue());
        }
        if (zotoebnojf2.dau != null) {
            zotoebnojf.getsocial(6, (byte) 15);
            zotoebnojf.getsocial((byte) 11, zotoebnojf2.dau.size());
            for (String str : zotoebnojf2.dau) {
                zotoebnojf.getsocial(str);
            }
        }
        zotoebnojf.getsocial();
    }
}
