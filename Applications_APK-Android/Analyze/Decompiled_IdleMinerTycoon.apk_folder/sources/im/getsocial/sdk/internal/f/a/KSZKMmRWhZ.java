package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THActivityPostContent */
public final class KSZKMmRWhZ {
    public String acquisition;
    public String attribution;
    public String dau;
    public String getsocial;
    public pdwpUtZXDT mau;
    public String mobile;
    public String retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof KSZKMmRWhZ)) {
            return false;
        }
        KSZKMmRWhZ kSZKMmRWhZ = (KSZKMmRWhZ) obj;
        return (this.getsocial == kSZKMmRWhZ.getsocial || (this.getsocial != null && this.getsocial.equals(kSZKMmRWhZ.getsocial))) && (this.attribution == kSZKMmRWhZ.attribution || (this.attribution != null && this.attribution.equals(kSZKMmRWhZ.attribution))) && ((this.acquisition == kSZKMmRWhZ.acquisition || (this.acquisition != null && this.acquisition.equals(kSZKMmRWhZ.acquisition))) && ((this.mobile == kSZKMmRWhZ.mobile || (this.mobile != null && this.mobile.equals(kSZKMmRWhZ.mobile))) && ((this.retention == kSZKMmRWhZ.retention || (this.retention != null && this.retention.equals(kSZKMmRWhZ.retention))) && ((this.dau == kSZKMmRWhZ.dau || (this.dau != null && this.dau.equals(kSZKMmRWhZ.dau))) && (this.mau == kSZKMmRWhZ.mau || (this.mau != null && this.mau.equals(kSZKMmRWhZ.mau)))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035;
        if (this.mau != null) {
            i = this.mau.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THActivityPostContent{text=" + this.getsocial + ", imageUrl=" + this.attribution + ", buttonTitle=" + this.acquisition + ", buttonAction=" + this.mobile + ", language=" + this.retention + ", videoUrl=" + this.dau + ", action=" + this.mau + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, KSZKMmRWhZ kSZKMmRWhZ) {
        if (kSZKMmRWhZ.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(kSZKMmRWhZ.getsocial);
        }
        if (kSZKMmRWhZ.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(kSZKMmRWhZ.attribution);
        }
        if (kSZKMmRWhZ.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(kSZKMmRWhZ.acquisition);
        }
        if (kSZKMmRWhZ.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(kSZKMmRWhZ.mobile);
        }
        if (kSZKMmRWhZ.retention != null) {
            zotoebnojf.getsocial(5, (byte) 11);
            zotoebnojf.getsocial(kSZKMmRWhZ.retention);
        }
        if (kSZKMmRWhZ.dau != null) {
            zotoebnojf.getsocial(6, (byte) 11);
            zotoebnojf.getsocial(kSZKMmRWhZ.dau);
        }
        if (kSZKMmRWhZ.mau != null) {
            zotoebnojf.getsocial(7, (byte) 12);
            pdwpUtZXDT.getsocial(zotoebnojf, kSZKMmRWhZ.mau);
        }
        zotoebnojf.getsocial();
    }
}
