package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import im.getsocial.b.a.ztWNWCuZiM;
import java.util.HashMap;
import java.util.Map;

/* compiled from: THInviteProvider */
public final class SKUqohGtGQ {
    public KkSvQPDhNi acquisition;

    /* renamed from: android  reason: collision with root package name */
    public Boolean f483android;
    public String attribution;
    public Boolean cat;
    public Boolean dau;
    public Map<String, String> getsocial;
    public QhisXzMgay growth;
    public Boolean ios;
    public Boolean mau;
    public Integer mobile;
    public vkXhnjhKGp organic;
    public String retention;
    public Boolean viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof SKUqohGtGQ)) {
            return false;
        }
        SKUqohGtGQ sKUqohGtGQ = (SKUqohGtGQ) obj;
        return (this.getsocial == sKUqohGtGQ.getsocial || (this.getsocial != null && this.getsocial.equals(sKUqohGtGQ.getsocial))) && (this.attribution == sKUqohGtGQ.attribution || (this.attribution != null && this.attribution.equals(sKUqohGtGQ.attribution))) && ((this.acquisition == sKUqohGtGQ.acquisition || (this.acquisition != null && this.acquisition.equals(sKUqohGtGQ.acquisition))) && ((this.mobile == sKUqohGtGQ.mobile || (this.mobile != null && this.mobile.equals(sKUqohGtGQ.mobile))) && ((this.retention == sKUqohGtGQ.retention || (this.retention != null && this.retention.equals(sKUqohGtGQ.retention))) && ((this.dau == sKUqohGtGQ.dau || (this.dau != null && this.dau.equals(sKUqohGtGQ.dau))) && ((this.mau == sKUqohGtGQ.mau || (this.mau != null && this.mau.equals(sKUqohGtGQ.mau))) && ((this.cat == sKUqohGtGQ.cat || (this.cat != null && this.cat.equals(sKUqohGtGQ.cat))) && ((this.viral == sKUqohGtGQ.viral || (this.viral != null && this.viral.equals(sKUqohGtGQ.viral))) && ((this.organic == sKUqohGtGQ.organic || (this.organic != null && this.organic.equals(sKUqohGtGQ.organic))) && ((this.growth == sKUqohGtGQ.growth || (this.growth != null && this.growth.equals(sKUqohGtGQ.growth))) && ((this.f483android == sKUqohGtGQ.f483android || (this.f483android != null && this.f483android.equals(sKUqohGtGQ.f483android))) && (this.ios == sKUqohGtGQ.ios || (this.ios != null && this.ios.equals(sKUqohGtGQ.ios)))))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035) ^ (this.growth == null ? 0 : this.growth.hashCode())) * -2128831035) ^ (this.f483android == null ? 0 : this.f483android.hashCode())) * -2128831035;
        if (this.ios != null) {
            i = this.ios.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THInviteProvider{name=" + this.getsocial + ", providerId=" + this.attribution + ", os=" + this.acquisition + ", displayOrder=" + this.mobile + ", iconUrl=" + this.retention + ", enabled=" + this.dau + ", supportsInviteText=" + this.mau + ", supportsInviteImageUrl=" + this.cat + ", supportsInviteSubject=" + this.viral + ", properties=" + this.organic + ", inviteContent=" + this.growth + ", supportsGif=" + this.f483android + ", supportsVideo=" + this.ios + "}";
    }

    public static SKUqohGtGQ getsocial(zoToeBNOjF zotoebnojf) {
        SKUqohGtGQ sKUqohGtGQ = new SKUqohGtGQ();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            for (int i = 0; i < mobile2.acquisition; i++) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                            }
                            sKUqohGtGQ.getsocial = hashMap;
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            int organic2 = zotoebnojf.organic();
                            KkSvQPDhNi findByValue = KkSvQPDhNi.findByValue(organic2);
                            if (findByValue != null) {
                                sKUqohGtGQ.acquisition = findByValue;
                                break;
                            } else {
                                ztWNWCuZiM.jjbQypPegg jjbqyppegg = ztWNWCuZiM.jjbQypPegg.PROTOCOL_ERROR;
                                throw new ztWNWCuZiM(jjbqyppegg, "Unexpected value for enum-type THDeviceOs: " + organic2);
                            }
                        }
                    case 4:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.mobile = Integer.valueOf(zotoebnojf.organic());
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.retention = zotoebnojf.ios();
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.dau = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.mau = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.cat = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.viral = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.organic = vkXhnjhKGp.getsocial(zotoebnojf);
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.growth = QhisXzMgay.getsocial(zotoebnojf);
                            break;
                        }
                    case 12:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.f483android = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 13:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            sKUqohGtGQ.ios = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return sKUqohGtGQ;
            }
        }
    }
}
