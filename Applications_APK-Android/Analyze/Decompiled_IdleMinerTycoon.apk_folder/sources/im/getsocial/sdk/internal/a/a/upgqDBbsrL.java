package im.getsocial.sdk.internal.a.a;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.h.jjbQypPegg;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: AnalyticsDispatcherImpl */
public final class upgqDBbsrL implements jjbQypPegg {
    private static final long getsocial = TimeUnit.SECONDS.toMillis(10);
    private final jjbQypPegg acquisition;
    private final ScheduledExecutorService attribution = Executors.newSingleThreadScheduledExecutor();
    private long dau = getsocial;
    private final Runnable mobile = new Runnable() {
        public void run() {
            upgqDBbsrL.getsocial(upgqDBbsrL.this);
        }
    };
    private ScheduledFuture retention;

    @XdbacJlTDQ
    public upgqDBbsrL(jjbQypPegg jjbqyppegg) {
        this.acquisition = jjbqyppegg;
    }

    public final void getsocial() {
        this.retention = this.attribution.scheduleWithFixedDelay(this.mobile, this.dau, this.dau, TimeUnit.MILLISECONDS);
    }

    public final void attribution() {
        if (this.retention != null) {
            this.retention.cancel(false);
            this.retention = null;
        }
        if (this.acquisition.getsocial()) {
            new im.getsocial.sdk.internal.a.h.jjbQypPegg().getsocial(false);
        }
    }

    static /* synthetic */ void getsocial(upgqDBbsrL upgqdbbsrl) {
        if (upgqdbbsrl.acquisition.getsocial()) {
            new im.getsocial.sdk.internal.a.h.jjbQypPegg().getsocial(true);
        }
    }
}
