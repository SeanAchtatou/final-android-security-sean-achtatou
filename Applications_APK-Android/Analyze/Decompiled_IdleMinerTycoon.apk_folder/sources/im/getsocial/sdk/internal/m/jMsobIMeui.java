package im.getsocial.sdk.internal.m;

import android.content.Context;
import android.content.pm.PackageManager;

/* compiled from: PackageUtil */
public final class jMsobIMeui {
    private jMsobIMeui() {
    }

    public static boolean getsocial(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 128);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static boolean attribution(Context context, String str) {
        try {
            return context.getPackageManager().getPackageInfo(str, 128).applicationInfo.enabled;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
