package im.getsocial.sdk.internal.a.g;

import com.facebook.internal.AnalyticsEvents;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.onesignal.shortcutbadger.impl.NewHtcHomeBadger;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.a.b.jjbQypPegg;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.KCGqEGAizh;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.m.HptYHntaqF;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: AnalyticsEventTracker */
public class jjbQypPegg {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(jjbQypPegg.class);
    private final im.getsocial.sdk.internal.a.f.cjrhisSQCL acquisition;
    private final im.getsocial.sdk.internal.c.h.jjbQypPegg attribution;
    private final im.getsocial.sdk.internal.c.j.c.jjbQypPegg dau;
    private final AtomicInteger mau = new AtomicInteger();
    private final KCGqEGAizh mobile;
    private final JbBdMtJmlU retention;

    @XdbacJlTDQ
    jjbQypPegg(im.getsocial.sdk.internal.c.h.jjbQypPegg jjbqyppegg, im.getsocial.sdk.internal.a.f.cjrhisSQCL cjrhissqcl, KCGqEGAizh kCGqEGAizh, JbBdMtJmlU jbBdMtJmlU, im.getsocial.sdk.internal.c.j.c.jjbQypPegg jjbqyppegg2) {
        this.attribution = jjbqyppegg;
        this.acquisition = cjrhissqcl;
        this.mobile = kCGqEGAizh;
        this.retention = jbBdMtJmlU;
        this.dau = jjbqyppegg2;
    }

    public final void getsocial(String str, Map<String, String> map) {
        getsocial(str, map, false);
    }

    private void getsocial(String str, Map<String, String> map, boolean z) {
        im.getsocial.sdk.internal.a.b.jjbQypPegg jjbqyppegg = im.getsocial.sdk.internal.a.b.jjbQypPegg.getsocial(str, map, this.acquisition.getsocial(), UUID.randomUUID().toString(), z, this.mobile.getsocial() ? jjbQypPegg.C0013jjbQypPegg.SERVER_TIME : jjbQypPegg.C0013jjbQypPegg.PENDING);
        jjbqyppegg.getsocial(this.retention.connect());
        getsocial(jjbqyppegg);
    }

    public final void getsocial(GetSocialException getSocialException) {
        HptYHntaqF.jjbQypPegg jjbqyppegg = HptYHntaqF.getsocial(getSocialException);
        if (jjbqyppegg.attribution) {
            StringWriter stringWriter = new StringWriter();
            getSocialException.printStackTrace(new PrintWriter(stringWriter));
            HashMap hashMap = new HashMap();
            hashMap.put(AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, getSocialException.getMessage());
            hashMap.put("error_source", stringWriter.toString());
            hashMap.put("error_key", String.valueOf(getSocialException.getErrorCode()));
            hashMap.put("error_severity", jjbqyppegg.getsocial.name());
            getsocial("sdk_error", hashMap, false);
        }
    }

    public final void getsocial(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        String stringWriter2 = stringWriter.toString();
        if (HptYHntaqF.getsocial(stringWriter2)) {
            HashMap hashMap = new HashMap();
            hashMap.put(AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, th.getMessage());
            hashMap.put("error_source", stringWriter2);
            hashMap.put("error_key", "42");
            getsocial("sdk_error", hashMap, false);
        }
    }

    public final void attribution(String str, Map<String, String> map) {
        getsocial(str, map, true);
    }

    public final void getsocial(PurchaseData purchaseData) {
        getsocial(purchaseData, "manual");
    }

    public final void attribution(PurchaseData purchaseData) {
        getsocial(purchaseData, "auto");
    }

    public final void getsocial(long j, long j2) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_DURATION, String.valueOf(j2 - j));
        getsocial(im.getsocial.sdk.internal.a.b.jjbQypPegg.getsocial("app_session_end", hashMap, j, UUID.randomUUID().toString(), false, jjbQypPegg.C0013jjbQypPegg.LOCAL_TIME));
    }

    private void getsocial(PurchaseData purchaseData, String str) {
        if (!purchaseData.canSend()) {
            cjrhisSQCL cjrhissqcl = getsocial;
            cjrhissqcl.attribution("Purchase data is not valid, won't be send to backend: " + purchaseData);
            return;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("product_id", purchaseData.getProductId());
        hashMap.put("product_type", purchaseData.getProductType().toString());
        hashMap.put("product_title", purchaseData.getProductTitle());
        hashMap.put("price", Float.toString(purchaseData.getPrice()));
        hashMap.put("price_currency", purchaseData.getPriceCurrency());
        hashMap.put("purchase_date", Long.toString(purchaseData.getPurchaseDate()));
        hashMap.put("purchase_id", purchaseData.getPurchaseId());
        hashMap.put("source", str);
        hashMap.put("purchase_state", Long.toString(purchaseData.getValidationResult()));
        getsocial("inapp_purchase", hashMap, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.KSZKMmRWhZ.getsocial(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.KSZKMmRWhZ.getsocial(java.lang.String, int):int
      im.getsocial.sdk.internal.c.KSZKMmRWhZ.getsocial(java.lang.String, boolean):boolean */
    public final void getsocial(String str) {
        boolean z;
        if (!str.contains("trackAnalyticsEvents") && !str.contains("Connecting to hades.")) {
            if (!str.contains("[HADES] Connect to Hades") || !str.contains(AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED)) {
                if (!this.mobile.getsocial()) {
                    z = true;
                } else {
                    im.getsocial.sdk.internal.c.i.upgqDBbsrL attribution2 = this.dau.attribution();
                    z = (attribution2 == null || attribution2.retention() == null) ? false : attribution2.retention().getsocial("remote_logs", false);
                }
                if (z) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("message", str);
                    hashMap.put(NewHtcHomeBadger.COUNT, String.valueOf(this.mau.getAndIncrement()));
                    getsocial("sdk_log", hashMap, false);
                }
            }
        }
    }

    private void getsocial(im.getsocial.sdk.internal.a.b.jjbQypPegg jjbqyppegg) {
        try {
            jjbqyppegg.attribution().put("is_online", String.valueOf(this.attribution.getsocial()));
            new im.getsocial.sdk.internal.a.h.upgqDBbsrL().getsocial(jjbqyppegg);
        } catch (Throwable th) {
            getsocial.getsocial("Failed to track analytics event: %s, error: %s", jjbqyppegg.getsocial(), th.getMessage());
            getsocial.getsocial(th);
        }
    }
}
