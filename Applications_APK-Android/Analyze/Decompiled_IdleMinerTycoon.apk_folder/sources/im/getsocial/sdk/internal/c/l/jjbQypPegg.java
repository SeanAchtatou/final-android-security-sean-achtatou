package im.getsocial.sdk.internal.c.l;

import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import java.util.List;

/* compiled from: Check */
public final class jjbQypPegg {
    public static boolean getsocial(Object obj) {
        return obj != null;
    }

    private jjbQypPegg() {
    }

    public static boolean getsocial(String str) {
        C0021jjbQypPegg.getsocial(getsocial((Object) str), "Don't forget to check if argument is not null before empty check");
        return !str.isEmpty();
    }

    public static boolean attribution(String str) {
        return !acquisition(str);
    }

    public static boolean acquisition(String str) {
        return str == null || str.isEmpty();
    }

    /* compiled from: Check */
    public static final class cjrhisSQCL {
        private cjrhisSQCL() {
        }

        public static void getsocial(boolean z, String str) {
            if (!z) {
                throw new IllegalStateException(str);
            }
        }
    }

    /* compiled from: Check */
    public static final class upgqDBbsrL {
        private upgqDBbsrL() {
        }

        public static void getsocial(boolean z, String str) {
            if (!z) {
                throw new GetSocialException(ErrorCode.NO_INTERNET, str);
            }
        }
    }

    /* renamed from: im.getsocial.sdk.internal.c.l.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: Check */
    public static final class C0021jjbQypPegg {
        private C0021jjbQypPegg() {
        }

        public static void getsocial(boolean z, String str) {
            if (!z) {
                throw new IllegalArgumentException(str);
            }
        }
    }

    public static boolean getsocial(List list) {
        return !(list == null || list.isEmpty());
    }
}
