package im.getsocial.sdk.internal.c.j;

/* compiled from: HadesConfiguration */
public class jjbQypPegg {
    private final boolean attribution;
    private final String getsocial;

    public jjbQypPegg(String str, boolean z) {
        this.getsocial = str;
        this.attribution = z;
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final boolean attribution() {
        return this.attribution;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.attribution ? "https://" : "http://");
        sb.append(this.getsocial);
        return sb.toString();
    }
}
