package im.getsocial.sdk.internal.c.d.a;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;
import im.getsocial.sdk.invites.FetchReferralDataCallback;

/* compiled from: FetchReferralDataCallbackAdapter */
public class XdbacJlTDQ implements jjbQypPegg.C0016jjbQypPegg {
    private final FetchReferralDataCallback getsocial;

    XdbacJlTDQ(FetchReferralDataCallback fetchReferralDataCallback) {
        this.getsocial = fetchReferralDataCallback;
    }

    public final void getsocial(GetSocialException getSocialException) {
        if (this.getsocial != null) {
            this.getsocial.onFailure(getSocialException);
        }
    }
}
