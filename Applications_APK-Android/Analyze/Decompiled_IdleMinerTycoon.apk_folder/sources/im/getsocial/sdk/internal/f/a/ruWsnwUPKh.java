package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THAppPlatformProperties */
public final class ruWsnwUPKh {
    public Boolean a;
    public String acquisition;

    /* renamed from: android  reason: collision with root package name */
    public Boolean f486android;
    public Long attribution;
    public Boolean b;
    public String c;
    public String cat;
    public String connect;
    public String d;
    public String dau;
    public String e;
    public String engage;
    public Boolean feeds;
    public Long getsocial;
    public String growth;
    public Boolean invites;
    public String ios;
    public Boolean marketing;
    public String mau;
    public String mobile;
    public String organic;
    public Boolean referral;
    public String retention;
    public String sharing;
    public String social;
    public String unity;
    public String viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof ruWsnwUPKh)) {
            return false;
        }
        ruWsnwUPKh ruwsnwupkh = (ruWsnwUPKh) obj;
        return (this.getsocial == ruwsnwupkh.getsocial || (this.getsocial != null && this.getsocial.equals(ruwsnwupkh.getsocial))) && (this.attribution == ruwsnwupkh.attribution || (this.attribution != null && this.attribution.equals(ruwsnwupkh.attribution))) && ((this.acquisition == ruwsnwupkh.acquisition || (this.acquisition != null && this.acquisition.equals(ruwsnwupkh.acquisition))) && ((this.mobile == ruwsnwupkh.mobile || (this.mobile != null && this.mobile.equals(ruwsnwupkh.mobile))) && ((this.retention == ruwsnwupkh.retention || (this.retention != null && this.retention.equals(ruwsnwupkh.retention))) && ((this.dau == ruwsnwupkh.dau || (this.dau != null && this.dau.equals(ruwsnwupkh.dau))) && ((this.mau == ruwsnwupkh.mau || (this.mau != null && this.mau.equals(ruwsnwupkh.mau))) && ((this.cat == ruwsnwupkh.cat || (this.cat != null && this.cat.equals(ruwsnwupkh.cat))) && ((this.viral == ruwsnwupkh.viral || (this.viral != null && this.viral.equals(ruwsnwupkh.viral))) && ((this.organic == ruwsnwupkh.organic || (this.organic != null && this.organic.equals(ruwsnwupkh.organic))) && ((this.growth == ruwsnwupkh.growth || (this.growth != null && this.growth.equals(ruwsnwupkh.growth))) && ((this.f486android == ruwsnwupkh.f486android || (this.f486android != null && this.f486android.equals(ruwsnwupkh.f486android))) && ((this.ios == ruwsnwupkh.ios || (this.ios != null && this.ios.equals(ruwsnwupkh.ios))) && ((this.unity == ruwsnwupkh.unity || (this.unity != null && this.unity.equals(ruwsnwupkh.unity))) && ((this.connect == ruwsnwupkh.connect || (this.connect != null && this.connect.equals(ruwsnwupkh.connect))) && ((this.engage == ruwsnwupkh.engage || (this.engage != null && this.engage.equals(ruwsnwupkh.engage))) && ((this.referral == ruwsnwupkh.referral || (this.referral != null && this.referral.equals(ruwsnwupkh.referral))) && ((this.marketing == ruwsnwupkh.marketing || (this.marketing != null && this.marketing.equals(ruwsnwupkh.marketing))) && ((this.invites == ruwsnwupkh.invites || (this.invites != null && this.invites.equals(ruwsnwupkh.invites))) && ((this.feeds == ruwsnwupkh.feeds || (this.feeds != null && this.feeds.equals(ruwsnwupkh.feeds))) && ((this.sharing == ruwsnwupkh.sharing || (this.sharing != null && this.sharing.equals(ruwsnwupkh.sharing))) && ((this.social == ruwsnwupkh.social || (this.social != null && this.social.equals(ruwsnwupkh.social))) && ((this.a == ruwsnwupkh.a || (this.a != null && this.a.equals(ruwsnwupkh.a))) && ((this.b == ruwsnwupkh.b || (this.b != null && this.b.equals(ruwsnwupkh.b))) && ((this.c == ruwsnwupkh.c || (this.c != null && this.c.equals(ruwsnwupkh.c))) && ((this.d == ruwsnwupkh.d || (this.d != null && this.d.equals(ruwsnwupkh.d))) && (this.e == ruwsnwupkh.e || (this.e != null && this.e.equals(ruwsnwupkh.e)))))))))))))))))))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((((((((((((((((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035) ^ (this.growth == null ? 0 : this.growth.hashCode())) * -2128831035) ^ (this.f486android == null ? 0 : this.f486android.hashCode())) * -2128831035) ^ (this.ios == null ? 0 : this.ios.hashCode())) * -2128831035) ^ (this.unity == null ? 0 : this.unity.hashCode())) * -2128831035) ^ (this.connect == null ? 0 : this.connect.hashCode())) * -2128831035) ^ (this.engage == null ? 0 : this.engage.hashCode())) * -2128831035) ^ (this.referral == null ? 0 : this.referral.hashCode())) * -2128831035) ^ (this.marketing == null ? 0 : this.marketing.hashCode())) * -2128831035) ^ (this.invites == null ? 0 : this.invites.hashCode())) * -2128831035) ^ (this.feeds == null ? 0 : this.feeds.hashCode())) * -2128831035) ^ (this.sharing == null ? 0 : this.sharing.hashCode())) * -2128831035) ^ (this.social == null ? 0 : this.social.hashCode())) * -2128831035) ^ (this.a == null ? 0 : this.a.hashCode())) * -2128831035) ^ (this.b == null ? 0 : this.b.hashCode())) * -2128831035) ^ (this.c == null ? 0 : this.c.hashCode())) * -2128831035) ^ (this.d == null ? 0 : this.d.hashCode())) * -2128831035;
        if (this.e != null) {
            i = this.e.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THAppPlatformProperties{pushNotificationsCertificateExpiresDev=" + this.getsocial + ", pushNotificationsCertificateExpiresProd=" + this.attribution + ", pushNotificationsPrivateKeyDev=" + this.acquisition + ", pushNotificationsPrivateKeyProd=" + this.mobile + ", pushNotificationsCertificateDev=" + this.retention + ", pushNotificationsCertificateProd=" + this.dau + ", pushNotificationsEndpointDev=" + this.mau + ", pushNotificationsEndpointProd=" + this.cat + ", pushNotificationsAppKey=" + this.viral + ", pushNotificationsSenderId=" + this.organic + ", pushNotificationsActiveEnvironment=" + this.growth + ", pushNotificationsStatus=" + this.f486android + ", appleTeamId=" + this.ios + ", appleBundleId=" + this.unity + ", idExt=" + this.connect + ", androidCertificateFingerprint=" + this.engage + ", enabled=" + this.referral + ", verifySignature=" + this.marketing + ", pushNotificationsCertificateDevEnabled=" + this.invites + ", pushNotificationsCertificateProdEnabled=" + this.feeds + ", redirectUrl=" + this.sharing + ", customLandingPage=" + this.social + ", iapEnabled=" + this.a + ", iapValidationEnabled=" + this.b + ", iapPublicKey=" + this.c + ", iapSharedSecret=" + this.d + ", iapServiceAccountKey=" + this.e + "}";
    }

    public static ruWsnwUPKh getsocial(zoToeBNOjF zotoebnojf) {
        ruWsnwUPKh ruwsnwupkh = new ruWsnwUPKh();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.getsocial = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.attribution = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.mobile = zotoebnojf.ios();
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.retention = zotoebnojf.ios();
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.dau = zotoebnojf.ios();
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.mau = zotoebnojf.ios();
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.cat = zotoebnojf.ios();
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.viral = zotoebnojf.ios();
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.organic = zotoebnojf.ios();
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.growth = zotoebnojf.ios();
                            break;
                        }
                    case 12:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.f486android = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 13:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.ios = zotoebnojf.ios();
                            break;
                        }
                    case 14:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.unity = zotoebnojf.ios();
                            break;
                        }
                    case 15:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.connect = zotoebnojf.ios();
                            break;
                        }
                    case 16:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.engage = zotoebnojf.ios();
                            break;
                        }
                    case 17:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.referral = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 18:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.marketing = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 19:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.invites = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 20:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.feeds = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 21:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.sharing = zotoebnojf.ios();
                            break;
                        }
                    case 22:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.social = zotoebnojf.ios();
                            break;
                        }
                    case 23:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.a = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 24:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.b = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 25:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.c = zotoebnojf.ios();
                            break;
                        }
                    case 26:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.d = zotoebnojf.ios();
                            break;
                        }
                    case 27:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            ruwsnwupkh.e = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return ruwsnwupkh;
            }
        }
    }
}
