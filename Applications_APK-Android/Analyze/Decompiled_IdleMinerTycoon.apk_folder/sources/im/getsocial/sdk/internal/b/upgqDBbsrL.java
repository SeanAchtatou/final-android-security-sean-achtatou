package im.getsocial.sdk.internal.b;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.Callable;

/* compiled from: SafeListenerFactory */
public class upgqDBbsrL {
    /* access modifiers changed from: private */
    public final jjbQypPegg getsocial;

    @XdbacJlTDQ
    upgqDBbsrL(jjbQypPegg jjbqyppegg) {
        this.getsocial = jjbqyppegg;
    }

    public final <T> T getsocial(Class<T> cls, final T t, final Object obj) {
        return Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new InvocationHandler() {
            public Object invoke(Object obj, final Method method, final Object[] objArr) {
                return upgqDBbsrL.this.getsocial.getsocial(new Callable<Object>() {
                    public Object call() {
                        if (t != null) {
                            return method.invoke(t, objArr);
                        }
                        return obj;
                    }
                }, obj);
            }
        });
    }

    public final <T> T getsocial(Class<T> cls, T t) {
        return getsocial(cls, t, null);
    }
}
