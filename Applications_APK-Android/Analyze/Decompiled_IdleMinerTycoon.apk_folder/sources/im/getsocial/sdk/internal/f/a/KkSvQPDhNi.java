package im.getsocial.sdk.internal.f.a;

/* compiled from: THDeviceOs */
public enum KkSvQPDhNi {
    ANDROID(0),
    IOS(1),
    API(2),
    WEB_ANDROID(3),
    WEB_IOS(4),
    WEB_DESKTOP(5),
    OTHER(6),
    DESKTOP_WINDOWS(7),
    DESKTOP_MAC(8),
    DESKTOP_LINUX(9),
    UNITY_EDITOR(10);
    
    public final int value;

    private KkSvQPDhNi(int i) {
        this.value = i;
    }

    public static KkSvQPDhNi findByValue(int i) {
        switch (i) {
            case 0:
                return ANDROID;
            case 1:
                return IOS;
            case 2:
                return API;
            case 3:
                return WEB_ANDROID;
            case 4:
                return WEB_IOS;
            case 5:
                return WEB_DESKTOP;
            case 6:
                return OTHER;
            case 7:
                return DESKTOP_WINDOWS;
            case 8:
                return DESKTOP_MAC;
            case 9:
                return DESKTOP_LINUX;
            case 10:
                return UNITY_EDITOR;
            default:
                return null;
        }
    }
}
