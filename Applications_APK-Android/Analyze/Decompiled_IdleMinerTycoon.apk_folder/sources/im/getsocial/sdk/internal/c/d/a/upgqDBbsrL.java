package im.getsocial.sdk.internal.c.d.a;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.d.jjbQypPegg;
import im.getsocial.sdk.usermanagement.AddAuthIdentityCallback;

/* compiled from: AddAuthIdentityCallbackAdapter */
public class upgqDBbsrL implements jjbQypPegg.C0016jjbQypPegg {
    private final AddAuthIdentityCallback getsocial;

    upgqDBbsrL(AddAuthIdentityCallback addAuthIdentityCallback) {
        this.getsocial = addAuthIdentityCallback;
    }

    public final void getsocial(GetSocialException getSocialException) {
        if (this.getsocial != null) {
            this.getsocial.onFailure(getSocialException);
        }
    }
}
