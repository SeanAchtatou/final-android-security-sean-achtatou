package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.cjrhisSQCL;
import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: THPrivateUser */
public final class iqXBPEYHZB {
    public String acquisition;
    public String attribution;
    public List<JbBdMtJmlU> cat;
    public Map<String, String> dau;
    public String getsocial;
    public String growth;
    public Map<String, String> mau;
    public String mobile;
    public String organic;
    public Map<String, String> retention;
    public Map<String, String> viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof iqXBPEYHZB)) {
            return false;
        }
        iqXBPEYHZB iqxbpeyhzb = (iqXBPEYHZB) obj;
        return (this.getsocial == iqxbpeyhzb.getsocial || (this.getsocial != null && this.getsocial.equals(iqxbpeyhzb.getsocial))) && (this.attribution == iqxbpeyhzb.attribution || (this.attribution != null && this.attribution.equals(iqxbpeyhzb.attribution))) && ((this.acquisition == iqxbpeyhzb.acquisition || (this.acquisition != null && this.acquisition.equals(iqxbpeyhzb.acquisition))) && ((this.mobile == iqxbpeyhzb.mobile || (this.mobile != null && this.mobile.equals(iqxbpeyhzb.mobile))) && ((this.retention == iqxbpeyhzb.retention || (this.retention != null && this.retention.equals(iqxbpeyhzb.retention))) && ((this.dau == iqxbpeyhzb.dau || (this.dau != null && this.dau.equals(iqxbpeyhzb.dau))) && ((this.mau == iqxbpeyhzb.mau || (this.mau != null && this.mau.equals(iqxbpeyhzb.mau))) && ((this.cat == iqxbpeyhzb.cat || (this.cat != null && this.cat.equals(iqxbpeyhzb.cat))) && ((this.viral == iqxbpeyhzb.viral || (this.viral != null && this.viral.equals(iqxbpeyhzb.viral))) && ((this.organic == iqxbpeyhzb.organic || (this.organic != null && this.organic.equals(iqxbpeyhzb.organic))) && (this.growth == iqxbpeyhzb.growth || (this.growth != null && this.growth.equals(iqxbpeyhzb.growth)))))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035) ^ (this.viral == null ? 0 : this.viral.hashCode())) * -2128831035) ^ (this.organic == null ? 0 : this.organic.hashCode())) * -2128831035;
        if (this.growth != null) {
            i = this.growth.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THPrivateUser{id=" + this.getsocial + ", password=" + this.attribution + ", displayName=" + this.acquisition + ", avatarUrl=" + this.mobile + ", publicProperties=" + this.retention + ", privateProperties=" + this.dau + ", internalPublicProperties=" + this.mau + ", identities=" + this.cat + ", internalPrivateProperties=" + this.viral + ", installDate=" + this.organic + ", installProvider=" + this.growth + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, iqXBPEYHZB iqxbpeyhzb) {
        if (iqxbpeyhzb.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(iqxbpeyhzb.getsocial);
        }
        if (iqxbpeyhzb.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(iqxbpeyhzb.attribution);
        }
        if (iqxbpeyhzb.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(iqxbpeyhzb.acquisition);
        }
        if (iqxbpeyhzb.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(iqxbpeyhzb.mobile);
        }
        if (iqxbpeyhzb.retention != null) {
            zotoebnojf.getsocial(5, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, iqxbpeyhzb.retention.size());
            for (Map.Entry next : iqxbpeyhzb.retention.entrySet()) {
                zotoebnojf.getsocial((String) next.getKey());
                zotoebnojf.getsocial((String) next.getValue());
            }
        }
        if (iqxbpeyhzb.dau != null) {
            zotoebnojf.getsocial(6, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, iqxbpeyhzb.dau.size());
            for (Map.Entry next2 : iqxbpeyhzb.dau.entrySet()) {
                zotoebnojf.getsocial((String) next2.getKey());
                zotoebnojf.getsocial((String) next2.getValue());
            }
        }
        if (iqxbpeyhzb.mau != null) {
            zotoebnojf.getsocial(7, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, iqxbpeyhzb.mau.size());
            for (Map.Entry next3 : iqxbpeyhzb.mau.entrySet()) {
                zotoebnojf.getsocial((String) next3.getKey());
                zotoebnojf.getsocial((String) next3.getValue());
            }
        }
        if (iqxbpeyhzb.cat != null) {
            zotoebnojf.getsocial(8, (byte) 15);
            zotoebnojf.getsocial((byte) 12, iqxbpeyhzb.cat.size());
            for (JbBdMtJmlU jbBdMtJmlU : iqxbpeyhzb.cat) {
                JbBdMtJmlU.getsocial(zotoebnojf, jbBdMtJmlU);
            }
        }
        if (iqxbpeyhzb.viral != null) {
            zotoebnojf.getsocial(9, (byte) 13);
            zotoebnojf.getsocial((byte) 11, (byte) 11, iqxbpeyhzb.viral.size());
            for (Map.Entry next4 : iqxbpeyhzb.viral.entrySet()) {
                zotoebnojf.getsocial((String) next4.getKey());
                zotoebnojf.getsocial((String) next4.getValue());
            }
        }
        if (iqxbpeyhzb.organic != null) {
            zotoebnojf.getsocial(10, (byte) 11);
            zotoebnojf.getsocial(iqxbpeyhzb.organic);
        }
        if (iqxbpeyhzb.growth != null) {
            zotoebnojf.getsocial(11, (byte) 11);
            zotoebnojf.getsocial(iqxbpeyhzb.growth);
        }
        zotoebnojf.getsocial();
    }

    public static iqXBPEYHZB getsocial(zoToeBNOjF zotoebnojf) {
        iqXBPEYHZB iqxbpeyhzb = new iqXBPEYHZB();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            iqxbpeyhzb.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            iqxbpeyhzb.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            iqxbpeyhzb.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            iqxbpeyhzb.mobile = zotoebnojf.ios();
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            iqxbpeyhzb.retention = hashMap;
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile3 = zotoebnojf.mobile();
                            HashMap hashMap2 = new HashMap(mobile3.acquisition);
                            while (i < mobile3.acquisition) {
                                hashMap2.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            iqxbpeyhzb.dau = hashMap2;
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile4 = zotoebnojf.mobile();
                            HashMap hashMap3 = new HashMap(mobile4.acquisition);
                            while (i < mobile4.acquisition) {
                                hashMap3.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            iqxbpeyhzb.mau = hashMap3;
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 15) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cjrhisSQCL retention2 = zotoebnojf.retention();
                            ArrayList arrayList = new ArrayList(retention2.attribution);
                            while (i < retention2.attribution) {
                                arrayList.add(JbBdMtJmlU.getsocial(zotoebnojf));
                                i++;
                            }
                            iqxbpeyhzb.cat = arrayList;
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile5 = zotoebnojf.mobile();
                            HashMap hashMap4 = new HashMap(mobile5.acquisition);
                            while (i < mobile5.acquisition) {
                                hashMap4.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            iqxbpeyhzb.viral = hashMap4;
                            break;
                        }
                    case 10:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            iqxbpeyhzb.organic = zotoebnojf.ios();
                            break;
                        }
                    case 11:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            iqxbpeyhzb.growth = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return iqxbpeyhzb;
            }
        }
    }
}
