package im.getsocial.sdk.internal.m;

import im.getsocial.sdk.ErrorCode;
import im.getsocial.sdk.GetSocialException;
import java.util.HashMap;
import java.util.Map;

/* compiled from: GetSocialExceptionRankingUtil */
public final class HptYHntaqF {
    private static final Map<Integer, upgqDBbsrL> getsocial;

    /* compiled from: GetSocialExceptionRankingUtil */
    public static class jjbQypPegg {
        public final boolean attribution;
        public final upgqDBbsrL getsocial;

        public jjbQypPegg(upgqDBbsrL upgqdbbsrl, boolean z) {
            this.getsocial = upgqdbbsrl;
            this.attribution = z;
        }
    }

    /* compiled from: GetSocialExceptionRankingUtil */
    public enum upgqDBbsrL {
        IGNORE(0),
        APP_ERROR(2),
        INCORRECT_USAGE(4),
        SDK_ERROR(6),
        WTF(9);
        
        private final int _value;

        private upgqDBbsrL(int i) {
            this._value = i;
        }

        public final int getValue() {
            return this._value;
        }
    }

    static {
        HashMap hashMap = new HashMap();
        getsocial = hashMap;
        hashMap.put(0, upgqDBbsrL.WTF);
        getsocial.put(1, upgqDBbsrL.SDK_ERROR);
        getsocial.put(Integer.valueOf((int) ErrorCode.ACTION_DENIED), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.SDK_NOT_INITIALIZED), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.SDK_INITIALIZATION_FAILED), upgqDBbsrL.WTF);
        getsocial.put(204, upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.ILLEGAL_STATE), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.NULL_POINTER), upgqDBbsrL.WTF);
        getsocial.put(Integer.valueOf((int) ErrorCode.NOT_FOUND), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.USER_IS_BANNED), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.PLATFORM_DISABLED), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.APP_SIGNATURE_MISMATCH), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(Integer.valueOf((int) ErrorCode.USERID_TOKEN_MISMATCH), upgqDBbsrL.INCORRECT_USAGE);
        getsocial.put(100, upgqDBbsrL.IGNORE);
        getsocial.put(101, upgqDBbsrL.IGNORE);
        getsocial.put(102, upgqDBbsrL.IGNORE);
        getsocial.put(103, upgqDBbsrL.WTF);
        getsocial.put(Integer.valueOf((int) ErrorCode.CONNECTION_TIMEOUT), upgqDBbsrL.SDK_ERROR);
        getsocial.put(Integer.valueOf((int) ErrorCode.NO_INTERNET), upgqDBbsrL.IGNORE);
        getsocial.put(Integer.valueOf((int) ErrorCode.TRANSPORT_CLOSED), upgqDBbsrL.SDK_ERROR);
        getsocial.put(Integer.valueOf((int) ErrorCode.MEDIAUPLOAD_FAILED), upgqDBbsrL.SDK_ERROR);
        getsocial.put(Integer.valueOf((int) ErrorCode.MEDIAUPLOAD_RESOURCE_NOT_READY), upgqDBbsrL.SDK_ERROR);
    }

    private HptYHntaqF() {
    }

    public static jjbQypPegg getsocial(GetSocialException getSocialException) {
        Throwable cause = getSocialException.getCause();
        boolean z = true;
        if (cause == null) {
            int errorCode = getSocialException.getErrorCode();
            upgqDBbsrL upgqdbbsrl = getsocial.containsKey(Integer.valueOf(errorCode)) ? getsocial.get(Integer.valueOf(errorCode)) : upgqDBbsrL.SDK_ERROR;
            if (upgqdbbsrl == upgqDBbsrL.IGNORE || upgqdbbsrl == upgqDBbsrL.APP_ERROR) {
                z = false;
            }
            return new jjbQypPegg(upgqdbbsrl, z);
        }
        StackTraceElement[] stackTrace = cause.getStackTrace();
        if (cause instanceof IllegalStateException) {
            return stackTrace.length > 0 && stackTrace[0].getClassName().contains(im.getsocial.sdk.internal.c.l.jjbQypPegg.class.getSimpleName()) ? new jjbQypPegg(upgqDBbsrL.INCORRECT_USAGE, false) : new jjbQypPegg(upgqDBbsrL.SDK_ERROR, true);
        } else if (cause instanceof NullPointerException) {
            return getsocial(upgqDBbsrL.APP_ERROR, upgqDBbsrL.WTF, stackTrace);
        } else {
            return cause instanceof IllegalArgumentException ? getsocial(upgqDBbsrL.INCORRECT_USAGE, upgqDBbsrL.SDK_ERROR, stackTrace) : new jjbQypPegg(upgqDBbsrL.SDK_ERROR, true);
        }
    }

    public static boolean getsocial(String str) {
        return str.contains("im.getsocial");
    }

    private static jjbQypPegg getsocial(upgqDBbsrL upgqdbbsrl, upgqDBbsrL upgqdbbsrl2, StackTraceElement... stackTraceElementArr) {
        return stackTraceElementArr.length > 0 && stackTraceElementArr[0].getClassName().contains(GetSocialException.class.getPackage().getName()) ? new jjbQypPegg(upgqdbbsrl2, true) : new jjbQypPegg(upgqdbbsrl, false);
    }
}
