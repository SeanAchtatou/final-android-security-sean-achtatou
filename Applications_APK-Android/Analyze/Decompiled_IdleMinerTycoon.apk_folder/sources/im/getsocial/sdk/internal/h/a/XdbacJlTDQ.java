package im.getsocial.sdk.internal.h.a;

import com.android.billingclient.api.BillingResult;

/* compiled from: Result */
public final class XdbacJlTDQ {
    public final int _code;
    public final String _reason;

    XdbacJlTDQ(BillingResult billingResult) {
        this(billingResult.getResponseCode(), billingResult.getDebugMessage());
    }

    private XdbacJlTDQ(int i, String str) {
        this._code = i;
        this._reason = str;
    }

    XdbacJlTDQ(int i) {
        this(i, "Error");
    }

    public final String toString() {
        return "Result [ code = " + this._code + ", reason = " + this._reason + " ]";
    }
}
