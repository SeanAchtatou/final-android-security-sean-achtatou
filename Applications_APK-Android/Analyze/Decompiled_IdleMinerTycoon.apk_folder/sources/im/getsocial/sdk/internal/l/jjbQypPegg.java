package im.getsocial.sdk.internal.l;

import java.lang.reflect.Method;

/* compiled from: Accessor */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    /* JADX INFO: finally extract failed */
    public static upgqDBbsrL getsocial() {
        Method[] declaredMethods = Class.forName("im.getsocial.sdk.ui.GetSocialUi").getDeclaredMethods();
        int length = declaredMethods.length;
        int i = 0;
        while (i < length) {
            Method method = declaredMethods[i];
            boolean isAccessible = method.isAccessible();
            method.setAccessible(true);
            try {
                if (upgqDBbsrL.class.isAssignableFrom(method.getReturnType())) {
                    upgqDBbsrL upgqdbbsrl = (upgqDBbsrL) method.invoke(null, new Object[0]);
                    method.setAccessible(isAccessible);
                    return upgqdbbsrl;
                }
                method.setAccessible(isAccessible);
                i++;
            } catch (Throwable th) {
                method.setAccessible(isAccessible);
                throw th;
            }
        }
        throw new ClassNotFoundException("No class for UiReflectionHelper");
    }
}
