package im.getsocial.sdk.internal.i.d;

import im.getsocial.sdk.internal.a.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.jMsobIMeui;
import im.getsocial.sdk.internal.c.b.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.j.c.jjbQypPegg;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.zoToeBNOjF;
import java.util.EnumSet;
import java.util.Map;

/* compiled from: ChangeUserContextUseCase */
public final class upgqDBbsrL implements im.getsocial.sdk.internal.c.k.upgqDBbsrL {
    @XdbacJlTDQ
    cjrhisSQCL acquisition;
    @XdbacJlTDQ
    qdyNCsqjKt attribution;
    @XdbacJlTDQ
    pdwpUtZXDT dau;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.a.jjbQypPegg getsocial;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.g.jjbQypPegg mobile;
    @XdbacJlTDQ
    im.getsocial.sdk.internal.c.j.c.jjbQypPegg retention;

    /* compiled from: ChangeUserContextUseCase */
    interface jjbQypPegg {
        void getsocial(jMsobIMeui jmsobimeui);
    }

    public upgqDBbsrL() {
        ztWNWCuZiM.getsocial(this);
    }

    private void getsocial(Runnable runnable) {
        this.getsocial.attribution();
        try {
            runnable.run();
        } finally {
            this.getsocial.getsocial();
        }
    }

    public final void getsocial(final im.getsocial.sdk.usermanagement.a.a.cjrhisSQCL cjrhissqcl) {
        this.mobile.getsocial(this.attribution.acquisition("application_did_become_active_event_timestamp"), this.acquisition.attribution());
        getsocial(new Runnable() {
            public void run() {
                final zoToeBNOjF zotoebnojf = ((im.getsocial.sdk.internal.c.i.upgqDBbsrL) upgqDBbsrL.this.dau.getsocial(im.getsocial.sdk.internal.c.i.upgqDBbsrL.class)).getsocial();
                final im.getsocial.sdk.invites.a.b.upgqDBbsrL upgqdbbsrl = ((im.getsocial.sdk.invites.a.g.upgqDBbsrL) upgqDBbsrL.this.dau.getsocial(im.getsocial.sdk.invites.a.g.upgqDBbsrL.class)).getsocial();
                upgqDBbsrL.getsocial(upgqDBbsrL.this, new jjbQypPegg() {
                    public final void getsocial(jMsobIMeui jmsobimeui) {
                        upgqDBbsrL.this.retention.attribution().getsocial(zotoebnojf);
                        ((im.getsocial.sdk.invites.a.g.upgqDBbsrL) jmsobimeui.getsocial(im.getsocial.sdk.invites.a.g.upgqDBbsrL.class)).getsocial(upgqdbbsrl);
                        new jjbQypPegg().getsocial(jmsobimeui, cjrhissqcl);
                    }
                });
                new im.getsocial.sdk.pushnotifications.a.c.jjbQypPegg().run();
                new im.getsocial.sdk.usermanagement.a.e.XdbacJlTDQ().getsocial();
                upgqDBbsrL.getsocial(upgqDBbsrL.this);
            }
        });
    }

    static /* synthetic */ void getsocial(upgqDBbsrL upgqdbbsrl, jjbQypPegg jjbqyppegg) {
        final jMsobIMeui jmsobimeui = new jMsobIMeui(EnumSet.of(im.getsocial.sdk.internal.c.b.qdyNCsqjKt.SESSION, im.getsocial.sdk.internal.c.b.qdyNCsqjKt.USER));
        upgqdbbsrl.retention.getsocial(new jjbQypPegg.C0019jjbQypPegg() {
            public final im.getsocial.sdk.internal.c.i.upgqDBbsrL getsocial() {
                return (im.getsocial.sdk.internal.c.i.upgqDBbsrL) jmsobimeui.getsocial(im.getsocial.sdk.internal.c.i.upgqDBbsrL.class);
            }
        });
        try {
            jjbqyppegg.getsocial(jmsobimeui);
            upgqdbbsrl.dau.getsocial(jmsobimeui);
        } finally {
            upgqdbbsrl.retention.getsocial();
        }
    }

    static /* synthetic */ void getsocial(upgqDBbsrL upgqdbbsrl) {
        upgqdbbsrl.attribution.getsocial("application_did_become_active_event_timestamp", upgqdbbsrl.acquisition.attribution());
        upgqdbbsrl.mobile.getsocial("app_session_start", (Map<String, String>) null);
    }
}
