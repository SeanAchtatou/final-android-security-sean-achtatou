package im.getsocial.sdk.internal.unity;

import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;

public class InviteChannelPluginAdapter extends InviteChannelPlugin {
    private final InviteChannelPluginInterface attribution;

    public interface InviteChannelPluginInterface {
        boolean isAvailableForDevice(InviteChannel inviteChannel);

        void presentChannelInterface(InviteChannel inviteChannel, InvitePackage invitePackage, InviteCallback inviteCallback);
    }

    public InviteChannelPluginAdapter(InviteChannelPluginInterface inviteChannelPluginInterface) {
        this.attribution = inviteChannelPluginInterface;
    }

    public boolean isAvailableForDevice(InviteChannel inviteChannel) {
        return this.attribution.isAvailableForDevice(inviteChannel);
    }

    public void presentChannelInterface(InviteChannel inviteChannel, InvitePackage invitePackage, InviteCallback inviteCallback) {
        this.attribution.presentChannelInterface(inviteChannel, invitePackage, inviteCallback);
    }
}
