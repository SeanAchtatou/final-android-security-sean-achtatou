package im.getsocial.sdk.internal.g;

import android.graphics.Bitmap;

/* compiled from: ResizeTransformation */
public class upgqDBbsrL implements pdwpUtZXDT {
    protected int attribution;
    protected int getsocial;

    public upgqDBbsrL(int i, int i2) {
        this.getsocial = i;
        this.attribution = i2;
    }

    public Bitmap getsocial(Bitmap bitmap) {
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, this.getsocial, this.attribution, true);
        if (createScaledBitmap != bitmap) {
            bitmap.recycle();
        }
        return createScaledBitmap;
    }

    public int getsocial() {
        return this.getsocial;
    }

    public int attribution() {
        return this.attribution;
    }
}
