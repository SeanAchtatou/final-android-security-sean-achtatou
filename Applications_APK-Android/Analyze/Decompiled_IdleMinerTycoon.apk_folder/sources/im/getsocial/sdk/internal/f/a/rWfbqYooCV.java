package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import im.getsocial.b.a.ztWNWCuZiM;
import java.util.HashMap;
import java.util.Map;

/* compiled from: THError */
public final class rWfbqYooCV {
    public Map<String, String> acquisition;
    public String attribution;
    public QWVUXapsSm getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof rWfbqYooCV)) {
            return false;
        }
        rWfbqYooCV rwfbqyoocv = (rWfbqYooCV) obj;
        return (this.getsocial == rwfbqyoocv.getsocial || (this.getsocial != null && this.getsocial.equals(rwfbqyoocv.getsocial))) && (this.attribution == rwfbqyoocv.attribution || (this.attribution != null && this.attribution.equals(rwfbqyoocv.attribution))) && (this.acquisition == rwfbqyoocv.acquisition || (this.acquisition != null && this.acquisition.equals(rwfbqyoocv.acquisition)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035;
        if (this.acquisition != null) {
            i = this.acquisition.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THError{errorCode=" + this.getsocial + ", errorMsg=" + this.attribution + ", context=" + this.acquisition + "}";
    }

    public static rWfbqYooCV getsocial(zoToeBNOjF zotoebnojf) {
        rWfbqYooCV rwfbqyoocv = new rWfbqYooCV();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 8) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            int organic = zotoebnojf.organic();
                            QWVUXapsSm findByValue = QWVUXapsSm.findByValue(organic);
                            if (findByValue != null) {
                                rwfbqyoocv.getsocial = findByValue;
                                break;
                            } else {
                                ztWNWCuZiM.jjbQypPegg jjbqyppegg = ztWNWCuZiM.jjbQypPegg.PROTOCOL_ERROR;
                                throw new ztWNWCuZiM(jjbqyppegg, "Unexpected value for enum-type THErrorCode: " + organic);
                            }
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            rwfbqyoocv.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile.acquisition);
                            for (int i = 0; i < mobile.acquisition; i++) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                            }
                            rwfbqyoocv.acquisition = hashMap;
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return rwfbqyoocv;
            }
        }
    }
}
