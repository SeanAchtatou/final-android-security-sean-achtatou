package im.getsocial.sdk.internal.c.d;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.GlobalErrorListener;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.l.cjrhisSQCL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.util.concurrent.Callable;

/* compiled from: ExecutionPolicy */
public abstract class jjbQypPegg {
    private GlobalErrorListener attribution = ((GlobalErrorListener) cjrhisSQCL.getsocial(GlobalErrorListener.class));
    @XdbacJlTDQ
    im.getsocial.sdk.internal.a.g.jjbQypPegg getsocial;

    /* renamed from: im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: ExecutionPolicy */
    public interface C0016jjbQypPegg {
        void getsocial(GetSocialException getSocialException);
    }

    /* access modifiers changed from: protected */
    public abstract void getsocial(GetSocialException getSocialException, C0016jjbQypPegg jjbqyppegg);

    public jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(GlobalErrorListener globalErrorListener) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(globalErrorListener), "Error listener could not be null value");
        this.attribution = new im.getsocial.sdk.internal.b.jjbQypPegg(globalErrorListener);
    }

    public final void getsocial() {
        this.attribution = (GlobalErrorListener) cjrhisSQCL.getsocial(GlobalErrorListener.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T
     arg types: [im.getsocial.sdk.internal.c.d.jjbQypPegg$1, boolean]
     candidates:
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(im.getsocial.sdk.GetSocialException, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.lang.Runnable, im.getsocial.sdk.internal.c.d.jjbQypPegg$jjbQypPegg):void
      im.getsocial.sdk.internal.c.d.jjbQypPegg.getsocial(java.util.concurrent.Callable, java.lang.Object):T */
    public final boolean getsocial(final Runnable runnable) {
        return ((Boolean) getsocial((Callable) new Callable<Boolean>() {
            public /* synthetic */ Object call() {
                runnable.run();
                return true;
            }
        }, (Object) false)).booleanValue();
    }

    public final void getsocial(Runnable runnable, C0016jjbQypPegg jjbqyppegg) {
        try {
            runnable.run();
        } catch (Throwable th) {
            attribution(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(th), jjbqyppegg);
        }
    }

    public final <T> T getsocial(Callable callable, Object obj) {
        try {
            return callable.call();
        } catch (Throwable th) {
            attribution(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(th), (C0016jjbQypPegg) cjrhisSQCL.getsocial(C0016jjbQypPegg.class));
            return obj;
        }
    }

    public final void attribution(Runnable runnable) {
        try {
            runnable.run();
        } catch (Throwable th) {
            GetSocialException getSocialException = im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(th);
            getsocial(getSocialException);
            getsocial(getSocialException, (C0016jjbQypPegg) cjrhisSQCL.getsocial(C0016jjbQypPegg.class));
        }
    }

    private void attribution(GetSocialException getSocialException, C0016jjbQypPegg jjbqyppegg) {
        getsocial(getSocialException);
        this.attribution.onError(getSocialException);
        getsocial(getSocialException, jjbqyppegg);
    }

    private void getsocial(GetSocialException getSocialException) {
        if (this.getsocial != null) {
            this.getsocial.getsocial(getSocialException);
        }
    }
}
