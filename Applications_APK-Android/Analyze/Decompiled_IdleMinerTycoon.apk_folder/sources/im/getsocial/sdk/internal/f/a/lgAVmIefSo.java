package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.pdwpUtZXDT;
import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;
import java.util.HashMap;
import java.util.Map;

/* compiled from: THTokenInfo */
public final class lgAVmIefSo {
    public String acquisition;
    public String attribution;
    public Map<String, String> cat;
    public Map<String, String> dau;
    public String getsocial;
    public Boolean mau;
    public Boolean mobile;
    public Map<String, String> retention;
    public String viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof lgAVmIefSo)) {
            return false;
        }
        lgAVmIefSo lgavmiefso = (lgAVmIefSo) obj;
        return (this.getsocial == lgavmiefso.getsocial || (this.getsocial != null && this.getsocial.equals(lgavmiefso.getsocial))) && (this.attribution == lgavmiefso.attribution || (this.attribution != null && this.attribution.equals(lgavmiefso.attribution))) && ((this.acquisition == lgavmiefso.acquisition || (this.acquisition != null && this.acquisition.equals(lgavmiefso.acquisition))) && ((this.mobile == lgavmiefso.mobile || (this.mobile != null && this.mobile.equals(lgavmiefso.mobile))) && ((this.retention == lgavmiefso.retention || (this.retention != null && this.retention.equals(lgavmiefso.retention))) && ((this.dau == lgavmiefso.dau || (this.dau != null && this.dau.equals(lgavmiefso.dau))) && ((this.mau == lgavmiefso.mau || (this.mau != null && this.mau.equals(lgavmiefso.mau))) && ((this.cat == lgavmiefso.cat || (this.cat != null && this.cat.equals(lgavmiefso.cat))) && (this.viral == lgavmiefso.viral || (this.viral != null && this.viral.equals(lgavmiefso.viral)))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035;
        if (this.viral != null) {
            i = this.viral.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THTokenInfo{referrerUserId=" + this.getsocial + ", token=" + this.attribution + ", provider=" + this.acquisition + ", firstMatch=" + this.mobile + ", linkParams=" + this.retention + ", internalData=" + this.dau + ", guaranteedMatch=" + this.mau + ", originalData=" + this.cat + ", debug=" + this.viral + "}";
    }

    public static lgAVmIefSo getsocial(zoToeBNOjF zotoebnojf) {
        lgAVmIefSo lgavmiefso = new lgAVmIefSo();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                int i = 0;
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            lgavmiefso.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            lgavmiefso.attribution = zotoebnojf.ios();
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            lgavmiefso.acquisition = zotoebnojf.ios();
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            lgavmiefso.mobile = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile2 = zotoebnojf.mobile();
                            HashMap hashMap = new HashMap(mobile2.acquisition);
                            while (i < mobile2.acquisition) {
                                hashMap.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            lgavmiefso.retention = hashMap;
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile3 = zotoebnojf.mobile();
                            HashMap hashMap2 = new HashMap(mobile3.acquisition);
                            while (i < mobile3.acquisition) {
                                hashMap2.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            lgavmiefso.dau = hashMap2;
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            lgavmiefso.mau = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 13) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            pdwpUtZXDT mobile4 = zotoebnojf.mobile();
                            HashMap hashMap3 = new HashMap(mobile4.acquisition);
                            while (i < mobile4.acquisition) {
                                hashMap3.put(zotoebnojf.ios(), zotoebnojf.ios());
                                i++;
                            }
                            lgavmiefso.cat = hashMap3;
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            lgavmiefso.viral = zotoebnojf.ios();
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return lgavmiefso;
            }
        }
    }
}
