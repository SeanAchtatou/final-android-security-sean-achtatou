package im.getsocial.sdk.internal.f.a;

/* compiled from: THUploadType */
public enum JqGHVdWCqI {
    IMAGE(0),
    VIDEO(1);
    
    public final int value;

    private JqGHVdWCqI(int i) {
        this.value = i;
    }

    public static JqGHVdWCqI findByValue(int i) {
        switch (i) {
            case 0:
                return IMAGE;
            case 1:
                return VIDEO;
            default:
                return null;
        }
    }
}
