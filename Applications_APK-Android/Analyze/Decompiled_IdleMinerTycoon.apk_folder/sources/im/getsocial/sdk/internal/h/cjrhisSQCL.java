package im.getsocial.sdk.internal.h;

import android.content.Context;
import android.os.Looper;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.h.a.pdwpUtZXDT;
import im.getsocial.sdk.internal.h.a.zoToeBNOjF;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: PurchaseDataCollectorBillingClient */
public class cjrhisSQCL implements jjbQypPegg {
    /* access modifiers changed from: private */
    public static final im.getsocial.sdk.internal.c.f.cjrhisSQCL acquisition = upgqDBbsrL.getsocial(cjrhisSQCL.class);
    private static final BigDecimal attribution = new BigDecimal(1000000);
    private Runnable dau;
    @XdbacJlTDQ
    Context getsocial;
    private final upgqDBbsrL mau;
    /* access modifiers changed from: private */
    public final Map<String, SkuDetails> mobile = new HashMap();
    /* access modifiers changed from: private */
    public BillingClient retention;

    /* compiled from: PurchaseDataCollectorBillingClient */
    interface jjbQypPegg {
        void getsocial();
    }

    public final void getsocial() {
    }

    public cjrhisSQCL(upgqDBbsrL upgqdbbsrl) {
        ztWNWCuZiM.getsocial(this);
        this.mau = upgqdbbsrl;
    }

    private void getsocial(boolean z, Runnable runnable) {
        im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = acquisition;
        cjrhissqcl.attribution("Setup billing thread, forced: " + z);
        this.dau = runnable;
        if (this.retention == null || z) {
            try {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }
                this.retention = BillingClient.newBuilder(this.getsocial).setListener(new pdwpUtZXDT()).build();
                this.retention.startConnection(new im.getsocial.sdk.internal.h.a.jjbQypPegg() {
                    public void onSetupFinished(im.getsocial.sdk.internal.h.a.XdbacJlTDQ xdbacJlTDQ) {
                        im.getsocial.sdk.internal.c.f.cjrhisSQCL mobile = cjrhisSQCL.acquisition;
                        mobile.attribution("Billing client setup finished, response: " + xdbacJlTDQ);
                        cjrhisSQCL.this.retention();
                    }
                });
            } catch (Throwable th) {
                acquisition.attribution("Could not start BillingClient.");
                acquisition.getsocial(th);
            }
        } else {
            retention();
        }
    }

    public static boolean acquisition() {
        try {
            Class.forName("com.android.billingclient.api.BillingClient");
            return true;
        } catch (Exception e) {
            acquisition.attribution("Failed to find Billing Client class.");
            acquisition.getsocial(e);
            return false;
        }
    }

    private void attribution(List<PurchaseData.Builder> list, String str, String str2) {
        try {
            for (PurchaseData.Builder next : list) {
                SkuDetails skuDetails = this.mobile.get(next.getProductId());
                if (skuDetails != null) {
                    next.withPrice(new BigDecimal(skuDetails.getPriceAmountMicros()).divide(attribution).floatValue()).withProductTitle(skuDetails.getTitle()).withPriceCurrency(skuDetails.getPriceCurrencyCode()).withProductType("subs".equalsIgnoreCase(str) ? PurchaseData.ProductType.SUBSCRIPTION : PurchaseData.ProductType.ITEM);
                    this.mau.getsocial(next.build(), str, str2);
                }
            }
        } catch (Throwable th) {
            acquisition.attribution("Error while updating purchases with product details.");
            acquisition.getsocial(th);
        }
    }

    /* access modifiers changed from: private */
    public void retention() {
        if (this.dau != null) {
            acquisition.attribution("Calling cached runnable");
            this.dau.run();
        }
    }

    public final void getsocial(List<PurchaseData.Builder> list, final String str, String str2) {
        final ArrayList arrayList = new ArrayList();
        for (PurchaseData.Builder next : list) {
            if (!this.mobile.containsKey(next.getProductId())) {
                arrayList.add(next.getProductId());
            }
        }
        try {
            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl = acquisition;
            cjrhissqcl.attribution("Getting product details of " + arrayList);
            if (arrayList.isEmpty()) {
                attribution(list, str, str2);
            } else {
                getsocial(true, new Runnable() {
                    public void run() {
                        try {
                            cjrhisSQCL.this.retention.querySkuDetailsAsync(SkuDetailsParams.newBuilder().setType(str).setSkusList(arrayList).build(), new zoToeBNOjF() {
                                public void onResponse(im.getsocial.sdk.internal.h.a.XdbacJlTDQ xdbacJlTDQ, List<SkuDetails> list) {
                                    im.getsocial.sdk.internal.c.f.cjrhisSQCL mobile = cjrhisSQCL.acquisition;
                                    mobile.attribution("Received product details, result:" + xdbacJlTDQ);
                                    if (xdbacJlTDQ._code == 0 && list != null) {
                                        for (SkuDetails next : list) {
                                            cjrhisSQCL.this.mobile.put(next.getSku(), next);
                                        }
                                    }
                                }
                            });
                        } catch (Throwable th) {
                            cjrhisSQCL.acquisition.attribution("Error while getting product details.");
                            cjrhisSQCL.acquisition.getsocial(th);
                        }
                    }
                });
            }
        } catch (Throwable th) {
            acquisition.attribution("Error while fetching product details.");
            acquisition.getsocial(th);
        }
    }

    public final void attribution() {
        acquisition.attribution("Checking purchase history");
        getsocial(false, new Runnable() {
            public void run() {
                cjrhisSQCL.getsocial(cjrhisSQCL.this, "inapp", new jjbQypPegg() {
                    public final void getsocial() {
                        cjrhisSQCL.getsocial(cjrhisSQCL.this, "subs", (jjbQypPegg) null);
                    }
                });
            }
        });
    }

    static /* synthetic */ void getsocial(cjrhisSQCL cjrhissqcl, final String str, final jjbQypPegg jjbqyppegg) {
        try {
            im.getsocial.sdk.internal.c.f.cjrhisSQCL cjrhissqcl2 = acquisition;
            cjrhissqcl2.attribution("Reading purchase history, product type: " + str);
            cjrhissqcl.getsocial(true, new Runnable() {
                public void run() {
                    try {
                        cjrhisSQCL.acquisition.attribution("Invoke queryPurchaseHistoryAsync method to get purchase history");
                        cjrhisSQCL.this.retention.queryPurchaseHistoryAsync(str, new im.getsocial.sdk.internal.h.a.upgqDBbsrL() {
                            public final void getsocial(im.getsocial.sdk.internal.h.a.XdbacJlTDQ xdbacJlTDQ, List<im.getsocial.sdk.internal.h.a.cjrhisSQCL> list) {
                                im.getsocial.sdk.internal.c.f.cjrhisSQCL mobile = cjrhisSQCL.acquisition;
                                mobile.attribution("QueryPurchaseHistoryAsync response: " + xdbacJlTDQ);
                                if (xdbacJlTDQ._code == 0) {
                                    im.getsocial.sdk.internal.c.f.cjrhisSQCL mobile2 = cjrhisSQCL.acquisition;
                                    mobile2.attribution("purchase history response code:" + xdbacJlTDQ._code + ", size: " + list.size());
                                    cjrhisSQCL.getsocial(cjrhisSQCL.this, list, str);
                                }
                                if (jjbqyppegg != null) {
                                    jjbqyppegg.getsocial();
                                }
                            }
                        });
                    } catch (Throwable th) {
                        cjrhisSQCL.acquisition.attribution("Error while reading purchase history.");
                        cjrhisSQCL.acquisition.getsocial(th);
                    }
                }
            });
        } catch (Throwable th) {
            acquisition.attribution("Error while reading purchase history.");
            acquisition.getsocial(th);
        }
    }

    static /* synthetic */ void getsocial(cjrhisSQCL cjrhissqcl, List list, String str) {
        try {
            im.getsocial.a.a.upgqDBbsrL upgqdbbsrl = new im.getsocial.a.a.upgqDBbsrL();
            im.getsocial.a.a.upgqDBbsrL upgqdbbsrl2 = new im.getsocial.a.a.upgqDBbsrL();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                im.getsocial.sdk.internal.h.a.cjrhisSQCL cjrhissqcl2 = (im.getsocial.sdk.internal.h.a.cjrhisSQCL) it.next();
                upgqdbbsrl.add(cjrhissqcl2._json);
                upgqdbbsrl2.add(cjrhissqcl2._signature);
            }
            if (upgqdbbsrl.size() > 0 && upgqdbbsrl.size() == upgqdbbsrl2.size()) {
                cjrhissqcl.mau.getsocial(im.getsocial.a.a.upgqDBbsrL.getsocial(upgqdbbsrl).getBytes("UTF-8"), im.getsocial.a.a.upgqDBbsrL.getsocial(upgqdbbsrl2).getBytes("UTF-8"), str, false);
            }
        } catch (Throwable th) {
            acquisition.attribution("Error while validating purchases.");
            acquisition.getsocial(th);
        }
    }
}
