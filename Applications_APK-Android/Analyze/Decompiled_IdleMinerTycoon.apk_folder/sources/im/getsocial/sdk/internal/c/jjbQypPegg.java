package im.getsocial.sdk.internal.c;

import android.content.Context;
import android.content.pm.PackageManager;
import com.facebook.appevents.AppEventsConstants;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Locale;

/* compiled from: AndroidAppSignature */
public final class jjbQypPegg implements ztWNWCuZiM {
    private static String attribution = "";
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(jjbQypPegg.class);
    private final Context acquisition;

    @XdbacJlTDQ
    jjbQypPegg(Context context) {
        this.acquisition = context;
    }

    public final String getsocial() {
        return String.format("Package: %s, Signing-certificate fingerprint: %s", this.acquisition.getPackageName(), getsocial(this.acquisition));
    }

    public final String attribution() {
        return getsocial(this.acquisition);
    }

    private static String getsocial(Context context) {
        if (im.getsocial.sdk.internal.c.l.jjbQypPegg.acquisition(attribution)) {
            try {
                attribution = getsocial(MessageDigest.getInstance("SHA256").digest(((X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray()))).getEncoded()));
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("App signature fingerprint: " + attribution);
            } catch (PackageManager.NameNotFoundException e) {
                getsocial.acquisition("Failed to get signing certificate SHA256 fingerprint", e);
            } catch (NoSuchAlgorithmException e2) {
                getsocial.acquisition("Failed to get signing certificate SHA256 fingerprint", e2);
            } catch (CertificateException e3) {
                getsocial.acquisition("Failed to get signing certificate SHA256 fingerprint", e3);
            }
        }
        return attribution;
    }

    private static String getsocial(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length << 1);
        for (int i = 0; i < bArr.length; i++) {
            String hexString = Integer.toHexString(bArr[i]);
            int length = hexString.length();
            if (length == 1) {
                hexString = AppEventsConstants.EVENT_PARAM_VALUE_NO + hexString;
            }
            if (length > 2) {
                hexString = hexString.substring(length - 2, length);
            }
            sb.append(hexString.toUpperCase(Locale.US));
            if (i < bArr.length - 1) {
                sb.append(':');
            }
        }
        return sb.toString();
    }
}
