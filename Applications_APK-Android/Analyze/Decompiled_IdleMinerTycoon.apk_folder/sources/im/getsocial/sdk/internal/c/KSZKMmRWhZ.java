package im.getsocial.sdk.internal.c;

import java.util.HashMap;
import java.util.Map;

/* compiled from: ApplicationInfo */
public final class KSZKMmRWhZ {
    private final Map<String, String> acquisition;
    private final String attribution;
    private final String getsocial;

    public KSZKMmRWhZ(String str, String str2, Map<String, String> map) {
        this.getsocial = str;
        this.attribution = str2;
        this.acquisition = map == null ? new HashMap() : new HashMap(map);
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final String attribution() {
        return this.attribution;
    }

    public final int getsocial(String str, int i) {
        if (this.acquisition.containsKey(str)) {
            return Integer.parseInt(this.acquisition.get(str));
        }
        return 0;
    }

    public final boolean getsocial(String str, boolean z) {
        if (this.acquisition.containsKey(str)) {
            return Boolean.parseBoolean(this.acquisition.get(str));
        }
        return false;
    }
}
