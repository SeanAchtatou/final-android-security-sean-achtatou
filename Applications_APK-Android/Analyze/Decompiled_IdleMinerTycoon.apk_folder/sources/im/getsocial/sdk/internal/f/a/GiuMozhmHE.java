package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THUsersQuery */
public final class GiuMozhmHE {
    public String attribution;
    public Integer getsocial;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof GiuMozhmHE)) {
            return false;
        }
        GiuMozhmHE giuMozhmHE = (GiuMozhmHE) obj;
        return (this.getsocial == giuMozhmHE.getsocial || (this.getsocial != null && this.getsocial.equals(giuMozhmHE.getsocial))) && (this.attribution == giuMozhmHE.attribution || (this.attribution != null && this.attribution.equals(giuMozhmHE.attribution)));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035;
        if (this.attribution != null) {
            i = this.attribution.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THUsersQuery{limit=" + this.getsocial + ", name=" + this.attribution + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, GiuMozhmHE giuMozhmHE) {
        if (giuMozhmHE.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 8);
            zotoebnojf.getsocial(giuMozhmHE.getsocial.intValue());
        }
        if (giuMozhmHE.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(giuMozhmHE.attribution);
        }
        zotoebnojf.getsocial();
    }
}
