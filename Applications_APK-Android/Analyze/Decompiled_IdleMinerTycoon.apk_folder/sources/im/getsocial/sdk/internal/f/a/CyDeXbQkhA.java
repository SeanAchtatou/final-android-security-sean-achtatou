package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.d.jjbQypPegg;

/* compiled from: THSdkAuthResponse */
public final class CyDeXbQkhA {
    public Long acquisition;
    public iqXBPEYHZB attribution;
    public xEJQVTBATI cat;
    public jMsobIMeui dau;
    public String getsocial;
    public String mau;
    public ruWsnwUPKh mobile;
    public Boolean retention;
    public Long viral;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof CyDeXbQkhA)) {
            return false;
        }
        CyDeXbQkhA cyDeXbQkhA = (CyDeXbQkhA) obj;
        return (this.getsocial == cyDeXbQkhA.getsocial || (this.getsocial != null && this.getsocial.equals(cyDeXbQkhA.getsocial))) && (this.attribution == cyDeXbQkhA.attribution || (this.attribution != null && this.attribution.equals(cyDeXbQkhA.attribution))) && ((this.acquisition == cyDeXbQkhA.acquisition || (this.acquisition != null && this.acquisition.equals(cyDeXbQkhA.acquisition))) && ((this.mobile == cyDeXbQkhA.mobile || (this.mobile != null && this.mobile.equals(cyDeXbQkhA.mobile))) && ((this.retention == cyDeXbQkhA.retention || (this.retention != null && this.retention.equals(cyDeXbQkhA.retention))) && ((this.dau == cyDeXbQkhA.dau || (this.dau != null && this.dau.equals(cyDeXbQkhA.dau))) && ((this.mau == cyDeXbQkhA.mau || (this.mau != null && this.mau.equals(cyDeXbQkhA.mau))) && ((this.cat == cyDeXbQkhA.cat || (this.cat != null && this.cat.equals(cyDeXbQkhA.cat))) && (this.viral == cyDeXbQkhA.viral || (this.viral != null && this.viral.equals(cyDeXbQkhA.viral)))))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035) ^ (this.mau == null ? 0 : this.mau.hashCode())) * -2128831035) ^ (this.cat == null ? 0 : this.cat.hashCode())) * -2128831035;
        if (this.viral != null) {
            i = this.viral.hashCode();
        }
        return (hashCode ^ i) * -2128831035;
    }

    public final String toString() {
        return "THSdkAuthResponse{sessionId=" + this.getsocial + ", user=" + this.attribution + ", serverTime=" + this.acquisition + ", appPlatformProperties=" + this.mobile + ", firstSession=" + this.retention + ", applicationInfo=" + this.dau + ", uploadEndpoint=" + this.mau + ", uploadChunkSize=" + this.cat + ", uploadFileSizeLimit=" + this.viral + "}";
    }

    public static CyDeXbQkhA getsocial(zoToeBNOjF zotoebnojf) {
        CyDeXbQkhA cyDeXbQkhA = new CyDeXbQkhA();
        while (true) {
            upgqDBbsrL acquisition2 = zotoebnojf.acquisition();
            if (acquisition2.attribution != 0) {
                switch (acquisition2.acquisition) {
                    case 1:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.getsocial = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.attribution = iqXBPEYHZB.getsocial(zotoebnojf);
                            break;
                        }
                    case 3:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.acquisition = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    case 4:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.mobile = ruWsnwUPKh.getsocial(zotoebnojf);
                            break;
                        }
                    case 5:
                        if (acquisition2.attribution != 2) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.retention = Boolean.valueOf(zotoebnojf.mau());
                            break;
                        }
                    case 6:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.dau = jMsobIMeui.getsocial(zotoebnojf);
                            break;
                        }
                    case 7:
                        if (acquisition2.attribution != 11) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.mau = zotoebnojf.ios();
                            break;
                        }
                    case 8:
                        if (acquisition2.attribution != 12) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.cat = xEJQVTBATI.getsocial(zotoebnojf);
                            break;
                        }
                    case 9:
                        if (acquisition2.attribution != 10) {
                            jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                            break;
                        } else {
                            cyDeXbQkhA.viral = Long.valueOf(zotoebnojf.growth());
                            break;
                        }
                    default:
                        jjbQypPegg.getsocial(zotoebnojf, acquisition2.attribution);
                        break;
                }
            } else {
                return cyDeXbQkhA;
            }
        }
    }
}
