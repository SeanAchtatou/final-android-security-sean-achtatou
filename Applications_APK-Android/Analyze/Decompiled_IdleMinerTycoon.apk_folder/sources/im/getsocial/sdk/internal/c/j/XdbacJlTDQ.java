package im.getsocial.sdk.internal.c.j;

import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.activities.ReportingReason;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.KkSvQPDhNi;
import im.getsocial.sdk.internal.c.KluUZYuxme;
import im.getsocial.sdk.internal.f.a.BpPZzHFMaU;
import im.getsocial.sdk.internal.f.a.CyDeXbQkhA;
import im.getsocial.sdk.internal.f.a.FvojpKUsoc;
import im.getsocial.sdk.internal.f.a.GiuMozhmHE;
import im.getsocial.sdk.internal.f.a.HptYHntaqF;
import im.getsocial.sdk.internal.f.a.JWvbLzaedN;
import im.getsocial.sdk.internal.f.a.KCGqEGAizh;
import im.getsocial.sdk.internal.f.a.KSZKMmRWhZ;
import im.getsocial.sdk.internal.f.a.KdkQzTlDzz;
import im.getsocial.sdk.internal.f.a.NgDaxXOnCs;
import im.getsocial.sdk.internal.f.a.RbduBRVrSj;
import im.getsocial.sdk.internal.f.a.UwIeQkAzJH;
import im.getsocial.sdk.internal.f.a.VuXsWfriFX;
import im.getsocial.sdk.internal.f.a.YgeTlQwUNa;
import im.getsocial.sdk.internal.f.a.eTZqdsudqh;
import im.getsocial.sdk.internal.f.a.gnmRLOtbDV;
import im.getsocial.sdk.internal.f.a.iFpupLCESp;
import im.getsocial.sdk.internal.f.a.icjTFWWVFN;
import im.getsocial.sdk.internal.f.a.iqXBPEYHZB;
import im.getsocial.sdk.internal.f.a.krCuuqytsv;
import im.getsocial.sdk.internal.f.a.lgAVmIefSo;
import im.getsocial.sdk.internal.f.a.nGNJgptECj;
import im.getsocial.sdk.internal.f.a.pKlfFxuShO;
import im.getsocial.sdk.internal.f.a.rFvvVpjzZH;
import im.getsocial.sdk.internal.f.a.sdizKTglGl;
import im.getsocial.sdk.internal.f.a.sqEuGXwfLT;
import im.getsocial.sdk.internal.f.a.vWMekQpooZ;
import im.getsocial.sdk.internal.f.a.xAXgtBkRbG;
import im.getsocial.sdk.internal.f.a.xlPHPMtUBa;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.ReferralData;
import im.getsocial.sdk.invites.ReferredUser;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationsCountQuery;
import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import im.getsocial.sdk.pushnotifications.NotificationsSummary;
import im.getsocial.sdk.socialgraph.SuggestedFriend;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.PrivateUser;
import im.getsocial.sdk.usermanagement.PublicUser;
import im.getsocial.sdk.usermanagement.UserReference;
import im.getsocial.sdk.usermanagement.UsersQuery;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: SyncThriftyCommunicationLayer */
public class XdbacJlTDQ implements im.getsocial.sdk.internal.c.a.jjbQypPegg {
    private static final im.getsocial.sdk.internal.c.f.cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(XdbacJlTDQ.class);
    private final im.getsocial.sdk.internal.c.j.c.jjbQypPegg acquisition;
    private final im.getsocial.sdk.internal.c.j.b.jjbQypPegg attribution;

    /* compiled from: SyncThriftyCommunicationLayer */
    private interface upgqDBbsrL<T> {
        T getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl);
    }

    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    XdbacJlTDQ(im.getsocial.sdk.internal.c.j.b.jjbQypPegg jjbqyppegg, im.getsocial.sdk.internal.c.j.c.jjbQypPegg jjbqyppegg2) {
        this.attribution = jjbqyppegg;
        this.acquisition = jjbqyppegg2;
    }

    public final im.getsocial.sdk.usermanagement.a.a.upgqDBbsrL getsocial(im.getsocial.sdk.usermanagement.a.a.jjbQypPegg jjbqyppegg) {
        final RbduBRVrSj rbduBRVrSj = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(jjbqyppegg);
        AnonymousClass1 r0 = new upgqDBbsrL<CyDeXbQkhA>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(rbduBRVrSj);
            }
        };
        return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((CyDeXbQkhA) getsocial(r0, "authenticateSdk", "request: " + rbduBRVrSj));
    }

    public final im.getsocial.sdk.internal.i.b.upgqDBbsrL getsocial(im.getsocial.sdk.internal.i.b.jjbQypPegg jjbqyppegg) {
        final vWMekQpooZ vwmekqpooz = new vWMekQpooZ();
        vwmekqpooz.getsocial = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(jjbqyppegg.getsocial);
        KdkQzTlDzz kdkQzTlDzz = new KdkQzTlDzz();
        kdkQzTlDzz.getsocial = im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial(jjbqyppegg.attribution.getsocial);
        kdkQzTlDzz.attribution = Boolean.valueOf(jjbqyppegg.attribution.attribution);
        kdkQzTlDzz.acquisition = im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial(jjbqyppegg.attribution.acquisition);
        kdkQzTlDzz.mobile = jjbqyppegg.attribution.mobile;
        vwmekqpooz.attribution = kdkQzTlDzz;
        AnonymousClass12 r7 = new upgqDBbsrL<VuXsWfriFX>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(vwmekqpooz);
            }
        };
        VuXsWfriFX vuXsWfriFX = (VuXsWfriFX) getsocial(r7, "3in1", "request: " + vwmekqpooz);
        getsocial.getsocial("[HADES] Call `%s` response:", "3in1");
        getsocial.getsocial("[HADES] AuthResponse: `%s`", vuXsWfriFX.getsocial);
        getsocial.getsocial("[HADES] ReferralData: `%s`", vuXsWfriFX.attribution);
        getsocial.getsocial("[HADES] InviteChannels: `%s`", vuXsWfriFX.acquisition);
        im.getsocial.sdk.internal.i.b.upgqDBbsrL upgqdbbsrl = new im.getsocial.sdk.internal.i.b.upgqDBbsrL();
        upgqdbbsrl.getsocial = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(vuXsWfriFX.getsocial);
        upgqdbbsrl.acquisition = im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial(vuXsWfriFX.acquisition);
        upgqdbbsrl.attribution = im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial(vuXsWfriFX.attribution);
        return upgqdbbsrl;
    }

    public final ReferralData getsocial(KluUZYuxme kluUZYuxme, boolean z, Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, Map<String, String>> map, Map<String, String> map2) {
        final KdkQzTlDzz kdkQzTlDzz = new KdkQzTlDzz();
        kdkQzTlDzz.getsocial = im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial(kluUZYuxme);
        kdkQzTlDzz.attribution = Boolean.valueOf(z);
        kdkQzTlDzz.acquisition = im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial(map);
        kdkQzTlDzz.mobile = map2;
        AnonymousClass23 r2 = new upgqDBbsrL<lgAVmIefSo>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), kdkQzTlDzz);
            }
        };
        return im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial((lgAVmIefSo) getsocial(r2, "processAppOpen", "request: " + kdkQzTlDzz));
    }

    public final void getsocial(JbBdMtJmlU jbBdMtJmlU) {
        final eTZqdsudqh etzqdsudqh = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(jbBdMtJmlU);
        AnonymousClass34 r0 = new upgqDBbsrL<Boolean>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), etzqdsudqh);
            }
        };
        getsocial(r0, "updateSession", "properties: " + etzqdsudqh);
    }

    public final PrivateUser getsocial(im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT pdwputzxdt) {
        final iqXBPEYHZB iqxbpeyhzb = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(pdwputzxdt);
        AnonymousClass45 r0 = new upgqDBbsrL<iqXBPEYHZB>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), iqxbpeyhzb);
            }
        };
        return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((iqXBPEYHZB) getsocial(r0, "updateUser", "userUpdate: " + iqxbpeyhzb));
    }

    public final PrivateUser getsocial(AuthIdentity authIdentity) {
        final im.getsocial.sdk.internal.f.a.JbBdMtJmlU jbBdMtJmlU = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(authIdentity);
        AnonymousClass50 r0 = new upgqDBbsrL<iqXBPEYHZB>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), jbBdMtJmlU);
            }
        };
        return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((iqXBPEYHZB) getsocial(r0, "addIdentity", "identity: " + jbBdMtJmlU));
    }

    public final PrivateUser getsocial(String str) {
        final im.getsocial.sdk.internal.f.a.JbBdMtJmlU jbBdMtJmlU = new im.getsocial.sdk.internal.f.a.JbBdMtJmlU();
        jbBdMtJmlU.getsocial = str;
        AnonymousClass51 r5 = new upgqDBbsrL<iqXBPEYHZB>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), jbBdMtJmlU);
            }
        };
        return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((iqXBPEYHZB) getsocial(r5, "removeFriend", "identity: " + jbBdMtJmlU));
    }

    public final PrivateUser attribution(AuthIdentity authIdentity) {
        final im.getsocial.sdk.internal.f.a.JbBdMtJmlU jbBdMtJmlU = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(authIdentity);
        AnonymousClass52 r0 = new upgqDBbsrL<iqXBPEYHZB>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.acquisition(XdbacJlTDQ.this.acquisition.attribution().acquisition(), jbBdMtJmlU);
            }
        };
        return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((iqXBPEYHZB) getsocial(r0, "getPrivateUserByIdentity", "identity: " + jbBdMtJmlU));
    }

    public final List<UserReference> getsocial(UsersQuery usersQuery) {
        final GiuMozhmHE giuMozhmHE = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(usersQuery);
        AnonymousClass53 r0 = new upgqDBbsrL<List<gnmRLOtbDV>>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), giuMozhmHE);
            }
        };
        return new ztWNWCuZiM((byte) 0).getsocial((List) getsocial(r0, "findUsers", "query: " + giuMozhmHE));
    }

    public final List<ReferredUser> getsocial() {
        return new cjrhisSQCL<FvojpKUsoc, ReferredUser>() {
            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial((FvojpKUsoc) obj);
            }
        }.getsocial((List) getsocial(new upgqDBbsrL<List<FvojpKUsoc>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.mobile(XdbacJlTDQ.this.acquisition.attribution().acquisition());
            }
        }, "getReferredUsers", ""));
    }

    public final PublicUser attribution(final String str) {
        AnonymousClass4 r0 = new upgqDBbsrL<FvojpKUsoc>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((FvojpKUsoc) getsocial(r0, "getPublicUser", "userId: " + str));
    }

    public final Map<String, PublicUser> getsocial(final String str, final List<String> list) {
        AnonymousClass5 r0 = new upgqDBbsrL<Map<String, FvojpKUsoc>>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, list);
            }
        };
        return new pdwpUtZXDT<String, FvojpKUsoc, PublicUser>() {
            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((FvojpKUsoc) obj);
            }
        }.getsocial((Map) getsocial(r0, "getPublicUsersByIdentities", "providerId: " + str + ", providerUserIds: " + list));
    }

    public final Integer acquisition(final String str) {
        AnonymousClass7 r0 = new upgqDBbsrL<Integer>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return (Integer) getsocial(r0, "addFriend", "userId: " + str);
    }

    public final Integer attribution(final String str, final List<String> list) {
        AnonymousClass8 r0 = new upgqDBbsrL<Integer>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, list);
            }
        };
        return (Integer) getsocial(r0, "addFriendByIdentities", "providerId: " + str + ", providerUserIds: " + list);
    }

    public final Integer mobile(final String str) {
        AnonymousClass9 r0 = new upgqDBbsrL<Integer>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.acquisition(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return (Integer) getsocial(r0, "removeFriend", "userId: " + str);
    }

    public final Integer acquisition(final String str, final List<String> list) {
        AnonymousClass10 r0 = new upgqDBbsrL<Integer>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.acquisition(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, list);
            }
        };
        return (Integer) getsocial(r0, "removeFriendByIdentities", "providerId: " + str + ", providerUserIds: " + list);
    }

    public final Integer getsocial(final List<String> list) {
        AnonymousClass11 r0 = new upgqDBbsrL<Integer>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), list);
            }
        };
        return (Integer) getsocial(r0, "setFriends", "userIds: " + list);
    }

    public final Integer mobile(final String str, final List<String> list) {
        AnonymousClass13 r0 = new upgqDBbsrL<Integer>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.mobile(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, list);
            }
        };
        return (Integer) getsocial(r0, "setFriendsByIdentity", "providerId: " + str + ", providerUserIds: " + list);
    }

    public final Boolean retention(final String str) {
        AnonymousClass14 r0 = new upgqDBbsrL<Boolean>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.mobile(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return (Boolean) getsocial(r0, "isFriend", "userId: " + str);
    }

    public final Integer attribution() {
        return (Integer) getsocial(new upgqDBbsrL<Integer>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition());
            }
        }, "getFriendsCount", "");
    }

    public final List<PublicUser> getsocial(final int i, final int i2) {
        AnonymousClass16 r0 = new upgqDBbsrL<List<FvojpKUsoc>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), Integer.valueOf(i), Integer.valueOf(i2));
            }
        };
        return new zoToeBNOjF((byte) 0).getsocial((List) getsocial(r0, "getFriends", "offset: " + i + ", limit: " + i2));
    }

    public final List<SuggestedFriend> attribution(final int i, final int i2) {
        AnonymousClass17 r0 = new upgqDBbsrL<List<pKlfFxuShO>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), Integer.valueOf(i), Integer.valueOf(i2));
            }
        };
        return new cjrhisSQCL<pKlfFxuShO, SuggestedFriend>() {
            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return im.getsocial.sdk.socialgraph.a.b.jjbQypPegg.getsocial((pKlfFxuShO) obj);
            }
        }.getsocial((List) getsocial(r0, "getSuggestedFriends", "offset: " + i + ", limit: " + i2));
    }

    public final List<UserReference> acquisition() {
        return new ztWNWCuZiM((byte) 0).getsocial((List) getsocial(new upgqDBbsrL<List<gnmRLOtbDV>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition());
            }
        }, "findUsers", ""));
    }

    public final im.getsocial.sdk.invites.a.b.XdbacJlTDQ getsocial(String str, LinkParams linkParams) {
        Map<String, String> map;
        final iFpupLCESp ifpuplcesp = new iFpupLCESp();
        ifpuplcesp.getsocial = str;
        if (linkParams == null) {
            map = null;
        } else {
            map = linkParams.getStringValues();
        }
        ifpuplcesp.attribution = map;
        return im.getsocial.sdk.invites.a.i.jjbQypPegg.getsocial((sqEuGXwfLT) getsocial(new upgqDBbsrL<sqEuGXwfLT>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), ifpuplcesp);
            }
        }, "createInviteUrl", ifpuplcesp.toString()));
    }

    public final List<ActivityPost> dau(final String str) {
        AnonymousClass22 r0 = new upgqDBbsrL<List<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.retention(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return new jjbQypPegg((byte) 0).getsocial((List) getsocial(r0, "getStickyActivities", "feed: " + str));
    }

    public final List<ActivityPost> getsocial(final String str, ActivitiesQuery activitiesQuery) {
        final im.getsocial.sdk.internal.f.a.zoToeBNOjF zotoebnojf = im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial(activitiesQuery);
        AnonymousClass24 r0 = new upgqDBbsrL<List<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, zotoebnojf);
            }
        };
        return new jjbQypPegg((byte) 0).getsocial((List) getsocial(r0, "getActivities", "feed: " + str + ", query: " + zotoebnojf));
    }

    public final List<ActivityPost> attribution(final String str, ActivitiesQuery activitiesQuery) {
        final im.getsocial.sdk.internal.f.a.zoToeBNOjF zotoebnojf = im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial(activitiesQuery);
        AnonymousClass25 r0 = new upgqDBbsrL<List<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, zotoebnojf);
            }
        };
        return new jjbQypPegg((byte) 0).getsocial((List) getsocial(r0, "getComments", "activityId: " + str + ", query: " + zotoebnojf));
    }

    public final ActivityPost mau(final String str) {
        AnonymousClass26 r0 = new upgqDBbsrL<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.dau(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial((im.getsocial.sdk.internal.f.a.ztWNWCuZiM) getsocial(r0, "getActivity", "activityId: " + str));
    }

    public final ActivityPost getsocial(final String str, im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg) {
        final KSZKMmRWhZ kSZKMmRWhZ = im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial(jjbqyppegg);
        AnonymousClass27 r0 = new upgqDBbsrL<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, kSZKMmRWhZ);
            }
        };
        return im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial((im.getsocial.sdk.internal.f.a.ztWNWCuZiM) getsocial(r0, "postActivity", "feed: " + str + ", content: " + kSZKMmRWhZ));
    }

    public final ActivityPost attribution(final String str, im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg) {
        final KSZKMmRWhZ kSZKMmRWhZ = im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial(jjbqyppegg);
        AnonymousClass28 r0 = new upgqDBbsrL<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, kSZKMmRWhZ);
            }
        };
        return im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial((im.getsocial.sdk.internal.f.a.ztWNWCuZiM) getsocial(r0, "postComment", "activityId: " + str + ", content: " + kSZKMmRWhZ));
    }

    public final ActivityPost getsocial(final String str, final boolean z) {
        AnonymousClass29 r0 = new upgqDBbsrL<im.getsocial.sdk.internal.f.a.ztWNWCuZiM>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, Boolean.valueOf(z));
            }
        };
        return im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial((im.getsocial.sdk.internal.f.a.ztWNWCuZiM) getsocial(r0, "likeActivity", "activityId: " + str + ", isLiked: " + z));
    }

    public final List<PublicUser> getsocial(final String str, final int i, final int i2) {
        AnonymousClass30 r0 = new upgqDBbsrL<List<xlPHPMtUBa>>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, Integer.valueOf(i), Integer.valueOf(i2));
            }
        };
        return new C0018XdbacJlTDQ((byte) 0).getsocial((List) getsocial(r0, "getActivityLikers", "activityId: " + str + ", offset: " + i + ", limit: " + i2));
    }

    public final void getsocial(final String str, ReportingReason reportingReason) {
        final krCuuqytsv krcuuqytsv = im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial(reportingReason);
        AnonymousClass31 r0 = new upgqDBbsrL<Boolean>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str, krcuuqytsv);
            }
        };
        getsocial(r0, "reportActivity", "activityId: " + str + ", reportingReason: " + krcuuqytsv);
    }

    public final void attribution(final List<String> list) {
        AnonymousClass32 r0 = new upgqDBbsrL<Boolean>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), list);
            }
        };
        getsocial(r0, "removeActivities", "activityIds: " + list);
    }

    public final List<String> getsocial(TagsQuery tagsQuery) {
        final NgDaxXOnCs ngDaxXOnCs = im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial(tagsQuery);
        AnonymousClass33 r0 = new upgqDBbsrL<List<String>>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), ngDaxXOnCs);
            }
        };
        return (List) getsocial(r0, "findTags", "query: " + ngDaxXOnCs);
    }

    public final void getsocial(String str, String str2, KkSvQPDhNi kkSvQPDhNi, Boolean bool) {
        final BpPZzHFMaU bpPZzHFMaU = new BpPZzHFMaU();
        bpPZzHFMaU.getsocial = str;
        bpPZzHFMaU.attribution = str2;
        bpPZzHFMaU.acquisition = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(kkSvQPDhNi);
        bpPZzHFMaU.mobile = bool;
        getsocial(new upgqDBbsrL<Boolean>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), bpPZzHFMaU);
            }
        }, "registerPushTarget", bpPZzHFMaU.toString());
    }

    public final List<Notification> getsocial(NotificationsQuery notificationsQuery) {
        final YgeTlQwUNa ygeTlQwUNa = im.getsocial.sdk.pushnotifications.a.f.jjbQypPegg.getsocial(im.getsocial.sdk.pushnotifications.upgqDBbsrL.getsocial(notificationsQuery));
        AnonymousClass36 r0 = new upgqDBbsrL<List<nGNJgptECj>>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), ygeTlQwUNa);
            }
        };
        return new cjrhisSQCL<nGNJgptECj, Notification>() {
            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return im.getsocial.sdk.pushnotifications.a.f.jjbQypPegg.getsocial((nGNJgptECj) obj);
            }
        }.getsocial((List) getsocial(r0, "getNotificationsList", "query: " + ygeTlQwUNa));
    }

    public final Integer getsocial(NotificationsCountQuery notificationsCountQuery) {
        final YgeTlQwUNa ygeTlQwUNa = im.getsocial.sdk.pushnotifications.a.f.jjbQypPegg.getsocial(im.getsocial.sdk.pushnotifications.upgqDBbsrL.getsocial(notificationsCountQuery));
        AnonymousClass38 r0 = new upgqDBbsrL<Integer>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.attribution(XdbacJlTDQ.this.acquisition.attribution().acquisition(), ygeTlQwUNa);
            }
        };
        return (Integer) getsocial(r0, "getNotificationsCount", "query: " + ygeTlQwUNa);
    }

    public final void getsocial(List<String> list, String str) {
        final UwIeQkAzJH uwIeQkAzJH = new UwIeQkAzJH();
        uwIeQkAzJH.attribution = str;
        uwIeQkAzJH.getsocial = list;
        getsocial(new upgqDBbsrL<Boolean>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), uwIeQkAzJH);
            }
        }, "setNotificationsStatus", uwIeQkAzJH.toString());
    }

    public final void getsocial(final boolean z) {
        AnonymousClass40 r0 = new upgqDBbsrL<Boolean>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), Boolean.valueOf(z));
            }
        };
        getsocial(r0, "setPushNotificationsEnabled", "isEnabled: " + z);
    }

    public final Boolean mobile() {
        return (Boolean) getsocial(new upgqDBbsrL<Boolean>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.retention(XdbacJlTDQ.this.acquisition.attribution().acquisition());
            }
        }, "isPushNotificationsEnabled", "");
    }

    public final NotificationsSummary getsocial(List<String> list, im.getsocial.sdk.pushnotifications.a.b.upgqDBbsrL upgqdbbsrl) {
        final rFvvVpjzZH rfvvvpjzzh = im.getsocial.sdk.pushnotifications.a.f.jjbQypPegg.getsocial(upgqdbbsrl);
        rfvvvpjzzh.getsocial = list;
        AnonymousClass42 r4 = new upgqDBbsrL<sdizKTglGl>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), rfvvvpjzzh);
            }
        };
        return im.getsocial.sdk.pushnotifications.a.f.jjbQypPegg.getsocial((sdizKTglGl) getsocial(r4, "sendNotification", "notification: " + rfvvvpjzzh));
    }

    public final PromoCode getsocial(PromoCodeBuilder promoCodeBuilder) {
        final xAXgtBkRbG xaxgtbkrbg = im.getsocial.sdk.promocodes.a.a.jjbQypPegg.getsocial(promoCodeBuilder);
        AnonymousClass43 r0 = new upgqDBbsrL<xAXgtBkRbG>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), xaxgtbkrbg);
            }
        };
        return im.getsocial.sdk.promocodes.a.a.jjbQypPegg.getsocial((xAXgtBkRbG) getsocial(r0, "createPromoCode", "promoCode: " + xaxgtbkrbg));
    }

    public final PromoCode cat(final String str) {
        AnonymousClass44 r0 = new upgqDBbsrL<xAXgtBkRbG>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.cat(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return im.getsocial.sdk.promocodes.a.a.jjbQypPegg.getsocial((xAXgtBkRbG) getsocial(r0, "getPromoCode", "promoCode: " + str));
    }

    public final PromoCode viral(final String str) {
        AnonymousClass46 r0 = new upgqDBbsrL<xAXgtBkRbG>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.mau(XdbacJlTDQ.this.acquisition.attribution().acquisition(), str);
            }
        };
        return im.getsocial.sdk.promocodes.a.a.jjbQypPegg.getsocial((xAXgtBkRbG) getsocial(r0, "claimPromoCode", "promoCode: " + str));
    }

    public final void getsocial(JbBdMtJmlU jbBdMtJmlU, List<im.getsocial.sdk.internal.a.b.jjbQypPegg> list) {
        final eTZqdsudqh etzqdsudqh = im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(jbBdMtJmlU);
        final List list2 = new cjrhisSQCL<im.getsocial.sdk.internal.a.b.jjbQypPegg, HptYHntaqF>() {
            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return im.getsocial.sdk.internal.a.e.jjbQypPegg.getsocial((im.getsocial.sdk.internal.a.b.jjbQypPegg) obj);
            }
        }.getsocial((List) list);
        AnonymousClass48 r0 = new upgqDBbsrL<Boolean>() {
            public final /* bridge */ /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), etzqdsudqh, list2);
            }
        };
        getsocial(r0, "trackAnalyticsEvents", "superProperties: " + etzqdsudqh + ", events: " + list2);
    }

    public final im.getsocial.sdk.iap.a.b.upgqDBbsrL getsocial(final im.getsocial.sdk.iap.a.b.jjbQypPegg jjbqyppegg) {
        AnonymousClass49 r0 = new upgqDBbsrL<icjTFWWVFN>() {
            public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
                return upgqdbbsrl.getsocial(XdbacJlTDQ.this.acquisition.attribution().acquisition(), jjbqyppegg.getsocial, jjbqyppegg.attribution, jjbqyppegg.acquisition, Boolean.valueOf(jjbqyppegg.mobile), Boolean.valueOf(jjbqyppegg.retention));
            }
        };
        return im.getsocial.sdk.iap.a.e.jjbQypPegg.getsocial((icjTFWWVFN) getsocial(r0, "validateIAP", "checkpoint: " + jjbqyppegg.acquisition + ", isSubscription: " + jjbqyppegg.mobile + ", validateOnline: " + jjbqyppegg.retention));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(im.getsocial.sdk.internal.c.j.XdbacJlTDQ$upgqDBbsrL, java.lang.String, java.lang.String, boolean):T
     arg types: [im.getsocial.sdk.internal.c.j.XdbacJlTDQ$upgqDBbsrL<T>, java.lang.String, java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(im.getsocial.sdk.internal.c.KluUZYuxme, boolean, java.util.Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, java.util.Map<java.lang.String, java.lang.String>>, java.util.Map<java.lang.String, java.lang.String>):im.getsocial.sdk.invites.ReferralData
      im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(java.lang.String, java.lang.String, im.getsocial.sdk.internal.c.KkSvQPDhNi, java.lang.Boolean):void
      im.getsocial.sdk.internal.c.a.jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.KluUZYuxme, boolean, java.util.Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, java.util.Map<java.lang.String, java.lang.String>>, java.util.Map<java.lang.String, java.lang.String>):im.getsocial.sdk.invites.ReferralData
      im.getsocial.sdk.internal.c.a.jjbQypPegg.getsocial(java.lang.String, java.lang.String, im.getsocial.sdk.internal.c.KkSvQPDhNi, java.lang.Boolean):void
      im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(im.getsocial.sdk.internal.c.j.XdbacJlTDQ$upgqDBbsrL, java.lang.String, java.lang.String, boolean):T */
    private <T> T getsocial(upgqDBbsrL<T> upgqdbbsrl, String str, String str2) {
        return getsocial((upgqDBbsrL) upgqdbbsrl, str, str2, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(im.getsocial.sdk.internal.c.j.XdbacJlTDQ$upgqDBbsrL, java.lang.String, java.lang.String, boolean):T
     arg types: [im.getsocial.sdk.internal.c.j.XdbacJlTDQ$upgqDBbsrL<T>, java.lang.String, java.lang.String, int]
     candidates:
      im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(im.getsocial.sdk.internal.c.KluUZYuxme, boolean, java.util.Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, java.util.Map<java.lang.String, java.lang.String>>, java.util.Map<java.lang.String, java.lang.String>):im.getsocial.sdk.invites.ReferralData
      im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(java.lang.String, java.lang.String, im.getsocial.sdk.internal.c.KkSvQPDhNi, java.lang.Boolean):void
      im.getsocial.sdk.internal.c.a.jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.KluUZYuxme, boolean, java.util.Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, java.util.Map<java.lang.String, java.lang.String>>, java.util.Map<java.lang.String, java.lang.String>):im.getsocial.sdk.invites.ReferralData
      im.getsocial.sdk.internal.c.a.jjbQypPegg.getsocial(java.lang.String, java.lang.String, im.getsocial.sdk.internal.c.KkSvQPDhNi, java.lang.Boolean):void
      im.getsocial.sdk.internal.c.j.XdbacJlTDQ.getsocial(im.getsocial.sdk.internal.c.j.XdbacJlTDQ$upgqDBbsrL, java.lang.String, java.lang.String, boolean):T */
    private <T> T getsocial(upgqDBbsrL<T> upgqdbbsrl, String str, String str2, boolean z) {
        upgqDBbsrL<T> upgqdbbsrl2 = upgqdbbsrl;
        String str3 = str;
        String str4 = str2;
        long currentTimeMillis = System.currentTimeMillis();
        try {
            im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl3 = this.attribution.getsocial();
            getsocial.getsocial("[HADES] Connect to Hades(%d ms) succeeded", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
            getsocial.getsocial("[HADES] Call `%s` with params: [ %s ]", str3, str4);
            long currentTimeMillis2 = System.currentTimeMillis();
            try {
                T t = upgqdbbsrl2.getsocial(upgqdbbsrl3);
                if ("3in1".equalsIgnoreCase(str3)) {
                    getsocial.getsocial("[HADES] Call `%s`(%d ms) succeeded", str3, Long.valueOf(System.currentTimeMillis() - currentTimeMillis2));
                } else {
                    getsocial.getsocial("[HADES] Call `%s`(%d ms) succeeded, response: [ %s ]", str3, Long.valueOf(System.currentTimeMillis() - currentTimeMillis2), t);
                }
                if (upgqdbbsrl3 instanceof Closeable) {
                    try {
                        ((Closeable) upgqdbbsrl3).close();
                    } catch (IOException e) {
                        getsocial.attribution("Can not close the client:");
                        getsocial.getsocial(e);
                    }
                }
                return t;
            } catch (KCGqEGAizh e2) {
                GetSocialException getSocialException = im.getsocial.sdk.internal.c.j.a.jjbQypPegg.getsocial(e2);
                getsocial.getsocial("[HADES] Call `%s`(%d ms) failed, response: [ %s ]", str3, Long.valueOf(System.currentTimeMillis() - currentTimeMillis2), e2);
                if (!z || getSocialException.getErrorCode() != 202) {
                    throw im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(getSocialException);
                }
                new im.getsocial.sdk.internal.i.d.jjbQypPegg().getsocial();
                T t2 = getsocial((upgqDBbsrL) upgqdbbsrl2, str3, str4, false);
                if (upgqdbbsrl3 instanceof Closeable) {
                    try {
                        ((Closeable) upgqdbbsrl3).close();
                    } catch (IOException e3) {
                        getsocial.attribution("Can not close the client:");
                        getsocial.getsocial(e3);
                    }
                }
                return t2;
            } catch (Throwable th) {
                Throwable th2 = th;
                if (upgqdbbsrl3 instanceof Closeable) {
                    try {
                        ((Closeable) upgqdbbsrl3).close();
                    } catch (IOException e4) {
                        getsocial.attribution("Can not close the client:");
                        getsocial.getsocial(e4);
                    }
                }
                throw th2;
            }
        } catch (IOException e5) {
            getsocial.getsocial("[HADES] Connect to Hades(%d ms) failed, exception: [ %s ]", Long.valueOf(System.currentTimeMillis() - currentTimeMillis), e5);
            throw im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(e5);
        }
    }

    /* compiled from: SyncThriftyCommunicationLayer */
    private static abstract class cjrhisSQCL<T, R> {
        /* access modifiers changed from: protected */
        public abstract R getsocial(T t);

        private cjrhisSQCL() {
        }

        /* synthetic */ cjrhisSQCL(byte b) {
            this();
        }

        public final List<R> getsocial(List<T> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (T t : list) {
                arrayList.add(getsocial((Object) t));
            }
            return arrayList;
        }
    }

    /* compiled from: SyncThriftyCommunicationLayer */
    private static abstract class pdwpUtZXDT<K, V, R> {
        /* access modifiers changed from: protected */
        public abstract R getsocial(V v);

        private pdwpUtZXDT() {
        }

        /* synthetic */ pdwpUtZXDT(byte b) {
            this();
        }

        public final Map<K, R> getsocial(Map<K, V> map) {
            HashMap hashMap = new HashMap();
            for (Map.Entry next : map.entrySet()) {
                hashMap.put(next.getKey(), getsocial(next.getValue()));
            }
            return hashMap;
        }
    }

    /* compiled from: SyncThriftyCommunicationLayer */
    private static final class jjbQypPegg extends cjrhisSQCL<im.getsocial.sdk.internal.f.a.ztWNWCuZiM, ActivityPost> {
        private jjbQypPegg() {
            super((byte) 0);
        }

        /* synthetic */ jjbQypPegg(byte b) {
            this();
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
            return im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial((im.getsocial.sdk.internal.f.a.ztWNWCuZiM) obj);
        }
    }

    /* compiled from: SyncThriftyCommunicationLayer */
    private static final class zoToeBNOjF extends cjrhisSQCL<FvojpKUsoc, PublicUser> {
        private zoToeBNOjF() {
            super((byte) 0);
        }

        /* synthetic */ zoToeBNOjF(byte b) {
            this();
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
            return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((FvojpKUsoc) obj);
        }
    }

    /* renamed from: im.getsocial.sdk.internal.c.j.XdbacJlTDQ$XdbacJlTDQ  reason: collision with other inner class name */
    /* compiled from: SyncThriftyCommunicationLayer */
    private static final class C0018XdbacJlTDQ extends cjrhisSQCL<xlPHPMtUBa, PublicUser> {
        private C0018XdbacJlTDQ() {
            super((byte) 0);
        }

        /* synthetic */ C0018XdbacJlTDQ(byte b) {
            this();
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
            return im.getsocial.sdk.activities.a.c.jjbQypPegg.getsocial((xlPHPMtUBa) obj);
        }
    }

    /* compiled from: SyncThriftyCommunicationLayer */
    private static final class ztWNWCuZiM extends cjrhisSQCL<gnmRLOtbDV, UserReference> {
        private ztWNWCuZiM() {
            super((byte) 0);
        }

        /* synthetic */ ztWNWCuZiM(byte b) {
            this();
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
            return im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial((gnmRLOtbDV) obj);
        }
    }

    /* renamed from: im.getsocial.sdk.internal.c.j.XdbacJlTDQ$20  reason: invalid class name */
    /* compiled from: SyncThriftyCommunicationLayer */
    class AnonymousClass20 implements upgqDBbsrL<JWvbLzaedN> {
        final /* synthetic */ XdbacJlTDQ getsocial;

        public final /* synthetic */ Object getsocial(im.getsocial.sdk.internal.f.a.upgqDBbsrL upgqdbbsrl) {
            return upgqdbbsrl.acquisition(this.getsocial.acquisition.attribution().acquisition());
        }
    }
}
