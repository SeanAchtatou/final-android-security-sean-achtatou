package im.getsocial.sdk.internal.c.l;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: CollectionUtils */
public final class upgqDBbsrL {
    private upgqDBbsrL() {
    }

    public static <K, V> Map<K, V> getsocial(Map map) {
        return map == null ? Collections.emptyMap() : new HashMap(map);
    }

    public static <T> List<T> getsocial(List list) {
        return list == null ? Collections.emptyList() : new ArrayList(list);
    }

    public static <T> boolean getsocial(List<T> list, List<T> list2) {
        if (list == null && list2 == null) {
            return true;
        }
        return list != null && list2 != null && list.size() == list2.size() && list.containsAll(list2);
    }
}
