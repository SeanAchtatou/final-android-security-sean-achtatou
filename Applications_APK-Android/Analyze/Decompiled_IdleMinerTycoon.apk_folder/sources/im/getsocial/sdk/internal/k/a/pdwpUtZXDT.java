package im.getsocial.sdk.internal.k.a;

import java.util.Map;

/* compiled from: MediaUrlCollection */
public class pdwpUtZXDT {
    private static String acquisition = "gif";
    private static String attribution = "jpg";
    private static String mobile = "mp4";
    private final Map<String, String> getsocial;

    public pdwpUtZXDT(Map<String, String> map) {
        this.getsocial = map;
    }

    public final String getsocial() {
        if (this.getsocial.size() == 1) {
            return (String) this.getsocial.values().toArray()[0];
        }
        return this.getsocial.get(attribution);
    }

    public final String attribution() {
        return this.getsocial.get(acquisition);
    }

    public final String acquisition() {
        return this.getsocial.get(mobile);
    }
}
