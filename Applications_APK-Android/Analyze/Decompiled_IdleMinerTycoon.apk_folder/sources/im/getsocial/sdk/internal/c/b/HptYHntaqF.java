package im.getsocial.sdk.internal.c.b;

import android.app.Application;
import android.content.Context;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import im.getsocial.sdk.internal.c.IbawHMWljm;
import im.getsocial.sdk.internal.c.JbBdMtJmlU;
import im.getsocial.sdk.internal.c.KluUZYuxme;
import im.getsocial.sdk.internal.c.QCXFOjcJkE;
import im.getsocial.sdk.internal.c.QhisXzMgay;
import im.getsocial.sdk.internal.c.SKUqohGtGQ;
import im.getsocial.sdk.internal.c.cjrhisSQCL;
import im.getsocial.sdk.internal.c.h.jjbQypPegg;
import im.getsocial.sdk.internal.c.h.upgqDBbsrL;
import im.getsocial.sdk.internal.c.jMsobIMeui;
import im.getsocial.sdk.internal.c.qZypgoeblR;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.rFvvVpjzZH;
import im.getsocial.sdk.internal.c.rWfbqYooCV;
import im.getsocial.sdk.internal.c.ruWsnwUPKh;
import im.getsocial.sdk.internal.c.sqEuGXwfLT;
import im.getsocial.sdk.internal.c.vkXhnjhKGp;
import im.getsocial.sdk.internal.c.wWemqSpYTx;
import im.getsocial.sdk.internal.c.ztWNWCuZiM;
import im.getsocial.sdk.internal.m.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.k.XdbacJlTDQ;
import im.getsocial.sdk.invites.a.k.zoToeBNOjF;
import java.util.List;
import java.util.Map;

/* compiled from: NativeComponentHelper */
public final class HptYHntaqF {
    private HptYHntaqF() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, im.getsocial.sdk.internal.c.b.HptYHntaqF$1]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.qZypgoeblR]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
     arg types: [java.lang.Class, java.lang.String, im.getsocial.sdk.invites.a.k.cjrhisSQCL]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void */
    public static void getsocial(Application application, pdwpUtZXDT pdwputzxdt) {
        getsocial(pdwputzxdt, qdyNCsqjKt.class, wWemqSpYTx.class);
        getsocial(pdwputzxdt, jjbQypPegg.class, upgqDBbsrL.class);
        getsocial(pdwputzxdt, KluUZYuxme.class, ruWsnwUPKh.class);
        getsocial(pdwputzxdt, JbBdMtJmlU.class, QhisXzMgay.class);
        getsocial(pdwputzxdt, sqEuGXwfLT.class, rFvvVpjzZH.class);
        getsocial(pdwputzxdt, XdbacJlTDQ.class, zoToeBNOjF.class);
        getsocial(pdwputzxdt, ztWNWCuZiM.class, im.getsocial.sdk.internal.c.jjbQypPegg.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.pushnotifications.a.a.jjbQypPegg.class, im.getsocial.sdk.internal.c.XdbacJlTDQ.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.d.jjbQypPegg.class, im.getsocial.sdk.internal.c.d.upgqDBbsrL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.invites.a.k.jjbQypPegg.class, im.getsocial.sdk.invites.a.k.upgqDBbsrL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.b.upgqDBbsrL.class, im.getsocial.sdk.internal.b.upgqDBbsrL.class);
        getsocial(pdwputzxdt, QCXFOjcJkE.class, im.getsocial.sdk.internal.c.g.jjbQypPegg.class);
        getsocial(pdwputzxdt, rWfbqYooCV.class, im.getsocial.sdk.invites.a.jjbQypPegg.class);
        getsocial(pdwputzxdt, jMsobIMeui.class, cjrhisSQCL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.HptYHntaqF.class, im.getsocial.sdk.internal.c.upgqDBbsrL.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.m.XdbacJlTDQ.class, im.getsocial.sdk.internal.m.XdbacJlTDQ.class);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.k.upgqDBbsrL.class, im.getsocial.sdk.internal.k.jjbQypPegg.class);
        if (pdwputzxdt.acquisition(vkXhnjhKGp.class)) {
            pdwputzxdt.getsocial(vkXhnjhKGp.class, (cjrhisSQCL) new cjrhisSQCL<vkXhnjhKGp>() {
                public final /* synthetic */ Object getsocial(pdwpUtZXDT pdwputzxdt) {
                    SKUqohGtGQ sKUqohGtGQ = new SKUqohGtGQ();
                    return im.getsocial.sdk.internal.c.l.XdbacJlTDQ.getsocial((JbBdMtJmlU) pdwputzxdt.attribution(JbBdMtJmlU.class)) ? new im.getsocial.sdk.internal.m.qdyNCsqjKt(sKUqohGtGQ) : sKUqohGtGQ;
                }
            });
        }
        getsocial(pdwputzxdt, Context.class, application);
        getsocial(pdwputzxdt, Application.class, application);
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.e.a.upgqDBbsrL.class, pdwpUtZXDT.getsocial());
        getsocial(pdwputzxdt, im.getsocial.sdk.internal.c.l.pdwpUtZXDT.class, im.getsocial.sdk.internal.c.l.pdwpUtZXDT.ANDROID);
        if (pdwputzxdt.getsocial(Map.class, DeviceRequestsHelper.DEVICE_INFO_PARAM)) {
            pdwputzxdt.getsocial(Map.class, DeviceRequestsHelper.DEVICE_INFO_PARAM, (cjrhisSQCL) new qZypgoeblR());
        }
        if (pdwputzxdt.getsocial(List.class, "channels_handling_lifecycle")) {
            pdwputzxdt.getsocial(List.class, "channels_handling_lifecycle", (cjrhisSQCL) new im.getsocial.sdk.invites.a.k.cjrhisSQCL());
        }
        getsocial(pdwputzxdt, IbawHMWljm.class, new im.getsocial.sdk.internal.c.pdwpUtZXDT());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
     arg types: [java.lang.Class<T>, java.lang.Class<? extends T>]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void */
    private static <T> void getsocial(pdwpUtZXDT pdwputzxdt, Class<T> cls, Class<? extends T> cls2) {
        if (pdwputzxdt.acquisition(cls)) {
            pdwputzxdt.getsocial((Class) cls, (Class) cls2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class<T>, T]
     candidates:
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(im.getsocial.sdk.internal.c.b.pdwpUtZXDT, java.lang.Class):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.String, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.KSZKMmRWhZ):java.lang.Object
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, im.getsocial.sdk.internal.c.b.cjrhisSQCL):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Class):void
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.String):boolean
      im.getsocial.sdk.internal.c.b.pdwpUtZXDT.getsocial(java.lang.Class, java.lang.Object):void */
    private static <T> void getsocial(pdwpUtZXDT pdwputzxdt, Class<T> cls, T t) {
        if (pdwputzxdt.acquisition(cls)) {
            pdwputzxdt.getsocial((Class) cls, (Object) t);
        }
    }
}
