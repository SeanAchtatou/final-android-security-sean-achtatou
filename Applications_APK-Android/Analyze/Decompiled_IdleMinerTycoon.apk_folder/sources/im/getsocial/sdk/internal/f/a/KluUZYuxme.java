package im.getsocial.sdk.internal.f.a;

/* compiled from: THAppOpenSource */
public enum KluUZYuxme {
    Facebook(0),
    Google(1),
    DeepLink(2),
    API(3),
    Manual(4);
    
    public final int value;

    private KluUZYuxme(int i) {
        this.value = i;
    }

    public static KluUZYuxme findByValue(int i) {
        switch (i) {
            case 0:
                return Facebook;
            case 1:
                return Google;
            case 2:
                return DeepLink;
            case 3:
                return API;
            case 4:
                return Manual;
            default:
                return null;
        }
    }
}
