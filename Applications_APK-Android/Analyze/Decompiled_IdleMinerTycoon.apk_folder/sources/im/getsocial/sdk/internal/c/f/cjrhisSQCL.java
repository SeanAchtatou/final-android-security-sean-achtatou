package im.getsocial.sdk.internal.c.f;

import com.google.android.gms.nearby.messages.Strategy;

/* compiled from: Log */
public interface cjrhisSQCL {
    void acquisition(String str);

    void acquisition(String str, Object... objArr);

    void acquisition(Throwable th);

    void attribution(String str);

    void attribution(String str, Object... objArr);

    void attribution(Throwable th);

    void getsocial(String str, Object... objArr);

    void getsocial(Throwable th);

    void mobile(String str);

    void retention(String str);

    /* compiled from: Log */
    public enum jjbQypPegg {
        OFF(0),
        ERROR(100),
        WARN(200),
        INFO(Strategy.TTL_SECONDS_DEFAULT),
        DEBUG(400),
        ALL(Integer.MAX_VALUE);
        
        private final int _value;

        private jjbQypPegg(int i) {
            this._value = i;
        }

        public final int value() {
            return this._value;
        }
    }
}
