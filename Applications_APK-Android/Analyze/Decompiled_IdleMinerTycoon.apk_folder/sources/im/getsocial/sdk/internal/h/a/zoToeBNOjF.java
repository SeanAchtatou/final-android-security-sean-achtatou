package im.getsocial.sdk.internal.h.a;

import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import java.util.List;

/* compiled from: SkuDetailsResponseListenerProxy */
public abstract class zoToeBNOjF implements SkuDetailsResponseListener {
    public abstract void onResponse(XdbacJlTDQ xdbacJlTDQ, List<SkuDetails> list);

    public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
        onResponse(new XdbacJlTDQ(billingResult), list);
    }

    public void onSkuDetailsResponse(int i, List<SkuDetails> list) {
        onResponse(new XdbacJlTDQ(i), list);
    }
}
