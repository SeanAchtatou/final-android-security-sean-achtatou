package im.getsocial.sdk.internal.m;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;

/* compiled from: AdvertisingIdClient */
final class upgqDBbsrL {
    private static final cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);

    private upgqDBbsrL() {
    }

    static String getsocial(Context context) {
        try {
            return AdvertisingIdClient.getAdvertisingIdInfo(context.getApplicationContext()).getId();
        } catch (Throwable th) {
            getsocial.attribution("AdvertisingId is not available");
            getsocial.getsocial(th);
            return "";
        }
    }
}
