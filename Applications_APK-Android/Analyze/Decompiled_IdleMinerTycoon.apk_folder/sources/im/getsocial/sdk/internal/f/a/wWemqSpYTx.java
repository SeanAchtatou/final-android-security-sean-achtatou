package im.getsocial.sdk.internal.f.a;

import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: THFingerprint */
public final class wWemqSpYTx {
    public String acquisition;
    public String attribution;
    public String dau;
    public String getsocial;
    public String mau;
    public String mobile;
    public String retention;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof wWemqSpYTx)) {
            return false;
        }
        wWemqSpYTx wwemqspytx = (wWemqSpYTx) obj;
        return (this.getsocial == wwemqspytx.getsocial || (this.getsocial != null && this.getsocial.equals(wwemqspytx.getsocial))) && (this.attribution == wwemqspytx.attribution || (this.attribution != null && this.attribution.equals(wwemqspytx.attribution))) && ((this.acquisition == wwemqspytx.acquisition || (this.acquisition != null && this.acquisition.equals(wwemqspytx.acquisition))) && ((this.mobile == wwemqspytx.mobile || (this.mobile != null && this.mobile.equals(wwemqspytx.mobile))) && ((this.retention == wwemqspytx.retention || (this.retention != null && this.retention.equals(wwemqspytx.retention))) && ((this.dau == wwemqspytx.dau || (this.dau != null && this.dau.equals(wwemqspytx.dau))) && (this.mau == wwemqspytx.mau || (this.mau != null && this.mau.equals(wwemqspytx.mau)))))));
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((this.getsocial == null ? 0 : this.getsocial.hashCode()) ^ 16777619) * -2128831035) ^ (this.attribution == null ? 0 : this.attribution.hashCode())) * -2128831035) ^ (this.acquisition == null ? 0 : this.acquisition.hashCode())) * -2128831035) ^ (this.mobile == null ? 0 : this.mobile.hashCode())) * -2128831035) ^ (this.retention == null ? 0 : this.retention.hashCode())) * -2128831035) * -2128831035) ^ (this.dau == null ? 0 : this.dau.hashCode())) * -2128831035;
        if (this.mau != null) {
            i = this.mau.hashCode();
        }
        return (hashCode ^ i) * -2128831035 * -2128831035;
    }

    public final String toString() {
        return "THFingerprint{screenWidth=" + this.getsocial + ", screenHeight=" + this.attribution + ", devicePixelRatio=" + this.acquisition + ", osName=" + this.mobile + ", osVersion=" + this.retention + ", userAgent=" + ((String) null) + ", deviceBrand=" + this.dau + ", deviceModel=" + this.mau + ", ip=" + ((String) null) + "}";
    }

    public static void getsocial(zoToeBNOjF zotoebnojf, wWemqSpYTx wwemqspytx) {
        if (wwemqspytx.getsocial != null) {
            zotoebnojf.getsocial(1, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.getsocial);
        }
        if (wwemqspytx.attribution != null) {
            zotoebnojf.getsocial(2, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.attribution);
        }
        if (wwemqspytx.acquisition != null) {
            zotoebnojf.getsocial(3, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.acquisition);
        }
        if (wwemqspytx.mobile != null) {
            zotoebnojf.getsocial(4, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.mobile);
        }
        if (wwemqspytx.retention != null) {
            zotoebnojf.getsocial(5, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.retention);
        }
        if (wwemqspytx.dau != null) {
            zotoebnojf.getsocial(7, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.dau);
        }
        if (wwemqspytx.mau != null) {
            zotoebnojf.getsocial(9, (byte) 11);
            zotoebnojf.getsocial(wwemqspytx.mau);
        }
        zotoebnojf.getsocial();
    }
}
