package im.getsocial.sdk.internal.m;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import com.onesignal.OneSignalDbContract;

/* compiled from: AndroidServiceLocator */
public final class XdbacJlTDQ {
    private final Context getsocial;

    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    XdbacJlTDQ(Context context) {
        this.getsocial = context;
    }

    public final NotificationManager getsocial() {
        return (NotificationManager) this.getsocial.getSystemService(OneSignalDbContract.NotificationTable.TABLE_NAME);
    }

    public final Resources attribution() {
        return this.getsocial.getResources();
    }

    public final ConnectivityManager acquisition() {
        return (ConnectivityManager) this.getsocial.getSystemService("connectivity");
    }
}
