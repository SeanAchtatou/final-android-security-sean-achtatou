package im.getsocial.sdk.internal.m;

import java.util.Date;

/* compiled from: DateUtils */
public final class ztWNWCuZiM {
    private ztWNWCuZiM() {
    }

    public static Date getsocial(Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime());
    }
}
