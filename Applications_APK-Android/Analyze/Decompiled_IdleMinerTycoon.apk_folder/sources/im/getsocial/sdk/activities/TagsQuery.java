package im.getsocial.sdk.activities;

public final class TagsQuery {
    private int acquisition = 5;
    private String attribution = "";
    private String getsocial;

    private TagsQuery(String str) {
        this.getsocial = str;
    }

    public static TagsQuery tagsForGlobalFeed() {
        return tagsForFeed(ActivitiesQuery.GLOBAL_FEED);
    }

    public static TagsQuery tagsForFeed(String str) {
        return new TagsQuery(str);
    }

    public final TagsQuery withName(String str) {
        this.attribution = str;
        return this;
    }

    public final TagsQuery withLimit(int i) {
        this.acquisition = i;
        return this;
    }

    public final int getLimit() {
        return this.acquisition;
    }

    public final String getQuery() {
        return this.attribution;
    }

    public final String getFeedName() {
        return this.getsocial;
    }

    /* access modifiers changed from: package-private */
    public final void getsocial(String str) {
        this.getsocial = str;
    }
}
