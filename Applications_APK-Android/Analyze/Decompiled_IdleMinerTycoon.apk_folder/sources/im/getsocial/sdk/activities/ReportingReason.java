package im.getsocial.sdk.activities;

public enum ReportingReason {
    SPAM,
    INAPPROPRIATE_CONTENT
}
