package im.getsocial.sdk.activities;

public class Mention {
    /* access modifiers changed from: private */
    public int acquisition;
    /* access modifiers changed from: private */
    public int attribution;
    /* access modifiers changed from: private */
    public String getsocial;
    /* access modifiers changed from: private */
    public String mobile;

    public String getUserId() {
        return this.getsocial;
    }

    public int getStartIndex() {
        return this.attribution;
    }

    public int getEndIndex() {
        return this.acquisition;
    }

    public String getType() {
        return this.mobile;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final Mention getsocial = new Mention();

        Builder() {
        }

        public Builder withUserId(String str) {
            String unused = this.getsocial.getsocial = str;
            return this;
        }

        public Builder withStartIndex(int i) {
            int unused = this.getsocial.attribution = i;
            return this;
        }

        public Builder withEndIndex(int i) {
            int unused = this.getsocial.acquisition = i;
            return this;
        }

        public Builder withType(String str) {
            String unused = this.getsocial.mobile = str;
            return this;
        }

        public Mention build() {
            return this.getsocial;
        }
    }
}
