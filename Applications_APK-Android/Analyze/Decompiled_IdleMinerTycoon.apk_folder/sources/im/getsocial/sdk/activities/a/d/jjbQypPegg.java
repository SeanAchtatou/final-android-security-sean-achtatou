package im.getsocial.sdk.activities.a.d;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.activities.cjrhisSQCL;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.util.List;

/* compiled from: FindTagsUseCase */
public final class jjbQypPegg extends im.getsocial.sdk.internal.c.k.jjbQypPegg {
    public final void getsocial(final TagsQuery tagsQuery, Callback<List<String>> callback) {
        boolean z = false;
        jjbQypPegg.C0021jjbQypPegg.getsocial(tagsQuery.getLimit() > 0, "Limit can not be less than or equal 0");
        if (tagsQuery.getLimit() <= 20) {
            z = true;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Limit can not be greater that 20");
        cjrhisSQCL.getsocial(tagsQuery, im.getsocial.sdk.activities.a.b.jjbQypPegg.getsocial(tagsQuery.getFeedName()));
        getsocial(new jjbQypPegg.C0020jjbQypPegg<List<String>>() {
            public final /* bridge */ /* synthetic */ Object getsocial() {
                return jjbQypPegg.this.getsocial().getsocial(tagsQuery);
            }
        }, callback);
    }
}
