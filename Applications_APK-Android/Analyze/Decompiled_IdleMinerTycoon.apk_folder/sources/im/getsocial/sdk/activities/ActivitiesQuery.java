package im.getsocial.sdk.activities;

import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.upgqDBbsrL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ActivitiesQuery {
    public static final int DEFAULT_LIMIT = 10;
    public static final String GLOBAL_FEED = "g-global";
    private final String acquisition;
    private final String attribution;
    private boolean cat;
    private String dau;
    private final ActivityPost.Type getsocial;
    private String mau;
    private int mobile = 10;
    private Filter retention = Filter.NO_FILTER;
    private List<String> viral = Collections.emptyList();

    public enum Filter {
        NO_FILTER,
        OLDER,
        NEWER
    }

    private ActivitiesQuery(ActivityPost.Type type, String str, String str2) {
        this.getsocial = type;
        this.attribution = str;
        this.acquisition = str2;
    }

    public static ActivitiesQuery postsForFeed(String str) {
        return new ActivitiesQuery(ActivityPost.Type.POST, str, null);
    }

    public static ActivitiesQuery postsForGlobalFeed() {
        return new ActivitiesQuery(ActivityPost.Type.POST, GLOBAL_FEED, null);
    }

    public static ActivitiesQuery commentsToPost(String str) {
        return new ActivitiesQuery(ActivityPost.Type.COMMENT, null, str);
    }

    public final ActivitiesQuery withLimit(int i) {
        this.mobile = i;
        return this;
    }

    public final ActivitiesQuery withFilter(Filter filter, String str) {
        this.retention = filter;
        this.dau = str;
        return this;
    }

    public final ActivitiesQuery filterByUser(String str) {
        this.mau = str;
        return this;
    }

    public final ActivitiesQuery friendsFeed(boolean z) {
        this.cat = z;
        return this;
    }

    public final ActivitiesQuery withTags(String... strArr) {
        this.viral = Arrays.asList(strArr);
        return this;
    }

    public final String toString() {
        return "ActivitiesQuery{_type=" + this.getsocial + ", _feed='" + this.attribution + '\'' + ", _parentActivityId='" + this.acquisition + '\'' + ", _limit=" + this.mobile + ", _filter=" + this.retention + ", _filteringActivityId='" + this.dau + '\'' + ", _userId='" + this.mau + '\'' + ", _isFriendsFeed=" + this.cat + ", _tags=" + this.viral + '}';
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ActivitiesQuery activitiesQuery = (ActivitiesQuery) obj;
        if (this.mobile != activitiesQuery.mobile || this.cat != activitiesQuery.cat || this.getsocial != activitiesQuery.getsocial) {
            return false;
        }
        if (this.attribution == null ? activitiesQuery.attribution != null : !this.attribution.equals(activitiesQuery.attribution)) {
            return false;
        }
        if (this.acquisition == null ? activitiesQuery.acquisition != null : !this.acquisition.equals(activitiesQuery.acquisition)) {
            return false;
        }
        if (this.retention != activitiesQuery.retention) {
            return false;
        }
        if (this.dau == null ? activitiesQuery.dau != null : !this.dau.equals(activitiesQuery.dau)) {
            return false;
        }
        if (this.mau == null ? activitiesQuery.mau != null : !this.mau.equals(activitiesQuery.mau)) {
            return false;
        }
        if (this.viral != null) {
            return upgqDBbsrL.getsocial(this.viral, activitiesQuery.viral);
        }
        return activitiesQuery.viral == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((((this.getsocial != null ? this.getsocial.hashCode() : 0) * 31) + (this.attribution != null ? this.attribution.hashCode() : 0)) * 31) + (this.acquisition != null ? this.acquisition.hashCode() : 0)) * 31) + this.mobile) * 31) + (this.retention != null ? this.retention.hashCode() : 0)) * 31) + (this.dau != null ? this.dau.hashCode() : 0)) * 31) + (this.mau != null ? this.mau.hashCode() : 0)) * 31) + (this.cat ? 1 : 0)) * 31;
        if (this.viral != null) {
            i = this.viral.hashCode();
        }
        return hashCode + i;
    }

    /* access modifiers changed from: package-private */
    public final ActivityPost.Type getsocial() {
        return this.getsocial;
    }

    /* access modifiers changed from: package-private */
    public final String attribution() {
        return this.attribution;
    }

    /* access modifiers changed from: package-private */
    public final String acquisition() {
        return this.acquisition;
    }

    /* access modifiers changed from: package-private */
    public final int mobile() {
        return this.mobile;
    }

    /* access modifiers changed from: package-private */
    public final String retention() {
        if (this.retention == Filter.OLDER) {
            return this.dau;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final String dau() {
        if (this.retention == Filter.NEWER) {
            return this.dau;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final String mau() {
        return this.mau;
    }

    /* access modifiers changed from: package-private */
    public final boolean cat() {
        return this.cat;
    }

    /* access modifiers changed from: package-private */
    public final List<String> viral() {
        return this.viral;
    }

    /* access modifiers changed from: package-private */
    public final void organic() {
        if (this.getsocial == ActivityPost.Type.POST) {
            jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.attribution(this.attribution), "Can not create query with null feed");
        } else {
            jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial((Object) this.acquisition), "Can not create query with null activityId");
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(jjbQypPegg.getsocial(this.retention), "Filter should not be null, choose one of Enum values.");
        jjbQypPegg.C0021jjbQypPegg.getsocial(this.mobile > 0, "Limit should be greater that zero");
        if (this.mau != null) {
            im.getsocial.sdk.socialgraph.a.a.jjbQypPegg.getsocial(this.mau);
        }
    }
}
