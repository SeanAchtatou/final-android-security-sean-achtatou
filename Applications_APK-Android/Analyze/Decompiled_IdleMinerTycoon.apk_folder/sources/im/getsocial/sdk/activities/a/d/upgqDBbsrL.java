package im.getsocial.sdk.activities.a.d;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import java.util.List;

/* compiled from: GetActivitiesUseCase */
public final class upgqDBbsrL extends jjbQypPegg {
    public final void getsocial(final ActivitiesQuery activitiesQuery, Callback<List<ActivityPost>> callback) {
        jjbQypPegg.C0020jjbQypPegg jjbqyppegg;
        final im.getsocial.sdk.activities.jjbQypPegg jjbqyppegg2 = im.getsocial.sdk.activities.jjbQypPegg.getsocial(activitiesQuery);
        if (jjbqyppegg2.getsocial() == ActivityPost.Type.POST) {
            final String str = im.getsocial.sdk.activities.a.b.jjbQypPegg.getsocial(jjbqyppegg2.attribution());
            jjbqyppegg = new jjbQypPegg.C0020jjbQypPegg<List<ActivityPost>>() {
                public final /* bridge */ /* synthetic */ Object getsocial() {
                    return upgqDBbsrL.this.getsocial().getsocial(str, activitiesQuery);
                }
            };
        } else {
            jjbqyppegg = new jjbQypPegg.C0020jjbQypPegg<List<ActivityPost>>() {
                public final /* synthetic */ Object getsocial() {
                    return upgqDBbsrL.this.getsocial().attribution(jjbqyppegg2.acquisition(), activitiesQuery);
                }
            };
        }
        getsocial(jjbqyppegg, callback);
    }
}
