package im.getsocial.sdk.activities.a.c;

import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.activities.Mention;
import im.getsocial.sdk.activities.PostAuthor;
import im.getsocial.sdk.activities.ReportingReason;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.internal.f.a.KSZKMmRWhZ;
import im.getsocial.sdk.internal.f.a.NgDaxXOnCs;
import im.getsocial.sdk.internal.f.a.krCuuqytsv;
import im.getsocial.sdk.internal.f.a.ofLJAxfaCe;
import im.getsocial.sdk.internal.f.a.xlPHPMtUBa;
import im.getsocial.sdk.internal.f.a.zoToeBNOjF;
import im.getsocial.sdk.internal.f.a.ztWNWCuZiM;
import im.getsocial.sdk.internal.k.a.cjrhisSQCL;
import im.getsocial.sdk.internal.m.QCXFOjcJkE;
import im.getsocial.sdk.usermanagement.pdwpUtZXDT;

/* compiled from: ActivitiesThriftyConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static zoToeBNOjF getsocial(ActivitiesQuery activitiesQuery) {
        im.getsocial.sdk.activities.jjbQypPegg jjbqyppegg = im.getsocial.sdk.activities.jjbQypPegg.getsocial(activitiesQuery);
        zoToeBNOjF zotoebnojf = new zoToeBNOjF();
        zotoebnojf.acquisition = jjbqyppegg.dau();
        zotoebnojf.attribution = jjbqyppegg.retention();
        zotoebnojf.getsocial = Integer.valueOf(jjbqyppegg.mobile());
        zotoebnojf.mobile = jjbqyppegg.mau();
        zotoebnojf.retention = Boolean.valueOf(jjbqyppegg.cat());
        zotoebnojf.dau = jjbqyppegg.viral();
        return zotoebnojf;
    }

    public static ActivityPost getsocial(ztWNWCuZiM ztwnwcuzim) {
        Action action;
        String str;
        String str2;
        String str3;
        if (ztwnwcuzim.attribution.acquisition != null) {
            String str4 = ztwnwcuzim.attribution.acquisition.attribution;
            str = ztwnwcuzim.attribution.acquisition.getsocial;
            str2 = str4;
            action = im.getsocial.sdk.actions.a.a.jjbQypPegg.getsocial(ztwnwcuzim.attribution.acquisition.acquisition);
        } else {
            str2 = null;
            str = null;
            action = null;
        }
        ActivityPost.Builder content = ActivityPost.builder().id(ztwnwcuzim.getsocial).commentsCount(QCXFOjcJkE.getsocial(ztwnwcuzim.mau)).likesCount(QCXFOjcJkE.getsocial(ztwnwcuzim.cat)).likedByMe(QCXFOjcJkE.getsocial(ztwnwcuzim.viral)).createdAt(QCXFOjcJkE.attribution(ztwnwcuzim.mobile)).stickyStart(QCXFOjcJkE.attribution(ztwnwcuzim.retention)).stickyEnd(QCXFOjcJkE.attribution(ztwnwcuzim.dau)).author(getsocial(ztwnwcuzim.acquisition)).content(ztwnwcuzim.attribution.getsocial, ztwnwcuzim.attribution.attribution, ztwnwcuzim.attribution.mobile, str2, str, action);
        String str5 = ztwnwcuzim.organic;
        if (!im.getsocial.sdk.internal.c.l.ztWNWCuZiM.getsocial(str5)) {
            if (ActivitiesQuery.GLOBAL_FEED.equals(str5)) {
                str3 = ActivitiesQuery.GLOBAL_FEED;
            } else if (str5.startsWith("s-")) {
                str3 = str5.substring(2);
            }
            return content.feedId(str3).mentions(QCXFOjcJkE.getsocial(ztwnwcuzim.growth, new QCXFOjcJkE.jjbQypPegg<ofLJAxfaCe, Mention>() {
                public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                    return jjbQypPegg.getsocial((ofLJAxfaCe) obj);
                }
            })).build();
        }
        str3 = "";
        return content.feedId(str3).mentions(QCXFOjcJkE.getsocial(ztwnwcuzim.growth, new QCXFOjcJkE.jjbQypPegg<ofLJAxfaCe, Mention>() {
            public final /* bridge */ /* synthetic */ Object getsocial(Object obj) {
                return jjbQypPegg.getsocial((ofLJAxfaCe) obj);
            }
        })).build();
    }

    static Mention getsocial(ofLJAxfaCe ofljaxface) {
        return Mention.builder().withUserId(ofljaxface.acquisition).withType(ofljaxface.mobile).withStartIndex(QCXFOjcJkE.getsocial(ofljaxface.getsocial)).withEndIndex(QCXFOjcJkE.getsocial(ofljaxface.attribution)).build();
    }

    public static PostAuthor getsocial(xlPHPMtUBa xlphpmtuba) {
        PostAuthor build = new PostAuthor.Builder(xlphpmtuba.getsocial).setAvatarUrl(xlphpmtuba.acquisition).setDisplayName(xlphpmtuba.attribution).setIdentities(im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(xlphpmtuba.mobile)).setVerified(QCXFOjcJkE.getsocial(xlphpmtuba.retention)).setPublicProperties(xlphpmtuba.mau).build();
        pdwpUtZXDT.getsocial(build, QCXFOjcJkE.getsocial(xlphpmtuba.dau));
        return build;
    }

    public static KSZKMmRWhZ getsocial(im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg) {
        KSZKMmRWhZ kSZKMmRWhZ = new KSZKMmRWhZ();
        kSZKMmRWhZ.getsocial = jjbqyppegg.getsocial;
        kSZKMmRWhZ.retention = jjbqyppegg.retention;
        if (jjbqyppegg.dau != null) {
            kSZKMmRWhZ.attribution = jjbqyppegg.dau.getsocial(cjrhisSQCL.IMAGE);
            kSZKMmRWhZ.dau = jjbqyppegg.dau.getsocial(cjrhisSQCL.VIDEO);
        }
        kSZKMmRWhZ.acquisition = jjbqyppegg.attribution;
        kSZKMmRWhZ.mobile = jjbqyppegg.acquisition;
        kSZKMmRWhZ.mau = im.getsocial.sdk.actions.a.a.jjbQypPegg.getsocial(jjbqyppegg.mobile);
        return kSZKMmRWhZ;
    }

    /* renamed from: im.getsocial.sdk.activities.a.c.jjbQypPegg$2  reason: invalid class name */
    /* compiled from: ActivitiesThriftyConverter */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] getsocial = new int[ReportingReason.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                im.getsocial.sdk.activities.ReportingReason[] r0 = im.getsocial.sdk.activities.ReportingReason.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                im.getsocial.sdk.activities.a.c.jjbQypPegg.AnonymousClass2.getsocial = r0
                int[] r0 = im.getsocial.sdk.activities.a.c.jjbQypPegg.AnonymousClass2.getsocial     // Catch:{ NoSuchFieldError -> 0x0014 }
                im.getsocial.sdk.activities.ReportingReason r1 = im.getsocial.sdk.activities.ReportingReason.INAPPROPRIATE_CONTENT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = im.getsocial.sdk.activities.a.c.jjbQypPegg.AnonymousClass2.getsocial     // Catch:{ NoSuchFieldError -> 0x001f }
                im.getsocial.sdk.activities.ReportingReason r1 = im.getsocial.sdk.activities.ReportingReason.SPAM     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: im.getsocial.sdk.activities.a.c.jjbQypPegg.AnonymousClass2.<clinit>():void");
        }
    }

    public static krCuuqytsv getsocial(ReportingReason reportingReason) {
        if (AnonymousClass2.getsocial[reportingReason.ordinal()] != 1) {
            return krCuuqytsv.SPAM;
        }
        return krCuuqytsv.INAPPROPRIATE_CONTENT;
    }

    public static NgDaxXOnCs getsocial(TagsQuery tagsQuery) {
        NgDaxXOnCs ngDaxXOnCs = new NgDaxXOnCs();
        ngDaxXOnCs.getsocial = Integer.valueOf(tagsQuery.getLimit());
        ngDaxXOnCs.attribution = tagsQuery.getQuery();
        ngDaxXOnCs.acquisition = tagsQuery.getFeedName();
        return ngDaxXOnCs;
    }
}
