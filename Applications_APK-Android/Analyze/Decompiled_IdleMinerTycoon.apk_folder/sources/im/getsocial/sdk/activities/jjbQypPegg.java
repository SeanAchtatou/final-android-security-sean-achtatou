package im.getsocial.sdk.activities;

import im.getsocial.sdk.activities.ActivityPost;
import java.util.List;

/* compiled from: ActivitiesQueryAccessHelper */
public final class jjbQypPegg {
    private final ActivitiesQuery getsocial;

    private jjbQypPegg(ActivitiesQuery activitiesQuery) {
        this.getsocial = activitiesQuery;
    }

    public static jjbQypPegg getsocial(ActivitiesQuery activitiesQuery) {
        return new jjbQypPegg(activitiesQuery);
    }

    public final ActivityPost.Type getsocial() {
        return this.getsocial.getsocial();
    }

    public final String attribution() {
        return this.getsocial.attribution();
    }

    public final String acquisition() {
        return this.getsocial.acquisition();
    }

    public final int mobile() {
        return this.getsocial.mobile();
    }

    public final String retention() {
        return this.getsocial.retention();
    }

    public final String dau() {
        return this.getsocial.dau();
    }

    public final String mau() {
        return this.getsocial.mau();
    }

    public final boolean cat() {
        return this.getsocial.cat();
    }

    public final List<String> viral() {
        return this.getsocial.viral();
    }

    public final void organic() {
        this.getsocial.organic();
    }
}
