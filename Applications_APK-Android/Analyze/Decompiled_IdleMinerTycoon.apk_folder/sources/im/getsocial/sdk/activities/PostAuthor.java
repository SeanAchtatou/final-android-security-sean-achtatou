package im.getsocial.sdk.activities;

import im.getsocial.sdk.usermanagement.PublicUser;
import java.util.Map;

public class PostAuthor extends PublicUser {
    protected boolean getsocial;

    protected PostAuthor() {
    }

    public boolean isVerified() {
        return this.getsocial;
    }

    /* access modifiers changed from: protected */
    public final void getsocial(PostAuthor postAuthor) {
        getsocial((PublicUser) postAuthor);
        postAuthor.getsocial = this.getsocial;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && getClass() == obj.getClass() && this.getsocial == ((PostAuthor) obj).getsocial && super.equals(obj);
    }

    public int hashCode() {
        boolean z = this.getsocial;
        return (z ? 1 : 0) + super.hashCode();
    }

    public static class Builder {
        PostAuthor getsocial = new PostAuthor();

        public Builder(String str) {
            String unused = this.getsocial.attribution = str;
        }

        public Builder setAvatarUrl(String str) {
            String unused = this.getsocial.mobile = str;
            return this;
        }

        public Builder setDisplayName(String str) {
            String unused = this.getsocial.acquisition = str;
            return this;
        }

        public Builder setIdentities(Map<String, String> map) {
            Map unused = this.getsocial.retention = map;
            return this;
        }

        public Builder setPublicProperties(Map<String, String> map) {
            Map unused = this.getsocial.dau = map;
            return this;
        }

        public Builder setVerified(boolean z) {
            this.getsocial.getsocial = z;
            return this;
        }

        public PostAuthor build() {
            PostAuthor postAuthor = new PostAuthor();
            this.getsocial.getsocial(postAuthor);
            return postAuthor;
        }
    }
}
