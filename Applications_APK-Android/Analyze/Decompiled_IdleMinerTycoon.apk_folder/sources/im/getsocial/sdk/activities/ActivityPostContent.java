package im.getsocial.sdk.activities;

import android.graphics.Bitmap;
import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.activities.a.a.jjbQypPegg;
import im.getsocial.sdk.media.MediaAttachment;

public final class ActivityPostContent {
    final jjbQypPegg getsocial;

    public static Builder createBuilderWithText(String str) {
        return new Builder().withText(str);
    }

    @Deprecated
    public static Builder createBuilderWithImage(Bitmap bitmap) {
        return new Builder().withImage(bitmap);
    }

    @Deprecated
    public static Builder createBuilderWithVideo(byte[] bArr) {
        return new Builder().withVideo(bArr);
    }

    @Deprecated
    public static Builder createBuilderWithButton(String str, String str2) {
        return new Builder().withButton(str, str2);
    }

    public static Builder createBuilderWithButton(String str, Action action) {
        return new Builder().withButton(str, action);
    }

    public static Builder createBuilderWithMediaAttachment(MediaAttachment mediaAttachment) {
        return new Builder().withMediaAttachment(mediaAttachment);
    }

    ActivityPostContent(jjbQypPegg jjbqyppegg) {
        this.getsocial = jjbqyppegg;
    }

    public final String getText() {
        return this.getsocial.getsocial;
    }

    @Deprecated
    public final String getButtonAction() {
        return this.getsocial.acquisition;
    }

    public final String getButtonTitle() {
        return this.getsocial.attribution;
    }

    public final Action getAction() {
        return this.getsocial.mobile;
    }

    public static final class Builder {
        private final jjbQypPegg getsocial = new jjbQypPegg();

        public final Builder withText(String str) {
            this.getsocial.getsocial = str;
            return this;
        }

        @Deprecated
        public final Builder withImage(Bitmap bitmap) {
            return withMediaAttachment(MediaAttachment.image(bitmap));
        }

        @Deprecated
        public final Builder withVideo(byte[] bArr) {
            return withMediaAttachment(MediaAttachment.video(bArr));
        }

        @Deprecated
        public final Builder withButton(String str, String str2) {
            this.getsocial.attribution = str;
            this.getsocial.acquisition = str2;
            return this;
        }

        public final Builder withButton(String str, Action action) {
            this.getsocial.attribution = str;
            this.getsocial.mobile = action;
            return this;
        }

        public final Builder withMediaAttachment(MediaAttachment mediaAttachment) {
            if (mediaAttachment != null) {
                this.getsocial.dau = im.getsocial.sdk.media.jjbQypPegg.getsocial(mediaAttachment);
            }
            return this;
        }

        public final ActivityPostContent build() {
            return new ActivityPostContent(this.getsocial);
        }
    }
}
