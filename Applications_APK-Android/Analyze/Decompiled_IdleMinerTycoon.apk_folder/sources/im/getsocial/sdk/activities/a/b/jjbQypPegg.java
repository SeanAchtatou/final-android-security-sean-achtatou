package im.getsocial.sdk.activities.a.b;

import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.ztWNWCuZiM;
import java.util.regex.Pattern;

/* compiled from: ActivitiesCommonFunctions */
public final class jjbQypPegg {
    private static final Pattern getsocial = Pattern.compile("^[a-zA-Z0-9_.-]*$");

    private jjbQypPegg() {
    }

    public static String getsocial(String str) {
        if (ActivitiesQuery.GLOBAL_FEED.equals(str)) {
            return str;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "Feed name can not be null");
        jjbQypPegg.C0021jjbQypPegg.getsocial(str.length() <= 64, "Feed name length can not be more than 64");
        jjbQypPegg.C0021jjbQypPegg.getsocial(getsocial.matcher(str).matches(), "Feed name should contain only letters, numbers, dots(.), dashes(-) and underscores(_).");
        return "s-" + str;
    }

    public static void getsocial(im.getsocial.sdk.activities.a.a.jjbQypPegg jjbqyppegg) {
        boolean z = false;
        boolean z2 = jjbqyppegg.dau != null;
        boolean z3 = !ztWNWCuZiM.getsocial(jjbqyppegg.attribution) && (!ztWNWCuZiM.getsocial(jjbqyppegg.acquisition) || jjbqyppegg.mobile != null);
        boolean z4 = !ztWNWCuZiM.getsocial(jjbqyppegg.getsocial);
        if (z3 || z2 || z4) {
            z = true;
        }
        jjbQypPegg.C0021jjbQypPegg.getsocial(z, "Can not post without any data.");
    }
}
