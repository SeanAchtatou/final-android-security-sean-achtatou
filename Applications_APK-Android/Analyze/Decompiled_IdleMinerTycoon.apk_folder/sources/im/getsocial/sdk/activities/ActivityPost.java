package im.getsocial.sdk.activities;

import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.internal.c.l.upgqDBbsrL;
import java.util.Date;
import java.util.List;

public final class ActivityPost {
    /* access modifiers changed from: private */
    public PostAuthor acquisition;
    /* access modifiers changed from: private */
    public jjbQypPegg attribution;
    /* access modifiers changed from: private */
    public boolean cat;
    /* access modifiers changed from: private */
    public long dau;
    /* access modifiers changed from: private */
    public String getsocial;
    /* access modifiers changed from: private */
    public List<Mention> growth;
    /* access modifiers changed from: private */
    public int mau;
    /* access modifiers changed from: private */
    public long mobile;
    /* access modifiers changed from: private */
    public String organic;
    /* access modifiers changed from: private */
    public long retention;
    /* access modifiers changed from: private */
    public int viral;

    public enum Type {
        POST,
        COMMENT
    }

    ActivityPost() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public final String getId() {
        return this.getsocial;
    }

    public final boolean hasText() {
        return (this.attribution == null || this.attribution.getsocial == null || this.attribution.getsocial.isEmpty()) ? false : true;
    }

    public final String getText() {
        if (hasText()) {
            return this.attribution.getsocial;
        }
        return null;
    }

    public final boolean hasImage() {
        return (this.attribution == null || this.attribution.attribution == null || this.attribution.attribution.isEmpty()) ? false : true;
    }

    public final boolean hasVideo() {
        return (this.attribution == null || this.attribution.mobile == null || this.attribution.mobile.isEmpty()) ? false : true;
    }

    public final String getImageUrl() {
        if (hasImage()) {
            return this.attribution.attribution;
        }
        return null;
    }

    public final String getVideoUrl() {
        if (hasVideo()) {
            return this.attribution.mobile;
        }
        return null;
    }

    public final boolean hasButton() {
        return (this.attribution == null || this.attribution.acquisition == null || this.attribution.acquisition.getsocial == null) ? false : true;
    }

    public final long getCreatedAt() {
        return this.mobile;
    }

    public final String getButtonTitle() {
        if (hasButton()) {
            return this.attribution.acquisition.getsocial;
        }
        return null;
    }

    public final Action getAction() {
        if (hasButton()) {
            return this.attribution.acquisition.acquisition;
        }
        return null;
    }

    @Deprecated
    public final String getButtonAction() {
        if (hasButton()) {
            return this.attribution.acquisition.attribution;
        }
        return null;
    }

    public final PostAuthor getAuthor() {
        return this.acquisition;
    }

    public final int getCommentsCount() {
        return this.mau;
    }

    public final int getLikesCount() {
        return this.viral;
    }

    public final boolean isLikedByMe() {
        return this.cat;
    }

    public final long getStickyStart() {
        return this.retention;
    }

    public final long getStickyEnd() {
        return this.dau;
    }

    public final boolean isStickyAt(Date date) {
        return date.after(new Date(this.retention)) && date.before(new Date(this.dau));
    }

    public final String getFeedId() {
        return this.organic;
    }

    public final List<Mention> getMentions() {
        return this.growth;
    }

    public final String toString() {
        return "ActivityPost{_id='" + getId() + '\'' + ", _text='" + getText() + '\'' + ", _imageProvider='" + getImageUrl() + ", _videoUrl='" + getVideoUrl() + '\'' + ", _buttonTitle='" + getButtonTitle() + '\'' + ", _buttonAction[DEPRECATED]='" + getButtonAction() + '\'' + ", _buttonAction='" + getAction() + '\'' + '}';
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ActivityPost activityPost = (ActivityPost) obj;
        if (!this.getsocial.equals(activityPost.getsocial) || this.mobile != activityPost.mobile || this.retention != activityPost.retention || this.dau != activityPost.dau || this.mau != activityPost.mau || this.cat != activityPost.cat || this.viral != activityPost.viral) {
            return false;
        }
        if (this.attribution == null ? activityPost.attribution != null : !this.attribution.equals(activityPost.attribution)) {
            return false;
        }
        if (!this.acquisition.equals(activityPost.acquisition)) {
            return false;
        }
        if (this.organic == null ? activityPost.organic == null : this.organic.equals(activityPost.organic)) {
            return upgqDBbsrL.getsocial(this.growth, activityPost.growth);
        }
        return false;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = ((((((((((((((((this.getsocial.hashCode() * 31) + (this.attribution != null ? this.attribution.hashCode() : 0)) * 31) + this.acquisition.hashCode()) * 31) + ((int) (this.mobile ^ (this.mobile >>> 32)))) * 31) + ((int) (this.retention ^ (this.retention >>> 32)))) * 31) + ((int) (this.dau ^ (this.dau >>> 32)))) * 31) + this.mau) * 31) + (this.cat ? 1 : 0)) * 31) + this.viral) * 31;
        if (this.organic != null) {
            i = this.organic.hashCode();
        }
        return hashCode + i;
    }

    static class jjbQypPegg {
        C0011jjbQypPegg acquisition;
        String attribution;
        String getsocial;
        String mobile;

        jjbQypPegg() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            jjbQypPegg jjbqyppegg = (jjbQypPegg) obj;
            if (this.getsocial == null ? jjbqyppegg.getsocial != null : !this.getsocial.equals(jjbqyppegg.getsocial)) {
                return false;
            }
            if (this.attribution == null ? jjbqyppegg.attribution != null : !this.attribution.equals(jjbqyppegg.attribution)) {
                return false;
            }
            if (this.acquisition != null) {
                return this.acquisition.equals(jjbqyppegg.acquisition);
            }
            return jjbqyppegg.acquisition == null;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = (((this.getsocial != null ? this.getsocial.hashCode() : 0) * 31) + (this.attribution != null ? this.attribution.hashCode() : 0)) * 31;
            if (this.acquisition != null) {
                i = this.acquisition.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: im.getsocial.sdk.activities.ActivityPost$jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
        static class C0011jjbQypPegg {
            Action acquisition;
            String attribution;
            String getsocial;

            C0011jjbQypPegg() {
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                C0011jjbQypPegg jjbqyppegg = (C0011jjbQypPegg) obj;
                if (this.getsocial == null ? jjbqyppegg.getsocial != null : !this.getsocial.equals(jjbqyppegg.getsocial)) {
                    return false;
                }
                if (this.acquisition == null ? jjbqyppegg.acquisition != null : !this.acquisition.equals(jjbqyppegg.acquisition)) {
                    return false;
                }
                if (this.attribution != null) {
                    return this.attribution.equals(jjbqyppegg.attribution);
                }
                return jjbqyppegg.attribution == null;
            }

            public int hashCode() {
                int i = 0;
                int hashCode = (((this.getsocial != null ? this.getsocial.hashCode() : 0) * 31) + (this.attribution != null ? this.attribution.hashCode() : 0)) * 31;
                if (this.acquisition != null) {
                    i = this.acquisition.hashCode();
                }
                return hashCode + i;
            }
        }
    }

    public static class Builder {
        private final ActivityPost getsocial = new ActivityPost();

        Builder() {
        }

        public ActivityPost build() {
            return this.getsocial;
        }

        public Builder id(String str) {
            String unused = this.getsocial.getsocial = str;
            return this;
        }

        public Builder commentsCount(int i) {
            int unused = this.getsocial.mau = i;
            return this;
        }

        public Builder likesCount(int i) {
            int unused = this.getsocial.viral = i;
            return this;
        }

        public Builder likedByMe(boolean z) {
            boolean unused = this.getsocial.cat = z;
            return this;
        }

        public Builder createdAt(long j) {
            long unused = this.getsocial.mobile = j;
            return this;
        }

        public Builder stickyStart(long j) {
            long unused = this.getsocial.retention = j;
            return this;
        }

        public Builder stickyEnd(long j) {
            long unused = this.getsocial.dau = j;
            return this;
        }

        public Builder author(PostAuthor postAuthor) {
            PostAuthor unused = this.getsocial.acquisition = postAuthor;
            return this;
        }

        public Builder feedId(String str) {
            String unused = this.getsocial.organic = str;
            return this;
        }

        public Builder mentions(List<Mention> list) {
            List unused = this.getsocial.growth = upgqDBbsrL.getsocial(list);
            return this;
        }

        @Deprecated
        public Builder content(String str, String str2, String str3, String str4) {
            return content(str, str2, (String) null, str3, str4);
        }

        @Deprecated
        public Builder content(String str, String str2, String str3, String str4, String str5) {
            return content(str, str2, str3, str4, str5, null);
        }

        public Builder content(String str, String str2, String str3, String str4, Action action) {
            return content(str, str2, str3, str4, null, action);
        }

        public Builder content(String str, String str2, String str3, String str4, String str5, Action action) {
            jjbQypPegg unused = this.getsocial.attribution = new jjbQypPegg();
            this.getsocial.attribution.getsocial = str;
            this.getsocial.attribution.attribution = str2;
            this.getsocial.attribution.mobile = str3;
            if (!((str4 == null || str5 == null) && action == null)) {
                this.getsocial.attribution.acquisition = new jjbQypPegg.C0011jjbQypPegg();
                this.getsocial.attribution.acquisition.getsocial = str4;
                this.getsocial.attribution.acquisition.acquisition = action;
                this.getsocial.attribution.acquisition.attribution = str5;
            }
            return this;
        }
    }
}
