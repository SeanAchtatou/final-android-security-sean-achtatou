package im.getsocial.sdk.activities;

public final class MentionTypes {
    public static final String APP = "app";
    public static final String USER = "user";

    private MentionTypes() {
    }
}
