package im.getsocial.sdk.socialgraph.a.a;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.usermanagement.PrivateUser;
import im.getsocial.sdk.usermanagement.a.a.pdwpUtZXDT;
import im.getsocial.sdk.usermanagement.cjrhisSQCL;
import java.util.HashMap;
import java.util.List;

/* compiled from: SocialGraphCommonFunctions */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static void getsocial(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.attribution(str), "User id can not be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(str.trim()), "User id can not be null or empty");
        jjbQypPegg.C0021jjbQypPegg.getsocial(str.matches("[0-9]+"), "User id should contain only numbers");
    }

    public static pdwpUtZXDT getsocial(List<String> list) {
        String str = im.getsocial.sdk.socialgraph.a.d.jjbQypPegg.getsocial(list);
        pdwpUtZXDT pdwputzxdt = new pdwpUtZXDT();
        HashMap hashMap = new HashMap();
        hashMap.put("friendlist-hash", str);
        pdwputzxdt.mobile(hashMap);
        return pdwputzxdt;
    }

    public static boolean getsocial(List<String> list, PrivateUser privateUser) {
        return !im.getsocial.sdk.socialgraph.a.d.jjbQypPegg.getsocial(list).equalsIgnoreCase(new cjrhisSQCL(privateUser).getsocial("friendlist-hash"));
    }
}
