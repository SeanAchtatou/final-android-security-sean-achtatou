package im.getsocial.sdk.socialgraph.a.d;

import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.List;

/* compiled from: FriendsListValidator */
public final class jjbQypPegg {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(jjbQypPegg.class);

    private jjbQypPegg() {
    }

    public static String getsocial(List<String> list) {
        if (list == null || list.isEmpty()) {
            return "";
        }
        Collections.sort(list);
        StringBuilder sb = new StringBuilder();
        for (String append : list) {
            sb.append(append);
        }
        return getsocial(sb.toString());
    }

    private static String getsocial(String str) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA-256").digest(str.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            int length = digest.length;
            for (int i = 0; i < length; i++) {
                sb.append(String.format("%02x", Byte.valueOf(digest[i])));
            }
            return sb.toString();
        } catch (Exception e) {
            getsocial.getsocial("Could not generate hash: ", e);
            return "";
        }
    }
}
