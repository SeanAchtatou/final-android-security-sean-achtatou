package im.getsocial.sdk.socialgraph;

import im.getsocial.sdk.usermanagement.PublicUser;
import java.util.Map;

public class SuggestedFriend extends PublicUser {
    /* access modifiers changed from: private */
    public int getsocial;

    public int getMutualFriendsCount() {
        return this.getsocial;
    }

    /* access modifiers changed from: protected */
    public final void getsocial(SuggestedFriend suggestedFriend) {
        getsocial((PublicUser) suggestedFriend);
        suggestedFriend.getsocial = this.getsocial;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj) && this.getsocial == ((SuggestedFriend) obj).getsocial;
    }

    public int hashCode() {
        return super.hashCode() + this.getsocial;
    }

    public static class Builder {
        SuggestedFriend getsocial = new SuggestedFriend();

        public Builder(String str) {
            String unused = this.getsocial.attribution = str;
        }

        public Builder setAvatarUrl(String str) {
            String unused = this.getsocial.mobile = str;
            return this;
        }

        public Builder setDisplayName(String str) {
            String unused = this.getsocial.acquisition = str;
            return this;
        }

        public Builder setIdentities(Map<String, String> map) {
            Map unused = this.getsocial.retention = map;
            return this;
        }

        public Builder setPublicProperties(Map<String, String> map) {
            Map unused = this.getsocial.dau = map;
            return this;
        }

        public Builder setMutualFriendsCount(int i) {
            int unused = this.getsocial.getsocial = i;
            return this;
        }

        public SuggestedFriend build() {
            SuggestedFriend suggestedFriend = new SuggestedFriend();
            this.getsocial.getsocial(suggestedFriend);
            return suggestedFriend;
        }
    }
}
