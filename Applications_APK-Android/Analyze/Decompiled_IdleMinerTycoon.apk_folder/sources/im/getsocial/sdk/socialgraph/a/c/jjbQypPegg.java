package im.getsocial.sdk.socialgraph.a.c;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;

/* compiled from: AddFriendUseCase */
public final class jjbQypPegg extends im.getsocial.sdk.internal.c.k.jjbQypPegg {
    @XdbacJlTDQ
    im.getsocial.sdk.usermanagement.a.c.jjbQypPegg getsocial;

    public final void getsocial(final String str, Callback<Integer> callback) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(callback), "Can not executeInBackground AddFriendUseCase with null callback");
        jjbQypPegg.C0021jjbQypPegg.getsocial(!str.equals(this.getsocial.getsocial().getId()), "You can not add yourself to your friend list.");
        im.getsocial.sdk.socialgraph.a.a.jjbQypPegg.getsocial(str);
        getsocial(new jjbQypPegg.C0020jjbQypPegg<Integer>() {
            public final /* synthetic */ Object getsocial() {
                return jjbQypPegg.this.getsocial().acquisition(str);
            }
        }, callback);
    }
}
