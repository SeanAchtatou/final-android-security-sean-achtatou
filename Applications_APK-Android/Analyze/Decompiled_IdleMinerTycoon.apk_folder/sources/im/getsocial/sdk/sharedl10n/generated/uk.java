package im.getsocial.sdk.sharedl10n.generated;

public class uk extends LanguageStrings {
    public uk() {
        this.OkButton = "ОК";
        this.CancelButton = "Скасувати";
        this.ConnectionLostTitle = "Немає зв'язку";
        this.ConnectionLostMessage = "Ой! Здається, немає зв'язку з інтернетом. Будь ласка, спробуйте знову.";
        this.PullDownToRefresh = "Потягніть вниз, щоб оновити";
        this.PullUpToLoadMore = "Потягніть вгору, щоб завантажити більше";
        this.ReleaseToRefresh = "Відпустіть, щоб оновити";
        this.ReleaseToLoadMore = "Відпустіть, щоб завантажити більше";
        this.ErrorAlertMessageTitle = "Ой!";
        this.ErrorAlertMessage = "Щось пішло не так. Будь ласка, спробуйте знову";
        this.InviteFriends = "Запросити друзів";
        this.TimestampJustNow = "щойно";
        this.TimestampSeconds = "%1.0fс";
        this.TimestampMinutes = "%1.0fхв";
        this.TimestampHours = "%1.0fгод";
        this.TimestampDays = "%1.0fд";
        this.TimestampWeeks = "%1.0fт";
        this.TimestampYesterday = "Вчора";
        this.ActivityTitle = "Стрічка новин";
        this.ActivityPostPlaceholder = "Що нового?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Немає новин";
        this.ActivityNoSearchResultsPlaceholderTitle = "Немає результатів для **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Наразі тут немає постів. Чому б не додати декілька?";
        this.ViewCommentsLink = "коментарі";
        this.ViewComments2Link = "коментарів";
        this.ViewCommentLink = "коментар";
        this.CommentsTitle = "Коментарі";
        this.CommentsPostPlaceholder = "Залишити коментар";
        this.CommentsMoreCommentsButton = "Завантажити попередні коментарі";
        this.ViewLikesLink = "лайки";
        this.ViewLikes2Link = "лайків";
        this.ViewLikeLink = "лайк";
        this.ReportAsSpam = "Пожалітись на спам";
        this.ReportAsInappropriate = "Пожалітись на неприпустимий контент";
        this.ReportNotificationTitle = "Дякуємо!";
        this.ReportNotificationText = "Наш модератор перегляне позначений контент якнайшвидше";
        this.DeleteActivity = "Видалити пост";
        this.DeleteComment = "Видалити коментар";
        this.ActivityNotFound = "Пост більше не доступний";
        this.ErrorYoureBanned = "Твій доступ до деяких функцій тимчасово обмежений";
        this.NotificationsChannelName = "Соціальні";
        this.NCUITitle = "Всі сповіщення";
        this.NCUIFriendRequestConfirmButton = "Підтвердити";
        this.NCUIFriendRequestConfirmationText = "Тепер ви друзі";
        this.NCUISectionHeader = "Сповіщення";
        this.NCUIPlaceholderTitle = "Все прочитано";
        this.NCUIPlaceholderText = "Нові сповіщення появляться тут";
        this.NCUIDeleteAllButton = "Видалити всі сповіщення";
        this.NCUIMarkAllAsReadButton = "Позначити всі сповіщення прочитаними";
        this.NCUIMarkAsReadButton = "Позначити сповіщення прочитаним";
        this.NCUIMarkAsUnreadButton = "Позначити сповіщення не прочитаним";
        this.NCUIDeleteButton = "Видалити сповіщення";
        this.NCUIFriendRequestDeleteButton = "Видалити";
    }
}
