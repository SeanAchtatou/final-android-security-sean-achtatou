package im.getsocial.sdk.sharedl10n.generated;

public class nb extends LanguageStrings {
    public nb() {
        this.OkButton = "OK";
        this.CancelButton = "Avbryt";
        this.ConnectionLostTitle = "Mistet tilkoblingen";
        this.ConnectionLostMessage = "Oi sann! Ser ut som om du har mistet Internett-tilkoblingen. Koble deg til på nytt.";
        this.PullDownToRefresh = "Trekk ned for å oppdatere";
        this.PullUpToLoadMore = "Trekk opp til å laste mer";
        this.ReleaseToRefresh = "Slipp for å oppdatere";
        this.ReleaseToLoadMore = "Slipp for å laste mer";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Noe gikk galt. Vennligst prøv igjen!";
        this.InviteFriends = "Inviter venner";
        this.TimestampJustNow = "akkurat nå";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0ft";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fu";
        this.TimestampYesterday = "I går";
        this.ActivityTitle = "Aktivitet";
        this.ActivityPostPlaceholder = "Hva skjer?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Ingen aktivitet";
        this.ActivityNoSearchResultsPlaceholderTitle = "Ingen resultater for **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Det er ingen aktivitet her ennå. Hvorfor ikke starte noe?";
        this.ViewCommentsLink = "kommentarer";
        this.ViewComments2Link = "kommentarer";
        this.ViewCommentLink = "kommentar";
        this.CommentsTitle = "Kommentarer";
        this.CommentsPostPlaceholder = "Legg inn en kommentar";
        this.CommentsMoreCommentsButton = "Se eldre kommentarer";
        this.ViewLikesLink = "liker";
        this.ViewLikes2Link = "liker";
        this.ViewLikeLink = "lik";
        this.ReportAsSpam = "Rapporter som spam";
        this.ReportAsInappropriate = "Rapporter som upassende";
        this.ReportNotificationTitle = "Takk!";
        this.ReportNotificationText = "En av våre moderatorer vil gjennomgå innholdet så fort som mulig";
        this.DeleteActivity = "Slett aktivitet";
        this.DeleteComment = "Slett kommentar";
        this.ActivityNotFound = "Aktiviteten er ikke lenger tilgjengelig";
        this.ErrorYoureBanned = "Tilgangen din til enkelte funksjoner er midlertidig begrenset";
        this.NotificationsChannelName = "Sosial";
        this.NCUITitle = "Alle varsler";
        this.NCUIFriendRequestConfirmButton = "Bekreft";
        this.NCUIFriendRequestConfirmationText = "Du er nå tilkoblet";
        this.NCUISectionHeader = "Varsler";
        this.NCUIPlaceholderTitle = "Alle er fanget opp";
        this.NCUIPlaceholderText = "Nye varsler vises her";
        this.NCUIDeleteAllButton = "Slett alle varsler";
        this.NCUIMarkAllAsReadButton = "Merk alle varsler som lest";
        this.NCUIMarkAsReadButton = "Merk varsel som lest";
        this.NCUIMarkAsUnreadButton = "Merk varsel som ulest";
        this.NCUIDeleteButton = "Slett varsel";
        this.NCUIFriendRequestDeleteButton = "Slett";
    }
}
