package im.getsocial.sdk.sharedl10n.generated;

public class is extends LanguageStrings {
    public is() {
        this.OkButton = "Ókei";
        this.CancelButton = "Hætta";
        this.ConnectionLostTitle = "Tengingin rofnaði";
        this.ConnectionLostMessage = "Æ, æ! Tengingin þín virðist hafa rofnað. Vinsamlegast tengstu aftur.";
        this.PullDownToRefresh = "Dragðu niður til að endurhlaða";
        this.PullUpToLoadMore = "Dragðu upp til að hlaða fleiru";
        this.ReleaseToRefresh = "Slepptu til að endurhlaða";
        this.ReleaseToLoadMore = "Slepptu til að hlaða fleiru";
        this.ErrorAlertMessageTitle = "Úbbs";
        this.ErrorAlertMessage = "Eitthvað fór úrskeiðis. Vinsamlegast reyndu aftur!";
        this.InviteFriends = "Bjóða vinum";
        this.TimestampJustNow = "rétt í þessu";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "í gær";
        this.ActivityTitle = "Virkni";
        this.ActivityPostPlaceholder = "Hvað segirðu?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Engin virkni";
        this.ActivityNoSearchResultsPlaceholderTitle = "Engar niðurstöður fyrir **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Þetta svæði er óvirkt ennþá. Viltu ekki byrja á einhverju?";
        this.ViewCommentsLink = "Athugasemdir";
        this.ViewComments2Link = "Athugasemdir";
        this.ViewCommentLink = "athugasemd";
        this.CommentsTitle = "Athugasemdir";
        this.CommentsPostPlaceholder = "Skildu eftir athugasemd";
        this.CommentsMoreCommentsButton = "Sjá eldri athugasemdir";
        this.ViewLikesLink = "líkar við";
        this.ViewLikes2Link = "líkar við";
        this.ViewLikeLink = "líka við";
        this.ReportAsSpam = "Merkja sem ruslpóst";
        this.ReportAsInappropriate = "Merkja sem óviðeigandi";
        this.ReportNotificationTitle = "Takk fyrir!";
        this.ReportNotificationText = "Einn af umsjónarmönnum okkar mun fara yfir efnið eins fljótt og hægt er";
        this.DeleteActivity = "Eyða færslu";
        this.DeleteComment = "Eyða athugasemd";
        this.ActivityNotFound = "Aðgerð er ekki lengur til staðar";
        this.ErrorYoureBanned = "Aðgangur þinn að ákveðnum aðgerðum hefur verið takmarkaður tímabundið";
        this.NotificationsChannelName = "Félagsleg";
        this.NCUITitle = "Allar tilkynningar";
        this.NCUIFriendRequestConfirmButton = "Staðfesta";
        this.NCUIFriendRequestConfirmationText = "Þú ert núna tengdur";
        this.NCUISectionHeader = "Tilkynningar";
        this.NCUIPlaceholderTitle = "Búinn með allar";
        this.NCUIPlaceholderText = "Nýjar tilkynningar birtast hér";
        this.NCUIDeleteAllButton = "Eyða öllum tilkynningum";
        this.NCUIMarkAllAsReadButton = "Merkja allar tilkynningar sem lesnar";
        this.NCUIMarkAsReadButton = "Merkja tilkynningu sem lesna";
        this.NCUIMarkAsUnreadButton = "Merkja tilkynningu sem ólesna";
        this.NCUIDeleteButton = "Eyða tilkynningu";
        this.NCUIFriendRequestDeleteButton = "Eyða";
    }
}
