package im.getsocial.sdk.sharedl10n.generated;

public class zhHans extends LanguageStrings {
    public zhHans() {
        this.OkButton = "还行";
        this.CancelButton = "取消";
        this.ConnectionLostTitle = "失去连接";
        this.ConnectionLostMessage = "哦！看来您失去了互联网连接。请重新连接。";
        this.PullDownToRefresh = "下拉刷新";
        this.PullUpToLoadMore = "上拉加载更多";
        this.ReleaseToRefresh = "释放刷新";
        this.ReleaseToLoadMore = "释放加载更多";
        this.ErrorAlertMessageTitle = "哎呀";
        this.ErrorAlertMessage = "出错了。请重试！";
        this.InviteFriends = "邀请好友";
        this.TimestampJustNow = "立即";
        this.TimestampSeconds = "%1.0f秒";
        this.TimestampMinutes = "%1.0f分";
        this.TimestampHours = "%1.0f小时";
        this.TimestampDays = "%1.0f天";
        this.TimestampWeeks = "%1.0f周";
        this.ActivityTitle = "活动";
        this.ActivityPostPlaceholder = "发生了什么？";
        this.ActivityAllNoActivitiesPlaceholderTitle = "无活动";
        this.ActivityNoSearchResultsPlaceholderTitle = "没有关于**[SEARCH_TERM]**的结果";
        this.ActivityAllNoActivitiesPlaceholderMessage = "尚无活动。您为何不发起活动？";
        this.ViewCommentsLink = "评论";
        this.ViewComments2Link = "评论";
        this.ViewCommentLink = "评论";
        this.CommentsTitle = "评论";
        this.CommentsPostPlaceholder = "发表评论";
        this.CommentsMoreCommentsButton = "查看旧评论";
        this.ViewLikesLink = "喜欢";
        this.ViewLikes2Link = "喜欢";
        this.ViewLikeLink = "喜欢";
        this.ReportAsSpam = "报告为垃圾邮件";
        this.ReportAsInappropriate = "报告为不恰当";
        this.ReportNotificationTitle = "谢谢！";
        this.ReportNotificationText = "会有一名版主尽快审核内容";
        this.DeleteActivity = "删除活动";
        this.DeleteComment = "删除评论";
        this.ActivityNotFound = "活动已不再可用";
        this.ErrorYoureBanned = "您对有些功能的使用权限暂时受限";
        this.NotificationsChannelName = "社交";
        this.NCUITitle = "所有通知";
        this.NCUIFriendRequestConfirmButton = "确认";
        this.NCUIFriendRequestConfirmationText = "您已连接";
        this.NCUISectionHeader = "通知";
        this.NCUIPlaceholderTitle = "全部已读";
        this.NCUIPlaceholderText = "新通知将显示在此处";
        this.NCUIDeleteAllButton = "删除所有通知";
        this.NCUIMarkAllAsReadButton = "将所有通知标记为已读";
        this.NCUIMarkAsReadButton = "将通知标记为已读";
        this.NCUIMarkAsUnreadButton = "将通知标记为未读";
        this.NCUIDeleteButton = "删除通知";
        this.NCUIFriendRequestDeleteButton = "删除";
    }
}
