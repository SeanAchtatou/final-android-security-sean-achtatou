package im.getsocial.sdk.sharedl10n.generated;

public class pt extends LanguageStrings {
    public pt() {
        this.OkButton = "OK";
        this.CancelButton = "Cancelar";
        this.ConnectionLostTitle = "Ligação perdida";
        this.ConnectionLostMessage = "Uh oh! Parece que a tua ligação à internet perdeu-se. Por favor, liga-te novamente.";
        this.PullDownToRefresh = "Puxa para cima para carregar";
        this.PullUpToLoadMore = "Puxa para baixo para carregar mais";
        this.ReleaseToRefresh = "Larga para recarregar";
        this.ReleaseToLoadMore = "Solta para carregar mais";
        this.ErrorAlertMessageTitle = "Ops!";
        this.ErrorAlertMessage = "Algo de errado se passou. Por favor, tenta novamente!";
        this.InviteFriends = "Convidar amigos";
        this.TimestampJustNow = "agora mesmo";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fsem";
        this.TimestampYesterday = "Ontem";
        this.ActivityTitle = "Atividade";
        this.ActivityPostPlaceholder = "Diz qualquer coisa";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Sem actividades";
        this.ActivityNoSearchResultsPlaceholderTitle = "Sem resultados para **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Ainda não há nenhuma atividade aqui. Porque não começas algo?";
        this.ViewCommentsLink = "comentários";
        this.ViewComments2Link = "comentários";
        this.ViewCommentLink = "comentário";
        this.CommentsTitle = "Comentários";
        this.CommentsPostPlaceholder = "Comenta";
        this.CommentsMoreCommentsButton = "Ver comentários mais antigos";
        this.ViewLikesLink = "gostos";
        this.ViewLikes2Link = "gostos";
        this.ViewLikeLink = "gostar";
        this.ReportAsSpam = "Reportar como spam";
        this.ReportAsInappropriate = "Reportar como inapropriado";
        this.ReportNotificationTitle = "Obrigado!";
        this.ReportNotificationText = "Um dos nossos moderadores irá rever o conteúdo assim que possível";
        this.DeleteActivity = "Apagar atividade";
        this.DeleteComment = "Apagar comentário";
        this.ActivityNotFound = "A atividade já não está disponível";
        this.ErrorYoureBanned = "O seu acesso foi temporariamente limitado para algumas funcionalidades";
        this.NotificationsChannelName = "Social";
        this.NCUITitle = "Todas notificações";
        this.NCUIFriendRequestConfirmButton = "Confirmar";
        this.NCUIFriendRequestConfirmationText = "Você agora está conectado";
        this.NCUISectionHeader = "Notificações";
        this.NCUIPlaceholderTitle = "Todas lidas";
        this.NCUIPlaceholderText = "Novas notificações aparecerão aqui";
        this.NCUIDeleteAllButton = "Apagar todas notificações";
        this.NCUIMarkAllAsReadButton = "Marcar todas notificações como lidas";
        this.NCUIMarkAsReadButton = "Marcar notificação como lida";
        this.NCUIMarkAsUnreadButton = "Marcar notificação como não lida";
        this.NCUIDeleteButton = "Apagar notificação";
        this.NCUIFriendRequestDeleteButton = "Apagar";
    }
}
