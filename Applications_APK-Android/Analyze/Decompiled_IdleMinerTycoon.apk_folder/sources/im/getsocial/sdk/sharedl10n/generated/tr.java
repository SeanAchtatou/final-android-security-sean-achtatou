package im.getsocial.sdk.sharedl10n.generated;

public class tr extends LanguageStrings {
    public tr() {
        this.OkButton = "Tamam";
        this.CancelButton = "İptal";
        this.ConnectionLostTitle = "Bağlantı Kayıp";
        this.ConnectionLostMessage = "Uh oh! Internet bağlantınızı kaybettiğinizi gözlemledik. Lütfen yeniden bağlayın.";
        this.PullDownToRefresh = "Yenilemek için aşağı çek";
        this.PullUpToLoadMore = "Fazlası için yukarı çek";
        this.ReleaseToRefresh = "Yenilemek için serbest bırak";
        this.ReleaseToLoadMore = "Fazlası için serbest bırak";
        this.ErrorAlertMessageTitle = "Pardon!";
        this.ErrorAlertMessage = "Birseyler ters gitti. Lütfen tekrar deneyin!";
        this.InviteFriends = "Arkadaşlarını davet et";
        this.TimestampJustNow = "az önce";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Dün";
        this.ActivityTitle = "Etkinlik";
        this.ActivityPostPlaceholder = "Naber?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Hiçbir etkinlik yok";
        this.ActivityNoSearchResultsPlaceholderTitle = "**[SEARCH_TERM]** için hiçbir sonuç yok";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Burada herhangi bir etkinlik yok. Neden sen başlatmıyorsun?";
        this.ViewCommentsLink = "yorumlar";
        this.ViewComments2Link = "yorumlar";
        this.ViewCommentLink = "yorum";
        this.CommentsTitle = "Yorumlar";
        this.CommentsPostPlaceholder = "Yorum yaz";
        this.CommentsMoreCommentsButton = "Eski yorumları göster";
        this.ViewLikesLink = "beğeniler";
        this.ViewLikes2Link = "beğeniler";
        this.ViewLikeLink = "beğeni";
        this.ReportAsSpam = "Spam olarak işaretle";
        this.ReportAsInappropriate = "Uygunsuz olarak işaretle";
        this.ReportNotificationTitle = "Teşekkürler!";
        this.ReportNotificationText = "Moderatörlerimiz bu içeriği en kısa sürede inceleyecek";
        this.DeleteActivity = "Sil";
        this.DeleteComment = "Sil";
        this.ActivityNotFound = "Bu aktivite artık yok";
        this.ErrorYoureBanned = "Bazı özelliklere erişiminiz geçici olarak engellendi";
        this.NotificationsChannelName = "Sosyal";
        this.NCUITitle = "Tüm bildirimler";
        this.NCUIFriendRequestConfirmButton = "Onayla";
        this.NCUIFriendRequestConfirmationText = "Artık arkadaşsınız";
        this.NCUISectionHeader = "Bildirimler";
        this.NCUIPlaceholderTitle = "Hiç bildirim yok";
        this.NCUIPlaceholderText = "Yeni bildirimler burada gözükecek";
        this.NCUIDeleteAllButton = "Tümünü sil";
        this.NCUIMarkAllAsReadButton = "Hepsini okundu olarak işaretle";
        this.NCUIMarkAsReadButton = "Okundu olarak işaretle";
        this.NCUIMarkAsUnreadButton = "Okunmadı olarak işaretle";
        this.NCUIDeleteButton = "Sil";
        this.NCUIFriendRequestDeleteButton = "Sil";
    }
}
