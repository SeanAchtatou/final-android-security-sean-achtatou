package im.getsocial.sdk.sharedl10n.generated;

public class de extends LanguageStrings {
    public de() {
        this.OkButton = "OK";
        this.CancelButton = "Abbrechen";
        this.ConnectionLostTitle = "Verbindung unterbrochen";
        this.ConnectionLostMessage = "Oh-oh! Sieht so aus, als wäre Ihre Internetverbindung unterbrochen. Bitte erneut verbinden.";
        this.PullDownToRefresh = "Zum Aktualisieren nach unten ziehen";
        this.PullUpToLoadMore = "Nach oben ziehen, um mehr zu laden";
        this.ReleaseToRefresh = "Freigeben, um zu aktualisieren";
        this.ReleaseToLoadMore = "Freigeben, um mehr zu laden";
        this.ErrorAlertMessageTitle = "Ups!";
        this.ErrorAlertMessage = "Es hat leider nicht geklappt. Probier es bitte noch einmal!";
        this.InviteFriends = "Freunde einladen";
        this.TimestampJustNow = "Gerade eben";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Gestern";
        this.ActivityTitle = "Aktivität";
        this.ActivityPostPlaceholder = "Sagen Sie etwas!";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Keine Aktivität";
        this.ActivityNoSearchResultsPlaceholderTitle = "Keine Ergebnisse für **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Es gibt hier noch keine Aktivität. Warum starten Sie nicht etwas?";
        this.ViewCommentsLink = "kommentare";
        this.ViewComments2Link = "kommentare";
        this.ViewCommentLink = "kommentar";
        this.CommentsTitle = "Kommentare";
        this.CommentsPostPlaceholder = "Hinterlassen Sie einen Kommentar";
        this.CommentsMoreCommentsButton = "Ältere Kommentare ansehen";
        this.ViewLikesLink = "likes";
        this.ViewLikes2Link = "likes";
        this.ViewLikeLink = "like";
        this.ReportAsSpam = "Als Spam melden";
        this.ReportAsInappropriate = "Als unangemessenen Inhalt melden";
        this.ReportNotificationTitle = "Danke!";
        this.ReportNotificationText = "Einer unserer Moderatoren wird den Inhalt so schnell wie möglich überprüfen";
        this.DeleteActivity = "Aktivität löschen";
        this.DeleteComment = "Kommentar löschen";
        this.ActivityNotFound = "Aktivität ist nicht mehr verfügbar";
        this.ErrorYoureBanned = "Ihr Zugriff zu einigen Funktionen wurde vorübergehend begrenzt.";
        this.NotificationsChannelName = "Soziale";
        this.NCUITitle = "Alle Benachrichtigungen";
        this.NCUIFriendRequestConfirmButton = "Bestätigen";
        this.NCUIFriendRequestConfirmationText = "Sie sind jetzt verbunden";
        this.NCUISectionHeader = "Benachrichtigungen";
        this.NCUIPlaceholderTitle = "Alle gelesen";
        this.NCUIPlaceholderText = "Neue Benachrichtigungen erscheinen hier";
        this.NCUIDeleteAllButton = "Alle Benachrichtigungen löschen";
        this.NCUIMarkAllAsReadButton = "Alle Benachrichtigungen als Gelesen markieren";
        this.NCUIMarkAsReadButton = "Benachrichtigung als Gelesen markieren";
        this.NCUIMarkAsUnreadButton = "Benachrichtigung als Ungelesen markieren";
        this.NCUIDeleteButton = "Benachrichtigung löschen";
        this.NCUIFriendRequestDeleteButton = "Löschen";
    }
}
