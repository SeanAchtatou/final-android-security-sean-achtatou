package im.getsocial.sdk.sharedl10n.generated;

public class da extends LanguageStrings {
    public da() {
        this.OkButton = "OK";
        this.CancelButton = "Annuller";
        this.ConnectionLostTitle = "Forbindelse tabt";
        this.ConnectionLostMessage = "Åh nej! Det ser ud til, at din internetforbindelse er tabt. Tilslut venligst igen.";
        this.PullDownToRefresh = "Træk ned for at genopfriske";
        this.PullUpToLoadMore = "Træk op for at indlæse flere";
        this.ReleaseToRefresh = "Slip for at genopfriske";
        this.ReleaseToLoadMore = "Slip for at indlæse flere";
        this.ErrorAlertMessageTitle = "Ups!";
        this.ErrorAlertMessage = "Noget gik galt. Prøv igen!";
        this.InviteFriends = "Inviter venner";
        this.TimestampJustNow = "Lige nu";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0ft";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fu";
        this.TimestampYesterday = "I går";
        this.ActivityTitle = "Aktivitet";
        this.ActivityPostPlaceholder = "Sig noget!";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Ingen aktivitet";
        this.ActivityNoSearchResultsPlaceholderTitle = "Ingen resultater for **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Der er ingen aktivitet her endnu. Hvad med at du starter noget?";
        this.ViewCommentsLink = "kommentarer";
        this.ViewComments2Link = "kommentarer";
        this.ViewCommentLink = "kommentar";
        this.CommentsTitle = "Kommentarer";
        this.CommentsPostPlaceholder = "Efterlad en kommentar";
        this.CommentsMoreCommentsButton = "Se ældre kommentarer";
        this.ViewLikesLink = "likes";
        this.ViewLikes2Link = "likes";
        this.ViewLikeLink = "like";
        this.ReportAsSpam = "Indberet som spam";
        this.ReportAsInappropriate = "Indberet som upassende";
        this.ReportNotificationTitle = "Tak skal du have!";
        this.ReportNotificationText = "En af vores moderatorer vil vurdere indholdet så snart som muligt";
        this.DeleteActivity = "Slet aktivitet";
        this.DeleteComment = "Slet kommentar";
        this.ActivityNotFound = "Aktiviteten er ikke længere tilgængelig";
        this.ErrorYoureBanned = "Din adgang til nogle funktioner er blevet midlertidigt begrænset.";
        this.NotificationsChannelName = "Social";
        this.NCUITitle = "Alle beskeder";
        this.NCUIFriendRequestConfirmButton = "Bekræft";
        this.NCUIFriendRequestConfirmationText = "Du er tilsluttet nu";
        this.NCUISectionHeader = "Beskeder";
        this.NCUIPlaceholderTitle = "Helt opdateret";
        this.NCUIPlaceholderText = "Ny beskeder vises her";
        this.NCUIDeleteAllButton = "Slet alle beskeder";
        this.NCUIMarkAllAsReadButton = "Marker alle beskeder som læst";
        this.NCUIMarkAsReadButton = "Marker besked som læst";
        this.NCUIMarkAsUnreadButton = "Marker besked som ulæst";
        this.NCUIDeleteButton = "Slet besked";
        this.NCUIFriendRequestDeleteButton = "Slet";
    }
}
