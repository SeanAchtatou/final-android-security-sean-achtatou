package im.getsocial.sdk.sharedl10n.generated;

public class vi extends LanguageStrings {
    public vi() {
        this.OkButton = "đuợc";
        this.CancelButton = "Huỷ";
        this.ConnectionLostTitle = "Mất Kết nối";
        this.ConnectionLostMessage = "Ố ô! Có vẻ kết nối internet của bạn bị mất. Vui lòng kết nối lại.";
        this.PullDownToRefresh = "Kéo xuống để làm mới";
        this.PullUpToLoadMore = "Kéo lên để tải thêm";
        this.ReleaseToRefresh = "Thả để làm mới";
        this.ReleaseToLoadMore = "Thả để tải thêm";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Đã xảy ra lỗi. Vui lòng thử lại!";
        this.InviteFriends = "Mời bạn bè";
        this.TimestampJustNow = "luôn & ngay";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Hôm qua";
        this.ActivityTitle = "Hoạt động";
        this.ActivityPostPlaceholder = "Có chuyện gì thế?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Không có hoạt động";
        this.ActivityNoSearchResultsPlaceholderTitle = "Không tìm thấy kết quả cho **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Chưa có hoạt động nào tại đây. Sao bạn không bắt đầu một hoạt động nào đó?";
        this.ViewCommentsLink = "bình luận";
        this.ViewComments2Link = "bình luận";
        this.ViewCommentLink = "bình luận";
        this.CommentsTitle = "bình luận";
        this.CommentsPostPlaceholder = "Để lại bình luận";
        this.CommentsMoreCommentsButton = "Xem bình luận cũ";
        this.ViewLikesLink = "lượt thích";
        this.ViewLikes2Link = "lượt thích";
        this.ViewLikeLink = "như";
        this.ReportAsSpam = "Báo cáo thư rác";
        this.ReportAsInappropriate = "Báo cáo nội dung không phù hợp";
        this.ReportNotificationTitle = "Cảm ơn bạn!";
        this.ReportNotificationText = "Một điều hành viên của chúng tôi sẽ xem xét nội dung này trong thời gian sớm nhất có thể";
        this.DeleteActivity = "Xóa hoạt động";
        this.DeleteComment = "Xóa bình luận";
        this.ActivityNotFound = "Hành động này không còn sẵn có nữa";
        this.ErrorYoureBanned = "Quyền truy cập của bạn đến một số tính năng đã tạm thời bị hạn chế";
        this.NotificationsChannelName = "Xã hội";
        this.NCUITitle = "Tất cả thông báo";
        this.NCUIFriendRequestConfirmButton = "Xác nhận";
        this.NCUIFriendRequestConfirmationText = "Bạn đã kết nối";
        this.NCUISectionHeader = "Thông báo";
        this.NCUIPlaceholderTitle = "Tất cả thông báo đã được đọc";
        this.NCUIPlaceholderText = "Thông báo mới sẽ xuất hiện ở đây";
        this.NCUIDeleteAllButton = "Xóa tất cả thông báo";
        this.NCUIMarkAllAsReadButton = "Đánh dấu tất cả thông báo là đã đọc";
        this.NCUIMarkAsReadButton = "Đánh dấu thông báo là đã đọc";
        this.NCUIMarkAsUnreadButton = "Đánh dấu thông báo là chưa đọc";
        this.NCUIDeleteButton = "Xóa thông báo";
        this.NCUIFriendRequestDeleteButton = "Xóa";
    }
}
