package im.getsocial.sdk.sharedl10n.generated;

public class pl extends LanguageStrings {
    public pl() {
        this.OkButton = "OK";
        this.CancelButton = "Anuluj";
        this.ConnectionLostTitle = "Brak połączenia";
        this.ConnectionLostMessage = "Oops! Brak połączenia z Internetem. Sprobuj połączyć ponownie.";
        this.PullDownToRefresh = "Ciągnij dla aktualizacji";
        this.PullUpToLoadMore = "Ciągnij do góry, żeby załadować więcej";
        this.ReleaseToRefresh = "Zwolnij dla aktualizacji";
        this.ReleaseToLoadMore = "Zwolnij, żeby załadować więcej";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Coś się stało. Spróbuj ponownie!";
        this.InviteFriends = "Zaprosić znajomych";
        this.TimestampJustNow = "przed chwilą";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fmin";
        this.TimestampHours = "%1.0fgodz.";
        this.TimestampDays = "%1.0fdn.";
        this.TimestampWeeks = "%1.0ftydz.";
        this.TimestampYesterday = "Wczoraj";
        this.ActivityTitle = "Aktywności";
        this.ActivityPostPlaceholder = "Co słychać?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Brak aktywności";
        this.ActivityNoSearchResultsPlaceholderTitle = "Brak wyników dla **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Tutaj niema jeszcze żadnej aktywności. Opublikuj coś!";
        this.ViewCommentsLink = "komentarze";
        this.ViewComments2Link = "komentarze";
        this.ViewCommentLink = "komentarz";
        this.CommentsTitle = "Komentarze";
        this.CommentsPostPlaceholder = "Napisz komentarz";
        this.CommentsMoreCommentsButton = "Zobacz stare komentarze";
        this.ViewLikesLink = "lubią to";
        this.ViewLikes2Link = "lubią to";
        this.ViewLikeLink = "lubię to";
        this.ReportAsSpam = "Zgłoś jako spam";
        this.ReportAsInappropriate = "Zgłoś jako nieodpowiednie";
        this.ReportNotificationTitle = "Dziękuję!";
        this.ReportNotificationText = "Nasz moderator przeanalizuje zawrtość jak najszybciej.";
        this.DeleteActivity = "Usunąć aktywność";
        this.DeleteComment = "Usunąć komentarz";
        this.ActivityNotFound = "Aktywność już niedostępna";
        this.ErrorYoureBanned = "Niektórze funkcji zostały tymczasowo ograniczone dla ciebie";
        this.NotificationsChannelName = "Społecznościowe";
        this.NCUITitle = "Wszystkie powiadomienia";
        this.NCUIFriendRequestConfirmButton = "Potwierdź";
        this.NCUIFriendRequestConfirmationText = "Zostałeś/aś połączony/a";
        this.NCUISectionHeader = "Powiadomienia";
        this.NCUIPlaceholderTitle = "Przeczytano wszystko";
        this.NCUIPlaceholderText = "Nowe powiadomienia pojawią się tutaj";
        this.NCUIDeleteAllButton = "Skasuj wszystkie powiadomienia";
        this.NCUIMarkAllAsReadButton = "Oznacz wszystkie powiadomienia jako przeczytane";
        this.NCUIMarkAsReadButton = "Oznacz powiadomienie jako przeczytane";
        this.NCUIMarkAsUnreadButton = "Oznacz powiadomienie jako nieprzeczytane";
        this.NCUIDeleteButton = "Skasuj powiadomienie";
        this.NCUIFriendRequestDeleteButton = "Skasuj";
    }
}
