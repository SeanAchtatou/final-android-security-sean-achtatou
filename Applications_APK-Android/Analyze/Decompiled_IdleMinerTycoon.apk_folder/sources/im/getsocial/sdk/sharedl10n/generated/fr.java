package im.getsocial.sdk.sharedl10n.generated;

public class fr extends LanguageStrings {
    public fr() {
        this.OkButton = "OK";
        this.CancelButton = "Annuler";
        this.ConnectionLostTitle = "Connexion perdue";
        this.ConnectionLostMessage = "Oh là... On dirait que votre connexion Internet été perdue. Veuillez vous reconnecter.";
        this.PullDownToRefresh = "Tirer vers le bas pour actualiser";
        this.PullUpToLoadMore = "Tirer vers le haut pour charger plus";
        this.ReleaseToRefresh = "Lâcher pour actualiser";
        this.ReleaseToLoadMore = "Lâcher pour charger plus";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Quelque chose n'a pas bien fonctionné. Veuillez réessayer.";
        this.InviteFriends = "Inviter des amis";
        this.TimestampJustNow = "en ce moment";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Hier";
        this.ActivityTitle = "Activité";
        this.ActivityPostPlaceholder = "Quoi de neuf?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Aucune activité";
        this.ActivityNoSearchResultsPlaceholderTitle = "Aucun résultat pour **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Il n'y a encore aucune activité. Pourquoi ne commençez-vous pas quelque chose ?";
        this.ViewCommentsLink = "commentaires";
        this.ViewComments2Link = "commentaires";
        this.ViewCommentLink = "commentaire";
        this.CommentsTitle = "Commentaires";
        this.CommentsPostPlaceholder = "Laisser un commentaire";
        this.CommentsMoreCommentsButton = "Afficher anciens commentaires";
        this.ViewLikesLink = "J’aime";
        this.ViewLikes2Link = "J’aime";
        this.ViewLikeLink = "J’aime";
        this.ReportAsSpam = "Signaler comme indésirable";
        this.ReportAsInappropriate = "Signaler comme inapproprié";
        this.ReportNotificationTitle = "Merci !";
        this.ReportNotificationText = "Un de nos modérateurs examinera le contenu dès que possible";
        this.DeleteActivity = "Supprimer l'activité";
        this.DeleteComment = "Supprimer commentaire";
        this.ActivityNotFound = "Activité est plus disponible";
        this.ErrorYoureBanned = "Votre accès à certaines fonctionnalités a été temporairement limité";
        this.NotificationsChannelName = "Sociales";
        this.NCUITitle = "Toutes les notifications";
        this.NCUIFriendRequestConfirmButton = "Confirmer";
        this.NCUIFriendRequestConfirmationText = "Vous êtes maintenant connecté";
        this.NCUISectionHeader = "Notifications";
        this.NCUIPlaceholderTitle = "Toutes lues";
        this.NCUIPlaceholderText = "Les nouvelles notifications apparaîtront ici";
        this.NCUIDeleteAllButton = "Supprimer toutes les notifications";
        this.NCUIMarkAllAsReadButton = "Marquer toutes les notifications comme lues";
        this.NCUIMarkAsReadButton = "Marquer la notification comme lue";
        this.NCUIMarkAsUnreadButton = "Marquer la notification comme non lue";
        this.NCUIDeleteButton = "Supprimer la notification";
        this.NCUIFriendRequestDeleteButton = "Supprimer";
    }
}
