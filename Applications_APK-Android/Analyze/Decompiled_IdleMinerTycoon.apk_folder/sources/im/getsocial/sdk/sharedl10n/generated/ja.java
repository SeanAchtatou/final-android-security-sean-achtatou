package im.getsocial.sdk.sharedl10n.generated;

public class ja extends LanguageStrings {
    public ja() {
        this.OkButton = "おけ";
        this.CancelButton = "キャンセル";
        this.ConnectionLostTitle = "接続が切れました";
        this.ConnectionLostMessage = "あっ! インターネット接続が切れたようです。再接続してください。";
        this.PullDownToRefresh = "プルダウンで更新";
        this.PullUpToLoadMore = "プルアップでさらに読み込み";
        this.ReleaseToRefresh = "リリースで更新";
        this.ReleaseToLoadMore = "リリースでさらに読み込み";
        this.ErrorAlertMessageTitle = "おっと";
        this.ErrorAlertMessage = "問題が発生しました。再試行してください!";
        this.InviteFriends = "友達を招待";
        this.TimestampJustNow = "たった今";
        this.TimestampSeconds = "%1.0f 秒";
        this.TimestampMinutes = "%1.0f 分";
        this.TimestampHours = "%1.0f 時間";
        this.TimestampDays = "%1.0f 日";
        this.TimestampWeeks = "%1.0f 週";
        this.TimestampYesterday = "昨日";
        this.ActivityTitle = "アクティビティ";
        this.ActivityPostPlaceholder = "何してる?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "アクティビティなし";
        this.ActivityNoSearchResultsPlaceholderTitle = "**[SEARCH_TERM]**の結果が得られませんでした";
        this.ActivityAllNoActivitiesPlaceholderMessage = "ここではまだアクティビティがありません。何か始めませんか?";
        this.ViewCommentsLink = "コメント";
        this.ViewComments2Link = "コメント";
        this.ViewCommentLink = "コメント";
        this.CommentsTitle = "コメント";
        this.CommentsPostPlaceholder = "コメントを残す";
        this.CommentsMoreCommentsButton = "古いコメントを見る";
        this.ViewLikesLink = "いいね";
        this.ViewLikes2Link = "いいね";
        this.ViewLikeLink = "いいね";
        this.ReportAsSpam = "スパムとして報告";
        this.ReportAsInappropriate = "不適切な内容として報告";
        this.ReportNotificationTitle = "ありがとうございます！";
        this.ReportNotificationText = "弊社のモデレーターができるだけ早く内容を確認します";
        this.DeleteActivity = "アクティビティを削除";
        this.DeleteComment = "コメントの削除";
        this.ActivityNotFound = "このアクティビティは現在利用できません";
        this.ErrorYoureBanned = "いくつかの機能へのアクセスが一時的に制限されています";
        this.NotificationsChannelName = "ソーシャル";
        this.NCUITitle = "すべての通知";
        this.NCUIFriendRequestConfirmButton = "確認";
        this.NCUIFriendRequestConfirmationText = "接続されました";
        this.NCUISectionHeader = "通知";
        this.NCUIPlaceholderTitle = "すべて読み終わりました";
        this.NCUIPlaceholderText = "ここに新しい通知が表示されます";
        this.NCUIDeleteAllButton = "すべての通知を削除";
        this.NCUIMarkAllAsReadButton = "すべての通知を既読にする";
        this.NCUIMarkAsReadButton = "通知を既読にする";
        this.NCUIMarkAsUnreadButton = "通知を未読にする";
        this.NCUIDeleteButton = "通知を削除";
        this.NCUIFriendRequestDeleteButton = "削除";
    }
}
