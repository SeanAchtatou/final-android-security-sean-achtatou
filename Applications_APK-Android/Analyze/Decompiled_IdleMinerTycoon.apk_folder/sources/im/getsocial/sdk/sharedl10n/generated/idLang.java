package im.getsocial.sdk.sharedl10n.generated;

public class idLang extends LanguageStrings {
    public idLang() {
        this.OkButton = "Oke";
        this.CancelButton = "Batal";
        this.ConnectionLostTitle = "Koneksi Hilang";
        this.ConnectionLostMessage = "Ups! Tampaknya koneksi internet Anda hilang. Silakan hubungkan kembali.";
        this.PullDownToRefresh = "Tarik ke bawah untuk menyegarkan";
        this.PullUpToLoadMore = "Tarik ke atas untuk memuat lebih banyak";
        this.ReleaseToRefresh = "Lepaskan untuk menyegarkan";
        this.ReleaseToLoadMore = "Lepaskan untuk memuat lebih banyak";
        this.ErrorAlertMessageTitle = "Ups!";
        this.ErrorAlertMessage = "Terjadi kesalahan. Silakan coba lagi!";
        this.InviteFriends = "Undang teman";
        this.TimestampJustNow = "Baru saja";
        this.TimestampSeconds = "%1.0fd";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fj";
        this.TimestampDays = "%1.0fdhr";
        this.TimestampWeeks = "%1.0fmg";
        this.TimestampYesterday = "Kemarin";
        this.ActivityTitle = "Aktivitas";
        this.ActivityPostPlaceholder = "Apa kabar?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Tidak ada aktivitas";
        this.ActivityNoSearchResultsPlaceholderTitle = "Tidak ada hasil untuk **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Belum ada aktivitas di sini. Ayo mulai sesuatu!";
        this.ViewCommentsLink = "komentar";
        this.ViewComments2Link = "komentar";
        this.ViewCommentLink = "komentar";
        this.CommentsTitle = "Komentar";
        this.CommentsPostPlaceholder = "Tuliskan komentar";
        this.CommentsMoreCommentsButton = "Lihat komentar lama";
        this.ViewLikesLink = "suka";
        this.ViewLikes2Link = "suka";
        this.ViewLikeLink = "Suka";
        this.ReportAsSpam = "Laporkan sebagai spam.";
        this.ReportAsInappropriate = "Laporkan sebagai konten tidak layak.";
        this.ReportNotificationTitle = "Terima kasih!";
        this.ReportNotificationText = "Salah satu moderator kami akan meninjau konten sesegera mungkin.";
        this.DeleteActivity = "Hapus aktivitas.";
        this.DeleteComment = "Hapus komentar.";
        this.ActivityNotFound = "Aktivitas tidak lagi tersedia";
        this.ErrorYoureBanned = "Akses Anda ke beberapa fitur telah dibatasi untuk sementara";
        this.NotificationsChannelName = "Sosial";
        this.NCUITitle = "Semua pemberitahuan";
        this.NCUIFriendRequestConfirmButton = "Konfirmasi";
        this.NCUIFriendRequestConfirmationText = "Anda kini terhubung";
        this.NCUISectionHeader = "Pemberitahuan";
        this.NCUIPlaceholderTitle = "Semua sudah dibaca";
        this.NCUIPlaceholderText = "Pemberitahuan baru akan tampil di sini";
        this.NCUIDeleteAllButton = "Hapus semua pemberitahuan";
        this.NCUIMarkAllAsReadButton = "Tandai semua pemberitahuan sebagai dibaca";
        this.NCUIMarkAsReadButton = "Tandai pemberitahuan sebagai dibaca";
        this.NCUIMarkAsUnreadButton = "Tandai pemberitahuan sebagai belum dibaca";
        this.NCUIDeleteButton = "Hapus pemberitahuan";
        this.NCUIFriendRequestDeleteButton = "Hapus";
    }
}
