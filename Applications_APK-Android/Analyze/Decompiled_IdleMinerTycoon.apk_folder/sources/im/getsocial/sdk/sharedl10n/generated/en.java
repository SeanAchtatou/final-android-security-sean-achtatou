package im.getsocial.sdk.sharedl10n.generated;

import im.getsocial.sdk.pushnotifications.Notification;

public class en extends LanguageStrings {
    public en() {
        this.OkButton = "OK";
        this.CancelButton = "Cancel";
        this.ConnectionLostTitle = "Connection Lost";
        this.ConnectionLostMessage = "Uh oh! Looks like your internet connection is lost. Please reconnect.";
        this.PullDownToRefresh = "Pull down to refresh";
        this.PullUpToLoadMore = "Pull up to load more";
        this.ReleaseToRefresh = "Release to refresh";
        this.ReleaseToLoadMore = "Release to load more";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Something went wrong. Please try again!";
        this.InviteFriends = "Invite friends";
        this.TimestampJustNow = "just now";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Yesterday";
        this.ActivityTitle = "Activity";
        this.ActivityPostPlaceholder = "What's up?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "No activity";
        this.ActivityNoSearchResultsPlaceholderTitle = "No results for **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "There is no activity here yet. Why don't you start something?";
        this.ViewCommentsLink = "comments";
        this.ViewComments2Link = "comments";
        this.ViewCommentLink = Notification.NotificationType.COMMENT;
        this.CommentsTitle = "Comments";
        this.CommentsPostPlaceholder = "Leave a comment";
        this.CommentsMoreCommentsButton = "See older comments";
        this.ViewLikesLink = "likes";
        this.ViewLikes2Link = "likes";
        this.ViewLikeLink = "like";
        this.ReportAsSpam = "Report as spam";
        this.ReportAsInappropriate = "Report as inappropriate";
        this.ReportNotificationTitle = "Thank you!";
        this.ReportNotificationText = "One of our moderators will review the content as soon as possible";
        this.DeleteActivity = "Delete activity";
        this.DeleteComment = "Delete comment";
        this.ActivityNotFound = "Activity is no longer available";
        this.ErrorYoureBanned = "Your access to some features has been temporarily limited";
        this.NotificationsChannelName = "Social";
        this.NCUITitle = "All notifications";
        this.NCUIFriendRequestConfirmButton = "Confirm";
        this.NCUIFriendRequestConfirmationText = "You are now connected";
        this.NCUISectionHeader = "Notifications";
        this.NCUIPlaceholderTitle = "All caught up";
        this.NCUIPlaceholderText = "New notifications will appear here";
        this.NCUIDeleteAllButton = "Delete all notifications";
        this.NCUIMarkAllAsReadButton = "Mark all notifications as read";
        this.NCUIMarkAsReadButton = "Mark notification as read";
        this.NCUIMarkAsUnreadButton = "Mark notification as unread";
        this.NCUIDeleteButton = "Delete notification";
        this.NCUIFriendRequestDeleteButton = "Delete";
    }
}
