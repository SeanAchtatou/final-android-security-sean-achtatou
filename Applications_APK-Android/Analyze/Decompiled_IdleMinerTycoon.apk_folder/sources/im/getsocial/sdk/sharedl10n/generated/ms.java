package im.getsocial.sdk.sharedl10n.generated;

public class ms extends LanguageStrings {
    public ms() {
        this.OkButton = "Okey";
        this.CancelButton = "Batal";
        this.ConnectionLostTitle = "Sambungan Terputus";
        this.ConnectionLostMessage = "Uh Oh! Nampaknya sambungan internet anda terputus. Sila sambung semula.";
        this.PullDownToRefresh = "Tarik ke bawah untuk memuat semula";
        this.PullUpToLoadMore = "Tarik ke atas untuk memuat lebih banyak";
        this.ReleaseToRefresh = "Lepaskan untuk memuat semula";
        this.ReleaseToLoadMore = "Lepaskan untuk memuat lebih banyak";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Ada sesuatu yang tidak kena. Sila cuba lagi!";
        this.InviteFriends = "Jemput kawan";
        this.TimestampJustNow = "Baru sahaja";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Semalam";
        this.ActivityTitle = "Aktiviti";
        this.ActivityPostPlaceholder = "Apa yang menarik?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Tiada aktiviti";
        this.ActivityNoSearchResultsPlaceholderTitle = "Tiada keputusan untuk **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Masih tiada aktiviti. Apa kata anda mulakan sesuatu?";
        this.ViewCommentsLink = "komen";
        this.ViewComments2Link = "komen";
        this.ViewCommentLink = "komen";
        this.CommentsTitle = "Komen";
        this.CommentsPostPlaceholder = "Tinggalkan komen";
        this.CommentsMoreCommentsButton = "Lihat komen lama";
        this.ViewLikesLink = "suka";
        this.ViewLikes2Link = "suka";
        this.ViewLikeLink = "suka";
        this.ReportAsSpam = "Laporkan sebagai spam.";
        this.ReportAsInappropriate = "Laporkan sebagai tidak senonoh.";
        this.ReportNotificationTitle = "Terima kasih!";
        this.ReportNotificationText = "Salah seorang moderator kami akan mengkaji semula kandungan secepat mungkin";
        this.DeleteActivity = "Padamkan aktiviti.";
        this.DeleteComment = "Padamkan komen.";
        this.ActivityNotFound = "Aktiviti tidak lagi boleh didapati";
        this.ErrorYoureBanned = "Akses anda ke sesetengah ciri telah dihadkan buat sementara waktu";
        this.NotificationsChannelName = "Sosial";
        this.NCUITitle = "Semua pemberitahuan";
        this.NCUIFriendRequestConfirmButton = "Sahkan";
        this.NCUIFriendRequestConfirmationText = "Anda kini bersambung";
        this.NCUISectionHeader = "Pemberitahuan";
        this.NCUIPlaceholderTitle = "Sudah dikemas kini";
        this.NCUIPlaceholderText = "Pemberitahuan baharu akan muncul di sini";
        this.NCUIDeleteAllButton = "Padamkan semua pemberitahuan";
        this.NCUIMarkAllAsReadButton = "Tandakan semua pemberitahuan sebagai dibaca";
        this.NCUIMarkAsReadButton = "Tandakan pemberitahuan sebagai dibaca";
        this.NCUIMarkAsUnreadButton = "Tandakan pemberitahuan sebagai belum dibaca";
        this.NCUIDeleteButton = "Padamkan pemberitahuan";
        this.NCUIFriendRequestDeleteButton = "Padam";
    }
}
