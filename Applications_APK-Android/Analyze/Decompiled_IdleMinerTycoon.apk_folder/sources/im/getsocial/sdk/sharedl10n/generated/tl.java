package im.getsocial.sdk.sharedl10n.generated;

public class tl extends LanguageStrings {
    public tl() {
        this.OkButton = "OK";
        this.CancelButton = "Kanselahin";
        this.ConnectionLostTitle = "Nawala ang Koneksiyon";
        this.ConnectionLostMessage = "Ah-ah! Parang nawala ang koneksiyon ng internet mo. Pakikonekta ulit.";
        this.PullDownToRefresh = "Hatakin pababa para ma-refresh";
        this.PullUpToLoadMore = "Hatakin pataas para mag-load pa";
        this.ReleaseToRefresh = "Bitawan para ma-refresh";
        this.ReleaseToLoadMore = "Bitawan para mag-load pa";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "May nagkamali. Pakisubukan ulit!";
        this.InviteFriends = "Mag-imbita ng mga kaibigan";
        this.TimestampJustNow = "ngayon lang";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Kahapon";
        this.ActivityTitle = "Aktibidad";
        this.ActivityPostPlaceholder = "Anong meron?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Walang aktibidad";
        this.ActivityNoSearchResultsPlaceholderTitle = "Walang resulta para sa **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Wala pang aktibidad dito ngayon. Magsimula ka kaya?";
        this.ViewCommentsLink = "mga komento";
        this.ViewComments2Link = "mga komento";
        this.ViewCommentLink = "komento";
        this.CommentsTitle = "mga komento";
        this.CommentsPostPlaceholder = "Mag-iwan ng komento";
        this.CommentsMoreCommentsButton = "Tingnan ang lumang mga komento";
        this.ViewLikesLink = "likes";
        this.ViewLikes2Link = "likes";
        this.ViewLikeLink = "like";
        this.ReportAsSpam = "Iulat bilang spam";
        this.ReportAsInappropriate = "Iulat bilang hindi nararapat";
        this.ReportNotificationTitle = "Salamat!";
        this.ReportNotificationText = "Rerepasuhin ng isa sa aming mga tagapamagitan ang nilalaman sa lalong madaling panahon";
        this.DeleteActivity = "Burahin ang aktibidad";
        this.DeleteComment = "Burahin ang komento";
        this.ActivityNotFound = "Hindi na available ang aktibidad";
        this.ErrorYoureBanned = "Ang access mo sa ilan sa mga feature ay pansamantala limitado na.";
        this.NotificationsChannelName = "Panlipunan";
        this.NCUITitle = "Lahat ng mga notipikasyon";
        this.NCUIFriendRequestConfirmButton = "Kumpirmahin";
        this.NCUIFriendRequestConfirmationText = "Konektado ka na ngayon";
        this.NCUISectionHeader = "Mga Notipikasyon";
        this.NCUIPlaceholderTitle = "Nakuha na ang lahat";
        this.NCUIPlaceholderText = "Lalabas dito ang lahat ng mga bagong notipikasyon";
        this.NCUIDeleteAllButton = "Tanggalin ang lahat ng mga notipikasyon";
        this.NCUIMarkAllAsReadButton = "Markahan na nabasa na ang lahat ng mga notipikasyon";
        this.NCUIMarkAsReadButton = "Markahan na nabasa na ang notipikasyon";
        this.NCUIMarkAsUnreadButton = "Markahan na hindi pa nabasa ang notipikasyon";
        this.NCUIDeleteButton = "Tanggalin ang notipikasyon";
        this.NCUIFriendRequestDeleteButton = "Tanggalin";
    }
}
