package im.getsocial.sdk.sharedl10n.generated;

public class sv extends LanguageStrings {
    public sv() {
        this.OkButton = "Ok";
        this.CancelButton = "Avbryt";
        this.ConnectionLostTitle = "Anslutning förlorad";
        this.ConnectionLostMessage = "Uh oh! Det verkar som att internetanslutningen förlorades. Var vänlig anslut igen.";
        this.PullDownToRefresh = "Dra ner för att uppdatera";
        this.PullUpToLoadMore = "Dra upp för att läsa in fler";
        this.ReleaseToRefresh = "Släpp för att uppdatera";
        this.ReleaseToLoadMore = "Släpp för att läsa in fler";
        this.ErrorAlertMessageTitle = "Oj då!";
        this.ErrorAlertMessage = "Något gick fel. Försök igen!";
        this.InviteFriends = "Bjud in vänner";
        this.TimestampJustNow = "just nu";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0ft";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fv";
        this.TimestampYesterday = "Igår";
        this.ActivityTitle = "Aktivitet";
        this.ActivityPostPlaceholder = "Vad händer?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Ingen aktivitet";
        this.ActivityNoSearchResultsPlaceholderTitle = "Inga resultat för **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Det finns ingen aktivitet här ännu. Du kanske kan starta något?";
        this.ViewCommentsLink = "kommentarer";
        this.ViewComments2Link = "kommentarer";
        this.ViewCommentLink = "kommentar";
        this.CommentsTitle = "Kommentarer";
        this.CommentsPostPlaceholder = "Lämna en kommentar";
        this.CommentsMoreCommentsButton = "Se äldre kommentarer";
        this.ViewLikesLink = "gillar";
        this.ViewLikes2Link = "gillar";
        this.ViewLikeLink = "gilla";
        this.ReportAsSpam = "Rapportera som spam";
        this.ReportAsInappropriate = "Rapportera som olämpligt";
        this.ReportNotificationTitle = "Tack!";
        this.ReportNotificationText = "En av våra moderatorer kommer att granska innehållet så snart som möjligt";
        this.DeleteActivity = "Radera aktivitet";
        this.DeleteComment = "Radera kommentar";
        this.ActivityNotFound = "Aktiviteten är inte längre tillgänglig";
        this.ErrorYoureBanned = "Din åtkomst till vissa funktioner har tillfälligt begränsats";
        this.NotificationsChannelName = "Socialt";
        this.NCUITitle = "Alla notiser";
        this.NCUIFriendRequestConfirmButton = "Bekräfta";
        this.NCUIFriendRequestConfirmationText = "Du är nu ansluten";
        this.NCUISectionHeader = "Notiser";
        this.NCUIPlaceholderTitle = "Alla lästa";
        this.NCUIPlaceholderText = "Nya notiser visas här";
        this.NCUIDeleteAllButton = "Ta bort alla notiser";
        this.NCUIMarkAllAsReadButton = "Markera alla notiser som lästa";
        this.NCUIMarkAsReadButton = "Markera notis som läst";
        this.NCUIMarkAsUnreadButton = "Markera notiser som olästa";
        this.NCUIDeleteButton = "Radera notis";
        this.NCUIFriendRequestDeleteButton = "Radera";
    }
}
