package im.getsocial.sdk.sharedl10n.generated;

public class ko extends LanguageStrings {
    public ko() {
        this.OkButton = "알았어";
        this.CancelButton = "취소";
        this.ConnectionLostTitle = "연결 손실";
        this.ConnectionLostMessage = "어머! 인터넷 연결이 끊어진 것 같습니다. 다시 연결하십시오.";
        this.PullDownToRefresh = "아래로 당겨 새로 고침";
        this.PullUpToLoadMore = "위로 당겨 기타 로드";
        this.ReleaseToRefresh = "선택 해제하여 새로 고침";
        this.ReleaseToLoadMore = "선택 해제하여 기타 로드";
        this.ErrorAlertMessageTitle = "헉";
        this.ErrorAlertMessage = "문제가 발생했습니다. 다시 시도하십시오!";
        this.InviteFriends = "친구 초대";
        this.TimestampJustNow = "지금 막";
        this.TimestampSeconds = "%1.0f초";
        this.TimestampMinutes = "%1.0f분";
        this.TimestampHours = "%1.0f시 ";
        this.TimestampDays = "%1.0f일";
        this.TimestampWeeks = "%1.0f주";
        this.TimestampYesterday = "어제";
        this.ActivityTitle = "활동";
        this.ActivityPostPlaceholder = "무슨 일이야?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "활동이 없습니다";
        this.ActivityNoSearchResultsPlaceholderTitle = "**[SEARCH_TERM]**에 대한 검색 결과가 없음";
        this.ActivityAllNoActivitiesPlaceholderMessage = "여기에 활동 사항이 아직 없습니다. 무언가 시작하는 게 어떻습니까?";
        this.ViewCommentsLink = "댓글";
        this.ViewComments2Link = "댓글";
        this.ViewCommentLink = "댓글";
        this.CommentsTitle = "댓글";
        this.CommentsPostPlaceholder = "댓글 남기기";
        this.CommentsMoreCommentsButton = "이전 댓글 보기";
        this.ViewLikesLink = "좋아요";
        this.ViewLikes2Link = "좋아요";
        this.ViewLikeLink = "좋아요";
        this.ReportAsSpam = "스팸으로 신고";
        this.ReportAsInappropriate = "부적정으로 신고";
        this.ReportNotificationTitle = "감사합니다!";
        this.ReportNotificationText = "저희 관리자가 가능한 한 빠르게 내용을 확인 하겠습니다.";
        this.DeleteActivity = "활동 삭제";
        this.DeleteComment = "댓글 삭제";
        this.ActivityNotFound = "활동은 더 이상 이용 가능하지 않습니다";
        this.ErrorYoureBanned = "일부 기능의 사용이 일시적으로 제한됩니다";
        this.NotificationsChannelName = "소셜";
        this.NCUITitle = "모든 알림";
        this.NCUIFriendRequestConfirmButton = "확인";
        this.NCUIFriendRequestConfirmationText = "이제 연결되었습니다.";
        this.NCUISectionHeader = "알림";
        this.NCUIPlaceholderTitle = "모두 잡았다";
        this.NCUIPlaceholderText = "새 알림이 여기에 표시됩니다.";
        this.NCUIDeleteAllButton = "모든 알림 삭제";
        this.NCUIMarkAllAsReadButton = "모든 알림을 읽음으로 표시";
        this.NCUIMarkAsReadButton = "알림을 읽음으로 표시";
        this.NCUIMarkAsUnreadButton = "알림을 읽지 않은 상태로 표시";
        this.NCUIDeleteButton = "알림 삭제";
        this.NCUIFriendRequestDeleteButton = "지우다";
    }
}
