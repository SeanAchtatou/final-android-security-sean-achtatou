package im.getsocial.sdk.sharedl10n;

import im.getsocial.sdk.consts.LanguageCodes;
import im.getsocial.sdk.sharedl10n.generated.LanguageStrings;
import java.util.HashSet;
import java.util.Set;

public final class LocalizationAdapter {
    private static final Set<String> getsocial;

    static {
        HashSet hashSet = new HashSet();
        getsocial = hashSet;
        hashSet.add(LanguageCodes.UKRAINIAN);
        getsocial.add(LanguageCodes.RUSSIAN);
    }

    private LocalizationAdapter() {
    }

    public static String comments(Localization localization, int i) {
        LanguageStrings strings = localization.strings();
        return getsocial(new jjbQypPegg(strings.ViewCommentLink, strings.ViewCommentsLink, strings.ViewComments2Link), i, getsocial(localization.getsocial()));
    }

    public static String likes(Localization localization, int i) {
        LanguageStrings strings = localization.strings();
        return getsocial(new jjbQypPegg(strings.ViewLikeLink, strings.ViewLikesLink, strings.ViewLikes2Link), i, getsocial(localization.getsocial()));
    }

    public static String noResults(Localization localization, String str) {
        return localization.strings().ActivityNoSearchResultsPlaceholderTitle.replace("**[SEARCH_TERM]**", str);
    }

    private static boolean getsocial(String str) {
        return getsocial.contains(str);
    }

    private static String getsocial(jjbQypPegg jjbqyppegg, int i, boolean z) {
        if (!z) {
            return i == 1 ? jjbqyppegg.getsocial : jjbqyppegg.attribution;
        }
        if ((i / 10) % 10 == 1) {
            return jjbqyppegg.acquisition;
        }
        int i2 = i % 10;
        if (i2 >= 5) {
            return jjbqyppegg.acquisition;
        }
        if (i2 == 0) {
            return jjbqyppegg.acquisition;
        }
        if (i2 == 1) {
            return jjbqyppegg.getsocial;
        }
        return jjbqyppegg.attribution;
    }

    static class jjbQypPegg {
        final String acquisition;
        final String attribution;
        final String getsocial;

        jjbQypPegg(String str, String str2, String str3) {
            this.getsocial = str;
            this.attribution = str2;
            this.acquisition = str3;
        }
    }
}
