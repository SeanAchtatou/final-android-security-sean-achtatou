package im.getsocial.sdk.sharedl10n.generated;

public class zhHant extends LanguageStrings {
    public zhHant() {
        this.OkButton = "還行";
        this.CancelButton = "取消";
        this.ConnectionLostTitle = "連接丟失";
        this.ConnectionLostMessage = "啊哦！似乎您的網絡連接已丟失。請重新連接。";
        this.PullDownToRefresh = "下拉來刷新";
        this.PullUpToLoadMore = "上拉來載入更多";
        this.ReleaseToRefresh = "釋放來刷新";
        this.ReleaseToLoadMore = "釋放來載入更多";
        this.ErrorAlertMessageTitle = "哎呀";
        this.ErrorAlertMessage = "出現了問題。請重試！";
        this.InviteFriends = "邀請朋友";
        this.TimestampJustNow = "剛剛";
        this.TimestampSeconds = "%1.0f 秒";
        this.TimestampMinutes = "%1.0f分";
        this.TimestampHours = "%1.0f小時";
        this.TimestampDays = "%1.0f天 ";
        this.TimestampWeeks = "%1.0f週";
        this.TimestampYesterday = "昨天";
        this.ActivityTitle = "活動";
        this.ActivityPostPlaceholder = "近來怎樣";
        this.ActivityAllNoActivitiesPlaceholderTitle = "無活動";
        this.ActivityNoSearchResultsPlaceholderTitle = "沒有關於**[SEARCH_TERM]**的結果";
        this.ActivityAllNoActivitiesPlaceholderMessage = "這里尚無活動。你為什麽不開始做些什麽呢？";
        this.ViewCommentsLink = "評論";
        this.ViewComments2Link = "評論";
        this.ViewCommentLink = "評論";
        this.CommentsTitle = "評論";
        this.CommentsPostPlaceholder = "留下評論";
        this.CommentsMoreCommentsButton = "查看更早的評論";
        this.ViewLikesLink = "贊";
        this.ViewLikes2Link = "贊";
        this.ViewLikeLink = "贊";
        this.ReportAsSpam = "報告為垃圾郵件";
        this.ReportAsInappropriate = "報告為不合適";
        this.ReportNotificationTitle = "謝謝！";
        this.ReportNotificationText = "我們的一位審核人員將會儘快審核內容";
        this.DeleteActivity = "刪除活動";
        this.DeleteComment = "刪除評論";
        this.ActivityNotFound = "活動不再有效";
        this.ErrorYoureBanned = "您對一些功能的訪問已暫時受限";
        this.NotificationsChannelName = "社交";
        this.NCUITitle = "全部提醒";
        this.NCUIFriendRequestConfirmButton = "確認";
        this.NCUIFriendRequestConfirmationText = "您現在已連接";
        this.NCUISectionHeader = "提醒";
        this.NCUIPlaceholderTitle = "已全讀";
        this.NCUIPlaceholderText = "新提醒將在此處顯示";
        this.NCUIDeleteAllButton = "刪除全部提醒";
        this.NCUIMarkAllAsReadButton = "將全部提醒標記為已讀";
        this.NCUIMarkAsReadButton = "將提醒標記為已讀";
        this.NCUIMarkAsUnreadButton = "將提醒標記為未讀";
        this.NCUIDeleteButton = "刪除提醒";
        this.NCUIFriendRequestDeleteButton = "刪除";
    }
}
