package im.getsocial.sdk.sharedl10n.generated;

public class nl extends LanguageStrings {
    public nl() {
        this.OkButton = "OK";
        this.CancelButton = "Annuleren";
        this.ConnectionLostTitle = "Verbinding verbroken";
        this.ConnectionLostMessage = "O jee! Het lijkt erop dat je internetverbinding verbroken is. Probeer opnieuw verbinding te maken.";
        this.PullDownToRefresh = "Sleep naar beneden om te verversen";
        this.PullUpToLoadMore = "Sleep naar boven om meer te laden";
        this.ReleaseToRefresh = "Laat los om te verversen";
        this.ReleaseToLoadMore = "Laat los om meer te laden";
        this.ErrorAlertMessageTitle = "Oeps!";
        this.ErrorAlertMessage = "Er is iets fout gegaan. Probeer het opnieuw!";
        this.InviteFriends = "Vrienden uitnodigen";
        this.TimestampJustNow = "zojuist";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fu";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Gisteren";
        this.ActivityTitle = "Activiteit";
        this.ActivityPostPlaceholder = "Zeg iets!";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Geen activiteit";
        this.ActivityNoSearchResultsPlaceholderTitle = "Geen resultaten voor **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Er is hier nog geen activiteit. Jij kunt de eerste zijn!";
        this.ViewCommentsLink = "reacties";
        this.ViewComments2Link = "reacties";
        this.ViewCommentLink = "reactie";
        this.CommentsTitle = "Reacties";
        this.CommentsPostPlaceholder = "Laat een reactie achter";
        this.CommentsMoreCommentsButton = "Zie oudere reacties";
        this.ViewLikesLink = "likes";
        this.ViewLikes2Link = "likes";
        this.ViewLikeLink = "like";
        this.ReportAsSpam = "Meld spam";
        this.ReportAsInappropriate = "Meld als ongepast";
        this.ReportNotificationTitle = "Bedankt";
        this.ReportNotificationText = "Een van onze moderators zal de content zo snel mogelijk bekijken";
        this.DeleteActivity = "Verwijder activiteit";
        this.DeleteComment = "Verwijder reactie";
        this.ActivityNotFound = "Activiteit is niet meer beschikbaar";
        this.ErrorYoureBanned = "Je toegang tot bepaalde functies is tijdelijk beperkt";
        this.NotificationsChannelName = "Sociaal";
        this.NCUITitle = "Alle notificaties";
        this.NCUIFriendRequestConfirmButton = "Bevestig";
        this.NCUIFriendRequestConfirmationText = "Je bent nu verbonden";
        this.NCUISectionHeader = "Notificaties";
        this.NCUIPlaceholderTitle = "Je bent helemaal bij";
        this.NCUIPlaceholderText = "Nieuwe notificaties verschijnen hier";
        this.NCUIDeleteAllButton = "Verwijder alle notificaties";
        this.NCUIMarkAllAsReadButton = "Markeer alle notificaties als gelezen";
        this.NCUIMarkAsReadButton = "Markeer notificatie als gelezen";
        this.NCUIMarkAsUnreadButton = "markeer notificaties als ongelezen";
        this.NCUIDeleteButton = "Verwijder notificatie";
        this.NCUIFriendRequestDeleteButton = "verwijder";
    }
}
