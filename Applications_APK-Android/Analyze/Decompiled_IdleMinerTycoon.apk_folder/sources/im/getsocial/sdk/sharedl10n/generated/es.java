package im.getsocial.sdk.sharedl10n.generated;

public class es extends LanguageStrings {
    public es() {
        this.OkButton = "OK";
        this.CancelButton = "Cancelar";
        this.ConnectionLostTitle = "Pérdida de conexión";
        this.ConnectionLostMessage = "¡Vaya! Parece que has perdido la conexión a internet. Por favor, vuelve a conectar.";
        this.PullDownToRefresh = "Tira hacia abajo para recargar";
        this.PullUpToLoadMore = "Tira hacia arriba para cargar más";
        this.ReleaseToRefresh = "Suelta para recargar";
        this.ReleaseToLoadMore = "Suelta para cargar más ";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "A sucedido un error. ¡Por favor, reinténtalo!";
        this.InviteFriends = "Invitar amigos";
        this.TimestampJustNow = "ahora mismo";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fsem";
        this.TimestampYesterday = "Ayer";
        this.ActivityTitle = "Actividad";
        this.ActivityPostPlaceholder = "¡Cuenta algo!";
        this.ActivityAllNoActivitiesPlaceholderTitle = "No hay actividad";
        this.ActivityNoSearchResultsPlaceholderTitle = "No hay resultados para **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "No hay ninguna actividad aquí todavía. ¿Por qué no inicias algo?";
        this.ViewCommentsLink = "comentarios";
        this.ViewComments2Link = "comentarios";
        this.ViewCommentLink = "comentario";
        this.CommentsTitle = "Comentarios";
        this.CommentsPostPlaceholder = "Deja un comentario";
        this.CommentsMoreCommentsButton = "Ver comentarios anteriores";
        this.ViewLikesLink = "me gusta";
        this.ViewLikes2Link = "me gusta";
        this.ViewLikeLink = "me gusta";
        this.ReportAsSpam = "Denunciar como spam";
        this.ReportAsInappropriate = "Denunciar como inapropiado";
        this.ReportNotificationTitle = "¡Gracias!";
        this.ReportNotificationText = "Uno de nuestros moderadores revisará el contenido tan pronto como sea posible.";
        this.DeleteActivity = "Borrar Actividad";
        this.DeleteComment = "Borrar comentario";
        this.ActivityNotFound = "Esta actividad ya no está disponible";
        this.ErrorYoureBanned = "Tu acceso a algunas características se ha limitado temporalmente";
        this.NotificationsChannelName = "Sociales";
        this.NCUITitle = "Todas las notificaciones";
        this.NCUIFriendRequestConfirmButton = "Confirmar";
        this.NCUIFriendRequestConfirmationText = "Ahora está conectado";
        this.NCUISectionHeader = "Notificaciones";
        this.NCUIPlaceholderTitle = "Todas al día";
        this.NCUIPlaceholderText = "Las notificaciones nuevas aparecerán aquí";
        this.NCUIDeleteAllButton = "Borrar todas las notificaciones";
        this.NCUIMarkAllAsReadButton = "Marcar todas las notificaciones como leídas";
        this.NCUIMarkAsReadButton = "Marcar notificación como leída";
        this.NCUIMarkAsUnreadButton = "Marcar notificación como no leída";
        this.NCUIDeleteButton = "Borrar notificación";
        this.NCUIFriendRequestDeleteButton = "Borrar";
    }
}
