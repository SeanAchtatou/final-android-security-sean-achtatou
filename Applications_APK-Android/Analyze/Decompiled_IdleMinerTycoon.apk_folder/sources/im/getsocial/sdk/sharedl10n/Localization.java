package im.getsocial.sdk.sharedl10n;

import im.getsocial.sdk.consts.LanguageCodes;
import im.getsocial.sdk.sharedl10n.generated.LanguageStrings;
import im.getsocial.sdk.sharedl10n.generated.da;
import im.getsocial.sdk.sharedl10n.generated.de;
import im.getsocial.sdk.sharedl10n.generated.en;
import im.getsocial.sdk.sharedl10n.generated.es;
import im.getsocial.sdk.sharedl10n.generated.fr;
import im.getsocial.sdk.sharedl10n.generated.gu;
import im.getsocial.sdk.sharedl10n.generated.hi;
import im.getsocial.sdk.sharedl10n.generated.idLang;
import im.getsocial.sdk.sharedl10n.generated.is;
import im.getsocial.sdk.sharedl10n.generated.it;
import im.getsocial.sdk.sharedl10n.generated.ja;
import im.getsocial.sdk.sharedl10n.generated.ko;
import im.getsocial.sdk.sharedl10n.generated.ms;
import im.getsocial.sdk.sharedl10n.generated.nb;
import im.getsocial.sdk.sharedl10n.generated.nl;
import im.getsocial.sdk.sharedl10n.generated.pl;
import im.getsocial.sdk.sharedl10n.generated.pt;
import im.getsocial.sdk.sharedl10n.generated.ptbr;
import im.getsocial.sdk.sharedl10n.generated.ru;
import im.getsocial.sdk.sharedl10n.generated.sv;
import im.getsocial.sdk.sharedl10n.generated.tl;
import im.getsocial.sdk.sharedl10n.generated.tr;
import im.getsocial.sdk.sharedl10n.generated.uk;
import im.getsocial.sdk.sharedl10n.generated.vi;
import im.getsocial.sdk.sharedl10n.generated.zhHans;
import im.getsocial.sdk.sharedl10n.generated.zhHant;
import java.util.HashMap;
import java.util.Map;

public final class Localization {
    private static Map<String, LanguageStrings> acquisition = new HashMap();
    private static Map<String, Class<? extends LanguageStrings>> attribution;
    private static final LanguageStrings getsocial = new en();
    private final UserLanguageCodeProvider mobile;

    public interface UserLanguageCodeProvider {
        String getCurrentLanguageCode();
    }

    static {
        HashMap hashMap = new HashMap();
        attribution = hashMap;
        hashMap.put(LanguageCodes.CHINESE_SIMPLIFIED, zhHans.class);
        attribution.put(LanguageCodes.CHINESE_TRADITIONAL, zhHant.class);
        attribution.put(LanguageCodes.DANISH, da.class);
        attribution.put(LanguageCodes.DUTCH, nl.class);
        attribution.put("en", en.class);
        attribution.put(LanguageCodes.FRENCH, fr.class);
        attribution.put(LanguageCodes.GERMAN, de.class);
        attribution.put(LanguageCodes.GUJARATI, gu.class);
        attribution.put(LanguageCodes.HINDI, hi.class);
        attribution.put(LanguageCodes.ICELANDIC, is.class);
        attribution.put("id", idLang.class);
        attribution.put(LanguageCodes.ITALIAN, it.class);
        attribution.put(LanguageCodes.JAPANESE, ja.class);
        attribution.put(LanguageCodes.KOREAN, ko.class);
        attribution.put(LanguageCodes.MALAY, ms.class);
        attribution.put(LanguageCodes.NORWEGIAN, nb.class);
        attribution.put(LanguageCodes.POLISH, pl.class);
        attribution.put(LanguageCodes.PORTUGUESE, pt.class);
        attribution.put(LanguageCodes.PORTUGUESE_BRAZILLIAN, ptbr.class);
        attribution.put(LanguageCodes.RUSSIAN, ru.class);
        attribution.put(LanguageCodes.SPANISH, es.class);
        attribution.put(LanguageCodes.SWEDISH, sv.class);
        attribution.put(LanguageCodes.TAGALOG, tl.class);
        attribution.put(LanguageCodes.TURKISH, tr.class);
        attribution.put(LanguageCodes.UKRAINIAN, uk.class);
        attribution.put(LanguageCodes.VIETNAMESE, vi.class);
    }

    public Localization(UserLanguageCodeProvider userLanguageCodeProvider) {
        if (userLanguageCodeProvider != null) {
            this.mobile = userLanguageCodeProvider;
            return;
        }
        throw new NullPointerException("languageProvider can't be null");
    }

    public final LanguageStrings strings() {
        return forLanguage(getsocial());
    }

    /* access modifiers changed from: package-private */
    public final String getsocial() {
        return this.mobile.getCurrentLanguageCode();
    }

    public static LanguageStrings forLanguage(String str) {
        if (!attribution.containsKey(str)) {
            return getsocial;
        }
        if (!acquisition.containsKey(str)) {
            try {
                acquisition.put(str, attribution.get(str).newInstance());
            } catch (Exception e) {
                e.printStackTrace();
                return getsocial;
            }
        }
        return acquisition.get(str);
    }
}
