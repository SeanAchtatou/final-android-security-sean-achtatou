package im.getsocial.sdk.sharedl10n.generated;

public class hi extends LanguageStrings {
    public hi() {
        this.OkButton = "ठीक";
        this.CancelButton = "रद्द करना";
        this.ConnectionLostTitle = "संपर्क टूट गया";
        this.ConnectionLostMessage = "ऐसा लगता है कि आपका इंटरनेट कनेक्शन गुम हो गया है। कृपया पुनः कनेक्ट करें।";
        this.PullDownToRefresh = "ताज़ा करने के लिए नीचे खींचें";
        this.PullUpToLoadMore = "अधिक लोड करने के लिए ऊपर खींचें";
        this.ReleaseToRefresh = "ताज़ा होने के लिए छोड़ दें";
        this.ReleaseToLoadMore = "अधिक लोड करने के लिए रिलीज करे ";
        this.ErrorAlertMessageTitle = "अरे!";
        this.ErrorAlertMessage = "कुछ गलत हो गया। कृपया पुन: प्रयास करें!";
        this.InviteFriends = "दोस्तों को आमंत्रित करें";
        this.TimestampJustNow = "अभी";
        this.TimestampSeconds = "%1.0f सेकंड";
        this.TimestampMinutes = "%1.0f मिनट";
        this.TimestampHours = "%1.0f घंटे";
        this.TimestampDays = "%1.0f दिन";
        this.TimestampWeeks = "%1.0f सप्ताह";
        this.TimestampYesterday = "बिता कल";
        this.ActivityTitle = "गतिविधि";
        this.ActivityPostPlaceholder = "क्या चल रहा हैं";
        this.ActivityAllNoActivitiesPlaceholderTitle = "कोई गतिविधि नहीं";
        this.ActivityNoSearchResultsPlaceholderTitle = "**[SEARCH_TERM]** के लिए कोई परिणाम नहीं ";
        this.ActivityAllNoActivitiesPlaceholderMessage = "अभी तक यहां कोई गतिविधि नहीं है। आप कुछ क्यों शुरू नहीं करते हैं?";
        this.ViewCommentsLink = "टिप्पणियाँ";
        this.ViewComments2Link = "टिप्पणियाँ";
        this.ViewCommentLink = "टिप्पणी";
        this.CommentsTitle = "टिप्पणियाँ";
        this.CommentsPostPlaceholder = "एक टिप्पणी छोड़ें";
        this.CommentsMoreCommentsButton = "पुरानी टिप्पणियां देखें";
        this.ViewLikesLink = "पसंद";
        this.ViewLikes2Link = "पसंद";
        this.ViewLikeLink = "पसंद";
        this.ReportAsSpam = "स्पैम के रूप में रिपोर्ट करे";
        this.ReportAsInappropriate = "अनुचित के रूप में रिपोर्ट करें";
        this.ReportNotificationTitle = "धन्यवाद!";
        this.ReportNotificationText = "हमारे मॉडरेटर में से एक जल्द से जल्द सूचना की समीक्षा करेगा";
        this.DeleteActivity = "गतिविधि हटाएं";
        this.DeleteComment = "टिप्पणी हटाएं";
        this.ActivityNotFound = "गतिविधि अब उपलब्ध नहीं है";
        this.ErrorYoureBanned = "कुछ सुविधाओं तक आपकी पहुंच अस्थायी रूप से सीमित कर दी गई है";
        this.NotificationsChannelName = "सामाजिक";
        this.NCUITitle = "सभी अधिसूचनाएं";
        this.NCUIFriendRequestConfirmButton = "पुष्टि करें";
        this.NCUIFriendRequestConfirmationText = "अब आप कनेक्ट हैं";
        this.NCUISectionHeader = "अधिसूचनाएं";
        this.NCUIPlaceholderTitle = "सभी अधिसूचनाएं पढ़ ली गई हैं";
        this.NCUIPlaceholderText = "नई अधिसूचनाएं यहां दिखाई देंगी";
        this.NCUIDeleteAllButton = "सभी अधिसूचनाएं हटाएं";
        this.NCUIMarkAllAsReadButton = "पढ़ी गई सभी अधिसूचनाओं को चिह्नित करें";
        this.NCUIMarkAsReadButton = "अधिसूचना को पढ़ लिया के रूप में चिह्नित करें";
        this.NCUIMarkAsUnreadButton = "अधिसूचना को अपठित के रूप में चिह्नित करें";
        this.NCUIDeleteButton = "अधिसूचना हटाएं";
        this.NCUIFriendRequestDeleteButton = "हटाएं";
    }
}
