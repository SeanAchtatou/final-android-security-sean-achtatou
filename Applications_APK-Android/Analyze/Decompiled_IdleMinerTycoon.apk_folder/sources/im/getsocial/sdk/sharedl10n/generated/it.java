package im.getsocial.sdk.sharedl10n.generated;

public class it extends LanguageStrings {
    public it() {
        this.OkButton = "OK";
        this.CancelButton = "Annulla";
        this.ConnectionLostTitle = "Connessione persa";
        this.ConnectionLostMessage = "Oh no! Sembra che tu abbia perso la connessione a Internet. Riconnettiti.";
        this.PullDownToRefresh = "Tira giù per aggiornare";
        this.PullUpToLoadMore = "Tira su per caricare ancora";
        this.ReleaseToRefresh = "Rilascia per aggiornare";
        this.ReleaseToLoadMore = "Rilascia per caricare ancora";
        this.ErrorAlertMessageTitle = "Oops!";
        this.ErrorAlertMessage = "Si è verificato un errore. Riprova!";
        this.InviteFriends = "Invita amici";
        this.TimestampJustNow = "proprio ora";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fw";
        this.TimestampYesterday = "Ieri";
        this.ActivityTitle = "Attività";
        this.ActivityPostPlaceholder = "Dici qualcosa!";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Nessuna attività";
        this.ActivityNoSearchResultsPlaceholderTitle = "Nessun risultato per **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Ancora nessuna attività. Perché non iniziare?";
        this.ViewCommentsLink = "commenti";
        this.ViewComments2Link = "commenti";
        this.ViewCommentLink = "commento";
        this.CommentsTitle = "Commenti";
        this.CommentsPostPlaceholder = "Lascia un commento";
        this.CommentsMoreCommentsButton = "Vedi i commenti vecchi";
        this.ViewLikesLink = "likes";
        this.ViewLikes2Link = "likes";
        this.ViewLikeLink = "like";
        this.ReportAsSpam = "Segnala come spam";
        this.ReportAsInappropriate = "Segnala come inappropriato";
        this.ReportNotificationTitle = "Grazie!";
        this.ReportNotificationText = "Un moderatore revisionerà il contenuto al più presto";
        this.DeleteActivity = "Elimina attività";
        this.DeleteComment = "Elimina commento";
        this.ActivityNotFound = "L'attività non è più disponibile";
        this.ErrorYoureBanned = "Ti abbiamo limitato temporaneamente l'accesso ad alcune funzioni";
        this.NotificationsChannelName = "Social";
        this.NCUITitle = "Tutte le notifiche";
        this.NCUIFriendRequestConfirmButton = "Conferma";
        this.NCUIFriendRequestConfirmationText = "Ora sei connesso";
        this.NCUISectionHeader = "Notifiche";
        this.NCUIPlaceholderTitle = "Hai letto tutto";
        this.NCUIPlaceholderText = "Le nuove notifiche compariranno qui";
        this.NCUIDeleteAllButton = "Cancella tutte le notifiche";
        this.NCUIMarkAllAsReadButton = "Segna tutte le notifiche come lette";
        this.NCUIMarkAsReadButton = "Segna la notifica come letta";
        this.NCUIMarkAsUnreadButton = "Segna la notifica come non letta";
        this.NCUIDeleteButton = "Cancella la notifica";
        this.NCUIFriendRequestDeleteButton = "Annulla";
    }
}
