package im.getsocial.sdk.sharedl10n.generated;

public class ptbr extends LanguageStrings {
    public ptbr() {
        this.OkButton = "OK";
        this.CancelButton = "Cancelar";
        this.ConnectionLostTitle = "Conexão perdida";
        this.ConnectionLostMessage = "Ops! Parece que você perdeu sua conexão com a Internet. Por favor, reconecte.";
        this.PullDownToRefresh = "Puxe para baixo para atualizar";
        this.PullUpToLoadMore = "Puxe para cima para carregar mais";
        this.ReleaseToRefresh = "Solte para atualizar";
        this.ReleaseToLoadMore = "Solte para carregar mais";
        this.ErrorAlertMessageTitle = "Ops!";
        this.ErrorAlertMessage = "Ocorreu um erro. Por favor, tente outra vez!";
        this.InviteFriends = "Convide amigos";
        this.TimestampJustNow = "agora mesmo";
        this.TimestampSeconds = "%1.0fs";
        this.TimestampMinutes = "%1.0fm";
        this.TimestampHours = "%1.0fh";
        this.TimestampDays = "%1.0fd";
        this.TimestampWeeks = "%1.0fS";
        this.TimestampYesterday = "Ontem";
        this.ActivityTitle = "Atividade";
        this.ActivityPostPlaceholder = "E aí?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Nenhuma atividade";
        this.ActivityNoSearchResultsPlaceholderTitle = "Sem resultados para **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Ainda não há atividades aqui. Por que você não começa alguma coisa?";
        this.ViewCommentsLink = "comentários";
        this.ViewComments2Link = "comentários";
        this.ViewCommentLink = "comentário";
        this.CommentsTitle = "Comentários";
        this.CommentsPostPlaceholder = "Fazer um comentário";
        this.CommentsMoreCommentsButton = "Ver comentários mais antigos";
        this.ViewLikesLink = "curtidas";
        this.ViewLikes2Link = "curtidas";
        this.ViewLikeLink = "curtir";
        this.ReportAsSpam = "Reportar como spam";
        this.ReportAsInappropriate = "Reportar como inapropriado";
        this.ReportNotificationTitle = "Obrigado!";
        this.ReportNotificationText = "Um dos nossos moderadores irá rever o conteúdo assim que possível";
        this.DeleteActivity = "Apagar atividade";
        this.DeleteComment = "Apagar comentário";
        this.ActivityNotFound = "A atividade já não está disponível";
        this.ErrorYoureBanned = "O seu acesso foi temporariamente limitado para algumas funcionalidades";
        this.NotificationsChannelName = "Social";
        this.NCUITitle = "Todas notificações";
        this.NCUIFriendRequestConfirmButton = "Confirmar";
        this.NCUIFriendRequestConfirmationText = "Você agora está conectado";
        this.NCUISectionHeader = "Notificações";
        this.NCUIPlaceholderTitle = "Todas lidas";
        this.NCUIPlaceholderText = "Novas notificações aparecerão aqui";
        this.NCUIDeleteAllButton = "Apagar todas notificações";
        this.NCUIMarkAllAsReadButton = "Marcar todas notificações como lidas";
        this.NCUIMarkAsReadButton = "Marcar notificação como lida";
        this.NCUIMarkAsUnreadButton = "Marcar notificação como não lida";
        this.NCUIDeleteButton = "Apagar notificação";
        this.NCUIFriendRequestDeleteButton = "Apagar";
    }
}
