package im.getsocial.sdk.sharedl10n.generated;

public class ru extends LanguageStrings {
    public ru() {
        this.OkButton = "OK";
        this.CancelButton = "Отмена";
        this.ConnectionLostTitle = "Обрыв связи";
        this.ConnectionLostMessage = "Ой! Похоже, нет подключения к Интернету. Пожалуйста, попробуйте еще раз.";
        this.PullDownToRefresh = "Потяните вниз для обновления";
        this.PullUpToLoadMore = "Потяните вверх, чтобы загрузить больше";
        this.ReleaseToRefresh = "Отпустите для обновления";
        this.ReleaseToLoadMore = "Отпустите, чтобы загрузить больше";
        this.ErrorAlertMessageTitle = "Ой!";
        this.ErrorAlertMessage = "Что-то пошло не так. Пожалуйста, повторите попытку";
        this.InviteFriends = "Пригласить друзей";
        this.TimestampJustNow = "только что";
        this.TimestampSeconds = "%1.0fс";
        this.TimestampMinutes = "%1.0fм";
        this.TimestampHours = "%1.0fч";
        this.TimestampDays = "%1.0fд";
        this.TimestampWeeks = "%1.0fн";
        this.TimestampYesterday = "Вчера";
        this.ActivityTitle = "Лента новостей";
        this.ActivityPostPlaceholder = "Что нового?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "Новостей нет";
        this.ActivityNoSearchResultsPlaceholderTitle = "Нет результатов для **[SEARCH_TERM]**";
        this.ActivityAllNoActivitiesPlaceholderMessage = "Нет постов. Почему бы не добавить несколько?";
        this.ViewCommentsLink = "комментария";
        this.ViewComments2Link = "комментариев";
        this.ViewCommentLink = "комментарий";
        this.CommentsTitle = "Комментарии";
        this.CommentsPostPlaceholder = "Прокомментировать";
        this.CommentsMoreCommentsButton = "Предыдущие комментарии";
        this.ViewLikesLink = "лайка";
        this.ViewLikes2Link = "лайков";
        this.ViewLikeLink = "лайк";
        this.ReportAsSpam = "Пожаловаться на спам";
        this.ReportAsInappropriate = "Пожаловаться на неприемлемый контент";
        this.ReportNotificationTitle = "Спасибо!";
        this.ReportNotificationText = "Наш модератор рассмотрит отмеченный контент как можно скорее";
        this.DeleteActivity = "Удалить пост";
        this.DeleteComment = "Удалить комментарий";
        this.ActivityNotFound = "Пост больше не доступен";
        this.ErrorYoureBanned = "Твой доступ к некоторым функциям временно ограничен";
        this.NotificationsChannelName = "Социальные";
        this.NCUITitle = "Все уведомления";
        this.NCUIFriendRequestConfirmButton = "Подтвердить";
        this.NCUIFriendRequestConfirmationText = "Теперь вы друзья";
        this.NCUISectionHeader = "Уведомления";
        this.NCUIPlaceholderTitle = "Все прочитано";
        this.NCUIPlaceholderText = "Новые уведомления будут появляться здесь";
        this.NCUIDeleteAllButton = "Удалить все уведомления";
        this.NCUIMarkAllAsReadButton = "Отметить все уведомления как прочитанные";
        this.NCUIMarkAsReadButton = "Отметить уведомления как прочитанное";
        this.NCUIMarkAsUnreadButton = "Отметить уведомление как непрочитанное";
        this.NCUIDeleteButton = "Удалить уведомление";
        this.NCUIFriendRequestDeleteButton = "Удалить";
    }
}
