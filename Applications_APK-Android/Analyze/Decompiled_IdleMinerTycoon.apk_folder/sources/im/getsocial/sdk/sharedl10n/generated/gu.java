package im.getsocial.sdk.sharedl10n.generated;

public class gu extends LanguageStrings {
    public gu() {
        this.OkButton = "ઠીક છે";
        this.CancelButton = "રદ કરો";
        this.ConnectionLostTitle = "કનેક્શન ગુમાવ્યું";
        this.ConnectionLostMessage = "એવું લાગે છે કે તમારું ઇન્ટરનેટ કનેક્શન ગયું છે. ફરીથી કનેક્ટ કરો";
        this.PullDownToRefresh = "રીફ્રેશ કરવા માટે નીચે ખેંચો";
        this.PullUpToLoadMore = "વધુ લોડ કરવા માટે ઉપર ખેંચો";
        this.ReleaseToRefresh = "રીફ્રેશ કરવા માટે છોડો";
        this.ReleaseToLoadMore = "વધુ લોડ કરવા માટે રીલિઝ કરો";
        this.ErrorAlertMessageTitle = "અરે!";
        this.ErrorAlertMessage = "કંઈક ખોટું થયું. મહેરબાની કરીને ફરીથી પ્રયતન કરો!";
        this.InviteFriends = "મિત્રોને આમંત્રણ આપો";
        this.TimestampJustNow = "હમણાજ";
        this.TimestampSeconds = "%1.0f સેકંડ";
        this.TimestampMinutes = "%1.0f મિનિટ";
        this.TimestampHours = "%1.0f કલાક";
        this.TimestampDays = "%1.0f દિવસ";
        this.TimestampWeeks = "%1.0f અઠવાડિયા";
        this.TimestampYesterday = "ગઇકાલે";
        this.ActivityTitle = "પ્રવૃત્તિ";
        this.ActivityPostPlaceholder = "શું ચાલે છે?";
        this.ActivityAllNoActivitiesPlaceholderTitle = "કોઈ પ્રવૃત્તિ નથી";
        this.ActivityNoSearchResultsPlaceholderTitle = "**[SEARCH_TERM]** માટે કોઈ પરિણામો નથી";
        this.ActivityAllNoActivitiesPlaceholderMessage = "અહીં હજી કોઈ પ્રવૃત્તિ નથી. તમે કંઈક શા માટે શરૂ નથી કરતા?";
        this.ViewCommentsLink = "ટિપ્પણીઓ";
        this.ViewComments2Link = "ટિપ્પણીઓ";
        this.ViewCommentLink = "ટિપ્પણી";
        this.CommentsTitle = "ટિપ્પણીઓ";
        this.CommentsPostPlaceholder = "એક ટિપ્પણી મૂકો";
        this.CommentsMoreCommentsButton = "જૂની ટિપ્પણીઓ જુઓ";
        this.ViewLikesLink = "પસંદ";
        this.ViewLikes2Link = "પસંદ";
        this.ViewLikeLink = "પસંદ";
        this.ReportAsSpam = "સ્પામ તરીકે જાણ કરો";
        this.ReportAsInappropriate = "અયોગ્ય તરીકે જાણ કરો";
        this.ReportNotificationTitle = "આભાર!";
        this.ReportNotificationText = "અમારા સંચાલકો જલદીથી સંદેશની સમીક્ષા કરશે";
        this.DeleteActivity = "પ્રવૃત્તિ કાઢી નાખો";
        this.DeleteComment = "ટિપ્પણી કાઢી નાખો";
        this.ActivityNotFound = "પ્રવૃત્તિ હવે ઉપલબ્ધ રહેશે નહીં";
        this.ErrorYoureBanned = "કેટલીક સુવિધાઓની તમારી ઍક્સેસને અસ્થાયી રૂપે મર્યાદિત છે";
        this.NotificationsChannelName = "સામાજિક";
        this.NCUITitle = "બધી સૂચનાઓ";
        this.NCUIFriendRequestConfirmButton = "સમર્થિત કરો";
        this.NCUIFriendRequestConfirmationText = "તમે હવે જોડાયેલ છે";
        this.NCUISectionHeader = "જાહેરાતો";
        this.NCUIPlaceholderTitle = "બધા પકડેલા";
        this.NCUIPlaceholderText = "નવી જાહેરાતો અહીં દેખાશે";
        this.NCUIDeleteAllButton = "બધી જાહેરાતો કાઢી નાખો";
        this.NCUIMarkAllAsReadButton = "બધી જાહેરાતોને વાંચેલ તરીકેનું ચિહ્ન કરો";
        this.NCUIMarkAsReadButton = "જાહેરાતને વાંચેલ તરીકેનું ચિહ્ન કરો";
        this.NCUIMarkAsUnreadButton = "જાહેરાતને અવાચ્ય તરીકેનું ચિહ્ન કરો";
        this.NCUIDeleteButton = "જાહેરાતને કાઢી નાખો";
        this.NCUIFriendRequestDeleteButton = "કાઢી નાખો";
    }
}
