package im.getsocial.sdk.invites.a.c;

import im.getsocial.sdk.GetSocialException;

/* compiled from: CancelledInviteException */
public class jjbQypPegg extends GetSocialException {
    public jjbQypPegg() {
        super(100, "Invite was cancelled by user.");
    }
}
