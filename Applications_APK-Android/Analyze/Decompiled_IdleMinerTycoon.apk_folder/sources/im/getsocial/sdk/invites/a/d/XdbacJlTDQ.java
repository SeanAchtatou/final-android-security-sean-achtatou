package im.getsocial.sdk.invites.a.d;

import im.getsocial.sdk.internal.c.l.ztWNWCuZiM;
import im.getsocial.sdk.invites.LocalizableText;
import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import im.getsocial.sdk.invites.cjrhisSQCL;
import java.util.HashMap;
import java.util.Map;

/* compiled from: SmartInvitesCommonFunctions */
public final class XdbacJlTDQ {
    private XdbacJlTDQ() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.invites.a.d.XdbacJlTDQ.getsocial(im.getsocial.sdk.invites.a.b.pdwpUtZXDT, im.getsocial.sdk.invites.a.b.pdwpUtZXDT, boolean):im.getsocial.sdk.invites.a.b.pdwpUtZXDT
     arg types: [im.getsocial.sdk.invites.a.b.pdwpUtZXDT, im.getsocial.sdk.invites.a.b.pdwpUtZXDT, int]
     candidates:
      im.getsocial.sdk.invites.a.d.XdbacJlTDQ.getsocial(im.getsocial.sdk.invites.a.b.pdwpUtZXDT, im.getsocial.sdk.invites.a.b.pdwpUtZXDT, im.getsocial.sdk.invites.a.b.pdwpUtZXDT):im.getsocial.sdk.invites.a.b.pdwpUtZXDT
      im.getsocial.sdk.invites.a.d.XdbacJlTDQ.getsocial(java.util.Map<java.lang.String, java.lang.String>, java.util.Map<java.lang.String, java.lang.String>, java.lang.String):void
      im.getsocial.sdk.invites.a.d.XdbacJlTDQ.getsocial(im.getsocial.sdk.invites.a.b.pdwpUtZXDT, im.getsocial.sdk.invites.a.b.pdwpUtZXDT, boolean):im.getsocial.sdk.invites.a.b.pdwpUtZXDT */
    public static pdwpUtZXDT getsocial(pdwpUtZXDT pdwputzxdt, pdwpUtZXDT pdwputzxdt2, pdwpUtZXDT pdwputzxdt3) {
        return getsocial(getsocial(pdwputzxdt, pdwputzxdt2, false), pdwputzxdt3, true);
    }

    private static pdwpUtZXDT getsocial(pdwpUtZXDT pdwputzxdt, pdwpUtZXDT pdwputzxdt2, boolean z) {
        pdwpUtZXDT.jjbQypPegg mobile = pdwpUtZXDT.getsocial().getsocial(pdwputzxdt.acquisition()).attribution(pdwputzxdt.mobile()).acquisition(pdwputzxdt.retention()).retention(pdwputzxdt.cat()).mobile(pdwputzxdt.mau());
        if (pdwputzxdt2 != null) {
            if (!pdwputzxdt.attribution()) {
                boolean z2 = true;
                if (z) {
                    boolean z3 = pdwputzxdt2.retention() != null;
                    boolean z4 = pdwputzxdt2.cat() != null;
                    if (pdwputzxdt2.mau() == null) {
                        z2 = false;
                    }
                    if (z3 || z4 || z2) {
                        mobile.acquisition(pdwputzxdt2.retention());
                        mobile.retention(pdwputzxdt2.cat());
                        mobile.mobile(pdwputzxdt2.mau());
                    }
                } else {
                    if (pdwputzxdt2.retention() != null) {
                        mobile.acquisition(pdwputzxdt2.retention());
                    } else {
                        z2 = false;
                    }
                    if (pdwputzxdt2.cat() != null || z2) {
                        mobile.retention(pdwputzxdt2.cat());
                    }
                    if (pdwputzxdt2.mau() != null || z2) {
                        mobile.mobile(pdwputzxdt2.mau());
                    }
                }
            }
            if (pdwputzxdt2.acquisition() != null) {
                HashMap hashMap = new HashMap(cjrhisSQCL.getsocial(pdwputzxdt2.acquisition()));
                if (pdwputzxdt != null) {
                    getsocial(hashMap, cjrhisSQCL.getsocial(pdwputzxdt.acquisition()));
                }
                mobile.getsocial(new LocalizableText(hashMap));
            }
            if (pdwputzxdt2.mobile() != null) {
                HashMap hashMap2 = new HashMap(cjrhisSQCL.getsocial(pdwputzxdt2.mobile()));
                if (pdwputzxdt != null) {
                    getsocial(hashMap2, cjrhisSQCL.getsocial(pdwputzxdt.mobile()));
                }
                mobile.attribution(new LocalizableText(hashMap2));
            }
        }
        return mobile.getsocial();
    }

    private static void getsocial(Map<String, String> map, Map<String, String> map2) {
        for (String str : map2.keySet()) {
            getsocial(map, map2, str);
        }
        getsocial(map, map2, "en");
    }

    private static void getsocial(Map<String, String> map, Map<String, String> map2, String str) {
        if (ztWNWCuZiM.getsocial(map.get(str))) {
            String str2 = map2.get(str);
            if (!ztWNWCuZiM.getsocial(str2)) {
                map.put(str, str2);
            }
        }
    }
}
