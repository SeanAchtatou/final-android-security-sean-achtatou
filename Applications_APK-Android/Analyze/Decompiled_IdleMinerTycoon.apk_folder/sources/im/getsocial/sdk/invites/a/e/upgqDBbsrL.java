package im.getsocial.sdk.invites.a.e;

import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import com.google.android.gms.drive.DriveFile;
import im.getsocial.sdk.internal.m.iFpupLCESp;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;

/* compiled from: EmailInviteChannelPlugin */
public class upgqDBbsrL extends InviteChannelPlugin {
    public boolean isAvailableForDevice(InviteChannel inviteChannel) {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri
     arg types: [android.content.Context, android.graphics.Bitmap, int, java.lang.String]
     candidates:
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, java.lang.String, java.lang.String, im.getsocial.sdk.internal.m.iFpupLCESp$jjbQypPegg):void
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri */
    public void presentChannelInterface(final InviteChannel inviteChannel, InvitePackage invitePackage, final InviteCallback inviteCallback) {
        final Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("mailto:"));
        intent.putExtra("android.intent.extra.SUBJECT", invitePackage.getSubject());
        intent.putExtra("android.intent.extra.TEXT", Html.fromHtml(invitePackage.getText()).toString());
        AnonymousClass1 r1 = new iFpupLCESp.cjrhisSQCL() {
            public final void getsocial(Uri uri) {
                if (uri != null) {
                    intent.putExtra("android.intent.extra.STREAM", uri);
                }
                upgqDBbsrL.this.getsocial(intent, inviteChannel, inviteCallback);
            }
        };
        boolean z = true;
        boolean z2 = invitePackage.getGifUrl() != null;
        boolean z3 = invitePackage.getVideoUrl() != null;
        if (invitePackage.getImage() == null) {
            z = false;
        }
        if (z2) {
            iFpupLCESp.getsocial(getContext(), invitePackage.getGifUrl(), r1);
        } else if (z3) {
            iFpupLCESp.getsocial(getContext(), invitePackage.getVideoUrl(), r1);
        } else if (z) {
            r1.getsocial(iFpupLCESp.getsocial(getContext(), invitePackage.getImage(), false, "getsocial-smartinvite-tempimage.jpg"));
        } else {
            getsocial(intent, inviteChannel, inviteCallback);
        }
    }

    /* access modifiers changed from: private */
    public void getsocial(Intent intent, InviteChannel inviteChannel, InviteCallback inviteCallback) {
        try {
            Intent createChooser = Intent.createChooser(intent, inviteChannel.getChannelName());
            createChooser.setFlags(DriveFile.MODE_READ_ONLY);
            getContext().startActivity(createChooser);
            inviteCallback.onComplete();
        } catch (Exception e) {
            inviteCallback.onError(e);
        }
    }
}
