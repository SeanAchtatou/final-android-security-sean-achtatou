package im.getsocial.sdk.invites.a.j;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.b.KSZKMmRWhZ;
import im.getsocial.sdk.internal.c.b.qZypgoeblR;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.k.b.cjrhisSQCL;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.g.upgqDBbsrL;
import im.getsocial.sdk.invites.a.k.ztWNWCuZiM;
import java.util.Arrays;
import java.util.List;

/* compiled from: SendInviteUseCase */
public final class XdbacJlTDQ extends jjbQypPegg {
    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    upgqDBbsrL dau;
    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    @KSZKMmRWhZ(getsocial = "channels_handling_lifecycle")
    @qZypgoeblR
    List<String> getsocial;
    @im.getsocial.sdk.internal.c.b.XdbacJlTDQ
    im.getsocial.sdk.invites.a.g.jjbQypPegg mau;

    public final void getsocial(String str, pdwpUtZXDT pdwputzxdt, LinkParams linkParams, InviteCallback inviteCallback) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Can not call executeInBackground with null channelId");
        final im.getsocial.sdk.invites.a.b.upgqDBbsrL upgqdbbsrl = this.dau.getsocial();
        final InviteChannel inviteChannel = upgqdbbsrl.getsocial(str);
        final pdwpUtZXDT pdwputzxdt2 = pdwputzxdt;
        final LinkParams linkParams2 = linkParams;
        final String str2 = str;
        final InviteCallback inviteCallback2 = inviteCallback;
        getsocial(new Runnable() {
            public void run() {
                XdbacJlTDQ.getsocial(XdbacJlTDQ.this, str2, inviteChannel, im.getsocial.sdk.invites.a.d.XdbacJlTDQ.getsocial(upgqdbbsrl.getsocial(), im.getsocial.sdk.invites.upgqDBbsrL.attribution(inviteChannel), XdbacJlTDQ.getsocial(pdwputzxdt2, linkParams2)), linkParams2, inviteCallback2);
            }
        });
    }

    static /* synthetic */ pdwpUtZXDT getsocial(pdwpUtZXDT pdwputzxdt, LinkParams linkParams) {
        byte[] bArr;
        byte[] bArr2 = ztWNWCuZiM.getsocial(linkParams);
        byte[] bArr3 = null;
        if (pdwputzxdt == null) {
            bArr = null;
        } else {
            bArr = pdwputzxdt.organic();
        }
        if (pdwputzxdt != null) {
            bArr3 = pdwputzxdt.dau();
        }
        boolean equals = Arrays.equals(bArr2, bArr3);
        pdwpUtZXDT.jjbQypPegg jjbqyppegg = pdwpUtZXDT.getsocial();
        if (pdwputzxdt != null) {
            jjbqyppegg.getsocial(pdwputzxdt.acquisition());
            jjbqyppegg.attribution(pdwputzxdt.mobile());
            jjbqyppegg.acquisition(pdwputzxdt.retention());
            jjbqyppegg.retention(pdwputzxdt.cat());
            jjbqyppegg.mobile(pdwputzxdt.mau());
        }
        if (bArr3 != null && !equals) {
            ztWNWCuZiM.getsocial(jjbqyppegg, new cjrhisSQCL().getsocial(im.getsocial.sdk.internal.k.a.upgqDBbsrL.getsocial(bArr3), im.getsocial.sdk.internal.k.a.XdbacJlTDQ.CUSTOM_INVITE_IMAGE));
        }
        if (bArr != null) {
            ztWNWCuZiM.getsocial(jjbqyppegg, new cjrhisSQCL().getsocial(im.getsocial.sdk.internal.k.a.upgqDBbsrL.getsocial(bArr), im.getsocial.sdk.internal.k.a.XdbacJlTDQ.CUSTOM_INVITE_IMAGE));
        }
        if (bArr2 != null) {
            im.getsocial.sdk.internal.k.a.pdwpUtZXDT pdwputzxdt2 = new cjrhisSQCL().getsocial(im.getsocial.sdk.internal.k.a.upgqDBbsrL.getsocial(bArr3), im.getsocial.sdk.internal.k.a.XdbacJlTDQ.CUSTOM_INVITE_IMAGE);
            ztWNWCuZiM.getsocial(linkParams, pdwputzxdt2);
            if (equals) {
                ztWNWCuZiM.getsocial(jjbqyppegg, pdwputzxdt2);
            }
        }
        return jjbqyppegg.getsocial();
    }

    static /* synthetic */ void getsocial(XdbacJlTDQ xdbacJlTDQ, String str, InviteChannel inviteChannel, pdwpUtZXDT pdwputzxdt, LinkParams linkParams, InviteCallback inviteCallback) {
        final boolean z = !(xdbacJlTDQ.getsocial == null || xdbacJlTDQ.getsocial.contains(str)) || (xdbacJlTDQ.mau.getsocial(str) instanceof im.getsocial.sdk.invites.a.e.jjbQypPegg);
        final im.getsocial.sdk.invites.a.b.cjrhisSQCL cjrhissqcl = new im.getsocial.sdk.invites.a.b.cjrhisSQCL();
        cjrhissqcl.acquisition = pdwputzxdt;
        im.getsocial.sdk.invites.a.b.XdbacJlTDQ xdbacJlTDQ2 = xdbacJlTDQ.getsocial().getsocial(str, linkParams);
        cjrhissqcl.attribution = xdbacJlTDQ2.attribution();
        cjrhissqcl.getsocial = xdbacJlTDQ2.getsocial();
        cjrhissqcl.retention = linkParams;
        if (pdwputzxdt.retention() != null && pdwputzxdt.dau() == null && !pdwputzxdt.attribution()) {
            cjrhissqcl.mobile = new im.getsocial.sdk.internal.k.b.upgqDBbsrL().getsocial(pdwputzxdt.retention());
        }
        final InviteChannel inviteChannel2 = inviteChannel;
        final InviteCallback inviteCallback2 = inviteCallback;
        xdbacJlTDQ.attribution(new Runnable() {
            public void run() {
                try {
                    new im.getsocial.sdk.invites.a.d.cjrhisSQCL().getsocial(inviteChannel2, cjrhissqcl, new CompletionCallback() {
                        public void onSuccess() {
                            if (z) {
                                XdbacJlTDQ.this.mau.getsocial(inviteCallback2);
                            } else {
                                inviteCallback2.onComplete();
                            }
                        }

                        public void onFailure(GetSocialException getSocialException) {
                            if (getSocialException instanceof im.getsocial.sdk.invites.a.c.jjbQypPegg) {
                                XdbacJlTDQ.attribution.attribution("Invite cancelled by user.");
                                inviteCallback2.onCancel();
                                return;
                            }
                            inviteCallback2.onError(getSocialException);
                        }
                    });
                } catch (Exception e) {
                    inviteCallback2.onError(e);
                }
            }
        });
    }
}
