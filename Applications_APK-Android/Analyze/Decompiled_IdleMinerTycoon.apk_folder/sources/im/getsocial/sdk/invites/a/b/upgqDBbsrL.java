package im.getsocial.sdk.invites.a.b;

import im.getsocial.sdk.invites.InviteChannel;
import java.util.List;

/* compiled from: InviteChannelsDescriptor */
public class upgqDBbsrL {
    List<InviteChannel> attribution;
    pdwpUtZXDT getsocial;

    public upgqDBbsrL(pdwpUtZXDT pdwputzxdt, List<InviteChannel> list) {
        this.getsocial = pdwputzxdt;
        this.attribution = list;
    }

    public final pdwpUtZXDT getsocial() {
        return this.getsocial;
    }

    public final List<InviteChannel> attribution() {
        return this.attribution;
    }

    public final InviteChannel getsocial(String str) {
        for (InviteChannel next : this.attribution) {
            if (next.getChannelId().equals(str)) {
                return next;
            }
        }
        throw new IllegalArgumentException("Channel with id '" + str + "' not found");
    }
}
