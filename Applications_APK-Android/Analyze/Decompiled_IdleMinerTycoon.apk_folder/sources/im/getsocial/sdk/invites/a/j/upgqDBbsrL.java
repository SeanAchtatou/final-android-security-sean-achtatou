package im.getsocial.sdk.invites.a.j;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.a.g.jjbQypPegg;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: GetAvailableInviteChannelsUseCase */
public final class upgqDBbsrL implements im.getsocial.sdk.internal.c.k.upgqDBbsrL {
    @XdbacJlTDQ
    im.getsocial.sdk.invites.a.g.upgqDBbsrL attribution;
    @XdbacJlTDQ
    jjbQypPegg getsocial;

    public upgqDBbsrL() {
        ztWNWCuZiM.getsocial(this);
    }

    public final List<InviteChannel> getsocial() {
        List<InviteChannel> attribution2 = this.attribution.getsocial().attribution();
        jjbQypPegg jjbqyppegg = this.getsocial;
        ArrayList arrayList = new ArrayList();
        for (InviteChannel next : attribution2) {
            if (next.isEnabled() && jjbqyppegg.getsocial(next.getChannelId()).getsocial(next)) {
                arrayList.add(next);
            }
        }
        Collections.sort(arrayList, InviteChannel.INVITE_CHANNELS_COMPARATOR_BASED_ON_DISPLAY_ORDER);
        return arrayList;
    }
}
