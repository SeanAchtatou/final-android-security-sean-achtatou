package im.getsocial.sdk.invites;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.drive.DriveFile;
import com.mintegral.msdk.base.entity.CampaignEx;
import im.getsocial.sdk.internal.m.fOrCGNYyfk;
import java.io.File;

public class ImageContentProvider extends ContentProvider {
    private static final UriMatcher getsocial = new UriMatcher(-1);

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public boolean onCreate() {
        getsocial.addURI(fOrCGNYyfk.getsocial(getContext()), "smart-invite.jpg", 1);
        getsocial.addURI(fOrCGNYyfk.getsocial(getContext()), "smart-invite-video.mp4", 2);
        getsocial.addURI(fOrCGNYyfk.getsocial(getContext()), "smart-invite-gif.gif", 3);
        getsocial.addURI(fOrCGNYyfk.getsocial(getContext()), "smart-invite-sticker.png", 4);
        return false;
    }

    public String getType(Uri uri) {
        switch (getsocial.match(uri)) {
            case 1:
                return "image/jpeg";
            case 2:
                return "video/mp4";
            case 3:
                return "image/gif";
            case 4:
                return "image/png";
            default:
                return null;
        }
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) {
        switch (getsocial.match(uri)) {
            case 1:
                return getsocial("getsocial-smartinvite-tempimage.jpg", str);
            case 2:
                return getsocial("getsocial-smartinvite-tempvideo.mp4", str);
            case 3:
                return getsocial("getsocial-smartinvite-tempgif.gif", str);
            case 4:
                return getsocial("getsocial-smartinvite-tempsticker.png", str);
            default:
                return null;
        }
    }

    private ParcelFileDescriptor getsocial(String str, String str2) {
        int i;
        Context context = getContext();
        if (context == null) {
            return null;
        }
        File file = new File(context.getCacheDir(), str);
        if (CampaignEx.JSON_KEY_AD_R.equals(str2)) {
            i = DriveFile.MODE_READ_ONLY;
        } else if ("w".equals(str2) || "wt".equals(str2)) {
            i = 738197504;
        } else if ("wa".equals(str2)) {
            i = 704643072;
        } else if ("rw".equals(str2)) {
            i = 939524096;
        } else if ("rwt".equals(str2)) {
            i = 1006632960;
        } else {
            throw new IllegalArgumentException("Invalid mode: " + str2);
        }
        return ParcelFileDescriptor.open(file, i);
    }

    public String[] getStreamTypes(Uri uri, String str) {
        String uri2 = uri.toString();
        if (uri2.endsWith("jpg")) {
            return new String[]{"image/jpeg", "image/jpg"};
        } else if (uri2.endsWith("mp4")) {
            return new String[]{"video/mp4"};
        } else if (uri2.endsWith("gif")) {
            return new String[]{"image/gif"};
        } else if (!uri2.endsWith("png")) {
            return new String[0];
        } else {
            return new String[]{"image/png"};
        }
    }
}
