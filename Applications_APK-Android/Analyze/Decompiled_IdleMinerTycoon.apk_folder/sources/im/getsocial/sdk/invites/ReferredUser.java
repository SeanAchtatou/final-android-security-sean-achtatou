package im.getsocial.sdk.invites;

import im.getsocial.sdk.usermanagement.PublicUser;
import java.util.Map;

public class ReferredUser extends PublicUser {
    /* access modifiers changed from: private */

    /* renamed from: android  reason: collision with root package name */
    public boolean f491android;
    /* access modifiers changed from: private */
    public long getsocial;
    /* access modifiers changed from: private */
    public boolean growth;
    /* access modifiers changed from: private */
    public String organic;
    /* access modifiers changed from: private */
    public String viral;

    protected ReferredUser() {
    }

    /* access modifiers changed from: protected */
    public final void getsocial(ReferredUser referredUser) {
        getsocial((PublicUser) referredUser);
        referredUser.getsocial = this.getsocial;
        referredUser.viral = this.viral;
        referredUser.growth = this.growth;
        referredUser.f491android = this.f491android;
        referredUser.organic = this.organic;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReferredUser) || !super.equals(obj)) {
            return false;
        }
        ReferredUser referredUser = (ReferredUser) obj;
        if (this.getsocial != referredUser.getsocial || this.growth != referredUser.growth || this.f491android != referredUser.f491android) {
            return false;
        }
        if (this.viral == null ? referredUser.viral != null : !this.viral.equals(referredUser.viral)) {
            return false;
        }
        if (this.organic != null) {
            return this.organic.equals(referredUser.organic);
        }
        return referredUser.organic == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((super.hashCode() * 31) + ((int) (this.getsocial ^ (this.getsocial >>> 32)))) * 31) + (this.viral != null ? this.viral.hashCode() : 0)) * 31;
        if (this.organic != null) {
            i = this.organic.hashCode();
        }
        return ((((hashCode + i) * 31) + (this.growth ? 1 : 0)) * 31) + (this.f491android ? 1 : 0);
    }

    public long getInstallationDate() {
        return this.getsocial;
    }

    public String getInstallationChannel() {
        return this.viral;
    }

    public boolean isReinstall() {
        return this.growth;
    }

    public boolean isInstallSuspicious() {
        return this.f491android;
    }

    public String getInstallPlatform() {
        return this.organic;
    }

    public static class Builder {
        private final ReferredUser getsocial = new ReferredUser();

        public Builder(String str) {
            String unused = this.getsocial.attribution = str;
        }

        public Builder setDisplayName(String str) {
            String unused = this.getsocial.acquisition = str;
            return this;
        }

        public Builder setAvatarUrl(String str) {
            String unused = this.getsocial.mobile = str;
            return this;
        }

        public Builder setIdentities(Map<String, String> map) {
            Map unused = this.getsocial.retention = map;
            return this;
        }

        public Builder setPublicProperties(Map<String, String> map) {
            Map unused = this.getsocial.dau = map;
            return this;
        }

        public Builder setInstallationDate(long j) {
            long unused = this.getsocial.getsocial = j;
            return this;
        }

        public Builder setInstallationChannel(String str) {
            String unused = this.getsocial.viral = str;
            return this;
        }

        public Builder setReinstall(boolean z) {
            boolean unused = this.getsocial.growth = z;
            return this;
        }

        public Builder setInstallSuspicious(boolean z) {
            boolean unused = this.getsocial.f491android = z;
            return this;
        }

        public Builder setInstallPlatform(String str) {
            String unused = this.getsocial.organic = str;
            return this;
        }

        public ReferredUser build() {
            ReferredUser referredUser = new ReferredUser();
            this.getsocial.getsocial(referredUser);
            return referredUser;
        }
    }
}
