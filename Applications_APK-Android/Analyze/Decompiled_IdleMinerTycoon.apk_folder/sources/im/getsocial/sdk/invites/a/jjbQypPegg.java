package im.getsocial.sdk.invites.a;

import android.content.Context;
import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.ReferrerDetails;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.rWfbqYooCV;
import java.util.HashMap;

/* compiled from: InstallReferrerServiceHandler */
public class jjbQypPegg implements rWfbqYooCV {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(jjbQypPegg.class);
    /* access modifiers changed from: private */
    public final Context acquisition;
    /* access modifiers changed from: private */
    public InstallReferrerClient attribution;
    /* access modifiers changed from: private */
    public rWfbqYooCV.jjbQypPegg mobile;
    private boolean retention;

    /* renamed from: im.getsocial.sdk.invites.a.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: InstallReferrerServiceHandler */
    private interface C0024jjbQypPegg {
        void getsocial();
    }

    @XdbacJlTDQ
    jjbQypPegg(Context context) {
        this.acquisition = context;
    }

    public final void getsocial(rWfbqYooCV.jjbQypPegg jjbqyppegg) {
        this.mobile = jjbqyppegg;
        getsocial(new C0024jjbQypPegg() {
            public final void getsocial() {
                InstallReferrerClient unused = jjbQypPegg.this.attribution = InstallReferrerClient.newBuilder(jjbQypPegg.this.acquisition.getApplicationContext()).build();
                jjbQypPegg.this.attribution.startConnection(new im.getsocial.sdk.invites.a.f.upgqDBbsrL() {
                    public void onSetupFinished(int i) {
                        jjbQypPegg.getsocial(jjbQypPegg.this, i);
                    }
                });
            }
        });
    }

    private void attribution() {
        if (!this.retention) {
            this.mobile.getsocial(null);
            this.retention = true;
        }
    }

    private void getsocial(C0024jjbQypPegg jjbqyppegg) {
        try {
            jjbqyppegg.getsocial();
        } catch (Throwable th) {
            cjrhisSQCL cjrhissqcl = getsocial;
            cjrhissqcl.attribution("Failed to read ReferrerDetail, error: " + th);
            attribution();
        }
    }

    static /* synthetic */ void getsocial(jjbQypPegg jjbqyppegg, int i) {
        switch (i) {
            case -1:
                return;
            case 0:
                jjbqyppegg.getsocial(new C0024jjbQypPegg() {
                    public final void getsocial() {
                        ReferrerDetails installReferrer = jjbQypPegg.this.attribution.getInstallReferrer();
                        if (installReferrer == null) {
                            jjbQypPegg.getsocial.attribution("No referrer found");
                        } else {
                            String installReferrer2 = installReferrer.getInstallReferrer();
                            if (installReferrer2 != null) {
                                cjrhisSQCL cjrhissqcl = jjbQypPegg.getsocial;
                                cjrhissqcl.attribution("Referrer received from Google Play Service: " + installReferrer2);
                                HashMap hashMap = new HashMap();
                                hashMap.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.getsocial, installReferrer2);
                                hashMap.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.attribution, String.valueOf(installReferrer.getInstallBeginTimestampSeconds()));
                                hashMap.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.acquisition, String.valueOf(installReferrer.getReferrerClickTimestampSeconds()));
                                jjbQypPegg.this.mobile.getsocial(hashMap);
                            }
                        }
                        jjbQypPegg.mobile(jjbQypPegg.this);
                    }
                });
                return;
            case 1:
            case 2:
            case 3:
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("Failed to setup connection with InstallReferrerService, response code:" + i);
                jjbqyppegg.attribution();
                return;
            default:
                cjrhisSQCL cjrhissqcl2 = getsocial;
                cjrhissqcl2.attribution("Unknown response code received: " + i);
                jjbqyppegg.attribution();
                return;
        }
    }

    static /* synthetic */ void mobile(jjbQypPegg jjbqyppegg) {
        if (jjbqyppegg.attribution != null) {
            jjbqyppegg.getsocial(new C0024jjbQypPegg() {
                public final void getsocial() {
                    jjbQypPegg.this.attribution.endConnection();
                }
            });
            jjbqyppegg.attribution = null;
        }
    }
}
