package im.getsocial.sdk.invites.a.k;

import com.ironsource.sdk.constants.Constants;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.k.a.pdwpUtZXDT;
import im.getsocial.sdk.invites.InviteTextPlaceholders;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/* compiled from: InvitesUtil */
public final class ztWNWCuZiM {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(ztWNWCuZiM.class);

    private ztWNWCuZiM() {
    }

    public static String getsocial(String str, String str2, String str3, String str4, LinkParams linkParams, boolean z) {
        String str5;
        if (linkParams != null) {
            for (Map.Entry next : linkParams.getStringValues().entrySet()) {
                str = getsocial(str, Constants.RequestParameters.LEFT_BRACKETS + ((String) next.getKey()).toUpperCase() + Constants.RequestParameters.RIGHT_BRACKETS, (String) next.getValue(), z);
            }
        }
        String str6 = getsocial(getsocial(getsocial(str, "[APP_PACKAGE_NAME]", str2, z), InviteTextPlaceholders.PLACEHOLDER_USER_NAME, str3, z), InviteTextPlaceholders.PLACEHOLDER_APP_INVITE_URL, str4, z);
        if (linkParams == null) {
            str5 = "";
        } else {
            str5 = linkParams.getStringValues().get("$promo_code");
        }
        return getsocial(str6, InviteTextPlaceholders.PLACEHOLDER_PROMO_CODE, str5, z).trim();
    }

    private static String getsocial(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8").replace("-", "%2D");
        } catch (UnsupportedEncodingException e) {
            getsocial.attribution(e);
            return "";
        }
    }

    public static byte[] getsocial(LinkParams linkParams) {
        if (linkParams != null) {
            Object obj = linkParams.get(LinkParams.KEY_CUSTOM_IMAGE);
            if (obj instanceof byte[]) {
                return (byte[]) obj;
            }
        }
        return null;
    }

    public static void getsocial(LinkParams linkParams, pdwpUtZXDT pdwputzxdt) {
        if (linkParams != null && pdwputzxdt != null) {
            linkParams.put(LinkParams.KEY_CUSTOM_IMAGE, pdwputzxdt.getsocial());
        }
    }

    public static void getsocial(pdwpUtZXDT.jjbQypPegg jjbqyppegg, im.getsocial.sdk.internal.k.a.pdwpUtZXDT pdwputzxdt) {
        if (pdwputzxdt != null) {
            if (pdwputzxdt.getsocial() != null) {
                jjbqyppegg.acquisition(pdwputzxdt.getsocial());
            }
            if (pdwputzxdt.attribution() != null) {
                jjbqyppegg.retention(pdwputzxdt.attribution());
            }
            if (pdwputzxdt.acquisition() != null) {
                jjbqyppegg.mobile(pdwputzxdt.acquisition());
            }
        }
    }

    private static String getsocial(String str, String str2, String str3, boolean z) {
        if (str3 == null || str3.trim().isEmpty()) {
            return str;
        }
        if (z) {
            str3 = getsocial(str3);
        }
        return str.replace(str2, str3);
    }
}
