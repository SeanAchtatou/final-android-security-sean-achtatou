package im.getsocial.sdk.invites.a.g;

import im.getsocial.sdk.internal.c.b.KluUZYuxme;
import im.getsocial.sdk.internal.c.b.qdyNCsqjKt;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.ReferralData;
import im.getsocial.sdk.invites.a.pdwpUtZXDT;
import java.util.HashMap;
import java.util.Map;

/* compiled from: InvitesAppRepo */
public class jjbQypPegg implements KluUZYuxme {
    private InviteCallback acquisition;
    private ReferralData attribution;
    private final Map<String, pdwpUtZXDT> getsocial = new HashMap();

    public final pdwpUtZXDT getsocial(String str) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Missing required parameter: inviteChannelId");
        if (this.getsocial.containsKey(str)) {
            return this.getsocial.get(str);
        }
        return this.getsocial.get("generic");
    }

    public final void getsocial(String str, pdwpUtZXDT pdwputzxdt) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Missing required parameter: inviteChannelId");
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(pdwputzxdt), "Missing required parameter: sharedInviteChannelPlugin");
        this.getsocial.put(str, pdwputzxdt);
    }

    public final qdyNCsqjKt attribution() {
        return qdyNCsqjKt.APP;
    }

    public final ReferralData getsocial() {
        return this.attribution;
    }

    public final void getsocial(ReferralData referralData) {
        this.attribution = referralData;
    }

    public final void getsocial(InviteCallback inviteCallback) {
        this.acquisition = inviteCallback;
    }

    public final InviteCallback acquisition() {
        return this.acquisition;
    }
}
