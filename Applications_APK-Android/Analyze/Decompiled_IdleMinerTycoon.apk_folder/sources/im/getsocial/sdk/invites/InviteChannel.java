package im.getsocial.sdk.invites;

import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.b.zoToeBNOjF;
import java.util.Comparator;

public class InviteChannel {
    public static final Comparator<InviteChannel> INVITE_CHANNELS_COMPARATOR_BASED_ON_DISPLAY_ORDER = new Comparator<InviteChannel>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            InviteChannel inviteChannel = (InviteChannel) obj;
            InviteChannel inviteChannel2 = (InviteChannel) obj2;
            if (inviteChannel.getDisplayOrder() == inviteChannel2.getDisplayOrder()) {
                return inviteChannel.getChannelName().compareToIgnoreCase(inviteChannel2.getChannelName());
            }
            return inviteChannel.getDisplayOrder() - inviteChannel2.getDisplayOrder();
        }
    };
    private final String acquisition;
    private final LocalizableText attribution;
    private final zoToeBNOjF dau;
    private final String getsocial;
    private final pdwpUtZXDT mau;
    private final boolean mobile;
    private final int retention;

    InviteChannel(String str, LocalizableText localizableText, String str2, boolean z, int i, zoToeBNOjF zotoebnojf, pdwpUtZXDT pdwputzxdt) {
        this.getsocial = str;
        this.attribution = localizableText;
        this.acquisition = str2;
        this.mobile = z;
        this.retention = i;
        this.dau = zotoebnojf;
        this.mau = pdwputzxdt;
    }

    public String getChannelId() {
        return this.getsocial;
    }

    @Deprecated
    public LocalizableText getName() {
        return this.attribution;
    }

    public String getChannelName() {
        return this.attribution.getLocalisedString();
    }

    public String getIconImageUrl() {
        return this.acquisition;
    }

    public boolean isEnabled() {
        return this.mobile;
    }

    public int getDisplayOrder() {
        return this.retention;
    }

    /* access modifiers changed from: package-private */
    public final zoToeBNOjF getsocial() {
        return this.dau;
    }

    /* access modifiers changed from: package-private */
    public final pdwpUtZXDT attribution() {
        return this.mau;
    }
}
