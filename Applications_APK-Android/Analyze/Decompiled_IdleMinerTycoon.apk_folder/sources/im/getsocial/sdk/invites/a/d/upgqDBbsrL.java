package im.getsocial.sdk.invites.a.d;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.qZypgoeblR;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.rWfbqYooCV;
import im.getsocial.sdk.invites.a.h.jjbQypPegg;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/* compiled from: CheckGooglePlayReferrerFunc */
public final class upgqDBbsrL {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL acquisition = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);
    @XdbacJlTDQ
    jjbQypPegg attribution;
    @XdbacJlTDQ
    @qZypgoeblR
    rWfbqYooCV getsocial;

    private upgqDBbsrL() {
        ztWNWCuZiM.getsocial(this);
    }

    public static void getsocial() {
        upgqDBbsrL upgqdbbsrl = new upgqDBbsrL();
        if (upgqdbbsrl.getsocial != null) {
            acquisition.attribution("It's a first app launch, let's check Google referral data");
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            upgqdbbsrl.getsocial.getsocial(new rWfbqYooCV.jjbQypPegg() {
                public final void getsocial(Map<String, String> map) {
                    upgqDBbsrL.acquisition.getsocial("Got a response from fetchReferrer: %s", map);
                    if (map != null) {
                        upgqDBbsrL.this.attribution.getsocial(im.getsocial.sdk.invites.a.a.cjrhisSQCL.GOOGLE_PLAY, map);
                    }
                    countDownLatch.countDown();
                }
            });
            try {
                countDownLatch.await();
            } catch (InterruptedException unused) {
                acquisition.attribution("Interrupted await");
            }
        }
    }
}
