package im.getsocial.sdk.invites;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.util.HashMap;
import java.util.Map;

public class LocalizableText {
    public static final String NON_LOCALIZED_OVERRIDE = "non_localized_override";
    private static String getsocial;
    private final Map<String, String> attribution;

    LocalizableText(String str) {
        this.attribution = new HashMap();
        this.attribution.put(NON_LOCALIZED_OVERRIDE, str);
    }

    public LocalizableText(Map<String, String> map) {
        this.attribution = map;
    }

    public String getLocalisedString() {
        String str = this.attribution.get(NON_LOCALIZED_OVERRIDE);
        if (jjbQypPegg.acquisition(str) && getsocial != null) {
            str = this.attribution.get(getsocial);
        }
        return jjbQypPegg.acquisition(str) ? this.attribution.get("en") : str;
    }

    static void getsocial(String str) {
        getsocial = str;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> getsocial() {
        return this.attribution;
    }

    public int hashCode() {
        if (this.attribution != null) {
            return this.attribution.hashCode();
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LocalizableText localizableText = (LocalizableText) obj;
        if (this.attribution != null) {
            return this.attribution.equals(localizableText.attribution);
        }
        return localizableText.attribution == null;
    }

    public String toString() {
        return getLocalisedString();
    }
}
