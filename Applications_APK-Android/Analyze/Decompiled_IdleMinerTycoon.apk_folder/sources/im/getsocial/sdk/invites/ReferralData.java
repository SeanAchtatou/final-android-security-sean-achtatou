package im.getsocial.sdk.invites;

import java.util.Map;

public class ReferralData {
    private final String acquisition;
    private final String attribution;
    private final Map<String, String> cat;
    private final boolean dau;
    private final String getsocial;
    private final boolean mau;
    private final boolean mobile;
    private final boolean retention;
    private final Map<String, String> viral;

    ReferralData(String str, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, Map<String, String> map, Map<String, String> map2) {
        this.getsocial = str;
        this.attribution = str2;
        this.mobile = z;
        this.retention = z2;
        this.dau = z3;
        this.mau = z4;
        this.acquisition = str3;
        this.cat = map;
        this.viral = map2;
    }

    public CustomReferralData getOriginalCustomReferralData() {
        return new CustomReferralData(this.viral);
    }

    public LinkParams getOriginalLinkParams() {
        return getsocial(this.viral);
    }

    public Map<String, String> getOriginalReferralLinkParams() {
        return this.viral;
    }

    public CustomReferralData getCustomReferralData() {
        return new CustomReferralData(this.cat);
    }

    public Map<String, String> getReferralLinkParams() {
        return this.cat;
    }

    public LinkParams getLinkParams() {
        return getsocial(this.cat);
    }

    public String getReferrerUserId() {
        return this.attribution;
    }

    public String getReferrerChannelId() {
        return this.acquisition;
    }

    public String getToken() {
        return this.getsocial;
    }

    public boolean isFirstMatch() {
        return this.mobile;
    }

    public boolean isGuaranteedMatch() {
        return this.retention;
    }

    public boolean isReinstall() {
        return this.dau;
    }

    public boolean isFirstMatchLink() {
        return this.mau;
    }

    public String toString() {
        return "ReferralData{" + "token='" + this.getsocial + '\'' + ", referrerUserId='" + this.attribution + '\'' + ", referrerChannelId='" + this.acquisition + '\'' + ", isFirstMatch=" + this.mobile + ", isGuaranteedMatch=" + this.retention + ", isReinstall=" + this.dau + ", isFirstMatchLink=" + this.mau + ", referralLinkParams=" + this.cat + ", referralOriginalLinkParams=" + this.viral + '}';
    }

    private static LinkParams getsocial(Map<String, String> map) {
        LinkParams linkParams = new LinkParams();
        for (Map.Entry next : map.entrySet()) {
            linkParams.put(next.getKey(), next.getValue());
        }
        return linkParams;
    }
}
