package im.getsocial.sdk.invites;

import android.graphics.Bitmap;
import im.getsocial.sdk.media.MediaAttachment;

public class InviteContent {
    /* access modifiers changed from: private */
    public MediaAttachment acquisition;
    /* access modifiers changed from: private */
    public String attribution;
    /* access modifiers changed from: private */
    public String getsocial;

    InviteContent() {
    }

    public static Builder createBuilder() {
        return new Builder();
    }

    public String getSubject() {
        return this.getsocial;
    }

    public String getText() {
        return this.attribution;
    }

    public MediaAttachment getMediaAttachment() {
        return this.acquisition;
    }

    public String toString() {
        return "InviteContent{_subject='" + this.getsocial + '\'' + ", _text='" + this.attribution + '\'' + ", _mediaAttachment=" + this.acquisition + '}';
    }

    public static class Builder {
        InviteContent getsocial = new InviteContent();

        Builder() {
        }

        public Builder withSubject(String str) {
            String unused = this.getsocial.getsocial = str;
            return this;
        }

        public Builder withText(String str) {
            String unused = this.getsocial.attribution = str;
            return this;
        }

        @Deprecated
        public Builder withImageUrl(String str) {
            return withMediaAttachment(MediaAttachment.imageUrl(str));
        }

        @Deprecated
        public Builder withImage(Bitmap bitmap) {
            return withMediaAttachment(MediaAttachment.image(bitmap));
        }

        @Deprecated
        public Builder withVideo(byte[] bArr) {
            return withMediaAttachment(MediaAttachment.video(bArr));
        }

        public Builder withMediaAttachment(MediaAttachment mediaAttachment) {
            if (mediaAttachment != null) {
                MediaAttachment unused = this.getsocial.acquisition = mediaAttachment;
            }
            return this;
        }

        public InviteContent build() {
            InviteContent inviteContent = new InviteContent();
            String unused = inviteContent.getsocial = this.getsocial.getsocial;
            String unused2 = inviteContent.attribution = this.getsocial.attribution;
            MediaAttachment unused3 = inviteContent.acquisition = this.getsocial.acquisition;
            return inviteContent;
        }
    }
}
