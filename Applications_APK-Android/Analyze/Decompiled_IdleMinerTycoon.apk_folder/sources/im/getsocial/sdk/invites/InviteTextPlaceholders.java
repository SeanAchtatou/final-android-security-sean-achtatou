package im.getsocial.sdk.invites;

public final class InviteTextPlaceholders {
    public static final String PLACEHOLDER_APP_INVITE_URL = "[APP_INVITE_URL]";
    public static final String PLACEHOLDER_PROMO_CODE = "[PROMO_CODE]";
    public static final String PLACEHOLDER_USER_NAME = "[USER_NAME]";

    private InviteTextPlaceholders() {
    }
}
