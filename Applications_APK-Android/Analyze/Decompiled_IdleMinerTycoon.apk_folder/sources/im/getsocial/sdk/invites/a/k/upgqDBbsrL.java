package im.getsocial.sdk.invites.a.k;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.invites.InstallReferrerReceiver;
import im.getsocial.sdk.invites.MultipleInstallReferrerReceiver;
import java.util.ArrayList;

/* compiled from: AndroidManifestCheckImpl */
public final class upgqDBbsrL implements jjbQypPegg {
    private static final cjrhisSQCL attribution = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);
    private final Context acquisition;

    @XdbacJlTDQ
    public upgqDBbsrL(Context context) {
        this.acquisition = context;
    }

    public final boolean getsocial() {
        return getsocial(this.acquisition) != null;
    }

    public final int attribution() {
        ArrayList arrayList = new ArrayList();
        for (ResolveInfo next : this.acquisition.getPackageManager().queryBroadcastReceivers(new Intent("com.android.vending.INSTALL_REFERRER"), 0)) {
            if (next.activityInfo.packageName.equals(this.acquisition.getPackageName())) {
                arrayList.add(next.activityInfo.name);
            }
        }
        boolean z = arrayList.size() == 1;
        boolean z2 = arrayList.size() > 1;
        boolean contains = arrayList.contains(InstallReferrerReceiver.class.getName());
        boolean contains2 = arrayList.contains(MultipleInstallReferrerReceiver.class.getName());
        boolean z3 = arrayList.indexOf(InstallReferrerReceiver.class.getName()) == 0 || arrayList.indexOf(MultipleInstallReferrerReceiver.class.getName()) == 0;
        if (!z) {
            if (!z2) {
                attribution.acquisition("GetSocial Analytics will not be correct. %s is not declared in AndroidManifest.xml.", MultipleInstallReferrerReceiver.class.getName());
            } else if (contains2) {
                if (contains) {
                    attribution.acquisition("Only %s has to be defined. Remove %s.", MultipleInstallReferrerReceiver.class.getName(), InstallReferrerReceiver.class.getName());
                } else if (z3) {
                    attribution.acquisition("Correct configuration of INSTALL_REFERRER receivers.");
                } else {
                    attribution.attribution("GetSocial Analytics may not work. %s should be defined as a first INSTALL_REFERRER receiver.", MultipleInstallReferrerReceiver.class.getName());
                    return 0;
                }
            } else if (!contains) {
                attribution.acquisition("GetSocial Analytics will not be correct. %s is not declared in AndroidManifest.xml.", MultipleInstallReferrerReceiver.class.getName());
            } else if (z3) {
                attribution.attribution("Replace %s with %s to deliver INSTALL_REFERRER to other defined receivers.", InstallReferrerReceiver.class.getName(), MultipleInstallReferrerReceiver.class.getName());
                return 0;
            } else {
                attribution.attribution("GetSocial Analytics may not work. Declare %s in AndroidManifest.xml or call %s.onIntentReceived(...) from %s", MultipleInstallReferrerReceiver.class.getName(), InstallReferrerReceiver.class.getName(), arrayList.get(0));
                return 0;
            }
            return -1;
        } else if (contains || contains2) {
            attribution.acquisition("Correct configuration of INSTALL_REFERRER receivers.");
        } else {
            attribution.attribution("GetSocial Analytics may not work. Declare %s in AndroidManifest.xml or call %s.onIntentReceived(...) from %s", MultipleInstallReferrerReceiver.class.getName(), InstallReferrerReceiver.class.getName(), arrayList.get(0));
            return 0;
        }
        return 1;
    }

    public static ProviderInfo getsocial(Context context) {
        try {
            ProviderInfo[] providerInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 8).providers;
            if (providerInfoArr == null) {
                return null;
            }
            for (ProviderInfo providerInfo : providerInfoArr) {
                if (getsocial.equals(providerInfo.name)) {
                    return providerInfo;
                }
            }
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            attribution.getsocial("Failed to check if %s is declared in the AndroidManifest, error: %s", getsocial, e.getMessage());
            return null;
        }
    }
}
