package im.getsocial.sdk.invites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import im.getsocial.a.a.pdwpUtZXDT;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.wWemqSpYTx;
import java.util.HashMap;

public class InstallReferrerReceiver extends BroadcastReceiver {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(InstallReferrerReceiver.class);

    public void onReceive(Context context, Intent intent) {
        getsocial.attribution("InstallReferrerReceiver invoked.");
        onIntentReceived(context, intent);
    }

    public static void onIntentReceived(Context context, Intent intent) {
        String str;
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras == null) {
                str = null;
            } else {
                str = extras.getString("referrer");
            }
            if (str == null) {
                getsocial.attribution("No referrer found");
                return;
            }
            cjrhisSQCL cjrhissqcl = getsocial;
            cjrhissqcl.attribution("Referrer received from Google Play: " + str);
            HashMap hashMap = new HashMap();
            hashMap.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.getsocial, str);
            try {
                new im.getsocial.sdk.invites.a.cjrhisSQCL().getsocial(im.getsocial.sdk.invites.a.a.cjrhisSQCL.GOOGLE_PLAY, hashMap);
            } catch (RuntimeException unused) {
                new wWemqSpYTx(context).getsocial("google_referrer", pdwpUtZXDT.getsocial(new pdwpUtZXDT(hashMap)));
            }
        }
    }
}
