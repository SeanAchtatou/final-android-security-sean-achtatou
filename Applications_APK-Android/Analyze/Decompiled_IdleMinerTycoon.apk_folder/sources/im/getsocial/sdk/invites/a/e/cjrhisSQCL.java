package im.getsocial.sdk.invites.a.e;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.google.android.gms.drive.DriveFile;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;

/* compiled from: FacebookMessengerInvitePlugin */
public class cjrhisSQCL extends InviteChannelPlugin {
    private static boolean getsocial(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.orca", 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public boolean isAvailableForDevice(InviteChannel inviteChannel) {
        return getsocial(getContext());
    }

    public void presentChannelInterface(InviteChannel inviteChannel, InvitePackage invitePackage, InviteCallback inviteCallback) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.setPackage("com.facebook.orca");
            intent.putExtra("android.intent.extra.TEXT", invitePackage.getReferralUrl());
            Intent createChooser = Intent.createChooser(intent, "Share");
            createChooser.setFlags(DriveFile.MODE_READ_ONLY);
            getContext().startActivity(createChooser);
            inviteCallback.onComplete();
        } catch (Exception e) {
            inviteCallback.onError(e);
        }
    }
}
