package im.getsocial.sdk.invites.a.h;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import im.getsocial.a.a.pdwpUtZXDT;
import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.a.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.KCGqEGAizh;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.invites.a.a.upgqDBbsrL;
import im.getsocial.sdk.invites.a.j.zoToeBNOjF;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ReferrerStore */
public class jjbQypPegg {
    private final qdyNCsqjKt acquisition;
    private final im.getsocial.sdk.internal.a.g.jjbQypPegg attribution;
    private final KCGqEGAizh getsocial;
    private final cjrhisSQCL mobile;

    @XdbacJlTDQ
    jjbQypPegg(KCGqEGAizh kCGqEGAizh, im.getsocial.sdk.internal.a.g.jjbQypPegg jjbqyppegg, qdyNCsqjKt qdyncsqjkt, cjrhisSQCL cjrhissqcl) {
        this.getsocial = kCGqEGAizh;
        this.attribution = jjbqyppegg;
        this.acquisition = qdyncsqjkt;
        this.mobile = cjrhissqcl;
    }

    public final void getsocial(im.getsocial.sdk.invites.a.a.cjrhisSQCL cjrhissqcl, Map<String, String> map) {
        getsocial(cjrhissqcl, map, new CompletionCallback() {
            public void onFailure(GetSocialException getSocialException) {
            }

            public void onSuccess() {
            }
        });
    }

    public final void getsocial(im.getsocial.sdk.invites.a.a.cjrhisSQCL cjrhissqcl, Map<String, String> map, CompletionCallback completionCallback) {
        boolean z;
        String str;
        boolean z2 = this.getsocial.getsocial();
        if (!z2 || cjrhissqcl != im.getsocial.sdk.invites.a.a.cjrhisSQCL.DEEP_LINK) {
            if (z2) {
                HashMap hashMap = new HashMap();
                hashMap.put("source", cjrhissqcl.name());
                hashMap.put("content", map.get(upgqDBbsrL.getsocial));
                hashMap.put(IronSourceConstants.EVENTS_DURATION, String.valueOf(this.mobile.attribution() - this.acquisition.acquisition("application_did_become_active_event_timestamp")));
                this.attribution.getsocial("install_referrer_received_after_init", hashMap);
            } else {
                qdyNCsqjKt qdyncsqjkt = this.acquisition;
                switch (cjrhissqcl) {
                    case FACEBOOK:
                        str = "facebook_referrer";
                        break;
                    case GOOGLE_PLAY:
                        str = "google_referrer";
                        break;
                    case DEEP_LINK:
                        str = "deep_link_referrer";
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
                qdyncsqjkt.getsocial(str, pdwpUtZXDT.getsocial(new pdwpUtZXDT(map)));
            }
            z = false;
        } else {
            z = true;
        }
        if (z) {
            new zoToeBNOjF().getsocial(map.get(upgqDBbsrL.getsocial), completionCallback);
        } else {
            completionCallback.onSuccess();
        }
    }
}
