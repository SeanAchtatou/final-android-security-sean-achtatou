package im.getsocial.sdk.invites.a.i;

import im.getsocial.sdk.internal.c.KluUZYuxme;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.ztWNWCuZiM;
import im.getsocial.sdk.internal.f.a.FvojpKUsoc;
import im.getsocial.sdk.internal.f.a.JWvbLzaedN;
import im.getsocial.sdk.internal.f.a.QhisXzMgay;
import im.getsocial.sdk.internal.f.a.SKUqohGtGQ;
import im.getsocial.sdk.internal.f.a.fOrCGNYyfk;
import im.getsocial.sdk.internal.f.a.lgAVmIefSo;
import im.getsocial.sdk.internal.f.a.qdyNCsqjKt;
import im.getsocial.sdk.internal.f.a.sqEuGXwfLT;
import im.getsocial.sdk.internal.f.a.vkXhnjhKGp;
import im.getsocial.sdk.internal.f.a.wWemqSpYTx;
import im.getsocial.sdk.internal.m.QCXFOjcJkE;
import im.getsocial.sdk.invites.LocalizableText;
import im.getsocial.sdk.invites.ReferralData;
import im.getsocial.sdk.invites.ReferredUser;
import im.getsocial.sdk.invites.a.a.cjrhisSQCL;
import im.getsocial.sdk.invites.a.b.XdbacJlTDQ;
import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.b.upgqDBbsrL;
import im.getsocial.sdk.invites.a.b.zoToeBNOjF;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ThriftyInvitesConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static upgqDBbsrL getsocial(JWvbLzaedN jWvbLzaedN) {
        pdwpUtZXDT pdwputzxdt = getsocial(jWvbLzaedN.getsocial);
        List<SKUqohGtGQ> list = jWvbLzaedN.attribution;
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) list), "Unexpected response from the API. Providers list is empty");
        ArrayList arrayList = new ArrayList(list.size());
        for (SKUqohGtGQ next : list) {
            arrayList.add(im.getsocial.sdk.invites.jjbQypPegg.getsocial(next.attribution, new LocalizableText(next.getsocial), next.retention, QCXFOjcJkE.getsocial(next.dau), QCXFOjcJkE.getsocial(next.mobile), getsocial(next.organic), getsocial(next.growth)));
        }
        return new upgqDBbsrL(pdwputzxdt, arrayList);
    }

    public static XdbacJlTDQ getsocial(sqEuGXwfLT sqeugxwflt) {
        return new XdbacJlTDQ(sqeugxwflt.getsocial, sqeugxwflt.attribution);
    }

    private static pdwpUtZXDT getsocial(QhisXzMgay qhisXzMgay) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(qhisXzMgay), "Can not convert null THInviteContent.");
        pdwpUtZXDT.jjbQypPegg jjbqyppegg = pdwpUtZXDT.getsocial();
        if (acquisition(qhisXzMgay.acquisition)) {
            jjbqyppegg.attribution(new LocalizableText(new HashMap(qhisXzMgay.acquisition)));
        }
        if (acquisition(qhisXzMgay.attribution)) {
            jjbqyppegg.getsocial(new LocalizableText(new HashMap(qhisXzMgay.attribution)));
        }
        if (!ztWNWCuZiM.getsocial(qhisXzMgay.getsocial)) {
            jjbqyppegg.acquisition(qhisXzMgay.getsocial);
        }
        if (!ztWNWCuZiM.getsocial(qhisXzMgay.mobile)) {
            jjbqyppegg.retention(qhisXzMgay.mobile);
        }
        if (!ztWNWCuZiM.getsocial(qhisXzMgay.retention)) {
            jjbqyppegg.mobile(qhisXzMgay.retention);
        }
        return jjbqyppegg.getsocial();
    }

    private static zoToeBNOjF getsocial(vkXhnjhKGp vkxhnjhkgp) {
        im.getsocial.sdk.invites.a.b.jjbQypPegg jjbqyppegg;
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(vkxhnjhkgp), "Can not convert null THInviteProperties.");
        ArrayList arrayList = new ArrayList();
        if (vkxhnjhkgp.attribution != null) {
            Iterator<qdyNCsqjKt> it = vkxhnjhkgp.attribution.iterator();
            while (it.hasNext()) {
                switch (it.next()) {
                    case ExtraSubject:
                        jjbqyppegg = im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_SUBJECT;
                        break;
                    case ExtraText:
                        jjbqyppegg = im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_TEXT;
                        break;
                    case ExtraStream:
                        jjbqyppegg = im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_STREAM;
                        break;
                    case ExtraGif:
                        jjbqyppegg = im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_GIF;
                        break;
                    case ExtraVideo:
                        jjbqyppegg = im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_VIDEO;
                        break;
                    default:
                        jjbqyppegg = null;
                        break;
                }
                arrayList.add(jjbqyppegg);
            }
        }
        return new zoToeBNOjF(vkxhnjhkgp.getsocial, arrayList, vkxhnjhkgp.acquisition, vkxhnjhkgp.mobile, vkxhnjhkgp.retention, vkxhnjhkgp.dau, vkxhnjhkgp.cat);
    }

    public static wWemqSpYTx getsocial(KluUZYuxme kluUZYuxme) {
        wWemqSpYTx wwemqspytx = new wWemqSpYTx();
        wwemqspytx.mobile = kluUZYuxme.mobile();
        wwemqspytx.retention = kluUZYuxme.retention();
        wwemqspytx.attribution = String.valueOf(kluUZYuxme.getsocial());
        wwemqspytx.getsocial = String.valueOf(kluUZYuxme.attribution());
        wwemqspytx.acquisition = String.valueOf(kluUZYuxme.acquisition());
        wwemqspytx.mau = kluUZYuxme.mau();
        wwemqspytx.dau = kluUZYuxme.dau();
        return wwemqspytx;
    }

    public static Map<im.getsocial.sdk.internal.f.a.KluUZYuxme, Map<fOrCGNYyfk, String>> getsocial(Map<cjrhisSQCL, Map<String, String>> map) {
        im.getsocial.sdk.internal.f.a.KluUZYuxme kluUZYuxme;
        HashMap hashMap = new HashMap();
        for (Map.Entry key : map.entrySet()) {
            switch ((cjrhisSQCL) key.getKey()) {
                case GOOGLE_PLAY:
                    kluUZYuxme = im.getsocial.sdk.internal.f.a.KluUZYuxme.Google;
                    break;
                case FACEBOOK:
                    kluUZYuxme = im.getsocial.sdk.internal.f.a.KluUZYuxme.Facebook;
                    break;
                case DEEP_LINK:
                    kluUZYuxme = im.getsocial.sdk.internal.f.a.KluUZYuxme.DeepLink;
                    break;
                default:
                    kluUZYuxme = null;
                    break;
            }
            hashMap.put(kluUZYuxme, attribution((Map) key.getValue()));
        }
        return hashMap;
    }

    public static ReferralData getsocial(lgAVmIefSo lgavmiefso) {
        if (lgavmiefso == null || lgavmiefso.retention == null || !lgavmiefso.retention.containsKey("$token")) {
            return null;
        }
        HashMap hashMap = new HashMap(lgavmiefso.retention);
        HashMap hashMap2 = new HashMap();
        if (lgavmiefso.cat != null) {
            hashMap2.putAll(lgavmiefso.cat);
        }
        boolean parseBoolean = Boolean.parseBoolean(lgavmiefso.retention.get("$first_match"));
        boolean parseBoolean2 = Boolean.parseBoolean(lgavmiefso.retention.get("$guaranteed_match"));
        boolean parseBoolean3 = Boolean.parseBoolean(lgavmiefso.retention.get("$reinstall"));
        boolean parseBoolean4 = Boolean.parseBoolean(lgavmiefso.retention.get("$first_match_link"));
        return im.getsocial.sdk.invites.jjbQypPegg.getsocial(lgavmiefso.retention.get("$token"), lgavmiefso.retention.get("$referrer_user_guid"), lgavmiefso.retention.get("$channel"), parseBoolean, parseBoolean2, parseBoolean3, parseBoolean4, hashMap, hashMap2);
    }

    public static ReferredUser getsocial(FvojpKUsoc fvojpKUsoc) {
        ReferredUser build = new ReferredUser.Builder(fvojpKUsoc.getsocial).setAvatarUrl(fvojpKUsoc.acquisition).setDisplayName(fvojpKUsoc.attribution).setReinstall(QCXFOjcJkE.getsocial(fvojpKUsoc.organic)).setInstallPlatform(fvojpKUsoc.f482android).setInstallSuspicious(QCXFOjcJkE.getsocial(fvojpKUsoc.growth)).setDisplayName(fvojpKUsoc.attribution).setIdentities(im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(fvojpKUsoc.mobile)).setPublicProperties(fvojpKUsoc.retention).setInstallationDate(QCXFOjcJkE.getsocial(fvojpKUsoc.mau)).setInstallationChannel(fvojpKUsoc.cat).build();
        im.getsocial.sdk.usermanagement.pdwpUtZXDT.getsocial(build, fvojpKUsoc.dau);
        return build;
    }

    private static Map<fOrCGNYyfk, String> attribution(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : map.entrySet()) {
            hashMap.put(fOrCGNYyfk.valueOf((String) next.getKey()), next.getValue());
        }
        return hashMap;
    }

    private static boolean acquisition(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        for (String str : map.values()) {
            if (!ztWNWCuZiM.getsocial(str)) {
                return true;
            }
        }
        return false;
    }
}
