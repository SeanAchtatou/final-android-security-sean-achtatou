package im.getsocial.sdk.invites;

public class InviteChannelIds {
    public static final String EMAIL = "email";
    public static final String FACEBOOK = "facebook";
    public static final String FACEBOOK_MESSENGER = "facebookmessenger";
    public static final String FACEBOOK_STORIES = "facebookstories";
    public static final String GENERIC = "generic";
    @Deprecated
    public static final String GOOGLE_PLUS = "googleplus";
    public static final String HANGOUTS = "hangouts";
    public static final String INSTAGRAM_DIRECT = "instagram";
    public static final String INSTAGRAM_STORIES = "instagramstories";
    public static final String KAKAO = "kakao";
    public static final String KIK = "kik";
    public static final String LINE = "line";
    public static final String NATIVE_SHARE = "nativeshare";
    public static final String SMS = "sms";
    public static final String SNAPCHAT = "snapchat";
    public static final String TELEGRAM = "telegram";
    public static final String TWITTER = "twitter";
    public static final String VIBER = "viber";
    public static final String VK = "vk";
    public static final String WHATSAPP = "whatsapp";
}
