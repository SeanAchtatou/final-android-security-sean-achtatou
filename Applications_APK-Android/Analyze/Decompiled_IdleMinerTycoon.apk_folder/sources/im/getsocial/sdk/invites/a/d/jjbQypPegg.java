package im.getsocial.sdk.invites.a.d;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.invites.a.k.XdbacJlTDQ;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

/* compiled from: CheckFacebookReferrerFunc */
public final class jjbQypPegg {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL acquisition = upgqDBbsrL.getsocial(jjbQypPegg.class);
    @XdbacJlTDQ
    im.getsocial.sdk.invites.a.h.jjbQypPegg attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.invites.a.k.XdbacJlTDQ getsocial;

    private jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    public static void getsocial() {
        jjbQypPegg jjbqyppegg = new jjbQypPegg();
        acquisition.attribution("It's a first app launch, let's check FB referral data");
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        jjbqyppegg.getsocial.getsocial(new XdbacJlTDQ.jjbQypPegg() {
            public final void getsocial(String str) {
                jjbQypPegg.acquisition.getsocial("Got a response from fetchDeferredAppLinkData: %s", str);
                if (!im.getsocial.sdk.internal.c.l.ztWNWCuZiM.getsocial(str)) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.getsocial, str);
                    jjbQypPegg.this.attribution.getsocial(im.getsocial.sdk.invites.a.a.cjrhisSQCL.FACEBOOK, hashMap);
                }
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
            acquisition.attribution("Interrupted await");
        }
    }
}
