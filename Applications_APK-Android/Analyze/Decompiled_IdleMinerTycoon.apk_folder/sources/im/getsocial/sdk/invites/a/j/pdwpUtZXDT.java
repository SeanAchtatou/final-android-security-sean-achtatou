package im.getsocial.sdk.invites.a.j;

import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.k.upgqDBbsrL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.invites.InviteChannelIds;
import im.getsocial.sdk.invites.a.g.jjbQypPegg;

/* compiled from: RegisterInviteChannelPluginUseCase */
public final class pdwpUtZXDT implements upgqDBbsrL {
    private static final String[] attribution = {"generic", "email", "facebook", InviteChannelIds.HANGOUTS, InviteChannelIds.INSTAGRAM_DIRECT, InviteChannelIds.KAKAO, InviteChannelIds.KIK, InviteChannelIds.LINE, InviteChannelIds.FACEBOOK_MESSENGER, InviteChannelIds.NATIVE_SHARE, InviteChannelIds.SNAPCHAT, InviteChannelIds.SMS, InviteChannelIds.TWITTER, InviteChannelIds.TELEGRAM, InviteChannelIds.VIBER, InviteChannelIds.VK, InviteChannelIds.WHATSAPP, "googleplus", InviteChannelIds.INSTAGRAM_STORIES, InviteChannelIds.FACEBOOK_STORIES};
    @XdbacJlTDQ
    jjbQypPegg getsocial;

    public pdwpUtZXDT() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(String str, im.getsocial.sdk.invites.a.pdwpUtZXDT pdwputzxdt) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial((Object) str), "Channel ID should not be null");
        String[] strArr = attribution;
        int i = 0;
        while (i < 20) {
            if (!strArr[i].equalsIgnoreCase(str)) {
                i++;
            } else {
                this.getsocial.getsocial(str, pdwputzxdt);
                return;
            }
        }
        throw new IllegalArgumentException("Channel ID " + str + " is not valid. Check all valid Channel IDs.");
    }
}
