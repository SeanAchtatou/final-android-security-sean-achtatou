package im.getsocial.sdk.invites;

import android.content.Context;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;

public abstract class InviteChannelPlugin {
    @XdbacJlTDQ
    Context getsocial;

    public abstract boolean isAvailableForDevice(InviteChannel inviteChannel);

    public abstract void presentChannelInterface(InviteChannel inviteChannel, InvitePackage invitePackage, InviteCallback inviteCallback);

    protected InviteChannelPlugin() {
        ztWNWCuZiM.getsocial(this);
    }

    public Context getContext() {
        return this.getsocial;
    }
}
