package im.getsocial.sdk.invites.a.b;

import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.LocalizableText;
import im.getsocial.sdk.invites.cjrhisSQCL;

/* compiled from: InviteContentInternal */
public class pdwpUtZXDT {
    private final LocalizableText acquisition;
    private final byte[] attribution;
    private final LinkParams cat;
    private final String dau;
    private final String getsocial;
    private final byte[] mau;
    private final LocalizableText mobile;
    private final String retention;

    pdwpUtZXDT(LocalizableText localizableText, LocalizableText localizableText2, String str, byte[] bArr, String str2, String str3, byte[] bArr2, LinkParams linkParams) {
        this.getsocial = str;
        this.attribution = bArr;
        this.acquisition = localizableText;
        this.mobile = localizableText2;
        this.retention = str2;
        this.dau = str3;
        this.mau = bArr2;
        this.cat = linkParams;
    }

    public static jjbQypPegg getsocial() {
        return new jjbQypPegg();
    }

    public final boolean attribution() {
        return "-".equals(this.getsocial);
    }

    public final LocalizableText acquisition() {
        return this.acquisition;
    }

    public final LocalizableText mobile() {
        return this.mobile;
    }

    public final String retention() {
        return this.getsocial;
    }

    public final byte[] dau() {
        if (this.attribution == null) {
            return null;
        }
        return (byte[]) this.attribution.clone();
    }

    public final String mau() {
        return this.retention;
    }

    public final String cat() {
        return this.dau;
    }

    public final LinkParams viral() {
        return this.cat;
    }

    public final byte[] organic() {
        if (this.mau == null) {
            return null;
        }
        return (byte[]) this.mau.clone();
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.getsocial != null ? this.getsocial.hashCode() : 0) * 31) + (this.acquisition != null ? this.acquisition.hashCode() : 0)) * 31) + (this.mobile != null ? this.mobile.hashCode() : 0)) * 31) + (this.retention != null ? this.retention.hashCode() : 0)) * 31;
        if (this.dau != null) {
            i = this.dau.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        pdwpUtZXDT pdwputzxdt = (pdwpUtZXDT) obj;
        if (this.retention == null ? pdwputzxdt.retention != null : !this.retention.equals(pdwputzxdt.retention)) {
            return false;
        }
        if (this.dau == null ? pdwputzxdt.dau != null : !this.dau.equals(pdwputzxdt.dau)) {
            return false;
        }
        if (this.getsocial == null ? pdwputzxdt.getsocial != null : !this.getsocial.equals(pdwputzxdt.getsocial)) {
            return false;
        }
        if (this.acquisition == null ? pdwputzxdt.acquisition != null : !this.acquisition.equals(pdwputzxdt.acquisition)) {
            return false;
        }
        if (this.mobile != null) {
            return this.mobile.equals(pdwputzxdt.mobile);
        }
        return pdwputzxdt.mobile == null;
    }

    /* compiled from: InviteContentInternal */
    public static class jjbQypPegg {
        private LocalizableText acquisition;
        private String attribution;
        private LinkParams cat;
        private String dau;
        private byte[] getsocial;
        private byte[] mau;
        private LocalizableText mobile;
        private String retention;

        jjbQypPegg() {
        }

        public final jjbQypPegg getsocial(String str) {
            this.acquisition = cjrhisSQCL.getsocial(str);
            return this;
        }

        public final jjbQypPegg getsocial(LocalizableText localizableText) {
            this.acquisition = localizableText;
            return this;
        }

        public final jjbQypPegg attribution(String str) {
            this.mobile = cjrhisSQCL.getsocial(str);
            return this;
        }

        public final jjbQypPegg attribution(LocalizableText localizableText) {
            this.mobile = localizableText;
            return this;
        }

        public final jjbQypPegg getsocial(byte[] bArr) {
            if (bArr != null) {
                this.getsocial = (byte[]) bArr.clone();
            }
            return this;
        }

        public final jjbQypPegg acquisition(String str) {
            this.attribution = str;
            return this;
        }

        public final jjbQypPegg mobile(String str) {
            this.retention = str;
            return this;
        }

        public final jjbQypPegg retention(String str) {
            this.dau = str;
            return this;
        }

        public final jjbQypPegg getsocial(LinkParams linkParams) {
            this.cat = linkParams;
            return this;
        }

        public final jjbQypPegg attribution(byte[] bArr) {
            if (bArr != null) {
                this.mau = (byte[]) bArr.clone();
            }
            return this;
        }

        public final pdwpUtZXDT getsocial() {
            return new pdwpUtZXDT(this.acquisition, this.mobile, this.attribution, this.getsocial, this.retention, this.dau, this.mau, this.cat);
        }
    }
}
