package im.getsocial.sdk.invites;

public final class InstallPlatform {
    public static final String ANDROID = "Android";
    public static final String API = "API";
    public static final String DESKTOP_LINUX = "Desktop_Linux";
    public static final String DESKTOP_MAC = "Desktop_Mac";
    public static final String DESKTOP_WINDOWS = "Desktop_Windows";
    public static final String IOS = "iOS";
    public static final String OTHER = "Other";
    public static final String WEB_ANDROID = "Web_Android";
    public static final String WEB_DESKTOP = "Web_Desktop";
    public static final String WEB_IOS = "Web_iOS";
}
