package im.getsocial.sdk.invites;

import im.getsocial.sdk.invites.a.b.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.b.zoToeBNOjF;
import java.util.Map;

/* compiled from: AccessHelper */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static void getsocial(String str) {
        LocalizableText.getsocial(str);
    }

    public static ReferralData getsocial(String str, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, Map<String, String> map, Map<String, String> map2) {
        return new ReferralData(str, str2, str3, z, z2, z3, z4, map, map2);
    }

    public static InviteChannel getsocial(String str, LocalizableText localizableText, String str2, boolean z, int i, zoToeBNOjF zotoebnojf, pdwpUtZXDT pdwputzxdt) {
        return new InviteChannel(str, localizableText, str2, z, i, zotoebnojf, pdwputzxdt);
    }
}
