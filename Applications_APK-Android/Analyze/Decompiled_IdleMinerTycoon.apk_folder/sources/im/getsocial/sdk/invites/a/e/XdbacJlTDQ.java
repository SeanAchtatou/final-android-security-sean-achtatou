package im.getsocial.sdk.invites.a.e;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import im.getsocial.sdk.internal.m.iFpupLCESp;
import im.getsocial.sdk.internal.m.jMsobIMeui;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelIds;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;
import im.getsocial.sdk.invites.a.b.zoToeBNOjF;
import im.getsocial.sdk.invites.a.k.ztWNWCuZiM;
import java.util.HashMap;

/* compiled from: GenericInviteChannelPlugin */
public class XdbacJlTDQ extends InviteChannelPlugin {

    /* compiled from: GenericInviteChannelPlugin */
    private interface upgqDBbsrL {
        void getsocial(Intent intent);
    }

    public boolean isAvailableForDevice(InviteChannel inviteChannel) {
        String mobile = im.getsocial.sdk.invites.upgqDBbsrL.getsocial(inviteChannel).mobile();
        if (TextUtils.isEmpty(mobile)) {
            return false;
        }
        if (mobile.equals(InviteChannelIds.NATIVE_SHARE)) {
            return true;
        }
        if (!jMsobIMeui.getsocial(getContext(), mobile) || !jMsobIMeui.attribution(getContext(), mobile)) {
            return false;
        }
        return true;
    }

    public void presentChannelInterface(InviteChannel inviteChannel, InvitePackage invitePackage, final InviteCallback inviteCallback) {
        if (!(inviteCallback instanceof im.getsocial.sdk.invites.a.upgqDBbsrL) || !inviteChannel.getChannelId().equalsIgnoreCase(InviteChannelIds.NATIVE_SHARE)) {
            try {
                getsocial(getContext(), im.getsocial.sdk.invites.upgqDBbsrL.getsocial(inviteChannel), invitePackage, new upgqDBbsrL() {
                    public final void getsocial(Intent intent) {
                        try {
                            XdbacJlTDQ.this.getContext().startActivity(intent);
                            inviteCallback.onComplete();
                        } catch (Exception e) {
                            inviteCallback.onError(e);
                        }
                    }
                });
            } catch (Exception e) {
                inviteCallback.onError(e);
            }
        } else {
            final im.getsocial.sdk.invites.a.upgqDBbsrL upgqdbbsrl = (im.getsocial.sdk.invites.a.upgqDBbsrL) inviteCallback;
            try {
                getsocial(getContext(), im.getsocial.sdk.invites.upgqDBbsrL.getsocial(inviteChannel), invitePackage, new upgqDBbsrL() {
                    public final void getsocial(Intent intent) {
                        try {
                            if (Build.VERSION.SDK_INT >= 22) {
                                Intent createChooser = Intent.createChooser(intent, null, PendingIntent.getBroadcast(XdbacJlTDQ.this.getContext(), 0, intent, 134217728).getIntentSender());
                                IntentFilter intentFilter = new IntentFilter(intent.getAction(), intent.getType());
                                createChooser.addFlags(335544320);
                                XdbacJlTDQ.this.getContext().registerReceiver(new jjbQypPegg(upgqdbbsrl), intentFilter);
                                XdbacJlTDQ.this.getContext().startActivity(createChooser);
                                return;
                            }
                            XdbacJlTDQ.this.getContext().startActivity(intent);
                            upgqdbbsrl.onComplete();
                        } catch (Exception e) {
                            upgqdbbsrl.onError(e);
                        }
                    }
                });
            } catch (Exception e2) {
                upgqdbbsrl.onError(e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri
     arg types: [android.content.Context, android.graphics.Bitmap, int, java.lang.String]
     candidates:
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, java.lang.String, java.lang.String, im.getsocial.sdk.internal.m.iFpupLCESp$jjbQypPegg):void
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri */
    private void getsocial(Context context, zoToeBNOjF zotoebnojf, InvitePackage invitePackage, final upgqDBbsrL upgqdbbsrl) {
        String str = zotoebnojf.getsocial();
        String mobile = zotoebnojf.mobile();
        String retention = zotoebnojf.retention();
        String acquisition = zotoebnojf.acquisition();
        String dau = zotoebnojf.dau();
        final Intent intent = str == null ? new Intent() : new Intent(str);
        intent.addFlags(335544320);
        if (!TextUtils.isEmpty(mobile) && !mobile.equals(InviteChannelIds.NATIVE_SHARE)) {
            intent.setPackage(mobile);
        }
        if (!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(retention)) {
            intent.setClassName(mobile, retention);
        }
        if (!TextUtils.isEmpty(acquisition)) {
            intent.setType(acquisition);
        }
        if (zotoebnojf.attribution().contains(im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_SUBJECT)) {
            intent.putExtra("android.intent.extra.SUBJECT", invitePackage.getSubject());
        }
        if (zotoebnojf.attribution().contains(im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_TEXT)) {
            intent.putExtra("android.intent.extra.TEXT", invitePackage.getText());
        }
        if (!TextUtils.isEmpty(dau)) {
            intent.setData(Uri.parse(ztWNWCuZiM.getsocial(dau.replace("[APP_INVITE_SUBJECT]", iFpupLCESp.getsocial(invitePackage.getSubject())).replace("[APP_INVITE_TEXT]", iFpupLCESp.getsocial(invitePackage.getText())), mobile, invitePackage.getUserName(), invitePackage.getReferralUrl(), invitePackage.getLinkParams(), true)));
        }
        if (zotoebnojf.attribution().contains(im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_STREAM)) {
            boolean contains = zotoebnojf.attribution().contains(im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_VIDEO);
            boolean contains2 = zotoebnojf.attribution().contains(im.getsocial.sdk.invites.a.b.jjbQypPegg.EXTRA_GIF);
            boolean z = true;
            boolean z2 = invitePackage.getVideoUrl() != null;
            boolean z3 = invitePackage.getGifUrl() != null;
            if (invitePackage.getImage() == null) {
                z = false;
            }
            AnonymousClass3 r6 = new iFpupLCESp.cjrhisSQCL() {
                public final void getsocial(Uri uri) {
                    if (uri != null) {
                        intent.putExtra("android.intent.extra.STREAM", uri);
                    }
                    upgqdbbsrl.getsocial(intent);
                }
            };
            if (z2 && contains) {
                intent.setType("video/mp4");
                iFpupLCESp.getsocial(context, invitePackage.getVideoUrl(), r6);
            } else if (z3 && contains2) {
                intent.setType("image/gif");
                iFpupLCESp.getsocial(context, invitePackage.getGifUrl(), r6);
            } else if (z) {
                intent.setType("image/jpeg");
                r6.getsocial(iFpupLCESp.getsocial(context, invitePackage.getImage(), false, "getsocial-smartinvite-tempimage.jpg"));
            } else {
                upgqdbbsrl.getsocial(intent);
            }
        } else {
            upgqdbbsrl.getsocial(intent);
        }
    }

    @TargetApi(22)
    /* compiled from: GenericInviteChannelPlugin */
    private static class jjbQypPegg extends BroadcastReceiver {
        im.getsocial.sdk.invites.a.upgqDBbsrL getsocial;

        jjbQypPegg(im.getsocial.sdk.invites.a.upgqDBbsrL upgqdbbsrl) {
            this.getsocial = upgqdbbsrl;
        }

        public void onReceive(Context context, Intent intent) {
            HashMap hashMap = new HashMap();
            hashMap.put("sub_provider", ((ComponentName) intent.getExtras().getParcelable("android.intent.extra.CHOSEN_COMPONENT")).flattenToShortString());
            this.getsocial.getsocial(hashMap);
            context.unregisterReceiver(this);
        }
    }
}
