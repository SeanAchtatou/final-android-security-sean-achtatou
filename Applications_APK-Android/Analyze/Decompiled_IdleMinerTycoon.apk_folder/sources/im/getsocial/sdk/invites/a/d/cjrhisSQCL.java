package im.getsocial.sdk.invites.a.d;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.activities.MentionTypes;
import im.getsocial.sdk.internal.a.g.jjbQypPegg;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.a.b.zoToeBNOjF;
import im.getsocial.sdk.invites.a.pdwpUtZXDT;
import im.getsocial.sdk.invites.upgqDBbsrL;
import java.util.HashMap;
import java.util.Map;

/* compiled from: InvokeInviteChannelPluginFunc */
public class cjrhisSQCL {
    @XdbacJlTDQ
    jjbQypPegg acquisition;
    @XdbacJlTDQ
    im.getsocial.sdk.invites.a.g.jjbQypPegg attribution;
    @XdbacJlTDQ
    im.getsocial.sdk.usermanagement.a.c.jjbQypPegg getsocial;

    public cjrhisSQCL() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(final InviteChannel inviteChannel, im.getsocial.sdk.invites.a.b.cjrhisSQCL cjrhissqcl, final CompletionCallback completionCallback) {
        String str;
        String str2;
        String str3;
        String str4;
        byte[] bArr;
        pdwpUtZXDT pdwputzxdt = this.attribution.getsocial(inviteChannel.getChannelId());
        if (!pdwputzxdt.getsocial(inviteChannel)) {
            completionCallback.onFailure(new GetSocialException(204, "Invite channel [" + inviteChannel.getChannelId() + "] is not available."));
            return;
        }
        zoToeBNOjF zotoebnojf = upgqDBbsrL.getsocial(inviteChannel);
        if (zotoebnojf == null) {
            str = "";
        } else {
            str = zotoebnojf.mobile();
        }
        if (this.getsocial.getsocial().getDisplayName() == null) {
            str2 = "";
        } else {
            str2 = this.getsocial.getsocial().getDisplayName();
        }
        String str5 = cjrhissqcl.getsocial;
        final String str6 = cjrhissqcl.attribution;
        if (cjrhissqcl.acquisition.acquisition() == null) {
            str3 = "";
        } else {
            str3 = im.getsocial.sdk.invites.a.k.ztWNWCuZiM.getsocial(cjrhissqcl.acquisition.acquisition().getLocalisedString(), str, str2, str5, cjrhissqcl.retention, false);
        }
        String str7 = str3;
        if (cjrhissqcl.acquisition.mobile() == null) {
            str4 = "";
        } else {
            str4 = im.getsocial.sdk.invites.a.k.ztWNWCuZiM.getsocial(cjrhissqcl.acquisition.mobile().getLocalisedString(), str, str2, str5, cjrhissqcl.retention, false);
        }
        if (cjrhissqcl.acquisition.dau() == null) {
            bArr = cjrhissqcl.mobile;
        } else {
            bArr = cjrhissqcl.acquisition.dau();
        }
        pdwputzxdt.getsocial(inviteChannel, im.getsocial.sdk.invites.a.b.pdwpUtZXDT.getsocial().attribution(str4).getsocial(str7).getsocial(bArr).acquisition(cjrhissqcl.acquisition.retention()).retention(cjrhissqcl.acquisition.cat()).mobile(cjrhissqcl.acquisition.mau()).attribution(cjrhissqcl.acquisition.organic()).getsocial(cjrhissqcl.acquisition.viral()).getsocial(), str2, str5, new im.getsocial.sdk.invites.a.upgqDBbsrL() {
            public final void getsocial(Map<String, String> map) {
                cjrhisSQCL.getsocial(cjrhisSQCL.this, "invite_sent", inviteChannel.getChannelId(), str6, map);
                completionCallback.onSuccess();
            }

            public void onComplete() {
                cjrhisSQCL.getsocial(cjrhisSQCL.this, "invite_sent", inviteChannel.getChannelId(), str6, null);
                completionCallback.onSuccess();
            }

            public void onCancel() {
                cjrhisSQCL.getsocial(cjrhisSQCL.this, "invite_canceled", inviteChannel.getChannelId(), str6, null);
                completionCallback.onFailure(new im.getsocial.sdk.invites.a.c.jjbQypPegg());
            }

            public void onError(Throwable th) {
                cjrhisSQCL.getsocial(cjrhisSQCL.this, "invite_failed", inviteChannel.getChannelId(), str6, null);
                completionCallback.onFailure(im.getsocial.sdk.internal.c.c.jjbQypPegg.getsocial(th));
            }
        });
    }

    static /* synthetic */ void getsocial(cjrhisSQCL cjrhissqcl, String str, String str2, String str3, Map map) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", str2);
        hashMap.put("token", str3);
        hashMap.put("source", MentionTypes.USER);
        if (map != null) {
            hashMap.putAll(map);
        }
        cjrhissqcl.acquisition.getsocial(str, hashMap);
    }
}
