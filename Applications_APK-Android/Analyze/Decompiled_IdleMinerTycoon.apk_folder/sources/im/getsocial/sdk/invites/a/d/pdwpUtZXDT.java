package im.getsocial.sdk.invites.a.d;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.KluUZYuxme;
import im.getsocial.sdk.internal.c.a.jjbQypPegg;
import im.getsocial.sdk.internal.c.b.KSZKMmRWhZ;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.qZypgoeblR;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.internal.c.qdyNCsqjKt;
import im.getsocial.sdk.invites.ReferralData;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProcessAppOpenFunc */
public final class pdwpUtZXDT {
    private static final cjrhisSQCL retention = upgqDBbsrL.getsocial(pdwpUtZXDT.class);
    @XdbacJlTDQ
    qdyNCsqjKt acquisition;
    @XdbacJlTDQ
    KluUZYuxme attribution;
    @XdbacJlTDQ
    jjbQypPegg getsocial;
    @XdbacJlTDQ
    @KSZKMmRWhZ(getsocial = DeviceRequestsHelper.DEVICE_INFO_PARAM)
    @qZypgoeblR
    Map<String, String> mobile;

    private pdwpUtZXDT() {
        ztWNWCuZiM.getsocial(this);
    }

    private ReferralData attribution(String str, boolean z, boolean z2) {
        try {
            ReferralData referralData = this.getsocial.getsocial(this.attribution, z2 && z, attribution(str), this.mobile);
            attribution();
            return referralData;
        } catch (GetSocialException e) {
            if (e.getErrorCode() == 102) {
                retention.attribution("No referral data was found.");
                attribution();
                return null;
            }
            throw e;
        }
    }

    public static ReferralData getsocial(String str, boolean z, boolean z2) {
        return new pdwpUtZXDT().attribution(str, z, z2);
    }

    public static im.getsocial.sdk.invites.a.b.ztWNWCuZiM getsocial(boolean z) {
        pdwpUtZXDT pdwputzxdt = new pdwpUtZXDT();
        im.getsocial.sdk.invites.a.b.ztWNWCuZiM ztwnwcuzim = new im.getsocial.sdk.invites.a.b.ztWNWCuZiM();
        ztwnwcuzim.getsocial = pdwputzxdt.attribution;
        ztwnwcuzim.mobile = pdwputzxdt.mobile;
        ztwnwcuzim.acquisition = pdwputzxdt.attribution(null);
        ztwnwcuzim.attribution = z;
        return ztwnwcuzim;
    }

    public static void getsocial() {
        new pdwpUtZXDT().attribution();
    }

    private void attribution() {
        getsocial("deep_link_referrer");
        getsocial("facebook_referrer");
        getsocial("google_referrer");
    }

    private void getsocial(String str) {
        if (this.acquisition.getsocial(str)) {
            this.acquisition.retention(str);
        }
    }

    private Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, Map<String, String>> attribution(String str) {
        HashMap hashMap = new HashMap();
        getsocial("facebook_referrer", im.getsocial.sdk.invites.a.a.cjrhisSQCL.FACEBOOK, hashMap);
        getsocial("google_referrer", im.getsocial.sdk.invites.a.a.cjrhisSQCL.GOOGLE_PLAY, hashMap);
        if (str == null) {
            getsocial("deep_link_referrer", im.getsocial.sdk.invites.a.a.cjrhisSQCL.DEEP_LINK, hashMap);
        } else {
            HashMap hashMap2 = new HashMap();
            hashMap2.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.getsocial, str);
            hashMap.put(im.getsocial.sdk.invites.a.a.cjrhisSQCL.DEEP_LINK, hashMap2);
        }
        return hashMap;
    }

    private void getsocial(String str, im.getsocial.sdk.invites.a.a.cjrhisSQCL cjrhissqcl, Map<im.getsocial.sdk.invites.a.a.cjrhisSQCL, Map<String, String>> map) {
        Object obj = map.get(cjrhissqcl);
        if (obj == null) {
            obj = new HashMap();
            map.put(cjrhissqcl, obj);
        }
        if (this.acquisition.getsocial(str)) {
            obj.putAll(acquisition(this.acquisition.attribution(str)));
        }
    }

    private static Map<String, String> acquisition(String str) {
        HashMap hashMap = new HashMap();
        try {
            Object obj = new im.getsocial.a.a.a.cjrhisSQCL().getsocial(str, (im.getsocial.a.a.a.jjbQypPegg) null);
            if (obj instanceof im.getsocial.a.a.pdwpUtZXDT) {
                hashMap.putAll((im.getsocial.a.a.pdwpUtZXDT) obj);
            }
        } catch (im.getsocial.a.a.a.pdwpUtZXDT e) {
            retention.getsocial("Could not parse persisted referral parameters.", e);
        }
        return hashMap;
    }
}
