package im.getsocial.sdk.invites.a.e;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import com.google.android.gms.drive.DriveFile;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;

/* compiled from: SmsInviteChannelPlugin */
public class KSZKMmRWhZ extends InviteChannelPlugin {
    public boolean isAvailableForDevice(InviteChannel inviteChannel) {
        if (Build.VERSION.SDK_INT >= 19) {
            return getContext().getPackageManager().hasSystemFeature("android.hardware.telephony") && Telephony.Sms.getDefaultSmsPackage(getContext()) != null;
        }
        return getContext().getPackageManager().hasSystemFeature("android.hardware.telephony");
    }

    public void presentChannelInterface(InviteChannel inviteChannel, InvitePackage invitePackage, InviteCallback inviteCallback) {
        Intent intent = new Intent("android.intent.action.VIEW");
        if (Build.VERSION.SDK_INT >= 19) {
            intent.setData(Uri.parse("sms:"));
        } else {
            intent.setType("vnd.android-dir/mms-sms");
        }
        intent.putExtra("sms_body", invitePackage.getText());
        try {
            Intent createChooser = Intent.createChooser(intent, inviteChannel.getChannelName());
            createChooser.setFlags(DriveFile.MODE_READ_ONLY);
            getContext().startActivity(createChooser);
            inviteCallback.onComplete();
        } catch (Exception e) {
            inviteCallback.onError(e);
        }
    }
}
