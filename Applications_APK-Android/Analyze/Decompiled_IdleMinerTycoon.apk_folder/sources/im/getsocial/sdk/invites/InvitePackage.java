package im.getsocial.sdk.invites;

import android.graphics.Bitmap;

public class InvitePackage {
    /* access modifiers changed from: private */
    public String acquisition;
    /* access modifiers changed from: private */
    public String attribution;
    /* access modifiers changed from: private */
    public String cat;
    /* access modifiers changed from: private */
    public String dau;
    /* access modifiers changed from: private */
    public String getsocial;
    /* access modifiers changed from: private */
    public String mau;
    /* access modifiers changed from: private */
    public Bitmap mobile;
    /* access modifiers changed from: private */
    public String retention;
    /* access modifiers changed from: private */
    public LinkParams viral;

    public String getSubject() {
        return this.getsocial;
    }

    public String getText() {
        return this.attribution;
    }

    public String getUserName() {
        return this.acquisition;
    }

    public Bitmap getImage() {
        return this.mobile;
    }

    public String getImageUrl() {
        return this.retention;
    }

    public String getReferralUrl() {
        return this.dau;
    }

    public String getVideoUrl() {
        return this.mau;
    }

    public String getGifUrl() {
        return this.cat;
    }

    public LinkParams getLinkParams() {
        return this.viral;
    }

    public static class Builder {
        private final InvitePackage getsocial = new InvitePackage();

        public Builder withSubject(String str) {
            String unused = this.getsocial.getsocial = str;
            return this;
        }

        public Builder withUsername(String str) {
            String unused = this.getsocial.acquisition = str;
            return this;
        }

        public Builder withText(String str) {
            String unused = this.getsocial.attribution = str;
            return this;
        }

        public Builder withReferralUrl(String str) {
            String unused = this.getsocial.dau = str;
            return this;
        }

        public Builder withImage(Bitmap bitmap) {
            Bitmap unused = this.getsocial.mobile = bitmap;
            return this;
        }

        public Builder withImageUrl(String str) {
            String unused = this.getsocial.retention = str;
            return this;
        }

        public Builder withVideoUrl(String str) {
            String unused = this.getsocial.mau = str;
            return this;
        }

        public Builder withGifUrl(String str) {
            String unused = this.getsocial.cat = str;
            return this;
        }

        public Builder withLinkParams(LinkParams linkParams) {
            LinkParams unused = this.getsocial.viral = linkParams;
            return this;
        }

        public InvitePackage build() {
            InvitePackage invitePackage = new InvitePackage();
            String unused = invitePackage.acquisition = this.getsocial.acquisition;
            String unused2 = invitePackage.dau = this.getsocial.dau;
            String unused3 = invitePackage.getsocial = this.getsocial.getsocial;
            Bitmap unused4 = invitePackage.mobile = this.getsocial.mobile;
            String unused5 = invitePackage.attribution = this.getsocial.attribution;
            String unused6 = invitePackage.retention = this.getsocial.retention;
            String unused7 = invitePackage.cat = this.getsocial.cat;
            String unused8 = invitePackage.mau = this.getsocial.mau;
            LinkParams unused9 = invitePackage.viral = this.getsocial.viral;
            return invitePackage;
        }
    }
}
