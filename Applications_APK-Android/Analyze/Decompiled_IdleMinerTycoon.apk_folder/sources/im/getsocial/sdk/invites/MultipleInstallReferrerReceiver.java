package im.getsocial.sdk.invites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;

public class MultipleInstallReferrerReceiver extends BroadcastReceiver {
    private static boolean attribution;
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(MultipleInstallReferrerReceiver.class);

    public void onReceive(Context context, Intent intent) {
        getsocial.attribution("MultipleInstallReferrerReceiver invoked.");
        if (intent == null) {
            return;
        }
        if (attribution) {
            getsocial.attribution("Received duplicate intent. Ignoring.");
        } else if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
            attribution = true;
            InstallReferrerReceiver.onIntentReceived(context, intent);
            getsocial(context, intent);
        }
    }

    private static void getsocial(Context context, Intent intent) {
        for (ResolveInfo next : context.getPackageManager().queryBroadcastReceivers(new Intent("com.android.vending.INSTALL_REFERRER"), 0)) {
            String str = next.activityInfo.name;
            boolean equals = MultipleInstallReferrerReceiver.class.getName().equals(str);
            boolean equals2 = InstallReferrerReceiver.class.getName().equals(str);
            if (context.getPackageName().equals(next.activityInfo.packageName) && !equals && !equals2) {
                cjrhisSQCL cjrhissqcl = getsocial;
                cjrhissqcl.attribution("Invoke onReceive on class: " + str);
                try {
                    ((BroadcastReceiver) Class.forName(str).newInstance()).onReceive(context, intent);
                } catch (Throwable th) {
                    getsocial.acquisition("Failed to invoke BroadcastReceiver: %s", str);
                    getsocial.acquisition(th);
                }
            }
        }
    }
}
