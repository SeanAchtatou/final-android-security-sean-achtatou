package im.getsocial.sdk.invites;

import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import java.util.Map;

/* compiled from: LocalizableTextAccessHelper */
public final class cjrhisSQCL {
    private cjrhisSQCL() {
    }

    public static LocalizableText getsocial(String str) {
        if (jjbQypPegg.acquisition(str)) {
            return null;
        }
        return new LocalizableText(str);
    }

    public static Map<String, String> getsocial(LocalizableText localizableText) {
        if (localizableText != null) {
            return localizableText.getsocial();
        }
        return null;
    }
}
