package im.getsocial.sdk.invites;

import java.util.HashMap;
import java.util.Map;

public class CustomReferralData extends HashMap<String, String> {
    public CustomReferralData(Map<String, String> map) {
        super(map);
    }

    public CustomReferralData() {
    }
}
