package im.getsocial.sdk.invites.a.k;

import im.getsocial.sdk.CompletionCallback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.f.upgqDBbsrL;
import im.getsocial.sdk.invites.FetchReferralDataCallback;
import im.getsocial.sdk.invites.a.g.jjbQypPegg;
import java.util.HashMap;
import java.util.LinkedList;

/* compiled from: ReferralDataFetcher */
public class KSZKMmRWhZ {
    private static final cjrhisSQCL getsocial = upgqDBbsrL.getsocial(KSZKMmRWhZ.class);
    private final jjbQypPegg acquisition;
    private final im.getsocial.sdk.invites.a.h.jjbQypPegg attribution;
    private final LinkedList<FetchReferralDataCallback> dau = new LinkedList<>();
    /* access modifiers changed from: private */
    public boolean mobile = false;
    private boolean retention = false;

    @XdbacJlTDQ
    protected KSZKMmRWhZ(im.getsocial.sdk.invites.a.h.jjbQypPegg jjbqyppegg, jjbQypPegg jjbqyppegg2) {
        this.attribution = jjbqyppegg;
        this.acquisition = jjbqyppegg2;
    }

    public final void getsocial(FetchReferralDataCallback fetchReferralDataCallback) {
        if (!this.retention || this.mobile) {
            synchronized (this.dau) {
                this.dau.add(fetchReferralDataCallback);
            }
            return;
        }
        fetchReferralDataCallback.onSuccess(this.acquisition.getsocial());
    }

    public final void getsocial() {
        this.retention = false;
    }

    public final void getsocial(String str) {
        if (im.getsocial.sdk.internal.c.l.jjbQypPegg.acquisition(str)) {
            attribution();
            return;
        }
        getsocial.getsocial("GetSocial deep link detected: %s", str);
        this.mobile = true;
        HashMap hashMap = new HashMap();
        hashMap.put(im.getsocial.sdk.invites.a.a.upgqDBbsrL.getsocial, str);
        this.attribution.getsocial(im.getsocial.sdk.invites.a.a.cjrhisSQCL.DEEP_LINK, hashMap, new CompletionCallback() {
            public void onSuccess() {
                boolean unused = KSZKMmRWhZ.this.mobile = false;
                KSZKMmRWhZ.this.attribution();
            }

            public void onFailure(GetSocialException getSocialException) {
                boolean unused = KSZKMmRWhZ.this.mobile = false;
                KSZKMmRWhZ.this.attribution();
            }
        });
    }

    /* access modifiers changed from: private */
    public void attribution() {
        this.retention = true;
        if (!this.mobile) {
            synchronized (this.dau) {
                while (!this.dau.isEmpty()) {
                    this.dau.pop().onSuccess(this.acquisition.getsocial());
                }
            }
        }
    }
}
