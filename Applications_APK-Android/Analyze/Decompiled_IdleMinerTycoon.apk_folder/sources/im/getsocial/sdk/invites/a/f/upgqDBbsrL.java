package im.getsocial.sdk.invites.a.f;

import com.android.installreferrer.api.InstallReferrerStateListener;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;

/* compiled from: InstallReferrerStateListenerProxy */
public abstract class upgqDBbsrL implements InstallReferrerStateListener {
    private static final cjrhisSQCL getsocial = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(upgqDBbsrL.class);

    public abstract void onSetupFinished(int i);

    public void onInstallReferrerSetupFinished(int i) {
        onSetupFinished(i);
    }

    public void onInstallReferrerServiceDisconnected() {
        getsocial.attribution("Install referrer disconnected");
    }
}
