package im.getsocial.sdk.invites.a.e;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.drive.DriveFile;
import im.getsocial.sdk.internal.m.iFpupLCESp;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;

/* compiled from: InstagramStoryChannelInternalPlugin */
public class zoToeBNOjF extends InviteChannelPlugin {
    public boolean isAvailableForDevice(InviteChannel inviteChannel) {
        return getsocial(getContext());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri
     arg types: [android.content.Context, android.graphics.Bitmap, int, java.lang.String]
     candidates:
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, java.lang.String, java.lang.String, im.getsocial.sdk.internal.m.iFpupLCESp$jjbQypPegg):void
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri */
    public void presentChannelInterface(InviteChannel inviteChannel, final InvitePackage invitePackage, final InviteCallback inviteCallback) {
        if (invitePackage.getVideoUrl() != null) {
            iFpupLCESp.getsocial(getContext(), invitePackage.getVideoUrl(), new iFpupLCESp.cjrhisSQCL() {
                public final void getsocial(Uri uri) {
                    if (uri != null) {
                        zoToeBNOjF.this.getsocial(invitePackage, uri, "video/*", inviteCallback);
                    }
                }
            });
        } else if (invitePackage.getImage() != null) {
            Uri uri = iFpupLCESp.getsocial(getContext(), invitePackage.getImage(), false, "getsocial-smartinvite-tempimage.jpg");
            if (uri != null) {
                getsocial(invitePackage, uri, "image/*", inviteCallback);
            }
        } else {
            getsocial(invitePackage, null, "image/*", inviteCallback);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri
     arg types: [android.content.Context, android.graphics.Bitmap, int, java.lang.String]
     candidates:
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, java.lang.String, java.lang.String, im.getsocial.sdk.internal.m.iFpupLCESp$jjbQypPegg):void
      im.getsocial.sdk.internal.m.iFpupLCESp.getsocial(android.content.Context, android.graphics.Bitmap, boolean, java.lang.String):android.net.Uri */
    /* access modifiers changed from: private */
    public void getsocial(InvitePackage invitePackage, Uri uri, String str, InviteCallback inviteCallback) {
        Intent intent = new Intent("com.instagram.share.ADD_TO_STORY");
        if (uri == null) {
            intent.setType(str);
            intent.putExtra("top_background_color", "#000000");
        } else {
            intent.setDataAndType(uri, str);
        }
        if (invitePackage.getText() != null) {
            Uri uri2 = iFpupLCESp.getsocial(getContext(), iFpupLCESp.attribution(invitePackage.getText()), true, "getsocial-smartinvite-tempsticker.png");
            intent.putExtra(ShareConstants.STORY_INTERACTIVE_ASSET_URI, uri2);
            getContext().grantUriPermission("com.instagram.android", uri2, 1);
        }
        intent.setFlags(1);
        intent.putExtra(ShareConstants.STORY_DEEP_LINK_URL, invitePackage.getReferralUrl());
        if (getContext().getPackageManager().resolveActivity(intent, 0) != null) {
            intent.addFlags(DriveFile.MODE_READ_ONLY);
            try {
                getContext().startActivity(intent);
                inviteCallback.onComplete();
            } catch (Exception e) {
                inviteCallback.onError(e);
            }
        }
    }

    private static boolean getsocial(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.instagram.android", 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
