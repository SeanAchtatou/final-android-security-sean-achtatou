package im.getsocial.sdk.invites.a.e;

import android.graphics.BitmapFactory;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InvitePackage;
import im.getsocial.sdk.invites.a.k.jjbQypPegg;
import im.getsocial.sdk.invites.a.pdwpUtZXDT;
import im.getsocial.sdk.invites.a.upgqDBbsrL;

/* compiled from: SharedInviteChannelPluginAdapter */
public class ztWNWCuZiM implements pdwpUtZXDT {
    private final InviteChannelPlugin attribution;
    @XdbacJlTDQ
    jjbQypPegg getsocial;

    public ztWNWCuZiM(InviteChannelPlugin inviteChannelPlugin) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(inviteChannelPlugin), "Can not create SharedInviteProviderPluginAdapter with null plugin");
        this.attribution = inviteChannelPlugin;
        im.getsocial.sdk.internal.c.b.ztWNWCuZiM.getsocial(this);
    }

    public final boolean getsocial(InviteChannel inviteChannel) {
        return this.attribution.isAvailableForDevice(inviteChannel);
    }

    public final void getsocial(InviteChannel inviteChannel, im.getsocial.sdk.invites.a.b.pdwpUtZXDT pdwputzxdt, String str, String str2, upgqDBbsrL upgqdbbsrl) {
        if (!getsocial(inviteChannel)) {
            upgqdbbsrl.onError(new IllegalStateException("Invite Channel `" + inviteChannel.getChannelId() + "' is not available on device."));
        } else if (!this.getsocial.getsocial()) {
            upgqdbbsrl.onError(new IllegalStateException(String.format("%s is missing in the AndroidManifest. Follow integration guide to fix it: https://docs.getsocial.im/knowledge-base/manual-integration/android/#smart-invites", im.getsocial.sdk.invites.a.k.jjbQypPegg.getsocial)));
        } else {
            InvitePackage.Builder withLinkParams = new InvitePackage.Builder().withUsername(str).withReferralUrl(str2).withLinkParams(pdwputzxdt.viral());
            if (pdwputzxdt.acquisition() != null) {
                withLinkParams.withSubject(pdwputzxdt.acquisition().getLocalisedString());
            }
            if (pdwputzxdt.mobile() != null) {
                withLinkParams.withText(pdwputzxdt.mobile().getLocalisedString());
            }
            if (pdwputzxdt.retention() != null) {
                withLinkParams.withImageUrl(pdwputzxdt.retention());
            }
            if (pdwputzxdt.cat() != null) {
                withLinkParams.withGifUrl(pdwputzxdt.cat());
            }
            if (pdwputzxdt.mau() != null) {
                withLinkParams.withVideoUrl(pdwputzxdt.mau());
            }
            byte[] dau = pdwputzxdt.dau();
            if (dau != null) {
                withLinkParams.withImage(BitmapFactory.decodeByteArray(dau, 0, dau.length));
            }
            this.attribution.presentChannelInterface(inviteChannel, withLinkParams.build(), upgqdbbsrl);
        }
    }
}
