package im.getsocial.sdk.invites.a.b;

/* compiled from: InviteLinkDescriptor */
public class XdbacJlTDQ {
    private final String attribution;
    private final String getsocial;

    public XdbacJlTDQ(String str, String str2) {
        this.getsocial = str;
        this.attribution = str2;
    }

    public final String getsocial() {
        return this.getsocial;
    }

    public final String attribution() {
        return this.attribution;
    }
}
