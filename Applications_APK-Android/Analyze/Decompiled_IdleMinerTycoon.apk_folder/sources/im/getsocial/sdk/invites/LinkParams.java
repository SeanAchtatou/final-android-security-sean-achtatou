package im.getsocial.sdk.invites;

import java.util.HashMap;
import java.util.Map;

public class LinkParams extends HashMap<String, Object> {
    public static final String KEY_CUSTOM_DESCRIPTION = "$description";
    public static final String KEY_CUSTOM_IMAGE = "$image";
    public static final String KEY_CUSTOM_TITLE = "$title";
    public static final String KEY_CUSTOM_YOUTUBE_VIDEO = "$youtube_video";
    public static final String KEY_PROMO_CODE = "$promo_code";

    public LinkParams(Map<String, Object> map) {
        super(map);
    }

    public LinkParams() {
    }

    public Map<String, String> getStringValues() {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : entrySet()) {
            Object value = entry.getValue();
            if (value instanceof String) {
                hashMap.put(entry.getKey(), (String) value);
            }
        }
        return hashMap;
    }
}
