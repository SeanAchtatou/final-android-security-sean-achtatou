package im.getsocial.sdk;

import android.app.Application;
import android.graphics.Bitmap;
import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.activities.ActivitiesQuery;
import im.getsocial.sdk.activities.ActivityPost;
import im.getsocial.sdk.activities.ActivityPostContent;
import im.getsocial.sdk.activities.ReportingReason;
import im.getsocial.sdk.activities.TagsQuery;
import im.getsocial.sdk.iap.PurchaseData;
import im.getsocial.sdk.internal.jjbQypPegg;
import im.getsocial.sdk.invites.CustomReferralData;
import im.getsocial.sdk.invites.FetchReferralDataCallback;
import im.getsocial.sdk.invites.InviteCallback;
import im.getsocial.sdk.invites.InviteChannel;
import im.getsocial.sdk.invites.InviteChannelPlugin;
import im.getsocial.sdk.invites.InviteContent;
import im.getsocial.sdk.invites.LinkParams;
import im.getsocial.sdk.invites.ReferredUser;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import im.getsocial.sdk.pushnotifications.Notification;
import im.getsocial.sdk.pushnotifications.NotificationContent;
import im.getsocial.sdk.pushnotifications.NotificationListener;
import im.getsocial.sdk.pushnotifications.NotificationStatus;
import im.getsocial.sdk.pushnotifications.NotificationsCountQuery;
import im.getsocial.sdk.pushnotifications.NotificationsQuery;
import im.getsocial.sdk.pushnotifications.NotificationsSummary;
import im.getsocial.sdk.pushnotifications.PushTokenListener;
import im.getsocial.sdk.socialgraph.SuggestedFriend;
import im.getsocial.sdk.usermanagement.AddAuthIdentityCallback;
import im.getsocial.sdk.usermanagement.AuthIdentity;
import im.getsocial.sdk.usermanagement.OnUserChangedListener;
import im.getsocial.sdk.usermanagement.PublicUser;
import im.getsocial.sdk.usermanagement.UserReference;
import im.getsocial.sdk.usermanagement.UserUpdate;
import im.getsocial.sdk.usermanagement.UsersQuery;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class GetSocial {
    private static volatile jjbQypPegg getsocial;

    public static String getSdkVersion() {
        return "6.28.2";
    }

    private GetSocial() {
    }

    static jjbQypPegg getsocial() {
        if (getsocial == null) {
            synchronized (GetSocial.class) {
                if (getsocial == null) {
                    getsocial = new jjbQypPegg();
                }
            }
        }
        return getsocial;
    }

    public static void init() {
        getsocial().getsocial((String) null);
    }

    public static void init(String str) {
        getsocial().getsocial(str);
    }

    public static void whenInitialized(Runnable runnable) {
        getsocial().getsocial(runnable);
    }

    public static boolean isInitialized() {
        return getsocial().getsocial();
    }

    public static boolean setGlobalErrorListener(GlobalErrorListener globalErrorListener) {
        return getsocial().getsocial(globalErrorListener);
    }

    public static boolean removeGlobalErrorListener() {
        return getsocial().attribution();
    }

    public static String getLanguage() {
        return getsocial().acquisition();
    }

    public static boolean setLanguage(String str) {
        return getsocial().attribution(str);
    }

    public static boolean isInviteChannelAvailable(String str) {
        return getsocial().acquisition(str);
    }

    public static List<InviteChannel> getInviteChannels() {
        return getsocial().retention();
    }

    public static void sendInvite(String str, InviteCallback inviteCallback) {
        getsocial().getsocial(str, (InviteContent) null, (LinkParams) null, inviteCallback);
    }

    public static void sendInvite(String str, InviteContent inviteContent, InviteCallback inviteCallback) {
        getsocial().getsocial(str, inviteContent, (LinkParams) null, inviteCallback);
    }

    public static void sendInvite(String str, InviteContent inviteContent, CustomReferralData customReferralData, InviteCallback inviteCallback) {
        LinkParams linkParams = new LinkParams();
        if (customReferralData != null) {
            linkParams.putAll(customReferralData);
        }
        getsocial().getsocial(str, inviteContent, linkParams, inviteCallback);
    }

    public static void sendInvite(String str, InviteContent inviteContent, LinkParams linkParams, InviteCallback inviteCallback) {
        getsocial().getsocial(str, inviteContent, linkParams, inviteCallback);
    }

    public static boolean registerInviteChannelPlugin(String str, InviteChannelPlugin inviteChannelPlugin) {
        return getsocial().getsocial(str, inviteChannelPlugin);
    }

    public static void getReferralData(FetchReferralDataCallback fetchReferralDataCallback) {
        getsocial().getsocial(fetchReferralDataCallback);
    }

    public static void clearReferralData() {
        getsocial().mobile();
    }

    public static void getReferredUsers(Callback<List<ReferredUser>> callback) {
        getsocial().getsocial(callback);
    }

    public static void createInviteLink(LinkParams linkParams, Callback<String> callback) {
        getsocial().getsocial(linkParams, callback);
    }

    public static void getGlobalFeedAnnouncements(Callback<List<ActivityPost>> callback) {
        getsocial().getsocial(ActivitiesQuery.GLOBAL_FEED, callback);
    }

    public static void getAnnouncements(String str, Callback<List<ActivityPost>> callback) {
        getsocial().getsocial(str, callback);
    }

    public static void getActivities(ActivitiesQuery activitiesQuery, Callback<List<ActivityPost>> callback) {
        getsocial().getsocial(activitiesQuery, callback);
    }

    public static void getActivity(String str, Callback<ActivityPost> callback) {
        getsocial().attribution(str, callback);
    }

    public static void postActivityToGlobalFeed(ActivityPostContent activityPostContent, Callback<ActivityPost> callback) {
        getsocial().getsocial(ActivitiesQuery.GLOBAL_FEED, activityPostContent, callback);
    }

    public static void postActivityToFeed(String str, ActivityPostContent activityPostContent, Callback<ActivityPost> callback) {
        getsocial().getsocial(str, activityPostContent, callback);
    }

    public static void postCommentToActivity(String str, ActivityPostContent activityPostContent, Callback<ActivityPost> callback) {
        getsocial().attribution(str, activityPostContent, callback);
    }

    public static void likeActivity(String str, boolean z, Callback<ActivityPost> callback) {
        getsocial().getsocial(str, z, callback);
    }

    public static void getActivityLikers(String str, int i, int i2, Callback<List<PublicUser>> callback) {
        getsocial().getsocial(str, i, i2, callback);
    }

    public static void reportActivity(String str, ReportingReason reportingReason, CompletionCallback completionCallback) {
        getsocial().getsocial(str, reportingReason, completionCallback);
    }

    @Deprecated
    public static void deleteActivity(String str, CompletionCallback completionCallback) {
        getsocial().getsocial(Collections.singletonList(str), completionCallback);
    }

    public static void removeActivities(List<String> list, CompletionCallback completionCallback) {
        getsocial().getsocial(list, completionCallback);
    }

    public static void findTags(TagsQuery tagsQuery, Callback<List<String>> callback) {
        getsocial().getsocial(tagsQuery, callback);
    }

    public static void registerForPushNotifications() {
        getsocial().unity();
    }

    public static void setNotificationListener(NotificationListener notificationListener) {
        getsocial().getsocial(notificationListener);
    }

    public static void setPushNotificationTokenListener(PushTokenListener pushTokenListener) {
        getsocial().getsocial(pushTokenListener);
    }

    public static void processAction(Action action) {
        getsocial().getsocial(action);
    }

    public static void createPromoCode(PromoCodeBuilder promoCodeBuilder, Callback<PromoCode> callback) {
        getsocial().getsocial(promoCodeBuilder, callback);
    }

    public static void getPromoCode(String str, Callback<PromoCode> callback) {
        getsocial().mau(str, callback);
    }

    public static void claimPromoCode(String str, Callback<PromoCode> callback) {
        getsocial().cat(str, callback);
    }

    public static void getUserById(String str, Callback<PublicUser> callback) {
        getsocial().acquisition(str, callback);
    }

    public static void getUserByAuthIdentity(String str, String str2, Callback<PublicUser> callback) {
        getsocial().getsocial(str, str2, callback);
    }

    public static void getUsersByAuthIdentities(String str, List<String> list, Callback<Map<String, PublicUser>> callback) {
        getsocial().getsocial(str, list, callback);
    }

    public static void findUsers(UsersQuery usersQuery, Callback<List<UserReference>> callback) {
        getsocial().getsocial(usersQuery, callback);
    }

    public static class User {
        private User() {
        }

        public static boolean setOnUserChangedListener(OnUserChangedListener onUserChangedListener) {
            return GetSocial.getsocial().getsocial(onUserChangedListener);
        }

        public static boolean removeOnUserChangedListener() {
            return GetSocial.getsocial().dau();
        }

        public static void setUserDetails(UserUpdate userUpdate, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(userUpdate, completionCallback);
        }

        public static void setPublicProperty(String str, String str2, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(str, str2, completionCallback);
        }

        public static void setPrivateProperty(String str, String str2, CompletionCallback completionCallback) {
            GetSocial.getsocial().attribution(str, str2, completionCallback);
        }

        public static void removePublicProperty(String str, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(str, completionCallback);
        }

        public static void removePrivateProperty(String str, CompletionCallback completionCallback) {
            GetSocial.getsocial().attribution(str, completionCallback);
        }

        public static String getPublicProperty(String str) {
            return GetSocial.getsocial().retention(str);
        }

        public static String getPrivateProperty(String str) {
            return GetSocial.getsocial().mobile(str);
        }

        public static boolean hasPublicProperty(String str) {
            return GetSocial.getsocial().mau(str);
        }

        public static boolean hasPrivateProperty(String str) {
            return GetSocial.getsocial().dau(str);
        }

        public static Map<String, String> getAllPublicProperties() {
            return GetSocial.getsocial().mau();
        }

        public static Map<String, String> getAllPrivateProperties() {
            return GetSocial.getsocial().cat();
        }

        public static String getId() {
            return GetSocial.getsocial().viral();
        }

        public static void setDisplayName(String str, CompletionCallback completionCallback) {
            GetSocial.getsocial().mobile(str, completionCallback);
        }

        public static String getDisplayName() {
            return GetSocial.getsocial().growth();
        }

        public static void setAvatarUrl(String str, CompletionCallback completionCallback) {
            GetSocial.getsocial().retention(str, completionCallback);
        }

        public static void setAvatar(Bitmap bitmap, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(bitmap, completionCallback);
        }

        public static String getAvatarUrl() {
            return GetSocial.getsocial().android();
        }

        public static boolean isAnonymous() {
            return GetSocial.getsocial().organic();
        }

        public static void addAuthIdentity(AuthIdentity authIdentity, AddAuthIdentityCallback addAuthIdentityCallback) {
            GetSocial.getsocial().getsocial(authIdentity, addAuthIdentityCallback);
        }

        public static void switchUser(AuthIdentity authIdentity, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(authIdentity, completionCallback);
        }

        public static void removeAuthIdentity(String str, CompletionCallback completionCallback) {
            GetSocial.getsocial().acquisition(str, completionCallback);
        }

        public static Map<String, String> getAuthIdentities() {
            return GetSocial.getsocial().ios();
        }

        public static void addFriend(String str, Callback<Integer> callback) {
            GetSocial.getsocial().mobile(str, callback);
        }

        public static void addFriendsByAuthIdentities(String str, List<String> list, Callback<Integer> callback) {
            GetSocial.getsocial().attribution(str, list, callback);
        }

        public static void removeFriend(String str, Callback<Integer> callback) {
            GetSocial.getsocial().retention(str, callback);
        }

        public static void removeFriendsByAuthIdentities(String str, List<String> list, Callback<Integer> callback) {
            GetSocial.getsocial().acquisition(str, list, callback);
        }

        public static void setFriends(List<String> list, CompletionCallback completionCallback) {
            GetSocial.getsocial().attribution(list, completionCallback);
        }

        public static void setFriendsByAuthIdentities(String str, List<String> list, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(str, list, completionCallback);
        }

        public static void isFriend(String str, Callback<Boolean> callback) {
            GetSocial.getsocial().dau(str, callback);
        }

        public static void getFriendsCount(Callback<Integer> callback) {
            GetSocial.getsocial().attribution(callback);
        }

        public static void getFriends(int i, int i2, Callback<List<PublicUser>> callback) {
            GetSocial.getsocial().getsocial(i, i2, callback);
        }

        public static void getSuggestedFriends(int i, int i2, Callback<List<SuggestedFriend>> callback) {
            GetSocial.getsocial().attribution(i, i2, callback);
        }

        public static void getFriendsReferences(Callback<List<UserReference>> callback) {
            GetSocial.getsocial().acquisition(callback);
        }

        public static void getNotifications(NotificationsQuery notificationsQuery, Callback<List<Notification>> callback) {
            GetSocial.getsocial().getsocial(notificationsQuery, callback);
        }

        public static void getNotificationsCount(NotificationsCountQuery notificationsCountQuery, Callback<Integer> callback) {
            GetSocial.getsocial().getsocial(notificationsCountQuery, callback);
        }

        @Deprecated
        public static void setNotificationsRead(List<String> list, boolean z, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(list, z ? NotificationStatus.READ : NotificationStatus.UNREAD, completionCallback);
        }

        public static void setNotificationsStatus(List<String> list, String str, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(list, str, completionCallback);
        }

        public static void sendNotification(List<String> list, NotificationContent notificationContent, Callback<NotificationsSummary> callback) {
            GetSocial.getsocial().getsocial(list, notificationContent, callback);
        }

        public static void setPushNotificationsEnabled(boolean z, CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(z, completionCallback);
        }

        public static void isPushNotificationsEnabled(Callback<Boolean> callback) {
            GetSocial.getsocial().mobile(callback);
        }

        public static void reset(CompletionCallback completionCallback) {
            GetSocial.getsocial().getsocial(completionCallback);
        }
    }

    public static boolean trackPurchaseEvent(PurchaseData purchaseData) {
        return getsocial().getsocial(purchaseData);
    }

    public static boolean trackCustomEvent(String str, Map<String, String> map) {
        return getsocial().getsocial(str, map);
    }

    static void getsocial(Application application) {
        getsocial().getsocial(application);
    }

    static void handleOnStartUnityEvent() {
        getsocial().connect();
    }
}
