package im.getsocial.sdk;

public final class R {
    private R() {
    }

    public static final class id {
        public static final int notification_background = 2131231019;
        public static final int notification_content = 2131231020;
        public static final int notification_message = 2131231023;
        public static final int notification_title = 2131231024;

        private id() {
        }
    }

    public static final class layout {
        public static final int custom_notification_layout = 2131427363;

        private layout() {
        }
    }

    public static final class style {
        public static final int NotificationTitle = 2131755242;

        private style() {
        }
    }
}
