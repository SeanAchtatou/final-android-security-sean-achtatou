package im.getsocial.sdk;

import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import im.getsocial.sdk.internal.c.f.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.f.pdwpUtZXDT;
import im.getsocial.sdk.internal.d.jjbQypPegg;
import im.getsocial.sdk.internal.m.KSZKMmRWhZ;

public class AutoInitSdkContentProvider extends ContentProvider {
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public boolean onCreate() {
        Context context = getContext();
        if (context == null) {
            System.out.println("Context is null! Android OS bug. Report the OS version and device model.");
            return false;
        }
        Application application = (Application) context.getApplicationContext();
        pdwpUtZXDT.getsocial(new XdbacJlTDQ());
        KSZKMmRWhZ.getsocial(application);
        new jjbQypPegg().getsocial(application, true);
        return false;
    }
}
