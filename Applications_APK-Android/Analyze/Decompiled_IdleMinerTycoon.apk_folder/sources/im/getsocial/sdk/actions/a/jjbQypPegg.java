package im.getsocial.sdk.actions.a;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.GetSocialException;
import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.actions.ActionListener;
import im.getsocial.sdk.actions.ActionTypes;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.jMsobIMeui;
import im.getsocial.sdk.promocodes.PromoCode;

/* compiled from: CoreActionListener */
public final class jjbQypPegg implements ActionListener {
    @XdbacJlTDQ
    jMsobIMeui getsocial;

    public jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    public final boolean handleAction(Action action) {
        if (ActionTypes.OPEN_URL.equals(action.getType())) {
            this.getsocial.getsocial(action.getData().get("$url"));
            return true;
        } else if (ActionTypes.ADD_FRIEND.equals(action.getType())) {
            new im.getsocial.sdk.socialgraph.a.c.jjbQypPegg().getsocial(action.getData().get("$user_id"), new Callback<Integer>() {
                public void onFailure(GetSocialException getSocialException) {
                }

                public /* bridge */ /* synthetic */ void onSuccess(Object obj) {
                }
            });
            return true;
        } else if (!ActionTypes.CLAIM_PROMO_CODE.equals(action.getType())) {
            return false;
        } else {
            new im.getsocial.sdk.promocodes.a.b.jjbQypPegg().getsocial(action.getData().get("$promo_code"), new Callback<PromoCode>() {
                public void onFailure(GetSocialException getSocialException) {
                }

                public /* bridge */ /* synthetic */ void onSuccess(Object obj) {
                }
            });
            return true;
        }
    }
}
