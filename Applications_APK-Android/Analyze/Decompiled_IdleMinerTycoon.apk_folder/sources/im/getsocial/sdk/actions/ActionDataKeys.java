package im.getsocial.sdk.actions;

public final class ActionDataKeys {
    private ActionDataKeys() {
    }

    public static final class OpenActivity {
        public static final String ACTIVITY_ID = "$activity_id";
        public static final String COMMENT_ID = "$comment_id";
        public static final String FEED_NAME = "$feed_name";

        private OpenActivity() {
        }
    }

    public static final class OpenProfile {
        public static final String USER_ID = "$user_id";

        private OpenProfile() {
        }
    }

    public static final class OpenUrl {
        public static final String URL = "$url";

        private OpenUrl() {
        }
    }

    public static final class AddFriend {
        public static final String USER_ID = "$user_id";

        private AddFriend() {
        }
    }

    public static final class ClaimPromoCode {
        public static final String PROMO_CODE = "$promo_code";

        private ClaimPromoCode() {
        }
    }
}
