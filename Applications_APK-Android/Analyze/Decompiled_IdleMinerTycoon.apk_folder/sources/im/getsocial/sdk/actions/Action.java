package im.getsocial.sdk.actions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class Action {
    /* access modifiers changed from: private */
    public final Map<String, String> attribution = new HashMap();
    /* access modifiers changed from: private */
    public final String getsocial;

    Action(String str) {
        this.getsocial = str;
    }

    public static Builder builder(String str) {
        return new Builder(str);
    }

    public final String getType() {
        return this.getsocial;
    }

    public final Map<String, String> getData() {
        return Collections.unmodifiableMap(this.attribution);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Action)) {
            return false;
        }
        Action action = (Action) obj;
        if (!this.getsocial.equals(action.getsocial)) {
            return false;
        }
        return this.attribution.equals(action.attribution);
    }

    public final int hashCode() {
        return (this.getsocial.hashCode() * 31) + this.attribution.hashCode();
    }

    public final String toString() {
        return "Action{_type='" + this.getsocial + '\'' + ", _data=" + this.attribution + '}';
    }

    public static class Builder {
        private final Action getsocial;

        Builder(String str) {
            this.getsocial = new Action(str);
        }

        public Builder addActionData(String str, String str2) {
            this.getsocial.attribution.put(str, str2);
            return this;
        }

        public Builder addActionData(Map<String, String> map) {
            this.getsocial.attribution.putAll(map);
            return this;
        }

        public Action build() {
            Action action = this.getsocial;
            Action action2 = new Action(action.getsocial);
            action2.attribution.putAll(action.attribution);
            return action2;
        }
    }
}
