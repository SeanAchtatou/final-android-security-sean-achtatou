package im.getsocial.sdk.actions.a.a;

import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.internal.c.l.upgqDBbsrL;
import im.getsocial.sdk.internal.f.a.pdwpUtZXDT;

/* compiled from: ActionsThriftyConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static Action getsocial(pdwpUtZXDT pdwputzxdt) {
        if (pdwputzxdt == null || pdwputzxdt.getsocial == null) {
            return null;
        }
        return Action.builder(pdwputzxdt.getsocial).addActionData(upgqDBbsrL.getsocial(pdwputzxdt.attribution)).build();
    }

    public static pdwpUtZXDT getsocial(Action action) {
        if (action == null) {
            return null;
        }
        pdwpUtZXDT pdwputzxdt = new pdwpUtZXDT();
        pdwputzxdt.getsocial = action.getType();
        pdwputzxdt.attribution = action.getData();
        return pdwputzxdt;
    }
}
