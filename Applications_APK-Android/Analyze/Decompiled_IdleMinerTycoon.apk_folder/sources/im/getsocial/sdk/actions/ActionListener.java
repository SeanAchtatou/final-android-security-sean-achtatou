package im.getsocial.sdk.actions;

public interface ActionListener {
    boolean handleAction(Action action);
}
