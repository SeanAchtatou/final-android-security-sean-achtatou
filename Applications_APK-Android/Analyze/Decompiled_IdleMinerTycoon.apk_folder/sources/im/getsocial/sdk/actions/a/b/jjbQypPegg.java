package im.getsocial.sdk.actions.a.b;

import im.getsocial.sdk.actions.Action;
import im.getsocial.sdk.actions.ActionListener;
import im.getsocial.sdk.internal.c.b.KSZKMmRWhZ;
import im.getsocial.sdk.internal.c.b.XdbacJlTDQ;
import im.getsocial.sdk.internal.c.b.qZypgoeblR;
import im.getsocial.sdk.internal.c.b.ztWNWCuZiM;
import im.getsocial.sdk.internal.c.f.cjrhisSQCL;
import im.getsocial.sdk.internal.c.k.upgqDBbsrL;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.internal.c.vkXhnjhKGp;

/* compiled from: FireActionUseCase */
public final class jjbQypPegg implements upgqDBbsrL {
    /* access modifiers changed from: private */
    public static final cjrhisSQCL mobile = im.getsocial.sdk.internal.c.f.upgqDBbsrL.getsocial(jjbQypPegg.class);
    @XdbacJlTDQ
    vkXhnjhKGp acquisition;
    @XdbacJlTDQ
    @KSZKMmRWhZ(getsocial = "ui_action_listener")
    @qZypgoeblR
    ActionListener attribution;
    @XdbacJlTDQ
    @KSZKMmRWhZ(getsocial = "core_action_listener")
    ActionListener getsocial;

    public jjbQypPegg() {
        ztWNWCuZiM.getsocial(this);
    }

    public final void getsocial(final Action action) {
        jjbQypPegg.C0021jjbQypPegg.getsocial(im.getsocial.sdk.internal.c.l.jjbQypPegg.getsocial(action), "Can not invoke FireActionUseCase with null action");
        this.acquisition.getsocial(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0063  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0066  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL r0 = im.getsocial.sdk.actions.a.b.jjbQypPegg.mobile
                    java.lang.String r1 = "Processing action: [%s]"
                    r2 = 1
                    java.lang.Object[] r3 = new java.lang.Object[r2]
                    im.getsocial.sdk.actions.Action r4 = r3
                    r5 = 0
                    r3[r5] = r4
                    r0.getsocial(r1, r3)
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL r0 = im.getsocial.sdk.actions.a.b.jjbQypPegg.mobile
                    java.lang.String r1 = "Has UI action listener set: %b"
                    java.lang.Object[] r3 = new java.lang.Object[r2]
                    im.getsocial.sdk.actions.a.b.jjbQypPegg r4 = im.getsocial.sdk.actions.a.b.jjbQypPegg.this
                    im.getsocial.sdk.actions.ActionListener r4 = r4.attribution
                    if (r4 == 0) goto L_0x0021
                    r4 = 1
                    goto L_0x0022
                L_0x0021:
                    r4 = 0
                L_0x0022:
                    java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)
                    r3[r5] = r4
                    r0.getsocial(r1, r3)
                    im.getsocial.sdk.actions.a.b.jjbQypPegg r0 = im.getsocial.sdk.actions.a.b.jjbQypPegg.this     // Catch:{ Exception -> 0x004b }
                    im.getsocial.sdk.actions.ActionListener r0 = r0.attribution     // Catch:{ Exception -> 0x004b }
                    if (r0 == 0) goto L_0x003d
                    im.getsocial.sdk.actions.a.b.jjbQypPegg r0 = im.getsocial.sdk.actions.a.b.jjbQypPegg.this     // Catch:{ Exception -> 0x004b }
                    im.getsocial.sdk.actions.ActionListener r0 = r0.attribution     // Catch:{ Exception -> 0x004b }
                    im.getsocial.sdk.actions.Action r1 = r3     // Catch:{ Exception -> 0x004b }
                    boolean r0 = r0.handleAction(r1)     // Catch:{ Exception -> 0x004b }
                    if (r0 != 0) goto L_0x0049
                L_0x003d:
                    im.getsocial.sdk.actions.a.b.jjbQypPegg r0 = im.getsocial.sdk.actions.a.b.jjbQypPegg.this     // Catch:{ Exception -> 0x004b }
                    im.getsocial.sdk.actions.ActionListener r0 = r0.getsocial     // Catch:{ Exception -> 0x004b }
                    im.getsocial.sdk.actions.Action r1 = r3     // Catch:{ Exception -> 0x004b }
                    boolean r0 = r0.handleAction(r1)     // Catch:{ Exception -> 0x004b }
                    if (r0 == 0) goto L_0x0053
                L_0x0049:
                    r0 = 1
                    goto L_0x0054
                L_0x004b:
                    r0 = move-exception
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL r1 = im.getsocial.sdk.actions.a.b.jjbQypPegg.mobile
                    r1.acquisition(r0)
                L_0x0053:
                    r0 = 0
                L_0x0054:
                    im.getsocial.sdk.internal.c.f.cjrhisSQCL r1 = im.getsocial.sdk.actions.a.b.jjbQypPegg.mobile
                    java.lang.String r3 = "Action %s was handled: [%s]"
                    r4 = 2
                    java.lang.Object[] r4 = new java.lang.Object[r4]
                    im.getsocial.sdk.actions.Action r6 = r3
                    r4[r5] = r6
                    if (r0 == 0) goto L_0x0066
                    java.lang.String r0 = "true"
                    goto L_0x0068
                L_0x0066:
                    java.lang.String r0 = "false"
                L_0x0068:
                    r4[r2] = r0
                    r1.getsocial(r3, r4)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: im.getsocial.sdk.actions.a.b.jjbQypPegg.AnonymousClass1.run():void");
            }
        });
    }
}
