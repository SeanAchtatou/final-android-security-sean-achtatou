package im.getsocial.sdk.actions;

public final class ActionTypes {
    public static final String ADD_FRIEND = "add_friend";
    public static final String CLAIM_PROMO_CODE = "claim_promo_code";
    public static final String CUSTOM = "custom";
    public static final String OPEN_ACTIVITY = "open_activity";
    public static final String OPEN_INVITES = "open_invites";
    public static final String OPEN_PROFILE = "open_profile";
    public static final String OPEN_URL = "open_url";

    private ActionTypes() {
    }
}
