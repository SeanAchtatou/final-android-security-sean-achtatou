package im.getsocial.sdk.media;

/* compiled from: MediaAttachmentAccessHelper */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static im.getsocial.sdk.internal.k.a.jjbQypPegg getsocial(MediaAttachment mediaAttachment) {
        if (mediaAttachment == null) {
            return null;
        }
        return mediaAttachment.getsocial();
    }
}
