package im.getsocial.sdk.media;

import android.graphics.Bitmap;
import im.getsocial.sdk.internal.k.a.jjbQypPegg;
import im.getsocial.sdk.internal.m.iFpupLCESp;

public final class MediaAttachment {
    private final jjbQypPegg getsocial;

    private MediaAttachment(jjbQypPegg jjbqyppegg) {
        this.getsocial = jjbqyppegg;
    }

    public static MediaAttachment image(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return new MediaAttachment(jjbQypPegg.getsocial(iFpupLCESp.getsocial(bitmap)));
    }

    public static MediaAttachment imageUrl(String str) {
        if (str == null) {
            return null;
        }
        return new MediaAttachment(jjbQypPegg.getsocial(str));
    }

    public static MediaAttachment video(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return new MediaAttachment(jjbQypPegg.attribution(bArr));
    }

    public static MediaAttachment videoUrl(String str) {
        if (str == null) {
            return null;
        }
        return new MediaAttachment(jjbQypPegg.attribution(str));
    }

    /* access modifiers changed from: package-private */
    public final jjbQypPegg getsocial() {
        return this.getsocial;
    }
}
