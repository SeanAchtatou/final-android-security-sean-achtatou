package im.getsocial.sdk.promocodes;

import im.getsocial.sdk.internal.m.ztWNWCuZiM;
import im.getsocial.sdk.usermanagement.UserReference;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class PromoCode {
    /* access modifiers changed from: private */
    public int acquisition;
    /* access modifiers changed from: private */
    public final Map<String, String> attribution = new HashMap();
    /* access modifiers changed from: private */
    public boolean cat;
    /* access modifiers changed from: private */
    public Date dau;
    /* access modifiers changed from: private */
    public final String getsocial;
    /* access modifiers changed from: private */
    public int mau;
    /* access modifiers changed from: private */
    public Date mobile;
    /* access modifiers changed from: private */
    public UserReference retention;
    /* access modifiers changed from: private */
    public boolean viral;

    PromoCode(String str) {
        this.getsocial = str;
    }

    public static Builder builder(String str) {
        return new Builder(str);
    }

    public final String getCode() {
        return this.getsocial;
    }

    public final Map<String, String> getData() {
        return this.attribution;
    }

    public final int getMaxClaimCount() {
        return this.acquisition;
    }

    public final Date getStartDate() {
        return ztWNWCuZiM.getsocial(this.mobile);
    }

    public final Date getEndDate() {
        return ztWNWCuZiM.getsocial(this.dau);
    }

    public final int getClaimCount() {
        return this.mau;
    }

    public final boolean isEnabled() {
        return this.cat;
    }

    public final boolean isClaimable() {
        return this.viral;
    }

    public final UserReference getCreator() {
        return this.retention;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PromoCode)) {
            return false;
        }
        PromoCode promoCode = (PromoCode) obj;
        if (this.acquisition != promoCode.acquisition || this.mau != promoCode.mau || this.cat != promoCode.cat || this.viral != promoCode.viral) {
            return false;
        }
        if (this.getsocial == null ? promoCode.getsocial != null : !this.getsocial.equals(promoCode.getsocial)) {
            return false;
        }
        if (!this.attribution.equals(promoCode.attribution) || !this.mobile.equals(promoCode.mobile) || !this.retention.equals(promoCode.retention)) {
            return false;
        }
        if (this.dau != null) {
            return this.dau.equals(promoCode.dau);
        }
        return promoCode.dau == null;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = (((((((((this.getsocial != null ? this.getsocial.hashCode() : 0) * 31) + this.attribution.hashCode()) * 31) + this.acquisition) * 31) + this.mobile.hashCode()) * 31) + this.retention.hashCode()) * 31;
        if (this.dau != null) {
            i = this.dau.hashCode();
        }
        return ((((((hashCode + i) * 31) + this.mau) * 31) + (this.cat ? 1 : 0)) * 31) + (this.viral ? 1 : 0);
    }

    public final String toString() {
        return "PromoCode{_code='" + this.getsocial + '\'' + ", _data=" + this.attribution + ", _maxClaimCount=" + this.acquisition + ", _startDate=" + this.mobile + ", _creator=" + this.retention + ", _endDate=" + this.dau + ", _claimCount=" + this.mau + ", _isEnabled=" + this.cat + ", _isClaimable=" + this.viral + '}';
    }

    public static final class Builder {
        private final PromoCode getsocial;

        Builder(String str) {
            this.getsocial = new PromoCode(str);
        }

        public final Builder addData(Map<String, String> map) {
            this.getsocial.attribution.putAll(map);
            return this;
        }

        public final Builder addData(String str, String str2) {
            this.getsocial.attribution.put(str, str2);
            return this;
        }

        public final Builder withMaxClaimCount(int i) {
            int unused = this.getsocial.acquisition = i;
            return this;
        }

        public final Builder withStartDate(Date date) {
            Date unused = this.getsocial.mobile = date;
            return this;
        }

        public final Builder withEndDate(Date date) {
            Date unused = this.getsocial.dau = date;
            return this;
        }

        public final Builder withClaimCount(int i) {
            int unused = this.getsocial.mau = i;
            return this;
        }

        public final Builder withIsEnabled(boolean z) {
            boolean unused = this.getsocial.cat = z;
            return this;
        }

        public final Builder withIsClaimable(boolean z) {
            boolean unused = this.getsocial.viral = z;
            return this;
        }

        public final Builder withCreator(UserReference userReference) {
            UserReference unused = this.getsocial.retention = userReference;
            return this;
        }

        public final PromoCode build() {
            PromoCode promoCode = this.getsocial;
            PromoCode promoCode2 = new PromoCode(promoCode.getsocial);
            promoCode2.attribution.putAll(promoCode.attribution);
            Date unused = promoCode2.mobile = promoCode.mobile;
            Date unused2 = promoCode2.dau = promoCode.dau;
            boolean unused3 = promoCode2.cat = promoCode.cat;
            int unused4 = promoCode2.acquisition = promoCode.acquisition;
            int unused5 = promoCode2.mau = promoCode.mau;
            boolean unused6 = promoCode2.viral = promoCode.viral;
            UserReference unused7 = promoCode2.retention = promoCode.retention;
            return promoCode2;
        }
    }
}
