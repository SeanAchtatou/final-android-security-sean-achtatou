package im.getsocial.sdk.promocodes.a.b;

import im.getsocial.sdk.Callback;
import im.getsocial.sdk.internal.c.k.jjbQypPegg;
import im.getsocial.sdk.internal.c.l.jjbQypPegg;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import java.util.Date;

/* compiled from: CreatePromoCodeUseCase */
public final class upgqDBbsrL extends jjbQypPegg {
    public final void getsocial(final PromoCodeBuilder promoCodeBuilder, Callback<PromoCode> callback) {
        getsocial(new jjbQypPegg.C0020jjbQypPegg<PromoCode>() {
            public final /* bridge */ /* synthetic */ Object getsocial() {
                upgqDBbsrL.getsocial(promoCodeBuilder);
                return upgqDBbsrL.this.getsocial().getsocial(promoCodeBuilder);
            }
        }, callback);
    }

    static /* synthetic */ void getsocial(PromoCodeBuilder promoCodeBuilder) {
        if (promoCodeBuilder.getEndDate() != null) {
            if (promoCodeBuilder.getStartDate() != null) {
                jjbQypPegg.C0021jjbQypPegg.getsocial(promoCodeBuilder.getStartDate().before(promoCodeBuilder.getEndDate()), "Start time can not be after end time");
            }
            jjbQypPegg.C0021jjbQypPegg.getsocial(promoCodeBuilder.getEndDate().after(new Date()), "End time can not be before current date");
        }
    }
}
