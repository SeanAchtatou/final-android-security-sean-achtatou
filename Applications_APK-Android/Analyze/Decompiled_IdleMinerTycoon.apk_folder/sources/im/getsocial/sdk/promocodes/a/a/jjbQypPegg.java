package im.getsocial.sdk.promocodes.a.a;

import im.getsocial.sdk.internal.c.l.upgqDBbsrL;
import im.getsocial.sdk.internal.f.a.xAXgtBkRbG;
import im.getsocial.sdk.internal.m.QCXFOjcJkE;
import im.getsocial.sdk.promocodes.PromoCode;
import im.getsocial.sdk.promocodes.PromoCodeBuilder;
import java.util.Date;
import java.util.HashMap;

/* compiled from: PromoCodeThriftyConverter */
public final class jjbQypPegg {
    private jjbQypPegg() {
    }

    public static xAXgtBkRbG getsocial(PromoCodeBuilder promoCodeBuilder) {
        xAXgtBkRbG xaxgtbkrbg = new xAXgtBkRbG();
        xaxgtbkrbg.mobile = promoCodeBuilder.getCode();
        xaxgtbkrbg.retention = new HashMap(promoCodeBuilder.getData());
        xaxgtbkrbg.dau = Integer.valueOf(promoCodeBuilder.getMaxClaimCount());
        xaxgtbkrbg.cat = getsocial(promoCodeBuilder.getStartDate());
        xaxgtbkrbg.viral = getsocial(promoCodeBuilder.getEndDate());
        return xaxgtbkrbg;
    }

    public static PromoCode getsocial(xAXgtBkRbG xaxgtbkrbg) {
        return PromoCode.builder(xaxgtbkrbg.mobile).withStartDate(getsocial(xaxgtbkrbg.cat)).withEndDate(getsocial(xaxgtbkrbg.viral)).withIsClaimable(QCXFOjcJkE.getsocial(xaxgtbkrbg.unity)).withIsEnabled(QCXFOjcJkE.getsocial(xaxgtbkrbg.ios)).withMaxClaimCount(QCXFOjcJkE.getsocial(xaxgtbkrbg.dau)).withClaimCount(QCXFOjcJkE.getsocial(xaxgtbkrbg.mau)).withCreator(im.getsocial.sdk.usermanagement.a.d.jjbQypPegg.getsocial(xaxgtbkrbg.acquisition)).addData(upgqDBbsrL.getsocial(xaxgtbkrbg.retention)).build();
    }

    private static Long getsocial(Date date) {
        if (date == null) {
            return null;
        }
        return Long.valueOf(date.getTime() / 1000);
    }

    private static Date getsocial(Long l) {
        if (l == null) {
            return null;
        }
        return new Date(l.longValue() * 1000);
    }
}
