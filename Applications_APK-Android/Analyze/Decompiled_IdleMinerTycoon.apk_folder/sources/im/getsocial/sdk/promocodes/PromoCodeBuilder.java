package im.getsocial.sdk.promocodes;

import im.getsocial.sdk.internal.m.ztWNWCuZiM;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class PromoCodeBuilder {
    private int acquisition;
    private final Map<String, String> attribution = new HashMap();
    private final String getsocial;
    private Date mobile;
    private Date retention;

    private PromoCodeBuilder(String str) {
        this.getsocial = str;
    }

    public static PromoCodeBuilder createRandomCode() {
        return new PromoCodeBuilder(null);
    }

    public static PromoCodeBuilder createWithCode(String str) {
        return new PromoCodeBuilder(str);
    }

    public final PromoCodeBuilder withMaxClaimCount(int i) {
        this.acquisition = i;
        return this;
    }

    public final PromoCodeBuilder withTimeLimit(Date date, Date date2) {
        this.mobile = ztWNWCuZiM.getsocial(date);
        this.retention = ztWNWCuZiM.getsocial(date2);
        return this;
    }

    public final PromoCodeBuilder addData(String str, String str2) {
        this.attribution.put(str, str2);
        return this;
    }

    public final PromoCodeBuilder addData(Map<String, String> map) {
        this.attribution.putAll(map);
        return this;
    }

    public final String getCode() {
        return this.getsocial;
    }

    public final Map<String, String> getData() {
        return this.attribution;
    }

    public final int getMaxClaimCount() {
        return this.acquisition;
    }

    public final Date getStartDate() {
        return ztWNWCuZiM.getsocial(this.mobile);
    }

    public final Date getEndDate() {
        return ztWNWCuZiM.getsocial(this.retention);
    }
}
