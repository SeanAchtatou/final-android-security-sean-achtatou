package im.getsocial.sdk;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

public class GetSocialException extends RuntimeException {
    private static final List<String> attribution;
    private final int getsocial;

    static {
        ArrayList arrayList = new ArrayList();
        attribution = arrayList;
        arrayList.add(ThreadPoolExecutor.class.getSimpleName());
    }

    public GetSocialException(int i, String str) {
        this(i, str, null);
    }

    public GetSocialException(int i, String str, Throwable th) {
        super(str, th);
        this.getsocial = i;
        if (th != null) {
            setStackTrace(th.getStackTrace());
        }
    }

    public int getErrorCode() {
        return this.getsocial;
    }

    public int hashCode() {
        return this.getsocial;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && getClass() == obj.getClass() && this.getsocial == ((GetSocialException) obj).getsocial;
    }

    public void printStackTrace(PrintWriter printWriter) {
        for (StackTraceElement stackTraceElement : getStackTrace()) {
            boolean z = true;
            for (String contains : attribution) {
                if (stackTraceElement.getClassName().contains(contains)) {
                    z = false;
                }
            }
            if (z) {
                printWriter.println(stackTraceElement);
            }
        }
    }
}
