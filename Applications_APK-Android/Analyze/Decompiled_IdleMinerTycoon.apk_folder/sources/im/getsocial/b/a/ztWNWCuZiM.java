package im.getsocial.b.a;

import im.getsocial.b.a.a.upgqDBbsrL;
import im.getsocial.b.a.a.zoToeBNOjF;

/* compiled from: ThriftException */
public class ztWNWCuZiM extends RuntimeException {
    public final jjbQypPegg getsocial;

    /* compiled from: ThriftException */
    public enum jjbQypPegg {
        UNKNOWN(0),
        UNKNOWN_METHOD(1),
        INVALID_MESSAGE_TYPE(2),
        WRONG_METHOD_NAME(3),
        BAD_SEQUENCE_ID(4),
        MISSING_RESULT(5),
        INTERNAL_ERROR(6),
        PROTOCOL_ERROR(7),
        INVALID_TRANSFORM(8),
        INVALID_PROTOCOL(9),
        UNSUPPORTED_CLIENT_TYPE(10);
        
        final int value;

        private jjbQypPegg(int i) {
            this.value = i;
        }

        static jjbQypPegg findByValue(int i) {
            for (jjbQypPegg jjbqyppegg : values()) {
                if (jjbqyppegg.value == i) {
                    return jjbqyppegg;
                }
            }
            return UNKNOWN;
        }
    }

    public ztWNWCuZiM(jjbQypPegg jjbqyppegg, String str) {
        super(str);
        this.getsocial = jjbqyppegg;
    }

    public static ztWNWCuZiM getsocial(zoToeBNOjF zotoebnojf) {
        jjbQypPegg jjbqyppegg = jjbQypPegg.UNKNOWN;
        String str = null;
        while (true) {
            upgqDBbsrL acquisition = zotoebnojf.acquisition();
            if (acquisition.attribution != 0) {
                switch (acquisition.acquisition) {
                    case 1:
                        if (acquisition.attribution != 11) {
                            im.getsocial.b.a.d.jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            str = zotoebnojf.ios();
                            break;
                        }
                    case 2:
                        if (acquisition.attribution != 8) {
                            im.getsocial.b.a.d.jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                            break;
                        } else {
                            jjbqyppegg = jjbQypPegg.findByValue(zotoebnojf.organic());
                            break;
                        }
                    default:
                        im.getsocial.b.a.d.jjbQypPegg.getsocial(zotoebnojf, acquisition.attribution);
                        break;
                }
            } else {
                return new ztWNWCuZiM(jjbqyppegg, str);
            }
        }
    }
}
