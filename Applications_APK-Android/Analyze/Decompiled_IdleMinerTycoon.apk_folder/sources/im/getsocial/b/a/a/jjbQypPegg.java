package im.getsocial.b.a.a;

import com.ironsource.sdk.constants.Constants;
import java.io.EOFException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;

/* compiled from: BinaryProtocol */
public class jjbQypPegg extends zoToeBNOjF {
    private static final KSZKMmRWhZ attribution = new KSZKMmRWhZ("");
    private final long acquisition;
    private final long mobile;
    private final byte[] retention;

    public jjbQypPegg(im.getsocial.b.a.c.jjbQypPegg jjbqyppegg) {
        this(jjbqyppegg, -1, -1);
    }

    private jjbQypPegg(im.getsocial.b.a.c.jjbQypPegg jjbqyppegg, int i, int i2) {
        super(jjbqyppegg);
        this.retention = new byte[8];
        this.acquisition = -1;
        this.mobile = -1;
    }

    public final void getsocial(String str, byte b, int i) {
        getsocial(str);
        getsocial(b);
        getsocial(i);
    }

    public final void getsocial(int i, byte b) {
        getsocial(b);
        short s = (short) i;
        this.retention[0] = (byte) (s >> 8);
        this.retention[1] = (byte) s;
        this.getsocial.attribution(this.retention, 0, 2);
    }

    public final void getsocial() {
        getsocial((byte) 0);
    }

    public final void getsocial(byte b, byte b2, int i) {
        getsocial(b);
        getsocial(b2);
        getsocial(i);
    }

    public final void getsocial(byte b, int i) {
        getsocial(b);
        getsocial(i);
    }

    public final void getsocial(boolean z) {
        getsocial(z ? (byte) 1 : 0);
    }

    private void getsocial(byte b) {
        this.retention[0] = b;
        this.getsocial.attribution(this.retention, 0, 1);
    }

    public final void getsocial(int i) {
        this.retention[0] = (byte) (i >>> 24);
        this.retention[1] = (byte) (i >> 16);
        this.retention[2] = (byte) (i >> 8);
        this.retention[3] = (byte) i;
        this.getsocial.attribution(this.retention, 0, 4);
    }

    public final void getsocial(long j) {
        this.retention[0] = (byte) ((int) ((j >> 56) & 255));
        this.retention[1] = (byte) ((int) ((j >> 48) & 255));
        this.retention[2] = (byte) ((int) ((j >> 40) & 255));
        this.retention[3] = (byte) ((int) ((j >> 32) & 255));
        this.retention[4] = (byte) ((int) ((j >> 24) & 255));
        this.retention[5] = (byte) ((int) ((j >> 16) & 255));
        this.retention[6] = (byte) ((int) ((j >> 8) & 255));
        this.retention[7] = (byte) ((int) (j & 255));
        this.getsocial.attribution(this.retention, 0, 8);
    }

    public final void getsocial(String str) {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            getsocial(bytes.length);
            this.getsocial.getsocial(bytes);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public final void getsocial(byte[] bArr) {
        getsocial(bArr.length);
        this.getsocial.getsocial(bArr);
    }

    public final XdbacJlTDQ attribution() {
        int organic = organic();
        if (organic >= 0) {
            return new XdbacJlTDQ(attribution(organic), cat(), organic());
        }
        if ((-65536 & organic) == -2147418112) {
            return new XdbacJlTDQ(ios(), (byte) organic, organic());
        }
        throw new ProtocolException("Bad version in readMessageBegin");
    }

    public final upgqDBbsrL acquisition() {
        short s;
        byte cat = cat();
        if (cat == 0) {
            s = 0;
        } else {
            s = viral();
        }
        return new upgqDBbsrL("", cat, s);
    }

    public final pdwpUtZXDT mobile() {
        byte cat = cat();
        byte cat2 = cat();
        int organic = organic();
        if (this.mobile == -1 || ((long) organic) <= this.mobile) {
            return new pdwpUtZXDT(cat, cat2, organic);
        }
        throw new ProtocolException("Container size limit exceeded");
    }

    public final cjrhisSQCL retention() {
        byte cat = cat();
        int organic = organic();
        if (this.mobile == -1 || ((long) organic) <= this.mobile) {
            return new cjrhisSQCL(cat, organic);
        }
        throw new ProtocolException("Container size limit exceeded");
    }

    public final ztWNWCuZiM dau() {
        byte cat = cat();
        int organic = organic();
        if (this.mobile == -1 || ((long) organic) <= this.mobile) {
            return new ztWNWCuZiM(cat, organic);
        }
        throw new ProtocolException("Container size limit exceeded");
    }

    public final boolean mau() {
        return cat() == 1;
    }

    public final byte cat() {
        getsocial(this.retention, 1);
        return this.retention[0];
    }

    public final short viral() {
        getsocial(this.retention, 2);
        return (short) (((this.retention[0] & 255) << 8) | (this.retention[1] & 255));
    }

    public final int organic() {
        getsocial(this.retention, 4);
        return ((this.retention[0] & 255) << 24) | ((this.retention[1] & 255) << 16) | ((this.retention[2] & 255) << 8) | (this.retention[3] & 255);
    }

    public final long growth() {
        getsocial(this.retention, 8);
        return ((((long) this.retention[6]) & 255) << 8) | ((((long) this.retention[0]) & 255) << 56) | ((((long) this.retention[1]) & 255) << 48) | ((((long) this.retention[2]) & 255) << 40) | ((((long) this.retention[3]) & 255) << 32) | ((((long) this.retention[4]) & 255) << 24) | ((((long) this.retention[5]) & 255) << 16) | (((long) this.retention[7]) & 255);
    }

    public final double android() {
        return Double.longBitsToDouble(growth());
    }

    public final String ios() {
        int organic = organic();
        if (this.acquisition == -1 || ((long) organic) <= this.acquisition) {
            return attribution(organic);
        }
        throw new ProtocolException("String size limit exceeded");
    }

    private String attribution(int i) {
        if (i <= 1048576) {
            byte[] bArr = new byte[i];
            getsocial(bArr, i);
            return new String(bArr, "UTF-8");
        }
        throw new IOException("Invalid string size value received: [" + i + Constants.RequestParameters.RIGHT_BRACKETS);
    }

    private void getsocial(byte[] bArr, int i) {
        int i2 = i;
        int i3 = 0;
        while (i2 > 0) {
            int i4 = this.getsocial.getsocial(bArr, i3, i2);
            if (i4 != -1) {
                i2 -= i4;
                i3 += i4;
            } else {
                throw new EOFException("Expected " + i + " bytes; got " + i3);
            }
        }
    }
}
