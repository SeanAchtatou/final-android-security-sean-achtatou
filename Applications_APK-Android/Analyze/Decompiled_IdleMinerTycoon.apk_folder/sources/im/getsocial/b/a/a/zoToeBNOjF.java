package im.getsocial.b.a.a;

import android.support.v4.app.NotificationCompat;
import im.getsocial.b.a.c.jjbQypPegg;
import java.io.Closeable;

/* compiled from: Protocol */
public abstract class zoToeBNOjF implements Closeable {
    protected final jjbQypPegg getsocial;

    public abstract upgqDBbsrL acquisition();

    public abstract double android();

    public abstract XdbacJlTDQ attribution();

    public abstract byte cat();

    public abstract ztWNWCuZiM dau();

    public abstract void getsocial();

    public abstract void getsocial(byte b, byte b2, int i);

    public abstract void getsocial(byte b, int i);

    public abstract void getsocial(int i);

    public abstract void getsocial(int i, byte b);

    public abstract void getsocial(long j);

    public abstract void getsocial(String str);

    public abstract void getsocial(String str, byte b, int i);

    public abstract void getsocial(boolean z);

    public abstract void getsocial(byte[] bArr);

    public abstract long growth();

    public abstract String ios();

    public abstract boolean mau();

    public abstract pdwpUtZXDT mobile();

    public abstract int organic();

    public abstract cjrhisSQCL retention();

    public abstract short viral();

    protected zoToeBNOjF(jjbQypPegg jjbqyppegg) {
        if (jjbqyppegg != null) {
            this.getsocial = jjbqyppegg;
            return;
        }
        throw new NullPointerException(NotificationCompat.CATEGORY_TRANSPORT);
    }

    public final void unity() {
        this.getsocial.getsocial();
    }

    public void close() {
        this.getsocial.close();
    }
}
