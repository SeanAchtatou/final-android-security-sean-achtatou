package im.getsocial.b.a.c;

import java.io.Closeable;

/* compiled from: Transport */
public abstract class jjbQypPegg implements Closeable {
    public abstract void attribution(byte[] bArr, int i, int i2);

    public abstract int getsocial(byte[] bArr, int i, int i2);

    public abstract void getsocial();

    public final void getsocial(byte[] bArr) {
        attribution(bArr, 0, bArr.length);
    }
}
