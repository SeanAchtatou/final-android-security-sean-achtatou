package im.getsocial.b.a.a;

/* compiled from: MessageMetadata */
public class XdbacJlTDQ {
    public final int acquisition;
    public final byte attribution;
    public final String getsocial;

    public XdbacJlTDQ(String str, byte b, int i) {
        this.getsocial = str == null ? "" : str;
        this.attribution = b;
        this.acquisition = i;
    }
}
