package im.getsocial.b.a.b;

import im.getsocial.b.a.a.XdbacJlTDQ;
import im.getsocial.b.a.a.zoToeBNOjF;
import im.getsocial.b.a.ztWNWCuZiM;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ClientBase */
public class jjbQypPegg implements Closeable {
    private final zoToeBNOjF acquisition;
    private final AtomicInteger attribution = new AtomicInteger(0);
    final AtomicBoolean getsocial = new AtomicBoolean(true);

    protected jjbQypPegg(zoToeBNOjF zotoebnojf) {
        this.acquisition = zotoebnojf;
    }

    /* access modifiers changed from: protected */
    public final <T> T getsocial(upgqDBbsrL upgqdbbsrl) {
        if (this.getsocial.get()) {
            try {
                boolean z = upgqdbbsrl.attribution == 4;
                int incrementAndGet = this.attribution.incrementAndGet();
                this.acquisition.getsocial(upgqdbbsrl.getsocial, upgqdbbsrl.attribution, incrementAndGet);
                upgqdbbsrl.getsocial(this.acquisition);
                this.acquisition.unity();
                if (z) {
                    return null;
                }
                XdbacJlTDQ attribution2 = this.acquisition.attribution();
                if (attribution2.acquisition != incrementAndGet) {
                    throw new ztWNWCuZiM(ztWNWCuZiM.jjbQypPegg.BAD_SEQUENCE_ID, "Unrecognized sequence ID");
                } else if (attribution2.attribution == 3) {
                    throw new C0073jjbQypPegg(ztWNWCuZiM.getsocial(this.acquisition));
                } else if (attribution2.attribution != 2) {
                    ztWNWCuZiM.jjbQypPegg jjbqyppegg = ztWNWCuZiM.jjbQypPegg.INVALID_MESSAGE_TYPE;
                    throw new ztWNWCuZiM(jjbqyppegg, "Invalid message type: " + ((int) attribution2.attribution));
                } else if (attribution2.acquisition != this.attribution.get()) {
                    throw new ztWNWCuZiM(ztWNWCuZiM.jjbQypPegg.BAD_SEQUENCE_ID, "Out-of-order response");
                } else if (attribution2.getsocial.equals(upgqdbbsrl.getsocial)) {
                    return upgqdbbsrl.getsocial(this.acquisition, attribution2);
                } else {
                    ztWNWCuZiM.jjbQypPegg jjbqyppegg2 = ztWNWCuZiM.jjbQypPegg.WRONG_METHOD_NAME;
                    throw new ztWNWCuZiM(jjbqyppegg2, "Unexpected method name in reply; expected " + upgqdbbsrl.getsocial + " but received " + attribution2.getsocial);
                }
            } catch (C0073jjbQypPegg e) {
                throw e.getsocial;
            }
        } else {
            throw new IllegalStateException("Cannot write to a closed service client");
        }
    }

    public void close() {
        if (this.getsocial.compareAndSet(true, false)) {
            try {
                this.acquisition.close();
            } catch (IOException unused) {
            }
        }
    }

    /* renamed from: im.getsocial.b.a.b.jjbQypPegg$jjbQypPegg  reason: collision with other inner class name */
    /* compiled from: ClientBase */
    static class C0073jjbQypPegg extends Exception {
        final ztWNWCuZiM getsocial;

        C0073jjbQypPegg(ztWNWCuZiM ztwnwcuzim) {
            this.getsocial = ztwnwcuzim;
        }
    }
}
