package im.getsocial.c.a;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.annotation.Nonnull;

/* compiled from: TusClient */
public class zoToeBNOjF {
    private qZypgoeblR acquisition;
    private boolean attribution;
    private URL getsocial;
    private int mobile = 5000;

    public final void getsocial(URL url) {
        this.getsocial = url;
    }

    public final void getsocial(@Nonnull qZypgoeblR qzypgoeblr) {
        this.attribution = true;
        this.acquisition = qzypgoeblr;
    }

    private KluUZYuxme attribution(@Nonnull fOrCGNYyfk forcgnyyfk) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) this.getsocial.openConnection();
        httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
        getsocial(httpURLConnection);
        String acquisition2 = forcgnyyfk.acquisition();
        if (acquisition2.length() > 0) {
            httpURLConnection.setRequestProperty("Upload-Metadata", acquisition2);
        }
        httpURLConnection.addRequestProperty("Upload-Length", Long.toString(forcgnyyfk.getsocial()));
        httpURLConnection.connect();
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode < 200 || responseCode >= 300) {
            throw new cjrhisSQCL("unexpected status code (" + responseCode + ") while creating upload", httpURLConnection);
        }
        String headerField = httpURLConnection.getHeaderField("Location");
        if (headerField == null || headerField.length() == 0) {
            throw new cjrhisSQCL("missing upload URL in response for creating upload", httpURLConnection);
        }
        URL url = new URL(httpURLConnection.getURL(), headerField);
        HttpCookie attribution2 = attribution(httpURLConnection);
        if (this.attribution) {
            this.acquisition.getsocial((String) null, url);
            if (attribution2 != null) {
                this.acquisition.getsocial((String) null, attribution2);
            }
        }
        return new KluUZYuxme(url, forcgnyyfk.attribution(), 0, attribution2);
    }

    private void getsocial(@Nonnull HttpURLConnection httpURLConnection) {
        httpURLConnection.setInstanceFollowRedirects(Boolean.getBoolean("http.strictPostRedirect"));
        httpURLConnection.setConnectTimeout(this.mobile);
        httpURLConnection.addRequestProperty("Tus-Resumable", "1.0.0");
    }

    private static HttpCookie attribution(@Nonnull HttpURLConnection httpURLConnection) {
        List<String> list = httpURLConnection.getHeaderFields().get("Set-Cookie");
        if (list == null) {
            return null;
        }
        for (String parse : list) {
            HttpCookie httpCookie = HttpCookie.parse(parse).get(0);
            if (httpCookie != null && "AWSALB".equalsIgnoreCase(httpCookie.getName())) {
                return httpCookie;
            }
        }
        return null;
    }

    public final void getsocial(String str) {
        if (this.attribution) {
            this.acquisition.acquisition(str);
        }
    }

    public final HttpCookie attribution(String str) {
        if (this.attribution) {
            return this.acquisition.attribution(str);
        }
        return null;
    }

    public final KluUZYuxme getsocial(@Nonnull fOrCGNYyfk forcgnyyfk) {
        try {
            if (this.attribution) {
                URL url = this.acquisition.getsocial(null);
                if (url != null) {
                    HttpCookie attribution2 = this.acquisition.attribution(null);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_HEAD);
                    getsocial(httpURLConnection);
                    if (attribution2 != null) {
                        httpURLConnection.setRequestProperty("Cookie", attribution2.toString());
                    }
                    httpURLConnection.connect();
                    int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode < 200 || responseCode >= 300) {
                        throw new cjrhisSQCL("unexpected status code (" + responseCode + ") while resuming upload", httpURLConnection);
                    }
                    HttpCookie attribution3 = attribution(httpURLConnection);
                    if (this.attribution) {
                        this.acquisition.getsocial((String) null, attribution3);
                    }
                    String headerField = httpURLConnection.getHeaderField("Upload-Offset");
                    if (headerField == null || headerField.length() == 0) {
                        throw new cjrhisSQCL("missing upload offset in response for resuming upload", httpURLConnection);
                    }
                    return new KluUZYuxme(url, forcgnyyfk.attribution(), Long.parseLong(headerField), attribution3);
                }
                throw new jjbQypPegg(null);
            }
            throw new XdbacJlTDQ();
        } catch (XdbacJlTDQ | jjbQypPegg unused) {
            return attribution(forcgnyyfk);
        } catch (cjrhisSQCL e) {
            HttpURLConnection httpURLConnection2 = e.getsocial();
            if (httpURLConnection2 != null && httpURLConnection2.getResponseCode() == 404) {
                return attribution(forcgnyyfk);
            }
            throw e;
        }
    }
}
