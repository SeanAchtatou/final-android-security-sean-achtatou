package im.getsocial.c.a;

import java.io.IOException;
import java.net.HttpURLConnection;

/* compiled from: ProtocolException */
public class cjrhisSQCL extends Exception {
    private HttpURLConnection getsocial;

    public cjrhisSQCL(String str, HttpURLConnection httpURLConnection) {
        super(str);
        this.getsocial = httpURLConnection;
    }

    public final HttpURLConnection getsocial() {
        return this.getsocial;
    }

    public final boolean attribution() {
        if (this.getsocial == null) {
            return false;
        }
        try {
            int responseCode = this.getsocial.getResponseCode();
            if ((responseCode < 500 || responseCode >= 600) && responseCode != 423) {
                return false;
            }
            return true;
        } catch (IOException unused) {
            return false;
        }
    }
}
