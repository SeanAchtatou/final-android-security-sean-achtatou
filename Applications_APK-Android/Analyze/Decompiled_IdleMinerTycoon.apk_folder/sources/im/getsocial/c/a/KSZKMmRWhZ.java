package im.getsocial.c.a;

import java.io.BufferedInputStream;
import java.io.InputStream;

/* compiled from: TusInputStream */
class KSZKMmRWhZ {
    private long acquisition = -1;
    private long attribution;
    private InputStream getsocial;

    public KSZKMmRWhZ(InputStream inputStream) {
        this.getsocial = !inputStream.markSupported() ? new BufferedInputStream(inputStream) : inputStream;
    }

    public final int getsocial(byte[] bArr, int i) {
        int read = this.getsocial.read(bArr, 0, i);
        this.attribution += (long) read;
        return read;
    }

    public final void getsocial(long j) {
        if (this.acquisition != -1) {
            this.getsocial.reset();
            this.getsocial.skip(j - this.acquisition);
            this.acquisition = -1;
        } else {
            this.getsocial.skip(j);
        }
        this.attribution = j;
    }

    public final void getsocial(int i) {
        this.acquisition = this.attribution;
        this.getsocial.mark(i);
    }

    public final void getsocial() {
        this.getsocial.close();
    }
}
