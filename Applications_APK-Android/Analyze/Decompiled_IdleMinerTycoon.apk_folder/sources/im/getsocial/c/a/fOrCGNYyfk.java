package im.getsocial.c.a;

import java.io.InputStream;
import java.util.Map;

/* compiled from: TusUpload */
public class fOrCGNYyfk {
    private KSZKMmRWhZ acquisition;
    private InputStream attribution;
    private long getsocial;
    private Map<String, String> mobile;

    public final long getsocial() {
        return this.getsocial;
    }

    public final void getsocial(long j) {
        this.getsocial = j;
    }

    public final KSZKMmRWhZ attribution() {
        return this.acquisition;
    }

    public final void getsocial(InputStream inputStream) {
        this.attribution = inputStream;
        this.acquisition = new KSZKMmRWhZ(inputStream);
    }

    public final void getsocial(Map<String, String> map) {
        this.mobile = map;
    }

    public final String acquisition() {
        if (this.mobile == null || this.mobile.size() == 0) {
            return "";
        }
        String str = "";
        boolean z = true;
        for (Map.Entry next : this.mobile.entrySet()) {
            if (!z) {
                str = str + ",";
            }
            str = str + ((String) next.getKey()) + " " + getsocial(((String) next.getValue()).getBytes());
            z = false;
        }
        return str;
    }

    private static String getsocial(byte[] bArr) {
        StringBuilder sb = new StringBuilder((bArr.length << 2) / 3);
        for (int i = 0; i < bArr.length; i += 3) {
            sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt((bArr[i] & 252) >> 2));
            int i2 = (bArr[i] & 3) << 4;
            int i3 = i + 1;
            if (i3 < bArr.length) {
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(i2 | ((bArr[i3] & 240) >> 4)));
                int i4 = (bArr[i3] & 15) << 2;
                int i5 = i + 2;
                if (i5 < bArr.length) {
                    sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(i4 | ((bArr[i5] & 192) >> 6)));
                    sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(bArr[i5] & 63));
                } else {
                    sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(i4));
                    sb.append('=');
                }
            } else {
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(i2));
                sb.append("==");
            }
        }
        return sb.toString();
    }
}
