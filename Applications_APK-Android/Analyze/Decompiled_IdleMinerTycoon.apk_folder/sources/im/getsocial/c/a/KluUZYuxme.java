package im.getsocial.c.a;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.OutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;

/* compiled from: TusUploader */
public class KluUZYuxme {
    private long acquisition;
    private KSZKMmRWhZ attribution;
    private HttpURLConnection cat;
    private int dau;
    private URL getsocial;
    private HttpCookie mau;
    private byte[] mobile;
    private int retention = 1073741824;
    private OutputStream viral;

    public KluUZYuxme(URL url, KSZKMmRWhZ kSZKMmRWhZ, long j, HttpCookie httpCookie) {
        this.getsocial = url;
        this.attribution = kSZKMmRWhZ;
        this.acquisition = j;
        this.mau = httpCookie;
        kSZKMmRWhZ.getsocial(j);
        this.mobile = new byte[2097152];
    }

    public final void getsocial(int i) {
        this.mobile = new byte[i];
    }

    public final long attribution() {
        return this.acquisition;
    }

    public final URL acquisition() {
        return this.getsocial;
    }

    public final void mobile() {
        retention();
        this.attribution.getsocial();
    }

    private void retention() {
        if (this.viral != null) {
            this.viral.close();
        }
        if (this.cat != null) {
            int responseCode = this.cat.getResponseCode();
            this.cat.disconnect();
            if (responseCode < 200 || responseCode >= 300) {
                throw new cjrhisSQCL("unexpected status code (" + responseCode + ") while uploading chunk", this.cat);
            }
            long j = getsocial(this.cat, "Upload-Offset");
            if (j == -1) {
                throw new cjrhisSQCL("response to PATCH request contains no or invalid Upload-Offset header", this.cat);
            } else if (this.acquisition == j) {
                this.cat = null;
            } else {
                throw new cjrhisSQCL(String.format(Locale.ENGLISH, "response contains different Upload-Offset value (%d) than expected (%d)", Long.valueOf(j), Long.valueOf(this.acquisition)), this.cat);
            }
        }
    }

    private static long getsocial(URLConnection uRLConnection, String str) {
        String headerField = uRLConnection.getHeaderField(str);
        if (headerField == null) {
            return -1;
        }
        try {
            return Long.parseLong(headerField);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    public final int getsocial() {
        if (this.cat == null) {
            this.dau = this.retention;
            this.attribution.getsocial(this.retention);
            this.cat = (HttpURLConnection) this.getsocial.openConnection();
            if (this.mau != null) {
                this.cat.setRequestProperty("Cookie", this.mau.toString());
            }
            this.cat.setRequestProperty("Upload-Offset", Long.toString(this.acquisition));
            this.cat.setRequestProperty(HttpRequest.HEADER_CONTENT_TYPE, "application/offset+octet-stream");
            this.cat.setRequestProperty("Expect", "100-continue");
            this.cat.addRequestProperty("Tus-Resumable", "1.0.0");
            this.cat.setReadTimeout(0);
            try {
                this.cat.setRequestMethod("PATCH");
            } catch (ProtocolException unused) {
                this.cat.setRequestMethod(HttpRequest.METHOD_POST);
                this.cat.setRequestProperty("X-HTTP-Method-Override", "PATCH");
            }
            this.cat.setDoOutput(true);
            this.cat.setChunkedStreamingMode(this.mobile.length);
            try {
                this.viral = this.cat.getOutputStream();
            } catch (ProtocolException e) {
                if (this.cat.getResponseCode() != -1) {
                    mobile();
                }
                throw e;
            }
        }
        int i = this.attribution.getsocial(this.mobile, Math.min(this.mobile.length, this.dau));
        if (i == -1) {
            return -1;
        }
        this.viral.write(this.mobile, 0, i);
        this.viral.flush();
        this.acquisition += (long) i;
        this.dau -= i;
        if (this.dau <= 0) {
            retention();
        }
        return i;
    }
}
