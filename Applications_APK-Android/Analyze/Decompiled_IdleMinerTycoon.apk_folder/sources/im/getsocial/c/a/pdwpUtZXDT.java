package im.getsocial.c.a;

import com.ironsource.sdk.constants.Constants;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ReadyChecker */
public abstract class pdwpUtZXDT {
    /* access modifiers changed from: private */
    public final String acquisition;
    /* access modifiers changed from: private */
    public final String attribution;
    /* access modifiers changed from: private */
    public int dau = 1000;
    /* access modifiers changed from: private */
    public final zoToeBNOjF getsocial;
    private int mau = 8;
    /* access modifiers changed from: private */
    public int mobile = 5000;
    /* access modifiers changed from: private */
    public int retention = 5000;

    /* access modifiers changed from: protected */
    public abstract void getsocial(upgqDBbsrL upgqdbbsrl);

    /* access modifiers changed from: protected */
    public abstract void getsocial(Map<String, String> map);

    static /* synthetic */ int mau(pdwpUtZXDT pdwputzxdt) {
        int i = pdwputzxdt.mau;
        pdwputzxdt.mau = i - 1;
        return i;
    }

    public pdwpUtZXDT(zoToeBNOjF zotoebnojf, String str, int i, String str2) {
        this.getsocial = zotoebnojf;
        this.attribution = str;
        this.mau = i;
        this.acquisition = str2;
        new Thread(new Runnable() {
            /* JADX WARNING: Code restructure failed: missing block: B:15:0x0071, code lost:
                if (r2 != 200) goto L_0x0097;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
                r4.getsocial.getsocial(im.getsocial.c.a.pdwpUtZXDT.getsocial(r4.getsocial, r1.getHeaderField("Getsocial-Resource")));
                r1.disconnect();
                im.getsocial.c.a.pdwpUtZXDT.retention(r4.getsocial).getsocial(im.getsocial.c.a.pdwpUtZXDT.mobile(r4.getsocial));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:18:0x0096, code lost:
                return;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:19:0x0097, code lost:
                r1.disconnect();
                im.getsocial.c.a.pdwpUtZXDT.retention(r4.getsocial).getsocial(im.getsocial.c.a.pdwpUtZXDT.mobile(r4.getsocial));
                r4.getsocial.getsocial(new im.getsocial.c.a.upgqDBbsrL(im.getsocial.c.a.pdwpUtZXDT.attribution(r4.getsocial), r2));
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r4 = this;
                    r0 = 0
                L_0x0001:
                    if (r0 != 0) goto L_0x0011
                    im.getsocial.c.a.pdwpUtZXDT r1 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    int r1 = r1.dau     // Catch:{ IOException | InterruptedException -> 0x000e }
                    long r1 = (long) r1     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.Thread.sleep(r1)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    goto L_0x0011
                L_0x000e:
                    r0 = move-exception
                    goto L_0x00ba
                L_0x0011:
                    java.net.URL r1 = new java.net.URL     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r2 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r2 = r2.attribution     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r1.<init>(r2)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.net.URLConnection r1 = r1.openConnection()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r2 = "GET"
                    r1.setRequestMethod(r2)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r2 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    int r2 = r2.mobile     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r1.setConnectTimeout(r2)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r2 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.zoToeBNOjF r2 = r2.getsocial     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r3 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r3 = r3.acquisition     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.net.HttpCookie r2 = r2.attribution(r3)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    if (r2 == 0) goto L_0x004b
                    java.lang.String r3 = "Cookie"
                    java.lang.String r2 = r2.toString()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r1.setRequestProperty(r3, r2)     // Catch:{ IOException | InterruptedException -> 0x000e }
                L_0x004b:
                    r1.connect()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    int r2 = r1.getResponseCode()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r3 = 204(0xcc, float:2.86E-43)
                    if (r2 != r3) goto L_0x006f
                    r1.disconnect()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r1 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    int r1 = r1.retention     // Catch:{ IOException | InterruptedException -> 0x000e }
                    long r1 = (long) r1     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.Thread.sleep(r1)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    int r0 = r0 + 1
                    im.getsocial.c.a.pdwpUtZXDT r1 = im.getsocial.c.a.pdwpUtZXDT.this
                    int r1 = im.getsocial.c.a.pdwpUtZXDT.mau(r1)
                    r2 = 1
                    if (r1 > r2) goto L_0x0001
                    goto L_0x00c4
                L_0x006f:
                    r0 = 200(0xc8, float:2.8E-43)
                    if (r2 != r0) goto L_0x0097
                    im.getsocial.c.a.pdwpUtZXDT r0 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r2 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r3 = "Getsocial-Resource"
                    java.lang.String r3 = r1.getHeaderField(r3)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.util.Map r2 = im.getsocial.c.a.pdwpUtZXDT.getsocial(r2, r3)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r0.getsocial(r2)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r1.disconnect()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r0 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.zoToeBNOjF r0 = r0.getsocial     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r1 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r1 = r1.acquisition     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r0.getsocial(r1)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    return
                L_0x0097:
                    r1.disconnect()     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r0 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.zoToeBNOjF r0 = r0.getsocial     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r1 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r1 = r1.acquisition     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r0.getsocial(r1)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r0 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.upgqDBbsrL r1 = new im.getsocial.c.a.upgqDBbsrL     // Catch:{ IOException | InterruptedException -> 0x000e }
                    im.getsocial.c.a.pdwpUtZXDT r3 = im.getsocial.c.a.pdwpUtZXDT.this     // Catch:{ IOException | InterruptedException -> 0x000e }
                    java.lang.String r3 = r3.attribution     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r1.<init>(r3, r2)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    r0.getsocial(r1)     // Catch:{ IOException | InterruptedException -> 0x000e }
                    goto L_0x00c4
                L_0x00ba:
                    im.getsocial.c.a.pdwpUtZXDT r1 = im.getsocial.c.a.pdwpUtZXDT.this
                    im.getsocial.c.a.upgqDBbsrL r2 = new im.getsocial.c.a.upgqDBbsrL
                    r2.<init>(r0)
                    r1.getsocial(r2)
                L_0x00c4:
                    im.getsocial.c.a.pdwpUtZXDT r0 = im.getsocial.c.a.pdwpUtZXDT.this
                    im.getsocial.c.a.upgqDBbsrL r1 = new im.getsocial.c.a.upgqDBbsrL
                    im.getsocial.c.a.pdwpUtZXDT r2 = im.getsocial.c.a.pdwpUtZXDT.this
                    java.lang.String r2 = r2.attribution
                    r1.<init>(r2)
                    r0.getsocial(r1)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: im.getsocial.c.a.pdwpUtZXDT.AnonymousClass1.run():void");
            }
        }).start();
    }

    static /* synthetic */ Map getsocial(pdwpUtZXDT pdwputzxdt, String str) {
        if (str == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (String str2 : str.split("\\|")) {
            int indexOf = str2.indexOf(Constants.RequestParameters.EQUAL);
            if (!(indexOf == -1 || indexOf == str2.length() - 1)) {
                hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
            }
        }
        return hashMap;
    }
}
