package im.getsocial.c.a;

import java.util.Locale;

/* compiled from: NotReadyException */
public class upgqDBbsrL extends Exception {
    public upgqDBbsrL(String str) {
        super(String.format(Locale.ENGLISH, "File %s not ready after all attempts", str));
    }

    public upgqDBbsrL(String str, int i) {
        super(String.format(Locale.ENGLISH, "Unexpected HTTP response %d for %s", Integer.valueOf(i), str));
    }

    public upgqDBbsrL(Throwable th) {
        super(String.format(Locale.ENGLISH, "Unexpected error: %s", th.getMessage()));
    }
}
