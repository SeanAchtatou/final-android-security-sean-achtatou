package im.getsocial.a.a;

import java.io.Writer;
import java.util.Collection;
import java.util.Map;

/* compiled from: JSONValue */
public class zoToeBNOjF {
    public static void getsocial(Object obj, Writer writer) {
        if (obj == null) {
            writer.write("null");
        } else if (obj instanceof String) {
            writer.write(34);
            writer.write(getsocial((String) obj));
            writer.write(34);
        } else if (obj instanceof Double) {
            Double d = (Double) obj;
            if (d.isInfinite() || d.isNaN()) {
                writer.write("null");
            } else {
                writer.write(obj.toString());
            }
        } else if (obj instanceof Float) {
            Float f = (Float) obj;
            if (f.isInfinite() || f.isNaN()) {
                writer.write("null");
            } else {
                writer.write(obj.toString());
            }
        } else if (obj instanceof Number) {
            writer.write(obj.toString());
        } else if (obj instanceof Boolean) {
            writer.write(obj.toString());
        } else if (obj instanceof XdbacJlTDQ) {
            ((XdbacJlTDQ) obj).getsocial(writer);
        } else if (obj instanceof cjrhisSQCL) {
            writer.write(((cjrhisSQCL) obj).getsocial());
        } else if (obj instanceof Map) {
            pdwpUtZXDT.getsocial((Map) obj, writer);
        } else if (obj instanceof Collection) {
            upgqDBbsrL.getsocial((Collection) obj, writer);
        } else if (obj instanceof byte[]) {
            upgqDBbsrL.getsocial((byte[]) obj, writer);
        } else if (obj instanceof short[]) {
            upgqDBbsrL.getsocial((short[]) obj, writer);
        } else if (obj instanceof int[]) {
            upgqDBbsrL.getsocial((int[]) obj, writer);
        } else if (obj instanceof long[]) {
            upgqDBbsrL.getsocial((long[]) obj, writer);
        } else if (obj instanceof float[]) {
            upgqDBbsrL.getsocial((float[]) obj, writer);
        } else if (obj instanceof double[]) {
            upgqDBbsrL.getsocial((double[]) obj, writer);
        } else if (obj instanceof boolean[]) {
            upgqDBbsrL.getsocial((boolean[]) obj, writer);
        } else if (obj instanceof char[]) {
            upgqDBbsrL.getsocial((char[]) obj, writer);
        } else if (obj instanceof Object[]) {
            upgqDBbsrL.getsocial((Object[]) obj, writer);
        } else {
            writer.write(obj.toString());
        }
    }

    public static String getsocial(String str) {
        if (str == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        getsocial(str, stringBuffer);
        return stringBuffer.toString();
    }

    private static void getsocial(String str, StringBuffer stringBuffer) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"') {
                stringBuffer.append("\\\"");
            } else if (charAt == '/') {
                stringBuffer.append("\\/");
            } else if (charAt != '\\') {
                switch (charAt) {
                    case 8:
                        stringBuffer.append("\\b");
                        continue;
                    case 9:
                        stringBuffer.append("\\t");
                        continue;
                    case 10:
                        stringBuffer.append("\\n");
                        continue;
                    default:
                        switch (charAt) {
                            case 12:
                                stringBuffer.append("\\f");
                                continue;
                            case 13:
                                stringBuffer.append("\\r");
                                continue;
                            default:
                                if ((charAt < 0 || charAt > 31) && ((charAt < 127 || charAt > 159) && (charAt < 8192 || charAt > 8447))) {
                                    stringBuffer.append(charAt);
                                    break;
                                } else {
                                    String hexString = Integer.toHexString(charAt);
                                    stringBuffer.append("\\u");
                                    for (int i2 = 0; i2 < 4 - hexString.length(); i2++) {
                                        stringBuffer.append('0');
                                    }
                                    stringBuffer.append(hexString.toUpperCase());
                                    continue;
                                    continue;
                                }
                        }
                }
            } else {
                stringBuffer.append("\\\\");
            }
        }
    }
}
