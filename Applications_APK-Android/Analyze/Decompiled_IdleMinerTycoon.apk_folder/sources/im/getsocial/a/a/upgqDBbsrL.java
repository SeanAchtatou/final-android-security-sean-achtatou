package im.getsocial.a.a;

import com.ironsource.sdk.constants.Constants;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: JSONArray */
public class upgqDBbsrL<V> extends ArrayList<V> implements XdbacJlTDQ, cjrhisSQCL {
    public static void getsocial(Collection collection, Writer writer) {
        if (collection == null) {
            writer.write("null");
            return;
        }
        boolean z = true;
        writer.write(91);
        for (Object next : collection) {
            if (z) {
                z = false;
            } else {
                writer.write(44);
            }
            if (next == null) {
                writer.write("null");
            } else {
                zoToeBNOjF.getsocial(next, writer);
            }
        }
        writer.write(93);
    }

    public final void getsocial(Writer writer) {
        getsocial(this, writer);
    }

    public static String getsocial(Collection collection) {
        StringWriter stringWriter = new StringWriter();
        try {
            getsocial(collection, stringWriter);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void getsocial(byte[] bArr, Writer writer) {
        if (bArr == null) {
            writer.write("null");
        } else if (bArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf((int) bArr[0]));
            for (int i = 1; i < bArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf((int) bArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(short[] sArr, Writer writer) {
        if (sArr == null) {
            writer.write("null");
        } else if (sArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf((int) sArr[0]));
            for (int i = 1; i < sArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf((int) sArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(int[] iArr, Writer writer) {
        if (iArr == null) {
            writer.write("null");
        } else if (iArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf(iArr[0]));
            for (int i = 1; i < iArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf(iArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(long[] jArr, Writer writer) {
        if (jArr == null) {
            writer.write("null");
        } else if (jArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf(jArr[0]));
            for (int i = 1; i < jArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf(jArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(float[] fArr, Writer writer) {
        if (fArr == null) {
            writer.write("null");
        } else if (fArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf(fArr[0]));
            for (int i = 1; i < fArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf(fArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(double[] dArr, Writer writer) {
        if (dArr == null) {
            writer.write("null");
        } else if (dArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf(dArr[0]));
            for (int i = 1; i < dArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf(dArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(boolean[] zArr, Writer writer) {
        if (zArr == null) {
            writer.write("null");
        } else if (zArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            writer.write(String.valueOf(zArr[0]));
            for (int i = 1; i < zArr.length; i++) {
                writer.write(",");
                writer.write(String.valueOf(zArr[i]));
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public static void getsocial(char[] cArr, Writer writer) {
        if (cArr == null) {
            writer.write("null");
        } else if (cArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write("[\"");
            writer.write(String.valueOf(cArr[0]));
            for (int i = 1; i < cArr.length; i++) {
                writer.write("\",\"");
                writer.write(String.valueOf(cArr[i]));
            }
            writer.write("\"]");
        }
    }

    public static void getsocial(Object[] objArr, Writer writer) {
        if (objArr == null) {
            writer.write("null");
        } else if (objArr.length == 0) {
            writer.write("[]");
        } else {
            writer.write(Constants.RequestParameters.LEFT_BRACKETS);
            zoToeBNOjF.getsocial(objArr[0], writer);
            for (int i = 1; i < objArr.length; i++) {
                writer.write(",");
                zoToeBNOjF.getsocial(objArr[i], writer);
            }
            writer.write(Constants.RequestParameters.RIGHT_BRACKETS);
        }
    }

    public final String getsocial() {
        return getsocial(this);
    }

    public String toString() {
        return getsocial(this);
    }
}
