package im.getsocial.a.a.a;

/* compiled from: ParseException */
public class pdwpUtZXDT extends Exception {
    private int acquisition;
    private Object attribution;
    private int getsocial;

    public pdwpUtZXDT(int i) {
        this(-1, 2, null);
    }

    public pdwpUtZXDT(int i, Object obj) {
        this(-1, 2, obj);
    }

    public pdwpUtZXDT(int i, int i2, Object obj) {
        this.acquisition = i;
        this.getsocial = i2;
        this.attribution = obj;
    }

    public String getMessage() {
        StringBuffer stringBuffer = new StringBuffer();
        switch (this.getsocial) {
            case 0:
                stringBuffer.append("Unexpected character (");
                stringBuffer.append(this.attribution);
                stringBuffer.append(") at position ");
                stringBuffer.append(this.acquisition);
                stringBuffer.append(".");
                break;
            case 1:
                stringBuffer.append("Unexpected token ");
                stringBuffer.append(this.attribution);
                stringBuffer.append(" at position ");
                stringBuffer.append(this.acquisition);
                stringBuffer.append(".");
                break;
            case 2:
                stringBuffer.append("Unexpected exception at position ");
                stringBuffer.append(this.acquisition);
                stringBuffer.append(": ");
                stringBuffer.append(this.attribution);
                break;
            default:
                stringBuffer.append("Unkown error at position ");
                stringBuffer.append(this.acquisition);
                stringBuffer.append(".");
                break;
        }
        return stringBuffer.toString();
    }
}
