package im.getsocial.a.a.a;

/* compiled from: Yytoken */
public class zoToeBNOjF {
    public Object attribution = null;
    public int getsocial = 0;

    public zoToeBNOjF(int i, Object obj) {
        this.getsocial = i;
        this.attribution = obj;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        switch (this.getsocial) {
            case -1:
                stringBuffer.append("END OF FILE");
                break;
            case 0:
                stringBuffer.append("VALUE(");
                stringBuffer.append(this.attribution);
                stringBuffer.append(")");
                break;
            case 1:
                stringBuffer.append("LEFT BRACE({)");
                break;
            case 2:
                stringBuffer.append("RIGHT BRACE(})");
                break;
            case 3:
                stringBuffer.append("LEFT SQUARE([)");
                break;
            case 4:
                stringBuffer.append("RIGHT SQUARE(])");
                break;
            case 5:
                stringBuffer.append("COMMA(,)");
                break;
            case 6:
                stringBuffer.append("COLON(:)");
                break;
        }
        return stringBuffer.toString();
    }
}
