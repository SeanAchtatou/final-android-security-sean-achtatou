package io.fabric.unity.android;

class KitData {
    public final String fullyQualifiedClassName;
    public final String kitName;

    public KitData(String str, String str2) {
        this.kitName = str;
        this.fullyQualifiedClassName = str2;
    }
}
