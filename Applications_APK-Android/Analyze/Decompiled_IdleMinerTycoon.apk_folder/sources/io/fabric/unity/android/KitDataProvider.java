package io.fabric.unity.android;

public interface KitDataProvider {
    String getInitializationKitsList();

    String getInitializationType();

    KitData[] getKitData();
}
