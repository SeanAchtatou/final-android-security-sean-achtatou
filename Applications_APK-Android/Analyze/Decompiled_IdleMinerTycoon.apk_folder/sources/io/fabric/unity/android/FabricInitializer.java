package io.fabric.unity.android;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;

public class FabricInitializer {
    private static String AUTOMATIC = "Automatic";
    private static String MANUAL = "Manual";
    private static Context savedContext;

    public enum Caller {
        Android,
        Unity
    }

    public static String JNI_InitializeFabric() {
        return initializeFabric(savedContext, Caller.Unity);
    }

    public static String initializeFabric(Context context, Caller caller) {
        savedContext = context;
        if (savedContext != null) {
            Bundle manifestMetadata = getManifestMetadata(savedContext);
            if (manifestMetadata != null) {
                BundleKitDataProvider bundleKitDataProvider = new BundleKitDataProvider(manifestMetadata);
                Kit[] createKitsFromKitData = new KitInstantiator(manifestMetadata).createKitsFromKitData(bundleKitDataProvider.getKitData());
                if (createKitsFromKitData == null || createKitsFromKitData.length < 1) {
                    Log.w(Fabric.TAG, "Fabric found no kits to initialize.");
                    return "";
                }
                String initializationType = bundleKitDataProvider.getInitializationType();
                if ((initializationType.equals(AUTOMATIC) && caller == Caller.Android) || (initializationType.equals(MANUAL) && caller == Caller.Unity)) {
                    Fabric.with(savedContext, createKitsFromKitData);
                }
                return bundleKitDataProvider.getInitializationKitsList();
            }
            throw new FabricInitializationException("Fabric initialization metadata missing. Check your AndroidManifest.xml");
        }
        throw new FabricInitializationException("Fabric did not find a valid application context.");
    }

    private static Bundle getManifestMetadata(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(Fabric.TAG, "Could not retrieve application metadata", e);
            return null;
        }
    }
}
