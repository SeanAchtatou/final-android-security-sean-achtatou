package io.fabric.unity.android;

import android.app.Application;
import io.fabric.unity.android.FabricInitializer;

public class FabricApplication extends Application {
    static final String TAG = "Fabric";

    public void onCreate() {
        super.onCreate();
        FabricInitializer.initializeFabric(this, FabricInitializer.Caller.Android);
    }
}
