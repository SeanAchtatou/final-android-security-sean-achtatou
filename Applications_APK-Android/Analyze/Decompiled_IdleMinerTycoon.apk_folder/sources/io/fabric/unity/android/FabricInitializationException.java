package io.fabric.unity.android;

public class FabricInitializationException extends RuntimeException {
    public FabricInitializationException(String str) {
        super(str);
    }

    public FabricInitializationException(String str, Throwable th) {
        super(str, th);
    }
}
