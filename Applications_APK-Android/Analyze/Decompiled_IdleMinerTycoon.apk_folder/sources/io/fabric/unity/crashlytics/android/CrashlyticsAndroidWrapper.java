package io.fabric.unity.crashlytics.android;

public class CrashlyticsAndroidWrapper {
    public static void crash() {
        new Thread(new Runnable() {
            public void run() {
                throw new RuntimeException("Forced runtime exception");
            }
        }).start();
    }
}
