package android.support.v4.graphics;

import android.graphics.Path;
import android.support.annotation.RestrictTo;
import android.util.Log;
import java.util.ArrayList;

@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
public class PathParser {
    private static final String LOGTAG = "PathParser";

    static float[] copyOfRange(float[] fArr, int i, int i2) {
        if (i <= i2) {
            int length = fArr.length;
            if (i < 0 || i > length) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = i2 - i;
            int min = Math.min(i3, length - i);
            float[] fArr2 = new float[i3];
            System.arraycopy(fArr, i, fArr2, 0, min);
            return fArr2;
        }
        throw new IllegalArgumentException();
    }

    public static Path createPathFromPathData(String str) {
        Path path = new Path();
        PathDataNode[] createNodesFromPathData = createNodesFromPathData(str);
        if (createNodesFromPathData == null) {
            return null;
        }
        try {
            PathDataNode.nodesToPath(createNodesFromPathData, path);
            return path;
        } catch (RuntimeException e) {
            throw new RuntimeException("Error in parsing " + str, e);
        }
    }

    public static PathDataNode[] createNodesFromPathData(String str) {
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 1;
        int i2 = 0;
        while (i < str.length()) {
            int nextStart = nextStart(str, i);
            String trim = str.substring(i2, nextStart).trim();
            if (trim.length() > 0) {
                addNode(arrayList, trim.charAt(0), getFloats(trim));
            }
            i2 = nextStart;
            i = nextStart + 1;
        }
        if (i - i2 == 1 && i2 < str.length()) {
            addNode(arrayList, str.charAt(i2), new float[0]);
        }
        return (PathDataNode[]) arrayList.toArray(new PathDataNode[arrayList.size()]);
    }

    public static PathDataNode[] deepCopyNodes(PathDataNode[] pathDataNodeArr) {
        if (pathDataNodeArr == null) {
            return null;
        }
        PathDataNode[] pathDataNodeArr2 = new PathDataNode[pathDataNodeArr.length];
        for (int i = 0; i < pathDataNodeArr.length; i++) {
            pathDataNodeArr2[i] = new PathDataNode(pathDataNodeArr[i]);
        }
        return pathDataNodeArr2;
    }

    public static boolean canMorph(PathDataNode[] pathDataNodeArr, PathDataNode[] pathDataNodeArr2) {
        if (pathDataNodeArr == null || pathDataNodeArr2 == null || pathDataNodeArr.length != pathDataNodeArr2.length) {
            return false;
        }
        for (int i = 0; i < pathDataNodeArr.length; i++) {
            if (pathDataNodeArr[i].mType != pathDataNodeArr2[i].mType || pathDataNodeArr[i].mParams.length != pathDataNodeArr2[i].mParams.length) {
                return false;
            }
        }
        return true;
    }

    public static void updateNodes(PathDataNode[] pathDataNodeArr, PathDataNode[] pathDataNodeArr2) {
        for (int i = 0; i < pathDataNodeArr2.length; i++) {
            pathDataNodeArr[i].mType = pathDataNodeArr2[i].mType;
            for (int i2 = 0; i2 < pathDataNodeArr2[i].mParams.length; i2++) {
                pathDataNodeArr[i].mParams[i2] = pathDataNodeArr2[i].mParams[i2];
            }
        }
    }

    private static int nextStart(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (((charAt - 'A') * (charAt - 'Z') <= 0 || (charAt - 'a') * (charAt - 'z') <= 0) && charAt != 'e' && charAt != 'E') {
                return i;
            }
            i++;
        }
        return i;
    }

    private static void addNode(ArrayList<PathDataNode> arrayList, char c, float[] fArr) {
        arrayList.add(new PathDataNode(c, fArr));
    }

    private static class ExtractFloatResult {
        int mEndPosition;
        boolean mEndWithNegOrDot;

        ExtractFloatResult() {
        }
    }

    private static float[] getFloats(String str) {
        if (str.charAt(0) == 'z' || str.charAt(0) == 'Z') {
            return new float[0];
        }
        try {
            float[] fArr = new float[str.length()];
            ExtractFloatResult extractFloatResult = new ExtractFloatResult();
            int length = str.length();
            int i = 1;
            int i2 = 0;
            while (i < length) {
                extract(str, i, extractFloatResult);
                int i3 = extractFloatResult.mEndPosition;
                if (i < i3) {
                    fArr[i2] = Float.parseFloat(str.substring(i, i3));
                    i2++;
                }
                i = extractFloatResult.mEndWithNegOrDot ? i3 : i3 + 1;
            }
            return copyOfRange(fArr, 0, i2);
        } catch (NumberFormatException e) {
            throw new RuntimeException("error in parsing \"" + str + "\"", e);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003a A[LOOP:0: B:1:0x0007->B:20:0x003a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void extract(java.lang.String r8, int r9, android.support.v4.graphics.PathParser.ExtractFloatResult r10) {
        /*
            r0 = 0
            r10.mEndWithNegOrDot = r0
            r1 = r9
            r2 = 0
            r3 = 0
            r4 = 0
        L_0x0007:
            int r5 = r8.length()
            if (r1 >= r5) goto L_0x003d
            char r5 = r8.charAt(r1)
            r6 = 32
            r7 = 1
            if (r5 == r6) goto L_0x0035
            r6 = 69
            if (r5 == r6) goto L_0x0033
            r6 = 101(0x65, float:1.42E-43)
            if (r5 == r6) goto L_0x0033
            switch(r5) {
                case 44: goto L_0x0035;
                case 45: goto L_0x002a;
                case 46: goto L_0x0022;
                default: goto L_0x0021;
            }
        L_0x0021:
            goto L_0x0031
        L_0x0022:
            if (r3 != 0) goto L_0x0027
            r2 = 0
            r3 = 1
            goto L_0x0037
        L_0x0027:
            r10.mEndWithNegOrDot = r7
            goto L_0x0035
        L_0x002a:
            if (r1 == r9) goto L_0x0031
            if (r2 != 0) goto L_0x0031
            r10.mEndWithNegOrDot = r7
            goto L_0x0035
        L_0x0031:
            r2 = 0
            goto L_0x0037
        L_0x0033:
            r2 = 1
            goto L_0x0037
        L_0x0035:
            r2 = 0
            r4 = 1
        L_0x0037:
            if (r4 == 0) goto L_0x003a
            goto L_0x003d
        L_0x003a:
            int r1 = r1 + 1
            goto L_0x0007
        L_0x003d:
            r10.mEndPosition = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.graphics.PathParser.extract(java.lang.String, int, android.support.v4.graphics.PathParser$ExtractFloatResult):void");
    }

    public static class PathDataNode {
        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        public float[] mParams;
        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        public char mType;

        PathDataNode(char c, float[] fArr) {
            this.mType = c;
            this.mParams = fArr;
        }

        PathDataNode(PathDataNode pathDataNode) {
            this.mType = pathDataNode.mType;
            this.mParams = PathParser.copyOfRange(pathDataNode.mParams, 0, pathDataNode.mParams.length);
        }

        public static void nodesToPath(PathDataNode[] pathDataNodeArr, Path path) {
            float[] fArr = new float[6];
            char c = 'm';
            for (int i = 0; i < pathDataNodeArr.length; i++) {
                addCommand(path, fArr, c, pathDataNodeArr[i].mType, pathDataNodeArr[i].mParams);
                c = pathDataNodeArr[i].mType;
            }
        }

        public void interpolatePathDataNode(PathDataNode pathDataNode, PathDataNode pathDataNode2, float f) {
            for (int i = 0; i < pathDataNode.mParams.length; i++) {
                this.mParams[i] = (pathDataNode.mParams[i] * (1.0f - f)) + (pathDataNode2.mParams[i] * f);
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        private static void addCommand(Path path, float[] fArr, char c, char c2, float[] fArr2) {
            int i;
            int i2;
            int i3;
            float f;
            float f2;
            float f3;
            float f4;
            float f5;
            float f6;
            float f7;
            float f8;
            float f9;
            float f10;
            float f11;
            float f12;
            float f13;
            Path path2 = path;
            float[] fArr3 = fArr2;
            float f14 = fArr[0];
            float f15 = fArr[1];
            float f16 = fArr[2];
            float f17 = fArr[3];
            float f18 = fArr[4];
            float f19 = fArr[5];
            switch (c2) {
                case 'A':
                case 'a':
                    i = 7;
                    break;
                case 'C':
                case 'c':
                    i = 6;
                    break;
                case 'H':
                case 'V':
                case 'h':
                case 'v':
                    i = 1;
                    break;
                case 'L':
                case 'M':
                case 'T':
                case 'l':
                case 'm':
                case 't':
                default:
                    i = 2;
                    break;
                case 'Q':
                case 'S':
                case 'q':
                case 's':
                    i = 4;
                    break;
                case 'Z':
                case 'z':
                    path.close();
                    path2.moveTo(f18, f19);
                    f14 = f18;
                    f16 = f14;
                    f15 = f19;
                    f17 = f15;
                    i = 2;
                    break;
            }
            float f20 = f14;
            float f21 = f15;
            float f22 = f18;
            float f23 = f19;
            int i4 = 0;
            char c3 = c;
            while (i4 < fArr3.length) {
                float f24 = 0.0f;
                switch (c2) {
                    case 'A':
                        i3 = i4;
                        int i5 = i3 + 5;
                        int i6 = i3 + 6;
                        drawArc(path, f20, f21, fArr3[i5], fArr3[i6], fArr3[i3 + 0], fArr3[i3 + 1], fArr3[i3 + 2], fArr3[i3 + 3] != 0.0f, fArr3[i3 + 4] != 0.0f);
                        f = fArr3[i5];
                        f2 = fArr3[i6];
                        f17 = f21;
                        f16 = f20;
                        break;
                    case 'C':
                        i2 = i4;
                        int i7 = i2 + 2;
                        int i8 = i2 + 3;
                        int i9 = i2 + 4;
                        int i10 = i2 + 5;
                        path.cubicTo(fArr3[i2 + 0], fArr3[i2 + 1], fArr3[i7], fArr3[i8], fArr3[i9], fArr3[i10]);
                        f20 = fArr3[i9];
                        float f25 = fArr3[i10];
                        float f26 = fArr3[i7];
                        float f27 = fArr3[i8];
                        f21 = f25;
                        f17 = f27;
                        f16 = f26;
                        break;
                    case 'H':
                        i2 = i4;
                        int i11 = i2 + 0;
                        path2.lineTo(fArr3[i11], f21);
                        f20 = fArr3[i11];
                        break;
                    case 'L':
                        i2 = i4;
                        int i12 = i2 + 0;
                        int i13 = i2 + 1;
                        path2.lineTo(fArr3[i12], fArr3[i13]);
                        f20 = fArr3[i12];
                        f21 = fArr3[i13];
                        break;
                    case 'M':
                        i2 = i4;
                        int i14 = i2 + 0;
                        f20 = fArr3[i14];
                        int i15 = i2 + 1;
                        f21 = fArr3[i15];
                        if (i2 <= 0) {
                            path2.moveTo(fArr3[i14], fArr3[i15]);
                            f23 = f21;
                            f22 = f20;
                            break;
                        } else {
                            path2.lineTo(fArr3[i14], fArr3[i15]);
                            break;
                        }
                    case 'Q':
                        i2 = i4;
                        int i16 = i2 + 0;
                        int i17 = i2 + 1;
                        int i18 = i2 + 2;
                        int i19 = i2 + 3;
                        path2.quadTo(fArr3[i16], fArr3[i17], fArr3[i18], fArr3[i19]);
                        f4 = fArr3[i16];
                        f3 = fArr3[i17];
                        f5 = fArr3[i18];
                        f6 = fArr3[i19];
                        f16 = f4;
                        f17 = f3;
                        break;
                    case 'S':
                        float f28 = f21;
                        float f29 = f20;
                        i2 = i4;
                        if (c3 == 'c' || c3 == 's' || c3 == 'C' || c3 == 'S') {
                            float f30 = (f29 * 2.0f) - f16;
                            f7 = (f28 * 2.0f) - f17;
                            f8 = f30;
                        } else {
                            f8 = f29;
                            f7 = f28;
                        }
                        int i20 = i2 + 0;
                        int i21 = i2 + 1;
                        int i22 = i2 + 2;
                        int i23 = i2 + 3;
                        path.cubicTo(f8, f7, fArr3[i20], fArr3[i21], fArr3[i22], fArr3[i23]);
                        f4 = fArr3[i20];
                        f3 = fArr3[i21];
                        f5 = fArr3[i22];
                        f6 = fArr3[i23];
                        f16 = f4;
                        f17 = f3;
                        break;
                    case 'T':
                        float f31 = f21;
                        float f32 = f20;
                        i2 = i4;
                        if (c3 == 'q' || c3 == 't' || c3 == 'Q' || c3 == 'T') {
                            f31 = (f31 * 2.0f) - f17;
                            f32 = (f32 * 2.0f) - f16;
                        }
                        int i24 = i2 + 0;
                        int i25 = i2 + 1;
                        path2.quadTo(f32, f31, fArr3[i24], fArr3[i25]);
                        f20 = fArr3[i24];
                        f21 = fArr3[i25];
                        f16 = f32;
                        f17 = f31;
                        break;
                    case 'V':
                        i2 = i4;
                        int i26 = i2 + 0;
                        path2.lineTo(f20, fArr3[i26]);
                        f21 = fArr3[i26];
                        break;
                    case 'a':
                        int i27 = i4 + 5;
                        float f33 = fArr3[i27] + f20;
                        int i28 = i4 + 6;
                        float f34 = fArr3[i28] + f21;
                        float f35 = fArr3[i4 + 0];
                        float f36 = fArr3[i4 + 1];
                        float f37 = fArr3[i4 + 2];
                        float f38 = f20;
                        boolean z = fArr3[i4 + 3] != 0.0f;
                        i3 = i4;
                        drawArc(path, f20, f21, f33, f34, f35, f36, f37, z, fArr3[i4 + 4] != 0.0f);
                        f = f38 + fArr3[i27];
                        f2 = f21 + fArr3[i28];
                        f17 = f21;
                        f16 = f20;
                        break;
                    case 'c':
                        int i29 = i4 + 2;
                        int i30 = i4 + 3;
                        int i31 = i4 + 4;
                        int i32 = i4 + 5;
                        path.rCubicTo(fArr3[i4 + 0], fArr3[i4 + 1], fArr3[i29], fArr3[i30], fArr3[i31], fArr3[i32]);
                        f10 = fArr3[i29] + f20;
                        f9 = fArr3[i30] + f21;
                        f20 += fArr3[i31];
                        f21 += fArr3[i32];
                        f16 = f10;
                        f17 = f9;
                        i2 = i4;
                        break;
                    case 'h':
                        int i33 = i4 + 0;
                        path2.rLineTo(fArr3[i33], 0.0f);
                        f20 += fArr3[i33];
                        i2 = i4;
                        break;
                    case 'l':
                        int i34 = i4 + 0;
                        int i35 = i4 + 1;
                        path2.rLineTo(fArr3[i34], fArr3[i35]);
                        f20 += fArr3[i34];
                        f21 += fArr3[i35];
                        i2 = i4;
                        break;
                    case 'm':
                        int i36 = i4 + 0;
                        f20 += fArr3[i36];
                        int i37 = i4 + 1;
                        f21 += fArr3[i37];
                        if (i4 > 0) {
                            path2.rLineTo(fArr3[i36], fArr3[i37]);
                        } else {
                            path2.rMoveTo(fArr3[i36], fArr3[i37]);
                            f23 = f21;
                            f22 = f20;
                        }
                        i2 = i4;
                        break;
                    case 'q':
                        int i38 = i4 + 0;
                        int i39 = i4 + 1;
                        int i40 = i4 + 2;
                        int i41 = i4 + 3;
                        path2.rQuadTo(fArr3[i38], fArr3[i39], fArr3[i40], fArr3[i41]);
                        f10 = fArr3[i38] + f20;
                        f9 = fArr3[i39] + f21;
                        f20 += fArr3[i40];
                        f21 += fArr3[i41];
                        f16 = f10;
                        f17 = f9;
                        i2 = i4;
                        break;
                    case 's':
                        if (c3 == 'c' || c3 == 's' || c3 == 'C' || c3 == 'S') {
                            float f39 = f20 - f16;
                            f11 = f21 - f17;
                            f12 = f39;
                        } else {
                            f12 = 0.0f;
                            f11 = 0.0f;
                        }
                        int i42 = i4 + 0;
                        int i43 = i4 + 1;
                        int i44 = i4 + 2;
                        int i45 = i4 + 3;
                        path.rCubicTo(f12, f11, fArr3[i42], fArr3[i43], fArr3[i44], fArr3[i45]);
                        f10 = fArr3[i42] + f20;
                        f9 = fArr3[i43] + f21;
                        f20 += fArr3[i44];
                        f21 += fArr3[i45];
                        f16 = f10;
                        f17 = f9;
                        i2 = i4;
                        break;
                    case 't':
                        if (c3 == 'q' || c3 == 't' || c3 == 'Q' || c3 == 'T') {
                            f24 = f20 - f16;
                            f13 = f21 - f17;
                        } else {
                            f13 = 0.0f;
                        }
                        int i46 = i4 + 0;
                        int i47 = i4 + 1;
                        path2.rQuadTo(f24, f13, fArr3[i46], fArr3[i47]);
                        float f40 = f24 + f20;
                        float f41 = f13 + f21;
                        f20 += fArr3[i46];
                        f21 += fArr3[i47];
                        f17 = f41;
                        f16 = f40;
                        i2 = i4;
                        break;
                    case 'v':
                        int i48 = i4 + 0;
                        path2.rLineTo(0.0f, fArr3[i48]);
                        f21 += fArr3[i48];
                        i2 = i4;
                        break;
                    default:
                        i2 = i4;
                        break;
                }
                i4 = i2 + i;
                c3 = c2;
            }
            fArr[0] = f20;
            fArr[1] = f21;
            fArr[2] = f16;
            fArr[3] = f17;
            fArr[4] = f22;
            fArr[5] = f23;
        }

        private static void drawArc(Path path, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z, boolean z2) {
            double d;
            double d2;
            float f8 = f;
            float f9 = f3;
            float f10 = f5;
            double radians = Math.toRadians((double) f7);
            double cos = Math.cos(radians);
            double sin = Math.sin(radians);
            double d3 = (double) f8;
            Double.isNaN(d3);
            double d4 = d3 * cos;
            double d5 = d3;
            double d6 = (double) f2;
            Double.isNaN(d6);
            double d7 = (double) f10;
            Double.isNaN(d7);
            double d8 = (d4 + (d6 * sin)) / d7;
            double d9 = (double) (-f8);
            Double.isNaN(d9);
            Double.isNaN(d6);
            double d10 = d6;
            double d11 = (double) f6;
            Double.isNaN(d11);
            double d12 = (double) f9;
            Double.isNaN(d12);
            double d13 = ((d9 * sin) + (d6 * cos)) / d11;
            double d14 = (double) f4;
            Double.isNaN(d14);
            Double.isNaN(d7);
            double d15 = ((d12 * cos) + (d14 * sin)) / d7;
            double d16 = d7;
            double d17 = (double) (-f9);
            Double.isNaN(d17);
            Double.isNaN(d14);
            Double.isNaN(d11);
            double d18 = ((d17 * sin) + (d14 * cos)) / d11;
            double d19 = d8 - d15;
            double d20 = d13 - d18;
            double d21 = (d8 + d15) / 2.0d;
            double d22 = (d13 + d18) / 2.0d;
            double d23 = sin;
            double d24 = (d19 * d19) + (d20 * d20);
            if (d24 == 0.0d) {
                Log.w(PathParser.LOGTAG, " Points are coincident");
                return;
            }
            double d25 = (1.0d / d24) - 0.25d;
            if (d25 < 0.0d) {
                Log.w(PathParser.LOGTAG, "Points are too far apart " + d24);
                float sqrt = (float) (Math.sqrt(d24) / 1.99999d);
                drawArc(path, f, f2, f3, f4, f10 * sqrt, f6 * sqrt, f7, z, z2);
                return;
            }
            double sqrt2 = Math.sqrt(d25);
            double d26 = d19 * sqrt2;
            double d27 = sqrt2 * d20;
            boolean z3 = z2;
            if (z == z3) {
                d2 = d21 - d27;
                d = d22 + d26;
            } else {
                d2 = d21 + d27;
                d = d22 - d26;
            }
            double atan2 = Math.atan2(d13 - d, d8 - d2);
            double atan22 = Math.atan2(d18 - d, d15 - d2) - atan2;
            if (z3 != (atan22 >= 0.0d)) {
                atan22 = atan22 > 0.0d ? atan22 - 6.283185307179586d : atan22 + 6.283185307179586d;
            }
            Double.isNaN(d16);
            double d28 = d2 * d16;
            Double.isNaN(d11);
            double d29 = d * d11;
            arcToBezier(path, (d28 * cos) - (d29 * d23), (d28 * d23) + (d29 * cos), d16, d11, d5, d10, radians, atan2, atan22);
        }

        private static void arcToBezier(Path path, double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9) {
            double d10 = d3;
            int ceil = (int) Math.ceil(Math.abs((d9 * 4.0d) / 3.141592653589793d));
            double cos = Math.cos(d7);
            double sin = Math.sin(d7);
            double cos2 = Math.cos(d8);
            double sin2 = Math.sin(d8);
            double d11 = -d10;
            double d12 = d11 * cos;
            double d13 = d4 * sin;
            double d14 = d11 * sin;
            double d15 = d4 * cos;
            double d16 = (sin2 * d14) + (cos2 * d15);
            double d17 = (double) ceil;
            Double.isNaN(d17);
            double d18 = d9 / d17;
            int i = 0;
            double d19 = d6;
            double d20 = d16;
            double d21 = (d12 * sin2) - (d13 * cos2);
            double d22 = d5;
            double d23 = d8;
            while (i < ceil) {
                double d24 = d23 + d18;
                double sin3 = Math.sin(d24);
                double cos3 = Math.cos(d24);
                double d25 = d18;
                double d26 = (d + ((d10 * cos) * cos3)) - (d13 * sin3);
                double d27 = d2 + (d10 * sin * cos3) + (d15 * sin3);
                double d28 = (d12 * sin3) - (d13 * cos3);
                double d29 = (sin3 * d14) + (cos3 * d15);
                double d30 = d24 - d23;
                double tan = Math.tan(d30 / 2.0d);
                double sin4 = (Math.sin(d30) * (Math.sqrt(((tan * 3.0d) * tan) + 4.0d) - 1.0d)) / 3.0d;
                double d31 = d15;
                double d32 = d14;
                path.rLineTo(0.0f, 0.0f);
                float f = (float) (d19 + (d20 * sin4));
                float f2 = (float) (d26 - (sin4 * d28));
                path.cubicTo((float) (d22 + (d21 * sin4)), f, f2, (float) (d27 - (sin4 * d29)), (float) d26, (float) d27);
                i++;
                d19 = d27;
                d22 = d26;
                d23 = d24;
                d20 = d29;
                d21 = d28;
                d18 = d25;
                d15 = d31;
                d14 = d32;
                ceil = ceil;
                cos = cos;
                sin = sin;
                d10 = d3;
            }
        }
    }

    private PathParser() {
    }
}
