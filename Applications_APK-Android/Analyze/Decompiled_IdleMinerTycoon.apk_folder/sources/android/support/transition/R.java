package android.support.transition;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int alpha = 2130903082;
        public static final int font = 2130903266;
        public static final int fontProviderAuthority = 2130903268;
        public static final int fontProviderCerts = 2130903269;
        public static final int fontProviderFetchStrategy = 2130903270;
        public static final int fontProviderFetchTimeout = 2130903271;
        public static final int fontProviderPackage = 2130903272;
        public static final int fontProviderQuery = 2130903273;
        public static final int fontStyle = 2130903274;
        public static final int fontVariationSettings = 2130903275;
        public static final int fontWeight = 2130903276;
        public static final int ttcIndex = 2130903580;

        private attr() {
        }
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131034324;
        public static final int notification_icon_bg_color = 2131034325;
        public static final int ripple_material_light = 2131034338;
        public static final int secondary_text_default_material_light = 2131034340;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131099755;
        public static final int compat_button_inset_vertical_material = 2131099756;
        public static final int compat_button_padding_horizontal_material = 2131099757;
        public static final int compat_button_padding_vertical_material = 2131099758;
        public static final int compat_control_corner_material = 2131099759;
        public static final int compat_notification_large_icon_max_height = 2131099760;
        public static final int compat_notification_large_icon_max_width = 2131099761;
        public static final int notification_action_icon_size = 2131099910;
        public static final int notification_action_text_size = 2131099911;
        public static final int notification_big_circle_margin = 2131099912;
        public static final int notification_content_margin_start = 2131099913;
        public static final int notification_large_icon_height = 2131099914;
        public static final int notification_large_icon_width = 2131099915;
        public static final int notification_main_column_padding_top = 2131099916;
        public static final int notification_media_narrow_margin = 2131099917;
        public static final int notification_right_icon_size = 2131099918;
        public static final int notification_right_side_padding_top = 2131099919;
        public static final int notification_small_icon_background_padding = 2131099920;
        public static final int notification_small_icon_size_as_large = 2131099921;
        public static final int notification_subtext_size = 2131099922;
        public static final int notification_top_pad = 2131099923;
        public static final int notification_top_pad_large_text = 2131099924;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int notification_action_background = 2131165445;
        public static final int notification_bg = 2131165446;
        public static final int notification_bg_low = 2131165447;
        public static final int notification_bg_low_normal = 2131165448;
        public static final int notification_bg_low_pressed = 2131165449;
        public static final int notification_bg_normal = 2131165450;
        public static final int notification_bg_normal_pressed = 2131165451;
        public static final int notification_icon_background = 2131165452;
        public static final int notification_template_icon_bg = 2131165453;
        public static final int notification_template_icon_low_bg = 2131165454;
        public static final int notification_tile_bg = 2131165455;
        public static final int notify_panel_notification_icon_bg = 2131165456;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action_container = 2131230734;
        public static final int action_divider = 2131230736;
        public static final int action_image = 2131230737;
        public static final int action_text = 2131230743;
        public static final int actions = 2131230744;
        public static final int async = 2131230770;
        public static final int blocking = 2131230780;
        public static final int chronometer = 2131230800;
        public static final int forever = 2131230867;
        public static final int ghost_view = 2131230868;
        public static final int icon = 2131230918;
        public static final int icon_group = 2131230919;
        public static final int info = 2131230925;
        public static final int italic = 2131230929;
        public static final int line1 = 2131230937;
        public static final int line3 = 2131230938;
        public static final int normal = 2131231018;
        public static final int notification_background = 2131231019;
        public static final int notification_main_column = 2131231021;
        public static final int notification_main_column_container = 2131231022;
        public static final int parent_matrix = 2131231041;
        public static final int right_icon = 2131231069;
        public static final int right_side = 2131231070;
        public static final int save_image_matrix = 2131231071;
        public static final int save_non_transition_alpha = 2131231072;
        public static final int save_scale_type = 2131231073;
        public static final int tag_transition_group = 2131231138;
        public static final int tag_unhandled_key_event_manager = 2131231139;
        public static final int tag_unhandled_key_listeners = 2131231140;
        public static final int text = 2131231141;
        public static final int text2 = 2131231142;
        public static final int time = 2131231153;
        public static final int title = 2131231154;
        public static final int transition_current_scene = 2131231161;
        public static final int transition_layout_save = 2131231162;
        public static final int transition_position = 2131231163;
        public static final int transition_scene_layoutid_cache = 2131231164;
        public static final int transition_transform = 2131231165;

        private id() {
        }
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131296277;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131427460;
        public static final int notification_action_tombstone = 2131427461;
        public static final int notification_template_custom_big = 2131427468;
        public static final int notification_template_icon_group = 2131427469;
        public static final int notification_template_part_chronometer = 2131427473;
        public static final int notification_template_part_time = 2131427474;

        private layout() {
        }
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131689751;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131755322;
        public static final int TextAppearance_Compat_Notification_Info = 2131755323;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131755325;
        public static final int TextAppearance_Compat_Notification_Time = 2131755328;
        public static final int TextAppearance_Compat_Notification_Title = 2131755330;
        public static final int Widget_Compat_NotificationActionContainer = 2131755501;
        public static final int Widget_Compat_NotificationActionText = 2131755502;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] ColorStateListItem = {16843173, 16843551, com.fluffyfairygames.idleminertycoon.R.attr.alpha};
        public static final int ColorStateListItem_alpha = 2;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] FontFamily = {com.fluffyfairygames.idleminertycoon.R.attr.fontProviderAuthority, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderCerts, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderFetchStrategy, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderFetchTimeout, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderPackage, com.fluffyfairygames.idleminertycoon.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.fluffyfairygames.idleminertycoon.R.attr.font, com.fluffyfairygames.idleminertycoon.R.attr.fontStyle, com.fluffyfairygames.idleminertycoon.R.attr.fontVariationSettings, com.fluffyfairygames.idleminertycoon.R.attr.fontWeight, com.fluffyfairygames.idleminertycoon.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;

        private styleable() {
        }
    }
}
