package android.support.v7.cardview;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int cardBackgroundColor = 2130903132;
        public static final int cardCornerRadius = 2130903133;
        public static final int cardElevation = 2130903134;
        public static final int cardMaxElevation = 2130903135;
        public static final int cardPreventCornerOverlap = 2130903136;
        public static final int cardUseCompatPadding = 2130903137;
        public static final int cardViewStyle = 2130903138;
        public static final int contentPadding = 2130903209;
        public static final int contentPaddingBottom = 2130903210;
        public static final int contentPaddingLeft = 2130903211;
        public static final int contentPaddingRight = 2130903212;
        public static final int contentPaddingTop = 2130903213;

        private attr() {
        }
    }

    public static final class color {
        public static final int cardview_dark_background = 2131034162;
        public static final int cardview_light_background = 2131034163;
        public static final int cardview_shadow_end_color = 2131034164;
        public static final int cardview_shadow_start_color = 2131034165;

        private color() {
        }
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131099736;
        public static final int cardview_default_elevation = 2131099737;
        public static final int cardview_default_radius = 2131099738;

        private dimen() {
        }
    }

    public static final class style {
        public static final int Base_CardView = 2131755021;
        public static final int CardView = 2131755205;
        public static final int CardView_Dark = 2131755206;
        public static final int CardView_Light = 2131755207;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.fluffyfairygames.idleminertycoon.R.attr.cardBackgroundColor, com.fluffyfairygames.idleminertycoon.R.attr.cardCornerRadius, com.fluffyfairygames.idleminertycoon.R.attr.cardElevation, com.fluffyfairygames.idleminertycoon.R.attr.cardMaxElevation, com.fluffyfairygames.idleminertycoon.R.attr.cardPreventCornerOverlap, com.fluffyfairygames.idleminertycoon.R.attr.cardUseCompatPadding, com.fluffyfairygames.idleminertycoon.R.attr.contentPadding, com.fluffyfairygames.idleminertycoon.R.attr.contentPaddingBottom, com.fluffyfairygames.idleminertycoon.R.attr.contentPaddingLeft, com.fluffyfairygames.idleminertycoon.R.attr.contentPaddingRight, com.fluffyfairygames.idleminertycoon.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;

        private styleable() {
        }
    }
}
