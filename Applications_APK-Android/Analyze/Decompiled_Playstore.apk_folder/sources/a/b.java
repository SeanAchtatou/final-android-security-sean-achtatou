package a;

import java.io.File;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1a = new Character('.').toString();

    /* renamed from: b  reason: collision with root package name */
    private static final char f2b = File.separatorChar;

    public static int a(String str) {
        if (str == null) {
            return -1;
        }
        return Math.max(str.lastIndexOf(47), str.lastIndexOf(92));
    }

    public static int b(String str) {
        int lastIndexOf;
        if (str != null && a(str) <= (lastIndexOf = str.lastIndexOf(46))) {
            return lastIndexOf;
        }
        return -1;
    }

    public static String c(String str) {
        if (str == null) {
            return null;
        }
        int b2 = b(str);
        return b2 == -1 ? "" : str.substring(b2 + 1);
    }
}
