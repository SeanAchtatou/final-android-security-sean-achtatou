package net.droidjack.server;

import android.database.ContentObserver;
import android.os.Handler;

class d extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallListener f331a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(CallListener callListener, Handler handler) {
        super(handler);
        this.f331a = callListener;
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        super.onChange(z);
        this.f331a.b();
    }
}
