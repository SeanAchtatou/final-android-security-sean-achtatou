package net.droidjack.server;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

class bj implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bl f296a;

    bj(bl blVar) {
        this.f296a = blVar;
    }

    public void onLocationChanged(Location location) {
        this.f296a.a(new bk(location.getLatitude(), location.getLongitude()));
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
