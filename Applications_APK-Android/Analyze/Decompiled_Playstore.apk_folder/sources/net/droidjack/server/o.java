package net.droidjack.server;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

class o extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f345a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f346b;

    o(n nVar, String str) {
        this.f345a = nVar;
        this.f346b = str;
    }

    public void dispatchMessage(Message message) {
        handleMessage(message);
    }

    public void handleMessage(Message message) {
        Toast.makeText(Controller.s, this.f346b, 1).show();
    }
}
