package net.droidjack.server;

import android.os.Looper;

class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f344a;

    n(String str) {
        this.f344a = str;
    }

    public void run() {
        Looper.prepare();
        o oVar = new o(this, this.f344a);
        oVar.dispatchMessage(oVar.obtainMessage());
        Looper.loop();
    }
}
