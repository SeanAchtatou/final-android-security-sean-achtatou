package net.droidjack.server;

import java.text.SimpleDateFormat;
import java.util.Date;

class ay {

    /* renamed from: a  reason: collision with root package name */
    private String f287a;

    ay(Long l) {
        this.f287a = new SimpleDateFormat("dd/MM/yyy HH:mm:ss").format(new Date(l.longValue()));
    }

    /* access modifiers changed from: protected */
    public String a() {
        return this.f287a;
    }
}
