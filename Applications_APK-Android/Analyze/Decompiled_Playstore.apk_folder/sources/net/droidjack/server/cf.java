package net.droidjack.server;

import android.content.Context;
import android.media.AudioManager;

public class cf {

    /* renamed from: a  reason: collision with root package name */
    private Context f329a;

    /* renamed from: b  reason: collision with root package name */
    private AudioManager f330b;
    private int[] c;

    public cf(Context context) {
        int[] iArr = new int[6];
        iArr[0] = 4;
        iArr[1] = 3;
        iArr[2] = 5;
        iArr[3] = 2;
        iArr[4] = 1;
        this.c = iArr;
        this.f329a = context;
        this.f330b = (AudioManager) context.getSystemService("audio");
        ae.a();
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            for (int streamVolume : this.c) {
                stringBuffer.append(this.f330b.getStreamVolume(streamVolume));
                stringBuffer.append(",.");
            }
            return stringBuffer.toString().getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            String[] split = str.split(",.");
            for (int i = 0; i < this.c.length; i++) {
                this.f330b.setStreamVolume(this.c[i], Integer.parseInt(split[i]), 8);
            }
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
