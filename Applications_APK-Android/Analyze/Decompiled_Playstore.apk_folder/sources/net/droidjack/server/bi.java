package net.droidjack.server;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

class bi implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bl f295a;

    bi(bl blVar) {
        this.f295a = blVar;
    }

    public void onLocationChanged(Location location) {
        this.f295a.a(new bk(location.getLatitude(), location.getLongitude()));
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
