package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class by extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f318a;

    /* renamed from: b  reason: collision with root package name */
    private String f319b = "SystemConfigurationTable";

    public by(Context context) {
        super(context, "SandroRat_Configuration_Database", (SQLiteDatabase.CursorFactory) null, 1);
        ae.a();
    }

    private void a() {
        try {
            this.f318a = getWritableDatabase();
        } catch (Exception e) {
        }
    }

    private void b() {
        try {
            this.f318a = getReadableDatabase();
        } catch (Exception e) {
        }
    }

    private void c() {
        try {
            this.f318a.close();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        String str2;
        b();
        try {
            Cursor query = this.f318a.query(this.f319b, new String[]{"CONFIGURATION", "VALUE"}, "CONFIGURATION='" + str + "'", null, null, null, null);
            query.moveToNext();
            if (query.getString(0) != "") {
                str2 = query.getString(1);
                c();
                return str2;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        str2 = "";
        c();
        return str2;
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        if (!b(str)) {
            a();
            ContentValues contentValues = new ContentValues();
            contentValues.put("CONFIGURATION", str);
            contentValues.put("VALUE", str2);
            this.f318a.insert(this.f319b, null, contentValues);
            c();
            return;
        }
        a();
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("CONFIGURATION", str);
        contentValues2.put("VALUE", str2);
        try {
            this.f318a.update(this.f319b, contentValues2, "CONFIGURATION='" + str + "'", null);
        } catch (Exception e) {
        }
        c();
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        boolean z;
        b();
        try {
            z = this.f318a.query(this.f319b, new String[]{"CONFIGURATION"}, new StringBuilder("CONFIGURATION='").append(str).append("'").toString(), null, null, null, null).moveToFirst();
        } catch (Exception e) {
            e.printStackTrace();
            z = false;
        }
        c();
        return z;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f319b + " (CONFIGURATION TEXT, VALUE TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f319b);
        onCreate(sQLiteDatabase);
    }
}
