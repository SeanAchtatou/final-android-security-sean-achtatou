package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ad extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f253a;

    /* renamed from: b  reason: collision with root package name */
    private String f254b = "CrashReportTable";

    public ad(Context context) {
        super(context, "SandroRat_CrashReport_Database", (SQLiteDatabase.CursorFactory) null, 1);
    }

    private void a() {
        this.f253a = getWritableDatabase();
    }

    private void b() {
        this.f253a.close();
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        a();
        ContentValues contentValues = new ContentValues();
        contentValues.put("REPORT", str);
        contentValues.put("RDATE", str2);
        this.f253a.insert(this.f254b, null, contentValues);
        b();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f254b + " (REPORT TEXT, RDATE TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f254b);
        onCreate(sQLiteDatabase);
    }
}
