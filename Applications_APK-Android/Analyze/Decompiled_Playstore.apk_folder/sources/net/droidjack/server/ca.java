package net.droidjack.server;

import android.content.Context;
import java.util.concurrent.Executors;

public class ca {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f323a;

    ca(Context context) {
        this.f323a = context;
        ae.a();
    }

    /* access modifiers changed from: protected */
    public byte[] a(String str) {
        try {
            return (byte[]) Executors.newSingleThreadExecutor().submit(new cb(this, str)).get();
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
