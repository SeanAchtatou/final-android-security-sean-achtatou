package net.droidjack.server;

import android.os.Environment;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class cg {
    protected static File a(String str) {
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/DJTmpcpDIR.zip");
            if (file.exists()) {
                file.delete();
            }
            File file2 = new File(Environment.getExternalStorageDirectory() + "/DJTmpcpDIR.zip");
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file2));
            a("", str, zipOutputStream);
            zipOutputStream.flush();
            zipOutputStream.close();
            return file2;
        } catch (Exception e) {
            ae.a(e);
            return null;
        }
    }

    protected static void a(File file, String str) {
        try {
            ZipFile zipFile = new ZipFile(file);
            new File(str).mkdir();
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                File file2 = new File(str, zipEntry.getName());
                file2.getParentFile().mkdirs();
                if (!zipEntry.isDirectory()) {
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(zipFile.getInputStream(zipEntry));
                    byte[] bArr = new byte[2048];
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file2), 2048);
                    while (true) {
                        int read = bufferedInputStream.read(bArr, 0, 2048);
                        if (read == -1) {
                            break;
                        }
                        bufferedOutputStream.write(bArr, 0, read);
                    }
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                    bufferedInputStream.close();
                }
            }
        } catch (Exception e) {
            ae.a(e);
            e.printStackTrace();
        }
    }

    private static void a(String str, String str2, ZipOutputStream zipOutputStream) {
        File file = new File(str2);
        if (file.list().length == 0) {
            a(str, str2, zipOutputStream, true);
            return;
        }
        for (String str3 : file.list()) {
            if (str.equals("")) {
                a(file.getName(), String.valueOf(str2) + "/" + str3, zipOutputStream, false);
            } else {
                a(String.valueOf(str) + "/" + file.getName(), String.valueOf(str2) + "/" + str3, zipOutputStream, false);
            }
        }
    }

    private static void a(String str, String str2, ZipOutputStream zipOutputStream, boolean z) {
        File file = new File(str2);
        if (z) {
            zipOutputStream.putNextEntry(new ZipEntry(String.valueOf(str) + "/" + file.getName() + "/"));
        } else if (file.isDirectory()) {
            a(str, str2, zipOutputStream);
        } else {
            byte[] bArr = new byte[1024];
            FileInputStream fileInputStream = new FileInputStream(str2);
            zipOutputStream.putNextEntry(new ZipEntry(String.valueOf(str) + "/" + file.getName()));
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read > 0) {
                    zipOutputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        }
    }
}
