package net.droidjack.server;

import android.content.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ae {

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f255a = true;

    /* renamed from: b  reason: collision with root package name */
    protected static Context f256b;

    protected static void a() {
        Thread.setDefaultUncaughtExceptionHandler(new af());
    }

    protected static void a(Throwable th) {
        try {
            if (f255a) {
                b(th);
            }
        } catch (Exception e) {
            b(th);
            e.printStackTrace();
        }
    }

    private static void b(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        String stringWriter2 = stringWriter.toString();
        ad adVar = new ad(f256b);
        String str = "";
        try {
            str = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.getDefault()).format(new Date());
        } catch (Exception e) {
        }
        adVar.a(stringWriter2, str);
    }
}
