package b.a.a;

import com.esotericsoftware.kryo.util.DefaultClassResolver;

public class e {

    /* renamed from: a  reason: collision with root package name */
    public final byte[] f185a;

    /* renamed from: b  reason: collision with root package name */
    public final int f186b;
    private final int[] c;
    private final String[] d;
    private final int e;

    public e(byte[] bArr) {
        this(bArr, 0, bArr.length);
    }

    public e(byte[] bArr, int i, int i2) {
        int i3;
        this.f185a = bArr;
        if (c(6) > 51) {
            throw new IllegalArgumentException();
        }
        this.c = new int[b(i + 8)];
        int length = this.c.length;
        this.d = new String[length];
        int i4 = 0;
        int i5 = 1;
        int i6 = i + 10;
        while (i5 < length) {
            this.c[i5] = i6 + 1;
            switch (bArr[i6]) {
                case 1:
                    i3 = b(i6 + 1) + 3;
                    if (i3 <= i4) {
                        break;
                    } else {
                        i4 = i3;
                        break;
                    }
                case 2:
                case 7:
                case 8:
                case 13:
                case 14:
                case 16:
                case 17:
                default:
                    i3 = 3;
                    break;
                case 3:
                case 4:
                case 9:
                case 10:
                case 11:
                case 12:
                case 18:
                    i3 = 5;
                    break;
                case 5:
                case 6:
                    i3 = 9;
                    i5++;
                    break;
                case 15:
                    i3 = 4;
                    break;
            }
            i5++;
            i6 = i3 + i6;
        }
        this.e = i4;
        this.f186b = i6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.a.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      b.a.a.a.a(java.lang.String, java.lang.String):b.a.a.a
      b.a.a.a.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.e.a(int, char[], boolean, b.a.a.a):int
     arg types: [int, char[], int, ?[OBJECT, ARRAY]]
     candidates:
      b.a.a.e.a(int, char[], java.lang.String, b.a.a.a):int
      b.a.a.e.a(int, char[], boolean, b.a.a.a):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.e.a(int, char[], boolean, b.a.a.a):int
     arg types: [int, char[], int, b.a.a.a]
     candidates:
      b.a.a.e.a(int, char[], java.lang.String, b.a.a.a):int
      b.a.a.e.a(int, char[], boolean, b.a.a.a):int */
    private int a(int i, char[] cArr, String str, a aVar) {
        int i2 = 0;
        if (aVar == null) {
            switch (this.f185a[i] & DefaultClassResolver.NAME) {
                case 64:
                    return a(i + 3, cArr, true, (a) null);
                case 91:
                    return a(i + 1, cArr, false, (a) null);
                case 101:
                    return i + 5;
                default:
                    return i + 3;
            }
        } else {
            int i3 = i + 1;
            switch (this.f185a[i] & DefaultClassResolver.NAME) {
                case 64:
                    return a(i3 + 2, cArr, true, aVar.a(str, a(i3, cArr)));
                case 66:
                    aVar.a(str, new Byte((byte) d(this.c[b(i3)])));
                    return i3 + 2;
                case 67:
                    aVar.a(str, new Character((char) d(this.c[b(i3)])));
                    return i3 + 2;
                case 68:
                case 70:
                case 73:
                case 74:
                    aVar.a(str, c(b(i3), cArr));
                    return i3 + 2;
                case 83:
                    aVar.a(str, new Short((short) d(this.c[b(i3)])));
                    return i3 + 2;
                case 90:
                    aVar.a(str, d(this.c[b(i3)]) == 0 ? Boolean.FALSE : Boolean.TRUE);
                    return i3 + 2;
                case 91:
                    int b2 = b(i3);
                    int i4 = i3 + 2;
                    if (b2 == 0) {
                        return a(i4 - 2, cArr, false, aVar.a(str));
                    }
                    int i5 = i4 + 1;
                    switch (this.f185a[i4] & DefaultClassResolver.NAME) {
                        case 66:
                            byte[] bArr = new byte[b2];
                            while (i2 < b2) {
                                bArr[i2] = (byte) d(this.c[b(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, bArr);
                            return i5 - 1;
                        case 67:
                            char[] cArr2 = new char[b2];
                            while (i2 < b2) {
                                cArr2[i2] = (char) d(this.c[b(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, cArr2);
                            return i5 - 1;
                        case 68:
                            double[] dArr = new double[b2];
                            while (i2 < b2) {
                                dArr[i2] = Double.longBitsToDouble(e(this.c[b(i5)]));
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, dArr);
                            return i5 - 1;
                        case 70:
                            float[] fArr = new float[b2];
                            while (i2 < b2) {
                                fArr[i2] = Float.intBitsToFloat(d(this.c[b(i5)]));
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, fArr);
                            return i5 - 1;
                        case 73:
                            int[] iArr = new int[b2];
                            while (i2 < b2) {
                                iArr[i2] = d(this.c[b(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, iArr);
                            return i5 - 1;
                        case 74:
                            long[] jArr = new long[b2];
                            while (i2 < b2) {
                                jArr[i2] = e(this.c[b(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, jArr);
                            return i5 - 1;
                        case 83:
                            short[] sArr = new short[b2];
                            while (i2 < b2) {
                                sArr[i2] = (short) d(this.c[b(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            aVar.a(str, sArr);
                            return i5 - 1;
                        case 90:
                            boolean[] zArr = new boolean[b2];
                            int i6 = i5;
                            for (int i7 = 0; i7 < b2; i7++) {
                                zArr[i7] = d(this.c[b(i6)]) != 0;
                                i6 += 3;
                            }
                            aVar.a(str, zArr);
                            return i6 - 1;
                        default:
                            return a(i5 - 3, cArr, false, aVar.a(str));
                    }
                case 99:
                    aVar.a(str, s.a(a(i3, cArr)));
                    return i3 + 2;
                case 101:
                    aVar.a(str, a(i3, cArr), a(i3 + 2, cArr));
                    return i3 + 4;
                case 115:
                    aVar.a(str, (Object) a(i3, cArr));
                    return i3 + 2;
                default:
                    return i3;
            }
        }
    }

    private int a(int i, char[] cArr, boolean z, a aVar) {
        int i2;
        int b2 = b(i);
        int i3 = i + 2;
        if (z) {
            int i4 = b2;
            i2 = i3;
            int i5 = i4;
            while (i5 > 0) {
                i5--;
                i2 = a(i2 + 2, cArr, a(i2, cArr), aVar);
            }
        } else {
            int i6 = b2;
            int i7 = i3;
            int i8 = i6;
            while (i8 > 0) {
                i8--;
                i7 = a(i2, cArr, (String) null, aVar);
            }
        }
        if (aVar != null) {
            aVar.a();
        }
        return i2;
    }

    private int a(Object[] objArr, int i, int i2, char[] cArr, o[] oVarArr) {
        int i3 = i2 + 1;
        switch (this.f185a[i2] & DefaultClassResolver.NAME) {
            case 0:
                objArr[i] = r.f205a;
                return i3;
            case 1:
                objArr[i] = r.f206b;
                return i3;
            case 2:
                objArr[i] = r.c;
                return i3;
            case 3:
                objArr[i] = r.d;
                return i3;
            case 4:
                objArr[i] = r.e;
                return i3;
            case 5:
                objArr[i] = r.f;
                return i3;
            case 6:
                objArr[i] = r.g;
                return i3;
            case 7:
                objArr[i] = b(i3, cArr);
                return i3 + 2;
            default:
                objArr[i] = a(b(i3), oVarArr);
                return i3 + 2;
        }
    }

    private c a(c[] cVarArr, String str, int i, int i2, char[] cArr, int i3, o[] oVarArr) {
        for (int i4 = 0; i4 < cVarArr.length; i4++) {
            if (cVarArr[i4].f181a.equals(str)) {
                return cVarArr[i4].a(this, i, i2, cArr, i3, oVarArr);
            }
        }
        return new c(str).a(this, i, i2, (char[]) null, -1, (o[]) null);
    }

    private String a(int i, int i2, char[] cArr) {
        int i3;
        int i4 = i + i2;
        byte[] bArr = this.f185a;
        char c2 = 0;
        char c3 = 0;
        int i5 = 0;
        while (i < i4) {
            int i6 = i + 1;
            byte b2 = bArr[i];
            switch (c3) {
                case 0:
                    byte b3 = b2 & DefaultClassResolver.NAME;
                    if (b3 >= 128) {
                        if (b3 < 224 && b3 > 191) {
                            c2 = (char) (b3 & 31);
                            c3 = 1;
                            i3 = i5;
                            break;
                        } else {
                            c2 = (char) (b3 & 15);
                            c3 = 2;
                            i3 = i5;
                            break;
                        }
                    } else {
                        i3 = i5 + 1;
                        cArr[i5] = (char) b3;
                        break;
                    }
                case 1:
                    cArr[i5] = (char) ((b2 & 63) | (c2 << 6));
                    i3 = i5 + 1;
                    c3 = 0;
                    break;
                case 2:
                    c2 = (char) ((c2 << 6) | (b2 & 63));
                    c3 = 1;
                    i3 = i5;
                    break;
                default:
                    i3 = i5;
                    break;
            }
            i5 = i3;
            i = i6;
        }
        return new String(cArr, 0, i5);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.p.a(int, java.lang.String, boolean):b.a.a.a
     arg types: [int, java.lang.String, int]
     candidates:
      b.a.a.p.a(b.a.a.o, int[], b.a.a.o[]):void
      b.a.a.p.a(int, java.lang.String, boolean):b.a.a.a */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.e.a(int, char[], boolean, b.a.a.a):int
     arg types: [int, char[], int, b.a.a.a]
     candidates:
      b.a.a.e.a(int, char[], java.lang.String, b.a.a.a):int
      b.a.a.e.a(int, char[], boolean, b.a.a.a):int */
    private void a(int i, String str, char[] cArr, boolean z, p pVar) {
        int i2 = i + 1;
        byte b2 = this.f185a[i] & DefaultClassResolver.NAME;
        int length = s.d(str).length - b2;
        int i3 = 0;
        while (i3 < length) {
            a a2 = pVar.a(i3, "Ljava/lang/Synthetic;", false);
            if (a2 != null) {
                a2.a();
            }
            i3++;
        }
        while (true) {
            int i4 = i3;
            if (i4 < b2 + length) {
                i2 += 2;
                for (int b3 = b(i2); b3 > 0; b3--) {
                    i2 = a(i2 + 2, cArr, true, pVar.a(i4, a(i2, cArr), z));
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    public int a(int i) {
        return this.f185a[i] & DefaultClassResolver.NAME;
    }

    /* access modifiers changed from: protected */
    public o a(int i, o[] oVarArr) {
        if (oVarArr[i] == null) {
            oVarArr[i] = new o();
        }
        return oVarArr[i];
    }

    public String a(int i, char[] cArr) {
        int b2 = b(i);
        String str = this.d[b2];
        if (str != null) {
            return str;
        }
        int i2 = this.c[b2];
        String[] strArr = this.d;
        String a2 = a(i2 + 2, b(i2), cArr);
        strArr[b2] = a2;
        return a2;
    }

    public void a(f fVar, int i) {
        a(fVar, new c[0], i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.e.a(int, char[], boolean, b.a.a.a):int
     arg types: [int, char[], int, b.a.a.a]
     candidates:
      b.a.a.e.a(int, char[], java.lang.String, b.a.a.a):int
      b.a.a.e.a(int, char[], boolean, b.a.a.a):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.e.a(int, java.lang.String, char[], boolean, b.a.a.p):void
     arg types: [int, java.lang.String, char[], int, b.a.a.p]
     candidates:
      b.a.a.e.a(java.lang.Object[], int, int, char[], b.a.a.o[]):int
      b.a.a.e.a(int, java.lang.String, char[], boolean, b.a.a.p):void */
    public void a(f fVar, c[] cVarArr, int i) {
        int i2;
        int i3;
        String[] strArr;
        Object[] objArr;
        Object[] objArr2;
        int i4;
        int i5;
        byte b2;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int b3;
        int i13;
        boolean z;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        c cVar;
        int i19;
        boolean z2;
        int i20;
        int i21;
        int i22;
        int i23;
        int i24;
        int i25;
        c cVar2;
        int i26;
        int i27;
        int i28;
        int i29;
        c cVar3;
        int i30;
        int i31;
        int[] iArr;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        int i32;
        c cVar4;
        int i33;
        int i34;
        String str6;
        String str7;
        byte[] bArr = this.f185a;
        char[] cArr = new char[this.e];
        int i35 = 0;
        int i36 = 0;
        c cVar5 = null;
        int i37 = this.f186b;
        int b4 = b(i37);
        String b5 = b(i37 + 2, cArr);
        int i38 = this.c[b(i37 + 4)];
        String a2 = i38 == 0 ? null : a(i38, cArr);
        String[] strArr2 = new String[b(i37 + 6)];
        int i39 = 0;
        int i40 = i37 + 8;
        int i41 = 0;
        while (true) {
            i2 = i40;
            if (i41 >= strArr2.length) {
                break;
            }
            strArr2[i41] = b(i2, cArr);
            i40 = i2 + 2;
            i41++;
        }
        boolean z3 = (i & 1) != 0;
        boolean z4 = (i & 2) != 0;
        boolean z5 = (i & 8) != 0;
        int i42 = i2 + 2;
        for (int b6 = b(i2); b6 > 0; b6--) {
            i42 += 8;
            for (int b7 = b(i42 + 6); b7 > 0; b7--) {
                i42 += d(i42 + 2) + 6;
            }
        }
        int b8 = b(i42);
        int i43 = i42 + 2;
        for (int i44 = b8; i44 > 0; i44--) {
            i43 += 8;
            for (int b9 = b(i43 + 6); b9 > 0; b9--) {
                i43 += d(i43 + 2) + 6;
            }
        }
        String str8 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        int[] iArr2 = null;
        int b10 = b(i43);
        int i45 = i43 + 2;
        while (b10 > 0) {
            String a3 = a(i45, cArr);
            if ("SourceFile".equals(a3)) {
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = a(i45 + 6, cArr);
                i32 = i39;
                iArr = iArr2;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else if ("InnerClasses".equals(a3)) {
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i45 + 6;
                iArr = iArr2;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else if ("EnclosingMethod".equals(a3)) {
                String b11 = b(i45 + 6, cArr);
                int b12 = b(i45 + 8);
                if (b12 != 0) {
                    str7 = a(this.c[b12], cArr);
                    str6 = a(this.c[b12] + 2, cArr);
                } else {
                    str6 = str13;
                    str7 = str12;
                }
                str3 = b11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
                str2 = str7;
                str = str6;
                iArr = iArr2;
            } else if ("Signature".equals(a3)) {
                str8 = a(i45 + 6, cArr);
                iArr = iArr2;
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else if ("RuntimeVisibleAnnotations".equals(a3)) {
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i45 + 6;
                iArr = iArr2;
            } else if ("Deprecated".equals(a3)) {
                b4 |= 131072;
                iArr = iArr2;
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else if ("Synthetic".equals(a3)) {
                b4 |= 266240;
                iArr = iArr2;
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else if ("SourceDebugExtension".equals(a3)) {
                int d2 = d(i45 + 2);
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = a(i45 + 6, d2, new char[d2]);
                str5 = str9;
                i32 = i39;
                iArr = iArr2;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else if ("RuntimeInvisibleAnnotations".equals(a3)) {
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i45 + 6;
                i34 = i35;
                iArr = iArr2;
            } else if ("BootstrapMethods".equals(a3)) {
                int b13 = b(i45 + 6);
                iArr = new int[b13];
                int i46 = i45 + 8;
                for (int i47 = 0; i47 < b13; i47++) {
                    iArr[i47] = i46;
                    i46 += (b(i46 + 2) + 2) << 1;
                }
                str = str13;
                str2 = str12;
                str3 = str11;
                str4 = str10;
                str5 = str9;
                i32 = i39;
                cVar4 = cVar5;
                i33 = i36;
                i34 = i35;
            } else {
                c a4 = a(cVarArr, a3, i45 + 6, d(i45 + 2), cArr, -1, null);
                if (a4 != null) {
                    a4.c = cVar5;
                    str = str13;
                    str2 = str12;
                    str3 = str11;
                    str4 = str10;
                    str5 = str9;
                    i32 = i39;
                    cVar4 = a4;
                    i33 = i36;
                    i34 = i35;
                    iArr = iArr2;
                } else {
                    iArr = iArr2;
                    str = str13;
                    str2 = str12;
                    str3 = str11;
                    str4 = str10;
                    str5 = str9;
                    i32 = i39;
                    cVar4 = cVar5;
                    i33 = i36;
                    i34 = i35;
                }
            }
            iArr2 = iArr;
            b10--;
            i39 = i32;
            i45 += d(i45 + 2) + 6;
            cVar5 = cVar4;
            i36 = i33;
            i35 = i34;
            str12 = str2;
            str11 = str3;
            str10 = str4;
            str9 = str5;
            str13 = str;
        }
        fVar.a(d(4), b4, b5, str8, a2, strArr2);
        if (!z4 && !(str9 == null && str10 == null)) {
            fVar.a(str9, str10);
        }
        if (str11 != null) {
            fVar.a(str11, str12, str13);
        }
        int i48 = 1;
        while (i48 >= 0) {
            int i49 = i48 == 0 ? i36 : i35;
            if (i49 != 0) {
                int i50 = i49 + 2;
                int b14 = b(i49);
                while (b14 > 0) {
                    b14--;
                    i50 = a(i50 + 2, cArr, true, fVar.a(a(i50, cArr), i48 != 0));
                }
            }
            i48--;
        }
        while (cVar5 != null) {
            c cVar6 = cVar5.c;
            cVar5.c = null;
            fVar.a(cVar5);
            cVar5 = cVar6;
        }
        if (i39 != 0) {
            int b15 = b(i39);
            int i51 = i39 + 2;
            while (b15 > 0) {
                fVar.a(b(i51) == 0 ? null : b(i51, cArr), b(i51 + 2) == 0 ? null : b(i51 + 2, cArr), b(i51 + 4) == 0 ? null : a(i51 + 4, cArr), b(i51 + 6));
                b15--;
                i51 += 8;
            }
        }
        int i52 = i2 + 2;
        for (int b16 = b(i2); b16 > 0; b16--) {
            int b17 = b(i52);
            String a5 = a(i52 + 2, cArr);
            String a6 = a(i52 + 4, cArr);
            int i53 = 0;
            String str14 = null;
            int i54 = 0;
            int i55 = 0;
            c cVar7 = null;
            int b18 = b(i52 + 6);
            i52 += 8;
            while (b18 > 0) {
                String a7 = a(i52, cArr);
                if ("ConstantValue".equals(a7)) {
                    i28 = b(i52 + 6);
                    i29 = b17;
                    cVar3 = cVar7;
                    i30 = i55;
                    i31 = i54;
                } else if ("Signature".equals(a7)) {
                    str14 = a(i52 + 6, cArr);
                    i28 = i53;
                    i29 = b17;
                    cVar3 = cVar7;
                    i30 = i55;
                    i31 = i54;
                } else if ("Deprecated".equals(a7)) {
                    i29 = 131072 | b17;
                    cVar3 = cVar7;
                    i30 = i55;
                    i31 = i54;
                    i28 = i53;
                } else if ("Synthetic".equals(a7)) {
                    i29 = 266240 | b17;
                    cVar3 = cVar7;
                    i30 = i55;
                    i31 = i54;
                    i28 = i53;
                } else if ("RuntimeVisibleAnnotations".equals(a7)) {
                    i29 = b17;
                    cVar3 = cVar7;
                    i30 = i55;
                    i31 = i52 + 6;
                    i28 = i53;
                } else if ("RuntimeInvisibleAnnotations".equals(a7)) {
                    i29 = b17;
                    cVar3 = cVar7;
                    i30 = i52 + 6;
                    i31 = i54;
                    i28 = i53;
                } else {
                    c a8 = a(cVarArr, a7, i52 + 6, d(i52 + 2), cArr, -1, null);
                    if (a8 != null) {
                        a8.c = cVar7;
                        i29 = b17;
                        cVar3 = a8;
                        i30 = i55;
                        i31 = i54;
                        i28 = i53;
                    } else {
                        i28 = i53;
                        i29 = b17;
                        cVar3 = cVar7;
                        i30 = i55;
                        i31 = i54;
                    }
                }
                i53 = i28;
                b18--;
                b17 = i29;
                i52 += d(i52 + 2) + 6;
                cVar7 = cVar3;
                i55 = i30;
                i54 = i31;
            }
            i a9 = fVar.a(b17, a5, a6, str14, i53 == 0 ? null : c(i53, cArr));
            if (a9 != null) {
                int i56 = 1;
                while (i56 >= 0) {
                    int i57 = i56 == 0 ? i55 : i54;
                    if (i57 != 0) {
                        int i58 = i57 + 2;
                        int b19 = b(i57);
                        while (b19 > 0) {
                            b19--;
                            i58 = a(i58 + 2, cArr, true, a9.a(a(i58, cArr), i56 != 0));
                        }
                    }
                    i56--;
                }
                while (cVar7 != null) {
                    c cVar8 = cVar7.c;
                    cVar7.c = null;
                    a9.a(cVar7);
                    cVar7 = cVar8;
                }
                a9.a();
            }
        }
        int i59 = i52 + 2;
        for (int b20 = b(i52); b20 > 0; b20--) {
            int i60 = i59 + 6;
            int b21 = b(i59);
            String a10 = a(i59 + 2, cArr);
            String a11 = a(i59 + 4, cArr);
            String str15 = null;
            int i61 = 0;
            int i62 = 0;
            int i63 = 0;
            int i64 = 0;
            int i65 = 0;
            c cVar9 = null;
            int i66 = 0;
            int i67 = 0;
            int b22 = b(i59 + 6);
            i59 += 8;
            while (b22 > 0) {
                String a12 = a(i59, cArr);
                int d3 = d(i59 + 2);
                int i68 = i59 + 6;
                if ("Code".equals(a12)) {
                    if (!z3) {
                        i20 = i65;
                        i21 = i64;
                        i22 = i63;
                        i23 = i67;
                        i24 = i68;
                        i25 = b21;
                        cVar2 = cVar9;
                        i26 = i62;
                        i27 = i61;
                    }
                    i20 = i65;
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = b21;
                    cVar2 = cVar9;
                    i26 = i62;
                    i27 = i61;
                } else if ("Exceptions".equals(a12)) {
                    i20 = i65;
                    i21 = i64;
                    i22 = i63;
                    i23 = i68;
                    i24 = i66;
                    i25 = b21;
                    cVar2 = cVar9;
                    i26 = i62;
                    i27 = i61;
                } else if ("Signature".equals(a12)) {
                    str15 = a(i68, cArr);
                    i20 = i65;
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = b21;
                    cVar2 = cVar9;
                    i26 = i62;
                    i27 = i61;
                } else if ("Deprecated".equals(a12)) {
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = 131072 | b21;
                    i26 = i62;
                    i27 = i61;
                    i20 = i65;
                    cVar2 = cVar9;
                } else if ("RuntimeVisibleAnnotations".equals(a12)) {
                    i20 = i65;
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = b21;
                    cVar2 = cVar9;
                    i26 = i62;
                    i27 = i68;
                } else if ("AnnotationDefault".equals(a12)) {
                    i20 = i65;
                    i21 = i64;
                    i22 = i68;
                    i23 = i67;
                    i24 = i66;
                    i27 = i61;
                    cVar2 = cVar9;
                    i26 = i62;
                    i25 = b21;
                } else if ("Synthetic".equals(a12)) {
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = 266240 | b21;
                    i26 = i62;
                    i27 = i61;
                    i20 = i65;
                    cVar2 = cVar9;
                } else if ("RuntimeInvisibleAnnotations".equals(a12)) {
                    i20 = i65;
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = b21;
                    cVar2 = cVar9;
                    i26 = i68;
                    i27 = i61;
                } else if ("RuntimeVisibleParameterAnnotations".equals(a12)) {
                    i20 = i65;
                    i21 = i68;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i26 = i62;
                    cVar2 = cVar9;
                    i25 = b21;
                    i27 = i61;
                } else if ("RuntimeInvisibleParameterAnnotations".equals(a12)) {
                    i20 = i68;
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    cVar2 = cVar9;
                    i25 = b21;
                    i26 = i62;
                    i27 = i61;
                } else {
                    c a13 = a(cVarArr, a12, i68, d3, cArr, -1, null);
                    if (a13 != null) {
                        a13.c = cVar9;
                        i21 = i64;
                        i22 = i63;
                        i23 = i67;
                        i24 = i66;
                        i25 = b21;
                        i26 = i62;
                        i27 = i61;
                        int i69 = i65;
                        cVar2 = a13;
                        i20 = i69;
                    }
                    i20 = i65;
                    i21 = i64;
                    i22 = i63;
                    i23 = i67;
                    i24 = i66;
                    i25 = b21;
                    cVar2 = cVar9;
                    i26 = i62;
                    i27 = i61;
                }
                b22--;
                i66 = i24;
                b21 = i25;
                i59 = d3 + i68;
                cVar9 = cVar2;
                i62 = i26;
                i61 = i27;
                i65 = i20;
                i67 = i23;
                i64 = i21;
                i63 = i22;
            }
            if (i67 == 0) {
                i3 = i67;
                strArr = null;
            } else {
                String[] strArr3 = new String[b(i67)];
                i3 = i67 + 2;
                for (int i70 = 0; i70 < strArr3.length; i70++) {
                    strArr3[i70] = b(i3, cArr);
                    i3 += 2;
                }
                strArr = strArr3;
            }
            p a14 = fVar.a(b21, a10, a11, str15, strArr);
            if (a14 != null) {
                if (a14 instanceof q) {
                    q qVar = (q) a14;
                    if (qVar.c.d == this && str15 == qVar.d) {
                        boolean z6 = false;
                        if (strArr == null) {
                            z2 = qVar.g == 0;
                        } else {
                            if (strArr.length == qVar.g) {
                                z6 = true;
                                int i71 = i3;
                                int length = strArr.length - 1;
                                while (true) {
                                    if (length >= 0) {
                                        i71 -= 2;
                                        if (qVar.h[length] != b(i71)) {
                                            z2 = false;
                                        } else {
                                            length--;
                                        }
                                    }
                                }
                            }
                            z2 = z6;
                        }
                        if (z2) {
                            qVar.e = i60;
                            qVar.f = i59 - i60;
                        }
                    }
                }
                if (i63 != 0) {
                    a a15 = a14.a();
                    a(i63, cArr, (String) null, a15);
                    if (a15 != null) {
                        a15.a();
                    }
                }
                int i72 = 1;
                while (i72 >= 0) {
                    int i73 = i72 == 0 ? i62 : i61;
                    if (i73 != 0) {
                        int i74 = i73 + 2;
                        int b23 = b(i73);
                        while (b23 > 0) {
                            b23--;
                            i74 = a(i74 + 2, cArr, true, a14.a(a(i74, cArr), i72 != 0));
                        }
                    }
                    i72--;
                }
                if (i64 != 0) {
                    a(i64, a11, cArr, true, a14);
                }
                if (i65 != 0) {
                    a(i65, a11, cArr, false, a14);
                }
                while (cVar9 != null) {
                    c cVar10 = cVar9.c;
                    cVar9.c = null;
                    a14.a(cVar9);
                    cVar9 = cVar10;
                }
            }
            if (!(a14 == null || i66 == 0)) {
                int b24 = b(i66);
                int b25 = b(i66 + 2);
                int d4 = d(i66 + 4);
                int i75 = i66 + 8;
                int i76 = i75 + d4;
                a14.b();
                o[] oVarArr = new o[(d4 + 2)];
                a(d4 + 1, oVarArr);
                int i77 = i75;
                while (i77 < i76) {
                    int i78 = i77 - i75;
                    switch (g.c[bArr[i77] & DefaultClassResolver.NAME]) {
                        case 0:
                        case 4:
                            i19 = i77 + 1;
                            break;
                        case 1:
                        case 3:
                        case 11:
                            i19 = i77 + 2;
                            break;
                        case 2:
                        case 5:
                        case 6:
                        case 12:
                        case 13:
                            i19 = i77 + 3;
                            break;
                        case 7:
                        case 8:
                            i19 = i77 + 5;
                            break;
                        case 9:
                            a(c(i77 + 1) + i78, oVarArr);
                            i19 = i77 + 3;
                            break;
                        case 10:
                            a(d(i77 + 1) + i78, oVarArr);
                            i19 = i77 + 5;
                            break;
                        case 14:
                            int i79 = (i77 + 4) - (i78 & 3);
                            a(d(i79) + i78, oVarArr);
                            i19 = i79 + 12;
                            int d5 = (d(i79 + 8) - d(i79 + 4)) + 1;
                            while (d5 > 0) {
                                a(d(i19) + i78, oVarArr);
                                d5--;
                                i19 += 4;
                            }
                            break;
                        case 15:
                            int i80 = (i77 + 4) - (i78 & 3);
                            a(d(i80) + i78, oVarArr);
                            int i81 = i80 + 8;
                            int d6 = d(i80 + 4);
                            while (d6 > 0) {
                                a(d(i19 + 4) + i78, oVarArr);
                                d6--;
                                i81 = i19 + 8;
                            }
                            break;
                        case 16:
                        default:
                            i19 = i77 + 4;
                            break;
                        case 17:
                            if ((bArr[i77 + 1] & DefaultClassResolver.NAME) != 132) {
                                i19 = i77 + 4;
                                break;
                            } else {
                                i19 = i77 + 6;
                                break;
                            }
                    }
                    i77 = i19;
                }
                int i82 = i77 + 2;
                for (int b26 = b(i77); b26 > 0; b26--) {
                    o a16 = a(b(i82), oVarArr);
                    o a17 = a(b(i82 + 2), oVarArr);
                    o a18 = a(b(i82 + 4), oVarArr);
                    int b27 = b(i82 + 6);
                    if (b27 == 0) {
                        a14.a(a16, a17, a18, (String) null);
                    } else {
                        a14.a(a16, a17, a18, a(this.c[b27], cArr));
                    }
                    i82 += 8;
                }
                int i83 = 0;
                int i84 = 0;
                int i85 = 0;
                int i86 = 0;
                int i87 = 0;
                int i88 = 0;
                boolean z7 = true;
                c cVar11 = null;
                int b28 = b(i82);
                int i89 = i82 + 2;
                while (b28 > 0) {
                    String a19 = a(i89, cArr);
                    if ("LocalVariableTable".equals(a19)) {
                        if (!z4) {
                            int i90 = i89 + 6;
                            int i91 = i89 + 8;
                            for (int b29 = b(i89 + 6); b29 > 0; b29--) {
                                int b30 = b(i91);
                                if (oVarArr[b30] == null) {
                                    a(b30, oVarArr).f201a |= 1;
                                }
                                int b31 = b30 + b(i91 + 2);
                                if (oVarArr[b31] == null) {
                                    a(b31, oVarArr).f201a |= 1;
                                }
                                i91 += 10;
                            }
                            i14 = i87;
                            i15 = i86;
                            i16 = i85;
                            i17 = i84;
                            i18 = i90;
                            z = z7;
                        }
                        z = z7;
                        i14 = i87;
                        i15 = i86;
                        i16 = i85;
                        i17 = i84;
                        i18 = i83;
                    } else if ("LocalVariableTypeTable".equals(a19)) {
                        i14 = i87;
                        i15 = i86;
                        i16 = i85;
                        i17 = i89 + 6;
                        z = z7;
                        i18 = i83;
                    } else {
                        if ("LineNumberTable".equals(a19)) {
                            if (!z4) {
                                int i92 = i89 + 8;
                                for (int b32 = b(i89 + 6); b32 > 0; b32--) {
                                    int b33 = b(i92);
                                    if (oVarArr[b33] == null) {
                                        a(b33, oVarArr).f201a |= 1;
                                    }
                                    oVarArr[b33].f202b = b(i92 + 2);
                                    i92 += 4;
                                }
                            }
                        } else if ("StackMapTable".equals(a19)) {
                            if ((i & 4) == 0) {
                                i16 = i89 + 8;
                                i17 = i84;
                                i18 = i83;
                                i15 = d(i89 + 2);
                                i14 = b(i89 + 6);
                                z = z7;
                            }
                        } else if (!"StackMap".equals(a19)) {
                            int i93 = 0;
                            c cVar12 = cVar11;
                            while (i93 < cVarArr.length) {
                                if (!cVarArr[i93].f181a.equals(a19) || (cVar = cVarArr[i93].a(this, i89 + 6, d(i89 + 2), cArr, i75 - 8, oVarArr)) == null) {
                                    cVar = cVar12;
                                } else {
                                    cVar.c = cVar12;
                                }
                                i93++;
                                cVar12 = cVar;
                            }
                            z = z7;
                            i15 = i86;
                            cVar11 = cVar12;
                            i17 = i84;
                            i14 = i87;
                            i16 = i85;
                            i18 = i83;
                        } else if ((i & 4) == 0) {
                            i16 = i89 + 8;
                            i15 = d(i89 + 2);
                            i14 = b(i89 + 6);
                            z = false;
                            i17 = i84;
                            i18 = i83;
                        }
                        z = z7;
                        i14 = i87;
                        i15 = i86;
                        i16 = i85;
                        i17 = i84;
                        i18 = i83;
                    }
                    z7 = z;
                    i84 = i17;
                    i83 = i18;
                    b28--;
                    i89 += d(i89 + 2) + 6;
                    i86 = i15;
                    i85 = i16;
                    i87 = i14;
                }
                if (i85 != 0) {
                    Object[] objArr3 = new Object[b25];
                    objArr = new Object[b24];
                    if (z5) {
                        if ((b21 & 8) != 0) {
                            i13 = 0;
                        } else if ("<init>".equals(a10)) {
                            i13 = 1;
                            objArr3[0] = r.g;
                        } else {
                            i13 = 1;
                            objArr3[0] = b(this.f186b + 2, cArr);
                        }
                        int i94 = 1;
                        while (true) {
                            int i95 = i94 + 1;
                            switch (a11.charAt(i94)) {
                                case 'B':
                                case 'C':
                                case 'I':
                                case 'S':
                                case 'Z':
                                    objArr3[i12] = r.f206b;
                                    i12++;
                                    i94 = i95;
                                case 'D':
                                    objArr3[i12] = r.d;
                                    i12++;
                                    i94 = i95;
                                case 'F':
                                    objArr3[i12] = r.c;
                                    i12++;
                                    i94 = i95;
                                case 'J':
                                    objArr3[i12] = r.e;
                                    i12++;
                                    i94 = i95;
                                case 'L':
                                    while (a11.charAt(i95) != ';') {
                                        i95++;
                                    }
                                    int i96 = i94 + 1;
                                    i94 = i95 + 1;
                                    objArr3[i12] = a11.substring(i96, i95);
                                    i12++;
                                case '[':
                                    while (a11.charAt(i95) == '[') {
                                        i95++;
                                    }
                                    if (a11.charAt(i95) == 'L') {
                                        int i97 = i95 + 1;
                                        while (a11.charAt(i97) != ';') {
                                            i97++;
                                        }
                                        i95 = i97;
                                    }
                                    int i98 = i95 + 1;
                                    objArr3[i12] = a11.substring(i94, i98);
                                    i12++;
                                    i94 = i98;
                            }
                        }
                    } else {
                        i12 = 0;
                    }
                    i4 = -1;
                    for (int i99 = i85; i99 < (i85 + i86) - 2; i99++) {
                        if (bArr[i99] == 8 && (b3 = b(i99 + 1)) >= 0 && b3 < d4 && (bArr[i75 + b3] & DefaultClassResolver.NAME) == 187) {
                            a(b3, oVarArr);
                        }
                    }
                    i88 = i12;
                    objArr2 = objArr3;
                } else {
                    objArr = null;
                    objArr2 = null;
                    i4 = 0;
                }
                Object[] objArr4 = objArr2;
                int i100 = 0;
                int i101 = 0;
                int i102 = i88;
                int i103 = i85;
                int i104 = 0;
                int i105 = i87;
                int i106 = i4;
                for (int i107 = i75; i107 < i76; i107 = i5) {
                    int i108 = i107 - i75;
                    o oVar = oVarArr[i108];
                    if (oVar != null) {
                        a14.a(oVar);
                        if (!z4 && oVar.f202b > 0) {
                            a14.b(oVar.f202b, oVar);
                        }
                    }
                    int i109 = i105;
                    while (objArr4 != null && (i106 == i108 || i106 == -1)) {
                        if (!z7 || z5) {
                            a14.a(-1, i102, objArr4, i100, objArr);
                        } else if (i106 != -1) {
                            a14.a(i104, i101, objArr4, i100, objArr);
                        }
                        if (i109 > 0) {
                            if (z7) {
                                i7 = i103 + 1;
                                b2 = bArr[i103] & DefaultClassResolver.NAME;
                                i6 = i106;
                            } else {
                                b2 = DefaultClassResolver.NAME;
                                i6 = -1;
                                i7 = i103;
                            }
                            int i110 = 0;
                            if (b2 < 64) {
                                i10 = 3;
                                i9 = 0;
                                i11 = b2;
                            } else if (b2 < 128) {
                                i11 = b2 - 64;
                                i8 = a(objArr, 0, i8, cArr, oVarArr);
                                i10 = 4;
                                i9 = 1;
                            } else {
                                int b34 = b(i8);
                                i8 += 2;
                                if (b2 == 247) {
                                    i8 = a(objArr, 0, i8, cArr, oVarArr);
                                    i10 = 4;
                                    i9 = 1;
                                    i11 = b34;
                                } else if (b2 >= 248 && b2 < 251) {
                                    int i111 = 251 - b2;
                                    i102 -= i111;
                                    i10 = 2;
                                    i110 = i111;
                                    i9 = 0;
                                    i11 = b34;
                                } else if (b2 == 251) {
                                    i10 = 3;
                                    i9 = 0;
                                    i11 = b34;
                                } else if (b2 < 255) {
                                    int i112 = i8;
                                    int i113 = z5 ? i102 : 0;
                                    int i114 = b2 - 251;
                                    while (i114 > 0) {
                                        i112 = a(objArr4, i113, i112, cArr, oVarArr);
                                        i114--;
                                        i113++;
                                    }
                                    int i115 = b2 - 251;
                                    i102 += i115;
                                    i10 = 1;
                                    i8 = i112;
                                    i110 = i115;
                                    i9 = 0;
                                    i11 = b34;
                                } else {
                                    int b35 = b(i8);
                                    int i116 = i8 + 2;
                                    int i117 = 0;
                                    int i118 = b35;
                                    while (i118 > 0) {
                                        i116 = a(objArr4, i117, i116, cArr, oVarArr);
                                        i118--;
                                        i117++;
                                    }
                                    int b36 = b(i116);
                                    i8 = i116 + 2;
                                    int i119 = 0;
                                    int i120 = b36;
                                    while (i120 > 0) {
                                        i8 = a(objArr, i119, i8, cArr, oVarArr);
                                        i120--;
                                        i119++;
                                    }
                                    i9 = b36;
                                    i110 = b35;
                                    i10 = 0;
                                    i102 = b35;
                                    i11 = b34;
                                }
                            }
                            int i121 = i11 + 1 + i6;
                            a(i121, oVarArr);
                            i100 = i9;
                            i101 = i110;
                            i104 = i10;
                            i109--;
                            i106 = i121;
                            i103 = i8;
                        } else {
                            objArr4 = null;
                        }
                    }
                    byte b37 = bArr[i107] & DefaultClassResolver.NAME;
                    switch (g.c[b37]) {
                        case 0:
                            a14.a(b37);
                            i5 = i107 + 1;
                            break;
                        case 1:
                            a14.a(b37, bArr[i107 + 1]);
                            i5 = i107 + 2;
                            break;
                        case 2:
                            a14.a(b37, c(i107 + 1));
                            i5 = i107 + 3;
                            break;
                        case 3:
                            a14.b(b37, bArr[i107 + 1] & DefaultClassResolver.NAME);
                            i5 = i107 + 2;
                            break;
                        case 4:
                            if (b37 > 54) {
                                int i122 = b37 - 59;
                                a14.b((i122 >> 2) + 54, i122 & 3);
                            } else {
                                int i123 = b37 - 26;
                                a14.b((i123 >> 2) + 21, i123 & 3);
                            }
                            i5 = i107 + 1;
                            break;
                        case 5:
                            a14.a(b37, b(i107 + 1, cArr));
                            i5 = i107 + 3;
                            break;
                        case 6:
                        case 7:
                            int i124 = this.c[b(i107 + 1)];
                            String b38 = b(i124, cArr);
                            int i125 = this.c[b(i124 + 2)];
                            String a20 = a(i125, cArr);
                            String a21 = a(i125 + 2, cArr);
                            if (b37 < 182) {
                                a14.a(b37, b38, a20, a21);
                            } else {
                                a14.b(b37, b38, a20, a21);
                            }
                            if (b37 != 185) {
                                i5 = i107 + 3;
                                break;
                            } else {
                                i5 = i107 + 5;
                                break;
                            }
                        case 8:
                            int i126 = this.c[b(i107 + 1)];
                            int i127 = iArr2[b(i126)];
                            int i128 = this.c[b(i126 + 2)];
                            String a22 = a(i128, cArr);
                            String a23 = a(i128 + 2, cArr);
                            l lVar = (l) c(b(i127), cArr);
                            int b39 = b(i127 + 2);
                            Object[] objArr5 = new Object[b39];
                            int i129 = i127 + 4;
                            for (int i130 = 0; i130 < b39; i130++) {
                                objArr5[i130] = c(b(i129), cArr);
                                i129 += 2;
                            }
                            a14.a(a22, a23, lVar, objArr5);
                            i5 = i107 + 5;
                            break;
                        case 9:
                            a14.a(b37, oVarArr[c(i107 + 1) + i108]);
                            i5 = i107 + 3;
                            break;
                        case 10:
                            a14.a(b37 - 33, oVarArr[d(i107 + 1) + i108]);
                            i5 = i107 + 5;
                            break;
                        case 11:
                            a14.a(c(bArr[i107 + 1] & DefaultClassResolver.NAME, cArr));
                            i5 = i107 + 2;
                            break;
                        case 12:
                            a14.a(c(b(i107 + 1), cArr));
                            i5 = i107 + 3;
                            break;
                        case 13:
                            a14.c(bArr[i107 + 1] & DefaultClassResolver.NAME, bArr[i107 + 2]);
                            i5 = i107 + 3;
                            break;
                        case 14:
                            int i131 = (i107 + 4) - (i108 & 3);
                            int d7 = i108 + d(i131);
                            int d8 = d(i131 + 4);
                            int d9 = d(i131 + 8);
                            i5 = i131 + 12;
                            o[] oVarArr2 = new o[((d9 - d8) + 1)];
                            for (int i132 = 0; i132 < oVarArr2.length; i132++) {
                                oVarArr2[i132] = oVarArr[d(i5) + i108];
                                i5 += 4;
                            }
                            a14.a(d8, d9, oVarArr[d7], oVarArr2);
                            break;
                        case 15:
                            int i133 = (i107 + 4) - (i108 & 3);
                            int d10 = i108 + d(i133);
                            int d11 = d(i133 + 4);
                            int i134 = i133 + 8;
                            int[] iArr3 = new int[d11];
                            o[] oVarArr3 = new o[d11];
                            for (int i135 = 0; i135 < iArr3.length; i135++) {
                                iArr3[i135] = d(i5);
                                oVarArr3[i135] = oVarArr[d(i5 + 4) + i108];
                                i134 = i5 + 8;
                            }
                            a14.a(oVarArr[d10], iArr3, oVarArr3);
                            break;
                        case 16:
                        default:
                            a14.a(b(i107 + 1, cArr), bArr[i107 + 3] & DefaultClassResolver.NAME);
                            i5 = i107 + 4;
                            break;
                        case 17:
                            byte b40 = bArr[i107 + 1] & DefaultClassResolver.NAME;
                            if (b40 != 132) {
                                a14.b(b40, b(i107 + 2));
                                i5 = i107 + 4;
                                break;
                            } else {
                                a14.c(b(i107 + 2), c(i107 + 4));
                                i5 = i107 + 6;
                                break;
                            }
                    }
                    i105 = i109;
                }
                o oVar2 = oVarArr[i76 - i75];
                if (oVar2 != null) {
                    a14.a(oVar2);
                }
                if (!z4 && i83 != 0) {
                    int[] iArr4 = null;
                    if (i84 != 0) {
                        int b41 = b(i84) * 3;
                        int i136 = i84 + 2;
                        iArr4 = new int[b41];
                        while (b41 > 0) {
                            int i137 = b41 - 1;
                            iArr4[i137] = i136 + 6;
                            int i138 = i137 - 1;
                            iArr4[i138] = b(i136 + 8);
                            b41 = i138 - 1;
                            iArr4[b41] = b(i136);
                            i136 += 10;
                        }
                    }
                    int i139 = i83 + 2;
                    for (int b42 = b(i83); b42 > 0; b42--) {
                        int b43 = b(i139);
                        int b44 = b(i139 + 2);
                        int b45 = b(i139 + 8);
                        String str16 = null;
                        if (iArr4 != null) {
                            int i140 = 0;
                            while (true) {
                                if (i140 < iArr4.length) {
                                    if (iArr4[i140] == b43 && iArr4[i140 + 1] == b45) {
                                        str16 = a(iArr4[i140 + 2], cArr);
                                    } else {
                                        i140 += 3;
                                    }
                                }
                            }
                        }
                        a14.a(a(i139 + 4, cArr), a(i139 + 6, cArr), str16, oVarArr[b43], oVarArr[b43 + b44], b45);
                        i139 += 10;
                    }
                }
                while (cVar11 != null) {
                    c cVar13 = cVar11.c;
                    cVar11.c = null;
                    a14.a(cVar11);
                    cVar11 = cVar13;
                }
                a14.d(b24, b25);
            }
            if (a14 != null) {
                a14.c();
            }
        }
        fVar.a();
    }

    public int b(int i) {
        byte[] bArr = this.f185a;
        return (bArr[i + 1] & DefaultClassResolver.NAME) | ((bArr[i] & DefaultClassResolver.NAME) << 8);
    }

    public String b(int i, char[] cArr) {
        return a(this.c[b(i)], cArr);
    }

    public Object c(int i, char[] cArr) {
        int i2 = this.c[i];
        switch (this.f185a[i2 - 1]) {
            case 3:
                return new Integer(d(i2));
            case 4:
                return new Float(Float.intBitsToFloat(d(i2)));
            case 5:
                return new Long(e(i2));
            case 6:
                return new Double(Double.longBitsToDouble(e(i2)));
            case 7:
                return s.b(a(i2, cArr));
            case 8:
                return a(i2, cArr);
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            default:
                int a2 = a(i2);
                int[] iArr = this.c;
                int i3 = iArr[b(i2 + 1)];
                String b2 = b(i3, cArr);
                int i4 = iArr[b(i3 + 2)];
                return new l(a2, b2, a(i4, cArr), a(i4 + 2, cArr));
            case 16:
                return s.c(a(i2, cArr));
        }
    }

    public short c(int i) {
        byte[] bArr = this.f185a;
        return (short) ((bArr[i + 1] & DefaultClassResolver.NAME) | ((bArr[i] & DefaultClassResolver.NAME) << 8));
    }

    public int d(int i) {
        byte[] bArr = this.f185a;
        return (bArr[i + 3] & DefaultClassResolver.NAME) | ((bArr[i] & DefaultClassResolver.NAME) << 24) | ((bArr[i + 1] & DefaultClassResolver.NAME) << 16) | ((bArr[i + 2] & DefaultClassResolver.NAME) << 8);
    }

    public long e(int i) {
        return (((long) d(i)) << 32) | (((long) d(i + 4)) & 4294967295L);
    }
}
