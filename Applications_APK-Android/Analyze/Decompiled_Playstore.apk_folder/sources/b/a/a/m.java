package b.a.a;

class m {

    /* renamed from: a  reason: collision with root package name */
    o f197a;

    /* renamed from: b  reason: collision with root package name */
    o f198b;
    o c;
    String d;
    int e;
    m f;

    m() {
    }

    static m a(m mVar, o oVar, o oVar2) {
        if (mVar == null) {
            return null;
        }
        mVar.f = a(mVar.f, oVar, oVar2);
        int i = mVar.f197a.c;
        int i2 = mVar.f198b.c;
        int i3 = oVar.c;
        int i4 = oVar2 == null ? Integer.MAX_VALUE : oVar2.c;
        if (i3 >= i2 || i4 <= i) {
            return mVar;
        }
        if (i3 <= i) {
            if (i4 >= i2) {
                return mVar.f;
            }
            mVar.f197a = oVar2;
            return mVar;
        } else if (i4 >= i2) {
            mVar.f198b = oVar;
            return mVar;
        } else {
            m mVar2 = new m();
            mVar2.f197a = oVar2;
            mVar2.f198b = mVar.f198b;
            mVar2.c = mVar.c;
            mVar2.d = mVar.d;
            mVar2.e = mVar.e;
            mVar2.f = mVar.f;
            mVar.f198b = oVar;
            mVar.f = mVar2;
            return mVar;
        }
    }
}
