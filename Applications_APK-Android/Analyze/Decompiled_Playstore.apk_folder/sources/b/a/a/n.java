package b.a.a;

final class n {

    /* renamed from: a  reason: collision with root package name */
    int f199a;

    /* renamed from: b  reason: collision with root package name */
    int f200b;
    int c;
    long d;
    String e;
    String f;
    String g;
    int h;
    n i;

    n() {
    }

    n(int i2) {
        this.f199a = i2;
    }

    n(int i2, n nVar) {
        this.f199a = i2;
        this.f200b = nVar.f200b;
        this.c = nVar.c;
        this.d = nVar.d;
        this.e = nVar.e;
        this.f = nVar.f;
        this.g = nVar.g;
        this.h = nVar.h;
    }

    /* access modifiers changed from: package-private */
    public void a(double d2) {
        this.f200b = 6;
        this.d = Double.doubleToRawLongBits(d2);
        this.h = Integer.MAX_VALUE & (this.f200b + ((int) d2));
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        this.f200b = 4;
        this.c = Float.floatToRawIntBits(f2);
        this.h = Integer.MAX_VALUE & (this.f200b + ((int) f2));
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.f200b = 3;
        this.c = i2;
        this.h = Integer.MAX_VALUE & (this.f200b + i2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        this.f200b = 33;
        this.c = i2;
        this.h = i3;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str, String str2, String str3) {
        this.f200b = i2;
        this.e = str;
        this.f = str2;
        this.g = str3;
        switch (i2) {
            case 1:
            case 7:
            case 8:
            case 16:
            case 30:
                this.h = (str.hashCode() + i2) & Integer.MAX_VALUE;
                return;
            case 12:
                this.h = ((str.hashCode() * str2.hashCode()) + i2) & Integer.MAX_VALUE;
                return;
            default:
                this.h = ((str.hashCode() * str2.hashCode() * str3.hashCode()) + i2) & Integer.MAX_VALUE;
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        this.f200b = 5;
        this.d = j;
        this.h = Integer.MAX_VALUE & (this.f200b + ((int) j));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, int i2) {
        this.f200b = 18;
        this.d = (long) i2;
        this.e = str;
        this.f = str2;
        this.h = Integer.MAX_VALUE & ((this.e.hashCode() * i2 * this.f.hashCode()) + 18);
    }

    /* access modifiers changed from: package-private */
    public boolean a(n nVar) {
        switch (this.f200b) {
            case 1:
            case 7:
            case 8:
            case 16:
            case 30:
                return nVar.e.equals(this.e);
            case 3:
            case 4:
                return nVar.c == this.c;
            case 5:
            case 6:
            case 32:
                return nVar.d == this.d;
            case 12:
                return nVar.e.equals(this.e) && nVar.f.equals(this.f);
            case 18:
                return nVar.d == this.d && nVar.e.equals(this.e) && nVar.f.equals(this.f);
            case 31:
                return nVar.c == this.c && nVar.e.equals(this.e);
            default:
                return nVar.e.equals(this.e) && nVar.f.equals(this.f) && nVar.g.equals(this.g);
        }
    }
}
