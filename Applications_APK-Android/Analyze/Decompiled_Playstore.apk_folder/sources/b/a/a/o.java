package b.a.a;

import com.esotericsoftware.kryo.util.DefaultClassResolver;

public class o {

    /* renamed from: a  reason: collision with root package name */
    int f201a;

    /* renamed from: b  reason: collision with root package name */
    int f202b;
    int c;
    int d;
    int e;
    k f;
    o g;
    h h;
    o i;
    private int j;
    private int[] k;

    private void a(int i2, int i3) {
        if (this.k == null) {
            this.k = new int[6];
        }
        if (this.j >= this.k.length) {
            int[] iArr = new int[(this.k.length + 6)];
            System.arraycopy(this.k, 0, iArr, 0, this.k.length);
            this.k = iArr;
        }
        int[] iArr2 = this.k;
        int i4 = this.j;
        this.j = i4 + 1;
        iArr2[i4] = i2;
        int[] iArr3 = this.k;
        int i5 = this.j;
        this.j = i5 + 1;
        iArr3[i5] = i3;
    }

    /* access modifiers changed from: package-private */
    public o a() {
        return this.f == null ? this : this.f.f194b;
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, int i2) {
        if ((this.f201a & 1024) == 0) {
            this.f201a |= 1024;
            this.k = new int[(((i2 - 1) / 32) + 1)];
        }
        int[] iArr = this.k;
        int i3 = (int) (j2 >>> 32);
        iArr[i3] = iArr[i3] | ((int) j2);
    }

    /* access modifiers changed from: package-private */
    public void a(o oVar, long j2, int i2) {
        while (r4 != null) {
            o oVar2 = r4.i;
            r4.i = null;
            if (oVar != null) {
                if ((r4.f201a & 2048) != 0) {
                    r4 = oVar2;
                } else {
                    r4.f201a |= 2048;
                    if ((r4.f201a & 256) != 0 && !r4.a(oVar)) {
                        h hVar = new h();
                        hVar.f189a = r4.d;
                        hVar.f190b = oVar.h.f190b;
                        hVar.c = r4.h;
                        r4.h = hVar;
                    }
                }
            } else if (r4.a(j2)) {
                r4 = oVar2;
            } else {
                r4.a(j2, i2);
            }
            o oVar3 = oVar2;
            for (h hVar2 = r4.h; hVar2 != null; hVar2 = hVar2.c) {
                if (((r4.f201a & 128) == 0 || hVar2 != r4.h.c) && hVar2.f190b.i == null) {
                    hVar2.f190b.i = oVar3;
                    oVar3 = hVar2.f190b;
                }
            }
            r4 = oVar3;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(q qVar, d dVar, int i2, boolean z) {
        if ((this.f201a & 2) == 0) {
            if (z) {
                a(-1 - i2, dVar.f184b);
                dVar.c(-1);
                return;
            }
            a(i2, dVar.f184b);
            dVar.b(-1);
        } else if (z) {
            dVar.c(this.c - i2);
        } else {
            dVar.b(this.c - i2);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(long j2) {
        return ((this.f201a & 1024) == 0 || (this.k[(int) (j2 >>> 32)] & ((int) j2)) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(o oVar) {
        if ((this.f201a & 1024) == 0 || (oVar.f201a & 1024) == 0) {
            return false;
        }
        for (int i2 = 0; i2 < this.k.length; i2++) {
            if ((this.k[i2] & oVar.k[i2]) != 0) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(q qVar, int i2, byte[] bArr) {
        int i3 = 0;
        this.f201a |= 2;
        this.c = i2;
        boolean z = false;
        while (i3 < this.j) {
            int i4 = i3 + 1;
            int i5 = this.k[i3];
            i3 = i4 + 1;
            int i6 = this.k[i4];
            if (i5 >= 0) {
                int i7 = i2 - i5;
                if (i7 < -32768 || i7 > 32767) {
                    byte b2 = bArr[i6 - 1] & DefaultClassResolver.NAME;
                    if (b2 <= 168) {
                        bArr[i6 - 1] = (byte) (b2 + 49);
                    } else {
                        bArr[i6 - 1] = (byte) (b2 + 20);
                    }
                    z = true;
                }
                bArr[i6] = (byte) (i7 >>> 8);
                bArr[i6 + 1] = (byte) i7;
            } else {
                int i8 = i5 + i2 + 1;
                int i9 = i6 + 1;
                bArr[i6] = (byte) (i8 >>> 24);
                int i10 = i9 + 1;
                bArr[i9] = (byte) (i8 >>> 16);
                bArr[i10] = (byte) (i8 >>> 8);
                bArr[i10 + 1] = (byte) i8;
            }
        }
        return z;
    }

    public String toString() {
        return new StringBuffer().append("L").append(System.identityHashCode(this)).toString();
    }
}
