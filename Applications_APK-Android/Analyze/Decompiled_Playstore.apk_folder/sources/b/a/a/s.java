package b.a.a;

public class s {

    /* renamed from: a  reason: collision with root package name */
    public static final s f207a = new s(0, null, 1443168256, 1);

    /* renamed from: b  reason: collision with root package name */
    public static final s f208b = new s(1, null, 1509950721, 1);
    public static final s c = new s(2, null, 1124075009, 1);
    public static final s d = new s(3, null, 1107297537, 1);
    public static final s e = new s(4, null, 1392510721, 1);
    public static final s f = new s(5, null, 1224736769, 1);
    public static final s g = new s(6, null, 1174536705, 1);
    public static final s h = new s(7, null, 1241579778, 1);
    public static final s i = new s(8, null, 1141048066, 1);
    private final int j;
    private final char[] k;
    private final int l;
    private final int m;

    private s(int i2, char[] cArr, int i3, int i4) {
        this.j = i2;
        this.k = cArr;
        this.l = i3;
        this.m = i4;
    }

    public static s a(Class cls) {
        return cls.isPrimitive() ? cls == Integer.TYPE ? f : cls == Void.TYPE ? f207a : cls == Boolean.TYPE ? f208b : cls == Byte.TYPE ? d : cls == Character.TYPE ? c : cls == Short.TYPE ? e : cls == Double.TYPE ? i : cls == Float.TYPE ? g : h : a(b(cls));
    }

    public static s a(String str) {
        return a(str.toCharArray(), 0);
    }

    private static s a(char[] cArr, int i2) {
        int i3 = 1;
        switch (cArr[i2]) {
            case 'B':
                return d;
            case 'C':
                return c;
            case 'D':
                return i;
            case 'E':
            case 'G':
            case 'H':
            case 'K':
            case 'M':
            case 'N':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'T':
            case 'U':
            case 'W':
            case 'X':
            case 'Y':
            default:
                return new s(11, cArr, 0, cArr.length);
            case 'F':
                return g;
            case 'I':
                return f;
            case 'J':
                return h;
            case 'L':
                while (cArr[i2 + i3] != ';') {
                    i3++;
                }
                return new s(10, cArr, i2 + 1, i3 - 1);
            case 'S':
                return e;
            case 'V':
                return f207a;
            case 'Z':
                return f208b;
            case '[':
                while (cArr[i2 + i3] == '[') {
                    i3++;
                }
                if (cArr[i2 + i3] == 'L') {
                    while (true) {
                        i3++;
                        if (cArr[i2 + i3] != ';') {
                        }
                    }
                }
                return new s(9, cArr, i2, i3 + 1);
        }
    }

    private void a(StringBuffer stringBuffer) {
        if (this.k == null) {
            stringBuffer.append((char) ((this.l & -16777216) >>> 24));
        } else if (this.j == 10) {
            stringBuffer.append('L');
            stringBuffer.append(this.k, this.l, this.m);
            stringBuffer.append(';');
        } else {
            stringBuffer.append(this.k, this.l, this.m);
        }
    }

    private static void a(StringBuffer stringBuffer, Class cls) {
        while (!cls.isPrimitive()) {
            if (cls.isArray()) {
                stringBuffer.append('[');
                cls = cls.getComponentType();
            } else {
                stringBuffer.append('L');
                String name = cls.getName();
                int length = name.length();
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt = name.charAt(i2);
                    if (charAt == '.') {
                        charAt = '/';
                    }
                    stringBuffer.append(charAt);
                }
                stringBuffer.append(';');
                return;
            }
        }
        stringBuffer.append(cls == Integer.TYPE ? 'I' : cls == Void.TYPE ? 'V' : cls == Boolean.TYPE ? 'Z' : cls == Byte.TYPE ? 'B' : cls == Character.TYPE ? 'C' : cls == Short.TYPE ? 'S' : cls == Double.TYPE ? 'D' : cls == Float.TYPE ? 'F' : 'J');
    }

    public static s b(String str) {
        char[] charArray = str.toCharArray();
        return new s(charArray[0] == '[' ? 9 : 10, charArray, 0, charArray.length);
    }

    public static String b(Class cls) {
        StringBuffer stringBuffer = new StringBuffer();
        a(stringBuffer, cls);
        return stringBuffer.toString();
    }

    public static s c(String str) {
        return a(str.toCharArray(), 0);
    }

    public static s[] d(String str) {
        int i2 = 1;
        char[] charArray = str.toCharArray();
        int i3 = 0;
        int i4 = 1;
        while (true) {
            int i5 = i4 + 1;
            char c2 = charArray[i4];
            if (c2 == ')') {
                break;
            } else if (c2 == 'L') {
                while (true) {
                    int i6 = i5;
                    i5 = i6 + 1;
                    if (charArray[i6] == ';') {
                        break;
                    }
                }
                i3++;
                i4 = i5;
            } else if (c2 != '[') {
                i3++;
                i4 = i5;
            } else {
                i4 = i5;
            }
        }
        s[] sVarArr = new s[i3];
        int i7 = 0;
        while (charArray[i2] != ')') {
            sVarArr[i7] = a(charArray, i2);
            i2 += (sVarArr[i7].j == 10 ? 2 : 0) + sVarArr[i7].m;
            i7++;
        }
        return sVarArr;
    }

    public static int e(String str) {
        int i2;
        char charAt;
        int i3 = 1;
        int i4 = 1;
        int i5 = 1;
        while (true) {
            i2 = i4 + 1;
            char charAt2 = str.charAt(i4);
            if (charAt2 == ')') {
                break;
            } else if (charAt2 == 'L') {
                while (true) {
                    int i6 = i2;
                    i2 = i6 + 1;
                    if (str.charAt(i6) == ';') {
                        break;
                    }
                }
                i5++;
                i4 = i2;
            } else if (charAt2 == '[') {
                while (true) {
                    charAt = str.charAt(i2);
                    if (charAt != '[') {
                        break;
                    }
                    i2++;
                }
                if (charAt == 'D' || charAt == 'J') {
                    i5--;
                    i4 = i2;
                } else {
                    i4 = i2;
                }
            } else if (charAt2 == 'D' || charAt2 == 'J') {
                i5 += 2;
                i4 = i2;
            } else {
                i5++;
                i4 = i2;
            }
        }
        char charAt3 = str.charAt(i2);
        int i7 = i5 << 2;
        if (charAt3 == 'V') {
            i3 = 0;
        } else if (charAt3 == 'D' || charAt3 == 'J') {
            i3 = 2;
        }
        return i7 | i3;
    }

    public int a() {
        return this.j;
    }

    public int b() {
        int i2 = 1;
        while (this.k[this.l + i2] == '[') {
            i2++;
        }
        return i2;
    }

    public s c() {
        return a(this.k, this.l + b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String d() {
        switch (this.j) {
            case 0:
                return "void";
            case 1:
                return "boolean";
            case 2:
                return "char";
            case 3:
                return "byte";
            case 4:
                return "short";
            case 5:
                return "int";
            case 6:
                return "float";
            case 7:
                return "long";
            case 8:
                return "double";
            case 9:
                StringBuffer stringBuffer = new StringBuffer(c().d());
                for (int b2 = b(); b2 > 0; b2--) {
                    stringBuffer.append("[]");
                }
                return stringBuffer.toString();
            case 10:
                return new String(this.k, this.l, this.m).replace('/', '.');
            default:
                return null;
        }
    }

    public String e() {
        return new String(this.k, this.l, this.m);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof s)) {
            return false;
        }
        s sVar = (s) obj;
        if (this.j != sVar.j) {
            return false;
        }
        if (this.j < 9) {
            return true;
        }
        if (this.m != sVar.m) {
            return false;
        }
        int i2 = this.l;
        int i3 = sVar.l;
        int i4 = this.m + i2;
        while (i2 < i4) {
            if (this.k[i2] != sVar.k[i3]) {
                return false;
            }
            i2++;
            i3++;
        }
        return true;
    }

    public String f() {
        StringBuffer stringBuffer = new StringBuffer();
        a(stringBuffer);
        return stringBuffer.toString();
    }

    public int hashCode() {
        int i2 = this.j * 13;
        if (this.j >= 9) {
            int i3 = this.l;
            int i4 = i3 + this.m;
            while (i3 < i4) {
                int i5 = (i2 + this.k[i3]) * 17;
                i3++;
                i2 = i5;
            }
        }
        return i2;
    }

    public String toString() {
        return f();
    }
}
