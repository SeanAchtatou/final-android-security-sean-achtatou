package b.a.a;

import com.esotericsoftware.kryo.util.DefaultClassResolver;

class q extends p {
    private int[] A;
    private int B;
    private int[] C;
    private int D;
    private m E;
    private m F;
    private int G;
    private d H;
    private int I;
    private d J;
    private int K;
    private d L;
    private c M;
    private boolean N;
    private int O;
    private final int P;
    private o Q;
    private o R;
    private o S;
    private int T;
    private int U;
    final g c;
    String d;
    int e;
    int f;
    int g;
    int[] h;
    private int i;
    private final int j;
    private final int k;
    private final String l;
    private d m;
    private b n;
    private b o;
    private b[] p;
    private b[] q;
    private int r;
    private c s;
    private d t = new d();
    private int u;
    private int v;
    private int w;
    private int x;
    private d y;
    private int z;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    q(g gVar, int i2, String str, String str2, String str3, String[] strArr, boolean z2, boolean z3) {
        super(262144);
        int i3 = 0;
        if (gVar.t == null) {
            gVar.t = this;
        } else {
            gVar.u.f204b = this;
        }
        gVar.u = this;
        this.c = gVar;
        this.i = i2;
        this.j = gVar.a(str);
        this.k = gVar.a(str2);
        this.l = str2;
        this.d = str3;
        if (strArr != null && strArr.length > 0) {
            this.g = strArr.length;
            this.h = new int[this.g];
            for (int i4 = 0; i4 < this.g; i4++) {
                this.h[i4] = gVar.c(strArr[i4]);
            }
        }
        this.P = !z3 ? z2 ? 1 : 2 : i3;
        if (z2 || z3) {
            if (z3 && "<init>".equals(str)) {
                this.i |= 262144;
            }
            int e2 = s.e(this.l) >> 2;
            e2 = (i2 & 8) != 0 ? e2 - 1 : e2;
            this.v = e2;
            this.w = e2;
            this.Q = new o();
            this.Q.f201a |= 8;
            a(this.Q);
        }
    }

    static int a(byte[] bArr, int i2) {
        return ((bArr[i2] & DefaultClassResolver.NAME) << 8) | (bArr[i2 + 1] & DefaultClassResolver.NAME);
    }

    static int a(int[] iArr, int[] iArr2, int i2, int i3) {
        int i4 = i3 - i2;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            if (i2 < iArr[i5] && iArr[i5] <= i3) {
                i4 += iArr2[i5];
            } else if (i3 < iArr[i5] && iArr[i5] <= i2) {
                i4 -= iArr2[i5];
            }
        }
        return i4;
    }

    private void a(int i2, int i3, int i4) {
        int i5 = i3 + 3 + i4;
        if (this.C == null || this.C.length < i5) {
            this.C = new int[i5];
        }
        this.C[0] = i2;
        this.C[1] = i3;
        this.C[2] = i4;
        this.B = 3;
    }

    private void a(k kVar) {
        int i2 = 0;
        int[] iArr = kVar.c;
        int[] iArr2 = kVar.d;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < iArr.length) {
            int i6 = iArr[i3];
            if (i6 == 16777216) {
                i5++;
            } else {
                i4 += i5 + 1;
                i5 = 0;
            }
            if (i6 == 16777220 || i6 == 16777219) {
                i3++;
            }
            i3++;
        }
        int i7 = 0;
        int i8 = 0;
        while (i7 < iArr2.length) {
            int i9 = iArr2[i7];
            i8++;
            if (i9 == 16777220 || i9 == 16777219) {
                i7++;
            }
            i7++;
        }
        a(kVar.f194b.c, i4, i8);
        int i10 = 0;
        while (i4 > 0) {
            int i11 = iArr[i10];
            int[] iArr3 = this.C;
            int i12 = this.B;
            this.B = i12 + 1;
            iArr3[i12] = i11;
            if (i11 == 16777220 || i11 == 16777219) {
                i10++;
            }
            i10++;
            i4--;
        }
        while (i2 < iArr2.length) {
            int i13 = iArr2[i2];
            int[] iArr4 = this.C;
            int i14 = this.B;
            this.B = i14 + 1;
            iArr4[i14] = i13;
            if (i13 == 16777220 || i13 == 16777219) {
                i2++;
            }
            i2++;
        }
        f();
    }

    private void a(o oVar, o[] oVarArr) {
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(171, 0, (g) null, (n) null);
                c(0, oVar);
                oVar.a().f201a |= 16;
                for (int i2 = 0; i2 < oVarArr.length; i2++) {
                    c(0, oVarArr[i2]);
                    oVarArr[i2].a().f201a |= 16;
                }
            } else {
                this.T--;
                c(this.T, oVar);
                for (o c2 : oVarArr) {
                    c(this.T, c2);
                }
            }
            e();
        }
    }

    static void a(byte[] bArr, int i2, int i3) {
        bArr[i2] = (byte) (i3 >>> 8);
        bArr[i2 + 1] = (byte) i3;
    }

    static void a(int[] iArr, int[] iArr2, o oVar) {
        if ((oVar.f201a & 4) == 0) {
            oVar.c = a(iArr, iArr2, 0, oVar.c);
            oVar.f201a |= 4;
        }
    }

    static short b(byte[] bArr, int i2) {
        return (short) (((bArr[i2] & DefaultClassResolver.NAME) << 8) | (bArr[i2 + 1] & DefaultClassResolver.NAME));
    }

    private void b(Object obj) {
        if (obj instanceof String) {
            this.y.a(7).b(this.c.c((String) obj));
        } else if (obj instanceof Integer) {
            this.y.a(((Integer) obj).intValue());
        } else {
            this.y.a(8).b(((o) obj).c);
        }
    }

    static int c(byte[] bArr, int i2) {
        return ((bArr[i2] & DefaultClassResolver.NAME) << 24) | ((bArr[i2 + 1] & DefaultClassResolver.NAME) << 16) | ((bArr[i2 + 2] & DefaultClassResolver.NAME) << 8) | (bArr[i2 + 3] & DefaultClassResolver.NAME);
    }

    private void c(int i2, o oVar) {
        h hVar = new h();
        hVar.f189a = i2;
        hVar.f190b = oVar;
        hVar.c = this.S.h;
        this.S.h = hVar;
    }

    private void e() {
        if (this.P == 0) {
            o oVar = new o();
            oVar.f = new k();
            oVar.f.f194b = oVar;
            oVar.a(this, this.t.f184b, this.t.f183a);
            this.R.g = oVar;
            this.R = oVar;
        } else {
            this.S.e = this.U;
        }
        this.S = null;
    }

    private void e(int i2, int i3) {
        while (i2 < i3) {
            int i4 = this.C[i2];
            int i5 = -268435456 & i4;
            if (i5 == 0) {
                int i6 = i4 & 1048575;
                switch (i4 & 267386880) {
                    case 24117248:
                        this.y.a(7).b(this.c.c(this.c.n[i6].e));
                        continue;
                    case 25165824:
                        this.y.a(8).b(this.c.n[i6].c);
                        continue;
                    default:
                        this.y.a(i6);
                        continue;
                }
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                int i7 = i5 >> 28;
                while (true) {
                    int i8 = i7 - 1;
                    if (i7 > 0) {
                        stringBuffer.append('[');
                        i7 = i8;
                    } else {
                        if ((i4 & 267386880) != 24117248) {
                            switch (i4 & 15) {
                                case 1:
                                    stringBuffer.append('I');
                                    break;
                                case 2:
                                    stringBuffer.append('F');
                                    break;
                                case 3:
                                    stringBuffer.append('D');
                                    break;
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                default:
                                    stringBuffer.append('J');
                                    break;
                                case 9:
                                    stringBuffer.append('Z');
                                    break;
                                case 10:
                                    stringBuffer.append('B');
                                    break;
                                case 11:
                                    stringBuffer.append('C');
                                    break;
                                case 12:
                                    stringBuffer.append('S');
                                    break;
                            }
                        } else {
                            stringBuffer.append('L');
                            stringBuffer.append(this.c.n[i4 & 1048575].e);
                            stringBuffer.append(';');
                        }
                        this.y.a(7).b(this.c.c(stringBuffer.toString()));
                    }
                }
            }
            i2++;
        }
    }

    private void f() {
        if (this.A != null) {
            if (this.y == null) {
                this.y = new d();
            }
            g();
            this.x++;
        }
        this.A = this.C;
        this.C = null;
    }

    private void g() {
        int i2;
        char c2;
        int i3;
        char c3 = '@';
        int i4 = 0;
        int i5 = this.C[1];
        int i6 = this.C[2];
        if ((this.c.e & 65535) < 50) {
            this.y.b(this.C[0]).b(i5);
            e(3, i5 + 3);
            this.y.b(i6);
            e(i5 + 3, i5 + 3 + i6);
            return;
        }
        int i7 = this.A[1];
        int i8 = this.x == 0 ? this.C[0] : (this.C[0] - this.A[0]) - 1;
        if (i6 == 0) {
            i2 = i5 - i7;
            switch (i2) {
                case -3:
                case -2:
                case -1:
                    c2 = 248;
                    i7 = i5;
                    break;
                case 0:
                    if (i8 >= 64) {
                        c2 = 251;
                        break;
                    } else {
                        c2 = 0;
                        break;
                    }
                case 1:
                case 2:
                case 3:
                    c2 = 252;
                    break;
                default:
                    c2 = 255;
                    break;
            }
            i3 = i7;
        } else if (i5 == i7 && i6 == 1) {
            if (i8 >= 63) {
                c3 = 247;
            }
            i2 = 0;
            i3 = i7;
        } else {
            i2 = 0;
            c2 = 255;
            i3 = i7;
        }
        if (c2 != 255) {
            int i9 = 3;
            while (true) {
                if (i4 < i3) {
                    if (this.C[i9] != this.A[i9]) {
                        c2 = 255;
                    } else {
                        i9++;
                        i4++;
                    }
                }
            }
        }
        switch (c2) {
            case 0:
                this.y.a(i8);
                return;
            case '@':
                this.y.a(i8 + 64);
                e(i5 + 3, i5 + 4);
                return;
            case 247:
                this.y.a(247).b(i8);
                e(i5 + 3, i5 + 4);
                return;
            case 248:
                this.y.a(i2 + 251).b(i8);
                return;
            case 251:
                this.y.a(251).b(i8);
                return;
            case 252:
                this.y.a(i2 + 251).b(i8);
                e(i3 + 3, i5 + 3);
                return;
            default:
                this.y.a(255).b(i8).b(i5);
                e(3, i5 + 3);
                this.y.b(i6);
                e(i5 + 3, i5 + 3 + i6);
                return;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v49 */
    /* JADX WARN: Type inference failed for: r0v52, types: [int] */
    /* JADX WARN: Type inference failed for: r0v53, types: [int] */
    /* JADX WARN: Type inference failed for: r0v54, types: [int] */
    /* JADX WARN: Type inference failed for: r0v55, types: [int] */
    /* JADX WARN: Type inference failed for: r0v56, types: [int] */
    /* JADX WARN: Type inference failed for: r0v57, types: [int] */
    /* JADX WARN: Type inference failed for: r1v85, types: [int] */
    /* JADX WARN: Type inference failed for: r1v88, types: [int] */
    /* JADX WARN: Type inference failed for: r0v58, types: [int] */
    /* JADX WARN: Type inference failed for: r0v59, types: [int] */
    /* JADX WARN: Type inference failed for: r4v82, types: [int] */
    /* JADX WARN: Type inference failed for: r4v85, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h() {
        /*
            r13 = this;
            b.a.a.d r0 = r13.t
            byte[] r7 = r0.f183a
            r0 = 0
            int[] r3 = new int[r0]
            r0 = 0
            int[] r2 = new int[r0]
            b.a.a.d r0 = r13.t
            int r0 = r0.f184b
            boolean[] r8 = new boolean[r0]
            r0 = 3
        L_0x0011:
            r1 = 3
            if (r0 != r1) goto L_0x0015
            r0 = 2
        L_0x0015:
            r1 = 0
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0019:
            int r4 = r7.length
            if (r0 >= r4) goto L_0x010c
            byte r4 = r7[r0]
            r5 = r4 & 255(0xff, float:3.57E-43)
            r6 = 0
            byte[] r4 = b.a.a.g.c
            byte r4 = r4[r5]
            switch(r4) {
                case 0: goto L_0x004e;
                case 1: goto L_0x0100;
                case 2: goto L_0x0104;
                case 3: goto L_0x0100;
                case 4: goto L_0x004e;
                case 5: goto L_0x0104;
                case 6: goto L_0x0104;
                case 7: goto L_0x0108;
                case 8: goto L_0x0108;
                case 9: goto L_0x0051;
                case 10: goto L_0x0092;
                case 11: goto L_0x0100;
                case 12: goto L_0x0104;
                case 13: goto L_0x0104;
                case 14: goto L_0x0095;
                case 15: goto L_0x00c6;
                case 16: goto L_0x0028;
                case 17: goto L_0x00ee;
                default: goto L_0x0028;
            }
        L_0x0028:
            int r0 = r0 + 4
        L_0x002a:
            if (r6 == 0) goto L_0x0019
            int r4 = r3.length
            int r4 = r4 + 1
            int[] r5 = new int[r4]
            int r4 = r2.length
            int r4 = r4 + 1
            int[] r4 = new int[r4]
            r9 = 0
            r10 = 0
            int r11 = r3.length
            java.lang.System.arraycopy(r3, r9, r5, r10, r11)
            r9 = 0
            r10 = 0
            int r11 = r2.length
            java.lang.System.arraycopy(r2, r9, r4, r10, r11)
            int r3 = r3.length
            r5[r3] = r0
            int r2 = r2.length
            r4[r2] = r6
            if (r6 <= 0) goto L_0x0379
            r1 = 3
            r2 = r4
            r3 = r5
            goto L_0x0019
        L_0x004e:
            int r0 = r0 + 1
            goto L_0x002a
        L_0x0051:
            r4 = 201(0xc9, float:2.82E-43)
            if (r5 <= r4) goto L_0x0088
            r4 = 218(0xda, float:3.05E-43)
            if (r5 >= r4) goto L_0x0085
            int r4 = r5 + -49
        L_0x005b:
            int r5 = r0 + 1
            int r5 = a(r7, r5)
            int r5 = r5 + r0
            r12 = r5
            r5 = r4
            r4 = r12
        L_0x0065:
            int r4 = a(r3, r2, r0, r4)
            r9 = -32768(0xffffffffffff8000, float:NaN)
            if (r4 < r9) goto L_0x0071
            r9 = 32767(0x7fff, float:4.5916E-41)
            if (r4 <= r9) goto L_0x037d
        L_0x0071:
            boolean r4 = r8[r0]
            if (r4 != 0) goto L_0x037d
            r4 = 167(0xa7, float:2.34E-43)
            if (r5 == r4) goto L_0x007d
            r4 = 168(0xa8, float:2.35E-43)
            if (r5 != r4) goto L_0x0090
        L_0x007d:
            r4 = 2
        L_0x007e:
            r5 = 1
            r8[r0] = r5
        L_0x0081:
            int r0 = r0 + 3
            r6 = r4
            goto L_0x002a
        L_0x0085:
            int r4 = r5 + -20
            goto L_0x005b
        L_0x0088:
            int r4 = r0 + 1
            short r4 = b(r7, r4)
            int r4 = r4 + r0
            goto L_0x0065
        L_0x0090:
            r4 = 5
            goto L_0x007e
        L_0x0092:
            int r0 = r0 + 5
            goto L_0x002a
        L_0x0095:
            r4 = 1
            if (r1 != r4) goto L_0x00bc
            r4 = 0
            int r4 = a(r3, r2, r4, r0)
            r4 = r4 & 3
            int r6 = -r4
        L_0x00a0:
            int r4 = r0 + 4
            r0 = r0 & 3
            int r0 = r4 - r0
            int r4 = r0 + 8
            int r4 = c(r7, r4)
            int r5 = r0 + 4
            int r5 = c(r7, r5)
            int r4 = r4 - r5
            int r4 = r4 + 1
            int r4 = r4 * 4
            int r4 = r4 + 12
            int r0 = r0 + r4
            goto L_0x002a
        L_0x00bc:
            boolean r4 = r8[r0]
            if (r4 != 0) goto L_0x00a0
            r6 = r0 & 3
            r4 = 1
            r8[r0] = r4
            goto L_0x00a0
        L_0x00c6:
            r4 = 1
            if (r1 != r4) goto L_0x00e4
            r4 = 0
            int r4 = a(r3, r2, r4, r0)
            r4 = r4 & 3
            int r6 = -r4
        L_0x00d1:
            int r4 = r0 + 4
            r0 = r0 & 3
            int r0 = r4 - r0
            int r4 = r0 + 4
            int r4 = c(r7, r4)
            int r4 = r4 * 8
            int r4 = r4 + 8
            int r0 = r0 + r4
            goto L_0x002a
        L_0x00e4:
            boolean r4 = r8[r0]
            if (r4 != 0) goto L_0x00d1
            r6 = r0 & 3
            r4 = 1
            r8[r0] = r4
            goto L_0x00d1
        L_0x00ee:
            int r4 = r0 + 1
            byte r4 = r7[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 132(0x84, float:1.85E-43)
            if (r4 != r5) goto L_0x00fc
            int r0 = r0 + 6
            goto L_0x002a
        L_0x00fc:
            int r0 = r0 + 4
            goto L_0x002a
        L_0x0100:
            int r0 = r0 + 2
            goto L_0x002a
        L_0x0104:
            int r0 = r0 + 3
            goto L_0x002a
        L_0x0108:
            int r0 = r0 + 5
            goto L_0x002a
        L_0x010c:
            r0 = 3
            if (r1 >= r0) goto L_0x0111
            int r1 = r1 + -1
        L_0x0111:
            if (r1 != 0) goto L_0x0376
            b.a.a.d r6 = new b.a.a.d
            b.a.a.d r0 = r13.t
            int r0 = r0.f184b
            r6.<init>(r0)
            r0 = 0
        L_0x011d:
            b.a.a.d r1 = r13.t
            int r1 = r1.f184b
            if (r0 >= r1) goto L_0x0291
            byte r1 = r7[r0]
            r4 = r1 & 255(0xff, float:3.57E-43)
            byte[] r1 = b.a.a.g.c
            byte r1 = r1[r4]
            switch(r1) {
                case 0: goto L_0x0135;
                case 1: goto L_0x0279;
                case 2: goto L_0x0281;
                case 3: goto L_0x0279;
                case 4: goto L_0x0135;
                case 5: goto L_0x0281;
                case 6: goto L_0x0281;
                case 7: goto L_0x0289;
                case 8: goto L_0x0289;
                case 9: goto L_0x013b;
                case 10: goto L_0x01a1;
                case 11: goto L_0x0279;
                case 12: goto L_0x0281;
                case 13: goto L_0x0281;
                case 14: goto L_0x01b6;
                case 15: goto L_0x020f;
                case 16: goto L_0x012e;
                case 17: goto L_0x025f;
                default: goto L_0x012e;
            }
        L_0x012e:
            r1 = 4
            r6.a(r7, r0, r1)
            int r0 = r0 + 4
            goto L_0x011d
        L_0x0135:
            r6.a(r4)
            int r0 = r0 + 1
            goto L_0x011d
        L_0x013b:
            r1 = 201(0xc9, float:2.82E-43)
            if (r4 <= r1) goto L_0x016a
            r1 = 218(0xda, float:3.05E-43)
            if (r4 >= r1) goto L_0x0167
            int r1 = r4 + -49
        L_0x0145:
            int r4 = r0 + 1
            int r4 = a(r7, r4)
            int r4 = r4 + r0
            r12 = r4
            r4 = r1
            r1 = r12
        L_0x014f:
            int r5 = a(r3, r2, r0, r1)
            boolean r1 = r8[r0]
            if (r1 == 0) goto L_0x019a
            r1 = 167(0xa7, float:2.34E-43)
            if (r4 != r1) goto L_0x0172
            r1 = 200(0xc8, float:2.8E-43)
            r6.a(r1)
            r1 = r5
        L_0x0161:
            r6.c(r1)
        L_0x0164:
            int r0 = r0 + 3
            goto L_0x011d
        L_0x0167:
            int r1 = r4 + -20
            goto L_0x0145
        L_0x016a:
            int r1 = r0 + 1
            short r1 = b(r7, r1)
            int r1 = r1 + r0
            goto L_0x014f
        L_0x0172:
            r1 = 168(0xa8, float:2.35E-43)
            if (r4 != r1) goto L_0x017d
            r1 = 201(0xc9, float:2.82E-43)
            r6.a(r1)
            r1 = r5
            goto L_0x0161
        L_0x017d:
            r1 = 166(0xa6, float:2.33E-43)
            if (r4 > r1) goto L_0x0197
            int r1 = r4 + 1
            r1 = r1 ^ 1
            int r1 = r1 + -1
        L_0x0187:
            r6.a(r1)
            r1 = 8
            r6.b(r1)
            r1 = 200(0xc8, float:2.8E-43)
            r6.a(r1)
            int r1 = r5 + -3
            goto L_0x0161
        L_0x0197:
            r1 = r4 ^ 1
            goto L_0x0187
        L_0x019a:
            r6.a(r4)
            r6.b(r5)
            goto L_0x0164
        L_0x01a1:
            int r1 = r0 + 1
            int r1 = c(r7, r1)
            int r1 = r1 + r0
            int r1 = a(r3, r2, r0, r1)
            r6.a(r4)
            r6.c(r1)
            int r0 = r0 + 5
            goto L_0x011d
        L_0x01b6:
            int r1 = r0 + 4
            r4 = r0 & 3
            int r1 = r1 - r4
            r4 = 170(0xaa, float:2.38E-43)
            r6.a(r4)
            r4 = 0
            r5 = 0
            int r9 = r6.f184b
            int r9 = r9 % 4
            int r9 = 4 - r9
            int r9 = r9 % 4
            r6.a(r4, r5, r9)
            int r4 = c(r7, r1)
            int r4 = r4 + r0
            int r1 = r1 + 4
            int r4 = a(r3, r2, r0, r4)
            r6.c(r4)
            int r4 = c(r7, r1)
            int r5 = r1 + 4
            r6.c(r4)
            int r1 = c(r7, r5)
            int r1 = r1 - r4
            int r1 = r1 + 1
            int r4 = r5 + 4
            int r5 = r4 + -4
            int r5 = c(r7, r5)
            r6.c(r5)
            r12 = r1
            r1 = r4
            r4 = r12
        L_0x01f9:
            if (r4 <= 0) goto L_0x0373
            int r5 = c(r7, r1)
            int r9 = r0 + r5
            int r5 = r1 + 4
            int r1 = a(r3, r2, r0, r9)
            r6.c(r1)
            int r1 = r4 + -1
            r4 = r1
            r1 = r5
            goto L_0x01f9
        L_0x020f:
            int r1 = r0 + 4
            r4 = r0 & 3
            int r1 = r1 - r4
            r4 = 171(0xab, float:2.4E-43)
            r6.a(r4)
            r4 = 0
            r5 = 0
            int r9 = r6.f184b
            int r9 = r9 % 4
            int r9 = 4 - r9
            int r9 = r9 % 4
            r6.a(r4, r5, r9)
            int r4 = c(r7, r1)
            int r4 = r4 + r0
            int r5 = r1 + 4
            int r1 = a(r3, r2, r0, r4)
            r6.c(r1)
            int r1 = c(r7, r5)
            int r4 = r5 + 4
            r6.c(r1)
            r12 = r1
            r1 = r4
            r4 = r12
        L_0x0240:
            if (r4 <= 0) goto L_0x0373
            int r5 = c(r7, r1)
            r6.c(r5)
            int r1 = r1 + 4
            int r5 = c(r7, r1)
            int r9 = r0 + r5
            int r5 = r1 + 4
            int r1 = a(r3, r2, r0, r9)
            r6.c(r1)
            int r1 = r4 + -1
            r4 = r1
            r1 = r5
            goto L_0x0240
        L_0x025f:
            int r1 = r0 + 1
            byte r1 = r7[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r4 = 132(0x84, float:1.85E-43)
            if (r1 != r4) goto L_0x0271
            r1 = 6
            r6.a(r7, r0, r1)
            int r0 = r0 + 6
            goto L_0x011d
        L_0x0271:
            r1 = 4
            r6.a(r7, r0, r1)
            int r0 = r0 + 4
            goto L_0x011d
        L_0x0279:
            r1 = 2
            r6.a(r7, r0, r1)
            int r0 = r0 + 2
            goto L_0x011d
        L_0x0281:
            r1 = 3
            r6.a(r7, r0, r1)
            int r0 = r0 + 3
            goto L_0x011d
        L_0x0289:
            r1 = 5
            r6.a(r7, r0, r1)
            int r0 = r0 + 5
            goto L_0x011d
        L_0x0291:
            int r0 = r13.x
            if (r0 <= 0) goto L_0x02e4
            int r0 = r13.P
            if (r0 != 0) goto L_0x02df
            r0 = 0
            r13.x = r0
            r0 = 0
            r13.y = r0
            r0 = 0
            r13.A = r0
            r0 = 0
            r13.C = r0
            b.a.a.k r0 = new b.a.a.k
            r0.<init>()
            b.a.a.o r1 = r13.Q
            r0.f194b = r1
            java.lang.String r1 = r13.l
            b.a.a.s[] r1 = b.a.a.s.d(r1)
            b.a.a.g r4 = r13.c
            int r5 = r13.i
            int r7 = r13.v
            r0.a(r4, r5, r1, r7)
            r13.a(r0)
            b.a.a.o r0 = r13.Q
        L_0x02c2:
            if (r0 == 0) goto L_0x02e4
            int r1 = r0.c
            int r1 = r1 + -3
            int r4 = r0.f201a
            r4 = r4 & 32
            if (r4 != 0) goto L_0x02d4
            if (r1 < 0) goto L_0x02dc
            boolean r1 = r8[r1]
            if (r1 == 0) goto L_0x02dc
        L_0x02d4:
            a(r3, r2, r0)
            b.a.a.k r1 = r0.f
            r13.a(r1)
        L_0x02dc:
            b.a.a.o r0 = r0.g
            goto L_0x02c2
        L_0x02df:
            b.a.a.g r0 = r13.c
            r1 = 1
            r0.v = r1
        L_0x02e4:
            b.a.a.m r0 = r13.E
        L_0x02e6:
            if (r0 == 0) goto L_0x02fa
            b.a.a.o r1 = r0.f197a
            a(r3, r2, r1)
            b.a.a.o r1 = r0.f198b
            a(r3, r2, r1)
            b.a.a.o r1 = r0.c
            a(r3, r2, r1)
            b.a.a.m r0 = r0.f
            goto L_0x02e6
        L_0x02fa:
            r0 = 0
            r4 = r0
        L_0x02fc:
            r0 = 2
            if (r4 >= r0) goto L_0x0336
            if (r4 != 0) goto L_0x032e
            b.a.a.d r0 = r13.H
            r1 = r0
        L_0x0304:
            if (r1 == 0) goto L_0x0332
            byte[] r5 = r1.f183a
            r0 = 0
        L_0x0309:
            int r7 = r1.f184b
            if (r0 >= r7) goto L_0x0332
            int r7 = a(r5, r0)
            r8 = 0
            int r8 = a(r3, r2, r8, r7)
            a(r5, r0, r8)
            int r9 = r0 + 2
            int r9 = a(r5, r9)
            int r7 = r7 + r9
            r9 = 0
            int r7 = a(r3, r2, r9, r7)
            int r7 = r7 - r8
            int r8 = r0 + 2
            a(r5, r8, r7)
            int r0 = r0 + 10
            goto L_0x0309
        L_0x032e:
            b.a.a.d r0 = r13.J
            r1 = r0
            goto L_0x0304
        L_0x0332:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x02fc
        L_0x0336:
            b.a.a.d r0 = r13.L
            if (r0 == 0) goto L_0x0354
            b.a.a.d r0 = r13.L
            byte[] r1 = r0.f183a
            r0 = 0
        L_0x033f:
            b.a.a.d r4 = r13.L
            int r4 = r4.f184b
            if (r0 >= r4) goto L_0x0354
            r4 = 0
            int r5 = a(r1, r0)
            int r4 = a(r3, r2, r4, r5)
            a(r1, r0, r4)
            int r0 = r0 + 4
            goto L_0x033f
        L_0x0354:
            b.a.a.c r0 = r13.M
            r1 = r0
        L_0x0357:
            if (r1 == 0) goto L_0x0370
            b.a.a.o[] r4 = r1.b()
            if (r4 == 0) goto L_0x036c
            int r0 = r4.length
            int r0 = r0 + -1
        L_0x0362:
            if (r0 < 0) goto L_0x036c
            r5 = r4[r0]
            a(r3, r2, r5)
            int r0 = r0 + -1
            goto L_0x0362
        L_0x036c:
            b.a.a.c r0 = r1.c
            r1 = r0
            goto L_0x0357
        L_0x0370:
            r13.t = r6
            return
        L_0x0373:
            r0 = r1
            goto L_0x011d
        L_0x0376:
            r0 = r1
            goto L_0x0011
        L_0x0379:
            r2 = r4
            r3 = r5
            goto L_0x0019
        L_0x037d:
            r4 = r6
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.a.q.h():void");
    }

    public a a() {
        this.m = new d();
        return new b(this.c, false, this.m, null, 0);
    }

    public a a(int i2, String str, boolean z2) {
        d dVar = new d();
        if ("Ljava/lang/Synthetic;".equals(str)) {
            this.r = Math.max(this.r, i2 + 1);
            return new b(this.c, false, dVar, null, 0);
        }
        dVar.b(this.c.a(str)).b(0);
        b bVar = new b(this.c, true, dVar, dVar, 2);
        if (z2) {
            if (this.p == null) {
                this.p = new b[s.d(this.l).length];
            }
            bVar.c = this.p[i2];
            this.p[i2] = bVar;
            return bVar;
        }
        if (this.q == null) {
            this.q = new b[s.d(this.l).length];
        }
        bVar.c = this.q[i2];
        this.q[i2] = bVar;
        return bVar;
    }

    public a a(String str, boolean z2) {
        d dVar = new d();
        dVar.b(this.c.a(str)).b(0);
        b bVar = new b(this.c, true, dVar, dVar, 2);
        if (z2) {
            bVar.c = this.n;
            this.n = bVar;
        } else {
            bVar.c = this.o;
            this.o = bVar;
        }
        return bVar;
    }

    public void a(int i2) {
        this.t.a(i2);
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, 0, (g) null, (n) null);
            } else {
                int i3 = this.T + k.f193a[i2];
                if (i3 > this.U) {
                    this.U = i3;
                }
                this.T = i3;
            }
            if ((i2 >= 172 && i2 <= 177) || i2 == 191) {
                e();
            }
        }
    }

    public void a(int i2, int i3) {
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, i3, (g) null, (n) null);
            } else if (i2 != 188) {
                int i4 = this.T + 1;
                if (i4 > this.U) {
                    this.U = i4;
                }
                this.T = i4;
            }
        }
        if (i2 == 17) {
            this.t.b(i2, i3);
        } else {
            this.t.a(i2, i3);
        }
    }

    public void a(int i2, int i3, o oVar, o... oVarArr) {
        int i4 = this.t.f184b;
        this.t.a(170);
        this.t.a(null, 0, (4 - (this.t.f184b % 4)) % 4);
        oVar.a(this, this.t, i4, true);
        this.t.c(i2).c(i3);
        for (o a2 : oVarArr) {
            a2.a(this, this.t, i4, true);
        }
        a(oVar, oVarArr);
    }

    public void a(int i2, int i3, Object[] objArr, int i4, Object[] objArr2) {
        int i5;
        int i6 = 0;
        if (this.P != 0) {
            if (i2 == -1) {
                this.w = i3;
                a(this.t.f184b, i3, i4);
                for (int i7 = 0; i7 < i3; i7++) {
                    if (objArr[i7] instanceof String) {
                        int[] iArr = this.C;
                        int i8 = this.B;
                        this.B = i8 + 1;
                        iArr[i8] = this.c.e((String) objArr[i7]) | 24117248;
                    } else if (objArr[i7] instanceof Integer) {
                        int[] iArr2 = this.C;
                        int i9 = this.B;
                        this.B = i9 + 1;
                        iArr2[i9] = ((Integer) objArr[i7]).intValue();
                    } else {
                        int[] iArr3 = this.C;
                        int i10 = this.B;
                        this.B = i10 + 1;
                        iArr3[i10] = this.c.a("", ((o) objArr[i7]).c) | 25165824;
                    }
                }
                while (i6 < i4) {
                    if (objArr2[i6] instanceof String) {
                        int[] iArr4 = this.C;
                        int i11 = this.B;
                        this.B = i11 + 1;
                        iArr4[i11] = this.c.e((String) objArr2[i6]) | 24117248;
                    } else if (objArr2[i6] instanceof Integer) {
                        int[] iArr5 = this.C;
                        int i12 = this.B;
                        this.B = i12 + 1;
                        iArr5[i12] = ((Integer) objArr2[i6]).intValue();
                    } else {
                        int[] iArr6 = this.C;
                        int i13 = this.B;
                        this.B = i13 + 1;
                        iArr6[i13] = this.c.a("", ((o) objArr2[i6]).c) | 25165824;
                    }
                    i6++;
                }
                f();
            } else {
                if (this.y == null) {
                    this.y = new d();
                    i5 = this.t.f184b;
                } else {
                    i5 = (this.t.f184b - this.z) - 1;
                    if (i5 < 0) {
                        if (i2 != 3) {
                            throw new IllegalStateException();
                        }
                        return;
                    }
                }
                switch (i2) {
                    case 0:
                        this.w = i3;
                        this.y.a(255).b(i5).b(i3);
                        for (int i14 = 0; i14 < i3; i14++) {
                            b(objArr[i14]);
                        }
                        this.y.b(i4);
                        while (i6 < i4) {
                            b(objArr2[i6]);
                            i6++;
                        }
                        break;
                    case 1:
                        this.w += i3;
                        this.y.a(i3 + 251).b(i5);
                        for (int i15 = 0; i15 < i3; i15++) {
                            b(objArr[i15]);
                        }
                        break;
                    case 2:
                        this.w -= i3;
                        this.y.a(251 - i3).b(i5);
                        break;
                    case 3:
                        if (i5 >= 64) {
                            this.y.a(251).b(i5);
                            break;
                        } else {
                            this.y.a(i5);
                            break;
                        }
                    case 4:
                        if (i5 < 64) {
                            this.y.a(i5 + 64);
                        } else {
                            this.y.a(247).b(i5);
                        }
                        b(objArr2[0]);
                        break;
                }
                this.z = this.t.f184b;
                this.x++;
            }
            this.u = Math.max(this.u, i4);
            this.v = Math.max(this.v, this.w);
        }
    }

    public void a(int i2, o oVar) {
        o oVar2 = null;
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, 0, (g) null, (n) null);
                oVar.a().f201a |= 16;
                c(0, oVar);
                if (i2 != 167) {
                    oVar2 = new o();
                }
            } else if (i2 == 168) {
                if ((oVar.f201a & 512) == 0) {
                    oVar.f201a |= 512;
                    this.O++;
                }
                this.S.f201a |= 128;
                c(this.T + 1, oVar);
                oVar2 = new o();
            } else {
                this.T += k.f193a[i2];
                c(this.T, oVar);
            }
        }
        if ((oVar.f201a & 2) == 0 || oVar.c - this.t.f184b >= -32768) {
            this.t.a(i2);
            oVar.a(this, this.t, this.t.f184b - 1, false);
        } else {
            if (i2 == 167) {
                this.t.a(200);
            } else if (i2 == 168) {
                this.t.a(201);
            } else {
                if (oVar2 != null) {
                    oVar2.f201a |= 16;
                }
                this.t.a(i2 <= 166 ? ((i2 + 1) ^ 1) - 1 : i2 ^ 1);
                this.t.b(8);
                this.t.a(200);
            }
            oVar.a(this, this.t, this.t.f184b - 1, true);
        }
        if (this.S != null) {
            if (oVar2 != null) {
                a(oVar2);
            }
            if (i2 == 167) {
                e();
            }
        }
    }

    public void a(int i2, String str) {
        n b2 = this.c.b(str);
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, this.t.f184b, this.c, b2);
            } else if (i2 == 187) {
                int i3 = this.T + 1;
                if (i3 > this.U) {
                    this.U = i3;
                }
                this.T = i3;
            }
        }
        this.t.b(i2, b2.f199a);
    }

    public void a(int i2, String str, String str2, String str3) {
        int i3;
        int i4 = 1;
        int i5 = -2;
        n b2 = this.c.b(str, str2, str3);
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, 0, this.c, b2);
            } else {
                char charAt = str3.charAt(0);
                switch (i2) {
                    case 178:
                        int i6 = this.T;
                        if (charAt == 'D' || charAt == 'J') {
                            i4 = 2;
                        }
                        i3 = i4 + i6;
                        break;
                    case 179:
                        i3 = ((charAt == 'D' || charAt == 'J') ? -2 : -1) + this.T;
                        break;
                    case 180:
                        int i7 = this.T;
                        if (!(charAt == 'D' || charAt == 'J')) {
                            i4 = 0;
                        }
                        i3 = i4 + i7;
                        break;
                    default:
                        int i8 = this.T;
                        if (charAt == 'D' || charAt == 'J') {
                            i5 = -3;
                        }
                        i3 = i8 + i5;
                        break;
                }
                if (i3 > this.U) {
                    this.U = i3;
                }
                this.T = i3;
            }
        }
        this.t.b(i2, b2.f199a);
    }

    public void a(c cVar) {
        if (cVar.a()) {
            cVar.c = this.M;
            this.M = cVar;
            return;
        }
        cVar.c = this.s;
        this.s = cVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar) {
        boolean z2 = true;
        dVar.b(((393216 | ((this.i & 262144) / 64)) ^ -1) & this.i).b(this.j).b(this.k);
        if (this.e != 0) {
            dVar.a(this.c.d.f185a, this.e, this.f);
            return;
        }
        int i2 = this.t.f184b > 0 ? 1 : 0;
        if (this.g > 0) {
            i2++;
        }
        if ((this.i & 4096) != 0 && ((this.c.e & 65535) < 49 || (this.i & 262144) != 0)) {
            i2++;
        }
        if ((this.i & 131072) != 0) {
            i2++;
        }
        if (this.d != null) {
            i2++;
        }
        if (this.m != null) {
            i2++;
        }
        if (this.n != null) {
            i2++;
        }
        if (this.o != null) {
            i2++;
        }
        if (this.p != null) {
            i2++;
        }
        if (this.q != null) {
            i2++;
        }
        if (this.s != null) {
            i2 += this.s.c();
        }
        dVar.b(i2);
        if (this.t.f184b > 0) {
            int i3 = this.t.f184b + 12 + (this.D * 8);
            if (this.H != null) {
                i3 += this.H.f184b + 8;
            }
            if (this.J != null) {
                i3 += this.J.f184b + 8;
            }
            if (this.L != null) {
                i3 += this.L.f184b + 8;
            }
            int i4 = this.y != null ? i3 + this.y.f184b + 8 : i3;
            if (this.M != null) {
                i4 += this.M.b(this.c, this.t.f183a, this.t.f184b, this.u, this.v);
            }
            dVar.b(this.c.a("Code")).c(i4);
            dVar.b(this.u).b(this.v);
            dVar.c(this.t.f184b).a(this.t.f183a, 0, this.t.f184b);
            dVar.b(this.D);
            if (this.D > 0) {
                for (m mVar = this.E; mVar != null; mVar = mVar.f) {
                    dVar.b(mVar.f197a.c).b(mVar.f198b.c).b(mVar.c.c).b(mVar.e);
                }
            }
            int i5 = this.H != null ? 1 : 0;
            if (this.J != null) {
                i5++;
            }
            if (this.L != null) {
                i5++;
            }
            if (this.y != null) {
                i5++;
            }
            if (this.M != null) {
                i5 += this.M.c();
            }
            dVar.b(i5);
            if (this.H != null) {
                dVar.b(this.c.a("LocalVariableTable"));
                dVar.c(this.H.f184b + 2).b(this.G);
                dVar.a(this.H.f183a, 0, this.H.f184b);
            }
            if (this.J != null) {
                dVar.b(this.c.a("LocalVariableTypeTable"));
                dVar.c(this.J.f184b + 2).b(this.I);
                dVar.a(this.J.f183a, 0, this.J.f184b);
            }
            if (this.L != null) {
                dVar.b(this.c.a("LineNumberTable"));
                dVar.c(this.L.f184b + 2).b(this.K);
                dVar.a(this.L.f183a, 0, this.L.f184b);
            }
            if (this.y != null) {
                if ((this.c.e & 65535) < 50) {
                    z2 = false;
                }
                dVar.b(this.c.a(z2 ? "StackMapTable" : "StackMap"));
                dVar.c(this.y.f184b + 2).b(this.x);
                dVar.a(this.y.f183a, 0, this.y.f184b);
            }
            if (this.M != null) {
                this.M.a(this.c, this.t.f183a, this.t.f184b, this.v, this.u, dVar);
            }
        }
        if (this.g > 0) {
            dVar.b(this.c.a("Exceptions")).c((this.g * 2) + 2);
            dVar.b(this.g);
            for (int i6 = 0; i6 < this.g; i6++) {
                dVar.b(this.h[i6]);
            }
        }
        if ((this.i & 4096) != 0 && ((this.c.e & 65535) < 49 || (this.i & 262144) != 0)) {
            dVar.b(this.c.a("Synthetic")).c(0);
        }
        if ((this.i & 131072) != 0) {
            dVar.b(this.c.a("Deprecated")).c(0);
        }
        if (this.d != null) {
            dVar.b(this.c.a("Signature")).c(2).b(this.c.a(this.d));
        }
        if (this.m != null) {
            dVar.b(this.c.a("AnnotationDefault"));
            dVar.c(this.m.f184b);
            dVar.a(this.m.f183a, 0, this.m.f184b);
        }
        if (this.n != null) {
            dVar.b(this.c.a("RuntimeVisibleAnnotations"));
            this.n.a(dVar);
        }
        if (this.o != null) {
            dVar.b(this.c.a("RuntimeInvisibleAnnotations"));
            this.o.a(dVar);
        }
        if (this.p != null) {
            dVar.b(this.c.a("RuntimeVisibleParameterAnnotations"));
            b.a(this.p, this.r, dVar);
        }
        if (this.q != null) {
            dVar.b(this.c.a("RuntimeInvisibleParameterAnnotations"));
            b.a(this.q, this.r, dVar);
        }
        if (this.s != null) {
            this.s.a(this.c, (byte[]) null, 0, -1, -1, dVar);
        }
    }

    public void a(o oVar) {
        this.N |= oVar.a(this, this.t.f184b, this.t.f183a);
        if ((oVar.f201a & 1) == 0) {
            if (this.P == 0) {
                if (this.S != null) {
                    if (oVar.c == this.S.c) {
                        this.S.f201a |= oVar.f201a & 16;
                        oVar.f = this.S.f;
                        return;
                    }
                    c(0, oVar);
                }
                this.S = oVar;
                if (oVar.f == null) {
                    oVar.f = new k();
                    oVar.f.f194b = oVar;
                }
                if (this.R != null) {
                    if (oVar.c == this.R.c) {
                        this.R.f201a |= oVar.f201a & 16;
                        oVar.f = this.R.f;
                        this.S = this.R;
                        return;
                    }
                    this.R.g = oVar;
                }
                this.R = oVar;
            } else if (this.P == 1) {
                if (this.S != null) {
                    this.S.e = this.U;
                    c(this.T, oVar);
                }
                this.S = oVar;
                this.T = 0;
                this.U = 0;
                if (this.R != null) {
                    this.R.g = oVar;
                }
                this.R = oVar;
            }
        }
    }

    public void a(o oVar, o oVar2, o oVar3, String str) {
        this.D++;
        m mVar = new m();
        mVar.f197a = oVar;
        mVar.f198b = oVar2;
        mVar.c = oVar3;
        mVar.d = str;
        mVar.e = str != null ? this.c.c(str) : 0;
        if (this.F == null) {
            this.E = mVar;
        } else {
            this.F.f = mVar;
        }
        this.F = mVar;
    }

    public void a(o oVar, int[] iArr, o[] oVarArr) {
        int i2 = this.t.f184b;
        this.t.a(171);
        this.t.a(null, 0, (4 - (this.t.f184b % 4)) % 4);
        oVar.a(this, this.t, i2, true);
        this.t.c(oVarArr.length);
        for (int i3 = 0; i3 < oVarArr.length; i3++) {
            this.t.c(iArr[i3]);
            oVarArr[i3].a(this, this.t, i2, true);
        }
        a(oVar, oVarArr);
    }

    public void a(Object obj) {
        n a2 = this.c.a(obj);
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(18, 0, this.c, a2);
            } else {
                int i2 = (a2.f200b == 5 || a2.f200b == 6) ? this.T + 2 : this.T + 1;
                if (i2 > this.U) {
                    this.U = i2;
                }
                this.T = i2;
            }
        }
        int i3 = a2.f199a;
        if (a2.f200b == 5 || a2.f200b == 6) {
            this.t.b(20, i3);
        } else if (i3 >= 256) {
            this.t.b(19, i3);
        } else {
            this.t.a(18, i3);
        }
    }

    public void a(String str, int i2) {
        n b2 = this.c.b(str);
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(197, i2, this.c, b2);
            } else {
                this.T += 1 - i2;
            }
        }
        this.t.b(197, b2.f199a).a(i2);
    }

    public void a(String str, String str2, l lVar, Object... objArr) {
        n a2 = this.c.a(str, str2, lVar, objArr);
        int i2 = a2.c;
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(186, 0, this.c, a2);
            } else {
                if (i2 == 0) {
                    i2 = s.e(str2);
                    a2.c = i2;
                }
                int i3 = (i2 & 3) + (this.T - (i2 >> 2)) + 1;
                if (i3 > this.U) {
                    this.U = i3;
                }
                this.T = i3;
            }
        }
        this.t.b(186, a2.f199a);
        this.t.b(0);
    }

    public void a(String str, String str2, String str3, o oVar, o oVar2, int i2) {
        int i3 = 2;
        if (str3 != null) {
            if (this.J == null) {
                this.J = new d();
            }
            this.I++;
            this.J.b(oVar.c).b(oVar2.c - oVar.c).b(this.c.a(str)).b(this.c.a(str3)).b(i2);
        }
        if (this.H == null) {
            this.H = new d();
        }
        this.G++;
        this.H.b(oVar.c).b(oVar2.c - oVar.c).b(this.c.a(str)).b(this.c.a(str2)).b(i2);
        if (this.P != 2) {
            char charAt = str2.charAt(0);
            if (!(charAt == 'J' || charAt == 'D')) {
                i3 = 1;
            }
            int i4 = i3 + i2;
            if (i4 > this.v) {
                this.v = i4;
            }
        }
    }

    public void b() {
    }

    public void b(int i2, int i3) {
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, i3, (g) null, (n) null);
            } else if (i2 == 169) {
                this.S.f201a |= 256;
                this.S.d = this.T;
                e();
            } else {
                int i4 = this.T + k.f193a[i2];
                if (i4 > this.U) {
                    this.U = i4;
                }
                this.T = i4;
            }
        }
        if (this.P != 2) {
            int i5 = (i2 == 22 || i2 == 24 || i2 == 55 || i2 == 57) ? i3 + 2 : i3 + 1;
            if (i5 > this.v) {
                this.v = i5;
            }
        }
        if (i3 < 4 && i2 != 169) {
            this.t.a(i2 < 54 ? ((i2 - 21) << 2) + 26 + i3 : ((i2 - 54) << 2) + 59 + i3);
        } else if (i3 >= 256) {
            this.t.a(196).b(i2, i3);
        } else {
            this.t.a(i2, i3);
        }
        if (i2 >= 54 && this.P == 0 && this.D > 0) {
            a(new o());
        }
    }

    public void b(int i2, o oVar) {
        if (this.L == null) {
            this.L = new d();
        }
        this.K++;
        this.L.b(oVar.c);
        this.L.b(i2);
    }

    public void b(int i2, String str, String str2, String str3) {
        int i3;
        int i4;
        boolean z2 = i2 == 185;
        n a2 = this.c.a(str, str2, str3, z2);
        int i5 = a2.c;
        if (this.S != null) {
            if (this.P == 0) {
                this.S.f.a(i2, 0, this.c, a2);
            } else {
                if (i5 == 0) {
                    i4 = s.e(str3);
                    a2.c = i4;
                } else {
                    i4 = i5;
                }
                int i6 = i2 == 184 ? (this.T - (i4 >> 2)) + (i4 & 3) + 1 : (this.T - (i4 >> 2)) + (i4 & 3);
                if (i6 > this.U) {
                    this.U = i6;
                }
                this.T = i6;
                i5 = i4;
            }
        }
        if (z2) {
            if (i5 == 0) {
                i3 = s.e(str3);
                a2.c = i3;
            } else {
                i3 = i5;
            }
            this.t.b(185, a2.f199a).a(i3 >> 2, 0);
            return;
        }
        this.t.b(i2, a2.f199a);
    }

    public void c() {
    }

    public void c(int i2, int i3) {
        int i4;
        if (this.S != null && this.P == 0) {
            this.S.f.a(132, i2, (g) null, (n) null);
        }
        if (this.P != 2 && (i4 = i2 + 1) > this.v) {
            this.v = i4;
        }
        if (i2 > 255 || i3 > 127 || i3 < -128) {
            this.t.a(196).b(132, i2).b(i3);
        } else {
            this.t.a(132).a(i2, i3);
        }
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        int i2;
        if (this.e != 0) {
            return this.f + 6;
        }
        if (this.N) {
            h();
        }
        int i3 = 8;
        if (this.t.f184b > 0) {
            if (this.t.f184b > 65536) {
                throw new RuntimeException("Method code too large!");
            }
            this.c.a("Code");
            int i4 = this.t.f184b + 18 + (this.D * 8) + 8;
            if (this.H != null) {
                this.c.a("LocalVariableTable");
                i4 += this.H.f184b + 8;
            }
            if (this.J != null) {
                this.c.a("LocalVariableTypeTable");
                i4 += this.J.f184b + 8;
            }
            if (this.L != null) {
                this.c.a("LineNumberTable");
                i4 += this.L.f184b + 8;
            }
            if (this.y != null) {
                this.c.a((this.c.e & 65535) >= 50 ? "StackMapTable" : "StackMap");
                i3 = i4 + this.y.f184b + 8;
            } else {
                i3 = i4;
            }
            if (this.M != null) {
                i3 += this.M.b(this.c, this.t.f183a, this.t.f184b, this.u, this.v);
            }
        }
        if (this.g > 0) {
            this.c.a("Exceptions");
            i3 += (this.g * 2) + 8;
        }
        if ((this.i & 4096) != 0 && ((this.c.e & 65535) < 49 || (this.i & 262144) != 0)) {
            this.c.a("Synthetic");
            i3 += 6;
        }
        if ((this.i & 131072) != 0) {
            this.c.a("Deprecated");
            i3 += 6;
        }
        if (this.d != null) {
            this.c.a("Signature");
            this.c.a(this.d);
            i3 += 8;
        }
        if (this.m != null) {
            this.c.a("AnnotationDefault");
            i3 += this.m.f184b + 6;
        }
        if (this.n != null) {
            this.c.a("RuntimeVisibleAnnotations");
            i3 += this.n.b() + 8;
        }
        if (this.o != null) {
            this.c.a("RuntimeInvisibleAnnotations");
            i3 += this.o.b() + 8;
        }
        if (this.p != null) {
            this.c.a("RuntimeVisibleParameterAnnotations");
            i2 = i3 + ((this.p.length - this.r) * 2) + 7;
            int length = this.p.length;
            while (true) {
                length--;
                if (length < this.r) {
                    break;
                }
                i2 += this.p[length] == null ? 0 : this.p[length].b();
            }
        } else {
            i2 = i3;
        }
        if (this.q != null) {
            this.c.a("RuntimeInvisibleParameterAnnotations");
            int length2 = i2 + ((this.q.length - this.r) * 2) + 7;
            int length3 = this.q.length;
            while (true) {
                length3--;
                if (length3 < this.r) {
                    break;
                }
                length2 = i2 + (this.q[length3] == null ? 0 : this.q[length3].b());
            }
        }
        int i5 = i2;
        return this.s != null ? i5 + this.s.b(this.c, null, 0, -1, -1) : i5;
    }

    public void d(int i2, int i3) {
        o oVar;
        if (this.P == 0) {
            for (m mVar = this.E; mVar != null; mVar = mVar.f) {
                o a2 = mVar.f197a.a();
                o a3 = mVar.c.a();
                o a4 = mVar.f198b.a();
                int e2 = 24117248 | this.c.e(mVar.d == null ? "java/lang/Throwable" : mVar.d);
                a3.f201a |= 16;
                for (o oVar2 = a2; oVar2 != a4; oVar2 = oVar2.g) {
                    h hVar = new h();
                    hVar.f189a = e2;
                    hVar.f190b = a3;
                    hVar.c = oVar2.h;
                    oVar2.h = hVar;
                }
            }
            k kVar = this.Q.f;
            kVar.a(this.c, this.i, s.d(this.l), this.v);
            a(kVar);
            o oVar3 = this.Q;
            int i4 = 0;
            while (oVar3 != null) {
                o oVar4 = oVar3.i;
                oVar3.i = null;
                k kVar2 = oVar3.f;
                if ((oVar3.f201a & 16) != 0) {
                    oVar3.f201a |= 32;
                }
                oVar3.f201a |= 64;
                int length = kVar2.d.length + oVar3.e;
                if (length <= i4) {
                    length = i4;
                }
                h hVar2 = oVar3.h;
                while (hVar2 != null) {
                    o a5 = hVar2.f190b.a();
                    if (!kVar2.a(this.c, a5.f, hVar2.f189a) || a5.i != null) {
                        a5 = oVar4;
                    } else {
                        a5.i = oVar4;
                    }
                    hVar2 = hVar2.c;
                    oVar4 = a5;
                }
                oVar3 = oVar4;
                i4 = length;
            }
            int i5 = i4;
            for (o oVar5 = this.Q; oVar5 != null; oVar5 = oVar5.g) {
                k kVar3 = oVar5.f;
                if ((oVar5.f201a & 32) != 0) {
                    a(kVar3);
                }
                if ((oVar5.f201a & 64) == 0) {
                    o oVar6 = oVar5.g;
                    int i6 = oVar5.c;
                    int i7 = (oVar6 == null ? this.t.f184b : oVar6.c) - 1;
                    if (i7 >= i6) {
                        i5 = Math.max(i5, 1);
                        for (int i8 = i6; i8 < i7; i8++) {
                            this.t.f183a[i8] = 0;
                        }
                        this.t.f183a[i7] = -65;
                        a(i6, 0, 1);
                        int[] iArr = this.C;
                        int i9 = this.B;
                        this.B = i9 + 1;
                        iArr[i9] = this.c.e("java/lang/Throwable") | 24117248;
                        f();
                        this.E = m.a(this.E, oVar5, oVar6);
                    }
                }
            }
            this.D = 0;
            for (m mVar2 = this.E; mVar2 != null; mVar2 = mVar2.f) {
                this.D++;
            }
            this.u = i5;
        } else if (this.P == 1) {
            for (m mVar3 = this.E; mVar3 != null; mVar3 = mVar3.f) {
                o oVar7 = mVar3.c;
                o oVar8 = mVar3.f198b;
                for (o oVar9 = mVar3.f197a; oVar9 != oVar8; oVar9 = oVar9.g) {
                    h hVar3 = new h();
                    hVar3.f189a = Integer.MAX_VALUE;
                    hVar3.f190b = oVar7;
                    if ((oVar9.f201a & 128) == 0) {
                        hVar3.c = oVar9.h;
                        oVar9.h = hVar3;
                    } else {
                        hVar3.c = oVar9.h.c.c;
                        oVar9.h.c.c = hVar3;
                    }
                }
            }
            if (this.O > 0) {
                this.Q.a((o) null, 1, this.O);
                int i10 = 0;
                for (o oVar10 = this.Q; oVar10 != null; oVar10 = oVar10.g) {
                    if ((oVar10.f201a & 128) != 0) {
                        o oVar11 = oVar10.h.c.f190b;
                        if ((oVar11.f201a & 1024) == 0) {
                            i10++;
                            oVar11.a((o) null, ((((long) i10) / 32) << 32) | (1 << (i10 % 32)), this.O);
                        }
                    }
                }
                for (o oVar12 = this.Q; oVar12 != null; oVar12 = oVar12.g) {
                    if ((oVar12.f201a & 128) != 0) {
                        for (o oVar13 = this.Q; oVar13 != null; oVar13 = oVar13.g) {
                            oVar13.f201a &= -2049;
                        }
                        oVar12.h.c.f190b.a(oVar12, 0, this.O);
                    }
                }
            }
            o oVar14 = this.Q;
            int i11 = 0;
            while (oVar14 != null) {
                o oVar15 = oVar14.i;
                int i12 = oVar14.d;
                int i13 = oVar14.e + i12;
                if (i13 <= i11) {
                    i13 = i11;
                }
                h hVar4 = oVar14.h;
                h hVar5 = (oVar14.f201a & 128) != 0 ? hVar4.c : hVar4;
                while (hVar5 != null) {
                    o oVar16 = hVar5.f190b;
                    if ((oVar16.f201a & 8) == 0) {
                        oVar16.d = hVar5.f189a == Integer.MAX_VALUE ? 1 : hVar5.f189a + i12;
                        oVar16.f201a |= 8;
                        oVar16.i = oVar15;
                        oVar = oVar16;
                    } else {
                        oVar = oVar15;
                    }
                    hVar5 = hVar5.c;
                    oVar15 = oVar;
                }
                oVar14 = oVar15;
                i11 = i13;
            }
            this.u = Math.max(i2, i11);
        } else {
            this.u = i2;
            this.v = i3;
        }
    }
}
