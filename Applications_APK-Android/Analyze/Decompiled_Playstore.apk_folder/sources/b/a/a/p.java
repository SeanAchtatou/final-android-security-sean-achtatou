package b.a.a;

public abstract class p {

    /* renamed from: a  reason: collision with root package name */
    protected final int f203a;

    /* renamed from: b  reason: collision with root package name */
    protected p f204b;

    public p(int i) {
        this(i, null);
    }

    public p(int i, p pVar) {
        this.f203a = i;
        this.f204b = pVar;
    }

    public a a() {
        if (this.f204b != null) {
            return this.f204b.a();
        }
        return null;
    }

    public a a(int i, String str, boolean z) {
        if (this.f204b != null) {
            return this.f204b.a(i, str, z);
        }
        return null;
    }

    public a a(String str, boolean z) {
        if (this.f204b != null) {
            return this.f204b.a(str, z);
        }
        return null;
    }

    public void a(int i) {
        if (this.f204b != null) {
            this.f204b.a(i);
        }
    }

    public void a(int i, int i2) {
        if (this.f204b != null) {
            this.f204b.a(i, i2);
        }
    }

    public void a(int i, int i2, o oVar, o... oVarArr) {
        if (this.f204b != null) {
            this.f204b.a(i, i2, oVar, oVarArr);
        }
    }

    public void a(int i, int i2, Object[] objArr, int i3, Object[] objArr2) {
        if (this.f204b != null) {
            this.f204b.a(i, i2, objArr, i3, objArr2);
        }
    }

    public void a(int i, o oVar) {
        if (this.f204b != null) {
            this.f204b.a(i, oVar);
        }
    }

    public void a(int i, String str) {
        if (this.f204b != null) {
            this.f204b.a(i, str);
        }
    }

    public void a(int i, String str, String str2, String str3) {
        if (this.f204b != null) {
            this.f204b.a(i, str, str2, str3);
        }
    }

    public void a(c cVar) {
        if (this.f204b != null) {
            this.f204b.a(cVar);
        }
    }

    public void a(o oVar) {
        if (this.f204b != null) {
            this.f204b.a(oVar);
        }
    }

    public void a(o oVar, o oVar2, o oVar3, String str) {
        if (this.f204b != null) {
            this.f204b.a(oVar, oVar2, oVar3, str);
        }
    }

    public void a(o oVar, int[] iArr, o[] oVarArr) {
        if (this.f204b != null) {
            this.f204b.a(oVar, iArr, oVarArr);
        }
    }

    public void a(Object obj) {
        if (this.f204b != null) {
            this.f204b.a(obj);
        }
    }

    public void a(String str, int i) {
        if (this.f204b != null) {
            this.f204b.a(str, i);
        }
    }

    public void a(String str, String str2, l lVar, Object... objArr) {
        if (this.f204b != null) {
            this.f204b.a(str, str2, lVar, objArr);
        }
    }

    public void a(String str, String str2, String str3, o oVar, o oVar2, int i) {
        if (this.f204b != null) {
            this.f204b.a(str, str2, str3, oVar, oVar2, i);
        }
    }

    public void b() {
        if (this.f204b != null) {
            this.f204b.b();
        }
    }

    public void b(int i, int i2) {
        if (this.f204b != null) {
            this.f204b.b(i, i2);
        }
    }

    public void b(int i, o oVar) {
        if (this.f204b != null) {
            this.f204b.b(i, oVar);
        }
    }

    public void b(int i, String str, String str2, String str3) {
        if (this.f204b != null) {
            this.f204b.b(i, str, str2, str3);
        }
    }

    public void c() {
        if (this.f204b != null) {
            this.f204b.c();
        }
    }

    public void c(int i, int i2) {
        if (this.f204b != null) {
            this.f204b.c(i, i2);
        }
    }

    public void d(int i, int i2) {
        if (this.f204b != null) {
            this.f204b.d(i, i2);
        }
    }
}
