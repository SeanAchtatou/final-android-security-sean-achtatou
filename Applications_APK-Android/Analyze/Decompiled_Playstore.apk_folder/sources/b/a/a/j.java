package b.a.a;

final class j extends i {
    private final g c;
    private final int d;
    private final int e;
    private final int f;
    private int g;
    private int h;
    private b i;
    private b j;
    private c k;

    j(g gVar, int i2, String str, String str2, String str3, Object obj) {
        super(262144);
        if (gVar.r == null) {
            gVar.r = this;
        } else {
            gVar.s.f192b = this;
        }
        gVar.s = this;
        this.c = gVar;
        this.d = i2;
        this.e = gVar.a(str);
        this.f = gVar.a(str2);
        if (str3 != null) {
            this.g = gVar.a(str3);
        }
        if (obj != null) {
            this.h = gVar.a(obj).f199a;
        }
    }

    public a a(String str, boolean z) {
        d dVar = new d();
        dVar.b(this.c.a(str)).b(0);
        b bVar = new b(this.c, true, dVar, dVar, 2);
        if (z) {
            bVar.c = this.i;
            this.i = bVar;
        } else {
            bVar.c = this.j;
            this.j = bVar;
        }
        return bVar;
    }

    public void a() {
    }

    public void a(c cVar) {
        cVar.c = this.k;
        this.k = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        dVar.b(((393216 | ((this.d & 262144) / 64)) ^ -1) & this.d).b(this.e).b(this.f);
        int i2 = this.h != 0 ? 1 : 0;
        if ((this.d & 4096) != 0 && ((this.c.e & 65535) < 49 || (this.d & 262144) != 0)) {
            i2++;
        }
        if ((this.d & 131072) != 0) {
            i2++;
        }
        if (this.g != 0) {
            i2++;
        }
        if (this.i != null) {
            i2++;
        }
        if (this.j != null) {
            i2++;
        }
        if (this.k != null) {
            i2 += this.k.c();
        }
        dVar.b(i2);
        if (this.h != 0) {
            dVar.b(this.c.a("ConstantValue"));
            dVar.c(2).b(this.h);
        }
        if ((this.d & 4096) != 0 && ((this.c.e & 65535) < 49 || (this.d & 262144) != 0)) {
            dVar.b(this.c.a("Synthetic")).c(0);
        }
        if ((this.d & 131072) != 0) {
            dVar.b(this.c.a("Deprecated")).c(0);
        }
        if (this.g != 0) {
            dVar.b(this.c.a("Signature"));
            dVar.c(2).b(this.g);
        }
        if (this.i != null) {
            dVar.b(this.c.a("RuntimeVisibleAnnotations"));
            this.i.a(dVar);
        }
        if (this.j != null) {
            dVar.b(this.c.a("RuntimeInvisibleAnnotations"));
            this.j.a(dVar);
        }
        if (this.k != null) {
            this.k.a(this.c, (byte[]) null, 0, -1, -1, dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        int i2;
        int i3 = 8;
        if (this.h != 0) {
            this.c.a("ConstantValue");
            i3 = 16;
        }
        if ((this.d & 4096) != 0 && ((this.c.e & 65535) < 49 || (this.d & 262144) != 0)) {
            this.c.a("Synthetic");
            i3 += 6;
        }
        if ((this.d & 131072) != 0) {
            this.c.a("Deprecated");
            i3 += 6;
        }
        if (this.g != 0) {
            this.c.a("Signature");
            i3 += 8;
        }
        if (this.i != null) {
            this.c.a("RuntimeVisibleAnnotations");
            i3 += this.i.b() + 8;
        }
        if (this.j != null) {
            this.c.a("RuntimeInvisibleAnnotations");
            i2 = i3 + this.j.b() + 8;
        } else {
            i2 = i3;
        }
        return this.k != null ? i2 + this.k.b(this.c, null, 0, -1, -1) : i2;
    }
}
