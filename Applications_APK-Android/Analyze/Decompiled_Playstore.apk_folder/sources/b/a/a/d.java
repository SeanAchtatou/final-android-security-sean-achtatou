package b.a.a;

public class d {

    /* renamed from: a  reason: collision with root package name */
    byte[] f183a;

    /* renamed from: b  reason: collision with root package name */
    int f184b;

    public d() {
        this.f183a = new byte[64];
    }

    public d(int i) {
        this.f183a = new byte[i];
    }

    private void d(int i) {
        int length = this.f183a.length * 2;
        int i2 = this.f184b + i;
        if (length <= i2) {
            length = i2;
        }
        byte[] bArr = new byte[length];
        System.arraycopy(this.f183a, 0, bArr, 0, this.f184b);
        this.f183a = bArr;
    }

    public d a(int i) {
        int i2 = this.f184b;
        if (i2 + 1 > this.f183a.length) {
            d(1);
        }
        this.f183a[i2] = (byte) i;
        this.f184b = i2 + 1;
        return this;
    }

    /* access modifiers changed from: package-private */
    public d a(int i, int i2) {
        int i3 = this.f184b;
        if (i3 + 2 > this.f183a.length) {
            d(2);
        }
        byte[] bArr = this.f183a;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        bArr[i4] = (byte) i2;
        this.f184b = i4 + 1;
        return this;
    }

    public d a(long j) {
        int i = this.f184b;
        if (i + 8 > this.f183a.length) {
            d(8);
        }
        byte[] bArr = this.f183a;
        int i2 = (int) (j >>> 32);
        int i3 = i + 1;
        bArr[i] = (byte) (i2 >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i2 >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >>> 8);
        int i6 = i5 + 1;
        bArr[i5] = (byte) i2;
        int i7 = (int) j;
        int i8 = i6 + 1;
        bArr[i6] = (byte) (i7 >>> 24);
        int i9 = i8 + 1;
        bArr[i8] = (byte) (i7 >>> 16);
        int i10 = i9 + 1;
        bArr[i9] = (byte) (i7 >>> 8);
        bArr[i10] = (byte) i7;
        this.f184b = i10 + 1;
        return this;
    }

    public d a(String str) {
        int i;
        byte[] bArr;
        int i2;
        int length = str.length();
        int i3 = this.f184b;
        if (i3 + 2 + length > this.f183a.length) {
            d(length + 2);
        }
        byte[] bArr2 = this.f183a;
        int i4 = i3 + 1;
        bArr2[i3] = (byte) (length >>> 8);
        int i5 = i4 + 1;
        bArr2[i4] = (byte) length;
        int i6 = 0;
        while (true) {
            if (i6 >= length) {
                i = i5;
                break;
            }
            char charAt = str.charAt(i6);
            if (charAt < 1 || charAt > 127) {
                int i7 = i6;
            } else {
                bArr2[i5] = (byte) charAt;
                i6++;
                i5++;
            }
        }
        int i72 = i6;
        for (int i8 = i6; i8 < length; i8++) {
            char charAt2 = str.charAt(i8);
            i72 = (charAt2 < 1 || charAt2 > 127) ? charAt2 > 2047 ? i72 + 3 : i72 + 2 : i72 + 1;
        }
        bArr2[this.f184b] = (byte) (i72 >>> 8);
        bArr2[this.f184b + 1] = (byte) i72;
        if (this.f184b + 2 + i72 > bArr2.length) {
            this.f184b = i5;
            d(i72 + 2);
            bArr = this.f183a;
        } else {
            bArr = bArr2;
        }
        while (i6 < length) {
            char charAt3 = str.charAt(i6);
            if (charAt3 >= 1 && charAt3 <= 127) {
                i2 = i5 + 1;
                bArr[i5] = (byte) charAt3;
            } else if (charAt3 > 2047) {
                int i9 = i5 + 1;
                bArr[i5] = (byte) (((charAt3 >> 12) & 15) | 224);
                int i10 = i9 + 1;
                bArr[i9] = (byte) (((charAt3 >> 6) & 63) | 128);
                i2 = i10 + 1;
                bArr[i10] = (byte) ((charAt3 & '?') | 128);
            } else {
                int i11 = i5 + 1;
                bArr[i5] = (byte) (((charAt3 >> 6) & 31) | 192);
                i2 = i11 + 1;
                bArr[i11] = (byte) ((charAt3 & '?') | 128);
            }
            i6++;
            i5 = i2;
        }
        i = i5;
        this.f184b = i;
        return this;
    }

    public d a(byte[] bArr, int i, int i2) {
        if (this.f184b + i2 > this.f183a.length) {
            d(i2);
        }
        if (bArr != null) {
            System.arraycopy(bArr, i, this.f183a, this.f184b, i2);
        }
        this.f184b += i2;
        return this;
    }

    public d b(int i) {
        int i2 = this.f184b;
        if (i2 + 2 > this.f183a.length) {
            d(2);
        }
        byte[] bArr = this.f183a;
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 8);
        bArr[i3] = (byte) i;
        this.f184b = i3 + 1;
        return this;
    }

    /* access modifiers changed from: package-private */
    public d b(int i, int i2) {
        int i3 = this.f184b;
        if (i3 + 3 > this.f183a.length) {
            d(3);
        }
        byte[] bArr = this.f183a;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >>> 8);
        bArr[i5] = (byte) i2;
        this.f184b = i5 + 1;
        return this;
    }

    public d c(int i) {
        int i2 = this.f184b;
        if (i2 + 4 > this.f183a.length) {
            d(4);
        }
        byte[] bArr = this.f183a;
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i >>> 8);
        bArr[i5] = (byte) i;
        this.f184b = i5 + 1;
        return this;
    }
}
