package b.a.a;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected final int f179a;

    /* renamed from: b  reason: collision with root package name */
    protected a f180b;

    public a(int i) {
        this(i, null);
    }

    public a(int i, a aVar) {
        this.f179a = i;
        this.f180b = aVar;
    }

    public a a(String str) {
        if (this.f180b != null) {
            return this.f180b.a(str);
        }
        return null;
    }

    public a a(String str, String str2) {
        if (this.f180b != null) {
            return this.f180b.a(str, str2);
        }
        return null;
    }

    public void a() {
        if (this.f180b != null) {
            this.f180b.a();
        }
    }

    public void a(String str, Object obj) {
        if (this.f180b != null) {
            this.f180b.a(str, obj);
        }
    }

    public void a(String str, String str2, String str3) {
        if (this.f180b != null) {
            this.f180b.a(str, str2, str3);
        }
    }
}
