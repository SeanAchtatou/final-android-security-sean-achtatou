package b.a.a;

final class b extends a {
    b c;
    b d;
    private final g e;
    private int f;
    private final boolean g;
    private final d h;
    private final d i;
    private final int j;

    b(g gVar, boolean z, d dVar, d dVar2, int i2) {
        super(262144);
        this.e = gVar;
        this.g = z;
        this.h = dVar;
        this.i = dVar2;
        this.j = i2;
    }

    static void a(b[] bVarArr, int i2, d dVar) {
        int length = ((bVarArr.length - i2) * 2) + 1;
        for (int i3 = i2; i3 < bVarArr.length; i3++) {
            length += bVarArr[i3] == null ? 0 : bVarArr[i3].b();
        }
        dVar.c(length).a(bVarArr.length - i2);
        while (i2 < bVarArr.length) {
            b bVar = bVarArr[i2];
            b bVar2 = null;
            int i4 = 0;
            while (bVar != null) {
                i4++;
                bVar.a();
                bVar.d = bVar2;
                b bVar3 = bVar;
                bVar = bVar.c;
                bVar2 = bVar3;
            }
            dVar.b(i4);
            while (bVar2 != null) {
                dVar.a(bVar2.h.f183a, 0, bVar2.h.f184b);
                bVar2 = bVar2.d;
            }
            i2++;
        }
    }

    public a a(String str) {
        this.f++;
        if (this.g) {
            this.h.b(this.e.a(str));
        }
        this.h.b(91, 0);
        return new b(this.e, false, this.h, this.h, this.h.f184b - 2);
    }

    public a a(String str, String str2) {
        this.f++;
        if (this.g) {
            this.h.b(this.e.a(str));
        }
        this.h.b(64, this.e.a(str2)).b(0);
        return new b(this.e, true, this.h, this.h, this.h.f184b - 2);
    }

    public void a() {
        if (this.i != null) {
            byte[] bArr = this.i.f183a;
            bArr[this.j] = (byte) (this.f >>> 8);
            bArr[this.j + 1] = (byte) this.f;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        b bVar = null;
        int i2 = 2;
        int i3 = 0;
        for (b bVar2 = this; bVar2 != null; bVar2 = bVar2.c) {
            i3++;
            i2 += bVar2.h.f184b;
            bVar2.a();
            bVar2.d = bVar;
            bVar = bVar2;
        }
        dVar.c(i2);
        dVar.b(i3);
        while (bVar != null) {
            dVar.a(bVar.h.f183a, 0, bVar.h.f184b);
            bVar = bVar.d;
        }
    }

    public void a(String str, Object obj) {
        int i2 = 1;
        int i3 = 0;
        this.f++;
        if (this.g) {
            this.h.b(this.e.a(str));
        }
        if (obj instanceof String) {
            this.h.b(115, this.e.a((String) obj));
        } else if (obj instanceof Byte) {
            this.h.b(66, this.e.a((int) ((Byte) obj).byteValue()).f199a);
        } else if (obj instanceof Boolean) {
            if (!((Boolean) obj).booleanValue()) {
                i2 = 0;
            }
            this.h.b(90, this.e.a(i2).f199a);
        } else if (obj instanceof Character) {
            this.h.b(67, this.e.a((int) ((Character) obj).charValue()).f199a);
        } else if (obj instanceof Short) {
            this.h.b(83, this.e.a((int) ((Short) obj).shortValue()).f199a);
        } else if (obj instanceof s) {
            this.h.b(99, this.e.a(((s) obj).f()));
        } else if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            this.h.b(91, bArr.length);
            while (i3 < bArr.length) {
                this.h.b(66, this.e.a((int) bArr[i3]).f199a);
                i3++;
            }
        } else if (obj instanceof boolean[]) {
            boolean[] zArr = (boolean[]) obj;
            this.h.b(91, zArr.length);
            for (int i4 = 0; i4 < zArr.length; i4++) {
                this.h.b(90, this.e.a(zArr[i4] ? 1 : 0).f199a);
            }
        } else if (obj instanceof short[]) {
            short[] sArr = (short[]) obj;
            this.h.b(91, sArr.length);
            while (i3 < sArr.length) {
                this.h.b(83, this.e.a((int) sArr[i3]).f199a);
                i3++;
            }
        } else if (obj instanceof char[]) {
            char[] cArr = (char[]) obj;
            this.h.b(91, cArr.length);
            while (i3 < cArr.length) {
                this.h.b(67, this.e.a((int) cArr[i3]).f199a);
                i3++;
            }
        } else if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            this.h.b(91, iArr.length);
            while (i3 < iArr.length) {
                this.h.b(73, this.e.a(iArr[i3]).f199a);
                i3++;
            }
        } else if (obj instanceof long[]) {
            long[] jArr = (long[]) obj;
            this.h.b(91, jArr.length);
            while (i3 < jArr.length) {
                this.h.b(74, this.e.a(jArr[i3]).f199a);
                i3++;
            }
        } else if (obj instanceof float[]) {
            float[] fArr = (float[]) obj;
            this.h.b(91, fArr.length);
            while (i3 < fArr.length) {
                this.h.b(70, this.e.a(fArr[i3]).f199a);
                i3++;
            }
        } else if (obj instanceof double[]) {
            double[] dArr = (double[]) obj;
            this.h.b(91, dArr.length);
            while (i3 < dArr.length) {
                this.h.b(68, this.e.a(dArr[i3]).f199a);
                i3++;
            }
        } else {
            n a2 = this.e.a(obj);
            this.h.b(".s.IFJDCS".charAt(a2.f200b), a2.f199a);
        }
    }

    public void a(String str, String str2, String str3) {
        this.f++;
        if (this.g) {
            this.h.b(this.e.a(str));
        }
        this.h.b(101, this.e.a(str2)).b(this.e.a(str3));
    }

    /* access modifiers changed from: package-private */
    public int b() {
        int i2 = 0;
        while (this != null) {
            i2 += this.h.f184b;
            this = this.c;
        }
        return i2;
    }
}
