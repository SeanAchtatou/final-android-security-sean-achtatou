package b.a.a;

public class g extends f {
    static final byte[] c;
    private int A;
    private int B;
    private int[] C;
    private int D;
    private d E;
    private int F;
    private int G;
    private b H;
    private b I;
    private c J;
    private int K;
    private d L;
    private final boolean M;
    private final boolean N;
    e d;
    int e;
    int f = 1;
    final d g = new d();
    n[] h = new n[256];
    int i = ((int) (0.75d * ((double) this.h.length)));
    final n j = new n();
    final n k = new n();
    final n l = new n();
    final n m = new n();
    n[] n;
    String o;
    int p;
    d q;
    j r;
    j s;
    q t;
    q u;
    boolean v;
    private short w;
    private int x;
    private int y;
    private int z;

    static {
        byte[] bArr = new byte[220];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = (byte) ("AAAAAAAAAAAAAAAABCLMMDDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAADDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAAAAAAAAAAAAAAAAAJJJJJJJJJJJJJJJJDOPAAAAAAGGGGGGGHIFBFAAFFAARQJJKKJJJJJJJJJJJJJJJJJJ".charAt(i2) - 'A');
        }
        c = bArr;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(int i2) {
        super(262144);
        boolean z2 = true;
        this.M = (i2 & 1) != 0;
        this.N = (i2 & 2) == 0 ? false : z2;
    }

    private n a(n nVar) {
        this.w = (short) (this.w + 1);
        n nVar2 = new n(this.w, this.j);
        c(nVar2);
        if (this.n == null) {
            this.n = new n[16];
        }
        if (this.w == this.n.length) {
            n[] nVarArr = new n[(this.n.length * 2)];
            System.arraycopy(this.n, 0, nVarArr, 0, this.n.length);
            this.n = nVarArr;
        }
        this.n[this.w] = nVar2;
        return nVar2;
    }

    private void a(int i2, int i3, int i4) {
        this.g.b(i2, i3).b(i4);
    }

    private n b(n nVar) {
        n nVar2 = this.h[nVar.h % this.h.length];
        while (nVar2 != null && (nVar2.f200b != nVar.f200b || !nVar.a(nVar2))) {
            nVar2 = nVar2.i;
        }
        return nVar2;
    }

    private void b(int i2, int i3, int i4) {
        this.g.a(i2, i3).b(i4);
    }

    private void c(n nVar) {
        if (this.f + this.w > this.i) {
            int length = this.h.length;
            int i2 = (length * 2) + 1;
            n[] nVarArr = new n[i2];
            for (int i3 = length - 1; i3 >= 0; i3--) {
                n nVar2 = this.h[i3];
                while (nVar2 != null) {
                    int length2 = nVar2.h % nVarArr.length;
                    n nVar3 = nVar2.i;
                    nVar2.i = nVarArr[length2];
                    nVarArr[length2] = nVar2;
                    nVar2 = nVar3;
                }
            }
            this.h = nVarArr;
            this.i = (int) (((double) i2) * 0.75d);
        }
        int length3 = nVar.h % this.h.length;
        nVar.i = this.h[length3];
        this.h[length3] = nVar;
    }

    private n f(String str) {
        this.k.a(8, str, null, null);
        n b2 = b(this.k);
        if (b2 != null) {
            return b2;
        }
        this.g.b(8, a(str));
        int i2 = this.f;
        this.f = i2 + 1;
        n nVar = new n(i2, this.k);
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        this.k.f200b = 32;
        this.k.d = ((long) i2) | (((long) i3) << 32);
        this.k.h = Integer.MAX_VALUE & (i2 + 32 + i3);
        n b2 = b(this.k);
        if (b2 == null) {
            String str = this.n[i2].e;
            String str2 = this.n[i3].e;
            this.k.c = e(d(str, str2));
            b2 = new n(0, this.k);
            c(b2);
        }
        return b2.c;
    }

    public int a(String str) {
        this.j.a(1, str, null, null);
        n b2 = b(this.j);
        if (b2 == null) {
            this.g.a(1).a(str);
            int i2 = this.f;
            this.f = i2 + 1;
            b2 = new n(i2, this.j);
            c(b2);
        }
        return b2.f199a;
    }

    /* access modifiers changed from: package-private */
    public int a(String str, int i2) {
        this.j.f200b = 31;
        this.j.c = i2;
        this.j.e = str;
        this.j.h = Integer.MAX_VALUE & (str.hashCode() + 31 + i2);
        n b2 = b(this.j);
        if (b2 == null) {
            b2 = a(this.j);
        }
        return b2.f199a;
    }

    public final a a(String str, boolean z2) {
        d dVar = new d();
        dVar.b(a(str)).b(0);
        b bVar = new b(this, true, dVar, dVar, 2);
        if (z2) {
            bVar.c = this.H;
            this.H = bVar;
        } else {
            bVar.c = this.I;
            this.I = bVar;
        }
        return bVar;
    }

    public final i a(int i2, String str, String str2, String str3, Object obj) {
        return new j(this, i2, str, str2, str3, obj);
    }

    /* access modifiers changed from: package-private */
    public n a(double d2) {
        this.j.a(d2);
        n b2 = b(this.j);
        if (b2 != null) {
            return b2;
        }
        this.g.a(6).a(this.j.d);
        n nVar = new n(this.f, this.j);
        this.f += 2;
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n a(float f2) {
        this.j.a(f2);
        n b2 = b(this.j);
        if (b2 != null) {
            return b2;
        }
        this.g.a(4).c(this.j.c);
        int i2 = this.f;
        this.f = i2 + 1;
        n nVar = new n(i2, this.j);
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n a(int i2) {
        this.j.a(i2);
        n b2 = b(this.j);
        if (b2 != null) {
            return b2;
        }
        this.g.a(3).c(i2);
        int i3 = this.f;
        this.f = i3 + 1;
        n nVar = new n(i3, this.j);
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n a(int i2, String str, String str2, String str3) {
        this.m.a(i2 + 20, str, str2, str3);
        n b2 = b(this.m);
        if (b2 != null) {
            return b2;
        }
        if (i2 <= 4) {
            b(15, i2, c(str, str2, str3));
        } else {
            b(15, i2, b(str, str2, str3, i2 == 9));
        }
        int i3 = this.f;
        this.f = i3 + 1;
        n nVar = new n(i3, this.m);
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n a(long j2) {
        this.j.a(j2);
        n b2 = b(this.j);
        if (b2 != null) {
            return b2;
        }
        this.g.a(5).a(j2);
        n nVar = new n(this.f, this.j);
        this.f += 2;
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n a(Object obj) {
        if (obj instanceof Integer) {
            return a(((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return a(((Byte) obj).intValue());
        }
        if (obj instanceof Character) {
            return a((int) ((Character) obj).charValue());
        }
        if (obj instanceof Short) {
            return a(((Short) obj).intValue());
        }
        if (obj instanceof Boolean) {
            return a(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return a(((Float) obj).floatValue());
        } else {
            if (obj instanceof Long) {
                return a(((Long) obj).longValue());
            }
            if (obj instanceof Double) {
                return a(((Double) obj).doubleValue());
            }
            if (obj instanceof String) {
                return f((String) obj);
            }
            if (obj instanceof s) {
                s sVar = (s) obj;
                int a2 = sVar.a();
                return a2 == 9 ? b(sVar.f()) : a2 == 10 ? b(sVar.e()) : d(sVar.f());
            } else if (obj instanceof l) {
                l lVar = (l) obj;
                return a(lVar.f195a, lVar.f196b, lVar.c, lVar.d);
            } else {
                throw new IllegalArgumentException(new StringBuffer().append("value ").append(obj).toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public n a(String str, String str2, l lVar, Object... objArr) {
        int i2;
        d dVar = this.q;
        if (dVar == null) {
            dVar = new d();
            this.q = dVar;
        }
        int i3 = dVar.f184b;
        int hashCode = lVar.hashCode();
        dVar.b(b(lVar.f195a, lVar.f196b, lVar.c, lVar.d));
        dVar.b(r5);
        int i4 = hashCode;
        for (Object obj : objArr) {
            i4 ^= obj.hashCode();
            dVar.b(b(obj));
        }
        byte[] bArr = dVar.f183a;
        int i5 = (r5 + 2) << 1;
        int i6 = i4 & Integer.MAX_VALUE;
        n nVar = this.h[i6 % this.h.length];
        loop1:
        while (nVar != null) {
            if (nVar.f200b == 33 && nVar.h == i6) {
                int i7 = nVar.c;
                int i8 = 0;
                while (i8 < i5) {
                    if (bArr[i3 + i8] != bArr[i7 + i8]) {
                        nVar = nVar.i;
                    } else {
                        i8++;
                    }
                }
                break loop1;
            }
            nVar = nVar.i;
        }
        if (nVar != null) {
            int i9 = nVar.f199a;
            dVar.f184b = i3;
            i2 = i9;
        } else {
            i2 = this.p;
            this.p = i2 + 1;
            n nVar2 = new n(i2);
            nVar2.a(i3, i6);
            c(nVar2);
        }
        this.l.a(str, str2, i2);
        n b2 = b(this.l);
        if (b2 != null) {
            return b2;
        }
        a(18, i2, b(str, str2));
        int i10 = this.f;
        this.f = i10 + 1;
        n nVar3 = new n(i10, this.l);
        c(nVar3);
        return nVar3;
    }

    /* access modifiers changed from: package-private */
    public n a(String str, String str2, String str3, boolean z2) {
        int i2 = z2 ? 11 : 10;
        this.l.a(i2, str, str2, str3);
        n b2 = b(this.l);
        if (b2 != null) {
            return b2;
        }
        a(i2, c(str), b(str2, str3));
        int i3 = this.f;
        this.f = i3 + 1;
        n nVar = new n(i3, this.l);
        c(nVar);
        return nVar;
    }

    public final p a(int i2, String str, String str2, String str3, String[] strArr) {
        return new q(this, i2, str, str2, str3, strArr, this.M, this.N);
    }

    public final void a() {
    }

    public final void a(int i2, int i3, String str, String str2, String str3, String[] strArr) {
        this.e = i2;
        this.x = i3;
        this.y = c(str);
        this.o = str;
        if (str2 != null) {
            this.z = a(str2);
        }
        this.A = str3 == null ? 0 : c(str3);
        if (strArr != null && strArr.length > 0) {
            this.B = strArr.length;
            this.C = new int[this.B];
            for (int i4 = 0; i4 < this.B; i4++) {
                this.C[i4] = c(strArr[i4]);
            }
        }
    }

    public final void a(c cVar) {
        cVar.c = this.J;
        this.J = cVar;
    }

    public final void a(String str, String str2) {
        if (str != null) {
            this.D = a(str);
        }
        if (str2 != null) {
            this.E = new d().a(str2);
        }
    }

    public final void a(String str, String str2, String str3) {
        this.F = c(str);
        if (str2 != null && str3 != null) {
            this.G = b(str2, str3);
        }
    }

    public final void a(String str, String str2, String str3, int i2) {
        int i3 = 0;
        if (this.L == null) {
            this.L = new d();
        }
        this.K++;
        this.L.b(str == null ? 0 : c(str));
        this.L.b(str2 == null ? 0 : c(str2));
        d dVar = this.L;
        if (str3 != null) {
            i3 = a(str3);
        }
        dVar.b(i3);
        this.L.b(i2);
    }

    public int b(int i2, String str, String str2, String str3) {
        return a(i2, str, str2, str3).f199a;
    }

    public int b(Object obj) {
        return a(obj).f199a;
    }

    public int b(String str, String str2) {
        return c(str, str2).f199a;
    }

    public int b(String str, String str2, String str3, boolean z2) {
        return a(str, str2, str3, z2).f199a;
    }

    /* access modifiers changed from: package-private */
    public n b(String str) {
        this.k.a(7, str, null, null);
        n b2 = b(this.k);
        if (b2 != null) {
            return b2;
        }
        this.g.b(7, a(str));
        int i2 = this.f;
        this.f = i2 + 1;
        n nVar = new n(i2, this.k);
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n b(String str, String str2, String str3) {
        this.l.a(9, str, str2, str3);
        n b2 = b(this.l);
        if (b2 != null) {
            return b2;
        }
        a(9, c(str), b(str2, str3));
        int i2 = this.f;
        this.f = i2 + 1;
        n nVar = new n(i2, this.l);
        c(nVar);
        return nVar;
    }

    public byte[] b() {
        int i2;
        int i3;
        if (this.f > 32767) {
            throw new RuntimeException("Class file too large!");
        }
        int i4 = (this.B * 2) + 24;
        j jVar = this.r;
        int i5 = 0;
        while (jVar != null) {
            i4 += jVar.b();
            jVar = (j) jVar.f192b;
            i5++;
        }
        q qVar = this.t;
        int i6 = 0;
        while (qVar != null) {
            i4 += qVar.d();
            qVar = (q) qVar.f204b;
            i6++;
        }
        if (this.q != null) {
            i2 = 1;
            i4 += this.q.f184b + 8;
            a("BootstrapMethods");
        } else {
            i2 = 0;
        }
        if (this.z != 0) {
            i2++;
            i4 += 8;
            a("Signature");
        }
        if (this.D != 0) {
            i2++;
            i4 += 8;
            a("SourceFile");
        }
        if (this.E != null) {
            i2++;
            i4 += this.E.f184b + 4;
            a("SourceDebugExtension");
        }
        if (this.F != 0) {
            i2++;
            i4 += 10;
            a("EnclosingMethod");
        }
        if ((this.x & 131072) != 0) {
            i2++;
            i4 += 6;
            a("Deprecated");
        }
        if ((this.x & 4096) != 0 && ((this.e & 65535) < 49 || (this.x & 262144) != 0)) {
            i2++;
            i4 += 6;
            a("Synthetic");
        }
        if (this.L != null) {
            i2++;
            i4 += this.L.f184b + 8;
            a("InnerClasses");
        }
        if (this.H != null) {
            i2++;
            i4 += this.H.b() + 8;
            a("RuntimeVisibleAnnotations");
        }
        if (this.I != null) {
            i2++;
            i4 += this.I.b() + 8;
            a("RuntimeInvisibleAnnotations");
        }
        int i7 = i4;
        if (this.J != null) {
            int c2 = i2 + this.J.c();
            i7 += this.J.b(this, null, 0, -1, -1);
            i3 = c2;
        } else {
            i3 = i2;
        }
        d dVar = new d(this.g.f184b + i7);
        dVar.c(-889275714).c(this.e);
        dVar.b(this.f).a(this.g.f183a, 0, this.g.f184b);
        dVar.b(((393216 | ((this.x & 262144) / 64)) ^ -1) & this.x).b(this.y).b(this.A);
        dVar.b(this.B);
        for (int i8 = 0; i8 < this.B; i8++) {
            dVar.b(this.C[i8]);
        }
        dVar.b(i5);
        for (j jVar2 = this.r; jVar2 != null; jVar2 = (j) jVar2.f192b) {
            jVar2.a(dVar);
        }
        dVar.b(i6);
        for (q qVar2 = this.t; qVar2 != null; qVar2 = (q) qVar2.f204b) {
            qVar2.a(dVar);
        }
        dVar.b(i3);
        if (this.q != null) {
            dVar.b(a("BootstrapMethods"));
            dVar.c(this.q.f184b + 2).b(this.p);
            dVar.a(this.q.f183a, 0, this.q.f184b);
        }
        if (this.z != 0) {
            dVar.b(a("Signature")).c(2).b(this.z);
        }
        if (this.D != 0) {
            dVar.b(a("SourceFile")).c(2).b(this.D);
        }
        if (this.E != null) {
            int i9 = this.E.f184b - 2;
            dVar.b(a("SourceDebugExtension")).c(i9);
            dVar.a(this.E.f183a, 2, i9);
        }
        if (this.F != 0) {
            dVar.b(a("EnclosingMethod")).c(4);
            dVar.b(this.F).b(this.G);
        }
        if ((this.x & 131072) != 0) {
            dVar.b(a("Deprecated")).c(0);
        }
        if ((this.x & 4096) != 0 && ((this.e & 65535) < 49 || (this.x & 262144) != 0)) {
            dVar.b(a("Synthetic")).c(0);
        }
        if (this.L != null) {
            dVar.b(a("InnerClasses"));
            dVar.c(this.L.f184b + 2).b(this.K);
            dVar.a(this.L.f183a, 0, this.L.f184b);
        }
        if (this.H != null) {
            dVar.b(a("RuntimeVisibleAnnotations"));
            this.H.a(dVar);
        }
        if (this.I != null) {
            dVar.b(a("RuntimeInvisibleAnnotations"));
            this.I.a(dVar);
        }
        if (this.J != null) {
            this.J.a(this, (byte[]) null, 0, -1, -1, dVar);
        }
        if (!this.v) {
            return dVar.f183a;
        }
        g gVar = new g(2);
        new e(dVar.f183a).a(gVar, 4);
        return gVar.b();
    }

    public int c(String str) {
        return b(str).f199a;
    }

    public int c(String str, String str2, String str3) {
        return b(str, str2, str3).f199a;
    }

    /* access modifiers changed from: package-private */
    public n c(String str, String str2) {
        this.k.a(12, str, str2, null);
        n b2 = b(this.k);
        if (b2 != null) {
            return b2;
        }
        a(12, a(str), a(str2));
        int i2 = this.f;
        this.f = i2 + 1;
        n nVar = new n(i2, this.k);
        c(nVar);
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public n d(String str) {
        this.k.a(16, str, null, null);
        n b2 = b(this.k);
        if (b2 != null) {
            return b2;
        }
        this.g.b(16, a(str));
        int i2 = this.f;
        this.f = i2 + 1;
        n nVar = new n(i2, this.k);
        c(nVar);
        return nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: protected */
    public String d(String str, String str2) {
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            Class<?> cls = Class.forName(str.replace('/', '.'), false, classLoader);
            Class<?> cls2 = Class.forName(str2.replace('/', '.'), false, classLoader);
            if (cls.isAssignableFrom(cls2)) {
                return str;
            }
            if (cls2.isAssignableFrom(cls)) {
                return str2;
            }
            if (cls.isInterface() || cls2.isInterface()) {
                return "java/lang/Object";
            }
            do {
                cls = cls.getSuperclass();
            } while (!cls.isAssignableFrom(cls2));
            return cls.getName().replace('.', '/');
        } catch (Exception e2) {
            throw new RuntimeException(e2.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public int e(String str) {
        this.j.a(30, str, null, null);
        n b2 = b(this.j);
        if (b2 == null) {
            b2 = a(this.j);
        }
        return b2.f199a;
    }
}
