package b.a.a;

public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    protected final int f191a;

    /* renamed from: b  reason: collision with root package name */
    protected i f192b;

    public i(int i) {
        this(i, null);
    }

    public i(int i, i iVar) {
        this.f191a = i;
        this.f192b = iVar;
    }

    public a a(String str, boolean z) {
        if (this.f192b != null) {
            return this.f192b.a(str, z);
        }
        return null;
    }

    public void a() {
        if (this.f192b != null) {
            this.f192b.a();
        }
    }

    public void a(c cVar) {
        if (this.f192b != null) {
            this.f192b.a(cVar);
        }
    }
}
