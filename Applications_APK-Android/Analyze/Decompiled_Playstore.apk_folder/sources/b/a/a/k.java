package b.a.a;

final class k {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f193a;

    /* renamed from: b  reason: collision with root package name */
    o f194b;
    int[] c;
    int[] d;
    private int[] e;
    private int[] f;
    private int g;
    private int h;
    private int[] i;

    static {
        int[] iArr = new int[202];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = "EFFFFFFFFGGFFFGGFFFEEFGFGFEEEEEEEEEEEEEEEEEEEEDEDEDDDDDCDCDEEEEEEEEEEEEEEEEEEEEBABABBBBDCFFFGGGEDCDCDCDCDCDCDCDCDCDCEEEEDDDDDDDCDCDCEFEFDDEEFFDEDEEEBDDBBDDDDDDCCCCCCCCEFEDDDCDCDEEEEEEEEEEFEEEEEEDDEEDDEE".charAt(i2) - 'E';
        }
        f193a = iArr;
    }

    k() {
    }

    private int a() {
        if (this.g > 0) {
            int[] iArr = this.f;
            int i2 = this.g - 1;
            this.g = i2;
            return iArr[i2];
        }
        o oVar = this.f194b;
        int i3 = oVar.d - 1;
        oVar.d = i3;
        return 50331648 | (-i3);
    }

    private int a(int i2) {
        if (this.e == null || i2 >= this.e.length) {
            return 33554432 | i2;
        }
        int i3 = this.e[i2];
        if (i3 != 0) {
            return i3;
        }
        int i4 = 33554432 | i2;
        this.e[i2] = i4;
        return i4;
    }

    private int a(g gVar, int i2) {
        int e2;
        if (i2 == 16777222) {
            e2 = gVar.e(gVar.o) | 24117248;
        } else if ((-1048576 & i2) != 25165824) {
            return i2;
        } else {
            e2 = gVar.e(gVar.n[1048575 & i2].e) | 24117248;
        }
        for (int i3 = 0; i3 < this.h; i3++) {
            int i4 = this.i[i3];
            int i5 = -268435456 & i4;
            int i6 = 251658240 & i4;
            if (i6 == 33554432) {
                i4 = this.c[i4 & 8388607] + i5;
            } else if (i6 == 50331648) {
                i4 = this.d[this.d.length - (i4 & 8388607)] + i5;
            }
            if (i2 == i4) {
                return e2;
            }
        }
        return i2;
    }

    private void a(int i2, int i3) {
        if (this.e == null) {
            this.e = new int[10];
        }
        int length = this.e.length;
        if (i2 >= length) {
            int[] iArr = new int[Math.max(i2 + 1, length * 2)];
            System.arraycopy(this.e, 0, iArr, 0, length);
            this.e = iArr;
        }
        this.e[i2] = i3;
    }

    private void a(g gVar, String str) {
        int b2 = b(gVar, str);
        if (b2 != 0) {
            b(b2);
            if (b2 == 16777220 || b2 == 16777219) {
                b(16777216);
            }
        }
    }

    private void a(String str) {
        char charAt = str.charAt(0);
        if (charAt == '(') {
            c((s.e(str) >> 2) - 1);
        } else if (charAt == 'J' || charAt == 'D') {
            c(2);
        } else {
            c(1);
        }
    }

    private static boolean a(g gVar, int i2, int[] iArr, int i3) {
        int i4;
        int i5 = iArr[i3];
        if (i5 == i2) {
            return false;
        }
        if ((268435455 & i2) != 16777221) {
            i4 = i2;
        } else if (i5 == 16777221) {
            return false;
        } else {
            i4 = 16777221;
        }
        if (i5 == 0) {
            iArr[i3] = i4;
            return true;
        }
        if ((i5 & 267386880) == 24117248 || (i5 & -268435456) != 0) {
            if (i4 == 16777221) {
                return false;
            }
            i4 = (-1048576 & i4) == (-1048576 & i5) ? (i5 & 267386880) == 24117248 ? gVar.a(i4 & 1048575, 1048575 & i5) | (i4 & -268435456) | 24117248 : gVar.e("java/lang/Object") | 24117248 : ((i4 & 267386880) == 24117248 || (i4 & -268435456) != 0) ? gVar.e("java/lang/Object") | 24117248 : 16777216;
        } else if (i5 != 16777221) {
            i4 = 16777216;
        } else if ((i4 & 267386880) != 24117248 && (i4 & -268435456) == 0) {
            i4 = 16777216;
        }
        if (i5 == i4) {
            return false;
        }
        iArr[i3] = i4;
        return true;
    }

    private static int b(g gVar, String str) {
        int i2 = 16777217;
        int indexOf = str.charAt(0) == '(' ? str.indexOf(41) + 1 : 0;
        switch (str.charAt(indexOf)) {
            case 'B':
            case 'C':
            case 'I':
            case 'S':
            case 'Z':
                return 16777217;
            case 'D':
                return 16777219;
            case 'F':
                return 16777218;
            case 'J':
                return 16777220;
            case 'L':
                return 24117248 | gVar.e(str.substring(indexOf + 1, str.length() - 1));
            case 'V':
                return 0;
            default:
                int i3 = indexOf + 1;
                while (str.charAt(i3) == '[') {
                    i3++;
                }
                switch (str.charAt(i3)) {
                    case 'B':
                        i2 = 16777226;
                        break;
                    case 'C':
                        i2 = 16777227;
                        break;
                    case 'D':
                        i2 = 16777219;
                        break;
                    case 'F':
                        i2 = 16777218;
                        break;
                    case 'I':
                        break;
                    case 'J':
                        i2 = 16777220;
                        break;
                    case 'S':
                        i2 = 16777228;
                        break;
                    case 'Z':
                        i2 = 16777225;
                        break;
                    default:
                        i2 = gVar.e(str.substring(i3 + 1, str.length() - 1)) | 24117248;
                        break;
                }
                return ((i3 - indexOf) << 28) | i2;
        }
    }

    private void b(int i2) {
        if (this.f == null) {
            this.f = new int[10];
        }
        int length = this.f.length;
        if (this.g >= length) {
            int[] iArr = new int[Math.max(this.g + 1, length * 2)];
            System.arraycopy(this.f, 0, iArr, 0, length);
            this.f = iArr;
        }
        int[] iArr2 = this.f;
        int i3 = this.g;
        this.g = i3 + 1;
        iArr2[i3] = i2;
        int i4 = this.f194b.d + this.g;
        if (i4 > this.f194b.e) {
            this.f194b.e = i4;
        }
    }

    private void c(int i2) {
        if (this.g >= i2) {
            this.g -= i2;
            return;
        }
        this.f194b.d -= i2 - this.g;
        this.g = 0;
    }

    private void d(int i2) {
        if (this.i == null) {
            this.i = new int[2];
        }
        int length = this.i.length;
        if (this.h >= length) {
            int[] iArr = new int[Math.max(this.h + 1, length * 2)];
            System.arraycopy(this.i, 0, iArr, 0, length);
            this.i = iArr;
        }
        int[] iArr2 = this.i;
        int i3 = this.h;
        this.h = i3 + 1;
        iArr2[i3] = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, g gVar, n nVar) {
        switch (i2) {
            case 0:
            case 116:
            case 117:
            case 118:
            case 119:
            case 145:
            case 146:
            case 147:
            case 167:
            case 177:
                return;
            case 1:
                b(16777221);
                return;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 16:
            case 17:
            case 21:
                b(16777217);
                return;
            case 9:
            case 10:
            case 22:
                b(16777220);
                b(16777216);
                return;
            case 11:
            case 12:
            case 13:
            case 23:
                b(16777218);
                return;
            case 14:
            case 15:
            case 24:
                b(16777219);
                b(16777216);
                return;
            case 18:
                switch (nVar.f200b) {
                    case 3:
                        b(16777217);
                        return;
                    case 4:
                        b(16777218);
                        return;
                    case 5:
                        b(16777220);
                        b(16777216);
                        return;
                    case 6:
                        b(16777219);
                        b(16777216);
                        return;
                    case 7:
                        b(24117248 | gVar.e("java/lang/Class"));
                        return;
                    case 8:
                        b(24117248 | gVar.e("java/lang/String"));
                        return;
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    default:
                        b(24117248 | gVar.e("java/lang/invoke/MethodHandle"));
                        return;
                    case 16:
                        b(24117248 | gVar.e("java/lang/invoke/MethodType"));
                        return;
                }
            case 19:
            case 20:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 196:
            case 197:
            default:
                c(i3);
                a(gVar, nVar.e);
                return;
            case 25:
                b(a(i3));
                return;
            case 46:
            case 51:
            case 52:
            case 53:
                c(2);
                b(16777217);
                return;
            case 47:
            case 143:
                c(2);
                b(16777220);
                b(16777216);
                return;
            case 48:
                c(2);
                b(16777218);
                return;
            case 49:
            case 138:
                c(2);
                b(16777219);
                b(16777216);
                return;
            case 50:
                c(1);
                b(a() - 268435456);
                return;
            case 54:
            case 56:
            case 58:
                a(i3, a());
                if (i3 > 0) {
                    int a2 = a(i3 - 1);
                    if (a2 == 16777220 || a2 == 16777219) {
                        a(i3 - 1, 16777216);
                        return;
                    } else if ((251658240 & a2) != 16777216) {
                        a(i3 - 1, a2 | 8388608);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 55:
            case 57:
                c(1);
                a(i3, a());
                a(i3 + 1, 16777216);
                if (i3 > 0) {
                    int a3 = a(i3 - 1);
                    if (a3 == 16777220 || a3 == 16777219) {
                        a(i3 - 1, 16777216);
                        return;
                    } else if ((251658240 & a3) != 16777216) {
                        a(i3 - 1, a3 | 8388608);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 79:
            case 81:
            case 83:
            case 84:
            case 85:
            case 86:
                c(3);
                return;
            case 80:
            case 82:
                c(4);
                return;
            case 87:
            case 153:
            case 154:
            case 155:
            case 156:
            case 157:
            case 158:
            case 170:
            case 171:
            case 172:
            case 174:
            case 176:
            case 191:
            case 194:
            case 195:
            case 198:
            case 199:
                c(1);
                return;
            case 88:
            case 159:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 173:
            case 175:
                c(2);
                return;
            case 89:
                int a4 = a();
                b(a4);
                b(a4);
                return;
            case 90:
                int a5 = a();
                int a6 = a();
                b(a5);
                b(a6);
                b(a5);
                return;
            case 91:
                int a7 = a();
                int a8 = a();
                int a9 = a();
                b(a7);
                b(a9);
                b(a8);
                b(a7);
                return;
            case 92:
                int a10 = a();
                int a11 = a();
                b(a11);
                b(a10);
                b(a11);
                b(a10);
                return;
            case 93:
                int a12 = a();
                int a13 = a();
                int a14 = a();
                b(a13);
                b(a12);
                b(a14);
                b(a13);
                b(a12);
                return;
            case 94:
                int a15 = a();
                int a16 = a();
                int a17 = a();
                int a18 = a();
                b(a16);
                b(a15);
                b(a18);
                b(a17);
                b(a16);
                b(a15);
                return;
            case 95:
                int a19 = a();
                int a20 = a();
                b(a19);
                b(a20);
                return;
            case 96:
            case 100:
            case 104:
            case 108:
            case 112:
            case 120:
            case 122:
            case 124:
            case 126:
            case 128:
            case 130:
            case 136:
            case 142:
            case 149:
            case 150:
                c(2);
                b(16777217);
                return;
            case 97:
            case 101:
            case 105:
            case 109:
            case 113:
            case 127:
            case 129:
            case 131:
                c(4);
                b(16777220);
                b(16777216);
                return;
            case 98:
            case 102:
            case 106:
            case 110:
            case 114:
            case 137:
            case 144:
                c(2);
                b(16777218);
                return;
            case 99:
            case 103:
            case 107:
            case 111:
            case 115:
                c(4);
                b(16777219);
                b(16777216);
                return;
            case 121:
            case 123:
            case 125:
                c(3);
                b(16777220);
                b(16777216);
                return;
            case 132:
                a(i3, 16777217);
                return;
            case 133:
            case 140:
                c(1);
                b(16777220);
                b(16777216);
                return;
            case 134:
                c(1);
                b(16777218);
                return;
            case 135:
            case 141:
                c(1);
                b(16777219);
                b(16777216);
                return;
            case 139:
            case 190:
            case 193:
                c(1);
                b(16777217);
                return;
            case 148:
            case 151:
            case 152:
                c(4);
                b(16777217);
                return;
            case 168:
            case 169:
                throw new RuntimeException("JSR/RET are not supported with computeFrames option");
            case 178:
                a(gVar, nVar.g);
                return;
            case 179:
                a(nVar.g);
                return;
            case 180:
                c(1);
                a(gVar, nVar.g);
                return;
            case 181:
                a(nVar.g);
                a();
                return;
            case 182:
            case 183:
            case 184:
            case 185:
                a(nVar.g);
                if (i2 != 184) {
                    int a21 = a();
                    if (i2 == 183 && nVar.f.charAt(0) == '<') {
                        d(a21);
                    }
                }
                a(gVar, nVar.g);
                return;
            case 186:
                a(nVar.f);
                a(gVar, nVar.f);
                return;
            case 187:
                b(25165824 | gVar.a(nVar.e, i3));
                return;
            case 188:
                a();
                switch (i3) {
                    case 4:
                        b(285212681);
                        return;
                    case 5:
                        b(285212683);
                        return;
                    case 6:
                        b(285212674);
                        return;
                    case 7:
                        b(285212675);
                        return;
                    case 8:
                        b(285212682);
                        return;
                    case 9:
                        b(285212684);
                        return;
                    case 10:
                        b(285212673);
                        return;
                    default:
                        b(285212676);
                        return;
                }
            case 189:
                String str = nVar.e;
                a();
                if (str.charAt(0) == '[') {
                    a(gVar, new StringBuffer().append('[').append(str).toString());
                    return;
                } else {
                    b(gVar.e(str) | 292552704);
                    return;
                }
            case 192:
                String str2 = nVar.e;
                a();
                if (str2.charAt(0) == '[') {
                    a(gVar, str2);
                    return;
                } else {
                    b(gVar.e(str2) | 24117248);
                    return;
                }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(g gVar, int i2, s[] sVarArr, int i3) {
        int i4;
        int i5 = 1;
        this.c = new int[i3];
        this.d = new int[0];
        if ((i2 & 8) != 0) {
            i5 = 0;
        } else if ((262144 & i2) == 0) {
            this.c[0] = 24117248 | gVar.e(gVar.o);
        } else {
            this.c[0] = 16777222;
        }
        for (s f2 : sVarArr) {
            int b2 = b(gVar, f2.f());
            int i6 = i4 + 1;
            this.c[i4] = b2;
            if (b2 == 16777220 || b2 == 16777219) {
                i4 = i6 + 1;
                this.c[i6] = 16777216;
            } else {
                i4 = i6;
            }
        }
        while (i4 < i3) {
            this.c[i4] = 16777216;
            i4++;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(g gVar, k kVar, int i2) {
        boolean z;
        int i3;
        boolean z2 = false;
        int length = this.c.length;
        int length2 = this.d.length;
        if (kVar.c == null) {
            kVar.c = new int[length];
            z2 = true;
        }
        boolean z3 = z2;
        for (int i4 = 0; i4 < length; i4++) {
            if (this.e == null || i4 >= this.e.length) {
                i3 = this.c[i4];
            } else {
                i3 = this.e[i4];
                if (i3 == 0) {
                    i3 = this.c[i4];
                } else {
                    int i5 = -268435456 & i3;
                    int i6 = 251658240 & i3;
                    if (i6 != 16777216) {
                        int i7 = i6 == 33554432 ? i5 + this.c[8388607 & i3] : i5 + this.d[length2 - (8388607 & i3)];
                        i3 = ((i3 & 8388608) == 0 || !(i7 == 16777220 || i7 == 16777219)) ? i7 : 16777216;
                    }
                }
            }
            if (this.i != null) {
                i3 = a(gVar, i3);
            }
            z3 |= a(gVar, i3, kVar.c, i4);
        }
        if (i2 > 0) {
            int i8 = 0;
            boolean z4 = z3;
            while (i8 < length) {
                boolean a2 = a(gVar, this.c[i8], kVar.c, i8) | z4;
                i8++;
                z4 = a2;
            }
            if (kVar.d == null) {
                kVar.d = new int[1];
                z4 = true;
            }
            return a(gVar, i2, kVar.d, 0) | z4;
        }
        int length3 = this.d.length + this.f194b.d;
        if (kVar.d == null) {
            kVar.d = new int[(this.g + length3)];
            z = true;
        } else {
            z = z3;
        }
        boolean z5 = z;
        for (int i9 = 0; i9 < length3; i9++) {
            int i10 = this.d[i9];
            if (this.i != null) {
                i10 = a(gVar, i10);
            }
            z5 |= a(gVar, i10, kVar.d, i9);
        }
        for (int i11 = 0; i11 < this.g; i11++) {
            int i12 = this.f[i11];
            int i13 = -268435456 & i12;
            int i14 = 251658240 & i12;
            if (i14 != 16777216) {
                int i15 = i14 == 33554432 ? i13 + this.c[8388607 & i12] : i13 + this.d[length2 - (8388607 & i12)];
                i12 = ((i12 & 8388608) == 0 || !(i15 == 16777220 || i15 == 16777219)) ? i15 : 16777216;
            }
            if (this.i != null) {
                i12 = a(gVar, i12);
            }
            z5 |= a(gVar, i12, kVar.d, length3 + i11);
        }
        return z5;
    }
}
