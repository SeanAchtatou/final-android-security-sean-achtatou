package b.a.a;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f181a;

    /* renamed from: b  reason: collision with root package name */
    byte[] f182b;
    c c;

    protected c(String str) {
        this.f181a = str;
    }

    /* access modifiers changed from: protected */
    public c a(e eVar, int i, int i2, char[] cArr, int i3, o[] oVarArr) {
        c cVar = new c(this.f181a);
        cVar.f182b = new byte[i2];
        System.arraycopy(eVar.f185a, i, cVar.f182b, 0, i2);
        return cVar;
    }

    /* access modifiers changed from: protected */
    public d a(g gVar, byte[] bArr, int i, int i2, int i3) {
        d dVar = new d();
        dVar.f183a = this.f182b;
        dVar.f184b = this.f182b.length;
        return dVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(g gVar, byte[] bArr, int i, int i2, int i3, d dVar) {
        for (c cVar = this; cVar != null; cVar = cVar.c) {
            d a2 = cVar.a(gVar, bArr, i, i2, i3);
            dVar.b(gVar.a(cVar.f181a)).c(a2.f184b);
            dVar.a(a2.f183a, 0, a2.f184b);
        }
    }

    public boolean a() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final int b(g gVar, byte[] bArr, int i, int i2, int i3) {
        int i4 = 0;
        c cVar = this;
        while (cVar != null) {
            gVar.a(cVar.f181a);
            cVar = cVar.c;
            i4 = cVar.a(gVar, bArr, i, i2, i3).f184b + 6 + i4;
        }
        return i4;
    }

    /* access modifiers changed from: protected */
    public o[] b() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        int i = 0;
        while (this != null) {
            i++;
            this = this.c;
        }
        return i;
    }
}
