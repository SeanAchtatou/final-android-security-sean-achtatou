package b.a.a;

public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    protected final int f187a;

    /* renamed from: b  reason: collision with root package name */
    protected f f188b;

    public f(int i) {
        this(i, null);
    }

    public f(int i, f fVar) {
        this.f187a = i;
        this.f188b = fVar;
    }

    public a a(String str, boolean z) {
        if (this.f188b != null) {
            return this.f188b.a(str, z);
        }
        return null;
    }

    public i a(int i, String str, String str2, String str3, Object obj) {
        if (this.f188b != null) {
            return this.f188b.a(i, str, str2, str3, obj);
        }
        return null;
    }

    public p a(int i, String str, String str2, String str3, String[] strArr) {
        if (this.f188b != null) {
            return this.f188b.a(i, str, str2, str3, strArr);
        }
        return null;
    }

    public void a() {
        if (this.f188b != null) {
            this.f188b.a();
        }
    }

    public void a(int i, int i2, String str, String str2, String str3, String[] strArr) {
        if (this.f188b != null) {
            this.f188b.a(i, i2, str, str2, str3, strArr);
        }
    }

    public void a(c cVar) {
        if (this.f188b != null) {
            this.f188b.a(cVar);
        }
    }

    public void a(String str, String str2) {
        if (this.f188b != null) {
            this.f188b.a(str, str2);
        }
    }

    public void a(String str, String str2, String str3) {
        if (this.f188b != null) {
            this.f188b.a(str, str2, str3);
        }
    }

    public void a(String str, String str2, String str3, int i) {
        if (this.f188b != null) {
            this.f188b.a(str, str2, str3, i);
        }
    }
}
