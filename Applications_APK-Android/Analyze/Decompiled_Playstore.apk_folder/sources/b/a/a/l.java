package b.a.a;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    final int f195a;

    /* renamed from: b  reason: collision with root package name */
    final String f196b;
    final String c;
    final String d;

    public l(int i, String str, String str2, String str3) {
        this.f195a = i;
        this.f196b = str;
        this.c = str2;
        this.d = str3;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        return this.f195a == lVar.f195a && this.f196b.equals(lVar.f196b) && this.c.equals(lVar.c) && this.d.equals(lVar.d);
    }

    public int hashCode() {
        return this.f195a + (this.f196b.hashCode() * this.c.hashCode() * this.d.hashCode());
    }

    public String toString() {
        return new StringBuffer().append(this.f196b).append('.').append(this.c).append(this.d).append(" (").append(this.f195a).append(')').toString();
    }
}
