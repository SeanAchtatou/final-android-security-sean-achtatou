package android.support.v4.a;

import android.support.v4.c.d;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class c {

    /* renamed from: a  reason: collision with root package name */
    int f5a;

    /* renamed from: b  reason: collision with root package name */
    d f6b;
    boolean c;
    boolean d;
    boolean e;
    boolean f;
    boolean g;

    public String a(Object obj) {
        StringBuilder sb = new StringBuilder(64);
        d.a(obj, sb);
        sb.append("}");
        return sb.toString();
    }

    public final void a() {
        this.c = true;
        this.e = false;
        this.d = false;
        b();
    }

    public void a(int i, d dVar) {
        if (this.f6b != null) {
            throw new IllegalStateException("There is already a listener registered");
        }
        this.f6b = dVar;
        this.f5a = i;
    }

    public void a(d dVar) {
        if (this.f6b == null) {
            throw new IllegalStateException("No listener register");
        } else if (this.f6b != dVar) {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        } else {
            this.f6b = null;
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.f5a);
        printWriter.print(" mListener=");
        printWriter.println(this.f6b);
        if (this.c || this.f || this.g) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.c);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.f);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.g);
        }
        if (this.d || this.e) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.d);
            printWriter.print(" mReset=");
            printWriter.println(this.e);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public void c() {
        this.c = false;
        d();
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    public void e() {
        f();
        this.e = true;
        this.c = false;
        this.d = false;
        this.f = false;
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        d.a(this, sb);
        sb.append(" id=");
        sb.append(this.f5a);
        sb.append("}");
        return sb.toString();
    }
}
