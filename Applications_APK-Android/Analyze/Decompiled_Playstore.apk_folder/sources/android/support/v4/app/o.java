package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.c.l;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class o extends Activity {

    /* renamed from: a  reason: collision with root package name */
    final Handler f40a;

    /* renamed from: b  reason: collision with root package name */
    final t f41b;
    final q c;
    boolean d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    l l;
    ap m;

    private static String a(View view) {
        String str;
        char c2 = 'F';
        char c3 = '.';
        StringBuilder sb = new StringBuilder(128);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        sb.append(view.isFocusable() ? 'F' : '.');
        sb.append(view.isEnabled() ? 'E' : '.');
        sb.append(view.willNotDraw() ? '.' : 'D');
        sb.append(view.isHorizontalScrollBarEnabled() ? 'H' : '.');
        sb.append(view.isVerticalScrollBarEnabled() ? 'V' : '.');
        sb.append(view.isClickable() ? 'C' : '.');
        sb.append(view.isLongClickable() ? 'L' : '.');
        sb.append(' ');
        if (!view.isFocused()) {
            c2 = '.';
        }
        sb.append(c2);
        sb.append(view.isSelected() ? 'S' : '.');
        if (view.isPressed()) {
            c3 = 'P';
        }
        sb.append(c3);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e2) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void a(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(a(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i2 = 0; i2 < childCount; i2++) {
                a(str2, printWriter, viewGroup.getChildAt(i2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ap a(String str, boolean z, boolean z2) {
        if (this.l == null) {
            this.l = new l();
        }
        ap apVar = (ap) this.l.get(str);
        if (apVar != null) {
            apVar.a(this);
            return apVar;
        } else if (!z2) {
            return apVar;
        } else {
            ap apVar2 = new ap(str, this, z);
            this.l.put(str, apVar2);
            return apVar2;
        }
    }

    public void a() {
        a.a(this);
    }

    public void a(l lVar) {
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        ap apVar;
        if (this.l != null && (apVar = (ap) this.l.get(str)) != null && !apVar.g) {
            apVar.h();
            this.l.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (!this.g) {
            this.g = true;
            this.h = z;
            this.f40a.removeMessages(1);
            e();
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f41b.m();
    }

    public Object c() {
        return null;
    }

    public void d() {
        if (Build.VERSION.SDK_INT >= 11) {
            c.a(this);
        } else {
            this.i = true;
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.d);
        printWriter.print("mResumed=");
        printWriter.print(this.e);
        printWriter.print(" mStopped=");
        printWriter.print(this.f);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.g);
        printWriter.print(str2);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.k);
        if (this.m != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.m)));
            printWriter.println(":");
            this.m.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
        this.f41b.a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        a(str + "  ", printWriter, getWindow().getDecorView());
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.k) {
            this.k = false;
            if (this.m != null) {
                if (!this.h) {
                    this.m.c();
                } else {
                    this.m.d();
                }
            }
        }
        this.f41b.p();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        this.f41b.i();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            if (this.f41b.f == null || i5 < 0 || i5 >= this.f41b.f.size()) {
                Log.w("FragmentActivity", "Activity result fragment index out of range: 0x" + Integer.toHexString(i2));
                return;
            }
            l lVar = (l) this.f41b.f.get(i5);
            if (lVar == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for index: 0x" + Integer.toHexString(i2));
            } else {
                lVar.a(65535 & i2, i3, intent);
            }
        } else {
            super.onActivityResult(i2, i3, intent);
        }
    }

    public void onBackPressed() {
        if (!this.f41b.c()) {
            a();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.f41b.a(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.f41b.a(this, this.c, (l) null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
        p pVar = (p) getLastNonConfigurationInstance();
        if (pVar != null) {
            this.l = pVar.e;
        }
        if (bundle != null) {
            this.f41b.a(bundle.getParcelable("android:support:fragments"), pVar != null ? pVar.d : null);
        }
        this.f41b.j();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu) | this.f41b.a(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        if (!"fragment".equals(str)) {
            return super.onCreateView(str, context, attributeSet);
        }
        View onCreateView = this.f41b.onCreateView(str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(str, context, attributeSet) : onCreateView;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(false);
        this.f41b.r();
        if (this.m != null) {
            this.m.h();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 5 || i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.f41b.s();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.f41b.a(menuItem);
            case 6:
                return this.f41b.b(menuItem);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.f41b.i();
    }

    public void onPanelClosed(int i2, Menu menu) {
        switch (i2) {
            case 0:
                this.f41b.b(menu);
                break;
        }
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e = false;
        if (this.f40a.hasMessages(2)) {
            this.f40a.removeMessages(2);
            b();
        }
        this.f41b.n();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.f40a.removeMessages(2);
        b();
        this.f41b.e();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        if (i2 != 0 || menu == null) {
            return super.onPreparePanel(i2, view, menu);
        }
        if (this.i) {
            this.i = false;
            menu.clear();
            onCreatePanelMenu(i2, menu);
        }
        return a(view, menu) | this.f41b.a(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.f40a.sendEmptyMessage(2);
        this.e = true;
        this.f41b.e();
    }

    public final Object onRetainNonConfigurationInstance() {
        boolean z;
        if (this.f) {
            a(true);
        }
        Object c2 = c();
        ArrayList g2 = this.f41b.g();
        if (this.l != null) {
            int size = this.l.size();
            ap[] apVarArr = new ap[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                apVarArr[i2] = (ap) this.l.c(i2);
            }
            z = false;
            for (int i3 = 0; i3 < size; i3++) {
                ap apVar = apVarArr[i3];
                if (apVar.g) {
                    z = true;
                } else {
                    apVar.h();
                    this.l.remove(apVar.d);
                }
            }
        } else {
            z = false;
        }
        if (g2 == null && !z && c2 == null) {
            return null;
        }
        p pVar = new p();
        pVar.f42a = null;
        pVar.f43b = c2;
        pVar.c = null;
        pVar.d = g2;
        pVar.e = this.l;
        return pVar;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable h2 = this.f41b.h();
        if (h2 != null) {
            bundle.putParcelable("android:support:fragments", h2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.o.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap */
    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f = false;
        this.g = false;
        this.f40a.removeMessages(1);
        if (!this.d) {
            this.d = true;
            this.f41b.k();
        }
        this.f41b.i();
        this.f41b.e();
        if (!this.k) {
            this.k = true;
            if (this.m != null) {
                this.m.b();
            } else if (!this.j) {
                this.m = a("(root)", this.k, false);
                if (this.m != null && !this.m.f) {
                    this.m.b();
                }
            }
            this.j = true;
        }
        this.f41b.l();
        if (this.l != null) {
            int size = this.l.size();
            ap[] apVarArr = new ap[size];
            for (int i2 = size - 1; i2 >= 0; i2--) {
                apVarArr[i2] = (ap) this.l.c(i2);
            }
            for (int i3 = 0; i3 < size; i3++) {
                ap apVar = apVarArr[i3];
                apVar.e();
                apVar.g();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f = true;
        this.f40a.sendEmptyMessage(1);
        this.f41b.o();
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (i2 == -1 || (-65536 & i2) == 0) {
            super.startActivityForResult(intent, i2);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }
}
