package android.support.v4.app;

import android.view.View;

class m implements q {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f39a;

    m(l lVar) {
        this.f39a = lVar;
    }

    public View a(int i) {
        if (this.f39a.J != null) {
            return this.f39a.J.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }

    public boolean a() {
        return this.f39a.J != null;
    }
}
