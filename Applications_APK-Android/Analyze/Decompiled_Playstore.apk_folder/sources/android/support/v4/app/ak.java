package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class ak implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f16a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Transition f17b;
    final /* synthetic */ View c;
    final /* synthetic */ ArrayList d;
    final /* synthetic */ Transition e;
    final /* synthetic */ ArrayList f;
    final /* synthetic */ Transition g;
    final /* synthetic */ ArrayList h;
    final /* synthetic */ Map i;
    final /* synthetic */ ArrayList j;
    final /* synthetic */ Transition k;

    ak(View view, Transition transition, View view2, ArrayList arrayList, Transition transition2, ArrayList arrayList2, Transition transition3, ArrayList arrayList3, Map map, ArrayList arrayList4, Transition transition4) {
        this.f16a = view;
        this.f17b = transition;
        this.c = view2;
        this.d = arrayList;
        this.e = transition2;
        this.f = arrayList2;
        this.g = transition3;
        this.h = arrayList3;
        this.i = map;
        this.j = arrayList4;
        this.k = transition4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition}
     arg types: [android.view.View, int]
     candidates:
      ClspMth{android.transition.Transition.excludeTarget(int, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.Class, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.String, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition} */
    public boolean onPreDraw() {
        this.f16a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.f17b != null) {
            this.f17b.removeTarget(this.c);
            ag.a(this.f17b, this.d);
        }
        if (this.e != null) {
            ag.a(this.e, this.f);
        }
        if (this.g != null) {
            ag.a(this.g, this.h);
        }
        for (Map.Entry entry : this.i.entrySet()) {
            ((View) entry.getValue()).setTransitionName((String) entry.getKey());
        }
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k.excludeTarget((View) this.j.get(i2), false);
        }
        this.k.excludeTarget(this.c, false);
        return true;
    }
}
