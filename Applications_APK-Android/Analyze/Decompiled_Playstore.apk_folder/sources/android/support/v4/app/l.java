package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.c.d;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class l implements ComponentCallbacks, View.OnCreateContextMenuListener {

    /* renamed from: a  reason: collision with root package name */
    static final Object f37a = new Object();
    private static final android.support.v4.c.l aa = new android.support.v4.c.l();
    boolean A;
    boolean B;
    boolean C;
    boolean D;
    boolean E;
    boolean F = true;
    boolean G;
    int H;
    ViewGroup I;
    View J;
    View K;
    boolean L;
    boolean M = true;
    ap N;
    boolean O;
    boolean P;
    Object Q = null;
    Object R = f37a;
    Object S = null;
    Object T = f37a;
    Object U = null;
    Object V = f37a;
    Boolean W;
    Boolean X;
    as Y = null;
    as Z = null;

    /* renamed from: b  reason: collision with root package name */
    int f38b = 0;
    View c;
    int d;
    Bundle e;
    SparseArray f;
    int g = -1;
    String h;
    Bundle i;
    l j;
    int k = -1;
    int l;
    boolean m;
    boolean n;
    boolean o;
    boolean p;
    boolean q;
    boolean r;
    int s;
    t t;
    o u;
    t v;
    l w;
    int x;
    int y;
    String z;

    public static l a(Context context, String str) {
        return a(context, str, (Bundle) null);
    }

    public static l a(Context context, String str, Bundle bundle) {
        try {
            Class<?> cls = (Class) aa.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                aa.put(str, cls);
            }
            l lVar = (l) cls.newInstance();
            if (bundle != null) {
                bundle.setClassLoader(lVar.getClass().getClassLoader());
                lVar.i = bundle;
            }
            return lVar;
        } catch (ClassNotFoundException e2) {
            throw new n("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e2);
        } catch (InstantiationException e3) {
            throw new n("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e3);
        } catch (IllegalAccessException e4) {
            throw new n("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e4);
        }
    }

    static boolean b(Context context, String str) {
        try {
            Class<?> cls = (Class) aa.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                aa.put(str, cls);
            }
            return l.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void A() {
        if (this.v != null) {
            this.v.i();
            this.v.e();
        }
        this.G = false;
        i();
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onStart()");
        }
        if (this.v != null) {
            this.v.l();
        }
        if (this.N != null) {
            this.N.g();
        }
    }

    /* access modifiers changed from: package-private */
    public void B() {
        if (this.v != null) {
            this.v.i();
            this.v.e();
        }
        this.G = false;
        j();
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onResume()");
        } else if (this.v != null) {
            this.v.m();
            this.v.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void C() {
        onLowMemory();
        if (this.v != null) {
            this.v.s();
        }
    }

    /* access modifiers changed from: package-private */
    public void D() {
        if (this.v != null) {
            this.v.n();
        }
        this.G = false;
        k();
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    /* access modifiers changed from: package-private */
    public void E() {
        if (this.v != null) {
            this.v.o();
        }
        this.G = false;
        l();
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.o.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap */
    /* access modifiers changed from: package-private */
    public void F() {
        if (this.v != null) {
            this.v.p();
        }
        if (this.O) {
            this.O = false;
            if (!this.P) {
                this.P = true;
                this.N = this.u.a(this.h, this.O, false);
            }
            if (this.N == null) {
                return;
            }
            if (!this.u.h) {
                this.N.c();
            } else {
                this.N.d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void G() {
        if (this.v != null) {
            this.v.q();
        }
        this.G = false;
        m();
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onDestroyView()");
        } else if (this.N != null) {
            this.N.f();
        }
    }

    /* access modifiers changed from: package-private */
    public void H() {
        if (this.v != null) {
            this.v.r();
        }
        this.G = false;
        n();
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onDestroy()");
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    public Animation a(int i2, boolean z2, int i3) {
        return null;
    }

    public void a(int i2, int i3, Intent intent) {
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, l lVar) {
        this.g = i2;
        if (lVar != null) {
            this.h = lVar.h + ":" + this.g;
        } else {
            this.h = "android:fragment:" + this.g;
        }
    }

    public void a(Activity activity) {
        this.G = true;
    }

    public void a(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        this.G = true;
    }

    /* access modifiers changed from: package-private */
    public void a(Configuration configuration) {
        onConfigurationChanged(configuration);
        if (this.v != null) {
            this.v.a(configuration);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Bundle bundle) {
        if (this.f != null) {
            this.K.restoreHierarchyState(this.f);
            this.f = null;
        }
        this.G = false;
        e(bundle);
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onViewStateRestored()");
        }
    }

    public void a(Menu menu) {
    }

    public void a(Menu menu, MenuInflater menuInflater) {
    }

    public void a(View view, Bundle bundle) {
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.x));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.y));
        printWriter.print(" mTag=");
        printWriter.println(this.z);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.f38b);
        printWriter.print(" mIndex=");
        printWriter.print(this.g);
        printWriter.print(" mWho=");
        printWriter.print(this.h);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.s);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.m);
        printWriter.print(" mRemoving=");
        printWriter.print(this.n);
        printWriter.print(" mResumed=");
        printWriter.print(this.o);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.p);
        printWriter.print(" mInLayout=");
        printWriter.println(this.q);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.A);
        printWriter.print(" mDetached=");
        printWriter.print(this.B);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.F);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.E);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.C);
        printWriter.print(" mRetaining=");
        printWriter.print(this.D);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.M);
        if (this.t != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.t);
        }
        if (this.u != null) {
            printWriter.print(str);
            printWriter.print("mActivity=");
            printWriter.println(this.u);
        }
        if (this.w != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.w);
        }
        if (this.i != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.i);
        }
        if (this.e != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.e);
        }
        if (this.f != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.f);
        }
        if (this.j != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(this.j);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.l);
        }
        if (this.H != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            printWriter.println(this.H);
        }
        if (this.I != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.I);
        }
        if (this.J != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.J);
        }
        if (this.K != null) {
            printWriter.print(str);
            printWriter.print("mInnerView=");
            printWriter.println(this.J);
        }
        if (this.c != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(this.c);
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            printWriter.println(this.d);
        }
        if (this.N != null) {
            printWriter.print(str);
            printWriter.println("Loader Manager:");
            this.N.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.v != null) {
            printWriter.print(str);
            printWriter.println("Child " + this.v + ":");
            this.v.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    public void a(boolean z2) {
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.s > 0;
    }

    public boolean a(MenuItem menuItem) {
        return false;
    }

    public final o b() {
        return this.u;
    }

    public LayoutInflater b(Bundle bundle) {
        LayoutInflater cloneInContext = this.u.getLayoutInflater().cloneInContext(this.u);
        d();
        cloneInContext.setFactory(this.v.t());
        return cloneInContext;
    }

    /* access modifiers changed from: package-private */
    public View b(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (this.v != null) {
            this.v.i();
        }
        return a(layoutInflater, viewGroup, bundle);
    }

    public void b(Menu menu) {
    }

    /* access modifiers changed from: package-private */
    public boolean b(Menu menu, MenuInflater menuInflater) {
        boolean z2 = false;
        if (this.A) {
            return false;
        }
        if (this.E && this.F) {
            z2 = true;
            a(menu, menuInflater);
        }
        return this.v != null ? z2 | this.v.a(menu, menuInflater) : z2;
    }

    public boolean b(MenuItem menuItem) {
        return false;
    }

    public final Resources c() {
        if (this.u != null) {
            return this.u.getResources();
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    public void c(Bundle bundle) {
        this.G = true;
    }

    /* access modifiers changed from: package-private */
    public boolean c(Menu menu) {
        boolean z2 = false;
        if (this.A) {
            return false;
        }
        if (this.E && this.F) {
            z2 = true;
            a(menu);
        }
        return this.v != null ? z2 | this.v.a(menu) : z2;
    }

    /* access modifiers changed from: package-private */
    public boolean c(MenuItem menuItem) {
        if (!this.A) {
            if (!this.E || !this.F || !a(menuItem)) {
                return this.v != null && this.v.a(menuItem);
            }
            return true;
        }
    }

    public final r d() {
        if (this.v == null) {
            z();
            if (this.f38b >= 5) {
                this.v.m();
            } else if (this.f38b >= 4) {
                this.v.l();
            } else if (this.f38b >= 2) {
                this.v.k();
            } else if (this.f38b >= 1) {
                this.v.j();
            }
        }
        return this.v;
    }

    public void d(Bundle bundle) {
        this.G = true;
    }

    /* access modifiers changed from: package-private */
    public void d(Menu menu) {
        if (!this.A) {
            if (this.E && this.F) {
                b(menu);
            }
            if (this.v != null) {
                this.v.b(menu);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d(MenuItem menuItem) {
        if (!this.A) {
            if (b(menuItem)) {
                return true;
            }
            return this.v != null && this.v.b(menuItem);
        }
    }

    public void e(Bundle bundle) {
        this.G = true;
    }

    public final boolean e() {
        return this.u != null && this.m;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public void f(Bundle bundle) {
    }

    public final boolean f() {
        return this.B;
    }

    /* access modifiers changed from: package-private */
    public void g(Bundle bundle) {
        Parcelable parcelable;
        if (this.v != null) {
            this.v.i();
        }
        this.G = false;
        c(bundle);
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onCreate()");
        } else if (bundle != null && (parcelable = bundle.getParcelable("android:support:fragments")) != null) {
            if (this.v == null) {
                z();
            }
            this.v.a(parcelable, (ArrayList) null);
            this.v.j();
        }
    }

    public final boolean g() {
        return this.A;
    }

    public View h() {
        return this.J;
    }

    /* access modifiers changed from: package-private */
    public void h(Bundle bundle) {
        if (this.v != null) {
            this.v.i();
        }
        this.G = false;
        d(bundle);
        if (!this.G) {
            throw new at("Fragment " + this + " did not call through to super.onActivityCreated()");
        } else if (this.v != null) {
            this.v.k();
        }
    }

    public final int hashCode() {
        return super.hashCode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.o.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap */
    public void i() {
        this.G = true;
        if (!this.O) {
            this.O = true;
            if (!this.P) {
                this.P = true;
                this.N = this.u.a(this.h, this.O, false);
            }
            if (this.N != null) {
                this.N.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void i(Bundle bundle) {
        Parcelable h2;
        f(bundle);
        if (this.v != null && (h2 = this.v.h()) != null) {
            bundle.putParcelable("android:support:fragments", h2);
        }
    }

    public void j() {
        this.G = true;
    }

    public void k() {
        this.G = true;
    }

    public void l() {
        this.G = true;
    }

    public void m() {
        this.G = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.o.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.o.a(java.lang.String, boolean, boolean):android.support.v4.app.ap */
    public void n() {
        this.G = true;
        if (!this.P) {
            this.P = true;
            this.N = this.u.a(this.h, this.O, false);
        }
        if (this.N != null) {
            this.N.h();
        }
    }

    /* access modifiers changed from: package-private */
    public void o() {
        this.g = -1;
        this.h = null;
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = 0;
        this.t = null;
        this.v = null;
        this.u = null;
        this.x = 0;
        this.y = 0;
        this.z = null;
        this.A = false;
        this.B = false;
        this.D = false;
        this.N = null;
        this.O = false;
        this.P = false;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.G = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        b().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.G = true;
    }

    public void p() {
        this.G = true;
    }

    public void q() {
    }

    public Object r() {
        return this.Q;
    }

    public Object s() {
        return this.R == f37a ? r() : this.R;
    }

    public Object t() {
        return this.S;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        d.a(this, sb);
        if (this.g >= 0) {
            sb.append(" #");
            sb.append(this.g);
        }
        if (this.x != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.x));
        }
        if (this.z != null) {
            sb.append(" ");
            sb.append(this.z);
        }
        sb.append('}');
        return sb.toString();
    }

    public Object u() {
        return this.T == f37a ? t() : this.T;
    }

    public Object v() {
        return this.U;
    }

    public Object w() {
        return this.V == f37a ? v() : this.V;
    }

    public boolean x() {
        if (this.X == null) {
            return true;
        }
        return this.X.booleanValue();
    }

    public boolean y() {
        if (this.W == null) {
            return true;
        }
        return this.W.booleanValue();
    }

    /* access modifiers changed from: package-private */
    public void z() {
        this.v = new t();
        this.v.a(this.u, new m(this), this);
    }
}
