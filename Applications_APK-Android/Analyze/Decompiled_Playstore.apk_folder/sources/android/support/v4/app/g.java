package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;

class g implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f29a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ i f30b;
    final /* synthetic */ int c;
    final /* synthetic */ Object d;
    final /* synthetic */ d e;

    g(d dVar, View view, i iVar, int i, Object obj) {
        this.e = dVar;
        this.f29a = view;
        this.f30b = iVar;
        this.c = i;
        this.d = obj;
    }

    public boolean onPreDraw() {
        this.f29a.getViewTreeObserver().removeOnPreDrawListener(this);
        this.e.a(this.f30b, this.c, this.d);
        return true;
    }
}
