package android.support.v4.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f7a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private Context f8b;
    private r c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private ae f;
    private boolean g;

    public FragmentTabHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    private af a(String str, af afVar) {
        ae aeVar = null;
        int i = 0;
        while (i < this.f7a.size()) {
            ae aeVar2 = (ae) this.f7a.get(i);
            if (!aeVar2.f10a.equals(str)) {
                aeVar2 = aeVar;
            }
            i++;
            aeVar = aeVar2;
        }
        if (aeVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != aeVar) {
            if (afVar == null) {
                afVar = this.c.a();
            }
            if (!(this.f == null || this.f.d == null)) {
                afVar.a(this.f.d);
            }
            if (aeVar != null) {
                if (aeVar.d == null) {
                    l unused = aeVar.d = l.a(this.f8b, aeVar.f11b.getName(), aeVar.c);
                    afVar.a(this.d, aeVar.d, aeVar.f10a);
                } else {
                    afVar.b(aeVar.d);
                }
            }
            this.f = aeVar;
        }
        return afVar;
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842995}, 0, 0);
        this.d = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener(this);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        af afVar = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f7a.size()) {
                break;
            }
            ae aeVar = (ae) this.f7a.get(i2);
            l unused = aeVar.d = this.c.a(aeVar.f10a);
            if (aeVar.d != null && !aeVar.d.f()) {
                if (aeVar.f10a.equals(currentTabTag)) {
                    this.f = aeVar;
                } else {
                    if (afVar == null) {
                        afVar = this.c.a();
                    }
                    afVar.a(aeVar.d);
                }
            }
            i = i2 + 1;
        }
        this.g = true;
        af a2 = a(currentTabTag, afVar);
        if (a2 != null) {
            a2.a();
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        ac acVar = (ac) parcelable;
        super.onRestoreInstanceState(acVar.getSuperState());
        setCurrentTabByTag(acVar.f9a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        ac acVar = new ac(super.onSaveInstanceState());
        acVar.f9a = getCurrentTabTag();
        return acVar;
    }

    public void onTabChanged(String str) {
        af a2;
        if (this.g && (a2 = a(str, (af) null)) != null) {
            a2.a();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
