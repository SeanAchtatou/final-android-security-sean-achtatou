package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class ai implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f13a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ am f14b;
    final /* synthetic */ Map c;
    final /* synthetic */ Map d;
    final /* synthetic */ Transition e;
    final /* synthetic */ ArrayList f;

    ai(View view, am amVar, Map map, Map map2, Transition transition, ArrayList arrayList) {
        this.f13a = view;
        this.f14b = amVar;
        this.c = map;
        this.d = map2;
        this.e = transition;
        this.f = arrayList;
    }

    public boolean onPreDraw() {
        this.f13a.getViewTreeObserver().removeOnPreDrawListener(this);
        View a2 = this.f14b.a();
        if (a2 == null) {
            return true;
        }
        if (!this.c.isEmpty()) {
            ag.a(this.d, a2);
            this.d.keySet().retainAll(this.c.values());
            for (Map.Entry entry : this.c.entrySet()) {
                View view = (View) this.d.get((String) entry.getValue());
                if (view != null) {
                    view.setTransitionName((String) entry.getKey());
                }
            }
        }
        if (this.e == null) {
            return true;
        }
        ag.b(this.f, a2);
        this.f.removeAll(this.d.values());
        ag.b(this.e, this.f);
        return true;
    }
}
