package android.support.v4.app;

import android.os.Build;
import android.support.v4.c.a;
import android.support.v4.c.e;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

final class d extends af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final t f23a;

    /* renamed from: b  reason: collision with root package name */
    h f24b;
    h c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    boolean k;
    boolean l = true;
    String m;
    boolean n;
    int o = -1;
    int p;
    CharSequence q;
    int r;
    CharSequence s;
    ArrayList t;
    ArrayList u;

    public d(t tVar) {
        this.f23a = tVar;
    }

    private i a(SparseArray sparseArray, SparseArray sparseArray2, boolean z) {
        i iVar = new i(this);
        iVar.d = new View(this.f23a.o);
        int i2 = 0;
        boolean z2 = false;
        while (i2 < sparseArray.size()) {
            boolean z3 = a(sparseArray.keyAt(i2), iVar, z, sparseArray, sparseArray2) ? true : z2;
            i2++;
            z2 = z3;
        }
        for (int i3 = 0; i3 < sparseArray2.size(); i3++) {
            int keyAt = sparseArray2.keyAt(i3);
            if (sparseArray.get(keyAt) == null && a(keyAt, iVar, z, sparseArray, sparseArray2)) {
                z2 = true;
            }
        }
        if (!z2) {
            return null;
        }
        return iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ag.a(java.util.Map, android.view.View):void
     arg types: [android.support.v4.c.a, android.view.View]
     candidates:
      android.support.v4.app.ag.a(android.transition.Transition, android.support.v4.app.al):void
      android.support.v4.app.ag.a(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.ag.a(java.lang.Object, android.view.View):void
      android.support.v4.app.ag.a(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.ag.a(java.util.ArrayList, android.view.View):void
      android.support.v4.app.ag.a(java.util.Map, android.view.View):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void
     arg types: [android.support.v4.app.i, android.support.v4.c.a, int]
     candidates:
      android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.i, boolean, android.support.v4.app.l):android.support.v4.c.a
      android.support.v4.app.d.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.a):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.l, android.support.v4.app.l, boolean):java.lang.Object
      android.support.v4.app.d.a(android.support.v4.app.d, android.support.v4.c.a, android.support.v4.app.i):void
      android.support.v4.app.d.a(android.support.v4.app.i, int, java.lang.Object):void
      android.support.v4.app.d.a(android.support.v4.app.i, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.d.a(android.support.v4.c.a, java.lang.String, java.lang.String):void
      android.support.v4.app.d.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.b(android.support.v4.app.i, android.support.v4.c.a, boolean):void
     arg types: [android.support.v4.app.i, android.support.v4.c.a, int]
     candidates:
      android.support.v4.app.d.b(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.b(android.support.v4.app.i, android.support.v4.c.a, boolean):void */
    private a a(i iVar, l lVar, boolean z) {
        a aVar = new a();
        if (this.t != null) {
            ag.a((Map) aVar, lVar.h());
            if (z) {
                aVar.a((Collection) this.u);
            } else {
                aVar = a(this.t, this.u, aVar);
            }
        }
        if (z) {
            if (lVar.Y != null) {
                lVar.Y.a(this.u, aVar);
            }
            a(iVar, aVar, false);
        } else {
            if (lVar.Z != null) {
                lVar.Z.a(this.u, aVar);
            }
            b(iVar, aVar, false);
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void
     arg types: [android.support.v4.app.i, android.support.v4.c.a, int]
     candidates:
      android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.i, boolean, android.support.v4.app.l):android.support.v4.c.a
      android.support.v4.app.d.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.a):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.l, android.support.v4.app.l, boolean):java.lang.Object
      android.support.v4.app.d.a(android.support.v4.app.d, android.support.v4.c.a, android.support.v4.app.i):void
      android.support.v4.app.d.a(android.support.v4.app.i, int, java.lang.Object):void
      android.support.v4.app.d.a(android.support.v4.app.i, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.d.a(android.support.v4.c.a, java.lang.String, java.lang.String):void
      android.support.v4.app.d.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.b(android.support.v4.app.i, android.support.v4.c.a, boolean):void
     arg types: [android.support.v4.app.i, android.support.v4.c.a, int]
     candidates:
      android.support.v4.app.d.b(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.b(android.support.v4.app.i, android.support.v4.c.a, boolean):void */
    /* access modifiers changed from: private */
    public a a(i iVar, boolean z, l lVar) {
        a b2 = b(iVar, lVar, z);
        if (z) {
            if (lVar.Z != null) {
                lVar.Z.a(this.u, b2);
            }
            a(iVar, b2, true);
        } else {
            if (lVar.Y != null) {
                lVar.Y.a(this.u, b2);
            }
            b(iVar, b2, true);
        }
        return b2;
    }

    private static a a(ArrayList arrayList, ArrayList arrayList2, a aVar) {
        if (aVar.isEmpty()) {
            return aVar;
        }
        a aVar2 = new a();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) aVar.get(arrayList.get(i2));
            if (view != null) {
                aVar2.put(arrayList2.get(i2), view);
            }
        }
        return aVar2;
    }

    private static Object a(l lVar, l lVar2, boolean z) {
        if (lVar == null || lVar2 == null) {
            return null;
        }
        return ag.a(z ? lVar2.w() : lVar.v());
    }

    private static Object a(l lVar, boolean z) {
        if (lVar == null) {
            return null;
        }
        return ag.a(z ? lVar.u() : lVar.r());
    }

    private static Object a(Object obj, l lVar, ArrayList arrayList, a aVar) {
        return obj != null ? ag.a(obj, lVar.h(), arrayList, aVar) : obj;
    }

    private void a(int i2, l lVar, String str, int i3) {
        lVar.t = this.f23a;
        if (str != null) {
            if (lVar.z == null || str.equals(lVar.z)) {
                lVar.z = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + lVar + ": was " + lVar.z + " now " + str);
            }
        }
        if (i2 != 0) {
            if (lVar.x == 0 || lVar.x == i2) {
                lVar.x = i2;
                lVar.y = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + lVar + ": was " + lVar.x + " now " + i2);
            }
        }
        h hVar = new h();
        hVar.c = i3;
        hVar.d = lVar;
        a(hVar);
    }

    /* access modifiers changed from: private */
    public void a(i iVar, int i2, Object obj) {
        if (this.f23a.g != null) {
            for (int i3 = 0; i3 < this.f23a.g.size(); i3++) {
                l lVar = (l) this.f23a.g.get(i3);
                if (!(lVar.J == null || lVar.I == null || lVar.y != i2)) {
                    if (!lVar.A) {
                        ag.a(obj, lVar.J, false);
                        iVar.f34b.remove(lVar.J);
                    } else if (!iVar.f34b.contains(lVar.J)) {
                        ag.a(obj, lVar.J, true);
                        iVar.f34b.add(lVar.J);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(i iVar, l lVar, l lVar2, boolean z, a aVar) {
        as asVar = z ? lVar2.Y : lVar.Y;
        if (asVar != null) {
            asVar.b(new ArrayList(aVar.keySet()), new ArrayList(aVar.values()), null);
        }
    }

    private void a(i iVar, a aVar, boolean z) {
        int size = this.u == null ? 0 : this.u.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) this.t.get(i2);
            View view = (View) aVar.get((String) this.u.get(i2));
            if (view != null) {
                String a2 = ag.a(view);
                if (z) {
                    a(iVar.f33a, str, a2);
                } else {
                    a(iVar.f33a, a2, str);
                }
            }
        }
    }

    private void a(i iVar, View view, Object obj, l lVar, l lVar2, boolean z, ArrayList arrayList) {
        view.getViewTreeObserver().addOnPreDrawListener(new f(this, view, obj, arrayList, iVar, z, lVar, lVar2));
    }

    private static void a(i iVar, ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < arrayList.size()) {
                    a(iVar.f33a, (String) arrayList.get(i3), (String) arrayList2.get(i3));
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(a aVar, i iVar) {
        View view;
        if (this.u != null && !aVar.isEmpty() && (view = (View) aVar.get(this.u.get(0))) != null) {
            iVar.c.f18a = view;
        }
    }

    private static void a(a aVar, String str, String str2) {
        if (str != null && str2 != null && !str.equals(str2)) {
            for (int i2 = 0; i2 < aVar.size(); i2++) {
                if (str.equals(aVar.c(i2))) {
                    aVar.a(i2, str2);
                    return;
                }
            }
            aVar.put(str, str2);
        }
    }

    private static void a(SparseArray sparseArray, l lVar) {
        int i2;
        if (lVar != null && (i2 = lVar.y) != 0 && !lVar.g() && lVar.e() && lVar.h() != null && sparseArray.get(i2) == null) {
            sparseArray.put(i2, lVar);
        }
    }

    private void a(View view, i iVar, int i2, Object obj) {
        view.getViewTreeObserver().addOnPreDrawListener(new g(this, view, iVar, i2, obj));
    }

    private boolean a(int i2, i iVar, boolean z, SparseArray sparseArray, SparseArray sparseArray2) {
        View view;
        ViewGroup viewGroup = (ViewGroup) this.f23a.p.a(i2);
        if (viewGroup == null) {
            return false;
        }
        l lVar = (l) sparseArray2.get(i2);
        l lVar2 = (l) sparseArray.get(i2);
        Object a2 = a(lVar, z);
        Object a3 = a(lVar, lVar2, z);
        Object b2 = b(lVar2, z);
        if (a2 == null && a3 == null && b2 == null) {
            return false;
        }
        a aVar = null;
        ArrayList arrayList = new ArrayList();
        if (a3 != null) {
            aVar = a(iVar, lVar2, z);
            if (aVar.isEmpty()) {
                arrayList.add(iVar.d);
            } else {
                arrayList.addAll(aVar.values());
            }
            as asVar = z ? lVar2.Y : lVar.Y;
            if (asVar != null) {
                asVar.a(new ArrayList(aVar.keySet()), new ArrayList(aVar.values()), null);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Object a4 = a(b2, lVar2, arrayList2, aVar);
        if (!(this.u == null || aVar == null || (view = (View) aVar.get(this.u.get(0))) == null)) {
            if (a4 != null) {
                ag.a(a4, view);
            }
            if (a3 != null) {
                ag.a(a3, view);
            }
        }
        e eVar = new e(this, lVar);
        if (a3 != null) {
            a(iVar, viewGroup, a3, lVar, lVar2, z, arrayList);
        }
        ArrayList arrayList3 = new ArrayList();
        a aVar2 = new a();
        Object a5 = ag.a(a2, a4, a3, z ? lVar.y() : lVar.x());
        if (a5 != null) {
            ag.a(a2, a3, viewGroup, eVar, iVar.d, iVar.c, iVar.f33a, arrayList3, aVar2, arrayList);
            a(viewGroup, iVar, i2, a5);
            ag.a(a5, iVar.d, true);
            a(iVar, i2, a5);
            ag.a(viewGroup, a5);
            ag.a(viewGroup, iVar.d, a2, arrayList3, a4, arrayList2, a3, arrayList, a5, iVar.f34b, aVar2);
        }
        return a5 != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ag.a(java.util.Map, android.view.View):void
     arg types: [android.support.v4.c.a, android.view.View]
     candidates:
      android.support.v4.app.ag.a(android.transition.Transition, android.support.v4.app.al):void
      android.support.v4.app.ag.a(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.ag.a(java.lang.Object, android.view.View):void
      android.support.v4.app.ag.a(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.ag.a(java.util.ArrayList, android.view.View):void
      android.support.v4.app.ag.a(java.util.Map, android.view.View):void */
    private a b(i iVar, l lVar, boolean z) {
        a aVar = new a();
        View h2 = lVar.h();
        if (h2 == null || this.t == null) {
            return aVar;
        }
        ag.a((Map) aVar, h2);
        if (z) {
            return a(this.t, this.u, aVar);
        }
        aVar.a((Collection) this.u);
        return aVar;
    }

    private static Object b(l lVar, boolean z) {
        if (lVar == null) {
            return null;
        }
        return ag.a(z ? lVar.s() : lVar.t());
    }

    private void b(i iVar, a aVar, boolean z) {
        int size = aVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) aVar.b(i2);
            String a2 = ag.a((View) aVar.c(i2));
            if (z) {
                a(iVar.f33a, str, a2);
            } else {
                a(iVar.f33a, a2, str);
            }
        }
    }

    private void b(SparseArray sparseArray, l lVar) {
        int i2;
        if (lVar != null && (i2 = lVar.y) != 0) {
            sparseArray.put(i2, lVar);
        }
    }

    private void b(SparseArray sparseArray, SparseArray sparseArray2) {
        l lVar;
        if (this.f23a.p.a()) {
            for (h hVar = this.f24b; hVar != null; hVar = hVar.f31a) {
                switch (hVar.c) {
                    case 1:
                        b(sparseArray2, hVar.d);
                        break;
                    case 2:
                        l lVar2 = hVar.d;
                        if (this.f23a.g != null) {
                            int i2 = 0;
                            lVar = lVar2;
                            while (true) {
                                int i3 = i2;
                                if (i3 < this.f23a.g.size()) {
                                    l lVar3 = (l) this.f23a.g.get(i3);
                                    if (lVar == null || lVar3.y == lVar.y) {
                                        if (lVar3 == lVar) {
                                            lVar = null;
                                        } else {
                                            a(sparseArray, lVar3);
                                        }
                                    }
                                    i2 = i3 + 1;
                                }
                            }
                        } else {
                            lVar = lVar2;
                        }
                        b(sparseArray2, lVar);
                        break;
                    case 3:
                        a(sparseArray, hVar.d);
                        break;
                    case 4:
                        a(sparseArray, hVar.d);
                        break;
                    case 5:
                        b(sparseArray2, hVar.d);
                        break;
                    case 6:
                        a(sparseArray, hVar.d);
                        break;
                    case 7:
                        b(sparseArray2, hVar.d);
                        break;
                }
            }
        }
    }

    public int a() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z) {
        if (this.n) {
            throw new IllegalStateException("commit already called");
        }
        if (t.f44a) {
            Log.v("FragmentManager", "Commit: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new e("FragmentManager")), (String[]) null);
        }
        this.n = true;
        if (this.k) {
            this.o = this.f23a.a(this);
        } else {
            this.o = -1;
        }
        this.f23a.a(this, z);
        return this.o;
    }

    public af a(int i2, l lVar, String str) {
        a(i2, lVar, str, 1);
        return this;
    }

    public af a(l lVar) {
        h hVar = new h();
        hVar.c = 6;
        hVar.d = lVar;
        a(hVar);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.i, boolean, android.support.v4.app.l):android.support.v4.c.a
      android.support.v4.app.d.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.a):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.l, android.support.v4.app.l, boolean):java.lang.Object
      android.support.v4.app.d.a(android.support.v4.app.d, android.support.v4.c.a, android.support.v4.app.i):void
      android.support.v4.app.d.a(android.support.v4.app.i, int, java.lang.Object):void
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void
      android.support.v4.app.d.a(android.support.v4.app.i, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.d.a(android.support.v4.c.a, java.lang.String, java.lang.String):void
      android.support.v4.app.d.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.l, boolean):void
     arg types: [android.support.v4.app.l, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.l
      android.support.v4.app.t.a(int, android.support.v4.app.d):void
      android.support.v4.app.t.a(int, boolean):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.l, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.support.v4.app.l, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(int, int, int, boolean):void */
    public i a(boolean z, i iVar, SparseArray sparseArray, SparseArray sparseArray2) {
        if (t.f44a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new e("FragmentManager")), (String[]) null);
        }
        if (iVar == null) {
            if (!(sparseArray.size() == 0 && sparseArray2.size() == 0)) {
                iVar = a(sparseArray, sparseArray2, true);
            }
        } else if (!z) {
            a(iVar, this.u, this.t);
        }
        a(-1);
        int i2 = iVar != null ? 0 : this.j;
        int i3 = iVar != null ? 0 : this.i;
        for (h hVar = this.c; hVar != null; hVar = hVar.f32b) {
            int i4 = iVar != null ? 0 : hVar.g;
            int i5 = iVar != null ? 0 : hVar.h;
            switch (hVar.c) {
                case 1:
                    l lVar = hVar.d;
                    lVar.H = i5;
                    this.f23a.a(lVar, t.c(i3), i2);
                    break;
                case 2:
                    l lVar2 = hVar.d;
                    if (lVar2 != null) {
                        lVar2.H = i5;
                        this.f23a.a(lVar2, t.c(i3), i2);
                    }
                    if (hVar.i == null) {
                        break;
                    } else {
                        for (int i6 = 0; i6 < hVar.i.size(); i6++) {
                            l lVar3 = (l) hVar.i.get(i6);
                            lVar3.H = i4;
                            this.f23a.a(lVar3, false);
                        }
                        break;
                    }
                case 3:
                    l lVar4 = hVar.d;
                    lVar4.H = i4;
                    this.f23a.a(lVar4, false);
                    break;
                case 4:
                    l lVar5 = hVar.d;
                    lVar5.H = i4;
                    this.f23a.c(lVar5, t.c(i3), i2);
                    break;
                case 5:
                    l lVar6 = hVar.d;
                    lVar6.H = i5;
                    this.f23a.b(lVar6, t.c(i3), i2);
                    break;
                case 6:
                    l lVar7 = hVar.d;
                    lVar7.H = i4;
                    this.f23a.e(lVar7, t.c(i3), i2);
                    break;
                case 7:
                    l lVar8 = hVar.d;
                    lVar8.H = i4;
                    this.f23a.d(lVar8, t.c(i3), i2);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + hVar.c);
            }
        }
        if (z) {
            this.f23a.a(this.f23a.n, t.c(i3), i2, true);
            iVar = null;
        }
        if (this.o >= 0) {
            this.f23a.b(this.o);
            this.o = -1;
        }
        return iVar;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.k) {
            if (t.f44a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            for (h hVar = this.f24b; hVar != null; hVar = hVar.f31a) {
                if (hVar.d != null) {
                    hVar.d.s += i2;
                    if (t.f44a) {
                        Log.v("FragmentManager", "Bump nesting of " + hVar.d + " to " + hVar.d.s);
                    }
                }
                if (hVar.i != null) {
                    for (int size = hVar.i.size() - 1; size >= 0; size--) {
                        l lVar = (l) hVar.i.get(size);
                        lVar.s += i2;
                        if (t.f44a) {
                            Log.v("FragmentManager", "Bump nesting of " + lVar + " to " + lVar.s);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar) {
        if (this.f24b == null) {
            this.c = hVar;
            this.f24b = hVar;
        } else {
            hVar.f32b = this.c;
            this.c.f31a = hVar;
            this.c = hVar;
        }
        hVar.e = this.e;
        hVar.f = this.f;
        hVar.g = this.g;
        hVar.h = this.h;
        this.d++;
    }

    public void a(SparseArray sparseArray, SparseArray sparseArray2) {
        if (this.f23a.p.a()) {
            for (h hVar = this.f24b; hVar != null; hVar = hVar.f31a) {
                switch (hVar.c) {
                    case 1:
                        a(sparseArray, hVar.d);
                        break;
                    case 2:
                        if (hVar.i != null) {
                            for (int size = hVar.i.size() - 1; size >= 0; size--) {
                                b(sparseArray2, (l) hVar.i.get(size));
                            }
                        }
                        a(sparseArray, hVar.d);
                        break;
                    case 3:
                        b(sparseArray2, hVar.d);
                        break;
                    case 4:
                        b(sparseArray2, hVar.d);
                        break;
                    case 5:
                        a(sparseArray, hVar.d);
                        break;
                    case 6:
                        b(sparseArray2, hVar.d);
                        break;
                    case 7:
                        a(sparseArray, hVar.d);
                        break;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.i, boolean, android.support.v4.app.l):android.support.v4.c.a
      android.support.v4.app.d.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.a):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.l, android.support.v4.app.l, boolean):java.lang.Object
      android.support.v4.app.d.a(android.support.v4.app.d, android.support.v4.c.a, android.support.v4.app.i):void
      android.support.v4.app.d.a(android.support.v4.app.i, int, java.lang.Object):void
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void
      android.support.v4.app.d.a(android.support.v4.app.i, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.d.a(android.support.v4.c.a, java.lang.String, java.lang.String):void
      android.support.v4.app.d.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.af.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.m);
            printWriter.print(" mIndex=");
            printWriter.print(this.o);
            printWriter.print(" mCommitted=");
            printWriter.println(this.n);
            if (this.i != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.i));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.j));
            }
            if (!(this.e == 0 && this.f == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.e));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f));
            }
            if (!(this.g == 0 && this.h == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.g));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.h));
            }
            if (!(this.p == 0 && this.q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.p));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.q);
            }
            if (!(this.r == 0 && this.s == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.r));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.s);
            }
        }
        if (this.f24b != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i2 = 0;
            h hVar = this.f24b;
            while (hVar != null) {
                switch (hVar.c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + hVar.c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(hVar.d);
                if (z) {
                    if (!(hVar.e == 0 && hVar.f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(hVar.e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(hVar.f));
                    }
                    if (!(hVar.g == 0 && hVar.h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(hVar.g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(hVar.h));
                    }
                }
                if (hVar.i != null && hVar.i.size() > 0) {
                    for (int i3 = 0; i3 < hVar.i.size(); i3++) {
                        printWriter.print(str3);
                        if (hVar.i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i3 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i3);
                            printWriter.print(": ");
                        }
                        printWriter.println(hVar.i.get(i3));
                    }
                }
                hVar = hVar.f31a;
                i2++;
            }
        }
    }

    public af b(l lVar) {
        h hVar = new h();
        hVar.c = 7;
        hVar.d = lVar;
        a(hVar);
        return this;
    }

    public String b() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.app.l, boolean):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.i, boolean, android.support.v4.app.l):android.support.v4.c.a
      android.support.v4.app.d.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.c.a):android.support.v4.c.a
      android.support.v4.app.d.a(android.support.v4.app.l, android.support.v4.app.l, boolean):java.lang.Object
      android.support.v4.app.d.a(android.support.v4.app.d, android.support.v4.c.a, android.support.v4.app.i):void
      android.support.v4.app.d.a(android.support.v4.app.i, int, java.lang.Object):void
      android.support.v4.app.d.a(android.support.v4.app.i, android.support.v4.c.a, boolean):void
      android.support.v4.app.d.a(android.support.v4.app.i, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.d.a(android.support.v4.c.a, java.lang.String, java.lang.String):void
      android.support.v4.app.d.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.af.a(int, android.support.v4.app.l, java.lang.String):android.support.v4.app.af
      android.support.v4.app.d.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.l, boolean):void
     arg types: [android.support.v4.app.l, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.l
      android.support.v4.app.t.a(int, android.support.v4.app.d):void
      android.support.v4.app.t.a(int, boolean):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.l, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.support.v4.app.l, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(int, int, int, boolean):void */
    public void run() {
        i iVar;
        l lVar;
        if (t.f44a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.k || this.o >= 0) {
            a(1);
            if (Build.VERSION.SDK_INT >= 21) {
                SparseArray sparseArray = new SparseArray();
                SparseArray sparseArray2 = new SparseArray();
                b(sparseArray, sparseArray2);
                iVar = a(sparseArray, sparseArray2, false);
            } else {
                iVar = null;
            }
            int i2 = iVar != null ? 0 : this.j;
            int i3 = iVar != null ? 0 : this.i;
            for (h hVar = this.f24b; hVar != null; hVar = hVar.f31a) {
                int i4 = iVar != null ? 0 : hVar.e;
                int i5 = iVar != null ? 0 : hVar.f;
                switch (hVar.c) {
                    case 1:
                        l lVar2 = hVar.d;
                        lVar2.H = i4;
                        this.f23a.a(lVar2, false);
                        break;
                    case 2:
                        l lVar3 = hVar.d;
                        if (this.f23a.g != null) {
                            lVar = lVar3;
                            for (int i6 = 0; i6 < this.f23a.g.size(); i6++) {
                                l lVar4 = (l) this.f23a.g.get(i6);
                                if (t.f44a) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + lVar + " old=" + lVar4);
                                }
                                if (lVar == null || lVar4.y == lVar.y) {
                                    if (lVar4 == lVar) {
                                        hVar.d = null;
                                        lVar = null;
                                    } else {
                                        if (hVar.i == null) {
                                            hVar.i = new ArrayList();
                                        }
                                        hVar.i.add(lVar4);
                                        lVar4.H = i5;
                                        if (this.k) {
                                            lVar4.s++;
                                            if (t.f44a) {
                                                Log.v("FragmentManager", "Bump nesting of " + lVar4 + " to " + lVar4.s);
                                            }
                                        }
                                        this.f23a.a(lVar4, i3, i2);
                                    }
                                }
                            }
                        } else {
                            lVar = lVar3;
                        }
                        if (lVar == null) {
                            break;
                        } else {
                            lVar.H = i4;
                            this.f23a.a(lVar, false);
                            break;
                        }
                    case 3:
                        l lVar5 = hVar.d;
                        lVar5.H = i5;
                        this.f23a.a(lVar5, i3, i2);
                        break;
                    case 4:
                        l lVar6 = hVar.d;
                        lVar6.H = i5;
                        this.f23a.b(lVar6, i3, i2);
                        break;
                    case 5:
                        l lVar7 = hVar.d;
                        lVar7.H = i4;
                        this.f23a.c(lVar7, i3, i2);
                        break;
                    case 6:
                        l lVar8 = hVar.d;
                        lVar8.H = i5;
                        this.f23a.d(lVar8, i3, i2);
                        break;
                    case 7:
                        l lVar9 = hVar.d;
                        lVar9.H = i4;
                        this.f23a.e(lVar9, i3, i2);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + hVar.c);
                }
            }
            this.f23a.a(this.f23a.n, i3, i2, true);
            if (this.k) {
                this.f23a.b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.o >= 0) {
            sb.append(" #");
            sb.append(this.o);
        }
        if (this.m != null) {
            sb.append(" ");
            sb.append(this.m);
        }
        sb.append("}");
        return sb.toString();
    }
}
