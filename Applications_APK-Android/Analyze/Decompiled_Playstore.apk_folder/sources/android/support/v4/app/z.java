package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

final class z implements Parcelable {
    public static final Parcelable.Creator CREATOR = new aa();

    /* renamed from: a  reason: collision with root package name */
    final String f52a;

    /* renamed from: b  reason: collision with root package name */
    final int f53b;
    final boolean c;
    final int d;
    final int e;
    final String f;
    final boolean g;
    final boolean h;
    final Bundle i;
    Bundle j;
    l k;

    public z(Parcel parcel) {
        boolean z = true;
        this.f52a = parcel.readString();
        this.f53b = parcel.readInt();
        this.c = parcel.readInt() != 0;
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt() != 0;
        this.h = parcel.readInt() == 0 ? false : z;
        this.i = parcel.readBundle();
        this.j = parcel.readBundle();
    }

    public z(l lVar) {
        this.f52a = lVar.getClass().getName();
        this.f53b = lVar.g;
        this.c = lVar.p;
        this.d = lVar.x;
        this.e = lVar.y;
        this.f = lVar.z;
        this.g = lVar.C;
        this.h = lVar.B;
        this.i = lVar.i;
    }

    public l a(o oVar, l lVar) {
        if (this.k != null) {
            return this.k;
        }
        if (this.i != null) {
            this.i.setClassLoader(oVar.getClassLoader());
        }
        this.k = l.a(oVar, this.f52a, this.i);
        if (this.j != null) {
            this.j.setClassLoader(oVar.getClassLoader());
            this.k.e = this.j;
        }
        this.k.a(this.f53b, lVar);
        this.k.p = this.c;
        this.k.r = true;
        this.k.x = this.d;
        this.k.y = this.e;
        this.k.z = this.f;
        this.k.C = this.g;
        this.k.B = this.h;
        this.k.t = oVar.f41b;
        if (t.f44a) {
            Log.v("FragmentManager", "Instantiated fragment " + this.k);
        }
        return this.k;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeString(this.f52a);
        parcel.writeInt(this.f53b);
        parcel.writeInt(this.c ? 1 : 0);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g ? 1 : 0);
        if (!this.h) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeBundle(this.i);
        parcel.writeBundle(this.j);
    }
}
