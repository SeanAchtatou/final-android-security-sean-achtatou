package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class j implements Parcelable {
    public static final Parcelable.Creator CREATOR = new k();

    /* renamed from: a  reason: collision with root package name */
    final int[] f35a;

    /* renamed from: b  reason: collision with root package name */
    final int f36b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;
    final ArrayList j;
    final ArrayList k;

    public j(Parcel parcel) {
        this.f35a = parcel.createIntArray();
        this.f36b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.createStringArrayList();
        this.k = parcel.createStringArrayList();
    }

    public j(t tVar, d dVar) {
        int i2 = 0;
        for (h hVar = dVar.f24b; hVar != null; hVar = hVar.f31a) {
            if (hVar.i != null) {
                i2 += hVar.i.size();
            }
        }
        this.f35a = new int[(i2 + (dVar.d * 7))];
        if (!dVar.k) {
            throw new IllegalStateException("Not on back stack");
        }
        int i3 = 0;
        for (h hVar2 = dVar.f24b; hVar2 != null; hVar2 = hVar2.f31a) {
            int i4 = i3 + 1;
            this.f35a[i3] = hVar2.c;
            int i5 = i4 + 1;
            this.f35a[i4] = hVar2.d != null ? hVar2.d.g : -1;
            int i6 = i5 + 1;
            this.f35a[i5] = hVar2.e;
            int i7 = i6 + 1;
            this.f35a[i6] = hVar2.f;
            int i8 = i7 + 1;
            this.f35a[i7] = hVar2.g;
            int i9 = i8 + 1;
            this.f35a[i8] = hVar2.h;
            if (hVar2.i != null) {
                int size = hVar2.i.size();
                int i10 = i9 + 1;
                this.f35a[i9] = size;
                int i11 = 0;
                while (i11 < size) {
                    this.f35a[i10] = ((l) hVar2.i.get(i11)).g;
                    i11++;
                    i10++;
                }
                i3 = i10;
            } else {
                i3 = i9 + 1;
                this.f35a[i9] = 0;
            }
        }
        this.f36b = dVar.i;
        this.c = dVar.j;
        this.d = dVar.m;
        this.e = dVar.o;
        this.f = dVar.p;
        this.g = dVar.q;
        this.h = dVar.r;
        this.i = dVar.s;
        this.j = dVar.t;
        this.k = dVar.u;
    }

    public d a(t tVar) {
        d dVar = new d(tVar);
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.f35a.length) {
            h hVar = new h();
            int i4 = i3 + 1;
            hVar.c = this.f35a[i3];
            if (t.f44a) {
                Log.v("FragmentManager", "Instantiate " + dVar + " op #" + i2 + " base fragment #" + this.f35a[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.f35a[i4];
            if (i6 >= 0) {
                hVar.d = (l) tVar.f.get(i6);
            } else {
                hVar.d = null;
            }
            int i7 = i5 + 1;
            hVar.e = this.f35a[i5];
            int i8 = i7 + 1;
            hVar.f = this.f35a[i7];
            int i9 = i8 + 1;
            hVar.g = this.f35a[i8];
            int i10 = i9 + 1;
            hVar.h = this.f35a[i9];
            int i11 = i10 + 1;
            int i12 = this.f35a[i10];
            if (i12 > 0) {
                hVar.i = new ArrayList(i12);
                int i13 = 0;
                while (i13 < i12) {
                    if (t.f44a) {
                        Log.v("FragmentManager", "Instantiate " + dVar + " set remove fragment #" + this.f35a[i11]);
                    }
                    hVar.i.add((l) tVar.f.get(this.f35a[i11]));
                    i13++;
                    i11++;
                }
            }
            dVar.a(hVar);
            i2++;
            i3 = i11;
        }
        dVar.i = this.f36b;
        dVar.j = this.c;
        dVar.m = this.d;
        dVar.o = this.e;
        dVar.k = true;
        dVar.p = this.f;
        dVar.q = this.g;
        dVar.r = this.h;
        dVar.s = this.i;
        dVar.t = this.j;
        dVar.u = this.k;
        dVar.a(1);
        return dVar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.f35a);
        parcel.writeInt(this.f36b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        TextUtils.writeToParcel(this.g, parcel, 0);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeStringList(this.j);
        parcel.writeStringList(this.k);
    }
}
