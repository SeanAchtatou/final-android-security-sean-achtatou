package android.support.v4.app;

import android.view.animation.Animation;

class v implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f47a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ t f48b;

    v(t tVar, l lVar) {
        this.f48b = tVar;
        this.f47a = lVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.l, int, int, int, boolean):void
     arg types: [android.support.v4.app.l, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.l, int, int, int, boolean):void */
    public void onAnimationEnd(Animation animation) {
        if (this.f47a.c != null) {
            this.f47a.c = null;
            this.f48b.a(this.f47a, this.f47a.d, 0, 0, false);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
