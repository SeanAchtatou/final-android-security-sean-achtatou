package android.support.v4.app;

import android.support.v4.c.d;
import android.support.v4.c.m;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

class ap extends an {

    /* renamed from: a  reason: collision with root package name */
    static boolean f19a = false;

    /* renamed from: b  reason: collision with root package name */
    final m f20b = new m();
    final m c = new m();
    final String d;
    o e;
    boolean f;
    boolean g;

    ap(String str, o oVar, boolean z) {
        this.d = str;
        this.e = oVar;
        this.f = z;
    }

    /* access modifiers changed from: package-private */
    public void a(o oVar) {
        this.e = oVar;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.f20b.b() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.f20b.b(); i++) {
                aq aqVar = (aq) this.f20b.b(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.f20b.a(i));
                printWriter.print(": ");
                printWriter.println(aqVar.toString());
                aqVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.c.b() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.c.b(); i2++) {
                aq aqVar2 = (aq) this.c.b(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.c.a(i2));
                printWriter.print(": ");
                printWriter.println(aqVar2.toString());
                aqVar2.a(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean a() {
        int b2 = this.f20b.b();
        boolean z = false;
        for (int i = 0; i < b2; i++) {
            aq aqVar = (aq) this.f20b.b(i);
            z |= aqVar.h && !aqVar.f;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (f19a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f = true;
        for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
            ((aq) this.f20b.b(b2)).a();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (f19a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
            ((aq) this.f20b.b(b2)).e();
        }
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (f19a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.g = true;
        this.f = false;
        for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
            ((aq) this.f20b.b(b2)).b();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.g) {
            if (f19a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.g = false;
            for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
                ((aq) this.f20b.b(b2)).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
            ((aq) this.f20b.b(b2)).k = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
            ((aq) this.f20b.b(b2)).d();
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (!this.g) {
            if (f19a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int b2 = this.f20b.b() - 1; b2 >= 0; b2--) {
                ((aq) this.f20b.b(b2)).f();
            }
            this.f20b.c();
        }
        if (f19a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int b3 = this.c.b() - 1; b3 >= 0; b3--) {
            ((aq) this.c.b(b3)).f();
        }
        this.c.c();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        d.a(this.e, sb);
        sb.append("}}");
        return sb.toString();
    }
}
