package android.support.v4.app;

import android.support.v4.c.a;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;

class f implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f27a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Object f28b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ i d;
    final /* synthetic */ boolean e;
    final /* synthetic */ l f;
    final /* synthetic */ l g;
    final /* synthetic */ d h;

    f(d dVar, View view, Object obj, ArrayList arrayList, i iVar, boolean z, l lVar, l lVar2) {
        this.h = dVar;
        this.f27a = view;
        this.f28b = obj;
        this.c = arrayList;
        this.d = iVar;
        this.e = z;
        this.f = lVar;
        this.g = lVar2;
    }

    public boolean onPreDraw() {
        this.f27a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.f28b == null) {
            return true;
        }
        ag.a(this.f28b, this.c);
        this.c.clear();
        a a2 = this.h.a(this.d, this.e, this.f);
        if (a2.isEmpty()) {
            this.c.add(this.d.d);
        } else {
            this.c.addAll(a2.values());
        }
        ag.b(this.f28b, this.c);
        this.h.a(a2, this.d);
        this.h.a(this.d, this.f, this.g, this.e, a2);
        return true;
    }
}
