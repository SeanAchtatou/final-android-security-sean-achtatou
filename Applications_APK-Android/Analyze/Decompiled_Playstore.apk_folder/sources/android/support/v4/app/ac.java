package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

class ac extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new ad();

    /* renamed from: a  reason: collision with root package name */
    String f9a;

    private ac(Parcel parcel) {
        super(parcel);
        this.f9a = parcel.readString();
    }

    ac(Parcelable parcelable) {
        super(parcelable);
    }

    public String toString() {
        return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f9a + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f9a);
    }
}
