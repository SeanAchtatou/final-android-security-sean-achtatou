package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class x implements Parcelable {
    public static final Parcelable.Creator CREATOR = new y();

    /* renamed from: a  reason: collision with root package name */
    z[] f50a;

    /* renamed from: b  reason: collision with root package name */
    int[] f51b;
    j[] c;

    public x() {
    }

    public x(Parcel parcel) {
        this.f50a = (z[]) parcel.createTypedArray(z.CREATOR);
        this.f51b = parcel.createIntArray();
        this.c = (j[]) parcel.createTypedArray(j.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.f50a, i);
        parcel.writeIntArray(this.f51b);
        parcel.writeTypedArray(this.c, i);
    }
}
