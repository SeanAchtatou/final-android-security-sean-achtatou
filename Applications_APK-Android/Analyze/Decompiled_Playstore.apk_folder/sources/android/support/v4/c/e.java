package android.support.v4.c;

import android.util.Log;
import java.io.Writer;

public class e extends Writer {

    /* renamed from: a  reason: collision with root package name */
    private final String f60a;

    /* renamed from: b  reason: collision with root package name */
    private StringBuilder f61b = new StringBuilder(128);

    public e(String str) {
        this.f60a = str;
    }

    private void a() {
        if (this.f61b.length() > 0) {
            Log.d(this.f60a, this.f61b.toString());
            this.f61b.delete(0, this.f61b.length());
        }
    }

    public void close() {
        a();
    }

    public void flush() {
        a();
    }

    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                a();
            } else {
                this.f61b.append(c);
            }
        }
    }
}
