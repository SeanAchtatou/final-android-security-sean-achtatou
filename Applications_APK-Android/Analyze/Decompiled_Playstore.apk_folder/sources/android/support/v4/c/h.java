package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class h implements Set {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f65a;

    h(f fVar) {
        this.f65a = fVar;
    }

    /* renamed from: a */
    public boolean add(Map.Entry entry) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        int a2 = this.f65a.a();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            this.f65a.a(entry.getKey(), entry.getValue());
        }
        return a2 != this.f65a.a();
    }

    public void clear() {
        this.f65a.c();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int a2 = this.f65a.a(entry.getKey());
        if (a2 >= 0) {
            return c.a(this.f65a.a(a2, 1), entry.getValue());
        }
        return false;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.c.f.a(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.c.h, java.lang.Object]
     candidates:
      android.support.v4.c.f.a(java.util.Map, java.util.Collection):boolean
      android.support.v4.c.f.a(int, int):java.lang.Object
      android.support.v4.c.f.a(int, java.lang.Object):java.lang.Object
      android.support.v4.c.f.a(java.lang.Object, java.lang.Object):void
      android.support.v4.c.f.a(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.c.f.a(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return f.a((Set) this, obj);
    }

    public int hashCode() {
        int a2 = this.f65a.a() - 1;
        int i = 0;
        while (a2 >= 0) {
            Object a3 = this.f65a.a(a2, 0);
            Object a4 = this.f65a.a(a2, 1);
            a2--;
            i += (a4 == null ? 0 : a4.hashCode()) ^ (a3 == null ? 0 : a3.hashCode());
        }
        return i;
    }

    public boolean isEmpty() {
        return this.f65a.a() == 0;
    }

    public Iterator iterator() {
        return new j(this.f65a);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.f65a.a();
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }
}
