package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;

final class k implements Collection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f69a;

    k(f fVar) {
        this.f69a = fVar;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.f69a.c();
    }

    public boolean contains(Object obj) {
        return this.f69a.b(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.f69a.a() == 0;
    }

    public Iterator iterator() {
        return new g(this.f69a, 1);
    }

    public boolean remove(Object obj) {
        int b2 = this.f69a.b(obj);
        if (b2 < 0) {
            return false;
        }
        this.f69a.a(b2);
        return true;
    }

    public boolean removeAll(Collection collection) {
        int i = 0;
        int a2 = this.f69a.a();
        boolean z = false;
        while (i < a2) {
            if (collection.contains(this.f69a.a(i, 1))) {
                this.f69a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection collection) {
        int i = 0;
        int a2 = this.f69a.a();
        boolean z = false;
        while (i < a2) {
            if (!collection.contains(this.f69a.a(i, 1))) {
                this.f69a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.f69a.a();
    }

    public Object[] toArray() {
        return this.f69a.b(1);
    }

    public Object[] toArray(Object[] objArr) {
        return this.f69a.a(objArr, 1);
    }
}
