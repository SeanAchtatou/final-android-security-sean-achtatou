package android.support.v4.c;

import java.util.Map;

class b extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f57a;

    b(a aVar) {
        this.f57a = aVar;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return this.f57a.h;
    }

    /* access modifiers changed from: protected */
    public int a(Object obj) {
        return this.f57a.a(obj);
    }

    /* access modifiers changed from: protected */
    public Object a(int i, int i2) {
        return this.f57a.g[(i << 1) + i2];
    }

    /* access modifiers changed from: protected */
    public Object a(int i, Object obj) {
        return this.f57a.a(i, obj);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f57a.d(i);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object obj2) {
        this.f57a.put(obj, obj2);
    }

    /* access modifiers changed from: protected */
    public int b(Object obj) {
        return this.f57a.b(obj);
    }

    /* access modifiers changed from: protected */
    public Map b() {
        return this.f57a;
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.f57a.clear();
    }
}
