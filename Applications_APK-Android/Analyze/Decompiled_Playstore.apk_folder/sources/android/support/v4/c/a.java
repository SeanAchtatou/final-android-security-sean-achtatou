package android.support.v4.c;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class a extends l implements Map {

    /* renamed from: a  reason: collision with root package name */
    f f56a;

    private f b() {
        if (this.f56a == null) {
            this.f56a = new b(this);
        }
        return this.f56a;
    }

    public boolean a(Collection collection) {
        return f.c(this, collection);
    }

    public Set entrySet() {
        return b().d();
    }

    public Set keySet() {
        return b().e();
    }

    public void putAll(Map map) {
        a(this.h + map.size());
        for (Map.Entry entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public Collection values() {
        return b().f();
    }
}
