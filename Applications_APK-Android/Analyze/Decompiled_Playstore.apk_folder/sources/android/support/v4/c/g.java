package android.support.v4.c;

import java.util.Iterator;

final class g implements Iterator {

    /* renamed from: a  reason: collision with root package name */
    final int f63a;

    /* renamed from: b  reason: collision with root package name */
    int f64b;
    int c;
    boolean d = false;
    final /* synthetic */ f e;

    g(f fVar, int i) {
        this.e = fVar;
        this.f63a = i;
        this.f64b = fVar.a();
    }

    public boolean hasNext() {
        return this.c < this.f64b;
    }

    public Object next() {
        Object a2 = this.e.a(this.c, this.f63a);
        this.c++;
        this.d = true;
        return a2;
    }

    public void remove() {
        if (!this.d) {
            throw new IllegalStateException();
        }
        this.c--;
        this.f64b--;
        this.d = false;
        this.e.a(this.c);
    }
}
