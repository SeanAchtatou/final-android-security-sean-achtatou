package android.support.v4.view;

import android.database.DataSetObserver;

class cj extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ViewPager f112a;

    private cj(ViewPager viewPager) {
        this.f112a = viewPager;
    }

    /* synthetic */ cj(ViewPager viewPager, ca caVar) {
        this(viewPager);
    }

    public void onChanged() {
        this.f112a.b();
    }

    public void onInvalidated() {
        this.f112a.b();
    }
}
