package android.support.v4.view.a;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

class o implements u {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f85a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ n f86b;

    o(n nVar, l lVar) {
        this.f86b = nVar;
        this.f85a = lVar;
    }

    public Object a(int i) {
        a a2 = this.f85a.a(i);
        if (a2 == null) {
            return null;
        }
        return a2.a();
    }

    public List a(String str, int i) {
        List a2 = this.f85a.a(str, i);
        ArrayList arrayList = new ArrayList();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            arrayList.add(((a) a2.get(i2)).a());
        }
        return arrayList;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return this.f85a.a(i, i2, bundle);
    }
}
