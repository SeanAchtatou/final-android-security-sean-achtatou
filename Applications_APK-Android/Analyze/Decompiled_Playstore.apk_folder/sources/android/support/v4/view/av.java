package android.support.v4.view;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.WeakHashMap;

class av implements bd {

    /* renamed from: a  reason: collision with root package name */
    WeakHashMap f101a = null;

    av() {
    }

    public int a(View view) {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return 10;
    }

    public void a(View view, float f) {
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        view.invalidate(i, i2, i3, i4);
    }

    public void a(View view, int i, Paint paint) {
    }

    public void a(View view, Paint paint) {
    }

    public void a(View view, a aVar) {
    }

    public void a(View view, Runnable runnable) {
        view.postDelayed(runnable, a());
    }

    public void a(ViewGroup viewGroup, boolean z) {
    }

    public boolean a(View view, int i) {
        return false;
    }

    public void b(View view) {
        view.invalidate();
    }

    public void b(View view, float f) {
    }

    public boolean b(View view, int i) {
        return false;
    }

    public int c(View view) {
        return 0;
    }

    public void c(View view, float f) {
    }

    public void c(View view, int i) {
    }

    public int d(View view) {
        return 0;
    }

    public int e(View view) {
        return 0;
    }

    public ViewParent f(View view) {
        return view.getParent();
    }

    public boolean g(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    public float h(View view) {
        return 0.0f;
    }

    public boolean i(View view) {
        return false;
    }
}
