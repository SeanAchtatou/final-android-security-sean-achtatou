package android.support.v4.view.a;

import android.os.Build;
import android.os.Bundle;
import java.util.List;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private static final m f83a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f84b;

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f83a = new p();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f83a = new n();
        } else {
            f83a = new r();
        }
    }

    public l() {
        this.f84b = f83a.a(this);
    }

    public l(Object obj) {
        this.f84b = obj;
    }

    public a a(int i) {
        return null;
    }

    public Object a() {
        return this.f84b;
    }

    public List a(String str, int i) {
        return null;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }
}
