package android.support.v4.view.a;

import android.graphics.Rect;
import android.view.View;

class c extends h {
    c() {
    }

    public Object a(Object obj) {
        return i.a(obj);
    }

    public void a(Object obj, int i) {
        i.a(obj, i);
    }

    public void a(Object obj, Rect rect) {
        i.a(obj, rect);
    }

    public void a(Object obj, View view) {
        i.a(obj, view);
    }

    public void a(Object obj, CharSequence charSequence) {
        i.a(obj, charSequence);
    }

    public void a(Object obj, boolean z) {
        i.a(obj, z);
    }

    public int b(Object obj) {
        return i.b(obj);
    }

    public void b(Object obj, Rect rect) {
        i.b(obj, rect);
    }

    public void b(Object obj, View view) {
        i.b(obj, view);
    }

    public void b(Object obj, CharSequence charSequence) {
        i.b(obj, charSequence);
    }

    public void b(Object obj, boolean z) {
        i.b(obj, z);
    }

    public CharSequence c(Object obj) {
        return i.c(obj);
    }

    public void c(Object obj, Rect rect) {
        i.c(obj, rect);
    }

    public void c(Object obj, View view) {
        i.c(obj, view);
    }

    public void c(Object obj, CharSequence charSequence) {
        i.c(obj, charSequence);
    }

    public void c(Object obj, boolean z) {
        i.c(obj, z);
    }

    public CharSequence d(Object obj) {
        return i.d(obj);
    }

    public void d(Object obj, Rect rect) {
        i.d(obj, rect);
    }

    public void d(Object obj, boolean z) {
        i.d(obj, z);
    }

    public CharSequence e(Object obj) {
        return i.e(obj);
    }

    public void e(Object obj, boolean z) {
        i.e(obj, z);
    }

    public CharSequence f(Object obj) {
        return i.f(obj);
    }

    public void f(Object obj, boolean z) {
        i.f(obj, z);
    }

    public void g(Object obj, boolean z) {
        i.g(obj, z);
    }

    public boolean g(Object obj) {
        return i.g(obj);
    }

    public boolean h(Object obj) {
        return i.h(obj);
    }

    public boolean i(Object obj) {
        return i.i(obj);
    }

    public boolean j(Object obj) {
        return i.j(obj);
    }

    public boolean k(Object obj) {
        return i.k(obj);
    }

    public boolean l(Object obj) {
        return i.l(obj);
    }

    public boolean m(Object obj) {
        return i.m(obj);
    }

    public boolean n(Object obj) {
        return i.n(obj);
    }

    public boolean o(Object obj) {
        return i.o(obj);
    }

    public boolean p(Object obj) {
        return i.p(obj);
    }

    public void q(Object obj) {
        i.q(obj);
    }
}
