package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class at {

    /* renamed from: a  reason: collision with root package name */
    static final bd f100a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f100a = new au();
        } else if (i >= 19) {
            f100a = new bc();
        } else if (i >= 17) {
            f100a = new bb();
        } else if (i >= 16) {
            f100a = new ba();
        } else if (i >= 14) {
            f100a = new az();
        } else if (i >= 11) {
            f100a = new ay();
        } else if (i >= 9) {
            f100a = new ax();
        } else if (i >= 7) {
            f100a = new aw();
        } else {
            f100a = new av();
        }
    }

    public static int a(View view) {
        return f100a.a(view);
    }

    public static void a(View view, float f) {
        f100a.b(view, f);
    }

    public static void a(View view, int i, int i2, int i3, int i4) {
        f100a.a(view, i, i2, i3, i4);
    }

    public static void a(View view, int i, Paint paint) {
        f100a.a(view, i, paint);
    }

    public static void a(View view, Paint paint) {
        f100a.a(view, paint);
    }

    public static void a(View view, a aVar) {
        f100a.a(view, aVar);
    }

    public static void a(View view, Runnable runnable) {
        f100a.a(view, runnable);
    }

    public static void a(ViewGroup viewGroup, boolean z) {
        f100a.a(viewGroup, z);
    }

    public static boolean a(View view, int i) {
        return f100a.a(view, i);
    }

    public static void b(View view) {
        f100a.b(view);
    }

    public static void b(View view, float f) {
        f100a.c(view, f);
    }

    public static boolean b(View view, int i) {
        return f100a.b(view, i);
    }

    public static int c(View view) {
        return f100a.c(view);
    }

    public static void c(View view, float f) {
        f100a.a(view, f);
    }

    public static void c(View view, int i) {
        f100a.c(view, i);
    }

    public static int d(View view) {
        return f100a.d(view);
    }

    public static int e(View view) {
        return f100a.e(view);
    }

    public static ViewParent f(View view) {
        return f100a.f(view);
    }

    public static boolean g(View view) {
        return f100a.g(view);
    }

    public static float h(View view) {
        return f100a.h(view);
    }

    public static boolean i(View view) {
        return f100a.i(view);
    }
}
