package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

interface bd {
    int a(View view);

    void a(View view, float f);

    void a(View view, int i, int i2, int i3, int i4);

    void a(View view, int i, Paint paint);

    void a(View view, Paint paint);

    void a(View view, a aVar);

    void a(View view, Runnable runnable);

    void a(ViewGroup viewGroup, boolean z);

    boolean a(View view, int i);

    void b(View view);

    void b(View view, float f);

    boolean b(View view, int i);

    int c(View view);

    void c(View view, float f);

    void c(View view, int i);

    int d(View view);

    int e(View view);

    ViewParent f(View view);

    boolean g(View view);

    float h(View view);

    boolean i(View view);
}
