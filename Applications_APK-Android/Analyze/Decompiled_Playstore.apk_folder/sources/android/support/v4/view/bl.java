package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

public class bl {

    /* renamed from: a  reason: collision with root package name */
    static final bq f104a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f104a = new bp();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f104a = new bo();
        } else if (Build.VERSION.SDK_INT >= 8) {
            f104a = new bn();
        } else {
            f104a = new bm();
        }
    }

    public static int a(ViewConfiguration viewConfiguration) {
        return f104a.a(viewConfiguration);
    }
}
