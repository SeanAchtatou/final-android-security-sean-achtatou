package android.support.v4.view;

import android.support.v4.view.a.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

class c implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f106a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f107b;

    c(b bVar, a aVar) {
        this.f107b = bVar;
        this.f106a = aVar;
    }

    public void a(View view, int i) {
        this.f106a.a(view, i);
    }

    public void a(View view, Object obj) {
        this.f106a.a(view, new a(obj));
    }

    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.f106a.b(view, accessibilityEvent);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f106a.a(viewGroup, view, accessibilityEvent);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        this.f106a.d(view, accessibilityEvent);
    }

    public void c(View view, AccessibilityEvent accessibilityEvent) {
        this.f106a.c(view, accessibilityEvent);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        this.f106a.a(view, accessibilityEvent);
    }
}
