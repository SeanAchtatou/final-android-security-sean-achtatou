package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

public class ao {

    /* renamed from: a  reason: collision with root package name */
    static final ar f99a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f99a = new aq();
        } else {
            f99a = new ap();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return f99a.a(velocityTracker, i);
    }

    public static float b(VelocityTracker velocityTracker, int i) {
        return f99a.b(velocityTracker, i);
    }
}
