package android.support.v4.view;

import android.content.Context;
import android.text.method.SingleLineTransformationMethod;
import android.view.View;
import java.util.Locale;

class an extends SingleLineTransformationMethod {

    /* renamed from: a  reason: collision with root package name */
    private Locale f98a;

    public an(Context context) {
        this.f98a = context.getResources().getConfiguration().locale;
    }

    public CharSequence getTransformation(CharSequence charSequence, View view) {
        CharSequence transformation = super.getTransformation(charSequence, view);
        if (transformation != null) {
            return transformation.toString().toUpperCase(this.f98a);
        }
        return null;
    }
}
