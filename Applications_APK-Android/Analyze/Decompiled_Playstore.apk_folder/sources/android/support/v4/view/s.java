package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

public class s {

    /* renamed from: a  reason: collision with root package name */
    static final w f120a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f120a = new v();
        } else {
            f120a = new t();
        }
    }

    public static boolean a(KeyEvent keyEvent) {
        return f120a.b(keyEvent.getMetaState());
    }

    public static boolean a(KeyEvent keyEvent, int i) {
        return f120a.a(keyEvent.getMetaState(), i);
    }

    public static void b(KeyEvent keyEvent) {
        f120a.a(keyEvent);
    }
}
