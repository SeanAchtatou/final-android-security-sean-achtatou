package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.y;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

class cf extends a {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ViewPager f111b;

    cf(ViewPager viewPager) {
        this.f111b = viewPager;
    }

    private boolean b() {
        return this.f111b.h != null && this.f111b.h.a() > 1;
    }

    public void a(View view, a aVar) {
        super.a(view, aVar);
        aVar.b(ViewPager.class.getName());
        aVar.i(b());
        if (this.f111b.canScrollHorizontally(1)) {
            aVar.a(4096);
        }
        if (this.f111b.canScrollHorizontally(-1)) {
            aVar.a(8192);
        }
    }

    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (!this.f111b.canScrollHorizontally(1)) {
                    return false;
                }
                this.f111b.setCurrentItem(this.f111b.i + 1);
                return true;
            case 8192:
                if (!this.f111b.canScrollHorizontally(-1)) {
                    return false;
                }
                this.f111b.setCurrentItem(this.f111b.i - 1);
                return true;
            default:
                return false;
        }
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        super.d(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
        y a2 = y.a();
        a2.a(b());
        if (accessibilityEvent.getEventType() == 4096 && this.f111b.h != null) {
            a2.a(this.f111b.h.a());
            a2.b(this.f111b.i);
            a2.c(this.f111b.i);
        }
    }
}
