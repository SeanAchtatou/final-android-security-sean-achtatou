package android.support.v4.view;

import android.animation.ValueAnimator;
import android.graphics.Paint;
import android.view.View;

class bh {
    public static int a(View view) {
        return view.getLayerType();
    }

    static long a() {
        return ValueAnimator.getFrameDelay();
    }

    public static void a(View view, float f) {
        view.setScaleX(f);
    }

    public static void a(View view, int i, Paint paint) {
        view.setLayerType(i, paint);
    }

    public static float b(View view) {
        return view.getScaleX();
    }

    public static void b(View view, float f) {
        view.setScaleY(f);
    }
}
