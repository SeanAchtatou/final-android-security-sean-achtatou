package android.support.v4.view.a;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

final class t extends AccessibilityNodeProvider {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f89a;

    t(u uVar) {
        this.f89a = uVar;
    }

    public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
        return (AccessibilityNodeInfo) this.f89a.a(i);
    }

    public List findAccessibilityNodeInfosByText(String str, int i) {
        return this.f89a.a(str, i);
    }

    public boolean performAction(int i, int i2, Bundle bundle) {
        return this.f89a.a(i, i2, bundle);
    }
}
