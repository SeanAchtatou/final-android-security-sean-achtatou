package android.support.v4.view.a;

import android.os.Build;

public class y {

    /* renamed from: a  reason: collision with root package name */
    private static final ab f91a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f92b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f91a = new ac();
        } else if (Build.VERSION.SDK_INT >= 15) {
            f91a = new aa();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f91a = new z();
        } else {
            f91a = new ad();
        }
    }

    public y(Object obj) {
        this.f92b = obj;
    }

    public static y a() {
        return new y(f91a.a());
    }

    public void a(int i) {
        f91a.b(this.f92b, i);
    }

    public void a(boolean z) {
        f91a.a(this.f92b, z);
    }

    public void b(int i) {
        f91a.a(this.f92b, i);
    }

    public void c(int i) {
        f91a.c(this.f92b, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        y yVar = (y) obj;
        return this.f92b == null ? yVar.f92b == null : this.f92b.equals(yVar.f92b);
    }

    public int hashCode() {
        if (this.f92b == null) {
            return 0;
        }
        return this.f92b.hashCode();
    }
}
