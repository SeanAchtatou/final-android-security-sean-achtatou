package android.support.v4.view.a;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final d f81a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f82b;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f81a = new b();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f81a = new g();
        } else if (Build.VERSION.SDK_INT >= 18) {
            f81a = new f();
        } else if (Build.VERSION.SDK_INT >= 16) {
            f81a = new e();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f81a = new c();
        } else {
            f81a = new h();
        }
    }

    public a(Object obj) {
        this.f82b = obj;
    }

    public static a a(a aVar) {
        return a(f81a.a(aVar.f82b));
    }

    static a a(Object obj) {
        if (obj != null) {
            return new a(obj);
        }
        return null;
    }

    private static String c(int i) {
        switch (i) {
            case 1:
                return "ACTION_FOCUS";
            case 2:
                return "ACTION_CLEAR_FOCUS";
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    public Object a() {
        return this.f82b;
    }

    public void a(int i) {
        f81a.a(this.f82b, i);
    }

    public void a(Rect rect) {
        f81a.a(this.f82b, rect);
    }

    public void a(View view) {
        f81a.c(this.f82b, view);
    }

    public void a(CharSequence charSequence) {
        f81a.c(this.f82b, charSequence);
    }

    public void a(boolean z) {
        f81a.c(this.f82b, z);
    }

    public int b() {
        return f81a.b(this.f82b);
    }

    public void b(int i) {
        f81a.b(this.f82b, i);
    }

    public void b(Rect rect) {
        f81a.c(this.f82b, rect);
    }

    public void b(View view) {
        f81a.a(this.f82b, view);
    }

    public void b(CharSequence charSequence) {
        f81a.a(this.f82b, charSequence);
    }

    public void b(boolean z) {
        f81a.d(this.f82b, z);
    }

    public int c() {
        return f81a.r(this.f82b);
    }

    public void c(Rect rect) {
        f81a.b(this.f82b, rect);
    }

    public void c(View view) {
        f81a.b(this.f82b, view);
    }

    public void c(CharSequence charSequence) {
        f81a.b(this.f82b, charSequence);
    }

    public void c(boolean z) {
        f81a.h(this.f82b, z);
    }

    public void d(Rect rect) {
        f81a.d(this.f82b, rect);
    }

    public void d(boolean z) {
        f81a.i(this.f82b, z);
    }

    public boolean d() {
        return f81a.g(this.f82b);
    }

    public void e(boolean z) {
        f81a.g(this.f82b, z);
    }

    public boolean e() {
        return f81a.h(this.f82b);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        a aVar = (a) obj;
        return this.f82b == null ? aVar.f82b == null : this.f82b.equals(aVar.f82b);
    }

    public void f(boolean z) {
        f81a.a(this.f82b, z);
    }

    public boolean f() {
        return f81a.k(this.f82b);
    }

    public void g(boolean z) {
        f81a.e(this.f82b, z);
    }

    public boolean g() {
        return f81a.l(this.f82b);
    }

    public void h(boolean z) {
        f81a.b(this.f82b, z);
    }

    public boolean h() {
        return f81a.s(this.f82b);
    }

    public int hashCode() {
        if (this.f82b == null) {
            return 0;
        }
        return this.f82b.hashCode();
    }

    public void i(boolean z) {
        f81a.f(this.f82b, z);
    }

    public boolean i() {
        return f81a.t(this.f82b);
    }

    public boolean j() {
        return f81a.p(this.f82b);
    }

    public boolean k() {
        return f81a.i(this.f82b);
    }

    public boolean l() {
        return f81a.m(this.f82b);
    }

    public boolean m() {
        return f81a.j(this.f82b);
    }

    public boolean n() {
        return f81a.n(this.f82b);
    }

    public boolean o() {
        return f81a.o(this.f82b);
    }

    public CharSequence p() {
        return f81a.e(this.f82b);
    }

    public CharSequence q() {
        return f81a.c(this.f82b);
    }

    public CharSequence r() {
        return f81a.f(this.f82b);
    }

    public CharSequence s() {
        return f81a.d(this.f82b);
    }

    public void t() {
        f81a.q(this.f82b);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        c(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ").append(p());
        sb.append("; className: ").append(q());
        sb.append("; text: ").append(r());
        sb.append("; contentDescription: ").append(s());
        sb.append("; viewId: ").append(u());
        sb.append("; checkable: ").append(d());
        sb.append("; checked: ").append(e());
        sb.append("; focusable: ").append(f());
        sb.append("; focused: ").append(g());
        sb.append("; selected: ").append(j());
        sb.append("; clickable: ").append(k());
        sb.append("; longClickable: ").append(l());
        sb.append("; enabled: ").append(m());
        sb.append("; password: ").append(n());
        sb.append("; scrollable: " + o());
        sb.append("; [");
        int b2 = b();
        while (b2 != 0) {
            int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(b2);
            b2 &= numberOfTrailingZeros ^ -1;
            sb.append(c(numberOfTrailingZeros));
            if (b2 != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public String u() {
        return f81a.u(this.f82b);
    }
}
