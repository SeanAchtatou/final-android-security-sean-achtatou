package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class ay extends ax {
    ay() {
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return bh.a();
    }

    public void a(View view, int i, Paint paint) {
        bh.a(view, i, paint);
    }

    public void a(View view, Paint paint) {
        a(view, d(view), paint);
        view.invalidate();
    }

    public void b(View view, float f) {
        bh.a(view, f);
    }

    public void c(View view, float f) {
        bh.b(view, f);
    }

    public int d(View view) {
        return bh.a(view);
    }

    public float h(View view) {
        return bh.b(view);
    }
}
