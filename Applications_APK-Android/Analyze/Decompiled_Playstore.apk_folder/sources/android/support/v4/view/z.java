package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;

public class z {

    /* renamed from: a  reason: collision with root package name */
    static final ac f121a;

    static {
        if (Build.VERSION.SDK_INT >= 5) {
            f121a = new ab();
        } else {
            f121a = new aa();
        }
    }

    public static int a(MotionEvent motionEvent) {
        return motionEvent.getAction() & 255;
    }

    public static int a(MotionEvent motionEvent, int i) {
        return f121a.a(motionEvent, i);
    }

    public static int b(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static int b(MotionEvent motionEvent, int i) {
        return f121a.b(motionEvent, i);
    }

    public static float c(MotionEvent motionEvent, int i) {
        return f121a.c(motionEvent, i);
    }

    public static int c(MotionEvent motionEvent) {
        return f121a.a(motionEvent);
    }

    public static float d(MotionEvent motionEvent, int i) {
        return f121a.d(motionEvent, i);
    }
}
