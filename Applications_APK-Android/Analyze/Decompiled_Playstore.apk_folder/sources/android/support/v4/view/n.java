package android.support.v4.view;

import android.os.Build;

public class n {

    /* renamed from: a  reason: collision with root package name */
    static final o f119a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f119a = new q();
        } else {
            f119a = new p();
        }
    }

    public static int a(int i, int i2) {
        return f119a.a(i, i2);
    }
}
