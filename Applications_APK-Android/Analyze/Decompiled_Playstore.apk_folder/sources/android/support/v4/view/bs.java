package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

public class bs {

    /* renamed from: a  reason: collision with root package name */
    static final bw f105a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f105a = new bt();
        } else if (i >= 18) {
            f105a = new bx();
        } else if (i >= 14) {
            f105a = new bv();
        } else if (i >= 11) {
            f105a = new bu();
        } else {
            f105a = new by();
        }
    }

    public static void a(ViewGroup viewGroup, boolean z) {
        f105a.a(viewGroup, z);
    }
}
