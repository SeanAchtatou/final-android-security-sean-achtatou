package android.support.v4.view;

import android.database.DataSetObserver;

class ai extends DataSetObserver implements cg, ch {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PagerTitleStrip f96a;

    /* renamed from: b  reason: collision with root package name */
    private int f97b;

    private ai(PagerTitleStrip pagerTitleStrip) {
        this.f96a = pagerTitleStrip;
    }

    public void a(int i) {
        float f = 0.0f;
        if (this.f97b == 0) {
            this.f96a.a(this.f96a.f73a.getCurrentItem(), this.f96a.f73a.getAdapter());
            if (this.f96a.g >= 0.0f) {
                f = this.f96a.g;
            }
            this.f96a.a(this.f96a.f73a.getCurrentItem(), f, true);
        }
    }

    public void a(int i, float f, int i2) {
        if (f > 0.5f) {
            i++;
        }
        this.f96a.a(i, f, false);
    }

    public void a(ae aeVar, ae aeVar2) {
        this.f96a.a(aeVar, aeVar2);
    }

    public void b(int i) {
        this.f97b = i;
    }

    public void onChanged() {
        float f = 0.0f;
        this.f96a.a(this.f96a.f73a.getCurrentItem(), this.f96a.f73a.getAdapter());
        if (this.f96a.g >= 0.0f) {
            f = this.f96a.g;
        }
        this.f96a.a(this.f96a.f73a.getCurrentItem(), f, true);
    }
}
