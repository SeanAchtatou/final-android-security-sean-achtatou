package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.l;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

class f implements m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f115a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ e f116b;

    f(e eVar, a aVar) {
        this.f116b = eVar;
        this.f115a = aVar;
    }

    public Object a(View view) {
        l a2 = this.f115a.a(view);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    public void a(View view, int i) {
        this.f115a.a(view, i);
    }

    public void a(View view, Object obj) {
        this.f115a.a(view, new a(obj));
    }

    public boolean a(View view, int i, Bundle bundle) {
        return this.f115a.a(view, i, bundle);
    }

    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.f115a.b(view, accessibilityEvent);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f115a.a(viewGroup, view, accessibilityEvent);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        this.f115a.d(view, accessibilityEvent);
    }

    public void c(View view, AccessibilityEvent accessibilityEvent) {
        this.f115a.c(view, accessibilityEvent);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        this.f115a.a(view, accessibilityEvent);
    }
}
