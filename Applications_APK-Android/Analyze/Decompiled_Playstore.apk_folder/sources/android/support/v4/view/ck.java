package android.support.v4.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.b.a;
import android.view.View;

public class ck extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = a.a(new cl());

    /* renamed from: a  reason: collision with root package name */
    int f113a;

    /* renamed from: b  reason: collision with root package name */
    Parcelable f114b;
    ClassLoader c;

    ck(Parcel parcel, ClassLoader classLoader) {
        super(parcel);
        classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
        this.f113a = parcel.readInt();
        this.f114b = parcel.readParcelable(classLoader);
        this.c = classLoader;
    }

    public ck(Parcelable parcelable) {
        super(parcelable);
    }

    public String toString() {
        return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.f113a + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.f113a);
        parcel.writeParcelable(this.f114b, i);
    }
}
