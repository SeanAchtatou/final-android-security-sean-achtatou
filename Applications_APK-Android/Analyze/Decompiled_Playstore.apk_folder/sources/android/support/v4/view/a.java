package android.support.v4.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.l;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class a {

    /* renamed from: b  reason: collision with root package name */
    private static final d f79b;
    private static final Object c = f79b.a();

    /* renamed from: a  reason: collision with root package name */
    final Object f80a = f79b.a(this);

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f79b = new e();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f79b = new b();
        } else {
            f79b = new g();
        }
    }

    public l a(View view) {
        return f79b.a(c, view);
    }

    /* access modifiers changed from: package-private */
    public Object a() {
        return this.f80a;
    }

    public void a(View view, int i) {
        f79b.a(c, view, i);
    }

    public void a(View view, android.support.v4.view.a.a aVar) {
        f79b.a(c, view, aVar);
    }

    public void a(View view, AccessibilityEvent accessibilityEvent) {
        f79b.d(c, view, accessibilityEvent);
    }

    public boolean a(View view, int i, Bundle bundle) {
        return f79b.a(c, view, i, bundle);
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return f79b.a(c, viewGroup, view, accessibilityEvent);
    }

    public boolean b(View view, AccessibilityEvent accessibilityEvent) {
        return f79b.a(c, view, accessibilityEvent);
    }

    public void c(View view, AccessibilityEvent accessibilityEvent) {
        f79b.c(c, view, accessibilityEvent);
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        f79b.b(c, view, accessibilityEvent);
    }
}
