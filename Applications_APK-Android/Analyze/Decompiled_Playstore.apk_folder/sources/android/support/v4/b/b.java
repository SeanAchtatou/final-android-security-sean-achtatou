package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

class b implements Parcelable.Creator {

    /* renamed from: a  reason: collision with root package name */
    final c f54a;

    public b(c cVar) {
        this.f54a = cVar;
    }

    public Object createFromParcel(Parcel parcel) {
        return this.f54a.a(parcel, null);
    }

    public Object[] newArray(int i) {
        return this.f54a.a(i);
    }
}
