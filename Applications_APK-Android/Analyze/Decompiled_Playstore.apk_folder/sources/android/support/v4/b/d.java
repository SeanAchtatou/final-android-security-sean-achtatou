package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

class d implements Parcelable.ClassLoaderCreator {

    /* renamed from: a  reason: collision with root package name */
    private final c f55a;

    public d(c cVar) {
        this.f55a = cVar;
    }

    public Object createFromParcel(Parcel parcel) {
        return this.f55a.a(parcel, null);
    }

    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return this.f55a.a(parcel, classLoader);
    }

    public Object[] newArray(int i) {
        return this.f55a.a(i);
    }
}
