package android.support.v4.widget;

import android.support.v4.view.a;
import android.view.View;

final class f extends a {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ DrawerLayout f164b;

    f(DrawerLayout drawerLayout) {
        this.f164b = drawerLayout;
    }

    public void a(View view, android.support.v4.view.a.a aVar) {
        super.a(view, aVar);
        if (!DrawerLayout.m(view)) {
            aVar.c((View) null);
        }
    }
}
