package android.support.v4.widget;

import android.content.Context;
import android.support.v4.view.ao;
import android.support.v4.view.z;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import java.util.Arrays;

public class bd {
    private static final Interpolator v = new be();

    /* renamed from: a  reason: collision with root package name */
    private int f158a;

    /* renamed from: b  reason: collision with root package name */
    private int f159b;
    private int c = -1;
    private float[] d;
    private float[] e;
    private float[] f;
    private float[] g;
    private int[] h;
    private int[] i;
    private int[] j;
    private int k;
    private VelocityTracker l;
    private float m;
    private float n;
    private int o;
    private int p;
    private ad q;
    private final bg r;
    private View s;
    private boolean t;
    private final ViewGroup u;
    private final Runnable w = new bf(this);

    private bd(Context context, ViewGroup viewGroup, bg bgVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (bgVar == null) {
            throw new IllegalArgumentException("Callback may not be null");
        } else {
            this.u = viewGroup;
            this.r = bgVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.f159b = viewConfiguration.getScaledTouchSlop();
            this.m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.q = ad.a(context, v);
        }
    }

    private float a(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        if (abs < f3) {
            return 0.0f;
        }
        return abs > f4 ? f2 <= 0.0f ? -f4 : f4 : f2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private int a(int i2, int i3, int i4) {
        if (i2 == 0) {
            return 0;
        }
        int width = this.u.getWidth();
        int i5 = width / 2;
        float b2 = (b(Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width))) * ((float) i5)) + ((float) i5);
        int abs = Math.abs(i3);
        return Math.min(abs > 0 ? Math.round(Math.abs(b2 / ((float) abs)) * 1000.0f) * 4 : (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f), 600);
    }

    private int a(View view, int i2, int i3, int i4, int i5) {
        int b2 = b(i4, (int) this.n, (int) this.m);
        int b3 = b(i5, (int) this.n, (int) this.m);
        int abs = Math.abs(i2);
        int abs2 = Math.abs(i3);
        int abs3 = Math.abs(b2);
        int abs4 = Math.abs(b3);
        int i6 = abs3 + abs4;
        int i7 = abs + abs2;
        return (int) (((b3 != 0 ? ((float) abs4) / ((float) i6) : ((float) abs2) / ((float) i7)) * ((float) a(i3, b3, this.r.b(view)))) + ((b2 != 0 ? ((float) abs3) / ((float) i6) : ((float) abs) / ((float) i7)) * ((float) a(i2, b2, this.r.a(view)))));
    }

    public static bd a(ViewGroup viewGroup, float f2, bg bgVar) {
        bd a2 = a(viewGroup, bgVar);
        a2.f159b = (int) (((float) a2.f159b) * (1.0f / f2));
        return a2;
    }

    public static bd a(ViewGroup viewGroup, bg bgVar) {
        return new bd(viewGroup.getContext(), viewGroup, bgVar);
    }

    private void a(float f2, float f3) {
        this.t = true;
        this.r.a(this.s, f2, f3);
        this.t = false;
        if (this.f158a == 1) {
            c(0);
        }
    }

    private void a(float f2, float f3, int i2) {
        f(i2);
        float[] fArr = this.d;
        this.f[i2] = f2;
        fArr[i2] = f2;
        float[] fArr2 = this.e;
        this.g[i2] = f3;
        fArr2[i2] = f3;
        this.h[i2] = e((int) f2, (int) f3);
        this.k |= 1 << i2;
    }

    private boolean a(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.h[i2] & i3) != i3 || (this.p & i3) == 0 || (this.j[i2] & i3) == i3 || (this.i[i2] & i3) == i3) {
            return false;
        }
        if (abs <= ((float) this.f159b) && abs2 <= ((float) this.f159b)) {
            return false;
        }
        if (abs >= abs2 * 0.5f || !this.r.b(i3)) {
            return (this.i[i2] & i3) == 0 && abs > ((float) this.f159b);
        }
        int[] iArr = this.j;
        iArr[i2] = iArr[i2] | i3;
        return false;
    }

    private boolean a(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.q.f();
            c(0);
            return false;
        }
        this.q.a(left, top, i6, i7, a(this.s, i6, i7, i4, i5));
        c(2);
        return true;
    }

    private boolean a(View view, float f2, float f3) {
        if (view == null) {
            return false;
        }
        boolean z = this.r.a(view) > 0;
        boolean z2 = this.r.b(view) > 0;
        if (z && z2) {
            return (f2 * f2) + (f3 * f3) > ((float) (this.f159b * this.f159b));
        }
        if (z) {
            return Math.abs(f2) > ((float) this.f159b);
        }
        if (z2) {
            return Math.abs(f3) > ((float) this.f159b);
        }
        return false;
    }

    private float b(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    private int b(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        return abs > i4 ? i2 <= 0 ? -i4 : i4 : i2;
    }

    private void b(float f2, float f3, int i2) {
        int i3 = 1;
        if (!a(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (a(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (a(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (a(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.i;
            iArr[i2] = iArr[i2] | i3;
            this.r.b(i3, i2);
        }
    }

    private void b(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int left = this.s.getLeft();
        int top = this.s.getTop();
        if (i4 != 0) {
            i6 = this.r.a(this.s, i2, i4);
            this.s.offsetLeftAndRight(i6 - left);
        } else {
            i6 = i2;
        }
        if (i5 != 0) {
            i7 = this.r.b(this.s, i3, i5);
            this.s.offsetTopAndBottom(i7 - top);
        } else {
            i7 = i3;
        }
        if (i4 != 0 || i5 != 0) {
            this.r.a(this.s, i6, i7, i6 - left, i7 - top);
        }
    }

    private void c(MotionEvent motionEvent) {
        int c2 = z.c(motionEvent);
        for (int i2 = 0; i2 < c2; i2++) {
            int b2 = z.b(motionEvent, i2);
            float c3 = z.c(motionEvent, i2);
            float d2 = z.d(motionEvent, i2);
            this.f[b2] = c3;
            this.g[b2] = d2;
        }
    }

    private int e(int i2, int i3) {
        int i4 = 0;
        if (i2 < this.u.getLeft() + this.o) {
            i4 = 1;
        }
        if (i3 < this.u.getTop() + this.o) {
            i4 |= 4;
        }
        if (i2 > this.u.getRight() - this.o) {
            i4 |= 2;
        }
        return i3 > this.u.getBottom() - this.o ? i4 | 8 : i4;
    }

    private void e(int i2) {
        if (this.d != null) {
            this.d[i2] = 0.0f;
            this.e[i2] = 0.0f;
            this.f[i2] = 0.0f;
            this.g[i2] = 0.0f;
            this.h[i2] = 0;
            this.i[i2] = 0;
            this.j[i2] = 0;
            this.k &= (1 << i2) ^ -1;
        }
    }

    private void f(int i2) {
        if (this.d == null || this.d.length <= i2) {
            float[] fArr = new float[(i2 + 1)];
            float[] fArr2 = new float[(i2 + 1)];
            float[] fArr3 = new float[(i2 + 1)];
            float[] fArr4 = new float[(i2 + 1)];
            int[] iArr = new int[(i2 + 1)];
            int[] iArr2 = new int[(i2 + 1)];
            int[] iArr3 = new int[(i2 + 1)];
            if (this.d != null) {
                System.arraycopy(this.d, 0, fArr, 0, this.d.length);
                System.arraycopy(this.e, 0, fArr2, 0, this.e.length);
                System.arraycopy(this.f, 0, fArr3, 0, this.f.length);
                System.arraycopy(this.g, 0, fArr4, 0, this.g.length);
                System.arraycopy(this.h, 0, iArr, 0, this.h.length);
                System.arraycopy(this.i, 0, iArr2, 0, this.i.length);
                System.arraycopy(this.j, 0, iArr3, 0, this.j.length);
            }
            this.d = fArr;
            this.e = fArr2;
            this.f = fArr3;
            this.g = fArr4;
            this.h = iArr;
            this.i = iArr2;
            this.j = iArr3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    private void g() {
        if (this.d != null) {
            Arrays.fill(this.d, 0.0f);
            Arrays.fill(this.e, 0.0f);
            Arrays.fill(this.f, 0.0f);
            Arrays.fill(this.g, 0.0f);
            Arrays.fill(this.h, 0);
            Arrays.fill(this.i, 0);
            Arrays.fill(this.j, 0);
            this.k = 0;
        }
    }

    private void h() {
        this.l.computeCurrentVelocity(1000, this.m);
        a(a(ao.a(this.l, this.c), this.n, this.m), a(ao.b(this.l, this.c), this.n, this.m));
    }

    public int a() {
        return this.f158a;
    }

    public void a(float f2) {
        this.n = f2;
    }

    public void a(int i2) {
        this.p = i2;
    }

    public void a(View view, int i2) {
        if (view.getParent() != this.u) {
            throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.u + ")");
        }
        this.s = view;
        this.c = i2;
        this.r.b(view, i2);
        c(1);
    }

    public boolean a(int i2, int i3) {
        if (this.t) {
            return a(i2, i3, (int) ao.a(this.l, this.c), (int) ao.b(this.l, this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ed, code lost:
        if (r8 != r7) goto L_0x00fc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.MotionEvent r14) {
        /*
            r13 = this;
            int r0 = android.support.v4.view.z.a(r14)
            int r1 = android.support.v4.view.z.b(r14)
            if (r0 != 0) goto L_0x000d
            r13.e()
        L_0x000d:
            android.view.VelocityTracker r2 = r13.l
            if (r2 != 0) goto L_0x0017
            android.view.VelocityTracker r2 = android.view.VelocityTracker.obtain()
            r13.l = r2
        L_0x0017:
            android.view.VelocityTracker r2 = r13.l
            r2.addMovement(r14)
            switch(r0) {
                case 0: goto L_0x0026;
                case 1: goto L_0x0119;
                case 2: goto L_0x0092;
                case 3: goto L_0x0119;
                case 4: goto L_0x001f;
                case 5: goto L_0x005a;
                case 6: goto L_0x0110;
                default: goto L_0x001f;
            }
        L_0x001f:
            int r0 = r13.f158a
            r1 = 1
            if (r0 != r1) goto L_0x011e
            r0 = 1
        L_0x0025:
            return r0
        L_0x0026:
            float r0 = r14.getX()
            float r1 = r14.getY()
            r2 = 0
            int r2 = android.support.v4.view.z.b(r14, r2)
            r13.a(r0, r1, r2)
            int r0 = (int) r0
            int r1 = (int) r1
            android.view.View r0 = r13.d(r0, r1)
            android.view.View r1 = r13.s
            if (r0 != r1) goto L_0x0048
            int r1 = r13.f158a
            r3 = 2
            if (r1 != r3) goto L_0x0048
            r13.b(r0, r2)
        L_0x0048:
            int[] r0 = r13.h
            r0 = r0[r2]
            int r1 = r13.p
            r1 = r1 & r0
            if (r1 == 0) goto L_0x001f
            android.support.v4.widget.bg r1 = r13.r
            int r3 = r13.p
            r0 = r0 & r3
            r1.a(r0, r2)
            goto L_0x001f
        L_0x005a:
            int r0 = android.support.v4.view.z.b(r14, r1)
            float r2 = android.support.v4.view.z.c(r14, r1)
            float r1 = android.support.v4.view.z.d(r14, r1)
            r13.a(r2, r1, r0)
            int r3 = r13.f158a
            if (r3 != 0) goto L_0x007f
            int[] r1 = r13.h
            r1 = r1[r0]
            int r2 = r13.p
            r2 = r2 & r1
            if (r2 == 0) goto L_0x001f
            android.support.v4.widget.bg r2 = r13.r
            int r3 = r13.p
            r1 = r1 & r3
            r2.a(r1, r0)
            goto L_0x001f
        L_0x007f:
            int r3 = r13.f158a
            r4 = 2
            if (r3 != r4) goto L_0x001f
            int r2 = (int) r2
            int r1 = (int) r1
            android.view.View r1 = r13.d(r2, r1)
            android.view.View r2 = r13.s
            if (r1 != r2) goto L_0x001f
            r13.b(r1, r0)
            goto L_0x001f
        L_0x0092:
            int r2 = android.support.v4.view.z.c(r14)
            r0 = 0
            r1 = r0
        L_0x0098:
            if (r1 >= r2) goto L_0x00f5
            int r3 = android.support.v4.view.z.b(r14, r1)
            float r0 = android.support.v4.view.z.c(r14, r1)
            float r4 = android.support.v4.view.z.d(r14, r1)
            float[] r5 = r13.d
            r5 = r5[r3]
            float r5 = r0 - r5
            float[] r6 = r13.e
            r6 = r6[r3]
            float r6 = r4 - r6
            int r0 = (int) r0
            int r4 = (int) r4
            android.view.View r4 = r13.d(r0, r4)
            if (r4 == 0) goto L_0x00fa
            boolean r0 = r13.a(r4, r5, r6)
            if (r0 == 0) goto L_0x00fa
            r0 = 1
        L_0x00c1:
            if (r0 == 0) goto L_0x00fc
            int r7 = r4.getLeft()
            int r8 = (int) r5
            int r8 = r8 + r7
            android.support.v4.widget.bg r9 = r13.r
            int r10 = (int) r5
            int r8 = r9.a(r4, r8, r10)
            int r9 = r4.getTop()
            int r10 = (int) r6
            int r10 = r10 + r9
            android.support.v4.widget.bg r11 = r13.r
            int r12 = (int) r6
            int r10 = r11.b(r4, r10, r12)
            android.support.v4.widget.bg r11 = r13.r
            int r11 = r11.a(r4)
            android.support.v4.widget.bg r12 = r13.r
            int r12 = r12.b(r4)
            if (r11 == 0) goto L_0x00ef
            if (r11 <= 0) goto L_0x00fc
            if (r8 != r7) goto L_0x00fc
        L_0x00ef:
            if (r12 == 0) goto L_0x00f5
            if (r12 <= 0) goto L_0x00fc
            if (r10 != r9) goto L_0x00fc
        L_0x00f5:
            r13.c(r14)
            goto L_0x001f
        L_0x00fa:
            r0 = 0
            goto L_0x00c1
        L_0x00fc:
            r13.b(r5, r6, r3)
            int r5 = r13.f158a
            r6 = 1
            if (r5 == r6) goto L_0x00f5
            if (r0 == 0) goto L_0x010c
            boolean r0 = r13.b(r4, r3)
            if (r0 != 0) goto L_0x00f5
        L_0x010c:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0098
        L_0x0110:
            int r0 = android.support.v4.view.z.b(r14, r1)
            r13.e(r0)
            goto L_0x001f
        L_0x0119:
            r13.e()
            goto L_0x001f
        L_0x011e:
            r0 = 0
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.bd.a(android.view.MotionEvent):boolean");
    }

    public boolean a(View view, int i2, int i3) {
        this.s = view;
        this.c = -1;
        boolean a2 = a(i2, i3, 0, 0);
        if (!a2 && this.f158a == 0 && this.s != null) {
            this.s = null;
        }
        return a2;
    }

    public boolean a(boolean z) {
        boolean z2;
        if (this.f158a == 2) {
            boolean e2 = this.q.e();
            int a2 = this.q.a();
            int b2 = this.q.b();
            int left = a2 - this.s.getLeft();
            int top = b2 - this.s.getTop();
            if (left != 0) {
                this.s.offsetLeftAndRight(left);
            }
            if (top != 0) {
                this.s.offsetTopAndBottom(top);
            }
            if (!(left == 0 && top == 0)) {
                this.r.a(this.s, a2, b2, left, top);
            }
            if (e2 && a2 == this.q.c() && b2 == this.q.d()) {
                this.q.f();
                z2 = false;
            } else {
                z2 = e2;
            }
            if (!z2) {
                if (z) {
                    this.u.post(this.w);
                } else {
                    c(0);
                }
            }
        }
        return this.f158a == 2;
    }

    public int b() {
        return this.o;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.bd.a(float, float):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.bd.a(android.view.ViewGroup, android.support.v4.widget.bg):android.support.v4.widget.bd
      android.support.v4.widget.bd.a(android.view.View, int):void
      android.support.v4.widget.bd.a(int, int):boolean
      android.support.v4.widget.bd.a(float, float):void */
    public void b(MotionEvent motionEvent) {
        int i2;
        int i3 = 0;
        int a2 = z.a(motionEvent);
        int b2 = z.b(motionEvent);
        if (a2 == 0) {
            e();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int b3 = z.b(motionEvent, 0);
                View d2 = d((int) x, (int) y);
                a(x, y, b3);
                b(d2, b3);
                int i4 = this.h[b3];
                if ((this.p & i4) != 0) {
                    this.r.a(i4 & this.p, b3);
                    return;
                }
                return;
            case 1:
                if (this.f158a == 1) {
                    h();
                }
                e();
                return;
            case 2:
                if (this.f158a == 1) {
                    int a3 = z.a(motionEvent, this.c);
                    float c2 = z.c(motionEvent, a3);
                    float d3 = z.d(motionEvent, a3);
                    int i5 = (int) (c2 - this.f[this.c]);
                    int i6 = (int) (d3 - this.g[this.c]);
                    b(this.s.getLeft() + i5, this.s.getTop() + i6, i5, i6);
                    c(motionEvent);
                    return;
                }
                int c3 = z.c(motionEvent);
                while (i3 < c3) {
                    int b4 = z.b(motionEvent, i3);
                    float c4 = z.c(motionEvent, i3);
                    float d4 = z.d(motionEvent, i3);
                    float f2 = c4 - this.d[b4];
                    float f3 = d4 - this.e[b4];
                    b(f2, f3, b4);
                    if (this.f158a != 1) {
                        View d5 = d((int) c4, (int) d4);
                        if (!a(d5, f2, f3) || !b(d5, b4)) {
                            i3++;
                        }
                    }
                    c(motionEvent);
                    return;
                }
                c(motionEvent);
                return;
            case 3:
                if (this.f158a == 1) {
                    a(0.0f, 0.0f);
                }
                e();
                return;
            case 4:
            default:
                return;
            case 5:
                int b5 = z.b(motionEvent, b2);
                float c5 = z.c(motionEvent, b2);
                float d6 = z.d(motionEvent, b2);
                a(c5, d6, b5);
                if (this.f158a == 0) {
                    b(d((int) c5, (int) d6), b5);
                    int i7 = this.h[b5];
                    if ((this.p & i7) != 0) {
                        this.r.a(i7 & this.p, b5);
                        return;
                    }
                    return;
                } else if (c((int) c5, (int) d6)) {
                    b(this.s, b5);
                    return;
                } else {
                    return;
                }
            case 6:
                int b6 = z.b(motionEvent, b2);
                if (this.f158a == 1 && b6 == this.c) {
                    int c6 = z.c(motionEvent);
                    while (true) {
                        if (i3 >= c6) {
                            i2 = -1;
                        } else {
                            int b7 = z.b(motionEvent, i3);
                            if (b7 != this.c) {
                                if (d((int) z.c(motionEvent, i3), (int) z.d(motionEvent, i3)) == this.s && b(this.s, b7)) {
                                    i2 = this.c;
                                }
                            }
                            i3++;
                        }
                    }
                    if (i2 == -1) {
                        h();
                    }
                }
                e(b6);
                return;
        }
    }

    public boolean b(int i2) {
        return (this.k & (1 << i2)) != 0;
    }

    public boolean b(int i2, int i3) {
        if (!b(i3)) {
            return false;
        }
        boolean z = (i2 & 1) == 1;
        boolean z2 = (i2 & 2) == 2;
        float f2 = this.f[i3] - this.d[i3];
        float f3 = this.g[i3] - this.e[i3];
        if (z && z2) {
            return (f2 * f2) + (f3 * f3) > ((float) (this.f159b * this.f159b));
        }
        if (z) {
            return Math.abs(f2) > ((float) this.f159b);
        }
        if (z2) {
            return Math.abs(f3) > ((float) this.f159b);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b(View view, int i2) {
        if (view == this.s && this.c == i2) {
            return true;
        }
        if (view == null || !this.r.a(view, i2)) {
            return false;
        }
        this.c = i2;
        a(view, i2);
        return true;
    }

    public boolean b(View view, int i2, int i3) {
        return view != null && i2 >= view.getLeft() && i2 < view.getRight() && i3 >= view.getTop() && i3 < view.getBottom();
    }

    public View c() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        if (this.f158a != i2) {
            this.f158a = i2;
            this.r.a(i2);
            if (this.f158a == 0) {
                this.s = null;
            }
        }
    }

    public boolean c(int i2, int i3) {
        return b(this.s, i2, i3);
    }

    public int d() {
        return this.f159b;
    }

    public View d(int i2, int i3) {
        for (int childCount = this.u.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.u.getChildAt(this.r.c(childCount));
            if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    public boolean d(int i2) {
        int length = this.d.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (b(i2, i3)) {
                return true;
            }
        }
        return false;
    }

    public void e() {
        this.c = -1;
        g();
        if (this.l != null) {
            this.l.recycle();
            this.l = null;
        }
    }

    public void f() {
        e();
        if (this.f158a == 2) {
            int a2 = this.q.a();
            int b2 = this.q.b();
            this.q.f();
            int a3 = this.q.a();
            int b3 = this.q.b();
            this.r.a(this.s, a3, b3, a3 - a2, b3 - b2);
        }
        c(0);
    }
}
