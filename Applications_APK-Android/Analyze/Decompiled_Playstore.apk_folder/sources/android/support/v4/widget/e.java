package android.support.v4.widget;

import android.graphics.Rect;
import android.support.v4.view.a;
import android.support.v4.view.at;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

class e extends a {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ DrawerLayout f163b;
    private final Rect c = new Rect();

    e(DrawerLayout drawerLayout) {
        this.f163b = drawerLayout;
    }

    private void a(android.support.v4.view.a.a aVar, android.support.v4.view.a.a aVar2) {
        Rect rect = this.c;
        aVar2.a(rect);
        aVar.b(rect);
        aVar2.c(rect);
        aVar.d(rect);
        aVar.c(aVar2.h());
        aVar.a(aVar2.p());
        aVar.b(aVar2.q());
        aVar.c(aVar2.s());
        aVar.h(aVar2.m());
        aVar.f(aVar2.k());
        aVar.a(aVar2.f());
        aVar.b(aVar2.g());
        aVar.d(aVar2.i());
        aVar.e(aVar2.j());
        aVar.g(aVar2.l());
        aVar.a(aVar2.b());
    }

    private void a(android.support.v4.view.a.a aVar, ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (DrawerLayout.m(childAt)) {
                aVar.b(childAt);
            }
        }
    }

    public void a(View view, android.support.v4.view.a.a aVar) {
        if (DrawerLayout.c) {
            super.a(view, aVar);
        } else {
            android.support.v4.view.a.a a2 = android.support.v4.view.a.a.a(aVar);
            super.a(view, a2);
            aVar.a(view);
            ViewParent f = at.f(view);
            if (f instanceof View) {
                aVar.c((View) f);
            }
            a(aVar, a2);
            a2.t();
            a(aVar, (ViewGroup) view);
        }
        aVar.b(DrawerLayout.class.getName());
    }

    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        if (DrawerLayout.c || DrawerLayout.m(view)) {
            return super.a(viewGroup, view, accessibilityEvent);
        }
        return false;
    }

    public boolean b(View view, AccessibilityEvent accessibilityEvent) {
        CharSequence a2;
        if (accessibilityEvent.getEventType() != 32) {
            return super.b(view, accessibilityEvent);
        }
        List<CharSequence> text = accessibilityEvent.getText();
        View a3 = this.f163b.h();
        if (!(a3 == null || (a2 = this.f163b.a(this.f163b.e(a3))) == null)) {
            text.add(a2);
        }
        return true;
    }

    public void d(View view, AccessibilityEvent accessibilityEvent) {
        super.d(view, accessibilityEvent);
        accessibilityEvent.setClassName(DrawerLayout.class.getName());
    }
}
