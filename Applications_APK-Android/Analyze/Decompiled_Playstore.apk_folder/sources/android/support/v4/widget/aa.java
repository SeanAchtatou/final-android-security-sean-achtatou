package android.support.v4.widget;

import android.view.animation.AccelerateDecelerateInterpolator;

class aa extends AccelerateDecelerateInterpolator {
    private aa() {
    }

    /* synthetic */ aa(x xVar) {
        this();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public float getInterpolation(float f) {
        return super.getInterpolation(Math.max(0.0f, (f - 0.5f) * 2.0f));
    }
}
