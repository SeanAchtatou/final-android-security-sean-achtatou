package android.support.v4.widget;

import android.content.Context;
import android.view.animation.Interpolator;

class ag implements ae {
    ag() {
    }

    public int a(Object obj) {
        return ai.a(obj);
    }

    public Object a(Context context, Interpolator interpolator) {
        return ai.a(context, interpolator);
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5) {
        ai.a(obj, i, i2, i3, i4, i5);
    }

    public int b(Object obj) {
        return ai.b(obj);
    }

    public boolean c(Object obj) {
        return ai.c(obj);
    }

    public void d(Object obj) {
        ai.d(obj);
    }

    public int e(Object obj) {
        return ai.e(obj);
    }

    public int f(Object obj) {
        return ai.f(obj);
    }
}
