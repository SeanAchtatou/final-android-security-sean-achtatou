package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class bb extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwipeRefreshLayout f157a;

    bb(SwipeRefreshLayout swipeRefreshLayout) {
        this.f157a = swipeRefreshLayout;
    }

    public void applyTransformation(float f, Transformation transformation) {
        this.f157a.setAnimationProgress(this.f157a.u + ((-this.f157a.u) * f));
        this.f157a.a(f);
    }
}
