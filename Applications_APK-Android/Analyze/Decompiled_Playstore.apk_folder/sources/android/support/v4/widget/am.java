package android.support.v4.widget;

import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;

class am extends bg {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlidingPaneLayout f143a;

    private am(SlidingPaneLayout slidingPaneLayout) {
        this.f143a = slidingPaneLayout;
    }

    public int a(View view) {
        return this.f143a.k;
    }

    public int a(View view, int i, int i2) {
        SlidingPaneLayout.LayoutParams layoutParams = (SlidingPaneLayout.LayoutParams) this.f143a.h.getLayoutParams();
        if (this.f143a.f()) {
            int width = this.f143a.getWidth() - ((layoutParams.rightMargin + this.f143a.getPaddingRight()) + this.f143a.h.getWidth());
            return Math.max(Math.min(i, width), width - this.f143a.k);
        }
        int paddingLeft = layoutParams.leftMargin + this.f143a.getPaddingLeft();
        return Math.min(Math.max(i, paddingLeft), this.f143a.k + paddingLeft);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean
     arg types: [android.support.v4.widget.SlidingPaneLayout, int]
     candidates:
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, int):void
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, android.view.View):void
      android.support.v4.widget.SlidingPaneLayout.a(android.view.View, int):boolean
      android.support.v4.widget.SlidingPaneLayout.a(float, int):boolean
      android.support.v4.widget.SlidingPaneLayout.a(android.support.v4.widget.SlidingPaneLayout, boolean):boolean */
    public void a(int i) {
        if (this.f143a.q.a() != 0) {
            return;
        }
        if (this.f143a.i == 0.0f) {
            this.f143a.d(this.f143a.h);
            this.f143a.c(this.f143a.h);
            boolean unused = this.f143a.r = false;
            return;
        }
        this.f143a.b(this.f143a.h);
        boolean unused2 = this.f143a.r = true;
    }

    public void a(View view, float f, float f2) {
        int paddingLeft;
        SlidingPaneLayout.LayoutParams layoutParams = (SlidingPaneLayout.LayoutParams) view.getLayoutParams();
        if (this.f143a.f()) {
            int paddingRight = layoutParams.rightMargin + this.f143a.getPaddingRight();
            if (f < 0.0f || (f == 0.0f && this.f143a.i > 0.5f)) {
                paddingRight += this.f143a.k;
            }
            paddingLeft = (this.f143a.getWidth() - paddingRight) - this.f143a.h.getWidth();
        } else {
            paddingLeft = layoutParams.leftMargin + this.f143a.getPaddingLeft();
            if (f > 0.0f || (f == 0.0f && this.f143a.i > 0.5f)) {
                paddingLeft += this.f143a.k;
            }
        }
        this.f143a.q.a(paddingLeft, view.getTop());
        this.f143a.invalidate();
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        this.f143a.a(i);
        this.f143a.invalidate();
    }

    public boolean a(View view, int i) {
        if (this.f143a.l) {
            return false;
        }
        return ((SlidingPaneLayout.LayoutParams) view.getLayoutParams()).f131b;
    }

    public int b(View view, int i, int i2) {
        return view.getTop();
    }

    public void b(int i, int i2) {
        this.f143a.q.a(this.f143a.h, i2);
    }

    public void b(View view, int i) {
        this.f143a.a();
    }
}
