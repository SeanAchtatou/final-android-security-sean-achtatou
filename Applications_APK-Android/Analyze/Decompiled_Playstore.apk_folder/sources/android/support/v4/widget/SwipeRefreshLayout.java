package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.at;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;

public class SwipeRefreshLayout extends ViewGroup {
    private static final String c = SwipeRefreshLayout.class.getSimpleName();
    private static final int[] r = {16842766};
    private Animation A;
    /* access modifiers changed from: private */
    public float B;
    /* access modifiers changed from: private */
    public boolean C;
    private int D;
    private int E;
    /* access modifiers changed from: private */
    public boolean F;
    private Animation.AnimationListener G = new au(this);
    private final Animation H = new az(this);
    private final Animation I = new ba(this);

    /* renamed from: a  reason: collision with root package name */
    protected int f132a;

    /* renamed from: b  reason: collision with root package name */
    protected int f133b;
    private View d;
    /* access modifiers changed from: private */
    public bc e;
    /* access modifiers changed from: private */
    public boolean f = false;
    private int g;
    private float h = -1.0f;
    private int i;
    /* access modifiers changed from: private */
    public int j;
    private boolean k = false;
    private float l;
    private boolean m;
    private int n = -1;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;
    private final DecelerateInterpolator q;
    /* access modifiers changed from: private */
    public a s;
    private int t = -1;
    /* access modifiers changed from: private */
    public float u;
    /* access modifiers changed from: private */
    public w v;
    private Animation w;
    private Animation x;
    private Animation y;
    private Animation z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.at.a(android.view.ViewGroup, boolean):void
     arg types: [android.support.v4.widget.SwipeRefreshLayout, int]
     candidates:
      android.support.v4.view.at.a(android.view.View, float):void
      android.support.v4.view.at.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.at.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.at.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.at.a(android.view.View, int):boolean
      android.support.v4.view.at.a(android.view.ViewGroup, boolean):void */
    public SwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = ViewConfiguration.get(context).getScaledTouchSlop();
        this.i = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.q = new DecelerateInterpolator(2.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, r);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.D = (int) (displayMetrics.density * 40.0f);
        this.E = (int) (displayMetrics.density * 40.0f);
        b();
        at.a((ViewGroup) this, true);
        this.B = displayMetrics.density * 64.0f;
        this.h = this.B;
    }

    private float a(MotionEvent motionEvent, int i2) {
        int a2 = z.a(motionEvent, i2);
        if (a2 < 0) {
            return -1.0f;
        }
        return z.d(motionEvent, a2);
    }

    private Animation a(int i2, int i3) {
        if (this.o && c()) {
            return null;
        }
        ax axVar = new ax(this, i2, i3);
        axVar.setDuration(300);
        this.s.a(null);
        this.s.clearAnimation();
        this.s.startAnimation(axVar);
        return axVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* access modifiers changed from: private */
    public void a(float f2) {
        a((this.f132a + ((int) (((float) (this.f133b - this.f132a)) * f2))) - this.s.getTop(), false);
    }

    private void a(int i2, Animation.AnimationListener animationListener) {
        this.f132a = i2;
        this.H.reset();
        this.H.setDuration(200);
        this.H.setInterpolator(this.q);
        if (animationListener != null) {
            this.s.a(animationListener);
        }
        this.s.clearAnimation();
        this.s.startAnimation(this.H);
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z2) {
        this.s.bringToFront();
        this.s.offsetTopAndBottom(i2);
        this.j = this.s.getTop();
        if (z2 && Build.VERSION.SDK_INT < 11) {
            invalidate();
        }
    }

    private void a(MotionEvent motionEvent) {
        int b2 = z.b(motionEvent);
        if (z.b(motionEvent, b2) == this.n) {
            this.n = z.b(motionEvent, b2 == 0 ? 1 : 0);
        }
    }

    private void a(Animation.AnimationListener animationListener) {
        this.s.setVisibility(0);
        if (Build.VERSION.SDK_INT >= 11) {
            this.v.setAlpha(255);
        }
        this.w = new av(this);
        this.w.setDuration((long) this.i);
        if (animationListener != null) {
            this.s.a(animationListener);
        }
        this.s.clearAnimation();
        this.s.startAnimation(this.w);
    }

    private void a(boolean z2, boolean z3) {
        if (this.f != z2) {
            this.C = z3;
            f();
            this.f = z2;
            if (this.f) {
                a(this.j, this.G);
            } else {
                b(this.G);
            }
        }
    }

    private boolean a(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    private void b() {
        this.s = new a(getContext(), -328966, 20.0f);
        this.v = new w(getContext(), this);
        this.v.b(-328966);
        this.s.setImageDrawable(this.v);
        this.s.setVisibility(8);
        addView(this.s);
    }

    private void b(int i2, Animation.AnimationListener animationListener) {
        if (this.o) {
            c(i2, animationListener);
            return;
        }
        this.f132a = i2;
        this.I.reset();
        this.I.setDuration(200);
        this.I.setInterpolator(this.q);
        if (animationListener != null) {
            this.s.a(animationListener);
        }
        this.s.clearAnimation();
        this.s.startAnimation(this.I);
    }

    /* access modifiers changed from: private */
    public void b(Animation.AnimationListener animationListener) {
        this.x = new aw(this);
        this.x.setDuration(150);
        this.s.a(animationListener);
        this.s.clearAnimation();
        this.s.startAnimation(this.x);
    }

    private void c(int i2, Animation.AnimationListener animationListener) {
        this.f132a = i2;
        if (c()) {
            this.u = (float) this.v.a();
        } else {
            this.u = at.h(this.s);
        }
        this.A = new bb(this);
        this.A.setDuration(150);
        if (animationListener != null) {
            this.s.a(animationListener);
        }
        this.s.clearAnimation();
        this.s.startAnimation(this.A);
    }

    private boolean c() {
        return Build.VERSION.SDK_INT < 11;
    }

    private void d() {
        this.y = a(this.v.a(), 76);
    }

    private void e() {
        this.z = a(this.v.a(), 255);
    }

    private void f() {
        if (this.d == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.s)) {
                    this.d = childAt;
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void setAnimationProgress(float f2) {
        if (c()) {
            setColorViewAlpha((int) (255.0f * f2));
            return;
        }
        at.a(this.s, f2);
        at.b(this.s, f2);
    }

    /* access modifiers changed from: private */
    public void setColorViewAlpha(int i2) {
        this.s.getBackground().setAlpha(i2);
        this.v.setAlpha(i2);
    }

    public boolean a() {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 14) {
            return at.b(this.d, -1);
        }
        if (this.d instanceof AbsListView) {
            AbsListView absListView = (AbsListView) this.d;
            return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
        }
        if (this.d.getScrollY() <= 0) {
            z2 = false;
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        return this.t < 0 ? i3 : i3 == i2 + -1 ? this.t : i3 >= this.t ? i3 + 1 : i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -1
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            r0 = 0
            r6.f()
            int r1 = android.support.v4.view.z.a(r7)
            boolean r2 = r6.p
            if (r2 == 0) goto L_0x0014
            if (r1 != 0) goto L_0x0014
            r6.p = r0
        L_0x0014:
            boolean r2 = r6.isEnabled()
            if (r2 == 0) goto L_0x0028
            boolean r2 = r6.p
            if (r2 != 0) goto L_0x0028
            boolean r2 = r6.a()
            if (r2 != 0) goto L_0x0028
            boolean r2 = r6.f
            if (r2 == 0) goto L_0x0029
        L_0x0028:
            return r0
        L_0x0029:
            switch(r1) {
                case 0: goto L_0x002f;
                case 1: goto L_0x0082;
                case 2: goto L_0x004f;
                case 3: goto L_0x0082;
                case 4: goto L_0x002c;
                case 5: goto L_0x002c;
                case 6: goto L_0x007e;
                default: goto L_0x002c;
            }
        L_0x002c:
            boolean r0 = r6.m
            goto L_0x0028
        L_0x002f:
            int r1 = r6.f133b
            android.support.v4.widget.a r2 = r6.s
            int r2 = r2.getTop()
            int r1 = r1 - r2
            r6.a(r1, r5)
            int r1 = android.support.v4.view.z.b(r7, r0)
            r6.n = r1
            r6.m = r0
            int r1 = r6.n
            float r1 = r6.a(r7, r1)
            int r2 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x0028
            r6.l = r1
        L_0x004f:
            int r1 = r6.n
            if (r1 != r4) goto L_0x005b
            java.lang.String r1 = android.support.v4.widget.SwipeRefreshLayout.c
            java.lang.String r2 = "Got ACTION_MOVE event but don't have an active pointer id."
            android.util.Log.e(r1, r2)
            goto L_0x0028
        L_0x005b:
            int r1 = r6.n
            float r1 = r6.a(r7, r1)
            int r2 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x0028
            float r0 = r6.l
            float r0 = r1 - r0
            int r1 = r6.g
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x002c
            boolean r0 = r6.m
            if (r0 != 0) goto L_0x002c
            r6.m = r5
            android.support.v4.widget.w r0 = r6.v
            r1 = 76
            r0.setAlpha(r1)
            goto L_0x002c
        L_0x007e:
            r6.a(r7)
            goto L_0x002c
        L_0x0082:
            r6.m = r0
            r6.n = r4
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SwipeRefreshLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.d == null) {
                f();
            }
            if (this.d != null) {
                View view = this.d;
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.s.getMeasuredWidth();
                this.s.layout((measuredWidth / 2) - (measuredWidth2 / 2), this.j, (measuredWidth / 2) + (measuredWidth2 / 2), this.j + this.s.getMeasuredHeight());
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.d == null) {
            f();
        }
        if (this.d != null) {
            this.d.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.s.measure(View.MeasureSpec.makeMeasureSpec(this.D, 1073741824), View.MeasureSpec.makeMeasureSpec(this.E, 1073741824));
            if (!this.F && !this.k) {
                this.k = true;
                int i4 = -this.s.getMeasuredHeight();
                this.f133b = i4;
                this.j = i4;
            }
            this.t = -1;
            for (int i5 = 0; i5 < getChildCount(); i5++) {
                if (getChildAt(i5) == this.s) {
                    this.t = i5;
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.at.a(android.view.View, float):void
     arg types: [android.support.v4.widget.a, int]
     candidates:
      android.support.v4.view.at.a(android.view.View, android.graphics.Paint):void
      android.support.v4.view.at.a(android.view.View, android.support.v4.view.a):void
      android.support.v4.view.at.a(android.view.View, java.lang.Runnable):void
      android.support.v4.view.at.a(android.view.ViewGroup, boolean):void
      android.support.v4.view.at.a(android.view.View, int):boolean
      android.support.v4.view.at.a(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.at.b(android.view.View, float):void
     arg types: [android.support.v4.widget.a, int]
     candidates:
      android.support.v4.view.at.b(android.view.View, int):boolean
      android.support.v4.view.at.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = z.a(motionEvent);
        if (this.p && a2 == 0) {
            this.p = false;
        }
        if (!isEnabled() || this.p || a()) {
            return false;
        }
        switch (a2) {
            case 0:
                this.n = z.b(motionEvent, 0);
                this.m = false;
                break;
            case 1:
            case 3:
                if (this.n == -1) {
                    if (a2 == 1) {
                        Log.e(c, "Got ACTION_UP event but don't have an active pointer id.");
                    }
                    return false;
                }
                this.m = false;
                if ((z.d(motionEvent, z.a(motionEvent, this.n)) - this.l) * 0.5f > this.h) {
                    a(true, true);
                } else {
                    this.f = false;
                    this.v.a(0.0f, 0.0f);
                    ay ayVar = null;
                    if (!this.o) {
                        ayVar = new ay(this);
                    }
                    b(this.j, ayVar);
                    this.v.a(false);
                }
                this.n = -1;
                return false;
            case 2:
                int a3 = z.a(motionEvent, this.n);
                if (a3 >= 0) {
                    float d2 = 0.5f * (z.d(motionEvent, a3) - this.l);
                    if (this.m) {
                        this.v.a(true);
                        float f2 = d2 / this.h;
                        if (f2 >= 0.0f) {
                            float min = Math.min(1.0f, Math.abs(f2));
                            float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
                            float abs = Math.abs(d2) - this.h;
                            float f3 = this.F ? this.B - ((float) this.f133b) : this.B;
                            float max2 = Math.max(0.0f, Math.min(abs, 2.0f * f3) / f3);
                            float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * 2.0f;
                            int i2 = ((int) ((f3 * min) + (f3 * pow * 2.0f))) + this.f133b;
                            if (this.s.getVisibility() != 0) {
                                this.s.setVisibility(0);
                            }
                            if (!this.o) {
                                at.a((View) this.s, 1.0f);
                                at.b((View) this.s, 1.0f);
                            }
                            if (d2 < this.h) {
                                if (this.o) {
                                    setAnimationProgress(d2 / this.h);
                                }
                                if (this.v.a() > 76 && !a(this.y)) {
                                    d();
                                }
                                this.v.a(0.0f, Math.min(0.8f, 0.8f * max));
                                this.v.a(Math.min(1.0f, max));
                            } else if (this.v.a() < 255 && !a(this.z)) {
                                e();
                            }
                            this.v.b((-0.25f + (0.4f * max) + (2.0f * pow)) * 0.5f);
                            a(i2 - this.j, true);
                            break;
                        } else {
                            return false;
                        }
                    }
                } else {
                    Log.e(c, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    return false;
                }
                break;
            case 5:
                this.n = z.b(motionEvent, z.b(motionEvent));
                break;
            case 6:
                a(motionEvent);
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeColors(int... iArr) {
        f();
        this.v.a(iArr);
    }

    public void setColorSchemeResources(int... iArr) {
        Resources resources = getResources();
        int[] iArr2 = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr2[i2] = resources.getColor(iArr[i2]);
        }
        setColorSchemeColors(iArr2);
    }

    public void setDistanceToTriggerSync(int i2) {
        this.h = (float) i2;
    }

    public void setOnRefreshListener(bc bcVar) {
        this.e = bcVar;
    }

    public void setProgressBackgroundColor(int i2) {
        this.s.setBackgroundColor(i2);
        this.v.b(getResources().getColor(i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void */
    public void setRefreshing(boolean z2) {
        if (!z2 || this.f == z2) {
            a(z2, false);
            return;
        }
        this.f = z2;
        a((!this.F ? (int) (this.B + ((float) this.f133b)) : (int) this.B) - this.j, true);
        this.C = false;
        a(this.G);
    }

    public void setSize(int i2) {
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                int i3 = (int) (displayMetrics.density * 56.0f);
                this.D = i3;
                this.E = i3;
            } else {
                int i4 = (int) (displayMetrics.density * 40.0f);
                this.D = i4;
                this.E = i4;
            }
            this.s.setImageDrawable(null);
            this.v.a(i2);
            this.s.setImageDrawable(this.v);
        }
    }
}
