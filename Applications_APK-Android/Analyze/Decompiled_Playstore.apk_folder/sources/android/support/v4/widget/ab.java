package android.support.v4.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

class ab {

    /* renamed from: a  reason: collision with root package name */
    private final RectF f136a = new RectF();

    /* renamed from: b  reason: collision with root package name */
    private final Paint f137b = new Paint();
    private final Paint c = new Paint();
    private final Drawable.Callback d;
    private float e = 0.0f;
    private float f = 0.0f;
    private float g = 0.0f;
    private float h = 5.0f;
    private float i = 2.5f;
    private int[] j;
    private int k;
    private float l;
    private float m;
    private float n;
    private boolean o;
    private Path p;
    private float q;
    private double r;
    private int s;
    private int t;
    private int u;
    private final Paint v = new Paint();
    private int w;

    public ab(Drawable.Callback callback) {
        this.d = callback;
        this.f137b.setStrokeCap(Paint.Cap.SQUARE);
        this.f137b.setAntiAlias(true);
        this.f137b.setStyle(Paint.Style.STROKE);
        this.c.setStyle(Paint.Style.FILL);
        this.c.setAntiAlias(true);
    }

    private void a(Canvas canvas, float f2, float f3, Rect rect) {
        if (this.o) {
            if (this.p == null) {
                this.p = new Path();
                this.p.setFillType(Path.FillType.EVEN_ODD);
            } else {
                this.p.reset();
            }
            float f4 = ((float) (((int) this.i) / 2)) * this.q;
            float cos = (float) ((this.r * Math.cos(0.0d)) + ((double) rect.exactCenterX()));
            this.p.moveTo(0.0f, 0.0f);
            this.p.lineTo(((float) this.s) * this.q, 0.0f);
            this.p.lineTo((((float) this.s) * this.q) / 2.0f, ((float) this.t) * this.q);
            this.p.offset(cos - f4, (float) ((this.r * Math.sin(0.0d)) + ((double) rect.exactCenterY())));
            this.p.close();
            this.c.setColor(this.j[this.k]);
            canvas.rotate((f2 + f3) - 5.0f, rect.exactCenterX(), rect.exactCenterY());
            canvas.drawPath(this.p, this.c);
        }
    }

    private void l() {
        this.d.invalidateDrawable(null);
    }

    public void a() {
        this.k = (this.k + 1) % this.j.length;
    }

    public void a(double d2) {
        this.r = d2;
    }

    public void a(float f2) {
        this.h = f2;
        this.f137b.setStrokeWidth(f2);
        l();
    }

    public void a(float f2, float f3) {
        this.s = (int) f2;
        this.t = (int) f3;
    }

    public void a(int i2) {
        this.w = i2;
    }

    public void a(int i2, int i3) {
        float min = (float) Math.min(i2, i3);
        this.i = (this.r <= 0.0d || min < 0.0f) ? (float) Math.ceil((double) (this.h / 2.0f)) : (float) (((double) (min / 2.0f)) - this.r);
    }

    public void a(Canvas canvas, Rect rect) {
        RectF rectF = this.f136a;
        rectF.set(rect);
        rectF.inset(this.i, this.i);
        float f2 = (this.e + this.g) * 360.0f;
        float f3 = ((this.f + this.g) * 360.0f) - f2;
        this.f137b.setColor(this.j[this.k]);
        canvas.drawArc(rectF, f2, f3, false, this.f137b);
        a(canvas, f2, f3, rect);
        if (this.u < 255) {
            this.v.setColor(this.w);
            this.v.setAlpha(255 - this.u);
            canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), (float) (rect.width() / 2), this.v);
        }
    }

    public void a(ColorFilter colorFilter) {
        this.f137b.setColorFilter(colorFilter);
        l();
    }

    public void a(boolean z) {
        if (this.o != z) {
            this.o = z;
            l();
        }
    }

    public void a(int[] iArr) {
        this.j = iArr;
        b(0);
    }

    public int b() {
        return this.u;
    }

    public void b(float f2) {
        this.e = f2;
        l();
    }

    public void b(int i2) {
        this.k = i2;
    }

    public float c() {
        return this.h;
    }

    public void c(float f2) {
        this.f = f2;
        l();
    }

    public void c(int i2) {
        this.u = i2;
    }

    public float d() {
        return this.e;
    }

    public void d(float f2) {
        this.g = f2;
        l();
    }

    public float e() {
        return this.l;
    }

    public void e(float f2) {
        if (f2 != this.q) {
            this.q = f2;
            l();
        }
    }

    public float f() {
        return this.m;
    }

    public float g() {
        return this.f;
    }

    public double h() {
        return this.r;
    }

    public float i() {
        return this.n;
    }

    public void j() {
        this.l = this.e;
        this.m = this.f;
        this.n = this.g;
    }

    public void k() {
        this.l = 0.0f;
        this.m = 0.0f;
        this.n = 0.0f;
        b(0.0f);
        c(0.0f);
        d(0.0f);
    }
}
