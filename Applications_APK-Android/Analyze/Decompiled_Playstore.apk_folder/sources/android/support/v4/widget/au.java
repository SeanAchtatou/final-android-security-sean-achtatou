package android.support.v4.widget;

import android.view.animation.Animation;

class au implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwipeRefreshLayout f147a;

    au(SwipeRefreshLayout swipeRefreshLayout) {
        this.f147a = swipeRefreshLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void
     arg types: [android.support.v4.widget.SwipeRefreshLayout, int]
     candidates:
      android.support.v4.widget.SwipeRefreshLayout.a(android.view.MotionEvent, int):float
      android.support.v4.widget.SwipeRefreshLayout.a(int, int):android.view.animation.Animation
      android.support.v4.widget.SwipeRefreshLayout.a(int, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(int, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, int):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, android.view.animation.Animation$AnimationListener):void
      android.support.v4.widget.SwipeRefreshLayout.a(boolean, boolean):void
      android.support.v4.widget.SwipeRefreshLayout.a(android.support.v4.widget.SwipeRefreshLayout, float):void */
    public void onAnimationEnd(Animation animation) {
        if (this.f147a.f) {
            this.f147a.v.setAlpha(255);
            this.f147a.v.start();
            if (this.f147a.C && this.f147a.e != null) {
                this.f147a.e.a();
            }
        } else {
            this.f147a.v.stop();
            this.f147a.s.setVisibility(8);
            this.f147a.setColorViewAlpha(255);
            if (this.f147a.o) {
                this.f147a.setAnimationProgress(0.0f);
            } else {
                this.f147a.a(this.f147a.f133b - this.f147a.j, true);
            }
        }
        int unused = this.f147a.j = this.f147a.s.getTop();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
