package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;

public class r {

    /* renamed from: b  reason: collision with root package name */
    private static final u f170b;

    /* renamed from: a  reason: collision with root package name */
    private Object f171a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f170b = new t();
        } else {
            f170b = new s();
        }
    }

    public r(Context context) {
        this.f171a = f170b.a(context);
    }

    public void a(int i, int i2) {
        f170b.a(this.f171a, i, i2);
    }

    public boolean a() {
        return f170b.a(this.f171a);
    }

    public boolean a(float f) {
        return f170b.a(this.f171a, f);
    }

    public boolean a(Canvas canvas) {
        return f170b.a(this.f171a, canvas);
    }

    public void b() {
        f170b.b(this.f171a);
    }

    public boolean c() {
        return f170b.c(this.f171a);
    }
}
