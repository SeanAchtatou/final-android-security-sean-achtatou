package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class x extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f174a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ w f175b;

    x(w wVar, ab abVar) {
        this.f175b = wVar;
        this.f174a = abVar;
    }

    public void applyTransformation(float f, Transformation transformation) {
        if (this.f175b.f173a) {
            this.f175b.a(f, this.f174a);
            return;
        }
        float f2 = this.f174a.f();
        float e = this.f174a.e();
        float i = this.f174a.i();
        this.f174a.c(((0.8f - ((float) Math.toRadians(((double) this.f174a.c()) / (6.283185307179586d * this.f174a.h())))) * w.d.getInterpolation(f)) + f2);
        this.f174a.b((w.c.getInterpolation(f) * 0.8f) + e);
        this.f174a.d((0.25f * f) + i);
        this.f175b.c((144.0f * f) + (720.0f * (this.f175b.m / 5.0f)));
    }
}
