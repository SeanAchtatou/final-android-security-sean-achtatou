package android.support.v4.widget;

import android.content.Context;
import android.os.Build;
import android.view.animation.Interpolator;

public class ad {

    /* renamed from: a  reason: collision with root package name */
    Object f138a;

    /* renamed from: b  reason: collision with root package name */
    ae f139b;

    private ad(int i, Context context, Interpolator interpolator) {
        if (i >= 14) {
            this.f139b = new ah();
        } else if (i >= 9) {
            this.f139b = new ag();
        } else {
            this.f139b = new af();
        }
        this.f138a = this.f139b.a(context, interpolator);
    }

    ad(Context context, Interpolator interpolator) {
        this(Build.VERSION.SDK_INT, context, interpolator);
    }

    public static ad a(Context context, Interpolator interpolator) {
        return new ad(context, interpolator);
    }

    public int a() {
        return this.f139b.a(this.f138a);
    }

    public void a(int i, int i2, int i3, int i4, int i5) {
        this.f139b.a(this.f138a, i, i2, i3, i4, i5);
    }

    public int b() {
        return this.f139b.b(this.f138a);
    }

    public int c() {
        return this.f139b.e(this.f138a);
    }

    public int d() {
        return this.f139b.f(this.f138a);
    }

    public boolean e() {
        return this.f139b.c(this.f138a);
    }

    public void f() {
        this.f139b.d(this.f138a);
    }
}
