package android.support.v4.widget;

import android.support.v4.view.at;
import android.view.View;

class al implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final View f141a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SlidingPaneLayout f142b;

    al(SlidingPaneLayout slidingPaneLayout, View view) {
        this.f142b = slidingPaneLayout;
        this.f141a = view;
    }

    public void run() {
        if (this.f141a.getParent() == this.f142b) {
            at.a(this.f141a, 0, null);
            this.f142b.g(this.f141a);
        }
        this.f142b.u.remove(this);
    }
}
