package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class ax extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f150a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f151b;
    final /* synthetic */ SwipeRefreshLayout c;

    ax(SwipeRefreshLayout swipeRefreshLayout, int i, int i2) {
        this.c = swipeRefreshLayout;
        this.f150a = i;
        this.f151b = i2;
    }

    public void applyTransformation(float f, Transformation transformation) {
        this.c.v.setAlpha((int) (((float) this.f150a) + (((float) (this.f151b - this.f150a)) * f)));
    }
}
