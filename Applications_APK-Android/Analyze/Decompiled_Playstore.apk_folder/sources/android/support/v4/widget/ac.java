package android.support.v4.widget;

import android.view.animation.AccelerateDecelerateInterpolator;

class ac extends AccelerateDecelerateInterpolator {
    private ac() {
    }

    /* synthetic */ ac(x xVar) {
        this();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public float getInterpolation(float f) {
        return super.getInterpolation(Math.min(1.0f, 2.0f * f));
    }
}
