package android.support.v4.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

public class k extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new l();

    /* renamed from: a  reason: collision with root package name */
    int f165a = 0;

    /* renamed from: b  reason: collision with root package name */
    int f166b = 0;
    int c = 0;

    public k(Parcel parcel) {
        super(parcel);
        this.f165a = parcel.readInt();
    }

    public k(Parcelable parcelable) {
        super(parcelable);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.f165a);
    }
}
