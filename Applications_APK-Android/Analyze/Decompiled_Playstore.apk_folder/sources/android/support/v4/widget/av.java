package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class av extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwipeRefreshLayout f148a;

    av(SwipeRefreshLayout swipeRefreshLayout) {
        this.f148a = swipeRefreshLayout;
    }

    public void applyTransformation(float f, Transformation transformation) {
        this.f148a.setAnimationProgress(f);
    }
}
