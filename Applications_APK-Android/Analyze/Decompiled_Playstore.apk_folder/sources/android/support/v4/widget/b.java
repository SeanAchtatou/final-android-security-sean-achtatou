package android.support.v4.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.shapes.OvalShape;

class b extends OvalShape {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f154a;

    /* renamed from: b  reason: collision with root package name */
    private RadialGradient f155b;
    private int c;
    private Paint d = new Paint();
    private int e;

    public b(a aVar, int i, int i2) {
        this.f154a = aVar;
        this.c = i;
        this.e = i2;
        this.f155b = new RadialGradient((float) (this.e / 2), (float) (this.e / 2), (float) this.c, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
        this.d.setShader(this.f155b);
    }

    public void draw(Canvas canvas, Paint paint) {
        int width = this.f154a.getWidth();
        int height = this.f154a.getHeight();
        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) ((this.e / 2) + this.c), this.d);
        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (this.e / 2), paint);
    }
}
