package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class aw extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwipeRefreshLayout f149a;

    aw(SwipeRefreshLayout swipeRefreshLayout) {
        this.f149a = swipeRefreshLayout;
    }

    public void applyTransformation(float f, Transformation transformation) {
        this.f149a.setAnimationProgress(1.0f - f);
    }
}
