package android.support.v4.widget;

import android.support.v4.widget.DrawerLayout;
import android.view.View;

class m extends bg {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DrawerLayout f167a;

    /* renamed from: b  reason: collision with root package name */
    private final int f168b;
    private bd c;
    private final Runnable d = new n(this);

    public m(DrawerLayout drawerLayout, int i) {
        this.f167a = drawerLayout;
        this.f168b = i;
    }

    private void b() {
        int i = 3;
        if (this.f168b == 3) {
            i = 5;
        }
        View b2 = this.f167a.b(i);
        if (b2 != null) {
            this.f167a.i(b2);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        View view;
        int i;
        int i2 = 0;
        int b2 = this.c.b();
        boolean z = this.f168b == 3;
        if (z) {
            View b3 = this.f167a.b(3);
            if (b3 != null) {
                i2 = -b3.getWidth();
            }
            int i3 = i2 + b2;
            view = b3;
            i = i3;
        } else {
            View b4 = this.f167a.b(5);
            int width = this.f167a.getWidth() - b2;
            view = b4;
            i = width;
        }
        if (view == null) {
            return;
        }
        if (((z && view.getLeft() < i) || (!z && view.getLeft() > i)) && this.f167a.a(view) == 0) {
            this.c.a(view, i, view.getTop());
            ((DrawerLayout.LayoutParams) view.getLayoutParams()).c = true;
            this.f167a.invalidate();
            b();
            this.f167a.c();
        }
    }

    public int a(View view) {
        if (this.f167a.g(view)) {
            return view.getWidth();
        }
        return 0;
    }

    public int a(View view, int i, int i2) {
        if (this.f167a.a(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.f167a.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    public void a() {
        this.f167a.removeCallbacks(this.d);
    }

    public void a(int i) {
        this.f167a.a(this.f168b, i, this.c.c());
    }

    public void a(int i, int i2) {
        this.f167a.postDelayed(this.d, 160);
    }

    public void a(bd bdVar) {
        this.c = bdVar;
    }

    public void a(View view, float f, float f2) {
        int width;
        float d2 = this.f167a.d(view);
        int width2 = view.getWidth();
        if (this.f167a.a(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && d2 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.f167a.getWidth();
            if (f < 0.0f || (f == 0.0f && d2 > 0.5f)) {
                width -= width2;
            }
        }
        this.c.a(width, view.getTop());
        this.f167a.invalidate();
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        int width = view.getWidth();
        float width2 = this.f167a.a(view, 3) ? ((float) (width + i)) / ((float) width) : ((float) (this.f167a.getWidth() - i)) / ((float) width);
        this.f167a.b(view, width2);
        view.setVisibility(width2 == 0.0f ? 4 : 0);
        this.f167a.invalidate();
    }

    public boolean a(View view, int i) {
        return this.f167a.g(view) && this.f167a.a(view, this.f168b) && this.f167a.a(view) == 0;
    }

    public int b(View view, int i, int i2) {
        return view.getTop();
    }

    public void b(int i, int i2) {
        View b2 = (i & 1) == 1 ? this.f167a.b(3) : this.f167a.b(5);
        if (b2 != null && this.f167a.a(b2) == 0) {
            this.c.a(b2, i2);
        }
    }

    public void b(View view, int i) {
        ((DrawerLayout.LayoutParams) view.getLayoutParams()).c = false;
        b();
    }

    public boolean b(int i) {
        return false;
    }
}
