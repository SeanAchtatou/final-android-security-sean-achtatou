package android.support.v4.widget;

import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class as extends ar {

    /* renamed from: a  reason: collision with root package name */
    private Method f145a;

    /* renamed from: b  reason: collision with root package name */
    private Field f146b;

    as() {
        try {
            this.f145a = View.class.getDeclaredMethod("getDisplayList", null);
        } catch (NoSuchMethodException e) {
            Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", e);
        }
        try {
            this.f146b = View.class.getDeclaredField("mRecreateDisplayList");
            this.f146b.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e2);
        }
    }

    public void a(SlidingPaneLayout slidingPaneLayout, View view) {
        if (this.f145a == null || this.f146b == null) {
            view.invalidate();
            return;
        }
        try {
            this.f146b.setBoolean(view, true);
            this.f145a.invoke(view, null);
        } catch (Exception e) {
            Log.e("SlidingPaneLayout", "Error refreshing display list state", e);
        }
        super.a(slidingPaneLayout, view);
    }
}
