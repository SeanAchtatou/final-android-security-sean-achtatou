package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;

class w extends Drawable implements Animatable {

    /* renamed from: b  reason: collision with root package name */
    private static final Interpolator f172b = new LinearInterpolator();
    /* access modifiers changed from: private */
    public static final Interpolator c = new aa(null);
    /* access modifiers changed from: private */
    public static final Interpolator d = new ac(null);
    private static final Interpolator e = new AccelerateDecelerateInterpolator();

    /* renamed from: a  reason: collision with root package name */
    boolean f173a;
    private final int[] f = {-16777216};
    private final ArrayList g = new ArrayList();
    private final ab h;
    private float i;
    private Resources j;
    private View k;
    private Animation l;
    /* access modifiers changed from: private */
    public float m;
    private double n;
    private double o;
    private final Drawable.Callback p = new z(this);

    public w(Context context, View view) {
        this.k = view;
        this.j = context.getResources();
        this.h = new ab(this.p);
        this.h.a(this.f);
        a(1);
        d();
    }

    private void a(double d2, double d3, double d4, double d5, float f2, float f3) {
        ab abVar = this.h;
        float f4 = this.j.getDisplayMetrics().density;
        this.n = ((double) f4) * d2;
        this.o = ((double) f4) * d3;
        abVar.a(((float) d5) * f4);
        abVar.a(((double) f4) * d4);
        abVar.b(0);
        abVar.a(f2 * f4, f4 * f3);
        abVar.a((int) this.n, (int) this.o);
    }

    /* access modifiers changed from: private */
    public void a(float f2, ab abVar) {
        abVar.b(abVar.e() + ((abVar.f() - abVar.e()) * f2));
        abVar.d(((((float) (Math.floor((double) (abVar.i() / 0.8f)) + 1.0d)) - abVar.i()) * f2) + abVar.i());
    }

    private void d() {
        ab abVar = this.h;
        x xVar = new x(this, abVar);
        xVar.setRepeatCount(-1);
        xVar.setRepeatMode(1);
        xVar.setInterpolator(f172b);
        xVar.setAnimationListener(new y(this, abVar));
        this.l = xVar;
    }

    public int a() {
        return this.h.b();
    }

    public void a(float f2) {
        this.h.e(f2);
    }

    public void a(float f2, float f3) {
        this.h.b(f2);
        this.h.c(f3);
    }

    public void a(int i2) {
        if (i2 == 0) {
            a(56.0d, 56.0d, 12.5d, 3.0d, 12.0f, 6.0f);
        } else {
            a(40.0d, 40.0d, 8.75d, 2.5d, 10.0f, 5.0f);
        }
    }

    public void a(boolean z) {
        this.h.a(z);
    }

    public void a(int... iArr) {
        this.h.a(iArr);
        this.h.b(0);
    }

    public void b(float f2) {
        this.h.d(f2);
    }

    public void b(int i2) {
        this.h.a(i2);
    }

    /* access modifiers changed from: package-private */
    public void c(float f2) {
        this.i = f2;
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        int save = canvas.save();
        canvas.rotate(this.i, bounds.exactCenterX(), bounds.exactCenterY());
        this.h.a(canvas, bounds);
        canvas.restoreToCount(save);
    }

    public int getIntrinsicHeight() {
        return (int) this.o;
    }

    public int getIntrinsicWidth() {
        return (int) this.n;
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        ArrayList arrayList = this.g;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Animation animation = (Animation) arrayList.get(i2);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public void setAlpha(int i2) {
        this.h.c(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.h.a(colorFilter);
    }

    public void start() {
        this.l.reset();
        this.h.j();
        if (this.h.g() != this.h.d()) {
            this.f173a = true;
            this.l.setDuration(666);
            this.k.startAnimation(this.l);
            return;
        }
        this.h.b(0);
        this.h.k();
        this.l.setDuration(1333);
        this.k.startAnimation(this.l);
    }

    public void stop() {
        this.k.clearAnimation();
        c(0.0f);
        this.h.a(false);
        this.h.b(0);
        this.h.k();
    }
}
