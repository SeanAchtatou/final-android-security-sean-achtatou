package android.support.v4.widget;

import android.view.animation.Animation;

class y implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f176a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ w f177b;

    y(w wVar, ab abVar) {
        this.f177b = wVar;
        this.f176a = abVar;
    }

    public void onAnimationEnd(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
        this.f176a.j();
        this.f176a.a();
        this.f176a.b(this.f176a.g());
        if (this.f177b.f173a) {
            this.f177b.f173a = false;
            animation.setDuration(1333);
            this.f176a.a(false);
            return;
        }
        float unused = this.f177b.m = (this.f177b.m + 1.0f) % 5.0f;
    }

    public void onAnimationStart(Animation animation) {
        float unused = this.f177b.m = 0.0f;
    }
}
