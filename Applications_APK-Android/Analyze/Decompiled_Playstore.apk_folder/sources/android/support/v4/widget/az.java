package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Transformation;

class az extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwipeRefreshLayout f153a;

    az(SwipeRefreshLayout swipeRefreshLayout) {
        this.f153a = swipeRefreshLayout;
    }

    public void applyTransformation(float f, Transformation transformation) {
        this.f153a.a((((int) (((float) ((!this.f153a.F ? (int) (this.f153a.B - ((float) Math.abs(this.f153a.f133b))) : (int) this.f153a.B) - this.f153a.f132a)) * f)) + this.f153a.f132a) - this.f153a.s.getTop(), false);
    }
}
