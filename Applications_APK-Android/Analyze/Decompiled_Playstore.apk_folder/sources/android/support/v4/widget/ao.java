package android.support.v4.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

class ao extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new ap();

    /* renamed from: a  reason: collision with root package name */
    boolean f144a;

    private ao(Parcel parcel) {
        super(parcel);
        this.f144a = parcel.readInt() != 0;
    }

    ao(Parcelable parcelable) {
        super(parcelable);
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.f144a ? 1 : 0);
    }
}
