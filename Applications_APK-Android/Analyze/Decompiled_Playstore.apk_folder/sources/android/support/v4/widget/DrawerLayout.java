package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.a.a;
import android.support.v4.view.at;
import android.support.v4.view.bs;
import android.support.v4.view.n;
import android.support.v4.view.s;
import android.support.v4.view.z;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class DrawerLayout extends ViewGroup implements q {

    /* renamed from: a  reason: collision with root package name */
    static final g f124a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f125b = {16842931};
    /* access modifiers changed from: private */
    public static final boolean c;
    private CharSequence A;
    private Object B;
    private boolean C;
    private final f d;
    private int e;
    private int f;
    private float g;
    private Paint h;
    private final bd i;
    private final bd j;
    private final m k;
    private final m l;
    private int m;
    private boolean n;
    private boolean o;
    private int p;
    private int q;
    private boolean r;
    private boolean s;
    private j t;
    private float u;
    private float v;
    private Drawable w;
    private Drawable x;
    private Drawable y;
    private CharSequence z;

    public class LayoutParams extends ViewGroup.MarginLayoutParams {

        /* renamed from: a  reason: collision with root package name */
        public int f126a = 0;

        /* renamed from: b  reason: collision with root package name */
        float f127b;
        boolean c;
        boolean d;

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.f125b);
            this.f126a = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.f126a = layoutParams.f126a;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }
    }

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 19) {
            z2 = false;
        }
        c = z2;
        if (Build.VERSION.SDK_INT >= 21) {
            f124a = new h();
        } else {
            f124a = new i();
        }
    }

    public DrawerLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.at.c(android.view.View, int):void
     arg types: [android.support.v4.widget.DrawerLayout, int]
     candidates:
      android.support.v4.view.at.c(android.view.View, float):void
      android.support.v4.view.at.c(android.view.View, int):void */
    public DrawerLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new f(this);
        this.f = -1728053248;
        this.h = new Paint();
        this.o = true;
        setDescendantFocusability(262144);
        float f2 = getResources().getDisplayMetrics().density;
        this.e = (int) ((64.0f * f2) + 0.5f);
        float f3 = f2 * 400.0f;
        this.k = new m(this, 3);
        this.l = new m(this, 5);
        this.i = bd.a(this, 1.0f, this.k);
        this.i.a(1);
        this.i.a(f3);
        this.k.a(this.i);
        this.j = bd.a(this, 1.0f, this.l);
        this.j.a(2);
        this.j.a(f3);
        this.l.a(this.j);
        setFocusableInTouchMode(true);
        at.c((View) this, 1);
        at.a(this, new e(this));
        bs.a(this, false);
        if (at.i(this)) {
            f124a.a((View) this);
        }
    }

    private void a(View view, boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((z2 || g(childAt)) && (!z2 || childAt != view)) {
                at.c(childAt, 4);
            } else {
                at.c(childAt, 1);
            }
        }
    }

    static String c(int i2) {
        return (i2 & 3) == 3 ? "LEFT" : (i2 & 5) == 5 ? "RIGHT" : Integer.toHexString(i2);
    }

    private boolean f() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (((LayoutParams) getChildAt(i2).getLayoutParams()).c) {
                return true;
            }
        }
        return false;
    }

    private boolean g() {
        return h() != null;
    }

    /* access modifiers changed from: private */
    public View h() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (g(childAt) && j(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    private static boolean l(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    /* access modifiers changed from: private */
    public static boolean m(View view) {
        return (at.c(view) == 4 || at.c(view) == 2) ? false : true;
    }

    public int a(View view) {
        int e2 = e(view);
        if (e2 == 3) {
            return this.p;
        }
        if (e2 == 5) {
            return this.q;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public View a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (((LayoutParams) childAt.getLayoutParams()).d) {
                return childAt;
            }
        }
        return null;
    }

    public CharSequence a(int i2) {
        int a2 = n.a(i2, at.e(this));
        if (a2 == 3) {
            return this.z;
        }
        if (a2 == 5) {
            return this.A;
        }
        return null;
    }

    public void a(int i2, int i3) {
        int a2 = n.a(i3, at.e(this));
        if (a2 == 3) {
            this.p = i2;
        } else if (a2 == 5) {
            this.q = i2;
        }
        if (i2 != 0) {
            (a2 == 3 ? this.i : this.j).e();
        }
        switch (i2) {
            case 1:
                View b2 = b(a2);
                if (b2 != null) {
                    i(b2);
                    return;
                }
                return;
            case 2:
                View b3 = b(a2);
                if (b3 != null) {
                    h(b3);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, View view) {
        int i4 = 1;
        int a2 = this.i.a();
        int a3 = this.j.a();
        if (!(a2 == 1 || a3 == 1)) {
            i4 = (a2 == 2 || a3 == 2) ? 2 : 0;
        }
        if (view != null && i3 == 0) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            if (layoutParams.f127b == 0.0f) {
                b(view);
            } else if (layoutParams.f127b == 1.0f) {
                c(view);
            }
        }
        if (i4 != this.m) {
            this.m = i4;
            if (this.t != null) {
                this.t.a(i4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, float f2) {
        if (this.t != null) {
            this.t.a(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        int childCount = getChildCount();
        boolean z3 = false;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (g(childAt) && (!z2 || layoutParams.c)) {
                z3 = a(childAt, 3) ? z3 | this.i.a(childAt, -childAt.getWidth(), childAt.getTop()) : z3 | this.j.a(childAt, getWidth(), childAt.getTop());
                layoutParams.c = false;
            }
        }
        this.k.a();
        this.l.a();
        if (z3) {
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2) {
        return (e(view) & i2) == i2;
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i2, layoutParams);
        if (a() != null || g(view)) {
            at.c(view, 4);
        } else {
            at.c(view, 1);
        }
        if (!c) {
            at.a(view, this.d);
        }
    }

    /* access modifiers changed from: package-private */
    public View b(int i2) {
        int a2 = n.a(i2, at.e(this)) & 7;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if ((e(childAt) & 7) == a2) {
                return childAt;
            }
        }
        return null;
    }

    public void b() {
        a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(View view) {
        View rootView;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (layoutParams.d) {
            layoutParams.d = false;
            if (this.t != null) {
                this.t.b(view);
            }
            a(view, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view, float f2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (f2 != layoutParams.f127b) {
            layoutParams.f127b = f2;
            a(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!this.s) {
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                getChildAt(i2).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.s = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(View view) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (!layoutParams.d) {
            layoutParams.d = true;
            if (this.t != null) {
                this.t.a(view);
            }
            a(view, true);
            view.requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof LayoutParams) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f2 = 0.0f;
        for (int i2 = 0; i2 < childCount; i2++) {
            f2 = Math.max(f2, ((LayoutParams) getChildAt(i2).getLayoutParams()).f127b);
        }
        this.g = f2;
        if (this.i.a(true) || this.j.a(true)) {
            at.b(this);
        }
    }

    /* access modifiers changed from: package-private */
    public float d(View view) {
        return ((LayoutParams) view.getLayoutParams()).f127b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        int i2;
        int height = getHeight();
        boolean f2 = f(view);
        int i3 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (f2) {
            int childCount = getChildCount();
            int i4 = 0;
            while (i4 < childCount) {
                View childAt = getChildAt(i4);
                if (childAt != view && childAt.getVisibility() == 0 && l(childAt) && g(childAt)) {
                    if (childAt.getHeight() < height) {
                        i2 = width;
                    } else if (a(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i3) {
                            right = i3;
                        }
                        i3 = right;
                        i2 = width;
                    } else {
                        i2 = childAt.getLeft();
                        if (i2 < width) {
                        }
                    }
                    i4++;
                    width = i2;
                }
                i2 = width;
                i4++;
                width = i2;
            }
            canvas.clipRect(i3, 0, width, getHeight());
        }
        int i5 = width;
        boolean drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        if (this.g > 0.0f && f2) {
            this.h.setColor((((int) (((float) ((this.f & -16777216) >>> 24)) * this.g)) << 24) | (this.f & 16777215));
            canvas.drawRect((float) i3, 0.0f, (float) i5, (float) getHeight(), this.h);
        } else if (this.w != null && a(view, 3)) {
            int intrinsicWidth = this.w.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.i.b()), 1.0f));
            this.w.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.w.setAlpha((int) (255.0f * max));
            this.w.draw(canvas);
        } else if (this.x != null && a(view, 5)) {
            int intrinsicWidth2 = this.x.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.j.b()), 1.0f));
            this.x.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.x.setAlpha((int) (255.0f * max2));
            this.x.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public int e(View view) {
        return n.a(((LayoutParams) view.getLayoutParams()).f126a, at.e(this));
    }

    /* access modifiers changed from: package-private */
    public boolean f(View view) {
        return ((LayoutParams) view.getLayoutParams()).f126a == 0;
    }

    /* access modifiers changed from: package-private */
    public boolean g(View view) {
        return (n.a(((LayoutParams) view.getLayoutParams()).f126a, at.e(view)) & 7) != 0;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams ? new LayoutParams((LayoutParams) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams) : new LayoutParams(layoutParams);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.a(int, int):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.a(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.a(android.view.View, boolean):void */
    public void h(View view) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.o) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.f127b = 1.0f;
            layoutParams.d = true;
            a(view, true);
        } else if (a(view, 3)) {
            this.i.a(view, 0, view.getTop());
        } else {
            this.j.a(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    public void i(View view) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.o) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            layoutParams.f127b = 0.0f;
            layoutParams.d = false;
        } else if (a(view, 3)) {
            this.i.a(view, -view.getWidth(), view.getTop());
        } else {
            this.j.a(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    public boolean j(View view) {
        if (g(view)) {
            return ((LayoutParams) view.getLayoutParams()).f127b > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.o = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.o = true;
    }

    public void onDraw(Canvas canvas) {
        int a2;
        super.onDraw(canvas);
        if (this.C && this.y != null && (a2 = f124a.a(this.B)) > 0) {
            this.y.setBounds(0, 0, getWidth(), a2);
            this.y.draw(canvas);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        View d2;
        int a2 = z.a(motionEvent);
        boolean a3 = this.i.a(motionEvent) | this.j.a(motionEvent);
        switch (a2) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                this.u = x2;
                this.v = y2;
                z2 = this.g > 0.0f && (d2 = this.i.d((int) x2, (int) y2)) != null && f(d2);
                this.r = false;
                this.s = false;
                break;
            case 1:
            case 3:
                a(true);
                this.r = false;
                this.s = false;
                z2 = false;
                break;
            case 2:
                if (this.i.d(3)) {
                    this.k.a();
                    this.l.a();
                    z2 = false;
                    break;
                }
                z2 = false;
                break;
            default:
                z2 = false;
                break;
        }
        return a3 || z2 || f() || this.s;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !g()) {
            return super.onKeyDown(i2, keyEvent);
        }
        s.b(keyEvent);
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        View h2 = h();
        if (h2 != null && a(h2) == 0) {
            b();
        }
        return h2 != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        float f2;
        this.n = true;
        int i7 = i4 - i2;
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (f(childAt)) {
                    childAt.layout(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.leftMargin + childAt.getMeasuredWidth(), layoutParams.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a(childAt, 3)) {
                        i6 = ((int) (((float) measuredWidth) * layoutParams.f127b)) + (-measuredWidth);
                        f2 = ((float) (measuredWidth + i6)) / ((float) measuredWidth);
                    } else {
                        i6 = i7 - ((int) (((float) measuredWidth) * layoutParams.f127b));
                        f2 = ((float) (i7 - i6)) / ((float) measuredWidth);
                    }
                    boolean z3 = f2 != layoutParams.f127b;
                    switch (layoutParams.f126a & 112) {
                        case 16:
                            int i9 = i5 - i3;
                            int i10 = (i9 - measuredHeight) / 2;
                            if (i10 < layoutParams.topMargin) {
                                i10 = layoutParams.topMargin;
                            } else if (i10 + measuredHeight > i9 - layoutParams.bottomMargin) {
                                i10 = (i9 - layoutParams.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i6, i10, measuredWidth + i6, measuredHeight + i10);
                            break;
                        case 80:
                            int i11 = i5 - i3;
                            childAt.layout(i6, (i11 - layoutParams.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i11 - layoutParams.bottomMargin);
                            break;
                        default:
                            childAt.layout(i6, layoutParams.topMargin, measuredWidth + i6, measuredHeight + layoutParams.topMargin);
                            break;
                    }
                    if (z3) {
                        b(childAt, f2);
                    }
                    int i12 = layoutParams.f127b > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i12) {
                        childAt.setVisibility(i12);
                    }
                }
            }
        }
        this.n = false;
        this.o = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        if (r5 != 0) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r1 = 300(0x12c, float:4.2E-43)
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            r12 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.getMode(r14)
            int r5 = android.view.View.MeasureSpec.getMode(r15)
            int r2 = android.view.View.MeasureSpec.getSize(r14)
            int r0 = android.view.View.MeasureSpec.getSize(r15)
            if (r3 != r12) goto L_0x001b
            if (r5 == r12) goto L_0x0056
        L_0x001b:
            boolean r6 = r13.isInEditMode()
            if (r6 == 0) goto L_0x0058
            if (r3 != r7) goto L_0x0050
        L_0x0023:
            if (r5 != r7) goto L_0x0054
            r1 = r0
        L_0x0026:
            r13.setMeasuredDimension(r2, r1)
            java.lang.Object r0 = r13.B
            if (r0 == 0) goto L_0x0060
            boolean r0 = android.support.v4.view.at.i(r13)
            if (r0 == 0) goto L_0x0060
            r0 = 1
            r3 = r0
        L_0x0035:
            int r6 = android.support.v4.view.at.e(r13)
            int r7 = r13.getChildCount()
            r5 = r4
        L_0x003e:
            if (r5 >= r7) goto L_0x0138
            android.view.View r8 = r13.getChildAt(r5)
            int r0 = r8.getVisibility()
            r9 = 8
            if (r0 != r9) goto L_0x0062
        L_0x004c:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x003e
        L_0x0050:
            if (r3 != 0) goto L_0x0023
            r2 = r1
            goto L_0x0023
        L_0x0054:
            if (r5 == 0) goto L_0x0026
        L_0x0056:
            r1 = r0
            goto L_0x0026
        L_0x0058:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0060:
            r3 = r4
            goto L_0x0035
        L_0x0062:
            android.view.ViewGroup$LayoutParams r0 = r8.getLayoutParams()
            android.support.v4.widget.DrawerLayout$LayoutParams r0 = (android.support.v4.widget.DrawerLayout.LayoutParams) r0
            if (r3 == 0) goto L_0x007d
            int r9 = r0.f126a
            int r9 = android.support.v4.view.n.a(r9, r6)
            boolean r10 = android.support.v4.view.at.i(r8)
            if (r10 == 0) goto L_0x009e
            android.support.v4.widget.g r10 = android.support.v4.widget.DrawerLayout.f124a
            java.lang.Object r11 = r13.B
            r10.a(r8, r11, r9)
        L_0x007d:
            boolean r9 = r13.f(r8)
            if (r9 == 0) goto L_0x00a6
            int r9 = r0.leftMargin
            int r9 = r2 - r9
            int r10 = r0.rightMargin
            int r9 = r9 - r10
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r12)
            int r10 = r0.topMargin
            int r10 = r1 - r10
            int r0 = r0.bottomMargin
            int r0 = r10 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r12)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x009e:
            android.support.v4.widget.g r10 = android.support.v4.widget.DrawerLayout.f124a
            java.lang.Object r11 = r13.B
            r10.a(r0, r11, r9)
            goto L_0x007d
        L_0x00a6:
            boolean r9 = r13.g(r8)
            if (r9 == 0) goto L_0x0109
            int r9 = r13.e(r8)
            r9 = r9 & 7
            r10 = r4 & r9
            if (r10 == 0) goto L_0x00eb
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child drawer has absolute gravity "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = c(r9)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " but this "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "DrawerLayout"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " already has a "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "drawer view along that edge"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00eb:
            int r9 = r13.e
            int r10 = r0.leftMargin
            int r9 = r9 + r10
            int r10 = r0.rightMargin
            int r9 = r9 + r10
            int r10 = r0.width
            int r9 = getChildMeasureSpec(r14, r9, r10)
            int r10 = r0.topMargin
            int r11 = r0.bottomMargin
            int r10 = r10 + r11
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r15, r10, r0)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x0109:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0138:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View b2;
        k kVar = (k) parcelable;
        super.onRestoreInstanceState(kVar.getSuperState());
        if (!(kVar.f165a == 0 || (b2 = b(kVar.f165a)) == null)) {
            h(b2);
        }
        a(kVar.f166b, 3);
        a(kVar.c, 5);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        k kVar = new k(super.onSaveInstanceState());
        View a2 = a();
        if (a2 != null) {
            kVar.f165a = ((LayoutParams) a2.getLayoutParams()).f126a;
        }
        kVar.f166b = this.p;
        kVar.c = this.q;
        return kVar;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        View a2;
        this.i.b(motionEvent);
        this.j.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                this.u = x2;
                this.v = y2;
                this.r = false;
                this.s = false;
                break;
            case 1:
                float x3 = motionEvent.getX();
                float y3 = motionEvent.getY();
                View d2 = this.i.d((int) x3, (int) y3);
                if (d2 != null && f(d2)) {
                    float f2 = x3 - this.u;
                    float f3 = y3 - this.v;
                    int d3 = this.i.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d3 * d3)) && (a2 = a()) != null) {
                        z2 = a(a2) == 2;
                        a(z2);
                        this.r = false;
                        break;
                    }
                }
                z2 = true;
                a(z2);
                this.r = false;
            case 3:
                a(true);
                this.r = false;
                this.s = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z2) {
        super.requestDisallowInterceptTouchEvent(z2);
        this.r = z2;
        if (z2) {
            a(true);
        }
    }

    public void requestLayout() {
        if (!this.n) {
            super.requestLayout();
        }
    }

    public void setDrawerListener(j jVar) {
        this.t = jVar;
    }

    public void setDrawerLockMode(int i2) {
        a(i2, 3);
        a(i2, 5);
    }

    public void setScrimColor(int i2) {
        this.f = i2;
        invalidate();
    }

    public void setStatusBarBackground(int i2) {
        this.y = i2 != 0 ? a.a(getContext(), i2) : null;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.y = drawable;
    }

    public void setStatusBarBackgroundColor(int i2) {
        this.y = new ColorDrawable(i2);
    }
}
