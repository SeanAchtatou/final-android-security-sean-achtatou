package android.support.v4.widget;

import android.graphics.drawable.Drawable;

class z implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ w f178a;

    z(w wVar) {
        this.f178a = wVar;
    }

    public void invalidateDrawable(Drawable drawable) {
        this.f178a.invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        this.f178a.scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        this.f178a.unscheduleSelf(runnable);
    }
}
