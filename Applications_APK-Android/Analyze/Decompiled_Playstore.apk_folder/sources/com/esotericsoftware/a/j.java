package com.esotericsoftware.a;

import java.io.Writer;
import java.util.ArrayList;

public class j extends Writer {

    /* renamed from: a  reason: collision with root package name */
    final Writer f221a;

    /* renamed from: b  reason: collision with root package name */
    private final ArrayList f222b = new ArrayList();
    private k c;
    private boolean d;
    private q e = q.json;

    public j(Writer writer) {
        this.f221a = writer;
    }

    public j a() {
        if (this.c != null) {
            if (this.c.f223a) {
                if (!this.c.f224b) {
                    this.c.f224b = true;
                } else {
                    this.f221a.write(44);
                }
            } else if (this.d || this.c.f223a) {
                this.d = false;
            } else {
                throw new IllegalStateException("Name must be set.");
            }
        }
        ArrayList arrayList = this.f222b;
        k kVar = new k(this, false);
        this.c = kVar;
        arrayList.add(kVar);
        return this;
    }

    public j a(Object obj) {
        if (obj instanceof Number) {
            Number number = (Number) obj;
            long longValue = number.longValue();
            if (number.doubleValue() == ((double) longValue)) {
                obj = Long.valueOf(longValue);
            }
        }
        if (this.c != null) {
            if (this.c.f223a) {
                if (!this.c.f224b) {
                    this.c.f224b = true;
                } else {
                    this.f221a.write(44);
                }
            } else if (!this.d) {
                throw new IllegalStateException("Name must be set.");
            } else {
                this.d = false;
            }
        }
        this.f221a.write(this.e.a(obj));
        return this;
    }

    public j a(String str) {
        if (this.c == null || this.c.f223a) {
            throw new IllegalStateException("Current item must be an object.");
        }
        if (!this.c.f224b) {
            this.c.f224b = true;
        } else {
            this.f221a.write(44);
        }
        this.f221a.write(this.e.a(str));
        this.f221a.write(58);
        this.d = true;
        return this;
    }

    public j a(String str, Object obj) {
        return a(str).a(obj);
    }

    public void a(q qVar) {
        this.e = qVar;
    }

    public j b() {
        if (this.c != null) {
            if (this.c.f223a) {
                if (!this.c.f224b) {
                    this.c.f224b = true;
                } else {
                    this.f221a.write(44);
                }
            } else if (this.d || this.c.f223a) {
                this.d = false;
            } else {
                throw new IllegalStateException("Name must be set.");
            }
        }
        ArrayList arrayList = this.f222b;
        k kVar = new k(this, true);
        this.c = kVar;
        arrayList.add(kVar);
        return this;
    }

    public j c() {
        if (this.d) {
            throw new IllegalStateException("Expected an object, array, or value since a name was set.");
        }
        ((k) this.f222b.remove(this.f222b.size() - 1)).a();
        this.c = this.f222b.isEmpty() ? null : (k) this.f222b.get(this.f222b.size() - 1);
        return this;
    }

    public void close() {
        while (this.f222b.size() > 0) {
            c();
        }
        this.f221a.close();
    }

    public void flush() {
        this.f221a.flush();
    }

    public void write(char[] cArr, int i, int i2) {
        this.f221a.write(cArr, i, i2);
    }
}
