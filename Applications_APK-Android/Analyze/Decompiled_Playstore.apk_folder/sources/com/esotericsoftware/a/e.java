package com.esotericsoftware.a;

public class e extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    private StringBuffer f213a;

    public e() {
    }

    public e(String str) {
        super(str);
    }

    public e(String str, Throwable th) {
        super(str, th);
    }

    public e(Throwable th) {
        super("", th);
    }

    public void a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("info cannot be null.");
        }
        if (this.f213a == null) {
            this.f213a = new StringBuffer(512);
        }
        this.f213a.append(10);
        this.f213a.append(str);
    }

    public String getMessage() {
        if (this.f213a == null) {
            return super.getMessage();
        }
        StringBuffer stringBuffer = new StringBuffer(512);
        stringBuffer.append(super.getMessage());
        if (stringBuffer.length() > 0) {
            stringBuffer.append(10);
        }
        stringBuffer.append("Serialization trace:");
        stringBuffer.append(this.f213a);
        return stringBuffer.toString();
    }
}
