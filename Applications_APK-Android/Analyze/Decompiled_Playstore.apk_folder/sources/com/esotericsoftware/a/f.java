package com.esotericsoftware.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f214a = b();

    /* renamed from: b  reason: collision with root package name */
    private static final short[] f215b = c();
    private static final char[] c = d();
    private static final byte[] d = e();
    private static final byte[] e = f();
    private static final short[] f = g();
    private static final byte[] g = h();
    private static final byte[] h = i();
    private static final byte[] i = j();
    private final ArrayList j = new ArrayList(8);
    private g k;
    private g l;

    private void a(String str, g gVar) {
        gVar.c(str);
        if (this.l.h() || this.l.i()) {
            this.l.a(gVar);
        } else {
            this.k = this.l;
        }
    }

    private static byte[] b() {
        return new byte[]{0, 1, 0, 1, 1, 1, 2, 1, 3, 1, 4, 1, 5, 1, 9, 1, 10, 1, 11, 1, 12, 2, 0, 2, 2, 0, 3, 2, 3, 10, 2, 3, 12, 2, 4, 10, 2, 4, 12, 2, 5, 10, 2, 5, 12, 2, 6, 3, 2, 7, 3, 2, 8, 3, 3, 6, 3, 10, 3, 6, 3, 12, 3, 7, 3, 10, 3, 7, 3, 12, 3, 8, 3, 10, 3, 8, 3, 12};
    }

    private String c(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(length + 16);
        int i2 = 0;
        while (i2 < length) {
            int i3 = i2 + 1;
            char charAt = str.charAt(i2);
            if (charAt != '\\') {
                sb.append(charAt);
                i2 = i3;
            } else if (i3 == length) {
                return sb.toString();
            } else {
                int i4 = i3 + 1;
                char charAt2 = str.charAt(i3);
                if (charAt2 == 'u') {
                    sb.append(Character.toChars(Integer.parseInt(str.substring(i4, i4 + 4), 16)));
                    i2 = i4 + 4;
                } else {
                    switch (charAt2) {
                        case '\"':
                        case '/':
                        case '\\':
                            break;
                        case 'b':
                            charAt2 = 8;
                            break;
                        case 'f':
                            charAt2 = 12;
                            break;
                        case 'n':
                            charAt2 = 10;
                            break;
                        case 'r':
                            charAt2 = 13;
                            break;
                        case 't':
                            charAt2 = 9;
                            break;
                        default:
                            throw new e("Illegal escaped character: \\" + charAt2);
                    }
                    sb.append(charAt2);
                    i2 = i4;
                }
            }
        }
        return sb.toString();
    }

    private static short[] c() {
        return new short[]{0, 0, 18, 20, 22, 31, 33, 37, 39, 54, 56, 58, 62, 80, 82, 84, 89, 103, 110, 112, 115, 123, 127, 129, 135, 144, 151, 153, 161, 170, 174, 176, 183, 191, 199, 207, 215, 222, 230, 238, 246, 253, 261, 269, 277, 284, 293, 313, 315, 317, 322, 341, 348, 350, 358, 367, 371, 373, 380, 388, 396, 404, 412, 419, 427, 435, 443, 450, 458, 466, 474, 481, 490, 493, 500, 506, 513, 518, 526, 534, 542, 550, 557, 565, 573, 581, 588, 596, 604, 612, 619, 619};
    }

    private static char[] d() {
        return new char[]{' ', '\"', '$', '-', '[', '_', 'f', 'n', 't', '{', 9, 13, '0', '9', 'A', 'Z', 'a', 'z', '\"', '\\', '\"', '\\', '\"', '/', '\\', 'b', 'f', 'n', 'r', 't', 'u', '0', '9', '+', '-', '0', '9', '0', '9', ' ', '\"', '$', ',', '-', '_', '}', 9, 13, '0', '9', 'A', 'Z', 'a', 'z', '\"', '\\', '\"', '\\', ' ', ':', 9, 13, ' ', '\"', '$', '-', '[', '_', 'f', 'n', 't', '{', 9, 13, '0', '9', 'A', 'Z', 'a', 'z', '\"', '\\', '\"', '\\', ' ', ',', '}', 9, 13, ' ', '\"', '$', '-', '_', '}', 9, 13, '0', '9', 'A', 'Z', 'a', 'z', ' ', ',', ':', ']', '}', 9, 13, '0', '9', '.', '0', '9', ' ', ':', 'E', 'e', 9, 13, '0', '9', '+', '-', '0', '9', '0', '9', ' ', ':', 9, 13, '0', '9', '\"', '/', '\\', 'b', 'f', 'n', 'r', 't', 'u', ' ', ',', ':', ']', '}', 9, 13, '0', '9', ' ', ',', '.', '}', 9, 13, '0', '9', ' ', ',', 'E', 'e', '}', 9, 13, '0', '9', '+', '-', '0', '9', '0', '9', ' ', ',', '}', 9, 13, '0', '9', ' ', ',', ':', ']', 'a', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', 's', '}', 9, 13, ' ', ',', ':', ']', 'e', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', ',', ':', ']', 'u', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', ',', ':', ']', 'r', '}', 9, 13, ' ', ',', ':', ']', 'u', '}', 9, 13, ' ', ',', ':', ']', 'e', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, '\"', '/', '\\', 'b', 'f', 'n', 'r', 't', 'u', ' ', '\"', '$', ',', '-', '[', ']', '_', 'f', 'n', 't', '{', 9, 13, '0', '9', 'A', 'Z', 'a', 'z', '\"', '\\', '\"', '\\', ' ', ',', ']', 9, 13, ' ', '\"', '$', '-', '[', ']', '_', 'f', 'n', 't', '{', 9, 13, '0', '9', 'A', 'Z', 'a', 'z', ' ', ',', ':', ']', '}', 9, 13, '0', '9', ' ', ',', '.', ']', 9, 13, '0', '9', ' ', ',', 'E', ']', 'e', 9, 13, '0', '9', '+', '-', '0', '9', '0', '9', ' ', ',', ']', 9, 13, '0', '9', ' ', ',', ':', ']', 'a', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', 's', '}', 9, 13, ' ', ',', ':', ']', 'e', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', ',', ':', ']', 'u', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', ',', ':', ']', 'r', '}', 9, 13, ' ', ',', ':', ']', 'u', '}', 9, 13, ' ', ',', ':', ']', 'e', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, '\"', '/', '\\', 'b', 'f', 'n', 'r', 't', 'u', ' ', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', '.', 9, 13, '0', '9', ' ', 'E', 'e', 9, 13, '0', '9', ' ', 9, 13, '0', '9', ' ', ',', ':', ']', 'a', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', 's', '}', 9, 13, ' ', ',', ':', ']', 'e', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', ',', ':', ']', 'u', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', 'l', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, ' ', ',', ':', ']', 'r', '}', 9, 13, ' ', ',', ':', ']', 'u', '}', 9, 13, ' ', ',', ':', ']', 'e', '}', 9, 13, ' ', ',', ':', ']', '}', 9, 13, 0};
    }

    private static byte[] e() {
        return new byte[]{0, 10, 2, 2, 7, 0, 2, 0, 7, 2, 2, 2, 10, 2, 2, 3, 6, 5, 0, 1, 4, 2, 0, 2, 7, 5, 0, 4, 5, 2, 0, 3, 6, 6, 6, 6, 5, 6, 6, 6, 5, 6, 6, 6, 5, 7, 12, 2, 2, 3, 11, 5, 0, 4, 5, 2, 0, 3, 6, 6, 6, 6, 5, 6, 6, 6, 5, 6, 6, 6, 5, 7, 1, 5, 2, 3, 1, 6, 6, 6, 6, 5, 6, 6, 6, 5, 6, 6, 6, 5, 0, 0};
    }

    private static byte[] f() {
        return new byte[]{0, 4, 0, 0, 1, 1, 1, 1, 4, 0, 0, 1, 4, 0, 0, 1, 4, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 0, 0, 1, 4, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0};
    }

    private static short[] g() {
        return new short[]{0, 0, 15, 18, 21, 30, 32, 36, 38, 50, 53, 56, 60, 75, 78, 81, 86, 97, 104, 106, 109, 116, 120, 122, 127, 136, 143, 145, 152, 160, 164, 166, 172, 180, 188, 196, 204, 211, 219, 227, 235, 242, 250, 258, 266, 273, 282, 299, 302, 305, 310, 326, 333, 335, 342, 350, 354, 356, 362, 370, 378, 386, 394, 401, 409, 417, 425, 432, 440, 448, 456, 463, 472, 475, 482, 487, 493, 497, 505, 513, 521, 529, 536, 544, 552, 560, 567, 575, 583, 591, 598, 599};
    }

    private static byte[] h() {
        return new byte[]{1, 2, 73, 5, 72, 73, 77, 82, 86, 72, 1, 74, 73, 73, 0, 72, 4, 3, 72, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 74, 0, 7, 7, 76, 0, 76, 0, 8, 9, 17, 16, 18, 17, 90, 8, 17, 17, 17, 0, 11, 45, 10, 11, 45, 10, 11, 12, 11, 0, 12, 13, 25, 26, 15, 25, 32, 37, 41, 15, 12, 27, 25, 25, 0, 15, 24, 14, 15, 24, 14, 15, 16, 90, 15, 0, 16, 9, 17, 18, 17, 90, 16, 17, 17, 17, 0, 11, 0, 12, 0, 0, 11, 17, 19, 0, 20, 19, 0, 11, 12, 21, 21, 11, 20, 0, 22, 22, 23, 0, 23, 0, 11, 12, 11, 23, 0, 14, 14, 14, 14, 14, 14, 14, 14, 0, 15, 16, 0, 0, 90, 15, 25, 27, 0, 15, 16, 28, 90, 15, 27, 0, 15, 16, 29, 29, 90, 15, 28, 0, 30, 30, 31, 0, 31, 0, 15, 16, 90, 15, 31, 0, 15, 16, 0, 0, 33, 90, 15, 25, 15, 16, 0, 0, 34, 90, 15, 25, 15, 16, 0, 0, 35, 90, 15, 25, 15, 16, 0, 0, 36, 90, 15, 25, 15, 16, 0, 0, 90, 15, 25, 15, 16, 0, 0, 38, 90, 15, 25, 15, 16, 0, 0, 39, 90, 15, 25, 15, 16, 0, 0, 40, 90, 15, 25, 15, 16, 0, 0, 90, 15, 25, 15, 16, 0, 0, 42, 90, 15, 25, 15, 16, 0, 0, 43, 90, 15, 25, 15, 16, 0, 0, 44, 90, 15, 25, 15, 16, 0, 0, 90, 15, 25, 10, 10, 10, 10, 10, 10, 10, 10, 0, 46, 47, 51, 50, 52, 49, 91, 51, 58, 63, 67, 49, 46, 53, 51, 51, 0, 49, 71, 48, 49, 71, 48, 49, 50, 91, 49, 0, 50, 47, 51, 52, 49, 91, 51, 58, 63, 67, 49, 50, 53, 51, 51, 0, 49, 50, 0, 91, 0, 49, 51, 53, 0, 49, 50, 54, 91, 49, 53, 0, 49, 50, 55, 91, 55, 49, 54, 0, 56, 56, 57, 0, 57, 0, 49, 50, 91, 49, 57, 0, 49, 50, 0, 91, 59, 0, 49, 51, 49, 50, 0, 91, 60, 0, 49, 51, 49, 50, 0, 91, 61, 0, 49, 51, 49, 50, 0, 91, 62, 0, 49, 51, 49, 50, 0, 91, 0, 49, 51, 49, 50, 0, 91, 64, 0, 49, 51, 49, 50, 0, 91, 65, 0, 49, 51, 49, 50, 0, 91, 66, 0, 49, 51, 49, 50, 0, 91, 0, 49, 51, 49, 50, 0, 91, 68, 0, 49, 51, 49, 50, 0, 91, 69, 0, 49, 51, 49, 50, 0, 91, 70, 0, 49, 51, 49, 50, 0, 91, 0, 49, 51, 48, 48, 48, 48, 48, 48, 48, 48, 0, 72, 72, 0, 72, 0, 0, 0, 0, 72, 73, 72, 75, 72, 74, 0, 72, 6, 6, 72, 75, 0, 72, 72, 76, 0, 72, 0, 0, 0, 78, 0, 72, 73, 72, 0, 0, 0, 79, 0, 72, 73, 72, 0, 0, 0, 80, 0, 72, 73, 72, 0, 0, 0, 81, 0, 72, 73, 72, 0, 0, 0, 0, 72, 73, 72, 0, 0, 0, 83, 0, 72, 73, 72, 0, 0, 0, 84, 0, 72, 73, 72, 0, 0, 0, 85, 0, 72, 73, 72, 0, 0, 0, 0, 72, 73, 72, 0, 0, 0, 87, 0, 72, 73, 72, 0, 0, 0, 88, 0, 72, 73, 72, 0, 0, 0, 89, 0, 72, 73, 72, 0, 0, 0, 0, 72, 73, 0, 0, 0};
    }

    private static byte[] i() {
        return new byte[]{0, 0, 1, 1, 17, 1, 1, 1, 1, 13, 0, 1, 1, 1, 0, 24, 1, 1, 7, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 15, 0, 1, 1, 1, 0, 21, 1, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 17, 1, 1, 1, 1, 13, 0, 1, 1, 1, 0, 24, 1, 1, 7, 0, 0, 0, 0, 15, 0, 0, 0, 0, 1, 1, 1, 15, 0, 1, 1, 1, 0, 5, 0, 5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 0, 7, 7, 0, 0, 27, 7, 0, 0, 0, 11, 11, 0, 39, 11, 0, 0, 9, 9, 0, 0, 33, 9, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9, 33, 9, 0, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 48, 48, 0, 0, 62, 48, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 51, 51, 0, 0, 70, 51, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 7, 7, 0, 0, 0, 27, 7, 0, 45, 45, 0, 0, 54, 45, 0, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 1, 0, 1, 17, 19, 1, 1, 1, 1, 13, 0, 1, 1, 1, 0, 24, 1, 1, 7, 0, 0, 0, 0, 19, 0, 0, 0, 0, 1, 1, 17, 19, 1, 1, 1, 1, 13, 0, 1, 1, 1, 0, 7, 7, 0, 30, 0, 7, 0, 0, 0, 11, 11, 0, 42, 11, 0, 0, 9, 9, 0, 36, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9, 36, 9, 0, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 48, 48, 0, 66, 0, 48, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 51, 51, 0, 74, 0, 51, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 7, 7, 0, 30, 0, 0, 7, 0, 45, 45, 0, 58, 0, 45, 0, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 7, 0, 0, 0, 0, 7, 0, 11, 0, 11, 0, 0, 9, 0, 0, 9, 0, 0, 9, 9, 0, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 48, 0, 0, 0, 0, 48, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 51, 0, 0, 0, 0, 51, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 7, 0, 0, 0, 0, 0, 7, 0, 45, 0, 0, 0, 0, 45, 0, 0, 0, 0};
    }

    private static byte[] j() {
        return new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 11, 9, 9, 7, 7, 7, 7, 48, 7, 7, 7, 51, 7, 7, 7, 45, 0, 0};
    }

    public g a(InputStream inputStream) {
        try {
            return a(new InputStreamReader(inputStream, "ISO-8859-1"));
        } catch (IOException e2) {
            throw new e(e2);
        }
    }

    public g a(Reader reader) {
        char[] cArr;
        int i2 = 0;
        try {
            char[] cArr2 = new char[1024];
            while (true) {
                int read = reader.read(cArr2, i2, cArr2.length - i2);
                if (read == -1) {
                    break;
                }
                if (read == 0) {
                    cArr = new char[(cArr2.length * 2)];
                    System.arraycopy(cArr2, 0, cArr, 0, cArr2.length);
                } else {
                    i2 += read;
                    cArr = cArr2;
                }
                cArr2 = cArr;
            }
            g a2 = a(cArr2, 0, i2);
            try {
                reader.close();
            } catch (IOException e2) {
            }
            return a2;
        } catch (IOException e3) {
            throw new e(e3);
        } catch (Throwable th) {
            try {
                reader.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:186:0x001d */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:192:0x001d */
    /* JADX WARN: Type inference failed for: r11v2 */
    /* JADX WARN: Type inference failed for: r8v4, types: [int] */
    /* JADX WARN: Type inference failed for: r10v18, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r10v19, types: [byte] */
    /* JADX WARN: Type inference failed for: r12v8, types: [int] */
    /* JADX WARN: Type inference failed for: r12v9 */
    /* JADX WARN: Type inference failed for: r4v85 */
    /* JADX WARN: Type inference failed for: r11v5 */
    /* JADX WARN: Type inference failed for: r12v12 */
    /* JADX WARN: Type inference failed for: r11v6 */
    /* JADX WARN: Type inference failed for: r12v17, types: [int] */
    /* JADX WARN: Type inference failed for: r12v18 */
    /* JADX WARN: Type inference failed for: r14v7, types: [int] */
    /* JADX WARN: Type inference failed for: r13v12, types: [int] */
    /* JADX WARN: Type inference failed for: r10v28, types: [int] */
    /* JADX WARN: Type inference failed for: r14v8 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.esotericsoftware.a.f.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.esotericsoftware.a.f.a(java.lang.String, com.esotericsoftware.a.g):void
      com.esotericsoftware.a.f.a(java.lang.String, double):void
      com.esotericsoftware.a.f.a(java.lang.String, long):void
      com.esotericsoftware.a.f.a(java.lang.String, java.lang.String):void
      com.esotericsoftware.a.f.a(java.lang.String, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x02a1  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02a9  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x03f9  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0078 A[Catch:{ RuntimeException -> 0x03c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0027  */
    /* JADX WARNING: Unknown variable types count: 5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.esotericsoftware.a.g a(char[] r23, int r24, int r25) {
        /*
            r22 = this;
            r4 = 4
            int[] r11 = new int[r4]
            r8 = 0
            java.util.ArrayList r16 = new java.util.ArrayList
            r4 = 8
            r0 = r16
            r0.<init>(r4)
            r6 = 0
            r5 = 0
            r9 = 0
            r10 = 1
            r7 = 0
            r4 = 0
            r15 = r7
            r7 = r24
            r21 = r6
            r6 = r10
            r10 = r4
            r4 = r5
            r5 = r21
        L_0x001d:
            switch(r10) {
                case 0: goto L_0x003b;
                case 1: goto L_0x0045;
                case 2: goto L_0x0458;
                case 3: goto L_0x0020;
                case 4: goto L_0x044f;
                default: goto L_0x0020;
            }
        L_0x0020:
            r6 = r7
        L_0x0021:
            r7 = r6
            r6 = r9
        L_0x0023:
            r0 = r25
            if (r7 >= r0) goto L_0x03f9
            r5 = 1
            r4 = 0
            r21 = r4
            r4 = r5
            r5 = r21
        L_0x002e:
            if (r5 >= r7) goto L_0x03cd
            char r8 = r23[r5]
            r9 = 10
            if (r8 != r9) goto L_0x0038
            int r4 = r4 + 1
        L_0x0038:
            int r5 = r5 + 1
            goto L_0x002e
        L_0x003b:
            r0 = r25
            if (r7 != r0) goto L_0x0041
            r10 = 4
            goto L_0x001d
        L_0x0041:
            if (r6 != 0) goto L_0x0045
            r10 = 5
            goto L_0x001d
        L_0x0045:
            short[] r10 = com.esotericsoftware.a.f.f215b     // Catch:{ RuntimeException -> 0x03c9 }
            short r13 = r10[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            short[] r10 = com.esotericsoftware.a.f.f     // Catch:{ RuntimeException -> 0x03c9 }
            short r10 = r10[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            byte[] r12 = com.esotericsoftware.a.f.d     // Catch:{ RuntimeException -> 0x03c9 }
            byte r17 = r12[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            if (r17 <= 0) goto L_0x005e
            int r12 = r13 + r17
            int r12 = r12 + -1
            r14 = r13
        L_0x0058:
            if (r12 >= r14) goto L_0x009f
            int r13 = r13 + r17
            int r10 = r10 + r17
        L_0x005e:
            byte[] r12 = com.esotericsoftware.a.f.e     // Catch:{ RuntimeException -> 0x03c9 }
            byte r14 = r12[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            if (r14 <= 0) goto L_0x044c
            int r6 = r14 << 1
            int r6 = r6 + r13
            int r6 = r6 + -2
            r12 = r13
        L_0x006a:
            if (r6 >= r12) goto L_0x00c5
            int r6 = r10 + r14
        L_0x006e:
            byte[] r10 = com.esotericsoftware.a.f.g     // Catch:{ RuntimeException -> 0x03c9 }
            byte r10 = r10[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            byte[] r12 = com.esotericsoftware.a.f.h     // Catch:{ RuntimeException -> 0x03c9 }
            byte r12 = r12[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            if (r12 == 0) goto L_0x029f
            byte[] r12 = com.esotericsoftware.a.f.h     // Catch:{ RuntimeException -> 0x03c9 }
            byte r6 = r12[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            byte[] r13 = com.esotericsoftware.a.f.f214a     // Catch:{ RuntimeException -> 0x03c9 }
            int r12 = r6 + 1
            byte r6 = r13[r6]     // Catch:{ RuntimeException -> 0x03c9 }
            r13 = r12
            r21 = r6
            r6 = r5
            r5 = r4
            r4 = r21
        L_0x0089:
            int r12 = r4 + -1
            if (r4 <= 0) goto L_0x029d
            byte[] r4 = com.esotericsoftware.a.f.f214a     // Catch:{ RuntimeException -> 0x03c9 }
            int r14 = r13 + 1
            byte r4 = r4[r13]     // Catch:{ RuntimeException -> 0x03c9 }
            switch(r4) {
                case 0: goto L_0x00f2;
                case 1: goto L_0x00f6;
                case 2: goto L_0x00fe;
                case 3: goto L_0x0118;
                case 4: goto L_0x014c;
                case 5: goto L_0x017b;
                case 6: goto L_0x01aa;
                case 7: goto L_0x01cb;
                case 8: goto L_0x01ec;
                case 9: goto L_0x020d;
                case 10: goto L_0x0248;
                case 11: goto L_0x0257;
                case 12: goto L_0x028e;
                default: goto L_0x0096;
            }     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x0096:
            r4 = r5
            r5 = r6
            r6 = r8
        L_0x0099:
            r13 = r14
            r8 = r6
            r6 = r5
            r5 = r4
            r4 = r12
            goto L_0x0089
        L_0x009f:
            int r18 = r12 - r14
            int r18 = r18 >> 1
            int r18 = r18 + r14
            char r19 = r23[r7]     // Catch:{ RuntimeException -> 0x03c9 }
            char[] r20 = com.esotericsoftware.a.f.c     // Catch:{ RuntimeException -> 0x03c9 }
            char r20 = r20[r18]     // Catch:{ RuntimeException -> 0x03c9 }
            r0 = r19
            r1 = r20
            if (r0 >= r1) goto L_0x00b4
            int r12 = r18 + -1
            goto L_0x0058
        L_0x00b4:
            char r14 = r23[r7]     // Catch:{ RuntimeException -> 0x03c9 }
            char[] r19 = com.esotericsoftware.a.f.c     // Catch:{ RuntimeException -> 0x03c9 }
            char r19 = r19[r18]     // Catch:{ RuntimeException -> 0x03c9 }
            r0 = r19
            if (r14 <= r0) goto L_0x00c1
            int r14 = r18 + 1
            goto L_0x0058
        L_0x00c1:
            int r6 = r18 - r13
            int r6 = r6 + r10
            goto L_0x006e
        L_0x00c5:
            int r17 = r6 - r12
            int r17 = r17 >> 1
            r17 = r17 & -2
            int r17 = r17 + r12
            char r18 = r23[r7]     // Catch:{ RuntimeException -> 0x03c9 }
            char[] r19 = com.esotericsoftware.a.f.c     // Catch:{ RuntimeException -> 0x03c9 }
            char r19 = r19[r17]     // Catch:{ RuntimeException -> 0x03c9 }
            r0 = r18
            r1 = r19
            if (r0 >= r1) goto L_0x00dc
            int r6 = r17 + -2
            goto L_0x006a
        L_0x00dc:
            char r12 = r23[r7]     // Catch:{ RuntimeException -> 0x03c9 }
            char[] r18 = com.esotericsoftware.a.f.c     // Catch:{ RuntimeException -> 0x03c9 }
            int r19 = r17 + 1
            char r18 = r18[r19]     // Catch:{ RuntimeException -> 0x03c9 }
            r0 = r18
            if (r12 <= r0) goto L_0x00eb
            int r12 = r17 + 2
            goto L_0x006a
        L_0x00eb:
            int r6 = r17 - r13
            int r6 = r6 >> 1
            int r6 = r6 + r10
            goto L_0x006e
        L_0x00f2:
            r5 = 0
            r4 = 0
            r6 = r7
            goto L_0x0099
        L_0x00f6:
            r4 = 1
            r6 = r8
            r21 = r4
            r4 = r5
            r5 = r21
            goto L_0x0099
        L_0x00fe:
            java.lang.String r4 = new java.lang.String     // Catch:{ RuntimeException -> 0x03c9 }
            int r13 = r7 - r8
            r0 = r23
            r4.<init>(r0, r8, r13)     // Catch:{ RuntimeException -> 0x03c9 }
            if (r6 == 0) goto L_0x010f
            r0 = r22
            java.lang.String r4 = r0.c(r4)     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x010f:
            r0 = r16
            r0.add(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x0099
        L_0x0118:
            if (r5 != 0) goto L_0x0096
            java.lang.String r4 = new java.lang.String     // Catch:{ RuntimeException -> 0x03c9 }
            int r13 = r7 - r8
            r0 = r23
            r4.<init>(r0, r8, r13)     // Catch:{ RuntimeException -> 0x03c9 }
            if (r6 == 0) goto L_0x0449
            r0 = r22
            java.lang.String r4 = r0.c(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            r8 = r4
        L_0x012c:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x014a
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x0140:
            r0 = r22
            r0.a(r4, r8)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x0099
        L_0x014a:
            r4 = 0
            goto L_0x0140
        L_0x014c:
            java.lang.String r13 = new java.lang.String     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r7 - r8
            r0 = r23
            r13.<init>(r0, r8, r4)     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x0179
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x0169:
            double r18 = java.lang.Double.parseDouble(r13)     // Catch:{ RuntimeException -> 0x03c9 }
            r0 = r22
            r1 = r18
            r0.a(r4, r1)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x0099
        L_0x0179:
            r4 = 0
            goto L_0x0169
        L_0x017b:
            java.lang.String r13 = new java.lang.String     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r7 - r8
            r0 = r23
            r13.<init>(r0, r8, r4)     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x01a8
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x0198:
            long r18 = java.lang.Long.parseLong(r13)     // Catch:{ RuntimeException -> 0x03c9 }
            r0 = r22
            r1 = r18
            r0.a(r4, r1)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x0099
        L_0x01a8:
            r4 = 0
            goto L_0x0198
        L_0x01aa:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x01c9
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x01be:
            r5 = 1
            r0 = r22
            r0.a(r4, r5)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = 1
            r5 = r6
            r6 = r8
            goto L_0x0099
        L_0x01c9:
            r4 = 0
            goto L_0x01be
        L_0x01cb:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x01ea
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x01df:
            r5 = 0
            r0 = r22
            r0.a(r4, r5)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = 1
            r5 = r6
            r6 = r8
            goto L_0x0099
        L_0x01ea:
            r4 = 0
            goto L_0x01df
        L_0x01ec:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x020b
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x0200:
            r5 = 0
            r0 = r22
            r0.a(r4, r5)     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = 1
            r5 = r6
            r6 = r8
            goto L_0x0099
        L_0x020b:
            r4 = 0
            goto L_0x0200
        L_0x020d:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x0246
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x0221:
            r0 = r22
            r0.a(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r11.length     // Catch:{ RuntimeException -> 0x03c9 }
            if (r15 != r4) goto L_0x0235
            int r4 = r11.length     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 * 2
            int[] r4 = new int[r4]     // Catch:{ RuntimeException -> 0x03c9 }
            r12 = 0
            r13 = 0
            int r14 = r11.length     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.System.arraycopy(r11, r12, r4, r13, r14)     // Catch:{ RuntimeException -> 0x03c9 }
            r11 = r4
        L_0x0235:
            int r12 = r15 + 1
            r11[r15] = r10     // Catch:{ RuntimeException -> 0x03c9 }
            r10 = 8
            r4 = 2
            r15 = r12
            r21 = r5
            r5 = r6
            r6 = r10
            r10 = r4
            r4 = r21
            goto L_0x001d
        L_0x0246:
            r4 = 0
            goto L_0x0221
        L_0x0248:
            r22.a()     // Catch:{ RuntimeException -> 0x03c9 }
            int r10 = r15 + -1
            r12 = r11[r10]     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = 2
            r15 = r10
            r10 = r4
            r4 = r5
            r5 = r6
            r6 = r12
            goto L_0x001d
        L_0x0257:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            if (r4 <= 0) goto L_0x028c
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x026b:
            r0 = r22
            r0.b(r4)     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r11.length     // Catch:{ RuntimeException -> 0x03c9 }
            if (r15 != r4) goto L_0x0446
            int r4 = r11.length     // Catch:{ RuntimeException -> 0x03c9 }
            int r4 = r4 * 2
            int[] r4 = new int[r4]     // Catch:{ RuntimeException -> 0x03c9 }
            r12 = 0
            r13 = 0
            int r14 = r11.length     // Catch:{ RuntimeException -> 0x03c9 }
            java.lang.System.arraycopy(r11, r12, r4, r13, r14)     // Catch:{ RuntimeException -> 0x03c9 }
        L_0x027e:
            int r11 = r15 + 1
            r4[r15] = r10     // Catch:{ RuntimeException -> 0x03c9 }
            r12 = 46
            r10 = 2
            r15 = r11
            r11 = r4
            r4 = r5
            r5 = r6
            r6 = r12
            goto L_0x001d
        L_0x028c:
            r4 = 0
            goto L_0x026b
        L_0x028e:
            r22.a()     // Catch:{ RuntimeException -> 0x03c9 }
            int r10 = r15 + -1
            r12 = r11[r10]     // Catch:{ RuntimeException -> 0x03c9 }
            r4 = 2
            r15 = r10
            r10 = r4
            r4 = r5
            r5 = r6
            r6 = r12
            goto L_0x001d
        L_0x029d:
            r4 = r5
            r5 = r6
        L_0x029f:
            if (r10 != 0) goto L_0x02a9
            r6 = 5
            r21 = r6
            r6 = r10
            r10 = r21
            goto L_0x001d
        L_0x02a9:
            int r6 = r7 + 1
            r0 = r25
            if (r6 == r0) goto L_0x02b8
            r7 = 1
            r21 = r7
            r7 = r6
            r6 = r10
            r10 = r21
            goto L_0x001d
        L_0x02b8:
            r7 = r4
            r12 = r5
            r4 = r10
        L_0x02bb:
            r0 = r25
            if (r6 != r0) goto L_0x0021
            byte[] r5 = com.esotericsoftware.a.f.i     // Catch:{ RuntimeException -> 0x0440 }
            byte r4 = r5[r4]     // Catch:{ RuntimeException -> 0x0440 }
            byte[] r10 = com.esotericsoftware.a.f.f214a     // Catch:{ RuntimeException -> 0x0440 }
            int r5 = r4 + 1
            byte r4 = r10[r4]     // Catch:{ RuntimeException -> 0x0440 }
            r10 = r5
            r5 = r7
            r7 = r8
        L_0x02cc:
            int r8 = r4 + -1
            if (r4 <= 0) goto L_0x0021
            byte[] r4 = com.esotericsoftware.a.f.f214a     // Catch:{ RuntimeException -> 0x0440 }
            int r11 = r10 + 1
            byte r4 = r4[r10]     // Catch:{ RuntimeException -> 0x0440 }
            switch(r4) {
                case 3: goto L_0x02e0;
                case 4: goto L_0x0312;
                case 5: goto L_0x033d;
                case 6: goto L_0x0369;
                case 7: goto L_0x0389;
                case 8: goto L_0x03a9;
                default: goto L_0x02d9;
            }     // Catch:{ RuntimeException -> 0x0440 }
        L_0x02d9:
            r4 = r5
            r5 = r7
        L_0x02db:
            r10 = r11
            r7 = r5
            r5 = r4
            r4 = r8
            goto L_0x02cc
        L_0x02e0:
            if (r5 != 0) goto L_0x02d9
            java.lang.String r4 = new java.lang.String     // Catch:{ RuntimeException -> 0x0440 }
            int r10 = r6 - r7
            r0 = r23
            r4.<init>(r0, r7, r10)     // Catch:{ RuntimeException -> 0x0440 }
            if (r12 == 0) goto L_0x0443
            r0 = r22
            java.lang.String r4 = r0.c(r4)     // Catch:{ RuntimeException -> 0x0440 }
            r7 = r4
        L_0x02f4:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            if (r4 <= 0) goto L_0x0310
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x0440 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0440 }
        L_0x0308:
            r0 = r22
            r0.a(r4, r7)     // Catch:{ RuntimeException -> 0x0440 }
            r4 = r5
            r5 = r6
            goto L_0x02db
        L_0x0310:
            r4 = 0
            goto L_0x0308
        L_0x0312:
            java.lang.String r10 = new java.lang.String     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r6 - r7
            r0 = r23
            r10.<init>(r0, r7, r4)     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            if (r4 <= 0) goto L_0x033b
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x0440 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0440 }
        L_0x032f:
            double r14 = java.lang.Double.parseDouble(r10)     // Catch:{ RuntimeException -> 0x0440 }
            r0 = r22
            r0.a(r4, r14)     // Catch:{ RuntimeException -> 0x0440 }
            r4 = r5
            r5 = r6
            goto L_0x02db
        L_0x033b:
            r4 = 0
            goto L_0x032f
        L_0x033d:
            java.lang.String r10 = new java.lang.String     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r6 - r7
            r0 = r23
            r10.<init>(r0, r7, r4)     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            if (r4 <= 0) goto L_0x0367
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x0440 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0440 }
        L_0x035a:
            long r14 = java.lang.Long.parseLong(r10)     // Catch:{ RuntimeException -> 0x0440 }
            r0 = r22
            r0.a(r4, r14)     // Catch:{ RuntimeException -> 0x0440 }
            r4 = r5
            r5 = r6
            goto L_0x02db
        L_0x0367:
            r4 = 0
            goto L_0x035a
        L_0x0369:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            if (r4 <= 0) goto L_0x0387
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x0440 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0440 }
        L_0x037d:
            r5 = 1
            r0 = r22
            r0.a(r4, r5)     // Catch:{ RuntimeException -> 0x0440 }
            r4 = 1
            r5 = r7
            goto L_0x02db
        L_0x0387:
            r4 = 0
            goto L_0x037d
        L_0x0389:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            if (r4 <= 0) goto L_0x03a7
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x0440 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0440 }
        L_0x039d:
            r5 = 0
            r0 = r22
            r0.a(r4, r5)     // Catch:{ RuntimeException -> 0x0440 }
            r4 = 1
            r5 = r7
            goto L_0x02db
        L_0x03a7:
            r4 = 0
            goto L_0x039d
        L_0x03a9:
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            if (r4 <= 0) goto L_0x03c7
            int r4 = r16.size()     // Catch:{ RuntimeException -> 0x0440 }
            int r4 = r4 + -1
            r0 = r16
            java.lang.Object r4 = r0.remove(r4)     // Catch:{ RuntimeException -> 0x0440 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ RuntimeException -> 0x0440 }
        L_0x03bd:
            r5 = 0
            r0 = r22
            r0.a(r4, r5)     // Catch:{ RuntimeException -> 0x0440 }
            r4 = 1
            r5 = r7
            goto L_0x02db
        L_0x03c7:
            r4 = 0
            goto L_0x03bd
        L_0x03c9:
            r4 = move-exception
        L_0x03ca:
            r6 = r4
            goto L_0x0023
        L_0x03cd:
            com.esotericsoftware.a.e r5 = new com.esotericsoftware.a.e
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Error parsing JSON on line "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r4 = r8.append(r4)
            java.lang.String r8 = " near: "
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.String r8 = new java.lang.String
            int r9 = r25 - r7
            r0 = r23
            r8.<init>(r0, r7, r9)
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.String r4 = r4.toString()
            r5.<init>(r4, r6)
            throw r5
        L_0x03f9:
            r0 = r22
            java.util.ArrayList r4 = r0.j
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x0436
            r0 = r22
            java.util.ArrayList r4 = r0.j
            r0 = r22
            java.util.ArrayList r5 = r0.j
            int r5 = r5.size()
            int r5 = r5 + -1
            java.lang.Object r4 = r4.get(r5)
            com.esotericsoftware.a.g r4 = (com.esotericsoftware.a.g) r4
            r0 = r22
            java.util.ArrayList r5 = r0.j
            r5.clear()
            if (r4 == 0) goto L_0x042e
            boolean r4 = r4.i()
            if (r4 == 0) goto L_0x042e
            com.esotericsoftware.a.e r4 = new com.esotericsoftware.a.e
            java.lang.String r5 = "Error parsing JSON, unmatched brace."
            r4.<init>(r5)
            throw r4
        L_0x042e:
            com.esotericsoftware.a.e r4 = new com.esotericsoftware.a.e
            java.lang.String r5 = "Error parsing JSON, unmatched bracket."
            r4.<init>(r5)
            throw r4
        L_0x0436:
            r0 = r22
            com.esotericsoftware.a.g r4 = r0.k
            r5 = 0
            r0 = r22
            r0.k = r5
            return r4
        L_0x0440:
            r4 = move-exception
            r7 = r6
            goto L_0x03ca
        L_0x0443:
            r7 = r4
            goto L_0x02f4
        L_0x0446:
            r4 = r11
            goto L_0x027e
        L_0x0449:
            r8 = r4
            goto L_0x012c
        L_0x044c:
            r6 = r10
            goto L_0x006e
        L_0x044f:
            r12 = r5
            r21 = r4
            r4 = r6
            r6 = r7
            r7 = r21
            goto L_0x02bb
        L_0x0458:
            r10 = r6
            goto L_0x029f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.a.f.a(char[], int, int):com.esotericsoftware.a.g");
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.k = (g) this.j.remove(this.j.size() - 1);
        this.l = !this.j.isEmpty() ? (g) this.j.get(this.j.size() - 1) : null;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        g gVar = new g(i.object);
        if (this.l != null) {
            a(str, gVar);
        }
        this.j.add(gVar);
        this.l = gVar;
    }

    /* access modifiers changed from: protected */
    public void a(String str, double d2) {
        a(str, new g(d2));
    }

    /* access modifiers changed from: protected */
    public void a(String str, long j2) {
        a(str, new g(j2));
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        a(str, new g(str2));
    }

    /* access modifiers changed from: protected */
    public void a(String str, boolean z) {
        a(str, new g(z));
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        g gVar = new g(i.array);
        if (this.l != null) {
            a(str, gVar);
        }
        this.j.add(gVar);
        this.l = gVar;
    }
}
