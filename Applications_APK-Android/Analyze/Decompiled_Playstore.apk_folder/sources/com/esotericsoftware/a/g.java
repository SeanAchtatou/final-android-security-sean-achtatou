package com.esotericsoftware.a;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private String f216a;

    /* renamed from: b  reason: collision with root package name */
    private i f217b;
    private String c;
    private Boolean d;
    private Double e;
    private long f;
    private g g;
    private g h;
    private g i;

    public g(double d2) {
        a(d2);
    }

    public g(long j) {
        a(j);
    }

    public g(i iVar) {
        this.f217b = iVar;
    }

    public g(String str) {
        d(str);
    }

    public g(boolean z) {
        a(z);
    }

    private static void a(int i2, StringBuilder sb) {
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append(9);
        }
    }

    private void a(g gVar, StringBuilder sb, q qVar, int i2, int i3) {
        if (gVar.i()) {
            if (gVar.r() == null) {
                sb.append("{}");
                return;
            }
            boolean z = !b(gVar);
            int length = sb.length();
            boolean z2 = z;
            loop0:
            while (true) {
                sb.append(z2 ? "{\n" : "{ ");
                g r = gVar.r();
                while (r != null) {
                    if (z2) {
                        a(i2, sb);
                    }
                    sb.append(qVar.a(r.q()));
                    sb.append(": ");
                    a(r, sb, qVar, i2 + 1, i3);
                    if (r.s() != null) {
                        sb.append(",");
                    }
                    sb.append(z2 ? 10 : ' ');
                    if (z2 || sb.length() - length <= i3) {
                        r = r.s();
                    } else {
                        sb.setLength(length);
                        z2 = true;
                    }
                }
                break loop0;
            }
            if (z2) {
                a(i2 - 1, sb);
            }
            sb.append('}');
        } else if (gVar.h()) {
            if (gVar.r() == null) {
                sb.append("[]");
                return;
            }
            boolean z3 = !b(gVar);
            int length2 = sb.length();
            boolean z4 = z3;
            loop2:
            while (true) {
                sb.append(z4 ? "[\n" : "[ ");
                g r2 = gVar.r();
                while (r2 != null) {
                    if (z4) {
                        a(i2, sb);
                    }
                    a(r2, sb, qVar, i2 + 1, i3);
                    if (r2.s() != null) {
                        sb.append(",");
                    }
                    sb.append(z4 ? 10 : ' ');
                    if (z4 || sb.length() - length2 <= i3) {
                        r2 = r2.s();
                    } else {
                        sb.setLength(length2);
                        z4 = true;
                    }
                }
                break loop2;
            }
            if (z4) {
                a(i2 - 1, sb);
            }
            sb.append(']');
        } else if (gVar.j()) {
            sb.append(qVar.a((Object) gVar.b()));
        } else if (gVar.l()) {
            double d2 = gVar.d();
            long e2 = gVar.e();
            if (d2 == ((double) e2)) {
                d2 = (double) e2;
            }
            sb.append(d2);
        } else if (gVar.m()) {
            sb.append(gVar.e());
        } else if (gVar.n()) {
            sb.append(gVar.g());
        } else if (gVar.o()) {
            sb.append("null");
        } else {
            throw new e("Unknown object type: " + gVar);
        }
    }

    private static boolean b(g gVar) {
        for (g r = gVar.r(); r != null; r = r.s()) {
            if (r.i() || r.h()) {
                return false;
            }
        }
        return true;
    }

    public int a() {
        int i2 = 0;
        for (g gVar = this.g; gVar != null; gVar = gVar.h) {
            i2++;
        }
        return i2;
    }

    public g a(String str) {
        g gVar = this.g;
        while (gVar != null && !gVar.f216a.equalsIgnoreCase(str)) {
            gVar = gVar.h;
        }
        return gVar;
    }

    public String a(q qVar, int i2) {
        StringBuilder sb = new StringBuilder(512);
        a(this, sb, qVar, 0, i2);
        return sb.toString();
    }

    public String a(String str, String str2) {
        g a2 = a(str);
        return (a2 == null || !a2.p() || a2.o()) ? str2 : a2.b();
    }

    public void a(double d2) {
        this.e = Double.valueOf(d2);
        this.f = (long) d2;
        this.f217b = i.doubleValue;
    }

    public void a(long j) {
        this.f = j;
        this.e = Double.valueOf((double) j);
        this.f217b = i.longValue;
    }

    public void a(g gVar) {
        g gVar2 = this.g;
        if (gVar2 == null) {
            this.g = gVar;
            return;
        }
        while (gVar2.h != null) {
            gVar2 = gVar2.h;
        }
        gVar2.h = gVar;
        gVar.i = gVar2;
    }

    public void a(boolean z) {
        this.d = Boolean.valueOf(z);
        this.f217b = i.booleanValue;
    }

    public g b(String str) {
        g a2 = a(str);
        if (a2 == null) {
            return null;
        }
        if (a2.i == null) {
            this.g = a2.h;
            if (this.g != null) {
                this.g.i = null;
            }
        } else {
            a2.i.h = a2.h;
            if (a2.h != null) {
                a2.h.i = a2.i;
            }
        }
        return a2;
    }

    public String b() {
        if (this.c != null) {
            return this.c;
        }
        if (this.e != null) {
            return this.e.doubleValue() == ((double) this.f) ? Long.toString(this.f) : Double.toString(this.e.doubleValue());
        }
        if (this.d != null) {
            return Boolean.toString(this.d.booleanValue());
        }
        if (this.f217b == i.nullValue) {
            return null;
        }
        throw new IllegalStateException("Value cannot be converted to string: " + this.f217b);
    }

    public float c() {
        if (this.e != null) {
            return this.e.floatValue();
        }
        if (this.c != null) {
            try {
                return Float.parseFloat(this.c);
            } catch (NumberFormatException e2) {
            }
        }
        if (this.d != null) {
            return this.d.booleanValue() ? 1.0f : 0.0f;
        }
        throw new IllegalStateException("Value cannot be converted to float: " + this.f217b);
    }

    public void c(String str) {
        this.f216a = str;
    }

    public double d() {
        if (this.e != null) {
            return this.e.doubleValue();
        }
        if (this.c != null) {
            try {
                return Double.parseDouble(this.c);
            } catch (NumberFormatException e2) {
            }
        }
        if (this.d != null) {
            return this.d.booleanValue() ? 1.0d : 0.0d;
        }
        throw new IllegalStateException("Value cannot be converted to double: " + this.f217b);
    }

    public void d(String str) {
        this.c = str;
        this.f217b = str == null ? i.nullValue : i.stringValue;
    }

    public long e() {
        if (this.e != null) {
            return this.f;
        }
        if (this.c != null) {
            try {
                return Long.parseLong(this.c);
            } catch (NumberFormatException e2) {
            }
        }
        if (this.d != null) {
            return this.d.booleanValue() ? 1 : 0;
        }
        throw new IllegalStateException("Value cannot be converted to long: " + this.f217b);
    }

    public int f() {
        if (this.e != null) {
            return (int) this.f;
        }
        if (this.c != null) {
            try {
                return Integer.parseInt(this.c);
            } catch (NumberFormatException e2) {
            }
        }
        if (this.d != null) {
            return this.d.booleanValue() ? 1 : 0;
        }
        throw new IllegalStateException("Value cannot be converted to int: " + this.f217b);
    }

    public boolean g() {
        if (this.d != null) {
            return this.d.booleanValue();
        }
        if (this.e != null) {
            return this.f == 0;
        }
        if (this.c != null) {
            return this.c.equalsIgnoreCase("true");
        }
        throw new IllegalStateException("Value cannot be converted to boolean: " + this.f217b);
    }

    public boolean h() {
        return this.f217b == i.array;
    }

    public boolean i() {
        return this.f217b == i.object;
    }

    public boolean j() {
        return this.f217b == i.stringValue;
    }

    public boolean k() {
        return this.f217b == i.doubleValue || this.f217b == i.longValue;
    }

    public boolean l() {
        return this.f217b == i.doubleValue;
    }

    public boolean m() {
        return this.f217b == i.longValue;
    }

    public boolean n() {
        return this.f217b == i.booleanValue;
    }

    public boolean o() {
        return this.f217b == i.nullValue;
    }

    public boolean p() {
        switch (h.f218a[this.f217b.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return true;
            default:
                return false;
        }
    }

    public String q() {
        return this.f216a;
    }

    public g r() {
        return this.g;
    }

    public g s() {
        return this.h;
    }

    public String toString() {
        return a(q.minimal, 0);
    }
}
