package com.esotericsoftware.a;

import java.util.Random;

public class l {
    static final Random f = new Random();

    /* renamed from: a  reason: collision with root package name */
    public int f225a;

    /* renamed from: b  reason: collision with root package name */
    Object[] f226b;
    Object[] c;
    int d;
    int e;
    private float g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private m m;
    private p n;

    public l() {
        this(32, 0.8f);
    }

    public l(int i2, float f2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + i2);
        } else if (this.d > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + i2);
        } else {
            this.d = b(i2);
            if (f2 <= 0.0f) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + f2);
            }
            this.g = f2;
            this.j = (int) (((float) this.d) * f2);
            this.i = this.d - 1;
            this.h = 31 - Integer.numberOfTrailingZeros(this.d);
            this.k = Math.max(3, ((int) Math.ceil(Math.log((double) this.d))) * 2);
            this.l = Math.max(Math.min(this.d, 8), ((int) Math.sqrt((double) this.d)) / 8);
            this.f226b = new Object[(this.d + this.k)];
            this.c = new Object[this.f226b.length];
        }
    }

    private void a(Object obj, Object obj2, int i2, Object obj3, int i3, Object obj4, int i4, Object obj5) {
        Object[] objArr = this.f226b;
        Object[] objArr2 = this.c;
        int i5 = this.i;
        int i6 = 0;
        int i7 = this.l;
        do {
            switch (f.nextInt(3)) {
                case 0:
                    Object obj6 = objArr2[i2];
                    objArr[i2] = obj;
                    objArr2[i2] = obj2;
                    obj2 = obj6;
                    obj = obj3;
                    break;
                case 1:
                    Object obj7 = objArr2[i3];
                    objArr[i3] = obj;
                    objArr2[i3] = obj2;
                    obj2 = obj7;
                    obj = obj4;
                    break;
                default:
                    Object obj8 = objArr2[i4];
                    objArr[i4] = obj;
                    objArr2[i4] = obj2;
                    obj2 = obj8;
                    obj = obj5;
                    break;
            }
            int hashCode = obj.hashCode();
            i2 = hashCode & i5;
            obj3 = objArr[i2];
            if (obj3 == null) {
                objArr[i2] = obj;
                objArr2[i2] = obj2;
                int i8 = this.f225a;
                this.f225a = i8 + 1;
                if (i8 >= this.j) {
                    c(this.d << 1);
                    return;
                }
                return;
            }
            i3 = d(hashCode);
            obj4 = objArr[i3];
            if (obj4 == null) {
                objArr[i3] = obj;
                objArr2[i3] = obj2;
                int i9 = this.f225a;
                this.f225a = i9 + 1;
                if (i9 >= this.j) {
                    c(this.d << 1);
                    return;
                }
                return;
            }
            i4 = e(hashCode);
            obj5 = objArr[i4];
            if (obj5 == null) {
                objArr[i4] = obj;
                objArr2[i4] = obj2;
                int i10 = this.f225a;
                this.f225a = i10 + 1;
                if (i10 >= this.j) {
                    c(this.d << 1);
                    return;
                }
                return;
            }
            i6++;
        } while (i6 != i7);
        d(obj, obj2);
    }

    static int b(int i2) {
        if (i2 == 0) {
            return 1;
        }
        int i3 = i2 - 1;
        int i4 = i3 | (i3 >> 1);
        int i5 = i4 | (i4 >> 2);
        int i6 = i5 | (i5 >> 4);
        int i7 = i6 | (i6 >> 8);
        return (i7 | (i7 >> 16)) + 1;
    }

    private Object b(Object obj, Object obj2) {
        Object[] objArr = this.f226b;
        int hashCode = obj.hashCode();
        int i2 = hashCode & this.i;
        Object obj3 = objArr[i2];
        if (obj.equals(obj3)) {
            Object obj4 = this.c[i2];
            this.c[i2] = obj2;
            return obj4;
        }
        int d2 = d(hashCode);
        Object obj5 = objArr[d2];
        if (obj.equals(obj5)) {
            Object obj6 = this.c[d2];
            this.c[d2] = obj2;
            return obj6;
        }
        int e2 = e(hashCode);
        Object obj7 = objArr[e2];
        if (obj.equals(obj7)) {
            Object obj8 = this.c[e2];
            this.c[e2] = obj2;
            return obj8;
        }
        int i3 = this.d;
        int i4 = i3 + this.e;
        for (int i5 = i3; i5 < i4; i5++) {
            if (obj.equals(objArr[i5])) {
                Object obj9 = this.c[i5];
                this.c[i5] = obj2;
                return obj9;
            }
        }
        if (obj3 == null) {
            objArr[i2] = obj;
            this.c[i2] = obj2;
            int i6 = this.f225a;
            this.f225a = i6 + 1;
            if (i6 >= this.j) {
                c(this.d << 1);
            }
            return null;
        } else if (obj5 == null) {
            objArr[d2] = obj;
            this.c[d2] = obj2;
            int i7 = this.f225a;
            this.f225a = i7 + 1;
            if (i7 >= this.j) {
                c(this.d << 1);
            }
            return null;
        } else if (obj7 == null) {
            objArr[e2] = obj;
            this.c[e2] = obj2;
            int i8 = this.f225a;
            this.f225a = i8 + 1;
            if (i8 >= this.j) {
                c(this.d << 1);
            }
            return null;
        } else {
            a(obj, obj2, i2, obj3, d2, obj5, e2, obj7);
            return null;
        }
    }

    private Object c(Object obj) {
        Object[] objArr = this.f226b;
        int i2 = this.d;
        int i3 = this.e + i2;
        while (i2 < i3) {
            if (obj.equals(objArr[i2])) {
                return this.c[i2];
            }
            i2++;
        }
        return null;
    }

    private void c(int i2) {
        int i3 = this.e + this.d;
        this.d = i2;
        this.j = (int) (((float) i2) * this.g);
        this.i = i2 - 1;
        this.h = 31 - Integer.numberOfTrailingZeros(i2);
        this.k = Math.max(3, ((int) Math.ceil(Math.log((double) i2))) * 2);
        this.l = Math.max(Math.min(i2, 8), ((int) Math.sqrt((double) i2)) / 8);
        Object[] objArr = this.f226b;
        Object[] objArr2 = this.c;
        this.f226b = new Object[(this.k + i2)];
        this.c = new Object[(this.k + i2)];
        this.f225a = 0;
        this.e = 0;
        for (int i4 = 0; i4 < i3; i4++) {
            Object obj = objArr[i4];
            if (obj != null) {
                c(obj, objArr2[i4]);
            }
        }
    }

    private void c(Object obj, Object obj2) {
        int hashCode = obj.hashCode();
        int i2 = hashCode & this.i;
        Object obj3 = this.f226b[i2];
        if (obj3 == null) {
            this.f226b[i2] = obj;
            this.c[i2] = obj2;
            int i3 = this.f225a;
            this.f225a = i3 + 1;
            if (i3 >= this.j) {
                c(this.d << 1);
                return;
            }
            return;
        }
        int d2 = d(hashCode);
        Object obj4 = this.f226b[d2];
        if (obj4 == null) {
            this.f226b[d2] = obj;
            this.c[d2] = obj2;
            int i4 = this.f225a;
            this.f225a = i4 + 1;
            if (i4 >= this.j) {
                c(this.d << 1);
                return;
            }
            return;
        }
        int e2 = e(hashCode);
        Object obj5 = this.f226b[e2];
        if (obj5 == null) {
            this.f226b[e2] = obj;
            this.c[e2] = obj2;
            int i5 = this.f225a;
            this.f225a = i5 + 1;
            if (i5 >= this.j) {
                c(this.d << 1);
                return;
            }
            return;
        }
        a(obj, obj2, i2, obj3, d2, obj4, e2, obj5);
    }

    private int d(int i2) {
        int i3 = -1262997959 * i2;
        return (i3 ^ (i3 >>> this.h)) & this.i;
    }

    private void d(Object obj, Object obj2) {
        if (this.e == this.k) {
            c(this.d << 1);
            b(obj, obj2);
            return;
        }
        int i2 = this.d + this.e;
        this.f226b[i2] = obj;
        this.c[i2] = obj2;
        this.e++;
        this.f225a++;
    }

    private boolean d(Object obj) {
        Object[] objArr = this.f226b;
        int i2 = this.d;
        int i3 = this.e + i2;
        while (i2 < i3) {
            if (obj.equals(objArr[i2])) {
                return true;
            }
            i2++;
        }
        return false;
    }

    private int e(int i2) {
        int i3 = -825114047 * i2;
        return (i3 ^ (i3 >>> this.h)) & this.i;
    }

    public m a() {
        if (this.m == null) {
            this.m = new m(this);
        } else {
            this.m.b();
        }
        return this.m;
    }

    public Object a(Object obj) {
        int hashCode = obj.hashCode();
        int i2 = this.i & hashCode;
        if (!obj.equals(this.f226b[i2])) {
            i2 = d(hashCode);
            if (!obj.equals(this.f226b[i2])) {
                i2 = e(hashCode);
                if (!obj.equals(this.f226b[i2])) {
                    return c(obj);
                }
            }
        }
        return this.c[i2];
    }

    public Object a(Object obj, Object obj2) {
        if (obj != null) {
            return b(obj, obj2);
        }
        throw new IllegalArgumentException("key cannot be null.");
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.e--;
        int i3 = this.d + this.e;
        if (i2 < i3) {
            this.f226b[i2] = this.f226b[i3];
            this.c[i2] = this.c[i3];
            this.c[i3] = null;
            return;
        }
        this.c[i2] = null;
    }

    public p b() {
        if (this.n == null) {
            this.n = new p(this);
        } else {
            this.n.b();
        }
        return this.n;
    }

    public boolean b(Object obj) {
        int hashCode = obj.hashCode();
        if (!obj.equals(this.f226b[this.i & hashCode])) {
            if (!obj.equals(this.f226b[d(hashCode)])) {
                if (!obj.equals(this.f226b[e(hashCode)])) {
                    return d(obj);
                }
            }
        }
        return true;
    }

    public String toString() {
        if (this.f225a == 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append('{');
        Object[] objArr = this.f226b;
        Object[] objArr2 = this.c;
        int length = objArr.length;
        while (true) {
            int i2 = length;
            length = i2 - 1;
            if (i2 > 0) {
                Object obj = objArr[length];
                if (obj != null) {
                    sb.append(obj);
                    sb.append('=');
                    sb.append(objArr2[length]);
                    break;
                }
            } else {
                break;
            }
        }
        while (true) {
            int i3 = length - 1;
            if (length > 0) {
                Object obj2 = objArr[i3];
                if (obj2 == null) {
                    length = i3;
                } else {
                    sb.append(", ");
                    sb.append(obj2);
                    sb.append('=');
                    sb.append(objArr2[i3]);
                    length = i3;
                }
            } else {
                sb.append('}');
                return sb.toString();
            }
        }
    }
}
