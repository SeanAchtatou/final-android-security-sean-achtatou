package com.esotericsoftware.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private j f209a;

    /* renamed from: b  reason: collision with root package name */
    private String f210b = "class";
    private boolean c = true;
    private q d = q.minimal;
    private final l e = new l();
    private final l f = new l();
    private final l g = new l();
    private final l h = new l();
    private final l i = new l();
    private boolean j;

    private l b(Class cls) {
        ArrayList arrayList = new ArrayList();
        for (Class cls2 = cls; cls2 != Object.class; cls2 = cls2.getSuperclass()) {
            Collections.addAll(arrayList, cls2.getDeclaredFields());
        }
        l lVar = new l();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Field field = (Field) arrayList.get(i2);
            int modifiers = field.getModifiers();
            if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers) && !field.isSynthetic()) {
                if (!field.isAccessible()) {
                    try {
                        field.setAccessible(true);
                    } catch (AccessControlException e2) {
                    }
                }
                lVar.a(field.getName(), new b(field));
            }
        }
        this.e.a(cls, lVar);
        return lVar;
    }

    private String b(Object obj) {
        return obj instanceof Class ? ((Class) obj).getName() : String.valueOf(obj);
    }

    private Object[] c(Class cls) {
        if (!this.c) {
            return null;
        }
        if (this.i.b(cls)) {
            return (Object[]) this.i.a(cls);
        }
        try {
            Object d2 = d(cls);
            l lVar = (l) this.e.a(cls);
            if (lVar == null) {
                lVar = b(cls);
            }
            Object[] objArr = new Object[lVar.f225a];
            this.i.a(cls, objArr);
            int i2 = 0;
            Iterator it = lVar.b().iterator();
            while (it.hasNext()) {
                Field field = ((b) it.next()).f211a;
                int i3 = i2 + 1;
                try {
                    objArr[i2] = field.get(d2);
                    i2 = i3;
                } catch (IllegalAccessException e2) {
                    throw new e("Error accessing field: " + field.getName() + " (" + cls.getName() + ")", e2);
                } catch (e e3) {
                    e3.a(field + " (" + cls.getName() + ")");
                    throw e3;
                } catch (RuntimeException e4) {
                    e eVar = new e(e4);
                    eVar.a(field + " (" + cls.getName() + ")");
                    throw eVar;
                }
            }
            return objArr;
        } catch (Exception e5) {
            this.i.a(cls, null);
            return null;
        }
    }

    private Object d(Class cls) {
        try {
            return cls.newInstance();
        } catch (Exception e2) {
            e = e2;
            try {
                Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
                declaredConstructor.setAccessible(true);
                return declaredConstructor.newInstance(new Object[0]);
            } catch (SecurityException e3) {
            } catch (NoSuchMethodException e4) {
                if (cls.isArray()) {
                    throw new e("Encountered JSON object when expected array of type: " + cls.getName(), e);
                } else if (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) {
                    throw new e("Class cannot be created (missing no-arg constructor): " + cls.getName(), e);
                } else {
                    throw new e("Class cannot be created (non-static member class): " + cls.getName(), e);
                }
            } catch (Exception e5) {
                e = e5;
            }
        }
        throw new e("Error constructing instance of class: " + cls.getName(), e);
    }

    public Object a(Class cls, InputStream inputStream) {
        return a(cls, (Class) null, new f().a(inputStream));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:128:0x01f8, code lost:
        if (r7 != java.lang.Boolean.class) goto L_0x0205;
     */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0215  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x02e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(java.lang.Class<?> r7, java.lang.Class<?> r8, com.esotericsoftware.a.g r9) {
        /*
            r6 = this;
            r3 = 0
            r2 = 0
            if (r9 != 0) goto L_0x0006
            r1 = r2
        L_0x0005:
            return r1
        L_0x0006:
            boolean r0 = r9.i()
            if (r0 == 0) goto L_0x00c0
            java.lang.String r0 = r6.f210b
            if (r0 != 0) goto L_0x0048
            r0 = r2
        L_0x0011:
            if (r0 == 0) goto L_0x001d
            java.lang.String r1 = r6.f210b
            r9.b(r1)
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x004f }
            r7 = r0
        L_0x001d:
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Float> r0 = java.lang.Float.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Short> r0 = java.lang.Short.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Byte> r0 = java.lang.Byte.class
            if (r7 == r0) goto L_0x0041
            java.lang.Class<java.lang.Character> r0 = java.lang.Character.class
            if (r7 != r0) goto L_0x0060
        L_0x0041:
            java.lang.String r0 = "value"
            java.lang.Object r1 = r6.a(r0, r7, r9)
            goto L_0x0005
        L_0x0048:
            java.lang.String r0 = r6.f210b
            java.lang.String r0 = r9.a(r0, r2)
            goto L_0x0011
        L_0x004f:
            r1 = move-exception
            com.esotericsoftware.a.l r3 = r6.f
            java.lang.Object r0 = r3.a(r0)
            java.lang.Class r0 = (java.lang.Class) r0
            if (r0 != 0) goto L_0x02eb
            com.esotericsoftware.a.e r0 = new com.esotericsoftware.a.e
            r0.<init>(r1)
            throw r0
        L_0x0060:
            if (r7 == 0) goto L_0x009c
            com.esotericsoftware.a.l r0 = r6.h
            java.lang.Object r0 = r0.a(r7)
            com.esotericsoftware.a.d r0 = (com.esotericsoftware.a.d) r0
            if (r0 == 0) goto L_0x0071
            java.lang.Object r1 = r0.a(r6, r9, r7)
            goto L_0x0005
        L_0x0071:
            java.lang.Object r1 = r6.d(r7)
            boolean r0 = r1 instanceof com.esotericsoftware.a.c
            if (r0 == 0) goto L_0x0080
            r0 = r1
            com.esotericsoftware.a.c r0 = (com.esotericsoftware.a.c) r0
            r0.a(r6, r9)
            goto L_0x0005
        L_0x0080:
            boolean r0 = r1 instanceof java.util.HashMap
            if (r0 == 0) goto L_0x009f
            java.util.HashMap r1 = (java.util.HashMap) r1
            com.esotericsoftware.a.g r0 = r9.r()
        L_0x008a:
            if (r0 == 0) goto L_0x0005
            java.lang.String r3 = r0.q()
            java.lang.Object r4 = r6.a(r8, r2, r0)
            r1.put(r3, r4)
            com.esotericsoftware.a.g r0 = r0.s()
            goto L_0x008a
        L_0x009c:
            r1 = r9
            goto L_0x0005
        L_0x009f:
            boolean r0 = r1 instanceof com.esotericsoftware.a.l
            if (r0 == 0) goto L_0x00bb
            com.esotericsoftware.a.l r1 = (com.esotericsoftware.a.l) r1
            com.esotericsoftware.a.g r0 = r9.r()
        L_0x00a9:
            if (r0 == 0) goto L_0x0005
            java.lang.String r3 = r0.q()
            java.lang.Object r4 = r6.a(r8, r2, r0)
            r1.a(r3, r4)
            com.esotericsoftware.a.g r0 = r0.s()
            goto L_0x00a9
        L_0x00bb:
            r6.a(r1, r9)
            goto L_0x0005
        L_0x00c0:
            if (r7 == 0) goto L_0x00d2
            com.esotericsoftware.a.l r0 = r6.h
            java.lang.Object r0 = r0.a(r7)
            com.esotericsoftware.a.d r0 = (com.esotericsoftware.a.d) r0
            if (r0 == 0) goto L_0x00d2
            java.lang.Object r1 = r0.a(r6, r9, r7)
            goto L_0x0005
        L_0x00d2:
            boolean r0 = r9.h()
            if (r0 == 0) goto L_0x015a
            java.lang.Class<java.util.List> r0 = java.util.List.class
            boolean r0 = r0.isAssignableFrom(r7)
            if (r0 == 0) goto L_0x0103
            if (r7 != 0) goto L_0x00f9
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x00e7:
            com.esotericsoftware.a.g r1 = r9.r()
        L_0x00eb:
            if (r1 == 0) goto L_0x0100
            java.lang.Object r3 = r6.a(r8, r2, r1)
            r0.add(r3)
            com.esotericsoftware.a.g r1 = r1.s()
            goto L_0x00eb
        L_0x00f9:
            java.lang.Object r0 = r6.d(r7)
            java.util.List r0 = (java.util.List) r0
            goto L_0x00e7
        L_0x0100:
            r1 = r0
            goto L_0x0005
        L_0x0103:
            boolean r0 = r7.isArray()
            if (r0 == 0) goto L_0x012d
            java.lang.Class r0 = r7.getComponentType()
            if (r8 != 0) goto L_0x0110
            r8 = r0
        L_0x0110:
            int r1 = r9.a()
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r0, r1)
            com.esotericsoftware.a.g r0 = r9.r()
        L_0x011c:
            if (r0 == 0) goto L_0x0005
            int r4 = r3 + 1
            java.lang.Object r5 = r6.a(r8, r2, r0)
            java.lang.reflect.Array.set(r1, r3, r5)
            com.esotericsoftware.a.g r0 = r0.s()
            r3 = r4
            goto L_0x011c
        L_0x012d:
            com.esotericsoftware.a.e r0 = new com.esotericsoftware.a.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unable to convert value to required type: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = " ("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x015a:
            boolean r0 = r9.k()
            if (r0 == 0) goto L_0x01ea
            if (r7 == 0) goto L_0x016a
            java.lang.Class r0 = java.lang.Float.TYPE     // Catch:{ NumberFormatException -> 0x01df }
            if (r7 == r0) goto L_0x016a
            java.lang.Class<java.lang.Float> r0 = java.lang.Float.class
            if (r7 != r0) goto L_0x0174
        L_0x016a:
            float r0 = r9.c()     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.Float r1 = java.lang.Float.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x0174:
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NumberFormatException -> 0x01df }
            if (r7 == r0) goto L_0x017c
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r7 != r0) goto L_0x0186
        L_0x017c:
            int r0 = r9.f()     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x0186:
            java.lang.Class r0 = java.lang.Long.TYPE     // Catch:{ NumberFormatException -> 0x01df }
            if (r7 == r0) goto L_0x018e
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r7 != r0) goto L_0x0198
        L_0x018e:
            long r0 = r9.e()     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.Long r1 = java.lang.Long.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x0198:
            java.lang.Class r0 = java.lang.Double.TYPE     // Catch:{ NumberFormatException -> 0x01df }
            if (r7 == r0) goto L_0x01a0
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r7 != r0) goto L_0x01ab
        L_0x01a0:
            float r0 = r9.c()     // Catch:{ NumberFormatException -> 0x01df }
            double r0 = (double) r0     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.Double r1 = java.lang.Double.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x01ab:
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r7 != r0) goto L_0x01b9
            float r0 = r9.c()     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.String r1 = java.lang.Float.toString(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x01b9:
            java.lang.Class r0 = java.lang.Short.TYPE     // Catch:{ NumberFormatException -> 0x01df }
            if (r7 == r0) goto L_0x01c1
            java.lang.Class<java.lang.Short> r0 = java.lang.Short.class
            if (r7 != r0) goto L_0x01cc
        L_0x01c1:
            int r0 = r9.f()     // Catch:{ NumberFormatException -> 0x01df }
            short r0 = (short) r0     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.Short r1 = java.lang.Short.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x01cc:
            java.lang.Class r0 = java.lang.Byte.TYPE     // Catch:{ NumberFormatException -> 0x01df }
            if (r7 == r0) goto L_0x01d4
            java.lang.Class<java.lang.Byte> r0 = java.lang.Byte.class
            if (r7 != r0) goto L_0x01e0
        L_0x01d4:
            int r0 = r9.f()     // Catch:{ NumberFormatException -> 0x01df }
            byte r0 = (byte) r0     // Catch:{ NumberFormatException -> 0x01df }
            java.lang.Byte r1 = java.lang.Byte.valueOf(r0)     // Catch:{ NumberFormatException -> 0x01df }
            goto L_0x0005
        L_0x01df:
            r0 = move-exception
        L_0x01e0:
            com.esotericsoftware.a.g r0 = new com.esotericsoftware.a.g
            java.lang.String r1 = r9.b()
            r0.<init>(r1)
            r9 = r0
        L_0x01ea:
            boolean r0 = r9.n()
            if (r0 == 0) goto L_0x020f
            if (r7 == 0) goto L_0x01fa
            java.lang.Class r0 = java.lang.Boolean.TYPE     // Catch:{ NumberFormatException -> 0x0204 }
            if (r7 == r0) goto L_0x01fa
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            if (r7 != r0) goto L_0x0205
        L_0x01fa:
            boolean r0 = r9.g()     // Catch:{ NumberFormatException -> 0x0204 }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)     // Catch:{ NumberFormatException -> 0x0204 }
            goto L_0x0005
        L_0x0204:
            r0 = move-exception
        L_0x0205:
            com.esotericsoftware.a.g r0 = new com.esotericsoftware.a.g
            java.lang.String r1 = r9.b()
            r0.<init>(r1)
            r9 = r0
        L_0x020f:
            boolean r0 = r9.j()
            if (r0 == 0) goto L_0x02e8
            java.lang.String r1 = r9.b()
            if (r7 == 0) goto L_0x0005
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            if (r7 == r0) goto L_0x0005
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NumberFormatException -> 0x0273 }
            if (r7 == r0) goto L_0x0227
            java.lang.Class<java.lang.Integer> r0 = java.lang.Integer.class
            if (r7 != r0) goto L_0x022d
        L_0x0227:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0273 }
            goto L_0x0005
        L_0x022d:
            java.lang.Class r0 = java.lang.Float.TYPE     // Catch:{ NumberFormatException -> 0x0273 }
            if (r7 == r0) goto L_0x0235
            java.lang.Class<java.lang.Float> r0 = java.lang.Float.class
            if (r7 != r0) goto L_0x023b
        L_0x0235:
            java.lang.Float r1 = java.lang.Float.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0273 }
            goto L_0x0005
        L_0x023b:
            java.lang.Class r0 = java.lang.Long.TYPE     // Catch:{ NumberFormatException -> 0x0273 }
            if (r7 == r0) goto L_0x0243
            java.lang.Class<java.lang.Long> r0 = java.lang.Long.class
            if (r7 != r0) goto L_0x0249
        L_0x0243:
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0273 }
            goto L_0x0005
        L_0x0249:
            java.lang.Class r0 = java.lang.Double.TYPE     // Catch:{ NumberFormatException -> 0x0273 }
            if (r7 == r0) goto L_0x0251
            java.lang.Class<java.lang.Double> r0 = java.lang.Double.class
            if (r7 != r0) goto L_0x0257
        L_0x0251:
            java.lang.Double r1 = java.lang.Double.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0273 }
            goto L_0x0005
        L_0x0257:
            java.lang.Class r0 = java.lang.Short.TYPE     // Catch:{ NumberFormatException -> 0x0273 }
            if (r7 == r0) goto L_0x025f
            java.lang.Class<java.lang.Short> r0 = java.lang.Short.class
            if (r7 != r0) goto L_0x0265
        L_0x025f:
            java.lang.Short r1 = java.lang.Short.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0273 }
            goto L_0x0005
        L_0x0265:
            java.lang.Class r0 = java.lang.Byte.TYPE     // Catch:{ NumberFormatException -> 0x0273 }
            if (r7 == r0) goto L_0x026d
            java.lang.Class<java.lang.Byte> r0 = java.lang.Byte.class
            if (r7 != r0) goto L_0x0274
        L_0x026d:
            java.lang.Byte r1 = java.lang.Byte.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0273 }
            goto L_0x0005
        L_0x0273:
            r0 = move-exception
        L_0x0274:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            if (r7 == r0) goto L_0x027c
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            if (r7 != r0) goto L_0x0282
        L_0x027c:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            goto L_0x0005
        L_0x0282:
            java.lang.Class r0 = java.lang.Character.TYPE
            if (r7 == r0) goto L_0x028a
            java.lang.Class<java.lang.Character> r0 = java.lang.Character.class
            if (r7 != r0) goto L_0x0294
        L_0x028a:
            char r0 = r1.charAt(r3)
            java.lang.Character r1 = java.lang.Character.valueOf(r0)
            goto L_0x0005
        L_0x0294:
            java.lang.Class<java.lang.Enum> r0 = java.lang.Enum.class
            boolean r0 = r0.isAssignableFrom(r7)
            if (r0 == 0) goto L_0x02b7
            java.lang.Object[] r2 = r7.getEnumConstants()
            int r4 = r2.length
            r0 = r3
        L_0x02a2:
            if (r0 >= r4) goto L_0x02b7
            r3 = r2[r0]
            java.lang.String r3 = r3.toString()
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x02b4
            r1 = r2[r0]
            goto L_0x0005
        L_0x02b4:
            int r0 = r0 + 1
            goto L_0x02a2
        L_0x02b7:
            java.lang.Class<java.lang.CharSequence> r0 = java.lang.CharSequence.class
            if (r7 == r0) goto L_0x0005
            com.esotericsoftware.a.e r0 = new com.esotericsoftware.a.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unable to convert value to required type: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = " ("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x02e8:
            r1 = r2
            goto L_0x0005
        L_0x02eb:
            r7 = r0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.a.a.a(java.lang.Class, java.lang.Class, com.esotericsoftware.a.g):java.lang.Object");
    }

    public Object a(String str, Class cls, g gVar) {
        return a(cls, (Class) null, gVar.a(str));
    }

    public void a() {
        try {
            this.f209a.c();
        } catch (IOException e2) {
            throw new e(e2);
        }
    }

    public void a(Writer writer) {
        this.f209a = (j) (!(writer instanceof j) ? new j(writer) : writer);
        this.f209a.a(this.d);
    }

    public void a(Class cls) {
        if (this.f210b != null) {
            String str = (String) this.g.a(cls);
            if (str == null) {
                str = cls.getName();
            }
            try {
                this.f209a.a(this.f210b, str);
            } catch (IOException e2) {
                throw new e(e2);
            }
        }
    }

    public void a(Class cls, Class cls2) {
        try {
            this.f209a.a();
            if (cls2 == null || cls2 != cls) {
                a(cls);
            }
        } catch (IOException e2) {
            throw new e(e2);
        }
    }

    public void a(Object obj) {
        Class<?> cls = obj.getClass();
        Object[] c2 = c(cls);
        l lVar = (l) this.e.a(cls);
        if (lVar == null) {
            lVar = b((Class) cls);
        }
        int i2 = 0;
        Iterator it = new p(lVar).iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            Field field = bVar.f211a;
            try {
                Object obj2 = field.get(obj);
                if (c2 != null) {
                    int i3 = i2 + 1;
                    Object obj3 = c2[i2];
                    if (obj2 == null && obj3 == null) {
                        i2 = i3;
                    } else if (obj2 == null || obj3 == null || !obj2.equals(obj3)) {
                        i2 = i3;
                    } else {
                        i2 = i3;
                    }
                }
                this.f209a.a(field.getName());
                a(obj2, field.getType(), bVar.f212b);
            } catch (IllegalAccessException e2) {
                throw new e("Error accessing field: " + field.getName() + " (" + cls.getName() + ")", e2);
            } catch (e e3) {
                e3.a(field + " (" + cls.getName() + ")");
                throw e3;
            } catch (Exception e4) {
                e eVar = new e(e4);
                eVar.a(field + " (" + cls.getName() + ")");
                throw eVar;
            }
        }
    }

    public void a(Object obj, g gVar) {
        Class<?> cls = obj.getClass();
        l lVar = (l) this.e.a(cls);
        l b2 = lVar == null ? b((Class) cls) : lVar;
        for (g r = gVar.r(); r != null; r = r.s()) {
            b bVar = (b) b2.a(r.q());
            if (bVar != null) {
                Field field = bVar.f211a;
                try {
                    field.set(obj, a(field.getType(), bVar.f212b, r));
                } catch (IllegalAccessException e2) {
                    throw new e("Error accessing field: " + field.getName() + " (" + cls.getName() + ")", e2);
                } catch (e e3) {
                    e3.a(field.getName() + " (" + cls.getName() + ")");
                    throw e3;
                } catch (RuntimeException e4) {
                    e eVar = new e(e4);
                    eVar.a(field.getName() + " (" + cls.getName() + ")");
                    throw eVar;
                }
            } else if (!this.j) {
                throw new e("Field not found: " + r.q() + " (" + cls.getName() + ")");
            }
        }
    }

    public void a(Object obj, Class cls, Class cls2) {
        if (obj == null) {
            try {
                this.f209a.a((Object) null);
            } catch (IOException e2) {
                throw new e(e2);
            }
        } else if ((cls != null && cls.isPrimitive()) || cls == String.class || cls == Integer.class || cls == Boolean.class || cls == Float.class || cls == Long.class || cls == Double.class || cls == Short.class || cls == Byte.class || cls == Character.class) {
            this.f209a.a(obj);
        } else {
            Class<?> cls3 = obj.getClass();
            if (cls3.isPrimitive() || cls3 == String.class || cls3 == Integer.class || cls3 == Boolean.class || cls3 == Float.class || cls3 == Long.class || cls3 == Double.class || cls3 == Short.class || cls3 == Byte.class || cls3 == Character.class) {
                a(cls3, (Class) null);
                a("value", obj);
                a();
            } else if (obj instanceof c) {
                a(cls3, cls);
                ((c) obj).a(this);
                a();
            } else {
                d dVar = (d) this.h.a(cls3);
                if (dVar != null) {
                    dVar.a(this, obj, cls);
                } else if (obj instanceof Collection) {
                    if (cls == null || cls3 == cls || cls3 == ArrayList.class) {
                        b();
                        for (Object a2 : (Collection) obj) {
                            a(a2, cls2, (Class) null);
                        }
                        c();
                        return;
                    }
                    throw new e("Serialization of a Collection other than the known type is not supported.\nKnown type: " + cls + "\nActual type: " + cls3);
                } else if (cls3.isArray()) {
                    if (cls2 == null) {
                        cls2 = cls3.getComponentType();
                    }
                    int length = Array.getLength(obj);
                    b();
                    for (int i2 = 0; i2 < length; i2++) {
                        a(Array.get(obj, i2), cls2, (Class) null);
                    }
                    c();
                } else if (obj instanceof l) {
                    if (cls == null) {
                        cls = l.class;
                    }
                    a(cls3, cls);
                    Iterator it = ((l) obj).a().iterator();
                    while (it.hasNext()) {
                        n nVar = (n) it.next();
                        this.f209a.a(b(nVar.f228a));
                        a(nVar.f229b, cls2, (Class) null);
                    }
                    a();
                } else if (obj instanceof Map) {
                    if (cls == null) {
                        cls = HashMap.class;
                    }
                    a(cls3, cls);
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        this.f209a.a(b(entry.getKey()));
                        a(entry.getValue(), cls2, (Class) null);
                    }
                    a();
                } else if (Enum.class.isAssignableFrom(cls3)) {
                    this.f209a.a(obj);
                } else {
                    a(cls3, cls);
                    a(obj);
                    a();
                }
            }
        }
    }

    public void a(String str, Class cls) {
        this.f.a(str, cls);
        this.g.a(cls, str);
    }

    public void a(String str, Object obj) {
        try {
            this.f209a.a(str);
            if (obj == null) {
                a(obj, (Class) null, (Class) null);
            } else {
                a(obj, obj.getClass(), (Class) null);
            }
        } catch (IOException e2) {
            throw new e(e2);
        }
    }

    public void b() {
        try {
            this.f209a.b();
        } catch (IOException e2) {
            throw new e(e2);
        }
    }

    public void c() {
        try {
            this.f209a.c();
        } catch (IOException e2) {
            throw new e(e2);
        }
    }
}
