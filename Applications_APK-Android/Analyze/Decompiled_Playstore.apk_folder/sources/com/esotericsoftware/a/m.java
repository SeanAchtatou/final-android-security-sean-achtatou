package com.esotericsoftware.a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class m extends o implements Iterable, Iterator {

    /* renamed from: a  reason: collision with root package name */
    n f227a = new n();

    public m(l lVar) {
        super(lVar);
    }

    /* renamed from: a */
    public n next() {
        if (!this.f230b) {
            throw new NoSuchElementException();
        }
        Object[] objArr = this.c.f226b;
        this.f227a.f228a = objArr[this.d];
        this.f227a.f229b = this.c.c[this.d];
        this.e = this.d;
        c();
        return this.f227a;
    }

    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public boolean hasNext() {
        return this.f230b;
    }

    public Iterator iterator() {
        return this;
    }

    public /* bridge */ /* synthetic */ void remove() {
        super.remove();
    }
}
