package com.esotericsoftware.a;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

class b {

    /* renamed from: a  reason: collision with root package name */
    Field f211a;

    /* renamed from: b  reason: collision with root package name */
    Class f212b;

    public b(Field field) {
        this.f211a = field;
        Type genericType = field.getGenericType();
        if (genericType instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) genericType).getActualTypeArguments();
            if (actualTypeArguments.length == 1) {
                Type type = actualTypeArguments[0];
                if (type instanceof Class) {
                    this.f212b = (Class) type;
                } else if (type instanceof ParameterizedType) {
                    this.f212b = (Class) ((ParameterizedType) type).getRawType();
                }
            }
        }
    }
}
