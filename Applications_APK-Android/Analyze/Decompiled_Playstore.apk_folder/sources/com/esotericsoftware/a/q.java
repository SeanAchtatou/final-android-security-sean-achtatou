package com.esotericsoftware.a;

import java.util.regex.Pattern;

public enum q {
    json,
    javascript,
    minimal;
    
    private static Pattern d = Pattern.compile("[a-zA-Z_$][a-zA-Z_$0-9]*");
    private static Pattern e = Pattern.compile("[a-zA-Z_$][^:}\\], ]*");
    private static Pattern f = Pattern.compile("[a-zA-Z0-9_$][^:}\\], ]*");

    public String a(Object obj) {
        if (obj == null || (obj instanceof Number) || (obj instanceof Boolean)) {
            return String.valueOf(obj);
        }
        String replace = String.valueOf(obj).replace("\\", "\\\\");
        return (this != minimal || replace.equals("true") || replace.equals("false") || replace.equals("null") || !e.matcher(replace).matches()) ? '\"' + replace.replace("\"", "\\\"") + '\"' : replace;
    }

    public String a(String str) {
        String replace = str.replace("\\", "\\\\");
        switch (r.f233a[ordinal()]) {
            case 1:
                return !f.matcher(replace).matches() ? '\"' + replace.replace("\"", "\\\"") + '\"' : replace;
            case 2:
                return !d.matcher(replace).matches() ? '\"' + replace.replace("\"", "\\\"") + '\"' : replace;
            default:
                return '\"' + replace.replace("\"", "\\\"") + '\"';
        }
    }
}
