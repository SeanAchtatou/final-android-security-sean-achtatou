package com.esotericsoftware.a;

class o {

    /* renamed from: b  reason: collision with root package name */
    public boolean f230b;
    final l c;
    int d;
    int e;

    public o(l lVar) {
        this.c = lVar;
        b();
    }

    public void b() {
        this.e = -1;
        this.d = -1;
        c();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f230b = false;
        Object[] objArr = this.c.f226b;
        int i = this.c.d + this.c.e;
        do {
            int i2 = this.d + 1;
            this.d = i2;
            if (i2 >= i) {
                return;
            }
        } while (objArr[this.d] == null);
        this.f230b = true;
    }

    public void remove() {
        if (this.e < 0) {
            throw new IllegalStateException("next must be called before remove.");
        }
        if (this.e >= this.c.d) {
            this.c.a(this.e);
        } else {
            this.c.f226b[this.e] = null;
            this.c.c[this.e] = null;
        }
        this.e = -1;
        l lVar = this.c;
        lVar.f225a--;
    }
}
