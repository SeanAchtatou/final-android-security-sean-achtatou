package com.esotericsoftware.kryo.io;

import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.util.DefaultClassResolver;
import java.io.IOException;
import java.io.InputStream;

public class Input extends InputStream {
    private byte[] buffer;
    private int capacity;
    private char[] chars;
    private InputStream inputStream;
    private int limit;
    private int position;
    private int total;

    public Input() {
        this.chars = new char[32];
    }

    public Input(int i) {
        this.chars = new char[32];
        this.capacity = i;
        this.buffer = new byte[i];
    }

    public Input(InputStream inputStream2) {
        this(4096);
        if (inputStream2 == null) {
            throw new IllegalArgumentException("inputStream cannot be null.");
        }
        this.inputStream = inputStream2;
    }

    public Input(InputStream inputStream2, int i) {
        this(i);
        if (inputStream2 == null) {
            throw new IllegalArgumentException("inputStream cannot be null.");
        }
        this.inputStream = inputStream2;
    }

    public Input(byte[] bArr) {
        this.chars = new char[32];
        setBuffer(bArr, 0, bArr.length);
    }

    public Input(byte[] bArr, int i, int i2) {
        this.chars = new char[32];
        setBuffer(bArr, i, i2);
    }

    private int optional(int i) {
        int i2 = this.limit - this.position;
        if (i2 >= i) {
            return i;
        }
        int min = Math.min(i, this.capacity);
        System.arraycopy(this.buffer, this.position, this.buffer, 0, i2);
        this.total += this.position;
        this.position = 0;
        do {
            int fill = fill(this.buffer, i2, this.capacity - i2);
            if (fill == -1) {
                break;
            }
            i2 += fill;
        } while (i2 < min);
        this.limit = i2;
        return i2 == 0 ? -1 : Math.min(i2, min);
    }

    private String readAscii() {
        byte[] bArr = this.buffer;
        int i = this.position;
        int i2 = i - 1;
        int i3 = this.limit;
        while (i != i3) {
            int i4 = i + 1;
            if ((bArr[i] & 128) != 0) {
                int i5 = i4 - 1;
                bArr[i5] = (byte) (bArr[i5] & Byte.MAX_VALUE);
                String str = new String(bArr, 0, i2, i4 - i2);
                int i6 = i4 - 1;
                bArr[i6] = (byte) (bArr[i6] | 128);
                this.position = i4;
                return str;
            }
            i = i4;
        }
        return readAscii_slow();
    }

    private String readAscii_slow() {
        this.position--;
        int i = this.limit - this.position;
        if (i > this.chars.length) {
            this.chars = new char[(i * 2)];
        }
        char[] cArr = this.chars;
        byte[] bArr = this.buffer;
        int i2 = this.position;
        int i3 = this.limit;
        int i4 = i2;
        int i5 = 0;
        while (i4 < i3) {
            cArr[i5] = (char) bArr[i4];
            i4++;
            i5++;
        }
        this.position = this.limit;
        while (true) {
            require(1);
            int i6 = this.position;
            this.position = i6 + 1;
            byte b2 = bArr[i6];
            if (i == cArr.length) {
                char[] cArr2 = new char[(i * 2)];
                System.arraycopy(cArr, 0, cArr2, 0, i);
                this.chars = cArr2;
                cArr = cArr2;
            }
            if ((b2 & 128) == 128) {
                cArr[i] = (char) (b2 & Byte.MAX_VALUE);
                return new String(cArr, 0, i + 1);
            }
            cArr[i] = (char) b2;
            i++;
        }
    }

    private int readInt_slow(boolean z) {
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte b2 = bArr[i];
        byte b3 = b2 & Byte.MAX_VALUE;
        if ((b2 & 128) != 0) {
            require(1);
            byte[] bArr2 = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            byte b4 = bArr2[i2];
            b3 |= (b4 & Byte.MAX_VALUE) << 7;
            if ((b4 & 128) != 0) {
                require(1);
                int i3 = this.position;
                this.position = i3 + 1;
                byte b5 = bArr2[i3];
                b3 |= (b5 & Byte.MAX_VALUE) << 14;
                if ((b5 & 128) != 0) {
                    require(1);
                    int i4 = this.position;
                    this.position = i4 + 1;
                    byte b6 = bArr2[i4];
                    b3 |= (b6 & Byte.MAX_VALUE) << 21;
                    if ((b6 & 128) != 0) {
                        require(1);
                        int i5 = this.position;
                        this.position = i5 + 1;
                        b3 |= (bArr2[i5] & Byte.MAX_VALUE) << 28;
                    }
                }
            }
        }
        if (z) {
            return b3;
        }
        return (-(b3 & 1)) ^ (b3 >>> 1);
    }

    private long readLong_slow(boolean z) {
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte b2 = bArr[i];
        long j = (long) (b2 & Byte.MAX_VALUE);
        if ((b2 & 128) != 0) {
            require(1);
            byte[] bArr2 = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            byte b3 = bArr2[i2];
            j |= (long) ((b3 & Byte.MAX_VALUE) << 7);
            if ((b3 & 128) != 0) {
                require(1);
                int i3 = this.position;
                this.position = i3 + 1;
                byte b4 = bArr2[i3];
                j |= (long) ((b4 & Byte.MAX_VALUE) << 14);
                if ((b4 & 128) != 0) {
                    require(1);
                    int i4 = this.position;
                    this.position = i4 + 1;
                    byte b5 = bArr2[i4];
                    j |= (long) ((b5 & Byte.MAX_VALUE) << 21);
                    if ((b5 & 128) != 0) {
                        require(1);
                        int i5 = this.position;
                        this.position = i5 + 1;
                        byte b6 = bArr2[i5];
                        j |= ((long) (b6 & Byte.MAX_VALUE)) << 28;
                        if ((b6 & 128) != 0) {
                            require(1);
                            int i6 = this.position;
                            this.position = i6 + 1;
                            byte b7 = bArr2[i6];
                            j |= ((long) (b7 & Byte.MAX_VALUE)) << 35;
                            if ((b7 & 128) != 0) {
                                require(1);
                                int i7 = this.position;
                                this.position = i7 + 1;
                                byte b8 = bArr2[i7];
                                j |= ((long) (b8 & Byte.MAX_VALUE)) << 42;
                                if ((b8 & 128) != 0) {
                                    require(1);
                                    int i8 = this.position;
                                    this.position = i8 + 1;
                                    byte b9 = bArr2[i8];
                                    j |= ((long) (b9 & Byte.MAX_VALUE)) << 49;
                                    if ((b9 & 128) != 0) {
                                        require(1);
                                        int i9 = this.position;
                                        this.position = i9 + 1;
                                        j |= ((long) bArr2[i9]) << 56;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (z) {
            return j;
        }
        return (-(j & 1)) ^ (j >>> 1);
    }

    private void readUtf8(int i) {
        byte[] bArr = this.buffer;
        char[] cArr = this.chars;
        int min = Math.min(require(1), i);
        int i2 = this.position;
        int i3 = 0;
        while (true) {
            if (i3 >= min) {
                break;
            }
            int i4 = i2 + 1;
            byte b2 = bArr[i2];
            if (b2 < 0) {
                i2 = i4 - 1;
                break;
            }
            cArr[i3] = (char) b2;
            i3++;
            i2 = i4;
        }
        this.position = i2;
        if (i3 < i) {
            readUtf8_slow(i, i3);
        }
    }

    private int readUtf8Length(int i) {
        int i2 = i & 63;
        if ((i & 64) == 0) {
            return i2;
        }
        byte[] bArr = this.buffer;
        int i3 = this.position;
        this.position = i3 + 1;
        byte b2 = bArr[i3];
        int i4 = i2 | ((b2 & Byte.MAX_VALUE) << 6);
        if ((b2 & 128) == 0) {
            return i4;
        }
        int i5 = this.position;
        this.position = i5 + 1;
        byte b3 = bArr[i5];
        int i6 = i4 | ((b3 & Byte.MAX_VALUE) << 13);
        if ((b3 & 128) == 0) {
            return i6;
        }
        int i7 = this.position;
        this.position = i7 + 1;
        byte b4 = bArr[i7];
        int i8 = i6 | ((b4 & Byte.MAX_VALUE) << 20);
        if ((b4 & 128) == 0) {
            return i8;
        }
        int i9 = this.position;
        this.position = i9 + 1;
        return i8 | ((bArr[i9] & Byte.MAX_VALUE) << 27);
    }

    private int readUtf8Length_slow(int i) {
        int i2 = i & 63;
        if ((i & 64) == 0) {
            return i2;
        }
        require(1);
        byte[] bArr = this.buffer;
        int i3 = this.position;
        this.position = i3 + 1;
        byte b2 = bArr[i3];
        int i4 = i2 | ((b2 & Byte.MAX_VALUE) << 6);
        if ((b2 & 128) == 0) {
            return i4;
        }
        require(1);
        int i5 = this.position;
        this.position = i5 + 1;
        byte b3 = bArr[i5];
        int i6 = i4 | ((b3 & Byte.MAX_VALUE) << 13);
        if ((b3 & 128) == 0) {
            return i6;
        }
        require(1);
        int i7 = this.position;
        this.position = i7 + 1;
        byte b4 = bArr[i7];
        int i8 = i6 | ((b4 & Byte.MAX_VALUE) << 20);
        if ((b4 & 128) == 0) {
            return i8;
        }
        require(1);
        int i9 = this.position;
        this.position = i9 + 1;
        return i8 | ((bArr[i9] & Byte.MAX_VALUE) << 27);
    }

    private void readUtf8_slow(int i, int i2) {
        char[] cArr = this.chars;
        byte[] bArr = this.buffer;
        while (i2 < i) {
            if (this.position == this.limit) {
                require(1);
            }
            int i3 = this.position;
            this.position = i3 + 1;
            byte b2 = bArr[i3] & DefaultClassResolver.NAME;
            switch (b2 >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    cArr[i2] = (char) b2;
                    break;
                case 12:
                case 13:
                    if (this.position == this.limit) {
                        require(1);
                    }
                    int i4 = this.position;
                    this.position = i4 + 1;
                    cArr[i2] = (char) (((b2 & 31) << 6) | (bArr[i4] & 63));
                    break;
                case 14:
                    require(2);
                    int i5 = this.position;
                    this.position = i5 + 1;
                    byte b3 = ((b2 & 15) << 12) | ((bArr[i5] & 63) << 6);
                    int i6 = this.position;
                    this.position = i6 + 1;
                    cArr[i2] = (char) (b3 | (bArr[i6] & 63));
                    break;
            }
            i2++;
        }
    }

    private int require(int i) {
        int i2 = this.limit - this.position;
        if (i2 < i) {
            if (i > this.capacity) {
                throw new KryoException("Buffer too small: capacity: " + this.capacity + ", required: " + i);
            }
            System.arraycopy(this.buffer, this.position, this.buffer, 0, i2);
            this.total += this.position;
            this.position = 0;
            while (true) {
                int fill = fill(this.buffer, i2, this.capacity - i2);
                if (fill != -1) {
                    i2 += fill;
                    if (i2 >= i) {
                        break;
                    }
                } else if (i2 >= i) {
                    this.limit = i2;
                } else {
                    throw new KryoException("Buffer underflow.");
                }
            }
            this.limit = i2;
        }
        return i2;
    }

    public boolean canReadInt() {
        if (this.limit - this.position >= 5) {
            return true;
        }
        if (optional(5) <= 0) {
            return false;
        }
        int i = this.position;
        int i2 = i + 1;
        if ((this.buffer[i] & 128) == 0) {
            return true;
        }
        if (i2 == this.limit) {
            return false;
        }
        int i3 = i2 + 1;
        if ((this.buffer[i2] & 128) == 0) {
            return true;
        }
        if (i3 == this.limit) {
            return false;
        }
        int i4 = i3 + 1;
        if ((this.buffer[i3] & 128) == 0) {
            return true;
        }
        if (i4 == this.limit) {
            return false;
        }
        return (this.buffer[i4] & 128) == 0 || i4 + 1 != this.limit;
    }

    public boolean canReadLong() {
        if (this.limit - this.position >= 9) {
            return true;
        }
        if (optional(5) <= 0) {
            return false;
        }
        int i = this.position;
        int i2 = i + 1;
        if ((this.buffer[i] & 128) == 0) {
            return true;
        }
        if (i2 == this.limit) {
            return false;
        }
        int i3 = i2 + 1;
        if ((this.buffer[i2] & 128) == 0) {
            return true;
        }
        if (i3 == this.limit) {
            return false;
        }
        int i4 = i3 + 1;
        if ((this.buffer[i3] & 128) == 0) {
            return true;
        }
        if (i4 == this.limit) {
            return false;
        }
        int i5 = i4 + 1;
        if ((this.buffer[i4] & 128) == 0) {
            return true;
        }
        if (i5 == this.limit) {
            return false;
        }
        int i6 = i5 + 1;
        if ((this.buffer[i5] & 128) == 0) {
            return true;
        }
        if (i6 == this.limit) {
            return false;
        }
        int i7 = i6 + 1;
        if ((this.buffer[i6] & 128) == 0) {
            return true;
        }
        if (i7 == this.limit) {
            return false;
        }
        int i8 = i7 + 1;
        if ((this.buffer[i7] & 128) == 0) {
            return true;
        }
        if (i8 == this.limit) {
            return false;
        }
        return (this.buffer[i8] & 128) == 0 || i8 + 1 != this.limit;
    }

    public void close() {
        if (this.inputStream != null) {
            try {
                this.inputStream.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public int fill(byte[] bArr, int i, int i2) {
        if (this.inputStream == null) {
            return -1;
        }
        try {
            return this.inputStream.read(bArr, i, i2);
        } catch (IOException e) {
            throw new KryoException(e);
        }
    }

    public byte[] getBuffer() {
        return this.buffer;
    }

    public InputStream getInputStream() {
        return this.inputStream;
    }

    public int limit() {
        return this.limit;
    }

    public int position() {
        return this.position;
    }

    public int read() {
        if (optional(1) == 0) {
            return -1;
        }
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        return bArr[i] & DefaultClassResolver.NAME;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("bytes cannot be null.");
        }
        int min = Math.min(this.limit - this.position, i2);
        int i3 = i2;
        while (true) {
            System.arraycopy(this.buffer, this.position, bArr, i, min);
            this.position += min;
            i3 -= min;
            if (i3 != 0) {
                i += min;
                min = optional(i3);
                if (min != -1) {
                    if (this.position == this.limit) {
                        break;
                    }
                } else if (i2 == i3) {
                    return -1;
                }
            } else {
                break;
            }
        }
        return i2 - i3;
    }

    public boolean readBoolean() {
        require(1);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        return bArr[i] == 1;
    }

    public byte readByte() {
        require(1);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        return bArr[i];
    }

    public int readByteUnsigned() {
        require(1);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        return bArr[i] & DefaultClassResolver.NAME;
    }

    public void readBytes(byte[] bArr) {
        readBytes(bArr, 0, bArr.length);
    }

    public void readBytes(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("bytes cannot be null.");
        }
        int min = Math.min(this.limit - this.position, i2);
        while (true) {
            System.arraycopy(this.buffer, this.position, bArr, i, min);
            this.position += min;
            i2 -= min;
            if (i2 != 0) {
                i += min;
                min = Math.min(i2, this.capacity);
                require(min);
            } else {
                return;
            }
        }
    }

    public byte[] readBytes(int i) {
        byte[] bArr = new byte[i];
        readBytes(bArr, 0, i);
        return bArr;
    }

    public char readChar() {
        require(2);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte[] bArr2 = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        return (char) (((bArr[i] & DefaultClassResolver.NAME) << 8) | (bArr2[i2] & DefaultClassResolver.NAME));
    }

    public double readDouble() {
        return Double.longBitsToDouble(readLong());
    }

    public double readDouble(double d, boolean z) {
        return ((double) readLong(z)) / d;
    }

    public float readFloat() {
        return Float.intBitsToFloat(readInt());
    }

    public float readFloat(float f, boolean z) {
        return ((float) readInt(z)) / f;
    }

    public int readInt() {
        require(4);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 4;
        return (bArr[i + 3] & DefaultClassResolver.NAME) | ((bArr[i] & DefaultClassResolver.NAME) << 24) | ((bArr[i + 1] & DefaultClassResolver.NAME) << 16) | ((bArr[i + 2] & DefaultClassResolver.NAME) << 8);
    }

    public int readInt(boolean z) {
        if (require(1) < 5) {
            return readInt_slow(z);
        }
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte b2 = bArr[i];
        byte b3 = b2 & Byte.MAX_VALUE;
        if ((b2 & 128) != 0) {
            byte[] bArr2 = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            byte b4 = bArr2[i2];
            b3 |= (b4 & Byte.MAX_VALUE) << 7;
            if ((b4 & 128) != 0) {
                int i3 = this.position;
                this.position = i3 + 1;
                byte b5 = bArr2[i3];
                b3 |= (b5 & Byte.MAX_VALUE) << 14;
                if ((b5 & 128) != 0) {
                    int i4 = this.position;
                    this.position = i4 + 1;
                    byte b6 = bArr2[i4];
                    b3 |= (b6 & Byte.MAX_VALUE) << 21;
                    if ((b6 & 128) != 0) {
                        int i5 = this.position;
                        this.position = i5 + 1;
                        b3 |= (bArr2[i5] & Byte.MAX_VALUE) << 28;
                    }
                }
            }
        }
        if (z) {
            return b3;
        }
        return (-(b3 & 1)) ^ (b3 >>> 1);
    }

    public long readLong() {
        require(8);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        int i2 = this.position;
        this.position = i2 + 1;
        long j = (((long) bArr[i]) << 56) | (((long) (bArr[i2] & DefaultClassResolver.NAME)) << 48);
        int i3 = this.position;
        this.position = i3 + 1;
        long j2 = j | (((long) (bArr[i3] & DefaultClassResolver.NAME)) << 40);
        int i4 = this.position;
        this.position = i4 + 1;
        long j3 = j2 | (((long) (bArr[i4] & DefaultClassResolver.NAME)) << 32);
        int i5 = this.position;
        this.position = i5 + 1;
        long j4 = j3 | (((long) (bArr[i5] & DefaultClassResolver.NAME)) << 24);
        int i6 = this.position;
        this.position = i6 + 1;
        long j5 = j4 | ((long) ((bArr[i6] & DefaultClassResolver.NAME) << 16));
        int i7 = this.position;
        this.position = i7 + 1;
        long j6 = j5 | ((long) ((bArr[i7] & DefaultClassResolver.NAME) << 8));
        int i8 = this.position;
        this.position = i8 + 1;
        return ((long) (bArr[i8] & DefaultClassResolver.NAME)) | j6;
    }

    public long readLong(boolean z) {
        if (require(1) < 9) {
            return readLong_slow(z);
        }
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte b2 = bArr[i];
        long j = (long) (b2 & Byte.MAX_VALUE);
        if ((b2 & 128) != 0) {
            byte[] bArr2 = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            byte b3 = bArr2[i2];
            j |= (long) ((b3 & Byte.MAX_VALUE) << 7);
            if ((b3 & 128) != 0) {
                int i3 = this.position;
                this.position = i3 + 1;
                byte b4 = bArr2[i3];
                j |= (long) ((b4 & Byte.MAX_VALUE) << 14);
                if ((b4 & 128) != 0) {
                    int i4 = this.position;
                    this.position = i4 + 1;
                    byte b5 = bArr2[i4];
                    j |= (long) ((b5 & Byte.MAX_VALUE) << 21);
                    if ((b5 & 128) != 0) {
                        int i5 = this.position;
                        this.position = i5 + 1;
                        byte b6 = bArr2[i5];
                        j |= ((long) (b6 & Byte.MAX_VALUE)) << 28;
                        if ((b6 & 128) != 0) {
                            int i6 = this.position;
                            this.position = i6 + 1;
                            byte b7 = bArr2[i6];
                            j |= ((long) (b7 & Byte.MAX_VALUE)) << 35;
                            if ((b7 & 128) != 0) {
                                int i7 = this.position;
                                this.position = i7 + 1;
                                byte b8 = bArr2[i7];
                                j |= ((long) (b8 & Byte.MAX_VALUE)) << 42;
                                if ((b8 & 128) != 0) {
                                    int i8 = this.position;
                                    this.position = i8 + 1;
                                    byte b9 = bArr2[i8];
                                    j |= ((long) (b9 & Byte.MAX_VALUE)) << 49;
                                    if ((b9 & 128) != 0) {
                                        int i9 = this.position;
                                        this.position = i9 + 1;
                                        j |= ((long) bArr2[i9]) << 56;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (z) {
            return j;
        }
        return (-(j & 1)) ^ (j >>> 1);
    }

    public short readShort() {
        require(2);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte[] bArr2 = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        return (short) (((bArr[i] & DefaultClassResolver.NAME) << 8) | (bArr2[i2] & DefaultClassResolver.NAME));
    }

    public int readShortUnsigned() {
        require(2);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte[] bArr2 = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        return ((bArr[i] & DefaultClassResolver.NAME) << 8) | (bArr2[i2] & DefaultClassResolver.NAME);
    }

    public String readString() {
        int require = require(1);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte b2 = bArr[i];
        if ((b2 & 128) == 0) {
            return readAscii();
        }
        int readUtf8Length = require >= 5 ? readUtf8Length(b2) : readUtf8Length_slow(b2);
        switch (readUtf8Length) {
            case 0:
                return null;
            case 1:
                return "";
            default:
                int i2 = readUtf8Length - 1;
                if (this.chars.length < i2) {
                    this.chars = new char[i2];
                }
                readUtf8(i2);
                return new String(this.chars, 0, i2);
        }
    }

    public StringBuilder readStringBuilder() {
        int require = require(1);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        byte b2 = bArr[i];
        if ((b2 & 128) == 0) {
            return new StringBuilder(readAscii());
        }
        int readUtf8Length = require >= 5 ? readUtf8Length(b2) : readUtf8Length_slow(b2);
        switch (readUtf8Length) {
            case 0:
                return null;
            case 1:
                return new StringBuilder("");
            default:
                int i2 = readUtf8Length - 1;
                if (this.chars.length < i2) {
                    this.chars = new char[i2];
                }
                readUtf8(i2);
                StringBuilder sb = new StringBuilder(i2);
                sb.append(this.chars, 0, i2);
                return sb;
        }
    }

    public void rewind() {
        this.position = 0;
        this.total = 0;
    }

    public void setBuffer(byte[] bArr) {
        setBuffer(bArr, 0, bArr.length);
    }

    public void setBuffer(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("bytes cannot be null.");
        }
        this.buffer = bArr;
        this.position = i;
        this.limit = i2;
        this.capacity = bArr.length;
        this.total = 0;
        this.inputStream = null;
    }

    public void setInputStream(InputStream inputStream2) {
        this.inputStream = inputStream2;
        this.limit = 0;
        rewind();
    }

    public void setLimit(int i) {
        this.limit = i;
    }

    public void setPosition(int i) {
        this.position = i;
    }

    public void setTotal(int i) {
        this.total = i;
    }

    public long skip(long j) {
        long j2 = j;
        while (j2 > 0) {
            int max = Math.max(Integer.MAX_VALUE, (int) j2);
            skip(max);
            j2 -= (long) max;
        }
        return j;
    }

    public void skip(int i) {
        int min = Math.min(this.limit - this.position, i);
        while (true) {
            this.position += min;
            i -= min;
            if (i != 0) {
                min = Math.min(i, this.capacity);
                require(min);
            } else {
                return;
            }
        }
    }

    public int total() {
        return this.total + this.position;
    }
}
