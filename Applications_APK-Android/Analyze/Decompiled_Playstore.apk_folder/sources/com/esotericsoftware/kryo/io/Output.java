package com.esotericsoftware.kryo.io;

import com.esotericsoftware.kryo.KryoException;
import java.io.IOException;
import java.io.OutputStream;

public class Output extends OutputStream {
    private byte[] buffer;
    private int capacity;
    private int maxCapacity;
    private OutputStream outputStream;
    private int position;
    private int total;

    public Output() {
    }

    public Output(int i) {
        this(i, i);
    }

    public Output(int i, int i2) {
        if (i2 < -1) {
            throw new IllegalArgumentException("maxBufferSize cannot be < -1: " + i2);
        }
        this.capacity = i;
        this.maxCapacity = i2 == -1 ? Integer.MAX_VALUE : i2;
        this.buffer = new byte[i];
    }

    public Output(OutputStream outputStream2) {
        this(4096, 4096);
        if (outputStream2 == null) {
            throw new IllegalArgumentException("outputStream cannot be null.");
        }
        this.outputStream = outputStream2;
    }

    public Output(OutputStream outputStream2, int i) {
        this(i, i);
        if (outputStream2 == null) {
            throw new IllegalArgumentException("outputStream cannot be null.");
        }
        this.outputStream = outputStream2;
    }

    public Output(byte[] bArr) {
        this(bArr, bArr.length);
    }

    public Output(byte[] bArr, int i) {
        if (bArr == null) {
            throw new IllegalArgumentException("buffer cannot be null.");
        }
        setBuffer(bArr, i);
    }

    public static int intLength(int i, boolean z) {
        if (!z) {
            i = (i << 1) ^ (i >> 31);
        }
        if ((i >>> 7) == 0) {
            return 1;
        }
        if ((i >>> 14) == 0) {
            return 2;
        }
        if ((i >>> 21) == 0) {
            return 3;
        }
        return (i >>> 28) == 0 ? 4 : 5;
    }

    public static int longLength(long j, boolean z) {
        if (!z) {
            j = (j << 1) ^ (j >> 63);
        }
        if ((j >>> 7) == 0) {
            return 1;
        }
        if ((j >>> 14) == 0) {
            return 2;
        }
        if ((j >>> 21) == 0) {
            return 3;
        }
        if ((j >>> 28) == 0) {
            return 4;
        }
        if ((j >>> 35) == 0) {
            return 5;
        }
        if ((j >>> 42) == 0) {
            return 6;
        }
        if ((j >>> 49) == 0) {
            return 7;
        }
        return (j >>> 56) == 0 ? 8 : 9;
    }

    private boolean require(int i) {
        if (this.capacity - this.position >= i) {
            return false;
        }
        if (i > this.maxCapacity) {
            throw new KryoException("Buffer overflow. Max capacity: " + this.maxCapacity + ", required: " + i);
        }
        flush();
        while (this.capacity - this.position < i) {
            if (this.capacity == this.maxCapacity) {
                throw new KryoException("Buffer overflow. Available: " + (this.capacity - this.position) + ", required: " + i);
            }
            this.capacity = Math.min(this.capacity * 2, this.maxCapacity);
            if (this.capacity < 0) {
                this.capacity = this.maxCapacity;
            }
            byte[] bArr = new byte[this.capacity];
            System.arraycopy(this.buffer, 0, bArr, 0, this.position);
            this.buffer = bArr;
        }
        return true;
    }

    private void writeAscii_slow(String str, int i) {
        byte[] bArr = this.buffer;
        int i2 = 0;
        int min = Math.min(i, this.capacity - this.position);
        while (i2 < i) {
            str.getBytes(i2, i2 + min, bArr, this.position);
            i2 += min;
            this.position = min + this.position;
            min = Math.min(i - i2, this.capacity);
            if (require(min)) {
                bArr = this.buffer;
            }
        }
    }

    private void writeString_slow(CharSequence charSequence, int i, int i2) {
        while (i2 < i) {
            if (this.position == this.capacity) {
                require(Math.min(this.capacity, i - i2));
            }
            char charAt = charSequence.charAt(i2);
            if (charAt <= 127) {
                byte[] bArr = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr[i3] = (byte) charAt;
            } else if (charAt > 2047) {
                byte[] bArr2 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                bArr2[i4] = (byte) (((charAt >> 12) & 15) | 224);
                require(2);
                byte[] bArr3 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                bArr3[i5] = (byte) (((charAt >> 6) & 63) | 128);
                byte[] bArr4 = this.buffer;
                int i6 = this.position;
                this.position = i6 + 1;
                bArr4[i6] = (byte) ((charAt & '?') | 128);
            } else {
                byte[] bArr5 = this.buffer;
                int i7 = this.position;
                this.position = i7 + 1;
                bArr5[i7] = (byte) (((charAt >> 6) & 31) | 192);
                require(1);
                byte[] bArr6 = this.buffer;
                int i8 = this.position;
                this.position = i8 + 1;
                bArr6[i8] = (byte) ((charAt & '?') | 128);
            }
            i2++;
        }
    }

    private void writeUtf8Length(int i) {
        if ((i >>> 6) == 0) {
            require(1);
            byte[] bArr = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            bArr[i2] = (byte) (i | 128);
        } else if ((i >>> 13) == 0) {
            require(2);
            byte[] bArr2 = this.buffer;
            int i3 = this.position;
            this.position = i3 + 1;
            bArr2[i3] = (byte) (i | 64 | 128);
            int i4 = this.position;
            this.position = i4 + 1;
            bArr2[i4] = (byte) (i >>> 6);
        } else if ((i >>> 20) == 0) {
            require(3);
            byte[] bArr3 = this.buffer;
            int i5 = this.position;
            this.position = i5 + 1;
            bArr3[i5] = (byte) (i | 64 | 128);
            int i6 = this.position;
            this.position = i6 + 1;
            bArr3[i6] = (byte) ((i >>> 6) | 128);
            int i7 = this.position;
            this.position = i7 + 1;
            bArr3[i7] = (byte) (i >>> 13);
        } else if ((i >>> 27) == 0) {
            require(4);
            byte[] bArr4 = this.buffer;
            int i8 = this.position;
            this.position = i8 + 1;
            bArr4[i8] = (byte) (i | 64 | 128);
            int i9 = this.position;
            this.position = i9 + 1;
            bArr4[i9] = (byte) ((i >>> 6) | 128);
            int i10 = this.position;
            this.position = i10 + 1;
            bArr4[i10] = (byte) ((i >>> 13) | 128);
            int i11 = this.position;
            this.position = i11 + 1;
            bArr4[i11] = (byte) (i >>> 20);
        } else {
            require(5);
            byte[] bArr5 = this.buffer;
            int i12 = this.position;
            this.position = i12 + 1;
            bArr5[i12] = (byte) (i | 64 | 128);
            int i13 = this.position;
            this.position = i13 + 1;
            bArr5[i13] = (byte) ((i >>> 6) | 128);
            int i14 = this.position;
            this.position = i14 + 1;
            bArr5[i14] = (byte) ((i >>> 13) | 128);
            int i15 = this.position;
            this.position = i15 + 1;
            bArr5[i15] = (byte) ((i >>> 20) | 128);
            int i16 = this.position;
            this.position = i16 + 1;
            bArr5[i16] = (byte) (i >>> 27);
        }
    }

    public void clear() {
        this.position = 0;
        this.total = 0;
    }

    public void close() {
        flush();
        if (this.outputStream != null) {
            try {
                this.outputStream.close();
            } catch (IOException e) {
            }
        }
    }

    public void flush() {
        if (this.outputStream != null) {
            try {
                this.outputStream.write(this.buffer, 0, this.position);
                this.total += this.position;
                this.position = 0;
            } catch (IOException e) {
                throw new KryoException(e);
            }
        }
    }

    public byte[] getBuffer() {
        return this.buffer;
    }

    public OutputStream getOutputStream() {
        return this.outputStream;
    }

    public int position() {
        return this.position;
    }

    public void setBuffer(byte[] bArr) {
        setBuffer(bArr, bArr.length);
    }

    public void setBuffer(byte[] bArr, int i) {
        if (bArr == null) {
            throw new IllegalArgumentException("buffer cannot be null.");
        } else if (i < -1) {
            throw new IllegalArgumentException("maxBufferSize cannot be < -1: " + i);
        } else {
            this.buffer = bArr;
            if (i == -1) {
                i = Integer.MAX_VALUE;
            }
            this.maxCapacity = i;
            this.capacity = bArr.length;
            this.position = 0;
            this.total = 0;
            this.outputStream = null;
        }
    }

    public void setOutputStream(OutputStream outputStream2) {
        this.outputStream = outputStream2;
        this.position = 0;
        this.total = 0;
    }

    public void setPosition(int i) {
        this.position = i;
    }

    public byte[] toBytes() {
        byte[] bArr = new byte[this.position];
        System.arraycopy(this.buffer, 0, bArr, 0, this.position);
        return bArr;
    }

    public int total() {
        return this.total + this.position;
    }

    public void write(int i) {
        if (this.position == this.capacity) {
            require(1);
        }
        byte[] bArr = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public void write(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("bytes cannot be null.");
        }
        writeBytes(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        writeBytes(bArr, i, i2);
    }

    public void writeAscii(String str) {
        if (str == null) {
            writeByte(128);
            return;
        }
        int length = str.length();
        if (length == 0) {
            writeByte(129);
            return;
        }
        if (this.capacity - this.position < length) {
            writeAscii_slow(str, length);
        } else {
            str.getBytes(0, length, this.buffer, this.position);
            this.position = length + this.position;
        }
        byte[] bArr = this.buffer;
        int i = this.position - 1;
        bArr[i] = (byte) (bArr[i] | 128);
    }

    public void writeBoolean(boolean z) {
        int i = 1;
        require(1);
        byte[] bArr = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        if (!z) {
            i = 0;
        }
        bArr[i2] = (byte) i;
    }

    public void writeByte(byte b2) {
        if (this.position == this.capacity) {
            require(1);
        }
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        bArr[i] = b2;
    }

    public void writeByte(int i) {
        if (this.position == this.capacity) {
            require(1);
        }
        byte[] bArr = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        bArr[i2] = (byte) i;
    }

    public void writeBytes(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("bytes cannot be null.");
        }
        writeBytes(bArr, 0, bArr.length);
    }

    public void writeBytes(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("bytes cannot be null.");
        }
        int min = Math.min(this.capacity - this.position, i2);
        while (true) {
            System.arraycopy(bArr, i, this.buffer, this.position, min);
            this.position += min;
            i2 -= min;
            if (i2 != 0) {
                i += min;
                min = Math.min(this.capacity, i2);
                require(min);
            } else {
                return;
            }
        }
    }

    public void writeChar(char c) {
        require(2);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        bArr[i] = (byte) (c >>> 8);
        byte[] bArr2 = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        bArr2[i2] = (byte) c;
    }

    public int writeDouble(double d, double d2, boolean z) {
        return writeLong((long) (d * d2), z);
    }

    public void writeDouble(double d) {
        writeLong(Double.doubleToLongBits(d));
    }

    public int writeFloat(float f, float f2, boolean z) {
        return writeInt((int) (f * f2), z);
    }

    public void writeFloat(float f) {
        writeInt(Float.floatToIntBits(f));
    }

    public int writeInt(int i, boolean z) {
        if (!z) {
            i = (i << 1) ^ (i >> 31);
        }
        if ((i >>> 7) == 0) {
            require(1);
            byte[] bArr = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            bArr[i2] = (byte) i;
            return 1;
        } else if ((i >>> 14) == 0) {
            require(2);
            byte[] bArr2 = this.buffer;
            int i3 = this.position;
            this.position = i3 + 1;
            bArr2[i3] = (byte) ((i & 127) | 128);
            byte[] bArr3 = this.buffer;
            int i4 = this.position;
            this.position = i4 + 1;
            bArr3[i4] = (byte) (i >>> 7);
            return 2;
        } else if ((i >>> 21) == 0) {
            require(3);
            byte[] bArr4 = this.buffer;
            int i5 = this.position;
            this.position = i5 + 1;
            bArr4[i5] = (byte) ((i & 127) | 128);
            byte[] bArr5 = this.buffer;
            int i6 = this.position;
            this.position = i6 + 1;
            bArr5[i6] = (byte) ((i >>> 7) | 128);
            byte[] bArr6 = this.buffer;
            int i7 = this.position;
            this.position = i7 + 1;
            bArr6[i7] = (byte) (i >>> 14);
            return 3;
        } else if ((i >>> 28) == 0) {
            require(4);
            byte[] bArr7 = this.buffer;
            int i8 = this.position;
            this.position = i8 + 1;
            bArr7[i8] = (byte) ((i & 127) | 128);
            byte[] bArr8 = this.buffer;
            int i9 = this.position;
            this.position = i9 + 1;
            bArr8[i9] = (byte) ((i >>> 7) | 128);
            byte[] bArr9 = this.buffer;
            int i10 = this.position;
            this.position = i10 + 1;
            bArr9[i10] = (byte) ((i >>> 14) | 128);
            byte[] bArr10 = this.buffer;
            int i11 = this.position;
            this.position = i11 + 1;
            bArr10[i11] = (byte) (i >>> 21);
            return 4;
        } else {
            require(5);
            byte[] bArr11 = this.buffer;
            int i12 = this.position;
            this.position = i12 + 1;
            bArr11[i12] = (byte) ((i & 127) | 128);
            byte[] bArr12 = this.buffer;
            int i13 = this.position;
            this.position = i13 + 1;
            bArr12[i13] = (byte) ((i >>> 7) | 128);
            byte[] bArr13 = this.buffer;
            int i14 = this.position;
            this.position = i14 + 1;
            bArr13[i14] = (byte) ((i >>> 14) | 128);
            byte[] bArr14 = this.buffer;
            int i15 = this.position;
            this.position = i15 + 1;
            bArr14[i15] = (byte) ((i >>> 21) | 128);
            byte[] bArr15 = this.buffer;
            int i16 = this.position;
            this.position = i16 + 1;
            bArr15[i16] = (byte) (i >>> 28);
            return 5;
        }
    }

    public void writeInt(int i) {
        require(4);
        byte[] bArr = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        bArr[i2] = (byte) (i >> 24);
        int i3 = this.position;
        this.position = i3 + 1;
        bArr[i3] = (byte) (i >> 16);
        int i4 = this.position;
        this.position = i4 + 1;
        bArr[i4] = (byte) (i >> 8);
        int i5 = this.position;
        this.position = i5 + 1;
        bArr[i5] = (byte) i;
    }

    public int writeLong(long j, boolean z) {
        if (!z) {
            j = (j << 1) ^ (j >> 63);
        }
        if ((j >>> 7) == 0) {
            require(1);
            byte[] bArr = this.buffer;
            int i = this.position;
            this.position = i + 1;
            bArr[i] = (byte) ((int) j);
            return 1;
        } else if ((j >>> 14) == 0) {
            require(2);
            byte[] bArr2 = this.buffer;
            int i2 = this.position;
            this.position = i2 + 1;
            bArr2[i2] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr3 = this.buffer;
            int i3 = this.position;
            this.position = i3 + 1;
            bArr3[i3] = (byte) ((int) (j >>> 7));
            return 2;
        } else if ((j >>> 21) == 0) {
            require(3);
            byte[] bArr4 = this.buffer;
            int i4 = this.position;
            this.position = i4 + 1;
            bArr4[i4] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr5 = this.buffer;
            int i5 = this.position;
            this.position = i5 + 1;
            bArr5[i5] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr6 = this.buffer;
            int i6 = this.position;
            this.position = i6 + 1;
            bArr6[i6] = (byte) ((int) (j >>> 14));
            return 3;
        } else if ((j >>> 28) == 0) {
            require(4);
            byte[] bArr7 = this.buffer;
            int i7 = this.position;
            this.position = i7 + 1;
            bArr7[i7] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr8 = this.buffer;
            int i8 = this.position;
            this.position = i8 + 1;
            bArr8[i8] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr9 = this.buffer;
            int i9 = this.position;
            this.position = i9 + 1;
            bArr9[i9] = (byte) ((int) ((j >>> 14) | 128));
            byte[] bArr10 = this.buffer;
            int i10 = this.position;
            this.position = i10 + 1;
            bArr10[i10] = (byte) ((int) (j >>> 21));
            return 4;
        } else if ((j >>> 35) == 0) {
            require(5);
            byte[] bArr11 = this.buffer;
            int i11 = this.position;
            this.position = i11 + 1;
            bArr11[i11] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr12 = this.buffer;
            int i12 = this.position;
            this.position = i12 + 1;
            bArr12[i12] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr13 = this.buffer;
            int i13 = this.position;
            this.position = i13 + 1;
            bArr13[i13] = (byte) ((int) ((j >>> 14) | 128));
            byte[] bArr14 = this.buffer;
            int i14 = this.position;
            this.position = i14 + 1;
            bArr14[i14] = (byte) ((int) ((j >>> 21) | 128));
            byte[] bArr15 = this.buffer;
            int i15 = this.position;
            this.position = i15 + 1;
            bArr15[i15] = (byte) ((int) (j >>> 28));
            return 5;
        } else if ((j >>> 42) == 0) {
            require(6);
            byte[] bArr16 = this.buffer;
            int i16 = this.position;
            this.position = i16 + 1;
            bArr16[i16] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr17 = this.buffer;
            int i17 = this.position;
            this.position = i17 + 1;
            bArr17[i17] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr18 = this.buffer;
            int i18 = this.position;
            this.position = i18 + 1;
            bArr18[i18] = (byte) ((int) ((j >>> 14) | 128));
            byte[] bArr19 = this.buffer;
            int i19 = this.position;
            this.position = i19 + 1;
            bArr19[i19] = (byte) ((int) ((j >>> 21) | 128));
            byte[] bArr20 = this.buffer;
            int i20 = this.position;
            this.position = i20 + 1;
            bArr20[i20] = (byte) ((int) ((j >>> 28) | 128));
            byte[] bArr21 = this.buffer;
            int i21 = this.position;
            this.position = i21 + 1;
            bArr21[i21] = (byte) ((int) (j >>> 35));
            return 6;
        } else if ((j >>> 49) == 0) {
            require(7);
            byte[] bArr22 = this.buffer;
            int i22 = this.position;
            this.position = i22 + 1;
            bArr22[i22] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr23 = this.buffer;
            int i23 = this.position;
            this.position = i23 + 1;
            bArr23[i23] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr24 = this.buffer;
            int i24 = this.position;
            this.position = i24 + 1;
            bArr24[i24] = (byte) ((int) ((j >>> 14) | 128));
            byte[] bArr25 = this.buffer;
            int i25 = this.position;
            this.position = i25 + 1;
            bArr25[i25] = (byte) ((int) ((j >>> 21) | 128));
            byte[] bArr26 = this.buffer;
            int i26 = this.position;
            this.position = i26 + 1;
            bArr26[i26] = (byte) ((int) ((j >>> 28) | 128));
            byte[] bArr27 = this.buffer;
            int i27 = this.position;
            this.position = i27 + 1;
            bArr27[i27] = (byte) ((int) ((j >>> 35) | 128));
            byte[] bArr28 = this.buffer;
            int i28 = this.position;
            this.position = i28 + 1;
            bArr28[i28] = (byte) ((int) (j >>> 42));
            return 7;
        } else if ((j >>> 56) == 0) {
            require(8);
            byte[] bArr29 = this.buffer;
            int i29 = this.position;
            this.position = i29 + 1;
            bArr29[i29] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr30 = this.buffer;
            int i30 = this.position;
            this.position = i30 + 1;
            bArr30[i30] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr31 = this.buffer;
            int i31 = this.position;
            this.position = i31 + 1;
            bArr31[i31] = (byte) ((int) ((j >>> 14) | 128));
            byte[] bArr32 = this.buffer;
            int i32 = this.position;
            this.position = i32 + 1;
            bArr32[i32] = (byte) ((int) ((j >>> 21) | 128));
            byte[] bArr33 = this.buffer;
            int i33 = this.position;
            this.position = i33 + 1;
            bArr33[i33] = (byte) ((int) ((j >>> 28) | 128));
            byte[] bArr34 = this.buffer;
            int i34 = this.position;
            this.position = i34 + 1;
            bArr34[i34] = (byte) ((int) ((j >>> 35) | 128));
            byte[] bArr35 = this.buffer;
            int i35 = this.position;
            this.position = i35 + 1;
            bArr35[i35] = (byte) ((int) ((j >>> 42) | 128));
            byte[] bArr36 = this.buffer;
            int i36 = this.position;
            this.position = i36 + 1;
            bArr36[i36] = (byte) ((int) (j >>> 49));
            return 8;
        } else {
            require(9);
            byte[] bArr37 = this.buffer;
            int i37 = this.position;
            this.position = i37 + 1;
            bArr37[i37] = (byte) ((int) ((127 & j) | 128));
            byte[] bArr38 = this.buffer;
            int i38 = this.position;
            this.position = i38 + 1;
            bArr38[i38] = (byte) ((int) ((j >>> 7) | 128));
            byte[] bArr39 = this.buffer;
            int i39 = this.position;
            this.position = i39 + 1;
            bArr39[i39] = (byte) ((int) ((j >>> 14) | 128));
            byte[] bArr40 = this.buffer;
            int i40 = this.position;
            this.position = i40 + 1;
            bArr40[i40] = (byte) ((int) ((j >>> 21) | 128));
            byte[] bArr41 = this.buffer;
            int i41 = this.position;
            this.position = i41 + 1;
            bArr41[i41] = (byte) ((int) ((j >>> 28) | 128));
            byte[] bArr42 = this.buffer;
            int i42 = this.position;
            this.position = i42 + 1;
            bArr42[i42] = (byte) ((int) ((j >>> 35) | 128));
            byte[] bArr43 = this.buffer;
            int i43 = this.position;
            this.position = i43 + 1;
            bArr43[i43] = (byte) ((int) ((j >>> 42) | 128));
            byte[] bArr44 = this.buffer;
            int i44 = this.position;
            this.position = i44 + 1;
            bArr44[i44] = (byte) ((int) ((j >>> 49) | 128));
            byte[] bArr45 = this.buffer;
            int i45 = this.position;
            this.position = i45 + 1;
            bArr45[i45] = (byte) ((int) (j >>> 56));
            return 9;
        }
    }

    public void writeLong(long j) {
        require(8);
        byte[] bArr = this.buffer;
        int i = this.position;
        this.position = i + 1;
        bArr[i] = (byte) ((int) (j >>> 56));
        int i2 = this.position;
        this.position = i2 + 1;
        bArr[i2] = (byte) ((int) (j >>> 48));
        int i3 = this.position;
        this.position = i3 + 1;
        bArr[i3] = (byte) ((int) (j >>> 40));
        int i4 = this.position;
        this.position = i4 + 1;
        bArr[i4] = (byte) ((int) (j >>> 32));
        int i5 = this.position;
        this.position = i5 + 1;
        bArr[i5] = (byte) ((int) (j >>> 24));
        int i6 = this.position;
        this.position = i6 + 1;
        bArr[i6] = (byte) ((int) (j >>> 16));
        int i7 = this.position;
        this.position = i7 + 1;
        bArr[i7] = (byte) ((int) (j >>> 8));
        int i8 = this.position;
        this.position = i8 + 1;
        bArr[i8] = (byte) ((int) j);
    }

    public void writeShort(int i) {
        require(2);
        byte[] bArr = this.buffer;
        int i2 = this.position;
        this.position = i2 + 1;
        bArr[i2] = (byte) (i >>> 8);
        byte[] bArr2 = this.buffer;
        int i3 = this.position;
        this.position = i3 + 1;
        bArr2[i3] = (byte) i;
    }

    public void writeString(CharSequence charSequence) {
        if (charSequence == null) {
            writeByte(128);
            return;
        }
        int length = charSequence.length();
        if (length == 0) {
            writeByte(129);
            return;
        }
        writeUtf8Length(length + 1);
        int i = 0;
        if (this.capacity - this.position >= length) {
            byte[] bArr = this.buffer;
            int i2 = this.position;
            while (i < length) {
                char charAt = charSequence.charAt(i);
                if (charAt > 127) {
                    break;
                }
                bArr[i2] = (byte) charAt;
                i++;
                i2++;
            }
            this.position = i2;
        }
        if (i < length) {
            writeString_slow(charSequence, length, i);
        }
    }

    public void writeString(String str) {
        boolean z = true;
        int i = 0;
        if (str == null) {
            writeByte(128);
            return;
        }
        int length = str.length();
        if (length == 0) {
            writeByte(129);
            return;
        }
        if (length <= 1 || length >= 64) {
            z = false;
        } else {
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (str.charAt(i2) > 127) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
        }
        if (z) {
            if (this.capacity - this.position < length) {
                writeAscii_slow(str, length);
            } else {
                str.getBytes(0, length, this.buffer, this.position);
                this.position += length;
            }
            byte[] bArr = this.buffer;
            int i3 = this.position - 1;
            bArr[i3] = (byte) (bArr[i3] | 128);
            return;
        }
        writeUtf8Length(length + 1);
        if (this.capacity - this.position >= length) {
            byte[] bArr2 = this.buffer;
            int i4 = this.position;
            while (i < length) {
                char charAt = str.charAt(i);
                if (charAt > 127) {
                    break;
                }
                bArr2[i4] = (byte) charAt;
                i++;
                i4++;
            }
            this.position = i4;
        }
        if (i < length) {
            writeString_slow(str, length, i);
        }
    }
}
