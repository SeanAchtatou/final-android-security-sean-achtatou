package com.esotericsoftware.kryo.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class IdentityMap {
    private static final int PRIME1 = -1105259343;
    private static final int PRIME2 = -1262997959;
    private static final int PRIME3 = -825114047;
    int capacity;
    private Entries entries;
    private int hashShift;
    Object[] keyTable;
    private Keys keys;
    private float loadFactor;
    private int mask;
    private int pushIterations;
    public int size;
    private int stashCapacity;
    int stashSize;
    private int threshold;
    Object[] valueTable;
    private Values values;

    public class Entries extends MapIterator implements Iterable, Iterator {
        private Entry entry = new Entry();

        public Entries(IdentityMap identityMap) {
            super(identityMap);
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public Iterator iterator() {
            return this;
        }

        public Entry next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            }
            Object[] objArr = this.map.keyTable;
            this.entry.key = objArr[this.nextIndex];
            this.entry.value = this.map.valueTable[this.nextIndex];
            this.currentIndex = this.nextIndex;
            findNextIndex();
            return this.entry;
        }

        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }
    }

    public class Entry {
        public Object key;
        public Object value;

        public String toString() {
            return this.key + "=" + this.value;
        }
    }

    public class Keys extends MapIterator implements Iterable, Iterator {
        public Keys(IdentityMap identityMap) {
            super(identityMap);
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public Iterator iterator() {
            return this;
        }

        public Object next() {
            Object obj = this.map.keyTable[this.nextIndex];
            this.currentIndex = this.nextIndex;
            findNextIndex();
            return obj;
        }

        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public ArrayList toArray() {
            ArrayList arrayList = new ArrayList(this.map.size);
            while (this.hasNext) {
                arrayList.add(next());
            }
            return arrayList;
        }
    }

    class MapIterator {
        int currentIndex;
        public boolean hasNext;
        final IdentityMap map;
        int nextIndex;

        public MapIterator(IdentityMap identityMap) {
            this.map = identityMap;
            reset();
        }

        /* access modifiers changed from: package-private */
        public void findNextIndex() {
            this.hasNext = false;
            Object[] objArr = this.map.keyTable;
            int i = this.map.capacity + this.map.stashSize;
            do {
                int i2 = this.nextIndex + 1;
                this.nextIndex = i2;
                if (i2 >= i) {
                    return;
                }
            } while (objArr[this.nextIndex] == null);
            this.hasNext = true;
        }

        public void remove() {
            if (this.currentIndex < 0) {
                throw new IllegalStateException("next must be called before remove.");
            }
            if (this.currentIndex >= this.map.capacity) {
                this.map.removeStashIndex(this.currentIndex);
            } else {
                this.map.keyTable[this.currentIndex] = null;
                this.map.valueTable[this.currentIndex] = null;
            }
            this.currentIndex = -1;
            IdentityMap identityMap = this.map;
            identityMap.size--;
        }

        public void reset() {
            this.currentIndex = -1;
            this.nextIndex = -1;
            findNextIndex();
        }
    }

    public class Values extends MapIterator implements Iterable, Iterator {
        public Values(IdentityMap identityMap) {
            super(identityMap);
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public Iterator iterator() {
            return this;
        }

        public Object next() {
            Object obj = this.map.valueTable[this.nextIndex];
            this.currentIndex = this.nextIndex;
            findNextIndex();
            return obj;
        }

        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public ArrayList toArray() {
            ArrayList arrayList = new ArrayList(this.map.size);
            while (this.hasNext) {
                arrayList.add(next());
            }
            return arrayList;
        }

        public void toArray(ArrayList arrayList) {
            while (this.hasNext) {
                arrayList.add(next());
            }
        }
    }

    public IdentityMap() {
        this(32, 0.8f);
    }

    public IdentityMap(int i) {
        this(i, 0.8f);
    }

    public IdentityMap(int i, float f) {
        if (i < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + i);
        } else if (this.capacity > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + i);
        } else {
            this.capacity = ObjectMap.nextPowerOfTwo(i);
            if (f <= 0.0f) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + f);
            }
            this.loadFactor = f;
            this.threshold = (int) (((float) this.capacity) * f);
            this.mask = this.capacity - 1;
            this.hashShift = 31 - Integer.numberOfTrailingZeros(this.capacity);
            this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) this.capacity))) * 2);
            this.pushIterations = Math.max(Math.min(this.capacity, 8), ((int) Math.sqrt((double) this.capacity)) / 8);
            this.keyTable = new Object[(this.capacity + this.stashCapacity)];
            this.valueTable = new Object[this.keyTable.length];
        }
    }

    private boolean containsKeyStash(Object obj) {
        Object[] objArr = this.keyTable;
        int i = this.capacity;
        int i2 = this.stashSize + i;
        while (i < i2) {
            if (objArr[i] == obj) {
                return true;
            }
            i++;
        }
        return false;
    }

    private Object getStash(Object obj, Object obj2) {
        Object[] objArr = this.keyTable;
        int i = this.capacity;
        int i2 = this.stashSize + i;
        while (i < i2) {
            if (objArr[i] == obj) {
                return this.valueTable[i];
            }
            i++;
        }
        return obj2;
    }

    private int hash2(long j) {
        long j2 = -1262997959 * j;
        return (int) ((j2 ^ (j2 >>> this.hashShift)) & ((long) this.mask));
    }

    private int hash3(long j) {
        long j2 = -825114047 * j;
        return (int) ((j2 ^ (j2 >>> this.hashShift)) & ((long) this.mask));
    }

    private void push(Object obj, Object obj2, int i, Object obj3, int i2, Object obj4, int i3, Object obj5) {
        Object[] objArr = this.keyTable;
        Object[] objArr2 = this.valueTable;
        int i4 = this.mask;
        int i5 = 0;
        int i6 = this.pushIterations;
        do {
            switch (ObjectMap.random.nextInt(3)) {
                case 0:
                    Object obj6 = objArr2[i];
                    objArr[i] = obj;
                    objArr2[i] = obj2;
                    obj2 = obj6;
                    obj = obj3;
                    break;
                case 1:
                    Object obj7 = objArr2[i2];
                    objArr[i2] = obj;
                    objArr2[i2] = obj2;
                    obj2 = obj7;
                    obj = obj4;
                    break;
                default:
                    Object obj8 = objArr2[i3];
                    objArr[i3] = obj;
                    objArr2[i3] = obj2;
                    obj2 = obj8;
                    obj = obj5;
                    break;
            }
            int identityHashCode = System.identityHashCode(obj);
            i = identityHashCode & i4;
            obj3 = objArr[i];
            if (obj3 == null) {
                objArr[i] = obj;
                objArr2[i] = obj2;
                int i7 = this.size;
                this.size = i7 + 1;
                if (i7 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i2 = hash2((long) identityHashCode);
            obj4 = objArr[i2];
            if (obj4 == null) {
                objArr[i2] = obj;
                objArr2[i2] = obj2;
                int i8 = this.size;
                this.size = i8 + 1;
                if (i8 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i3 = hash3((long) identityHashCode);
            obj5 = objArr[i3];
            if (obj5 == null) {
                objArr[i3] = obj;
                objArr2[i3] = obj2;
                int i9 = this.size;
                this.size = i9 + 1;
                if (i9 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i5++;
        } while (i5 != i6);
        putStash(obj, obj2);
    }

    private void putResize(Object obj, Object obj2) {
        int identityHashCode = System.identityHashCode(obj);
        int i = identityHashCode & this.mask;
        Object obj3 = this.keyTable[i];
        if (obj3 == null) {
            this.keyTable[i] = obj;
            this.valueTable[i] = obj2;
            int i2 = this.size;
            this.size = i2 + 1;
            if (i2 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int hash2 = hash2((long) identityHashCode);
        Object obj4 = this.keyTable[hash2];
        if (obj4 == null) {
            this.keyTable[hash2] = obj;
            this.valueTable[hash2] = obj2;
            int i3 = this.size;
            this.size = i3 + 1;
            if (i3 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int hash3 = hash3((long) identityHashCode);
        Object obj5 = this.keyTable[hash3];
        if (obj5 == null) {
            this.keyTable[hash3] = obj;
            this.valueTable[hash3] = obj2;
            int i4 = this.size;
            this.size = i4 + 1;
            if (i4 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        push(obj, obj2, i, obj3, hash2, obj4, hash3, obj5);
    }

    private void putStash(Object obj, Object obj2) {
        if (this.stashSize == this.stashCapacity) {
            resize(this.capacity << 1);
            put(obj, obj2);
            return;
        }
        int i = this.capacity + this.stashSize;
        this.keyTable[i] = obj;
        this.valueTable[i] = obj2;
        this.stashSize++;
        this.size++;
    }

    private void resize(int i) {
        int i2 = this.stashSize + this.capacity;
        this.capacity = i;
        this.threshold = (int) (((float) i) * this.loadFactor);
        this.mask = i - 1;
        this.hashShift = 31 - Integer.numberOfTrailingZeros(i);
        this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) i))) * 2);
        this.pushIterations = Math.max(Math.min(i, 8), ((int) Math.sqrt((double) i)) / 8);
        Object[] objArr = this.keyTable;
        Object[] objArr2 = this.valueTable;
        this.keyTable = new Object[(this.stashCapacity + i)];
        this.valueTable = new Object[(this.stashCapacity + i)];
        this.size = 0;
        this.stashSize = 0;
        for (int i3 = 0; i3 < i2; i3++) {
            Object obj = objArr[i3];
            if (obj != null) {
                putResize(obj, objArr2[i3]);
            }
        }
    }

    public void clear() {
        Object[] objArr = this.keyTable;
        Object[] objArr2 = this.valueTable;
        int i = this.capacity + this.stashSize;
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                objArr[i2] = null;
                objArr2[i2] = null;
                i = i2;
            } else {
                this.size = 0;
                this.stashSize = 0;
                return;
            }
        }
    }

    public boolean containsKey(Object obj) {
        int identityHashCode = System.identityHashCode(obj);
        if (obj != this.keyTable[this.mask & identityHashCode]) {
            if (obj != this.keyTable[hash2((long) identityHashCode)]) {
                if (obj != this.keyTable[hash3((long) identityHashCode)]) {
                    return containsKeyStash(obj);
                }
            }
        }
        return true;
    }

    public boolean containsValue(Object obj, boolean z) {
        Object[] objArr = this.valueTable;
        if (obj != null) {
            if (!z) {
                int i = this.capacity + this.stashSize;
                while (true) {
                    int i2 = i - 1;
                    if (i <= 0) {
                        break;
                    } else if (obj.equals(objArr[i2])) {
                        return true;
                    } else {
                        i = i2;
                    }
                }
            } else {
                int i3 = this.capacity + this.stashSize;
                while (true) {
                    int i4 = i3 - 1;
                    if (i3 <= 0) {
                        break;
                    } else if (objArr[i4] == obj) {
                        return true;
                    } else {
                        i3 = i4;
                    }
                }
            }
        } else {
            Object[] objArr2 = this.keyTable;
            int i5 = this.capacity + this.stashSize;
            while (true) {
                int i6 = i5 - 1;
                if (i5 <= 0) {
                    break;
                } else if (objArr2[i6] != null && objArr[i6] == null) {
                    return true;
                } else {
                    i5 = i6;
                }
            }
        }
        return false;
    }

    public void ensureCapacity(int i) {
        int i2 = this.size + i;
        if (i2 >= this.threshold) {
            resize(ObjectMap.nextPowerOfTwo((int) (((float) i2) / this.loadFactor)));
        }
    }

    public Entries entries() {
        if (this.entries == null) {
            this.entries = new Entries(this);
        } else {
            this.entries.reset();
        }
        return this.entries;
    }

    public Object findKey(Object obj, boolean z) {
        Object[] objArr = this.valueTable;
        if (obj != null) {
            if (!z) {
                int i = this.capacity + this.stashSize;
                while (true) {
                    int i2 = i - 1;
                    if (i <= 0) {
                        break;
                    } else if (obj.equals(objArr[i2])) {
                        return this.keyTable[i2];
                    } else {
                        i = i2;
                    }
                }
            } else {
                int i3 = this.capacity + this.stashSize;
                while (true) {
                    int i4 = i3 - 1;
                    if (i3 <= 0) {
                        break;
                    } else if (objArr[i4] == obj) {
                        return this.keyTable[i4];
                    } else {
                        i3 = i4;
                    }
                }
            }
        } else {
            Object[] objArr2 = this.keyTable;
            int i5 = this.capacity + this.stashSize;
            while (true) {
                int i6 = i5 - 1;
                if (i5 <= 0) {
                    break;
                } else if (objArr2[i6] != null && objArr[i6] == null) {
                    return objArr2[i6];
                } else {
                    i5 = i6;
                }
            }
        }
        return null;
    }

    public Object get(Object obj) {
        int identityHashCode = System.identityHashCode(obj);
        int i = this.mask & identityHashCode;
        if (obj != this.keyTable[i]) {
            i = hash2((long) identityHashCode);
            if (obj != this.keyTable[i]) {
                i = hash3((long) identityHashCode);
                if (obj != this.keyTable[i]) {
                    return getStash(obj, null);
                }
            }
        }
        return this.valueTable[i];
    }

    public Object get(Object obj, Object obj2) {
        int identityHashCode = System.identityHashCode(obj);
        int i = this.mask & identityHashCode;
        if (obj != this.keyTable[i]) {
            i = hash2((long) identityHashCode);
            if (obj != this.keyTable[i]) {
                i = hash3((long) identityHashCode);
                if (obj != this.keyTable[i]) {
                    return getStash(obj, obj2);
                }
            }
        }
        return this.valueTable[i];
    }

    public Keys keys() {
        if (this.keys == null) {
            this.keys = new Keys(this);
        } else {
            this.keys.reset();
        }
        return this.keys;
    }

    public Object put(Object obj, Object obj2) {
        if (obj == null) {
            throw new IllegalArgumentException("key cannot be null.");
        }
        Object[] objArr = this.keyTable;
        int identityHashCode = System.identityHashCode(obj);
        int i = identityHashCode & this.mask;
        Object obj3 = objArr[i];
        if (obj3 == obj) {
            Object obj4 = this.valueTable[i];
            this.valueTable[i] = obj2;
            return obj4;
        }
        int hash2 = hash2((long) identityHashCode);
        Object obj5 = objArr[hash2];
        if (obj5 == obj) {
            Object obj6 = this.valueTable[hash2];
            this.valueTable[hash2] = obj2;
            return obj6;
        }
        int hash3 = hash3((long) identityHashCode);
        Object obj7 = objArr[hash3];
        if (obj7 == obj) {
            Object obj8 = this.valueTable[hash3];
            this.valueTable[hash3] = obj2;
            return obj8;
        }
        int i2 = this.capacity;
        int i3 = i2 + this.stashSize;
        for (int i4 = i2; i4 < i3; i4++) {
            if (objArr[i4] == obj) {
                Object obj9 = this.valueTable[i4];
                this.valueTable[i4] = obj2;
                return obj9;
            }
        }
        if (obj3 == null) {
            objArr[i] = obj;
            this.valueTable[i] = obj2;
            int i5 = this.size;
            this.size = i5 + 1;
            if (i5 >= this.threshold) {
                resize(this.capacity << 1);
            }
            return null;
        } else if (obj5 == null) {
            objArr[hash2] = obj;
            this.valueTable[hash2] = obj2;
            int i6 = this.size;
            this.size = i6 + 1;
            if (i6 >= this.threshold) {
                resize(this.capacity << 1);
            }
            return null;
        } else if (obj7 == null) {
            objArr[hash3] = obj;
            this.valueTable[hash3] = obj2;
            int i7 = this.size;
            this.size = i7 + 1;
            if (i7 >= this.threshold) {
                resize(this.capacity << 1);
            }
            return null;
        } else {
            push(obj, obj2, i, obj3, hash2, obj5, hash3, obj7);
            return null;
        }
    }

    public Object remove(Object obj) {
        int identityHashCode = System.identityHashCode(obj);
        int i = this.mask & identityHashCode;
        if (this.keyTable[i] == obj) {
            this.keyTable[i] = null;
            Object obj2 = this.valueTable[i];
            this.valueTable[i] = null;
            this.size--;
            return obj2;
        }
        int hash2 = hash2((long) identityHashCode);
        if (this.keyTable[hash2] == obj) {
            this.keyTable[hash2] = null;
            Object obj3 = this.valueTable[hash2];
            this.valueTable[hash2] = null;
            this.size--;
            return obj3;
        }
        int hash3 = hash3((long) identityHashCode);
        if (this.keyTable[hash3] != obj) {
            return removeStash(obj);
        }
        this.keyTable[hash3] = null;
        Object obj4 = this.valueTable[hash3];
        this.valueTable[hash3] = null;
        this.size--;
        return obj4;
    }

    /* access modifiers changed from: package-private */
    public Object removeStash(Object obj) {
        Object[] objArr = this.keyTable;
        int i = this.capacity;
        int i2 = i + this.stashSize;
        for (int i3 = i; i3 < i2; i3++) {
            if (objArr[i3] == obj) {
                Object obj2 = this.valueTable[i3];
                removeStashIndex(i3);
                this.size--;
                return obj2;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void removeStashIndex(int i) {
        this.stashSize--;
        int i2 = this.capacity + this.stashSize;
        if (i < i2) {
            this.keyTable[i] = this.keyTable[i2];
            this.valueTable[i] = this.valueTable[i2];
            this.valueTable[i2] = null;
            return;
        }
        this.valueTable[i] = null;
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append('[');
        Object[] objArr = this.keyTable;
        Object[] objArr2 = this.valueTable;
        int length = objArr.length;
        while (true) {
            int i = length;
            length = i - 1;
            if (i > 0) {
                Object obj = objArr[length];
                if (obj != null) {
                    sb.append(obj);
                    sb.append('=');
                    sb.append(objArr2[length]);
                    break;
                }
            } else {
                break;
            }
        }
        while (true) {
            int i2 = length - 1;
            if (length > 0) {
                Object obj2 = objArr[i2];
                if (obj2 == null) {
                    length = i2;
                } else {
                    sb.append(", ");
                    sb.append(obj2);
                    sb.append('=');
                    sb.append(objArr2[i2]);
                    length = i2;
                }
            } else {
                sb.append(']');
                return sb.toString();
            }
        }
    }

    public Values values() {
        if (this.values == null) {
            this.values = new Values(this);
        } else {
            this.values.reset();
        }
        return this.values;
    }
}
