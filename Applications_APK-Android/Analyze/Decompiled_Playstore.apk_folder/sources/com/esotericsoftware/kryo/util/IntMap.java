package com.esotericsoftware.kryo.util;

public class IntMap {
    private static final int EMPTY = 0;
    private static final int PRIME1 = -1105259343;
    private static final int PRIME2 = -1262997959;
    private static final int PRIME3 = -825114047;
    int capacity;
    boolean hasZeroValue;
    private int hashShift;
    int[] keyTable;
    private float loadFactor;
    private int mask;
    private int pushIterations;
    public int size;
    private int stashCapacity;
    int stashSize;
    private int threshold;
    Object[] valueTable;
    Object zeroValue;

    public IntMap() {
        this(32, 0.8f);
    }

    public IntMap(int i) {
        this(i, 0.8f);
    }

    public IntMap(int i, float f) {
        if (i < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + i);
        } else if (this.capacity > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + i);
        } else {
            this.capacity = ObjectMap.nextPowerOfTwo(i);
            if (f <= 0.0f) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + f);
            }
            this.loadFactor = f;
            this.threshold = (int) (((float) this.capacity) * f);
            this.mask = this.capacity - 1;
            this.hashShift = 31 - Integer.numberOfTrailingZeros(this.capacity);
            this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) this.capacity))) * 2);
            this.pushIterations = Math.max(Math.min(this.capacity, 8), ((int) Math.sqrt((double) this.capacity)) / 8);
            this.keyTable = new int[(this.capacity + this.stashCapacity)];
            this.valueTable = new Object[this.keyTable.length];
        }
    }

    private boolean containsKeyStash(int i) {
        int[] iArr = this.keyTable;
        int i2 = this.capacity;
        int i3 = this.stashSize + i2;
        while (i2 < i3) {
            if (iArr[i2] == i) {
                return true;
            }
            i2++;
        }
        return false;
    }

    private Object getStash(int i, Object obj) {
        int[] iArr = this.keyTable;
        int i2 = this.capacity;
        int i3 = this.stashSize + i2;
        while (i2 < i3) {
            if (iArr[i2] == i) {
                return this.valueTable[i2];
            }
            i2++;
        }
        return obj;
    }

    private int hash2(int i) {
        int i2 = PRIME2 * i;
        return (i2 ^ (i2 >>> this.hashShift)) & this.mask;
    }

    private int hash3(int i) {
        int i2 = PRIME3 * i;
        return (i2 ^ (i2 >>> this.hashShift)) & this.mask;
    }

    private void push(int i, Object obj, int i2, int i3, int i4, int i5, int i6, int i7) {
        int[] iArr = this.keyTable;
        Object[] objArr = this.valueTable;
        int i8 = this.mask;
        int i9 = EMPTY;
        int i10 = this.pushIterations;
        do {
            switch (ObjectMap.random.nextInt(3)) {
                case EMPTY /*0*/:
                    Object obj2 = objArr[i2];
                    iArr[i2] = i;
                    objArr[i2] = obj;
                    obj = obj2;
                    i = i3;
                    break;
                case 1:
                    Object obj3 = objArr[i4];
                    iArr[i4] = i;
                    objArr[i4] = obj;
                    obj = obj3;
                    i = i5;
                    break;
                default:
                    Object obj4 = objArr[i6];
                    iArr[i6] = i;
                    objArr[i6] = obj;
                    obj = obj4;
                    i = i7;
                    break;
            }
            i2 = i & i8;
            i3 = iArr[i2];
            if (i3 == 0) {
                iArr[i2] = i;
                objArr[i2] = obj;
                int i11 = this.size;
                this.size = i11 + 1;
                if (i11 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i4 = hash2(i);
            i5 = iArr[i4];
            if (i5 == 0) {
                iArr[i4] = i;
                objArr[i4] = obj;
                int i12 = this.size;
                this.size = i12 + 1;
                if (i12 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i6 = hash3(i);
            i7 = iArr[i6];
            if (i7 == 0) {
                iArr[i6] = i;
                objArr[i6] = obj;
                int i13 = this.size;
                this.size = i13 + 1;
                if (i13 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i9++;
        } while (i9 != i10);
        putStash(i, obj);
    }

    private void putResize(int i, Object obj) {
        if (i == 0) {
            this.zeroValue = obj;
            this.hasZeroValue = true;
            return;
        }
        int i2 = i & this.mask;
        int i3 = this.keyTable[i2];
        if (i3 == 0) {
            this.keyTable[i2] = i;
            this.valueTable[i2] = obj;
            int i4 = this.size;
            this.size = i4 + 1;
            if (i4 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int hash2 = hash2(i);
        int i5 = this.keyTable[hash2];
        if (i5 == 0) {
            this.keyTable[hash2] = i;
            this.valueTable[hash2] = obj;
            int i6 = this.size;
            this.size = i6 + 1;
            if (i6 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int hash3 = hash3(i);
        int i7 = this.keyTable[hash3];
        if (i7 == 0) {
            this.keyTable[hash3] = i;
            this.valueTable[hash3] = obj;
            int i8 = this.size;
            this.size = i8 + 1;
            if (i8 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        push(i, obj, i2, i3, hash2, i5, hash3, i7);
    }

    private void putStash(int i, Object obj) {
        if (this.stashSize == this.stashCapacity) {
            resize(this.capacity << 1);
            put(i, obj);
            return;
        }
        int i2 = this.capacity + this.stashSize;
        this.keyTable[i2] = i;
        this.valueTable[i2] = obj;
        this.stashSize++;
        this.size++;
    }

    private void resize(int i) {
        int i2 = this.stashSize + this.capacity;
        this.capacity = i;
        this.threshold = (int) (((float) i) * this.loadFactor);
        this.mask = i - 1;
        this.hashShift = 31 - Integer.numberOfTrailingZeros(i);
        this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) i))) * 2);
        this.pushIterations = Math.max(Math.min(i, 8), ((int) Math.sqrt((double) i)) / 8);
        int[] iArr = this.keyTable;
        Object[] objArr = this.valueTable;
        this.keyTable = new int[(this.stashCapacity + i)];
        this.valueTable = new Object[(this.stashCapacity + i)];
        this.size = this.hasZeroValue ? 1 : EMPTY;
        this.stashSize = EMPTY;
        for (int i3 = EMPTY; i3 < i2; i3++) {
            int i4 = iArr[i3];
            if (i4 != 0) {
                putResize(i4, objArr[i3]);
            }
        }
    }

    public void clear() {
        int[] iArr = this.keyTable;
        Object[] objArr = this.valueTable;
        int i = this.capacity + this.stashSize;
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                iArr[i2] = EMPTY;
                objArr[i2] = null;
                i = i2;
            } else {
                this.size = EMPTY;
                this.stashSize = EMPTY;
                this.zeroValue = null;
                this.hasZeroValue = false;
                return;
            }
        }
    }

    public boolean containsKey(int i) {
        if (i == 0) {
            return this.hasZeroValue;
        }
        if (this.keyTable[this.mask & i] != i) {
            if (this.keyTable[hash2(i)] != i) {
                if (this.keyTable[hash3(i)] != i) {
                    return containsKeyStash(i);
                }
            }
        }
        return true;
    }

    public boolean containsValue(Object obj, boolean z) {
        Object[] objArr = this.valueTable;
        if (obj == null) {
            if (!this.hasZeroValue || this.zeroValue != null) {
                int[] iArr = this.keyTable;
                int i = this.capacity + this.stashSize;
                while (true) {
                    int i2 = i - 1;
                    if (i <= 0) {
                        break;
                    } else if (iArr[i2] != 0 && objArr[i2] == null) {
                        return true;
                    } else {
                        i = i2;
                    }
                }
            } else {
                return true;
            }
        } else if (z) {
            if (obj != this.zeroValue) {
                int i3 = this.capacity + this.stashSize;
                while (true) {
                    int i4 = i3 - 1;
                    if (i3 <= 0) {
                        break;
                    } else if (objArr[i4] == obj) {
                        return true;
                    } else {
                        i3 = i4;
                    }
                }
            } else {
                return true;
            }
        } else if (!this.hasZeroValue || !obj.equals(this.zeroValue)) {
            int i5 = this.capacity + this.stashSize;
            while (true) {
                int i6 = i5 - 1;
                if (i5 <= 0) {
                    break;
                } else if (obj.equals(objArr[i6])) {
                    return true;
                } else {
                    i5 = i6;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public void ensureCapacity(int i) {
        int i2 = this.size + i;
        if (i2 >= this.threshold) {
            resize(ObjectMap.nextPowerOfTwo((int) (((float) i2) / this.loadFactor)));
        }
    }

    public int findKey(Object obj, boolean z, int i) {
        Object[] objArr = this.valueTable;
        if (obj == null) {
            if (this.hasZeroValue && this.zeroValue == null) {
                return EMPTY;
            }
            int[] iArr = this.keyTable;
            int i2 = this.capacity + this.stashSize;
            while (true) {
                int i3 = i2 - 1;
                if (i2 <= 0) {
                    return i;
                }
                if (iArr[i3] != 0 && objArr[i3] == null) {
                    return iArr[i3];
                }
                i2 = i3;
            }
        } else if (z) {
            if (obj == this.zeroValue) {
                return EMPTY;
            }
            int i4 = this.capacity + this.stashSize;
            while (true) {
                int i5 = i4 - 1;
                if (i4 <= 0) {
                    return i;
                }
                if (objArr[i5] == obj) {
                    return this.keyTable[i5];
                }
                i4 = i5;
            }
        } else if (this.hasZeroValue && obj.equals(this.zeroValue)) {
            return EMPTY;
        } else {
            int i6 = this.capacity + this.stashSize;
            while (true) {
                int i7 = i6 - 1;
                if (i6 <= 0) {
                    return i;
                }
                if (obj.equals(objArr[i7])) {
                    return this.keyTable[i7];
                }
                i6 = i7;
            }
        }
    }

    public Object get(int i) {
        if (i == 0) {
            return this.zeroValue;
        }
        int i2 = this.mask & i;
        if (this.keyTable[i2] != i) {
            i2 = hash2(i);
            if (this.keyTable[i2] != i) {
                i2 = hash3(i);
                if (this.keyTable[i2] != i) {
                    return getStash(i, null);
                }
            }
        }
        return this.valueTable[i2];
    }

    public Object get(int i, Object obj) {
        if (i == 0) {
            return this.zeroValue;
        }
        int i2 = this.mask & i;
        if (this.keyTable[i2] != i) {
            i2 = hash2(i);
            if (this.keyTable[i2] != i) {
                i2 = hash3(i);
                if (this.keyTable[i2] != i) {
                    return getStash(i, obj);
                }
            }
        }
        return this.valueTable[i2];
    }

    public Object put(int i, Object obj) {
        if (i == 0) {
            Object obj2 = this.zeroValue;
            this.zeroValue = obj;
            this.hasZeroValue = true;
            this.size++;
            return obj2;
        }
        int[] iArr = this.keyTable;
        int i2 = i & this.mask;
        int i3 = iArr[i2];
        if (i3 == i) {
            Object obj3 = this.valueTable[i2];
            this.valueTable[i2] = obj;
            return obj3;
        }
        int hash2 = hash2(i);
        int i4 = iArr[hash2];
        if (i4 == i) {
            Object obj4 = this.valueTable[hash2];
            this.valueTable[hash2] = obj;
            return obj4;
        }
        int hash3 = hash3(i);
        int i5 = iArr[hash3];
        if (i5 == i) {
            Object obj5 = this.valueTable[hash3];
            this.valueTable[hash3] = obj;
            return obj5;
        }
        int i6 = this.capacity;
        int i7 = i6 + this.stashSize;
        for (int i8 = i6; i8 < i7; i8++) {
            if (i == iArr[i8]) {
                Object obj6 = this.valueTable[i8];
                this.valueTable[i8] = obj;
                return obj6;
            }
        }
        if (i3 == 0) {
            iArr[i2] = i;
            this.valueTable[i2] = obj;
            int i9 = this.size;
            this.size = i9 + 1;
            if (i9 >= this.threshold) {
                resize(this.capacity << 1);
            }
            return null;
        } else if (i4 == 0) {
            iArr[hash2] = i;
            this.valueTable[hash2] = obj;
            int i10 = this.size;
            this.size = i10 + 1;
            if (i10 >= this.threshold) {
                resize(this.capacity << 1);
            }
            return null;
        } else if (i5 == 0) {
            iArr[hash3] = i;
            this.valueTable[hash3] = obj;
            int i11 = this.size;
            this.size = i11 + 1;
            if (i11 >= this.threshold) {
                resize(this.capacity << 1);
            }
            return null;
        } else {
            push(i, obj, i2, i3, hash2, i4, hash3, i5);
            return null;
        }
    }

    public Object remove(int i) {
        if (i != 0) {
            int i2 = i & this.mask;
            if (this.keyTable[i2] == i) {
                this.keyTable[i2] = EMPTY;
                Object obj = this.valueTable[i2];
                this.valueTable[i2] = null;
                this.size--;
                return obj;
            }
            int hash2 = hash2(i);
            if (this.keyTable[hash2] == i) {
                this.keyTable[hash2] = EMPTY;
                Object obj2 = this.valueTable[hash2];
                this.valueTable[hash2] = null;
                this.size--;
                return obj2;
            }
            int hash3 = hash3(i);
            if (this.keyTable[hash3] != i) {
                return removeStash(i);
            }
            this.keyTable[hash3] = EMPTY;
            Object obj3 = this.valueTable[hash3];
            this.valueTable[hash3] = null;
            this.size--;
            return obj3;
        } else if (!this.hasZeroValue) {
            return null;
        } else {
            Object obj4 = this.zeroValue;
            this.zeroValue = null;
            this.hasZeroValue = false;
            this.size--;
            return obj4;
        }
    }

    /* access modifiers changed from: package-private */
    public Object removeStash(int i) {
        int[] iArr = this.keyTable;
        int i2 = this.capacity;
        int i3 = i2 + this.stashSize;
        for (int i4 = i2; i4 < i3; i4++) {
            if (iArr[i4] == i) {
                Object obj = this.valueTable[i4];
                removeStashIndex(i4);
                this.size--;
                return obj;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void removeStashIndex(int i) {
        this.stashSize--;
        int i2 = this.capacity + this.stashSize;
        if (i < i2) {
            this.keyTable[i] = this.keyTable[i2];
            this.valueTable[i] = this.valueTable[i2];
            this.valueTable[i2] = null;
            return;
        }
        this.valueTable[i] = null;
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append('[');
        int[] iArr = this.keyTable;
        Object[] objArr = this.valueTable;
        int length = iArr.length;
        while (true) {
            int i = length;
            length = i - 1;
            if (i > 0) {
                int i2 = iArr[length];
                if (i2 != 0) {
                    sb.append(i2);
                    sb.append('=');
                    sb.append(objArr[length]);
                    break;
                }
            } else {
                break;
            }
        }
        while (true) {
            int i3 = length - 1;
            if (length > 0) {
                int i4 = iArr[i3];
                if (i4 == 0) {
                    length = i3;
                } else {
                    sb.append(", ");
                    sb.append(i4);
                    sb.append('=');
                    sb.append(objArr[i3]);
                    length = i3;
                }
            } else {
                sb.append(']');
                return sb.toString();
            }
        }
    }
}
