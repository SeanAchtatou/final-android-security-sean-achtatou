package com.esotericsoftware.kryo.util;

public class IdentityObjectIntMap {
    private static final int PRIME1 = -1105259343;
    private static final int PRIME2 = -1262997959;
    private static final int PRIME3 = -825114047;
    int capacity;
    private int hashShift;
    Object[] keyTable;
    private float loadFactor;
    private int mask;
    private int pushIterations;
    public int size;
    private int stashCapacity;
    int stashSize;
    private int threshold;
    int[] valueTable;

    public IdentityObjectIntMap() {
        this(32, 0.8f);
    }

    public IdentityObjectIntMap(int i) {
        this(i, 0.8f);
    }

    public IdentityObjectIntMap(int i, float f) {
        if (i < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + i);
        } else if (this.capacity > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + i);
        } else {
            this.capacity = ObjectMap.nextPowerOfTwo(i);
            if (f <= 0.0f) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + f);
            }
            this.loadFactor = f;
            this.threshold = (int) (((float) this.capacity) * f);
            this.mask = this.capacity - 1;
            this.hashShift = 31 - Integer.numberOfTrailingZeros(this.capacity);
            this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) this.capacity))) * 2);
            this.pushIterations = Math.max(Math.min(this.capacity, 8), ((int) Math.sqrt((double) this.capacity)) / 8);
            this.keyTable = new Object[(this.capacity + this.stashCapacity)];
            this.valueTable = new int[this.keyTable.length];
        }
    }

    private boolean containsKeyStash(Object obj) {
        Object[] objArr = this.keyTable;
        int i = this.capacity;
        int i2 = this.stashSize + i;
        while (i < i2) {
            if (obj == objArr[i]) {
                return true;
            }
            i++;
        }
        return false;
    }

    private int getAndIncrementStash(Object obj, int i, int i2) {
        Object[] objArr = this.keyTable;
        int i3 = this.capacity;
        int i4 = this.stashSize + i3;
        while (i3 < i4) {
            if (obj == objArr[i3]) {
                int i5 = this.valueTable[i3];
                this.valueTable[i3] = i5 + i2;
                return i5;
            }
            i3++;
        }
        put(obj, i + i2);
        return i;
    }

    private int getStash(Object obj, int i) {
        Object[] objArr = this.keyTable;
        int i2 = this.capacity;
        int i3 = this.stashSize + i2;
        while (i2 < i3) {
            if (obj == objArr[i2]) {
                return this.valueTable[i2];
            }
            i2++;
        }
        return i;
    }

    private int hash2(int i) {
        int i2 = PRIME2 * i;
        return (i2 ^ (i2 >>> this.hashShift)) & this.mask;
    }

    private int hash3(int i) {
        int i2 = PRIME3 * i;
        return (i2 ^ (i2 >>> this.hashShift)) & this.mask;
    }

    private void push(Object obj, int i, int i2, Object obj2, int i3, Object obj3, int i4, Object obj4) {
        Object[] objArr = this.keyTable;
        int[] iArr = this.valueTable;
        int i5 = this.mask;
        int i6 = 0;
        int i7 = this.pushIterations;
        do {
            switch (ObjectMap.random.nextInt(3)) {
                case 0:
                    int i8 = iArr[i2];
                    objArr[i2] = obj;
                    iArr[i2] = i;
                    i = i8;
                    obj = obj2;
                    break;
                case 1:
                    int i9 = iArr[i3];
                    objArr[i3] = obj;
                    iArr[i3] = i;
                    i = i9;
                    obj = obj3;
                    break;
                default:
                    int i10 = iArr[i4];
                    objArr[i4] = obj;
                    iArr[i4] = i;
                    i = i10;
                    obj = obj4;
                    break;
            }
            int identityHashCode = System.identityHashCode(obj);
            i2 = identityHashCode & i5;
            obj2 = objArr[i2];
            if (obj2 == null) {
                objArr[i2] = obj;
                iArr[i2] = i;
                int i11 = this.size;
                this.size = i11 + 1;
                if (i11 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i3 = hash2(identityHashCode);
            obj3 = objArr[i3];
            if (obj3 == null) {
                objArr[i3] = obj;
                iArr[i3] = i;
                int i12 = this.size;
                this.size = i12 + 1;
                if (i12 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i4 = hash3(identityHashCode);
            obj4 = objArr[i4];
            if (obj4 == null) {
                objArr[i4] = obj;
                iArr[i4] = i;
                int i13 = this.size;
                this.size = i13 + 1;
                if (i13 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i6++;
        } while (i6 != i7);
        putStash(obj, i);
    }

    private void putResize(Object obj, int i) {
        int identityHashCode = System.identityHashCode(obj);
        int i2 = identityHashCode & this.mask;
        Object obj2 = this.keyTable[i2];
        if (obj2 == null) {
            this.keyTable[i2] = obj;
            this.valueTable[i2] = i;
            int i3 = this.size;
            this.size = i3 + 1;
            if (i3 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int hash2 = hash2(identityHashCode);
        Object obj3 = this.keyTable[hash2];
        if (obj3 == null) {
            this.keyTable[hash2] = obj;
            this.valueTable[hash2] = i;
            int i4 = this.size;
            this.size = i4 + 1;
            if (i4 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int hash3 = hash3(identityHashCode);
        Object obj4 = this.keyTable[hash3];
        if (obj4 == null) {
            this.keyTable[hash3] = obj;
            this.valueTable[hash3] = i;
            int i5 = this.size;
            this.size = i5 + 1;
            if (i5 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        push(obj, i, i2, obj2, hash2, obj3, hash3, obj4);
    }

    private void putStash(Object obj, int i) {
        if (this.stashSize == this.stashCapacity) {
            resize(this.capacity << 1);
            put(obj, i);
            return;
        }
        int i2 = this.capacity + this.stashSize;
        this.keyTable[i2] = obj;
        this.valueTable[i2] = i;
        this.stashSize++;
        this.size++;
    }

    private void resize(int i) {
        int i2 = this.stashSize + this.capacity;
        this.capacity = i;
        this.threshold = (int) (((float) i) * this.loadFactor);
        this.mask = i - 1;
        this.hashShift = 31 - Integer.numberOfTrailingZeros(i);
        this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) i))) * 2);
        this.pushIterations = Math.max(Math.min(i, 8), ((int) Math.sqrt((double) i)) / 8);
        Object[] objArr = this.keyTable;
        int[] iArr = this.valueTable;
        this.keyTable = new Object[(this.stashCapacity + i)];
        this.valueTable = new int[(this.stashCapacity + i)];
        this.size = 0;
        this.stashSize = 0;
        for (int i3 = 0; i3 < i2; i3++) {
            Object obj = objArr[i3];
            if (obj != null) {
                putResize(obj, iArr[i3]);
            }
        }
    }

    public void clear() {
        Object[] objArr = this.keyTable;
        int[] iArr = this.valueTable;
        int i = this.capacity + this.stashSize;
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                objArr[i2] = null;
                i = i2;
            } else {
                this.size = 0;
                this.stashSize = 0;
                return;
            }
        }
    }

    public boolean containsKey(Object obj) {
        int identityHashCode = System.identityHashCode(obj);
        if (obj != this.keyTable[this.mask & identityHashCode]) {
            if (obj != this.keyTable[hash2(identityHashCode)]) {
                if (obj != this.keyTable[hash3(identityHashCode)]) {
                    return containsKeyStash(obj);
                }
            }
        }
        return true;
    }

    public boolean containsValue(int i) {
        int[] iArr = this.valueTable;
        int i2 = this.capacity + this.stashSize;
        while (true) {
            int i3 = i2 - 1;
            if (i2 <= 0) {
                return false;
            }
            if (iArr[i3] == i) {
                return true;
            }
            i2 = i3;
        }
    }

    public void ensureCapacity(int i) {
        int i2 = this.size + i;
        if (i2 >= this.threshold) {
            resize(ObjectMap.nextPowerOfTwo((int) (((float) i2) / this.loadFactor)));
        }
    }

    public Object findKey(int i) {
        int[] iArr = this.valueTable;
        int i2 = this.capacity + this.stashSize;
        while (true) {
            int i3 = i2 - 1;
            if (i2 <= 0) {
                return null;
            }
            if (iArr[i3] == i) {
                return this.keyTable[i3];
            }
            i2 = i3;
        }
    }

    public int get(Object obj, int i) {
        int identityHashCode = System.identityHashCode(obj);
        int i2 = this.mask & identityHashCode;
        if (obj != this.keyTable[i2]) {
            i2 = hash2(identityHashCode);
            if (obj != this.keyTable[i2]) {
                i2 = hash3(identityHashCode);
                if (obj != this.keyTable[i2]) {
                    return getStash(obj, i);
                }
            }
        }
        return this.valueTable[i2];
    }

    public int getAndIncrement(Object obj, int i, int i2) {
        int identityHashCode = System.identityHashCode(obj);
        int i3 = this.mask & identityHashCode;
        if (obj != this.keyTable[i3]) {
            i3 = hash2(identityHashCode);
            if (obj != this.keyTable[i3]) {
                i3 = hash3(identityHashCode);
                if (obj != this.keyTable[i3]) {
                    return getAndIncrementStash(obj, i, i2);
                }
            }
        }
        int i4 = this.valueTable[i3];
        this.valueTable[i3] = i4 + i2;
        return i4;
    }

    public void put(Object obj, int i) {
        if (obj == null) {
            throw new IllegalArgumentException("key cannot be null.");
        }
        Object[] objArr = this.keyTable;
        int identityHashCode = System.identityHashCode(obj);
        int i2 = identityHashCode & this.mask;
        Object obj2 = objArr[i2];
        if (obj == obj2) {
            this.valueTable[i2] = i;
            return;
        }
        int hash2 = hash2(identityHashCode);
        Object obj3 = objArr[hash2];
        if (obj == obj3) {
            this.valueTable[hash2] = i;
            return;
        }
        int hash3 = hash3(identityHashCode);
        Object obj4 = objArr[hash3];
        if (obj == obj4) {
            this.valueTable[hash3] = i;
            return;
        }
        int i3 = this.capacity;
        int i4 = this.stashSize + i3;
        while (i3 < i4) {
            if (objArr[i3] == obj) {
                this.valueTable[i3] = i;
                return;
            }
            i3++;
        }
        if (obj2 == null) {
            objArr[i2] = obj;
            this.valueTable[i2] = i;
            int i5 = this.size;
            this.size = i5 + 1;
            if (i5 >= this.threshold) {
                resize(this.capacity << 1);
            }
        } else if (obj3 == null) {
            objArr[hash2] = obj;
            this.valueTable[hash2] = i;
            int i6 = this.size;
            this.size = i6 + 1;
            if (i6 >= this.threshold) {
                resize(this.capacity << 1);
            }
        } else if (obj4 == null) {
            objArr[hash3] = obj;
            this.valueTable[hash3] = i;
            int i7 = this.size;
            this.size = i7 + 1;
            if (i7 >= this.threshold) {
                resize(this.capacity << 1);
            }
        } else {
            push(obj, i, i2, obj2, hash2, obj3, hash3, obj4);
        }
    }

    public int remove(Object obj, int i) {
        int identityHashCode = System.identityHashCode(obj);
        int i2 = this.mask & identityHashCode;
        if (obj == this.keyTable[i2]) {
            this.keyTable[i2] = null;
            this.size--;
            return this.valueTable[i2];
        }
        int hash2 = hash2(identityHashCode);
        if (obj == this.keyTable[hash2]) {
            this.keyTable[hash2] = null;
            this.size--;
            return this.valueTable[hash2];
        }
        int hash3 = hash3(identityHashCode);
        if (obj != this.keyTable[hash3]) {
            return removeStash(obj, i);
        }
        this.keyTable[hash3] = null;
        this.size--;
        return this.valueTable[hash3];
    }

    /* access modifiers changed from: package-private */
    public int removeStash(Object obj, int i) {
        Object[] objArr = this.keyTable;
        int i2 = this.capacity;
        int i3 = this.stashSize + i2;
        while (i2 < i3) {
            if (obj == objArr[i2]) {
                int i4 = this.valueTable[i2];
                removeStashIndex(i2);
                this.size--;
                return i4;
            }
            i2++;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public void removeStashIndex(int i) {
        this.stashSize--;
        int i2 = this.capacity + this.stashSize;
        if (i < i2) {
            this.keyTable[i] = this.keyTable[i2];
            this.valueTable[i] = this.valueTable[i2];
        }
    }

    public String toString() {
        if (this.size == 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append('{');
        Object[] objArr = this.keyTable;
        int[] iArr = this.valueTable;
        int length = objArr.length;
        while (true) {
            int i = length;
            length = i - 1;
            if (i > 0) {
                Object obj = objArr[length];
                if (obj != null) {
                    sb.append(obj);
                    sb.append('=');
                    sb.append(iArr[length]);
                    break;
                }
            } else {
                break;
            }
        }
        while (true) {
            int i2 = length - 1;
            if (length > 0) {
                Object obj2 = objArr[i2];
                if (obj2 == null) {
                    length = i2;
                } else {
                    sb.append(", ");
                    sb.append(obj2);
                    sb.append('=');
                    sb.append(iArr[i2]);
                    length = i2;
                }
            } else {
                sb.append('}');
                return sb.toString();
            }
        }
    }
}
