package com.esotericsoftware.kryo;

import b.b.a.a;
import com.esotericsoftware.kryo.util.Util;

public class Registration {
    private final int id;
    private a instantiator;
    private Serializer serializer;
    private final Class type;

    public Registration(Class cls, Serializer serializer2, int i) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (serializer2 == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            this.type = cls;
            this.serializer = serializer2;
            this.id = i;
        }
    }

    public int getId() {
        return this.id;
    }

    public a getInstantiator() {
        return this.instantiator;
    }

    public Serializer getSerializer() {
        return this.serializer;
    }

    public Class getType() {
        return this.type;
    }

    public void setInstantiator(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("instantiator cannot be null.");
        }
        this.instantiator = aVar;
    }

    public void setSerializer(Serializer serializer2) {
        if (serializer2 == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        }
        this.serializer = serializer2;
    }

    public String toString() {
        return "[" + this.id + ", " + Util.className(this.type) + "]";
    }
}
