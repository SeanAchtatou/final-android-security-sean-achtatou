package com.esotericsoftware.kryo;

import b.b.b.a;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;
import com.esotericsoftware.kryo.serializers.DefaultArraySerializers;
import com.esotericsoftware.kryo.serializers.DefaultSerializers;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import com.esotericsoftware.kryo.util.DefaultClassResolver;
import com.esotericsoftware.kryo.util.IdentityMap;
import com.esotericsoftware.kryo.util.IntArray;
import com.esotericsoftware.kryo.util.MapReferenceResolver;
import com.esotericsoftware.kryo.util.ObjectMap;
import com.esotericsoftware.kryo.util.Util;
import com.esotericsoftware.reflectasm.ConstructorAccess;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.EnumSet;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

public class Kryo {
    public static final byte NOT_NULL = 1;
    private static final int NO_REF = -2;
    public static final byte NULL = 0;
    private static final int REF = -1;
    private boolean autoReset;
    private ClassLoader classLoader;
    private final ClassResolver classResolver;
    private ObjectMap context;
    private int copyDepth;
    private boolean copyShallow;
    private Class defaultSerializer;
    private final ArrayList defaultSerializers;
    private int depth;
    private ObjectMap graphContext;
    private final int lowPriorityDefaultSerializerCount;
    private int maxDepth;
    private Class memoizedClass;
    private Registration memoizedClassValue;
    private Object needsCopyReference;
    private int nextRegisterID;
    private IdentityMap originalToCopy;
    private Object readObject;
    private final IntArray readReferenceIds;
    private ReferenceResolver referenceResolver;
    private boolean references;
    private boolean registrationRequired;
    private a strategy;
    private volatile Thread thread;

    final class DefaultSerializerEntry {
        Serializer serializer;
        Class serializerClass;
        Class type;

        DefaultSerializerEntry() {
        }
    }

    public Kryo() {
        this(new DefaultClassResolver(), new MapReferenceResolver());
    }

    public Kryo(ClassResolver classResolver2, ReferenceResolver referenceResolver2) {
        this.defaultSerializer = FieldSerializer.class;
        this.defaultSerializers = new ArrayList(32);
        this.classLoader = getClass().getClassLoader();
        this.maxDepth = Integer.MAX_VALUE;
        this.autoReset = true;
        this.readReferenceIds = new IntArray(0);
        if (classResolver2 == null) {
            throw new IllegalArgumentException("classResolver cannot be null.");
        }
        this.classResolver = classResolver2;
        classResolver2.setKryo(this);
        this.referenceResolver = referenceResolver2;
        if (referenceResolver2 != null) {
            referenceResolver2.setKryo(this);
            this.references = true;
        }
        addDefaultSerializer(byte[].class, DefaultArraySerializers.ByteArraySerializer.class);
        addDefaultSerializer(char[].class, DefaultArraySerializers.CharArraySerializer.class);
        addDefaultSerializer(short[].class, DefaultArraySerializers.ShortArraySerializer.class);
        addDefaultSerializer(int[].class, DefaultArraySerializers.IntArraySerializer.class);
        addDefaultSerializer(long[].class, DefaultArraySerializers.LongArraySerializer.class);
        addDefaultSerializer(float[].class, DefaultArraySerializers.FloatArraySerializer.class);
        addDefaultSerializer(double[].class, DefaultArraySerializers.DoubleArraySerializer.class);
        addDefaultSerializer(boolean[].class, DefaultArraySerializers.BooleanArraySerializer.class);
        addDefaultSerializer(String[].class, DefaultArraySerializers.StringArraySerializer.class);
        addDefaultSerializer(Object[].class, DefaultArraySerializers.ObjectArraySerializer.class);
        addDefaultSerializer(BigInteger.class, DefaultSerializers.BigIntegerSerializer.class);
        addDefaultSerializer(BigDecimal.class, DefaultSerializers.BigDecimalSerializer.class);
        addDefaultSerializer(Class.class, DefaultSerializers.ClassSerializer.class);
        addDefaultSerializer(Date.class, DefaultSerializers.DateSerializer.class);
        addDefaultSerializer(Enum.class, DefaultSerializers.EnumSerializer.class);
        addDefaultSerializer(EnumSet.class, DefaultSerializers.EnumSetSerializer.class);
        addDefaultSerializer(Currency.class, DefaultSerializers.CurrencySerializer.class);
        addDefaultSerializer(StringBuffer.class, DefaultSerializers.StringBufferSerializer.class);
        addDefaultSerializer(StringBuilder.class, DefaultSerializers.StringBuilderSerializer.class);
        addDefaultSerializer(Collections.EMPTY_LIST.getClass(), DefaultSerializers.CollectionsEmptyListSerializer.class);
        addDefaultSerializer(Collections.EMPTY_MAP.getClass(), DefaultSerializers.CollectionsEmptyMapSerializer.class);
        addDefaultSerializer(Collections.EMPTY_SET.getClass(), DefaultSerializers.CollectionsEmptySetSerializer.class);
        addDefaultSerializer(Collections.singletonList(null).getClass(), DefaultSerializers.CollectionsSingletonListSerializer.class);
        addDefaultSerializer(Collections.singletonMap(null, null).getClass(), DefaultSerializers.CollectionsSingletonMapSerializer.class);
        addDefaultSerializer(Collections.singleton(null).getClass(), DefaultSerializers.CollectionsSingletonSetSerializer.class);
        addDefaultSerializer(Collection.class, CollectionSerializer.class);
        addDefaultSerializer(TreeMap.class, DefaultSerializers.TreeMapSerializer.class);
        addDefaultSerializer(Map.class, MapSerializer.class);
        addDefaultSerializer(KryoSerializable.class, DefaultSerializers.KryoSerializableSerializer.class);
        addDefaultSerializer(TimeZone.class, DefaultSerializers.TimeZoneSerializer.class);
        addDefaultSerializer(Calendar.class, DefaultSerializers.CalendarSerializer.class);
        this.lowPriorityDefaultSerializerCount = this.defaultSerializers.size();
        register(Integer.TYPE, new DefaultSerializers.IntSerializer());
        register(String.class, new DefaultSerializers.StringSerializer());
        register(Float.TYPE, new DefaultSerializers.FloatSerializer());
        register(Boolean.TYPE, new DefaultSerializers.BooleanSerializer());
        register(Byte.TYPE, new DefaultSerializers.ByteSerializer());
        register(Character.TYPE, new DefaultSerializers.CharSerializer());
        register(Short.TYPE, new DefaultSerializers.ShortSerializer());
        register(Long.TYPE, new DefaultSerializers.LongSerializer());
        register(Double.TYPE, new DefaultSerializers.DoubleSerializer());
    }

    public Kryo(ReferenceResolver referenceResolver2) {
        this(new DefaultClassResolver(), referenceResolver2);
    }

    private void beginObject() {
        if (this.depth == this.maxDepth) {
            throw new KryoException("Max depth exceeded: " + this.depth);
        }
        this.depth++;
    }

    public static Class[] getGenerics(Type type) {
        int i;
        if (!(type instanceof ParameterizedType)) {
            return null;
        }
        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Class[] clsArr = new Class[actualTypeArguments.length];
        int length = actualTypeArguments.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            Type type2 = actualTypeArguments[i2];
            if (type2 instanceof Class) {
                clsArr[i2] = (Class) type2;
            } else if (type2 instanceof ParameterizedType) {
                clsArr[i2] = (Class) ((ParameterizedType) type2).getRawType();
            } else {
                i = i3;
                i2++;
                i3 = i;
            }
            i = i3 + 1;
            i2++;
            i3 = i;
        }
        if (i3 == 0) {
            return null;
        }
        return clsArr;
    }

    public void addDefaultSerializer(Class cls, Serializer serializer) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (serializer == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            DefaultSerializerEntry defaultSerializerEntry = new DefaultSerializerEntry();
            defaultSerializerEntry.type = cls;
            defaultSerializerEntry.serializer = serializer;
            this.defaultSerializers.add(this.defaultSerializers.size() - this.lowPriorityDefaultSerializerCount, defaultSerializerEntry);
        }
    }

    public void addDefaultSerializer(Class cls, Class cls2) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (cls2 == null) {
            throw new IllegalArgumentException("serializerClass cannot be null.");
        } else {
            DefaultSerializerEntry defaultSerializerEntry = new DefaultSerializerEntry();
            defaultSerializerEntry.type = cls;
            defaultSerializerEntry.serializerClass = cls2;
            this.defaultSerializers.add(this.defaultSerializers.size() - this.lowPriorityDefaultSerializerCount, defaultSerializerEntry);
        }
    }

    public Object copy(Object obj) {
        if (obj == null) {
            return null;
        }
        if (this.copyShallow) {
            return obj;
        }
        this.copyDepth++;
        try {
            if (this.originalToCopy == null) {
                this.originalToCopy = new IdentityMap();
            }
            Object obj2 = this.originalToCopy.get(obj);
            if (obj2 != null) {
                return obj2;
            }
            this.needsCopyReference = obj;
            Object copy = obj instanceof KryoCopyable ? ((KryoCopyable) obj).copy(this) : getSerializer(obj.getClass()).copy(this, obj);
            if (this.needsCopyReference != null) {
                reference(copy);
            }
            int i = this.copyDepth + REF;
            this.copyDepth = i;
            if (i != 0) {
                return copy;
            }
            reset();
            return copy;
        } finally {
            int i2 = this.copyDepth + REF;
            this.copyDepth = i2;
            if (i2 == 0) {
                reset();
            }
        }
    }

    public Object copy(Object obj, Serializer serializer) {
        if (obj == null) {
            return null;
        }
        if (this.copyShallow) {
            return obj;
        }
        this.copyDepth++;
        try {
            if (this.originalToCopy == null) {
                this.originalToCopy = new IdentityMap();
            }
            Object obj2 = this.originalToCopy.get(obj);
            if (obj2 != null) {
                return obj2;
            }
            this.needsCopyReference = obj;
            Object copy = obj instanceof KryoCopyable ? ((KryoCopyable) obj).copy(this) : serializer.copy(this, obj);
            if (this.needsCopyReference != null) {
                reference(copy);
            }
            int i = this.copyDepth + REF;
            this.copyDepth = i;
            if (i != 0) {
                return copy;
            }
            reset();
            return copy;
        } finally {
            int i2 = this.copyDepth + REF;
            this.copyDepth = i2;
            if (i2 == 0) {
                reset();
            }
        }
    }

    public Object copyShallow(Object obj) {
        if (obj == null) {
            return null;
        }
        this.copyDepth++;
        this.copyShallow = true;
        try {
            if (this.originalToCopy == null) {
                this.originalToCopy = new IdentityMap();
            }
            Object obj2 = this.originalToCopy.get(obj);
            if (obj2 != null) {
            }
            this.needsCopyReference = obj;
            Object copy = obj instanceof KryoCopyable ? ((KryoCopyable) obj).copy(this) : getSerializer(obj.getClass()).copy(this, obj);
            if (this.needsCopyReference != null) {
                reference(copy);
            }
            this.copyShallow = false;
            int i = this.copyDepth + REF;
            this.copyDepth = i;
            if (i != 0) {
                return copy;
            }
            reset();
            return copy;
        } finally {
            this.copyShallow = false;
            int i2 = this.copyDepth + REF;
            this.copyDepth = i2;
            if (i2 == 0) {
                reset();
            }
        }
    }

    public Object copyShallow(Object obj, Serializer serializer) {
        if (obj == null) {
            return null;
        }
        this.copyDepth++;
        this.copyShallow = true;
        try {
            if (this.originalToCopy == null) {
                this.originalToCopy = new IdentityMap();
            }
            Object obj2 = this.originalToCopy.get(obj);
            if (obj2 != null) {
            }
            this.needsCopyReference = obj;
            Object copy = obj instanceof KryoCopyable ? ((KryoCopyable) obj).copy(this) : serializer.copy(this, obj);
            if (this.needsCopyReference != null) {
                reference(copy);
            }
            this.copyShallow = false;
            int i = this.copyDepth + REF;
            this.copyDepth = i;
            if (i != 0) {
                return copy;
            }
            reset();
            return copy;
        } finally {
            this.copyShallow = false;
            int i2 = this.copyDepth + REF;
            this.copyDepth = i2;
            if (i2 == 0) {
                reset();
            }
        }
    }

    public ClassLoader getClassLoader() {
        return this.classLoader;
    }

    public ClassResolver getClassResolver() {
        return this.classResolver;
    }

    public ObjectMap getContext() {
        if (this.context == null) {
            this.context = new ObjectMap();
        }
        return this.context;
    }

    public Serializer getDefaultSerializer(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (cls.isAnnotationPresent(DefaultSerializer.class)) {
            return newSerializer(((DefaultSerializer) cls.getAnnotation(DefaultSerializer.class)).value(), cls);
        } else {
            int size = this.defaultSerializers.size();
            for (int i = 0; i < size; i++) {
                DefaultSerializerEntry defaultSerializerEntry = (DefaultSerializerEntry) this.defaultSerializers.get(i);
                if (defaultSerializerEntry.type.isAssignableFrom(cls)) {
                    return defaultSerializerEntry.serializer != null ? defaultSerializerEntry.serializer : newSerializer(defaultSerializerEntry.serializerClass, cls);
                }
            }
            return newDefaultSerializer(cls);
        }
    }

    public int getDepth() {
        return this.depth;
    }

    public ObjectMap getGraphContext() {
        if (this.graphContext == null) {
            this.graphContext = new ObjectMap();
        }
        return this.graphContext;
    }

    public int getNextRegistrationId() {
        int i = this.nextRegisterID;
        while (this.classResolver.getRegistration(i) != null) {
            i++;
        }
        return i;
    }

    public ReferenceResolver getReferenceResolver() {
        return this.referenceResolver;
    }

    public boolean getReferences() {
        return this.references;
    }

    public Registration getRegistration(int i) {
        return this.classResolver.getRegistration(i);
    }

    public Registration getRegistration(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (cls == this.memoizedClass) {
            return this.memoizedClassValue;
        } else {
            Registration registration = this.classResolver.getRegistration(cls);
            if (registration == null) {
                if (Proxy.isProxyClass(cls)) {
                    registration = getRegistration(InvocationHandler.class);
                } else if (!cls.isEnum() && Enum.class.isAssignableFrom(cls)) {
                    registration = getRegistration(cls.getEnclosingClass());
                } else if (EnumSet.class.isAssignableFrom(cls)) {
                    registration = this.classResolver.getRegistration(EnumSet.class);
                }
                if (registration == null) {
                    if (this.registrationRequired) {
                        throw new IllegalArgumentException("Class is not registered: " + Util.className(cls) + "\nNote: To register this class use: kryo.register(" + Util.className(cls) + ".class);");
                    }
                    registration = this.classResolver.registerImplicit(cls);
                }
            }
            this.memoizedClass = cls;
            this.memoizedClassValue = registration;
            return registration;
        }
    }

    public Serializer getSerializer(Class cls) {
        return getRegistration(cls).getSerializer();
    }

    public boolean isFinal(Class cls) {
        if (cls != null) {
            return cls.isArray() ? Modifier.isFinal(Util.getElementClass(cls).getModifiers()) : Modifier.isFinal(cls.getModifiers());
        }
        throw new IllegalArgumentException("type cannot be null.");
    }

    public boolean isRegistrationRequired() {
        return this.registrationRequired;
    }

    /* access modifiers changed from: protected */
    public Serializer newDefaultSerializer(Class cls) {
        return newSerializer(this.defaultSerializer, cls);
    }

    public Object newInstance(Class cls) {
        Registration registration = getRegistration(cls);
        b.b.a.a instantiator = registration.getInstantiator();
        if (instantiator == null) {
            instantiator = newInstantiator(cls);
            registration.setInstantiator(instantiator);
        }
        return instantiator.newInstance();
    }

    /* access modifiers changed from: protected */
    public b.b.a.a newInstantiator(final Class cls) {
        final Constructor constructor;
        if (!Util.isAndroid) {
            try {
                final ConstructorAccess constructorAccess = ConstructorAccess.get(cls);
                return new b.b.a.a() {
                    public Object newInstance() {
                        try {
                            return constructorAccess.newInstance();
                        } catch (Exception e) {
                            throw new KryoException("Error constructing instance of class: " + Util.className(cls), e);
                        }
                    }
                };
            } catch (Exception e) {
            }
        }
        try {
            constructor = cls.getConstructor(null);
        } catch (Exception e2) {
            Constructor declaredConstructor = cls.getDeclaredConstructor(null);
            declaredConstructor.setAccessible(true);
            constructor = declaredConstructor;
        }
        try {
            return new b.b.a.a() {
                public Object newInstance() {
                    try {
                        return constructor.newInstance(new Object[0]);
                    } catch (Exception e) {
                        throw new KryoException("Error constructing instance of class: " + Util.className(cls), e);
                    }
                }
            };
        } catch (Exception e3) {
            if (this.strategy != null) {
                return this.strategy.a(cls);
            }
            if (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) {
                throw new KryoException("Class cannot be created (missing no-arg constructor): " + Util.className(cls));
            }
            throw new KryoException("Class cannot be created (non-static member class): " + Util.className(cls));
        }
    }

    public Serializer newSerializer(Class cls, Class cls2) {
        try {
            return (Serializer) cls.getConstructor(Kryo.class, Class.class).newInstance(this, cls2);
        } catch (NoSuchMethodException e) {
            try {
                return (Serializer) cls.getConstructor(Kryo.class).newInstance(this);
            } catch (NoSuchMethodException e2) {
                try {
                    return (Serializer) cls.getConstructor(Class.class).newInstance(cls2);
                } catch (NoSuchMethodException e3) {
                    try {
                        return (Serializer) cls.newInstance();
                    } catch (Exception e4) {
                        throw new IllegalArgumentException("Unable to create serializer \"" + cls.getName() + "\" for class: " + Util.className(cls2), e4);
                    }
                }
            }
        }
    }

    public Registration readClass(Input input) {
        if (input == null) {
            throw new IllegalArgumentException("input cannot be null.");
        }
        try {
            return this.classResolver.readClass(input);
        } finally {
            if (this.depth == 0 && this.autoReset) {
                reset();
            }
        }
    }

    public Object readClassAndObject(Input input) {
        int i;
        Object read;
        if (input == null) {
            throw new IllegalArgumentException("input cannot be null.");
        }
        beginObject();
        try {
            Registration readClass = readClass(input);
            if (readClass == null) {
                read = null;
            } else {
                Class type = readClass.getType();
                if (this.references) {
                    int readReferenceOrNull = readReferenceOrNull(input, type, false);
                    if (readReferenceOrNull == REF) {
                        read = this.readObject;
                        int i2 = this.depth + REF;
                        this.depth = i2;
                        if (i2 == 0 && this.autoReset) {
                            reset();
                        }
                    } else {
                        read = readClass.getSerializer().read(this, input, type);
                        if (readReferenceOrNull == this.readReferenceIds.size) {
                            reference(read);
                        }
                    }
                } else {
                    read = readClass.getSerializer().read(this, input, type);
                }
                int i3 = this.depth + REF;
                this.depth = i3;
                if (i3 == 0 && this.autoReset) {
                    reset();
                }
            }
            return read;
        } finally {
            i = this.depth + REF;
            this.depth = i;
            if (i == 0 && this.autoReset) {
                reset();
            }
        }
    }

    public Object readObject(Input input, Class cls) {
        Object read;
        if (input == null) {
            throw new IllegalArgumentException("input cannot be null.");
        } else if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else {
            beginObject();
            try {
                if (this.references) {
                    int readReferenceOrNull = readReferenceOrNull(input, cls, false);
                    if (readReferenceOrNull == REF) {
                        read = this.readObject;
                    } else {
                        read = getRegistration(cls).getSerializer().read(this, input, cls);
                        if (readReferenceOrNull == this.readReferenceIds.size) {
                            reference(read);
                        }
                        int i = this.depth + REF;
                        this.depth = i;
                        if (i == 0 && this.autoReset) {
                            reset();
                        }
                    }
                } else {
                    read = getRegistration(cls).getSerializer().read(this, input, cls);
                    int i2 = this.depth + REF;
                    this.depth = i2;
                    reset();
                }
                return read;
            } finally {
                int i3 = this.depth + REF;
                this.depth = i3;
                if (i3 == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    public Object readObject(Input input, Class cls, Serializer serializer) {
        Object read;
        if (input == null) {
            throw new IllegalArgumentException("input cannot be null.");
        } else if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (serializer == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            beginObject();
            try {
                if (this.references) {
                    int readReferenceOrNull = readReferenceOrNull(input, cls, false);
                    if (readReferenceOrNull == REF) {
                        read = this.readObject;
                    } else {
                        read = serializer.read(this, input, cls);
                        if (readReferenceOrNull == this.readReferenceIds.size) {
                            reference(read);
                        }
                        int i = this.depth + REF;
                        this.depth = i;
                        if (i == 0 && this.autoReset) {
                            reset();
                        }
                    }
                } else {
                    read = serializer.read(this, input, cls);
                    int i2 = this.depth + REF;
                    this.depth = i2;
                    reset();
                }
                return read;
            } finally {
                int i3 = this.depth + REF;
                this.depth = i3;
                if (i3 == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    public Object readObjectOrNull(Input input, Class cls) {
        Object read;
        if (input == null) {
            throw new IllegalArgumentException("input cannot be null.");
        } else if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else {
            beginObject();
            try {
                if (this.references) {
                    int readReferenceOrNull = readReferenceOrNull(input, cls, true);
                    if (readReferenceOrNull == REF) {
                        read = this.readObject;
                    } else {
                        read = getRegistration(cls).getSerializer().read(this, input, cls);
                        if (readReferenceOrNull == this.readReferenceIds.size) {
                            reference(read);
                        }
                        int i = this.depth + REF;
                        this.depth = i;
                        if (i == 0 && this.autoReset) {
                            reset();
                        }
                    }
                } else {
                    Serializer serializer = getRegistration(cls).getSerializer();
                    if (serializer.getAcceptsNull() || input.readByte() != 0) {
                        read = serializer.read(this, input, cls);
                        int i2 = this.depth + REF;
                        this.depth = i2;
                        reset();
                    } else {
                        read = null;
                        int i3 = this.depth + REF;
                        this.depth = i3;
                        if (i3 == 0 && this.autoReset) {
                            reset();
                        }
                    }
                }
                return read;
            } finally {
                int i4 = this.depth + REF;
                this.depth = i4;
                if (i4 == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    public Object readObjectOrNull(Input input, Class cls, Serializer serializer) {
        Object read;
        if (input == null) {
            throw new IllegalArgumentException("input cannot be null.");
        } else if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (serializer == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            beginObject();
            try {
                if (this.references) {
                    int readReferenceOrNull = readReferenceOrNull(input, cls, true);
                    if (readReferenceOrNull == REF) {
                        read = this.readObject;
                    } else {
                        read = serializer.read(this, input, cls);
                        if (readReferenceOrNull == this.readReferenceIds.size) {
                            reference(read);
                        }
                        int i = this.depth + REF;
                        this.depth = i;
                        if (i == 0 && this.autoReset) {
                            reset();
                        }
                    }
                } else if (serializer.getAcceptsNull() || input.readByte() != 0) {
                    read = serializer.read(this, input, cls);
                    int i2 = this.depth + REF;
                    this.depth = i2;
                    reset();
                } else {
                    read = null;
                    int i3 = this.depth + REF;
                    this.depth = i3;
                    if (i3 == 0 && this.autoReset) {
                        reset();
                    }
                }
                return read;
            } finally {
                int i4 = this.depth + REF;
                this.depth = i4;
                if (i4 == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int readReferenceOrNull(Input input, Class cls, boolean z) {
        int i;
        if (cls.isPrimitive()) {
            cls = Util.getWrapperClass(cls);
        }
        boolean useReferences = this.referenceResolver.useReferences(cls);
        if (z) {
            i = input.readInt(true);
            if (i == 0) {
                this.readObject = null;
                return REF;
            } else if (!useReferences) {
                this.readReferenceIds.add(NO_REF);
                return this.readReferenceIds.size;
            }
        } else if (!useReferences) {
            this.readReferenceIds.add(NO_REF);
            return this.readReferenceIds.size;
        } else {
            i = input.readInt(true);
        }
        if (i == 1) {
            this.readReferenceIds.add(this.referenceResolver.nextReadId(cls));
            return this.readReferenceIds.size;
        }
        this.readObject = this.referenceResolver.getReadObject(cls, i + NO_REF);
        return REF;
    }

    public void reference(Object obj) {
        int pop;
        if (this.copyDepth > 0) {
            if (this.needsCopyReference == null) {
                return;
            }
            if (obj == null) {
                throw new IllegalArgumentException("object cannot be null.");
            }
            this.originalToCopy.put(this.needsCopyReference, obj);
            this.needsCopyReference = null;
        } else if (this.references && obj != null && (pop = this.readReferenceIds.pop()) != NO_REF) {
            this.referenceResolver.addReadObject(pop, obj);
        }
    }

    public Registration register(Registration registration) {
        int id = registration.getId();
        if (id < 0) {
            throw new IllegalArgumentException("id must be > 0: " + id);
        }
        Registration registration2 = getRegistration(registration.getId());
        if (registration2 == null || registration2.getType() == registration.getType()) {
            return this.classResolver.register(registration);
        }
        throw new KryoException("An existing registration with a different type already uses ID: " + registration.getId() + "\nExisting registration: " + registration2 + "\nUnable to set registration: " + registration);
    }

    public Registration register(Class cls) {
        Registration registration = this.classResolver.getRegistration(cls);
        return registration != null ? registration : register(cls, getDefaultSerializer(cls));
    }

    public Registration register(Class cls, int i) {
        Registration registration = this.classResolver.getRegistration(cls);
        return registration != null ? registration : register(cls, getDefaultSerializer(cls), i);
    }

    public Registration register(Class cls, Serializer serializer) {
        Registration registration = this.classResolver.getRegistration(cls);
        if (registration == null) {
            return this.classResolver.register(new Registration(cls, serializer, getNextRegistrationId()));
        }
        registration.setSerializer(serializer);
        return registration;
    }

    public Registration register(Class cls, Serializer serializer, int i) {
        if (i >= 0) {
            return register(new Registration(cls, serializer, i));
        }
        throw new IllegalArgumentException("id must be >= 0: " + i);
    }

    public void reset() {
        this.depth = 0;
        if (this.graphContext != null) {
            this.graphContext.clear();
        }
        this.classResolver.reset();
        if (this.references) {
            this.referenceResolver.reset();
            this.readObject = null;
        }
        this.copyDepth = 0;
        if (this.originalToCopy != null) {
            this.originalToCopy.clear();
        }
    }

    public void setAutoReset(boolean z) {
        this.autoReset = z;
    }

    public void setClassLoader(ClassLoader classLoader2) {
        if (classLoader2 == null) {
            throw new IllegalArgumentException("classLoader cannot be null.");
        }
        this.classLoader = classLoader2;
    }

    public void setDefaultSerializer(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        }
        this.defaultSerializer = cls;
    }

    public void setInstantiatorStrategy(a aVar) {
        this.strategy = aVar;
    }

    public void setMaxDepth(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("maxDepth must be > 0.");
        }
        this.maxDepth = i;
    }

    public void setReferenceResolver(ReferenceResolver referenceResolver2) {
        if (referenceResolver2 == null) {
            throw new IllegalArgumentException("referenceResolver cannot be null.");
        }
        this.references = true;
        this.referenceResolver = referenceResolver2;
    }

    public boolean setReferences(boolean z) {
        if (z == this.references) {
            return z;
        }
        this.references = z;
        if (z && this.referenceResolver == null) {
            this.referenceResolver = new MapReferenceResolver();
        }
        return !z;
    }

    public void setRegistrationRequired(boolean z) {
        this.registrationRequired = z;
    }

    public Registration writeClass(Output output, Class cls) {
        if (output == null) {
            throw new IllegalArgumentException("output cannot be null.");
        }
        try {
            return this.classResolver.writeClass(output, cls);
        } finally {
            if (this.depth == 0 && this.autoReset) {
                reset();
            }
        }
    }

    public void writeClassAndObject(Output output, Object obj) {
        if (output == null) {
            throw new IllegalArgumentException("output cannot be null.");
        }
        beginObject();
        if (obj == null) {
            try {
                writeClass(output, null);
            } finally {
                int i = this.depth + REF;
                this.depth = i;
                if (i == 0 && this.autoReset) {
                    reset();
                }
            }
        } else {
            Registration writeClass = writeClass(output, obj.getClass());
            if (!this.references || !writeReferenceOrNull(output, obj, false)) {
                writeClass.getSerializer().write(this, output, obj);
                int i2 = this.depth + REF;
                this.depth = i2;
                if (i2 == 0 && this.autoReset) {
                    reset();
                    return;
                }
                return;
            }
            int i3 = this.depth + REF;
            this.depth = i3;
            if (i3 == 0 && this.autoReset) {
                reset();
            }
        }
    }

    public void writeObject(Output output, Object obj) {
        if (output == null) {
            throw new IllegalArgumentException("output cannot be null.");
        } else if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        } else {
            beginObject();
            try {
                if (!this.references || !writeReferenceOrNull(output, obj, false)) {
                    getRegistration(obj.getClass()).getSerializer().write(this, output, obj);
                    int i = this.depth + REF;
                    this.depth = i;
                    if (i == 0 && this.autoReset) {
                        reset();
                    }
                }
            } finally {
                int i2 = this.depth + REF;
                this.depth = i2;
                if (i2 == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    public void writeObject(Output output, Object obj, Serializer serializer) {
        if (output == null) {
            throw new IllegalArgumentException("output cannot be null.");
        } else if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        } else if (serializer == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            beginObject();
            try {
                if (!this.references || !writeReferenceOrNull(output, obj, false)) {
                    serializer.write(this, output, obj);
                    int i = this.depth + REF;
                    this.depth = i;
                    if (i == 0 && this.autoReset) {
                        reset();
                    }
                }
            } finally {
                int i2 = this.depth + REF;
                this.depth = i2;
                if (i2 == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    public void writeObjectOrNull(Output output, Object obj, Serializer serializer) {
        int i;
        boolean z;
        if (output == null) {
            throw new IllegalArgumentException("output cannot be null.");
        } else if (serializer == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            beginObject();
            try {
                if (this.references) {
                    if (writeReferenceOrNull(output, obj, true)) {
                        if (i == 0 && z) {
                            return;
                        }
                        return;
                    }
                } else if (!serializer.getAcceptsNull()) {
                    if (obj == null) {
                        output.writeByte((byte) 0);
                        int i2 = this.depth + REF;
                        this.depth = i2;
                        if (i2 == 0 && this.autoReset) {
                            reset();
                            return;
                        }
                        return;
                    }
                    output.writeByte((byte) 1);
                }
                serializer.write(this, output, obj);
                int i3 = this.depth + REF;
                this.depth = i3;
                if (i3 == 0 && this.autoReset) {
                    reset();
                }
            } finally {
                i = this.depth + REF;
                this.depth = i;
                if (i == 0 && this.autoReset) {
                    reset();
                }
            }
        }
    }

    public void writeObjectOrNull(Output output, Object obj, Class cls) {
        int i;
        boolean z;
        if (output == null) {
            throw new IllegalArgumentException("output cannot be null.");
        }
        beginObject();
        try {
            Serializer serializer = getRegistration(cls).getSerializer();
            if (this.references) {
                if (writeReferenceOrNull(output, obj, true)) {
                    if (i == 0 && z) {
                        return;
                    }
                    return;
                }
            } else if (!serializer.getAcceptsNull()) {
                if (obj == null) {
                    output.writeByte((byte) 0);
                    int i2 = this.depth + REF;
                    this.depth = i2;
                    if (i2 == 0 && this.autoReset) {
                        reset();
                        return;
                    }
                    return;
                }
                output.writeByte((byte) 1);
            }
            serializer.write(this, output, obj);
            int i3 = this.depth + REF;
            this.depth = i3;
            if (i3 == 0 && this.autoReset) {
                reset();
            }
        } finally {
            i = this.depth + REF;
            this.depth = i;
            if (i == 0 && this.autoReset) {
                reset();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean writeReferenceOrNull(Output output, Object obj, boolean z) {
        if (obj == null) {
            output.writeByte((byte) 0);
            return true;
        } else if (!this.referenceResolver.useReferences(obj.getClass())) {
            if (z) {
                output.writeByte((byte) 1);
            }
            return false;
        } else {
            int writtenId = this.referenceResolver.getWrittenId(obj);
            if (writtenId != REF) {
                output.writeInt(writtenId + 2, true);
                return true;
            }
            this.referenceResolver.addWrittenObject(obj);
            output.writeByte((byte) 1);
            return false;
        }
    }
}
