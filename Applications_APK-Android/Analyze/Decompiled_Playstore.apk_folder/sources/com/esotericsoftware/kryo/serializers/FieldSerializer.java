package com.esotericsoftware.kryo.serializers;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.NotNull;
import com.esotericsoftware.kryo.Registration;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.IntArray;
import com.esotericsoftware.kryo.util.ObjectMap;
import com.esotericsoftware.kryo.util.Util;
import com.esotericsoftware.reflectasm.FieldAccess;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FieldSerializer extends Serializer implements Comparator {
    Object access;
    private CachedField[] fields = new CachedField[0];
    private boolean fieldsCanBeNull = true;
    private boolean fixedFieldTypes;
    private boolean ignoreSyntheticFields = true;
    final Kryo kryo;
    private boolean setFieldsAsAccessible = true;
    final Class type;

    abstract class AsmCachedField extends CachedField {
        FieldAccess access = ((FieldAccess) FieldSerializer.this.access);

        AsmCachedField() {
            super();
        }
    }

    class BooleanField extends AsmCachedField {
        BooleanField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setBoolean(obj2, this.accessIndex, this.access.getBoolean(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setBoolean(obj, this.accessIndex, input.readBoolean());
        }

        public void write(Output output, Object obj) {
            output.writeBoolean(this.access.getBoolean(obj, this.accessIndex));
        }
    }

    class ByteField extends AsmCachedField {
        ByteField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setByte(obj2, this.accessIndex, this.access.getByte(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setByte(obj, this.accessIndex, input.readByte());
        }

        public void write(Output output, Object obj) {
            output.writeByte(this.access.getByte(obj, this.accessIndex));
        }
    }

    public abstract class CachedField {
        int accessIndex = -1;
        boolean canBeNull;
        Field field;
        Serializer serializer;
        Class valueClass;

        public CachedField() {
        }

        public abstract void copy(Object obj, Object obj2);

        public Field getField() {
            return this.field;
        }

        public abstract void read(Input input, Object obj);

        public void setCanBeNull(boolean z) {
            this.canBeNull = z;
        }

        public void setClass(Class cls) {
            this.valueClass = cls;
            this.serializer = null;
        }

        public void setClass(Class cls, Serializer serializer2) {
            this.valueClass = cls;
            this.serializer = serializer2;
        }

        public void setSerializer(Serializer serializer2) {
            this.serializer = serializer2;
        }

        public String toString() {
            return this.field.getName();
        }

        public abstract void write(Output output, Object obj);
    }

    class CharField extends AsmCachedField {
        CharField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setChar(obj2, this.accessIndex, this.access.getChar(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setChar(obj, this.accessIndex, input.readChar());
        }

        public void write(Output output, Object obj) {
            output.writeChar(this.access.getChar(obj, this.accessIndex));
        }
    }

    class DoubleField extends AsmCachedField {
        DoubleField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setDouble(obj2, this.accessIndex, this.access.getDouble(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setDouble(obj, this.accessIndex, input.readDouble());
        }

        public void write(Output output, Object obj) {
            output.writeDouble(this.access.getDouble(obj, this.accessIndex));
        }
    }

    class FloatField extends AsmCachedField {
        FloatField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setFloat(obj2, this.accessIndex, this.access.getFloat(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setFloat(obj, this.accessIndex, input.readFloat());
        }

        public void write(Output output, Object obj) {
            output.writeFloat(this.access.getFloat(obj, this.accessIndex));
        }
    }

    class IntField extends AsmCachedField {
        IntField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setInt(obj2, this.accessIndex, this.access.getInt(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setInt(obj, this.accessIndex, input.readInt(false));
        }

        public void write(Output output, Object obj) {
            output.writeInt(this.access.getInt(obj, this.accessIndex), false);
        }
    }

    class LongField extends AsmCachedField {
        LongField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setLong(obj2, this.accessIndex, this.access.getLong(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setLong(obj, this.accessIndex, input.readLong(false));
        }

        public void write(Output output, Object obj) {
            output.writeLong(this.access.getLong(obj, this.accessIndex), false);
        }
    }

    class ObjectField extends CachedField {
        Class[] generics;

        ObjectField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            try {
                if (this.accessIndex != -1) {
                    FieldAccess fieldAccess = (FieldAccess) FieldSerializer.this.access;
                    fieldAccess.set(obj2, this.accessIndex, FieldSerializer.this.kryo.copy(fieldAccess.get(obj, this.accessIndex)));
                    return;
                }
                this.field.set(obj2, FieldSerializer.this.kryo.copy(this.field.get(obj)));
            } catch (IllegalAccessException e) {
                throw new KryoException("Error accessing field: " + this + " (" + FieldSerializer.this.type.getName() + ")", e);
            } catch (KryoException e2) {
                e2.addTrace(this + " (" + FieldSerializer.this.type.getName() + ")");
                throw e2;
            } catch (RuntimeException e3) {
                KryoException kryoException = new KryoException(e3);
                kryoException.addTrace(this + " (" + FieldSerializer.this.type.getName() + ")");
                throw kryoException;
            }
        }

        public void read(Input input, Object obj) {
            Object readObjectOrNull;
            Object readObject;
            try {
                Class cls = this.valueClass;
                Serializer serializer = this.serializer;
                if (cls == null) {
                    Registration readClass = FieldSerializer.this.kryo.readClass(input);
                    if (readClass == null) {
                        readObject = null;
                    } else {
                        if (serializer == null) {
                            serializer = readClass.getSerializer();
                        }
                        if (this.generics != null) {
                            serializer.setGenerics(FieldSerializer.this.kryo, this.generics);
                        }
                        readObject = FieldSerializer.this.kryo.readObject(input, readClass.getType(), serializer);
                    }
                    readObjectOrNull = readObject;
                } else {
                    if (serializer == null) {
                        serializer = FieldSerializer.this.kryo.getSerializer(this.valueClass);
                        this.serializer = serializer;
                    }
                    if (this.generics != null) {
                        serializer.setGenerics(FieldSerializer.this.kryo, this.generics);
                    }
                    readObjectOrNull = this.canBeNull ? FieldSerializer.this.kryo.readObjectOrNull(input, cls, serializer) : FieldSerializer.this.kryo.readObject(input, cls, serializer);
                }
                if (this.accessIndex != -1) {
                    ((FieldAccess) FieldSerializer.this.access).set(obj, this.accessIndex, readObjectOrNull);
                } else {
                    this.field.set(obj, readObjectOrNull);
                }
            } catch (IllegalAccessException e) {
                throw new KryoException("Error accessing field: " + this + " (" + FieldSerializer.this.type.getName() + ")", e);
            } catch (KryoException e2) {
                e2.addTrace(this + " (" + FieldSerializer.this.type.getName() + ")");
                throw e2;
            } catch (RuntimeException e3) {
                KryoException kryoException = new KryoException(e3);
                kryoException.addTrace(this + " (" + FieldSerializer.this.type.getName() + ")");
                throw kryoException;
            }
        }

        public void write(Output output, Object obj) {
            try {
                Object obj2 = this.accessIndex != -1 ? ((FieldAccess) FieldSerializer.this.access).get(obj, this.accessIndex) : this.field.get(obj);
                Serializer serializer = this.serializer;
                if (this.valueClass != null) {
                    if (serializer == null) {
                        serializer = FieldSerializer.this.kryo.getSerializer(this.valueClass);
                        this.serializer = serializer;
                    }
                    if (this.generics != null) {
                        serializer.setGenerics(FieldSerializer.this.kryo, this.generics);
                    }
                    if (this.canBeNull) {
                        FieldSerializer.this.kryo.writeObjectOrNull(output, obj2, serializer);
                    } else if (obj2 == null) {
                        throw new KryoException("Field value is null but canBeNull is false: " + this + " (" + obj.getClass().getName() + ")");
                    } else {
                        FieldSerializer.this.kryo.writeObject(output, obj2, serializer);
                    }
                } else if (obj2 == null) {
                    FieldSerializer.this.kryo.writeClass(output, null);
                } else {
                    Registration writeClass = FieldSerializer.this.kryo.writeClass(output, obj2.getClass());
                    if (serializer == null) {
                        serializer = writeClass.getSerializer();
                    }
                    if (this.generics != null) {
                        serializer.setGenerics(FieldSerializer.this.kryo, this.generics);
                    }
                    FieldSerializer.this.kryo.writeObject(output, obj2, serializer);
                }
            } catch (IllegalAccessException e) {
                throw new KryoException("Error accessing field: " + this + " (" + obj.getClass().getName() + ")", e);
            } catch (KryoException e2) {
                e2.addTrace(this + " (" + obj.getClass().getName() + ")");
                throw e2;
            } catch (RuntimeException e3) {
                KryoException kryoException = new KryoException(e3);
                kryoException.addTrace(this + " (" + obj.getClass().getName() + ")");
                throw kryoException;
            }
        }
    }

    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Optional {
        String value();
    }

    class ShortField extends AsmCachedField {
        ShortField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.setShort(obj2, this.accessIndex, this.access.getShort(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.setShort(obj, this.accessIndex, input.readShort());
        }

        public void write(Output output, Object obj) {
            output.writeShort(this.access.getShort(obj, this.accessIndex));
        }
    }

    class StringField extends AsmCachedField {
        StringField() {
            super();
        }

        public void copy(Object obj, Object obj2) {
            this.access.set(obj2, this.accessIndex, this.access.getString(obj, this.accessIndex));
        }

        public void read(Input input, Object obj) {
            this.access.set(obj, this.accessIndex, input.readString());
        }

        public void write(Output output, Object obj) {
            output.writeString(this.access.getString(obj, this.accessIndex));
        }
    }

    public FieldSerializer(Kryo kryo2, Class cls) {
        this.kryo = kryo2;
        this.type = cls;
        rebuildCachedFields();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private CachedField newCachedField(Field field, int i, int i2) {
        CachedField cachedField;
        Class<?> type2 = field.getType();
        if (i2 != -1) {
            cachedField = type2.isPrimitive() ? type2 == Boolean.TYPE ? new BooleanField() : type2 == Byte.TYPE ? new ByteField() : type2 == Character.TYPE ? new CharField() : type2 == Short.TYPE ? new ShortField() : type2 == Integer.TYPE ? new IntField() : type2 == Long.TYPE ? new LongField() : type2 == Float.TYPE ? new FloatField() : type2 == Double.TYPE ? new DoubleField() : new ObjectField() : (type2 != String.class || (this.kryo.getReferences() && this.kryo.getReferenceResolver().useReferences(String.class))) ? new ObjectField() : new StringField();
        } else {
            CachedField objectField = new ObjectField();
            ((ObjectField) objectField).generics = Kryo.getGenerics(field.getGenericType());
            cachedField = objectField;
        }
        cachedField.field = field;
        cachedField.accessIndex = i2;
        cachedField.canBeNull = this.fieldsCanBeNull && !type2.isPrimitive() && !field.isAnnotationPresent(NotNull.class);
        if (this.kryo.isFinal(type2) || this.fixedFieldTypes) {
            cachedField.valueClass = type2;
        }
        return cachedField;
    }

    private void rebuildCachedFields() {
        if (this.type.isInterface()) {
            this.fields = new CachedField[0];
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (Class<? super Object> cls = this.type; cls != Object.class; cls = cls.getSuperclass()) {
            Collections.addAll(arrayList, cls.getDeclaredFields());
        }
        ObjectMap context = this.kryo.getContext();
        IntArray intArray = new IntArray();
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Field field = (Field) arrayList.get(i);
            int modifiers = field.getModifiers();
            if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers) && (!field.isSynthetic() || !this.ignoreSyntheticFields)) {
                if (!field.isAccessible()) {
                    if (this.setFieldsAsAccessible) {
                        try {
                            field.setAccessible(true);
                        } catch (AccessControlException e) {
                        }
                    }
                }
                Optional optional = (Optional) field.getAnnotation(Optional.class);
                if (optional == null || context.containsKey(optional.value())) {
                    arrayList2.add(field);
                    intArray.add((Modifier.isFinal(modifiers) || !Modifier.isPublic(modifiers) || !Modifier.isPublic(field.getType().getModifiers())) ? 0 : 1);
                }
            }
        }
        if (!Util.isAndroid && Modifier.isPublic(this.type.getModifiers()) && intArray.indexOf(1) != -1) {
            try {
                this.access = FieldAccess.get(this.type);
            } catch (RuntimeException e2) {
            }
        }
        ArrayList arrayList3 = new ArrayList(arrayList2.size());
        int size2 = arrayList2.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Field field2 = (Field) arrayList2.get(i2);
            arrayList3.add(newCachedField(field2, arrayList3.size(), (this.access == null || intArray.get(i2) != 1) ? -1 : ((FieldAccess) this.access).getIndex(field2.getName())));
        }
        Collections.sort(arrayList3, this);
        this.fields = (CachedField[]) arrayList3.toArray(new CachedField[arrayList3.size()]);
        initializeCachedFields();
    }

    public int compare(CachedField cachedField, CachedField cachedField2) {
        return cachedField.field.getName().compareTo(cachedField2.field.getName());
    }

    public Object copy(Kryo kryo2, Object obj) {
        Object createCopy = createCopy(kryo2, obj);
        kryo2.reference(createCopy);
        for (CachedField copy : this.fields) {
            copy.copy(obj, createCopy);
        }
        return createCopy;
    }

    /* access modifiers changed from: protected */
    public Object create(Kryo kryo2, Input input, Class cls) {
        return kryo2.newInstance(cls);
    }

    /* access modifiers changed from: protected */
    public Object createCopy(Kryo kryo2, Object obj) {
        return kryo2.newInstance(obj.getClass());
    }

    public CachedField getField(String str) {
        for (CachedField cachedField : this.fields) {
            if (cachedField.field.getName().equals(str)) {
                return cachedField;
            }
        }
        throw new IllegalArgumentException("Field \"" + str + "\" not found on class: " + this.type.getName());
    }

    public CachedField[] getFields() {
        return this.fields;
    }

    public Class getType() {
        return this.type;
    }

    /* access modifiers changed from: protected */
    public void initializeCachedFields() {
    }

    public Object read(Kryo kryo2, Input input, Class cls) {
        Object create = create(kryo2, input, cls);
        kryo2.reference(create);
        for (CachedField read : this.fields) {
            read.read(input, create);
        }
        return create;
    }

    public void removeField(String str) {
        for (int i = 0; i < this.fields.length; i++) {
            if (this.fields[i].field.getName().equals(str)) {
                CachedField[] cachedFieldArr = new CachedField[(this.fields.length - 1)];
                System.arraycopy(this.fields, 0, cachedFieldArr, 0, i);
                System.arraycopy(this.fields, i + 1, cachedFieldArr, i, cachedFieldArr.length - i);
                this.fields = cachedFieldArr;
                return;
            }
        }
        throw new IllegalArgumentException("Field \"" + str + "\" not found on class: " + this.type.getName());
    }

    public void setFieldsAsAccessible(boolean z) {
        this.setFieldsAsAccessible = z;
        rebuildCachedFields();
    }

    public void setFieldsCanBeNull(boolean z) {
        this.fieldsCanBeNull = z;
        rebuildCachedFields();
    }

    public void setFixedFieldTypes(boolean z) {
        this.fixedFieldTypes = z;
        rebuildCachedFields();
    }

    public void setIgnoreSyntheticFields(boolean z) {
        this.ignoreSyntheticFields = z;
        rebuildCachedFields();
    }

    public void write(Kryo kryo2, Output output, Object obj) {
        for (CachedField write : this.fields) {
            write.write(output, obj);
        }
    }
}
