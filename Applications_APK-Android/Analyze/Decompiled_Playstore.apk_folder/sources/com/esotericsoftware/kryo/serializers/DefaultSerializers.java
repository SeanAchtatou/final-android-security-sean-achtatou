package com.esotericsoftware.kryo.serializers;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.Registration;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

public class DefaultSerializers {

    public class BigDecimalSerializer extends Serializer {
        private BigIntegerSerializer bigIntegerSerializer = new BigIntegerSerializer();

        public BigDecimalSerializer() {
            setAcceptsNull(true);
            setImmutable(true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.esotericsoftware.kryo.serializers.DefaultSerializers.BigIntegerSerializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.math.BigInteger
         arg types: [com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, ?[OBJECT, ARRAY]]
         candidates:
          com.esotericsoftware.kryo.serializers.DefaultSerializers.BigIntegerSerializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.lang.Object
          com.esotericsoftware.kryo.Serializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.lang.Object
          com.esotericsoftware.kryo.serializers.DefaultSerializers.BigIntegerSerializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.math.BigInteger */
        public BigDecimal read(Kryo kryo, Input input, Class cls) {
            BigInteger read = this.bigIntegerSerializer.read(kryo, input, (Class) null);
            if (read == null) {
                return null;
            }
            return new BigDecimal(read, input.readInt(false));
        }

        public void write(Kryo kryo, Output output, BigDecimal bigDecimal) {
            if (bigDecimal == null) {
                output.writeByte((byte) 0);
                return;
            }
            this.bigIntegerSerializer.write(kryo, output, bigDecimal.unscaledValue());
            output.writeInt(bigDecimal.scale(), false);
        }
    }

    public class BigIntegerSerializer extends Serializer {
        public BigIntegerSerializer() {
            setImmutable(true);
            setAcceptsNull(true);
        }

        public BigInteger read(Kryo kryo, Input input, Class cls) {
            int readInt = input.readInt(true);
            if (readInt == 0) {
                return null;
            }
            return new BigInteger(input.readBytes(readInt - 1));
        }

        public void write(Kryo kryo, Output output, BigInteger bigInteger) {
            if (bigInteger == null) {
                output.writeByte((byte) 0);
                return;
            }
            byte[] byteArray = bigInteger.toByteArray();
            output.writeInt(byteArray.length + 1, true);
            output.writeBytes(byteArray);
        }
    }

    public class BooleanSerializer extends Serializer {
        public BooleanSerializer() {
            setImmutable(true);
        }

        public Boolean read(Kryo kryo, Input input, Class cls) {
            return Boolean.valueOf(input.readBoolean());
        }

        public void write(Kryo kryo, Output output, Boolean bool) {
            output.writeBoolean(bool.booleanValue());
        }
    }

    public class ByteSerializer extends Serializer {
        public ByteSerializer() {
            setImmutable(true);
        }

        public Byte read(Kryo kryo, Input input, Class cls) {
            return Byte.valueOf(input.readByte());
        }

        public void write(Kryo kryo, Output output, Byte b2) {
            output.writeByte(b2.byteValue());
        }
    }

    public class CalendarSerializer extends Serializer {
        private static final long DEFAULT_GREGORIAN_CUTOVER = -12219292800000L;
        TimeZoneSerializer timeZoneSerializer = new TimeZoneSerializer();

        public Calendar copy(Kryo kryo, Calendar calendar) {
            return (Calendar) calendar.clone();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.esotericsoftware.kryo.serializers.DefaultSerializers.TimeZoneSerializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.util.TimeZone
         arg types: [com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class]
         candidates:
          com.esotericsoftware.kryo.serializers.DefaultSerializers.TimeZoneSerializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.lang.Object
          com.esotericsoftware.kryo.Serializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.lang.Object
          com.esotericsoftware.kryo.serializers.DefaultSerializers.TimeZoneSerializer.read(com.esotericsoftware.kryo.Kryo, com.esotericsoftware.kryo.io.Input, java.lang.Class):java.util.TimeZone */
        public Calendar read(Kryo kryo, Input input, Class cls) {
            Calendar instance = Calendar.getInstance(this.timeZoneSerializer.read(kryo, input, TimeZone.class));
            instance.setTimeInMillis(input.readLong(true));
            instance.setLenient(input.readBoolean());
            instance.setFirstDayOfWeek(input.readInt(true));
            instance.setMinimalDaysInFirstWeek(input.readInt(true));
            long readLong = input.readLong(false);
            if (readLong != DEFAULT_GREGORIAN_CUTOVER && (instance instanceof GregorianCalendar)) {
                ((GregorianCalendar) instance).setGregorianChange(new Date(readLong));
            }
            return instance;
        }

        public void write(Kryo kryo, Output output, Calendar calendar) {
            this.timeZoneSerializer.write(kryo, output, calendar.getTimeZone());
            output.writeLong(calendar.getTimeInMillis(), true);
            output.writeBoolean(calendar.isLenient());
            output.writeInt(calendar.getFirstDayOfWeek(), true);
            output.writeInt(calendar.getMinimalDaysInFirstWeek(), true);
            if (calendar instanceof GregorianCalendar) {
                output.writeLong(((GregorianCalendar) calendar).getGregorianChange().getTime(), false);
            } else {
                output.writeLong(DEFAULT_GREGORIAN_CUTOVER, false);
            }
        }
    }

    public class CharSerializer extends Serializer {
        public CharSerializer() {
            setImmutable(true);
        }

        public Character read(Kryo kryo, Input input, Class cls) {
            return Character.valueOf(input.readChar());
        }

        public void write(Kryo kryo, Output output, Character ch) {
            output.writeChar(ch.charValue());
        }
    }

    public class ClassSerializer extends Serializer {
        public ClassSerializer() {
            setImmutable(true);
            setAcceptsNull(true);
        }

        public Class read(Kryo kryo, Input input, Class cls) {
            return kryo.readClass(input).getType();
        }

        public void write(Kryo kryo, Output output, Class cls) {
            kryo.writeClass(output, cls);
        }
    }

    public class CollectionsEmptyListSerializer extends Serializer {
        public CollectionsEmptyListSerializer() {
            setImmutable(true);
        }

        public Object read(Kryo kryo, Input input, Class cls) {
            return Collections.EMPTY_LIST;
        }

        public void write(Kryo kryo, Output output, Object obj) {
        }
    }

    public class CollectionsEmptyMapSerializer extends Serializer {
        public CollectionsEmptyMapSerializer() {
            setImmutable(true);
        }

        public Object read(Kryo kryo, Input input, Class cls) {
            return Collections.EMPTY_MAP;
        }

        public void write(Kryo kryo, Output output, Object obj) {
        }
    }

    public class CollectionsEmptySetSerializer extends Serializer {
        public CollectionsEmptySetSerializer() {
            setImmutable(true);
        }

        public Object read(Kryo kryo, Input input, Class cls) {
            return Collections.EMPTY_SET;
        }

        public void write(Kryo kryo, Output output, Object obj) {
        }
    }

    public class CollectionsSingletonListSerializer extends Serializer {
        public CollectionsSingletonListSerializer() {
            setImmutable(true);
        }

        public List read(Kryo kryo, Input input, Class cls) {
            return Collections.singletonList(kryo.readClassAndObject(input));
        }

        public void write(Kryo kryo, Output output, List list) {
            kryo.writeClassAndObject(output, list.get(0));
        }
    }

    public class CollectionsSingletonMapSerializer extends Serializer {
        public CollectionsSingletonMapSerializer() {
            setImmutable(true);
        }

        public Map read(Kryo kryo, Input input, Class cls) {
            return Collections.singletonMap(kryo.readClassAndObject(input), kryo.readClassAndObject(input));
        }

        public void write(Kryo kryo, Output output, Map map) {
            Map.Entry entry = (Map.Entry) map.entrySet().iterator().next();
            kryo.writeClassAndObject(output, entry.getKey());
            kryo.writeClassAndObject(output, entry.getValue());
        }
    }

    public class CollectionsSingletonSetSerializer extends Serializer {
        public CollectionsSingletonSetSerializer() {
            setImmutable(true);
        }

        public Set read(Kryo kryo, Input input, Class cls) {
            return Collections.singleton(kryo.readClassAndObject(input));
        }

        public void write(Kryo kryo, Output output, Set set) {
            kryo.writeClassAndObject(output, set.iterator().next());
        }
    }

    public class CurrencySerializer extends Serializer {
        public CurrencySerializer() {
            setImmutable(true);
            setAcceptsNull(true);
        }

        public Currency read(Kryo kryo, Input input, Class cls) {
            String readString = input.readString();
            if (readString == null) {
                return null;
            }
            return Currency.getInstance(readString);
        }

        public void write(Kryo kryo, Output output, Currency currency) {
            output.writeString(currency == null ? null : currency.getCurrencyCode());
        }
    }

    public class DateSerializer extends Serializer {
        public Date copy(Kryo kryo, Date date) {
            return new Date(date.getTime());
        }

        public Date read(Kryo kryo, Input input, Class cls) {
            return new Date(input.readLong(true));
        }

        public void write(Kryo kryo, Output output, Date date) {
            output.writeLong(date.getTime(), true);
        }
    }

    public class DoubleSerializer extends Serializer {
        public DoubleSerializer() {
            setImmutable(true);
        }

        public Double read(Kryo kryo, Input input, Class cls) {
            return Double.valueOf(input.readDouble());
        }

        public void write(Kryo kryo, Output output, Double d) {
            output.writeDouble(d.doubleValue());
        }
    }

    public class EnumSerializer extends Serializer {
        private Object[] enumConstants;

        public EnumSerializer(Class cls) {
            setImmutable(true);
            setAcceptsNull(true);
            this.enumConstants = cls.getEnumConstants();
            if (this.enumConstants == null) {
                throw new IllegalArgumentException("The type must be an enum: " + cls);
            }
        }

        public Enum read(Kryo kryo, Input input, Class cls) {
            int readInt = input.readInt(true);
            if (readInt == 0) {
                return null;
            }
            int i = readInt - 1;
            if (i >= 0 && i <= this.enumConstants.length - 1) {
                return (Enum) this.enumConstants[i];
            }
            throw new KryoException("Invalid ordinal for enum \"" + cls.getName() + "\": " + i);
        }

        public void write(Kryo kryo, Output output, Enum enumR) {
            if (enumR == null) {
                output.writeByte((byte) 0);
            } else {
                output.writeInt(enumR.ordinal() + 1, true);
            }
        }
    }

    public class EnumSetSerializer extends Serializer {
        public EnumSet copy(Kryo kryo, EnumSet enumSet) {
            return EnumSet.copyOf(enumSet);
        }

        public EnumSet read(Kryo kryo, Input input, Class cls) {
            Registration readClass = kryo.readClass(input);
            EnumSet noneOf = EnumSet.noneOf(readClass.getType());
            Serializer serializer = readClass.getSerializer();
            int readInt = input.readInt(true);
            for (int i = 0; i < readInt; i++) {
                noneOf.add(serializer.read(kryo, input, null));
            }
            return noneOf;
        }

        public void write(Kryo kryo, Output output, EnumSet enumSet) {
            if (enumSet.isEmpty()) {
                throw new KryoException("An empty EnumSet cannot be serialized.");
            }
            Serializer serializer = kryo.writeClass(output, enumSet.iterator().next().getClass()).getSerializer();
            output.writeInt(enumSet.size(), true);
            Iterator it = enumSet.iterator();
            while (it.hasNext()) {
                serializer.write(kryo, output, it.next());
            }
        }
    }

    public class FloatSerializer extends Serializer {
        public FloatSerializer() {
            setImmutable(true);
        }

        public Float read(Kryo kryo, Input input, Class cls) {
            return Float.valueOf(input.readFloat());
        }

        public void write(Kryo kryo, Output output, Float f) {
            output.writeFloat(f.floatValue());
        }
    }

    public class IntSerializer extends Serializer {
        public IntSerializer() {
            setImmutable(true);
        }

        public Integer read(Kryo kryo, Input input, Class cls) {
            return Integer.valueOf(input.readInt(false));
        }

        public void write(Kryo kryo, Output output, Integer num) {
            output.writeInt(num.intValue(), false);
        }
    }

    public class KryoSerializableSerializer extends Serializer {
        public KryoSerializable read(Kryo kryo, Input input, Class cls) {
            KryoSerializable kryoSerializable = (KryoSerializable) kryo.newInstance(cls);
            kryo.reference(kryoSerializable);
            kryoSerializable.read(kryo, input);
            return kryoSerializable;
        }

        public void write(Kryo kryo, Output output, KryoSerializable kryoSerializable) {
            kryoSerializable.write(kryo, output);
        }
    }

    public class LongSerializer extends Serializer {
        public LongSerializer() {
            setImmutable(true);
        }

        public Long read(Kryo kryo, Input input, Class cls) {
            return Long.valueOf(input.readLong(false));
        }

        public void write(Kryo kryo, Output output, Long l) {
            output.writeLong(l.longValue(), false);
        }
    }

    public class ShortSerializer extends Serializer {
        public ShortSerializer() {
            setImmutable(true);
        }

        public Short read(Kryo kryo, Input input, Class cls) {
            return Short.valueOf(input.readShort());
        }

        public void write(Kryo kryo, Output output, Short sh) {
            output.writeShort(sh.shortValue());
        }
    }

    public class StringBufferSerializer extends Serializer {
        public StringBufferSerializer() {
            setAcceptsNull(true);
        }

        public StringBuffer copy(Kryo kryo, StringBuffer stringBuffer) {
            return new StringBuffer(stringBuffer);
        }

        public StringBuffer read(Kryo kryo, Input input, Class cls) {
            String readString = input.readString();
            if (readString == null) {
                return null;
            }
            return new StringBuffer(readString);
        }

        public void write(Kryo kryo, Output output, StringBuffer stringBuffer) {
            output.writeString(stringBuffer);
        }
    }

    public class StringBuilderSerializer extends Serializer {
        public StringBuilderSerializer() {
            setAcceptsNull(true);
        }

        public StringBuilder copy(Kryo kryo, StringBuilder sb) {
            return new StringBuilder(sb);
        }

        public StringBuilder read(Kryo kryo, Input input, Class cls) {
            return input.readStringBuilder();
        }

        public void write(Kryo kryo, Output output, StringBuilder sb) {
            output.writeString(sb);
        }
    }

    public class StringSerializer extends Serializer {
        public StringSerializer() {
            setImmutable(true);
            setAcceptsNull(true);
        }

        public String read(Kryo kryo, Input input, Class cls) {
            return input.readString();
        }

        public void write(Kryo kryo, Output output, String str) {
            output.writeString(str);
        }
    }

    public class TimeZoneSerializer extends Serializer {
        public TimeZoneSerializer() {
            setImmutable(true);
        }

        public TimeZone read(Kryo kryo, Input input, Class cls) {
            return TimeZone.getTimeZone(input.readString());
        }

        public void write(Kryo kryo, Output output, TimeZone timeZone) {
            output.writeString(timeZone.getID());
        }
    }

    public class TreeMapSerializer extends MapSerializer {
        /* access modifiers changed from: protected */
        public Map create(Kryo kryo, Input input, Class cls) {
            kryo.setReferences(kryo.setReferences(false));
            return new TreeMap((Comparator) kryo.readClassAndObject(input));
        }

        /* access modifiers changed from: protected */
        public Map createCopy(Kryo kryo, Map map) {
            return new TreeMap(((TreeMap) map).comparator());
        }

        public void write(Kryo kryo, Output output, Map map) {
            boolean references = kryo.setReferences(false);
            kryo.writeClassAndObject(output, ((TreeMap) map).comparator());
            kryo.setReferences(references);
            super.write(kryo, output, map);
        }
    }
}
