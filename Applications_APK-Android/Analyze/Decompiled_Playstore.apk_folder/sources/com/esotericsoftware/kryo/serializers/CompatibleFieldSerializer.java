package com.esotericsoftware.kryo.serializers;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.InputChunked;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.io.OutputChunked;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryo.util.ObjectMap;

public class CompatibleFieldSerializer extends FieldSerializer {
    public CompatibleFieldSerializer(Kryo kryo, Class cls) {
        super(kryo, cls);
    }

    public Object read(Kryo kryo, Input input, Class cls) {
        Object newInstance = kryo.newInstance(cls);
        kryo.reference(newInstance);
        ObjectMap graphContext = kryo.getGraphContext();
        FieldSerializer.CachedField[] cachedFieldArr = (FieldSerializer.CachedField[]) graphContext.get(this);
        if (cachedFieldArr == null) {
            int readInt = input.readInt(true);
            String[] strArr = new String[readInt];
            for (int i = 0; i < readInt; i++) {
                strArr[i] = input.readString();
            }
            cachedFieldArr = new FieldSerializer.CachedField[readInt];
            FieldSerializer.CachedField[] fields = getFields();
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                String str = strArr[i2];
                int length2 = fields.length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length2) {
                        break;
                    } else if (fields[i3].field.getName().equals(str)) {
                        cachedFieldArr[i2] = fields[i3];
                        break;
                    } else {
                        i3++;
                    }
                }
            }
            graphContext.put(this, cachedFieldArr);
        }
        InputChunked inputChunked = new InputChunked(input, 1024);
        for (FieldSerializer.CachedField cachedField : cachedFieldArr) {
            if (cachedField == null) {
                inputChunked.nextChunks();
            } else {
                cachedField.read(inputChunked, newInstance);
                inputChunked.nextChunks();
            }
        }
        return newInstance;
    }

    public void write(Kryo kryo, Output output, Object obj) {
        FieldSerializer.CachedField[] fields = getFields();
        ObjectMap graphContext = kryo.getGraphContext();
        if (!graphContext.containsKey(this)) {
            graphContext.put(this, null);
            output.writeInt(fields.length, true);
            for (FieldSerializer.CachedField cachedField : fields) {
                output.writeString(cachedField.field.getName());
            }
        }
        OutputChunked outputChunked = new OutputChunked(output, 1024);
        for (FieldSerializer.CachedField write : fields) {
            write.write(outputChunked, obj);
            outputChunked.endChunks();
        }
    }
}
