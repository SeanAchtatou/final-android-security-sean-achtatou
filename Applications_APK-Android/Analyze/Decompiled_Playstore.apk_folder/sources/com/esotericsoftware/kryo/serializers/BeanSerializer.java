package com.esotericsoftware.kryo.serializers;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.reflectasm.MethodAccess;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class BeanSerializer extends Serializer {
    static final Object[] noArgs = new Object[0];
    Object access;
    private final Kryo kryo;
    private CachedProperty[] properties;

    class CachedProperty {
        Method getMethod;
        int getterAccessIndex;
        String name;
        Serializer serializer;
        Method setMethod;
        Class setMethodType;
        int setterAccessIndex;

        CachedProperty() {
        }

        /* access modifiers changed from: package-private */
        public Object get(Object obj) {
            return BeanSerializer.this.access != null ? ((MethodAccess) BeanSerializer.this.access).invoke(obj, this.getterAccessIndex, new Object[0]) : this.getMethod.invoke(obj, BeanSerializer.noArgs);
        }

        /* access modifiers changed from: package-private */
        public void set(Object obj, Object obj2) {
            if (BeanSerializer.this.access != null) {
                ((MethodAccess) BeanSerializer.this.access).invoke(obj, this.setterAccessIndex, obj2);
                return;
            }
            this.setMethod.invoke(obj, obj2);
        }

        public String toString() {
            return this.name;
        }
    }

    public BeanSerializer(Kryo kryo2, Class cls) {
        this.kryo = kryo2;
        try {
            PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(cls).getPropertyDescriptors();
            Arrays.sort(propertyDescriptors, new Comparator() {
                public int compare(PropertyDescriptor propertyDescriptor, PropertyDescriptor propertyDescriptor2) {
                    return propertyDescriptor.getName().compareTo(propertyDescriptor2.getName());
                }
            });
            ArrayList arrayList = new ArrayList(propertyDescriptors.length);
            for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                String name = propertyDescriptor.getName();
                if (!name.equals("class")) {
                    Method readMethod = propertyDescriptor.getReadMethod();
                    Method writeMethod = propertyDescriptor.getWriteMethod();
                    if (!(readMethod == null || writeMethod == null)) {
                        Serializer serializer = null;
                        Class<?> returnType = readMethod.getReturnType();
                        serializer = kryo2.isFinal(returnType) ? kryo2.getRegistration(returnType).getSerializer() : serializer;
                        CachedProperty cachedProperty = new CachedProperty();
                        cachedProperty.name = name;
                        cachedProperty.getMethod = readMethod;
                        cachedProperty.setMethod = writeMethod;
                        cachedProperty.serializer = serializer;
                        cachedProperty.setMethodType = writeMethod.getParameterTypes()[0];
                        arrayList.add(cachedProperty);
                    }
                }
            }
            this.properties = (CachedProperty[]) arrayList.toArray(new CachedProperty[arrayList.size()]);
            try {
                this.access = MethodAccess.get(cls);
                for (CachedProperty cachedProperty2 : this.properties) {
                    cachedProperty2.getterAccessIndex = ((MethodAccess) this.access).getIndex(cachedProperty2.getMethod.getName());
                    cachedProperty2.setterAccessIndex = ((MethodAccess) this.access).getIndex(cachedProperty2.setMethod.getName());
                }
            } catch (Throwable th) {
            }
        } catch (IntrospectionException e) {
            throw new KryoException("Error getting bean info.", e);
        }
    }

    public Object copy(Kryo kryo2, Object obj) {
        Object newInstance = kryo2.newInstance(obj.getClass());
        int i = 0;
        int length = this.properties.length;
        while (i < length) {
            CachedProperty cachedProperty = this.properties[i];
            try {
                cachedProperty.set(newInstance, cachedProperty.get(obj));
                i++;
            } catch (KryoException e) {
                e.addTrace(cachedProperty + " (" + newInstance.getClass().getName() + ")");
                throw e;
            } catch (RuntimeException e2) {
                KryoException kryoException = new KryoException(e2);
                kryoException.addTrace(cachedProperty + " (" + newInstance.getClass().getName() + ")");
                throw kryoException;
            } catch (Exception e3) {
                throw new KryoException("Error copying bean property: " + cachedProperty + " (" + newInstance.getClass().getName() + ")", e3);
            }
        }
        return newInstance;
    }

    public Object read(Kryo kryo2, Input input, Class cls) {
        Object newInstance = kryo2.newInstance(cls);
        kryo2.reference(newInstance);
        int length = this.properties.length;
        int i = 0;
        while (i < length) {
            CachedProperty cachedProperty = this.properties[i];
            try {
                Serializer serializer = cachedProperty.serializer;
                cachedProperty.set(newInstance, serializer != null ? kryo2.readObjectOrNull(input, cachedProperty.setMethodType, serializer) : kryo2.readClassAndObject(input));
                i++;
            } catch (IllegalAccessException e) {
                throw new KryoException("Error accessing setter method: " + cachedProperty + " (" + newInstance.getClass().getName() + ")", e);
            } catch (InvocationTargetException e2) {
                throw new KryoException("Error invoking setter method: " + cachedProperty + " (" + newInstance.getClass().getName() + ")", e2);
            } catch (KryoException e3) {
                e3.addTrace(cachedProperty + " (" + newInstance.getClass().getName() + ")");
                throw e3;
            } catch (RuntimeException e4) {
                KryoException kryoException = new KryoException(e4);
                kryoException.addTrace(cachedProperty + " (" + newInstance.getClass().getName() + ")");
                throw kryoException;
            }
        }
        return newInstance;
    }

    public void write(Kryo kryo2, Output output, Object obj) {
        Class<?> cls = obj.getClass();
        int i = 0;
        int length = this.properties.length;
        while (i < length) {
            CachedProperty cachedProperty = this.properties[i];
            try {
                Object obj2 = cachedProperty.get(obj);
                Serializer serializer = cachedProperty.serializer;
                if (serializer != null) {
                    kryo2.writeObjectOrNull(output, obj2, serializer);
                } else {
                    kryo2.writeClassAndObject(output, obj2);
                }
                i++;
            } catch (IllegalAccessException e) {
                throw new KryoException("Error accessing getter method: " + cachedProperty + " (" + cls.getName() + ")", e);
            } catch (InvocationTargetException e2) {
                throw new KryoException("Error invoking getter method: " + cachedProperty + " (" + cls.getName() + ")", e2);
            } catch (KryoException e3) {
                e3.addTrace(cachedProperty + " (" + cls.getName() + ")");
                throw e3;
            } catch (RuntimeException e4) {
                KryoException kryoException = new KryoException(e4);
                kryoException.addTrace(cachedProperty + " (" + cls.getName() + ")");
                throw kryoException;
            }
        }
    }
}
