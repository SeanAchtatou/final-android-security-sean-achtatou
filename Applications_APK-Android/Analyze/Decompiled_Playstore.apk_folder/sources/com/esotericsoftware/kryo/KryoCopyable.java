package com.esotericsoftware.kryo;

public interface KryoCopyable {
    Object copy(Kryo kryo);
}
