package com.esotericsoftware.kryo;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public abstract class Serializer {
    private boolean acceptsNull;
    private boolean immutable;

    public Serializer() {
    }

    public Serializer(boolean z) {
        this.acceptsNull = z;
    }

    public Serializer(boolean z, boolean z2) {
        this.acceptsNull = z;
        this.immutable = z2;
    }

    public Object copy(Kryo kryo, Object obj) {
        if (this.immutable) {
            return obj;
        }
        throw new KryoException("Serializer does not support copy: " + getClass().getName());
    }

    public boolean getAcceptsNull() {
        return this.acceptsNull;
    }

    public boolean isImmutable() {
        return this.immutable;
    }

    public abstract Object read(Kryo kryo, Input input, Class cls);

    public void setAcceptsNull(boolean z) {
        this.acceptsNull = z;
    }

    public void setGenerics(Kryo kryo, Class[] clsArr) {
    }

    public void setImmutable(boolean z) {
        this.immutable = z;
    }

    public abstract void write(Kryo kryo, Output output, Object obj);
}
