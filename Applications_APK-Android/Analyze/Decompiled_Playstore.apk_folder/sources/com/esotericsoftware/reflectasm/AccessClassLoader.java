package com.esotericsoftware.reflectasm;

import java.lang.reflect.Method;
import java.util.ArrayList;

class AccessClassLoader extends ClassLoader {
    private static final ArrayList accessClassLoaders = new ArrayList();

    private AccessClassLoader(ClassLoader classLoader) {
        super(classLoader);
    }

    static AccessClassLoader get(Class cls) {
        AccessClassLoader accessClassLoader;
        ClassLoader classLoader = cls.getClassLoader();
        synchronized (accessClassLoaders) {
            int size = accessClassLoaders.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    accessClassLoader = new AccessClassLoader(classLoader);
                    accessClassLoaders.add(accessClassLoader);
                    break;
                }
                accessClassLoader = (AccessClassLoader) accessClassLoaders.get(i);
                if (accessClassLoader.getParent() == classLoader) {
                    break;
                }
                i++;
            }
        }
        return accessClassLoader;
    }

    /* access modifiers changed from: package-private */
    public Class defineClass(String str, byte[] bArr) {
        Class<ClassLoader> cls = ClassLoader.class;
        try {
            Method declaredMethod = cls.getDeclaredMethod("defineClass", String.class, byte[].class, Integer.TYPE, Integer.TYPE);
            declaredMethod.setAccessible(true);
            return (Class) declaredMethod.invoke(getParent(), str, bArr, 0, Integer.valueOf(bArr.length));
        } catch (Exception e) {
            return defineClass(str, bArr, 0, bArr.length);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized Class loadClass(String str, boolean z) {
        return str.equals(FieldAccess.class.getName()) ? FieldAccess.class : str.equals(MethodAccess.class.getName()) ? MethodAccess.class : str.equals(ConstructorAccess.class.getName()) ? ConstructorAccess.class : super.loadClass(str, z);
    }
}
