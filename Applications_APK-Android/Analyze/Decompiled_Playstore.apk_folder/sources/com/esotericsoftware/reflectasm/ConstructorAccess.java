package com.esotericsoftware.reflectasm;

import b.a.a.g;
import b.a.a.p;
import java.lang.reflect.Modifier;

public abstract class ConstructorAccess {
    boolean isNonStaticMemberClass;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static ConstructorAccess get(Class cls) {
        String str;
        Class<?> defineClass;
        Class<?> enclosingClass = cls.getEnclosingClass();
        boolean z = enclosingClass != null && cls.isMemberClass() && !Modifier.isStatic(cls.getModifiers());
        String name = cls.getName();
        String str2 = name + "ConstructorAccess";
        String str3 = str2.startsWith("java.") ? "reflectasm." + str2 : str2;
        AccessClassLoader accessClassLoader = AccessClassLoader.get(cls);
        synchronized (accessClassLoader) {
            try {
                defineClass = accessClassLoader.loadClass(str3);
            } catch (Exception e) {
                throw new RuntimeException("Non-static member class cannot be created (missing enclosing class constructor): " + cls.getName());
            } catch (Exception e2) {
                throw new RuntimeException("Class cannot be created (missing no-arg constructor): " + cls.getName());
            } catch (ClassNotFoundException e3) {
                String replace = str3.replace('.', '/');
                String replace2 = name.replace('.', '/');
                if (!z) {
                    cls.getConstructor(null);
                    str = null;
                } else {
                    String replace3 = enclosingClass.getName().replace('.', '/');
                    cls.getConstructor(enclosingClass);
                    str = replace3;
                }
                g gVar = new g(0);
                gVar.a(196653, 33, replace, null, "com/esotericsoftware/reflectasm/ConstructorAccess", null);
                insertConstructor(gVar);
                insertNewInstance(gVar, replace2);
                insertNewInstanceInner(gVar, replace2, str);
                gVar.a();
                defineClass = accessClassLoader.defineClass(str3, gVar.b());
            }
        }
        try {
            ConstructorAccess constructorAccess = (ConstructorAccess) defineClass.newInstance();
            constructorAccess.isNonStaticMemberClass = z;
            return constructorAccess;
        } catch (Exception e4) {
            throw new RuntimeException("Error constructing constructor access class: " + str3, e4);
        }
    }

    private static void insertConstructor(g gVar) {
        p a2 = gVar.a(1, "<init>", "()V", (String) null, (String[]) null);
        a2.b();
        a2.b(25, 0);
        a2.b(183, "com/esotericsoftware/reflectasm/ConstructorAccess", "<init>", "()V");
        a2.a(177);
        a2.d(1, 1);
        a2.c();
    }

    static void insertNewInstance(g gVar, String str) {
        p a2 = gVar.a(1, "newInstance", "()Ljava/lang/Object;", (String) null, (String[]) null);
        a2.b();
        a2.a(187, str);
        a2.a(89);
        a2.b(183, str, "<init>", "()V");
        a2.a(176);
        a2.d(2, 1);
        a2.c();
    }

    static void insertNewInstanceInner(g gVar, String str, String str2) {
        p a2 = gVar.a(1, "newInstance", "(Ljava/lang/Object;)Ljava/lang/Object;", (String) null, (String[]) null);
        a2.b();
        if (str2 != null) {
            a2.a(187, str);
            a2.a(89);
            a2.b(25, 1);
            a2.a(192, str2);
            a2.a(89);
            a2.b(182, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
            a2.a(87);
            a2.b(183, str, "<init>", "(L" + str2 + ";)V");
            a2.a(176);
            a2.d(4, 2);
        } else {
            a2.a(187, "java/lang/UnsupportedOperationException");
            a2.a(89);
            a2.a("Not an inner class.");
            a2.b(183, "java/lang/UnsupportedOperationException", "<init>", "(Ljava/lang/String;)V");
            a2.a(191);
            a2.d(3, 2);
        }
        a2.c();
    }

    public boolean isNonStaticMemberClass() {
        return this.isNonStaticMemberClass;
    }

    public abstract Object newInstance();

    public abstract Object newInstance(Object obj);
}
