package com.esotericsoftware.reflectasm;

import b.a.a.g;
import b.a.a.o;
import b.a.a.p;
import b.a.a.s;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public abstract class FieldAccess {
    private String[] fieldNames;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static FieldAccess get(Class cls) {
        Class<?> defineClass;
        ArrayList arrayList = new ArrayList();
        for (Class cls2 = cls; cls2 != Object.class; cls2 = cls2.getSuperclass()) {
            for (Field field : cls2.getDeclaredFields()) {
                int modifiers = field.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers)) {
                    arrayList.add(field);
                }
            }
        }
        String[] strArr = new String[arrayList.size()];
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            strArr[i] = ((Field) arrayList.get(i)).getName();
        }
        String name = cls.getName();
        String str = name + "FieldAccess";
        String str2 = str.startsWith("java.") ? "reflectasm." + str : str;
        AccessClassLoader accessClassLoader = AccessClassLoader.get(cls);
        synchronized (accessClassLoader) {
            try {
                defineClass = accessClassLoader.loadClass(str2);
            } catch (ClassNotFoundException e) {
                String replace = str2.replace('.', '/');
                String replace2 = name.replace('.', '/');
                g gVar = new g(0);
                gVar.a(196653, 33, replace, null, "com/esotericsoftware/reflectasm/FieldAccess", null);
                insertConstructor(gVar);
                insertGetObject(gVar, replace2, arrayList);
                insertSetObject(gVar, replace2, arrayList);
                insertGetPrimitive(gVar, replace2, arrayList, s.f208b);
                insertSetPrimitive(gVar, replace2, arrayList, s.f208b);
                insertGetPrimitive(gVar, replace2, arrayList, s.d);
                insertSetPrimitive(gVar, replace2, arrayList, s.d);
                insertGetPrimitive(gVar, replace2, arrayList, s.e);
                insertSetPrimitive(gVar, replace2, arrayList, s.e);
                insertGetPrimitive(gVar, replace2, arrayList, s.f);
                insertSetPrimitive(gVar, replace2, arrayList, s.f);
                insertGetPrimitive(gVar, replace2, arrayList, s.h);
                insertSetPrimitive(gVar, replace2, arrayList, s.h);
                insertGetPrimitive(gVar, replace2, arrayList, s.i);
                insertSetPrimitive(gVar, replace2, arrayList, s.i);
                insertGetPrimitive(gVar, replace2, arrayList, s.g);
                insertSetPrimitive(gVar, replace2, arrayList, s.g);
                insertGetPrimitive(gVar, replace2, arrayList, s.c);
                insertSetPrimitive(gVar, replace2, arrayList, s.c);
                insertGetString(gVar, replace2, arrayList);
                gVar.a();
                defineClass = accessClassLoader.defineClass(str2, gVar.b());
            }
        }
        try {
            FieldAccess fieldAccess = (FieldAccess) defineClass.newInstance();
            fieldAccess.fieldNames = strArr;
            return fieldAccess;
        } catch (Exception e2) {
            throw new RuntimeException("Error constructing field access class: " + str2, e2);
        }
    }

    private static void insertConstructor(g gVar) {
        p a2 = gVar.a(1, "<init>", "()V", (String) null, (String[]) null);
        a2.b();
        a2.b(25, 0);
        a2.b(183, "com/esotericsoftware/reflectasm/FieldAccess", "<init>", "()V");
        a2.a(177);
        a2.d(1, 1);
        a2.c();
    }

    private static void insertGetObject(g gVar, String str, ArrayList arrayList) {
        int i;
        p a2 = gVar.a(1, "get", "(Ljava/lang/Object;I)Ljava/lang/Object;", (String) null, (String[]) null);
        a2.b();
        a2.b(21, 2);
        if (!arrayList.isEmpty()) {
            o[] oVarArr = new o[arrayList.size()];
            int length = oVarArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                oVarArr[i2] = new o();
            }
            o oVar = new o();
            a2.a(0, oVarArr.length - 1, oVar, oVarArr);
            int length2 = oVarArr.length;
            for (int i3 = 0; i3 < length2; i3++) {
                Field field = (Field) arrayList.get(i3);
                a2.a(oVarArr[i3]);
                a2.a(3, 0, null, 0, null);
                a2.b(25, 1);
                a2.a(192, str);
                a2.a(180, str, field.getName(), s.b(field.getType()));
                switch (s.a(field.getType()).a()) {
                    case 1:
                        a2.b(184, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
                        break;
                    case 2:
                        a2.b(184, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
                        break;
                    case 3:
                        a2.b(184, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
                        break;
                    case 4:
                        a2.b(184, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
                        break;
                    case 5:
                        a2.b(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                        break;
                    case 6:
                        a2.b(184, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
                        break;
                    case 7:
                        a2.b(184, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
                        break;
                    case 8:
                        a2.b(184, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                        break;
                }
                a2.a(176);
            }
            a2.a(oVar);
            a2.a(3, 0, null, 0, null);
            i = 5;
        } else {
            i = 6;
        }
        insertThrowExceptionForFieldNotFound(a2);
        a2.d(i, 3);
        a2.c();
    }

    private static void insertGetPrimitive(g gVar, String str, ArrayList arrayList, s sVar) {
        String str2;
        int i;
        int i2;
        boolean z;
        String f = sVar.f();
        switch (sVar.a()) {
            case 1:
                str2 = "getBoolean";
                i = 172;
                break;
            case 2:
                str2 = "getChar";
                i = 172;
                break;
            case 3:
                str2 = "getByte";
                i = 172;
                break;
            case 4:
                str2 = "getShort";
                i = 172;
                break;
            case 5:
                str2 = "getInt";
                i = 172;
                break;
            case 6:
                str2 = "getFloat";
                i = 174;
                break;
            case 7:
                str2 = "getLong";
                i = 173;
                break;
            case 8:
                str2 = "getDouble";
                i = 175;
                break;
            default:
                str2 = "get";
                i = 176;
                break;
        }
        p a2 = gVar.a(1, str2, "(Ljava/lang/Object;I)" + f, (String) null, (String[]) null);
        a2.b();
        a2.b(21, 2);
        if (!arrayList.isEmpty()) {
            o[] oVarArr = new o[arrayList.size()];
            o oVar = new o();
            boolean z2 = false;
            int length = oVarArr.length;
            int i3 = 0;
            while (i3 < length) {
                if (s.a(((Field) arrayList.get(i3)).getType()).equals(sVar)) {
                    oVarArr[i3] = new o();
                    z = z2;
                } else {
                    oVarArr[i3] = oVar;
                    z = true;
                }
                i3++;
                z2 = z;
            }
            o oVar2 = new o();
            a2.a(0, oVarArr.length - 1, oVar2, oVarArr);
            int i4 = 0;
            int length2 = oVarArr.length;
            while (true) {
                int i5 = i4;
                if (i5 < length2) {
                    Field field = (Field) arrayList.get(i5);
                    if (!oVarArr[i5].equals(oVar)) {
                        a2.a(oVarArr[i5]);
                        a2.a(3, 0, null, 0, null);
                        a2.b(25, 1);
                        a2.a(192, str);
                        a2.a(180, str, field.getName(), f);
                        a2.a(i);
                    }
                    i4 = i5 + 1;
                } else {
                    if (z2) {
                        a2.a(oVar);
                        a2.a(3, 0, null, 0, null);
                        insertThrowExceptionForFieldType(a2, sVar.d());
                    }
                    a2.a(oVar2);
                    a2.a(3, 0, null, 0, null);
                    i2 = 5;
                }
            }
        } else {
            i2 = 6;
        }
        p insertThrowExceptionForFieldNotFound = insertThrowExceptionForFieldNotFound(a2);
        insertThrowExceptionForFieldNotFound.d(i2, 3);
        insertThrowExceptionForFieldNotFound.c();
    }

    private static void insertGetString(g gVar, String str, ArrayList arrayList) {
        int i;
        boolean z;
        p a2 = gVar.a(1, "getString", "(Ljava/lang/Object;I)Ljava/lang/String;", (String) null, (String[]) null);
        a2.b();
        a2.b(21, 2);
        if (!arrayList.isEmpty()) {
            o[] oVarArr = new o[arrayList.size()];
            o oVar = new o();
            boolean z2 = false;
            int length = oVarArr.length;
            int i2 = 0;
            while (i2 < length) {
                if (((Field) arrayList.get(i2)).getType().equals(String.class)) {
                    oVarArr[i2] = new o();
                    z = z2;
                } else {
                    oVarArr[i2] = oVar;
                    z = true;
                }
                i2++;
                z2 = z;
            }
            o oVar2 = new o();
            a2.a(0, oVarArr.length - 1, oVar2, oVarArr);
            int length2 = oVarArr.length;
            for (int i3 = 0; i3 < length2; i3++) {
                if (!oVarArr[i3].equals(oVar)) {
                    a2.a(oVarArr[i3]);
                    a2.a(3, 0, null, 0, null);
                    a2.b(25, 1);
                    a2.a(192, str);
                    a2.a(180, str, ((Field) arrayList.get(i3)).getName(), "Ljava/lang/String;");
                    a2.a(176);
                }
            }
            if (z2) {
                a2.a(oVar);
                a2.a(3, 0, null, 0, null);
                insertThrowExceptionForFieldType(a2, "String");
            }
            a2.a(oVar2);
            a2.a(3, 0, null, 0, null);
            i = 5;
        } else {
            i = 6;
        }
        insertThrowExceptionForFieldNotFound(a2);
        a2.d(i, 3);
        a2.c();
    }

    private static void insertSetObject(g gVar, String str, ArrayList arrayList) {
        int i;
        p a2 = gVar.a(1, "set", "(Ljava/lang/Object;ILjava/lang/Object;)V", (String) null, (String[]) null);
        a2.b();
        a2.b(21, 2);
        if (!arrayList.isEmpty()) {
            o[] oVarArr = new o[arrayList.size()];
            int length = oVarArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                oVarArr[i2] = new o();
            }
            o oVar = new o();
            a2.a(0, oVarArr.length - 1, oVar, oVarArr);
            int length2 = oVarArr.length;
            for (int i3 = 0; i3 < length2; i3++) {
                Field field = (Field) arrayList.get(i3);
                s a3 = s.a(field.getType());
                a2.a(oVarArr[i3]);
                a2.a(3, 0, null, 0, null);
                a2.b(25, 1);
                a2.a(192, str);
                a2.b(25, 3);
                switch (a3.a()) {
                    case 1:
                        a2.a(192, "java/lang/Boolean");
                        a2.b(182, "java/lang/Boolean", "booleanValue", "()Z");
                        break;
                    case 2:
                        a2.a(192, "java/lang/Character");
                        a2.b(182, "java/lang/Character", "charValue", "()C");
                        break;
                    case 3:
                        a2.a(192, "java/lang/Byte");
                        a2.b(182, "java/lang/Byte", "byteValue", "()B");
                        break;
                    case 4:
                        a2.a(192, "java/lang/Short");
                        a2.b(182, "java/lang/Short", "shortValue", "()S");
                        break;
                    case 5:
                        a2.a(192, "java/lang/Integer");
                        a2.b(182, "java/lang/Integer", "intValue", "()I");
                        break;
                    case 6:
                        a2.a(192, "java/lang/Float");
                        a2.b(182, "java/lang/Float", "floatValue", "()F");
                        break;
                    case 7:
                        a2.a(192, "java/lang/Long");
                        a2.b(182, "java/lang/Long", "longValue", "()J");
                        break;
                    case 8:
                        a2.a(192, "java/lang/Double");
                        a2.b(182, "java/lang/Double", "doubleValue", "()D");
                        break;
                    case 9:
                        a2.a(192, a3.f());
                        break;
                    case 10:
                        a2.a(192, a3.e());
                        break;
                }
                a2.a(181, str, field.getName(), a3.f());
                a2.a(177);
            }
            a2.a(oVar);
            a2.a(3, 0, null, 0, null);
            i = 5;
        } else {
            i = 6;
        }
        p insertThrowExceptionForFieldNotFound = insertThrowExceptionForFieldNotFound(a2);
        insertThrowExceptionForFieldNotFound.d(i, 4);
        insertThrowExceptionForFieldNotFound.c();
    }

    private static void insertSetPrimitive(g gVar, String str, ArrayList arrayList, s sVar) {
        String str2;
        int i;
        int i2;
        int i3;
        boolean z;
        String f = sVar.f();
        switch (sVar.a()) {
            case 1:
                str2 = "setBoolean";
                i = 21;
                i2 = 4;
                break;
            case 2:
                str2 = "setChar";
                i = 21;
                i2 = 4;
                break;
            case 3:
                str2 = "setByte";
                i = 21;
                i2 = 4;
                break;
            case 4:
                str2 = "setShort";
                i = 21;
                i2 = 4;
                break;
            case 5:
                str2 = "setInt";
                i = 21;
                i2 = 4;
                break;
            case 6:
                str2 = "setFloat";
                i = 23;
                i2 = 4;
                break;
            case 7:
                str2 = "setLong";
                i = 22;
                i2 = 5;
                break;
            case 8:
                str2 = "setDouble";
                i = 24;
                i2 = 5;
                break;
            default:
                str2 = "set";
                i = 25;
                i2 = 4;
                break;
        }
        p a2 = gVar.a(1, str2, "(Ljava/lang/Object;I" + f + ")V", (String) null, (String[]) null);
        a2.b();
        a2.b(21, 2);
        if (!arrayList.isEmpty()) {
            o[] oVarArr = new o[arrayList.size()];
            o oVar = new o();
            boolean z2 = false;
            int length = oVarArr.length;
            int i4 = 0;
            while (i4 < length) {
                if (s.a(((Field) arrayList.get(i4)).getType()).equals(sVar)) {
                    oVarArr[i4] = new o();
                    z = z2;
                } else {
                    oVarArr[i4] = oVar;
                    z = true;
                }
                i4++;
                z2 = z;
            }
            o oVar2 = new o();
            a2.a(0, oVarArr.length - 1, oVar2, oVarArr);
            int i5 = 0;
            int length2 = oVarArr.length;
            while (true) {
                int i6 = i5;
                if (i6 < length2) {
                    if (!oVarArr[i6].equals(oVar)) {
                        a2.a(oVarArr[i6]);
                        a2.a(3, 0, null, 0, null);
                        a2.b(25, 1);
                        a2.a(192, str);
                        a2.b(i, 3);
                        a2.a(181, str, ((Field) arrayList.get(i6)).getName(), f);
                        a2.a(177);
                    }
                    i5 = i6 + 1;
                } else {
                    if (z2) {
                        a2.a(oVar);
                        a2.a(3, 0, null, 0, null);
                        insertThrowExceptionForFieldType(a2, sVar.d());
                    }
                    a2.a(oVar2);
                    a2.a(3, 0, null, 0, null);
                    i3 = 5;
                }
            }
        } else {
            i3 = 6;
        }
        p insertThrowExceptionForFieldNotFound = insertThrowExceptionForFieldNotFound(a2);
        insertThrowExceptionForFieldNotFound.d(i3, i2);
        insertThrowExceptionForFieldNotFound.c();
    }

    private static p insertThrowExceptionForFieldNotFound(p pVar) {
        pVar.a(187, "java/lang/IllegalArgumentException");
        pVar.a(89);
        pVar.a(187, "java/lang/StringBuilder");
        pVar.a(89);
        pVar.a("Field not found: ");
        pVar.b(183, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V");
        pVar.b(21, 2);
        pVar.b(182, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
        pVar.b(182, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
        pVar.b(183, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
        pVar.a(191);
        return pVar;
    }

    private static p insertThrowExceptionForFieldType(p pVar, String str) {
        pVar.a(187, "java/lang/IllegalArgumentException");
        pVar.a(89);
        pVar.a(187, "java/lang/StringBuilder");
        pVar.a(89);
        pVar.a("Field not declared as " + str + ": ");
        pVar.b(183, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V");
        pVar.b(21, 2);
        pVar.b(182, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
        pVar.b(182, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
        pVar.b(183, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
        pVar.a(191);
        return pVar;
    }

    public abstract Object get(Object obj, int i);

    public Object get(Object obj, String str) {
        return get(obj, getIndex(str));
    }

    public abstract boolean getBoolean(Object obj, int i);

    public abstract byte getByte(Object obj, int i);

    public abstract char getChar(Object obj, int i);

    public abstract double getDouble(Object obj, int i);

    public String[] getFieldNames() {
        return this.fieldNames;
    }

    public abstract float getFloat(Object obj, int i);

    public int getIndex(String str) {
        int length = this.fieldNames.length;
        for (int i = 0; i < length; i++) {
            if (this.fieldNames[i].equals(str)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Unable to find public field: " + str);
    }

    public abstract int getInt(Object obj, int i);

    public abstract long getLong(Object obj, int i);

    public abstract short getShort(Object obj, int i);

    public abstract String getString(Object obj, int i);

    public abstract void set(Object obj, int i, Object obj2);

    public void set(Object obj, String str, Object obj2) {
        set(obj, getIndex(str), obj2);
    }

    public abstract void setBoolean(Object obj, int i, boolean z);

    public abstract void setByte(Object obj, int i, byte b2);

    public abstract void setChar(Object obj, int i, char c);

    public abstract void setDouble(Object obj, int i, double d);

    public abstract void setFloat(Object obj, int i, float f);

    public abstract void setInt(Object obj, int i, int i2);

    public abstract void setLong(Object obj, int i, long j);

    public abstract void setShort(Object obj, int i, short s);
}
