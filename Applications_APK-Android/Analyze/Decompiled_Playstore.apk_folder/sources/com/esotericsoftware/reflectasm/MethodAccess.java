package com.esotericsoftware.reflectasm;

import b.a.a.g;
import b.a.a.o;
import b.a.a.p;
import b.a.a.s;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class MethodAccess {
    private String[] methodNames;
    private Class[][] parameterTypes;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static MethodAccess get(Class cls) {
        Class<?> defineClass;
        ArrayList arrayList = new ArrayList();
        for (Class cls2 = cls; cls2 != Object.class; cls2 = cls2.getSuperclass()) {
            for (Method method : cls2.getDeclaredMethods()) {
                int modifiers = method.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers)) {
                    arrayList.add(method);
                }
            }
        }
        Class[][] clsArr = new Class[arrayList.size()][];
        String[] strArr = new String[arrayList.size()];
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            Method method2 = (Method) arrayList.get(i);
            strArr[i] = method2.getName();
            clsArr[i] = method2.getParameterTypes();
        }
        String name = cls.getName();
        String str = name + "MethodAccess";
        String str2 = str.startsWith("java.") ? "reflectasm." + str : str;
        AccessClassLoader accessClassLoader = AccessClassLoader.get(cls);
        synchronized (accessClassLoader) {
            try {
                defineClass = accessClassLoader.loadClass(str2);
            } catch (ClassNotFoundException e) {
                String replace = str2.replace('.', '/');
                String replace2 = name.replace('.', '/');
                g gVar = new g(1);
                gVar.a(196653, 33, replace, null, "com/esotericsoftware/reflectasm/MethodAccess", null);
                p a2 = gVar.a(1, "<init>", "()V", (String) null, (String[]) null);
                a2.b();
                a2.b(25, 0);
                a2.b(183, "com/esotericsoftware/reflectasm/MethodAccess", "<init>", "()V");
                a2.a(177);
                a2.d(0, 0);
                a2.c();
                p a3 = gVar.a(129, "invoke", "(Ljava/lang/Object;I[Ljava/lang/Object;)Ljava/lang/Object;", (String) null, (String[]) null);
                a3.b();
                if (!arrayList.isEmpty()) {
                    a3.b(25, 1);
                    a3.a(192, replace2);
                    a3.b(58, 4);
                    a3.b(21, 2);
                    o[] oVarArr = new o[arrayList.size()];
                    int length2 = oVarArr.length;
                    for (int i2 = 0; i2 < length2; i2++) {
                        oVarArr[i2] = new o();
                    }
                    o oVar = new o();
                    a3.a(0, oVarArr.length - 1, oVar, oVarArr);
                    StringBuilder sb = new StringBuilder(128);
                    int length3 = oVarArr.length;
                    for (int i3 = 0; i3 < length3; i3++) {
                        a3.a(oVarArr[i3]);
                        if (i3 == 0) {
                            a3.a(1, 1, new Object[]{replace2}, 0, null);
                        } else {
                            a3.a(3, 0, null, 0, null);
                        }
                        a3.b(25, 4);
                        sb.setLength(0);
                        sb.append('(');
                        Method method3 = (Method) arrayList.get(i3);
                        Class<?>[] parameterTypes2 = method3.getParameterTypes();
                        for (int i4 = 0; i4 < parameterTypes2.length; i4++) {
                            a3.b(25, 3);
                            a3.a(16, i4);
                            a3.a(50);
                            s a4 = s.a(parameterTypes2[i4]);
                            switch (a4.a()) {
                                case 1:
                                    a3.a(192, "java/lang/Boolean");
                                    a3.b(182, "java/lang/Boolean", "booleanValue", "()Z");
                                    break;
                                case 2:
                                    a3.a(192, "java/lang/Character");
                                    a3.b(182, "java/lang/Character", "charValue", "()C");
                                    break;
                                case 3:
                                    a3.a(192, "java/lang/Byte");
                                    a3.b(182, "java/lang/Byte", "byteValue", "()B");
                                    break;
                                case 4:
                                    a3.a(192, "java/lang/Short");
                                    a3.b(182, "java/lang/Short", "shortValue", "()S");
                                    break;
                                case 5:
                                    a3.a(192, "java/lang/Integer");
                                    a3.b(182, "java/lang/Integer", "intValue", "()I");
                                    break;
                                case 6:
                                    a3.a(192, "java/lang/Float");
                                    a3.b(182, "java/lang/Float", "floatValue", "()F");
                                    break;
                                case 7:
                                    a3.a(192, "java/lang/Long");
                                    a3.b(182, "java/lang/Long", "longValue", "()J");
                                    break;
                                case 8:
                                    a3.a(192, "java/lang/Double");
                                    a3.b(182, "java/lang/Double", "doubleValue", "()D");
                                    break;
                                case 9:
                                    a3.a(192, a4.f());
                                    break;
                                case 10:
                                    a3.a(192, a4.e());
                                    break;
                            }
                            sb.append(a4.f());
                        }
                        sb.append(')');
                        sb.append(s.b(method3.getReturnType()));
                        a3.b(182, replace2, method3.getName(), sb.toString());
                        switch (s.a(method3.getReturnType()).a()) {
                            case 0:
                                a3.a(1);
                                break;
                            case 1:
                                a3.b(184, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
                                break;
                            case 2:
                                a3.b(184, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
                                break;
                            case 3:
                                a3.b(184, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
                                break;
                            case 4:
                                a3.b(184, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
                                break;
                            case 5:
                                a3.b(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                                break;
                            case 6:
                                a3.b(184, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
                                break;
                            case 7:
                                a3.b(184, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
                                break;
                            case 8:
                                a3.b(184, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                                break;
                        }
                        a3.a(176);
                    }
                    a3.a(oVar);
                    a3.a(3, 0, null, 0, null);
                }
                a3.a(187, "java/lang/IllegalArgumentException");
                a3.a(89);
                a3.a(187, "java/lang/StringBuilder");
                a3.a(89);
                a3.a("Method not found: ");
                a3.b(183, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V");
                a3.b(21, 2);
                a3.b(182, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
                a3.b(182, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
                a3.b(183, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
                a3.a(191);
                a3.d(0, 0);
                a3.c();
                gVar.a();
                defineClass = accessClassLoader.defineClass(str2, gVar.b());
            }
        }
        try {
            MethodAccess methodAccess = (MethodAccess) defineClass.newInstance();
            methodAccess.methodNames = strArr;
            methodAccess.parameterTypes = clsArr;
            return methodAccess;
        } catch (Exception e2) {
            throw new RuntimeException("Error constructing method access class: " + str2, e2);
        }
    }

    public int getIndex(String str) {
        int length = this.methodNames.length;
        for (int i = 0; i < length; i++) {
            if (this.methodNames[i].equals(str)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Unable to find public method: " + str);
    }

    public int getIndex(String str, Class... clsArr) {
        int length = this.methodNames.length;
        for (int i = 0; i < length; i++) {
            if (this.methodNames[i].equals(str) && Arrays.equals(clsArr, this.parameterTypes[i])) {
                return i;
            }
        }
        throw new IllegalArgumentException("Unable to find public method: " + str + " " + Arrays.toString(this.parameterTypes));
    }

    public String[] getMethodNames() {
        return this.methodNames;
    }

    public Class[][] getParameterTypes() {
        return this.parameterTypes;
    }

    public abstract Object invoke(Object obj, int i, Object... objArr);

    public Object invoke(Object obj, String str, Object... objArr) {
        return invoke(obj, getIndex(str), objArr);
    }
}
