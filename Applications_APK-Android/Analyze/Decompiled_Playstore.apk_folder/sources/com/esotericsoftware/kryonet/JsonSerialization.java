package com.esotericsoftware.kryonet;

import com.esotericsoftware.a.a;
import com.esotericsoftware.a.e;
import com.esotericsoftware.kryo.io.ByteBufferInputStream;
import com.esotericsoftware.kryo.io.ByteBufferOutputStream;
import com.esotericsoftware.kryonet.FrameworkMessage;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.ByteBuffer;

public class JsonSerialization implements Serialization {
    private final ByteBufferInputStream byteBufferInputStream = new ByteBufferInputStream();
    private final ByteBufferOutputStream byteBufferOutputStream = new ByteBufferOutputStream();
    private final a json = new a();
    private byte[] logBuffer = new byte[0];
    private boolean logging = true;
    private boolean prettyPrint = true;
    private final OutputStreamWriter writer = new OutputStreamWriter(this.byteBufferOutputStream);

    public JsonSerialization() {
        this.json.a("RegisterTCP", FrameworkMessage.RegisterTCP.class);
        this.json.a("RegisterUDP", FrameworkMessage.RegisterUDP.class);
        this.json.a("KeepAlive", FrameworkMessage.KeepAlive.class);
        this.json.a("DiscoverHost", FrameworkMessage.DiscoverHost.class);
        this.json.a("Ping", FrameworkMessage.Ping.class);
        this.json.a((Writer) this.writer);
    }

    public int getLengthLength() {
        return 4;
    }

    public Object read(Connection connection, ByteBuffer byteBuffer) {
        this.byteBufferInputStream.setByteBuffer(byteBuffer);
        return this.json.a(Object.class, this.byteBufferInputStream);
    }

    public int readLength(ByteBuffer byteBuffer) {
        return byteBuffer.getInt();
    }

    public void setLogging(boolean z, boolean z2) {
        this.logging = z;
        this.prettyPrint = z2;
    }

    public void write(Connection connection, ByteBuffer byteBuffer, Object obj) {
        this.byteBufferOutputStream.setByteBuffer(byteBuffer);
        byteBuffer.position();
        try {
            this.json.a(obj, Object.class, (Class) null);
            this.writer.flush();
        } catch (Exception e) {
            throw new e("Error writing object: " + obj, e);
        }
    }

    public void writeLength(ByteBuffer byteBuffer, int i) {
        byteBuffer.putInt(i);
    }
}
