package com.esotericsoftware.kryonet;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

class TcpConnection {
    private static final int IPTOS_LOWDELAY = 16;
    boolean bufferPositionFix;
    private int currentObjectLength;
    float idleThreshold = 0.1f;
    int keepAliveMillis = 8000;
    private volatile long lastReadTime;
    private volatile long lastWriteTime;
    final ByteBuffer readBuffer;
    private SelectionKey selectionKey;
    final Serialization serialization;
    SocketChannel socketChannel;
    int timeoutMillis = 12000;
    final ByteBuffer writeBuffer;
    private final Object writeLock = new Object();

    public TcpConnection(Serialization serialization2, int i, int i2) {
        this.serialization = serialization2;
        this.writeBuffer = ByteBuffer.allocate(i);
        this.readBuffer = ByteBuffer.allocate(i2);
        this.readBuffer.flip();
    }

    private boolean writeToSocket() {
        SocketChannel socketChannel2 = this.socketChannel;
        if (socketChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        ByteBuffer byteBuffer = this.writeBuffer;
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            if (this.bufferPositionFix) {
                byteBuffer.compact();
                byteBuffer.flip();
            }
            if (socketChannel2.write(byteBuffer) == 0) {
                break;
            }
        }
        byteBuffer.compact();
        return byteBuffer.position() == 0;
    }

    public SelectionKey accept(Selector selector, SocketChannel socketChannel2) {
        this.writeBuffer.clear();
        this.readBuffer.clear();
        this.readBuffer.flip();
        this.currentObjectLength = 0;
        try {
            this.socketChannel = socketChannel2;
            socketChannel2.configureBlocking(false);
            socketChannel2.socket().setTcpNoDelay(true);
            this.selectionKey = socketChannel2.register(selector, 1);
            long currentTimeMillis = System.currentTimeMillis();
            this.lastWriteTime = currentTimeMillis;
            this.lastReadTime = currentTimeMillis;
            return this.selectionKey;
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public void close() {
        try {
            if (this.socketChannel != null) {
                this.socketChannel.close();
                this.socketChannel = null;
                if (this.selectionKey != null) {
                    this.selectionKey.selector().wakeup();
                }
            }
        } catch (IOException e) {
        }
    }

    public void connect(Selector selector, SocketAddress socketAddress, int i) {
        close();
        this.writeBuffer.clear();
        this.readBuffer.clear();
        this.readBuffer.flip();
        this.currentObjectLength = 0;
        try {
            SocketChannel openSocketChannel = selector.provider().openSocketChannel();
            Socket socket = openSocketChannel.socket();
            socket.setTcpNoDelay(true);
            socket.connect(socketAddress, i);
            openSocketChannel.configureBlocking(false);
            this.socketChannel = openSocketChannel;
            this.selectionKey = openSocketChannel.register(selector, 1);
            this.selectionKey.attach(this);
            long currentTimeMillis = System.currentTimeMillis();
            this.lastWriteTime = currentTimeMillis;
            this.lastReadTime = currentTimeMillis;
        } catch (IOException e) {
            close();
            IOException iOException = new IOException("Unable to connect to: " + socketAddress);
            iOException.initCause(e);
            throw iOException;
        }
    }

    public boolean isTimedOut(long j) {
        return this.socketChannel != null && this.timeoutMillis > 0 && j - this.lastReadTime > ((long) this.timeoutMillis);
    }

    public boolean needsKeepAlive(long j) {
        return this.socketChannel != null && this.keepAliveMillis > 0 && j - this.lastWriteTime > ((long) this.keepAliveMillis);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        if (r7.readBuffer.remaining() < r2) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00c7, code lost:
        if (r7.readBuffer.remaining() >= r2) goto L_0x00c9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object readObject(com.esotericsoftware.kryonet.Connection r8) {
        /*
            r7 = this;
            r0 = 0
            r6 = -1
            java.nio.channels.SocketChannel r1 = r7.socketChannel
            if (r1 != 0) goto L_0x000e
            java.net.SocketException r0 = new java.net.SocketException
            java.lang.String r1 = "Connection is closed."
            r0.<init>(r1)
            throw r0
        L_0x000e:
            int r2 = r7.currentObjectLength
            if (r2 != 0) goto L_0x0097
            com.esotericsoftware.kryonet.Serialization r2 = r7.serialization
            int r2 = r2.getLengthLength()
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r3 = r3.remaining()
            if (r3 >= r2) goto L_0x0049
            java.nio.ByteBuffer r3 = r7.readBuffer
            r3.compact()
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r3 = r1.read(r3)
            java.nio.ByteBuffer r4 = r7.readBuffer
            r4.flip()
            if (r3 != r6) goto L_0x003a
            java.net.SocketException r0 = new java.net.SocketException
            java.lang.String r1 = "Connection is closed."
            r0.<init>(r1)
            throw r0
        L_0x003a:
            long r4 = java.lang.System.currentTimeMillis()
            r7.lastReadTime = r4
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r3 = r3.remaining()
            if (r3 >= r2) goto L_0x0049
        L_0x0048:
            return r0
        L_0x0049:
            com.esotericsoftware.kryonet.Serialization r2 = r7.serialization
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r2 = r2.readLength(r3)
            r7.currentObjectLength = r2
            int r2 = r7.currentObjectLength
            if (r2 > 0) goto L_0x0072
            com.esotericsoftware.kryonet.KryoNetException r0 = new com.esotericsoftware.kryonet.KryoNetException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid object length: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r7.currentObjectLength
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0072:
            int r2 = r7.currentObjectLength
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r3 = r3.capacity()
            if (r2 <= r3) goto L_0x0097
            com.esotericsoftware.kryonet.KryoNetException r0 = new com.esotericsoftware.kryonet.KryoNetException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unable to read object larger than read buffer: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r7.currentObjectLength
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0097:
            int r2 = r7.currentObjectLength
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r3 = r3.remaining()
            if (r3 >= r2) goto L_0x00c9
            java.nio.ByteBuffer r3 = r7.readBuffer
            r3.compact()
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r1 = r1.read(r3)
            java.nio.ByteBuffer r3 = r7.readBuffer
            r3.flip()
            if (r1 != r6) goto L_0x00bb
            java.net.SocketException r0 = new java.net.SocketException
            java.lang.String r1 = "Connection is closed."
            r0.<init>(r1)
            throw r0
        L_0x00bb:
            long r4 = java.lang.System.currentTimeMillis()
            r7.lastReadTime = r4
            java.nio.ByteBuffer r1 = r7.readBuffer
            int r1 = r1.remaining()
            if (r1 < r2) goto L_0x0048
        L_0x00c9:
            r0 = 0
            r7.currentObjectLength = r0
            java.nio.ByteBuffer r0 = r7.readBuffer
            int r1 = r0.position()
            java.nio.ByteBuffer r0 = r7.readBuffer
            int r3 = r0.limit()
            java.nio.ByteBuffer r0 = r7.readBuffer
            int r4 = r1 + r2
            r0.limit(r4)
            com.esotericsoftware.kryonet.Serialization r0 = r7.serialization     // Catch:{ Exception -> 0x0120 }
            java.nio.ByteBuffer r4 = r7.readBuffer     // Catch:{ Exception -> 0x0120 }
            java.lang.Object r0 = r0.read(r8, r4)     // Catch:{ Exception -> 0x0120 }
            java.nio.ByteBuffer r4 = r7.readBuffer
            r4.limit(r3)
            java.nio.ByteBuffer r3 = r7.readBuffer
            int r3 = r3.position()
            int r3 = r3 - r1
            if (r3 == r2) goto L_0x0048
            com.esotericsoftware.kryonet.KryoNetException r3 = new com.esotericsoftware.kryonet.KryoNetException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Incorrect number of bytes ("
            java.lang.StringBuilder r4 = r4.append(r5)
            int r1 = r1 + r2
            java.nio.ByteBuffer r2 = r7.readBuffer
            int r2 = r2.position()
            int r1 = r1 - r2
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r2 = " remaining) used to deserialize object: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r3.<init>(r0)
            throw r3
        L_0x0120:
            r0 = move-exception
            com.esotericsoftware.kryonet.KryoNetException r1 = new com.esotericsoftware.kryonet.KryoNetException
            java.lang.String r2 = "Error during deserialization."
            r1.<init>(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.TcpConnection.readObject(com.esotericsoftware.kryonet.Connection):java.lang.Object");
    }

    public int send(Connection connection, Object obj) {
        int i;
        if (this.socketChannel == null) {
            throw new SocketException("Connection is closed.");
        }
        synchronized (this.writeLock) {
            int position = this.writeBuffer.position();
            int lengthLength = this.serialization.getLengthLength();
            this.writeBuffer.position(this.writeBuffer.position() + lengthLength);
            try {
                this.serialization.write(connection, this.writeBuffer, obj);
                int position2 = this.writeBuffer.position();
                this.writeBuffer.position(position);
                this.serialization.writeLength(this.writeBuffer, (position2 - lengthLength) - position);
                this.writeBuffer.position(position2);
                if (position != 0 || writeToSocket()) {
                    this.selectionKey.selector().wakeup();
                } else {
                    this.selectionKey.interestOps(5);
                }
                this.lastWriteTime = System.currentTimeMillis();
                i = position2 - position;
            } catch (KryoNetException e) {
                throw new KryoNetException("Error serializing object of type: " + obj.getClass().getName(), e);
            }
        }
        return i;
    }

    public void writeOperation() {
        synchronized (this.writeLock) {
            if (writeToSocket()) {
                this.selectionKey.interestOps(1);
            }
            this.lastWriteTime = System.currentTimeMillis();
        }
    }
}
