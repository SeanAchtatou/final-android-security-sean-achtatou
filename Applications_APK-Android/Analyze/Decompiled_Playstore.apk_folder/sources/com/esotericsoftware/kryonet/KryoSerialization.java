package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.ByteBufferInputStream;
import com.esotericsoftware.kryo.io.ByteBufferOutputStream;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryonet.FrameworkMessage;
import java.nio.ByteBuffer;

public class KryoSerialization implements Serialization {
    private final ByteBufferInputStream byteBufferInputStream;
    private final ByteBufferOutputStream byteBufferOutputStream;
    private final Input input;
    private final Kryo kryo;
    private final Output output;

    public KryoSerialization() {
        this(new Kryo());
        this.kryo.setReferences(false);
        this.kryo.setRegistrationRequired(true);
    }

    public KryoSerialization(Kryo kryo2) {
        this.byteBufferInputStream = new ByteBufferInputStream();
        this.byteBufferOutputStream = new ByteBufferOutputStream();
        this.kryo = kryo2;
        kryo2.register(FrameworkMessage.RegisterTCP.class);
        kryo2.register(FrameworkMessage.RegisterUDP.class);
        kryo2.register(FrameworkMessage.KeepAlive.class);
        kryo2.register(FrameworkMessage.DiscoverHost.class);
        kryo2.register(FrameworkMessage.Ping.class);
        this.input = new Input(this.byteBufferInputStream, 512);
        this.output = new Output(this.byteBufferOutputStream, 512);
    }

    public Kryo getKryo() {
        return this.kryo;
    }

    public int getLengthLength() {
        return 4;
    }

    public synchronized Object read(Connection connection, ByteBuffer byteBuffer) {
        this.byteBufferInputStream.setByteBuffer(byteBuffer);
        this.input.setInputStream(this.byteBufferInputStream);
        this.kryo.getContext().put("connection", connection);
        return this.kryo.readClassAndObject(this.input);
    }

    public int readLength(ByteBuffer byteBuffer) {
        return byteBuffer.getInt();
    }

    public synchronized void write(Connection connection, ByteBuffer byteBuffer, Object obj) {
        this.byteBufferOutputStream.setByteBuffer(byteBuffer);
        this.kryo.getContext().put("connection", connection);
        this.kryo.writeClassAndObject(this.output, obj);
        this.output.flush();
    }

    public void writeLength(ByteBuffer byteBuffer, int i) {
        byteBuffer.putInt(i);
    }
}
