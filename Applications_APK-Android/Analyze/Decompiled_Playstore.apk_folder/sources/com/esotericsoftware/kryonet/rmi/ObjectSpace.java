package com.esotericsoftware.kryonet.rmi;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryo.util.IntMap;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ObjectSpace {
    static ObjectSpace[] instances = new ObjectSpace[0];
    private static final Object instancesLock = new Object();
    private static final byte kReturnExMask = 64;
    private static final byte kReturnValMask = Byte.MIN_VALUE;
    private static final HashMap methodCache = new HashMap();
    Connection[] connections;
    final Object connectionsLock;
    Executor executor;
    final IntMap idToObject;
    private final Listener invokeListener;

    class CachedMethod {
        Method method;
        Serializer[] serializers;

        CachedMethod() {
        }
    }

    public class InvokeMethod implements KryoSerializable, FrameworkMessage {
        public Object[] args;
        public Method method;
        public int objectID;
        public byte responseID;

        public void read(Kryo kryo, Input input) {
            this.objectID = input.readInt(true);
            Class type = kryo.getRegistration(input.readInt(true)).getType();
            byte readByte = input.readByte();
            try {
                CachedMethod cachedMethod = ObjectSpace.getMethods(kryo, type)[readByte];
                this.method = cachedMethod.method;
                this.args = new Object[cachedMethod.serializers.length];
                int length = this.args.length;
                for (int i = 0; i < length; i++) {
                    Serializer serializer = cachedMethod.serializers[i];
                    if (serializer != null) {
                        this.args[i] = kryo.readObjectOrNull(input, this.method.getParameterTypes()[i], serializer);
                    } else {
                        this.args[i] = kryo.readClassAndObject(input);
                    }
                }
                this.responseID = input.readByte();
            } catch (IndexOutOfBoundsException e) {
                throw new KryoException("Invalid method index " + ((int) readByte) + " for class: " + type.getName());
            }
        }

        public void write(Kryo kryo, Output output) {
            output.writeInt(this.objectID, true);
            output.writeInt(kryo.getRegistration(this.method.getDeclaringClass()).getId(), true);
            CachedMethod[] methods = ObjectSpace.getMethods(kryo, this.method.getDeclaringClass());
            CachedMethod cachedMethod = null;
            int length = methods.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                cachedMethod = methods[i];
                if (cachedMethod.method.equals(this.method)) {
                    output.writeByte(i);
                    break;
                }
                i++;
            }
            int length2 = cachedMethod.serializers.length;
            for (int i2 = 0; i2 < length2; i2++) {
                Serializer serializer = cachedMethod.serializers[i2];
                if (serializer != null) {
                    kryo.writeObjectOrNull(output, this.args[i2], serializer);
                } else {
                    kryo.writeClassAndObject(output, this.args[i2]);
                }
            }
            output.writeByte(this.responseID);
        }
    }

    public class InvokeMethodResult implements FrameworkMessage {
        public int objectID;
        public byte responseID;
        public Object result;
    }

    class RemoteInvocationHandler implements InvocationHandler {
        private final Connection connection;
        private Byte lastResponseID;
        final ReentrantLock lock = new ReentrantLock();
        private byte nextResponseNum = 1;
        private boolean nonBlocking = false;
        final int objectID;
        final Condition responseCondition = this.lock.newCondition();
        private Listener responseListener;
        final ConcurrentHashMap responseTable = new ConcurrentHashMap();
        private int timeoutMillis = 3000;
        private boolean transmitExceptions = true;
        private boolean transmitReturnValue = true;

        public RemoteInvocationHandler(Connection connection2, final int i) {
            this.connection = connection2;
            this.objectID = i;
            this.responseListener = new Listener() {
                public void disconnected(Connection connection) {
                    RemoteInvocationHandler.this.close();
                }

                public void received(Connection connection, Object obj) {
                    if (obj instanceof InvokeMethodResult) {
                        InvokeMethodResult invokeMethodResult = (InvokeMethodResult) obj;
                        if (invokeMethodResult.objectID == i) {
                            RemoteInvocationHandler.this.responseTable.put(Byte.valueOf(invokeMethodResult.responseID), invokeMethodResult);
                            RemoteInvocationHandler.this.lock.lock();
                            try {
                                RemoteInvocationHandler.this.responseCondition.signalAll();
                            } finally {
                                RemoteInvocationHandler.this.lock.unlock();
                            }
                        }
                    }
                }
            };
            connection2.addListener(this.responseListener);
        }

        private Object waitForResponse(byte b2) {
            if (this.connection.getEndPoint().getUpdateThread() == Thread.currentThread()) {
                throw new IllegalStateException("Cannot wait for an RMI response on the connection's update thread.");
            }
            long currentTimeMillis = System.currentTimeMillis() + ((long) this.timeoutMillis);
            while (true) {
                long currentTimeMillis2 = currentTimeMillis - System.currentTimeMillis();
                if (this.responseTable.containsKey(Byte.valueOf(b2))) {
                    this.responseTable.remove(Byte.valueOf(b2));
                    this.lastResponseID = null;
                    return ((InvokeMethodResult) this.responseTable.get(Byte.valueOf(b2))).result;
                } else if (currentTimeMillis2 <= 0) {
                    throw new TimeoutException("Response timed out.");
                } else {
                    this.lock.lock();
                    try {
                        this.responseCondition.await(currentTimeMillis2, TimeUnit.MILLISECONDS);
                        this.lock.unlock();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        throw new RuntimeException(e);
                    } catch (Throwable th) {
                        this.lock.unlock();
                        throw th;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void close() {
            this.connection.removeListener(this.responseListener);
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            byte b2;
            boolean z = true;
            if (method.getDeclaringClass() == RemoteObject.class) {
                String name = method.getName();
                if (name.equals("close")) {
                    close();
                    return null;
                } else if (name.equals("setResponseTimeout")) {
                    this.timeoutMillis = ((Integer) objArr[0]).intValue();
                    return null;
                } else if (name.equals("setNonBlocking")) {
                    this.nonBlocking = ((Boolean) objArr[0]).booleanValue();
                    return null;
                } else if (name.equals("setTransmitReturnValue")) {
                    this.transmitReturnValue = ((Boolean) objArr[0]).booleanValue();
                    return null;
                } else if (name.equals("setTransmitExceptions")) {
                    this.transmitExceptions = ((Boolean) objArr[0]).booleanValue();
                    return null;
                } else if (name.equals("waitForLastResponse")) {
                    if (this.lastResponseID != null) {
                        return waitForResponse(this.lastResponseID.byteValue());
                    }
                    throw new IllegalStateException("There is no last response to wait for.");
                } else if (name.equals("getLastResponseID")) {
                    if (this.lastResponseID != null) {
                        return this.lastResponseID;
                    }
                    throw new IllegalStateException("There is no last response ID.");
                } else if (name.equals("waitForResponse")) {
                    if (this.transmitReturnValue || this.transmitExceptions || !this.nonBlocking) {
                        return waitForResponse(((Byte) objArr[0]).byteValue());
                    }
                    throw new IllegalStateException("This RemoteObject is currently set to ignore all responses.");
                } else if (name.equals("getConnection")) {
                    return this.connection;
                } else {
                    throw new RuntimeException("Invocation handler could not find RemoteObject method. Check ObjectSpace.java");
                }
            } else if (method.getDeclaringClass() != Object.class) {
                InvokeMethod invokeMethod = new InvokeMethod();
                invokeMethod.objectID = this.objectID;
                invokeMethod.method = method;
                invokeMethod.args = objArr;
                if (!this.transmitReturnValue && !this.transmitExceptions && this.nonBlocking) {
                    z = false;
                }
                if (z) {
                    synchronized (this) {
                        b2 = this.nextResponseNum;
                        this.nextResponseNum = (byte) (b2 + 1);
                        if (this.nextResponseNum == 64) {
                            this.nextResponseNum = 1;
                        }
                    }
                    if (this.transmitReturnValue) {
                        b2 = (byte) (b2 | ObjectSpace.kReturnValMask);
                    }
                    if (this.transmitExceptions) {
                        b2 = (byte) (b2 | ObjectSpace.kReturnExMask);
                    }
                    invokeMethod.responseID = b2;
                } else {
                    invokeMethod.responseID = 0;
                }
                this.connection.sendTCP(invokeMethod);
                if (invokeMethod.responseID != 0) {
                    this.lastResponseID = Byte.valueOf(invokeMethod.responseID);
                }
                if (this.nonBlocking) {
                    Class<?> returnType = method.getReturnType();
                    if (returnType.isPrimitive()) {
                        if (returnType == Integer.TYPE) {
                            return 0;
                        }
                        if (returnType == Boolean.TYPE) {
                            return Boolean.FALSE;
                        }
                        if (returnType == Float.TYPE) {
                            return Float.valueOf(0.0f);
                        }
                        if (returnType == Character.TYPE) {
                            return 0;
                        }
                        if (returnType == Long.TYPE) {
                            return 0L;
                        }
                        if (returnType == Short.TYPE) {
                            return (short) 0;
                        }
                        if (returnType == Byte.TYPE) {
                            return (byte) 0;
                        }
                        if (returnType == Double.TYPE) {
                            return Double.valueOf(0.0d);
                        }
                    }
                    return null;
                }
                try {
                    Object waitForResponse = waitForResponse(invokeMethod.responseID);
                    if (waitForResponse == null || !(waitForResponse instanceof Exception)) {
                        return waitForResponse;
                    }
                    throw ((Exception) waitForResponse);
                } catch (TimeoutException e) {
                    throw new TimeoutException("Response timed out: " + method.getDeclaringClass().getName() + "." + method.getName());
                }
            } else if (method.getName().equals("toString")) {
                return "<proxy>";
            } else {
                try {
                    return method.invoke(obj, objArr);
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
    }

    public ObjectSpace() {
        this.idToObject = new IntMap();
        this.connections = new Connection[0];
        this.connectionsLock = new Object();
        this.invokeListener = new Listener() {
            public void disconnected(Connection connection) {
                ObjectSpace.this.removeConnection(connection);
            }

            public void received(final Connection connection, Object obj) {
                if (obj instanceof InvokeMethod) {
                    if (ObjectSpace.this.connections != null) {
                        int i = 0;
                        int length = ObjectSpace.this.connections.length;
                        while (i < length && connection != ObjectSpace.this.connections[i]) {
                            i++;
                        }
                        if (i == length) {
                            return;
                        }
                    }
                    final InvokeMethod invokeMethod = (InvokeMethod) obj;
                    final Object obj2 = ObjectSpace.this.idToObject.get(invokeMethod.objectID);
                    if (obj2 == null) {
                        return;
                    }
                    if (ObjectSpace.this.executor == null) {
                        ObjectSpace.this.invoke(connection, obj2, invokeMethod);
                    } else {
                        ObjectSpace.this.executor.execute(new Runnable() {
                            public void run() {
                                ObjectSpace.this.invoke(connection, obj2, invokeMethod);
                            }
                        });
                    }
                }
            }
        };
        synchronized (instancesLock) {
            ObjectSpace[] objectSpaceArr = instances;
            ObjectSpace[] objectSpaceArr2 = new ObjectSpace[(objectSpaceArr.length + 1)];
            objectSpaceArr2[0] = this;
            System.arraycopy(objectSpaceArr, 0, objectSpaceArr2, 1, objectSpaceArr.length);
            instances = objectSpaceArr2;
        }
    }

    public ObjectSpace(Connection connection) {
        this();
        addConnection(connection);
    }

    static CachedMethod[] getMethods(Kryo kryo, Class cls) {
        CachedMethod[] cachedMethodArr = (CachedMethod[]) methodCache.get(cls);
        if (cachedMethodArr != null) {
            return cachedMethodArr;
        }
        ArrayList arrayList = new ArrayList();
        Class cls2 = cls;
        while (cls2 != null && cls2 != Object.class) {
            Collections.addAll(arrayList, cls2.getDeclaredMethods());
            cls2 = cls2.getSuperclass();
        }
        PriorityQueue priorityQueue = new PriorityQueue(Math.max(1, arrayList.size()), new Comparator() {
            public int compare(Method method, Method method2) {
                int compareTo = method.getName().compareTo(method2.getName());
                if (compareTo != 0) {
                    return compareTo;
                }
                Class<?>[] parameterTypes = method.getParameterTypes();
                Class<?>[] parameterTypes2 = method2.getParameterTypes();
                if (parameterTypes.length > parameterTypes2.length) {
                    return 1;
                }
                if (parameterTypes.length < parameterTypes2.length) {
                    return -1;
                }
                for (int i = 0; i < parameterTypes.length; i++) {
                    int compareTo2 = parameterTypes[i].getName().compareTo(parameterTypes2[i].getName());
                    if (compareTo2 != 0) {
                        return compareTo2;
                    }
                }
                throw new RuntimeException("Two methods with same signature!");
            }
        });
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Method method = (Method) arrayList.get(i);
            int modifiers = method.getModifiers();
            if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers) && !method.isSynthetic()) {
                priorityQueue.add(method);
            }
        }
        int size2 = priorityQueue.size();
        CachedMethod[] cachedMethodArr2 = new CachedMethod[size2];
        for (int i2 = 0; i2 < size2; i2++) {
            CachedMethod cachedMethod = new CachedMethod();
            cachedMethod.method = (Method) priorityQueue.poll();
            Class<?>[] parameterTypes = cachedMethod.method.getParameterTypes();
            cachedMethod.serializers = new Serializer[parameterTypes.length];
            int length = parameterTypes.length;
            for (int i3 = 0; i3 < length; i3++) {
                if (kryo.isFinal(parameterTypes[i3])) {
                    cachedMethod.serializers[i3] = kryo.getSerializer(parameterTypes[i3]);
                }
            }
            cachedMethodArr2[i2] = cachedMethod;
        }
        methodCache.put(cls, cachedMethodArr2);
        return cachedMethodArr2;
    }

    static Object getRegisteredObject(Connection connection, int i) {
        Object obj;
        for (ObjectSpace objectSpace : instances) {
            Connection[] connectionArr = objectSpace.connections;
            for (Connection connection2 : connectionArr) {
                if (connection2 == connection && (obj = objectSpace.idToObject.get(i)) != null) {
                    return obj;
                }
            }
        }
        return null;
    }

    public static RemoteObject getRemoteObject(Connection connection, int i, Class... clsArr) {
        if (connection == null) {
            throw new IllegalArgumentException("connection cannot be null.");
        } else if (clsArr == null) {
            throw new IllegalArgumentException("ifaces cannot be null.");
        } else {
            Class[] clsArr2 = new Class[(clsArr.length + 1)];
            clsArr2[0] = RemoteObject.class;
            System.arraycopy(clsArr, 0, clsArr2, 1, clsArr.length);
            return (RemoteObject) Proxy.newProxyInstance(ObjectSpace.class.getClassLoader(), clsArr2, new RemoteInvocationHandler(connection, i));
        }
    }

    public static Object getRemoteObject(Connection connection, int i, Class cls) {
        return getRemoteObject(connection, i, cls);
    }

    public static void registerClasses(Kryo kryo) {
        kryo.register(Object[].class);
        kryo.register(InvokeMethod.class);
        ((FieldSerializer) kryo.register(InvokeMethodResult.class).getSerializer()).getField("objectID").setClass(Integer.TYPE, new Serializer() {
            public Integer read(Kryo kryo, Input input, Class cls) {
                return Integer.valueOf(input.readInt(true));
            }

            public void write(Kryo kryo, Output output, Integer num) {
                output.writeInt(num.intValue(), true);
            }
        });
        kryo.register(InvocationHandler.class, new Serializer() {
            public Object read(Kryo kryo, Input input, Class cls) {
                return ObjectSpace.getRegisteredObject((Connection) kryo.getContext().get("connection"), input.readInt(true));
            }

            public void write(Kryo kryo, Output output, Object obj) {
                output.writeInt(((RemoteInvocationHandler) Proxy.getInvocationHandler(obj)).objectID, true);
            }
        });
    }

    public void addConnection(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("connection cannot be null.");
        }
        synchronized (this.connectionsLock) {
            Connection[] connectionArr = new Connection[(this.connections.length + 1)];
            connectionArr[0] = connection;
            System.arraycopy(this.connections, 0, connectionArr, 1, this.connections.length);
            this.connections = connectionArr;
        }
        connection.addListener(this.invokeListener);
    }

    public void close() {
        Connection[] connectionArr = this.connections;
        for (Connection removeListener : connectionArr) {
            removeListener.removeListener(this.invokeListener);
        }
        synchronized (instancesLock) {
            ArrayList arrayList = new ArrayList(Arrays.asList(instances));
            arrayList.remove(this);
            instances = (ObjectSpace[]) arrayList.toArray(new ObjectSpace[arrayList.size()]);
        }
    }

    /* access modifiers changed from: protected */
    public void invoke(Connection connection, Object obj, InvokeMethod invokeMethod) {
        Object cause;
        boolean z = true;
        byte b2 = invokeMethod.responseID;
        boolean z2 = (b2 & kReturnValMask) == Byte.MIN_VALUE;
        if ((b2 & kReturnExMask) != 64) {
            z = false;
        }
        Method method = invokeMethod.method;
        try {
            cause = method.invoke(obj, invokeMethod.args);
        } catch (InvocationTargetException e) {
            if (z) {
                cause = e.getCause();
            } else {
                throw new RuntimeException("Error invoking method: " + method.getDeclaringClass().getName() + "." + method.getName(), e);
            }
        } catch (Exception e2) {
            throw new RuntimeException("Error invoking method: " + method.getDeclaringClass().getName() + "." + method.getName(), e2);
        }
        if (b2 != 0) {
            InvokeMethodResult invokeMethodResult = new InvokeMethodResult();
            invokeMethodResult.objectID = invokeMethod.objectID;
            invokeMethodResult.responseID = b2;
            if (z2 || invokeMethod.method.getReturnType().isPrimitive()) {
                invokeMethodResult.result = cause;
            } else {
                invokeMethodResult.result = null;
            }
            connection.sendTCP(invokeMethodResult);
        }
    }

    public void register(int i, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        }
        this.idToObject.put(i, obj);
    }

    public void remove(int i) {
        this.idToObject.remove(i);
    }

    public void remove(Object obj) {
        if (this.idToObject.containsValue(obj, true)) {
            this.idToObject.remove(this.idToObject.findKey(obj, true, -1));
        }
    }

    public void removeConnection(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("connection cannot be null.");
        }
        connection.removeListener(this.invokeListener);
        synchronized (this.connectionsLock) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.connections));
            arrayList.remove(connection);
            this.connections = (Connection[]) arrayList.toArray(new Connection[arrayList.size()]);
        }
    }

    public void setExecutor(Executor executor2) {
        this.executor = executor2;
    }
}
