package com.esotericsoftware.kryonet;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

class UdpConnection {
    InetSocketAddress connectedAddress;
    DatagramChannel datagramChannel;
    int keepAliveMillis = 19000;
    private long lastCommunicationTime;
    final ByteBuffer readBuffer;
    private SelectionKey selectionKey;
    private final Serialization serialization;
    final ByteBuffer writeBuffer;
    private final Object writeLock = new Object();

    public UdpConnection(Serialization serialization2, int i) {
        this.serialization = serialization2;
        this.readBuffer = ByteBuffer.allocate(i);
        this.writeBuffer = ByteBuffer.allocateDirect(i);
    }

    public void bind(Selector selector, InetSocketAddress inetSocketAddress) {
        close();
        this.readBuffer.clear();
        this.writeBuffer.clear();
        try {
            this.datagramChannel = selector.provider().openDatagramChannel();
            this.datagramChannel.socket().bind(inetSocketAddress);
            this.datagramChannel.configureBlocking(false);
            this.selectionKey = this.datagramChannel.register(selector, 1);
            this.lastCommunicationTime = System.currentTimeMillis();
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public void close() {
        this.connectedAddress = null;
        try {
            if (this.datagramChannel != null) {
                this.datagramChannel.close();
                this.datagramChannel = null;
                if (this.selectionKey != null) {
                    this.selectionKey.selector().wakeup();
                }
            }
        } catch (IOException e) {
        }
    }

    public void connect(Selector selector, InetSocketAddress inetSocketAddress) {
        close();
        this.readBuffer.clear();
        this.writeBuffer.clear();
        try {
            this.datagramChannel = selector.provider().openDatagramChannel();
            this.datagramChannel.socket().bind(null);
            this.datagramChannel.socket().connect(inetSocketAddress);
            this.datagramChannel.configureBlocking(false);
            this.selectionKey = this.datagramChannel.register(selector, 1);
            this.lastCommunicationTime = System.currentTimeMillis();
            this.connectedAddress = inetSocketAddress;
        } catch (IOException e) {
            close();
            IOException iOException = new IOException("Unable to connect to: " + inetSocketAddress);
            iOException.initCause(e);
            throw iOException;
        }
    }

    public boolean needsKeepAlive(long j) {
        return this.connectedAddress != null && this.keepAliveMillis > 0 && j - this.lastCommunicationTime > ((long) this.keepAliveMillis);
    }

    public InetSocketAddress readFromAddress() {
        DatagramChannel datagramChannel2 = this.datagramChannel;
        if (datagramChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        this.lastCommunicationTime = System.currentTimeMillis();
        return (InetSocketAddress) datagramChannel2.receive(this.readBuffer);
    }

    public Object readObject(Connection connection) {
        this.readBuffer.flip();
        try {
            Object read = this.serialization.read(connection, this.readBuffer);
            if (this.readBuffer.hasRemaining()) {
                throw new KryoNetException("Incorrect number of bytes (" + this.readBuffer.remaining() + " remaining) used to deserialize object: " + read);
            }
            this.readBuffer.clear();
            return read;
        } catch (Exception e) {
            throw new KryoNetException("Error during deserialization.", e);
        } catch (Throwable th) {
            this.readBuffer.clear();
            throw th;
        }
    }

    public int send(Connection connection, Object obj, SocketAddress socketAddress) {
        int limit;
        DatagramChannel datagramChannel2 = this.datagramChannel;
        if (datagramChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        synchronized (this.writeLock) {
            try {
                this.serialization.write(connection, this.writeBuffer, obj);
                this.writeBuffer.flip();
                limit = this.writeBuffer.limit();
                datagramChannel2.send(this.writeBuffer, socketAddress);
                this.lastCommunicationTime = System.currentTimeMillis();
                if (!(!this.writeBuffer.hasRemaining())) {
                    limit = -1;
                }
                this.writeBuffer.clear();
            } catch (Exception e) {
                throw new KryoNetException("Error serializing object of type: " + obj.getClass().getName(), e);
            } catch (Throwable th) {
                this.writeBuffer.clear();
                throw th;
            }
        }
        return limit;
    }
}
