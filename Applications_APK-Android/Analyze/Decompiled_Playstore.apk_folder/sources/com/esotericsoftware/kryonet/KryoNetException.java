package com.esotericsoftware.kryonet;

public class KryoNetException extends RuntimeException {
    public KryoNetException() {
    }

    public KryoNetException(String str) {
        super(str);
    }

    public KryoNetException(String str, Throwable th) {
        super(str, th);
    }

    public KryoNetException(Throwable th) {
        super(th);
    }
}
