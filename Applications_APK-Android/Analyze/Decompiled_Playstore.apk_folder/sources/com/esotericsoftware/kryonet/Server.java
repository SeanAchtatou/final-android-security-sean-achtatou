package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.util.IntMap;
import com.esotericsoftware.kryonet.FrameworkMessage;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

public class Server implements EndPoint {
    private Connection[] connections;
    private Listener dispatchListener;
    private ByteBuffer emptyBuffer;
    private int emptySelects;
    private Object listenerLock;
    Listener[] listeners;
    private int nextConnectionID;
    private final int objectBufferSize;
    private IntMap pendingConnections;
    private final Selector selector;
    private final Serialization serialization;
    private ServerSocketChannel serverChannel;
    private volatile boolean shutdown;
    private UdpConnection udp;
    private Object updateLock;
    private Thread updateThread;
    private final int writeBufferSize;

    public Server() {
        this(16384, 2048);
    }

    public Server(int i, int i2) {
        this(i, i2, new KryoSerialization());
    }

    public Server(int i, int i2, Serialization serialization2) {
        this.connections = new Connection[0];
        this.pendingConnections = new IntMap();
        this.listeners = new Listener[0];
        this.listenerLock = new Object();
        this.nextConnectionID = 1;
        this.updateLock = new Object();
        this.emptyBuffer = ByteBuffer.allocate(0);
        this.dispatchListener = new Listener() {
            public void connected(Connection connection) {
                for (Listener connected : Server.this.listeners) {
                    connected.connected(connection);
                }
            }

            public void disconnected(Connection connection) {
                Server.this.removeConnection(connection);
                for (Listener disconnected : Server.this.listeners) {
                    disconnected.disconnected(connection);
                }
            }

            public void idle(Connection connection) {
                for (Listener idle : Server.this.listeners) {
                    idle.idle(connection);
                }
            }

            public void received(Connection connection, Object obj) {
                for (Listener received : Server.this.listeners) {
                    received.received(connection, obj);
                }
            }
        };
        this.writeBufferSize = i;
        this.objectBufferSize = i2;
        this.serialization = serialization2;
        try {
            this.selector = Selector.open();
        } catch (IOException e) {
            throw new RuntimeException("Error opening selector.", e);
        }
    }

    private void acceptOperation(SocketChannel socketChannel) {
        Connection newConnection = newConnection();
        newConnection.initialize(this.serialization, this.writeBufferSize, this.objectBufferSize);
        newConnection.endPoint = this;
        UdpConnection udpConnection = this.udp;
        if (udpConnection != null) {
            newConnection.udp = udpConnection;
        }
        try {
            newConnection.tcp.accept(this.selector, socketChannel).attach(newConnection);
            int i = this.nextConnectionID;
            this.nextConnectionID = i + 1;
            if (this.nextConnectionID == -1) {
                this.nextConnectionID = 1;
            }
            newConnection.id = i;
            newConnection.setConnected(true);
            newConnection.addListener(this.dispatchListener);
            if (udpConnection == null) {
                addConnection(newConnection);
            } else {
                this.pendingConnections.put(i, newConnection);
            }
            FrameworkMessage.RegisterTCP registerTCP = new FrameworkMessage.RegisterTCP();
            registerTCP.connectionID = i;
            newConnection.sendTCP(registerTCP);
            if (udpConnection == null) {
                newConnection.notifyConnected();
            }
        } catch (IOException e) {
            newConnection.close();
        }
    }

    private void addConnection(Connection connection) {
        Connection[] connectionArr = new Connection[(this.connections.length + 1)];
        connectionArr[0] = connection;
        System.arraycopy(this.connections, 0, connectionArr, 1, this.connections.length);
        this.connections = connectionArr;
    }

    public void addListener(Listener listener) {
        int i = 0;
        if (listener == null) {
            throw new IllegalArgumentException("listener cannot be null.");
        }
        synchronized (this.listenerLock) {
            Listener[] listenerArr = this.listeners;
            int length = listenerArr.length;
            while (i < length) {
                if (listener != listenerArr[i]) {
                    i++;
                } else {
                    return;
                }
            }
            Listener[] listenerArr2 = new Listener[(length + 1)];
            listenerArr2[0] = listener;
            System.arraycopy(listenerArr, 0, listenerArr2, 1, length);
            this.listeners = listenerArr2;
        }
    }

    public void bind(int i) {
        bind(new InetSocketAddress(i), (InetSocketAddress) null);
    }

    public void bind(int i, int i2) {
        bind(new InetSocketAddress(i), new InetSocketAddress(i2));
    }

    public void bind(InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2) {
        close();
        synchronized (this.updateLock) {
            this.selector.wakeup();
            try {
                this.serverChannel = this.selector.provider().openServerSocketChannel();
                this.serverChannel.socket().bind(inetSocketAddress);
                this.serverChannel.configureBlocking(false);
                this.serverChannel.register(this.selector, 16);
                if (inetSocketAddress2 != null) {
                    this.udp = new UdpConnection(this.serialization, this.objectBufferSize);
                    this.udp.bind(this.selector, inetSocketAddress2);
                }
            } catch (IOException e) {
                close();
                throw e;
            }
        }
    }

    public void close() {
        for (Connection close : this.connections) {
            close.close();
        }
        Connection[] connectionArr = new Connection[0];
        ServerSocketChannel serverSocketChannel = this.serverChannel;
        if (serverSocketChannel != null) {
            try {
                serverSocketChannel.close();
            } catch (IOException e) {
            }
            this.serverChannel = null;
        }
        UdpConnection udpConnection = this.udp;
        if (udpConnection != null) {
            udpConnection.close();
            this.udp = null;
        }
        synchronized (this.updateLock) {
            this.selector.wakeup();
            try {
                this.selector.selectNow();
            } catch (IOException e2) {
            }
        }
    }

    public Connection[] getConnections() {
        return this.connections;
    }

    public Kryo getKryo() {
        return ((KryoSerialization) this.serialization).getKryo();
    }

    public Serialization getSerialization() {
        return this.serialization;
    }

    public Thread getUpdateThread() {
        return this.updateThread;
    }

    /* access modifiers changed from: protected */
    public Connection newConnection() {
        return new Connection();
    }

    /* access modifiers changed from: package-private */
    public void removeConnection(Connection connection) {
        ArrayList arrayList = new ArrayList(Arrays.asList(this.connections));
        arrayList.remove(connection);
        this.connections = (Connection[]) arrayList.toArray(new Connection[arrayList.size()]);
        this.pendingConnections.remove(connection.id);
    }

    public void removeListener(Listener listener) {
        int i = 0;
        if (listener == null) {
            throw new IllegalArgumentException("listener cannot be null.");
        }
        synchronized (this.listenerLock) {
            Listener[] listenerArr = new Listener[(r5 - 1)];
            for (Listener listener2 : this.listeners) {
                if (listener != listener2) {
                    if (i != r5 - 1) {
                        listenerArr[i] = listener2;
                        i++;
                    } else {
                        return;
                    }
                }
            }
            this.listeners = listenerArr;
        }
    }

    public void run() {
        this.shutdown = false;
        while (!this.shutdown) {
            try {
                update(250);
            } catch (IOException e) {
                close();
            }
        }
    }

    public void sendToAllExceptTCP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id != i) {
                connection.sendTCP(obj);
            }
        }
    }

    public void sendToAllExceptUDP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id != i) {
                connection.sendUDP(obj);
            }
        }
    }

    public void sendToAllTCP(Object obj) {
        for (Connection sendTCP : this.connections) {
            sendTCP.sendTCP(obj);
        }
    }

    public void sendToAllUDP(Object obj) {
        for (Connection sendUDP : this.connections) {
            sendUDP.sendUDP(obj);
        }
    }

    public void sendToTCP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id == i) {
                connection.sendTCP(obj);
                return;
            }
        }
    }

    public void sendToUDP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id == i) {
                connection.sendUDP(obj);
                return;
            }
        }
    }

    public void start() {
        new Thread(this, "Server").start();
    }

    public void stop() {
        if (!this.shutdown) {
            close();
            this.shutdown = true;
        }
    }

    public void update(int i) {
        SelectionKey next;
        Connection connection;
        Connection connection2;
        this.updateThread = Thread.currentThread();
        synchronized (this.updateLock) {
        }
        long currentTimeMillis = System.currentTimeMillis();
        if ((i > 0 ? this.selector.select((long) i) : this.selector.selectNow()) == 0) {
            this.emptySelects++;
            if (this.emptySelects == 100) {
                this.emptySelects = 0;
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                if (currentTimeMillis2 < 25) {
                    try {
                        Thread.sleep(25 - currentTimeMillis2);
                    } catch (InterruptedException e) {
                    }
                }
            }
        } else {
            this.emptySelects = 0;
            Set<SelectionKey> selectedKeys = this.selector.selectedKeys();
            synchronized (selectedKeys) {
                UdpConnection udpConnection = this.udp;
                Iterator<SelectionKey> it = selectedKeys.iterator();
                while (it.hasNext()) {
                    next = it.next();
                    it.remove();
                    connection = (Connection) next.attachment();
                    try {
                        int readyOps = next.readyOps();
                        if (connection != null) {
                            if (udpConnection == null || connection.udpRemoteAddress != null) {
                                if ((readyOps & 1) == 1) {
                                    while (true) {
                                        try {
                                            Object readObject = connection.tcp.readObject(connection);
                                            if (readObject == null) {
                                                break;
                                            }
                                            connection.notifyReceived(readObject);
                                        } catch (IOException e2) {
                                            connection.close();
                                        } catch (KryoNetException e3) {
                                            connection.close();
                                        }
                                    }
                                }
                                if ((readyOps & 4) == 4) {
                                    try {
                                        connection.tcp.writeOperation();
                                    } catch (IOException e4) {
                                        connection.close();
                                    }
                                }
                            } else {
                                connection.close();
                            }
                        } else if ((readyOps & 16) == 16) {
                            ServerSocketChannel serverSocketChannel = this.serverChannel;
                            if (serverSocketChannel != null) {
                                try {
                                    SocketChannel accept = serverSocketChannel.accept();
                                    if (accept != null) {
                                        acceptOperation(accept);
                                    }
                                } catch (IOException e5) {
                                }
                            }
                        } else if (udpConnection == null) {
                            next.channel().close();
                        } else {
                            try {
                                InetSocketAddress readFromAddress = udpConnection.readFromAddress();
                                if (readFromAddress != null) {
                                    Connection[] connectionArr = this.connections;
                                    int length = connectionArr.length;
                                    int i2 = 0;
                                    while (true) {
                                        if (i2 >= length) {
                                            connection2 = connection;
                                            break;
                                        }
                                        connection2 = connectionArr[i2];
                                        if (!readFromAddress.equals(connection2.udpRemoteAddress)) {
                                            i2++;
                                        }
                                    }
                                    try {
                                        Object readObject2 = udpConnection.readObject(connection2);
                                        try {
                                            if (readObject2 instanceof FrameworkMessage) {
                                                if (readObject2 instanceof FrameworkMessage.RegisterUDP) {
                                                    Connection connection3 = (Connection) this.pendingConnections.remove(((FrameworkMessage.RegisterUDP) readObject2).connectionID);
                                                    if (connection3 != null && connection3.udpRemoteAddress == null) {
                                                        connection3.udpRemoteAddress = readFromAddress;
                                                        addConnection(connection3);
                                                        connection3.sendTCP(new FrameworkMessage.RegisterUDP());
                                                        connection3.notifyConnected();
                                                    }
                                                } else if (readObject2 instanceof FrameworkMessage.DiscoverHost) {
                                                    try {
                                                        udpConnection.datagramChannel.send(this.emptyBuffer, readFromAddress);
                                                    } catch (IOException e6) {
                                                    }
                                                }
                                            }
                                            if (connection2 != null) {
                                                connection2.notifyReceived(readObject2);
                                            }
                                        } catch (CancelledKeyException e7) {
                                            connection = connection2;
                                        }
                                    } catch (KryoNetException e8) {
                                    }
                                }
                            } catch (IOException e9) {
                            }
                        }
                    } catch (CancelledKeyException e10) {
                    }
                }
            }
        }
        long currentTimeMillis3 = System.currentTimeMillis();
        for (Connection connection4 : this.connections) {
            if (connection4.tcp.isTimedOut(currentTimeMillis3)) {
                connection4.close();
            } else if (connection4.tcp.needsKeepAlive(currentTimeMillis3)) {
                connection4.sendTCP(FrameworkMessage.keepAlive);
            }
            if (connection4.isIdle()) {
                connection4.notifyIdle();
            }
        }
        return;
        if (connection != null) {
            connection.close();
        } else {
            next.channel().close();
        }
    }
}
