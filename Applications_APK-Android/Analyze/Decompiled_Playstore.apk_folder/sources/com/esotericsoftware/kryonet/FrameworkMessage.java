package com.esotericsoftware.kryonet;

public interface FrameworkMessage {
    public static final KeepAlive keepAlive = new KeepAlive();

    public class DiscoverHost implements FrameworkMessage {
    }

    public class KeepAlive implements FrameworkMessage {
    }

    public class Ping implements FrameworkMessage {
        public int id;
        public boolean isReply;
    }

    public class RegisterTCP implements FrameworkMessage {
        public int connectionID;
    }

    public class RegisterUDP implements FrameworkMessage {
        public int connectionID;
    }
}
