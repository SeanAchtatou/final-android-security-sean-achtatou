package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.FrameworkMessage;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.security.AccessControlException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public class Client extends Connection implements EndPoint {
    private InetAddress connectHost;
    private int connectTcpPort;
    private int connectTimeout;
    private int connectUdpPort;
    private int emptySelects;
    private boolean isClosed;
    private Selector selector;
    private final Serialization serialization;
    private volatile boolean shutdown;
    private volatile boolean tcpRegistered;
    private Object tcpRegistrationLock;
    private volatile boolean udpRegistered;
    private Object udpRegistrationLock;
    private final Object updateLock;
    private Thread updateThread;

    static {
        try {
            System.setProperty("java.net.preferIPv6Addresses", "false");
        } catch (AccessControlException e) {
        }
    }

    public Client() {
        this(8192, 2048);
    }

    public Client(int i, int i2) {
        this(i, i2, new KryoSerialization());
    }

    public Client(int i, int i2, Serialization serialization2) {
        this.tcpRegistrationLock = new Object();
        this.udpRegistrationLock = new Object();
        this.updateLock = new Object();
        this.endPoint = this;
        this.serialization = serialization2;
        initialize(serialization2, i, i2);
        try {
            this.selector = Selector.open();
        } catch (IOException e) {
            throw new RuntimeException("Error opening selector.", e);
        }
    }

    private void broadcast(int i, DatagramSocket datagramSocket) {
        ByteBuffer allocate = ByteBuffer.allocate(64);
        this.serialization.write(null, allocate, new FrameworkMessage.DiscoverHost());
        allocate.flip();
        byte[] bArr = new byte[allocate.limit()];
        allocate.get(bArr);
        Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
        while (it.hasNext()) {
            Iterator it2 = Collections.list(((NetworkInterface) it.next()).getInetAddresses()).iterator();
            while (it2.hasNext()) {
                byte[] address = ((InetAddress) it2.next()).getAddress();
                address[3] = -1;
                try {
                    datagramSocket.send(new DatagramPacket(bArr, bArr.length, InetAddress.getByAddress(address), i));
                } catch (Exception e) {
                }
                address[2] = -1;
                try {
                    datagramSocket.send(new DatagramPacket(bArr, bArr.length, InetAddress.getByAddress(address), i));
                } catch (Exception e2) {
                }
            }
        }
    }

    public void addListener(Listener listener) {
        super.addListener(listener);
    }

    public void close() {
        super.close();
        synchronized (this.updateLock) {
            if (!this.isClosed) {
                this.isClosed = true;
                this.selector.wakeup();
                try {
                    this.selector.selectNow();
                } catch (IOException e) {
                }
            }
        }
    }

    public void connect(int i, String str, int i2) {
        connect(i, InetAddress.getByName(str), i2, -1);
    }

    public void connect(int i, String str, int i2, int i3) {
        connect(i, InetAddress.getByName(str), i2, i3);
    }

    public void connect(int i, InetAddress inetAddress, int i2) {
        connect(i, inetAddress, i2, -1);
    }

    public void connect(int i, InetAddress inetAddress, int i2, int i3) {
        if (inetAddress == null) {
            throw new IllegalArgumentException("host cannot be null.");
        } else if (Thread.currentThread() == getUpdateThread()) {
            throw new IllegalStateException("Cannot connect on the connection's update thread.");
        } else {
            this.connectTimeout = i;
            this.connectHost = inetAddress;
            this.connectTcpPort = i2;
            this.connectUdpPort = i3;
            close();
            this.id = -1;
            if (i3 != -1) {
                try {
                    this.udp = new UdpConnection(this.serialization, this.tcp.readBuffer.capacity());
                } catch (IOException e) {
                    close();
                    throw e;
                }
            }
            synchronized (this.updateLock) {
                this.tcpRegistered = false;
                this.selector.wakeup();
                long currentTimeMillis = System.currentTimeMillis() + ((long) i);
                this.tcp.connect(this.selector, new InetSocketAddress(inetAddress, i2), 5000);
            }
            synchronized (this.tcpRegistrationLock) {
                while (!this.tcpRegistered && System.currentTimeMillis() < currentTimeMillis) {
                    try {
                        this.tcpRegistrationLock.wait(100);
                    } catch (InterruptedException e2) {
                    }
                }
                if (!this.tcpRegistered) {
                    throw new SocketTimeoutException("Connected, but timed out during TCP registration.\nNote: Client#update must be called in a separate thread during connect.");
                }
            }
            if (i3 != -1) {
                InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, i3);
                synchronized (this.updateLock) {
                    this.udpRegistered = false;
                    this.selector.wakeup();
                    this.udp.connect(this.selector, inetSocketAddress);
                }
                synchronized (this.udpRegistrationLock) {
                    while (!this.udpRegistered && System.currentTimeMillis() < currentTimeMillis) {
                        FrameworkMessage.RegisterUDP registerUDP = new FrameworkMessage.RegisterUDP();
                        registerUDP.connectionID = this.id;
                        this.udp.send(this, registerUDP, inetSocketAddress);
                        try {
                            this.udpRegistrationLock.wait(100);
                        } catch (InterruptedException e3) {
                        }
                    }
                    if (!this.udpRegistered) {
                        throw new SocketTimeoutException("Connected, but timed out during UDP registration: " + inetAddress + ":" + i3);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.net.InetAddress discoverHost(int r7, int r8) {
        /*
            r6 = this;
            r0 = 0
            java.net.DatagramSocket r1 = new java.net.DatagramSocket     // Catch:{ IOException -> 0x0029, all -> 0x0031 }
            r1.<init>()     // Catch:{ IOException -> 0x0029, all -> 0x0031 }
            r6.broadcast(r7, r1)     // Catch:{ IOException -> 0x003d, all -> 0x003b }
            r1.setSoTimeout(r8)     // Catch:{ IOException -> 0x003d, all -> 0x003b }
            java.net.DatagramPacket r2 = new java.net.DatagramPacket     // Catch:{ IOException -> 0x003d, all -> 0x003b }
            r3 = 0
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x003d, all -> 0x003b }
            r4 = 0
            r2.<init>(r3, r4)     // Catch:{ IOException -> 0x003d, all -> 0x003b }
            r1.receive(r2)     // Catch:{ SocketTimeoutException -> 0x0022 }
            java.net.InetAddress r0 = r2.getAddress()     // Catch:{ IOException -> 0x003d, all -> 0x003b }
            if (r1 == 0) goto L_0x0021
            r1.close()
        L_0x0021:
            return r0
        L_0x0022:
            r2 = move-exception
            if (r1 == 0) goto L_0x0021
            r1.close()
            goto L_0x0021
        L_0x0029:
            r1 = move-exception
            r1 = r0
        L_0x002b:
            if (r1 == 0) goto L_0x0021
            r1.close()
            goto L_0x0021
        L_0x0031:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0035:
            if (r1 == 0) goto L_0x003a
            r1.close()
        L_0x003a:
            throw r0
        L_0x003b:
            r0 = move-exception
            goto L_0x0035
        L_0x003d:
            r2 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Client.discoverHost(int, int):java.net.InetAddress");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List discoverHosts(int r6, int r7) {
        /*
            r5 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r2 = 0
            java.net.DatagramSocket r1 = new java.net.DatagramSocket     // Catch:{ IOException -> 0x003d, all -> 0x0033 }
            r1.<init>()     // Catch:{ IOException -> 0x003d, all -> 0x0033 }
            r5.broadcast(r6, r1)     // Catch:{ IOException -> 0x0025, all -> 0x003b }
            r1.setSoTimeout(r7)     // Catch:{ IOException -> 0x0025, all -> 0x003b }
        L_0x0011:
            java.net.DatagramPacket r2 = new java.net.DatagramPacket     // Catch:{ IOException -> 0x0025, all -> 0x003b }
            r3 = 0
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0025, all -> 0x003b }
            r4 = 0
            r2.<init>(r3, r4)     // Catch:{ IOException -> 0x0025, all -> 0x003b }
            r1.receive(r2)     // Catch:{ SocketTimeoutException -> 0x002c }
            java.net.InetAddress r2 = r2.getAddress()     // Catch:{ IOException -> 0x0025, all -> 0x003b }
            r0.add(r2)     // Catch:{ IOException -> 0x0025, all -> 0x003b }
            goto L_0x0011
        L_0x0025:
            r2 = move-exception
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()
        L_0x002b:
            return r0
        L_0x002c:
            r2 = move-exception
            if (r1 == 0) goto L_0x002b
            r1.close()
            goto L_0x002b
        L_0x0033:
            r0 = move-exception
            r1 = r2
        L_0x0035:
            if (r1 == 0) goto L_0x003a
            r1.close()
        L_0x003a:
            throw r0
        L_0x003b:
            r0 = move-exception
            goto L_0x0035
        L_0x003d:
            r1 = move-exception
            r1 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Client.discoverHosts(int, int):java.util.List");
    }

    public Kryo getKryo() {
        return ((KryoSerialization) this.serialization).getKryo();
    }

    public Serialization getSerialization() {
        return this.serialization;
    }

    public Thread getUpdateThread() {
        return this.updateThread;
    }

    /* access modifiers changed from: package-private */
    public void keepAlive() {
        if (this.isConnected) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.tcp.needsKeepAlive(currentTimeMillis)) {
                sendTCP(FrameworkMessage.keepAlive);
            }
            if (this.udp != null && this.udpRegistered && this.udp.needsKeepAlive(currentTimeMillis)) {
                sendUDP(FrameworkMessage.keepAlive);
            }
        }
    }

    public void reconnect() {
        reconnect(this.connectTimeout);
    }

    public void reconnect(int i) {
        if (this.connectHost == null) {
            throw new IllegalStateException("This client has never been connected.");
        }
        connect(this.connectTimeout, this.connectHost, this.connectTcpPort, this.connectUdpPort);
    }

    public void removeListener(Listener listener) {
        super.removeListener(listener);
    }

    public void run() {
        this.shutdown = false;
        while (!this.shutdown) {
            try {
                update(250);
            } catch (IOException e) {
                close();
            } catch (KryoNetException e2) {
                close();
                throw e2;
            }
        }
    }

    public void setKeepAliveUDP(int i) {
        if (this.udp == null) {
            throw new IllegalStateException("Not connected via UDP.");
        }
        this.udp.keepAliveMillis = i;
    }

    public void start() {
        if (this.updateThread != null) {
            this.shutdown = true;
            try {
                this.updateThread.join(5000);
            } catch (InterruptedException e) {
            }
        }
        this.updateThread = new Thread(this, "Client");
        this.updateThread.setDaemon(true);
        this.updateThread.start();
    }

    public void stop() {
        if (!this.shutdown) {
            close();
            this.shutdown = true;
            this.selector.wakeup();
        }
    }

    public void update(int i) {
        Object readObject;
        this.updateThread = Thread.currentThread();
        synchronized (this.updateLock) {
        }
        long currentTimeMillis = System.currentTimeMillis();
        if ((i > 0 ? this.selector.select((long) i) : this.selector.selectNow()) == 0) {
            this.emptySelects++;
            if (this.emptySelects == 100) {
                this.emptySelects = 0;
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                if (currentTimeMillis2 < 25) {
                    try {
                        Thread.sleep(25 - currentTimeMillis2);
                    } catch (InterruptedException e) {
                    }
                }
            }
        } else {
            this.emptySelects = 0;
            this.isClosed = false;
            Set<SelectionKey> selectedKeys = this.selector.selectedKeys();
            synchronized (selectedKeys) {
                Iterator<SelectionKey> it = selectedKeys.iterator();
                while (it.hasNext()) {
                    SelectionKey next = it.next();
                    it.remove();
                    try {
                        int readyOps = next.readyOps();
                        if ((readyOps & 1) == 1) {
                            if (next.attachment() == this.tcp) {
                                while (true) {
                                    Object readObject2 = this.tcp.readObject(this);
                                    if (readObject2 == null) {
                                        break;
                                    } else if (!this.tcpRegistered) {
                                        if (readObject2 instanceof FrameworkMessage.RegisterTCP) {
                                            this.id = ((FrameworkMessage.RegisterTCP) readObject2).connectionID;
                                            synchronized (this.tcpRegistrationLock) {
                                                this.tcpRegistered = true;
                                                this.tcpRegistrationLock.notifyAll();
                                                if (this.udp == null) {
                                                    setConnected(true);
                                                }
                                            }
                                            if (this.udp == null) {
                                                notifyConnected();
                                            }
                                        } else {
                                            continue;
                                        }
                                    } else if (this.udp == null || this.udpRegistered) {
                                        if (this.isConnected) {
                                            keepAlive();
                                            notifyReceived(readObject2);
                                        }
                                    } else if (readObject2 instanceof FrameworkMessage.RegisterUDP) {
                                        synchronized (this.udpRegistrationLock) {
                                            this.udpRegistered = true;
                                            this.udpRegistrationLock.notifyAll();
                                            setConnected(true);
                                        }
                                        notifyConnected();
                                    } else {
                                        continue;
                                    }
                                }
                                while (true) {
                                }
                            } else if (!(this.udp.readFromAddress() == null || (readObject = this.udp.readObject(this)) == null)) {
                                keepAlive();
                                notifyReceived(readObject);
                            }
                        }
                        if ((readyOps & 4) == 4) {
                            this.tcp.writeOperation();
                        }
                    } catch (CancelledKeyException e2) {
                    }
                }
            }
        }
        if (this.isConnected) {
            if (this.tcp.isTimedOut(System.currentTimeMillis())) {
                close();
            } else {
                keepAlive();
            }
            if (isIdle()) {
                notifyIdle();
            }
        }
    }
}
