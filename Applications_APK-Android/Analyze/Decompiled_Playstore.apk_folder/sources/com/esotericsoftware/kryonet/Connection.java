package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryonet.FrameworkMessage;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

public class Connection {
    EndPoint endPoint;
    int id = -1;
    volatile boolean isConnected;
    private int lastPingID;
    private long lastPingSendTime;
    private Object listenerLock = new Object();
    private Listener[] listeners = new Listener[0];
    private String name;
    private int returnTripTime;
    TcpConnection tcp;
    UdpConnection udp;
    InetSocketAddress udpRemoteAddress;

    protected Connection() {
    }

    public void addListener(Listener listener) {
        int i = 0;
        if (listener == null) {
            throw new IllegalArgumentException("listener cannot be null.");
        }
        synchronized (this.listenerLock) {
            Listener[] listenerArr = this.listeners;
            int length = listenerArr.length;
            while (i < length) {
                if (listener != listenerArr[i]) {
                    i++;
                } else {
                    return;
                }
            }
            Listener[] listenerArr2 = new Listener[(length + 1)];
            listenerArr2[0] = listener;
            System.arraycopy(listenerArr, 0, listenerArr2, 1, length);
            this.listeners = listenerArr2;
        }
    }

    public void close() {
        boolean z = this.isConnected;
        this.isConnected = false;
        this.tcp.close();
        if (!(this.udp == null || this.udp.connectedAddress == null)) {
            this.udp.close();
        }
        if (z) {
            notifyDisconnected();
        }
        setConnected(false);
    }

    public EndPoint getEndPoint() {
        return this.endPoint;
    }

    public int getID() {
        return this.id;
    }

    public InetSocketAddress getRemoteAddressTCP() {
        Socket socket;
        if (this.tcp.socketChannel == null || (socket = this.tcp.socketChannel.socket()) == null) {
            return null;
        }
        return (InetSocketAddress) socket.getRemoteSocketAddress();
    }

    public InetSocketAddress getRemoteAddressUDP() {
        InetSocketAddress inetSocketAddress = this.udp.connectedAddress;
        return inetSocketAddress != null ? inetSocketAddress : this.udpRemoteAddress;
    }

    public int getReturnTripTime() {
        return this.returnTripTime;
    }

    public int getTcpWriteBufferSize() {
        return this.tcp.writeBuffer.position();
    }

    /* access modifiers changed from: package-private */
    public void initialize(Serialization serialization, int i, int i2) {
        this.tcp = new TcpConnection(serialization, i, i2);
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public boolean isIdle() {
        return ((float) this.tcp.writeBuffer.position()) / ((float) this.tcp.writeBuffer.capacity()) < this.tcp.idleThreshold;
    }

    /* access modifiers changed from: package-private */
    public void notifyConnected() {
        for (Listener connected : this.listeners) {
            connected.connected(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyDisconnected() {
        for (Listener disconnected : this.listeners) {
            disconnected.disconnected(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyIdle() {
        Listener[] listenerArr = this.listeners;
        int i = 0;
        int length = listenerArr.length;
        while (i < length) {
            listenerArr[i].idle(this);
            if (isIdle()) {
                i++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyReceived(Object obj) {
        if (obj instanceof FrameworkMessage.Ping) {
            FrameworkMessage.Ping ping = (FrameworkMessage.Ping) obj;
            if (!ping.isReply) {
                ping.isReply = true;
                sendTCP(ping);
            } else if (ping.id == this.lastPingID - 1) {
                this.returnTripTime = (int) (System.currentTimeMillis() - this.lastPingSendTime);
            }
        }
        for (Listener received : this.listeners) {
            received.received(this, obj);
        }
    }

    public void removeListener(Listener listener) {
        int i = 0;
        if (listener == null) {
            throw new IllegalArgumentException("listener cannot be null.");
        }
        synchronized (this.listenerLock) {
            if (r5 != 0) {
                Listener[] listenerArr = new Listener[(r5 - 1)];
                for (Listener listener2 : this.listeners) {
                    if (listener != listener2) {
                        if (i != r5 - 1) {
                            int i2 = i + 1;
                            listenerArr[i] = listener2;
                            i = i2;
                        } else {
                            return;
                        }
                    }
                }
                this.listeners = listenerArr;
            }
        }
    }

    public int sendTCP(Object obj) {
        int i = 0;
        if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        }
        try {
            i = this.tcp.send(this, obj);
            if (i == 0) {
            }
        } catch (IOException e) {
            close();
        } catch (KryoNetException e2) {
            close();
        }
        return i;
    }

    public int sendUDP(Object obj) {
        int i = 0;
        if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        }
        InetSocketAddress inetSocketAddress = this.udpRemoteAddress;
        if (inetSocketAddress == null && this.udp != null) {
            inetSocketAddress = this.udp.connectedAddress;
        }
        if (inetSocketAddress == null && this.isConnected) {
            throw new IllegalStateException("Connection is not connected via UDP.");
        } else if (inetSocketAddress == null) {
            try {
                throw new SocketException("Connection is closed.");
            } catch (IOException e) {
                close();
            } catch (KryoNetException e2) {
                close();
            }
        } else {
            i = this.udp.send(this, obj, inetSocketAddress);
            if (i == 0) {
            }
            return i;
        }
    }

    public void setBufferPositionFix(boolean z) {
        this.tcp.bufferPositionFix = z;
    }

    /* access modifiers changed from: package-private */
    public void setConnected(boolean z) {
        this.isConnected = z;
        if (z && this.name == null) {
            this.name = "Connection " + this.id;
        }
    }

    public void setIdleThreshold(float f) {
        this.tcp.idleThreshold = f;
    }

    public void setKeepAliveTCP(int i) {
        this.tcp.keepAliveMillis = i;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setTimeout(int i) {
        this.tcp.timeoutMillis = i;
    }

    public String toString() {
        return this.name != null ? this.name : "Connection " + this.id;
    }

    public void updateReturnTripTime() {
        FrameworkMessage.Ping ping = new FrameworkMessage.Ping();
        int i = this.lastPingID;
        this.lastPingID = i + 1;
        ping.id = i;
        this.lastPingSendTime = System.currentTimeMillis();
        sendTCP(ping);
    }
}
