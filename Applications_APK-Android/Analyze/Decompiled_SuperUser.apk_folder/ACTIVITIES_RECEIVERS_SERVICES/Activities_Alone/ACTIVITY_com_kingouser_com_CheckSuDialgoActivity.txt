package com.kingouser.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.kingouser.com.util.DeviceInfoUtils;
import com.kingouser.com.util.LanguageUtils;
import com.kingouser.com.util.MySharedPreference;
import com.kingouser.com.util.NetworkUtils;
import com.kingouser.com.util.RC4EncodeUtils;

public class CheckSuDialgoActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private CheckSuDialgoActivity f3938a;
    @BindView(R.id.ff)
    Button button;
    @BindView(R.id.fe)
    TextView tvInfo;
    @BindView(R.id.fd)
    TextView tvTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.al);
        ButterKnife.bind(this);
        this.f3938a = this;
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    private void a() {
        String charSequence = this.tvInfo.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.tvInfo.getText());
        if (!"ru_RU".equalsIgnoreCase(LanguageUtils.getLocalDefault())) {
            int indexOf = charSequence.indexOf(RC4EncodeUtils.decry_RC4("d6b3757eaeac535c711cc21ae20af388", "string_key"));
            spannableStringBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.y)), indexOf, indexOf + 16, 33);
            this.tvInfo.setText(spannableStringBuilder);
        }
        this.tvTitle.setTextSize(DeviceInfoUtils.getTextSize(this, 60));
    }

    @OnClick({2131624163})
    public void onClick(View view) {
        NetworkUtils.openAppByWeb(this, RC4EncodeUtils.decry_RC4("c9b07620b6ff1214690ac544a700f2820cc490f000dd5dd2db4b0fa0b52f03da12cdb66cd4c3b7cbe496a50fdc810ca0367dbf0daaf97b3fe05b3691f8d119215b468596f1fc4ae061a93c9490cd", "string_key"));
        finish();
    }

    public static void a(Context context) {
        if (MySharedPreference.getWheaterOnResume(context, false)) {
            Intent intent = new Intent();
            intent.setFlags(268435456);
            intent.setClass(context, CheckSuDialgoActivity.class);
            context.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
