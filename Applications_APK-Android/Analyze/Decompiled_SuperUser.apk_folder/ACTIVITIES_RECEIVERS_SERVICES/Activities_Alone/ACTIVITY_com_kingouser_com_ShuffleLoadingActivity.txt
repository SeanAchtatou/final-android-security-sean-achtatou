package com.kingouser.com;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.duapps.ad.DuNativeAd;
import com.kingouser.com.fragment.VipFragment;
import com.kingouser.com.util.ShellUtils;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.manager.a;
import com.pureapps.cleaner.util.b;
import explosionfield.ExplosionField;

public class ShuffleLoadingActivity extends BaseActivity implements c {
    private int A;
    private int B;
    /* access modifiers changed from: private */
    public Drawable[] C;
    private int D;
    /* access modifiers changed from: private */
    public boolean E = false;
    /* access modifiers changed from: private */
    public ObjectAnimator F;
    /* access modifiers changed from: private */
    public AnimatorSet G;
    /* access modifiers changed from: private */
    public ExplosionField H;
    /* access modifiers changed from: private */
    public boolean I = false;
    /* access modifiers changed from: private */
    public Runnable J = new Runnable() {
        public void run() {
            ShuffleLoadingActivity.this.finish();
        }
    };
    private Runnable K = new Runnable() {
        public void run() {
            com.pureapps.cleaner.manager.a.a().b(13, ShuffleLoadingActivity.this.adLayout, new a.C0089a() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.kingouser.com.ShuffleLoadingActivity.a(com.kingouser.com.ShuffleLoadingActivity, boolean):boolean
                 arg types: [com.kingouser.com.ShuffleLoadingActivity, int]
                 candidates:
                  com.kingouser.com.ShuffleLoadingActivity.a(float, float):float
                  com.kingouser.com.ShuffleLoadingActivity.a(com.kingouser.com.ShuffleLoadingActivity, android.animation.AnimatorSet):android.animation.AnimatorSet
                  com.kingouser.com.ShuffleLoadingActivity.a(android.view.ViewGroup, android.view.View):void
                  android.support.v4.app.FragmentActivity.a(android.view.View, android.view.Menu):boolean
                  com.kingouser.com.ShuffleLoadingActivity.a(com.kingouser.com.ShuffleLoadingActivity, boolean):boolean */
                public void a(DuNativeAd duNativeAd) {
                    if (!ShuffleLoadingActivity.this.isFinishing()) {
                        ShuffleLoadingActivity.this.adLayout.setVisibility(0);
                        boolean unused = ShuffleLoadingActivity.this.E = true;
                        ShuffleLoadingActivity.this.n.removeCallbacks(ShuffleLoadingActivity.this.p);
                        ShuffleLoadingActivity.this.relativeLayout.removeAllViews();
                        ShuffleLoadingActivity.this.F.start();
                        ImageView imageView = (ImageView) ShuffleLoadingActivity.this.adLayout.findViewById(R.id.ij);
                        ShuffleLoadingActivity.this.adLayout.findViewById(R.id.is).setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                ShuffleLoadingActivity.this.H.a(ShuffleLoadingActivity.this.adLayout);
                                ShuffleLoadingActivity.this.n.postDelayed(ShuffleLoadingActivity.this.J, 1024);
                            }
                        });
                        ShuffleLoadingActivity.this.adLayout.findViewById(R.id.ir).setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                if (ShellUtils.checkSuVerison()) {
                                    com.pureapps.cleaner.analytic.a.a(ShuffleLoadingActivity.this).c(ShuffleLoadingActivity.this.o, "BtnGiftFloatAdRemove");
                                    VipFragment.a(2).show(ShuffleLoadingActivity.this.e(), "VipFragment");
                                    boolean unused = ShuffleLoadingActivity.this.I = true;
                                }
                            }
                        });
                        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(imageView, "scaleX", 1.0f, 1.2f);
                        ofFloat.setDuration(12000L);
                        ofFloat.setInterpolator(new LinearInterpolator());
                        ofFloat.setRepeatCount(-1);
                        ofFloat.setRepeatMode(2);
                        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(imageView, "scaleY", 1.0f, 1.2f);
                        ofFloat2.setDuration(12000L);
                        ofFloat2.setInterpolator(new LinearInterpolator());
                        ofFloat2.setRepeatCount(-1);
                        ofFloat2.setRepeatMode(2);
                        AnimatorSet unused2 = ShuffleLoadingActivity.this.G = new AnimatorSet();
                        ShuffleLoadingActivity.this.G.play(ofFloat).with(ofFloat2);
                        ShuffleLoadingActivity.this.G.start();
                    }
                    if (duNativeAd != null) {
                        duNativeAd.clearCache();
                        duNativeAd.destory();
                    }
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.kingouser.com.ShuffleLoadingActivity.a(com.kingouser.com.ShuffleLoadingActivity, boolean):boolean
                 arg types: [com.kingouser.com.ShuffleLoadingActivity, int]
                 candidates:
                  com.kingouser.com.ShuffleLoadingActivity.a(float, float):float
                  com.kingouser.com.ShuffleLoadingActivity.a(com.kingouser.com.ShuffleLoadingActivity, android.animation.AnimatorSet):android.animation.AnimatorSet
                  com.kingouser.com.ShuffleLoadingActivity.a(android.view.ViewGroup, android.view.View):void
                  android.support.v4.app.FragmentActivity.a(android.view.View, android.view.Menu):boolean
                  com.kingouser.com.ShuffleLoadingActivity.a(com.kingouser.com.ShuffleLoadingActivity, boolean):boolean */
                public void a() {
                    ShuffleLoadingActivity.this.adLayout.setVisibility(0);
                    ShuffleLoadingActivity.this.failed_layout.setVisibility(0);
                    boolean unused = ShuffleLoadingActivity.this.E = true;
                    ShuffleLoadingActivity.this.n.removeCallbacks(ShuffleLoadingActivity.this.p);
                    ShuffleLoadingActivity.this.relativeLayout.removeAllViews();
                }

                public void b() {
                    ShuffleLoadingActivity.this.finish();
                }
            });
        }
    };
    @BindView(R.id.lq)
    FrameLayout adLayout;
    @BindView(R.id.ls)
    RelativeLayout failed_layout;
    Handler n = new Handler();
    Runnable p = new Runnable() {
        public void run() {
            a aVar = new a(-ShuffleLoadingActivity.this.a(ShuffleLoadingActivity.this.q, ShuffleLoadingActivity.this.r), ShuffleLoadingActivity.this.a(ShuffleLoadingActivity.this.s, ShuffleLoadingActivity.this.t), ShuffleLoadingActivity.this.a(ShuffleLoadingActivity.this.u, ShuffleLoadingActivity.this.v), ShuffleLoadingActivity.this.a(ShuffleLoadingActivity.this.w, ShuffleLoadingActivity.this.x), -ShuffleLoadingActivity.this.a(ShuffleLoadingActivity.this.y, ShuffleLoadingActivity.this.z));
            ImageView imageView = new ImageView(ShuffleLoadingActivity.this.getApplicationContext());
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    try {
                        ShuffleLoadingActivity.this.H.a(view);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            });
            if (ShuffleLoadingActivity.this.C.length == 1) {
                imageView.setBackgroundDrawable(ShuffleLoadingActivity.this.C[0]);
            } else {
                imageView.setBackgroundDrawable(ShuffleLoadingActivity.this.C[(int) ShuffleLoadingActivity.this.a(0.0f, (float) (ShuffleLoadingActivity.this.C.length - 1))]);
            }
            ShuffleLoadingActivity.this.a(ShuffleLoadingActivity.this.relativeLayout, imageView, aVar);
            ShuffleLoadingActivity.this.n.postDelayed(ShuffleLoadingActivity.this.p, 500);
        }
    };
    /* access modifiers changed from: private */
    public float q;
    /* access modifiers changed from: private */
    public float r;
    @BindView(R.id.jc)
    RelativeLayout relativeLayout;
    /* access modifiers changed from: private */
    public float s;
    /* access modifiers changed from: private */
    public float t;
    /* access modifiers changed from: private */
    public float u;
    /* access modifiers changed from: private */
    public float v;
    /* access modifiers changed from: private */
    public float w;
    /* access modifiers changed from: private */
    public float x;
    /* access modifiers changed from: private */
    public float y;
    /* access modifiers changed from: private */
    public float z;

    public static void a(Context context) {
        Intent intent = new Intent(context, ShuffleLoadingActivity.class);
        intent.setFlags(65536);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.pureapps.cleaner.analytic.a.a(this).b(this.o, "ShuffleLoadingActivity");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.pureapps.cleaner.b.a.a(this);
        requestWindowFeature(1);
        this.I = false;
        this.B = getResources().getDisplayMetrics().widthPixels;
        this.A = getResources().getDisplayMetrics().heightPixels;
        setContentView((int) R.layout.cn);
        this.adLayout.setVisibility(4);
        this.relativeLayout.getLayoutParams().width = this.A;
        this.D = ((int) (Math.random() * 3.0d)) + 1;
        d(this.D);
        e(this.D);
        this.n.postDelayed(this.p, 500);
        this.n.postDelayed(this.K, 3000);
        this.E = false;
        this.F = ObjectAnimator.ofFloat(this.adLayout, "alpha", 0.0f, 1.0f);
        this.F.setDuration(1000L);
        this.H = ExplosionField.a(this);
        this.adLayout.setVisibility(4);
        this.adLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (!ShuffleLoadingActivity.this.E && ShuffleLoadingActivity.this.adLayout.isShown()) {
                    ShuffleLoadingActivity.this.adLayout.setVisibility(4);
                }
            }
        });
    }

    @OnClick({2131624399})
    public void onClick(View view) {
        if (view.getId() == R.id.lt) {
            this.H.a(this.failed_layout);
            this.n.postDelayed(this.J, 1024);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.E) {
            this.n.postDelayed(this.p, 500);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
        this.n.removeCallbacks(this.K);
        this.n.removeCallbacks(this.p);
        if (this.G != null) {
            this.G.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.removeCallbacks(this.p);
    }

    private void d(int i) {
        if (i == 1) {
            Drawable drawable = getResources().getDrawable(R.drawable.i7);
            this.C = new Drawable[4];
            this.C[0] = drawable;
            this.C[0].setColorFilter(-65410, PorterDuff.Mode.SRC_IN);
            this.C[1] = a(this, drawable, (float) a(127.0f), (float) a(64.0f));
            this.C[1].setColorFilter(-16718593, PorterDuff.Mode.SRC_IN);
            this.C[2] = a(this, drawable, (float) a(143.0f), (float) a(72.0f));
            this.C[2].setColorFilter(-16711780, PorterDuff.Mode.SRC_IN);
            this.C[3] = a(this, drawable, (float) a(135.0f), (float) a(68.0f));
            this.C[3].setColorFilter(-4096, PorterDuff.Mode.SRC_IN);
        } else if (i == 2) {
            Drawable drawable2 = getResources().getDrawable(R.drawable.i8);
            this.C = new Drawable[5];
            this.C[0] = drawable2;
            this.C[1] = a(this, drawable2, (float) a(74.0f), (float) a(79.0f));
            this.C[1].setColorFilter(-6291544, PorterDuff.Mode.SRC_IN);
            this.C[2] = a(this, drawable2, (float) a(74.0f), (float) a(79.0f));
            this.C[2].setColorFilter(-6752257, PorterDuff.Mode.SRC_IN);
            this.C[3] = a(this, drawable2, (float) a(74.0f), (float) a(79.0f));
            this.C[3].setColorFilter(-2420, PorterDuff.Mode.SRC_IN);
            this.C[4] = a(this, drawable2, (float) a(74.0f), (float) a(79.0f));
            this.C[4].setColorFilter(-26472, PorterDuff.Mode.SRC_IN);
        } else if (i == 3) {
            Drawable drawable3 = getResources().getDrawable(R.drawable.i9);
            this.C = new Drawable[1];
            this.C[0] = drawable3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Drawable a(Context context, Drawable drawable, float f2, float f3) {
        int intrinsicHeight = drawable.getIntrinsicHeight();
        int intrinsicWidth = drawable.getIntrinsicWidth();
        Bitmap b2 = b.b(drawable);
        Matrix matrix = new Matrix();
        matrix.postScale(f3 / ((float) intrinsicWidth), f2 / ((float) intrinsicHeight));
        return new BitmapDrawable(context.getResources(), Bitmap.createBitmap(b2, 0, 0, intrinsicWidth, intrinsicHeight, matrix, true));
    }

    /* access modifiers changed from: private */
    public float a(float f2, float f3) {
        return ((float) (Math.random() * ((double) ((f3 - f2) + 1.0f)))) + f2;
    }

    public int a(float f2) {
        return (int) ((getResources().getDisplayMetrics().density * f2) + 0.5f);
    }

    private void e(int i) {
        switch (i) {
            case 1:
                this.q = (float) a(98.0f);
                break;
            case 2:
                this.q = (float) a(79.0f);
                break;
            case 3:
                this.q = (float) a(62.0f);
                break;
        }
        this.r = this.q;
        this.s = ((float) this.A) / 4.0f;
        this.t = ((float) this.A) * 0.8f;
        this.u = (((float) this.B) + this.r) / 3.0f;
        this.v = ((float) this.B) * 1.2f;
        this.w = 0.0f;
        this.x = ((float) this.A) / 3.0f;
        this.y = (float) (this.A / 2);
        this.z = (float) this.A;
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 34:
                if (this.I) {
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public class a {

        /* renamed from: a  reason: collision with root package name */
        public float f4092a;

        /* renamed from: b  reason: collision with root package name */
        public float f4093b;

        /* renamed from: c  reason: collision with root package name */
        public float f4094c;

        /* renamed from: d  reason: collision with root package name */
        public float f4095d;

        /* renamed from: e  reason: collision with root package name */
        public float f4096e;

        public a(float f2, float f3, float f4, float f5, float f6) {
            this.f4092a = f2;
            this.f4093b = f3;
            this.f4094c = f4;
            this.f4095d = f5;
            this.f4096e = f6;
        }
    }

    /* access modifiers changed from: private */
    public void a(final ViewGroup viewGroup, final ImageView imageView, final a aVar) {
        if (viewGroup != null) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.leftMargin = (-this.B) / 2;
            viewGroup.addView(imageView, layoutParams);
        }
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(3000L);
        valueAnimator.setObjectValues(new PointF(aVar.f4092a, aVar.f4093b));
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setEvaluator(new TypeEvaluator<PointF>() {
            /* renamed from: a */
            public PointF evaluate(float f2, PointF pointF, PointF pointF2) {
                return b(f2, pointF, pointF2);
            }

            public PointF b(float f2, PointF pointF, PointF pointF2) {
                PointF pointF3 = new PointF();
                float f3 = 3.0f * f2;
                pointF3.x = aVar.f4092a + (aVar.f4094c * f3);
                pointF3.y = (f3 * aVar.f4095d) + aVar.f4093b + (0.5f * aVar.f4096e * f3 * f3);
                return pointF3;
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                ShuffleLoadingActivity.this.a(viewGroup, imageView);
            }

            public void onAnimationCancel(Animator animator) {
                ShuffleLoadingActivity.this.a(viewGroup, imageView);
            }
        });
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @SuppressLint({"NewApi"})
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                PointF pointF = (PointF) valueAnimator.getAnimatedValue();
                if (!ShuffleLoadingActivity.this.isFinishing()) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                    if (layoutParams == null) {
                        layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                    }
                    layoutParams.leftMargin = (int) pointF.x;
                    layoutParams.topMargin = (int) pointF.y;
                    imageView.setLayoutParams(layoutParams);
                    return;
                }
                imageView.setX(pointF.x);
                imageView.setY(pointF.y);
            }
        });
        valueAnimator.start();
    }

    /* access modifiers changed from: private */
    public void a(final ViewGroup viewGroup, final View view) {
        if (viewGroup != null) {
            viewGroup.post(new Runnable() {
                public void run() {
                    viewGroup.removeView(view);
                }
            });
        }
    }
}
