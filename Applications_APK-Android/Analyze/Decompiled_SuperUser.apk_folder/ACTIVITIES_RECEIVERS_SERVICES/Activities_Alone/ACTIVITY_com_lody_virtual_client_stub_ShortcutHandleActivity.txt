package com.lody.virtual.client.stub;

import android.app.Activity;

public class ShortcutHandleActivity extends Activity {
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r1 = 0
            r3 = 0
            super.onCreate(r7)
            r6.finish()
            android.content.Intent r0 = r6.getIntent()
            if (r0 != 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            java.lang.String r2 = "_VA_|_user_id_"
            int r3 = r0.getIntExtra(r2, r3)
            java.lang.String r2 = "_VA_|_splash_"
            java.lang.String r2 = r0.getStringExtra(r2)
            java.lang.String r4 = "_VA_|_uri_"
            java.lang.String r4 = r0.getStringExtra(r4)
            if (r2 == 0) goto L_0x004e
            r0 = 0
            android.content.Intent r0 = android.content.Intent.parseUri(r2, r0)     // Catch:{ URISyntaxException -> 0x004a }
            r2 = r0
        L_0x0029:
            if (r4 == 0) goto L_0x0054
            r0 = 0
            android.content.Intent r0 = android.content.Intent.parseUri(r4, r0)     // Catch:{ URISyntaxException -> 0x0050 }
        L_0x0030:
            if (r0 == 0) goto L_0x000e
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 15
            if (r4 < r5) goto L_0x003b
            r0.setSelector(r1)
        L_0x003b:
            if (r2 != 0) goto L_0x0056
            com.lody.virtual.client.ipc.VActivityManager r1 = com.lody.virtual.client.ipc.VActivityManager.get()     // Catch:{ Throwable -> 0x0045 }
            r1.startActivity(r0, r3)     // Catch:{ Throwable -> 0x0045 }
            goto L_0x000e
        L_0x0045:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x000e
        L_0x004a:
            r0 = move-exception
            r0.printStackTrace()
        L_0x004e:
            r2 = r1
            goto L_0x0029
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0054:
            r0 = r1
            goto L_0x0030
        L_0x0056:
            java.lang.String r1 = "android.intent.extra.INTENT"
            r2.putExtra(r1, r0)
            java.lang.String r0 = "android.intent.extra.CC"
            r2.putExtra(r0, r3)
            r6.startActivity(r2)
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.client.stub.ShortcutHandleActivity.onCreate(android.os.Bundle):void");
    }
}
