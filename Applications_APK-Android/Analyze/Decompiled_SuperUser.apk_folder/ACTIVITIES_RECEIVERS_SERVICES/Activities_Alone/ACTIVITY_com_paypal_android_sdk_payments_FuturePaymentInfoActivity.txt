package com.paypal.android.sdk.payments;

import android.app.Activity;
import android.os.Bundle;
import com.paypal.android.sdk.ev;
import com.paypal.android.sdk.fs;

public final class FuturePaymentInfoActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private i f5050a;

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTheme(16973934);
        requestWindowFeature(8);
        this.f5050a = new i(this, (ag) getIntent().getExtras().getSerializable("com.paypal.details.scope"));
        setContentView(this.f5050a.f5309a);
        cf.a(this, this.f5050a.f5310b, (fs) null);
        this.f5050a.f5311c.setText(ev.a(fs.BACK_BUTTON));
        this.f5050a.f5311c.setOnClickListener(new h(this));
    }
}
