package com.kingouser.com;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.OnClick;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.pureapps.cleaner.analytic.a;

public class PaymentCompleteActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.cc);
        ActionBar f2 = f();
        if (f2 != null) {
            f2.a(true);
        }
        j();
    }

    private void j() {
        ((TextView) findViewById(R.id.ku)).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/booster_number_font.otf"));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(FirebaseAnalytics.getInstance(this), "PaymentCompletePage");
    }

    @OnClick({2131624364})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.kv /*2131624364*/:
                k();
                return;
            default:
                return;
        }
    }

    private void k() {
        a.a(this).b(FirebaseAnalytics.getInstance(this), "BtnPaymentCompleteGoRootManagerClick");
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), PolicAuthorityActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
