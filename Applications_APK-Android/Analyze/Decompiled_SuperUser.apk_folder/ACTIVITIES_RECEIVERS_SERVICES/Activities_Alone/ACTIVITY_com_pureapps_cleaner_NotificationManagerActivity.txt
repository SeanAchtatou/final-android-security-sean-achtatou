package com.pureapps.cleaner;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.kingouser.com.R;
import com.pureapps.cleaner.adapter.NotificationAdapter;
import com.pureapps.cleaner.analytic.a;
import com.pureapps.cleaner.b.c;
import com.pureapps.cleaner.bean.NotificationInfo;
import com.pureapps.cleaner.manager.f;
import com.pureapps.cleaner.service.NotificationMonitorService;
import com.pureapps.cleaner.util.i;
import com.pureapps.cleaner.view.BBaseActivity;
import com.pureapps.cleaner.view.ItemTouch.NotificationItemTouchHelperCallback;
import java.util.ArrayList;

public class NotificationManagerActivity extends BBaseActivity implements c {
    @BindView(R.id.dz)
    Button mCleanAllBtn;
    @BindView(R.id.dx)
    LinearLayout mDataLayout = null;
    @BindView(R.id.e0)
    TextView mNoNotificationText;
    @BindView(R.id.dy)
    RecyclerView mRecyclerView;
    private boolean n = false;
    /* access modifiers changed from: private */
    public NotificationAdapter o;
    private ItemTouchHelper p;
    /* access modifiers changed from: private */
    public boolean q = false;
    private ArrayList<NotificationInfo> r;
    /* access modifiers changed from: private */
    public Handler s = new Handler();
    private final int u = 400;
    /* access modifiers changed from: private */
    public int v = 0;
    /* access modifiers changed from: private */
    public Runnable w = new Runnable() {
        public void run() {
            if (NotificationMonitorService.f5847a.size() > 0 && NotificationManagerActivity.this.o != null) {
                NotificationManagerActivity.this.o.a(0);
            }
            NotificationManagerActivity.this.k();
            if (NotificationMonitorService.f5847a.size() > 0) {
                NotificationManagerActivity.this.s.postDelayed(NotificationManagerActivity.this.w, 400);
                return;
            }
            BoostResultActivity.a(NotificationManagerActivity.this, 4, String.valueOf(NotificationManagerActivity.this.v));
            NotificationManagerActivity.this.finish();
        }
    };

    public static void a(Context context, boolean z) {
        Intent intent = new Intent(context, NotificationManagerActivity.class);
        if (!NotificationMonitorService.b(context)) {
            intent.addFlags(335577088);
        }
        intent.putExtra("canbackhome", z);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a.a(this).b(this.t, "NotificationManagerView");
        if (!i.a(this).l()) {
            NotificationMonitorService.f5847a.clear();
            finish();
        }
        if (this.o != null && this.o.getItemCount() > 0 && i.a(this).i()) {
            new GuideDialog().show(e(), "GuideDialog");
            i.a(this).j();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.pureapps.cleaner.b.a.a(this);
        this.n = getIntent().getBooleanExtra("canbackhome", true);
        setContentView((int) R.layout.a9);
        a((Toolbar) findViewById(R.id.cu));
        f().a(true);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.ct);
        if (Build.VERSION.SDK_INT >= 19) {
            appBarLayout.setPadding(0, m(), 0, 0);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.b(1);
        this.mRecyclerView.setLayoutManager(linearLayoutManager);
        this.mRecyclerView.setHasFixedSize(true);
        try {
            this.r = (ArrayList) NotificationMonitorService.f5847a.clone();
        } catch (Exception e2) {
            this.r = new ArrayList<>();
        }
        this.o = new NotificationAdapter(this.r, this, this.mRecyclerView);
        this.mRecyclerView.setAdapter(this.o);
        this.p = new ItemTouchHelper(new NotificationItemTouchHelperCallback(this.o));
        this.p.a(this.mRecyclerView);
        k();
        this.mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.pureapps.cleaner.NotificationManagerActivity.a(com.pureapps.cleaner.NotificationManagerActivity, boolean):boolean
             arg types: [com.pureapps.cleaner.NotificationManagerActivity, int]
             candidates:
              com.pureapps.cleaner.NotificationManagerActivity.a(android.content.Context, boolean):void
              android.support.v4.app.FragmentActivity.a(android.view.View, android.view.Menu):boolean
              com.pureapps.cleaner.NotificationManagerActivity.a(com.pureapps.cleaner.NotificationManagerActivity, boolean):boolean */
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                    case 2:
                        boolean unused = NotificationManagerActivity.this.q = true;
                        break;
                    case 1:
                    case 3:
                        boolean unused2 = NotificationManagerActivity.this.q = false;
                        break;
                }
                return false;
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.f8345e, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.pureapps.cleaner.b.a.b(this);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                if (this.n) {
                }
                finish();
                return true;
            case R.id.my /*2131624441*/:
                NotificationSetActivity.a(this);
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onBackPressed() {
        if (this.n) {
        }
        super.onBackPressed();
    }

    public void a(int i, long j, Object obj) {
        switch (i) {
            case 20:
                if (j()) {
                    try {
                        this.r = (ArrayList) NotificationMonitorService.f5847a.clone();
                    } catch (Exception e2) {
                        this.r = new ArrayList<>();
                    }
                    if (this.o != null && !this.q) {
                        this.o.a(this.r);
                        this.o.notifyDataSetChanged();
                    }
                }
                k();
                return;
            case 21:
                k();
                return;
            default:
                return;
        }
    }

    private boolean j() {
        if (this.r == null || this.r.size() == 0) {
            return true;
        }
        if (this.r.size() != NotificationMonitorService.f5847a.size()) {
            return true;
        }
        int i = 0;
        while (i < this.r.size() && !this.r.get(i).d().equalsIgnoreCase(NotificationMonitorService.f5847a.get(i).d())) {
            i++;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.s.removeCallbacks(this.w);
    }

    @OnClick({2131624109})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dz /*2131624109*/:
                a.a(this).c(this.t, "BtnNotificationCleanerAll");
                com.pureapps.cleaner.manager.a.a().a(9);
                this.v = this.o.getItemCount();
                this.s.post(this.w);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.o.getItemCount() == 0) {
            this.mDataLayout.setVisibility(8);
            f.a(this).d();
            this.mNoNotificationText.setVisibility(0);
            return;
        }
        this.mDataLayout.setVisibility(0);
        this.mNoNotificationText.setVisibility(8);
    }
}
