package com.kingouser.com.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kingouser.com.util.MySharedPreference;

public class SystemLanguageChangedReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.LOCALE_CHANGED")) {
            MySharedPreference.setWeatherSystemLanguageChanged(context, true);
        }
    }
}
