package mirror;

import java.lang.reflect.Field;

public class RefDouble {
    private Field field;

    public RefDouble(Class cls, Field field2) {
        this.field = cls.getDeclaredField(field2.getName());
        this.field.setAccessible(true);
    }

    public double get(Object obj) {
        try {
            return this.field.getDouble(obj);
        } catch (Exception e2) {
            return 0.0d;
        }
    }

    public void set(Object obj, double d2) {
        try {
            this.field.setDouble(obj, d2);
        } catch (Exception e2) {
        }
    }
}
