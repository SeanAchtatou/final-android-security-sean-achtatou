package mirror;

import java.lang.reflect.Field;

public class RefFloat {
    private Field field;

    public RefFloat(Class cls, Field field2) {
        this.field = cls.getDeclaredField(field2.getName());
        this.field.setAccessible(true);
    }

    public float get(Object obj) {
        try {
            return this.field.getFloat(obj);
        } catch (Exception e2) {
            return 0.0f;
        }
    }

    public void set(Object obj, float f2) {
        try {
            this.field.setFloat(obj, f2);
        } catch (Exception e2) {
        }
    }
}
