package mirror.android.os;

import android.os.Handler;
import mirror.RefClass;
import mirror.RefObject;

public class Handler {
    public static Class<?> TYPE = RefClass.load(Handler.class, "android.os.Handler");
    public static RefObject<Handler.Callback> mCallback;
}
