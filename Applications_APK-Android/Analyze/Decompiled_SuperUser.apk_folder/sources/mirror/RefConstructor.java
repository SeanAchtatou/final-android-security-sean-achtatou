package mirror;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class RefConstructor<T> {
    private Constructor<?> ctor;

    public RefConstructor(Class<?> cls, Field field) {
        if (field.isAnnotationPresent(MethodParams.class)) {
            this.ctor = cls.getDeclaredConstructor(((MethodParams) field.getAnnotation(MethodParams.class)).value());
        } else if (field.isAnnotationPresent(MethodReflectParams.class)) {
            String[] value = ((MethodReflectParams) field.getAnnotation(MethodReflectParams.class)).value();
            Class[] clsArr = new Class[value.length];
            int i = 0;
            while (i < value.length) {
                try {
                    clsArr[i] = Class.forName(value[i]);
                    i++;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            this.ctor = cls.getDeclaredConstructor(clsArr);
        } else {
            this.ctor = cls.getDeclaredConstructor(new Class[0]);
        }
        if (this.ctor != null && !this.ctor.isAccessible()) {
            this.ctor.setAccessible(true);
        }
    }

    public T newInstance() {
        try {
            return this.ctor.newInstance(new Object[0]);
        } catch (Exception e2) {
            return null;
        }
    }

    public T newInstance(Object... objArr) {
        try {
            return this.ctor.newInstance(objArr);
        } catch (Exception e2) {
            return null;
        }
    }
}
