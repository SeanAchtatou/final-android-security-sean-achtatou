package cli.a;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: HTTPUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f2322a = (!a.class.desiredAssertionStatus());

    public static String a(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str.toString()).openConnection();
            httpURLConnection.setConnectTimeout(io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT);
            httpURLConnection.setRequestMethod("GET");
            if (httpURLConnection.getResponseCode() == 200) {
                return a(httpURLConnection.getInputStream());
            }
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    public static String a(InputStream inputStream) {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
        }
    }

    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v3 */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARN: Type inference failed for: r4v6 */
    /* JADX WARN: Type inference failed for: r4v7 */
    /* JADX WARN: Type inference failed for: r4v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b8 A[SYNTHETIC, Splitter:B:22:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bd A[SYNTHETIC, Splitter:B:25:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00f3 A[SYNTHETIC, Splitter:B:52:0x00f3] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f8 A[SYNTHETIC, Splitter:B:55:0x00f8] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r13, java.lang.String r14) {
        /*
            r1 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "downloadurl:"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r13)
            java.lang.String r0 = r0.toString()
            com.pureapps.cleaner.util.f.a(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "loadlfile:"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r14)
            java.lang.String r0 = r0.toString()
            com.pureapps.cleaner.util.f.a(r0)
            r6 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0118, all -> 0x00e9 }
            r0.<init>(r13)     // Catch:{ Exception -> 0x0118, all -> 0x00e9 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0118, all -> 0x00e9 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0118, all -> 0x00e9 }
            r2 = 10000(0x2710, float:1.4013E-41)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x011d, all -> 0x0104 }
            java.lang.String r2 = "GET"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x011d, all -> 0x0104 }
            r0.connect()     // Catch:{ Exception -> 0x011d, all -> 0x0104 }
            int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x011d, all -> 0x0104 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x012c
            java.io.InputStream r5 = r0.getInputStream()     // Catch:{ Exception -> 0x011d, all -> 0x0104 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0123, all -> 0x010a }
            r3.<init>(r14)     // Catch:{ Exception -> 0x0123, all -> 0x010a }
            int r2 = r0.getContentLength()     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            long r8 = (long) r2     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0091, all -> 0x010f }
        L_0x0062:
            int r4 = r5.read(r2)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            r10 = -1
            if (r4 == r10) goto L_0x00c2
            r10 = 0
            r3.write(r2, r10, r4)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            long r10 = (long) r4     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            long r6 = r6 + r10
            r10 = 100
            long r10 = r10 * r6
            long r10 = r10 / r8
            int r4 = (int) r10     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            r10.<init>()     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            java.lang.String r11 = "downloadFile:"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            java.lang.StringBuilder r4 = r10.append(r4)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            java.lang.String r10 = "%"
            java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            com.pureapps.cleaner.util.f.a(r4)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            goto L_0x0062
        L_0x0091:
            r2 = move-exception
            r4 = r5
            r12 = r3
            r3 = r0
            r0 = r2
            r2 = r12
        L_0x0097:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0113 }
            r5.<init>()     // Catch:{ all -> 0x0113 }
            java.lang.String r6 = "downloadFile:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0113 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0113 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x0113 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0113 }
            com.pureapps.cleaner.util.f.b(r0)     // Catch:{ all -> 0x0113 }
            if (r3 == 0) goto L_0x00b6
            r3.disconnect()
        L_0x00b6:
            if (r4 == 0) goto L_0x00bb
            r4.close()     // Catch:{ IOException -> 0x00fe }
        L_0x00bb:
            if (r2 == 0) goto L_0x012a
            r2.close()     // Catch:{ IOException -> 0x00e6 }
            r0 = r1
        L_0x00c1:
            return r0
        L_0x00c2:
            r10 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r10)     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            r5.close()     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            r3.close()     // Catch:{ Exception -> 0x0091, all -> 0x010f }
            int r2 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r2 != 0) goto L_0x00d2
            r1 = 1
        L_0x00d2:
            if (r0 == 0) goto L_0x00d7
            r0.disconnect()
        L_0x00d7:
            if (r5 == 0) goto L_0x00dc
            r5.close()     // Catch:{ IOException -> 0x00fc }
        L_0x00dc:
            if (r3 == 0) goto L_0x012a
            r3.close()     // Catch:{ IOException -> 0x00e3 }
            r0 = r1
            goto L_0x00c1
        L_0x00e3:
            r0 = move-exception
            r0 = r1
            goto L_0x00c1
        L_0x00e6:
            r0 = move-exception
            r0 = r1
            goto L_0x00c1
        L_0x00e9:
            r0 = move-exception
            r3 = r4
            r5 = r4
        L_0x00ec:
            if (r4 == 0) goto L_0x00f1
            r4.disconnect()
        L_0x00f1:
            if (r5 == 0) goto L_0x00f6
            r5.close()     // Catch:{ IOException -> 0x0100 }
        L_0x00f6:
            if (r3 == 0) goto L_0x00fb
            r3.close()     // Catch:{ IOException -> 0x0102 }
        L_0x00fb:
            throw r0
        L_0x00fc:
            r0 = move-exception
            goto L_0x00dc
        L_0x00fe:
            r0 = move-exception
            goto L_0x00bb
        L_0x0100:
            r1 = move-exception
            goto L_0x00f6
        L_0x0102:
            r1 = move-exception
            goto L_0x00fb
        L_0x0104:
            r1 = move-exception
            r3 = r4
            r5 = r4
            r4 = r0
            r0 = r1
            goto L_0x00ec
        L_0x010a:
            r1 = move-exception
            r3 = r4
            r4 = r0
            r0 = r1
            goto L_0x00ec
        L_0x010f:
            r1 = move-exception
            r4 = r0
            r0 = r1
            goto L_0x00ec
        L_0x0113:
            r0 = move-exception
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x00ec
        L_0x0118:
            r0 = move-exception
            r2 = r4
            r3 = r4
            goto L_0x0097
        L_0x011d:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r4
            goto L_0x0097
        L_0x0123:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r4
            r4 = r5
            goto L_0x0097
        L_0x012a:
            r0 = r1
            goto L_0x00c1
        L_0x012c:
            r3 = r4
            r5 = r4
            goto L_0x00d2
        */
        throw new UnsupportedOperationException("Method not decompiled: cli.a.a.a(java.lang.String, java.lang.String):boolean");
    }
}
