package a;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: AppRunTimeManager */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f12a;

    /* renamed from: b  reason: collision with root package name */
    private final List f13b = new LinkedList();

    /* renamed from: c  reason: collision with root package name */
    private final List f14c = new ArrayList();

    /* renamed from: d  reason: collision with root package name */
    private Handler f15d = new Handler(Looper.getMainLooper());

    /* renamed from: e  reason: collision with root package name */
    private boolean f16e = false;

    private b() {
    }

    public static b a() {
        if (f12a == null) {
            Class<b> cls = b.class;
            synchronized (b.class) {
                if (f12a == null) {
                    f12a = new b();
                }
            }
        }
        return f12a;
    }

    public long b() {
        return c();
    }

    private synchronized long c() {
        long b2;
        long j = 0;
        synchronized (this) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long a2 = a.a();
            a.a(elapsedRealtime);
            if (0 != a2) {
                j = elapsedRealtime >= a2 ? elapsedRealtime - a2 : elapsedRealtime;
            }
            b2 = j + a.b();
            a.b(b2);
        }
        return b2;
    }
}
