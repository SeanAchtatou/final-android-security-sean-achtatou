package a;

import android.content.SharedPreferences;

/* compiled from: AppRunTimeConfig */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences f11a;

    public static void a(long j) {
        f11a.edit().putLong("art_last_sys_elap_time", j).apply();
    }

    public static long a() {
        return f11a.getLong("art_last_sys_elap_time", 0);
    }

    public static void b(long j) {
        f11a.edit().putLong("art_app_used_time", j).apply();
    }

    public static long b() {
        return f11a.getLong("art_app_used_time", 0);
    }
}
