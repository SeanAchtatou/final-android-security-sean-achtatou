package com.squareup.okhttp;

import com.squareup.okhttp.internal.a.g;
import com.squareup.okhttp.internal.c;
import com.squareup.okhttp.p;
import com.squareup.okhttp.s;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;

/* compiled from: Call */
public class e {

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f6369a;

    /* renamed from: b  reason: collision with root package name */
    s f6370b;

    /* renamed from: c  reason: collision with root package name */
    g f6371c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final r f6372d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f6373e;

    protected e(r rVar, s sVar) {
        this.f6372d = rVar.w();
        this.f6370b = sVar;
    }

    public u a() {
        synchronized (this) {
            if (this.f6373e) {
                throw new IllegalStateException("Already Executed");
            }
            this.f6373e = true;
        }
        try {
            this.f6372d.r().a(this);
            u a2 = a(false);
            if (a2 != null) {
                return a2;
            }
            throw new IOException("Canceled");
        } finally {
            this.f6372d.r().b(this);
        }
    }

    /* access modifiers changed from: package-private */
    public Object b() {
        return this.f6370b.g();
    }

    public void c() {
        this.f6369a = true;
        if (this.f6371c != null) {
            this.f6371c.i();
        }
    }

    /* compiled from: Call */
    final class b extends c {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ e f6378a;

        /* renamed from: c  reason: collision with root package name */
        private final f f6379c;

        /* renamed from: d  reason: collision with root package name */
        private final boolean f6380d;

        /* access modifiers changed from: package-private */
        public String a() {
            return this.f6378a.f6370b.a().getHost();
        }

        /* access modifiers changed from: package-private */
        public Object b() {
            return this.f6378a.f6370b.g();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.f6378a.c();
        }

        /* access modifiers changed from: package-private */
        public e d() {
            return this.f6378a;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        /* access modifiers changed from: protected */
        public void e() {
            boolean z = true;
            try {
                u a2 = this.f6378a.a(this.f6380d);
                if (this.f6378a.f6369a) {
                    try {
                        this.f6379c.a(this.f6378a.f6370b, new IOException("Canceled"));
                    } catch (IOException e2) {
                        e = e2;
                    }
                } else {
                    this.f6379c.a(a2);
                }
                this.f6378a.f6372d.r().a(this);
            } catch (IOException e3) {
                e = e3;
                z = false;
                if (z) {
                    try {
                        com.squareup.okhttp.internal.a.f6394a.log(Level.INFO, "Callback failure for " + this.f6378a.d(), (Throwable) e);
                    } catch (Throwable th) {
                        this.f6378a.f6372d.r().a(this);
                        throw th;
                    }
                } else {
                    this.f6379c.a(this.f6378a.f6371c.d(), e);
                }
                this.f6378a.f6372d.r().a(this);
            }
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        String str = this.f6369a ? "canceled call" : "call";
        try {
            return str + " to " + new URL(this.f6370b.a(), "/...").toString();
        } catch (MalformedURLException e2) {
            return str;
        }
    }

    /* access modifiers changed from: private */
    public u a(boolean z) {
        return new a(0, this.f6370b, z).a(this.f6370b);
    }

    /* compiled from: Call */
    class a implements p.a {

        /* renamed from: b  reason: collision with root package name */
        private final int f6375b;

        /* renamed from: c  reason: collision with root package name */
        private final s f6376c;

        /* renamed from: d  reason: collision with root package name */
        private final boolean f6377d;

        a(int i, s sVar, boolean z) {
            this.f6375b = i;
            this.f6376c = sVar;
            this.f6377d = z;
        }

        public u a(s sVar) {
            if (this.f6375b >= e.this.f6372d.u().size()) {
                return e.this.a(sVar, this.f6377d);
            }
            return e.this.f6372d.u().get(this.f6375b).a(new a(this.f6375b + 1, sVar, this.f6377d));
        }
    }

    /* access modifiers changed from: package-private */
    public u a(s sVar, boolean z) {
        s sVar2;
        t f2 = sVar.f();
        if (f2 != null) {
            s.a h2 = sVar.h();
            q a2 = f2.a();
            if (a2 != null) {
                h2.a("Content-Type", a2.toString());
            }
            long b2 = f2.b();
            if (b2 != -1) {
                h2.a("Content-Length", Long.toString(b2));
                h2.b("Transfer-Encoding");
            } else {
                h2.a("Transfer-Encoding", "chunked");
                h2.b("Content-Length");
            }
            sVar2 = h2.b();
        } else {
            sVar2 = sVar;
        }
        this.f6371c = new g(this.f6372d, sVar2, false, false, z, null, null, null, null);
        int i = 0;
        while (!this.f6369a) {
            try {
                this.f6371c.a();
                this.f6371c.k();
                u e2 = this.f6371c.e();
                s l = this.f6371c.l();
                if (l != null) {
                    int i2 = i + 1;
                    if (i2 > 20) {
                        throw new ProtocolException("Too many follow-up requests: " + i2);
                    }
                    if (!this.f6371c.b(l.a())) {
                        this.f6371c.h();
                    }
                    this.f6371c = new g(this.f6372d, l, false, false, z, this.f6371c.j(), null, null, e2);
                    i = i2;
                } else if (z) {
                    return e2;
                } else {
                    this.f6371c.h();
                    return e2;
                }
            } catch (IOException e3) {
                g a3 = this.f6371c.a(e3, (okio.p) null);
                if (a3 != null) {
                    this.f6371c = a3;
                } else {
                    throw e3;
                }
            }
        }
        this.f6371c.h();
        return null;
    }
}
