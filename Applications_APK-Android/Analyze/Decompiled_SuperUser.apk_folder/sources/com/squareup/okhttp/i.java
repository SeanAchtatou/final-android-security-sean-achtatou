package com.squareup.okhttp;

import com.squareup.okhttp.internal.a.e;
import com.squareup.okhttp.internal.a.g;
import com.squareup.okhttp.internal.a.o;
import com.squareup.okhttp.internal.a.q;
import com.squareup.okhttp.internal.b.b;
import com.squareup.okhttp.internal.f;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.internal.spdy.m;
import com.squareup.okhttp.s;
import io.fabric.sdk.android.services.common.a;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;

/* compiled from: Connection */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final j f6386a;

    /* renamed from: b  reason: collision with root package name */
    private final w f6387b;

    /* renamed from: c  reason: collision with root package name */
    private Socket f6388c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f6389d = false;

    /* renamed from: e  reason: collision with root package name */
    private e f6390e;

    /* renamed from: f  reason: collision with root package name */
    private m f6391f;

    /* renamed from: g  reason: collision with root package name */
    private Protocol f6392g = Protocol.HTTP_1_1;

    /* renamed from: h  reason: collision with root package name */
    private long f6393h;
    private n i;
    private int j;
    private Object k;

    public i(j jVar, w wVar) {
        this.f6386a = jVar;
        this.f6387b = wVar;
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (!k()) {
            synchronized (this.f6386a) {
                if (this.k != null) {
                    throw new IllegalStateException("Connection already has an owner!");
                }
                this.k = obj;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        boolean z;
        synchronized (this.f6386a) {
            if (this.k == null) {
                z = false;
            } else {
                this.k = null;
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b(Object obj) {
        if (k()) {
            throw new IllegalStateException();
        }
        synchronized (this.f6386a) {
            if (this.k == obj) {
                this.k = null;
                this.f6388c.close();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, s sVar) {
        if (this.f6389d) {
            throw new IllegalStateException("already connected");
        }
        if (this.f6387b.f6741b.type() == Proxy.Type.DIRECT || this.f6387b.f6741b.type() == Proxy.Type.HTTP) {
            this.f6388c = this.f6387b.f6740a.f6348d.createSocket();
        } else {
            this.f6388c = new Socket(this.f6387b.f6741b);
        }
        this.f6388c.setSoTimeout(i3);
        f.a().a(this.f6388c, this.f6387b.f6742c, i2);
        if (this.f6387b.f6740a.f6349e != null) {
            a(sVar, i3, i4);
        } else {
            this.f6390e = new e(this.f6386a, this, this.f6388c);
        }
        this.f6389d = true;
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar, Object obj, s sVar) {
        a(obj);
        if (!b()) {
            a(rVar.a(), rVar.b(), rVar.c(), a(sVar));
            if (k()) {
                rVar.m().b(this);
            }
            rVar.q().b(c());
        }
        a(rVar.b(), rVar.c());
    }

    private s a(s sVar) {
        if (!this.f6387b.c()) {
            return null;
        }
        String host = sVar.a().getHost();
        int a2 = h.a(sVar.a());
        s.a a3 = new s.a().a(new URL("https", host, a2, "/")).a("Host", a2 == h.a("https") ? host : host + ":" + a2).a("Proxy-Connection", "Keep-Alive");
        String a4 = sVar.a(a.HEADER_USER_AGENT);
        if (a4 != null) {
            a3.a(a.HEADER_USER_AGENT, a4);
        }
        String a5 = sVar.a("Proxy-Authorization");
        if (a5 != null) {
            a3.a("Proxy-Authorization", a5);
        }
        return a3.b();
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    private void a(s sVar, int i2, int i3) {
        String b2;
        f a2 = f.a();
        if (sVar != null) {
            b(sVar, i2, i3);
        }
        this.f6388c = this.f6387b.f6740a.f6349e.createSocket(this.f6388c, this.f6387b.f6740a.f6346b, this.f6387b.f6740a.f6347c, true);
        SSLSocket sSLSocket = (SSLSocket) this.f6388c;
        this.f6387b.f6743d.a(sSLSocket, this.f6387b);
        try {
            sSLSocket.startHandshake();
            if (this.f6387b.f6743d.d() && (b2 = a2.b(sSLSocket)) != null) {
                this.f6392g = Protocol.a(b2);
            }
            a2.a(sSLSocket);
            this.i = n.a(sSLSocket.getSession());
            if (!this.f6387b.f6740a.f6350f.verify(this.f6387b.f6740a.f6346b, sSLSocket.getSession())) {
                X509Certificate x509Certificate = (X509Certificate) sSLSocket.getSession().getPeerCertificates()[0];
                throw new SSLPeerUnverifiedException("Hostname " + this.f6387b.f6740a.f6346b + " not verified:" + "\n    certificate: " + g.a((Certificate) x509Certificate) + "\n    DN: " + x509Certificate.getSubjectDN().getName() + "\n    subjectAltNames: " + b.a(x509Certificate));
            }
            this.f6387b.f6740a.f6351g.a(this.f6387b.f6740a.f6346b, this.i.b());
            if (this.f6392g == Protocol.SPDY_3 || this.f6392g == Protocol.HTTP_2) {
                sSLSocket.setSoTimeout(0);
                this.f6391f = new m.a(this.f6387b.f6740a.a(), true, this.f6388c).a(this.f6392g).a();
                this.f6391f.e();
                return;
            }
            this.f6390e = new e(this.f6386a, this, this.f6388c);
        } catch (Throwable th) {
            a2.a(sSLSocket);
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f6389d;
    }

    public w c() {
        return this.f6387b;
    }

    public Socket d() {
        return this.f6388c;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return !this.f6388c.isClosed() && !this.f6388c.isInputShutdown() && !this.f6388c.isOutputShutdown();
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (this.f6390e != null) {
            return this.f6390e.f();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.f6391f != null) {
            throw new IllegalStateException("spdyConnection != null");
        }
        this.f6393h = System.nanoTime();
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.f6391f == null || this.f6391f.b();
    }

    /* access modifiers changed from: package-private */
    public long i() {
        return this.f6391f == null ? this.f6393h : this.f6391f.c();
    }

    public n j() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public q a(g gVar) {
        return this.f6391f != null ? new o(gVar, this.f6391f) : new com.squareup.okhttp.internal.a.i(gVar, this.f6390e);
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        return this.f6391f != null;
    }

    public Protocol l() {
        return this.f6392g;
    }

    /* access modifiers changed from: package-private */
    public void a(Protocol protocol) {
        if (protocol == null) {
            throw new IllegalArgumentException("protocol == null");
        }
        this.f6392g = protocol;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        if (!this.f6389d) {
            throw new IllegalStateException("setTimeouts - not connected");
        } else if (this.f6390e != null) {
            this.f6388c.setSoTimeout(i2);
            this.f6390e.a(i2, i3);
        }
    }

    /* access modifiers changed from: package-private */
    public void m() {
        this.j++;
    }

    /* access modifiers changed from: package-private */
    public int n() {
        return this.j;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0074 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0091 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x005d  */
    private void b(com.squareup.okhttp.s r11, int r12, int r13) {
        /*
            r10 = this;
            r2 = 0
            com.squareup.okhttp.internal.a.e r4 = new com.squareup.okhttp.internal.a.e
            com.squareup.okhttp.j r0 = r10.f6386a
            java.net.Socket r1 = r10.f6388c
            r4.<init>(r0, r10, r1)
            r4.a(r12, r13)
            java.net.URL r0 = r11.a()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = "CONNECT "
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r0.getHost()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r1 = r1.append(r5)
            int r0 = r0.getPort()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = " HTTP/1.1"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r5 = r0.toString()
        L_0x003d:
            com.squareup.okhttp.o r0 = r11.e()
            r4.a(r0, r5)
            r4.d()
            com.squareup.okhttp.u$a r0 = r4.g()
            com.squareup.okhttp.u$a r0 = r0.a(r11)
            com.squareup.okhttp.u r6 = r0.a()
            long r0 = com.squareup.okhttp.internal.a.j.a(r6)
            r8 = -1
            int r7 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x005e
            r0 = r2
        L_0x005e:
            okio.q r0 = r4.b(r0)
            r1 = 2147483647(0x7fffffff, float:NaN)
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.MILLISECONDS
            com.squareup.okhttp.internal.h.b(r0, r1, r7)
            r0.close()
            int r0 = r6.c()
            switch(r0) {
                case 200: goto L_0x0091;
                case 407: goto L_0x00a1;
                default: goto L_0x0074;
            }
        L_0x0074:
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unexpected response code for CONNECT: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r6.c()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0091:
            long r0 = r4.e()
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00b9
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "TLS tunnel buffered too many bytes!"
            r0.<init>(r1)
            throw r0
        L_0x00a1:
            com.squareup.okhttp.w r0 = r10.f6387b
            com.squareup.okhttp.a r0 = r0.f6740a
            com.squareup.okhttp.b r0 = r0.f6352h
            com.squareup.okhttp.w r1 = r10.f6387b
            java.net.Proxy r1 = r1.f6741b
            com.squareup.okhttp.s r11 = com.squareup.okhttp.internal.a.j.a(r0, r6, r1)
            if (r11 != 0) goto L_0x003d
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "Failed to authenticate with proxy"
            r0.<init>(r1)
            throw r0
        L_0x00b9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.i.b(com.squareup.okhttp.s, int, int):void");
    }

    public String toString() {
        return "Connection{" + this.f6387b.f6740a.f6346b + ":" + this.f6387b.f6740a.f6347c + ", proxy=" + this.f6387b.f6741b + " hostAddress=" + this.f6387b.f6742c.getAddress().getHostAddress() + " cipherSuite=" + (this.i != null ? this.i.a() : "none") + " protocol=" + this.f6392g + '}';
    }
}
