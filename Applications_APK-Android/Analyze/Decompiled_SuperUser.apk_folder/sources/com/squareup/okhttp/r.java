package com.squareup.okhttp;

import com.squareup.okhttp.internal.a;
import com.squareup.okhttp.internal.a.q;
import com.squareup.okhttp.internal.b;
import com.squareup.okhttp.internal.d;
import com.squareup.okhttp.internal.g;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.o;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: OkHttpClient */
public class r implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final List<Protocol> f6698a = h.a(Protocol.HTTP_2, Protocol.SPDY_3, Protocol.HTTP_1_1);

    /* renamed from: b  reason: collision with root package name */
    private static final List<k> f6699b = h.a(k.f6670a, k.f6671b, k.f6672c);

    /* renamed from: c  reason: collision with root package name */
    private static SSLSocketFactory f6700c;
    private int A;

    /* renamed from: d  reason: collision with root package name */
    private final g f6701d;

    /* renamed from: e  reason: collision with root package name */
    private m f6702e;

    /* renamed from: f  reason: collision with root package name */
    private Proxy f6703f;

    /* renamed from: g  reason: collision with root package name */
    private List<Protocol> f6704g;

    /* renamed from: h  reason: collision with root package name */
    private List<k> f6705h;
    private final List<p> i;
    private final List<p> j;
    private ProxySelector k;
    private CookieHandler l;
    private b m;
    private c n;
    private SocketFactory o;
    private SSLSocketFactory p;
    private HostnameVerifier q;
    private g r;
    private b s;
    private j t;
    /* access modifiers changed from: private */
    public d u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y;
    private int z;

    static {
        a.f6395b = new a() {
            public q a(i iVar, com.squareup.okhttp.internal.a.g gVar) {
                return iVar.a(gVar);
            }

            public boolean a(i iVar) {
                return iVar.a();
            }

            public void a(i iVar, Object obj) {
                iVar.b(obj);
            }

            public int b(i iVar) {
                return iVar.n();
            }

            public void a(i iVar, Protocol protocol) {
                iVar.a(protocol);
            }

            public void b(i iVar, com.squareup.okhttp.internal.a.g gVar) {
                iVar.a((Object) gVar);
            }

            public boolean c(i iVar) {
                return iVar.f();
            }

            public void a(o.a aVar, String str) {
                aVar.a(str);
            }

            public b a(r rVar) {
                return rVar.g();
            }

            public void a(j jVar, i iVar) {
                jVar.a(iVar);
            }

            public g b(r rVar) {
                return rVar.q();
            }

            public d c(r rVar) {
                return rVar.u;
            }

            public void a(r rVar, i iVar, com.squareup.okhttp.internal.a.g gVar, s sVar) {
                iVar.a(rVar, gVar, sVar);
            }
        };
    }

    public r() {
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.v = true;
        this.w = true;
        this.x = true;
        this.f6701d = new g();
        this.f6702e = new m();
    }

    private r(r rVar) {
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.v = true;
        this.w = true;
        this.x = true;
        this.f6701d = rVar.f6701d;
        this.f6702e = rVar.f6702e;
        this.f6703f = rVar.f6703f;
        this.f6704g = rVar.f6704g;
        this.f6705h = rVar.f6705h;
        this.i.addAll(rVar.i);
        this.j.addAll(rVar.j);
        this.k = rVar.k;
        this.l = rVar.l;
        this.n = rVar.n;
        this.m = this.n != null ? this.n.f6353a : rVar.m;
        this.o = rVar.o;
        this.p = rVar.p;
        this.q = rVar.q;
        this.r = rVar.r;
        this.s = rVar.s;
        this.t = rVar.t;
        this.u = rVar.u;
        this.v = rVar.v;
        this.w = rVar.w;
        this.x = rVar.x;
        this.y = rVar.y;
        this.z = rVar.z;
        this.A = rVar.A;
    }

    public final void a(long j2, TimeUnit timeUnit) {
        if (j2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || j2 <= 0) {
                this.y = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        }
    }

    public final int a() {
        return this.y;
    }

    public final void b(long j2, TimeUnit timeUnit) {
        if (j2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || j2 <= 0) {
                this.z = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        }
    }

    public final int b() {
        return this.z;
    }

    public final int c() {
        return this.A;
    }

    public final Proxy d() {
        return this.f6703f;
    }

    public final ProxySelector e() {
        return this.k;
    }

    public final CookieHandler f() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public final b g() {
        return this.m;
    }

    public final SocketFactory h() {
        return this.o;
    }

    public final SSLSocketFactory i() {
        return this.p;
    }

    public final HostnameVerifier j() {
        return this.q;
    }

    public final g k() {
        return this.r;
    }

    public final b l() {
        return this.s;
    }

    public final j m() {
        return this.t;
    }

    public final boolean n() {
        return this.v;
    }

    public final boolean o() {
        return this.w;
    }

    public final boolean p() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public final g q() {
        return this.f6701d;
    }

    public final m r() {
        return this.f6702e;
    }

    public final List<Protocol> s() {
        return this.f6704g;
    }

    public final List<k> t() {
        return this.f6705h;
    }

    public List<p> u() {
        return this.i;
    }

    public List<p> v() {
        return this.j;
    }

    public e a(s sVar) {
        return new e(this, sVar);
    }

    public r a(Object obj) {
        r().a(obj);
        return this;
    }

    /* access modifiers changed from: package-private */
    public final r w() {
        r rVar = new r(this);
        if (rVar.k == null) {
            rVar.k = ProxySelector.getDefault();
        }
        if (rVar.l == null) {
            rVar.l = CookieHandler.getDefault();
        }
        if (rVar.o == null) {
            rVar.o = SocketFactory.getDefault();
        }
        if (rVar.p == null) {
            rVar.p = y();
        }
        if (rVar.q == null) {
            rVar.q = com.squareup.okhttp.internal.b.b.f6485a;
        }
        if (rVar.r == null) {
            rVar.r = g.f6381a;
        }
        if (rVar.s == null) {
            rVar.s = com.squareup.okhttp.internal.a.a.f6396a;
        }
        if (rVar.t == null) {
            rVar.t = j.a();
        }
        if (rVar.f6704g == null) {
            rVar.f6704g = f6698a;
        }
        if (rVar.f6705h == null) {
            rVar.f6705h = f6699b;
        }
        if (rVar.u == null) {
            rVar.u = d.f6488a;
        }
        return rVar;
    }

    private synchronized SSLSocketFactory y() {
        if (f6700c == null) {
            try {
                SSLContext instance = SSLContext.getInstance("TLS");
                instance.init(null, null, null);
                f6700c = instance.getSocketFactory();
            } catch (GeneralSecurityException e2) {
                throw new AssertionError();
            }
        }
        return f6700c;
    }

    /* renamed from: x */
    public final r clone() {
        try {
            return (r) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError();
        }
    }
}
