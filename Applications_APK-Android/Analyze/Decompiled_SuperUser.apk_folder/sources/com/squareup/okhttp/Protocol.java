package com.squareup.okhttp;

import java.io.IOException;

public enum Protocol {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2");
    

    /* renamed from: e  reason: collision with root package name */
    private final String f6338e;

    private Protocol(String str) {
        this.f6338e = str;
    }

    public static Protocol a(String str) {
        if (str.equals(HTTP_1_0.f6338e)) {
            return HTTP_1_0;
        }
        if (str.equals(HTTP_1_1.f6338e)) {
            return HTTP_1_1;
        }
        if (str.equals(HTTP_2.f6338e)) {
            return HTTP_2;
        }
        if (str.equals(SPDY_3.f6338e)) {
            return SPDY_3;
        }
        throw new IOException("Unexpected protocol: " + str);
    }

    public String toString() {
        return this.f6338e;
    }
}
