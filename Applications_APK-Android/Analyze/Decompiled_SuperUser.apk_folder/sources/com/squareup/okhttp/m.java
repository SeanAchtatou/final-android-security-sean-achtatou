package com.squareup.okhttp;

import com.squareup.okhttp.e;
import com.squareup.okhttp.internal.a.g;
import com.squareup.okhttp.internal.h;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: Dispatcher */
public final class m {

    /* renamed from: a  reason: collision with root package name */
    private int f6681a = 64;

    /* renamed from: b  reason: collision with root package name */
    private int f6682b = 5;

    /* renamed from: c  reason: collision with root package name */
    private ExecutorService f6683c;

    /* renamed from: d  reason: collision with root package name */
    private final Deque<e.b> f6684d = new ArrayDeque();

    /* renamed from: e  reason: collision with root package name */
    private final Deque<e.b> f6685e = new ArrayDeque();

    /* renamed from: f  reason: collision with root package name */
    private final Deque<e> f6686f = new ArrayDeque();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      com.squareup.okhttp.internal.h.a(java.lang.String, int):int
      com.squareup.okhttp.internal.h.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.squareup.okhttp.internal.h.a(java.io.Closeable, java.io.Closeable):void
      com.squareup.okhttp.internal.h.a(java.lang.Object, java.lang.Object):boolean
      com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public synchronized ExecutorService a() {
        if (this.f6683c == null) {
            this.f6683c = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), h.a("OkHttp Dispatcher", false));
        }
        return this.f6683c;
    }

    public synchronized void a(Object obj) {
        for (e.b next : this.f6684d) {
            if (h.a(obj, next.b())) {
                next.c();
            }
        }
        for (e.b next2 : this.f6685e) {
            if (h.a(obj, next2.b())) {
                next2.d().f6369a = true;
                g gVar = next2.d().f6371c;
                if (gVar != null) {
                    gVar.i();
                }
            }
        }
        for (e next3 : this.f6686f) {
            if (h.a(obj, next3.b())) {
                next3.c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(e.b bVar) {
        if (!this.f6685e.remove(bVar)) {
            throw new AssertionError("AsyncCall wasn't running!");
        }
        b();
    }

    private void b() {
        if (this.f6685e.size() < this.f6681a && !this.f6684d.isEmpty()) {
            Iterator<e.b> it = this.f6684d.iterator();
            while (it.hasNext()) {
                e.b next = it.next();
                if (b(next) < this.f6682b) {
                    it.remove();
                    this.f6685e.add(next);
                    a().execute(next);
                }
                if (this.f6685e.size() >= this.f6681a) {
                    return;
                }
            }
        }
    }

    private int b(e.b bVar) {
        int i = 0;
        Iterator<e.b> it = this.f6685e.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            if (it.next().a().equals(bVar.a())) {
                i = i2 + 1;
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(e eVar) {
        this.f6686f.add(eVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(e eVar) {
        if (!this.f6686f.remove(eVar)) {
            throw new AssertionError("Call wasn't in-flight!");
        }
    }
}
