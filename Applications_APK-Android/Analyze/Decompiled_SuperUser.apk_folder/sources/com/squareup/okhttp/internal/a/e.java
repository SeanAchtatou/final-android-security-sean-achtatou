package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.i;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.j;
import com.squareup.okhttp.o;
import com.squareup.okhttp.u;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import okio.k;
import okio.p;
import okio.q;
import okio.r;

/* compiled from: HttpConnection */
public final class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final j f6407a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final i f6408b;

    /* renamed from: c  reason: collision with root package name */
    private final Socket f6409c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final okio.e f6410d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final okio.d f6411e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public int f6412f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public int f6413g = 0;

    public e(j jVar, i iVar, Socket socket) {
        this.f6407a = jVar;
        this.f6408b = iVar;
        this.f6409c = socket;
        this.f6410d = k.a(k.b(socket));
        this.f6411e = k.a(k.a(socket));
    }

    public void a(int i, int i2) {
        if (i != 0) {
            this.f6410d.a().a((long) i, TimeUnit.MILLISECONDS);
        }
        if (i2 != 0) {
            this.f6411e.a().a((long) i2, TimeUnit.MILLISECONDS);
        }
    }

    public void a() {
        this.f6413g = 1;
        if (this.f6412f == 0) {
            this.f6413g = 0;
            com.squareup.okhttp.internal.a.f6395b.a(this.f6407a, this.f6408b);
        }
    }

    public void b() {
        this.f6413g = 2;
        if (this.f6412f == 0) {
            this.f6412f = 6;
            this.f6408b.d().close();
        }
    }

    public boolean c() {
        return this.f6412f == 6;
    }

    public void a(Object obj) {
        com.squareup.okhttp.internal.a.f6395b.a(this.f6408b, obj);
    }

    public void d() {
        this.f6411e.flush();
    }

    public long e() {
        return this.f6410d.c().b();
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean f() {
        /*
            r5 = this;
            r0 = 0
            r1 = 1
            java.net.Socket r2 = r5.f6409c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            int r3 = r2.getSoTimeout()     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            java.net.Socket r2 = r5.f6409c     // Catch:{ all -> 0x0023 }
            r4 = 1
            r2.setSoTimeout(r4)     // Catch:{ all -> 0x0023 }
            okio.e r2 = r5.f6410d     // Catch:{ all -> 0x0023 }
            boolean r2 = r2.f()     // Catch:{ all -> 0x0023 }
            if (r2 == 0) goto L_0x001c
            java.net.Socket r2 = r5.f6409c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r2.setSoTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
        L_0x001b:
            return r0
        L_0x001c:
            java.net.Socket r2 = r5.f6409c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r2.setSoTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r0 = r1
            goto L_0x001b
        L_0x0023:
            r2 = move-exception
            java.net.Socket r4 = r5.f6409c     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            r4.setSoTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
            throw r2     // Catch:{ SocketTimeoutException -> 0x002a, IOException -> 0x002d }
        L_0x002a:
            r0 = move-exception
            r0 = r1
            goto L_0x001b
        L_0x002d:
            r1 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.a.e.f():boolean");
    }

    public void a(o oVar, String str) {
        if (this.f6412f != 0) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6411e.b(str).b("\r\n");
        int a2 = oVar.a();
        for (int i = 0; i < a2; i++) {
            this.f6411e.b(oVar.a(i)).b(": ").b(oVar.b(i)).b("\r\n");
        }
        this.f6411e.b("\r\n");
        this.f6412f = 1;
    }

    public u.a g() {
        p a2;
        u.a a3;
        if (this.f6412f == 1 || this.f6412f == 3) {
            do {
                try {
                    a2 = p.a(this.f6410d.p());
                    a3 = new u.a().a(a2.f6475a).a(a2.f6476b).a(a2.f6477c);
                    o.a aVar = new o.a();
                    a(aVar);
                    aVar.a(j.f6455d, a2.f6475a.toString());
                    a3.a(aVar.a());
                } catch (EOFException e2) {
                    IOException iOException = new IOException("unexpected end of stream on " + this.f6408b + " (recycle count=" + com.squareup.okhttp.internal.a.f6395b.b(this.f6408b) + ")");
                    iOException.initCause(e2);
                    throw iOException;
                }
            } while (a2.f6476b == 100);
            this.f6412f = 4;
            return a3;
        }
        throw new IllegalStateException("state: " + this.f6412f);
    }

    public void a(o.a aVar) {
        while (true) {
            String p = this.f6410d.p();
            if (p.length() != 0) {
                com.squareup.okhttp.internal.a.f6395b.a(aVar, p);
            } else {
                return;
            }
        }
    }

    public p h() {
        if (this.f6412f != 1) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6412f = 2;
        return new b();
    }

    public p a(long j) {
        if (this.f6412f != 1) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6412f = 2;
        return new d(j);
    }

    public void a(m mVar) {
        if (this.f6412f != 1) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6412f = 3;
        mVar.a(this.f6411e);
    }

    public q b(long j) {
        if (this.f6412f != 4) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6412f = 5;
        return new C0093e(j);
    }

    public q a(g gVar) {
        if (this.f6412f != 4) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6412f = 5;
        return new c(gVar);
    }

    public q i() {
        if (this.f6412f != 4) {
            throw new IllegalStateException("state: " + this.f6412f);
        }
        this.f6412f = 5;
        return new f();
    }

    /* compiled from: HttpConnection */
    private final class d implements p {

        /* renamed from: b  reason: collision with root package name */
        private boolean f6423b;

        /* renamed from: c  reason: collision with root package name */
        private long f6424c;

        private d(long j) {
            this.f6424c = j;
        }

        public r a() {
            return e.this.f6411e.a();
        }

        public void a_(okio.c cVar, long j) {
            if (this.f6423b) {
                throw new IllegalStateException("closed");
            }
            h.a(cVar.b(), 0, j);
            if (j > this.f6424c) {
                throw new ProtocolException("expected " + this.f6424c + " bytes but received " + j);
            }
            e.this.f6411e.a_(cVar, j);
            this.f6424c -= j;
        }

        public void flush() {
            if (!this.f6423b) {
                e.this.f6411e.flush();
            }
        }

        public void close() {
            if (!this.f6423b) {
                this.f6423b = true;
                if (this.f6424c > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                int unused = e.this.f6412f = 3;
            }
        }
    }

    /* compiled from: HttpConnection */
    private final class b implements p {

        /* renamed from: b  reason: collision with root package name */
        private boolean f6417b;

        private b() {
        }

        public r a() {
            return e.this.f6411e.a();
        }

        public void a_(okio.c cVar, long j) {
            if (this.f6417b) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                e.this.f6411e.j(j);
                e.this.f6411e.b("\r\n");
                e.this.f6411e.a_(cVar, j);
                e.this.f6411e.b("\r\n");
            }
        }

        public synchronized void flush() {
            if (!this.f6417b) {
                e.this.f6411e.flush();
            }
        }

        public synchronized void close() {
            if (!this.f6417b) {
                this.f6417b = true;
                e.this.f6411e.b("0\r\n\r\n");
                int unused = e.this.f6412f = 3;
            }
        }
    }

    /* compiled from: HttpConnection */
    private abstract class a implements q {

        /* renamed from: a  reason: collision with root package name */
        protected boolean f6414a;

        private a() {
        }

        public r a() {
            return e.this.f6410d.a();
        }

        /* access modifiers changed from: protected */
        public final void a(boolean z) {
            if (e.this.f6412f != 5) {
                throw new IllegalStateException("state: " + e.this.f6412f);
            }
            int unused = e.this.f6412f = 0;
            if (z && e.this.f6413g == 1) {
                int unused2 = e.this.f6413g = 0;
                com.squareup.okhttp.internal.a.f6395b.a(e.this.f6407a, e.this.f6408b);
            } else if (e.this.f6413g == 2) {
                int unused3 = e.this.f6412f = 6;
                e.this.f6408b.d().close();
            }
        }

        /* access modifiers changed from: protected */
        public final void b() {
            h.a(e.this.f6408b.d());
            int unused = e.this.f6412f = 6;
        }
    }

    /* renamed from: com.squareup.okhttp.internal.a.e$e  reason: collision with other inner class name */
    /* compiled from: HttpConnection */
    private class C0093e extends a {

        /* renamed from: d  reason: collision with root package name */
        private long f6426d;

        public C0093e(long j) {
            super();
            this.f6426d = j;
            if (this.f6426d == 0) {
                a(true);
            }
        }

        public long a(okio.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f6414a) {
                throw new IllegalStateException("closed");
            } else if (this.f6426d == 0) {
                return -1;
            } else {
                long a2 = e.this.f6410d.a(cVar, Math.min(this.f6426d, j));
                if (a2 == -1) {
                    b();
                    throw new ProtocolException("unexpected end of stream");
                }
                this.f6426d -= a2;
                if (this.f6426d == 0) {
                    a(true);
                }
                return a2;
            }
        }

        public void close() {
            if (!this.f6414a) {
                if (this.f6426d != 0 && !h.a(this, 100, TimeUnit.MILLISECONDS)) {
                    b();
                }
                this.f6414a = true;
            }
        }
    }

    /* compiled from: HttpConnection */
    private class c extends a {

        /* renamed from: d  reason: collision with root package name */
        private long f6419d = -1;

        /* renamed from: e  reason: collision with root package name */
        private boolean f6420e = true;

        /* renamed from: f  reason: collision with root package name */
        private final g f6421f;

        c(g gVar) {
            super();
            this.f6421f = gVar;
        }

        public long a(okio.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f6414a) {
                throw new IllegalStateException("closed");
            } else if (!this.f6420e) {
                return -1;
            } else {
                if (this.f6419d == 0 || this.f6419d == -1) {
                    c();
                    if (!this.f6420e) {
                        return -1;
                    }
                }
                long a2 = e.this.f6410d.a(cVar, Math.min(j, this.f6419d));
                if (a2 == -1) {
                    b();
                    throw new IOException("unexpected end of stream");
                }
                this.f6419d -= a2;
                return a2;
            }
        }

        private void c() {
            if (this.f6419d != -1) {
                e.this.f6410d.p();
            }
            try {
                this.f6419d = e.this.f6410d.m();
                String trim = e.this.f6410d.p().trim();
                if (this.f6419d < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f6419d + trim + "\"");
                } else if (this.f6419d == 0) {
                    this.f6420e = false;
                    o.a aVar = new o.a();
                    e.this.a(aVar);
                    this.f6421f.a(aVar.a());
                    a(true);
                }
            } catch (NumberFormatException e2) {
                throw new ProtocolException(e2.getMessage());
            }
        }

        public void close() {
            if (!this.f6414a) {
                if (this.f6420e && !h.a(this, 100, TimeUnit.MILLISECONDS)) {
                    b();
                }
                this.f6414a = true;
            }
        }
    }

    /* compiled from: HttpConnection */
    private class f extends a {

        /* renamed from: d  reason: collision with root package name */
        private boolean f6428d;

        private f() {
            super();
        }

        public long a(okio.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f6414a) {
                throw new IllegalStateException("closed");
            } else if (this.f6428d) {
                return -1;
            } else {
                long a2 = e.this.f6410d.a(cVar, j);
                if (a2 != -1) {
                    return a2;
                }
                this.f6428d = true;
                a(false);
                return -1;
            }
        }

        public void close() {
            if (!this.f6414a) {
                if (!this.f6428d) {
                    b();
                }
                this.f6414a = true;
            }
        }
    }
}
