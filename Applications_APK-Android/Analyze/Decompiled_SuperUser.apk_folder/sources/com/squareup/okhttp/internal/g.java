package com.squareup.okhttp.internal;

import com.squareup.okhttp.w;
import java.util.LinkedHashSet;
import java.util.Set;

/* compiled from: RouteDatabase */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final Set<w> f6507a = new LinkedHashSet();

    public synchronized void a(w wVar) {
        this.f6507a.add(wVar);
    }

    public synchronized void b(w wVar) {
        this.f6507a.remove(wVar);
    }

    public synchronized boolean c(w wVar) {
        return this.f6507a.contains(wVar);
    }
}
