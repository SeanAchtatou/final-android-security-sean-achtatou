package com.squareup.okhttp.internal.spdy;

import okio.ByteString;

/* compiled from: Header */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final ByteString f6526a = ByteString.a(":status");

    /* renamed from: b  reason: collision with root package name */
    public static final ByteString f6527b = ByteString.a(":method");

    /* renamed from: c  reason: collision with root package name */
    public static final ByteString f6528c = ByteString.a(":path");

    /* renamed from: d  reason: collision with root package name */
    public static final ByteString f6529d = ByteString.a(":scheme");

    /* renamed from: e  reason: collision with root package name */
    public static final ByteString f6530e = ByteString.a(":authority");

    /* renamed from: f  reason: collision with root package name */
    public static final ByteString f6531f = ByteString.a(":host");

    /* renamed from: g  reason: collision with root package name */
    public static final ByteString f6532g = ByteString.a(":version");

    /* renamed from: h  reason: collision with root package name */
    public final ByteString f6533h;
    public final ByteString i;
    final int j;

    public c(String str, String str2) {
        this(ByteString.a(str), ByteString.a(str2));
    }

    public c(ByteString byteString, String str) {
        this(byteString, ByteString.a(str));
    }

    public c(ByteString byteString, ByteString byteString2) {
        this.f6533h = byteString;
        this.i = byteString2;
        this.j = byteString.g() + 32 + byteString2.g();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (!this.f6533h.equals(cVar.f6533h) || !this.i.equals(cVar.i)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.f6533h.hashCode() + 527) * 31) + this.i.hashCode();
    }

    public String toString() {
        return String.format("%s: %s", this.f6533h.a(), this.i.a());
    }
}
