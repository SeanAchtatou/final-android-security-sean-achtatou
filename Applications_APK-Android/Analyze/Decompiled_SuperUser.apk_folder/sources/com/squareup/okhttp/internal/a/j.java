package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.b;
import com.squareup.okhttp.h;
import com.squareup.okhttp.internal.f;
import com.squareup.okhttp.o;
import com.squareup.okhttp.s;
import com.squareup.okhttp.u;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: OkHeaders */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    static final String f6452a = f.a().b();

    /* renamed from: b  reason: collision with root package name */
    public static final String f6453b = (f6452a + "-Sent-Millis");

    /* renamed from: c  reason: collision with root package name */
    public static final String f6454c = (f6452a + "-Received-Millis");

    /* renamed from: d  reason: collision with root package name */
    public static final String f6455d = (f6452a + "-Selected-Protocol");

    /* renamed from: e  reason: collision with root package name */
    private static final Comparator<String> f6456e = new Comparator<String>() {
        /* renamed from: a */
        public int compare(String str, String str2) {
            if (str == str2) {
                return 0;
            }
            if (str == null) {
                return -1;
            }
            if (str2 == null) {
                return 1;
            }
            return String.CASE_INSENSITIVE_ORDER.compare(str, str2);
        }
    };

    public static long a(s sVar) {
        return a(sVar.e());
    }

    public static long a(u uVar) {
        return a(uVar.f());
    }

    public static long a(o oVar) {
        return b(oVar.a("Content-Length"));
    }

    private static long b(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e2) {
            return -1;
        }
    }

    public static Map<String, List<String>> a(o oVar, String str) {
        TreeMap treeMap = new TreeMap(f6456e);
        int a2 = oVar.a();
        for (int i = 0; i < a2; i++) {
            String a3 = oVar.a(i);
            String b2 = oVar.b(i);
            ArrayList arrayList = new ArrayList();
            List list = (List) treeMap.get(a3);
            if (list != null) {
                arrayList.addAll(list);
            }
            arrayList.add(b2);
            treeMap.put(a3, Collections.unmodifiableList(arrayList));
        }
        if (str != null) {
            treeMap.put(null, Collections.unmodifiableList(Collections.singletonList(str)));
        }
        return Collections.unmodifiableMap(treeMap);
    }

    public static void a(s.a aVar, Map<String, List<String>> map) {
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            if (("Cookie".equalsIgnoreCase(str) || "Cookie2".equalsIgnoreCase(str)) && !((List) next.getValue()).isEmpty()) {
                aVar.b(str, a((List) next.getValue()));
            }
        }
    }

    private static String a(List<String> list) {
        if (list.size() == 1) {
            return list.get(0);
        }
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    static boolean a(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    public static List<h> b(o oVar, String str) {
        ArrayList arrayList = new ArrayList();
        int a2 = oVar.a();
        for (int i = 0; i < a2; i++) {
            if (str.equalsIgnoreCase(oVar.a(i))) {
                String b2 = oVar.b(i);
                int i2 = 0;
                while (i2 < b2.length()) {
                    int a3 = d.a(b2, i2, " ");
                    String trim = b2.substring(i2, a3).trim();
                    int a4 = d.a(b2, a3);
                    if (!b2.regionMatches(true, a4, "realm=\"", 0, "realm=\"".length())) {
                        break;
                    }
                    int length = "realm=\"".length() + a4;
                    int a5 = d.a(b2, length, "\"");
                    String substring = b2.substring(length, a5);
                    i2 = d.a(b2, d.a(b2, a5 + 1, ",") + 1);
                    arrayList.add(new h(trim, substring));
                }
            }
        }
        return arrayList;
    }

    public static s a(b bVar, u uVar, Proxy proxy) {
        if (uVar.c() == 407) {
            return bVar.b(proxy, uVar);
        }
        return bVar.a(proxy, uVar);
    }
}
