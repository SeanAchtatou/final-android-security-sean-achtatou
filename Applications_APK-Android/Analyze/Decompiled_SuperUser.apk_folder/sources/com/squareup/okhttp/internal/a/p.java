package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.Protocol;
import java.net.ProtocolException;

/* compiled from: StatusLine */
public final class p {

    /* renamed from: a  reason: collision with root package name */
    public final Protocol f6475a;

    /* renamed from: b  reason: collision with root package name */
    public final int f6476b;

    /* renamed from: c  reason: collision with root package name */
    public final String f6477c;

    public p(Protocol protocol, int i, String str) {
        this.f6475a = protocol;
        this.f6476b = i;
        this.f6477c = str;
    }

    public static p a(String str) {
        Protocol protocol;
        String str2;
        int i = 9;
        if (str.startsWith("HTTP/1.")) {
            if (str.length() < 9 || str.charAt(8) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            }
            int charAt = str.charAt(7) - '0';
            if (charAt == 0) {
                protocol = Protocol.HTTP_1_0;
            } else if (charAt == 1) {
                protocol = Protocol.HTTP_1_1;
            } else {
                throw new ProtocolException("Unexpected status line: " + str);
            }
        } else if (str.startsWith("ICY ")) {
            protocol = Protocol.HTTP_1_0;
            i = 4;
        } else {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        if (str.length() < i + 3) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
        try {
            int parseInt = Integer.parseInt(str.substring(i, i + 3));
            if (str.length() <= i + 3) {
                str2 = "";
            } else if (str.charAt(i + 3) != ' ') {
                throw new ProtocolException("Unexpected status line: " + str);
            } else {
                str2 = str.substring(i + 4);
            }
            return new p(protocol, parseInt, str2);
        } catch (NumberFormatException e2) {
            throw new ProtocolException("Unexpected status line: " + str);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f6475a == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1");
        sb.append(' ').append(this.f6476b);
        if (this.f6477c != null) {
            sb.append(' ').append(this.f6477c);
        }
        return sb.toString();
    }
}
