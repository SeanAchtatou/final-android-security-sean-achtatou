package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.internal.spdy.ErrorCode;
import com.squareup.okhttp.internal.spdy.c;
import com.squareup.okhttp.internal.spdy.m;
import com.squareup.okhttp.internal.spdy.n;
import com.squareup.okhttp.o;
import com.squareup.okhttp.s;
import com.squareup.okhttp.u;
import com.squareup.okhttp.v;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okio.ByteString;
import okio.k;
import okio.p;

/* compiled from: SpdyTransport */
public final class o implements q {

    /* renamed from: a  reason: collision with root package name */
    private static final List<ByteString> f6470a = h.a(ByteString.a("connection"), ByteString.a("host"), ByteString.a("keep-alive"), ByteString.a("proxy-connection"), ByteString.a("transfer-encoding"));

    /* renamed from: b  reason: collision with root package name */
    private static final List<ByteString> f6471b = h.a(ByteString.a("connection"), ByteString.a("host"), ByteString.a("keep-alive"), ByteString.a("proxy-connection"), ByteString.a("te"), ByteString.a("transfer-encoding"), ByteString.a("encoding"), ByteString.a("upgrade"));

    /* renamed from: c  reason: collision with root package name */
    private final g f6472c;

    /* renamed from: d  reason: collision with root package name */
    private final m f6473d;

    /* renamed from: e  reason: collision with root package name */
    private n f6474e;

    public o(g gVar, m mVar) {
        this.f6472c = gVar;
        this.f6473d = mVar;
    }

    public p a(s sVar, long j) {
        return this.f6474e.g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.okhttp.internal.spdy.m.a(java.util.List<com.squareup.okhttp.internal.spdy.c>, boolean, boolean):com.squareup.okhttp.internal.spdy.n
     arg types: [java.util.List<com.squareup.okhttp.internal.spdy.c>, boolean, int]
     candidates:
      com.squareup.okhttp.internal.spdy.m.a(int, java.util.List<com.squareup.okhttp.internal.spdy.c>, boolean):void
      com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, int, com.squareup.okhttp.internal.spdy.ErrorCode):void
      com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, int, java.util.List):void
      com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, com.squareup.okhttp.internal.spdy.ErrorCode, com.squareup.okhttp.internal.spdy.ErrorCode):void
      com.squareup.okhttp.internal.spdy.m.a(java.util.List<com.squareup.okhttp.internal.spdy.c>, boolean, boolean):com.squareup.okhttp.internal.spdy.n */
    public void a(s sVar) {
        if (this.f6474e == null) {
            this.f6472c.b();
            this.f6474e = this.f6473d.a(a(sVar, this.f6473d.a(), l.a(this.f6472c.f().l())), this.f6472c.c(), true);
            this.f6474e.e().a((long) this.f6472c.f6434a.b(), TimeUnit.MILLISECONDS);
        }
    }

    public void a(m mVar) {
        mVar.a(this.f6474e.g());
    }

    public void a() {
        this.f6474e.g().close();
    }

    public u.a b() {
        return a(this.f6474e.d(), this.f6473d.a());
    }

    public static List<c> a(s sVar, Protocol protocol, String str) {
        com.squareup.okhttp.o e2 = sVar.e();
        ArrayList arrayList = new ArrayList(e2.a() + 10);
        arrayList.add(new c(c.f6527b, sVar.d()));
        arrayList.add(new c(c.f6528c, l.a(sVar.a())));
        String a2 = g.a(sVar.a());
        if (Protocol.SPDY_3 == protocol) {
            arrayList.add(new c(c.f6532g, str));
            arrayList.add(new c(c.f6531f, a2));
        } else if (Protocol.HTTP_2 == protocol) {
            arrayList.add(new c(c.f6530e, a2));
        } else {
            throw new AssertionError();
        }
        arrayList.add(new c(c.f6529d, sVar.a().getProtocol()));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int a3 = e2.a();
        for (int i = 0; i < a3; i++) {
            ByteString a4 = ByteString.a(e2.a(i).toLowerCase(Locale.US));
            String b2 = e2.b(i);
            if (!a(protocol, a4) && !a4.equals(c.f6527b) && !a4.equals(c.f6528c) && !a4.equals(c.f6529d) && !a4.equals(c.f6530e) && !a4.equals(c.f6531f) && !a4.equals(c.f6532g)) {
                if (linkedHashSet.add(a4)) {
                    arrayList.add(new c(a4, b2));
                } else {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= arrayList.size()) {
                            break;
                        } else if (((c) arrayList.get(i2)).f6533h.equals(a4)) {
                            arrayList.set(i2, new c(a4, a(((c) arrayList.get(i2)).i.a(), b2)));
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private static String a(String str, String str2) {
        return str + 0 + str2;
    }

    public static u.a a(List<c> list, Protocol protocol) {
        String str = null;
        String str2 = "HTTP/1.1";
        o.a aVar = new o.a();
        aVar.b(j.f6455d, protocol.toString());
        int size = list.size();
        int i = 0;
        while (i < size) {
            ByteString byteString = list.get(i).f6533h;
            String a2 = list.get(i).i.a();
            String str3 = str2;
            int i2 = 0;
            while (i2 < a2.length()) {
                int indexOf = a2.indexOf(0, i2);
                if (indexOf == -1) {
                    indexOf = a2.length();
                }
                String substring = a2.substring(i2, indexOf);
                if (!byteString.equals(c.f6526a)) {
                    if (byteString.equals(c.f6532g)) {
                        str3 = substring;
                        substring = str;
                    } else {
                        if (!a(protocol, byteString)) {
                            aVar.a(byteString.a(), substring);
                        }
                        substring = str;
                    }
                }
                str = substring;
                i2 = indexOf + 1;
            }
            i++;
            str2 = str3;
        }
        if (str == null) {
            throw new ProtocolException("Expected ':status' header not present");
        }
        p a3 = p.a(str2 + " " + str);
        return new u.a().a(protocol).a(a3.f6476b).a(a3.f6477c).a(aVar.a());
    }

    public v a(u uVar) {
        return new k(uVar.f(), k.a(this.f6474e.f()));
    }

    public void c() {
    }

    public void a(g gVar) {
        if (this.f6474e != null) {
            this.f6474e.a(ErrorCode.CANCEL);
        }
    }

    public boolean d() {
        return true;
    }

    private static boolean a(Protocol protocol, ByteString byteString) {
        if (protocol == Protocol.SPDY_3) {
            return f6470a.contains(byteString);
        }
        if (protocol == Protocol.HTTP_2) {
            return f6471b.contains(byteString);
        }
        throw new AssertionError(protocol);
    }
}
