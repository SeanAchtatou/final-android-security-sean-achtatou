package com.squareup.okhttp.internal.spdy;

import java.util.concurrent.CountDownLatch;

/* compiled from: Ping */
public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final CountDownLatch f6579a = new CountDownLatch(1);

    /* renamed from: b  reason: collision with root package name */
    private long f6580b = -1;

    /* renamed from: c  reason: collision with root package name */
    private long f6581c = -1;

    i() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f6580b != -1) {
            throw new IllegalStateException();
        }
        this.f6580b = System.nanoTime();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f6581c != -1 || this.f6580b == -1) {
            throw new IllegalStateException();
        }
        this.f6581c = System.nanoTime();
        this.f6579a.countDown();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f6581c != -1 || this.f6580b == -1) {
            throw new IllegalStateException();
        }
        this.f6581c = this.f6580b - 1;
        this.f6579a.countDown();
    }
}
