package com.squareup.okhttp.internal;

import java.net.InetAddress;
import java.net.UnknownHostException;

/* compiled from: Network */
public interface d {

    /* renamed from: a  reason: collision with root package name */
    public static final d f6488a = new d() {
        public InetAddress[] a(String str) {
            if (str != null) {
                return InetAddress.getAllByName(str);
            }
            throw new UnknownHostException("host == null");
        }
    };

    InetAddress[] a(String str);
}
