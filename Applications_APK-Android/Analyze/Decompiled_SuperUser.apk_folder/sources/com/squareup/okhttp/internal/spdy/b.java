package com.squareup.okhttp.internal.spdy;

import java.io.Closeable;
import java.util.List;
import okio.c;

/* compiled from: FrameWriter */
public interface b extends Closeable {
    void a();

    void a(int i, int i2, List<c> list);

    void a(int i, long j);

    void a(int i, ErrorCode errorCode);

    void a(int i, ErrorCode errorCode, byte[] bArr);

    void a(k kVar);

    void a(boolean z, int i, int i2);

    void a(boolean z, int i, c cVar, int i2);

    void a(boolean z, boolean z2, int i, int i2, List<c> list);

    void b();

    void b(k kVar);

    int c();
}
