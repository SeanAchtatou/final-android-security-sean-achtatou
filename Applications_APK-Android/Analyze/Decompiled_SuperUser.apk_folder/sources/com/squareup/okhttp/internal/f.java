package com.squareup.okhttp.internal;

import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.squareup.okhttp.Protocol;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.net.ssl.SSLSocket;

/* compiled from: Platform */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final f f6492a = c();

    public static f a() {
        return f6492a;
    }

    public String b() {
        return "OkHttp";
    }

    public void a(String str) {
        System.out.println(str);
    }

    public void a(Socket socket) {
    }

    public void b(Socket socket) {
    }

    public URI a(URL url) {
        return url.toURI();
    }

    public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
    }

    public void a(SSLSocket sSLSocket) {
    }

    public String b(SSLSocket sSLSocket) {
        return null;
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        socket.connect(inetSocketAddress, i);
    }

    private static f c() {
        e eVar;
        Method method;
        Method method2;
        e eVar2;
        Method method3;
        e eVar3;
        Method method4;
        Method method5;
        e eVar4;
        e eVar5;
        e eVar6;
        e eVar7;
        try {
            Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
        } catch (ClassNotFoundException e2) {
            Class.forName("org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl");
        }
        try {
            e eVar8 = new e(null, "setUseSessionTickets", Boolean.TYPE);
            e eVar9 = new e(null, "setHostname", String.class);
            try {
                Class<?> cls = Class.forName("android.net.TrafficStats");
                Method method6 = cls.getMethod("tagSocket", Socket.class);
                try {
                    method5 = cls.getMethod("untagSocket", Socket.class);
                    method = method5;
                    eVar3 = eVar5;
                    method3 = method6;
                    eVar2 = eVar6;
                } catch (ClassNotFoundException e3) {
                    method4 = method6;
                    method = null;
                    method2 = method4;
                    eVar = null;
                    eVar2 = null;
                    method3 = method2;
                    eVar3 = eVar;
                    return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
                } catch (NoSuchMethodException e4) {
                    eVar = null;
                    method = null;
                    method2 = method6;
                    eVar2 = null;
                    method3 = method2;
                    eVar3 = eVar;
                    return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
                }
                try {
                    Class.forName("android.net.Network");
                    eVar7 = new e(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                } catch (ClassNotFoundException e5) {
                    eVar4 = null;
                    eVar5 = eVar4;
                    eVar6 = null;
                    method = method5;
                    eVar3 = eVar5;
                    method3 = method6;
                    eVar2 = eVar6;
                    return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
                } catch (NoSuchMethodException e6) {
                    eVar = null;
                    method = method5;
                    method2 = method6;
                    eVar2 = null;
                    method3 = method2;
                    eVar3 = eVar;
                    return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
                }
                try {
                    eVar6 = new e(null, "setAlpnProtocols", byte[].class);
                    eVar5 = eVar7;
                } catch (ClassNotFoundException e7) {
                    eVar4 = eVar7;
                    eVar5 = eVar4;
                    eVar6 = null;
                    method = method5;
                    eVar3 = eVar5;
                    method3 = method6;
                    eVar2 = eVar6;
                    return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
                } catch (NoSuchMethodException e8) {
                    eVar = eVar7;
                    method = method5;
                    method2 = method6;
                    eVar2 = null;
                    method3 = method2;
                    eVar3 = eVar;
                    return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
                }
            } catch (ClassNotFoundException e9) {
                method4 = null;
                method = null;
                method2 = method4;
                eVar = null;
                eVar2 = null;
                method3 = method2;
                eVar3 = eVar;
                return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
            } catch (NoSuchMethodException e10) {
                eVar = null;
                method = null;
                method2 = null;
                eVar2 = null;
                method3 = method2;
                eVar3 = eVar;
                return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
            }
            return new a(eVar8, eVar9, method3, method, eVar3, eVar2);
        } catch (ClassNotFoundException e11) {
            try {
                Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN");
                Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
                return new b(cls2.getMethod("put", SSLSocket.class, cls3), cls2.getMethod("get", SSLSocket.class), cls2.getMethod(ProductAction.ACTION_REMOVE, SSLSocket.class), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider"), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider"));
            } catch (ClassNotFoundException | NoSuchMethodException e12) {
                return new f();
            }
        }
    }

    /* compiled from: Platform */
    private static class a extends f {

        /* renamed from: a  reason: collision with root package name */
        private final e<Socket> f6493a;

        /* renamed from: b  reason: collision with root package name */
        private final e<Socket> f6494b;

        /* renamed from: c  reason: collision with root package name */
        private final Method f6495c;

        /* renamed from: d  reason: collision with root package name */
        private final Method f6496d;

        /* renamed from: e  reason: collision with root package name */
        private final e<Socket> f6497e;

        /* renamed from: f  reason: collision with root package name */
        private final e<Socket> f6498f;

        public a(e<Socket> eVar, e<Socket> eVar2, Method method, Method method2, e<Socket> eVar3, e<Socket> eVar4) {
            this.f6493a = eVar;
            this.f6494b = eVar2;
            this.f6495c = method;
            this.f6496d = method2;
            this.f6497e = eVar3;
            this.f6498f = eVar4;
        }

        public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
            try {
                socket.connect(inetSocketAddress, i);
            } catch (SecurityException e2) {
                IOException iOException = new IOException("Exception in connect");
                iOException.initCause(e2);
                throw iOException;
            }
        }

        public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
            if (str != null) {
                this.f6493a.b(sSLSocket, true);
                this.f6494b.b(sSLSocket, str);
            }
            if (this.f6498f != null && this.f6498f.a(sSLSocket)) {
                this.f6498f.d(sSLSocket, a(list));
            }
        }

        public String b(SSLSocket sSLSocket) {
            if (this.f6497e == null || !this.f6497e.a(sSLSocket)) {
                return null;
            }
            byte[] bArr = (byte[]) this.f6497e.d(sSLSocket, new Object[0]);
            return bArr != null ? new String(bArr, h.f6510c) : null;
        }

        public void a(Socket socket) {
            if (this.f6495c != null) {
                try {
                    this.f6495c.invoke(null, socket);
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                } catch (InvocationTargetException e3) {
                    throw new RuntimeException(e3.getCause());
                }
            }
        }

        public void b(Socket socket) {
            if (this.f6496d != null) {
                try {
                    this.f6496d.invoke(null, socket);
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                } catch (InvocationTargetException e3) {
                    throw new RuntimeException(e3.getCause());
                }
            }
        }
    }

    /* compiled from: Platform */
    private static class b extends f {

        /* renamed from: a  reason: collision with root package name */
        private final Method f6499a;

        /* renamed from: b  reason: collision with root package name */
        private final Method f6500b;

        /* renamed from: c  reason: collision with root package name */
        private final Method f6501c;

        /* renamed from: d  reason: collision with root package name */
        private final Class<?> f6502d;

        /* renamed from: e  reason: collision with root package name */
        private final Class<?> f6503e;

        public b(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
            this.f6499a = method;
            this.f6500b = method2;
            this.f6501c = method3;
            this.f6502d = cls;
            this.f6503e = cls2;
        }

        public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
            ArrayList arrayList = new ArrayList(list.size());
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Protocol protocol = list.get(i);
                if (protocol != Protocol.HTTP_1_0) {
                    arrayList.add(protocol.toString());
                }
            }
            try {
                this.f6499a.invoke(null, sSLSocket, Proxy.newProxyInstance(f.class.getClassLoader(), new Class[]{this.f6502d, this.f6503e}, new c(arrayList)));
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError(e2);
            }
        }

        public void a(SSLSocket sSLSocket) {
            try {
                this.f6501c.invoke(null, sSLSocket);
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError();
            }
        }

        public String b(SSLSocket sSLSocket) {
            try {
                c cVar = (c) Proxy.getInvocationHandler(this.f6500b.invoke(null, sSLSocket));
                if (cVar.f6505b || cVar.f6506c != null) {
                    return cVar.f6505b ? null : cVar.f6506c;
                }
                a.f6394a.log(Level.INFO, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?");
                return null;
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError();
            }
        }
    }

    /* compiled from: Platform */
    private static class c implements InvocationHandler {

        /* renamed from: a  reason: collision with root package name */
        private final List<String> f6504a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public boolean f6505b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public String f6506c;

        public c(List<String> list) {
            this.f6504a = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = h.f6509b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f6505b = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f6504a;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f6504a.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.f6506c = str;
                            return str;
                        }
                    }
                    String str2 = this.f6504a.get(0);
                    this.f6506c = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.f6506c = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    static byte[] a(List<Protocol> list) {
        okio.c cVar = new okio.c();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                cVar.i(protocol.toString().length());
                cVar.b(protocol.toString());
            }
        }
        return cVar.q();
    }
}
