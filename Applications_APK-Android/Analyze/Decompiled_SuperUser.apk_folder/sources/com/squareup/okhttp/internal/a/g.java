package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.i;
import com.squareup.okhttp.internal.a.c;
import com.squareup.okhttp.internal.b;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.j;
import com.squareup.okhttp.o;
import com.squareup.okhttp.p;
import com.squareup.okhttp.q;
import com.squareup.okhttp.r;
import com.squareup.okhttp.s;
import com.squareup.okhttp.u;
import com.squareup.okhttp.v;
import com.squareup.okhttp.w;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okio.c;
import okio.d;
import okio.e;
import okio.k;
import okio.p;

/* compiled from: HttpEngine */
public final class g {

    /* renamed from: d  reason: collision with root package name */
    private static final v f6433d = new v() {
        public q a() {
            return null;
        }

        public long b() {
            return 0;
        }

        public e c() {
            return new c();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final r f6434a;

    /* renamed from: b  reason: collision with root package name */
    long f6435b = -1;

    /* renamed from: c  reason: collision with root package name */
    public final boolean f6436c;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public i f6437e;

    /* renamed from: f  reason: collision with root package name */
    private com.squareup.okhttp.a f6438f;

    /* renamed from: g  reason: collision with root package name */
    private n f6439g;

    /* renamed from: h  reason: collision with root package name */
    private w f6440h;
    private final u i;
    /* access modifiers changed from: private */
    public q j;
    private boolean k;
    private final s l;
    private s m;
    private u n;
    private u o;
    private p p;
    private d q;
    private final boolean r;
    private final boolean s;
    private b t;
    private c u;

    public g(r rVar, s sVar, boolean z, boolean z2, boolean z3, i iVar, n nVar, m mVar, u uVar) {
        this.f6434a = rVar;
        this.l = sVar;
        this.f6436c = z;
        this.r = z2;
        this.s = z3;
        this.f6437e = iVar;
        this.f6439g = nVar;
        this.p = mVar;
        this.i = uVar;
        if (iVar != null) {
            com.squareup.okhttp.internal.a.f6395b.b(iVar, this);
            this.f6440h = iVar.c();
            return;
        }
        this.f6440h = null;
    }

    public void a() {
        if (this.u == null) {
            if (this.j != null) {
                throw new IllegalStateException();
            }
            s a2 = a(this.l);
            b a3 = com.squareup.okhttp.internal.a.f6395b.a(this.f6434a);
            u a4 = a3 != null ? a3.a(a2) : null;
            this.u = new c.a(System.currentTimeMillis(), a2, a4).a();
            this.m = this.u.f6397a;
            this.n = this.u.f6398b;
            if (a3 != null) {
                a3.a(this.u);
            }
            if (a4 != null && this.n == null) {
                h.a(a4.g());
            }
            if (this.m != null) {
                if (this.f6437e == null) {
                    m();
                }
                this.j = com.squareup.okhttp.internal.a.f6395b.a(this.f6437e, this);
                if (this.r && c() && this.p == null) {
                    long a5 = j.a(a2);
                    if (!this.f6436c) {
                        this.j.a(this.m);
                        this.p = this.j.a(this.m, a5);
                    } else if (a5 > 2147483647L) {
                        throw new IllegalStateException("Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB.");
                    } else if (a5 != -1) {
                        this.j.a(this.m);
                        this.p = new m((int) a5);
                    } else {
                        this.p = new m();
                    }
                }
            } else {
                if (this.f6437e != null) {
                    com.squareup.okhttp.internal.a.f6395b.a(this.f6434a.m(), this.f6437e);
                    this.f6437e = null;
                }
                if (this.n != null) {
                    this.o = this.n.h().a(this.l).c(b(this.i)).b(b(this.n)).a();
                } else {
                    this.o = new u.a().a(this.l).c(b(this.i)).a(Protocol.HTTP_1_1).a(504).a("Unsatisfiable Request (only-if-cached)").a(f6433d).a();
                }
                this.o = c(this.o);
            }
        }
    }

    private static u b(u uVar) {
        return (uVar == null || uVar.g() == null) ? uVar : uVar.h().a((v) null).a();
    }

    private void m() {
        if (this.f6437e != null) {
            throw new IllegalStateException();
        }
        if (this.f6439g == null) {
            this.f6438f = a(this.f6434a, this.m);
            this.f6439g = n.a(this.f6438f, this.m, this.f6434a);
        }
        this.f6437e = n();
        this.f6440h = this.f6437e.c();
    }

    private i n() {
        i o2 = o();
        com.squareup.okhttp.internal.a.f6395b.a(this.f6434a, o2, this, this.m);
        return o2;
    }

    private i o() {
        j m2 = this.f6434a.m();
        while (true) {
            i a2 = m2.a(this.f6438f);
            if (a2 == null) {
                return new i(m2, this.f6439g.b());
            }
            if (this.m.d().equals("GET") || com.squareup.okhttp.internal.a.f6395b.c(a2)) {
                return a2;
            }
            a2.d().close();
        }
    }

    public void b() {
        if (this.f6435b != -1) {
            throw new IllegalStateException();
        }
        this.f6435b = System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return h.c(this.l.d());
    }

    public s d() {
        return this.l;
    }

    public u e() {
        if (this.o != null) {
            return this.o;
        }
        throw new IllegalStateException();
    }

    public i f() {
        return this.f6437e;
    }

    public g a(IOException iOException, p pVar) {
        if (!(this.f6439g == null || this.f6437e == null)) {
            a(this.f6439g, iOException);
        }
        boolean z = pVar == null || (pVar instanceof m);
        if ((this.f6439g == null && this.f6437e == null) || ((this.f6439g != null && !this.f6439g.a()) || !a(iOException) || !z)) {
            return null;
        }
        return new g(this.f6434a, this.l, this.f6436c, this.r, this.s, j(), this.f6439g, (m) pVar, this.i);
    }

    private void a(n nVar, IOException iOException) {
        if (com.squareup.okhttp.internal.a.f6395b.b(this.f6437e) <= 0) {
            nVar.a(this.f6437e.c(), iOException);
        }
    }

    private boolean a(IOException iOException) {
        if (!this.f6434a.p() || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof ProtocolException) && !(iOException instanceof InterruptedIOException)) {
            return true;
        }
        return false;
    }

    public w g() {
        return this.f6440h;
    }

    private void p() {
        b a2 = com.squareup.okhttp.internal.a.f6395b.a(this.f6434a);
        if (a2 != null) {
            if (c.a(this.o, this.m)) {
                this.t = a2.a(b(this.o));
            } else if (h.a(this.m.d())) {
                try {
                    a2.b(this.m);
                } catch (IOException e2) {
                }
            }
        }
    }

    public void h() {
        if (!(this.j == null || this.f6437e == null)) {
            this.j.c();
        }
        this.f6437e = null;
    }

    public void i() {
        if (this.j != null) {
            try {
                this.j.a(this);
            } catch (IOException e2) {
            }
        }
    }

    public i j() {
        if (this.q != null) {
            h.a(this.q);
        } else if (this.p != null) {
            h.a(this.p);
        }
        if (this.o == null) {
            if (this.f6437e != null) {
                h.a(this.f6437e.d());
            }
            this.f6437e = null;
            return null;
        }
        h.a(this.o.g());
        if (this.j == null || this.f6437e == null || this.j.d()) {
            if (this.f6437e != null && !com.squareup.okhttp.internal.a.f6395b.a(this.f6437e)) {
                this.f6437e = null;
            }
            i iVar = this.f6437e;
            this.f6437e = null;
            return iVar;
        }
        h.a(this.f6437e.d());
        this.f6437e = null;
        return null;
    }

    private u c(u uVar) {
        if (!this.k || !"gzip".equalsIgnoreCase(this.o.a("Content-Encoding")) || uVar.g() == null) {
            return uVar;
        }
        okio.i iVar = new okio.i(uVar.g().c());
        o a2 = uVar.f().b().b("Content-Encoding").b("Content-Length").a();
        return uVar.h().a(a2).a(new k(a2, k.a(iVar))).a();
    }

    public static boolean a(u uVar) {
        if (uVar.a().d().equals("HEAD")) {
            return false;
        }
        int c2 = uVar.c();
        if ((c2 < 100 || c2 >= 200) && c2 != 204 && c2 != 304) {
            return true;
        }
        if (j.a(uVar) != -1 || "chunked".equalsIgnoreCase(uVar.a("Transfer-Encoding"))) {
            return true;
        }
        return false;
    }

    private s a(s sVar) {
        s.a h2 = sVar.h();
        if (sVar.a("Host") == null) {
            h2.a("Host", a(sVar.a()));
        }
        if ((this.f6437e == null || this.f6437e.l() != Protocol.HTTP_1_0) && sVar.a("Connection") == null) {
            h2.a("Connection", "Keep-Alive");
        }
        if (sVar.a("Accept-Encoding") == null) {
            this.k = true;
            h2.a("Accept-Encoding", "gzip");
        }
        CookieHandler f2 = this.f6434a.f();
        if (f2 != null) {
            j.a(h2, f2.get(sVar.b(), j.a(h2.b().e(), (String) null)));
        }
        if (sVar.a(io.fabric.sdk.android.services.common.a.HEADER_USER_AGENT) == null) {
            h2.a(io.fabric.sdk.android.services.common.a.HEADER_USER_AGENT, com.squareup.okhttp.internal.i.a());
        }
        return h2.b();
    }

    public static String a(URL url) {
        if (h.a(url) != h.a(url.getProtocol())) {
            return url.getHost() + ":" + url.getPort();
        }
        return url.getHost();
    }

    public void k() {
        u q2;
        if (this.o == null) {
            if (this.m == null && this.n == null) {
                throw new IllegalStateException("call sendRequest() first!");
            } else if (this.m != null) {
                if (this.s) {
                    this.j.a(this.m);
                    q2 = q();
                } else if (!this.r) {
                    q2 = new a(0, this.m).a(this.m);
                } else {
                    if (this.q != null && this.q.c().b() > 0) {
                        this.q.e();
                    }
                    if (this.f6435b == -1) {
                        if (j.a(this.m) == -1 && (this.p instanceof m)) {
                            this.m = this.m.h().a("Content-Length", Long.toString(((m) this.p).b())).b();
                        }
                        this.j.a(this.m);
                    }
                    if (this.p != null) {
                        if (this.q != null) {
                            this.q.close();
                        } else {
                            this.p.close();
                        }
                        if (this.p instanceof m) {
                            this.j.a((m) this.p);
                        }
                    }
                    q2 = q();
                }
                a(q2.f());
                if (this.n != null) {
                    if (a(this.n, q2)) {
                        this.o = this.n.h().a(this.l).c(b(this.i)).a(a(this.n.f(), q2.f())).b(b(this.n)).a(b(q2)).a();
                        q2.g().close();
                        h();
                        b a2 = com.squareup.okhttp.internal.a.f6395b.a(this.f6434a);
                        a2.a();
                        a2.a(this.n, b(this.o));
                        this.o = c(this.o);
                        return;
                    }
                    h.a(this.n.g());
                }
                this.o = q2.h().a(this.l).c(b(this.i)).b(b(this.n)).a(b(q2)).a();
                if (a(this.o)) {
                    p();
                    this.o = c(a(this.t, this.o));
                }
            }
        }
    }

    /* compiled from: HttpEngine */
    class a implements p.a {

        /* renamed from: b  reason: collision with root package name */
        private final int f6447b;

        /* renamed from: c  reason: collision with root package name */
        private final s f6448c;

        /* renamed from: d  reason: collision with root package name */
        private int f6449d;

        a(int i, s sVar) {
            this.f6447b = i;
            this.f6448c = sVar;
        }

        public i a() {
            return g.this.f6437e;
        }

        public u a(s sVar) {
            this.f6449d++;
            if (this.f6447b > 0) {
                com.squareup.okhttp.p pVar = g.this.f6434a.v().get(this.f6447b - 1);
                com.squareup.okhttp.a a2 = a().c().a();
                if (!sVar.a().getHost().equals(a2.a()) || h.a(sVar.a()) != a2.b()) {
                    throw new IllegalStateException("network interceptor " + pVar + " must retain the same host and port");
                } else if (this.f6449d > 1) {
                    throw new IllegalStateException("network interceptor " + pVar + " must call proceed() exactly once");
                }
            }
            if (this.f6447b < g.this.f6434a.v().size()) {
                a aVar = new a(this.f6447b + 1, sVar);
                com.squareup.okhttp.p pVar2 = g.this.f6434a.v().get(this.f6447b);
                u a3 = pVar2.a(aVar);
                if (aVar.f6449d == 1) {
                    return a3;
                }
                throw new IllegalStateException("network interceptor " + pVar2 + " must call proceed() exactly once");
            }
            g.this.j.a(sVar);
            if (g.this.c() && sVar.f() != null) {
                d a4 = k.a(g.this.j.a(sVar, sVar.f().b()));
                sVar.f().a(a4);
                a4.close();
            }
            return g.this.q();
        }
    }

    /* access modifiers changed from: private */
    public u q() {
        this.j.a();
        u a2 = this.j.b().a(this.m).a(this.f6437e.j()).a(j.f6453b, Long.toString(this.f6435b)).a(j.f6454c, Long.toString(System.currentTimeMillis())).a();
        if (!this.s) {
            a2 = a2.h().a(this.j.a(a2)).a();
        }
        com.squareup.okhttp.internal.a.f6395b.a(this.f6437e, a2.b());
        return a2;
    }

    private u a(final b bVar, u uVar) {
        okio.p a2;
        if (bVar == null || (a2 = bVar.a()) == null) {
            return uVar;
        }
        final e c2 = uVar.g().c();
        final d a3 = k.a(a2);
        return uVar.h().a(new k(uVar.f(), k.a(new okio.q() {

            /* renamed from: a  reason: collision with root package name */
            boolean f6441a;

            public long a(okio.c cVar, long j) {
                try {
                    long a2 = c2.a(cVar, j);
                    if (a2 == -1) {
                        if (!this.f6441a) {
                            this.f6441a = true;
                            a3.close();
                        }
                        return -1;
                    }
                    cVar.a(a3.c(), cVar.b() - a2, a2);
                    a3.u();
                    return a2;
                } catch (IOException e2) {
                    if (!this.f6441a) {
                        this.f6441a = true;
                        bVar.b();
                    }
                    throw e2;
                }
            }

            public okio.r a() {
                return c2.a();
            }

            public void close() {
                if (!this.f6441a && !h.a(this, 100, TimeUnit.MILLISECONDS)) {
                    this.f6441a = true;
                    bVar.b();
                }
                c2.close();
            }
        }))).a();
    }

    private static boolean a(u uVar, u uVar2) {
        Date b2;
        if (uVar2.c() == 304) {
            return true;
        }
        Date b3 = uVar.f().b("Last-Modified");
        if (b3 == null || (b2 = uVar2.f().b("Last-Modified")) == null || b2.getTime() >= b3.getTime()) {
            return false;
        }
        return true;
    }

    private static o a(o oVar, o oVar2) {
        o.a aVar = new o.a();
        int a2 = oVar.a();
        for (int i2 = 0; i2 < a2; i2++) {
            String a3 = oVar.a(i2);
            String b2 = oVar.b(i2);
            if ((!"Warning".equalsIgnoreCase(a3) || !b2.startsWith("1")) && (!j.a(a3) || oVar2.a(a3) == null)) {
                aVar.a(a3, b2);
            }
        }
        int a4 = oVar2.a();
        for (int i3 = 0; i3 < a4; i3++) {
            String a5 = oVar2.a(i3);
            if (!"Content-Length".equalsIgnoreCase(a5) && j.a(a5)) {
                aVar.a(a5, oVar2.b(i3));
            }
        }
        return aVar.a();
    }

    public void a(o oVar) {
        CookieHandler f2 = this.f6434a.f();
        if (f2 != null) {
            f2.put(this.l.b(), j.a(oVar, (String) null));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.squareup.okhttp.s l() {
        /*
            r4 = this;
            r1 = 0
            com.squareup.okhttp.u r0 = r4.o
            if (r0 != 0) goto L_0x000b
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x000b:
            com.squareup.okhttp.w r0 = r4.g()
            if (r0 == 0) goto L_0x0024
            com.squareup.okhttp.w r0 = r4.g()
            java.net.Proxy r0 = r0.b()
        L_0x0019:
            com.squareup.okhttp.u r2 = r4.o
            int r2 = r2.c()
            switch(r2) {
                case 300: goto L_0x0066;
                case 301: goto L_0x0066;
                case 302: goto L_0x0066;
                case 303: goto L_0x0066;
                case 307: goto L_0x0048;
                case 308: goto L_0x0048;
                case 401: goto L_0x003b;
                case 407: goto L_0x002b;
                default: goto L_0x0022;
            }
        L_0x0022:
            r0 = r1
        L_0x0023:
            return r0
        L_0x0024:
            com.squareup.okhttp.r r0 = r4.f6434a
            java.net.Proxy r0 = r0.d()
            goto L_0x0019
        L_0x002b:
            java.net.Proxy$Type r1 = r0.type()
            java.net.Proxy$Type r2 = java.net.Proxy.Type.HTTP
            if (r1 == r2) goto L_0x003b
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Received HTTP_PROXY_AUTH (407) code while not using proxy"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            com.squareup.okhttp.r r1 = r4.f6434a
            com.squareup.okhttp.b r1 = r1.l()
            com.squareup.okhttp.u r2 = r4.o
            com.squareup.okhttp.s r0 = com.squareup.okhttp.internal.a.j.a(r1, r2, r0)
            goto L_0x0023
        L_0x0048:
            com.squareup.okhttp.s r0 = r4.l
            java.lang.String r0 = r0.d()
            java.lang.String r2 = "GET"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0066
            com.squareup.okhttp.s r0 = r4.l
            java.lang.String r0 = r0.d()
            java.lang.String r2 = "HEAD"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0066
            r0 = r1
            goto L_0x0023
        L_0x0066:
            com.squareup.okhttp.r r0 = r4.f6434a
            boolean r0 = r0.o()
            if (r0 != 0) goto L_0x0070
            r0 = r1
            goto L_0x0023
        L_0x0070:
            com.squareup.okhttp.u r0 = r4.o
            java.lang.String r2 = "Location"
            java.lang.String r0 = r0.a(r2)
            if (r0 != 0) goto L_0x007c
            r0 = r1
            goto L_0x0023
        L_0x007c:
            java.net.URL r2 = new java.net.URL
            com.squareup.okhttp.s r3 = r4.l
            java.net.URL r3 = r3.a()
            r2.<init>(r3, r0)
            java.lang.String r0 = r2.getProtocol()
            java.lang.String r3 = "https"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00a1
            java.lang.String r0 = r2.getProtocol()
            java.lang.String r3 = "http"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00a1
            r0 = r1
            goto L_0x0023
        L_0x00a1:
            java.lang.String r0 = r2.getProtocol()
            com.squareup.okhttp.s r3 = r4.l
            java.net.URL r3 = r3.a()
            java.lang.String r3 = r3.getProtocol()
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00c0
            com.squareup.okhttp.r r0 = r4.f6434a
            boolean r0 = r0.n()
            if (r0 != 0) goto L_0x00c0
            r0 = r1
            goto L_0x0023
        L_0x00c0:
            com.squareup.okhttp.s r0 = r4.l
            com.squareup.okhttp.s$a r0 = r0.h()
            com.squareup.okhttp.s r3 = r4.l
            java.lang.String r3 = r3.d()
            boolean r3 = com.squareup.okhttp.internal.a.h.c(r3)
            if (r3 == 0) goto L_0x00e6
            java.lang.String r3 = "GET"
            r0.a(r3, r1)
            java.lang.String r1 = "Transfer-Encoding"
            r0.b(r1)
            java.lang.String r1 = "Content-Length"
            r0.b(r1)
            java.lang.String r1 = "Content-Type"
            r0.b(r1)
        L_0x00e6:
            boolean r1 = r4.b(r2)
            if (r1 != 0) goto L_0x00f1
            java.lang.String r1 = "Authorization"
            r0.b(r1)
        L_0x00f1:
            com.squareup.okhttp.s$a r0 = r0.a(r2)
            com.squareup.okhttp.s r0 = r0.b()
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.a.g.l():com.squareup.okhttp.s");
    }

    public boolean b(URL url) {
        URL a2 = this.l.a();
        return a2.getHost().equals(url.getHost()) && h.a(a2) == h.a(url) && a2.getProtocol().equals(url.getProtocol());
    }

    private static com.squareup.okhttp.a a(r rVar, s sVar) {
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        com.squareup.okhttp.g gVar = null;
        String host = sVar.a().getHost();
        if (host == null || host.length() == 0) {
            throw new UnknownHostException(sVar.a().toString());
        }
        if (sVar.j()) {
            sSLSocketFactory = rVar.i();
            hostnameVerifier = rVar.j();
            gVar = rVar.k();
        } else {
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new com.squareup.okhttp.a(host, h.a(sVar.a()), rVar.h(), sSLSocketFactory, hostnameVerifier, gVar, rVar.l(), rVar.d(), rVar.s(), rVar.t(), rVar.e());
    }
}
