package com.squareup.okhttp.internal.spdy;

import java.util.List;
import okio.e;

/* compiled from: PushObserver */
public interface j {

    /* renamed from: a  reason: collision with root package name */
    public static final j f6582a = new j() {
        public boolean a(int i, List<c> list) {
            return true;
        }

        public boolean a(int i, List<c> list, boolean z) {
            return true;
        }

        public boolean a(int i, e eVar, int i2, boolean z) {
            eVar.g((long) i2);
            return true;
        }

        public void a(int i, ErrorCode errorCode) {
        }
    };

    void a(int i, ErrorCode errorCode);

    boolean a(int i, List<c> list);

    boolean a(int i, List<c> list, boolean z);

    boolean a(int i, e eVar, int i2, boolean z);
}
