package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.a;
import com.squareup.okhttp.internal.d;
import com.squareup.okhttp.internal.g;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.k;
import com.squareup.okhttp.r;
import com.squareup.okhttp.s;
import com.squareup.okhttp.w;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.UnknownServiceException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLProtocolException;

/* compiled from: RouteSelector */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    private final a f6462a;

    /* renamed from: b  reason: collision with root package name */
    private final URI f6463b;

    /* renamed from: c  reason: collision with root package name */
    private final d f6464c;

    /* renamed from: d  reason: collision with root package name */
    private final r f6465d;

    /* renamed from: e  reason: collision with root package name */
    private final g f6466e;

    /* renamed from: f  reason: collision with root package name */
    private final s f6467f;

    /* renamed from: g  reason: collision with root package name */
    private Proxy f6468g;

    /* renamed from: h  reason: collision with root package name */
    private InetSocketAddress f6469h;
    private k i;
    private List<Proxy> j = Collections.emptyList();
    private int k;
    private List<InetSocketAddress> l = Collections.emptyList();
    private int m;
    private List<k> n = Collections.emptyList();
    private int o;
    private final List<w> p = new ArrayList();

    private n(a aVar, URI uri, r rVar, s sVar) {
        this.f6462a = aVar;
        this.f6463b = uri;
        this.f6465d = rVar;
        this.f6466e = com.squareup.okhttp.internal.a.f6395b.b(rVar);
        this.f6464c = com.squareup.okhttp.internal.a.f6395b.c(rVar);
        this.f6467f = sVar;
        a(uri, aVar.d());
    }

    public static n a(a aVar, s sVar, r rVar) {
        return new n(aVar, sVar.b(), rVar, sVar);
    }

    public boolean a() {
        return h() || e() || c() || j();
    }

    public w b() {
        if (!h()) {
            if (!e()) {
                if (c()) {
                    this.f6468g = d();
                } else if (j()) {
                    return k();
                } else {
                    throw new NoSuchElementException();
                }
            }
            this.f6469h = f();
        }
        this.i = i();
        w wVar = new w(this.f6462a, this.f6468g, this.f6469h, this.i, a(this.i));
        if (!this.f6466e.c(wVar)) {
            return wVar;
        }
        this.p.add(wVar);
        return b();
    }

    private boolean a(k kVar) {
        if (kVar == this.n.get(0) || !kVar.a()) {
            return false;
        }
        return true;
    }

    public void a(w wVar, IOException iOException) {
        if (!(wVar.b().type() == Proxy.Type.DIRECT || this.f6462a.e() == null)) {
            this.f6462a.e().connectFailed(this.f6463b, wVar.b().address(), iOException);
        }
        this.f6466e.a(wVar);
        if (!(iOException instanceof SSLHandshakeException) && !(iOException instanceof SSLProtocolException)) {
            while (this.o < this.n.size()) {
                List<k> list = this.n;
                int i2 = this.o;
                this.o = i2 + 1;
                k kVar = list.get(i2);
                this.f6466e.a(new w(this.f6462a, this.f6468g, this.f6469h, kVar, a(kVar)));
            }
        }
    }

    private void a(URI uri, Proxy proxy) {
        if (proxy != null) {
            this.j = Collections.singletonList(proxy);
        } else {
            this.j = new ArrayList();
            List<Proxy> select = this.f6465d.e().select(uri);
            if (select != null) {
                this.j.addAll(select);
            }
            this.j.removeAll(Collections.singleton(Proxy.NO_PROXY));
            this.j.add(Proxy.NO_PROXY);
        }
        this.k = 0;
    }

    private boolean c() {
        return this.k < this.j.size();
    }

    private Proxy d() {
        if (!c()) {
            throw new SocketException("No route to " + this.f6462a.a() + "; exhausted proxy configurations: " + this.j);
        }
        List<Proxy> list = this.j;
        int i2 = this.k;
        this.k = i2 + 1;
        Proxy proxy = list.get(i2);
        a(proxy);
        return proxy;
    }

    private void a(Proxy proxy) {
        int i2;
        String str;
        this.l = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.f6462a.a();
            i2 = h.a(this.f6463b);
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            String a2 = a(inetSocketAddress);
            int port = inetSocketAddress.getPort();
            str = a2;
            i2 = port;
        }
        if (i2 < 1 || i2 > 65535) {
            throw new SocketException("No route to " + str + ":" + i2 + "; port is out of range");
        }
        for (InetAddress inetSocketAddress2 : this.f6464c.a(str)) {
            this.l.add(new InetSocketAddress(inetSocketAddress2, i2));
        }
        this.m = 0;
    }

    static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }

    private boolean e() {
        return this.m < this.l.size();
    }

    private InetSocketAddress f() {
        if (!e()) {
            throw new SocketException("No route to " + this.f6462a.a() + "; exhausted inet socket addresses: " + this.l);
        }
        List<InetSocketAddress> list = this.l;
        int i2 = this.m;
        this.m = i2 + 1;
        InetSocketAddress inetSocketAddress = list.get(i2);
        g();
        return inetSocketAddress;
    }

    private void g() {
        this.n = new ArrayList();
        List<k> c2 = this.f6462a.c();
        int size = c2.size();
        for (int i2 = 0; i2 < size; i2++) {
            k kVar = c2.get(i2);
            if (this.f6467f.j() == kVar.a()) {
                this.n.add(kVar);
            }
        }
        this.o = 0;
    }

    private boolean h() {
        return this.o < this.n.size();
    }

    private k i() {
        if (this.n.isEmpty()) {
            throw new UnknownServiceException("No route to " + (this.f6463b.getScheme() != null ? this.f6463b.getScheme() + "://" : "//") + this.f6462a.a() + "; no connection specs");
        } else if (!h()) {
            throw new SocketException("No route to " + (this.f6463b.getScheme() != null ? this.f6463b.getScheme() + "://" : "//") + this.f6462a.a() + "; exhausted connection specs: " + this.n);
        } else {
            List<k> list = this.n;
            int i2 = this.o;
            this.o = i2 + 1;
            return list.get(i2);
        }
    }

    private boolean j() {
        return !this.p.isEmpty();
    }

    private w k() {
        return this.p.remove(0);
    }
}
