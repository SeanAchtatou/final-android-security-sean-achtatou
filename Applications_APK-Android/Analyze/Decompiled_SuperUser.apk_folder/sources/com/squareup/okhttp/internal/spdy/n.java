package com.squareup.okhttp.internal.spdy;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;
import okio.e;
import okio.p;
import okio.q;
import okio.r;

/* compiled from: SpdyStream */
public final class n {

    /* renamed from: d  reason: collision with root package name */
    static final /* synthetic */ boolean f6642d = (!n.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    long f6643a = 0;

    /* renamed from: b  reason: collision with root package name */
    long f6644b;

    /* renamed from: c  reason: collision with root package name */
    final a f6645c;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final int f6646e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final m f6647f;

    /* renamed from: g  reason: collision with root package name */
    private final List<c> f6648g;

    /* renamed from: h  reason: collision with root package name */
    private List<c> f6649h;
    private final b i;
    /* access modifiers changed from: private */
    public final c j = new c();
    /* access modifiers changed from: private */
    public final c k = new c();
    /* access modifiers changed from: private */
    public ErrorCode l = null;

    n(int i2, m mVar, boolean z, boolean z2, List<c> list) {
        if (mVar == null) {
            throw new NullPointerException("connection == null");
        } else if (list == null) {
            throw new NullPointerException("requestHeaders == null");
        } else {
            this.f6646e = i2;
            this.f6647f = mVar;
            this.f6644b = (long) mVar.f6601f.e(65536);
            this.i = new b((long) mVar.f6600e.e(65536));
            this.f6645c = new a();
            boolean unused = this.i.f6661g = z2;
            boolean unused2 = this.f6645c.f6654e = z;
            this.f6648g = list;
        }
    }

    public int a() {
        return this.f6646e;
    }

    public synchronized boolean b() {
        boolean z = false;
        synchronized (this) {
            if (this.l == null) {
                if ((!this.i.f6661g && !this.i.f6660f) || ((!this.f6645c.f6654e && !this.f6645c.f6653d) || this.f6649h == null)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public boolean c() {
        boolean z;
        if ((this.f6646e & 1) == 1) {
            z = true;
        } else {
            z = false;
        }
        return this.f6647f.f6597b == z;
    }

    public synchronized List<c> d() {
        this.j.c();
        while (this.f6649h == null && this.l == null) {
            try {
                k();
            } catch (Throwable th) {
                this.j.b();
                throw th;
            }
        }
        this.j.b();
        if (this.f6649h != null) {
        } else {
            throw new IOException("stream was reset: " + this.l);
        }
        return this.f6649h;
    }

    public r e() {
        return this.j;
    }

    public q f() {
        return this.i;
    }

    public p g() {
        synchronized (this) {
            if (this.f6649h == null && !c()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.f6645c;
    }

    public void a(ErrorCode errorCode) {
        if (d(errorCode)) {
            this.f6647f.b(this.f6646e, errorCode);
        }
    }

    public void b(ErrorCode errorCode) {
        if (d(errorCode)) {
            this.f6647f.a(this.f6646e, errorCode);
        }
    }

    private boolean d(ErrorCode errorCode) {
        if (f6642d || !Thread.holdsLock(this)) {
            synchronized (this) {
                if (this.l != null) {
                    return false;
                }
                if (this.i.f6661g && this.f6645c.f6654e) {
                    return false;
                }
                this.l = errorCode;
                notifyAll();
                this.f6647f.b(this.f6646e);
                return true;
            }
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void a(List<c> list, HeadersMode headersMode) {
        if (f6642d || !Thread.holdsLock(this)) {
            ErrorCode errorCode = null;
            boolean z = true;
            synchronized (this) {
                if (this.f6649h == null) {
                    if (headersMode.c()) {
                        errorCode = ErrorCode.PROTOCOL_ERROR;
                    } else {
                        this.f6649h = list;
                        z = b();
                        notifyAll();
                    }
                } else if (headersMode.d()) {
                    errorCode = ErrorCode.STREAM_IN_USE;
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.f6649h);
                    arrayList.addAll(list);
                    this.f6649h = arrayList;
                }
            }
            if (errorCode != null) {
                b(errorCode);
            } else if (!z) {
                this.f6647f.b(this.f6646e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar, int i2) {
        if (f6642d || !Thread.holdsLock(this)) {
            this.i.a(eVar, (long) i2);
            return;
        }
        throw new AssertionError();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.okhttp.internal.spdy.n.b.a(com.squareup.okhttp.internal.spdy.n$b, boolean):boolean
     arg types: [com.squareup.okhttp.internal.spdy.n$b, int]
     candidates:
      com.squareup.okhttp.internal.spdy.n.b.a(okio.c, long):long
      com.squareup.okhttp.internal.spdy.n.b.a(okio.e, long):void
      okio.q.a(okio.c, long):long
      com.squareup.okhttp.internal.spdy.n.b.a(com.squareup.okhttp.internal.spdy.n$b, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void h() {
        boolean b2;
        if (f6642d || !Thread.holdsLock(this)) {
            synchronized (this) {
                boolean unused = this.i.f6661g = true;
                b2 = b();
                notifyAll();
            }
            if (!b2) {
                this.f6647f.b(this.f6646e);
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(ErrorCode errorCode) {
        if (this.l == null) {
            this.l = errorCode;
            notifyAll();
        }
    }

    /* compiled from: SpdyStream */
    private final class b implements q {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ boolean f6655a = (!n.class.desiredAssertionStatus());

        /* renamed from: c  reason: collision with root package name */
        private final okio.c f6657c;

        /* renamed from: d  reason: collision with root package name */
        private final okio.c f6658d;

        /* renamed from: e  reason: collision with root package name */
        private final long f6659e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public boolean f6660f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public boolean f6661g;

        private b(long j) {
            this.f6657c = new okio.c();
            this.f6658d = new okio.c();
            this.f6659e = j;
        }

        public long a(okio.c cVar, long j) {
            long a2;
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
            synchronized (n.this) {
                b();
                c();
                if (this.f6658d.b() == 0) {
                    a2 = -1;
                } else {
                    a2 = this.f6658d.a(cVar, Math.min(j, this.f6658d.b()));
                    n.this.f6643a += a2;
                    if (n.this.f6643a >= ((long) (n.this.f6647f.f6600e.e(65536) / 2))) {
                        n.this.f6647f.a(n.this.f6646e, n.this.f6643a);
                        n.this.f6643a = 0;
                    }
                    synchronized (n.this.f6647f) {
                        n.this.f6647f.f6598c += a2;
                        if (n.this.f6647f.f6598c >= ((long) (n.this.f6647f.f6600e.e(65536) / 2))) {
                            n.this.f6647f.a(0, n.this.f6647f.f6598c);
                            n.this.f6647f.f6598c = 0;
                        }
                    }
                }
            }
            return a2;
        }

        private void b() {
            n.this.j.c();
            while (this.f6658d.b() == 0 && !this.f6661g && !this.f6660f && n.this.l == null) {
                try {
                    n.this.k();
                } finally {
                    n.this.j.b();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(e eVar, long j) {
            boolean z;
            boolean z2;
            boolean z3;
            if (f6655a || !Thread.holdsLock(n.this)) {
                while (j > 0) {
                    synchronized (n.this) {
                        z = this.f6661g;
                        z2 = this.f6658d.b() + j > this.f6659e;
                    }
                    if (z2) {
                        eVar.g(j);
                        n.this.b(ErrorCode.FLOW_CONTROL_ERROR);
                        return;
                    } else if (z) {
                        eVar.g(j);
                        return;
                    } else {
                        long a2 = eVar.a(this.f6657c, j);
                        if (a2 == -1) {
                            throw new EOFException();
                        }
                        j -= a2;
                        synchronized (n.this) {
                            if (this.f6658d.b() == 0) {
                                z3 = true;
                            } else {
                                z3 = false;
                            }
                            this.f6658d.a(this.f6657c);
                            if (z3) {
                                n.this.notifyAll();
                            }
                        }
                    }
                }
                return;
            }
            throw new AssertionError();
        }

        public r a() {
            return n.this.j;
        }

        public void close() {
            synchronized (n.this) {
                this.f6660f = true;
                this.f6658d.r();
                n.this.notifyAll();
            }
            n.this.i();
        }

        private void c() {
            if (this.f6660f) {
                throw new IOException("stream closed");
            } else if (n.this.l != null) {
                throw new IOException("stream was reset: " + n.this.l);
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        boolean z;
        boolean b2;
        if (f6642d || !Thread.holdsLock(this)) {
            synchronized (this) {
                z = !this.i.f6661g && this.i.f6660f && (this.f6645c.f6654e || this.f6645c.f6653d);
                b2 = b();
            }
            if (z) {
                a(ErrorCode.CANCEL);
            } else if (!b2) {
                this.f6647f.b(this.f6646e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* compiled from: SpdyStream */
    final class a implements p {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ boolean f6650a = (!n.class.desiredAssertionStatus());

        /* renamed from: c  reason: collision with root package name */
        private final okio.c f6652c = new okio.c();
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public boolean f6653d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public boolean f6654e;

        a() {
        }

        public void a_(okio.c cVar, long j) {
            if (f6650a || !Thread.holdsLock(n.this)) {
                this.f6652c.a_(cVar, j);
                while (this.f6652c.b() >= 16384) {
                    a(false);
                }
                return;
            }
            throw new AssertionError();
        }

        private void a(boolean z) {
            long min;
            synchronized (n.this) {
                n.this.k.c();
                while (n.this.f6644b <= 0 && !this.f6654e && !this.f6653d && n.this.l == null) {
                    try {
                        n.this.k();
                    } catch (Throwable th) {
                        n.this.k.b();
                        throw th;
                    }
                }
                n.this.k.b();
                n.this.j();
                min = Math.min(n.this.f6644b, this.f6652c.b());
                n.this.f6644b -= min;
            }
            n.this.f6647f.a(n.this.f6646e, z && min == this.f6652c.b(), this.f6652c, min);
        }

        public void flush() {
            if (f6650a || !Thread.holdsLock(n.this)) {
                synchronized (n.this) {
                    n.this.j();
                }
                while (this.f6652c.b() > 0) {
                    a(false);
                }
                n.this.f6647f.d();
                return;
            }
            throw new AssertionError();
        }

        public r a() {
            return n.this.k;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.squareup.okhttp.internal.spdy.m.a(int, boolean, okio.c, long):void
         arg types: [int, int, ?[OBJECT, ARRAY], int]
         candidates:
          com.squareup.okhttp.internal.spdy.m.a(int, java.util.List<com.squareup.okhttp.internal.spdy.c>, boolean, boolean):com.squareup.okhttp.internal.spdy.n
          com.squareup.okhttp.internal.spdy.m.a(int, okio.e, int, boolean):void
          com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, int, java.util.List, boolean):void
          com.squareup.okhttp.internal.spdy.m.a(boolean, int, int, com.squareup.okhttp.internal.spdy.i):void
          com.squareup.okhttp.internal.spdy.m.a(int, boolean, okio.c, long):void */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
            if (r6.f6651b.f6645c.f6654e != false) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
            if (r6.f6652c.b() <= 0) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
            if (r6.f6652c.b() <= 0) goto L_0x0052;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
            com.squareup.okhttp.internal.spdy.n.a(r6.f6651b).a(com.squareup.okhttp.internal.spdy.n.b(r6.f6651b), true, (okio.c) null, 0L);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
            r1 = r6.f6651b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r6.f6653d = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0058, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
            com.squareup.okhttp.internal.spdy.n.a(r6.f6651b).d();
            com.squareup.okhttp.internal.spdy.n.f(r6.f6651b);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                boolean r0 = com.squareup.okhttp.internal.spdy.n.a.f6650a
                if (r0 != 0) goto L_0x0015
                com.squareup.okhttp.internal.spdy.n r0 = com.squareup.okhttp.internal.spdy.n.this
                boolean r0 = java.lang.Thread.holdsLock(r0)
                if (r0 == 0) goto L_0x0015
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0015:
                com.squareup.okhttp.internal.spdy.n r1 = com.squareup.okhttp.internal.spdy.n.this
                monitor-enter(r1)
                boolean r0 = r6.f6653d     // Catch:{ all -> 0x003f }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
            L_0x001d:
                return
            L_0x001e:
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                com.squareup.okhttp.internal.spdy.n r0 = com.squareup.okhttp.internal.spdy.n.this
                com.squareup.okhttp.internal.spdy.n$a r0 = r0.f6645c
                boolean r0 = r0.f6654e
                if (r0 != 0) goto L_0x0052
                okio.c r0 = r6.f6652c
                long r0 = r0.b()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0042
            L_0x0031:
                okio.c r0 = r6.f6652c
                long r0 = r0.b()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0052
                r6.a(r2)
                goto L_0x0031
            L_0x003f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003f }
                throw r0
            L_0x0042:
                com.squareup.okhttp.internal.spdy.n r0 = com.squareup.okhttp.internal.spdy.n.this
                com.squareup.okhttp.internal.spdy.m r0 = r0.f6647f
                com.squareup.okhttp.internal.spdy.n r1 = com.squareup.okhttp.internal.spdy.n.this
                int r1 = r1.f6646e
                r3 = 0
                r0.a(r1, r2, r3, r4)
            L_0x0052:
                com.squareup.okhttp.internal.spdy.n r1 = com.squareup.okhttp.internal.spdy.n.this
                monitor-enter(r1)
                r0 = 1
                r6.f6653d = r0     // Catch:{ all -> 0x0068 }
                monitor-exit(r1)     // Catch:{ all -> 0x0068 }
                com.squareup.okhttp.internal.spdy.n r0 = com.squareup.okhttp.internal.spdy.n.this
                com.squareup.okhttp.internal.spdy.m r0 = r0.f6647f
                r0.d()
                com.squareup.okhttp.internal.spdy.n r0 = com.squareup.okhttp.internal.spdy.n.this
                r0.i()
                goto L_0x001d
            L_0x0068:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0068 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.n.a.close():void");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.f6644b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f6645c.f6653d) {
            throw new IOException("stream closed");
        } else if (this.f6645c.f6654e) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new IOException("stream was reset: " + this.l);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        try {
            wait();
        } catch (InterruptedException e2) {
            throw new InterruptedIOException();
        }
    }

    /* compiled from: SpdyStream */
    class c extends okio.a {
        c() {
        }

        /* access modifiers changed from: protected */
        public void a() {
            n.this.b(ErrorCode.CANCEL);
        }

        public void b() {
            if (b_()) {
                throw new InterruptedIOException("timeout");
            }
        }
    }
}
