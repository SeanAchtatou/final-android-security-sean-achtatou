package com.squareup.okhttp.internal;

import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import okio.ByteString;
import okio.c;
import okio.q;

/* compiled from: Util */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f6508a = new byte[0];

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f6509b = new String[0];

    /* renamed from: c  reason: collision with root package name */
    public static final Charset f6510c = Charset.forName("UTF-8");

    public static int a(URI uri) {
        return a(uri.getScheme(), uri.getPort());
    }

    public static int a(URL url) {
        return a(url.getProtocol(), url.getPort());
    }

    private static int a(String str, int i) {
        return i != -1 ? i : a(str);
    }

    public static int a(String str) {
        if ("http".equals(str)) {
            return 80;
        }
        if ("https".equals(str)) {
            return 443;
        }
        return -1;
    }

    public static void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    public static void a(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    public static void a(Closeable closeable, Closeable closeable2) {
        Throwable th = null;
        try {
            closeable.close();
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            closeable2.close();
        } catch (Throwable th3) {
            if (th == null) {
                th = th3;
            }
        }
        if (th != null) {
            if (th instanceof IOException) {
                throw ((IOException) th);
            } else if (th instanceof RuntimeException) {
                throw ((RuntimeException) th);
            } else if (th instanceof Error) {
                throw ((Error) th);
            } else {
                throw new AssertionError(th);
            }
        }
    }

    public static boolean a(q qVar, int i, TimeUnit timeUnit) {
        try {
            return b(qVar, i, timeUnit);
        } catch (IOException e2) {
            return false;
        }
    }

    public static boolean b(q qVar, int i, TimeUnit timeUnit) {
        long nanoTime = System.nanoTime();
        long d2 = qVar.a().d_() ? qVar.a().d() - nanoTime : Long.MAX_VALUE;
        qVar.a().a(Math.min(d2, timeUnit.toNanos((long) i)) + nanoTime);
        try {
            c cVar = new c();
            while (qVar.a(cVar, 2048) != -1) {
                cVar.r();
            }
            if (d2 == Long.MAX_VALUE) {
                qVar.a().f_();
            } else {
                qVar.a().a(d2 + nanoTime);
            }
            return true;
        } catch (InterruptedIOException e2) {
            if (d2 == Long.MAX_VALUE) {
                qVar.a().f_();
            } else {
                qVar.a().a(d2 + nanoTime);
            }
            return false;
        } catch (Throwable th) {
            if (d2 == Long.MAX_VALUE) {
                qVar.a().f_();
            } else {
                qVar.a().a(d2 + nanoTime);
            }
            throw th;
        }
    }

    public static ByteString a(ByteString byteString) {
        try {
            return ByteString.a(MessageDigest.getInstance("SHA-1").digest(byteString.h()));
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public static <T> List<T> a(List list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    public static <T> List<T> a(Object... objArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) objArr.clone()));
    }

    public static <K, V> Map<K, V> a(Map map) {
        return Collections.unmodifiableMap(new LinkedHashMap(map));
    }

    public static ThreadFactory a(final String str, final boolean z) {
        return new ThreadFactory() {
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, str);
                thread.setDaemon(z);
                return thread;
            }
        };
    }

    public static <T> T[] a(Class cls, Object[] objArr, Object[] objArr2) {
        List a2 = a(objArr, objArr2);
        return a2.toArray((Object[]) Array.newInstance(cls, a2.size()));
    }

    private static <T> List<T> a(Object[] objArr, Object[] objArr2) {
        ArrayList arrayList = new ArrayList();
        for (Object obj : objArr) {
            int length = objArr2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Object obj2 = objArr2[i];
                if (obj.equals(obj2)) {
                    arrayList.add(obj2);
                    break;
                }
                i++;
            }
        }
        return arrayList;
    }
}
