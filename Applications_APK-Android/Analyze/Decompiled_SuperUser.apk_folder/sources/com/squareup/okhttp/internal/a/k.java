package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.o;
import com.squareup.okhttp.q;
import com.squareup.okhttp.v;
import okio.e;

/* compiled from: RealResponseBody */
public final class k extends v {

    /* renamed from: a  reason: collision with root package name */
    private final o f6457a;

    /* renamed from: b  reason: collision with root package name */
    private final e f6458b;

    public k(o oVar, e eVar) {
        this.f6457a = oVar;
        this.f6458b = eVar;
    }

    public q a() {
        String a2 = this.f6457a.a("Content-Type");
        if (a2 != null) {
            return q.a(a2);
        }
        return null;
    }

    public long b() {
        return j.a(this.f6457a);
    }

    public e c() {
        return this.f6458b;
    }
}
