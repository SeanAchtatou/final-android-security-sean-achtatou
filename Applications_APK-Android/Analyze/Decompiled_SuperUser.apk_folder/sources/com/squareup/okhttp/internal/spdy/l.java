package com.squareup.okhttp.internal.spdy;

import com.lody.virtual.os.VUserInfo;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.internal.spdy.a;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.List;
import java.util.zip.Deflater;
import okio.ByteString;
import okio.c;
import okio.d;
import okio.e;
import okio.f;
import okio.k;
import okio.p;

/* compiled from: Spdy3 */
public final class l implements o {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f6587a;

    static {
        try {
            f6587a = "\u0000\u0000\u0000\u0007options\u0000\u0000\u0000\u0004head\u0000\u0000\u0000\u0004post\u0000\u0000\u0000\u0003put\u0000\u0000\u0000\u0006delete\u0000\u0000\u0000\u0005trace\u0000\u0000\u0000\u0006accept\u0000\u0000\u0000\u000eaccept-charset\u0000\u0000\u0000\u000faccept-encoding\u0000\u0000\u0000\u000faccept-language\u0000\u0000\u0000\raccept-ranges\u0000\u0000\u0000\u0003age\u0000\u0000\u0000\u0005allow\u0000\u0000\u0000\rauthorization\u0000\u0000\u0000\rcache-control\u0000\u0000\u0000\nconnection\u0000\u0000\u0000\fcontent-base\u0000\u0000\u0000\u0010content-encoding\u0000\u0000\u0000\u0010content-language\u0000\u0000\u0000\u000econtent-length\u0000\u0000\u0000\u0010content-location\u0000\u0000\u0000\u000bcontent-md5\u0000\u0000\u0000\rcontent-range\u0000\u0000\u0000\fcontent-type\u0000\u0000\u0000\u0004date\u0000\u0000\u0000\u0004etag\u0000\u0000\u0000\u0006expect\u0000\u0000\u0000\u0007expires\u0000\u0000\u0000\u0004from\u0000\u0000\u0000\u0004host\u0000\u0000\u0000\bif-match\u0000\u0000\u0000\u0011if-modified-since\u0000\u0000\u0000\rif-none-match\u0000\u0000\u0000\bif-range\u0000\u0000\u0000\u0013if-unmodified-since\u0000\u0000\u0000\rlast-modified\u0000\u0000\u0000\blocation\u0000\u0000\u0000\fmax-forwards\u0000\u0000\u0000\u0006pragma\u0000\u0000\u0000\u0012proxy-authenticate\u0000\u0000\u0000\u0013proxy-authorization\u0000\u0000\u0000\u0005range\u0000\u0000\u0000\u0007referer\u0000\u0000\u0000\u000bretry-after\u0000\u0000\u0000\u0006server\u0000\u0000\u0000\u0002te\u0000\u0000\u0000\u0007trailer\u0000\u0000\u0000\u0011transfer-encoding\u0000\u0000\u0000\u0007upgrade\u0000\u0000\u0000\nuser-agent\u0000\u0000\u0000\u0004vary\u0000\u0000\u0000\u0003via\u0000\u0000\u0000\u0007warning\u0000\u0000\u0000\u0010www-authenticate\u0000\u0000\u0000\u0006method\u0000\u0000\u0000\u0003get\u0000\u0000\u0000\u0006status\u0000\u0000\u0000\u0006200 OK\u0000\u0000\u0000\u0007version\u0000\u0000\u0000\bHTTP/1.1\u0000\u0000\u0000\u0003url\u0000\u0000\u0000\u0006public\u0000\u0000\u0000\nset-cookie\u0000\u0000\u0000\nkeep-alive\u0000\u0000\u0000\u0006origin100101201202205206300302303304305306307402405406407408409410411412413414415416417502504505203 Non-Authoritative Information204 No Content301 Moved Permanently400 Bad Request401 Unauthorized403 Forbidden404 Not Found500 Internal Server Error501 Not Implemented503 Service UnavailableJan Feb Mar Apr May Jun Jul Aug Sept Oct Nov Dec 00:00:00 Mon, Tue, Wed, Thu, Fri, Sat, Sun, GMTchunked,text/html,image/png,image/jpg,image/gif,application/xml,application/xhtml+xml,text/plain,text/javascript,publicprivatemax-age=gzip,deflate,sdchcharset=utf-8charset=iso-8859-1,utf-,*,enq=0.".getBytes(h.f6510c.name());
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError();
        }
    }

    public a a(e eVar, boolean z) {
        return new a(eVar, z);
    }

    public b a(d dVar, boolean z) {
        return new b(dVar, z);
    }

    /* compiled from: Spdy3 */
    static final class a implements a {

        /* renamed from: a  reason: collision with root package name */
        private final e f6588a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f6589b;

        /* renamed from: c  reason: collision with root package name */
        private final h f6590c = new h(this.f6588a);

        a(e eVar, boolean z) {
            this.f6588a = eVar;
            this.f6589b = z;
        }

        public void a() {
        }

        public boolean a(a.C0094a aVar) {
            boolean z = false;
            try {
                int j = this.f6588a.j();
                int j2 = this.f6588a.j();
                int i = (-16777216 & j2) >>> 24;
                int i2 = j2 & 16777215;
                if ((Integer.MIN_VALUE & j) != 0) {
                    int i3 = (2147418112 & j) >>> 16;
                    int i4 = 65535 & j;
                    if (i3 != 3) {
                        throw new ProtocolException("version != 3: " + i3);
                    }
                    switch (i4) {
                        case 1:
                            a(aVar, i, i2);
                            return true;
                        case 2:
                            b(aVar, i, i2);
                            return true;
                        case 3:
                            c(aVar, i, i2);
                            return true;
                        case 4:
                            h(aVar, i, i2);
                            return true;
                        case 5:
                        default:
                            this.f6588a.g((long) i2);
                            return true;
                        case 6:
                            f(aVar, i, i2);
                            return true;
                        case 7:
                            g(aVar, i, i2);
                            return true;
                        case 8:
                            d(aVar, i, i2);
                            return true;
                        case 9:
                            e(aVar, i, i2);
                            return true;
                    }
                } else {
                    int i5 = Integer.MAX_VALUE & j;
                    if ((i & 1) != 0) {
                        z = true;
                    }
                    aVar.a(z, i5, this.f6588a, i2);
                    return true;
                }
            } catch (IOException e2) {
                return false;
            }
        }

        private void a(a.C0094a aVar, int i, int i2) {
            boolean z;
            boolean z2 = true;
            int j = this.f6588a.j() & Integer.MAX_VALUE;
            int j2 = this.f6588a.j() & Integer.MAX_VALUE;
            this.f6588a.i();
            List<c> a2 = this.f6590c.a(i2 - 10);
            if ((i & 1) != 0) {
                z = true;
            } else {
                z = false;
            }
            if ((i & 2) == 0) {
                z2 = false;
            }
            aVar.a(z2, z, j, j2, a2, HeadersMode.SPDY_SYN_STREAM);
        }

        private void b(a.C0094a aVar, int i, int i2) {
            boolean z;
            int j = this.f6588a.j() & Integer.MAX_VALUE;
            List<c> a2 = this.f6590c.a(i2 - 4);
            if ((i & 1) != 0) {
                z = true;
            } else {
                z = false;
            }
            aVar.a(false, z, j, -1, a2, HeadersMode.SPDY_REPLY);
        }

        private void c(a.C0094a aVar, int i, int i2) {
            if (i2 != 8) {
                throw a("TYPE_RST_STREAM length: %d != 8", Integer.valueOf(i2));
            }
            int j = this.f6588a.j() & Integer.MAX_VALUE;
            int j2 = this.f6588a.j();
            ErrorCode a2 = ErrorCode.a(j2);
            if (a2 == null) {
                throw a("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(j2));
            } else {
                aVar.a(j, a2);
            }
        }

        private void d(a.C0094a aVar, int i, int i2) {
            aVar.a(false, false, this.f6588a.j() & Integer.MAX_VALUE, -1, this.f6590c.a(i2 - 4), HeadersMode.SPDY_HEADERS);
        }

        private void e(a.C0094a aVar, int i, int i2) {
            if (i2 != 8) {
                throw a("TYPE_WINDOW_UPDATE length: %d != 8", Integer.valueOf(i2));
            }
            int j = this.f6588a.j() & Integer.MAX_VALUE;
            long j2 = (long) (this.f6588a.j() & Integer.MAX_VALUE);
            if (j2 == 0) {
                throw a("windowSizeIncrement was 0", Long.valueOf(j2));
            } else {
                aVar.a(j, j2);
            }
        }

        private void f(a.C0094a aVar, int i, int i2) {
            boolean z;
            boolean z2 = true;
            if (i2 != 4) {
                throw a("TYPE_PING length: %d != 4", Integer.valueOf(i2));
            }
            int j = this.f6588a.j();
            boolean z3 = this.f6589b;
            if ((j & 1) == 1) {
                z = true;
            } else {
                z = false;
            }
            if (z3 != z) {
                z2 = false;
            }
            aVar.a(z2, j, 0);
        }

        private void g(a.C0094a aVar, int i, int i2) {
            if (i2 != 8) {
                throw a("TYPE_GOAWAY length: %d != 8", Integer.valueOf(i2));
            }
            int j = this.f6588a.j() & Integer.MAX_VALUE;
            int j2 = this.f6588a.j();
            ErrorCode c2 = ErrorCode.c(j2);
            if (c2 == null) {
                throw a("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(j2));
            } else {
                aVar.a(j, c2, ByteString.f8183b);
            }
        }

        private void h(a.C0094a aVar, int i, int i2) {
            boolean z = true;
            int j = this.f6588a.j();
            if (i2 != (j * 8) + 4) {
                throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", Integer.valueOf(i2), Integer.valueOf(j));
            }
            k kVar = new k();
            for (int i3 = 0; i3 < j; i3++) {
                int j2 = this.f6588a.j();
                kVar.a(j2 & 16777215, (-16777216 & j2) >>> 24, this.f6588a.j());
            }
            if ((i & 1) == 0) {
                z = false;
            }
            aVar.a(z, kVar);
        }

        private static IOException a(String str, Object... objArr) {
            throw new IOException(String.format(str, objArr));
        }

        public void close() {
            this.f6590c.a();
        }
    }

    /* compiled from: Spdy3 */
    static final class b implements b {

        /* renamed from: a  reason: collision with root package name */
        private final d f6591a;

        /* renamed from: b  reason: collision with root package name */
        private final c f6592b = new c();

        /* renamed from: c  reason: collision with root package name */
        private final d f6593c;

        /* renamed from: d  reason: collision with root package name */
        private final boolean f6594d;

        /* renamed from: e  reason: collision with root package name */
        private boolean f6595e;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: okio.f.<init>(okio.p, java.util.zip.Deflater):void
         arg types: [okio.c, java.util.zip.Deflater]
         candidates:
          okio.f.<init>(okio.d, java.util.zip.Deflater):void
          okio.f.<init>(okio.p, java.util.zip.Deflater):void */
        b(d dVar, boolean z) {
            this.f6591a = dVar;
            this.f6594d = z;
            Deflater deflater = new Deflater();
            deflater.setDictionary(l.f6587a);
            this.f6593c = k.a(new f((p) this.f6592b, deflater));
        }

        public void a(k kVar) {
        }

        public void a(int i, int i2, List<c> list) {
        }

        public synchronized void a() {
        }

        public synchronized void b() {
            if (this.f6595e) {
                throw new IOException("closed");
            }
            this.f6591a.flush();
        }

        public synchronized void a(boolean z, boolean z2, int i, int i2, List<c> list) {
            int i3 = 0;
            synchronized (this) {
                if (this.f6595e) {
                    throw new IOException("closed");
                }
                a(list);
                int b2 = (int) (10 + this.f6592b.b());
                int i4 = z ? 1 : 0;
                if (z2) {
                    i3 = 2;
                }
                this.f6591a.g(-2147287039);
                this.f6591a.g((((i3 | i4) & VUserInfo.FLAG_MASK_USER_TYPE) << 24) | (b2 & 16777215));
                this.f6591a.g(i & Integer.MAX_VALUE);
                this.f6591a.g(i2 & Integer.MAX_VALUE);
                this.f6591a.h(0);
                this.f6591a.a(this.f6592b);
                this.f6591a.flush();
            }
        }

        public synchronized void a(int i, ErrorCode errorCode) {
            if (this.f6595e) {
                throw new IOException("closed");
            } else if (errorCode.t == -1) {
                throw new IllegalArgumentException();
            } else {
                this.f6591a.g(-2147287037);
                this.f6591a.g(8);
                this.f6591a.g(Integer.MAX_VALUE & i);
                this.f6591a.g(errorCode.t);
                this.f6591a.flush();
            }
        }

        public int c() {
            return 16383;
        }

        public synchronized void a(boolean z, int i, c cVar, int i2) {
            a(i, z ? 1 : 0, cVar, i2);
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, c cVar, int i3) {
            if (this.f6595e) {
                throw new IOException("closed");
            } else if (((long) i3) > 16777215) {
                throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: " + i3);
            } else {
                this.f6591a.g(Integer.MAX_VALUE & i);
                this.f6591a.g(((i2 & VUserInfo.FLAG_MASK_USER_TYPE) << 24) | (16777215 & i3));
                if (i3 > 0) {
                    this.f6591a.a_(cVar, (long) i3);
                }
            }
        }

        private void a(List<c> list) {
            if (this.f6592b.b() != 0) {
                throw new IllegalStateException();
            }
            this.f6593c.g(list.size());
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ByteString byteString = list.get(i).f6533h;
                this.f6593c.g(byteString.g());
                this.f6593c.b(byteString);
                ByteString byteString2 = list.get(i).i;
                this.f6593c.g(byteString2.g());
                this.f6593c.b(byteString2);
            }
            this.f6593c.flush();
        }

        public synchronized void b(k kVar) {
            if (this.f6595e) {
                throw new IOException("closed");
            }
            int b2 = kVar.b();
            this.f6591a.g(-2147287036);
            this.f6591a.g((((b2 * 8) + 4) & 16777215) | 0);
            this.f6591a.g(b2);
            for (int i = 0; i <= 10; i++) {
                if (kVar.a(i)) {
                    this.f6591a.g(((kVar.c(i) & VUserInfo.FLAG_MASK_USER_TYPE) << 24) | (i & 16777215));
                    this.f6591a.g(kVar.b(i));
                }
            }
            this.f6591a.flush();
        }

        public synchronized void a(boolean z, int i, int i2) {
            boolean z2;
            boolean z3 = true;
            synchronized (this) {
                if (this.f6595e) {
                    throw new IOException("closed");
                }
                boolean z4 = this.f6594d;
                if ((i & 1) == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (z4 == z2) {
                    z3 = false;
                }
                if (z != z3) {
                    throw new IllegalArgumentException("payload != reply");
                }
                this.f6591a.g(-2147287034);
                this.f6591a.g(4);
                this.f6591a.g(i);
                this.f6591a.flush();
            }
        }

        public synchronized void a(int i, ErrorCode errorCode, byte[] bArr) {
            if (this.f6595e) {
                throw new IOException("closed");
            } else if (errorCode.u == -1) {
                throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
            } else {
                this.f6591a.g(-2147287033);
                this.f6591a.g(8);
                this.f6591a.g(i);
                this.f6591a.g(errorCode.u);
                this.f6591a.flush();
            }
        }

        public synchronized void a(int i, long j) {
            if (this.f6595e) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: " + j);
            } else {
                this.f6591a.g(-2147287031);
                this.f6591a.g(8);
                this.f6591a.g(i);
                this.f6591a.g((int) j);
                this.f6591a.flush();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.squareup.okhttp.internal.h.a(java.io.Closeable, java.io.Closeable):void
         arg types: [okio.d, okio.d]
         candidates:
          com.squareup.okhttp.internal.h.a(java.lang.String, int):int
          com.squareup.okhttp.internal.h.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
          com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
          com.squareup.okhttp.internal.h.a(java.lang.Object, java.lang.Object):boolean
          com.squareup.okhttp.internal.h.a(java.io.Closeable, java.io.Closeable):void */
        public synchronized void close() {
            this.f6595e = true;
            h.a((Closeable) this.f6591a, (Closeable) this.f6593c);
        }
    }
}
