package com.squareup.okhttp.internal.spdy;

import java.util.Arrays;

/* compiled from: Settings */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    private int f6583a;

    /* renamed from: b  reason: collision with root package name */
    private int f6584b;

    /* renamed from: c  reason: collision with root package name */
    private int f6585c;

    /* renamed from: d  reason: collision with root package name */
    private final int[] f6586d = new int[10];

    /* access modifiers changed from: package-private */
    public void a() {
        this.f6585c = 0;
        this.f6584b = 0;
        this.f6583a = 0;
        Arrays.fill(this.f6586d, 0);
    }

    /* access modifiers changed from: package-private */
    public k a(int i, int i2, int i3) {
        if (i < this.f6586d.length) {
            int i4 = 1 << i;
            this.f6583a |= i4;
            if ((i2 & 1) != 0) {
                this.f6584b |= i4;
            } else {
                this.f6584b &= i4 ^ -1;
            }
            if ((i2 & 2) != 0) {
                this.f6585c = i4 | this.f6585c;
            } else {
                this.f6585c = (i4 ^ -1) & this.f6585c;
            }
            this.f6586d[i] = i3;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i) {
        if (((1 << i) & this.f6583a) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int b(int i) {
        return this.f6586d[i];
    }

    /* access modifiers changed from: package-private */
    public int c(int i) {
        int i2 = 0;
        if (g(i)) {
            i2 = 2;
        }
        if (f(i)) {
            return i2 | 1;
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return Integer.bitCount(this.f6583a);
    }

    /* access modifiers changed from: package-private */
    public int c() {
        if ((2 & this.f6583a) != 0) {
            return this.f6586d[1];
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        return (32 & this.f6583a) != 0 ? this.f6586d[5] : i;
    }

    /* access modifiers changed from: package-private */
    public int e(int i) {
        return (128 & this.f6583a) != 0 ? this.f6586d[7] : i;
    }

    /* access modifiers changed from: package-private */
    public boolean f(int i) {
        if (((1 << i) & this.f6584b) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean g(int i) {
        if (((1 << i) & this.f6585c) != 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(k kVar) {
        for (int i = 0; i < 10; i++) {
            if (kVar.a(i)) {
                a(i, kVar.c(i), kVar.b(i));
            }
        }
    }
}
