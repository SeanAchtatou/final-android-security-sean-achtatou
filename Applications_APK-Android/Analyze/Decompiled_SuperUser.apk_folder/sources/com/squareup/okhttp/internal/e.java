package com.squareup.okhttp.internal;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

/* compiled from: OptionalMethod */
class e<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Class<?> f6489a;

    /* renamed from: b  reason: collision with root package name */
    private final String f6490b;

    /* renamed from: c  reason: collision with root package name */
    private final Class[] f6491c;

    public e(Class<?> cls, String str, Class... clsArr) {
        this.f6489a = cls;
        this.f6490b = str;
        this.f6491c = clsArr;
    }

    public boolean a(Socket socket) {
        return a(socket.getClass()) != null;
    }

    public Object a(T t, Object... objArr) {
        Method a2 = a(t.getClass());
        if (a2 == null) {
            return null;
        }
        try {
            return a2.invoke(t, objArr);
        } catch (IllegalAccessException e2) {
            return null;
        }
    }

    public Object b(T t, Object... objArr) {
        try {
            return a(t, objArr);
        } catch (InvocationTargetException e2) {
            Throwable targetException = e2.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    public Object c(T t, Object... objArr) {
        Method a2 = a(t.getClass());
        if (a2 == null) {
            throw new AssertionError("Method " + this.f6490b + " not supported for object " + ((Object) t));
        }
        try {
            return a2.invoke(t, objArr);
        } catch (IllegalAccessException e2) {
            AssertionError assertionError = new AssertionError("Unexpectedly could not call: " + a2);
            assertionError.initCause(e2);
            throw assertionError;
        }
    }

    public Object d(T t, Object... objArr) {
        try {
            return c(t, objArr);
        } catch (InvocationTargetException e2) {
            Throwable targetException = e2.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            }
            AssertionError assertionError = new AssertionError("Unexpected exception");
            assertionError.initCause(targetException);
            throw assertionError;
        }
    }

    private Method a(Class<?> cls) {
        if (this.f6490b == null) {
            return null;
        }
        Method a2 = a(cls, this.f6490b, this.f6491c);
        if (a2 == null || this.f6489a == null || this.f6489a.isAssignableFrom(a2.getReturnType())) {
            return a2;
        }
        return null;
    }

    private static Method a(Class<?> cls, String str, Class[] clsArr) {
        try {
            Method method = cls.getMethod(str, clsArr);
            try {
                if ((method.getModifiers() & 1) == 0) {
                    return null;
                }
                return method;
            } catch (NoSuchMethodException e2) {
                return method;
            }
        } catch (NoSuchMethodException e3) {
            return null;
        }
    }
}
