package com.squareup.okhttp.internal.spdy;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kingouser.com.entity.UidPolicy;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okio.ByteString;
import okio.c;
import okio.e;
import okio.k;
import okio.q;

/* compiled from: Hpack */
final class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final c[] f6534a = {new c(c.f6530e, ""), new c(c.f6527b, "GET"), new c(c.f6527b, "POST"), new c(c.f6528c, "/"), new c(c.f6528c, "/index.html"), new c(c.f6529d, "http"), new c(c.f6529d, "https"), new c(c.f6526a, "200"), new c(c.f6526a, "204"), new c(c.f6526a, "206"), new c(c.f6526a, "304"), new c(c.f6526a, "400"), new c(c.f6526a, "404"), new c(c.f6526a, "500"), new c("accept-charset", ""), new c("accept-encoding", "gzip, deflate"), new c("accept-language", ""), new c("accept-ranges", ""), new c("accept", ""), new c("access-control-allow-origin", ""), new c("age", ""), new c((String) UidPolicy.ALLOW, ""), new c("authorization", ""), new c("cache-control", ""), new c("content-disposition", ""), new c("content-encoding", ""), new c("content-language", ""), new c("content-length", ""), new c("content-location", ""), new c("content-range", ""), new c("content-type", ""), new c("cookie", ""), new c("date", ""), new c("etag", ""), new c("expect", ""), new c("expires", ""), new c("from", ""), new c("host", ""), new c("if-match", ""), new c("if-modified-since", ""), new c("if-none-match", ""), new c("if-range", ""), new c("if-unmodified-since", ""), new c("last-modified", ""), new c("link", ""), new c((String) FirebaseAnalytics.Param.LOCATION, ""), new c("max-forwards", ""), new c("proxy-authenticate", ""), new c("proxy-authorization", ""), new c("range", ""), new c("referer", ""), new c("refresh", ""), new c("retry-after", ""), new c("server", ""), new c("set-cookie", ""), new c("strict-transport-security", ""), new c("transfer-encoding", ""), new c("user-agent", ""), new c("vary", ""), new c("via", ""), new c("www-authenticate", "")};
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Map<ByteString, Integer> f6535b = c();

    /* compiled from: Hpack */
    static final class a {

        /* renamed from: a  reason: collision with root package name */
        c[] f6536a = new c[8];

        /* renamed from: b  reason: collision with root package name */
        int f6537b = (this.f6536a.length - 1);

        /* renamed from: c  reason: collision with root package name */
        int f6538c = 0;

        /* renamed from: d  reason: collision with root package name */
        int f6539d = 0;

        /* renamed from: e  reason: collision with root package name */
        private final List<c> f6540e = new ArrayList();

        /* renamed from: f  reason: collision with root package name */
        private final e f6541f;

        /* renamed from: g  reason: collision with root package name */
        private int f6542g;

        /* renamed from: h  reason: collision with root package name */
        private int f6543h;

        a(int i, q qVar) {
            this.f6542g = i;
            this.f6543h = i;
            this.f6541f = k.a(qVar);
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.f6542g = i;
            this.f6543h = i;
            d();
        }

        private void d() {
            if (this.f6543h >= this.f6539d) {
                return;
            }
            if (this.f6543h == 0) {
                e();
            } else {
                b(this.f6539d - this.f6543h);
            }
        }

        private void e() {
            this.f6540e.clear();
            Arrays.fill(this.f6536a, (Object) null);
            this.f6537b = this.f6536a.length - 1;
            this.f6538c = 0;
            this.f6539d = 0;
        }

        private int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f6536a.length;
                while (true) {
                    length--;
                    if (length < this.f6537b || i <= 0) {
                        System.arraycopy(this.f6536a, this.f6537b + 1, this.f6536a, this.f6537b + 1 + i2, this.f6538c);
                        this.f6537b += i2;
                    } else {
                        i -= this.f6536a[length].j;
                        this.f6539d -= this.f6536a[length].j;
                        this.f6538c--;
                        i2++;
                    }
                }
                System.arraycopy(this.f6536a, this.f6537b + 1, this.f6536a, this.f6537b + 1 + i2, this.f6538c);
                this.f6537b += i2;
            }
            return i2;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            while (!this.f6541f.f()) {
                byte h2 = this.f6541f.h() & 255;
                if (h2 == 128) {
                    throw new IOException("index == 0");
                } else if ((h2 & 128) == 128) {
                    c(a(h2, 127) - 1);
                } else if (h2 == 64) {
                    g();
                } else if ((h2 & 64) == 64) {
                    f(a(h2, 63) - 1);
                } else if ((h2 & 32) == 32) {
                    this.f6543h = a(h2, 31);
                    if (this.f6543h < 0 || this.f6543h > this.f6542g) {
                        throw new IOException("Invalid dynamic table size update " + this.f6543h);
                    }
                    d();
                } else if (h2 == 16 || h2 == 0) {
                    f();
                } else {
                    e(a(h2, 15) - 1);
                }
            }
        }

        public List<c> b() {
            ArrayList arrayList = new ArrayList(this.f6540e);
            this.f6540e.clear();
            return arrayList;
        }

        private void c(int i) {
            if (h(i)) {
                this.f6540e.add(d.f6534a[i]);
                return;
            }
            int d2 = d(i - d.f6534a.length);
            if (d2 < 0 || d2 > this.f6536a.length - 1) {
                throw new IOException("Header index too large " + (i + 1));
            }
            this.f6540e.add(this.f6536a[d2]);
        }

        private int d(int i) {
            return this.f6537b + 1 + i;
        }

        private void e(int i) {
            this.f6540e.add(new c(g(i), c()));
        }

        private void f() {
            this.f6540e.add(new c(d.b(c()), c()));
        }

        private void f(int i) {
            a(-1, new c(g(i), c()));
        }

        private void g() {
            a(-1, new c(d.b(c()), c()));
        }

        private ByteString g(int i) {
            if (h(i)) {
                return d.f6534a[i].f6533h;
            }
            return this.f6536a[d(i - d.f6534a.length)].f6533h;
        }

        private boolean h(int i) {
            return i >= 0 && i <= d.f6534a.length + -1;
        }

        private void a(int i, c cVar) {
            this.f6540e.add(cVar);
            int i2 = cVar.j;
            if (i != -1) {
                i2 -= this.f6536a[d(i)].j;
            }
            if (i2 > this.f6543h) {
                e();
                return;
            }
            int b2 = b((this.f6539d + i2) - this.f6543h);
            if (i == -1) {
                if (this.f6538c + 1 > this.f6536a.length) {
                    c[] cVarArr = new c[(this.f6536a.length * 2)];
                    System.arraycopy(this.f6536a, 0, cVarArr, this.f6536a.length, this.f6536a.length);
                    this.f6537b = this.f6536a.length - 1;
                    this.f6536a = cVarArr;
                }
                int i3 = this.f6537b;
                this.f6537b = i3 - 1;
                this.f6536a[i3] = cVar;
                this.f6538c++;
            } else {
                this.f6536a[b2 + d(i) + i] = cVar;
            }
            this.f6539d = i2 + this.f6539d;
        }

        private int h() {
            return this.f6541f.h() & 255;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2) {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int h2 = h();
                if ((h2 & FileUtils.FileMode.MODE_IWUSR) == 0) {
                    return (h2 << i4) + i2;
                }
                i2 += (h2 & 127) << i4;
                i4 += 7;
            }
        }

        /* access modifiers changed from: package-private */
        public ByteString c() {
            int h2 = h();
            boolean z = (h2 & FileUtils.FileMode.MODE_IWUSR) == 128;
            int a2 = a(h2, 127);
            if (z) {
                return ByteString.a(f.a().a(this.f6541f.f((long) a2)));
            }
            return this.f6541f.c((long) a2);
        }
    }

    private static Map<ByteString, Integer> c() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f6534a.length);
        for (int i = 0; i < f6534a.length; i++) {
            if (!linkedHashMap.containsKey(f6534a[i].f6533h)) {
                linkedHashMap.put(f6534a[i].f6533h, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }

    /* compiled from: Hpack */
    static final class b {

        /* renamed from: a  reason: collision with root package name */
        private final c f6544a;

        b(c cVar) {
            this.f6544a = cVar;
        }

        /* access modifiers changed from: package-private */
        public void a(List<c> list) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ByteString f2 = list.get(i).f6533h.f();
                Integer num = (Integer) d.f6535b.get(f2);
                if (num != null) {
                    a(num.intValue() + 1, 15, 0);
                    a(list.get(i).i);
                } else {
                    this.f6544a.i(0);
                    a(f2);
                    a(list.get(i).i);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, int i3) {
            if (i < i2) {
                this.f6544a.i(i3 | i);
                return;
            }
            this.f6544a.i(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.f6544a.i((i4 & 127) | FileUtils.FileMode.MODE_IWUSR);
                i4 >>>= 7;
            }
            this.f6544a.i(i4);
        }

        /* access modifiers changed from: package-private */
        public void a(ByteString byteString) {
            a(byteString.g(), 127, 0);
            this.f6544a.b(byteString);
        }
    }

    /* access modifiers changed from: private */
    public static ByteString b(ByteString byteString) {
        int i = 0;
        int g2 = byteString.g();
        while (i < g2) {
            byte a2 = byteString.a(i);
            if (a2 < 65 || a2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + byteString.a());
            }
        }
        return byteString;
    }
}
