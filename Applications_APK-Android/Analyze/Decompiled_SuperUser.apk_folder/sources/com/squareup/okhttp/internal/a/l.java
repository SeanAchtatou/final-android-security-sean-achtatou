package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.s;
import java.net.Proxy;
import java.net.URL;

/* compiled from: RequestLine */
public final class l {
    static String a(s sVar, Proxy.Type type, Protocol protocol) {
        StringBuilder sb = new StringBuilder();
        sb.append(sVar.d());
        sb.append(' ');
        if (a(sVar, type)) {
            sb.append(sVar.a());
        } else {
            sb.append(a(sVar.a()));
        }
        sb.append(' ');
        sb.append(a(protocol));
        return sb.toString();
    }

    private static boolean a(s sVar, Proxy.Type type) {
        return !sVar.j() && type == Proxy.Type.HTTP;
    }

    public static String a(URL url) {
        String file = url.getFile();
        if (file == null) {
            return "/";
        }
        if (!file.startsWith("/")) {
            return "/" + file;
        }
        return file;
    }

    public static String a(Protocol protocol) {
        return protocol == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1";
    }
}
