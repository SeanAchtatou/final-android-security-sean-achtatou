package com.squareup.okhttp.internal;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.i;
import com.squareup.okhttp.internal.a.g;
import com.squareup.okhttp.internal.a.q;
import com.squareup.okhttp.j;
import com.squareup.okhttp.o;
import com.squareup.okhttp.r;
import com.squareup.okhttp.s;
import java.util.logging.Logger;

/* compiled from: Internal */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f6394a = Logger.getLogger(r.class.getName());

    /* renamed from: b  reason: collision with root package name */
    public static a f6395b;

    public abstract q a(i iVar, g gVar);

    public abstract b a(r rVar);

    public abstract void a(i iVar, Protocol protocol);

    public abstract void a(i iVar, Object obj);

    public abstract void a(j jVar, i iVar);

    public abstract void a(o.a aVar, String str);

    public abstract void a(r rVar, i iVar, g gVar, s sVar);

    public abstract boolean a(i iVar);

    public abstract int b(i iVar);

    public abstract g b(r rVar);

    public abstract void b(i iVar, g gVar);

    public abstract d c(r rVar);

    public abstract boolean c(i iVar);
}
