package com.squareup.okhttp.internal.spdy;

import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import com.squareup.okhttp.internal.spdy.a;
import com.squareup.okhttp.internal.spdy.d;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.ByteString;
import okio.q;
import okio.r;

/* compiled from: Http2 */
public final class e implements o {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f6545a = Logger.getLogger(b.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final ByteString f6546b = ByteString.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");

    public a a(okio.e eVar, boolean z) {
        return new c(eVar, CodedOutputStream.DEFAULT_BUFFER_SIZE, z);
    }

    public b a(okio.d dVar, boolean z) {
        return new d(dVar, z);
    }

    /* compiled from: Http2 */
    static final class c implements a {

        /* renamed from: a  reason: collision with root package name */
        final d.a f6556a;

        /* renamed from: b  reason: collision with root package name */
        private final okio.e f6557b;

        /* renamed from: c  reason: collision with root package name */
        private final a f6558c = new a(this.f6557b);

        /* renamed from: d  reason: collision with root package name */
        private final boolean f6559d;

        c(okio.e eVar, int i, boolean z) {
            this.f6557b = eVar;
            this.f6559d = z;
            this.f6556a = new d.a(i, this.f6558c);
        }

        public void a() {
            if (!this.f6559d) {
                ByteString c2 = this.f6557b.c((long) e.f6546b.g());
                if (e.f6545a.isLoggable(Level.FINE)) {
                    e.f6545a.fine(String.format("<< CONNECTION %s", c2.e()));
                }
                if (!e.f6546b.equals(c2)) {
                    throw e.d("Expected a connection header but was %s", c2.a());
                }
            }
        }

        public boolean a(a.C0094a aVar) {
            try {
                this.f6557b.a(9);
                int a2 = e.b(this.f6557b);
                if (a2 < 0 || a2 > 16384) {
                    throw e.d("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
                }
                byte h2 = (byte) (this.f6557b.h() & 255);
                byte h3 = (byte) (this.f6557b.h() & 255);
                int j = this.f6557b.j() & Integer.MAX_VALUE;
                if (e.f6545a.isLoggable(Level.FINE)) {
                    e.f6545a.fine(b.a(true, j, a2, h2, h3));
                }
                switch (h2) {
                    case 0:
                        b(aVar, a2, h3, j);
                        return true;
                    case 1:
                        a(aVar, a2, h3, j);
                        return true;
                    case 2:
                        c(aVar, a2, h3, j);
                        return true;
                    case 3:
                        d(aVar, a2, h3, j);
                        return true;
                    case 4:
                        e(aVar, a2, h3, j);
                        return true;
                    case 5:
                        f(aVar, a2, h3, j);
                        return true;
                    case 6:
                        g(aVar, a2, h3, j);
                        return true;
                    case 7:
                        h(aVar, a2, h3, j);
                        return true;
                    case 8:
                        i(aVar, a2, h3, j);
                        return true;
                    default:
                        this.f6557b.g((long) a2);
                        return true;
                }
            } catch (IOException e2) {
                return false;
            }
        }

        private void a(a.C0094a aVar, int i, byte b2, int i2) {
            short s;
            if (i2 == 0) {
                throw e.d("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
            }
            boolean z = (b2 & 1) != 0;
            if ((b2 & 8) != 0) {
                s = (short) (this.f6557b.h() & 255);
            } else {
                s = 0;
            }
            if ((b2 & 32) != 0) {
                a(aVar, i2);
                i -= 5;
            }
            aVar.a(false, z, i2, -1, a(e.b(i, b2, s), s, b2, i2), HeadersMode.HTTP_20_HEADERS);
        }

        private List<c> a(int i, short s, byte b2, int i2) {
            a aVar = this.f6558c;
            this.f6558c.f6550d = i;
            aVar.f6547a = i;
            this.f6558c.f6551e = s;
            this.f6558c.f6548b = b2;
            this.f6558c.f6549c = i2;
            this.f6556a.a();
            return this.f6556a.b();
        }

        private void b(a.C0094a aVar, int i, byte b2, int i2) {
            boolean z;
            boolean z2 = true;
            short s = 0;
            if ((b2 & 1) != 0) {
                z = true;
            } else {
                z = false;
            }
            if ((b2 & 32) == 0) {
                z2 = false;
            }
            if (z2) {
                throw e.d("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            }
            if ((b2 & 8) != 0) {
                s = (short) (this.f6557b.h() & 255);
            }
            aVar.a(z, i2, this.f6557b, e.b(i, b2, s));
            this.f6557b.g((long) s);
        }

        private void c(a.C0094a aVar, int i, byte b2, int i2) {
            if (i != 5) {
                throw e.d("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            } else if (i2 == 0) {
                throw e.d("TYPE_PRIORITY streamId == 0", new Object[0]);
            } else {
                a(aVar, i2);
            }
        }

        private void a(a.C0094a aVar, int i) {
            int j = this.f6557b.j();
            aVar.a(i, j & Integer.MAX_VALUE, (this.f6557b.h() & 255) + 1, (Integer.MIN_VALUE & j) != 0);
        }

        private void d(a.C0094a aVar, int i, byte b2, int i2) {
            if (i != 4) {
                throw e.d("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            } else if (i2 == 0) {
                throw e.d("TYPE_RST_STREAM streamId == 0", new Object[0]);
            } else {
                int j = this.f6557b.j();
                ErrorCode b3 = ErrorCode.b(j);
                if (b3 == null) {
                    throw e.d("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(j));
                } else {
                    aVar.a(i2, b3);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.squareup.okhttp.internal.spdy.a.a.a(boolean, com.squareup.okhttp.internal.spdy.k):void
         arg types: [int, com.squareup.okhttp.internal.spdy.k]
         candidates:
          com.squareup.okhttp.internal.spdy.a.a.a(int, long):void
          com.squareup.okhttp.internal.spdy.a.a.a(int, com.squareup.okhttp.internal.spdy.ErrorCode):void
          com.squareup.okhttp.internal.spdy.a.a.a(boolean, com.squareup.okhttp.internal.spdy.k):void */
        private void e(a.C0094a aVar, int i, byte b2, int i2) {
            if (i2 != 0) {
                throw e.d("TYPE_SETTINGS streamId != 0", new Object[0]);
            } else if ((b2 & 1) != 0) {
                if (i != 0) {
                    throw e.d("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                }
                aVar.a();
            } else if (i % 6 != 0) {
                throw e.d("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            } else {
                k kVar = new k();
                for (int i3 = 0; i3 < i; i3 += 6) {
                    short i4 = this.f6557b.i();
                    int j = this.f6557b.j();
                    switch (i4) {
                        case 1:
                        case 6:
                            break;
                        case 2:
                            if (!(j == 0 || j == 1)) {
                                throw e.d("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                            }
                        case 3:
                            i4 = 4;
                            break;
                        case 4:
                            i4 = 7;
                            if (j >= 0) {
                                break;
                            } else {
                                throw e.d("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            }
                        case 5:
                            if (j >= 16384 && j <= 16777215) {
                                break;
                            } else {
                                throw e.d("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(j));
                            }
                        default:
                            throw e.d("PROTOCOL_ERROR invalid settings id: %s", Short.valueOf(i4));
                    }
                    kVar.a(i4, 0, j);
                }
                aVar.a(false, kVar);
                if (kVar.c() >= 0) {
                    this.f6556a.a(kVar.c());
                }
            }
        }

        private void f(a.C0094a aVar, int i, byte b2, int i2) {
            short s = 0;
            if (i2 == 0) {
                throw e.d("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
            }
            if ((b2 & 8) != 0) {
                s = (short) (this.f6557b.h() & 255);
            }
            aVar.a(i2, this.f6557b.j() & Integer.MAX_VALUE, a(e.b(i - 4, b2, s), s, b2, i2));
        }

        private void g(a.C0094a aVar, int i, byte b2, int i2) {
            boolean z = true;
            if (i != 8) {
                throw e.d("TYPE_PING length != 8: %s", Integer.valueOf(i));
            } else if (i2 != 0) {
                throw e.d("TYPE_PING streamId != 0", new Object[0]);
            } else {
                int j = this.f6557b.j();
                int j2 = this.f6557b.j();
                if ((b2 & 1) == 0) {
                    z = false;
                }
                aVar.a(z, j, j2);
            }
        }

        private void h(a.C0094a aVar, int i, byte b2, int i2) {
            if (i < 8) {
                throw e.d("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            } else if (i2 != 0) {
                throw e.d("TYPE_GOAWAY streamId != 0", new Object[0]);
            } else {
                int j = this.f6557b.j();
                int j2 = this.f6557b.j();
                int i3 = i - 8;
                ErrorCode b3 = ErrorCode.b(j2);
                if (b3 == null) {
                    throw e.d("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(j2));
                }
                ByteString byteString = ByteString.f8183b;
                if (i3 > 0) {
                    byteString = this.f6557b.c((long) i3);
                }
                aVar.a(j, b3, byteString);
            }
        }

        private void i(a.C0094a aVar, int i, byte b2, int i2) {
            if (i != 4) {
                throw e.d("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
            }
            long j = ((long) this.f6557b.j()) & 2147483647L;
            if (j == 0) {
                throw e.d("windowSizeIncrement was 0", Long.valueOf(j));
            } else {
                aVar.a(i2, j);
            }
        }

        public void close() {
            this.f6557b.close();
        }
    }

    /* compiled from: Http2 */
    static final class d implements b {

        /* renamed from: a  reason: collision with root package name */
        private final okio.d f6560a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f6561b;

        /* renamed from: c  reason: collision with root package name */
        private final okio.c f6562c = new okio.c();

        /* renamed from: d  reason: collision with root package name */
        private final d.b f6563d = new d.b(this.f6562c);

        /* renamed from: e  reason: collision with root package name */
        private int f6564e = 16384;

        /* renamed from: f  reason: collision with root package name */
        private boolean f6565f;

        d(okio.d dVar, boolean z) {
            this.f6560a = dVar;
            this.f6561b = z;
        }

        public synchronized void b() {
            if (this.f6565f) {
                throw new IOException("closed");
            }
            this.f6560a.flush();
        }

        public synchronized void a(k kVar) {
            if (this.f6565f) {
                throw new IOException("closed");
            }
            this.f6564e = kVar.d(this.f6564e);
            a(0, 0, (byte) 4, (byte) 1);
            this.f6560a.flush();
        }

        public synchronized void a() {
            if (this.f6565f) {
                throw new IOException("closed");
            } else if (this.f6561b) {
                if (e.f6545a.isLoggable(Level.FINE)) {
                    e.f6545a.fine(String.format(">> CONNECTION %s", e.f6546b.e()));
                }
                this.f6560a.c(e.f6546b.h());
                this.f6560a.flush();
            }
        }

        public synchronized void a(boolean z, boolean z2, int i, int i2, List<c> list) {
            if (z2) {
                throw new UnsupportedOperationException();
            } else if (this.f6565f) {
                throw new IOException("closed");
            } else {
                a(z, i, list);
            }
        }

        public synchronized void a(int i, int i2, List<c> list) {
            if (this.f6565f) {
                throw new IOException("closed");
            } else if (this.f6562c.b() != 0) {
                throw new IllegalStateException();
            } else {
                this.f6563d.a(list);
                long b2 = this.f6562c.b();
                int min = (int) Math.min((long) (this.f6564e - 4), b2);
                a(i, min + 4, (byte) 5, b2 == ((long) min) ? (byte) 4 : 0);
                this.f6560a.g(Integer.MAX_VALUE & i2);
                this.f6560a.a_(this.f6562c, (long) min);
                if (b2 > ((long) min)) {
                    b(i, b2 - ((long) min));
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(boolean z, int i, List<c> list) {
            if (this.f6565f) {
                throw new IOException("closed");
            } else if (this.f6562c.b() != 0) {
                throw new IllegalStateException();
            } else {
                this.f6563d.a(list);
                long b2 = this.f6562c.b();
                int min = (int) Math.min((long) this.f6564e, b2);
                byte b3 = b2 == ((long) min) ? (byte) 4 : 0;
                if (z) {
                    b3 = (byte) (b3 | 1);
                }
                a(i, min, (byte) 1, b3);
                this.f6560a.a_(this.f6562c, (long) min);
                if (b2 > ((long) min)) {
                    b(i, b2 - ((long) min));
                }
            }
        }

        private void b(int i, long j) {
            while (j > 0) {
                int min = (int) Math.min((long) this.f6564e, j);
                j -= (long) min;
                a(i, min, (byte) 9, j == 0 ? (byte) 4 : 0);
                this.f6560a.a_(this.f6562c, (long) min);
            }
        }

        public synchronized void a(int i, ErrorCode errorCode) {
            if (this.f6565f) {
                throw new IOException("closed");
            } else if (errorCode.t == -1) {
                throw new IllegalArgumentException();
            } else {
                a(i, 4, (byte) 3, (byte) 0);
                this.f6560a.g(errorCode.s);
                this.f6560a.flush();
            }
        }

        public int c() {
            return this.f6564e;
        }

        public synchronized void a(boolean z, int i, okio.c cVar, int i2) {
            if (this.f6565f) {
                throw new IOException("closed");
            }
            byte b2 = 0;
            if (z) {
                b2 = (byte) 1;
            }
            a(i, b2, cVar, i2);
        }

        /* access modifiers changed from: package-private */
        public void a(int i, byte b2, okio.c cVar, int i2) {
            a(i, i2, (byte) 0, b2);
            if (i2 > 0) {
                this.f6560a.a_(cVar, (long) i2);
            }
        }

        public synchronized void b(k kVar) {
            int i;
            synchronized (this) {
                if (this.f6565f) {
                    throw new IOException("closed");
                }
                a(0, kVar.b() * 6, (byte) 4, (byte) 0);
                for (int i2 = 0; i2 < 10; i2++) {
                    if (kVar.a(i2)) {
                        if (i2 == 4) {
                            i = 3;
                        } else if (i2 == 7) {
                            i = 4;
                        } else {
                            i = i2;
                        }
                        this.f6560a.h(i);
                        this.f6560a.g(kVar.b(i2));
                    }
                }
                this.f6560a.flush();
            }
        }

        public synchronized void a(boolean z, int i, int i2) {
            byte b2 = 0;
            synchronized (this) {
                if (this.f6565f) {
                    throw new IOException("closed");
                }
                if (z) {
                    b2 = 1;
                }
                a(0, 8, (byte) 6, b2);
                this.f6560a.g(i);
                this.f6560a.g(i2);
                this.f6560a.flush();
            }
        }

        public synchronized void a(int i, ErrorCode errorCode, byte[] bArr) {
            if (this.f6565f) {
                throw new IOException("closed");
            } else if (errorCode.s == -1) {
                throw e.c("errorCode.httpCode == -1", new Object[0]);
            } else {
                a(0, bArr.length + 8, (byte) 7, (byte) 0);
                this.f6560a.g(i);
                this.f6560a.g(errorCode.s);
                if (bArr.length > 0) {
                    this.f6560a.c(bArr);
                }
                this.f6560a.flush();
            }
        }

        public synchronized void a(int i, long j) {
            if (this.f6565f) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw e.c("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", Long.valueOf(j));
            } else {
                a(i, 4, (byte) 8, (byte) 0);
                this.f6560a.g((int) j);
                this.f6560a.flush();
            }
        }

        public synchronized void close() {
            this.f6565f = true;
            this.f6560a.close();
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, byte b2, byte b3) {
            if (e.f6545a.isLoggable(Level.FINE)) {
                e.f6545a.fine(b.a(false, i, i2, b2, b3));
            }
            if (i2 > this.f6564e) {
                throw e.c("FRAME_SIZE_ERROR length > %d: %d", Integer.valueOf(this.f6564e), Integer.valueOf(i2));
            } else if ((Integer.MIN_VALUE & i) != 0) {
                throw e.c("reserved bit set: %s", Integer.valueOf(i));
            } else {
                e.b(this.f6560a, i2);
                this.f6560a.i(b2 & 255);
                this.f6560a.i(b3 & 255);
                this.f6560a.g(Integer.MAX_VALUE & i);
            }
        }
    }

    /* access modifiers changed from: private */
    public static IllegalArgumentException c(String str, Object... objArr) {
        throw new IllegalArgumentException(String.format(str, objArr));
    }

    /* access modifiers changed from: private */
    public static IOException d(String str, Object... objArr) {
        throw new IOException(String.format(str, objArr));
    }

    /* compiled from: Http2 */
    static final class a implements q {

        /* renamed from: a  reason: collision with root package name */
        int f6547a;

        /* renamed from: b  reason: collision with root package name */
        byte f6548b;

        /* renamed from: c  reason: collision with root package name */
        int f6549c;

        /* renamed from: d  reason: collision with root package name */
        int f6550d;

        /* renamed from: e  reason: collision with root package name */
        short f6551e;

        /* renamed from: f  reason: collision with root package name */
        private final okio.e f6552f;

        public a(okio.e eVar) {
            this.f6552f = eVar;
        }

        public long a(okio.c cVar, long j) {
            while (this.f6550d == 0) {
                this.f6552f.g((long) this.f6551e);
                this.f6551e = 0;
                if ((this.f6548b & 4) != 0) {
                    return -1;
                }
                b();
            }
            long a2 = this.f6552f.a(cVar, Math.min(j, (long) this.f6550d));
            if (a2 == -1) {
                return -1;
            }
            this.f6550d = (int) (((long) this.f6550d) - a2);
            return a2;
        }

        public r a() {
            return this.f6552f.a();
        }

        public void close() {
        }

        private void b() {
            int i = this.f6549c;
            int a2 = e.b(this.f6552f);
            this.f6550d = a2;
            this.f6547a = a2;
            byte h2 = (byte) (this.f6552f.h() & 255);
            this.f6548b = (byte) (this.f6552f.h() & 255);
            if (e.f6545a.isLoggable(Level.FINE)) {
                e.f6545a.fine(b.a(true, this.f6549c, this.f6547a, h2, this.f6548b));
            }
            this.f6549c = this.f6552f.j() & Integer.MAX_VALUE;
            if (h2 != 9) {
                throw e.d("%s != TYPE_CONTINUATION", Byte.valueOf(h2));
            } else if (this.f6549c != i) {
                throw e.d("TYPE_CONTINUATION streamId changed", new Object[0]);
            }
        }
    }

    /* access modifiers changed from: private */
    public static int b(int i, byte b2, short s) {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        throw d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
    }

    /* compiled from: Http2 */
    static final class b {

        /* renamed from: a  reason: collision with root package name */
        private static final String[] f6553a = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};

        /* renamed from: b  reason: collision with root package name */
        private static final String[] f6554b = new String[64];

        /* renamed from: c  reason: collision with root package name */
        private static final String[] f6555c = new String[FileUtils.FileMode.MODE_IRUSR];

        b() {
        }

        static String a(boolean z, int i, int i2, byte b2, byte b3) {
            String format = b2 < f6553a.length ? f6553a[b2] : String.format("0x%02x", Byte.valueOf(b2));
            String a2 = a(b2, b3);
            Object[] objArr = new Object[5];
            objArr[0] = z ? "<<" : ">>";
            objArr[1] = Integer.valueOf(i);
            objArr[2] = Integer.valueOf(i2);
            objArr[3] = format;
            objArr[4] = a2;
            return String.format("%s 0x%08x %5d %-13s %s", objArr);
        }

        static String a(byte b2, byte b3) {
            if (b3 == 0) {
                return "";
            }
            switch (b2) {
                case 2:
                case 3:
                case 7:
                case 8:
                    return f6555c[b3];
                case 4:
                case 6:
                    return b3 == 1 ? "ACK" : f6555c[b3];
                case 5:
                default:
                    String str = b3 < f6554b.length ? f6554b[b3] : f6555c[b3];
                    if (b2 == 5 && (b3 & 4) != 0) {
                        return str.replace("HEADERS", "PUSH_PROMISE");
                    }
                    if (b2 != 0 || (b3 & 32) == 0) {
                        return str;
                    }
                    return str.replace("PRIORITY", "COMPRESSED");
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
         arg types: [int, int]
         candidates:
          ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
          ClspMth{java.lang.String.replace(char, char):java.lang.String} */
        static {
            for (int i = 0; i < f6555c.length; i++) {
                f6555c[i] = String.format("%8s", Integer.toBinaryString(i)).replace(' ', '0');
            }
            f6554b[0] = "";
            f6554b[1] = "END_STREAM";
            int[] iArr = {1};
            f6554b[8] = "PADDED";
            for (int i2 : iArr) {
                f6554b[i2 | 8] = f6554b[i2] + "|PADDED";
            }
            f6554b[4] = "END_HEADERS";
            f6554b[32] = "PRIORITY";
            f6554b[36] = "END_HEADERS|PRIORITY";
            for (int i3 : new int[]{4, 32, 36}) {
                for (int i4 : iArr) {
                    f6554b[i4 | i3] = f6554b[i4] + '|' + f6554b[i3];
                    f6554b[i4 | i3 | 8] = f6554b[i4] + '|' + f6554b[i3] + "|PADDED";
                }
            }
            for (int i5 = 0; i5 < f6554b.length; i5++) {
                if (f6554b[i5] == null) {
                    f6554b[i5] = f6555c[i5];
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static int b(okio.e eVar) {
        return ((eVar.h() & 255) << 16) | ((eVar.h() & 255) << 8) | (eVar.h() & 255);
    }

    /* access modifiers changed from: private */
    public static void b(okio.d dVar, int i) {
        dVar.i((i >>> 16) & VUserInfo.FLAG_MASK_USER_TYPE);
        dVar.i((i >>> 8) & VUserInfo.FLAG_MASK_USER_TYPE);
        dVar.i(i & VUserInfo.FLAG_MASK_USER_TYPE);
    }
}
