package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.d;
import com.squareup.okhttp.o;
import com.squareup.okhttp.s;
import com.squareup.okhttp.u;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/* compiled from: CacheStrategy */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final s f6397a;

    /* renamed from: b  reason: collision with root package name */
    public final u f6398b;

    private c(s sVar, u uVar) {
        this.f6397a = sVar;
        this.f6398b = uVar;
    }

    public static boolean a(u uVar, s sVar) {
        switch (uVar.c()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            default:
                return false;
            case 302:
            case 307:
                if (uVar.a("Expires") == null && uVar.j().c() == -1 && !uVar.j().e() && !uVar.j().d()) {
                    return false;
                }
        }
        return !uVar.j().b() && !sVar.i().b();
    }

    /* compiled from: CacheStrategy */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final long f6399a;

        /* renamed from: b  reason: collision with root package name */
        final s f6400b;

        /* renamed from: c  reason: collision with root package name */
        final u f6401c;

        /* renamed from: d  reason: collision with root package name */
        private Date f6402d;

        /* renamed from: e  reason: collision with root package name */
        private String f6403e;

        /* renamed from: f  reason: collision with root package name */
        private Date f6404f;

        /* renamed from: g  reason: collision with root package name */
        private String f6405g;

        /* renamed from: h  reason: collision with root package name */
        private Date f6406h;
        private long i;
        private long j;
        private String k;
        private int l = -1;

        public a(long j2, s sVar, u uVar) {
            this.f6399a = j2;
            this.f6400b = sVar;
            this.f6401c = uVar;
            if (uVar != null) {
                o f2 = uVar.f();
                int a2 = f2.a();
                for (int i2 = 0; i2 < a2; i2++) {
                    String a3 = f2.a(i2);
                    String b2 = f2.b(i2);
                    if ("Date".equalsIgnoreCase(a3)) {
                        this.f6402d = f.a(b2);
                        this.f6403e = b2;
                    } else if ("Expires".equalsIgnoreCase(a3)) {
                        this.f6406h = f.a(b2);
                    } else if ("Last-Modified".equalsIgnoreCase(a3)) {
                        this.f6404f = f.a(b2);
                        this.f6405g = b2;
                    } else if ("ETag".equalsIgnoreCase(a3)) {
                        this.k = b2;
                    } else if ("Age".equalsIgnoreCase(a3)) {
                        this.l = d.b(b2, -1);
                    } else if (j.f6453b.equalsIgnoreCase(a3)) {
                        this.i = Long.parseLong(b2);
                    } else if (j.f6454c.equalsIgnoreCase(a3)) {
                        this.j = Long.parseLong(b2);
                    }
                }
            }
        }

        public c a() {
            c b2 = b();
            if (b2.f6397a == null || !this.f6400b.i().i()) {
                return b2;
            }
            return new c(null, null);
        }

        private c b() {
            long j2;
            long j3 = 0;
            if (this.f6401c == null) {
                return new c(this.f6400b, null);
            }
            if (this.f6400b.j() && this.f6401c.e() == null) {
                return new c(this.f6400b, null);
            }
            if (!c.a(this.f6401c, this.f6400b)) {
                return new c(this.f6400b, null);
            }
            d i2 = this.f6400b.i();
            if (i2.a() || a(this.f6400b)) {
                return new c(this.f6400b, null);
            }
            long d2 = d();
            long c2 = c();
            if (i2.c() != -1) {
                c2 = Math.min(c2, TimeUnit.SECONDS.toMillis((long) i2.c()));
            }
            if (i2.h() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) i2.h());
            } else {
                j2 = 0;
            }
            d j4 = this.f6401c.j();
            if (!j4.f() && i2.g() != -1) {
                j3 = TimeUnit.SECONDS.toMillis((long) i2.g());
            }
            if (j4.a() || d2 + j2 >= j3 + c2) {
                s.a h2 = this.f6400b.h();
                if (this.k != null) {
                    h2.a("If-None-Match", this.k);
                } else if (this.f6404f != null) {
                    h2.a("If-Modified-Since", this.f6405g);
                } else if (this.f6402d != null) {
                    h2.a("If-Modified-Since", this.f6403e);
                }
                s b2 = h2.b();
                if (a(b2)) {
                    return new c(b2, this.f6401c);
                }
                return new c(b2, null);
            }
            u.a h3 = this.f6401c.h();
            if (j2 + d2 >= c2) {
                h3.b("Warning", "110 HttpURLConnection \"Response is stale\"");
            }
            if (d2 > 86400000 && e()) {
                h3.b("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
            }
            return new c(null, h3.a());
        }

        private long c() {
            d j2 = this.f6401c.j();
            if (j2.c() != -1) {
                return TimeUnit.SECONDS.toMillis((long) j2.c());
            }
            if (this.f6406h != null) {
                long time = this.f6406h.getTime() - (this.f6402d != null ? this.f6402d.getTime() : this.j);
                if (time <= 0) {
                    time = 0;
                }
                return time;
            } else if (this.f6404f == null || this.f6401c.a().a().getQuery() != null) {
                return 0;
            } else {
                long time2 = (this.f6402d != null ? this.f6402d.getTime() : this.i) - this.f6404f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        private long d() {
            long j2 = 0;
            if (this.f6402d != null) {
                j2 = Math.max(0L, this.j - this.f6402d.getTime());
            }
            if (this.l != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) this.l));
            }
            return j2 + (this.j - this.i) + (this.f6399a - this.j);
        }

        private boolean e() {
            return this.f6401c.j().c() == -1 && this.f6406h == null;
        }

        private static boolean a(s sVar) {
            return (sVar.a("If-Modified-Since") == null && sVar.a("If-None-Match") == null) ? false : true;
        }
    }
}
