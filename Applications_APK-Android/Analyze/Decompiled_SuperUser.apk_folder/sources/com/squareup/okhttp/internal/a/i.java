package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.s;
import com.squareup.okhttp.u;
import com.squareup.okhttp.v;
import okio.k;
import okio.p;
import okio.q;

/* compiled from: HttpTransport */
public final class i implements q {

    /* renamed from: a  reason: collision with root package name */
    private final g f6450a;

    /* renamed from: b  reason: collision with root package name */
    private final e f6451b;

    public i(g gVar, e eVar) {
        this.f6450a = gVar;
        this.f6451b = eVar;
    }

    public p a(s sVar, long j) {
        if ("chunked".equalsIgnoreCase(sVar.a("Transfer-Encoding"))) {
            return this.f6451b.h();
        }
        if (j != -1) {
            return this.f6451b.a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    public void a() {
        this.f6451b.d();
    }

    public void a(m mVar) {
        this.f6451b.a(mVar);
    }

    public void a(s sVar) {
        this.f6450a.b();
        this.f6451b.a(sVar.e(), l.a(sVar, this.f6450a.f().c().b().type(), this.f6450a.f().l()));
    }

    public u.a b() {
        return this.f6451b.g();
    }

    public void c() {
        if (d()) {
            this.f6451b.a();
        } else {
            this.f6451b.b();
        }
    }

    public boolean d() {
        if (!"close".equalsIgnoreCase(this.f6450a.d().a("Connection")) && !"close".equalsIgnoreCase(this.f6450a.e().a("Connection")) && !this.f6451b.c()) {
            return true;
        }
        return false;
    }

    public v a(u uVar) {
        return new k(uVar.f(), k.a(b(uVar)));
    }

    private q b(u uVar) {
        if (!g.a(uVar)) {
            return this.f6451b.b(0);
        }
        if ("chunked".equalsIgnoreCase(uVar.a("Transfer-Encoding"))) {
            return this.f6451b.a(this.f6450a);
        }
        long a2 = j.a(uVar);
        if (a2 != -1) {
            return this.f6451b.b(a2);
        }
        return this.f6451b.i();
    }

    public void a(g gVar) {
        this.f6451b.a((Object) gVar);
    }
}
