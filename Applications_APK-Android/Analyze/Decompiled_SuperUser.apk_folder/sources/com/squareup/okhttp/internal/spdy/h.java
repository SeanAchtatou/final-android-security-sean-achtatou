package com.squareup.okhttp.internal.spdy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;
import okio.ByteString;
import okio.c;
import okio.e;
import okio.g;
import okio.j;
import okio.k;

/* compiled from: NameValueBlockReader */
class h {

    /* renamed from: a  reason: collision with root package name */
    private final j f6574a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f6575b;

    /* renamed from: c  reason: collision with root package name */
    private final e f6576c = k.a(this.f6574a);

    public h(e eVar) {
        this.f6574a = new j(new g(eVar) {
            public long a(c cVar, long j) {
                if (h.this.f6575b == 0) {
                    return -1;
                }
                long a2 = super.a(cVar, Math.min(j, (long) h.this.f6575b));
                if (a2 == -1) {
                    return -1;
                }
                int unused = h.this.f6575b = (int) (((long) h.this.f6575b) - a2);
                return a2;
            }
        }, new Inflater() {
            public int inflate(byte[] bArr, int i, int i2) {
                int inflate = super.inflate(bArr, i, i2);
                if (inflate != 0 || !needsDictionary()) {
                    return inflate;
                }
                setDictionary(l.f6587a);
                return super.inflate(bArr, i, i2);
            }
        });
    }

    public List<c> a(int i) {
        this.f6575b += i;
        int j = this.f6576c.j();
        if (j < 0) {
            throw new IOException("numberOfPairs < 0: " + j);
        } else if (j > 1024) {
            throw new IOException("numberOfPairs > 1024: " + j);
        } else {
            ArrayList arrayList = new ArrayList(j);
            for (int i2 = 0; i2 < j; i2++) {
                ByteString f2 = b().f();
                ByteString b2 = b();
                if (f2.g() == 0) {
                    throw new IOException("name.size == 0");
                }
                arrayList.add(new c(f2, b2));
            }
            c();
            return arrayList;
        }
    }

    private ByteString b() {
        return this.f6576c.c((long) this.f6576c.j());
    }

    private void c() {
        if (this.f6575b > 0) {
            this.f6574a.b();
            if (this.f6575b != 0) {
                throw new IOException("compressedLimit > 0: " + this.f6575b);
            }
        }
    }

    public void a() {
        this.f6576c.close();
    }
}
