package com.squareup.okhttp.internal;

/* compiled from: NamedRunnable */
public abstract class c implements Runnable {

    /* renamed from: b  reason: collision with root package name */
    protected final String f6487b;

    /* access modifiers changed from: protected */
    public abstract void e();

    public c(String str, Object... objArr) {
        this.f6487b = String.format(str, objArr);
    }

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f6487b);
        try {
            e();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
