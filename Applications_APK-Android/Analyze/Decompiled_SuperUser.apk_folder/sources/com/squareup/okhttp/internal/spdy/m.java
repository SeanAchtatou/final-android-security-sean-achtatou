package com.squareup.okhttp.internal.spdy;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.h;
import com.squareup.okhttp.internal.spdy.a;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okio.ByteString;
import okio.c;
import okio.e;
import okio.k;

/* compiled from: SpdyConnection */
public final class m implements Closeable {
    static final /* synthetic */ boolean k;
    /* access modifiers changed from: private */
    public static final ExecutorService l = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), h.a("OkHttp SpdyConnection", true));

    /* renamed from: a  reason: collision with root package name */
    final Protocol f6596a;

    /* renamed from: b  reason: collision with root package name */
    final boolean f6597b;

    /* renamed from: c  reason: collision with root package name */
    long f6598c;

    /* renamed from: d  reason: collision with root package name */
    long f6599d;

    /* renamed from: e  reason: collision with root package name */
    final k f6600e;

    /* renamed from: f  reason: collision with root package name */
    final k f6601f;

    /* renamed from: g  reason: collision with root package name */
    final o f6602g;

    /* renamed from: h  reason: collision with root package name */
    final Socket f6603h;
    final b i;
    final b j;
    /* access modifiers changed from: private */
    public final g m;
    /* access modifiers changed from: private */
    public final Map<Integer, n> n;
    /* access modifiers changed from: private */
    public final String o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public boolean r;
    private long s;
    private final ExecutorService t;
    private Map<Integer, i> u;
    /* access modifiers changed from: private */
    public final j v;
    private int w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public final Set<Integer> y;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      com.squareup.okhttp.internal.h.a(java.lang.String, int):int
      com.squareup.okhttp.internal.h.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.squareup.okhttp.internal.h.a(java.io.Closeable, java.io.Closeable):void
      com.squareup.okhttp.internal.h.a(java.lang.Object, java.lang.Object):boolean
      com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    static {
        boolean z;
        if (!m.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        k = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      com.squareup.okhttp.internal.h.a(java.lang.String, int):int
      com.squareup.okhttp.internal.h.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      com.squareup.okhttp.internal.h.a(java.io.Closeable, java.io.Closeable):void
      com.squareup.okhttp.internal.h.a(java.lang.Object, java.lang.Object):boolean
      com.squareup.okhttp.internal.h.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    private m(a aVar) {
        int i2 = 2;
        this.n = new HashMap();
        this.s = System.nanoTime();
        this.f6598c = 0;
        this.f6600e = new k();
        this.f6601f = new k();
        this.x = false;
        this.y = new LinkedHashSet();
        this.f6596a = aVar.f6633d;
        this.v = aVar.f6634e;
        this.f6597b = aVar.f6635f;
        this.m = aVar.f6632c;
        this.q = aVar.f6635f ? 1 : 2;
        if (aVar.f6635f && this.f6596a == Protocol.HTTP_2) {
            this.q += 2;
        }
        this.w = aVar.f6635f ? 1 : i2;
        if (aVar.f6635f) {
            this.f6600e.a(7, 0, 16777216);
        }
        this.o = aVar.f6630a;
        if (this.f6596a == Protocol.HTTP_2) {
            this.f6602g = new e();
            this.t = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), h.a(String.format("OkHttp %s Push Observer", this.o), true));
            this.f6601f.a(7, 0, 65535);
            this.f6601f.a(5, 0, 16384);
        } else if (this.f6596a == Protocol.SPDY_3) {
            this.f6602g = new l();
            this.t = null;
        } else {
            throw new AssertionError(this.f6596a);
        }
        this.f6599d = (long) this.f6601f.e(65536);
        this.f6603h = aVar.f6631b;
        this.i = this.f6602g.a(k.a(k.a(aVar.f6631b)), this.f6597b);
        this.j = new b();
        new Thread(this.j).start();
    }

    public Protocol a() {
        return this.f6596a;
    }

    /* access modifiers changed from: package-private */
    public synchronized n a(int i2) {
        return this.n.get(Integer.valueOf(i2));
    }

    /* access modifiers changed from: package-private */
    public synchronized n b(int i2) {
        n remove;
        remove = this.n.remove(Integer.valueOf(i2));
        if (remove != null && this.n.isEmpty()) {
            a(true);
        }
        return remove;
    }

    private synchronized void a(boolean z) {
        this.s = z ? System.nanoTime() : Long.MAX_VALUE;
    }

    public synchronized boolean b() {
        return this.s != Long.MAX_VALUE;
    }

    public synchronized long c() {
        return this.s;
    }

    public n a(List<c> list, boolean z, boolean z2) {
        return a(0, list, z, z2);
    }

    private n a(int i2, List<c> list, boolean z, boolean z2) {
        int i3;
        n nVar;
        boolean z3 = true;
        boolean z4 = !z;
        if (z2) {
            z3 = false;
        }
        synchronized (this.i) {
            synchronized (this) {
                if (this.r) {
                    throw new IOException("shutdown");
                }
                i3 = this.q;
                this.q += 2;
                nVar = new n(i3, this, z4, z3, list);
                if (nVar.b()) {
                    this.n.put(Integer.valueOf(i3), nVar);
                    a(false);
                }
            }
            if (i2 == 0) {
                this.i.a(z4, z3, i3, i2, list);
            } else if (this.f6597b) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.i.a(i2, i3, list);
            }
        }
        if (!z) {
            this.i.b();
        }
        return nVar;
    }

    public void a(int i2, boolean z, c cVar, long j2) {
        int min;
        boolean z2;
        if (j2 == 0) {
            this.i.a(z, i2, cVar, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.f6599d <= 0) {
                    try {
                        wait();
                    } catch (InterruptedException e2) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.f6599d), this.i.c());
                this.f6599d -= (long) min;
            }
            j2 -= (long) min;
            b bVar = this.i;
            if (!z || j2 != 0) {
                z2 = false;
            } else {
                z2 = true;
            }
            bVar.a(z2, i2, cVar, min);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.f6599d += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, ErrorCode errorCode) {
        final int i3 = i2;
        final ErrorCode errorCode2 = errorCode;
        l.submit(new com.squareup.okhttp.internal.c("OkHttp %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void e() {
                try {
                    m.this.b(i3, errorCode2);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, ErrorCode errorCode) {
        this.i.a(i2, errorCode);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, long j2) {
        final int i3 = i2;
        final long j3 = j2;
        l.execute(new com.squareup.okhttp.internal.c("OkHttp Window Update %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void e() {
                try {
                    m.this.i.a(i3, j3);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(boolean z, int i2, int i3, i iVar) {
        final boolean z2 = z;
        final int i4 = i2;
        final int i5 = i3;
        final i iVar2 = iVar;
        l.execute(new com.squareup.okhttp.internal.c("OkHttp %s ping %08x%08x", new Object[]{this.o, Integer.valueOf(i2), Integer.valueOf(i3)}) {
            public void e() {
                try {
                    m.this.b(z2, i4, i5, iVar2);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(boolean z, int i2, int i3, i iVar) {
        synchronized (this.i) {
            if (iVar != null) {
                iVar.a();
            }
            this.i.a(z, i2, i3);
        }
    }

    /* access modifiers changed from: private */
    public synchronized i c(int i2) {
        return this.u != null ? this.u.remove(Integer.valueOf(i2)) : null;
    }

    public void d() {
        this.i.b();
    }

    public void a(ErrorCode errorCode) {
        synchronized (this.i) {
            synchronized (this) {
                if (!this.r) {
                    this.r = true;
                    int i2 = this.p;
                    this.i.a(i2, errorCode, h.f6508a);
                }
            }
        }
    }

    public void close() {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    /* access modifiers changed from: private */
    public void a(ErrorCode errorCode, ErrorCode errorCode2) {
        IOException iOException;
        n[] nVarArr;
        i[] iVarArr;
        if (k || !Thread.holdsLock(this)) {
            try {
                a(errorCode);
                iOException = null;
            } catch (IOException e2) {
                iOException = e2;
            }
            synchronized (this) {
                if (!this.n.isEmpty()) {
                    this.n.clear();
                    a(false);
                    nVarArr = (n[]) this.n.values().toArray(new n[this.n.size()]);
                } else {
                    nVarArr = null;
                }
                if (this.u != null) {
                    this.u = null;
                    iVarArr = (i[]) this.u.values().toArray(new i[this.u.size()]);
                } else {
                    iVarArr = null;
                }
            }
            if (nVarArr != null) {
                IOException iOException2 = iOException;
                for (n a2 : nVarArr) {
                    try {
                        a2.a(errorCode2);
                    } catch (IOException e3) {
                        if (iOException2 != null) {
                            iOException2 = e3;
                        }
                    }
                }
                iOException = iOException2;
            }
            if (iVarArr != null) {
                for (i c2 : iVarArr) {
                    c2.c();
                }
            }
            try {
                this.i.close();
                e = iOException;
            } catch (IOException e4) {
                e = e4;
                if (iOException != null) {
                    e = iOException;
                }
            }
            try {
                this.f6603h.close();
            } catch (IOException e5) {
                e = e5;
            }
            if (e != null) {
                throw e;
            }
            return;
        }
        throw new AssertionError();
    }

    public void e() {
        this.i.a();
        this.i.b(this.f6600e);
        int e2 = this.f6600e.e(65536);
        if (e2 != 65536) {
            this.i.a(0, (long) (e2 - 65536));
        }
    }

    /* compiled from: SpdyConnection */
    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f6630a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public Socket f6631b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public g f6632c = g.f6573a;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public Protocol f6633d = Protocol.SPDY_3;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public j f6634e = j.f6582a;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public boolean f6635f;

        public a(String str, boolean z, Socket socket) {
            this.f6630a = str;
            this.f6635f = z;
            this.f6631b = socket;
        }

        public a a(Protocol protocol) {
            this.f6633d = protocol;
            return this;
        }

        public m a() {
            return new m(this);
        }
    }

    /* compiled from: SpdyConnection */
    class b extends com.squareup.okhttp.internal.c implements a.C0094a {

        /* renamed from: a  reason: collision with root package name */
        a f6636a;

        private b() {
            super("OkHttp %s", m.this.o);
        }

        /* access modifiers changed from: protected */
        public void e() {
            ErrorCode errorCode;
            Throwable th;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            ErrorCode errorCode3 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f6636a = m.this.f6602g.a(k.a(k.b(m.this.f6603h)), m.this.f6597b);
                if (!m.this.f6597b) {
                    this.f6636a.a();
                }
                do {
                } while (this.f6636a.a(this));
                try {
                    m.this.a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
                } catch (IOException e2) {
                }
                h.a(this.f6636a);
            } catch (IOException e3) {
                errorCode = ErrorCode.PROTOCOL_ERROR;
                try {
                    m.this.a(errorCode, ErrorCode.PROTOCOL_ERROR);
                } catch (IOException e4) {
                }
                h.a(this.f6636a);
            } catch (Throwable th2) {
                th = th2;
                m.this.a(errorCode, errorCode3);
                h.a(this.f6636a);
                throw th;
            }
        }

        public void a(boolean z, int i, e eVar, int i2) {
            if (m.this.d(i)) {
                m.this.a(i, eVar, i2, z);
                return;
            }
            n a2 = m.this.a(i);
            if (a2 == null) {
                m.this.a(i, ErrorCode.INVALID_STREAM);
                eVar.g((long) i2);
                return;
            }
            a2.a(eVar, i2);
            if (z) {
                a2.h();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0092, code lost:
            if (r14.b() == false) goto L_0x00a0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0094, code lost:
            r0.b(com.squareup.okhttp.internal.spdy.ErrorCode.f6514b);
            r8.f6637c.b(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a0, code lost:
            r0.a(r13, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a3, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a5, code lost:
            r0.h();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(boolean r9, boolean r10, int r11, int r12, java.util.List<com.squareup.okhttp.internal.spdy.c> r13, com.squareup.okhttp.internal.spdy.HeadersMode r14) {
            /*
                r8 = this;
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this
                boolean r0 = r0.d(r11)
                if (r0 == 0) goto L_0x000e
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this
                r0.a(r11, r13, r10)
            L_0x000d:
                return
            L_0x000e:
                com.squareup.okhttp.internal.spdy.m r6 = com.squareup.okhttp.internal.spdy.m.this
                monitor-enter(r6)
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                boolean r0 = r0.r     // Catch:{ all -> 0x001b }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x001b:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                throw r0
            L_0x001e:
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                com.squareup.okhttp.internal.spdy.n r0 = r0.a(r11)     // Catch:{ all -> 0x001b }
                if (r0 != 0) goto L_0x008d
                boolean r0 = r14.a()     // Catch:{ all -> 0x001b }
                if (r0 == 0) goto L_0x0035
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                com.squareup.okhttp.internal.spdy.ErrorCode r1 = com.squareup.okhttp.internal.spdy.ErrorCode.INVALID_STREAM     // Catch:{ all -> 0x001b }
                r0.a(r11, r1)     // Catch:{ all -> 0x001b }
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x0035:
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                int r0 = r0.p     // Catch:{ all -> 0x001b }
                if (r11 > r0) goto L_0x003f
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x003f:
                int r0 = r11 % 2
                com.squareup.okhttp.internal.spdy.m r1 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                int r1 = r1.q     // Catch:{ all -> 0x001b }
                int r1 = r1 % 2
                if (r0 != r1) goto L_0x004d
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x004d:
                com.squareup.okhttp.internal.spdy.n r0 = new com.squareup.okhttp.internal.spdy.n     // Catch:{ all -> 0x001b }
                com.squareup.okhttp.internal.spdy.m r2 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                r1 = r11
                r3 = r9
                r4 = r10
                r5 = r13
                r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x001b }
                com.squareup.okhttp.internal.spdy.m r1 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                int unused = r1.p = r11     // Catch:{ all -> 0x001b }
                com.squareup.okhttp.internal.spdy.m r1 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                java.util.Map r1 = r1.n     // Catch:{ all -> 0x001b }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x001b }
                r1.put(r2, r0)     // Catch:{ all -> 0x001b }
                java.util.concurrent.ExecutorService r1 = com.squareup.okhttp.internal.spdy.m.l     // Catch:{ all -> 0x001b }
                com.squareup.okhttp.internal.spdy.m$b$1 r2 = new com.squareup.okhttp.internal.spdy.m$b$1     // Catch:{ all -> 0x001b }
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x001b }
                r5 = 0
                com.squareup.okhttp.internal.spdy.m r7 = com.squareup.okhttp.internal.spdy.m.this     // Catch:{ all -> 0x001b }
                java.lang.String r7 = r7.o     // Catch:{ all -> 0x001b }
                r4[r5] = r7     // Catch:{ all -> 0x001b }
                r5 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x001b }
                r4[r5] = r7     // Catch:{ all -> 0x001b }
                r2.<init>(r3, r4, r0)     // Catch:{ all -> 0x001b }
                r1.execute(r2)     // Catch:{ all -> 0x001b }
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x008d:
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                boolean r1 = r14.b()
                if (r1 == 0) goto L_0x00a0
                com.squareup.okhttp.internal.spdy.ErrorCode r1 = com.squareup.okhttp.internal.spdy.ErrorCode.PROTOCOL_ERROR
                r0.b(r1)
                com.squareup.okhttp.internal.spdy.m r0 = com.squareup.okhttp.internal.spdy.m.this
                r0.b(r11)
                goto L_0x000d
            L_0x00a0:
                r0.a(r13, r14)
                if (r10 == 0) goto L_0x000d
                r0.h()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.spdy.m.b.a(boolean, boolean, int, int, java.util.List, com.squareup.okhttp.internal.spdy.HeadersMode):void");
        }

        public void a(int i, ErrorCode errorCode) {
            if (m.this.d(i)) {
                m.this.c(i, errorCode);
                return;
            }
            n b2 = m.this.b(i);
            if (b2 != null) {
                b2.c(errorCode);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, boolean):boolean
         arg types: [com.squareup.okhttp.internal.spdy.m, int]
         candidates:
          com.squareup.okhttp.internal.spdy.m.a(int, java.util.List<com.squareup.okhttp.internal.spdy.c>):void
          com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.ErrorCode, com.squareup.okhttp.internal.spdy.ErrorCode):void
          com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, int):boolean
          com.squareup.okhttp.internal.spdy.m.a(int, long):void
          com.squareup.okhttp.internal.spdy.m.a(int, com.squareup.okhttp.internal.spdy.ErrorCode):void
          com.squareup.okhttp.internal.spdy.m.a(com.squareup.okhttp.internal.spdy.m, boolean):boolean */
        public void a(boolean z, k kVar) {
            n[] nVarArr;
            long j;
            synchronized (m.this) {
                int e2 = m.this.f6601f.e(65536);
                if (z) {
                    m.this.f6601f.a();
                }
                m.this.f6601f.a(kVar);
                if (m.this.a() == Protocol.HTTP_2) {
                    a(kVar);
                }
                int e3 = m.this.f6601f.e(65536);
                if (e3 == -1 || e3 == e2) {
                    nVarArr = null;
                    j = 0;
                } else {
                    j = (long) (e3 - e2);
                    if (!m.this.x) {
                        m.this.a(j);
                        boolean unused = m.this.x = true;
                    }
                    nVarArr = !m.this.n.isEmpty() ? (n[]) m.this.n.values().toArray(new n[m.this.n.size()]) : null;
                }
            }
            if (nVarArr != null && j != 0) {
                for (n nVar : nVarArr) {
                    synchronized (nVar) {
                        nVar.a(j);
                    }
                }
            }
        }

        private void a(final k kVar) {
            m.l.execute(new com.squareup.okhttp.internal.c("OkHttp %s ACK Settings", new Object[]{m.this.o}) {
                public void e() {
                    try {
                        m.this.i.a(kVar);
                    } catch (IOException e2) {
                    }
                }
            });
        }

        public void a() {
        }

        public void a(boolean z, int i, int i2) {
            if (z) {
                i c2 = m.this.c(i);
                if (c2 != null) {
                    c2.b();
                    return;
                }
                return;
            }
            m.this.a(true, i, i2, null);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.squareup.okhttp.internal.spdy.m.b(com.squareup.okhttp.internal.spdy.m, boolean):boolean
         arg types: [com.squareup.okhttp.internal.spdy.m, int]
         candidates:
          com.squareup.okhttp.internal.spdy.m.b(com.squareup.okhttp.internal.spdy.m, int):int
          com.squareup.okhttp.internal.spdy.m.b(int, com.squareup.okhttp.internal.spdy.ErrorCode):void
          com.squareup.okhttp.internal.spdy.m.b(com.squareup.okhttp.internal.spdy.m, boolean):boolean */
        public void a(int i, ErrorCode errorCode, ByteString byteString) {
            n[] nVarArr;
            if (byteString.g() > 0) {
            }
            synchronized (m.this) {
                nVarArr = (n[]) m.this.n.values().toArray(new n[m.this.n.size()]);
                boolean unused = m.this.r = true;
            }
            for (n nVar : nVarArr) {
                if (nVar.a() > i && nVar.c()) {
                    nVar.c(ErrorCode.REFUSED_STREAM);
                    m.this.b(nVar.a());
                }
            }
        }

        public void a(int i, long j) {
            if (i == 0) {
                synchronized (m.this) {
                    m.this.f6599d += j;
                    m.this.notifyAll();
                }
                return;
            }
            n a2 = m.this.a(i);
            if (a2 != null) {
                synchronized (a2) {
                    a2.a(j);
                }
            }
        }

        public void a(int i, int i2, int i3, boolean z) {
        }

        public void a(int i, int i2, List<c> list) {
            m.this.a(i2, list);
        }
    }

    /* access modifiers changed from: private */
    public boolean d(int i2) {
        return this.f6596a == Protocol.HTTP_2 && i2 != 0 && (i2 & 1) == 0;
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<c> list) {
        synchronized (this) {
            if (this.y.contains(Integer.valueOf(i2))) {
                a(i2, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.y.add(Integer.valueOf(i2));
            final int i3 = i2;
            final List<c> list2 = list;
            this.t.execute(new com.squareup.okhttp.internal.c("OkHttp %s Push Request[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
                public void e() {
                    if (m.this.v.a(i3, list2)) {
                        try {
                            m.this.i.a(i3, ErrorCode.CANCEL);
                            synchronized (m.this) {
                                m.this.y.remove(Integer.valueOf(i3));
                            }
                        } catch (IOException e2) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<c> list, boolean z) {
        final int i3 = i2;
        final List<c> list2 = list;
        final boolean z2 = z;
        this.t.execute(new com.squareup.okhttp.internal.c("OkHttp %s Push Headers[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void e() {
                boolean a2 = m.this.v.a(i3, list2, z2);
                if (a2) {
                    try {
                        m.this.i.a(i3, ErrorCode.CANCEL);
                    } catch (IOException e2) {
                        return;
                    }
                }
                if (a2 || z2) {
                    synchronized (m.this) {
                        m.this.y.remove(Integer.valueOf(i3));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i2, e eVar, int i3, boolean z) {
        final c cVar = new c();
        eVar.a((long) i3);
        eVar.a(cVar, (long) i3);
        if (cVar.b() != ((long) i3)) {
            throw new IOException(cVar.b() + " != " + i3);
        }
        final int i4 = i2;
        final int i5 = i3;
        final boolean z2 = z;
        this.t.execute(new com.squareup.okhttp.internal.c("OkHttp %s Push Data[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void e() {
                try {
                    boolean a2 = m.this.v.a(i4, cVar, i5, z2);
                    if (a2) {
                        m.this.i.a(i4, ErrorCode.CANCEL);
                    }
                    if (a2 || z2) {
                        synchronized (m.this) {
                            m.this.y.remove(Integer.valueOf(i4));
                        }
                    }
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(int i2, ErrorCode errorCode) {
        final int i3 = i2;
        final ErrorCode errorCode2 = errorCode;
        this.t.execute(new com.squareup.okhttp.internal.c("OkHttp %s Push Reset[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void e() {
                m.this.v.a(i3, errorCode2);
                synchronized (m.this) {
                    m.this.y.remove(Integer.valueOf(i3));
                }
            }
        });
    }
}
