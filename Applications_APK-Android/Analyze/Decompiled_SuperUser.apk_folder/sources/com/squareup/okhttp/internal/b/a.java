package com.squareup.okhttp.internal.b;

import javax.security.auth.x500.X500Principal;

/* compiled from: DistinguishedNameParser */
final class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f6478a;

    /* renamed from: b  reason: collision with root package name */
    private final int f6479b = this.f6478a.length();

    /* renamed from: c  reason: collision with root package name */
    private int f6480c;

    /* renamed from: d  reason: collision with root package name */
    private int f6481d;

    /* renamed from: e  reason: collision with root package name */
    private int f6482e;

    /* renamed from: f  reason: collision with root package name */
    private int f6483f;

    /* renamed from: g  reason: collision with root package name */
    private char[] f6484g;

    public a(X500Principal x500Principal) {
        this.f6478a = x500Principal.getName("RFC2253");
    }

    private String a() {
        while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] == ' ') {
            this.f6480c++;
        }
        if (this.f6480c == this.f6479b) {
            return null;
        }
        this.f6481d = this.f6480c;
        this.f6480c++;
        while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] != '=' && this.f6484g[this.f6480c] != ' ') {
            this.f6480c++;
        }
        if (this.f6480c >= this.f6479b) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f6478a);
        }
        this.f6482e = this.f6480c;
        if (this.f6484g[this.f6480c] == ' ') {
            while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] != '=' && this.f6484g[this.f6480c] == ' ') {
                this.f6480c++;
            }
            if (this.f6484g[this.f6480c] != '=' || this.f6480c == this.f6479b) {
                throw new IllegalStateException("Unexpected end of DN: " + this.f6478a);
            }
        }
        this.f6480c++;
        while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] == ' ') {
            this.f6480c++;
        }
        if (this.f6482e - this.f6481d > 4 && this.f6484g[this.f6481d + 3] == '.' && ((this.f6484g[this.f6481d] == 'O' || this.f6484g[this.f6481d] == 'o') && ((this.f6484g[this.f6481d + 1] == 'I' || this.f6484g[this.f6481d + 1] == 'i') && (this.f6484g[this.f6481d + 2] == 'D' || this.f6484g[this.f6481d + 2] == 'd')))) {
            this.f6481d += 4;
        }
        return new String(this.f6484g, this.f6481d, this.f6482e - this.f6481d);
    }

    private String b() {
        this.f6480c++;
        this.f6481d = this.f6480c;
        this.f6482e = this.f6481d;
        while (this.f6480c != this.f6479b) {
            if (this.f6484g[this.f6480c] == '\"') {
                this.f6480c++;
                while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] == ' ') {
                    this.f6480c++;
                }
                return new String(this.f6484g, this.f6481d, this.f6482e - this.f6481d);
            }
            if (this.f6484g[this.f6480c] == '\\') {
                this.f6484g[this.f6482e] = e();
            } else {
                this.f6484g[this.f6482e] = this.f6484g[this.f6480c];
            }
            this.f6480c++;
            this.f6482e++;
        }
        throw new IllegalStateException("Unexpected end of DN: " + this.f6478a);
    }

    private String c() {
        if (this.f6480c + 4 >= this.f6479b) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f6478a);
        }
        this.f6481d = this.f6480c;
        this.f6480c++;
        while (true) {
            if (this.f6480c == this.f6479b || this.f6484g[this.f6480c] == '+' || this.f6484g[this.f6480c] == ',' || this.f6484g[this.f6480c] == ';') {
                this.f6482e = this.f6480c;
            } else if (this.f6484g[this.f6480c] == ' ') {
                this.f6482e = this.f6480c;
                this.f6480c++;
                while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] == ' ') {
                    this.f6480c++;
                }
            } else {
                if (this.f6484g[this.f6480c] >= 'A' && this.f6484g[this.f6480c] <= 'F') {
                    char[] cArr = this.f6484g;
                    int i = this.f6480c;
                    cArr[i] = (char) (cArr[i] + ' ');
                }
                this.f6480c++;
            }
        }
        this.f6482e = this.f6480c;
        int i2 = this.f6482e - this.f6481d;
        if (i2 < 5 || (i2 & 1) == 0) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f6478a);
        }
        byte[] bArr = new byte[(i2 / 2)];
        int i3 = this.f6481d + 1;
        for (int i4 = 0; i4 < bArr.length; i4++) {
            bArr[i4] = (byte) a(i3);
            i3 += 2;
        }
        return new String(this.f6484g, this.f6481d, i2);
    }

    private String d() {
        this.f6481d = this.f6480c;
        this.f6482e = this.f6480c;
        while (this.f6480c < this.f6479b) {
            switch (this.f6484g[this.f6480c]) {
                case ' ':
                    this.f6483f = this.f6482e;
                    this.f6480c++;
                    char[] cArr = this.f6484g;
                    int i = this.f6482e;
                    this.f6482e = i + 1;
                    cArr[i] = ' ';
                    while (this.f6480c < this.f6479b && this.f6484g[this.f6480c] == ' ') {
                        char[] cArr2 = this.f6484g;
                        int i2 = this.f6482e;
                        this.f6482e = i2 + 1;
                        cArr2[i2] = ' ';
                        this.f6480c++;
                    }
                    if (this.f6480c != this.f6479b && this.f6484g[this.f6480c] != ',' && this.f6484g[this.f6480c] != '+' && this.f6484g[this.f6480c] != ';') {
                        break;
                    } else {
                        return new String(this.f6484g, this.f6481d, this.f6483f - this.f6481d);
                    }
                    break;
                case '+':
                case ',':
                case ';':
                    return new String(this.f6484g, this.f6481d, this.f6482e - this.f6481d);
                case '\\':
                    char[] cArr3 = this.f6484g;
                    int i3 = this.f6482e;
                    this.f6482e = i3 + 1;
                    cArr3[i3] = e();
                    this.f6480c++;
                    break;
                default:
                    char[] cArr4 = this.f6484g;
                    int i4 = this.f6482e;
                    this.f6482e = i4 + 1;
                    cArr4[i4] = this.f6484g[this.f6480c];
                    this.f6480c++;
                    break;
            }
        }
        return new String(this.f6484g, this.f6481d, this.f6482e - this.f6481d);
    }

    private char e() {
        this.f6480c++;
        if (this.f6480c == this.f6479b) {
            throw new IllegalStateException("Unexpected end of DN: " + this.f6478a);
        }
        switch (this.f6484g[this.f6480c]) {
            case ' ':
            case '\"':
            case '#':
            case '%':
            case '*':
            case '+':
            case ',':
            case ';':
            case '<':
            case '=':
            case '>':
            case '\\':
            case '_':
                return this.f6484g[this.f6480c];
            default:
                return f();
        }
    }

    private char f() {
        int i;
        int i2;
        int a2 = a(this.f6480c);
        this.f6480c++;
        if (a2 < 128) {
            return (char) a2;
        }
        if (a2 < 192 || a2 > 247) {
            return '?';
        }
        if (a2 <= 223) {
            i = 1;
            i2 = a2 & 31;
        } else if (a2 <= 239) {
            i = 2;
            i2 = a2 & 15;
        } else {
            i = 3;
            i2 = a2 & 7;
        }
        int i3 = i2;
        for (int i4 = 0; i4 < i; i4++) {
            this.f6480c++;
            if (this.f6480c == this.f6479b || this.f6484g[this.f6480c] != '\\') {
                return '?';
            }
            this.f6480c++;
            int a3 = a(this.f6480c);
            this.f6480c++;
            if ((a3 & 192) != 128) {
                return '?';
            }
            i3 = (i3 << 6) + (a3 & 63);
        }
        return (char) i3;
    }

    private int a(int i) {
        int i2;
        int i3;
        if (i + 1 >= this.f6479b) {
            throw new IllegalStateException("Malformed DN: " + this.f6478a);
        }
        char c2 = this.f6484g[i];
        if (c2 >= '0' && c2 <= '9') {
            i2 = c2 - '0';
        } else if (c2 >= 'a' && c2 <= 'f') {
            i2 = c2 - 'W';
        } else if (c2 < 'A' || c2 > 'F') {
            throw new IllegalStateException("Malformed DN: " + this.f6478a);
        } else {
            i2 = c2 - '7';
        }
        char c3 = this.f6484g[i + 1];
        if (c3 >= '0' && c3 <= '9') {
            i3 = c3 - '0';
        } else if (c3 >= 'a' && c3 <= 'f') {
            i3 = c3 - 'W';
        } else if (c3 < 'A' || c3 > 'F') {
            throw new IllegalStateException("Malformed DN: " + this.f6478a);
        } else {
            i3 = c3 - '7';
        }
        return (i2 << 4) + i3;
    }

    public String a(String str) {
        this.f6480c = 0;
        this.f6481d = 0;
        this.f6482e = 0;
        this.f6483f = 0;
        this.f6484g = this.f6478a.toCharArray();
        String a2 = a();
        if (a2 == null) {
            return null;
        }
        do {
            String str2 = "";
            if (this.f6480c == this.f6479b) {
                return null;
            }
            switch (this.f6484g[this.f6480c]) {
                case '\"':
                    str2 = b();
                    break;
                case '#':
                    str2 = c();
                    break;
                case '+':
                case ',':
                case ';':
                    break;
                default:
                    str2 = d();
                    break;
            }
            if (str.equalsIgnoreCase(a2)) {
                return str2;
            }
            if (this.f6480c >= this.f6479b) {
                return null;
            }
            if (this.f6484g[this.f6480c] == ',' || this.f6484g[this.f6480c] == ';' || this.f6484g[this.f6480c] == '+') {
                this.f6480c++;
                a2 = a();
            } else {
                throw new IllegalStateException("Malformed DN: " + this.f6478a);
            }
        } while (a2 != null);
        throw new IllegalStateException("Malformed DN: " + this.f6478a);
    }
}
