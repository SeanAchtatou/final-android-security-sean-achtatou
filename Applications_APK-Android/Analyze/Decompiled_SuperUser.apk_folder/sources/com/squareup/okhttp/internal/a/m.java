package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.internal.h;
import java.net.ProtocolException;
import okio.c;
import okio.p;
import okio.r;

/* compiled from: RetryableSink */
public final class m implements p {

    /* renamed from: a  reason: collision with root package name */
    private boolean f6459a;

    /* renamed from: b  reason: collision with root package name */
    private final int f6460b;

    /* renamed from: c  reason: collision with root package name */
    private final c f6461c;

    public m(int i) {
        this.f6461c = new c();
        this.f6460b = i;
    }

    public m() {
        this(-1);
    }

    public void close() {
        if (!this.f6459a) {
            this.f6459a = true;
            if (this.f6461c.b() < ((long) this.f6460b)) {
                throw new ProtocolException("content-length promised " + this.f6460b + " bytes, but received " + this.f6461c.b());
            }
        }
    }

    public void a_(c cVar, long j) {
        if (this.f6459a) {
            throw new IllegalStateException("closed");
        }
        h.a(cVar.b(), 0, j);
        if (this.f6460b == -1 || this.f6461c.b() <= ((long) this.f6460b) - j) {
            this.f6461c.a_(cVar, j);
            return;
        }
        throw new ProtocolException("exceeded content-length limit of " + this.f6460b + " bytes");
    }

    public void flush() {
    }

    public r a() {
        return r.f8239b;
    }

    public long b() {
        return this.f6461c.b();
    }

    public void a(p pVar) {
        c cVar = new c();
        this.f6461c.a(cVar, 0, this.f6461c.b());
        pVar.a_(cVar, cVar.b());
    }
}
