package com.squareup.okhttp.internal.spdy;

import java.io.Closeable;
import java.util.List;
import okio.ByteString;
import okio.e;

/* compiled from: FrameReader */
public interface a extends Closeable {

    /* renamed from: com.squareup.okhttp.internal.spdy.a$a  reason: collision with other inner class name */
    /* compiled from: FrameReader */
    public interface C0094a {
        void a();

        void a(int i, int i2, int i3, boolean z);

        void a(int i, int i2, List<c> list);

        void a(int i, long j);

        void a(int i, ErrorCode errorCode);

        void a(int i, ErrorCode errorCode, ByteString byteString);

        void a(boolean z, int i, int i2);

        void a(boolean z, int i, e eVar, int i2);

        void a(boolean z, k kVar);

        void a(boolean z, boolean z2, int i, int i2, List<c> list, HeadersMode headersMode);
    }

    void a();

    boolean a(C0094a aVar);
}
