package com.squareup.okhttp.internal.a;

import com.squareup.okhttp.b;
import com.squareup.okhttp.h;
import com.squareup.okhttp.l;
import com.squareup.okhttp.s;
import com.squareup.okhttp.u;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

/* compiled from: AuthenticatorAdapter */
public final class a implements b {

    /* renamed from: a  reason: collision with root package name */
    public static final b f6396a = new a();

    public s a(Proxy proxy, u uVar) {
        PasswordAuthentication requestPasswordAuthentication;
        List<h> i = uVar.i();
        s a2 = uVar.a();
        URL a3 = a2.a();
        int size = i.size();
        for (int i2 = 0; i2 < size; i2++) {
            h hVar = i.get(i2);
            if ("Basic".equalsIgnoreCase(hVar.a()) && (requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(a3.getHost(), a(proxy, a3), a3.getPort(), a3.getProtocol(), hVar.b(), hVar.a(), a3, Authenticator.RequestorType.SERVER)) != null) {
                return a2.h().a("Authorization", l.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()))).b();
            }
        }
        return null;
    }

    public s b(Proxy proxy, u uVar) {
        List<h> i = uVar.i();
        s a2 = uVar.a();
        URL a3 = a2.a();
        int size = i.size();
        for (int i2 = 0; i2 < size; i2++) {
            h hVar = i.get(i2);
            if ("Basic".equalsIgnoreCase(hVar.a())) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) proxy.address();
                PasswordAuthentication requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(inetSocketAddress.getHostName(), a(proxy, a3), inetSocketAddress.getPort(), a3.getProtocol(), hVar.b(), hVar.a(), a3, Authenticator.RequestorType.PROXY);
                if (requestPasswordAuthentication != null) {
                    return a2.h().a("Proxy-Authorization", l.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()))).b();
                }
            }
        }
        return null;
    }

    private InetAddress a(Proxy proxy, URL url) {
        if (proxy == null || proxy.type() == Proxy.Type.DIRECT) {
            return InetAddress.getByName(url.getHost());
        }
        return ((InetSocketAddress) proxy.address()).getAddress();
    }
}
