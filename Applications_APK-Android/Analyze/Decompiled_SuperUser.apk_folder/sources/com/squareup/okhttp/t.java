package com.squareup.okhttp;

import com.squareup.okhttp.internal.h;
import java.nio.charset.Charset;
import okio.d;

/* compiled from: RequestBody */
public abstract class t {
    public abstract q a();

    public abstract void a(d dVar);

    public long b() {
        return -1;
    }

    public static t a(q qVar, String str) {
        Charset charset = h.f6510c;
        if (qVar != null && (charset = qVar.a()) == null) {
            charset = h.f6510c;
            qVar = q.a(qVar + "; charset=utf-8");
        }
        return a(qVar, str.getBytes(charset));
    }

    public static t a(q qVar, byte[] bArr) {
        return a(qVar, bArr, 0, bArr.length);
    }

    public static t a(final q qVar, final byte[] bArr, final int i, final int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        h.a((long) bArr.length, (long) i, (long) i2);
        return new t() {
            public q a() {
                return qVar;
            }

            public long b() {
                return (long) i2;
            }

            public void a(d dVar) {
                dVar.c(bArr, i, i2);
            }
        };
    }
}
