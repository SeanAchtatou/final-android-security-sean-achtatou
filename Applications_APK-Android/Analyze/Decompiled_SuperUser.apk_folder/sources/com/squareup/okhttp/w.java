package com.squareup.okhttp;

import java.net.InetSocketAddress;
import java.net.Proxy;

/* compiled from: Route */
public final class w {

    /* renamed from: a  reason: collision with root package name */
    final a f6740a;

    /* renamed from: b  reason: collision with root package name */
    final Proxy f6741b;

    /* renamed from: c  reason: collision with root package name */
    final InetSocketAddress f6742c;

    /* renamed from: d  reason: collision with root package name */
    final k f6743d;

    /* renamed from: e  reason: collision with root package name */
    final boolean f6744e;

    public w(a aVar, Proxy proxy, InetSocketAddress inetSocketAddress, k kVar, boolean z) {
        if (aVar == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else if (kVar == null) {
            throw new NullPointerException("connectionConfiguration == null");
        } else {
            this.f6740a = aVar;
            this.f6741b = proxy;
            this.f6742c = inetSocketAddress;
            this.f6743d = kVar;
            this.f6744e = z;
        }
    }

    public a a() {
        return this.f6740a;
    }

    public Proxy b() {
        return this.f6741b;
    }

    public boolean c() {
        return this.f6740a.f6349e != null && this.f6741b.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof w)) {
            return false;
        }
        w wVar = (w) obj;
        if (!this.f6740a.equals(wVar.f6740a) || !this.f6741b.equals(wVar.f6741b) || !this.f6742c.equals(wVar.f6742c) || !this.f6743d.equals(wVar.f6743d) || this.f6744e != wVar.f6744e) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f6744e ? 1 : 0) + ((((((((this.f6740a.hashCode() + 527) * 31) + this.f6741b.hashCode()) * 31) + this.f6742c.hashCode()) * 31) + this.f6743d.hashCode()) * 31);
    }
}
