package com.squareup.okhttp;

/* compiled from: Challenge */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final String f6384a;

    /* renamed from: b  reason: collision with root package name */
    private final String f6385b;

    public h(String str, String str2) {
        this.f6384a = str;
        this.f6385b = str2;
    }

    public String a() {
        return this.f6384a;
    }

    public String b() {
        return this.f6385b;
    }

    public boolean equals(Object obj) {
        return (obj instanceof h) && com.squareup.okhttp.internal.h.a(this.f6384a, ((h) obj).f6384a) && com.squareup.okhttp.internal.h.a(this.f6385b, ((h) obj).f6385b);
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.f6385b != null) {
            i = this.f6385b.hashCode();
        } else {
            i = 0;
        }
        int i3 = (i + 899) * 31;
        if (this.f6384a != null) {
            i2 = this.f6384a.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        return this.f6384a + " realm=\"" + this.f6385b + "\"";
    }
}
