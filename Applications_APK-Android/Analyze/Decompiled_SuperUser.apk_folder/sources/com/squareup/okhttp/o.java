package com.squareup.okhttp;

import com.kingouser.com.util.ShellUtils;
import com.squareup.okhttp.internal.a.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/* compiled from: Headers */
public final class o {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f6690a;

    private o(a aVar) {
        this.f6690a = (String[]) aVar.f6691a.toArray(new String[aVar.f6691a.size()]);
    }

    public String a(String str) {
        return a(this.f6690a, str);
    }

    public Date b(String str) {
        String a2 = a(str);
        if (a2 != null) {
            return f.a(a2);
        }
        return null;
    }

    public int a() {
        return this.f6690a.length / 2;
    }

    public String a(int i) {
        int i2 = i * 2;
        if (i2 < 0 || i2 >= this.f6690a.length) {
            return null;
        }
        return this.f6690a[i2];
    }

    public String b(int i) {
        int i2 = (i * 2) + 1;
        if (i2 < 0 || i2 >= this.f6690a.length) {
            return null;
        }
        return this.f6690a[i2];
    }

    public a b() {
        a aVar = new a();
        Collections.addAll(aVar.f6691a, this.f6690a);
        return aVar;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            sb.append(a(i)).append(": ").append(b(i)).append(ShellUtils.COMMAND_LINE_END);
        }
        return sb.toString();
    }

    private static String a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    /* compiled from: Headers */
    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final List<String> f6691a = new ArrayList(20);

        /* access modifiers changed from: package-private */
        public a a(String str) {
            int indexOf = str.indexOf(":", 1);
            if (indexOf != -1) {
                return c(str.substring(0, indexOf), str.substring(indexOf + 1));
            }
            if (str.startsWith(":")) {
                return c("", str.substring(1));
            }
            return c("", str);
        }

        public a a(String str, String str2) {
            if (str == null) {
                throw new IllegalArgumentException("name == null");
            } else if (str2 == null) {
                throw new IllegalArgumentException("value == null");
            } else if (str.length() != 0 && str.indexOf(0) == -1 && str2.indexOf(0) == -1) {
                return c(str, str2);
            } else {
                throw new IllegalArgumentException("Unexpected header: " + str + ": " + str2);
            }
        }

        private a c(String str, String str2) {
            this.f6691a.add(str);
            this.f6691a.add(str2.trim());
            return this;
        }

        public a b(String str) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f6691a.size()) {
                    return this;
                }
                if (str.equalsIgnoreCase(this.f6691a.get(i2))) {
                    this.f6691a.remove(i2);
                    this.f6691a.remove(i2);
                    i2 -= 2;
                }
                i = i2 + 2;
            }
        }

        public a b(String str, String str2) {
            b(str);
            a(str, str2);
            return this;
        }

        public o a() {
            return new o(this);
        }
    }
}
