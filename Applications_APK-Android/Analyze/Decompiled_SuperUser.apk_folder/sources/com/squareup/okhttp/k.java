package com.squareup.okhttp;

import com.squareup.okhttp.internal.f;
import com.squareup.okhttp.internal.h;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

/* compiled from: ConnectionSpec */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    public static final k f6670a = new a(true).a(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_DHE_DSS_WITH_AES_128_CBC_SHA, CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256, CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA, CipherSuite.TLS_RSA_WITH_3DES_EDE_CBC_SHA).a(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0).a(true).a();

    /* renamed from: b  reason: collision with root package name */
    public static final k f6671b = new a(f6670a).a(TlsVersion.TLS_1_0).a(true).a();

    /* renamed from: c  reason: collision with root package name */
    public static final k f6672c = new a(false).a();

    /* renamed from: d  reason: collision with root package name */
    final boolean f6673d;

    /* renamed from: e  reason: collision with root package name */
    final boolean f6674e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final String[] f6675f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public final String[] f6676g;

    private k(a aVar) {
        this.f6673d = aVar.f6677a;
        this.f6675f = aVar.f6678b;
        this.f6676g = aVar.f6679c;
        this.f6674e = aVar.f6680d;
    }

    public boolean a() {
        return this.f6673d;
    }

    public List<CipherSuite> b() {
        if (this.f6675f == null) {
            return null;
        }
        CipherSuite[] cipherSuiteArr = new CipherSuite[this.f6675f.length];
        for (int i = 0; i < this.f6675f.length; i++) {
            cipherSuiteArr[i] = CipherSuite.a(this.f6675f[i]);
        }
        return h.a(cipherSuiteArr);
    }

    public List<TlsVersion> c() {
        TlsVersion[] tlsVersionArr = new TlsVersion[this.f6676g.length];
        for (int i = 0; i < this.f6676g.length; i++) {
            tlsVersionArr[i] = TlsVersion.a(this.f6676g[i]);
        }
        return h.a(tlsVersionArr);
    }

    public boolean d() {
        return this.f6674e;
    }

    /* access modifiers changed from: package-private */
    public void a(SSLSocket sSLSocket, w wVar) {
        k a2 = a(sSLSocket);
        sSLSocket.setEnabledProtocols(a2.f6676g);
        String[] strArr = a2.f6675f;
        if (wVar.f6744e && Arrays.asList(sSLSocket.getSupportedCipherSuites()).contains("TLS_FALLBACK_SCSV")) {
            if (strArr == null) {
                strArr = sSLSocket.getEnabledCipherSuites();
            }
            String[] strArr2 = new String[(strArr.length + 1)];
            System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
            strArr2[strArr2.length - 1] = "TLS_FALLBACK_SCSV";
            strArr = strArr2;
        }
        if (strArr != null) {
            sSLSocket.setEnabledCipherSuites(strArr);
        }
        f a3 = f.a();
        if (a2.f6674e) {
            a3.a(sSLSocket, wVar.f6740a.f6346b, wVar.f6740a.i);
        }
    }

    private k a(SSLSocket sSLSocket) {
        String[] strArr;
        if (this.f6675f != null) {
            strArr = (String[]) h.a(String.class, this.f6675f, sSLSocket.getEnabledCipherSuites());
        } else {
            strArr = null;
        }
        return new a(this).a(strArr).b((String[]) h.a(String.class, this.f6676g, sSLSocket.getEnabledProtocols())).a();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof k)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        k kVar = (k) obj;
        if (this.f6673d != kVar.f6673d) {
            return false;
        }
        if (!this.f6673d || (Arrays.equals(this.f6675f, kVar.f6675f) && Arrays.equals(this.f6676g, kVar.f6676g) && this.f6674e == kVar.f6674e)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (!this.f6673d) {
            return 17;
        }
        return (this.f6674e ? 0 : 1) + ((((Arrays.hashCode(this.f6675f) + 527) * 31) + Arrays.hashCode(this.f6676g)) * 31);
    }

    public String toString() {
        if (!this.f6673d) {
            return "ConnectionSpec()";
        }
        List<CipherSuite> b2 = b();
        return "ConnectionSpec(cipherSuites=" + (b2 == null ? "[use default]" : b2.toString()) + ", tlsVersions=" + c() + ", supportsTlsExtensions=" + this.f6674e + ")";
    }

    /* compiled from: ConnectionSpec */
    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public boolean f6677a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public String[] f6678b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public String[] f6679c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public boolean f6680d;

        a(boolean z) {
            this.f6677a = z;
        }

        public a(k kVar) {
            this.f6677a = kVar.f6673d;
            this.f6678b = kVar.f6675f;
            this.f6679c = kVar.f6676g;
            this.f6680d = kVar.f6674e;
        }

        public a a(CipherSuite... cipherSuiteArr) {
            if (!this.f6677a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            }
            String[] strArr = new String[cipherSuiteArr.length];
            for (int i = 0; i < cipherSuiteArr.length; i++) {
                strArr[i] = cipherSuiteArr[i].aS;
            }
            this.f6678b = strArr;
            return this;
        }

        public a a(String... strArr) {
            if (!this.f6677a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            }
            if (strArr == null) {
                this.f6678b = null;
            } else {
                this.f6678b = (String[]) strArr.clone();
            }
            return this;
        }

        public a a(TlsVersion... tlsVersionArr) {
            if (!this.f6677a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            }
            String[] strArr = new String[tlsVersionArr.length];
            for (int i = 0; i < tlsVersionArr.length; i++) {
                strArr[i] = tlsVersionArr[i].f6344e;
            }
            this.f6679c = strArr;
            return this;
        }

        public a b(String... strArr) {
            if (!this.f6677a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            }
            if (strArr == null) {
                this.f6679c = null;
            } else {
                this.f6679c = (String[]) strArr.clone();
            }
            return this;
        }

        public a a(boolean z) {
            if (!this.f6677a) {
                throw new IllegalStateException("no TLS extensions for cleartext connections");
            }
            this.f6680d = z;
            return this;
        }

        public k a() {
            return new k(this);
        }
    }
}
