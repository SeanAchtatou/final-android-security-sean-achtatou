package com.squareup.okhttp;

import com.squareup.okhttp.internal.h;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: Address */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    final Proxy f6345a;

    /* renamed from: b  reason: collision with root package name */
    final String f6346b;

    /* renamed from: c  reason: collision with root package name */
    final int f6347c;

    /* renamed from: d  reason: collision with root package name */
    final SocketFactory f6348d;

    /* renamed from: e  reason: collision with root package name */
    final SSLSocketFactory f6349e;

    /* renamed from: f  reason: collision with root package name */
    final HostnameVerifier f6350f;

    /* renamed from: g  reason: collision with root package name */
    final g f6351g;

    /* renamed from: h  reason: collision with root package name */
    final b f6352h;
    final List<Protocol> i;
    final List<k> j;
    final ProxySelector k;

    public a(String str, int i2, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, g gVar, b bVar, Proxy proxy, List<Protocol> list, List<k> list2, ProxySelector proxySelector) {
        if (str == null) {
            throw new NullPointerException("uriHost == null");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("uriPort <= 0: " + i2);
        } else if (bVar == null) {
            throw new IllegalArgumentException("authenticator == null");
        } else if (list == null) {
            throw new IllegalArgumentException("protocols == null");
        } else if (proxySelector == null) {
            throw new IllegalArgumentException("proxySelector == null");
        } else {
            this.f6345a = proxy;
            this.f6346b = str;
            this.f6347c = i2;
            this.f6348d = socketFactory;
            this.f6349e = sSLSocketFactory;
            this.f6350f = hostnameVerifier;
            this.f6351g = gVar;
            this.f6352h = bVar;
            this.i = h.a(list);
            this.j = h.a(list2);
            this.k = proxySelector;
        }
    }

    public String a() {
        return this.f6346b;
    }

    public int b() {
        return this.f6347c;
    }

    public List<k> c() {
        return this.j;
    }

    public Proxy d() {
        return this.f6345a;
    }

    public ProxySelector e() {
        return this.k;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (!h.a(this.f6345a, aVar.f6345a) || !this.f6346b.equals(aVar.f6346b) || this.f6347c != aVar.f6347c || !h.a(this.f6349e, aVar.f6349e) || !h.a(this.f6350f, aVar.f6350f) || !h.a(this.f6351g, aVar.f6351g) || !h.a(this.f6352h, aVar.f6352h) || !h.a(this.i, aVar.i) || !h.a(this.j, aVar.j) || !h.a(this.k, aVar.k)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i2;
        int i3;
        int i4 = 0;
        int hashCode = ((((((this.f6345a != null ? this.f6345a.hashCode() : 0) + 527) * 31) + this.f6346b.hashCode()) * 31) + this.f6347c) * 31;
        if (this.f6349e != null) {
            i2 = this.f6349e.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i2 + hashCode) * 31;
        if (this.f6350f != null) {
            i3 = this.f6350f.hashCode();
        } else {
            i3 = 0;
        }
        int i6 = (i3 + i5) * 31;
        if (this.f6351g != null) {
            i4 = this.f6351g.hashCode();
        }
        return ((((((((i6 + i4) * 31) + this.f6352h.hashCode()) * 31) + this.i.hashCode()) * 31) + this.j.hashCode()) * 31) + this.k.hashCode();
    }
}
