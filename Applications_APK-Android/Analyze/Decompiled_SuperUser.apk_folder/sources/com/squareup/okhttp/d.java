package com.squareup.okhttp;

import java.util.concurrent.TimeUnit;

/* compiled from: CacheControl */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    public static final d f6354a = new a().a().c();

    /* renamed from: b  reason: collision with root package name */
    public static final d f6355b = new a().b().a(Integer.MAX_VALUE, TimeUnit.SECONDS).c();

    /* renamed from: c  reason: collision with root package name */
    String f6356c;

    /* renamed from: d  reason: collision with root package name */
    private final boolean f6357d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f6358e;

    /* renamed from: f  reason: collision with root package name */
    private final int f6359f;

    /* renamed from: g  reason: collision with root package name */
    private final int f6360g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f6361h;
    private final boolean i;
    private final boolean j;
    private final int k;
    private final int l;
    private final boolean m;
    private final boolean n;

    private d(boolean z, boolean z2, int i2, int i3, boolean z3, boolean z4, boolean z5, int i4, int i5, boolean z6, boolean z7, String str) {
        this.f6357d = z;
        this.f6358e = z2;
        this.f6359f = i2;
        this.f6360g = i3;
        this.f6361h = z3;
        this.i = z4;
        this.j = z5;
        this.k = i4;
        this.l = i5;
        this.m = z6;
        this.n = z7;
        this.f6356c = str;
    }

    private d(a aVar) {
        this.f6357d = aVar.f6362a;
        this.f6358e = aVar.f6363b;
        this.f6359f = aVar.f6364c;
        this.f6360g = -1;
        this.f6361h = false;
        this.i = false;
        this.j = false;
        this.k = aVar.f6365d;
        this.l = aVar.f6366e;
        this.m = aVar.f6367f;
        this.n = aVar.f6368g;
    }

    public boolean a() {
        return this.f6357d;
    }

    public boolean b() {
        return this.f6358e;
    }

    public int c() {
        return this.f6359f;
    }

    public boolean d() {
        return this.f6361h;
    }

    public boolean e() {
        return this.i;
    }

    public boolean f() {
        return this.j;
    }

    public int g() {
        return this.k;
    }

    public int h() {
        return this.l;
    }

    public boolean i() {
        return this.m;
    }

    public static d a(o oVar) {
        boolean z;
        String str;
        String str2;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = -1;
        int i3 = -1;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        int i4 = -1;
        int i5 = -1;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = true;
        int a2 = oVar.a();
        int i6 = 0;
        String str3 = null;
        while (true) {
            z = z2;
            if (i6 >= a2) {
                break;
            }
            String a3 = oVar.a(i6);
            String b2 = oVar.b(i6);
            if (a3.equalsIgnoreCase("Cache-Control")) {
                if (str3 != null) {
                    z9 = false;
                } else {
                    str3 = b2;
                }
            } else if (a3.equalsIgnoreCase("Pragma")) {
                z9 = false;
            } else {
                z2 = z;
                i6++;
            }
            z2 = z;
            int i7 = 0;
            while (i7 < b2.length()) {
                int a4 = com.squareup.okhttp.internal.a.d.a(b2, i7, "=,;");
                String trim = b2.substring(i7, a4).trim();
                if (a4 == b2.length() || b2.charAt(a4) == ',' || b2.charAt(a4) == ';') {
                    i7 = a4 + 1;
                    str2 = null;
                } else {
                    int a5 = com.squareup.okhttp.internal.a.d.a(b2, a4 + 1);
                    if (a5 >= b2.length() || b2.charAt(a5) != '\"') {
                        int a6 = com.squareup.okhttp.internal.a.d.a(b2, a5, ",;");
                        String trim2 = b2.substring(a5, a6).trim();
                        i7 = a6;
                        str2 = trim2;
                    } else {
                        int i8 = a5 + 1;
                        int a7 = com.squareup.okhttp.internal.a.d.a(b2, i8, "\"");
                        String substring = b2.substring(i8, a7);
                        i7 = a7 + 1;
                        str2 = substring;
                    }
                }
                if ("no-cache".equalsIgnoreCase(trim)) {
                    z2 = true;
                } else if ("no-store".equalsIgnoreCase(trim)) {
                    z3 = true;
                } else if ("max-age".equalsIgnoreCase(trim)) {
                    i2 = com.squareup.okhttp.internal.a.d.b(str2, -1);
                } else if ("s-maxage".equalsIgnoreCase(trim)) {
                    i3 = com.squareup.okhttp.internal.a.d.b(str2, -1);
                } else if ("private".equalsIgnoreCase(trim)) {
                    z4 = true;
                } else if ("public".equalsIgnoreCase(trim)) {
                    z5 = true;
                } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                    z6 = true;
                } else if ("max-stale".equalsIgnoreCase(trim)) {
                    i4 = com.squareup.okhttp.internal.a.d.b(str2, Integer.MAX_VALUE);
                } else if ("min-fresh".equalsIgnoreCase(trim)) {
                    i5 = com.squareup.okhttp.internal.a.d.b(str2, -1);
                } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                    z7 = true;
                } else if ("no-transform".equalsIgnoreCase(trim)) {
                    z8 = true;
                }
            }
            i6++;
        }
        if (!z9) {
            str = null;
        } else {
            str = str3;
        }
        return new d(z, z3, i2, i3, z4, z5, z6, i4, i5, z7, z8, str);
    }

    public String toString() {
        String str = this.f6356c;
        if (str != null) {
            return str;
        }
        String j2 = j();
        this.f6356c = j2;
        return j2;
    }

    private String j() {
        StringBuilder sb = new StringBuilder();
        if (this.f6357d) {
            sb.append("no-cache, ");
        }
        if (this.f6358e) {
            sb.append("no-store, ");
        }
        if (this.f6359f != -1) {
            sb.append("max-age=").append(this.f6359f).append(", ");
        }
        if (this.f6360g != -1) {
            sb.append("s-maxage=").append(this.f6360g).append(", ");
        }
        if (this.f6361h) {
            sb.append("private, ");
        }
        if (this.i) {
            sb.append("public, ");
        }
        if (this.j) {
            sb.append("must-revalidate, ");
        }
        if (this.k != -1) {
            sb.append("max-stale=").append(this.k).append(", ");
        }
        if (this.l != -1) {
            sb.append("min-fresh=").append(this.l).append(", ");
        }
        if (this.m) {
            sb.append("only-if-cached, ");
        }
        if (this.n) {
            sb.append("no-transform, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }

    /* compiled from: CacheControl */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f6362a;

        /* renamed from: b  reason: collision with root package name */
        boolean f6363b;

        /* renamed from: c  reason: collision with root package name */
        int f6364c = -1;

        /* renamed from: d  reason: collision with root package name */
        int f6365d = -1;

        /* renamed from: e  reason: collision with root package name */
        int f6366e = -1;

        /* renamed from: f  reason: collision with root package name */
        boolean f6367f;

        /* renamed from: g  reason: collision with root package name */
        boolean f6368g;

        public a a() {
            this.f6362a = true;
            return this;
        }

        public a a(int i, TimeUnit timeUnit) {
            if (i < 0) {
                throw new IllegalArgumentException("maxStale < 0: " + i);
            }
            long seconds = timeUnit.toSeconds((long) i);
            this.f6365d = seconds > 2147483647L ? Integer.MAX_VALUE : (int) seconds;
            return this;
        }

        public a b() {
            this.f6367f = true;
            return this;
        }

        public d c() {
            return new d(this);
        }
    }
}
