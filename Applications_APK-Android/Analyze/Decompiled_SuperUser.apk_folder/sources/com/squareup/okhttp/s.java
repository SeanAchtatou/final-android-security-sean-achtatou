package com.squareup.okhttp;

import com.squareup.okhttp.internal.a.h;
import com.squareup.okhttp.internal.f;
import com.squareup.okhttp.o;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/* compiled from: Request */
public final class s {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final String f6706a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f6707b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final o f6708c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final t f6709d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final Object f6710e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public volatile URL f6711f;

    /* renamed from: g  reason: collision with root package name */
    private volatile URI f6712g;

    /* renamed from: h  reason: collision with root package name */
    private volatile d f6713h;

    private s(a aVar) {
        Object obj;
        this.f6706a = aVar.f6714a;
        this.f6707b = aVar.f6716c;
        this.f6708c = aVar.f6717d.a();
        this.f6709d = aVar.f6718e;
        if (aVar.f6719f != null) {
            obj = aVar.f6719f;
        } else {
            obj = this;
        }
        this.f6710e = obj;
        this.f6711f = aVar.f6715b;
    }

    public URL a() {
        try {
            URL url = this.f6711f;
            if (url != null) {
                return url;
            }
            URL url2 = new URL(this.f6706a);
            this.f6711f = url2;
            return url2;
        } catch (MalformedURLException e2) {
            throw new RuntimeException("Malformed URL: " + this.f6706a, e2);
        }
    }

    public URI b() {
        try {
            URI uri = this.f6712g;
            if (uri != null) {
                return uri;
            }
            URI a2 = f.a().a(a());
            this.f6712g = a2;
            return a2;
        } catch (URISyntaxException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    public String c() {
        return this.f6706a;
    }

    public String d() {
        return this.f6707b;
    }

    public o e() {
        return this.f6708c;
    }

    public String a(String str) {
        return this.f6708c.a(str);
    }

    public t f() {
        return this.f6709d;
    }

    public Object g() {
        return this.f6710e;
    }

    public a h() {
        return new a();
    }

    public d i() {
        d dVar = this.f6713h;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f6708c);
        this.f6713h = a2;
        return a2;
    }

    public boolean j() {
        return a().getProtocol().equals("https");
    }

    public String toString() {
        return "Request{method=" + this.f6707b + ", url=" + this.f6706a + ", tag=" + (this.f6710e != this ? this.f6710e : null) + '}';
    }

    /* compiled from: Request */
    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f6714a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public URL f6715b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public String f6716c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public o.a f6717d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public t f6718e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public Object f6719f;

        public a() {
            this.f6716c = "GET";
            this.f6717d = new o.a();
        }

        private a(s sVar) {
            this.f6714a = sVar.f6706a;
            this.f6715b = sVar.f6711f;
            this.f6716c = sVar.f6707b;
            this.f6718e = sVar.f6709d;
            this.f6719f = sVar.f6710e;
            this.f6717d = sVar.f6708c.b();
        }

        public a a(String str) {
            if (str == null) {
                throw new IllegalArgumentException("url == null");
            }
            this.f6714a = str;
            this.f6715b = null;
            return this;
        }

        public a a(URL url) {
            if (url == null) {
                throw new IllegalArgumentException("url == null");
            }
            this.f6715b = url;
            this.f6714a = url.toString();
            return this;
        }

        public a a(String str, String str2) {
            this.f6717d.b(str, str2);
            return this;
        }

        public a b(String str, String str2) {
            this.f6717d.a(str, str2);
            return this;
        }

        public a b(String str) {
            this.f6717d.b(str);
            return this;
        }

        public a a() {
            return a("GET", (t) null);
        }

        public a a(t tVar) {
            return a("POST", tVar);
        }

        public a a(String str, t tVar) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("method == null || method.length() == 0");
            } else if (tVar == null || h.c(str)) {
                if (tVar == null && h.c(str)) {
                    tVar = t.a((q) null, com.squareup.okhttp.internal.h.f6508a);
                }
                this.f6716c = str;
                this.f6718e = tVar;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            }
        }

        public a a(Object obj) {
            this.f6719f = obj;
            return this;
        }

        public s b() {
            if (this.f6714a != null) {
                return new s(this);
            }
            throw new IllegalStateException("url == null");
        }
    }
}
