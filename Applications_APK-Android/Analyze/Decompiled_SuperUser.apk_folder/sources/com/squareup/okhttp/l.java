package com.squareup.okhttp;

import java.io.UnsupportedEncodingException;
import okio.ByteString;

/* compiled from: Credentials */
public final class l {
    public static String a(String str, String str2) {
        try {
            return "Basic " + ByteString.a((str + ":" + str2).getBytes("ISO-8859-1")).b();
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError();
        }
    }
}
