package com.squareup.okhttp;

import com.squareup.okhttp.internal.h;
import java.io.Closeable;
import java.io.IOException;
import java.nio.charset.Charset;
import okio.e;

/* compiled from: ResponseBody */
public abstract class v implements Closeable {
    public abstract q a();

    public abstract long b();

    public abstract e c();

    /* JADX INFO: finally extract failed */
    public final byte[] d() {
        long b2 = b();
        if (b2 > 2147483647L) {
            throw new IOException("Cannot buffer entire body for content length: " + b2);
        }
        e c2 = c();
        try {
            byte[] q = c2.q();
            h.a(c2);
            if (b2 == -1 || b2 == ((long) q.length)) {
                return q;
            }
            throw new IOException("Content-Length and stream length disagree");
        } catch (Throwable th) {
            h.a(c2);
            throw th;
        }
    }

    public final String e() {
        return new String(d(), f().name());
    }

    private Charset f() {
        q a2 = a();
        return a2 != null ? a2.a(h.f6510c) : h.f6510c;
    }

    public void close() {
        c().close();
    }
}
