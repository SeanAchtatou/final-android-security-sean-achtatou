package com.squareup.okhttp;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: MediaType */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f6692a = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f6693b = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");

    /* renamed from: c  reason: collision with root package name */
    private final String f6694c;

    /* renamed from: d  reason: collision with root package name */
    private final String f6695d;

    /* renamed from: e  reason: collision with root package name */
    private final String f6696e;

    /* renamed from: f  reason: collision with root package name */
    private final String f6697f;

    private q(String str, String str2, String str3, String str4) {
        this.f6694c = str;
        this.f6695d = str2;
        this.f6696e = str3;
        this.f6697f = str4;
    }

    public static q a(String str) {
        String group;
        Matcher matcher = f6692a.matcher(str);
        if (!matcher.lookingAt()) {
            return null;
        }
        String lowerCase = matcher.group(1).toLowerCase(Locale.US);
        String lowerCase2 = matcher.group(2).toLowerCase(Locale.US);
        Matcher matcher2 = f6693b.matcher(str);
        String str2 = null;
        for (int end = matcher.end(); end < str.length(); end = matcher2.end()) {
            matcher2.region(end, str.length());
            if (!matcher2.lookingAt()) {
                return null;
            }
            String group2 = matcher2.group(1);
            if (group2 != null && group2.equalsIgnoreCase("charset")) {
                if (matcher2.group(2) != null) {
                    group = matcher2.group(2);
                } else {
                    group = matcher2.group(3);
                }
                if (str2 == null || group.equalsIgnoreCase(str2)) {
                    str2 = group;
                } else {
                    throw new IllegalArgumentException("Multiple different charsets: " + str);
                }
            }
        }
        return new q(str, lowerCase, lowerCase2, str2);
    }

    public Charset a() {
        if (this.f6697f != null) {
            return Charset.forName(this.f6697f);
        }
        return null;
    }

    public Charset a(Charset charset) {
        return this.f6697f != null ? Charset.forName(this.f6697f) : charset;
    }

    public String toString() {
        return this.f6694c;
    }

    public boolean equals(Object obj) {
        return (obj instanceof q) && ((q) obj).f6694c.equals(this.f6694c);
    }

    public int hashCode() {
        return this.f6694c.hashCode();
    }
}
