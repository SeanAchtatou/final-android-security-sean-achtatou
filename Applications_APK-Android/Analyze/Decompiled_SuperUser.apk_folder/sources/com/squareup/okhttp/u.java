package com.squareup.okhttp;

import com.squareup.okhttp.internal.a.j;
import com.squareup.okhttp.o;
import java.util.Collections;
import java.util.List;

/* compiled from: Response */
public final class u {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final s f6724a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Protocol f6725b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final int f6726c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final String f6727d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final n f6728e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final o f6729f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public final v f6730g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public u f6731h;
    /* access modifiers changed from: private */
    public u i;
    /* access modifiers changed from: private */
    public final u j;
    private volatile d k;

    private u(a aVar) {
        this.f6724a = aVar.f6732a;
        this.f6725b = aVar.f6733b;
        this.f6726c = aVar.f6734c;
        this.f6727d = aVar.f6735d;
        this.f6728e = aVar.f6736e;
        this.f6729f = aVar.f6737f.a();
        this.f6730g = aVar.f6738g;
        this.f6731h = aVar.f6739h;
        this.i = aVar.i;
        this.j = aVar.j;
    }

    public s a() {
        return this.f6724a;
    }

    public Protocol b() {
        return this.f6725b;
    }

    public int c() {
        return this.f6726c;
    }

    public boolean d() {
        return this.f6726c >= 200 && this.f6726c < 300;
    }

    public n e() {
        return this.f6728e;
    }

    public String a(String str) {
        return a(str, null);
    }

    public String a(String str, String str2) {
        String a2 = this.f6729f.a(str);
        return a2 != null ? a2 : str2;
    }

    public o f() {
        return this.f6729f;
    }

    public v g() {
        return this.f6730g;
    }

    public a h() {
        return new a();
    }

    public List<h> i() {
        String str;
        if (this.f6726c == 401) {
            str = "WWW-Authenticate";
        } else if (this.f6726c != 407) {
            return Collections.emptyList();
        } else {
            str = "Proxy-Authenticate";
        }
        return j.b(f(), str);
    }

    public d j() {
        d dVar = this.k;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f6729f);
        this.k = a2;
        return a2;
    }

    public String toString() {
        return "Response{protocol=" + this.f6725b + ", code=" + this.f6726c + ", message=" + this.f6727d + ", url=" + this.f6724a.c() + '}';
    }

    /* compiled from: Response */
    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public s f6732a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public Protocol f6733b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public int f6734c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public String f6735d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public n f6736e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public o.a f6737f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public v f6738g;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public u f6739h;
        /* access modifiers changed from: private */
        public u i;
        /* access modifiers changed from: private */
        public u j;

        public a() {
            this.f6734c = -1;
            this.f6737f = new o.a();
        }

        private a(u uVar) {
            this.f6734c = -1;
            this.f6732a = uVar.f6724a;
            this.f6733b = uVar.f6725b;
            this.f6734c = uVar.f6726c;
            this.f6735d = uVar.f6727d;
            this.f6736e = uVar.f6728e;
            this.f6737f = uVar.f6729f.b();
            this.f6738g = uVar.f6730g;
            this.f6739h = uVar.f6731h;
            this.i = uVar.i;
            this.j = uVar.j;
        }

        public a a(s sVar) {
            this.f6732a = sVar;
            return this;
        }

        public a a(Protocol protocol) {
            this.f6733b = protocol;
            return this;
        }

        public a a(int i2) {
            this.f6734c = i2;
            return this;
        }

        public a a(String str) {
            this.f6735d = str;
            return this;
        }

        public a a(n nVar) {
            this.f6736e = nVar;
            return this;
        }

        public a a(String str, String str2) {
            this.f6737f.b(str, str2);
            return this;
        }

        public a b(String str, String str2) {
            this.f6737f.a(str, str2);
            return this;
        }

        public a a(o oVar) {
            this.f6737f = oVar.b();
            return this;
        }

        public a a(v vVar) {
            this.f6738g = vVar;
            return this;
        }

        public a a(u uVar) {
            if (uVar != null) {
                a("networkResponse", uVar);
            }
            this.f6739h = uVar;
            return this;
        }

        public a b(u uVar) {
            if (uVar != null) {
                a("cacheResponse", uVar);
            }
            this.i = uVar;
            return this;
        }

        private void a(String str, u uVar) {
            if (uVar.f6730g != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (uVar.f6731h != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (uVar.i != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (uVar.j != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        public a c(u uVar) {
            if (uVar != null) {
                d(uVar);
            }
            this.j = uVar;
            return this;
        }

        private void d(u uVar) {
            if (uVar.f6730g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        public u a() {
            if (this.f6732a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f6733b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.f6734c >= 0) {
                return new u(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.f6734c);
            }
        }
    }
}
