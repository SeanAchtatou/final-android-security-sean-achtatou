package com.squareup.okhttp;

import com.squareup.okhttp.internal.h;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

/* compiled from: Handshake */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    private final String f6687a;

    /* renamed from: b  reason: collision with root package name */
    private final List<Certificate> f6688b;

    /* renamed from: c  reason: collision with root package name */
    private final List<Certificate> f6689c;

    private n(String str, List<Certificate> list, List<Certificate> list2) {
        this.f6687a = str;
        this.f6688b = list;
        this.f6689c = list2;
    }

    public static n a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        List emptyList;
        List emptyList2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        }
        try {
            certificateArr = sSLSession.getPeerCertificates();
        } catch (SSLPeerUnverifiedException e2) {
            certificateArr = null;
        }
        if (certificateArr != null) {
            emptyList = h.a(certificateArr);
        } else {
            emptyList = Collections.emptyList();
        }
        Certificate[] localCertificates = sSLSession.getLocalCertificates();
        if (localCertificates != null) {
            emptyList2 = h.a(localCertificates);
        } else {
            emptyList2 = Collections.emptyList();
        }
        return new n(cipherSuite, emptyList, emptyList2);
    }

    public String a() {
        return this.f6687a;
    }

    public List<Certificate> b() {
        return this.f6688b;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        if (!this.f6687a.equals(nVar.f6687a) || !this.f6688b.equals(nVar.f6688b) || !this.f6689c.equals(nVar.f6689c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((this.f6687a.hashCode() + 527) * 31) + this.f6688b.hashCode()) * 31) + this.f6689c.hashCode();
    }
}
