package com.squareup.okhttp;

import com.squareup.okhttp.internal.h;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLPeerUnverifiedException;
import okio.ByteString;

/* compiled from: CertificatePinner */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final g f6381a = new a().a();

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, List<ByteString>> f6382b;

    private g(a aVar) {
        this.f6382b = h.a(aVar.f6383a);
    }

    public void a(String str, List<Certificate> list) {
        List list2 = this.f6382b.get(str);
        if (list2 != null) {
            int size = list.size();
            int i = 0;
            while (i < size) {
                if (!list2.contains(a((X509Certificate) list.get(i)))) {
                    i++;
                } else {
                    return;
                }
            }
            StringBuilder append = new StringBuilder().append("Certificate pinning failure!").append("\n  Peer certificate chain:");
            int size2 = list.size();
            for (int i2 = 0; i2 < size2; i2++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i2);
                append.append("\n    ").append(a((Certificate) x509Certificate)).append(": ").append(x509Certificate.getSubjectDN().getName());
            }
            append.append("\n  Pinned certificates for ").append(str).append(":");
            int size3 = list2.size();
            for (int i3 = 0; i3 < size3; i3++) {
                append.append("\n    sha1/").append(((ByteString) list2.get(i3)).b());
            }
            throw new SSLPeerUnverifiedException(append.toString());
        }
    }

    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha1/" + a((X509Certificate) certificate).b();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    private static ByteString a(X509Certificate x509Certificate) {
        return h.a(ByteString.a(x509Certificate.getPublicKey().getEncoded()));
    }

    /* compiled from: CertificatePinner */
    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Map<String, List<ByteString>> f6383a = new LinkedHashMap();

        public g a() {
            return new g(this);
        }
    }
}
