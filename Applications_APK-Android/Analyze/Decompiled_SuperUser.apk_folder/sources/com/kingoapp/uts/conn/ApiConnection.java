package com.kingoapp.uts.conn;

import com.kingoapp.uts.model.Result;
import com.squareup.okhttp.q;
import com.squareup.okhttp.r;
import com.squareup.okhttp.s;
import com.squareup.okhttp.t;
import com.squareup.okhttp.u;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class ApiConnection {
    private static final String CONTENT_TYPE_LABEL = "Content-Type";
    private static final String CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8";
    private static final String TAG = "ApiConnection";
    private byte[] bodyJson;
    private r httpClient = createHttpClient();
    private Result response;
    private URL url;

    private ApiConnection(String str) {
        this.url = new URL(str);
    }

    public static ApiConnection createApiConn(String str) {
        return new ApiConnection(str);
    }

    public Result requestSyncCall(String str, byte[] bArr) {
        this.bodyJson = bArr;
        if ("GET".equals(str)) {
            connectToGetApi();
        } else {
            connectToPostApi();
        }
        return this.response;
    }

    private void connectToGetApi() {
        executor(createRequest("GET", null));
    }

    private void connectToPostApi() {
        executor(createRequest("POST", this.bodyJson));
    }

    private s createRequest(String str, byte[] bArr) {
        if (str.equals("GET")) {
            return new s.a().a(this.url).a().b(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON).b();
        }
        return new s.a().a(this.url).a(t.a(q.a(CONTENT_TYPE_VALUE_JSON), bArr)).b(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON).b("App", "kingoroot").b();
    }

    private void executor(s sVar) {
        try {
            u a2 = this.httpClient.a(sVar).a();
            this.response = new Result(a2.c(), a2.g().d());
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private r createHttpClient() {
        r rVar = new r();
        rVar.a(50, TimeUnit.SECONDS);
        rVar.b(50, TimeUnit.SECONDS);
        return rVar;
    }
}
