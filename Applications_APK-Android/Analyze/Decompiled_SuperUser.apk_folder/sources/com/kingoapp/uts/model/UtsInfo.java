package com.kingoapp.uts.model;

import android.os.Build;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.lang.reflect.Array;

public class UtsInfo {
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("data")
    @Expose
    public Object[][] datas = ((Object[][]) Array.newInstance(Object.class, 1, 6));
    @SerializedName("model_id")
    @Expose
    private String modelID = Build.MODEL;
    @SerializedName("pid")
    @Expose
    private String pId = "com.kingoapp.root";
    @SerializedName("time")
    @Expose
    private long[] times = {System.currentTimeMillis() / 1000};
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("version_code")
    @Expose
    private String versionCode;

    public void setChannel(String str) {
        this.channel = str;
    }

    public void setVersionCode(String str) {
        this.versionCode = str;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public void setpId(String str) {
        this.pId = str;
    }
}
