package com.kingoapp.uts.model;

import android.os.Build;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlugInfo {
    @SerializedName("android_brand")
    @Expose
    private String androidBrand = Build.BRAND;
    @SerializedName("android_sdk")
    @Expose
    private int androidSdk = Build.VERSION.SDK_INT;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("faild_message")
    @Expose
    private String faildMessage;
    @SerializedName("is_system")
    @Expose
    private boolean isSystem;
    @SerializedName("pid")
    @Expose
    private String pId = "com.kingoapp.root";
    @SerializedName("phone_device")
    @Expose
    private String phoneDevice = Build.DEVICE;
    @SerializedName("phone_id")
    @Expose
    private String phoneId = Build.ID;
    @SerializedName("phone_model")
    @Expose
    private String phoneModel = Build.MODEL;
    @SerializedName("plug_id")
    @Expose
    private String plugId;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public void setPlugId(String str) {
        this.plugId = str;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public void setChannel(String str) {
        this.channel = str;
    }

    public void setpId(String str) {
        this.pId = str;
    }

    public void setSystem(boolean z) {
        this.isSystem = z;
    }

    public void setFaildMessage(String str) {
        this.faildMessage = str;
    }
}
