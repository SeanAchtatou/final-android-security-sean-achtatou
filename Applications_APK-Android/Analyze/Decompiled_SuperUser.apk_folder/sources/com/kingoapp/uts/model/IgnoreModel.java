package com.kingoapp.uts.model;

import java.util.ArrayList;
import java.util.List;

public class IgnoreModel {
    private List<String> ignoreList = new ArrayList();
    private int is_ignore;

    public int getIs_ignore() {
        return this.is_ignore;
    }

    public void setIs_ignore(int i) {
        this.is_ignore = i;
    }

    public List<String> getIgnoreList() {
        return this.ignoreList;
    }

    public void setIgnoreList(List<String> list) {
        this.ignoreList = list;
    }
}
