package com.kingoapp.uts.model;

import com.duapps.ad.AdError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PhoneInfo implements Serializable {
    public String SDK_version;
    private List<String> apps = new ArrayList();
    public String channel_name;
    public long current_time;
    public String device_name;
    public String finger_Md5;
    public long first_install_time;
    private int flag = AdError.NETWORK_ERROR_CODE;
    public String ip;
    public boolean is_net_work_roaming;
    public boolean is_system_app;
    public String local_country;
    public String local_language;
    public String manu_facture;
    public String net_type;
    public String package_name;
    public String pub_key;
    public String sign_name;
    public String sign_number;
    public String subject_dn;
    public String subscriber_id;
    public String system_version;
    private String userId;
    public int version_code;
    public String version_name;

    public void setApps(List<String> list) {
        this.apps = list;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public void setFlag(int i) {
        this.flag = i;
    }

    public String toString() {
        return "current_time:" + this.current_time + " , channel_name:" + this.channel_name + " , version_name:" + this.version_name + " , version_code:" + this.version_code + " , package_name:" + this.package_name + " , device_name:" + this.device_name + " , manu_facture:" + this.manu_facture + " , SDK_version:" + this.SDK_version + " , system_version:" + this.system_version + " , net_type:" + this.net_type + " , ip:" + this.ip + " , subscriber_id:" + this.subscriber_id + " , is_net_work_roaming:" + this.is_net_work_roaming + " , local_language:" + this.local_language + " , local_country:" + this.local_country + " , is_system_app:" + this.is_system_app + " , sign_name:" + this.sign_name + " , pub_key:" + this.pub_key + " , sign_number:" + this.sign_number + " , subject_dn:" + this.subject_dn + " , finger_Md5:" + this.finger_Md5 + " , first_install_time:" + this.first_install_time;
    }
}
