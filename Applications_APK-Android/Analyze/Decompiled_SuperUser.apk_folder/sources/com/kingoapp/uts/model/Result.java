package com.kingoapp.uts.model;

public class Result {
    private int code;
    private byte[] result;

    public Result(int i, byte[] bArr) {
        this.code = i;
        this.result = bArr;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int i) {
        this.code = i;
    }

    public byte[] getResult() {
        return this.result;
    }

    public void setResult(byte[] bArr) {
        this.result = bArr;
    }

    public String toString() {
        return "Response{code=" + this.code + ", result='" + this.result + '\'' + '}';
    }
}
