package com.kingoapp.uts.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class JobExecutor implements Executor {
    private static final int CORE_SIZE = count;
    private static JobExecutor INSTANCE = null;
    private static final int KEEP_ALIVE_TIME = 5;
    private static final int MAX_SIZE = (count + 2);
    private static final TimeUnit UNIT = TimeUnit.SECONDS;
    private static final int count = ((Runtime.getRuntime().availableProcessors() * 3) + 2);
    private final BlockingQueue<Runnable> blockingDeque = new LinkedBlockingQueue(5);
    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(CORE_SIZE, MAX_SIZE, 5, UNIT, this.blockingDeque, this.threadFactory);
    private final CustomerThreadFactory threadFactory = new CustomerThreadFactory();

    private JobExecutor() {
    }

    public static JobExecutor newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JobExecutor();
        }
        return INSTANCE;
    }

    public void execute(Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException("task must be can't be null");
        } else if (!this.executor.isShutdown()) {
            this.executor.execute(runnable);
        }
    }

    public void shutdownAllNow() {
        this.executor.shutdownNow();
    }

    private static class CustomerThreadFactory implements ThreadFactory {
        private static String THREAD_NAME = "xpp_";
        private int counter;

        private CustomerThreadFactory() {
            this.counter = 0;
        }

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, THREAD_NAME + this.counter);
        }
    }
}
