package com.kingoapp.uts.builder;

import com.kingoapp.uts.model.UtsInfo;

public class UtsInfoBuilder {
    private static UtsInfo utsInfo = new UtsInfo();

    public static void setMainMenu(String str) {
        utsInfo.datas[0][1] = str;
    }

    public static void setChildMenu(String str) {
        utsInfo.datas[0][2] = str;
    }

    public static void setEvent(String str) {
        utsInfo.datas[0][3] = str;
    }

    public static void setUserId(String str) {
        utsInfo.setUserId(str);
    }

    public static void setChannel(String str) {
        utsInfo.setChannel(str);
    }

    public static void setVersionCode(String str) {
        utsInfo.setVersionCode(str);
    }

    public static void setPId(String str) {
        utsInfo.setpId(str);
    }

    public static UtsInfo builder() {
        utsInfo.datas[0][0] = Long.valueOf(System.currentTimeMillis());
        return utsInfo;
    }
}
