package com.kingoapp.uts;

import android.util.Log;
import com.google.gson.Gson;
import com.kingoapp.uts.api.UtsApi;
import com.kingoapp.uts.model.PlugInfo;
import io.reactivex.a.d;

public class PlugPresenter {
    private static PlugPresenter INSTANCE = new PlugPresenter();
    public static final String TAG = "uts";

    public static PlugPresenter newInstance() {
        return INSTANCE;
    }

    public void pushInfo(PlugInfo plugInfo) {
        new UtsApi().pushPlugInfo(new Gson().toJson(plugInfo)).a(new d<Boolean>() {
            public void accept(Boolean bool) {
                if (bool.booleanValue()) {
                    Log.i("uts", "success");
                } else {
                    Log.i("uts", "failed");
                }
            }
        }, new d<Throwable>() {
            public void accept(Throwable th) {
                Log.e("uts", th.getMessage() + "");
            }
        });
    }
}
