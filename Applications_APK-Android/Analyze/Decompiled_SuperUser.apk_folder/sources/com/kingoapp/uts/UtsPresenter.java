package com.kingoapp.uts;

import android.util.Log;
import com.google.gson.Gson;
import com.kingoapp.uts.api.UtsApi;
import com.kingoapp.uts.model.UtsInfo;
import io.reactivex.a.d;

public class UtsPresenter {
    private static UtsPresenter INSTANCE = new UtsPresenter();
    public static final String TAG = "uts";

    private UtsPresenter() {
    }

    public static UtsPresenter newInstance() {
        return INSTANCE;
    }

    public void pushInfo(UtsInfo utsInfo) {
        new UtsApi().pushInfo(new Gson().toJson(utsInfo)).a(new d<Boolean>() {
            public void accept(Boolean bool) {
                if (bool.booleanValue()) {
                    Log.i("uts", "success");
                } else {
                    Log.i("uts", "failed");
                }
            }
        }, new d<Throwable>() {
            public void accept(Throwable th) {
                Log.e("uts", th.getMessage() + "");
            }
        });
    }
}
