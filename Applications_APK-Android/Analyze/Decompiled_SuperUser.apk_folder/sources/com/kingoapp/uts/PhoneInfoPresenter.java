package com.kingoapp.uts;

import android.content.Context;
import android.util.Log;
import com.duapps.ad.AdError;
import com.google.gson.Gson;
import com.kingoapp.uts.api.UtsApi;
import com.kingoapp.uts.model.PhoneInfo;
import com.kingoapp.uts.util.DeviceIdGenerator;
import com.kingoapp.uts.util.PhoneInfoUtils;
import io.reactivex.a.d;
import java.util.List;

public class PhoneInfoPresenter {
    private static PhoneInfoPresenter INSTANCE = null;
    public static final String TAG = "PhoneInfoPresenter";
    private String channelName;
    private Context context;
    private Gson gson = new Gson();

    private PhoneInfoPresenter(Context context2, String str) {
        this.context = context2;
        this.channelName = str;
    }

    public static PhoneInfoPresenter getInstance(Context context2, String str) {
        if (INSTANCE == null) {
            INSTANCE = new PhoneInfoPresenter(context2, str);
        }
        return INSTANCE;
    }

    private PhoneInfo getReportInfo() {
        PhoneInfo phoneInfo = new PhoneInfo();
        phoneInfo.setUserId(DeviceIdGenerator.readDeviceId(this.context));
        phoneInfo.setApps(PhoneInfoUtils.getInstalledApp(this.context));
        phoneInfo.current_time = PhoneInfoUtils.getCurrentTime();
        phoneInfo.channel_name = this.channelName;
        phoneInfo.version_name = PhoneInfoUtils.getVersionName(this.context);
        phoneInfo.version_code = PhoneInfoUtils.getVersionCode(this.context);
        phoneInfo.package_name = PhoneInfoUtils.getPackageName(this.context);
        phoneInfo.device_name = PhoneInfoUtils.getDeviceName();
        phoneInfo.manu_facture = PhoneInfoUtils.getManuFacture();
        phoneInfo.SDK_version = PhoneInfoUtils.getSDKVersion();
        phoneInfo.system_version = PhoneInfoUtils.getSystemVersion();
        phoneInfo.net_type = PhoneInfoUtils.getNetType(this.context);
        phoneInfo.ip = PhoneInfoUtils.getIP(this.context);
        phoneInfo.subscriber_id = PhoneInfoUtils.getSubscriberId(this.context);
        phoneInfo.is_net_work_roaming = PhoneInfoUtils.isNetworkRoaming(this.context);
        phoneInfo.local_language = PhoneInfoUtils.getLocalLanguage();
        phoneInfo.local_country = PhoneInfoUtils.getLocalCountry();
        phoneInfo.is_system_app = PhoneInfoUtils.isSystemApp(this.context);
        phoneInfo.sign_name = PhoneInfoUtils.getSignName(this.context);
        phoneInfo.pub_key = PhoneInfoUtils.getPubKey(this.context);
        phoneInfo.sign_number = PhoneInfoUtils.getSingnNumber(this.context);
        phoneInfo.subject_dn = PhoneInfoUtils.getSubjectDn(this.context);
        phoneInfo.finger_Md5 = PhoneInfoUtils.getFingerMd5(this.context);
        phoneInfo.first_install_time = PhoneInfoUtils.getFirstInstallTime(this.context);
        return phoneInfo;
    }

    public void pushPhoneInfo() {
        new UtsApi().pushInfo(new Gson().toJson(getReportInfo())).a(new d<Boolean>() {
            public void accept(Boolean bool) {
                if (bool.booleanValue()) {
                    Log.i(PhoneInfoPresenter.TAG, "success");
                } else {
                    Log.i(PhoneInfoPresenter.TAG, "failed");
                }
            }
        }, new d<Throwable>() {
            public void accept(Throwable th) {
                Log.e(PhoneInfoPresenter.TAG, th.getMessage() + "");
            }
        });
    }

    public void pushIgnoreList(List<String> list) {
        PhoneInfo phoneInfo = new PhoneInfo();
        phoneInfo.setFlag(AdError.NO_FILL_ERROR_CODE);
        phoneInfo.setUserId(DeviceIdGenerator.readDeviceId(this.context));
        phoneInfo.setApps(list);
        new UtsApi().pushInfo(this.gson.toJson(phoneInfo)).a(new d<Boolean>() {
            public void accept(Boolean bool) {
                if (bool.booleanValue()) {
                    Log.i(PhoneInfoPresenter.TAG, "success");
                } else {
                    Log.i(PhoneInfoPresenter.TAG, "failed");
                }
            }
        }, new d<Throwable>() {
            public void accept(Throwable th) {
                Log.e(PhoneInfoPresenter.TAG, th.getMessage() + "");
            }
        });
    }
}
