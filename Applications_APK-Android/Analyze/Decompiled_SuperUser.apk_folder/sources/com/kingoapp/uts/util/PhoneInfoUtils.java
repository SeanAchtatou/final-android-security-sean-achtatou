package com.kingoapp.uts.util;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class PhoneInfoUtils {
    public static final int NETWORK_TYPE_2G = 1;
    public static final int NETWORK_TYPE_3G = 2;
    public static final int NETWORK_TYPE_MOBILE = 3;
    public static final int NETWORK_TYPE_NONE = 0;
    public static final int NETWORK_TYPE_OTHER = 5;
    public static final int NETWORK_TYPE_WIFI = 4;
    public static final String PUB_KEY = "pubKey";
    public static final String SINGN_NAME = "signName";
    public static final String SINGN_NUMBER = "signNumber";
    public static final String SUBJECT_DN = "subjectDN";

    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            return "";
        }
    }

    public static int getVersionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionCode;
        } catch (Exception e2) {
            return 0;
        }
    }

    public static String getPackageName(Context context) {
        try {
            return context.getPackageName();
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getDeviceName() {
        try {
            return BluetoothAdapter.getDefaultAdapter().getName();
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getManuFacture() {
        try {
            return Build.MANUFACTURER;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getSDKVersion() {
        try {
            return Build.VERSION.SDK;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getSystemVersion() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getImei(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getNetType(Context context) {
        try {
            int netWorkType = getNetWorkType(context);
            if (netWorkType == 0) {
                return "not_net";
            }
            if (1 == netWorkType) {
                return "2G";
            }
            if (2 == netWorkType) {
                return "3G";
            }
            if (3 == netWorkType) {
                return "mobile";
            }
            if (4 == netWorkType) {
                return "wifi";
            }
            return "other";
        } catch (Exception e2) {
            return "not_net";
        }
    }

    public static int getNetWorkType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return 0;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return 0;
        }
        if (activeNetworkInfo.getType() == 0) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                return 3;
            }
            switch (telephonyManager.getNetworkType()) {
                case 1:
                case 2:
                    return 1;
                case 3:
                case 8:
                case 9:
                case 10:
                    return 2;
                case 4:
                case 5:
                case 6:
                case 7:
                default:
                    return 3;
            }
        } else if (1 == activeNetworkInfo.getType()) {
            return 4;
        } else {
            return 5;
        }
    }

    public static String getIP(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (!wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(true);
            }
            return formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        } catch (Exception e2) {
            return "";
        }
    }

    public static String formatIpAddress(int i) {
        return (i & VUserInfo.FLAG_MASK_USER_TYPE) + "." + ((i >> 8) & VUserInfo.FLAG_MASK_USER_TYPE) + "." + ((i >> 16) & VUserInfo.FLAG_MASK_USER_TYPE) + "." + ((i >> 24) & VUserInfo.FLAG_MASK_USER_TYPE);
    }

    public static String getSubscriberId(Context context) {
        try {
            String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
            if (!TextUtils.isEmpty(subscriberId)) {
                return subscriberId;
            }
            return "";
        } catch (Exception e2) {
            return "";
        }
    }

    public static boolean isNetworkRoaming(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).isNetworkRoaming();
        } catch (Exception e2) {
            return false;
        }
    }

    public static String getLocalLanguage() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getLocalCountry() {
        try {
            return Locale.getDefault().getCountry();
        } catch (Exception e2) {
            return "";
        }
    }

    public static boolean isRegistMultipleActions(Context context, String str) {
        int i;
        try {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
            int i2 = 0;
            int i3 = 0;
            while (i2 < queryIntentActivities.size()) {
                ResolveInfo resolveInfo = queryIntentActivities.get(i2);
                ActivityInfo activityInfo = resolveInfo.activityInfo;
                IntentFilter intentFilter = resolveInfo.filter;
                if (intentFilter != null) {
                    Iterator<String> actionsIterator = intentFilter.actionsIterator();
                    i = i3;
                    while (actionsIterator.hasNext()) {
                        i++;
                    }
                } else {
                    i = i3;
                }
                i2++;
                i3 = i;
            }
            if (i3 > 1) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean isSystemApp(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).applicationInfo;
            if ((applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) == 0 && (applicationInfo.flags & 1) != 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public static String getSignName(Context context) {
        try {
            return getSingnInfo(context, SINGN_NAME);
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getPubKey(Context context) {
        try {
            return getSingnInfo(context, PUB_KEY);
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getSingnNumber(Context context) {
        try {
            return getSingnInfo(context, SINGN_NUMBER);
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getSubjectDn(Context context) {
        try {
            return getSingnInfo(context, SUBJECT_DN);
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getFingerMd5(Context context) {
        try {
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures;
            if (0 >= signatureArr.length) {
                return "";
            }
            Signature signature = signatureArr[0];
            MessageDigest instance = MessageDigest.getInstance("SHA");
            instance.update(signature.toByteArray());
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            int length = digest.length;
            for (int i = 0; i < length; i++) {
                sb.append(String.format("%02x", Integer.valueOf(digest[i] & 255)));
            }
            MessageDigest instance2 = MessageDigest.getInstance("MD5");
            instance2.update(signature.toByteArray());
            byte[] digest2 = instance2.digest();
            StringBuilder sb2 = new StringBuilder();
            int length2 = digest2.length;
            for (int i2 = 0; i2 < length2; i2++) {
                sb2.append(String.format("%02x", Integer.valueOf(digest2[i2] & 255)));
            }
            return sb2.toString();
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getSingnInfo(Context context, String str) {
        try {
            return parseSignature(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray(), str);
        } catch (Exception e2) {
            return "";
        }
    }

    public static String parseSignature(byte[] bArr, String str) {
        try {
            X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr));
            if (SINGN_NAME.equalsIgnoreCase(str)) {
                return x509Certificate.getSigAlgName();
            }
            if (PUB_KEY.equalsIgnoreCase(str)) {
                return x509Certificate.getPublicKey().toString();
            }
            if (SINGN_NUMBER.equalsIgnoreCase(str)) {
                return x509Certificate.getSerialNumber().toString();
            }
            if (SUBJECT_DN.equalsIgnoreCase(str)) {
                return x509Certificate.getSubjectDN().toString();
            }
            return "";
        } catch (CertificateException e2) {
        }
    }

    public static long getFirstInstallTime(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
        } catch (Exception e2) {
            return 0;
        }
    }

    public static List<String> getInstalledApp(Context context) {
        int i = 0;
        ArrayList arrayList = new ArrayList();
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        while (true) {
            int i2 = i;
            if (i2 >= installedPackages.size()) {
                return arrayList;
            }
            PackageInfo packageInfo = installedPackages.get(i2);
            if ((packageInfo.applicationInfo.flags & 1) == 0) {
                arrayList.add(packageInfo.packageName);
            }
            i = i2 + 1;
        }
    }
}
