package com.kingoapp.uts.util;

import android.util.Log;

public class LogUtils {
    static boolean isdebug = true;
    static String tag = "wyy";

    public static void d(String str, String str2) {
        if (isdebug) {
            Log.d(str, str2);
        }
    }

    public static void d(String str) {
        if (isdebug) {
            Log.d(tag, str);
        }
    }

    public static void e(String str) {
        if (isdebug) {
            Log.e(tag, str);
        }
    }

    public static void e(String str, String str2) {
        if (isdebug) {
            Log.e(str, str2);
        }
    }

    public static void i(String str, String str2) {
        if (isdebug) {
            Log.i(str, str2);
        }
    }
}
