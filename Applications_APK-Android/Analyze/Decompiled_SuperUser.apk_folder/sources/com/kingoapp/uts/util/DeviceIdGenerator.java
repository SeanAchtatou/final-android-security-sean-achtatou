package com.kingoapp.uts.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.UUID;

public class DeviceIdGenerator {
    private static final String[] BAD_SERIAL_PATTERNS = {"1234567", "abcdef", "dead00beef"};
    private static final String EMULATOR_ANDROID_ID = "9774d56d682e549c";

    public static String readDeviceId(Context context) {
        String str = null;
        try {
            str = Build.SERIAL;
        } catch (NoSuchFieldError e2) {
        }
        if (TextUtils.isEmpty(str) || "unknown".equals(str) || isBadSerial(str)) {
            str = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (TextUtils.isEmpty(str) || EMULATOR_ANDROID_ID.equals(str) || isBadDeviceId(str) || str.length() != EMULATOR_ANDROID_ID.length()) {
                str = SoftInstallationId.id(context);
            }
        }
        return UUID.nameUUIDFromBytes(str.getBytes()).toString();
    }

    private static boolean isBadDeviceId(String str) {
        return TextUtils.isEmpty(str) || TextUtils.isEmpty(str.replace('0', ' ').replace('-', ' ').trim());
    }

    private static boolean isBadSerial(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        String lowerCase = str.toLowerCase();
        for (String contains : BAD_SERIAL_PATTERNS) {
            if (lowerCase.contains(contains)) {
                return true;
            }
        }
        return false;
    }

    private static class SoftInstallationId {
        private static final String INSTALLATION = "INSTALLATION";
        private static String sID = null;

        private SoftInstallationId() {
        }

        public static synchronized String id(Context context) {
            String str;
            synchronized (SoftInstallationId.class) {
                if (sID == null) {
                    File file = new File(context.getFilesDir(), INSTALLATION);
                    try {
                        if (!file.exists()) {
                            writeInstallationFile(file);
                        }
                        sID = readInstallationFile(file);
                    } catch (Exception e2) {
                        throw new RuntimeException(e2);
                    }
                }
                str = sID;
            }
            return str;
        }

        /* JADX INFO: finally extract failed */
        private static String readInstallationFile(File file) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            try {
                byte[] bArr = new byte[((int) randomAccessFile.length())];
                randomAccessFile.readFully(bArr);
                randomAccessFile.close();
                String str = new String(bArr);
                randomAccessFile.close();
                return str;
            } catch (Throwable th) {
                randomAccessFile.close();
                throw th;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static void writeInstallationFile(java.io.File r3) {
            /*
                r2 = 0
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x001b }
                r1.<init>(r3)     // Catch:{ all -> 0x001b }
                java.util.UUID r0 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x0023 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0023 }
                byte[] r0 = r0.getBytes()     // Catch:{ all -> 0x0023 }
                r1.write(r0)     // Catch:{ all -> 0x0023 }
                if (r1 == 0) goto L_0x001a
                r1.close()
            L_0x001a:
                return
            L_0x001b:
                r0 = move-exception
                r1 = r2
            L_0x001d:
                if (r1 == 0) goto L_0x0022
                r1.close()
            L_0x0022:
                throw r0
            L_0x0023:
                r0 = move-exception
                goto L_0x001d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.kingoapp.uts.util.DeviceIdGenerator.SoftInstallationId.writeInstallationFile(java.io.File):void");
        }
    }
}
