package com.kingoapp.uts.util;

import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserInfo;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RC4Util {
    public static String RC4(String str, String str2) {
        char[] cArr = new char[FileUtils.FileMode.MODE_IRUSR];
        char[] cArr2 = new char[FileUtils.FileMode.MODE_IRUSR];
        for (int i = 0; i < 256; i++) {
            cArr[i] = str.charAt(i % str.length());
            cArr2[i] = (char) i;
        }
        char c2 = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            c2 = (c2 + cArr2[i2] + cArr[i2]) & 255;
            char c3 = cArr2[i2];
            cArr2[i2] = cArr2[c2];
            cArr2[c2] = c3;
        }
        int i3 = 0;
        char c4 = 0;
        String str3 = "";
        for (int i4 = 0; i4 < str2.length(); i4++) {
            i3 = (i3 + 1) & VUserInfo.FLAG_MASK_USER_TYPE;
            char c5 = cArr2[i3];
            c4 = (c4 + c5) & 255;
            char c6 = (char) ((cArr2[c4] + c5) & VUserInfo.FLAG_MASK_USER_TYPE);
            cArr2[i3] = cArr2[c4];
            cArr2[c4] = c5;
            try {
                str3 = str3 + new String(new char[]{(char) (cArr2[c6] ^ str2.charAt(i4))});
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return str3;
    }

    public static String decry_RC4(byte[] bArr, String str) {
        if (bArr == null || str == null) {
            return null;
        }
        return asString(RC4Base(bArr, str));
    }

    public static String decry_RC4(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        return new String(RC4Base(HexString2Bytes(str), str2));
    }

    public static byte[] encry_RC4_byte(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        return RC4Base(str.getBytes(), str2);
    }

    public static String encry_RC4_string(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        return toHexString(asString(encry_RC4_byte(str, str2)));
    }

    private static String asString(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(bArr.length);
        for (byte b2 : bArr) {
            stringBuffer.append((char) b2);
        }
        return stringBuffer.toString();
    }

    private static byte[] initKey(String str) {
        byte[] bytes = str.getBytes();
        byte[] bArr = new byte[FileUtils.FileMode.MODE_IRUSR];
        for (int i = 0; i < 256; i++) {
            bArr[i] = (byte) i;
        }
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < 256; i4++) {
            i2 = (i2 + (bytes[i3] & 255) + (bArr[i4] & 255)) & VUserInfo.FLAG_MASK_USER_TYPE;
            byte b2 = bArr[i4];
            bArr[i4] = bArr[i2];
            bArr[i2] = b2;
            i3 = (i3 + 1) % bytes.length;
        }
        return bArr;
    }

    public static String toHexString(String str) {
        String str2 = "";
        for (int i = 0; i < str.length(); i++) {
            String hexString = Integer.toHexString(str.charAt(i) & 255);
            if (hexString.length() == 1) {
                hexString = '0' + hexString;
            }
            str2 = str2 + hexString;
        }
        return str2;
    }

    public static byte[] HexString2Bytes(String str) {
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        byte[] bytes = str.getBytes();
        for (int i = 0; i < length / 2; i++) {
            bArr[i] = uniteBytes(bytes[i * 2], bytes[(i * 2) + 1]);
        }
        return bArr;
    }

    private static byte uniteBytes(byte b2, byte b3) {
        return (byte) (((char) (((char) Byte.decode("0x" + new String(new byte[]{b2})).byteValue()) << 4)) ^ ((char) Byte.decode("0x" + new String(new byte[]{b3})).byteValue()));
    }

    private static byte[] RC4Base(byte[] bArr, String str) {
        byte[] initKey = initKey(str);
        byte[] bArr2 = new byte[bArr.length];
        byte b2 = 0;
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i = (i + 1) & VUserInfo.FLAG_MASK_USER_TYPE;
            b2 = (b2 + (initKey[i] & 255)) & 255;
            byte b3 = initKey[i];
            initKey[i] = initKey[b2];
            initKey[b2] = b3;
            bArr2[i2] = (byte) (initKey[((initKey[i] & 255) + (initKey[b2] & 255)) & VUserInfo.FLAG_MASK_USER_TYPE] ^ bArr[i2]);
        }
        return bArr2;
    }

    public static String getKeyMD5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("md5");
            instance.update(str.getBytes("utf-8"));
            return new String(instance.digest());
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public static void main(String[] strArr) {
        System.out.println(getKeyMD5("cpy"));
    }
}
