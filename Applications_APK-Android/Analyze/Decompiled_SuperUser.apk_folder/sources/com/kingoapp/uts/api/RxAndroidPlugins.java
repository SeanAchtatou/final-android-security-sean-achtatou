package com.kingoapp.uts.api;

import io.reactivex.a.e;
import io.reactivex.exceptions.a;
import io.reactivex.h;
import java.util.concurrent.Callable;

public final class RxAndroidPlugins {
    private static volatile e<Callable<h>, h> onInitMainThreadHandler;
    private static volatile e<h, h> onMainThreadHandler;

    public static void setInitMainThreadSchedulerHandler(e<Callable<h>, h> eVar) {
        onInitMainThreadHandler = eVar;
    }

    public static h initMainThreadScheduler(Callable<h> callable) {
        if (callable == null) {
            throw new NullPointerException("scheduler == null");
        }
        e<Callable<h>, h> eVar = onInitMainThreadHandler;
        if (eVar == null) {
            return callRequireNonNull(callable);
        }
        return applyRequireNonNull(eVar, callable);
    }

    public static void setMainThreadSchedulerHandler(e<h, h> eVar) {
        onMainThreadHandler = eVar;
    }

    public static h onMainThreadScheduler(h hVar) {
        if (hVar == null) {
            throw new NullPointerException("scheduler == null");
        }
        e<h, h> eVar = onMainThreadHandler;
        return eVar == null ? hVar : (h) apply(eVar, hVar);
    }

    public static void reset() {
        setInitMainThreadSchedulerHandler(null);
        setMainThreadSchedulerHandler(null);
    }

    static h callRequireNonNull(Callable<h> callable) {
        try {
            h call = callable.call();
            if (call != null) {
                return call;
            }
            throw new NullPointerException("Scheduler Callable returned null");
        } catch (Throwable th) {
            throw a.a(th);
        }
    }

    static h applyRequireNonNull(e<Callable<h>, h> eVar, Callable<h> callable) {
        h hVar = (h) apply(eVar, callable);
        if (hVar != null) {
            return hVar;
        }
        throw new NullPointerException("Scheduler Callable returned null");
    }

    static <T, R> R apply(e<T, R> eVar, T t) {
        try {
            return eVar.a(t);
        } catch (Throwable th) {
            throw a.a(th);
        }
    }

    private RxAndroidPlugins() {
        throw new AssertionError("No instances.");
    }
}
