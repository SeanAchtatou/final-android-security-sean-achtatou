package com.kingoapp.uts.api;

import android.os.Handler;
import android.os.Looper;
import com.kingoapp.uts.conn.ApiConnection;
import com.kingoapp.uts.model.Result;
import com.kingoapp.uts.util.RC4Util;
import io.reactivex.BackpressureStrategy;
import io.reactivex.c.a;
import io.reactivex.d;
import io.reactivex.e;
import io.reactivex.f;
import io.reactivex.h;
import java.net.MalformedURLException;
import java.util.concurrent.Callable;

public class UtsApi {
    private static final h MAIN_THREAD = RxAndroidPlugins.initMainThreadScheduler(new Callable<h>() {
        public h call() {
            return MainHolder.DEFAULT;
        }
    });
    private static final String TAG = "UtsApi";
    /* access modifiers changed from: private */
    public static final char[] key = {'S', 'J', 'K', '@', '9', 'x', '.', '1'};
    private static final String plug_url = "https://log.kgmobi.com/adlog";
    private static final String url = "https://log.kgmobi.com/booster";

    private static final class MainHolder {
        static final h DEFAULT = new HandlerScheduler(new Handler(Looper.getMainLooper()));

        private MainHolder() {
        }
    }

    public d<Boolean> pushPlugInfo(final String str) {
        return d.a(new f<Boolean>() {
            public void subscribe(e<Boolean> eVar) {
                try {
                    Result requestSyncCall = ApiConnection.createApiConn(UtsApi.plug_url).requestSyncCall("POST", RC4Util.encry_RC4_byte(str, String.valueOf(UtsApi.key)));
                    if (requestSyncCall == null) {
                        eVar.a(Boolean.FALSE);
                    } else if (requestSyncCall.getCode() == 200) {
                        eVar.a(Boolean.TRUE);
                    } else {
                        eVar.a(Boolean.FALSE);
                    }
                    eVar.a();
                } catch (MalformedURLException e2) {
                    e2.printStackTrace();
                    eVar.a(e2);
                }
            }
        }, BackpressureStrategy.ERROR).b(a.b()).a(RxAndroidPlugins.onMainThreadScheduler(MAIN_THREAD));
    }

    public d<Boolean> pushInfo(final String str) {
        return d.a(new f<Boolean>() {
            public void subscribe(e<Boolean> eVar) {
                try {
                    Result requestSyncCall = ApiConnection.createApiConn(UtsApi.url).requestSyncCall("POST", RC4Util.encry_RC4_byte(str, String.valueOf(UtsApi.key)));
                    if (requestSyncCall == null) {
                        eVar.a(Boolean.FALSE);
                    } else if (requestSyncCall.getCode() == 200) {
                        eVar.a(Boolean.TRUE);
                    } else {
                        eVar.a(Boolean.FALSE);
                    }
                    eVar.a();
                } catch (MalformedURLException e2) {
                    e2.printStackTrace();
                    eVar.a(e2);
                }
            }
        }, BackpressureStrategy.ERROR).b(a.b()).a(RxAndroidPlugins.onMainThreadScheduler(MAIN_THREAD));
    }
}
