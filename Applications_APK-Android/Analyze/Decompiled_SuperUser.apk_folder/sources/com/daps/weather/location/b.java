package com.daps.weather.location;

import android.os.AsyncTask;
import android.os.Build;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: WeatherLocationAsynTask */
public abstract class b extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f3406a;

    public void a(Object... objArr) {
        if (f3406a == null) {
            f3406a = Executors.newSingleThreadExecutor();
        }
        if (Build.VERSION.SDK_INT < 11) {
            super.execute(objArr);
        } else {
            super.executeOnExecutor(f3406a, objArr);
        }
    }
}
