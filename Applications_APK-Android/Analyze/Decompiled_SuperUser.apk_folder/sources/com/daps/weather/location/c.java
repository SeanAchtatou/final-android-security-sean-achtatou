package com.daps.weather.location;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.daps.weather.reciver.DapWeatherBroadcastReceiver;
import com.daps.weather.service.DapWeatherMsgService;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: WeatherLocationManager */
public class c extends Observable {

    /* renamed from: a  reason: collision with root package name */
    public static c f3407a;

    /* renamed from: d  reason: collision with root package name */
    public static double f3408d = 3.141592653589793d;

    /* renamed from: e  reason: collision with root package name */
    public static double f3409e = 6378245.0d;

    /* renamed from: f  reason: collision with root package name */
    public static double f3410f = 0.006693421622965943d;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public static final String f3411g = c.class.getSimpleName();
    private static Handler j = new Handler() {
        public void dispatchMessage(Message message) {
            super.dispatchMessage(message);
            if (c.a() != null && TextUtils.isEmpty(c.a().f3413c)) {
            }
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public b f3412b = b.NOT_CONNECT;

    /* renamed from: c  reason: collision with root package name */
    public String f3413c;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public Application f3414h;
    /* access modifiers changed from: private */
    public Timer i;

    /* compiled from: WeatherLocationManager */
    public enum b {
        NOT_CONNECT,
        TRYING_FIRST,
        LOW_POWER,
        NOT_TRACK
    }

    public static c a() {
        return f3407a;
    }

    public static void a(final Application application) {
        com.daps.weather.base.c.a("prod");
        d.a(f3411g, "准备开启gps");
        f3407a = new c();
        f3407a.f3414h = application;
        try {
            application.startService(new Intent(application, DapWeatherMsgService.class));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (c.f3407a != null && c.f3407a.f3414h != null && c.f3407a.f3412b == b.NOT_CONNECT) {
                    d.a(c.f3411g, "该手机没有安装谷歌框架服务,使用Android原生获取吧,警告：你没有安装谷歌服务框架，请root后安装");
                    try {
                        application.startService(new Intent(c.f3407a.f3414h, DapWeatherLocationsService.class));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        d.b(c.f3411g, e2.getMessage());
                    }
                    d.a(c.f3411g, "启动了service");
                    if (c.f3407a.i == null) {
                        Timer unused = c.f3407a.i = new Timer();
                    }
                    try {
                        c.f3407a.i.scheduleAtFixedRate(new a(), 0, 600000);
                    } catch (Exception e3) {
                        d.a(c.f3411g, "开启locationtask出现异常", e3);
                    }
                }
            }
        }, 9000);
        a(application, new DapWeatherBroadcastReceiver());
        c();
        if (e.a(application)) {
            f.b.a(application).b();
        }
    }

    public void a(Context context) {
        setChanged();
        notifyObservers(context);
    }

    private static void c() {
    }

    private static void a(Context context, BroadcastReceiver broadcastReceiver) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        context.registerReceiver(broadcastReceiver, intentFilter);
    }

    /* compiled from: WeatherLocationManager */
    public static class a extends TimerTask {
        public void run() {
            d.a(c.f3411g, "location task 执行manager是" + c.f3407a + "    destroy是" + d.isDestory);
            if (c.f3407a != null && d.isDestory) {
                try {
                    c.f3407a.f3414h.startService(new Intent(c.f3407a.f3414h, DapWeatherLocationsService.class));
                } catch (Exception e2) {
                    e2.printStackTrace();
                    d.b(c.f3411g, e2.getMessage().toString());
                }
                d.a(c.f3411g, "使用原生api获取地理位置");
            }
        }
    }
}
