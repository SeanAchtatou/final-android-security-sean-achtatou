package com.daps.weather.location;

import android.app.IntentService;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: WeatherLocationService */
public class d extends IntentService {
    /* access modifiers changed from: private */
    public static final String TAG = d.class.getSimpleName();
    public static boolean isDestory;
    private ArrayList PROVIDER_ARRAY;
    private LocationListener gpsLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String str) {
            com.daps.weather.base.d.a(d.TAG, "GPS -> onProviderEnabled");
            d.this.getBestLocationProvider();
        }

        public void onProviderDisabled(String str) {
            com.daps.weather.base.d.a(d.TAG, "GPS -> onProviderDisabled");
            d.this.getBestLocationProvider();
        }
    };
    private LocationManager locationManager;
    private String locationProvider;
    private LocationListener networkLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String str) {
            com.daps.weather.base.d.a(d.TAG, "Network -> onProviderEnabled");
            d.this.getBestLocationProvider();
        }

        public void onProviderDisabled(String str) {
            com.daps.weather.base.d.a(d.TAG, "Network -> onProviderDisabled");
            d.this.getBestLocationProvider();
        }
    };
    private LocationListener passiveLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onProviderEnabled(String str) {
            com.daps.weather.base.d.a(d.TAG, "Passive -> onProviderEnabled");
            d.this.getBestLocationProvider();
        }

        public void onProviderDisabled(String str) {
            com.daps.weather.base.d.a(d.TAG, "Passive -> onProviderDisabled");
            d.this.getBestLocationProvider();
        }
    };

    public d() {
        super("GPS");
        com.daps.weather.base.d.a(TAG, "启动LocationService");
        this.PROVIDER_ARRAY = new ArrayList();
        this.PROVIDER_ARRAY.add("gps");
        this.PROVIDER_ARRAY.add("network");
        this.PROVIDER_ARRAY.add("passive");
        isDestory = false;
    }

    /* access modifiers changed from: private */
    public synchronized void getBestLocationProvider() {
        Location location;
        String str;
        Location location2;
        if (this.locationManager == null) {
            this.locationProvider = null;
        } else {
            List<String> allProviders = this.locationManager.getAllProviders();
            if (allProviders == null || allProviders.size() <= 0) {
                this.locationProvider = null;
            } else {
                Location location3 = null;
                String str2 = null;
                for (String next : allProviders) {
                    com.daps.weather.base.d.a(TAG, "getBestLocationProvider  ->  provider => " + next);
                    if (next != null && this.PROVIDER_ARRAY.contains(next) && e.d(getApplicationContext())) {
                        try {
                            location2 = this.locationManager.getLastKnownLocation(next);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            com.daps.weather.base.d.b(TAG, "gps获取失败:" + e2.getMessage());
                            location2 = null;
                        }
                        com.daps.weather.base.d.a(TAG, "getBestLocationProvider  ->  location => " + location2);
                        if (location2 != null) {
                            com.daps.weather.base.d.a(TAG, "getBestLocationProvider  ->  bestLocation => " + location3);
                            if (location3 == null) {
                                location3 = location2;
                                str2 = next;
                            } else {
                                com.daps.weather.base.d.a(TAG, "getBestLocationProvider  ->  location.getAccuracy() => " + location2.getAccuracy() + "  bestLocation.getAccuracy() => " + location3.getAccuracy());
                                if (Float.valueOf(location2.getAccuracy()).compareTo(Float.valueOf(location3.getAccuracy())) >= 0) {
                                    Location location4 = location2;
                                    str = next;
                                    location = location4;
                                    location3 = location;
                                    str2 = str;
                                }
                            }
                        }
                    }
                    location = location3;
                    str = str2;
                    location3 = location;
                    str2 = str;
                }
                this.locationProvider = str2;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 167 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHandleIntent(android.content.Intent r8) {
        /*
            r7 = this;
            r2 = 0
            java.lang.String r0 = com.daps.weather.location.d.TAG
            java.lang.String r1 = "onHandleIntent --> start"
            com.daps.weather.base.d.a(r0, r1)
            android.content.Context r0 = r7.getApplicationContext()
            boolean r0 = com.daps.weather.base.e.d(r0)
            if (r0 != 0) goto L_0x001a
            java.lang.String r0 = com.daps.weather.location.d.TAG
            java.lang.String r1 = "没有定位权限，走策略定位"
            com.daps.weather.base.d.a(r0, r1)
        L_0x0019:
            return
        L_0x001a:
            r7.locationProvider = r2
            r7.locationManager = r2
            java.lang.String r0 = "location"
            java.lang.Object r0 = r7.getSystemService(r0)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            r7.locationManager = r0
            android.location.LocationManager r0 = r7.locationManager
            if (r0 == 0) goto L_0x0019
            android.location.LocationManager r0 = r7.locationManager
            java.util.List r0 = r0.getAllProviders()
            java.lang.String r1 = com.daps.weather.location.d.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "AllProviders  -> "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.daps.weather.base.d.a(r1, r2)
            if (r0 == 0) goto L_0x0125
            java.util.Iterator r6 = r0.iterator()
        L_0x0050:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0125
            java.lang.Object r0 = r6.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = com.daps.weather.location.d.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "AllProviders  ->  provider => "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.daps.weather.base.d.a(r1, r2)
            if (r0 == 0) goto L_0x0050
            java.util.ArrayList r1 = r7.PROVIDER_ARRAY
            boolean r1 = r1.contains(r0)
            if (r1 == 0) goto L_0x0050
            android.content.Context r1 = r7.getApplicationContext()
            boolean r1 = com.daps.weather.base.e.d(r1)
            if (r1 == 0) goto L_0x0050
            java.lang.String r1 = "gps"
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x00a5 }
            if (r1 == 0) goto L_0x00c6
            java.lang.String r0 = com.daps.weather.location.d.TAG     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = "AllProviders  ->  provider => add gpsLocationListener"
            com.daps.weather.base.d.a(r0, r1)     // Catch:{ Exception -> 0x00a5 }
            android.location.LocationManager r0 = r7.locationManager     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = "gps"
            r2 = 600000(0x927c0, double:2.964394E-318)
            r4 = 0
            android.location.LocationListener r5 = r7.gpsLocationListener     // Catch:{ Exception -> 0x00a5 }
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x00a5 }
            goto L_0x0050
        L_0x00a5:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = com.daps.weather.location.d.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "gps异常:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.daps.weather.base.d.b(r1, r0)
            goto L_0x0050
        L_0x00c6:
            java.lang.String r1 = "network"
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x00a5 }
            if (r1 == 0) goto L_0x00e4
            java.lang.String r0 = com.daps.weather.location.d.TAG     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = "AllProviders  ->  provider => add networkLocationListener"
            com.daps.weather.base.d.a(r0, r1)     // Catch:{ Exception -> 0x00a5 }
            android.location.LocationManager r0 = r7.locationManager     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = "network"
            r2 = 600000(0x927c0, double:2.964394E-318)
            r4 = 0
            android.location.LocationListener r5 = r7.networkLocationListener     // Catch:{ Exception -> 0x00a5 }
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x00a5 }
            goto L_0x0050
        L_0x00e4:
            java.lang.String r1 = "passive"
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x00a5 }
            if (r0 == 0) goto L_0x0050
            java.lang.String r0 = com.daps.weather.location.d.TAG     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = "AllProviders  ->  provider => add passiveLocationListener"
            com.daps.weather.base.d.a(r0, r1)     // Catch:{ Exception -> 0x00a5 }
            android.location.LocationManager r0 = r7.locationManager     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = "passive"
            r2 = 600000(0x927c0, double:2.964394E-318)
            r4 = 0
            android.location.LocationListener r5 = r7.passiveLocationListener     // Catch:{ Exception -> 0x00a5 }
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x00a5 }
            goto L_0x0050
        L_0x0102:
            java.lang.String r0 = r7.locationProvider
            if (r0 == 0) goto L_0x0179
            java.util.ArrayList r0 = r7.PROVIDER_ARRAY
            java.lang.String r1 = r7.locationProvider
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0179
            com.daps.weather.location.a r0 = com.daps.weather.location.a.a()     // Catch:{ InterruptedException -> 0x0170 }
            double r0 = r0.f3404a     // Catch:{ InterruptedException -> 0x0170 }
            com.daps.weather.location.a r2 = com.daps.weather.location.a.a()     // Catch:{ InterruptedException -> 0x0170 }
            double r2 = r2.f3405b     // Catch:{ InterruptedException -> 0x0170 }
            boolean r0 = isWrongPosition(r0, r2)     // Catch:{ InterruptedException -> 0x0170 }
            if (r0 != 0) goto L_0x0169
            r0 = 1
            com.daps.weather.location.d.isDestory = r0     // Catch:{ InterruptedException -> 0x0170 }
        L_0x0125:
            boolean r0 = com.daps.weather.location.d.isDestory
            if (r0 != 0) goto L_0x0019
            r7.getBestLocationProvider()
            java.lang.String r0 = com.daps.weather.location.d.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "locationProvider => "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.locationProvider
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.daps.weather.base.d.a(r0, r1)
            r7.updateLocation()
            java.lang.String r0 = com.daps.weather.location.d.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "是否要停下"
            java.lang.StringBuilder r1 = r1.append(r2)
            boolean r2 = com.daps.weather.location.d.isDestory
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.daps.weather.base.d.a(r0, r1)
            boolean r0 = com.daps.weather.location.d.isDestory
            if (r0 == 0) goto L_0x0102
            goto L_0x0019
        L_0x0169:
            r0 = 600000(0x927c0, double:2.964394E-318)
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0170 }
            goto L_0x0125
        L_0x0170:
            r0 = move-exception
            java.lang.String r1 = com.daps.weather.location.d.TAG
            java.lang.String r2 = " onHandleIntent "
            com.daps.weather.base.d.a(r1, r2, r0)
            goto L_0x0125
        L_0x0179:
            r0 = 600000(0x927c0, double:2.964394E-318)
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0180 }
            goto L_0x0125
        L_0x0180:
            r0 = move-exception
            java.lang.String r1 = com.daps.weather.location.d.TAG
            java.lang.String r2 = " onHandleIntent "
            com.daps.weather.base.d.a(r1, r2, r0)
            goto L_0x0125
        */
        throw new UnsupportedOperationException("Method not decompiled: com.daps.weather.location.d.onHandleIntent(android.content.Intent):void");
    }

    private void updateLocation() {
        com.daps.weather.base.d.a(TAG, " ----> updateLocation <---- locationProvider => " + this.locationProvider);
        if (this.locationProvider != null && !this.locationProvider.equals("") && this.PROVIDER_ARRAY.contains(this.locationProvider)) {
            try {
                Location lastKnownLocation = this.locationManager.getLastKnownLocation(this.locationProvider);
                com.daps.weather.base.d.a(TAG, "\"通过旧版service取到GPS，经度:" + lastKnownLocation.getLongitude() + "纬度:" + lastKnownLocation.getLatitude());
                if (lastKnownLocation != null) {
                    double latitude = lastKnownLocation.getLatitude();
                    double longitude = lastKnownLocation.getLongitude();
                    com.daps.weather.base.d.a(TAG, "locationProvider -> " + latitude + " : " + longitude + "精确度" + lastKnownLocation.getAccuracy());
                    if (!isWrongPosition(latitude, longitude)) {
                        isDestory = true;
                    }
                    SharedPrefsUtils.b(getApplicationContext(), String.valueOf(lastKnownLocation.getLatitude()));
                    SharedPrefsUtils.c(getApplicationContext(), String.valueOf(lastKnownLocation.getLongitude()));
                    SharedPrefsUtils.d(getApplicationContext(), String.valueOf(lastKnownLocation.getAccuracy()));
                    c.a().a(getApplicationContext());
                }
            } catch (Exception e2) {
                com.daps.weather.base.d.a(TAG, " updateLocation ", e2);
            }
        }
    }

    public static boolean isWrongPosition(double d2, double d3) {
        if (Math.abs(d2) >= 0.01d || Math.abs(d3) >= 0.1d) {
            return false;
        }
        return true;
    }

    public void onDestroy() {
        com.daps.weather.base.d.a(TAG, " --> onDestroy");
        super.onDestroy();
        isDestory = true;
        if (e.d(getApplicationContext())) {
            try {
                if (!(this.locationManager == null || this.gpsLocationListener == null)) {
                    this.locationManager.removeUpdates(this.gpsLocationListener);
                }
                if (!(this.locationManager == null || this.networkLocationListener == null)) {
                    this.locationManager.removeUpdates(this.networkLocationListener);
                }
                if (this.locationManager != null && this.passiveLocationListener != null) {
                    this.locationManager.removeUpdates(this.passiveLocationListener);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                com.daps.weather.base.d.b(TAG, "获取gps失败:" + e2.getMessage());
            }
        }
    }
}
