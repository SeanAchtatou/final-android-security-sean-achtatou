package com.daps.weather.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.DisplayMetrics;
import f.b;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* compiled from: Util */
public class e {
    public static String a(String str) {
        return new SimpleDateFormat("MM/dd HH:mm").format(new Date(new Long(str).longValue() * 1000));
    }

    public static String b(String str) {
        return new SimpleDateFormat("MM/dd").format(new Date(new Long(str).longValue() * 1000));
    }

    public static String c(String str) {
        return new SimpleDateFormat("HH:mm").format(new Date(new Long(str).longValue()));
    }

    public static String d(String str) {
        return new SimpleDateFormat("yyyy/MM/dd").format(new Date(new Long(str).longValue()));
    }

    public static String e(String str) {
        int i = 0;
        Date date = new Date(new Long(str).longValue() * 1000);
        Date date2 = new Date(System.currentTimeMillis());
        String[] strArr = {"Sun", "Mon", "Tue", "Web", "Thu", "Fri", "Sat"};
        Calendar instance = Calendar.getInstance();
        if (date != null) {
            instance.setTime(date);
        }
        int i2 = instance.get(7) - 1;
        if (i2 < 0) {
            i2 = 0;
        }
        String str2 = strArr[i2];
        if (date2 != null) {
            instance.setTime(date2);
        }
        int i3 = instance.get(7) - 1;
        if (i3 >= 0) {
            i = i3;
        }
        String str3 = strArr[i];
        if (str2 == null || str3 == null) {
            return str2;
        }
        if (str2.equals(str3)) {
            return "Today";
        }
        return str2;
    }

    public static int a(int i) {
        return (int) (((double) i) / 1.609344d);
    }

    public static int a(double d2) {
        return b((d2 - 32.0d) / 1.8d);
    }

    public static int b(double d2) {
        return (int) Double.parseDouble(new BigDecimal(d2).setScale(0, 4).toString());
    }

    public static int c(double d2) {
        return (int) (25.4d * d2);
    }

    public static int a(Context context, float f2) {
        return (int) ((context.getResources().getDisplayMetrics().density * f2) + 0.5f);
    }

    public static long a() {
        String d2 = d(String.valueOf(System.currentTimeMillis()));
        Calendar.getInstance();
        String[] split = d2.split("/", 3);
        try {
            return new SimpleDateFormat("yyyy/MM/dd hh:ss").parse(split[0] + "/" + split[1] + "/" + (Integer.parseInt(split[2]) - 1) + " 00:00").getTime();
        } catch (ParseException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void a(Context context, String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(str + "&partner=baidudap"));
        context.startActivity(intent);
    }

    public static Bitmap a(Context context, int i) {
        return b.a(context).a(i);
    }

    public static int f(String str) {
        if ("E".equals(str)) {
            return 90;
        }
        if ("ENE".equals(str)) {
            return 60;
        }
        if ("ESE".equals(str)) {
            return 120;
        }
        if ("N".equals(str)) {
            return 0;
        }
        if ("NE".equals(str)) {
            return 45;
        }
        if ("NNE".equals(str)) {
            return 30;
        }
        if ("NNW".equals(str)) {
            return 330;
        }
        if ("NW".equals(str)) {
            return 300;
        }
        if ("S".equals(str)) {
            return 180;
        }
        if ("SE".equals(str)) {
            return 120;
        }
        if ("SSE".equals(str)) {
            return 150;
        }
        if ("SSW".equals(str)) {
            return 210;
        }
        if ("SW".equals(str)) {
            return 240;
        }
        if ("W".equals(str)) {
            return 270;
        }
        if ("WNW".equals(str)) {
            return 300;
        }
        if ("WSW".equals(str)) {
            return 240;
        }
        return 0;
    }

    public static boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static int b(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        float f2 = displayMetrics.density;
        int i3 = displayMetrics.densityDpi;
        return i;
    }

    public static int c(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static boolean d(Context context) {
        return b(context, "android.permission.ACCESS_FINE_LOCATION");
    }

    public static boolean e(Context context) {
        return b(context, "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    public static boolean b(Context context, String str) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), (int) CodedOutputStream.DEFAULT_BUFFER_SIZE).requestedPermissions;
            if (strArr == null) {
                return false;
            }
            for (String equals : strArr) {
                if (equals.equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
