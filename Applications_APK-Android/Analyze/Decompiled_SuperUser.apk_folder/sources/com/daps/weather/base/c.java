package com.daps.weather.base;

/* compiled from: Constants */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static String f3376a = "http://api.accuweather.com/";

    /* renamed from: b  reason: collision with root package name */
    public static boolean f3377b = false;

    /* renamed from: c  reason: collision with root package name */
    public static boolean f3378c = true;

    public static void a(String str) {
        if ("prod".equals(str)) {
            f3376a = "http://api.accuweather.com/";
            f3377b = false;
            f3378c = true;
        } else if ("dev".equals(str) || "test".equals(str)) {
            f3376a = "http://apidev.accuweather.com/";
            f3377b = true;
            f3378c = false;
        }
    }
}
