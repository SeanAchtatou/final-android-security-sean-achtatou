package com.daps.weather.base;

import android.util.Log;

/* compiled from: LogHelper */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f3379a = c.f3377b;

    public static boolean a() {
        return f3379a;
    }

    public static void a(String str, String str2) {
        if (f3379a) {
            Log.d("dap_weather", c(str, str2));
        }
    }

    public static void b(String str, String str2) {
        Log.e("dap_weather", c(str, str2));
    }

    public static void a(String str, String str2, Throwable th) {
        Log.e("dap_weather", c(str, str2), th);
    }

    private static String c(String str, String str2) {
        return new StringBuffer().append("{").append(Thread.currentThread().getName()).append("}").append("[").append(str).append("] ").append(str2).toString();
    }
}
