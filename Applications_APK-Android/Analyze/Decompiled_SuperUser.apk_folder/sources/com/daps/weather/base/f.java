package com.daps.weather.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: WeatherDataSQLite */
public class f extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static f f3380a;

    private f(Context context) {
        super(context, "du_weather_data.db", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public static f a(Context context) {
        if (f3380a == null) {
            synchronized (f.class) {
                if (f3380a == null) {
                    f3380a = new f(context);
                }
            }
        }
        return f3380a;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS locations(_id INTEGER  primary key autoincrement,key TEXT,data TEXT,log TEXT,ts INTEGER);");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS currentconditions(_id INTEGER  primary key autoincrement,key TEXT,data TEXT,log TEXT,ts INTEGER);");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS forecasts(_id INTEGER  primary key autoincrement,key TEXT,data TEXT,log TEXT,ts INTEGER);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }
}
