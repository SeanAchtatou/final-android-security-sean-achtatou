package com.daps.weather.base;

import a.b;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import com.daps.weather.floatdisplay.c;
import com.daps.weather.floatdisplay.d;
import com.duapps.ad.internal.b.a;

public class SharedPrefsUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3373a = SharedPrefsUtils.class.getSimpleName();

    private SharedPrefsUtils() {
    }

    public static void a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "location_key", str);
        }
    }

    public static String a(Context context) {
        return b(context, "location_key", "");
    }

    public static void b(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "location_latitude", str);
        }
    }

    public static String b(Context context) {
        String b2 = b(context, "location_latitude", "");
        if (TextUtils.isEmpty(b2)) {
            String str = null;
            try {
                str = a.a(context).split(",")[1];
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (!TextUtils.isEmpty(str)) {
                d.a(f3373a, "没有gps经纬度，使用策略纬度:" + str);
                h(context, "online");
                return str;
            }
            d.a(f3373a, "没有gps经纬度，策略纬度也是空");
        } else {
            h(context, "gps");
        }
        return b2;
    }

    public static void c(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "location_longitude", str);
        }
    }

    public static String c(Context context) {
        String str;
        String str2;
        String b2 = b(context, "location_longitude", "");
        if (TextUtils.isEmpty(b2)) {
            String str3 = null;
            try {
                str3 = a.a(context).split(",")[0];
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (!TextUtils.isEmpty(str3)) {
                h(context, "online");
                d.a(f3373a, "没有gps经纬度，使用策略经度:" + str3);
                str2 = str3;
                str = "策略经度:" + str3;
            } else {
                d.a(f3373a, "没有gps经纬度，策略也没有经度");
                str = "";
                str2 = b2;
            }
        } else {
            str = "GPS经度:" + b2;
            h(context, "gps");
            str2 = b2;
        }
        d.a(f3373a, "经纬度来源:" + str);
        return str2;
    }

    public static void d(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "location_accuracy", str);
        }
    }

    public static void e(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "push_today", str);
        }
    }

    public static String d(Context context) {
        return b(context, "push_today", "");
    }

    public static void f(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "push_day", str);
        }
    }

    public static String e(Context context) {
        return b(context, "push_day", "");
    }

    public static void g(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "push_nextday", str);
        }
    }

    public static String f(Context context) {
        return b(context, "push_nextday", "");
    }

    public static void h(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "location_type", str);
        }
    }

    public static String g(Context context) {
        return b(context, "location_type", "");
    }

    public static void i(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(context, "push_lift", str);
        }
    }

    public static String h(Context context) {
        return b(context, "push_lift", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean i(Context context) {
        return b(context, "day_is_new", false);
    }

    public static void a(Context context, boolean z) {
        a(context, "day_is_new", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean j(Context context) {
        return b(context, "push_isPushElevatingTemperature", true);
    }

    public static void b(Context context, boolean z) {
        a(context, "push_isPushElevatingTemperature", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean k(Context context) {
        return b(context, "push_isPushTodayTomorrowWeather", true);
    }

    public static void c(Context context, boolean z) {
        a(context, "push_isPushTodayTomorrowWeather", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean l(Context context) {
        return b(context, "notification_ongoing", true);
    }

    public static void d(Context context, boolean z) {
        a(context, "notification_ongoing", z);
    }

    public static void a(Context context, Boolean bool) {
        if (c.a(context).c()) {
            a(context, "fwss", bool.booleanValue());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean m(Context context) {
        return b(context, "fwss", false);
    }

    public static void b(Context context, Boolean bool) {
        a(context, "ufcs", bool.booleanValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean n(Context context) {
        return b(context, "ufcs", false);
    }

    public static boolean o(Context context) {
        long p = p(context);
        d.a(f3373a, "proTime = " + p + " --getAppUsedTime = " + b.a().b());
        return p > b.a().b();
    }

    public static long p(Context context) {
        return ((long) b(context, "fspt", 12)) * 3600000;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean q(Context context) {
        return b(context, "fss", false);
    }

    public static void a(Context context, int i) {
        a(context, "fwshv_x", i);
    }

    public static int r(Context context) {
        return b(context, "fwshv_x", d.a(context));
    }

    public static void b(Context context, int i) {
        a(context, "fwshv_y", i);
    }

    public static int s(Context context) {
        return b(context, "fwshv_y", d.b(context) / 6);
    }

    public static void c(Context context, int i) {
        a(context, "pid_result", i);
    }

    public static int t(Context context) {
        return b(context, "pid_result", 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, int):int
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean isSuspensionOn(Context context) {
        return b(context, "key_prefs_suspension", true);
    }

    public static void setSuspensionOn(Context context, boolean z) {
        a(context, "key_prefs_suspension", z);
    }

    public static void a(Context context, String str, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_weather_prefs", 0).edit();
        edit.putInt(str, i);
        edit.apply();
    }

    public static int b(Context context, String str, int i) {
        return context.getSharedPreferences("_weather_prefs", 0).getInt(str, i);
    }

    private static void a(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_weather_prefs", 0).edit();
        edit.putString(str, str2);
        a(edit);
    }

    private static String b(Context context, String str, String str2) {
        return context.getSharedPreferences("_weather_prefs", 0).getString(str, str2);
    }

    private static void a(Context context, String str, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("_weather_prefs", 0).edit();
        edit.putBoolean(str, z);
        a(edit);
    }

    private static boolean b(Context context, String str, boolean z) {
        return context.getSharedPreferences("_weather_prefs", 0).getBoolean(str, z);
    }

    public static void a(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT <= 8) {
            editor.commit();
        } else {
            editor.apply();
        }
    }
}
