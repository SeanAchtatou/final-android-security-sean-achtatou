package com.daps.weather.base;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.daps.weather.bean.currentconditions.CurrentCondition;
import com.daps.weather.bean.forecasts.Forecast;
import com.daps.weather.bean.locations.Location;
import com.google.weathergson.Gson;
import f.b;
import java.util.List;

/* compiled from: WeatherDataSQLiteHelper */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3381a = g.class.getSimpleName();

    public static List a(Context context, SQLiteDatabase sQLiteDatabase) {
        long a2 = e.a();
        if (a2 > 0 && SharedPrefsUtils.i(context)) {
            a(sQLiteDatabase, a2);
        }
        return b.a(context).a(a(sQLiteDatabase, "locations", "data"), Location.class);
    }

    public static void a(SQLiteDatabase sQLiteDatabase, long j) {
        if (a(sQLiteDatabase, "locations", "ts<=?", String.valueOf(j)) > 0) {
            d.a(f3381a, "删除Locations过期数据成功");
        } else {
            d.a(f3381a, "删除Locations过期数据失败");
        }
    }

    public static List b(Context context, SQLiteDatabase sQLiteDatabase) {
        long a2 = e.a();
        if (a2 > 0 && SharedPrefsUtils.i(context)) {
            b(sQLiteDatabase, a2);
        }
        return b.a(context).a(a(sQLiteDatabase, "currentconditions", "data"), CurrentCondition.class);
    }

    public static void b(SQLiteDatabase sQLiteDatabase, long j) {
        if (a(sQLiteDatabase, "currentconditions", "ts<=?", String.valueOf(j)) > 0) {
            d.a(f3381a, "删除CurrentCondition过期数据成功");
        } else {
            d.a(f3381a, "删除CurrentCondition过期数据失败");
        }
    }

    public static Forecast c(Context context, SQLiteDatabase sQLiteDatabase) {
        long a2 = e.a();
        if (a2 > 0 && SharedPrefsUtils.i(context)) {
            c(sQLiteDatabase, a2);
        }
        return (Forecast) new Gson().fromJson(a(sQLiteDatabase, "forecasts", "data"), Forecast.class);
    }

    public static void c(SQLiteDatabase sQLiteDatabase, long j) {
        if (a(sQLiteDatabase, "forecasts", "ts<=?", String.valueOf(j)) > 0) {
            d.a(f3381a, "删除Forecast过期数据成功");
        } else {
            d.a(f3381a, "删除Forecast过期数据失败");
        }
    }

    private static String a(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        Cursor query = sQLiteDatabase.query(str, null, null, null, null, null, null);
        if (!query.moveToFirst()) {
            return "";
        }
        String str3 = "";
        for (int i = 0; i < query.getCount(); i++) {
            if (query.moveToLast()) {
                str3 = query.getString(query.getColumnIndex(str2));
            }
        }
        return str3;
    }

    private static int a(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3) {
        return sQLiteDatabase.delete(str, str2, new String[]{str3});
    }
}
