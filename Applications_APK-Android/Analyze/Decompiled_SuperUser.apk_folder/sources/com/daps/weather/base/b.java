package com.daps.weather.base;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/* compiled from: CalculatorUtils */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final Map f3375a = new HashMap();

    static {
        f3375a.put('=', 0);
        f3375a.put('-', 1);
        f3375a.put('+', 1);
        f3375a.put('*', 2);
        f3375a.put('/', 2);
        f3375a.put('(', 3);
        f3375a.put(')', 1);
    }

    public static boolean a(String str) {
        if ('=' != str.charAt(str.length() - 1)) {
            return false;
        }
        if (!b(Character.valueOf(str.charAt(0))) && str.charAt(0) != '(') {
            return false;
        }
        for (int i = 1; i < str.length() - 1; i++) {
            char charAt = str.charAt(i);
            if (!a(Character.valueOf(charAt))) {
                return false;
            }
            if (!b(Character.valueOf(charAt))) {
                if (charAt == '-' || charAt == '+' || charAt == '*' || charAt == '/') {
                    if (charAt != '-' || str.charAt(i - 1) != '(') {
                        if (!b(Character.valueOf(str.charAt(i - 1))) && str.charAt(i - 1) != ')') {
                            return false;
                        }
                    }
                }
                if (charAt == '.' && (!b(Character.valueOf(str.charAt(i - 1))) || !b(Character.valueOf(str.charAt(i + 1))))) {
                    return false;
                }
            }
        }
        return c(str);
    }

    public static String b(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (i != 0 && charAt == '(' && (b(Character.valueOf(str.charAt(i - 1))) || str.charAt(i - 1) == ')')) {
                sb.append("*(");
            } else if (charAt == '-' && str.charAt(i - 1) == '(') {
                sb.append("0-");
            } else {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    public static boolean c(String str) {
        LinkedList linkedList = new LinkedList();
        for (char c2 : str.toCharArray()) {
            if (c2 == '(') {
                linkedList.add(Character.valueOf(c2));
            } else if (c2 != ')') {
                continue;
            } else if (linkedList.isEmpty()) {
                return false;
            } else {
                linkedList.removeLast();
            }
        }
        if (linkedList.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean a(Character ch) {
        if (('0' <= ch.charValue() && ch.charValue() <= '9') || ch.charValue() == '-' || ch.charValue() == '+' || ch.charValue() == '*' || ch.charValue() == '/' || ch.charValue() == '(' || ch.charValue() == ')' || ch.charValue() == '.') {
            return true;
        }
        return false;
    }

    public static boolean b(Character ch) {
        if (ch.charValue() < '0' || ch.charValue() > '9') {
            return false;
        }
        return true;
    }

    public static double a(double d2, double d3) {
        return d2 + d3;
    }

    public static double b(double d2, double d3) {
        return d2 - d3;
    }

    public static double c(double d2, double d3) {
        return d2 * d3;
    }

    public static double d(double d2, double d3) {
        return d2 / d3;
    }
}
