package com.daps.weather.base;

import java.util.ArrayList;

/* compiled from: Calculator */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3374a = a.class.getSimpleName();

    public Double a(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        if (str.length() > 500) {
            d.a(f3374a, "表达式过长");
            return null;
        }
        String replaceAll = str.replaceAll(" ", "");
        if ('-' == replaceAll.charAt(0)) {
            replaceAll = 0 + replaceAll;
        }
        if (!b.a(replaceAll)) {
            d.a(f3374a, "表达式错误");
            return null;
        }
        String b2 = b.b(replaceAll);
        String[] split = b2.split("[^.0-9]");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < split.length; i++) {
            if (!"".equals(split[i])) {
                arrayList.add(Double.valueOf(Double.parseDouble(split[i])));
            }
        }
        return a(b2.replaceAll("[.0-9]", ""), arrayList);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x000e, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Double a(java.lang.String r13, java.util.List r14) {
        /*
            r12 = this;
            java.util.LinkedList r4 = new java.util.LinkedList
            r4.<init>()
            java.util.LinkedList r5 = new java.util.LinkedList
            r5.<init>()
            r1 = 0
            r0 = 0
            r2 = r1
            r1 = r0
        L_0x000e:
            boolean r0 = r4.isEmpty()
            if (r0 != 0) goto L_0x002a
            java.lang.Object r0 = r4.getLast()
            java.lang.Character r0 = (java.lang.Character) r0
            char r0 = r0.charValue()
            r3 = 61
            if (r0 != r3) goto L_0x002a
            char r0 = r13.charAt(r1)
            r3 = 61
            if (r0 == r3) goto L_0x0146
        L_0x002a:
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x0043
            r0 = 61
            java.lang.Character r0 = java.lang.Character.valueOf(r0)
            r4.add(r0)
            int r0 = r2 + 1
            java.lang.Object r2 = r14.get(r2)
            r5.add(r2)
            r2 = r0
        L_0x0043:
            java.util.Map r0 = com.daps.weather.base.b.f3375a
            char r3 = r13.charAt(r1)
            java.lang.Character r3 = java.lang.Character.valueOf(r3)
            java.lang.Object r0 = r0.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r3 = r0.intValue()
            java.util.Map r0 = com.daps.weather.base.b.f3375a
            java.lang.Object r6 = r4.getLast()
            java.lang.Object r0 = r0.get(r6)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            if (r3 <= r0) goto L_0x009a
            char r0 = r13.charAt(r1)
            r3 = 40
            if (r0 != r3) goto L_0x0080
            int r0 = r1 + 1
            char r1 = r13.charAt(r1)
            java.lang.Character r1 = java.lang.Character.valueOf(r1)
            r4.add(r1)
            r1 = r0
            goto L_0x000e
        L_0x0080:
            int r3 = r2 + 1
            java.lang.Object r0 = r14.get(r2)
            r5.add(r0)
            int r0 = r1 + 1
            char r1 = r13.charAt(r1)
            java.lang.Character r1 = java.lang.Character.valueOf(r1)
            r4.add(r1)
            r1 = r0
            r2 = r3
            goto L_0x000e
        L_0x009a:
            char r0 = r13.charAt(r1)
            r3 = 41
            if (r0 != r3) goto L_0x00b8
            java.lang.Object r0 = r4.getLast()
            java.lang.Character r0 = (java.lang.Character) r0
            char r0 = r0.charValue()
            r3 = 40
            if (r0 != r3) goto L_0x00b8
            int r0 = r1 + 1
            r4.removeLast()
            r1 = r0
            goto L_0x000e
        L_0x00b8:
            java.lang.Object r0 = r4.getLast()
            java.lang.Character r0 = (java.lang.Character) r0
            char r0 = r0.charValue()
            r3 = 40
            if (r0 != r3) goto L_0x00e0
            int r3 = r2 + 1
            java.lang.Object r0 = r14.get(r2)
            r5.add(r0)
            int r0 = r1 + 1
            char r1 = r13.charAt(r1)
            java.lang.Character r1 = java.lang.Character.valueOf(r1)
            r4.add(r1)
            r1 = r0
            r2 = r3
            goto L_0x000e
        L_0x00e0:
            java.lang.Object r0 = r5.removeLast()
            java.lang.Double r0 = (java.lang.Double) r0
            double r6 = r0.doubleValue()
            java.lang.Object r0 = r5.removeLast()
            java.lang.Double r0 = (java.lang.Double) r0
            double r8 = r0.doubleValue()
            java.lang.Object r0 = r4.removeLast()
            java.lang.Character r0 = (java.lang.Character) r0
            char r0 = r0.charValue()
            switch(r0) {
                case 42: goto L_0x0103;
                case 43: goto L_0x0110;
                case 44: goto L_0x0101;
                case 45: goto L_0x011d;
                case 46: goto L_0x0101;
                case 47: goto L_0x012a;
                default: goto L_0x0101;
            }
        L_0x0101:
            goto L_0x000e
        L_0x0103:
            double r6 = com.daps.weather.base.b.c(r8, r6)
            java.lang.Double r0 = java.lang.Double.valueOf(r6)
            r5.add(r0)
            goto L_0x000e
        L_0x0110:
            double r6 = com.daps.weather.base.b.a(r8, r6)
            java.lang.Double r0 = java.lang.Double.valueOf(r6)
            r5.add(r0)
            goto L_0x000e
        L_0x011d:
            double r6 = com.daps.weather.base.b.b(r8, r6)
            java.lang.Double r0 = java.lang.Double.valueOf(r6)
            r5.add(r0)
            goto L_0x000e
        L_0x012a:
            r10 = 0
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 != 0) goto L_0x0139
            java.lang.String r0 = com.daps.weather.base.a.f3374a
            java.lang.String r1 = "存在除数为0"
            com.daps.weather.base.d.a(r0, r1)
            r0 = 0
        L_0x0138:
            return r0
        L_0x0139:
            double r6 = com.daps.weather.base.b.d(r8, r6)
            java.lang.Double r0 = java.lang.Double.valueOf(r6)
            r5.add(r0)
            goto L_0x000e
        L_0x0146:
            java.lang.Object r0 = r5.removeLast()
            java.lang.Double r0 = (java.lang.Double) r0
            goto L_0x0138
        */
        throw new UnsupportedOperationException("Method not decompiled: com.daps.weather.base.a.a(java.lang.String, java.util.List):java.lang.Double");
    }
}
