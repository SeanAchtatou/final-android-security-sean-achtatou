package com.daps.weather.floatdisplay;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.d;
import com.duapps.ad.internal.b.a;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import java.util.List;

public class FloatDisplayController {
    private static final String TAG = "FloatDisplayController";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, int):void
      com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, java.lang.String):void
      com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, boolean):void
      com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, java.lang.Boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, int):void
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.String):void
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, boolean):void
      com.daps.weather.base.SharedPrefsUtils.b(android.content.Context, java.lang.Boolean):void */
    public static void setFloatSerachWindowIsShow(Context context, Boolean bool) {
        try {
            if (isMainProcess(context)) {
                d.a(TAG, "悬浮窗_主线程");
                if (!a.b(context)) {
                    SharedPrefsUtils.a(context, (Boolean) false);
                    SharedPrefsUtils.b(context, (Boolean) false);
                    c.a(context).a();
                    return;
                }
                SharedPrefsUtils.a(context, bool);
                SharedPrefsUtils.b(context, bool);
                c.a(context).a();
                return;
            }
            d.a(TAG, "悬浮窗_其他线程");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static boolean getFloatSearchWindowIsShow(Context context) {
        if (SharedPrefsUtils.n(context)) {
            return true;
        }
        if (!SharedPrefsUtils.m(context) || SharedPrefsUtils.o(context)) {
            return false;
        }
        return true;
    }

    public static boolean getFloatSearchSettingIsShow(Context context) {
        return SharedPrefsUtils.q(context) && c.a(context).c();
    }

    private static boolean isMainProcess(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == Process.myPid()) {
                    return context.getPackageName().equals(next.processName);
                }
            }
        }
        return true;
    }
}
