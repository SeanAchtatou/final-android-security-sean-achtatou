package com.daps.weather.floatdisplay;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.daps.weather.a;
import com.daps.weather.base.SharedPrefsUtils;

/* compiled from: CircleFloatView */
public class b extends View {

    /* renamed from: a  reason: collision with root package name */
    private int f3383a;

    /* renamed from: b  reason: collision with root package name */
    private Paint f3384b;

    /* renamed from: c  reason: collision with root package name */
    private int f3385c;

    /* renamed from: d  reason: collision with root package name */
    private Context f3386d;

    public b(Context context) {
        this(context, null);
    }

    public b(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public b(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3385c = 0;
        a(context);
    }

    private void a(Context context) {
        this.f3386d = context;
        this.f3383a = context.getResources().getDimensionPixelSize(a.b.e8);
        a();
    }

    private void a() {
        this.f3384b = new Paint();
        this.f3384b.setAntiAlias(true);
        this.f3384b.setStyle(Paint.Style.FILL);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        this.f3383a = this.f3385c;
        this.f3384b.setColor(-1);
        canvas.drawCircle((float) SharedPrefsUtils.r(this.f3386d), (float) SharedPrefsUtils.s(this.f3386d), (float) this.f3383a, this.f3384b);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
    }

    public void setHeight(int i) {
        this.f3385c = i;
        invalidate();
    }
}
