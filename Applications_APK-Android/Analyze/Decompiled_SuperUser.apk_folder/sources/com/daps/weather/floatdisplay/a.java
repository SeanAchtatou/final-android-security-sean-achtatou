package com.daps.weather.floatdisplay;

import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/* compiled from: BuildProperties */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Properties f3382a = new Properties();

    private a() {
        this.f3382a.load(new FileInputStream(new File(Environment.getRootDirectory(), "build.prop")));
    }

    public String a(String str, String str2) {
        return this.f3382a.getProperty(str, str2);
    }

    public static a a() {
        return new a();
    }
}
