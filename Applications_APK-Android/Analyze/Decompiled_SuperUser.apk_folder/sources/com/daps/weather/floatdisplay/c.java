package com.daps.weather.floatdisplay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.daps.weather.DapWeatherActivity;
import com.daps.weather.a;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.d;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import e.a;
import g.a;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/* compiled from: FloatHelperMgr */
public class c implements a.C0101a {

    /* renamed from: a  reason: collision with root package name */
    private static c f3387a;

    /* renamed from: b  reason: collision with root package name */
    private static Handler f3388b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f3389c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public WindowManager.LayoutParams f3390d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public FrameLayout f3391e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public ImageView f3392f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public WindowManager f3393g;

    /* renamed from: h  reason: collision with root package name */
    private ObjectAnimator f3394h;
    private ObjectAnimator i;
    private ValueAnimator j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public boolean o = false;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public Rect q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public long s;
    /* access modifiers changed from: private */
    public b t;
    private FrameLayout u;
    private WindowManager.LayoutParams v;
    private Runnable w = new Runnable() {
        public void run() {
            c.this.p();
        }
    };

    public static c a(Context context) {
        if (f3387a == null) {
            synchronized (c.class) {
                if (f3387a == null) {
                    f3387a = new c(context.getApplicationContext());
                }
            }
        }
        return f3387a;
    }

    private c(Context context) {
        this.f3389c = context;
        f(context);
    }

    private void f(Context context) {
        this.l = d.a(context);
        this.k = this.l >> 1;
        this.f3392f = new ImageView(context);
        this.f3392f.setImageResource(a.c.hz);
        this.r = context.getResources().getDimensionPixelSize(a.b.e8);
        i();
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!this.o) {
            f();
        }
    }

    private void f() {
        if (this.f3392f == null || this.f3393g == null || this.f3390d == null || this.q == null || this.f3391e == null) {
            h();
        }
        this.f3390d.x = SharedPrefsUtils.r(this.f3389c);
        this.f3390d.y = SharedPrefsUtils.s(this.f3389c);
        if (!(this.f3391e == null || this.f3391e.getParent() == null)) {
            this.f3393g.removeView(this.f3391e);
        }
        this.f3393g.addView(this.f3391e, this.f3390d);
        l();
        this.o = true;
    }

    /* access modifiers changed from: private */
    public void g() {
        if (!FloatDisplayController.getFloatSearchWindowIsShow(this.f3389c)) {
            g.a.a(this.f3389c).b(this);
        }
        if (this.o) {
            m();
            k();
            o();
            if (!b()) {
                if (this.f3391e != null) {
                    this.f3393g.removeView(this.f3391e);
                    this.o = false;
                }
                if (!c() || !FloatDisplayController.getFloatSearchWindowIsShow(this.f3389c)) {
                    q();
                }
            } else if (!c() || !FloatDisplayController.getFloatSearchWindowIsShow(this.f3389c)) {
                if (this.f3391e != null) {
                    this.f3393g.removeView(this.f3391e);
                    this.o = false;
                }
                q();
            }
        }
    }

    public void a() {
        f3388b.post(new Runnable() {
            public void run() {
                if (!FloatDisplayController.getFloatSearchWindowIsShow(c.this.f3389c) || !c.a(c.this.f3389c).c()) {
                    c.this.g();
                    return;
                }
                g.a.a(c.this.f3389c).b(c.this);
                g.a.a(c.this.f3389c).a(c.this);
                c.this.e();
            }
        });
    }

    private void h() {
        if (this.f3393g == null) {
            this.f3393g = (WindowManager) this.f3389c.getSystemService("window");
        }
        if (this.f3392f == null) {
            f(this.f3389c);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -2);
            layoutParams.height = this.r;
            layoutParams.width = this.r;
            this.f3392f.setLayoutParams(layoutParams);
        }
        if (this.f3391e == null) {
            this.f3391e = new FrameLayout(this.f3389c);
            this.f3391e.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            this.f3391e.addView(this.f3392f);
        }
        if (this.f3390d == null) {
            this.f3390d = new WindowManager.LayoutParams();
            this.f3390d.type = 2002;
            this.f3390d.format = 1;
            this.f3390d.flags = 131112;
            this.f3390d.gravity = 51;
            this.f3390d.width = this.r;
            this.f3390d.height = this.r;
        }
        if (this.q == null) {
            this.q = new Rect();
        }
    }

    private void i() {
        this.f3392f.setOnTouchListener(new View.OnTouchListener() {

            /* renamed from: a  reason: collision with root package name */
            boolean f3397a = false;

            public boolean onTouch(View view, MotionEvent motionEvent) {
                int i = 0;
                switch (motionEvent.getAction()) {
                    case 0:
                        if (System.currentTimeMillis() - c.this.s >= ((long) ViewConfiguration.getDoubleTapTimeout())) {
                            long unused = c.this.s = System.currentTimeMillis();
                            int unused2 = c.this.m = (int) motionEvent.getX();
                            int unused3 = c.this.n = (int) motionEvent.getY();
                            c.this.f3392f.getWindowVisibleDisplayFrame(c.this.q);
                            int unused4 = c.this.p = c.this.q.top;
                            this.f3397a = false;
                            c.this.o();
                            c.this.f3392f.setAlpha(1.0f);
                            break;
                        } else {
                            return false;
                        }
                    case 1:
                    case 3:
                    case 4:
                        if (!this.f3397a) {
                            c.this.j();
                            break;
                        } else {
                            int rawX = ((int) motionEvent.getRawX()) - c.this.m;
                            if (rawX >= c.this.k) {
                                i = c.this.l - c.this.r;
                            }
                            SharedPrefsUtils.a(c.this.f3389c, i);
                            SharedPrefsUtils.b(c.this.f3389c, c.this.f3390d.y);
                            c.this.a(rawX, i);
                            break;
                        }
                    case 2:
                        if (c.this.o && (this.f3397a || Math.abs(motionEvent.getX() - ((float) c.this.m)) > ((float) ViewConfiguration.get(c.this.f3389c).getScaledTouchSlop()) || Math.abs(motionEvent.getY() - ((float) c.this.n)) > ((float) ViewConfiguration.get(c.this.f3389c).getScaledTouchSlop()))) {
                            c.this.f3390d.x = (int) (motionEvent.getRawX() - ((float) c.this.m));
                            c.this.f3390d.y = (int) ((motionEvent.getRawY() - ((float) c.this.n)) - ((float) c.this.p));
                            this.f3397a = true;
                            c.this.f3393g.updateViewLayout(c.this.f3391e, c.this.f3390d);
                            break;
                        }
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void j() {
        g();
        if (this.f3393g == null) {
            this.f3393g = (WindowManager) this.f3389c.getSystemService("window");
        }
        this.t = new b(this.f3389c);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -1);
        layoutParams.height = d.b(this.f3389c);
        layoutParams.width = d.b(this.f3389c);
        this.t.setLayoutParams(layoutParams);
        this.u = new FrameLayout(this.f3389c);
        this.u.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.v = new WindowManager.LayoutParams();
        this.v.type = 2002;
        this.v.format = 1;
        this.v.flags = 131112;
        this.v.gravity = 51;
        this.v.width = d.b(this.f3389c);
        this.v.height = d.b(this.f3389c);
        this.v.x = SharedPrefsUtils.r(this.f3389c);
        this.v.y = SharedPrefsUtils.s(this.f3389c);
        ValueAnimator ofInt = ValueAnimator.ofInt(0, d.b(this.f3389c));
        ofInt.setDuration(400L);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                c.this.t.setAlpha(((float) ((Integer) valueAnimator.getAnimatedValue()).intValue()) / ((float) d.b(c.this.f3389c)));
                c.this.t.setHeight(((Integer) valueAnimator.getAnimatedValue()).intValue());
            }
        });
        e.a.c(this.f3389c, a.C0097a.SUSPENSION);
        Intent intent = new Intent();
        intent.setFlags(268435456);
        intent.setClass(this.f3389c, DapWeatherActivity.class);
        this.f3389c.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        k();
        this.j = ValueAnimator.ofInt(i2, i3);
        this.j.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (c.this.f3391e != null && c.this.o) {
                    c.this.f3390d.x = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                    c.this.f3393g.updateViewLayout(c.this.f3391e, c.this.f3390d);
                }
            }
        });
        this.j.addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                c.this.f3392f.setEnabled(false);
            }

            public void onAnimationEnd(Animator animator) {
                c.this.f3392f.setEnabled(true);
                c.this.n();
            }

            public void onAnimationCancel(Animator animator) {
                c.this.f3392f.setEnabled(true);
            }
        });
        this.j.setDuration((long) ((Math.abs(i2 - i3) * 300) / this.k));
        this.j.start();
    }

    private void k() {
        if (this.j != null && this.j.isRunning()) {
            this.j.cancel();
            this.j.removeAllUpdateListeners();
            this.j.removeAllListeners();
            this.j = null;
        }
    }

    private void l() {
        this.i = ObjectAnimator.ofFloat(this.f3392f, "alpha", 0.0f, 1.0f);
        this.i.setDuration(150L);
        this.i.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                c.this.n();
            }
        });
        this.i.start();
    }

    private void m() {
        if (this.i != null && this.i.isRunning()) {
            this.i.removeAllListeners();
            this.i.removeAllUpdateListeners();
            this.i.cancel();
            this.i = null;
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        o();
        a(this.w, 3000);
    }

    /* access modifiers changed from: private */
    public void o() {
        a(this.w);
        if (this.f3394h != null && this.f3394h.isRunning()) {
            this.f3394h.removeAllListeners();
            this.f3394h.removeAllUpdateListeners();
            this.f3394h.cancel();
            this.f3394h = null;
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        this.f3394h = ObjectAnimator.ofFloat(this.f3392f, "alpha", 1.0f, 0.44f);
        this.f3394h.setDuration(500L);
        this.f3394h.start();
    }

    private void q() {
        if (this.f3392f != null) {
            this.f3392f.setOnTouchListener(null);
            this.f3392f = null;
        }
        this.f3393g = null;
        this.f3390d = null;
        this.f3391e = null;
        this.q = null;
        g.a.a(this.f3389c).b(this);
    }

    public void a(String[] strArr) {
        d.a("FloatHelperMgr", "enter App");
        if (c(strArr)) {
            e();
        } else {
            g();
        }
    }

    public void b(String[] strArr) {
    }

    public boolean c(String[] strArr) {
        if (strArr == null) {
            strArr = b(this.f3389c);
        }
        if (strArr == null || TextUtils.isEmpty(strArr[0]) || !FloatDisplayController.getFloatSearchWindowIsShow(this.f3389c)) {
            return true;
        }
        List c2 = c(this.f3389c);
        if (c2 == null || !c2.contains(strArr[0])) {
            return false;
        }
        return true;
    }

    public static String[] b(Context context) {
        String[] strArr;
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ServiceManagerNative.ACTIVITY);
        if (Build.VERSION.SDK_INT >= 21) {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses == null || runningAppProcesses.isEmpty()) {
                return null;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = runningAppProcesses.get(0);
            if (runningAppProcessInfo == null) {
                d.a("FloatHelperMgr", "L top running null");
                strArr = null;
            } else if (runningAppProcessInfo.importance != 100) {
                d.a("FloatHelperMgr", "L top running process:%s importance not IMPORTANCE_FOREGROUND" + runningAppProcessInfo.processName);
                strArr = null;
            } else if (runningAppProcessInfo.pkgList == null || runningAppProcessInfo.pkgList.length <= 0) {
                d.a("FloatHelperMgr", "L top running info null or empty pkgList for process:%s" + runningAppProcessInfo.processName);
                strArr = null;
            } else {
                strArr = runningAppProcessInfo.pkgList;
            }
            return strArr;
        }
        List<ActivityManager.RunningTaskInfo> runningTasks = activityManager.getRunningTasks(1);
        if (runningTasks == null || runningTasks.isEmpty()) {
            d.a("FloatHelperMgr", "top running taskList empty");
            return null;
        }
        ActivityManager.RunningTaskInfo runningTaskInfo = runningTasks.get(0);
        if (runningTaskInfo != null) {
            ComponentName componentName = runningTaskInfo.topActivity;
            if (componentName != null) {
                String packageName = componentName.getPackageName();
                d.b("FloatHelperMgr", "top running %s" + packageName);
                return new String[]{packageName};
            }
            d.a("FloatHelperMgr", "top running null topActivity");
            return null;
        }
        d.a("FloatHelperMgr", "top running first taskInfo is null");
        return null;
    }

    public void a(Runnable runnable, long j2) {
        f3388b.postDelayed(runnable, j2);
    }

    public void a(Runnable runnable) {
        f3388b.removeCallbacks(runnable);
    }

    public static List c(Context context) {
        ActivityInfo activityInfo;
        if (context == null) {
            return null;
        }
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
            if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
                ArrayList arrayList = new ArrayList();
                for (ResolveInfo next : queryIntentActivities) {
                    if (!(next == null || (activityInfo = next.activityInfo) == null)) {
                        arrayList.add(activityInfo.packageName);
                    }
                }
                return arrayList;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public static boolean b() {
        return Build.VERSION.SDK_INT >= 22;
    }

    public boolean c() {
        if (Build.VERSION.SDK_INT >= 23 && !d(this.f3389c.getApplicationContext())) {
            return false;
        }
        if (!d() || e(this.f3389c)) {
            return true;
        }
        return false;
    }

    public static boolean d(Context context) {
        Boolean bool;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                bool = (Boolean) Settings.class.getDeclaredMethod("canDrawOverlays", Context.class).invoke(null, context);
            } catch (Exception e2) {
                if (d.a()) {
                    d.b("FloatHelperMgr", "Check canDrawOverlays exception : " + e2.toString());
                }
            }
            return bool.booleanValue();
        }
        bool = false;
        return bool.booleanValue();
    }

    public static boolean d() {
        try {
            a a2 = a.a();
            if (a2 == null || a2.a("ro.build.hw_emui_api_level", null) == null) {
                return false;
            }
            return true;
        } catch (IOException e2) {
            if (!d.a()) {
                return false;
            }
            d.b("FloatHelperMgr", "check is emui exception : " + e2.toString());
            return false;
        }
    }

    public static boolean e(Context context) {
        boolean z;
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 23) {
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService("appops");
            try {
                Method declaredMethod = AppOpsManager.class.getDeclaredMethod("checkOp", Integer.TYPE, Integer.TYPE, String.class);
                if (declaredMethod != null) {
                    if (((Integer) declaredMethod.invoke(appOpsManager, 24, Integer.valueOf(Binder.getCallingUid()), context.getPackageName())).intValue() == 0) {
                        z = true;
                    } else {
                        z = false;
                    }
                } else {
                    z = true;
                }
                return z;
            } catch (Exception e2) {
                if (!d.a()) {
                    return true;
                }
                d.b("FloatHelperMgr", "check emui float window permission exception : " + e2.toString());
                return true;
            }
        } else if (Build.VERSION.SDK_INT >= 23) {
            return d(context);
        } else {
            return true;
        }
    }
}
