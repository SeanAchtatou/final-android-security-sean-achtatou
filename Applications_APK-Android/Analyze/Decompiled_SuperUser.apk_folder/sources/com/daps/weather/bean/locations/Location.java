package com.daps.weather.bean.locations;

import java.io.Serializable;

public class Location implements Serializable {
    private static final long serialVersionUID = 8927986883795818673L;
    private String EnglishName;
    private String Key;
    private int Rank;
    private String Type;
    private int Version;

    public String getKey() {
        return this.Key;
    }

    public void setKey(String str) {
        this.Key = str;
    }

    public int getRank() {
        return this.Rank;
    }

    public void setRank(int i) {
        this.Rank = i;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String str) {
        this.Type = str;
    }

    public String getEnglishName() {
        return this.EnglishName;
    }

    public void setEnglishName(String str) {
        this.EnglishName = str;
    }

    public int getVersion() {
        return this.Version;
    }

    public void setVersion(int i) {
        this.Version = i;
    }
}
