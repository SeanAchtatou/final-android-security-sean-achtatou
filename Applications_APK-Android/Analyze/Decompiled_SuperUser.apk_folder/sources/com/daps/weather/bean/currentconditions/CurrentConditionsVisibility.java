package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentConditionsVisibility implements Serializable {
    private static final long serialVersionUID = 3911560314940582834L;
    private CurrentConditionsVisibilityImperial Imperial;
    private CurrentConditionsVisibilityMetric Metric;

    public CurrentConditionsVisibilityMetric getMetric() {
        return this.Metric;
    }

    public void setMetric(CurrentConditionsVisibilityMetric currentConditionsVisibilityMetric) {
        this.Metric = currentConditionsVisibilityMetric;
    }

    public CurrentConditionsVisibilityImperial getImperial() {
        return this.Imperial;
    }

    public void setImperial(CurrentConditionsVisibilityImperial currentConditionsVisibilityImperial) {
        this.Imperial = currentConditionsVisibilityImperial;
    }
}
