package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentConditionsWindSpeed implements Serializable {
    private static final long serialVersionUID = 9185583150320205476L;
    private CurrentConditionsWindSpeedImperial Imperial;
    private CurrentConditionsWindSpeedMetric Metric;

    public CurrentConditionsWindSpeedMetric getMetric() {
        return this.Metric;
    }

    public void setMetric(CurrentConditionsWindSpeedMetric currentConditionsWindSpeedMetric) {
        this.Metric = currentConditionsWindSpeedMetric;
    }

    public CurrentConditionsWindSpeedImperial getImperial() {
        return this.Imperial;
    }

    public void setImperial(CurrentConditionsWindSpeedImperial currentConditionsWindSpeedImperial) {
        this.Imperial = currentConditionsWindSpeedImperial;
    }
}
