package com.daps.weather.bean.forecasts;

import java.io.Serializable;

public class Forecast implements Serializable {
    private static final long serialVersionUID = -5350853813446305984L;
    private ForecastsDailyForecasts[] DailyForecasts;

    public ForecastsDailyForecasts[] getDailyForecasts() {
        return this.DailyForecasts;
    }

    public void setDailyForecasts(ForecastsDailyForecasts[] forecastsDailyForecastsArr) {
        this.DailyForecasts = forecastsDailyForecastsArr;
    }
}
