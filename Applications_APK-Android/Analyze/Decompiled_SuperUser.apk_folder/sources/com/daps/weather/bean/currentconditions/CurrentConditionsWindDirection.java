package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentConditionsWindDirection implements Serializable {
    private static final long serialVersionUID = -7570521318835037590L;
    private int Degrees;
    private String English;
    private String Localized;

    public int getDegrees() {
        return this.Degrees;
    }

    public void setDegrees(int i) {
        this.Degrees = i;
    }

    public String getLocalized() {
        return this.Localized;
    }

    public void setLocalized(String str) {
        this.Localized = str;
    }

    public String getEnglish() {
        return this.English;
    }

    public void setEnglish(String str) {
        this.English = str;
    }
}
