package com.daps.weather.bean.forecasts;

import java.io.Serializable;

public class ForecastsDailyForecastsTemperature implements Serializable {
    private static final long serialVersionUID = 1585812722703681214L;
    private ForecastsDailyForecastsTemperatureMaximum Maximum;
    private ForecastsDailyForecastsTemperatureMinimum Minimum;

    public ForecastsDailyForecastsTemperatureMaximum getMaximum() {
        return this.Maximum;
    }

    public void setMaximum(ForecastsDailyForecastsTemperatureMaximum forecastsDailyForecastsTemperatureMaximum) {
        this.Maximum = forecastsDailyForecastsTemperatureMaximum;
    }

    public ForecastsDailyForecastsTemperatureMinimum getMinimum() {
        return this.Minimum;
    }

    public void setMinimum(ForecastsDailyForecastsTemperatureMinimum forecastsDailyForecastsTemperatureMinimum) {
        this.Minimum = forecastsDailyForecastsTemperatureMinimum;
    }
}
