package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentConditionsTemperatureImperial implements Serializable {
    private static final long serialVersionUID = -5832548894228455593L;
    private String Unit;
    private int UnitType;
    private int Value;

    public int getValue() {
        return this.Value;
    }

    public void setValue(int i) {
        this.Value = i;
    }

    public String getUnit() {
        return this.Unit;
    }

    public void setUnit(String str) {
        this.Unit = str;
    }

    public int getUnitType() {
        return this.UnitType;
    }

    public void setUnitType(int i) {
        this.UnitType = i;
    }
}
