package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentConditionsWind implements Serializable {
    private static final long serialVersionUID = 8419567117272844379L;
    private CurrentConditionsWindDirection Direction;
    private CurrentConditionsWindSpeed Speed;

    public CurrentConditionsWindSpeed getSpeed() {
        return this.Speed;
    }

    public void setSpeed(CurrentConditionsWindSpeed currentConditionsWindSpeed) {
        this.Speed = currentConditionsWindSpeed;
    }

    public CurrentConditionsWindDirection getDirection() {
        return this.Direction;
    }

    public void setDirection(CurrentConditionsWindDirection currentConditionsWindDirection) {
        this.Direction = currentConditionsWindDirection;
    }
}
