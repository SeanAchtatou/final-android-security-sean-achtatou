package com.daps.weather.bean.forecasts;

import java.io.Serializable;

public class ForecastsDailyForecastsDay implements Serializable {
    private static final long serialVersionUID = -8250601575216304003L;
    private int Icon;
    private ForecastsDailyForecastsDayRain Rain;

    public ForecastsDailyForecastsDayRain getRain() {
        return this.Rain;
    }

    public void setRain(ForecastsDailyForecastsDayRain forecastsDailyForecastsDayRain) {
        this.Rain = forecastsDailyForecastsDayRain;
    }

    public int getIcon() {
        return this.Icon;
    }

    public void setIcon(int i) {
        this.Icon = i;
    }
}
