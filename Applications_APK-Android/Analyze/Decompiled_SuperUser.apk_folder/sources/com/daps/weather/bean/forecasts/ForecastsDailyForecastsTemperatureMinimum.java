package com.daps.weather.bean.forecasts;

import java.io.Serializable;

public class ForecastsDailyForecastsTemperatureMinimum implements Serializable {
    private static final long serialVersionUID = 753631421683341866L;
    private String Unit;
    private int UnitType;
    private double Value;

    public double getValue() {
        return this.Value;
    }

    public void setValue(double d2) {
        this.Value = d2;
    }

    public String getUnit() {
        return this.Unit;
    }

    public void setUnit(String str) {
        this.Unit = str;
    }

    public int getUnitType() {
        return this.UnitType;
    }

    public void setUnitType(int i) {
        this.UnitType = i;
    }
}
