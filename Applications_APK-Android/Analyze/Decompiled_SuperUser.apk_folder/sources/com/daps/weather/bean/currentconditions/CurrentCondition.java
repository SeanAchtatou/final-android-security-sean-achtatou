package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentCondition implements Serializable {
    private static final long serialVersionUID = 7013519560122361957L;
    private int EpochTime;
    private boolean IsDayTime;
    private String Link;
    private String MobileLink;
    private int RelativeHumidity;
    private CurrentConditionsTemperature Temperature;
    private int UVIndex;
    private CurrentConditionsVisibility Visibility;
    private int WeatherIcon;
    private String WeatherText;
    private CurrentConditionsWind Wind;

    public String getWeatherText() {
        return this.WeatherText;
    }

    public void setWeatherText(String str) {
        this.WeatherText = str;
    }

    public String getMobileLink() {
        return this.MobileLink;
    }

    public void setMobileLink(String str) {
        this.MobileLink = str;
    }

    public CurrentConditionsWind getWind() {
        return this.Wind;
    }

    public void setWind(CurrentConditionsWind currentConditionsWind) {
        this.Wind = currentConditionsWind;
    }

    public int getEpochTime() {
        return this.EpochTime;
    }

    public void setEpochTime(int i) {
        this.EpochTime = i;
    }

    public int getRelativeHumidity() {
        return this.RelativeHumidity;
    }

    public void setRelativeHumidity(int i) {
        this.RelativeHumidity = i;
    }

    public CurrentConditionsTemperature getTemperature() {
        return this.Temperature;
    }

    public void setTemperature(CurrentConditionsTemperature currentConditionsTemperature) {
        this.Temperature = currentConditionsTemperature;
    }

    public String getLink() {
        return this.Link;
    }

    public void setLink(String str) {
        this.Link = str;
    }

    public int getWeatherIcon() {
        return this.WeatherIcon;
    }

    public void setWeatherIcon(int i) {
        this.WeatherIcon = i;
    }

    public CurrentConditionsVisibility getVisibility() {
        return this.Visibility;
    }

    public void setVisibility(CurrentConditionsVisibility currentConditionsVisibility) {
        this.Visibility = currentConditionsVisibility;
    }

    public boolean getIsDayTime() {
        return this.IsDayTime;
    }

    public void setIsDayTime(boolean z) {
        this.IsDayTime = z;
    }
}
