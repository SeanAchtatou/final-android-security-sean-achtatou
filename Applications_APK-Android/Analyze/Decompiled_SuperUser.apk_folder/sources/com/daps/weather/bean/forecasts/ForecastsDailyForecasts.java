package com.daps.weather.bean.forecasts;

import java.io.Serializable;

public class ForecastsDailyForecasts implements Serializable {
    private static final long serialVersionUID = 4819433578810583257L;
    private String Date;
    private ForecastsDailyForecastsDay Day;
    private int EpochDate;
    private String Link;
    private String MobileLink;
    private ForecastsDailyForecastsTemperature Temperature;

    public String getMobileLink() {
        return this.MobileLink;
    }

    public void setMobileLink(String str) {
        this.MobileLink = str;
    }

    public ForecastsDailyForecastsTemperature getTemperature() {
        return this.Temperature;
    }

    public void setTemperature(ForecastsDailyForecastsTemperature forecastsDailyForecastsTemperature) {
        this.Temperature = forecastsDailyForecastsTemperature;
    }

    public ForecastsDailyForecastsDay getDay() {
        return this.Day;
    }

    public void setDay(ForecastsDailyForecastsDay forecastsDailyForecastsDay) {
        this.Day = forecastsDailyForecastsDay;
    }

    public String getLink() {
        return this.Link;
    }

    public void setLink(String str) {
        this.Link = str;
    }

    public String getDate() {
        return this.Date;
    }

    public void setDate(String str) {
        this.Date = str;
    }

    public int getEpochDate() {
        return this.EpochDate;
    }

    public void setEpochDate(int i) {
        this.EpochDate = i;
    }
}
