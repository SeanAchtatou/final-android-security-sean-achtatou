package com.daps.weather.bean.currentconditions;

import java.io.Serializable;

public class CurrentConditionsTemperature implements Serializable {
    private static final long serialVersionUID = -1732554240130471060L;
    private CurrentConditionsTemperatureImperial Imperial;
    private CurrentConditionsTemperatureMetric Metric;

    public CurrentConditionsTemperatureMetric getMetric() {
        return this.Metric;
    }

    public void setMetric(CurrentConditionsTemperatureMetric currentConditionsTemperatureMetric) {
        this.Metric = currentConditionsTemperatureMetric;
    }

    public CurrentConditionsTemperatureImperial getImperial() {
        return this.Imperial;
    }

    public void setImperial(CurrentConditionsTemperatureImperial currentConditionsTemperatureImperial) {
        this.Imperial = currentConditionsTemperatureImperial;
    }
}
