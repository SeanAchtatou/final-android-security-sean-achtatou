package com.daps.weather.weathercard;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.daps.weather.DapWeatherActivity;
import com.daps.weather.a;
import e.a;

public class WeatherEnterImageView extends ImageView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3473a;

    public WeatherEnterImageView(Context context) {
        super(context);
        a(context);
    }

    public WeatherEnterImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public WeatherEnterImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    private void a(Context context) {
        this.f3473a = context;
        setBackgroundResource(a.c.hz);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WeatherEnterImageView.this.f3473a.startActivity(new Intent(WeatherEnterImageView.this.f3473a, DapWeatherActivity.class));
                e.a.b(WeatherEnterImageView.this.f3473a, a.C0097a.ENTER_IV);
            }
        });
    }
}
