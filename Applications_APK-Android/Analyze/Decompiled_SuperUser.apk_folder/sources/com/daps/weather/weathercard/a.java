package com.daps.weather.weathercard;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.daps.weather.DapWeatherActivity;
import com.daps.weather.a;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.daps.weather.base.f;
import com.daps.weather.base.g;
import com.daps.weather.bean.currentconditions.CurrentCondition;
import com.daps.weather.bean.locations.Location;
import com.daps.weather.location.c;
import e.a;
import f.b;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/* compiled from: WeatherView */
public class a extends LinearLayout implements View.OnClickListener, Observer {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f3475a = a.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Context f3476b;

    /* renamed from: c  reason: collision with root package name */
    private LayoutInflater f3477c;

    /* renamed from: d  reason: collision with root package name */
    private View f3478d;

    /* renamed from: e  reason: collision with root package name */
    private ImageView f3479e;

    /* renamed from: f  reason: collision with root package name */
    private TextView f3480f;

    /* renamed from: g  reason: collision with root package name */
    private TextView f3481g;

    /* renamed from: h  reason: collision with root package name */
    private TextView f3482h;
    private f i;
    private SQLiteDatabase j;
    private boolean k = false;

    public a(Context context) {
        super(context);
        this.f3476b = context;
        this.f3477c = LayoutInflater.from(context);
        c.a().addObserver(this);
        b();
        c();
    }

    private void b() {
        this.i = f.a(this.f3476b);
        this.j = this.i.getReadableDatabase();
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3476b = context;
        this.f3477c = LayoutInflater.from(context);
        b();
        c();
    }

    public a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f3476b = context;
        this.f3477c = LayoutInflater.from(context);
        b();
        c();
    }

    private void c() {
        this.k = false;
        this.f3478d = this.f3477c.inflate(a.e.b0, (ViewGroup) null);
        this.f3479e = (ImageView) this.f3478d.findViewById(a.d.i3);
        this.f3480f = (TextView) this.f3478d.findViewById(a.d.i5);
        this.f3481g = (TextView) this.f3478d.findViewById(a.d.i6);
        this.f3482h = (TextView) this.f3478d.findViewById(a.d.i4);
        removeAllViews();
        addView(this.f3478d);
        this.f3478d.setOnClickListener(this);
        e.a.a(this.f3476b, a.C0097a.VIEW);
    }

    public void load() {
        e();
    }

    private void d() {
        List a2 = g.a(this.f3476b, this.j);
        List b2 = g.b(this.f3476b, this.j);
        if (a2 == null || a2.size() <= 0) {
            d.a(f3475a, "数据库中没有location数据");
        } else {
            a(a2, 1);
        }
        if (b2 == null || b2.size() <= 0) {
            d.a(f3475a, "数据库中没有currentCondition数据");
        } else {
            a(b2);
        }
    }

    private void e() {
        h();
        if (TextUtils.isEmpty(SharedPrefsUtils.b(this.f3476b)) || TextUtils.isEmpty(SharedPrefsUtils.c(this.f3476b))) {
            d.a(f3475a, "经纬度是空");
        } else if (e.a(this.f3476b)) {
            d.a(f3475a, "有网络，拿线上数据");
            f();
        } else {
            d.a(f3475a, "无网络，拿天气数据");
            d();
        }
    }

    private void f() {
        try {
            b.a(this.f3476b).a(SharedPrefsUtils.b(this.f3476b), SharedPrefsUtils.c(this.f3476b), new b.a() {
                public void a(List list) {
                    d.a(a.f3475a, "777");
                    a.this.a(list, 2);
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
            e.a.a(this.f3476b, a.C0097a.VIEW, e2.getMessage());
            d.b(f3475a, "拉取失败：" + e2.getMessage());
            d();
        }
    }

    /* access modifiers changed from: private */
    public void a(List list, int i2) {
        if (list != null && list.size() > 0) {
            Location location = (Location) list.get(0);
            String key = location.getKey();
            if (!TextUtils.isEmpty(key)) {
                SharedPrefsUtils.a(this.f3476b, key);
                e.a.a(this.f3476b, a.C0097a.VIEW, i2);
                this.f3480f.setText(location.getEnglishName());
                g();
            }
        }
    }

    private void g() {
        h();
        try {
            b.a(this.f3476b).a(SharedPrefsUtils.a(this.f3476b), new b.a() {
                public void a(List list) {
                    a.this.a(list);
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(List list) {
        if (list != null && list.size() > 0) {
            CurrentCondition currentCondition = (CurrentCondition) list.get(0);
            if (currentCondition != null) {
                this.k = true;
                Bitmap a2 = e.a(this.f3476b, currentCondition.getWeatherIcon());
                if (a2 != null) {
                    this.f3479e.setImageBitmap(a2);
                }
                this.f3482h.setText(String.valueOf(e.b(currentCondition.getTemperature().getMetric().getValue())) + "°");
                this.f3481g.setText(currentCondition.getWeatherText());
                return;
            }
            this.k = false;
        }
    }

    private void h() {
    }

    public void onClick(View view) {
        this.f3476b.startActivity(new Intent(this.f3476b, DapWeatherActivity.class));
        e.a.b(this.f3476b, a.C0097a.VIEW);
    }

    public void update(Observable observable, Object obj) {
        d.a(f3475a, "obser_view");
        if (!isShown() || this.k) {
            d.a(f3475a, "obser_no_load");
            return;
        }
        d.a(f3475a, "obser_load");
        load();
    }
}
