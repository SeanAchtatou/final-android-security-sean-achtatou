package com.daps.weather.weathercard;

import android.content.Context;
import android.util.AttributeSet;

public class DapWeatherView extends a {
    public DapWeatherView(Context context) {
        super(context);
    }

    public DapWeatherView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DapWeatherView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void load() {
        super.load();
    }
}
