package com.daps.weather.weathercard;

import android.content.Context;
import android.util.AttributeSet;

public class DapWeatherEnterImageView extends WeatherEnterImageView {
    public DapWeatherEnterImageView(Context context) {
        super(context);
    }

    public DapWeatherEnterImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DapWeatherEnterImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
