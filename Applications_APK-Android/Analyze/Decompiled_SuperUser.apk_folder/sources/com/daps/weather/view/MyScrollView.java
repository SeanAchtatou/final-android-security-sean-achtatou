package com.daps.weather.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class MyScrollView extends ScrollView {

    /* renamed from: a  reason: collision with root package name */
    private a f3456a = null;

    public interface a {
        void a(int i, int i2, int i3, int i4);
    }

    public MyScrollView(Context context) {
        super(context);
    }

    public MyScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MyScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.f3456a != null) {
            this.f3456a.a(i, i2, i3, i4);
        }
    }

    public void setOnScrollViewListener(a aVar) {
        this.f3456a = aVar;
    }
}
