package com.daps.weather.view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ag;
import android.support.v4.widget.EdgeEffectCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Scroller;
import com.daps.weather.a;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/* compiled from: HorizontalListView */
public class a extends AdapterView {
    /* access modifiers changed from: private */
    public boolean A = false;
    private boolean B = false;
    /* access modifiers changed from: private */
    public View.OnClickListener C;
    private DataSetObserver D = new DataSetObserver() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.daps.weather.view.a.a(com.daps.weather.view.a, boolean):boolean
         arg types: [com.daps.weather.view.a, int]
         candidates:
          com.daps.weather.view.a.a(int, int):void
          com.daps.weather.view.a.a(int, android.view.View):void
          com.daps.weather.view.a.a(android.content.Context, android.util.AttributeSet):void
          com.daps.weather.view.a.a(android.graphics.Canvas, android.graphics.Rect):void
          com.daps.weather.view.a.a(android.view.View, int):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, int):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, com.daps.weather.view.a$d$a):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, java.lang.Boolean):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.daps.weather.view.a.b(com.daps.weather.view.a, boolean):boolean
         arg types: [com.daps.weather.view.a, int]
         candidates:
          com.daps.weather.view.a.b(int, int):void
          com.daps.weather.view.a.b(com.daps.weather.view.a, boolean):boolean */
        public void onChanged() {
            boolean unused = a.this.i = true;
            boolean unused2 = a.this.u = false;
            a.this.f();
            a.this.invalidate();
            a.this.requestLayout();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.daps.weather.view.a.b(com.daps.weather.view.a, boolean):boolean
         arg types: [com.daps.weather.view.a, int]
         candidates:
          com.daps.weather.view.a.b(int, int):void
          com.daps.weather.view.a.b(com.daps.weather.view.a, boolean):boolean */
        public void onInvalidated() {
            boolean unused = a.this.u = false;
            a.this.f();
            a.this.c();
            a.this.invalidate();
            a.this.requestLayout();
        }
    };
    private Runnable E = new Runnable() {
        public void run() {
            a.this.requestLayout();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final C0046a f3457a = new C0046a();

    /* renamed from: b  reason: collision with root package name */
    protected Scroller f3458b = new Scroller(getContext());

    /* renamed from: c  reason: collision with root package name */
    protected ListAdapter f3459c;

    /* renamed from: d  reason: collision with root package name */
    protected int f3460d;

    /* renamed from: e  reason: collision with root package name */
    protected int f3461e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public GestureDetector f3462f;

    /* renamed from: g  reason: collision with root package name */
    private int f3463g;

    /* renamed from: h  reason: collision with root package name */
    private List f3464h = new ArrayList();
    /* access modifiers changed from: private */
    public boolean i = false;
    private Rect j = new Rect();
    private View k = null;
    private int l = 0;
    private Drawable m = null;
    private Integer n = null;
    private int o = Integer.MAX_VALUE;
    /* access modifiers changed from: private */
    public int p;
    private int q;
    private int r;
    private e s = null;
    private int t = 0;
    /* access modifiers changed from: private */
    public boolean u = false;
    private d v = null;
    private d.C0047a w = d.C0047a.SCROLL_STATE_IDLE;
    private EdgeEffectCompat x;
    private EdgeEffectCompat y;
    private int z;

    /* compiled from: HorizontalListView */
    public interface d {

        /* renamed from: com.daps.weather.view.a$d$a  reason: collision with other inner class name */
        /* compiled from: HorizontalListView */
        public enum C0047a {
            SCROLL_STATE_IDLE,
            SCROLL_STATE_TOUCH_SCROLL,
            SCROLL_STATE_FLING
        }

        void a(C0047a aVar);
    }

    /* compiled from: HorizontalListView */
    public interface e {
        void a();
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.x = new EdgeEffectCompat(context);
        this.y = new EdgeEffectCompat(context);
        this.f3462f = new GestureDetector(context, this.f3457a);
        a();
        b();
        a(context, attributeSet);
        setWillNotDraw(false);
        if (Build.VERSION.SDK_INT >= 11) {
            b.a(this.f3458b, 0.009f);
        }
    }

    private void a() {
        setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return a.this.f3462f.onTouchEvent(motionEvent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(Boolean bool) {
        if (this.B != bool.booleanValue()) {
            for (View view = this; view.getParent() instanceof View; view = (View) view.getParent()) {
                if ((view.getParent() instanceof ListView) || (view.getParent() instanceof ScrollView)) {
                    view.getParent().requestDisallowInterceptTouchEvent(bool.booleanValue());
                    this.B = bool.booleanValue();
                    return;
                }
            }
        }
    }

    private void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.g.HorizontalListView);
            Drawable drawable = obtainStyledAttributes.getDrawable(a.g.HorizontalListView_android_divider);
            if (drawable != null) {
                setDivider(drawable);
            }
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(a.g.HorizontalListView_dividerWidth, 0);
            if (dimensionPixelSize != 0) {
                setDividerWidth(dimensionPixelSize);
            }
            obtainStyledAttributes.recycle();
        }
    }

    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("BUNDLE_ID_PARENT_STATE", super.onSaveInstanceState());
        bundle.putInt("BUNDLE_ID_CURRENT_X", this.f3460d);
        return bundle;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.n = Integer.valueOf(bundle.getInt("BUNDLE_ID_CURRENT_X"));
            super.onRestoreInstanceState(bundle.getParcelable("BUNDLE_ID_PARENT_STATE"));
        }
    }

    public void setDivider(Drawable drawable) {
        this.m = drawable;
        if (drawable != null) {
            setDividerWidth(drawable.getIntrinsicWidth());
        } else {
            setDividerWidth(0);
        }
    }

    public void setDividerWidth(int i2) {
        this.l = i2;
        requestLayout();
        invalidate();
    }

    private void b() {
        this.p = -1;
        this.q = -1;
        this.f3463g = 0;
        this.f3460d = 0;
        this.f3461e = 0;
        this.o = Integer.MAX_VALUE;
        setCurrentScrollState(d.C0047a.SCROLL_STATE_IDLE);
    }

    /* access modifiers changed from: private */
    public void c() {
        b();
        removeAllViewsInLayout();
        requestLayout();
    }

    public void setSelection(int i2) {
        this.r = i2;
    }

    public View getSelectedView() {
        return g(this.r);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (this.f3459c != null) {
            this.f3459c.unregisterDataSetObserver(this.D);
        }
        if (listAdapter != null) {
            this.u = false;
            this.f3459c = listAdapter;
            this.f3459c.registerDataSetObserver(this.D);
        }
        a(this.f3459c.getViewTypeCount());
        c();
    }

    public ListAdapter getAdapter() {
        return this.f3459c;
    }

    private void a(int i2) {
        this.f3464h.clear();
        for (int i3 = 0; i3 < i2; i3++) {
            this.f3464h.add(new LinkedList());
        }
    }

    private View b(int i2) {
        int itemViewType = this.f3459c.getItemViewType(i2);
        if (c(itemViewType)) {
            return (View) ((Queue) this.f3464h.get(itemViewType)).poll();
        }
        return null;
    }

    private void a(int i2, View view) {
        int itemViewType = this.f3459c.getItemViewType(i2);
        if (c(itemViewType)) {
            ((Queue) this.f3464h.get(itemViewType)).offer(view);
        }
    }

    private boolean c(int i2) {
        return i2 < this.f3464h.size();
    }

    private void a(View view, int i2) {
        addViewInLayout(view, i2, b(view), true);
        a(view);
    }

    private void a(View view) {
        int makeMeasureSpec;
        ViewGroup.LayoutParams b2 = b(view);
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(this.z, getPaddingTop() + getPaddingBottom(), b2.height);
        if (b2.width > 0) {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(b2.width, 1073741824);
        } else {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        view.measure(makeMeasureSpec, childMeasureSpec);
    }

    private ViewGroup.LayoutParams b(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            return new ViewGroup.LayoutParams(-2, -1);
        }
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"WrongCall"})
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.f3459c != null) {
            invalidate();
            if (this.i) {
                int i6 = this.f3460d;
                b();
                removeAllViewsInLayout();
                this.f3461e = i6;
                this.i = false;
            }
            if (this.n != null) {
                this.f3461e = this.n.intValue();
                this.n = null;
            }
            if (this.f3458b.computeScrollOffset()) {
                this.f3461e = this.f3458b.getCurrX();
            }
            if (this.f3461e < 0) {
                this.f3461e = 0;
                if (this.x.a()) {
                    this.x.a((int) d());
                }
                this.f3458b.forceFinished(true);
                setCurrentScrollState(d.C0047a.SCROLL_STATE_IDLE);
            } else if (this.f3461e > this.o) {
                this.f3461e = this.o;
                if (this.y.a()) {
                    this.y.a((int) d());
                }
                this.f3458b.forceFinished(true);
                setCurrentScrollState(d.C0047a.SCROLL_STATE_IDLE);
            }
            int i7 = this.f3460d - this.f3461e;
            e(i7);
            d(i7);
            f(i7);
            this.f3460d = this.f3461e;
            if (e()) {
                onLayout(z2, i2, i3, i4, i5);
            } else if (!this.f3458b.isFinished()) {
                ag.a(this, this.E);
            } else if (this.w == d.C0047a.SCROLL_STATE_FLING) {
                setCurrentScrollState(d.C0047a.SCROLL_STATE_IDLE);
            }
        }
    }

    /* access modifiers changed from: protected */
    public float getLeftFadingEdgeStrength() {
        int horizontalFadingEdgeLength = getHorizontalFadingEdgeLength();
        if (this.f3460d == 0) {
            return 0.0f;
        }
        if (this.f3460d < horizontalFadingEdgeLength) {
            return ((float) this.f3460d) / ((float) horizontalFadingEdgeLength);
        }
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getRightFadingEdgeStrength() {
        int horizontalFadingEdgeLength = getHorizontalFadingEdgeLength();
        if (this.f3460d == this.o) {
            return 0.0f;
        }
        if (this.o - this.f3460d < horizontalFadingEdgeLength) {
            return ((float) (this.o - this.f3460d)) / ((float) horizontalFadingEdgeLength);
        }
        return 1.0f;
    }

    private float d() {
        if (Build.VERSION.SDK_INT >= 14) {
            return c.a(this.f3458b);
        }
        return 30.0f;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.z = i3;
    }

    private boolean e() {
        View rightmostChild;
        if (!h(this.q) || (rightmostChild = getRightmostChild()) == null) {
            return false;
        }
        int i2 = this.o;
        this.o = ((rightmostChild.getRight() - getPaddingLeft()) + this.f3460d) - getRenderWidth();
        if (this.o < 0) {
            this.o = 0;
        }
        if (this.o != i2) {
            return true;
        }
        return false;
    }

    private void d(int i2) {
        int i3;
        int i4 = 0;
        View rightmostChild = getRightmostChild();
        if (rightmostChild != null) {
            i3 = rightmostChild.getRight();
        } else {
            i3 = 0;
        }
        a(i3, i2);
        View leftmostChild = getLeftmostChild();
        if (leftmostChild != null) {
            i4 = leftmostChild.getLeft();
        }
        b(i4, i2);
    }

    private void e(int i2) {
        int measuredWidth;
        View leftmostChild = getLeftmostChild();
        while (leftmostChild != null && leftmostChild.getRight() + i2 <= 0) {
            int i3 = this.f3463g;
            if (h(this.p)) {
                measuredWidth = leftmostChild.getMeasuredWidth();
            } else {
                measuredWidth = this.l + leftmostChild.getMeasuredWidth();
            }
            this.f3463g = measuredWidth + i3;
            a(this.p, leftmostChild);
            removeViewInLayout(leftmostChild);
            this.p++;
            leftmostChild = getLeftmostChild();
        }
        View rightmostChild = getRightmostChild();
        while (rightmostChild != null && rightmostChild.getLeft() + i2 >= getWidth()) {
            a(this.q, rightmostChild);
            removeViewInLayout(rightmostChild);
            this.q--;
            rightmostChild = getRightmostChild();
        }
    }

    private void a(int i2, int i3) {
        while (i2 + i3 + this.l < getWidth() && this.q + 1 < this.f3459c.getCount()) {
            this.q++;
            if (this.p < 0) {
                this.p = this.q;
            }
            View view = this.f3459c.getView(this.q, b(this.q), this);
            a(view, -1);
            i2 += (this.q == 0 ? 0 : this.l) + view.getMeasuredWidth();
            h();
        }
    }

    private void b(int i2, int i3) {
        while ((i2 + i3) - this.l > 0 && this.p >= 1) {
            this.p--;
            View view = this.f3459c.getView(this.p, b(this.p), this);
            a(view, 0);
            i2 -= this.p == 0 ? view.getMeasuredWidth() : this.l + view.getMeasuredWidth();
            this.f3463g -= i2 + i3 == 0 ? view.getMeasuredWidth() : this.l + view.getMeasuredWidth();
        }
    }

    private void f(int i2) {
        int childCount = getChildCount();
        if (childCount > 0) {
            this.f3463g += i2;
            int i3 = this.f3463g;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                int paddingLeft = getPaddingLeft() + i3;
                int paddingTop = getPaddingTop();
                childAt.layout(paddingLeft, paddingTop, childAt.getMeasuredWidth() + paddingLeft, childAt.getMeasuredHeight() + paddingTop);
                i3 += childAt.getMeasuredWidth() + this.l;
            }
        }
    }

    private View getLeftmostChild() {
        return getChildAt(0);
    }

    private View getRightmostChild() {
        return getChildAt(getChildCount() - 1);
    }

    private View g(int i2) {
        if (i2 < this.p || i2 > this.q) {
            return null;
        }
        return getChildAt(i2 - this.p);
    }

    /* access modifiers changed from: private */
    public int c(int i2, int i3) {
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).getHitRect(this.j);
            if (this.j.contains(i2, i3)) {
                return i4;
            }
        }
        return -1;
    }

    private boolean h(int i2) {
        return i2 == this.f3459c.getCount() + -1;
    }

    private int getRenderHeight() {
        return (getHeight() - getPaddingTop()) - getPaddingBottom();
    }

    private int getRenderWidth() {
        return (getWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public int getFirstVisiblePosition() {
        return this.p;
    }

    public int getLastVisiblePosition() {
        return this.q;
    }

    private void a(Canvas canvas) {
        if (this.x != null && !this.x.a() && i()) {
            int save = canvas.save();
            int height = getHeight();
            canvas.rotate(-90.0f, 0.0f, 0.0f);
            canvas.translate((float) ((-height) + getPaddingBottom()), 0.0f);
            this.x.a(getRenderHeight(), getRenderWidth());
            if (this.x.a(canvas)) {
                invalidate();
            }
            canvas.restoreToCount(save);
        } else if (this.y != null && !this.y.a() && i()) {
            int save2 = canvas.save();
            int width = getWidth();
            canvas.rotate(90.0f, 0.0f, 0.0f);
            canvas.translate((float) getPaddingTop(), (float) (-width));
            this.y.a(getRenderHeight(), getRenderWidth());
            if (this.y.a(canvas)) {
                invalidate();
            }
            canvas.restoreToCount(save2);
        }
    }

    private void b(Canvas canvas) {
        int childCount = getChildCount();
        Rect rect = this.j;
        this.j.top = getPaddingTop();
        this.j.bottom = this.j.top + getRenderHeight();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (i2 != childCount - 1 || !h(this.q)) {
                View childAt = getChildAt(i2);
                rect.left = childAt.getRight();
                rect.right = childAt.getRight() + this.l;
                if (rect.left < getPaddingLeft()) {
                    rect.left = getPaddingLeft();
                }
                if (rect.right > getWidth() - getPaddingRight()) {
                    rect.right = getWidth() - getPaddingRight();
                }
                a(canvas, rect);
                if (i2 == 0 && childAt.getLeft() > getPaddingLeft()) {
                    rect.left = getPaddingLeft();
                    rect.right = childAt.getLeft();
                    a(canvas, rect);
                }
            }
        }
    }

    private void a(Canvas canvas, Rect rect) {
        if (this.m != null) {
            this.m.setBounds(rect);
            this.m.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        b(canvas);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        a(canvas);
    }

    /* access modifiers changed from: protected */
    public void dispatchSetPressed(boolean z2) {
    }

    /* access modifiers changed from: protected */
    public boolean a(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.f3458b.fling(this.f3461e, 0, (int) (-f2), 0, 0, this.o, 0, 0);
        setCurrentScrollState(d.C0047a.SCROLL_STATE_FLING);
        requestLayout();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(MotionEvent motionEvent) {
        int c2;
        this.A = !this.f3458b.isFinished();
        this.f3458b.forceFinished(true);
        setCurrentScrollState(d.C0047a.SCROLL_STATE_IDLE);
        f();
        if (!this.A && (c2 = c((int) motionEvent.getX(), (int) motionEvent.getY())) >= 0) {
            this.k = getChildAt(c2);
            if (this.k != null) {
                this.k.setPressed(true);
                refreshDrawableState();
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.k != null) {
            this.k.setPressed(false);
            refreshDrawableState();
            this.k = null;
        }
    }

    /* renamed from: com.daps.weather.view.a$a  reason: collision with other inner class name */
    /* compiled from: HorizontalListView */
    private class C0046a extends GestureDetector.SimpleOnGestureListener {
        private C0046a() {
        }

        public boolean onDown(MotionEvent motionEvent) {
            return a.this.a(motionEvent);
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return a.this.a(motionEvent, motionEvent2, f2, f3);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.daps.weather.view.a.a(com.daps.weather.view.a, java.lang.Boolean):void
         arg types: [com.daps.weather.view.a, int]
         candidates:
          com.daps.weather.view.a.a(int, int):void
          com.daps.weather.view.a.a(int, android.view.View):void
          com.daps.weather.view.a.a(android.content.Context, android.util.AttributeSet):void
          com.daps.weather.view.a.a(android.graphics.Canvas, android.graphics.Rect):void
          com.daps.weather.view.a.a(android.view.View, int):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, int):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, com.daps.weather.view.a$d$a):void
          com.daps.weather.view.a.a(com.daps.weather.view.a, boolean):boolean
          com.daps.weather.view.a.a(com.daps.weather.view.a, java.lang.Boolean):void */
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            a.this.a((Boolean) true);
            a.this.setCurrentScrollState(d.C0047a.SCROLL_STATE_TOUCH_SCROLL);
            a.this.f();
            a.this.f3461e += (int) f2;
            a.this.i(Math.round(f2));
            a.this.requestLayout();
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            a.this.f();
            AdapterView.OnItemClickListener onItemClickListener = a.this.getOnItemClickListener();
            int a2 = a.this.c((int) motionEvent.getX(), (int) motionEvent.getY());
            if (a2 >= 0 && !a.this.A) {
                View childAt = a.this.getChildAt(a2);
                int e2 = a.this.p + a2;
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(a.this, childAt, e2, a.this.f3459c.getItemId(e2));
                    return true;
                }
            }
            if (a.this.C != null && !a.this.A) {
                a.this.C.onClick(a.this);
            }
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
            a.this.f();
            int a2 = a.this.c((int) motionEvent.getX(), (int) motionEvent.getY());
            if (a2 >= 0 && !a.this.A) {
                View childAt = a.this.getChildAt(a2);
                AdapterView.OnItemLongClickListener onItemLongClickListener = a.this.getOnItemLongClickListener();
                if (onItemLongClickListener != null) {
                    int e2 = a.this.p + a2;
                    if (onItemLongClickListener.onItemLongClick(a.this, childAt, e2, a.this.f3459c.getItemId(e2))) {
                        a.this.performHapticFeedback(0);
                    }
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            if (this.f3458b == null || this.f3458b.isFinished()) {
                setCurrentScrollState(d.C0047a.SCROLL_STATE_IDLE);
            }
            a((Boolean) false);
            g();
        } else if (motionEvent.getAction() == 3) {
            f();
            g();
            a((Boolean) false);
        }
        return super.onTouchEvent(motionEvent);
    }

    private void g() {
        if (this.x != null) {
            this.x.c();
        }
        if (this.y != null) {
            this.y.c();
        }
    }

    private void h() {
        if (this.s != null && this.f3459c != null && this.f3459c.getCount() - (this.q + 1) < this.t && !this.u) {
            this.u = true;
            this.s.a();
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.C = onClickListener;
    }

    public void setOnScrollStateChangedListener(d dVar) {
        this.v = dVar;
    }

    /* access modifiers changed from: private */
    public void setCurrentScrollState(d.C0047a aVar) {
        if (!(this.w == aVar || this.v == null)) {
            this.v.a(aVar);
        }
        this.w = aVar;
    }

    /* access modifiers changed from: private */
    public void i(int i2) {
        if (this.x != null && this.y != null) {
            int i3 = this.f3460d + i2;
            if (this.f3458b != null && !this.f3458b.isFinished()) {
                return;
            }
            if (i3 < 0) {
                this.x.a(((float) Math.abs(i2)) / ((float) getRenderWidth()));
                if (!this.y.a()) {
                    this.y.c();
                }
            } else if (i3 > this.o) {
                this.y.a(((float) Math.abs(i2)) / ((float) getRenderWidth()));
                if (!this.x.a()) {
                    this.x.c();
                }
            }
        }
    }

    private boolean i() {
        if (this.f3459c == null || this.f3459c.isEmpty() || this.o <= 0) {
            return false;
        }
        return true;
    }

    @TargetApi(11)
    /* compiled from: HorizontalListView */
    private static final class b {
        static {
            if (Build.VERSION.SDK_INT < 11) {
                throw new RuntimeException("Should not get to HoneycombPlus class unless sdk is >= 11!");
            }
        }

        public static void a(Scroller scroller, float f2) {
            if (scroller != null) {
                scroller.setFriction(f2);
            }
        }
    }

    @TargetApi(14)
    /* compiled from: HorizontalListView */
    private static final class c {
        static {
            if (Build.VERSION.SDK_INT < 14) {
                throw new RuntimeException("Should not get to IceCreamSandwichPlus class unless sdk is >= 14!");
            }
        }

        public static float a(Scroller scroller) {
            return scroller.getCurrVelocity();
        }
    }
}
