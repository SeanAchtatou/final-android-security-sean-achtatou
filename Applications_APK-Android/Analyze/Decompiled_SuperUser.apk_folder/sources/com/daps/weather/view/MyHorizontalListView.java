package com.daps.weather.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyHorizontalListView extends a {

    /* renamed from: a  reason: collision with root package name */
    public boolean f3455a = true;

    public MyHorizontalListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 2 || this.f3455a) {
            return super.dispatchTouchEvent(motionEvent);
        }
        return true;
    }
}
