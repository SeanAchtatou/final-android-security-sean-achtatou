package com.daps.weather.reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.daps.weather.DapWeatherActivity;
import com.daps.weather.base.d;
import com.daps.weather.location.DapWeatherLocationsService;
import com.daps.weather.notification.DapWeatherNotification;
import com.daps.weather.service.DapWeatherMsgService;
import e.a;

public class HandleBroadCastReciver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3447a = HandleBroadCastReciver.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private Context f3448b;

    public void onReceive(Context context, Intent intent) {
        this.f3448b = context;
        if (intent != null) {
            String stringExtra = intent.getStringExtra(DapWeatherActivity.f3350a);
            if (!TextUtils.isEmpty(stringExtra)) {
                d.a(f3447a, "添加通知栏回调");
                DapWeatherNotification.getInstance(context);
                DapWeatherNotification.a aVar = DapWeatherNotification.f3424a;
                if (aVar != null) {
                    d.a(f3447a, "listener");
                    if (!TextUtils.isEmpty(stringExtra)) {
                        aVar.a(Integer.parseInt(stringExtra));
                    }
                } else {
                    d.b(f3447a, "weatherlistener onclick is null");
                }
                a.b(context, stringExtra);
                Intent intent2 = new Intent(context, DapWeatherActivity.class);
                intent2.setFlags(805306368);
                intent2.putExtra(DapWeatherActivity.f3350a, stringExtra);
                context.startActivity(intent2);
                return;
            }
        }
        try {
            d.a(f3447a, "msgService被关闭了，开启");
            context.startService(new Intent(context, DapWeatherMsgService.class));
            a(context);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(Context context) {
        try {
            if (!com.daps.weather.location.d.isDestory) {
                d.a(f3447a, "开启locationservice");
                context.startService(new Intent(context, DapWeatherLocationsService.class));
                return;
            }
            d.a(f3447a, "Service在运行");
        } catch (Exception e2) {
            e2.printStackTrace();
            d.b(f3447a, e2.getMessage().toString());
        }
    }
}
