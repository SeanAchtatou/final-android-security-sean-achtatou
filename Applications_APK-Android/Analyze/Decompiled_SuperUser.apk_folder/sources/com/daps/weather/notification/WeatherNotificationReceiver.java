package com.daps.weather.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.daps.weather.DapWeatherActivity;
import com.daps.weather.base.d;

public class WeatherNotificationReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3431a = WeatherNotificationReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        d.a(f3431a, intent.getAction());
        if (!TextUtils.isEmpty(intent.getAction()) && intent.getAction().equals("schedulePage")) {
            Intent intent2 = new Intent();
            intent2.setFlags(268435456);
            intent2.setClass(context, DapWeatherActivity.class);
            context.startActivity(intent2);
            a.a(context).b(context);
        }
    }
}
