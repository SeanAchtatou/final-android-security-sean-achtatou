package com.daps.weather.notification;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.f;

public class DapWeatherNotification {

    /* renamed from: a  reason: collision with root package name */
    public static a f3424a;

    /* renamed from: b  reason: collision with root package name */
    private static DapWeatherNotification f3425b;

    /* renamed from: c  reason: collision with root package name */
    private Context f3426c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f3427d = false;

    /* renamed from: e  reason: collision with root package name */
    private SQLiteDatabase f3428e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3429f = true;

    /* renamed from: g  reason: collision with root package name */
    private boolean f3430g = true;

    public interface a {
        void a(int i);

        void b(int i);
    }

    public static DapWeatherNotification getInstance(Context context) {
        if (f3425b == null) {
            synchronized (DapWeatherNotification.class) {
                if (f3425b == null) {
                    f3425b = new DapWeatherNotification(context);
                }
            }
        }
        return f3425b;
    }

    DapWeatherNotification(Context context) {
        this.f3426c = context;
        this.f3428e = f.a(this.f3426c).getReadableDatabase();
    }

    public void setPushElevatingTemperature(boolean z) {
        this.f3429f = z;
        SharedPrefsUtils.b(this.f3426c, z);
    }

    public void setPushTodayTomorrowWeather(boolean z) {
        this.f3430g = z;
        SharedPrefsUtils.c(this.f3426c, z);
    }

    public boolean isOngoing() {
        return SharedPrefsUtils.l(this.f3426c);
    }

    public void setOngoing(boolean z) {
        SharedPrefsUtils.d(this.f3426c, z);
    }

    public void setNotificationClickListener(a aVar) {
        f3424a = aVar;
    }
}
