package com.daps.weather.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.daps.weather.DapWeatherActivity;
import com.daps.weather.a;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.daps.weather.base.f;
import com.daps.weather.bean.currentconditions.CurrentCondition;
import com.daps.weather.bean.currentconditions.CurrentConditionsWind;
import com.daps.weather.bean.forecasts.Forecast;
import com.daps.weather.bean.forecasts.ForecastsDailyForecasts;
import com.daps.weather.bean.forecasts.ForecastsDailyForecastsDay;
import com.daps.weather.bean.forecasts.ForecastsDailyForecastsTemperature;
import com.daps.weather.bean.locations.Location;
import com.daps.weather.notification.DapWeatherNotification;
import com.daps.weather.reciver.DapWeatherBroadcastReceiver;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import e.a;
import f.b;
import java.lang.reflect.Method;
import java.util.List;

/* compiled from: WeatherNotificationHelper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f3432a = ServiceManagerNative.NOTIFICATION;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f3433b = a.class.getSimpleName();

    /* renamed from: c  reason: collision with root package name */
    private static a f3434c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public Context f3435d;

    /* renamed from: e  reason: collision with root package name */
    private RemoteViews f3436e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3437f = false;

    /* renamed from: g  reason: collision with root package name */
    private SQLiteDatabase f3438g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public String f3439h = "";
    /* access modifiers changed from: private */
    public String i = "";
    private String j = "DUMMY_TITLE";

    public static a a(Context context) {
        if (f3434c == null) {
            synchronized (DapWeatherNotification.class) {
                if (f3434c == null) {
                    f3434c = new a(context);
                }
            }
        }
        return f3434c;
    }

    a(Context context) {
        this.f3435d = context;
        this.f3438g = f.a(this.f3435d).getReadableDatabase();
    }

    public void a(String str) {
        this.i = str;
        String b2 = SharedPrefsUtils.b(this.f3435d);
        String c2 = SharedPrefsUtils.c(this.f3435d);
        d.a(f3433b, "latitude:" + b2);
        d.a(f3433b, "longitude:" + c2);
        if (!TextUtils.isEmpty(b2) && !TextUtils.isEmpty(c2)) {
            try {
                b.a(this.f3435d).a(b2, c2, new b.a() {
                    public void a(List list) {
                        Location location;
                        if (list != null && list.size() > 0 && (location = (Location) list.get(0)) != null) {
                            String key = location.getKey();
                            if (!TextUtils.isEmpty(key)) {
                                SharedPrefsUtils.a(a.this.f3435d, key);
                                a.this.a(location, key);
                            }
                        }
                    }
                });
            } catch (Exception e2) {
                e2.printStackTrace();
                e.a.a(this.f3435d, a.C0097a.NOTIFICATION, e2.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public String a(ForecastsDailyForecasts forecastsDailyForecasts, ForecastsDailyForecasts forecastsDailyForecasts2) {
        String str;
        String str2 = "";
        if (!(forecastsDailyForecasts == null || forecastsDailyForecasts2 == null)) {
            try {
                double a2 = (double) e.a(forecastsDailyForecasts.getTemperature().getMaximum().getValue());
                double a3 = (double) e.a(forecastsDailyForecasts.getTemperature().getMinimum().getValue());
                double a4 = (double) e.a(forecastsDailyForecasts2.getTemperature().getMaximum().getValue());
                double a5 = (double) e.a(forecastsDailyForecasts2.getTemperature().getMinimum().getValue());
                com.daps.weather.base.a aVar = new com.daps.weather.base.a();
                int b2 = e.b(aVar.a("(" + a4 + ")-(" + a2 + ")=").doubleValue());
                int b3 = e.b(aVar.a("(" + a5 + ")-(" + a3 + ")=").doubleValue());
                int abs = Math.abs(b2);
                int abs2 = Math.abs(b3);
                d.a(f3433b, "结果:" + String.valueOf(abs) + ",today:" + a2 + a3 + ",nextday:" + a4 + a5 + ",needMin:" + String.valueOf(abs2));
                d.a(f3433b, "needMax:" + abs + ",needMin:" + abs2);
                if (abs < 4) {
                    if (abs2 >= 4) {
                        if (a5 > a3) {
                            d.a(f3433b, "升温了");
                            str = String.format(this.f3435d.getString(a.f.gt), Double.valueOf(Math.abs(a5 - a3)));
                        } else if (a5 < a3) {
                            d.a(f3433b, "降温了");
                            str = String.format(this.f3435d.getString(a.f.gr), Double.valueOf(Math.abs(a5 - a3)));
                        }
                    }
                    str = str2;
                } else if (a4 > a2) {
                    d.a(f3433b, "升温了");
                    str = String.format(this.f3435d.getString(a.f.gt), Double.valueOf(Math.abs(a4 - a2)));
                } else {
                    if (a4 < a2) {
                        d.a(f3433b, "降温了");
                        str = String.format(this.f3435d.getString(a.f.gr), Double.valueOf(Math.abs(a4 - a2)));
                    }
                    str = str2;
                }
                if (abs < 4 || abs2 < 4) {
                    str2 = str;
                } else if (a4 > a2 && a5 > a3) {
                    try {
                        d.a(f3433b, "升温了");
                        str2 = String.format(this.f3435d.getString(a.f.gt), Double.valueOf(Math.abs(a4 - a2)));
                    } catch (Exception e2) {
                        Exception exc = e2;
                        str2 = str;
                        e = exc;
                        e.printStackTrace();
                        return str2;
                    }
                } else if (a5 >= a3 || a4 >= a2) {
                    d.a(f3433b, "极端天气");
                    str2 = this.f3435d.getString(a.f.gs);
                } else {
                    d.a(f3433b, "降温了");
                    str2 = String.format(this.f3435d.getString(a.f.gr), Double.valueOf(Math.abs(a4 - a2)));
                }
                d.a(f3433b, "needStr:" + str2);
            } catch (Exception e3) {
                e = e3;
                e.printStackTrace();
                return str2;
            }
        }
        return str2;
    }

    /* access modifiers changed from: private */
    public void a(final Location location, final String str) {
        try {
            b.a(this.f3435d).a(str, new b.a() {
                /* JADX WARNING: Code restructure failed: missing block: B:3:0x0008, code lost:
                    r0 = (com.daps.weather.bean.currentconditions.CurrentCondition) r6.get(0);
                 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void a(java.util.List r6) {
                    /*
                        r5 = this;
                        if (r6 == 0) goto L_0x0029
                        int r0 = r6.size()
                        if (r0 <= 0) goto L_0x0029
                        r0 = 0
                        java.lang.Object r0 = r6.get(r0)
                        com.daps.weather.bean.currentconditions.CurrentCondition r0 = (com.daps.weather.bean.currentconditions.CurrentCondition) r0
                        if (r0 == 0) goto L_0x0029
                        java.lang.String r1 = ""
                        java.lang.String r1 = "10"
                        com.daps.weather.notification.a r2 = com.daps.weather.notification.a.this     // Catch:{ Exception -> 0x002a }
                        android.content.Context r2 = r2.f3435d     // Catch:{ Exception -> 0x002a }
                        f.b r2 = f.b.a(r2)     // Catch:{ Exception -> 0x002a }
                        java.lang.String r3 = r4     // Catch:{ Exception -> 0x002a }
                        com.daps.weather.notification.a$2$1 r4 = new com.daps.weather.notification.a$2$1     // Catch:{ Exception -> 0x002a }
                        r4.<init>(r0)     // Catch:{ Exception -> 0x002a }
                        r2.a(r3, r1, r4)     // Catch:{ Exception -> 0x002a }
                    L_0x0029:
                        return
                    L_0x002a:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x0029
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.daps.weather.notification.a.AnonymousClass2.a(java.util.List):void");
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void a(Location location, CurrentCondition currentCondition, Forecast forecast) {
        ForecastsDailyForecasts forecastsDailyForecasts;
        Bitmap a2;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.f3435d);
        builder.a(a.c.hu);
        this.f3436e = new RemoteViews(this.f3435d.getPackageName(), a.e.az);
        int i2 = a.e.az;
        builder.a(this.f3436e);
        Intent intent = new Intent(this.f3435d, DapWeatherBroadcastReceiver.class);
        intent.setAction("com.daps.weather.broadcast");
        intent.addFlags(536870912);
        intent.addFlags(268435456);
        intent.putExtra(DapWeatherActivity.f3350a, this.i);
        d.a(f3433b, "isPushNextDayWeather:" + this.i);
        PendingIntent broadcast = PendingIntent.getBroadcast(this.f3435d, 2, intent, 134217728);
        builder.a(broadcast);
        this.f3436e.setOnClickPendingIntent(i2, broadcast);
        if (location != null) {
            String englishName = location.getEnglishName();
            if (!TextUtils.isEmpty(englishName)) {
                this.f3436e.setTextViewText(a.d.ho, englishName);
            }
        }
        if (currentCondition != null) {
            String weatherText = currentCondition.getWeatherText();
            if (!TextUtils.isEmpty(weatherText)) {
                this.f3436e.setTextViewText(a.d.hx, weatherText);
            }
            int weatherIcon = currentCondition.getWeatherIcon();
            if (!(weatherIcon == 0 || (a2 = e.a(this.f3435d, weatherIcon)) == null)) {
                this.f3436e.setImageViewBitmap(a.d.hv, a2);
            }
            int epochTime = currentCondition.getEpochTime();
            if (epochTime != 0) {
                this.f3436e.setTextViewText(a.d.hr, e.a(String.valueOf(epochTime)));
            }
            CurrentConditionsWind wind = currentCondition.getWind();
            if (wind != null) {
                this.f3436e.setTextViewText(a.d.i0, String.valueOf(e.a(wind.getSpeed().getMetric().getValue())) + "mph");
                String english = wind.getDirection().getEnglish();
                Bitmap bitmap = ((BitmapDrawable) this.f3435d.getResources().getDrawable(a.c.i1)).getBitmap();
                Matrix matrix = new Matrix();
                matrix.setRotate((float) e.f(english));
                this.f3436e.setImageViewBitmap(a.d.hz, Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true));
            }
        }
        if (forecast != null) {
            ForecastsDailyForecasts[] dailyForecasts = forecast.getDailyForecasts();
            if (dailyForecasts.length > 0) {
                if (!"3".equals(this.i) || dailyForecasts.length <= 1) {
                    forecastsDailyForecasts = dailyForecasts[0];
                    this.f3436e.setTextViewText(a.d.hp, this.f3435d.getResources().getString(a.f.gu));
                } else {
                    forecastsDailyForecasts = dailyForecasts[1];
                    this.f3436e.setTextViewText(a.d.hp, this.f3435d.getResources().getString(a.f.gv));
                    Bitmap a3 = e.a(this.f3435d, forecastsDailyForecasts.getDay().getIcon());
                    if (a3 != null) {
                        this.f3436e.setImageViewBitmap(a.d.hv, a3);
                    }
                }
                if (forecastsDailyForecasts != null) {
                    ForecastsDailyForecastsTemperature temperature = forecastsDailyForecasts.getTemperature();
                    if (temperature != null) {
                        this.f3436e.setTextViewText(a.d.hy, String.valueOf(e.a(temperature.getMaximum().getValue())) + "°/" + String.valueOf(e.a(temperature.getMinimum().getValue())) + "°");
                    }
                    ForecastsDailyForecastsDay day = forecastsDailyForecasts.getDay();
                    if (day != null) {
                        this.f3436e.setTextViewText(a.d.i2, String.valueOf(e.c(day.getRain().getValue())) + "mm");
                    }
                }
            }
        }
        if (TextUtils.isEmpty(this.f3439h) || !"2".equals(this.i)) {
            this.f3436e.setViewVisibility(a.d.hu, 0);
            this.f3436e.setViewVisibility(a.d.hs, 8);
        } else {
            this.f3436e.setTextViewText(a.d.ht, this.f3439h);
            this.f3436e.setViewVisibility(a.d.hu, 8);
            this.f3436e.setViewVisibility(a.d.hs, 0);
        }
        if (!"-1".equals(this.f3439h)) {
            DapWeatherNotification.getInstance(this.f3435d);
            DapWeatherNotification.a aVar = DapWeatherNotification.f3424a;
            if (aVar == null) {
                d.a(f3433b, "weatherlistener onShow is null");
            } else if (!TextUtils.isEmpty(this.i)) {
                aVar.b(Integer.parseInt(this.i));
            }
            builder.a(SharedPrefsUtils.l(this.f3435d));
            builder.b(2);
            ((NotificationManager) this.f3435d.getSystemService(ServiceManagerNative.NOTIFICATION)).notify(100, builder.b());
        }
    }

    /* access modifiers changed from: private */
    public void b(Location location, CurrentCondition currentCondition, Forecast forecast) {
        a(location, currentCondition, forecast);
    }

    public void b(Context context) {
        Method method;
        try {
            Object systemService = context.getSystemService("statusbar");
            if (Build.VERSION.SDK_INT <= 16) {
                method = systemService.getClass().getMethod("collapse", new Class[0]);
            } else {
                method = systemService.getClass().getMethod("collapsePanels", new Class[0]);
            }
            method.invoke(systemService, new Object[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
