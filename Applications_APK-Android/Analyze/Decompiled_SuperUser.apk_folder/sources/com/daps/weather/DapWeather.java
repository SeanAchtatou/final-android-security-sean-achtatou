package com.daps.weather;

import android.app.Application;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.location.c;
import com.daps.weather.notification.DapWeatherNotification;

public class DapWeather {

    /* renamed from: a  reason: collision with root package name */
    private static DapWeather f3348a;

    /* renamed from: b  reason: collision with root package name */
    private Application f3349b;

    public static DapWeather getInstance(Application application) {
        if (f3348a == null) {
            synchronized (DapWeatherNotification.class) {
                if (f3348a == null) {
                    f3348a = new DapWeather(application);
                }
            }
        }
        return f3348a;
    }

    DapWeather(Application application) {
        this.f3349b = application;
    }

    public static void init(Application application, int i) {
        c.a(application);
        SharedPrefsUtils.c(application, i);
    }
}
