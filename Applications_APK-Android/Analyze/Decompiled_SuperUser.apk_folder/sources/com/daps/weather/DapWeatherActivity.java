package com.daps.weather;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import b.a;
import com.daps.weather.a;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.c;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.daps.weather.base.f;
import com.daps.weather.base.g;
import com.daps.weather.bean.currentconditions.CurrentCondition;
import com.daps.weather.bean.currentconditions.CurrentConditionsTemperature;
import com.daps.weather.bean.currentconditions.CurrentConditionsTemperatureMetric;
import com.daps.weather.bean.currentconditions.CurrentConditionsWind;
import com.daps.weather.bean.forecasts.Forecast;
import com.daps.weather.bean.forecasts.ForecastsDailyForecasts;
import com.daps.weather.bean.locations.Location;
import com.daps.weather.floatdisplay.FloatDisplayController;
import com.daps.weather.view.MyHorizontalListView;
import com.daps.weather.view.MyScrollView;
import com.duapps.ad.AdError;
import com.duapps.ad.DuAdListener;
import com.duapps.ad.DuAdMediaView;
import com.duapps.ad.DuNativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import e.a;
import f.b;
import java.util.ArrayList;
import java.util.List;

public class DapWeatherActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public static String f3350a = "junp_this";
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public static final String f3351g = DapWeatherActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public ImageView A;
    /* access modifiers changed from: private */
    public ImageView B;
    private RelativeLayout C;
    private LinearLayout D;
    private MyScrollView E;
    /* access modifiers changed from: private */
    public LinearLayout F;
    /* access modifiers changed from: private */
    public DuAdMediaView G;
    private SQLiteDatabase H;
    private ObjectAnimator I;
    /* access modifiers changed from: private */
    public CurrentCondition J;
    private int K;
    private int L;
    /* access modifiers changed from: private */
    public LinearLayout M;
    private boolean N;
    private a O;

    /* renamed from: b  reason: collision with root package name */
    DuNativeAd f3352b;

    /* renamed from: c  reason: collision with root package name */
    d.a f3353c;

    /* renamed from: d  reason: collision with root package name */
    NativeContentAdView f3354d;

    /* renamed from: e  reason: collision with root package name */
    NativeAppInstallAdView f3355e;

    /* renamed from: f  reason: collision with root package name */
    DuAdListener f3356f = new DuAdListener() {
        public void onError(DuNativeAd duNativeAd, AdError adError) {
            d.a(DapWeatherActivity.f3351g, "onError : " + adError.getErrorCode());
        }

        public void onClick(DuNativeAd duNativeAd) {
            d.a(DapWeatherActivity.f3351g, "onClick : click ad");
        }

        public void onAdLoaded(final DuNativeAd duNativeAd) {
            d.a(DapWeatherActivity.f3351g, "onAdLoaded : " + duNativeAd.getTitle());
            DapWeatherActivity.this.runOnUiThread(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.duapps.ad.a.<init>(android.content.Context, com.duapps.ad.DuNativeAd, boolean):void
                 arg types: [com.daps.weather.DapWeatherActivity, com.duapps.ad.DuNativeAd, int]
                 candidates:
                  com.duapps.ad.a.<init>(android.content.Context, com.duapps.ad.entity.a.a, boolean):void
                  com.duapps.ad.a.<init>(android.content.Context, com.duapps.ad.DuNativeAd, boolean):void */
                public void run() {
                    if (DapWeatherActivity.this.f3355e != null) {
                        DapWeatherActivity.this.f3355e.removeAllViews();
                    }
                    if (DapWeatherActivity.this.f3354d != null) {
                        DapWeatherActivity.this.f3354d.removeAllViews();
                    }
                    DapWeatherActivity.this.r.setVisibility(0);
                    DapWeatherActivity.this.u.setText(duNativeAd.getTitle());
                    DapWeatherActivity.this.f3353c.a(duNativeAd.getIconUrl(), DapWeatherActivity.this.A);
                    if (TextUtils.isEmpty(duNativeAd.getImageUrl())) {
                        DapWeatherActivity.this.z.setImageBitmap(null);
                    } else {
                        DapWeatherActivity.this.f3353c.a(duNativeAd.getImageUrl(), DapWeatherActivity.this.z);
                    }
                    DapWeatherActivity.this.v.setText(duNativeAd.getShortDesc());
                    DapWeatherActivity.this.s.setText(duNativeAd.getCallToAction());
                    DapWeatherActivity.this.F.removeAllViews();
                    int adChannelType = duNativeAd.getAdChannelType();
                    if (adChannelType == 4) {
                        if (DapWeatherActivity.this.f3355e == null) {
                            DapWeatherActivity.this.f3355e = new NativeAppInstallAdView(DapWeatherActivity.this);
                        }
                        DapWeatherActivity.this.f3355e.setHeadlineView(DapWeatherActivity.this.u);
                        DapWeatherActivity.this.f3355e.setIconView(DapWeatherActivity.this.A);
                        DapWeatherActivity.this.f3355e.setBodyView(DapWeatherActivity.this.v);
                        DapWeatherActivity.this.f3355e.setImageView(DapWeatherActivity.this.z);
                        DapWeatherActivity.this.f3355e.setCallToActionView(DapWeatherActivity.this.s);
                        DapWeatherActivity.this.f3355e.addView(DapWeatherActivity.this.r, new FrameLayout.LayoutParams(-2, -2));
                        DapWeatherActivity.this.f3352b.registerViewForInteraction(DapWeatherActivity.this.f3355e);
                    } else if (adChannelType == 5) {
                        if (DapWeatherActivity.this.f3354d == null) {
                            DapWeatherActivity.this.f3354d = new NativeContentAdView(DapWeatherActivity.this);
                        }
                        DapWeatherActivity.this.f3354d.setHeadlineView(DapWeatherActivity.this.u);
                        DapWeatherActivity.this.f3354d.setLogoView(DapWeatherActivity.this.A);
                        DapWeatherActivity.this.f3354d.setBodyView(DapWeatherActivity.this.v);
                        DapWeatherActivity.this.f3354d.setCallToActionView(DapWeatherActivity.this.s);
                        DapWeatherActivity.this.f3354d.addView(DapWeatherActivity.this.r, new FrameLayout.LayoutParams(-2, -2));
                        DapWeatherActivity.this.f3352b.registerViewForInteraction(DapWeatherActivity.this.f3354d);
                    } else {
                        if (adChannelType == 2) {
                            DapWeatherActivity.this.F.addView(new com.duapps.ad.a((Context) DapWeatherActivity.this, duNativeAd, true));
                            DapWeatherActivity.this.z.setVisibility(8);
                            DapWeatherActivity.this.G.setVisibility(0);
                            DapWeatherActivity.this.G.setAutoPlay(true);
                            DapWeatherActivity.this.G.setNativeAd(duNativeAd.getRealSource());
                        } else {
                            DapWeatherActivity.this.z.setVisibility(0);
                            DapWeatherActivity.this.G.setVisibility(8);
                        }
                        DapWeatherActivity.this.f3352b.registerViewForInteraction(DapWeatherActivity.this.r);
                    }
                }
            });
        }
    };

    /* renamed from: h  reason: collision with root package name */
    private TextView f3357h;
    private ImageView i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private ImageView n;
    private MyHorizontalListView o;
    private RelativeLayout p;
    private int q = 137065;
    /* access modifiers changed from: private */
    public RelativeLayout r;
    /* access modifiers changed from: private */
    public TextView s;
    private TextView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public TextView v;
    private TextView w;
    /* access modifiers changed from: private */
    public TextView x;
    private ImageView y;
    /* access modifiers changed from: private */
    public ImageView z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(a.e.ax);
        this.H = f.a(this).getReadableDatabase();
        getIntent().getStringExtra(f3350a);
        d.a(f3351g, "onCreate，直接进入");
        e.a.a(this, a.C0097a.LANDING_PAGE);
        e();
        d();
        f();
        c();
        LocationManager locationManager = (LocationManager) getSystemService(FirebaseAnalytics.Param.LOCATION);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!TextUtils.isEmpty(intent.getStringExtra(f3350a))) {
            d.a(f3351g, "onNewIntent,点击了通知栏");
            e.a.a(this, a.C0097a.LANDING_PAGE);
            f();
            this.f3352b.load();
        }
        c();
    }

    private void c() {
        this.N = SharedPrefsUtils.m(this);
        d.a(f3351g, "isfloatSearchWindowShow_oncreate:" + this.N);
        FloatDisplayController.setFloatSerachWindowIsShow(getApplicationContext(), false);
    }

    private void d() {
        this.r = (RelativeLayout) findViewById(a.d.h8);
        d.a(f3351g, "you set cacheSize is 1");
        this.f3353c = d.a.a();
        if (!c.f3378c) {
            this.q = 11095;
        } else {
            this.q = SharedPrefsUtils.t(this);
        }
        d.a(f3351g, "Pid:" + this.q);
        this.f3352b = new DuNativeAd(this, this.q, 1);
        this.f3352b.setMobulaAdListener(this.f3356f);
        this.f3352b.setProcessClickCallback(new com.duapps.ad.d() {
            public void a() {
                DapWeatherActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        DapWeatherActivity.this.a("onPreClick");
                    }
                });
            }

            public void b() {
                DapWeatherActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        DapWeatherActivity.this.a();
                    }
                });
            }
        });
        this.f3352b.load();
    }

    private void e() {
        this.K = e.b(this);
        this.L = e.c(this);
        this.f3357h = (TextView) findViewById(a.d.gw);
        this.i = (ImageView) findViewById(a.d.gy);
        this.j = (TextView) findViewById(a.d.gz);
        this.w = (TextView) findViewById(a.d.h1);
        this.k = (TextView) findViewById(a.d.h0);
        this.l = (TextView) findViewById(a.d.h4);
        this.m = (TextView) findViewById(a.d.h5);
        this.n = (ImageView) findViewById(a.d.h6);
        this.y = (ImageView) findViewById(a.d.h2);
        this.o = (MyHorizontalListView) findViewById(a.d.h7);
        this.p = (RelativeLayout) findViewById(a.d.gq);
        this.t = (TextView) findViewById(a.d.h3);
        this.x = (TextView) findViewById(a.d.gs);
        this.B = (ImageView) findViewById(a.d.gt);
        this.C = (RelativeLayout) findViewById(a.d.gv);
        this.D = (LinearLayout) findViewById(a.d.gu);
        this.E = (MyScrollView) findViewById(a.d.gr);
        this.s = (TextView) findViewById(a.d.hb);
        this.u = (TextView) findViewById(a.d.h_);
        this.v = (TextView) findViewById(a.d.ha);
        this.z = (ImageView) findViewById(a.d.hf);
        this.A = (ImageView) findViewById(a.d.h9);
        this.F = (LinearLayout) findViewById(a.d.he);
        this.G = (DuAdMediaView) findViewById(a.d.hd);
        this.C.setOnClickListener(this);
        this.M = (LinearLayout) findViewById(a.d.hg);
        this.M.setOnClickListener(this);
        this.E.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (DapWeatherActivity.this.M != null && !DapWeatherActivity.this.M.isShown()) {
                    DapWeatherActivity.this.M.setVisibility(0);
                }
                return false;
            }
        });
    }

    private void f() {
        this.B.setVisibility(0);
        this.x.setVisibility(8);
        g();
        String b2 = SharedPrefsUtils.b(this);
        String c2 = SharedPrefsUtils.c(this);
        if (TextUtils.isEmpty(b2) || TextUtils.isEmpty(c2)) {
            d.a(f3351g, "经纬度是空");
        } else if (e.a(this)) {
            d.a(f3351g, "有网络，拿线上数据");
            try {
                b.a(this).a(b2, c2, new b.a() {
                    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0008, code lost:
                        r0 = (com.daps.weather.bean.locations.Location) r6.get(0);
                     */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void a(java.util.List r6) {
                        /*
                            r5 = this;
                            if (r6 == 0) goto L_0x0048
                            int r0 = r6.size()
                            if (r0 <= 0) goto L_0x0048
                            r0 = 0
                            java.lang.Object r0 = r6.get(r0)
                            com.daps.weather.bean.locations.Location r0 = (com.daps.weather.bean.locations.Location) r0
                            if (r0 == 0) goto L_0x0048
                            java.lang.String r1 = r0.getKey()
                            java.lang.String r2 = com.daps.weather.DapWeatherActivity.f3351g
                            java.lang.StringBuilder r3 = new java.lang.StringBuilder
                            r3.<init>()
                            java.lang.String r4 = "key:"
                            java.lang.StringBuilder r3 = r3.append(r4)
                            java.lang.StringBuilder r3 = r3.append(r1)
                            java.lang.String r3 = r3.toString()
                            com.daps.weather.base.d.a(r2, r3)
                            boolean r2 = android.text.TextUtils.isEmpty(r1)
                            if (r2 != 0) goto L_0x0048
                            com.daps.weather.DapWeatherActivity r2 = com.daps.weather.DapWeatherActivity.this
                            com.daps.weather.base.SharedPrefsUtils.a(r2, r1)
                            com.daps.weather.DapWeatherActivity r2 = com.daps.weather.DapWeatherActivity.this
                            f.b r2 = f.b.a(r2)
                            com.daps.weather.DapWeatherActivity$3$1 r3 = new com.daps.weather.DapWeatherActivity$3$1
                            r3.<init>(r1, r0)
                            r2.a(r1, r3)
                        L_0x0048:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.daps.weather.DapWeatherActivity.AnonymousClass3.a(java.util.List):void");
                    }
                });
            } catch (Exception e2) {
                e2.printStackTrace();
                d.b(f3351g, "天气加载失败:" + e2.getMessage());
                this.B.setVisibility(8);
                e.a.a(this, a.C0097a.LANDING_PAGE, e2.getMessage());
                i();
            }
        } else {
            d.a(f3351g, "没有网络，拿数据库数据");
            i();
        }
    }

    private void g() {
        this.I = ObjectAnimator.ofFloat(this.B, "rotation", 0.0f, 360.0f);
        this.I.setDuration(3000L);
        this.I.setRepeatCount(-1);
        this.I.start();
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.I != null && this.I.isRunning()) {
            this.I.end();
        }
    }

    private void i() {
        List a2 = g.a(this, this.H);
        List b2 = g.b(this, this.H);
        Forecast c2 = g.c(this, this.H);
        CurrentCondition currentCondition = null;
        if (b2 == null || b2.size() <= 0) {
            d.a(f3351g, "数据库中没有currentCondition数据");
        } else {
            currentCondition = (CurrentCondition) b2.get(0);
        }
        if (c2 == null) {
            d.a(f3351g, "数据库中没有forecastData数据");
        }
        if (!(c2 == null || currentCondition == null)) {
            this.J = currentCondition;
            a(currentCondition, 1);
            a(c2, 1);
        }
        if (a2 != null && a2.size() > 0 && a2.get(0) != null) {
            String englishName = ((Location) a2.get(0)).getEnglishName();
            if (!TextUtils.isEmpty(englishName)) {
                this.x.setText(englishName);
                if (this.B != null) {
                    this.B.setVisibility(8);
                }
                this.x.setVisibility(0);
                h();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Forecast forecast, int i2) {
        ForecastsDailyForecasts[] dailyForecasts;
        if (forecast != null && (dailyForecasts = forecast.getDailyForecasts()) != null && dailyForecasts.length > 0) {
            ArrayList arrayList = new ArrayList();
            for (int i3 = 0; i3 < dailyForecasts.length; i3++) {
                if (dailyForecasts[i3] != null && i3 <= 5) {
                    arrayList.add(dailyForecasts[i3]);
                }
            }
            this.o.setAdapter((ListAdapter) new c.a(this, arrayList));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.e.a(android.content.Context, int):android.graphics.Bitmap
     arg types: [com.daps.weather.DapWeatherActivity, int]
     candidates:
      com.daps.weather.base.e.a(android.content.Context, float):int
      com.daps.weather.base.e.a(android.content.Context, java.lang.String):void
      com.daps.weather.base.e.a(android.content.Context, int):android.graphics.Bitmap */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.daps.weather.base.e.a(android.content.Context, float):int
     arg types: [com.daps.weather.DapWeatherActivity, int]
     candidates:
      com.daps.weather.base.e.a(android.content.Context, int):android.graphics.Bitmap
      com.daps.weather.base.e.a(android.content.Context, java.lang.String):void
      com.daps.weather.base.e.a(android.content.Context, float):int */
    /* access modifiers changed from: private */
    public void a(CurrentCondition currentCondition, int i2) {
        CurrentConditionsTemperatureMetric metric;
        e.a.a(this, a.C0097a.LANDING_PAGE, i2);
        CurrentConditionsTemperature temperature = currentCondition.getTemperature();
        if (!(temperature == null || (metric = temperature.getMetric()) == null || this.f3357h == null)) {
            this.f3357h.setText(String.valueOf(e.b(metric.getValue()) + "°"));
        }
        CurrentConditionsWind wind = currentCondition.getWind();
        if (wind != null) {
            int value = wind.getSpeed().getMetric().getValue();
            String english = wind.getDirection().getEnglish();
            this.j.setText(english);
            this.i.setImageDrawable(getResources().getDrawable(a.c.i1));
            this.i.animate().rotation((float) e.f(english)).setDuration(0);
            this.k.setText(String.valueOf(e.a(value)) + "mph");
        }
        this.l.setText(String.valueOf(currentCondition.getRelativeHumidity()) + "%");
        this.m.setText(currentCondition.getWeatherText());
        Bitmap a2 = e.a((Context) this, currentCondition.getWeatherIcon());
        if (a2 != null) {
            this.n.setImageBitmap(a2);
        }
        if (currentCondition.getIsDayTime()) {
            this.p.setBackgroundColor(getResources().getColor(a.C0044a.e0));
        } else {
            this.p.setBackgroundColor(getResources().getColor(a.C0044a.e1));
        }
        this.t.setVisibility(0);
        this.y.setVisibility(0);
        this.w.setVisibility(0);
        if (this.K >= 1074) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.topMargin = e.a((Context) this, 18.0f);
            layoutParams.leftMargin = e.a((Context) this, 10.0f);
            layoutParams.rightMargin = e.a((Context) this, 10.0f);
            this.r.setLayoutParams(layoutParams);
            this.D.setVisibility(0);
            return;
        }
        this.D.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (this.O == null) {
            this.O = j();
        }
        this.O.a(str);
        this.O.show();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!isFinishing() && this.O != null) {
            this.O.dismiss();
        }
    }

    private b.a j() {
        b.a aVar = new b.a(this);
        aVar.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (d.a()) {
                    d.a(DapWeatherActivity.f3351g, "User Canceled Dialog.");
                }
            }
        });
        return aVar;
    }

    public void onClick(View view) {
        if ((view.getId() == a.d.gv || view.getId() == a.d.hg) && this.J != null && !TextUtils.isEmpty(this.J.getMobileLink())) {
            d.a(f3351g, "jump_url:" + this.J.getMobileLink());
            try {
                e.a(this, this.J.getMobileLink());
                e.a.b(this, a.C0097a.LANDING_PAGE);
            } catch (Exception e2) {
                e2.printStackTrace();
                Toast.makeText(this, getString(a.f.gf), 0).show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        d.a(f3351g, "isfloatSearchWindowShow:" + this.N);
        FloatDisplayController.setFloatSerachWindowIsShow(getApplicationContext(), Boolean.valueOf(this.N));
    }
}
