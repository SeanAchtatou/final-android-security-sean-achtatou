package com.daps.weather;

import com.kingouser.com.R;

/* compiled from: R */
public final class a {

    /* renamed from: com.daps.weather.a$a  reason: collision with other inner class name */
    /* compiled from: R */
    public static final class C0044a {

        /* renamed from: huafeng_text_color */
        public static final int bs = 2131558492;

        /* renamed from: notification_bg_color */
        public static final int ck = 2131558521;

        /* renamed from: weather_act_ad_color */
        public static final int dz = 2131558573;

        /* renamed from: weather_act_day_bg_color */
        public static final int e0 = 2131558574;

        /* renamed from: weather_act_night_bg_color */
        public static final int e1 = 2131558575;

        /* renamed from: weather_view_text_color */
        public static final int e2 = 2131558576;

        /* renamed from: white */
        public static final int e3 = 2131558577;
    }

    /* compiled from: R */
    public static final class b {

        /* renamed from: float_search_window_width */
        public static final int e8 = 2131230915;

        /* renamed from: main_current_temperature_text_size */
        public static final int eu = 2131230938;

        /* renamed from: main_current_title_text_size */
        public static final int ev = 2131230939;

        /* renamed from: main_current_wind_text_size */
        public static final int ew = 2131230940;
    }

    /* compiled from: R */
    public static final class c {

        /* renamed from: card_image_default_bg */
        public static final int ba = 2130837628;

        /* renamed from: defalut_main_ad_big */
        public static final int h3 = 2130837656;

        /* renamed from: defalut_view_cloud */
        public static final int h4 = 2130837657;

        /* renamed from: default_main_ad_logo */
        public static final int h5 = 2130837658;

        /* renamed from: default_main_loading */
        public static final int h6 = 2130837659;

        /* renamed from: default_view_item_icon */
        public static final int h7 = 2130837660;

        /* renamed from: fb_banner_bg */
        public static final int bv = 2130837672;

        /* renamed from: huafeng_logo */
        public static final int h9 = 2130837680;

        /* renamed from: notification_more */
        public static final int hn = 2130837756;

        /* renamed from: sunny */
        public static final int hu = 2130837807;

        /* renamed from: v2_facebook_card_badge */
        public static final int n = 2130837815;

        /* renamed from: v2_toolbox_card_btn_bg */
        public static final int cq = 2130837818;

        /* renamed from: v2_toolbox_card_btn_normal_bg */
        public static final int hw = 2130837819;

        /* renamed from: v2_toolbox_card_btn_pressed_bg */
        public static final int hx = 2130837820;

        /* renamed from: weather_ad */
        public static final int ie = 2130837828;

        /* renamed from: weather_enter_iv */
        public static final int hz = 2130837829;

        /* renamed from: weather_notification_rain */
        public static final int i0 = 2130837830;

        /* renamed from: wind_n */
        public static final int i1 = 2130837832;
    }

    /* compiled from: R */
    public static final class d {

        /* renamed from: card_ad_choices_container */
        public static final int he = 2131624236;

        /* renamed from: card_media_view */
        public static final int hd = 2131624235;

        /* renamed from: content_layout  reason: collision with root package name */
        public static final int f3372content_layout = 2131624283;

        /* renamed from: dap_weather_ad_btn_tv */
        public static final int hb = 2131624233;

        /* renamed from: dap_weather_ad_card_iv */
        public static final int hf = 2131624237;

        /* renamed from: dap_weather_ad_desc_tv */
        public static final int ha = 2131624232;

        /* renamed from: dap_weather_ad_icon_iv */
        public static final int h9 = 2131624230;

        /* renamed from: dap_weather_ad_source_jump_ll */
        public static final int hg = 2131624238;

        /* renamed from: dap_weather_ad_title_tv */
        public static final int h_ = 2131624231;

        /* renamed from: dap_weather_forecast_item_icon */
        public static final int hk = 2131624242;

        /* renamed from: dap_weather_forecast_item_layout_rl */
        public static final int hh = 2131624239;

        /* renamed from: dap_weather_forecast_item_line_iv */
        public static final int hm = 2131624244;

        /* renamed from: dap_weather_forecast_item_temperature */
        public static final int hl = 2131624243;

        /* renamed from: dap_weather_forecast_item_time */
        public static final int hj = 2131624241;

        /* renamed from: dap_weather_forecast_item_week_time */
        public static final int hi = 2131624240;

        /* renamed from: dap_weather_layout_city_tv */
        public static final int gs = 2131624213;

        /* renamed from: dap_weather_layout_jump_rl */
        public static final int gv = 2131624216;

        /* renamed from: dap_weather_layout_load_iv */
        public static final int gt = 2131624214;

        /* renamed from: dap_weather_layout_main_rl */
        public static final int gq = 2131624211;

        /* renamed from: dap_weather_layout_margin_ll */
        public static final int gu = 2131624215;

        /* renamed from: dap_weather_layout_rain_iv */
        public static final int h2 = 2131624223;

        /* renamed from: dap_weather_layout_rain_wind_layout */
        public static final int gx = 2131624218;

        /* renamed from: dap_weather_layout_relativeHumidity_text_tv */
        public static final int h4 = 2131624225;

        /* renamed from: dap_weather_layout_relativeHumidity_titile_tv */
        public static final int h3 = 2131624224;

        /* renamed from: dap_weather_layout_sl */
        public static final int gr = 2131624212;

        /* renamed from: dap_weather_layout_weather_ad */
        public static final int h8 = 2131624229;

        /* renamed from: dap_weather_layout_weather_icon_iv */
        public static final int h6 = 2131624227;

        /* renamed from: dap_weather_layout_weather_listview */
        public static final int h7 = 2131624228;

        /* renamed from: dap_weather_layout_weather_temp_text_tv */
        public static final int gw = 2131624217;

        /* renamed from: dap_weather_layout_weather_text_tv */
        public static final int h5 = 2131624226;

        /* renamed from: dap_weather_layout_wind_iv */
        public static final int gy = 2131624219;

        /* renamed from: dap_weather_layout_wind_text_tv */
        public static final int gz = 2131624220;

        /* renamed from: dap_weather_layout_windgs_text_line_tv */
        public static final int h1 = 2131624222;

        /* renamed from: dap_weather_layout_windgs_text_tv */
        public static final int h0 = 2131624221;

        /* renamed from: dap_weather_notification_cloudy_tv */
        public static final int hx = 2131624255;

        /* renamed from: dap_weather_notification_date_desc_tv */
        public static final int hr = 2131624249;

        /* renamed from: dap_weather_notification_date_title_tv */
        public static final int hq = 2131624248;

        /* renamed from: dap_weather_notification_icon_iv */
        public static final int hv = 2131624253;

        /* renamed from: dap_weather_notification_info_tv */
        public static final int hp = 2131624247;

        /* renamed from: dap_weather_notification_layout */
        public static final int hn = 2131624245;

        /* renamed from: dap_weather_notification_line_iv */
        public static final int hw = 2131624254;

        /* renamed from: dap_weather_notification_local_tv */
        public static final int ho = 2131624246;

        /* renamed from: dap_weather_notification_rain_iv */
        public static final int i1 = 2131624259;

        /* renamed from: dap_weather_notification_rain_tv */
        public static final int i2 = 2131624260;

        /* renamed from: dap_weather_notification_temperature_iv */
        public static final int hz = 2131624257;

        /* renamed from: dap_weather_notification_temperature_tv */
        public static final int hy = 2131624256;

        /* renamed from: dap_weather_notification_two_customer_rl */
        public static final int hu = 2131624252;

        /* renamed from: dap_weather_notification_two_text_ll */
        public static final int hs = 2131624250;

        /* renamed from: dap_weather_notification_two_text_tv */
        public static final int ht = 2131624251;

        /* renamed from: dap_weather_notification_wind_tv */
        public static final int i0 = 2131624258;

        /* renamed from: dap_weather_view_cloudy_tv */
        public static final int i6 = 2131624264;

        /* renamed from: dap_weather_view_icon */
        public static final int i3 = 2131624261;

        /* renamed from: dap_weather_view_local_tv */
        public static final int i5 = 2131624263;

        /* renamed from: dap_weather_view_temperature_tv */
        public static final int i4 = 2131624262;

        /* renamed from: fb_image */
        public static final int hc = 2131624234;
        public static final int icon = 2131624019;
        public static final int image = 2131624016;
        public static final int title = 2131624020;

        /* renamed from: viewpager */
        public static final int d2 = 2131624075;
    }

    /* compiled from: R */
    public static final class e {

        /* renamed from: dap_weahter_layout */
        public static final int ax = 2130968636;

        /* renamed from: dap_weather_forecast_item */
        public static final int ay = 2130968637;

        /* renamed from: dap_weather_notification_dark */
        public static final int az = 2130968638;

        /* renamed from: dap_weather_view */
        public static final int b0 = 2130968639;
    }

    /* compiled from: R */
    public static final class f {
        public static final int common_google_play_services_unknown_issue = 2131296282;

        /* renamed from: no_browser */
        public static final int gf = 2131296527;

        /* renamed from: weather_msg_down_one */
        public static final int gr = 2131296539;

        /* renamed from: weather_msg_down_up */
        public static final int gs = 2131296540;

        /* renamed from: weather_msg_up_one */
        public static final int gt = 2131296541;

        /* renamed from: weather_notifaction_info */
        public static final int gu = 2131296542;

        /* renamed from: weather_notifaction_info_next_day */
        public static final int gv = 2131296543;

        /* renamed from: weather_notification_date_title */
        public static final int gw = 2131296544;
    }

    /* compiled from: R */
    public static final class g {
        public static final int[] Arc = {R.attr.e8, R.attr.e9};
        public static final int Arc_ArcColor = 0;
        public static final int Arc_ArcLevel = 1;
        public static final int[] HorizontalListView = {16842976, 16843049, 16843685, R.attr.g5};
        public static final int HorizontalListView_android_divider = 1;
        public static final int HorizontalListView_android_fadingEdgeLength = 0;
        public static final int HorizontalListView_android_requiresFadingEdge = 2;
        public static final int HorizontalListView_dividerWidth = 3;
    }
}
