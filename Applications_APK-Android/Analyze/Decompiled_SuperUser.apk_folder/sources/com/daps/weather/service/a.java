package com.daps.weather.service;

import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import com.daps.weather.base.SharedPrefsUtils;
import com.daps.weather.base.d;
import com.daps.weather.base.e;
import com.daps.weather.base.f;
import com.daps.weather.base.g;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: MsgService */
public class a extends Service {
    /* access modifiers changed from: private */
    public static final String TAG = a.class.getSimpleName();
    public static boolean isDestory = false;
    private long TIME = 600;
    private boolean isAllowPush = true;
    /* access modifiers changed from: private */
    public Calendar mCalendar;
    private ScheduledExecutorService mExecutor;
    /* access modifiers changed from: private */
    public SQLiteDatabase mReadableDatabase;
    private f mWeatherDataSQLite;
    /* access modifiers changed from: private */
    public String preDay = "";

    /* compiled from: MsgService */
    public enum b {
        TODAY,
        NEXTDAY,
        LIFT,
        NOMAL
    }

    public void onCreate() {
        d.a(TAG, "启动了MsgService_oncreate");
        this.mCalendar = Calendar.getInstance();
        initSqlite();
        initExecutor();
        super.onCreate();
    }

    /* access modifiers changed from: private */
    public void initSqlite() {
        if (this.mWeatherDataSQLite == null) {
            this.mWeatherDataSQLite = f.a(getApplicationContext());
            this.mReadableDatabase = this.mWeatherDataSQLite.getReadableDatabase();
        }
    }

    private void initExecutor() {
        if (this.mExecutor != null) {
            this.mExecutor.shutdown();
            this.mExecutor = null;
        }
        this.mExecutor = Executors.newSingleThreadScheduledExecutor();
        this.mExecutor.scheduleAtFixedRate(new C0045a(), 3, this.TIME, TimeUnit.SECONDS);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        d.a(TAG, "启动了MsgService");
        if (e.a(getApplicationContext())) {
            f.b.a(getApplicationContext()).b();
        }
        isDestory = false;
        if (this.mExecutor == null || this.mExecutor.isShutdown()) {
            initExecutor();
        }
        return super.onStartCommand(intent, i, i2);
    }

    /* renamed from: com.daps.weather.service.a$a  reason: collision with other inner class name */
    /* compiled from: MsgService */
    private class C0045a implements Runnable {
        private C0045a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, boolean):void
         arg types: [android.content.Context, int]
         candidates:
          com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, int):void
          com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, java.lang.Boolean):void
          com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, java.lang.String):void
          com.daps.weather.base.SharedPrefsUtils.a(android.content.Context, boolean):void */
        public void run() {
            a.isDestory = false;
            a.this.mCalendar.setTime(new Date(System.currentTimeMillis()));
            int i = a.this.mCalendar.get(11);
            int i2 = a.this.mCalendar.get(12);
            String b2 = e.b(String.valueOf(System.currentTimeMillis() / 1000));
            String unused = a.this.preDay = SharedPrefsUtils.e(a.this.getApplicationContext());
            if (!a.this.preDay.equals(b2)) {
                SharedPrefsUtils.f(a.this.getApplicationContext(), b2);
                String unused2 = a.this.preDay = b2;
                d.a(a.TAG, "新的一天，重置push");
                SharedPrefsUtils.e(a.this.getApplicationContext(), "0");
                SharedPrefsUtils.g(a.this.getApplicationContext(), "0");
                SharedPrefsUtils.i(a.this.getApplicationContext(), "0");
                SharedPrefsUtils.a(a.this.getApplicationContext(), true);
                a.this.initSqlite();
                long a2 = e.a();
                if (!"".equals(a.this.preDay) && a2 > 0) {
                    d.a(a.TAG, "新的一天，清空前一天的数据库");
                    g.a(a.this.mReadableDatabase, a2);
                    g.b(a.this.mReadableDatabase, a2);
                    g.c(a.this.mReadableDatabase, a2);
                }
            } else {
                SharedPrefsUtils.a(a.this.getApplicationContext(), false);
            }
            float parseFloat = Float.parseFloat(e.c(String.valueOf(System.currentTimeMillis())).replace(":", "."));
            d.a(a.TAG, "hour:" + i + "minute:" + i2 + ",needTime:" + parseFloat);
            if (parseFloat <= 23.0f && parseFloat >= 7.0f) {
                if (parseFloat <= 12.0f && ((double) parseFloat) >= 7.3d) {
                    d.a(a.TAG, "准备Push当日天气:" + SharedPrefsUtils.k(a.this.getApplicationContext()) + ",is0:" + SharedPrefsUtils.d(a.this.getApplicationContext()));
                    if (SharedPrefsUtils.k(a.this.getApplicationContext()) && "0".equals(SharedPrefsUtils.d(a.this.getApplicationContext()))) {
                        a.this.loadWeatherData(b.TODAY);
                        d.a(a.TAG, "notification_去拉取 当日 天气");
                    }
                }
                if (parseFloat >= 12.0f) {
                    if (((double) parseFloat) >= 20.3d) {
                        d.a(a.TAG, "准备Push次日天气:" + SharedPrefsUtils.k(a.this.getApplicationContext()) + ",is0:" + SharedPrefsUtils.f(a.this.getApplicationContext()));
                        if (SharedPrefsUtils.k(a.this.getApplicationContext()) && "0".equals(SharedPrefsUtils.f(a.this.getApplicationContext()))) {
                            a.this.loadWeatherData(b.NEXTDAY);
                            d.a(a.TAG, "notification_去拉取 明日 天气");
                        }
                    }
                    d.a(a.TAG, "push升降温:" + SharedPrefsUtils.j(a.this.getApplicationContext()) + ",is0:" + SharedPrefsUtils.h(a.this.getApplicationContext()));
                    if (SharedPrefsUtils.j(a.this.getApplicationContext()) && parseFloat >= 13.0f && ((double) parseFloat) < 20.3d && "0".equals(SharedPrefsUtils.h(a.this.getApplicationContext()))) {
                        a.this.loadWeatherData(b.LIFT);
                        d.a(a.TAG, "notification_去拉取 升降温 天气");
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void loadWeatherData(b bVar) {
        if (bVar == b.TODAY) {
            pushTodayWeather();
        } else if (bVar == b.NEXTDAY) {
            pushNextDayWeather();
        } else if (bVar == b.LIFT) {
            pushLiftWeather();
        }
    }

    private void pushNextDayWeather() {
        d.a(TAG, "notification_pushNextDayWeather");
        com.daps.weather.notification.a.a(getApplicationContext()).a("3");
    }

    private void pushTodayWeather() {
        d.a(TAG, "notification_pushTodayWeather");
        com.daps.weather.notification.a.a(getApplicationContext()).a("1");
    }

    private void pushLiftWeather() {
        d.a(TAG, "notification_pushLiftWeather");
        com.daps.weather.notification.a.a(getApplicationContext()).a("2");
    }

    public void onDestroy() {
        super.onDestroy();
        isDestory = true;
        if (this.mExecutor != null || !this.mExecutor.isShutdown()) {
            this.mExecutor.shutdownNow();
            this.mExecutor = null;
        }
    }
}
