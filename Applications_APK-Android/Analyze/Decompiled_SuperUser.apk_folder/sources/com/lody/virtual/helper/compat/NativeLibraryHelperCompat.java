package com.lody.virtual.helper.compat;

import android.annotation.TargetApi;
import android.os.Build;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.VLog;
import java.io.File;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import mirror.com.android.internal.content.NativeLibraryHelper;
import mirror.dalvik.system.VMRuntime;

public class NativeLibraryHelperCompat {
    private static String TAG = NativeLibraryHelperCompat.class.getSimpleName();

    public static int copyNativeBinaries(File file, File file2) {
        if (Build.VERSION.SDK_INT >= 21) {
            return copyNativeBinariesAfterL(file, file2);
        }
        return copyNativeBinariesBeforeL(file, file2);
    }

    private static int copyNativeBinariesBeforeL(File file, File file2) {
        try {
            return ((Integer) Reflect.on(NativeLibraryHelper.TYPE).call("copyNativeBinariesIfNeededLI", file, file2).get()).intValue();
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    @TargetApi(21)
    private static int copyNativeBinariesAfterL(File file, File file2) {
        String str;
        try {
            Object call = NativeLibraryHelper.Handle.create.call(file);
            if (call == null) {
                return -1;
            }
            String str2 = null;
            Set<String> aBIsFromApk = getABIsFromApk(file.getAbsolutePath());
            if (aBIsFromApk == null || aBIsFromApk.isEmpty()) {
                return 0;
            }
            if (!VMRuntime.is64Bit.call(VMRuntime.getRuntime.call(new Object[0]), new Object[0]).booleanValue() || !isVM64(aBIsFromApk)) {
                if (Build.SUPPORTED_32_BIT_ABIS.length > 0) {
                    int intValue = NativeLibraryHelper.findSupportedAbi.call(call, Build.SUPPORTED_32_BIT_ABIS).intValue();
                    if (intValue >= 0) {
                        str2 = Build.SUPPORTED_32_BIT_ABIS[intValue];
                    }
                }
            } else if (Build.SUPPORTED_64_BIT_ABIS.length > 0) {
                int intValue2 = NativeLibraryHelper.findSupportedAbi.call(call, Build.SUPPORTED_64_BIT_ABIS).intValue();
                if (intValue2 >= 0) {
                    str = Build.SUPPORTED_64_BIT_ABIS[intValue2];
                } else {
                    str = null;
                }
                str2 = str;
            }
            if (str2 == null) {
                VLog.e(TAG, "Not match any abi [%s].", file.getPath());
                return -1;
            }
            return NativeLibraryHelper.copyNativeBinaries.call(call, file2, str2).intValue();
        } catch (Throwable th) {
            VLog.d(TAG, "copyNativeBinaries with error : %s", th.getLocalizedMessage());
            th.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    @android.annotation.TargetApi(21)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean isVM64(java.util.Set<java.lang.String> r5) {
        /*
            r2 = 1
            r1 = 0
            java.lang.String[] r0 = android.os.Build.SUPPORTED_64_BIT_ABIS
            int r0 = r0.length
            if (r0 != 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            if (r5 == 0) goto L_0x0011
            boolean r0 = r5.isEmpty()
            if (r0 == 0) goto L_0x0013
        L_0x0011:
            r0 = r2
            goto L_0x0008
        L_0x0013:
            java.util.Iterator r3 = r5.iterator()
        L_0x0017:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x003d
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r4 = "arm64-v8a"
            boolean r4 = r4.endsWith(r0)
            if (r4 != 0) goto L_0x003b
            java.lang.String r4 = "x86_64"
            boolean r4 = r4.equals(r0)
            if (r4 != 0) goto L_0x003b
            java.lang.String r4 = "mips64"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0017
        L_0x003b:
            r0 = r2
            goto L_0x0008
        L_0x003d:
            r0 = r1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.helper.compat.NativeLibraryHelperCompat.isVM64(java.util.Set):boolean");
    }

    private static Set<String> getABIsFromApk(String str) {
        try {
            Enumeration<? extends ZipEntry> entries = new ZipFile(str).entries();
            HashSet hashSet = new HashSet();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                String name = zipEntry.getName();
                if (!name.contains("../") && name.startsWith("lib/") && !zipEntry.isDirectory() && name.endsWith(".so")) {
                    hashSet.add(name.substring(name.indexOf("/") + 1, name.lastIndexOf("/")));
                }
            }
            return hashSet;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
