package com.lody.virtual.helper.compat;

import android.content.ContentProviderClient;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemClock;

public class ContentProviderCompat {
    public static Bundle call(Context context, Uri uri, String str, String str2, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 17) {
            return context.getContentResolver().call(uri, str, str2, bundle);
        }
        ContentProviderClient crazyAcquireContentProvider = crazyAcquireContentProvider(context, uri);
        try {
            return crazyAcquireContentProvider.call(str, str2, bundle);
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return null;
        } finally {
            releaseQuietly(crazyAcquireContentProvider);
        }
    }

    private static ContentProviderClient acquireContentProviderClient(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT >= 16) {
            return context.getContentResolver().acquireUnstableContentProviderClient(uri);
        }
        return context.getContentResolver().acquireContentProviderClient(uri);
    }

    public static ContentProviderClient crazyAcquireContentProvider(Context context, Uri uri) {
        ContentProviderClient acquireContentProviderClient = acquireContentProviderClient(context, uri);
        if (acquireContentProviderClient == null) {
            int i = 0;
            while (i < 5 && acquireContentProviderClient == null) {
                SystemClock.sleep(100);
                int i2 = i + 1;
                acquireContentProviderClient = acquireContentProviderClient(context, uri);
                i = i2;
            }
        }
        return acquireContentProviderClient;
    }

    public static ContentProviderClient crazyAcquireContentProvider(Context context, String str) {
        ContentProviderClient acquireContentProviderClient = acquireContentProviderClient(context, str);
        if (acquireContentProviderClient == null) {
            int i = 0;
            while (i < 5 && acquireContentProviderClient == null) {
                SystemClock.sleep(100);
                int i2 = i + 1;
                acquireContentProviderClient = acquireContentProviderClient(context, str);
                i = i2;
            }
        }
        return acquireContentProviderClient;
    }

    private static ContentProviderClient acquireContentProviderClient(Context context, String str) {
        if (Build.VERSION.SDK_INT >= 16) {
            return context.getContentResolver().acquireUnstableContentProviderClient(str);
        }
        return context.getContentResolver().acquireContentProviderClient(str);
    }

    public static void releaseQuietly(ContentProviderClient contentProviderClient) {
        if (contentProviderClient != null) {
            try {
                if (Build.VERSION.SDK_INT >= 24) {
                    contentProviderClient.close();
                } else {
                    contentProviderClient.release();
                }
            } catch (Exception e2) {
            }
        }
    }
}
