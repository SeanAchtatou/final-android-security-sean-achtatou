package com.lody.virtual.helper.compat;

import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageParser;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.Process;
import android.util.DisplayMetrics;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.os.VUserHandle;
import java.io.File;
import mirror.android.content.pm.PackageParserJellyBean;
import mirror.android.content.pm.PackageParserJellyBean17;
import mirror.android.content.pm.PackageParserLollipop;
import mirror.android.content.pm.PackageParserLollipop22;
import mirror.android.content.pm.PackageParserMarshmallow;
import mirror.android.content.pm.PackageParserNougat;
import mirror.android.content.pm.PackageUserState;

public class PackageParserCompat {
    private static final int API_LEVEL = Build.VERSION.SDK_INT;
    public static final int[] GIDS = VirtualCore.get().getGids();
    private static final int myUserId = VUserHandle.getUserId(Process.myUid());
    private static final Object sUserState = (API_LEVEL >= 17 ? PackageUserState.ctor.newInstance() : null);

    public static PackageParser createParser(File file) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.ctor.newInstance();
        }
        if (API_LEVEL >= 22) {
            return PackageParserLollipop22.ctor.newInstance();
        }
        if (API_LEVEL >= 21) {
            return PackageParserLollipop.ctor.newInstance();
        }
        if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.ctor.newInstance(file.getAbsolutePath());
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.ctor.newInstance(file.getAbsolutePath());
        } else {
            return mirror.android.content.pm.PackageParser.ctor.newInstance(file.getAbsolutePath());
        }
    }

    public static PackageParser.Package parsePackage(PackageParser packageParser, File file, int i) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.parsePackage.callWithException(packageParser, file, Integer.valueOf(i));
        } else if (API_LEVEL >= 22) {
            return PackageParserLollipop22.parsePackage.callWithException(packageParser, file, Integer.valueOf(i));
        } else if (API_LEVEL >= 21) {
            return PackageParserLollipop.parsePackage.callWithException(packageParser, file, Integer.valueOf(i));
        } else if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.parsePackage.callWithException(packageParser, file, null, new DisplayMetrics(), Integer.valueOf(i));
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.parsePackage.callWithException(packageParser, file, null, new DisplayMetrics(), Integer.valueOf(i));
        } else {
            return mirror.android.content.pm.PackageParser.parsePackage.callWithException(packageParser, file, null, new DisplayMetrics(), Integer.valueOf(i));
        }
    }

    public static ServiceInfo generateServiceInfo(PackageParser.Service service, int i) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.generateServiceInfo.call(service, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 22) {
            return PackageParserLollipop22.generateServiceInfo.call(service, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 21) {
            return PackageParserLollipop.generateServiceInfo.call(service, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.generateServiceInfo.call(service, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.generateServiceInfo.call(service, Integer.valueOf(i), false, 1, Integer.valueOf(myUserId));
        } else {
            return mirror.android.content.pm.PackageParser.generateServiceInfo.call(service, Integer.valueOf(i));
        }
    }

    public static ApplicationInfo generateApplicationInfo(PackageParser.Package packageR, int i) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.generateApplicationInfo.call(packageR, Integer.valueOf(i), sUserState);
        } else if (API_LEVEL >= 22) {
            return PackageParserLollipop22.generateApplicationInfo.call(packageR, Integer.valueOf(i), sUserState);
        } else if (API_LEVEL >= 21) {
            return PackageParserLollipop.generateApplicationInfo.call(packageR, Integer.valueOf(i), sUserState);
        } else if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.generateApplicationInfo.call(packageR, Integer.valueOf(i), sUserState);
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.generateApplicationInfo.call(packageR, Integer.valueOf(i), false, 1);
        } else {
            return mirror.android.content.pm.PackageParser.generateApplicationInfo.call(packageR, Integer.valueOf(i));
        }
    }

    public static ActivityInfo generateActivityInfo(PackageParser.Activity activity, int i) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.generateActivityInfo.call(activity, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 22) {
            return PackageParserLollipop22.generateActivityInfo.call(activity, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 21) {
            return PackageParserLollipop.generateActivityInfo.call(activity, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.generateActivityInfo.call(activity, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.generateActivityInfo.call(activity, Integer.valueOf(i), false, 1, Integer.valueOf(myUserId));
        } else {
            return mirror.android.content.pm.PackageParser.generateActivityInfo.call(activity, Integer.valueOf(i));
        }
    }

    public static ProviderInfo generateProviderInfo(PackageParser.Provider provider, int i) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.generateProviderInfo.call(provider, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 22) {
            return PackageParserLollipop22.generateProviderInfo.call(provider, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 21) {
            return PackageParserLollipop.generateProviderInfo.call(provider, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.generateProviderInfo.call(provider, Integer.valueOf(i), sUserState, Integer.valueOf(myUserId));
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.generateProviderInfo.call(provider, Integer.valueOf(i), false, 1, Integer.valueOf(myUserId));
        } else {
            return mirror.android.content.pm.PackageParser.generateProviderInfo.call(provider, Integer.valueOf(i));
        }
    }

    public static PackageInfo generatePackageInfo(PackageParser.Package packageR, int i, long j, long j2) {
        if (API_LEVEL >= 23) {
            return PackageParserMarshmallow.generatePackageInfo.call(packageR, GIDS, Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2), null, sUserState);
        } else if (API_LEVEL >= 21) {
            if (PackageParserLollipop22.generatePackageInfo != null) {
                return PackageParserLollipop22.generatePackageInfo.call(packageR, GIDS, Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2), null, sUserState);
            }
            return PackageParserLollipop.generatePackageInfo.call(packageR, GIDS, Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2), null, sUserState);
        } else if (API_LEVEL >= 17) {
            return PackageParserJellyBean17.generatePackageInfo.call(packageR, GIDS, Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2), null, sUserState);
        } else if (API_LEVEL >= 16) {
            return PackageParserJellyBean.generatePackageInfo.call(packageR, GIDS, Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2), null);
        } else {
            return mirror.android.content.pm.PackageParser.generatePackageInfo.call(packageR, GIDS, Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2));
        }
    }

    public static void collectCertificates(PackageParser packageParser, PackageParser.Package packageR, int i) {
        if (API_LEVEL >= 24) {
            PackageParserNougat.collectCertificates.callWithException(packageR, Integer.valueOf(i));
        } else if (API_LEVEL >= 23) {
            PackageParserMarshmallow.collectCertificates.callWithException(packageParser, packageR, Integer.valueOf(i));
        } else if (API_LEVEL >= 22) {
            PackageParserLollipop22.collectCertificates.callWithException(packageParser, packageR, Integer.valueOf(i));
        } else if (API_LEVEL >= 21) {
            PackageParserLollipop.collectCertificates.callWithException(packageParser, packageR, Integer.valueOf(i));
        } else if (API_LEVEL >= 17) {
            PackageParserJellyBean17.collectCertificates.callWithException(packageParser, packageR, Integer.valueOf(i));
        } else if (API_LEVEL >= 16) {
            PackageParserJellyBean.collectCertificates.callWithException(packageParser, packageR, Integer.valueOf(i));
        } else {
            mirror.android.content.pm.PackageParser.collectCertificates.call(packageParser, packageR, Integer.valueOf(i));
        }
    }
}
