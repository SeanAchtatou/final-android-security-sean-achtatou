package com.lody.virtual.helper.compat;

import com.lody.virtual.helper.utils.Reflect;

public class SystemPropertiesCompat {
    private static Class<?> sClass;

    private static Class getSystemPropertiesClass() {
        if (sClass == null) {
            sClass = Class.forName("android.os.SystemProperties");
        }
        return sClass;
    }

    private static String getInner(String str, String str2) {
        return (String) Reflect.on((Class<?>) getSystemPropertiesClass()).call("get", str, str2).get();
    }

    public static String get(String str, String str2) {
        try {
            return getInner(str, str2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return str2;
        }
    }
}
