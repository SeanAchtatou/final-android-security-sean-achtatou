package com.lody.virtual.helper.compat;

import android.os.Parcelable;
import java.lang.reflect.Method;
import java.util.List;
import mirror.android.content.pm.ParceledListSlice;
import mirror.android.content.pm.ParceledListSliceJBMR2;

public class ParceledListSliceCompat {
    public static boolean isReturnParceledListSlice(Method method) {
        return method != null && method.getReturnType() == ParceledListSlice.TYPE;
    }

    public static Object create(List list) {
        if (ParceledListSliceJBMR2.ctor != null) {
            return ParceledListSliceJBMR2.ctor.newInstance(list);
        }
        Parcelable newInstance = ParceledListSlice.ctor.newInstance();
        for (Object next : list) {
            ParceledListSlice.append.call(newInstance, next);
        }
        ParceledListSlice.setLastSlice.call(newInstance, true);
        return newInstance;
    }
}
