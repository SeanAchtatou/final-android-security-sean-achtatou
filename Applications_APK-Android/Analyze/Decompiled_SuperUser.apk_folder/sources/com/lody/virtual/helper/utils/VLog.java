package com.lody.virtual.helper.utils;

import android.os.Bundle;
import android.util.Log;
import java.util.Set;

public class VLog {
    public static boolean OPEN_LOG = true;

    public static void i(String str, String str2, Object... objArr) {
        if (OPEN_LOG) {
            Log.i(str, String.format(str2, objArr));
        }
    }

    public static void d(String str, String str2, Object... objArr) {
        if (OPEN_LOG) {
            Log.d(str, String.format(str2, objArr));
        }
    }

    public static void w(String str, String str2, Object... objArr) {
        if (OPEN_LOG) {
            Log.w(str, String.format(str2, objArr));
        }
    }

    public static void e(String str, String str2, Object... objArr) {
        if (OPEN_LOG) {
            Log.e(str, String.format(str2, objArr));
        }
    }

    public static void v(String str, String str2, Object... objArr) {
        if (OPEN_LOG) {
            Log.v(str, String.format(str2, objArr));
        }
    }

    public static String toString(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (Reflect.on(bundle).get("mParcelledData") == null) {
            return bundle.toString();
        }
        Set<String> keySet = bundle.keySet();
        StringBuilder sb = new StringBuilder("Bundle[");
        if (keySet != null) {
            for (String next : keySet) {
                sb.append(next);
                sb.append("=");
                sb.append(bundle.get(next));
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static String getStackTraceString(Throwable th) {
        return Log.getStackTraceString(th);
    }

    public static void printStackTrace(String str) {
        Log.e(str, getStackTraceString(new Exception()));
    }

    public static void e(String str, Throwable th) {
        Log.e(str, getStackTraceString(th));
    }
}
