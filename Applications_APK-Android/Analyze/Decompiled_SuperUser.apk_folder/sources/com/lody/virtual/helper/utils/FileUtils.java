package com.lody.virtual.helper.utils;

import android.os.Build;
import android.os.Parcel;
import android.system.Os;
import android.text.TextUtils;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FileUtils {

    public interface FileMode {
        public static final int MODE_755 = 493;
        public static final int MODE_IRGRP = 32;
        public static final int MODE_IROTH = 4;
        public static final int MODE_IRUSR = 256;
        public static final int MODE_ISGID = 1024;
        public static final int MODE_ISUID = 2048;
        public static final int MODE_ISVTX = 512;
        public static final int MODE_IWGRP = 16;
        public static final int MODE_IWOTH = 2;
        public static final int MODE_IWUSR = 128;
        public static final int MODE_IXGRP = 8;
        public static final int MODE_IXOTH = 1;
        public static final int MODE_IXUSR = 64;
    }

    public static void chmod(String str, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                Os.chmod(str, i);
                return;
            } catch (Exception e2) {
            }
        }
        String str2 = "chmod ";
        if (new File(str).isDirectory()) {
            str2 = str2 + " -R ";
        }
        Runtime.getRuntime().exec(str2 + String.format("%o", Integer.valueOf(i)) + " " + str).waitFor();
    }

    public static void createSymlink(String str, String str2) {
        if (Build.VERSION.SDK_INT >= 21) {
            Os.symlink(str, str2);
        } else {
            Runtime.getRuntime().exec("ln -s " + str + " " + str2).waitFor();
        }
    }

    public static boolean isSymlink(File file) {
        if (file == null) {
            throw new NullPointerException("File must not be null");
        }
        if (file.getParent() != null) {
            file = new File(file.getParentFile().getCanonicalFile(), file.getName());
        }
        return !file.getCanonicalFile().equals(file.getAbsoluteFile());
    }

    public static void writeParcelToFile(Parcel parcel, File file) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(parcel.marshall());
        fileOutputStream.close();
    }

    public static byte[] toByteArray(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[100];
        while (true) {
            int read = inputStream.read(bArr, 0, 100);
            if (read <= 0) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static boolean deleteDir(File file) {
        if (file.isDirectory()) {
            for (String file2 : file.list()) {
                if (!deleteDir(new File(file, file2))) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    public static boolean deleteDir(String str) {
        return deleteDir(new File(str));
    }

    public static void writeToFile(InputStream inputStream, File file) {
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
        byte[] bArr = new byte[FileMode.MODE_ISGID];
        while (true) {
            int read = inputStream.read(bArr, 0, FileMode.MODE_ISGID);
            if (read != -1) {
                bufferedOutputStream.write(bArr, 0, read);
            } else {
                bufferedOutputStream.close();
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeToFile(byte[] r8, java.io.File r9) {
        /*
            r2 = 0
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream     // Catch:{ all -> 0x002a }
            r0.<init>(r8)     // Catch:{ all -> 0x002a }
            java.nio.channels.ReadableByteChannel r1 = java.nio.channels.Channels.newChannel(r0)     // Catch:{ all -> 0x002a }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ all -> 0x003d }
            r6.<init>(r9)     // Catch:{ all -> 0x003d }
            java.nio.channels.FileChannel r0 = r6.getChannel()     // Catch:{ all -> 0x0043 }
            r2 = 0
            int r4 = r8.length     // Catch:{ all -> 0x0049 }
            long r4 = (long) r4     // Catch:{ all -> 0x0049 }
            r0.transferFrom(r1, r2, r4)     // Catch:{ all -> 0x0049 }
            if (r6 == 0) goto L_0x001f
            r6.close()
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()
        L_0x0024:
            if (r0 == 0) goto L_0x0029
            r0.close()
        L_0x0029:
            return
        L_0x002a:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x002d:
            if (r3 == 0) goto L_0x0032
            r3.close()
        L_0x0032:
            if (r2 == 0) goto L_0x0037
            r2.close()
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            throw r0
        L_0x003d:
            r0 = move-exception
            r3 = r2
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x002d
        L_0x0043:
            r0 = move-exception
            r3 = r6
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x002d
        L_0x0049:
            r2 = move-exception
            r3 = r6
            r7 = r0
            r0 = r2
            r2 = r1
            r1 = r7
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.helper.utils.FileUtils.writeToFile(byte[], java.io.File):void");
    }

    public static void copyFile(File file, File file2) {
        FileOutputStream fileOutputStream;
        FileInputStream fileInputStream = null;
        try {
            FileInputStream fileInputStream2 = new FileInputStream(file);
            try {
                fileOutputStream = new FileOutputStream(file2);
                try {
                    FileChannel channel = fileInputStream2.getChannel();
                    FileChannel channel2 = fileOutputStream.getChannel();
                    ByteBuffer allocate = ByteBuffer.allocate(FileMode.MODE_ISGID);
                    while (true) {
                        allocate.clear();
                        if (channel.read(allocate) == -1) {
                            closeQuietly(fileInputStream2);
                            closeQuietly(fileOutputStream);
                            return;
                        }
                        allocate.limit(allocate.position());
                        allocate.position(0);
                        channel2.write(allocate);
                    }
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = null;
                fileInputStream = fileInputStream2;
                closeQuietly(fileInputStream);
                closeQuietly(fileOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            closeQuietly(fileInputStream);
            closeQuietly(fileOutputStream);
            throw th;
        }
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e2) {
            }
        }
    }

    public static int peekInt(byte[] bArr, int i, ByteOrder byteOrder) {
        if (byteOrder == ByteOrder.BIG_ENDIAN) {
            int i2 = i + 1;
            int i3 = i2 + 1;
            return ((bArr[i2] & 255) << 16) | ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 8) | (bArr[i3 + 1] & 255);
        }
        int i4 = i + 1;
        int i5 = i4 + 1;
        return ((bArr[i4] & 255) << 8) | (bArr[i] & 255) | ((bArr[i5] & 255) << 16) | ((bArr[i5 + 1] & 255) << 24);
    }

    private static boolean isValidExtFilenameChar(char c2) {
        switch (c2) {
            case 0:
            case '/':
                return false;
            default:
                return true;
        }
    }

    public static boolean isValidExtFilename(String str) {
        return str != null && str.equals(buildValidExtFilename(str));
    }

    public static String buildValidExtFilename(String str) {
        if (TextUtils.isEmpty(str) || ".".equals(str) || "..".equals(str)) {
            return "(invalid)";
        }
        StringBuilder sb = new StringBuilder(str.length());
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (isValidExtFilenameChar(charAt)) {
                sb.append(charAt);
            } else {
                sb.append('_');
            }
        }
        return sb.toString();
    }

    public static class FileLock {
        private static FileLock singleton;
        private Map<String, FileLockCount> mRefCountMap = new ConcurrentHashMap();

        public static FileLock getInstance() {
            if (singleton == null) {
                singleton = new FileLock();
            }
            return singleton;
        }

        private int RefCntInc(String str, java.nio.channels.FileLock fileLock, RandomAccessFile randomAccessFile, FileChannel fileChannel) {
            if (this.mRefCountMap.containsKey(str)) {
                FileLockCount fileLockCount = this.mRefCountMap.get(str);
                int i = fileLockCount.mRefCount;
                fileLockCount.mRefCount = i + 1;
                return i;
            }
            this.mRefCountMap.put(str, new FileLockCount(fileLock, 1, randomAccessFile, fileChannel));
            return 1;
        }

        private int RefCntDec(String str) {
            if (!this.mRefCountMap.containsKey(str)) {
                return 0;
            }
            FileLockCount fileLockCount = this.mRefCountMap.get(str);
            int i = fileLockCount.mRefCount - 1;
            fileLockCount.mRefCount = i;
            if (i <= 0) {
                this.mRefCountMap.remove(str);
            }
            return i;
        }

        public boolean LockExclusive(File file) {
            if (file == null) {
                return false;
            }
            try {
                File file2 = new File(file.getParentFile().getAbsolutePath().concat("/lock"));
                if (!file2.exists()) {
                    file2.createNewFile();
                }
                RandomAccessFile randomAccessFile = new RandomAccessFile(file2.getAbsolutePath(), "rw");
                FileChannel channel = randomAccessFile.getChannel();
                java.nio.channels.FileLock lock = channel.lock();
                if (!lock.isValid()) {
                    return false;
                }
                RefCntInc(file2.getAbsolutePath(), lock, randomAccessFile, channel);
                return true;
            } catch (Exception e2) {
                return false;
            }
        }

        public void unLock(File file) {
            FileLockCount fileLockCount;
            File file2 = new File(file.getParentFile().getAbsolutePath().concat("/lock"));
            if (file2.exists() && this.mRefCountMap.containsKey(file2.getAbsolutePath()) && (fileLockCount = this.mRefCountMap.get(file2.getAbsolutePath())) != null) {
                java.nio.channels.FileLock fileLock = fileLockCount.mFileLock;
                RandomAccessFile randomAccessFile = fileLockCount.fOs;
                FileChannel fileChannel = fileLockCount.fChannel;
                try {
                    if (RefCntDec(file2.getAbsolutePath()) <= 0) {
                        if (fileLock != null && fileLock.isValid()) {
                            fileLock.release();
                        }
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                        }
                        if (fileChannel != null) {
                            fileChannel.close();
                        }
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }

        private class FileLockCount {
            FileChannel fChannel;
            RandomAccessFile fOs;
            java.nio.channels.FileLock mFileLock;
            int mRefCount;

            FileLockCount(java.nio.channels.FileLock fileLock, int i, RandomAccessFile randomAccessFile, FileChannel fileChannel) {
                this.mFileLock = fileLock;
                this.mRefCount = i;
                this.fOs = randomAccessFile;
                this.fChannel = fileChannel;
            }
        }
    }
}
