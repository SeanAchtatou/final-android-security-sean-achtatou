package com.lody.virtual.helper;

import android.os.Parcel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class PersistenceLayer {
    private File mPersistenceFile;

    public abstract int getCurrentVersion();

    public abstract void readPersistenceData(Parcel parcel);

    public abstract void writePersistenceData(Parcel parcel);

    public PersistenceLayer(File file) {
        this.mPersistenceFile = file;
    }

    public final File getPersistenceFile() {
        return this.mPersistenceFile;
    }

    public void writeMagic(Parcel parcel) {
    }

    public boolean verifyMagic(Parcel parcel) {
        return true;
    }

    public boolean onVersionConflict(int i, int i2) {
        return false;
    }

    public void onPersistenceFileDamage() {
    }

    public void save() {
        Parcel obtain = Parcel.obtain();
        try {
            writeMagic(obtain);
            obtain.writeInt(getCurrentVersion());
            writePersistenceData(obtain);
            FileOutputStream fileOutputStream = new FileOutputStream(this.mPersistenceFile);
            fileOutputStream.write(obtain.marshall());
            fileOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            obtain.recycle();
        }
    }

    public void read() {
        File file = this.mPersistenceFile;
        Parcel obtain = Parcel.obtain();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bArr = new byte[((int) file.length())];
            int read = fileInputStream.read(bArr);
            fileInputStream.close();
            if (read != bArr.length) {
                throw new IOException("Unable to read Persistence file.");
            }
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            if (!verifyMagic(obtain)) {
                onPersistenceFileDamage();
                throw new IOException("Invalid persistence file.");
            }
            int readInt = obtain.readInt();
            int currentVersion = getCurrentVersion();
            if (readInt == getCurrentVersion() || onVersionConflict(readInt, currentVersion)) {
                readPersistenceData(obtain);
                return;
            }
            throw new IOException("Unable to process the bad version persistence file.");
        } catch (Exception e2) {
            if (!(e2 instanceof FileNotFoundException)) {
                e2.printStackTrace();
            }
        } finally {
            obtain.recycle();
        }
    }
}
