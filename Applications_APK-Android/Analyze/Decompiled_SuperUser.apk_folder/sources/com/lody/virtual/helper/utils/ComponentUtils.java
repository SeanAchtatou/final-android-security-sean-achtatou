package com.lody.virtual.helper.utils;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import com.lody.virtual.GmsSupport;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.SpecialComponentList;
import com.lody.virtual.helper.compat.ObjectsCompat;

public class ComponentUtils {
    public static String getTaskAffinity(ActivityInfo activityInfo) {
        if (activityInfo.launchMode == 3) {
            return "-SingleInstance-" + activityInfo.packageName + "/" + activityInfo.name;
        }
        if (activityInfo.taskAffinity == null && activityInfo.applicationInfo.taskAffinity == null) {
            return activityInfo.packageName;
        }
        if (activityInfo.taskAffinity != null) {
            return activityInfo.taskAffinity;
        }
        return activityInfo.applicationInfo.taskAffinity;
    }

    public static boolean isSameIntent(Intent intent, Intent intent2) {
        if (!(intent == null || intent2 == null)) {
            if (!ObjectsCompat.equals(intent.getAction(), intent2.getAction())) {
                return false;
            }
            if (!ObjectsCompat.equals(intent.getData(), intent2.getData())) {
                return false;
            }
            if (!ObjectsCompat.equals(intent.getType(), intent2.getType())) {
                return false;
            }
            String str = intent.getPackage();
            if (str == null && intent.getComponent() != null) {
                str = intent.getComponent().getPackageName();
            }
            String str2 = intent2.getPackage();
            if (str2 == null && intent2.getComponent() != null) {
                str2 = intent2.getComponent().getPackageName();
            }
            if (!ObjectsCompat.equals(str, str2)) {
                return false;
            }
            if (!ObjectsCompat.equals(intent.getComponent(), intent2.getComponent())) {
                return false;
            }
            if (!ObjectsCompat.equals(intent.getCategories(), intent2.getCategories())) {
                return false;
            }
        }
        return true;
    }

    public static String getProcessName(ComponentInfo componentInfo) {
        String str = componentInfo.processName;
        if (str != null) {
            return str;
        }
        String str2 = componentInfo.packageName;
        componentInfo.processName = str2;
        return str2;
    }

    public static boolean isSameComponent(ComponentInfo componentInfo, ComponentInfo componentInfo2) {
        if (componentInfo == null || componentInfo2 == null) {
            return false;
        }
        String str = componentInfo.name + "";
        String str2 = componentInfo2.name + "";
        if (!(componentInfo.packageName + "").equals(componentInfo2.packageName + "") || !str.equals(str2)) {
            return false;
        }
        return true;
    }

    public static ComponentName toComponentName(ComponentInfo componentInfo) {
        return new ComponentName(componentInfo.packageName, componentInfo.name);
    }

    public static boolean isSystemApp(ApplicationInfo applicationInfo) {
        return !GmsSupport.isGmsFamilyPackage(applicationInfo.packageName) && ((applicationInfo.flags & 1) != 0 || SpecialComponentList.isSpecSystemPackage(applicationInfo.packageName));
    }

    public static boolean isStubComponent(Intent intent) {
        return (intent == null || intent.getComponent() == null || !VirtualCore.get().getHostPkg().equals(intent.getComponent().getPackageName())) ? false : true;
    }

    public static Intent redirectBroadcastIntent(Intent intent, int i) {
        Intent cloneFilter = intent.cloneFilter();
        cloneFilter.setComponent(null);
        cloneFilter.setPackage(null);
        ComponentName component = intent.getComponent();
        String str = intent.getPackage();
        if (component != null) {
            cloneFilter.putExtra("_VA_|_user_id_", i);
            cloneFilter.setAction(String.format("_VA_%s_%s", component.getPackageName(), component.getClassName()));
            cloneFilter.putExtra("_VA_|_component_", component);
            cloneFilter.putExtra("_VA_|_intent_", new Intent(intent));
        } else if (str != null) {
            cloneFilter.putExtra("_VA_|_user_id_", i);
            cloneFilter.putExtra("_VA_|_creator_", str);
            cloneFilter.putExtra("_VA_|_intent_", new Intent(intent));
            String protectAction = SpecialComponentList.protectAction(intent.getAction());
            if (protectAction != null) {
                cloneFilter.setAction(protectAction);
            }
        } else {
            cloneFilter.putExtra("_VA_|_user_id_", i);
            cloneFilter.putExtra("_VA_|_intent_", new Intent(intent));
            String protectAction2 = SpecialComponentList.protectAction(intent.getAction());
            if (protectAction2 != null) {
                cloneFilter.setAction(protectAction2);
            }
        }
        return cloneFilter;
    }
}
