package com.lody.virtual.helper.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class Reflect {
    private final boolean isClass = true;
    /* access modifiers changed from: private */
    public final Object object;

    private Reflect(Class<?> cls) {
        this.object = cls;
    }

    private Reflect(Object obj) {
        this.object = obj;
    }

    public static Reflect on(String str) {
        return on(forName(str));
    }

    public static Reflect on(String str, ClassLoader classLoader) {
        return on(forName(str, classLoader));
    }

    public static Reflect on(Class<?> cls) {
        return new Reflect(cls);
    }

    public static Reflect on(Object obj) {
        return new Reflect(obj);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends java.lang.reflect.AccessibleObject> T accessible(T r2) {
        /*
            if (r2 != 0) goto L_0x0004
            r2 = 0
        L_0x0003:
            return r2
        L_0x0004:
            boolean r0 = r2 instanceof java.lang.reflect.Member
            if (r0 == 0) goto L_0x0023
            r0 = r2
            java.lang.reflect.Member r0 = (java.lang.reflect.Member) r0
            int r1 = r0.getModifiers()
            boolean r1 = java.lang.reflect.Modifier.isPublic(r1)
            if (r1 == 0) goto L_0x0023
            java.lang.Class r0 = r0.getDeclaringClass()
            int r0 = r0.getModifiers()
            boolean r0 = java.lang.reflect.Modifier.isPublic(r0)
            if (r0 != 0) goto L_0x0003
        L_0x0023:
            boolean r0 = r2.isAccessible()
            if (r0 != 0) goto L_0x0003
            r0 = 1
            r2.setAccessible(r0)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.helper.utils.Reflect.accessible(java.lang.reflect.AccessibleObject):java.lang.reflect.AccessibleObject");
    }

    /* access modifiers changed from: private */
    public static String property(String str) {
        int length = str.length();
        if (length == 0) {
            return "";
        }
        if (length == 1) {
            return str.toLowerCase();
        }
        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    private static Reflect on(Constructor<?> constructor, Object... objArr) {
        try {
            return on(((Constructor) accessible(constructor)).newInstance(objArr));
        } catch (Exception e2) {
            throw new ReflectException(e2);
        }
    }

    private static Reflect on(Method method, Object obj, Object... objArr) {
        try {
            accessible(method);
            if (method.getReturnType() != Void.TYPE) {
                return on(method.invoke(obj, objArr));
            }
            method.invoke(obj, objArr);
            return on(obj);
        } catch (Exception e2) {
            throw new ReflectException(e2);
        }
    }

    private static Object unwrap(Object obj) {
        if (obj instanceof Reflect) {
            return ((Reflect) obj).get();
        }
        return obj;
    }

    private static Class<?>[] types(Object... objArr) {
        if (objArr == null) {
            return new Class[0];
        }
        Class<?>[] clsArr = new Class[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            clsArr[i] = obj == null ? NULL.class : obj.getClass();
        }
        return clsArr;
    }

    private static Class<?> forName(String str) {
        try {
            return Class.forName(str);
        } catch (Exception e2) {
            throw new ReflectException(e2);
        }
    }

    private static Class<?> forName(String str, ClassLoader classLoader) {
        try {
            return Class.forName(str, true, classLoader);
        } catch (Exception e2) {
            throw new ReflectException(e2);
        }
    }

    public static Class<?> wrapper(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        if (!cls.isPrimitive()) {
            return cls;
        }
        if (Boolean.TYPE == cls) {
            return Boolean.class;
        }
        if (Integer.TYPE == cls) {
            return Integer.class;
        }
        if (Long.TYPE == cls) {
            return Long.class;
        }
        if (Short.TYPE == cls) {
            return Short.class;
        }
        if (Byte.TYPE == cls) {
            return Byte.class;
        }
        if (Double.TYPE == cls) {
            return Double.class;
        }
        if (Float.TYPE == cls) {
            return Float.class;
        }
        if (Character.TYPE == cls) {
            return Character.class;
        }
        if (Void.TYPE == cls) {
            return Void.class;
        }
        return cls;
    }

    public <T> T get() {
        return this.object;
    }

    public Reflect set(String str, Object obj) {
        try {
            Field field0 = field0(str);
            field0.setAccessible(true);
            field0.set(this.object, unwrap(obj));
            return this;
        } catch (Exception e2) {
            throw new ReflectException(e2);
        }
    }

    public <T> T get(String str) {
        return field(str).get();
    }

    public Reflect field(String str) {
        try {
            return on(field0(str).get(this.object));
        } catch (Exception e2) {
            throw new ReflectException(this.object.getClass().getName(), e2);
        }
    }

    private Field field0(String str) {
        Class<?> cls;
        Class<?> type = type();
        try {
            return type.getField(str);
        } catch (NoSuchFieldException e2) {
            while (true) {
                try {
                    cls = type;
                    return (Field) accessible(cls.getDeclaredField(str));
                } catch (NoSuchFieldException e3) {
                    type = cls.getSuperclass();
                    if (type == null) {
                        throw new ReflectException(e2);
                    }
                }
            }
        }
    }

    public Map<String, Reflect> fields() {
        boolean z;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Class<?> type = type();
        do {
            for (Field field : type.getDeclaredFields()) {
                if (!this.isClass) {
                    z = true;
                } else {
                    z = false;
                }
                if (z ^ Modifier.isStatic(field.getModifiers())) {
                    String name = field.getName();
                    if (!linkedHashMap.containsKey(name)) {
                        linkedHashMap.put(name, field(name));
                    }
                }
            }
            type = type.getSuperclass();
        } while (type != null);
        return linkedHashMap;
    }

    public Reflect call(String str) {
        return call(str, new Object[0]);
    }

    public Reflect call(String str, Object... objArr) {
        Class<?>[] types = types(objArr);
        try {
            return on(exactMethod(str, types), this.object, objArr);
        } catch (NoSuchMethodException e2) {
            try {
                return on(similarMethod(str, types), this.object, objArr);
            } catch (NoSuchMethodException e3) {
                throw new ReflectException(e3);
            }
        }
    }

    public Method exactMethod(String str, Class<?>[] clsArr) {
        Class<?> type = type();
        try {
            return type.getMethod(str, clsArr);
        } catch (NoSuchMethodException e2) {
            do {
                try {
                    return type.getDeclaredMethod(str, clsArr);
                } catch (NoSuchMethodException e3) {
                    type = type.getSuperclass();
                    if (type != null) {
                        throw new NoSuchMethodException();
                    }
                }
            } while (type != null);
            throw new NoSuchMethodException();
        }
    }

    private Method similarMethod(String str, Class<?>[] clsArr) {
        Class<?> type = type();
        for (Method method : type.getMethods()) {
            if (isSimilarSignature(method, str, clsArr)) {
                return method;
            }
        }
        do {
            for (Method method2 : type.getDeclaredMethods()) {
                if (isSimilarSignature(method2, str, clsArr)) {
                    return method2;
                }
            }
            type = type.getSuperclass();
        } while (type != null);
        throw new NoSuchMethodException("No similar method " + str + " with params " + Arrays.toString(clsArr) + " could be found on type " + type() + ".");
    }

    private boolean isSimilarSignature(Method method, String str, Class<?>[] clsArr) {
        return method.getName().equals(str) && match(method.getParameterTypes(), clsArr);
    }

    public Reflect create() {
        return create(new Object[0]);
    }

    public Reflect create(Object... objArr) {
        Class<?>[] types = types(objArr);
        try {
            return on(type().getDeclaredConstructor(types), objArr);
        } catch (NoSuchMethodException e2) {
            for (Constructor<?> constructor : type().getDeclaredConstructors()) {
                if (match(constructor.getParameterTypes(), types)) {
                    return on(constructor, objArr);
                }
            }
            throw new ReflectException(e2);
        }
    }

    public <P> P as(Class<P> cls) {
        final boolean z = this.object instanceof Map;
        AnonymousClass1 r1 = new InvocationHandler() {
            public Object invoke(Object obj, Method method, Object[] objArr) {
                String name = method.getName();
                try {
                    return Reflect.on(Reflect.this.object).call(name, objArr).get();
                } catch (ReflectException e2) {
                    ReflectException reflectException = e2;
                    if (z) {
                        Map map = (Map) Reflect.this.object;
                        int length = objArr == null ? 0 : objArr.length;
                        if (length == 0 && name.startsWith("get")) {
                            return map.get(Reflect.property(name.substring(3)));
                        }
                        if (length == 0 && name.startsWith("is")) {
                            return map.get(Reflect.property(name.substring(2)));
                        }
                        if (length == 1 && name.startsWith("set")) {
                            map.put(Reflect.property(name.substring(3)), objArr[0]);
                            return null;
                        }
                    }
                    throw reflectException;
                }
            }
        };
        return Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, r1);
    }

    private boolean match(Class<?>[] clsArr, Class<?>[] clsArr2) {
        if (clsArr.length != clsArr2.length) {
            return false;
        }
        for (int i = 0; i < clsArr2.length; i++) {
            if (clsArr2[i] != NULL.class && !wrapper(clsArr[i]).isAssignableFrom(wrapper(clsArr2[i]))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return this.object.hashCode();
    }

    public boolean equals(Object obj) {
        return (obj instanceof Reflect) && this.object.equals(((Reflect) obj).get());
    }

    public String toString() {
        return this.object.toString();
    }

    public Class<?> type() {
        if (this.isClass) {
            return (Class) this.object;
        }
        return this.object.getClass();
    }

    public static String getMethodDetails(Method method) {
        StringBuilder sb = new StringBuilder(40);
        sb.append(Modifier.toString(method.getModifiers())).append(" ").append(method.getReturnType().getName()).append(" ").append(method.getName()).append("(");
        Class<?>[] parameterTypes = method.getParameterTypes();
        for (Class<?> name : parameterTypes) {
            sb.append(name.getName()).append(", ");
        }
        if (parameterTypes.length > 0) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(")");
        return sb.toString();
    }

    private static class NULL {
        private NULL() {
        }
    }

    public Reflect callBest(String str, Object... objArr) {
        Object[] objArr2;
        Class<?>[] types = types(objArr);
        Method method = null;
        Method[] declaredMethods = type().getDeclaredMethods();
        int length = declaredMethods.length;
        int i = 0;
        char c2 = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Method method2 = declaredMethods[i];
            if (isSimilarSignature(method2, str, types)) {
                c2 = 2;
                method = method2;
                break;
            }
            if (matchObjectMethod(method2, str, types)) {
                c2 = 1;
            } else if (!method2.getName().equals(str) || method2.getParameterTypes().length != 0 || c2 != 0) {
                method2 = method;
            }
            i++;
            method = method2;
        }
        if (method != null) {
            if (c2 == 0) {
                objArr2 = new Object[0];
            } else {
                objArr2 = objArr;
            }
            return on(method, this.object, c2 == 1 ? new Object[]{objArr2} : objArr2);
        }
        throw new ReflectException("no method found for " + str, new NoSuchMethodException("No best method " + str + " with params " + Arrays.toString(types) + " could be found on type " + type() + "."));
    }

    private boolean matchObjectMethod(Method method, String str, Class<?>[] clsArr) {
        return method.getName().equals(str) && matchObject(method.getParameterTypes());
    }

    private boolean matchObject(Class<?>[] clsArr) {
        Class<Object[]> cls = Object[].class;
        if (clsArr.length <= 0 || !clsArr[0].isAssignableFrom(cls)) {
            return false;
        }
        return true;
    }
}
