package com.lody.virtual.helper.utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public interface XmlSerializerAndParser<T> {
    T createFromXml(XmlPullParser xmlPullParser);

    void writeAsXml(T t, XmlSerializer xmlSerializer);
}
