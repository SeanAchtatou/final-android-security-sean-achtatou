package com.lody.virtual.os;

import android.content.Context;
import android.os.Build;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import java.io.File;

public class VEnvironment {
    private static final File DALVIK_CACHE_DIRECTORY = ensureCreated(new File(ROOT, "opt"));
    private static final File DATA_DIRECTORY = ensureCreated(new File(ROOT, "data"));
    private static final File ROOT = ensureCreated(new File(new File(getContext().getApplicationInfo().dataDir), "virtual"));
    private static final String TAG = VEnvironment.class.getSimpleName();
    private static final File USER_DIRECTORY = ensureCreated(new File(DATA_DIRECTORY, ServiceManagerNative.USER));

    public static void systemReady() {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                FileUtils.chmod(ROOT.getAbsolutePath(), FileUtils.FileMode.MODE_755);
                FileUtils.chmod(DATA_DIRECTORY.getAbsolutePath(), FileUtils.FileMode.MODE_755);
                FileUtils.chmod(getDataAppDirectory().getAbsolutePath(), FileUtils.FileMode.MODE_755);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static Context getContext() {
        return VirtualCore.get().getContext();
    }

    private static File ensureCreated(File file) {
        if (!file.exists() && !file.mkdirs()) {
            VLog.w(TAG, "Unable to create the directory: %s.", file.getPath());
        }
        return file;
    }

    public static File getDataUserPackageDirectory(int i, String str) {
        return ensureCreated(new File(getUserSystemDirectory(i), str));
    }

    public static File getPackageResourcePath(String str) {
        return new File(getDataAppPackageDirectory(str), "base.apk");
    }

    public static File getDataAppDirectory() {
        return ensureCreated(new File(getDataDirectory(), ServiceManagerNative.APP));
    }

    public static File getUidListFile() {
        return new File(getSystemSecureDirectory(), "uid-list.ini");
    }

    public static File getBakUidListFile() {
        return new File(getSystemSecureDirectory(), "uid-list.ini.bak");
    }

    public static File getAccountConfigFile() {
        return new File(getSystemSecureDirectory(), "account-list.ini");
    }

    public static File getVirtualLocationFile() {
        return new File(getSystemSecureDirectory(), "virtual-loc.ini");
    }

    public static File getDeviceInfoFile() {
        return new File(getSystemSecureDirectory(), "device-info.ini");
    }

    public static File getPackageListFile() {
        return new File(getSystemSecureDirectory(), "packages.ini");
    }

    public static File getVSConfigFile() {
        return new File(getSystemSecureDirectory(), "vss.ini");
    }

    public static File getBakPackageListFile() {
        return new File(getSystemSecureDirectory(), "packages.ini.bak");
    }

    public static File getJobConfigFile() {
        return new File(getSystemSecureDirectory(), "job-list.ini");
    }

    public static File getDalvikCacheDirectory() {
        return DALVIK_CACHE_DIRECTORY;
    }

    public static File getOdexFile(String str) {
        return new File(DALVIK_CACHE_DIRECTORY, "data@app@" + str + "-1@base.apk@classes.dex");
    }

    public static File getDataAppPackageDirectory(String str) {
        return ensureCreated(new File(getDataAppDirectory(), str));
    }

    public static File getPackageCacheFile(String str) {
        return new File(getDataAppPackageDirectory(str), "package.ini");
    }

    public static File getSignatureFile(String str) {
        return new File(getDataAppPackageDirectory(str), "signature.ini");
    }

    public static File getUserSystemDirectory() {
        return USER_DIRECTORY;
    }

    public static File getUserSystemDirectory(int i) {
        return new File(USER_DIRECTORY, String.valueOf(i));
    }

    public static File getWifiMacFile(int i) {
        return new File(getUserSystemDirectory(i), "wifiMacAddress");
    }

    public static File getDataDirectory() {
        return DATA_DIRECTORY;
    }

    public static File getSystemSecureDirectory() {
        return ensureCreated(new File(getDataAppDirectory(), "system"));
    }

    public static File getPackageInstallerStageDir() {
        return ensureCreated(new File(DATA_DIRECTORY, ".session_dir"));
    }
}
