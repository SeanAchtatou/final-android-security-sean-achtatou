package com.lody.virtual.os;

import android.os.Parcel;
import android.os.Parcelable;

public class VUserInfo implements Parcelable {
    public static final Parcelable.Creator<VUserInfo> CREATOR = new Parcelable.Creator<VUserInfo>() {
        public VUserInfo createFromParcel(Parcel parcel) {
            return new VUserInfo(parcel);
        }

        public VUserInfo[] newArray(int i) {
            return new VUserInfo[i];
        }
    };
    public static final int FLAG_ADMIN = 2;
    public static final int FLAG_DISABLED = 64;
    public static final int FLAG_GUEST = 4;
    public static final int FLAG_INITIALIZED = 16;
    public static final int FLAG_MANAGED_PROFILE = 32;
    public static final int FLAG_MASK_USER_TYPE = 255;
    public static final int FLAG_PRIMARY = 1;
    public static final int FLAG_RESTRICTED = 8;
    public static final int NO_PROFILE_GROUP_ID = -1;
    public long creationTime;
    public int flags;
    public boolean guestToRemove;
    public String iconPath;
    public int id;
    public long lastLoggedInTime;
    public String name;
    public boolean partial;
    public int profileGroupId;
    public int serialNumber;

    public VUserInfo(int i, String str, int i2) {
        this(i, str, null, i2);
    }

    public VUserInfo(int i, String str, String str2, int i2) {
        this.id = i;
        this.name = str;
        this.flags = i2;
        this.iconPath = str2;
        this.profileGroupId = -1;
    }

    public boolean isPrimary() {
        return (this.flags & 1) == 1;
    }

    public boolean isAdmin() {
        return (this.flags & 2) == 2;
    }

    public boolean isGuest() {
        return (this.flags & 4) == 4;
    }

    public boolean isRestricted() {
        return (this.flags & 8) == 8;
    }

    public boolean isManagedProfile() {
        return (this.flags & 32) == 32;
    }

    public boolean isEnabled() {
        return (this.flags & 64) != 64;
    }

    public VUserInfo() {
    }

    public VUserInfo(VUserInfo vUserInfo) {
        this.name = vUserInfo.name;
        this.iconPath = vUserInfo.iconPath;
        this.id = vUserInfo.id;
        this.flags = vUserInfo.flags;
        this.serialNumber = vUserInfo.serialNumber;
        this.creationTime = vUserInfo.creationTime;
        this.lastLoggedInTime = vUserInfo.lastLoggedInTime;
        this.partial = vUserInfo.partial;
        this.profileGroupId = vUserInfo.profileGroupId;
        this.guestToRemove = vUserInfo.guestToRemove;
    }

    public String toString() {
        return "UserInfo{" + this.id + ":" + this.name + ":" + Integer.toHexString(this.flags) + "}";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        int i3 = 1;
        parcel.writeInt(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.iconPath);
        parcel.writeInt(this.flags);
        parcel.writeInt(this.serialNumber);
        parcel.writeLong(this.creationTime);
        parcel.writeLong(this.lastLoggedInTime);
        if (this.partial) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeInt(this.profileGroupId);
        if (!this.guestToRemove) {
            i3 = 0;
        }
        parcel.writeInt(i3);
    }

    private VUserInfo(Parcel parcel) {
        boolean z;
        boolean z2 = true;
        this.id = parcel.readInt();
        this.name = parcel.readString();
        this.iconPath = parcel.readString();
        this.flags = parcel.readInt();
        this.serialNumber = parcel.readInt();
        this.creationTime = parcel.readLong();
        this.lastLoggedInTime = parcel.readLong();
        if (parcel.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.partial = z;
        this.profileGroupId = parcel.readInt();
        this.guestToRemove = parcel.readInt() == 0 ? false : z2;
    }
}
