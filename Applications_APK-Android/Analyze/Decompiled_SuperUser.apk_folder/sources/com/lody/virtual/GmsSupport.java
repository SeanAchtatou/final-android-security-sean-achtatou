package com.lody.virtual;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.lody.virtual.client.core.VirtualCore;
import java.util.Arrays;
import java.util.List;

public class GmsSupport {
    private static final List<String> GOOGLE_APP = Arrays.asList("com.android.vending", "com.google.android.play.games", "com.google.android.wearable.app", "com.google.android.wearable.app.cn");
    private static final List<String> GOOGLE_SERVICE = Arrays.asList("com.google.android.gsf", "com.google.android.gms", "com.google.android.gsf.login", "com.google.android.backuptransport", "com.google.android.backup", "com.google.android.configupdater", "com.google.android.syncadapters.contacts", "com.google.android.feedback", "com.google.android.onetimeinitializer", "com.google.android.partnersetup", "com.google.android.setupwizard", "com.google.android.syncadapters.calendar");

    public static boolean isGmsFamilyPackage(String str) {
        return str.equals("com.android.vending") || str.equals("com.google.android.gms");
    }

    public static boolean isGoogleFrameworkInstalled() {
        return VirtualCore.get().isAppInstalled("com.google.android.gms");
    }

    public static boolean isOutsideGoogleFrameworkExist() {
        return VirtualCore.get().isOutsideInstalled("com.google.android.gms");
    }

    private static void installPackages(List<String> list, int i) {
        VirtualCore virtualCore = VirtualCore.get();
        for (String next : list) {
            if (!virtualCore.isAppInstalledAsUser(i, next)) {
                ApplicationInfo applicationInfo = null;
                try {
                    applicationInfo = VirtualCore.get().getUnHookPackageManager().getApplicationInfo(next, 0);
                } catch (PackageManager.NameNotFoundException e2) {
                }
                if (!(applicationInfo == null || applicationInfo.sourceDir == null)) {
                    if (i == 0) {
                        virtualCore.installPackage(applicationInfo.sourceDir, 32);
                    } else {
                        virtualCore.installPackageAsUser(i, next);
                    }
                }
            }
        }
    }

    public static void installGApps(int i) {
        installPackages(GOOGLE_SERVICE, i);
        installPackages(GOOGLE_APP, i);
    }

    public static void installGoogleService(int i) {
        installPackages(GOOGLE_SERVICE, i);
    }

    public static void installGoogleApp(int i) {
        installPackages(GOOGLE_APP, i);
    }
}
