package com.lody.virtual;

public final class R {

    public static final class dimen {

        /* renamed from: match_parent */
        public static final int ez = 2131230943;

        /* renamed from: notification_max_height */
        public static final int f7 = 2131230951;

        /* renamed from: notification_mid_height */
        public static final int f9 = 2131230953;

        /* renamed from: notification_min_height */
        public static final int f_ = 2131230954;

        /* renamed from: notification_padding */
        public static final int fa = 2131230955;

        /* renamed from: notification_panel_width */
        public static final int fb = 2131230956;

        /* renamed from: notification_side_padding */
        public static final int fe = 2131230959;

        /* renamed from: standard_notification_panel_width */
        public static final int fw = 2131230977;
    }

    public static final class id {

        /* renamed from: account_row_icon */
        public static final int fg = 2131624164;

        /* renamed from: account_row_text */
        public static final int fh = 2131624165;

        /* renamed from: btn_1 */
        public static final int fu = 2131624178;

        /* renamed from: btn_10 */
        public static final int g3 = 2131624187;

        /* renamed from: btn_11 */
        public static final int g4 = 2131624188;

        /* renamed from: btn_12 */
        public static final int g5 = 2131624189;

        /* renamed from: btn_13 */
        public static final int g6 = 2131624190;

        /* renamed from: btn_14 */
        public static final int g7 = 2131624191;

        /* renamed from: btn_15 */
        public static final int g8 = 2131624192;

        /* renamed from: btn_16 */
        public static final int g9 = 2131624193;

        /* renamed from: btn_17 */
        public static final int g_ = 2131624194;

        /* renamed from: btn_18 */
        public static final int ga = 2131624195;

        /* renamed from: btn_19 */
        public static final int gb = 2131624196;

        /* renamed from: btn_2 */
        public static final int fv = 2131624179;

        /* renamed from: btn_20 */
        public static final int gc = 2131624197;

        /* renamed from: btn_21 */
        public static final int gd = 2131624198;

        /* renamed from: btn_22 */
        public static final int ge = 2131624199;

        /* renamed from: btn_23 */
        public static final int gf = 2131624200;

        /* renamed from: btn_24 */
        public static final int gg = 2131624201;

        /* renamed from: btn_25 */
        public static final int gh = 2131624202;

        /* renamed from: btn_26 */
        public static final int gi = 2131624203;

        /* renamed from: btn_27 */
        public static final int gj = 2131624204;

        /* renamed from: btn_28 */
        public static final int gk = 2131624205;

        /* renamed from: btn_29 */
        public static final int gl = 2131624206;

        /* renamed from: btn_3 */
        public static final int fw = 2131624180;

        /* renamed from: btn_30 */
        public static final int gm = 2131624207;

        /* renamed from: btn_31 */
        public static final int gn = 2131624208;

        /* renamed from: btn_32 */
        public static final int go = 2131624209;

        /* renamed from: btn_4 */
        public static final int fx = 2131624181;

        /* renamed from: btn_5 */
        public static final int fy = 2131624182;

        /* renamed from: btn_6 */
        public static final int fz = 2131624183;

        /* renamed from: btn_7 */
        public static final int g0 = 2131624184;

        /* renamed from: btn_8 */
        public static final int g1 = 2131624185;

        /* renamed from: btn_9 */
        public static final int g2 = 2131624186;

        /* renamed from: button_bar */
        public static final int f0 = 2131624147;

        /* renamed from: description */
        public static final int ez = 2131624146;
        public static final int icon = 2131624019;

        /* renamed from: im_main */
        public static final int ft = 2131624177;
        public static final int text1 = 2131624395;
        public static final int text2 = 2131624344;
    }

    public static final class integer {

        /* renamed from: config_maxResolverActivityColumns  reason: collision with root package name */
        public static final int f4467config_maxResolverActivityColumns = 2131492870;
    }

    public static final class layout {

        /* renamed from: app_not_authorized */
        public static final int ah = 2130968620;

        /* renamed from: choose_account_row */
        public static final int am = 2130968625;

        /* renamed from: choose_account_type */
        public static final int an = 2130968626;

        /* renamed from: choose_type_and_account */
        public static final int ao = 2130968627;

        /* renamed from: custom_notification */
        public static final int au = 2130968633;

        /* renamed from: custom_notification_lite */
        public static final int av = 2130968634;

        /* renamed from: resolve_list_item */
        public static final int ci = 2130968701;
    }

    public static final class string {

        /* renamed from: add_account_button_label */
        public static final int g3 = 2131296507;

        /* renamed from: choose */
        public static final int g9 = 2131296513;

        /* renamed from: choose_empty */
        public static final int g_ = 2131296514;

        /* renamed from: engine_process_name */
        public static final int gc = 2131296519;

        /* renamed from: noApplications */
        public static final int ge = 2131296526;

        /* renamed from: owner_name */
        public static final int gg = 2131296528;

        /* renamed from: virtual_installer */
        public static final int gq = 2131296538;
    }

    public static final class style {

        /* renamed from: VAAlertTheme */
        public static final int jg = 2131362192;

        /* renamed from: VATheme */
        public static final int jh = 2131362193;

        /* renamed from: notification_button */
        public static final int ls = 2131362278;

        /* renamed from: notification_layout */
        public static final int lt = 2131362279;
    }
}
