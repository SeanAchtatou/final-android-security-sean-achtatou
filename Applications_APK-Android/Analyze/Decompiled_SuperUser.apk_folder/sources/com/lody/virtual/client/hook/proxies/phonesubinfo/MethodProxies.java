package com.lody.virtual.client.hook.proxies.phonesubinfo;

import com.lody.virtual.client.hook.base.MethodProxy;
import java.lang.reflect.Method;

class MethodProxies {
    MethodProxies() {
    }

    static class GetDeviceId extends MethodProxy {
        GetDeviceId() {
        }

        public String getMethodName() {
            return "getDeviceId";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return getDeviceInfo().deviceId;
        }
    }

    static class GetDeviceIdForSubscriber extends GetDeviceId {
        GetDeviceIdForSubscriber() {
        }

        public String getMethodName() {
            return "getDeviceIdForSubscriber";
        }
    }

    static class GetIccSerialNumber extends MethodProxy {
        GetIccSerialNumber() {
        }

        public String getMethodName() {
            return "getIccSerialNumber";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return getDeviceInfo().iccId;
        }
    }

    static class getIccSerialNumberForSubscriber extends GetIccSerialNumber {
        getIccSerialNumberForSubscriber() {
        }

        public String getMethodName() {
            return "getIccSerialNumberForSubscriber";
        }
    }
}
