package com.lody.virtual.client.hook.providers;

import android.os.Build;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.client.hook.base.MethodBox;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class SettingsProviderHook extends ExternalProviderHook {
    private static final int METHOD_GET = 0;
    private static final int METHOD_PUT = 1;
    private static final Map<String, String> PRE_SET_VALUES = new HashMap();
    private static final String TAG = SettingsProviderHook.class.getSimpleName();

    static {
        PRE_SET_VALUES.put("user_setup_complete", "1");
        PRE_SET_VALUES.put("install_non_market_apps", "0");
    }

    public SettingsProviderHook(Object obj) {
        super(obj);
    }

    private static int getMethodType(String str) {
        if (str.startsWith("GET_")) {
            return 0;
        }
        if (str.startsWith("PUT_")) {
            return 1;
        }
        return -1;
    }

    private static boolean isSecureMethod(String str) {
        return str.endsWith("secure");
    }

    public Bundle call(MethodBox methodBox, String str, String str2, Bundle bundle) {
        String str3;
        int methodType = getMethodType(str);
        if (methodType == 0 && (str3 = PRE_SET_VALUES.get(str2)) != null) {
            return wrapBundle(str2, str3);
        }
        if (1 == methodType && isSecureMethod(str)) {
            return null;
        }
        try {
            return (Bundle) methodBox.call();
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof SecurityException) {
                return null;
            }
            throw e2;
        }
    }

    private Bundle wrapBundle(String str, String str2) {
        Bundle bundle = new Bundle();
        if (Build.VERSION.SDK_INT >= 24) {
            bundle.putString("name", str);
            bundle.putString(FirebaseAnalytics.Param.VALUE, str2);
        } else {
            bundle.putString(str, str2);
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void processArgs(Method method, Object... objArr) {
        super.processArgs(method, objArr);
    }
}
