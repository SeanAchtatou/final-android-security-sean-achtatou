package com.lody.virtual.client.hook.proxies.bluetooth;

import android.os.Build;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.StaticMethodProxy;
import java.lang.reflect.Method;
import mirror.android.bluetooth.IBluetooth;

public class BluetoothStub extends BinderInvocationProxy {
    public static final String SERVICE_NAME = (Build.VERSION.SDK_INT >= 17 ? "bluetooth_manager" : "bluetooth");

    public BluetoothStub() {
        super(IBluetooth.Stub.asInterface, SERVICE_NAME);
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new GetAddress());
    }

    private static class GetAddress extends StaticMethodProxy {
        GetAddress() {
            super("getAddress");
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return getDeviceInfo().bluetoothMac;
        }
    }
}
