package com.lody.virtual.client.ipc;

import android.os.RemoteException;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.remote.VDeviceInfo;
import com.lody.virtual.server.IDeviceInfoManager;

public class VDeviceManager {
    private static final VDeviceManager sInstance = new VDeviceManager();
    private IDeviceInfoManager mRemote;

    public static VDeviceManager get() {
        return sInstance;
    }

    public IDeviceInfoManager getRemote() {
        if (this.mRemote == null || (!this.mRemote.asBinder().isBinderAlive() && !VirtualCore.get().isVAppProcess())) {
            synchronized (this) {
                this.mRemote = (IDeviceInfoManager) LocalProxyUtils.genProxy(IDeviceInfoManager.class, getRemoteInterface());
            }
        }
        return this.mRemote;
    }

    private Object getRemoteInterface() {
        return IDeviceInfoManager.Stub.asInterface(ServiceManagerNative.getService(ServiceManagerNative.DEVICE));
    }

    public VDeviceInfo getDeviceInfo(int i) {
        try {
            return getRemote().getDeviceInfo(i);
        } catch (RemoteException e2) {
            return (VDeviceInfo) VirtualRuntime.crash(e2);
        }
    }
}
