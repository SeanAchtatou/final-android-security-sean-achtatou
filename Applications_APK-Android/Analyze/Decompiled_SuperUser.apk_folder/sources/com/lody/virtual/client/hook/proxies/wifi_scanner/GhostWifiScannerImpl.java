package com.lody.virtual.client.hook.proxies.wifi_scanner;

import android.net.wifi.a;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Messenger;
import java.util.ArrayList;
import mirror.android.net.wifi.WifiScanner;

public class GhostWifiScannerImpl extends a.C0006a {
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public Messenger getMessenger() {
        return new Messenger(this.mHandler);
    }

    public Bundle getAvailableChannels(int i) {
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(WifiScanner.GET_AVAILABLE_CHANNELS_EXTRA.get(), new ArrayList(0));
        return bundle;
    }
}
