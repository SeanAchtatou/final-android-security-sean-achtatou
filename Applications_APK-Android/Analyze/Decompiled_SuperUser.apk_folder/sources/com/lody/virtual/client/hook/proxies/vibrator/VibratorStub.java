package com.lody.virtual.client.hook.proxies.vibrator;

import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.ReplaceCallingPkgMethodProxy;
import java.lang.reflect.Method;
import mirror.com.android.internal.os.IVibratorService;

public class VibratorStub extends BinderInvocationProxy {
    public VibratorStub() {
        super(IVibratorService.Stub.asInterface, "vibrator");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        addMethodProxy(new VibrateMethodProxy("vibrateMagnitude"));
        addMethodProxy(new VibrateMethodProxy("vibratePatternMagnitude"));
        addMethodProxy(new VibrateMethodProxy("vibrate"));
        addMethodProxy(new VibrateMethodProxy("vibratePattern"));
    }

    private static final class VibrateMethodProxy extends ReplaceCallingPkgMethodProxy {
        private VibrateMethodProxy(String str) {
            super(str);
        }

        public boolean beforeCall(Object obj, Method method, Object... objArr) {
            if (objArr[0] instanceof Integer) {
                objArr[0] = Integer.valueOf(getRealUid());
            }
            return super.beforeCall(obj, method, objArr);
        }
    }
}
