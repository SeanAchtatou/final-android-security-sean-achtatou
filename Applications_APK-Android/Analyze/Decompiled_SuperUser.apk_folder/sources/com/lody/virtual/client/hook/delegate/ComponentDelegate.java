package com.lody.virtual.client.hook.delegate;

import android.app.Activity;
import android.content.Intent;

public interface ComponentDelegate {
    public static final ComponentDelegate EMPTY = new ComponentDelegate() {
        public void beforeActivityCreate(Activity activity) {
        }

        public void beforeActivityResume(Activity activity) {
        }

        public void beforeActivityPause(Activity activity) {
        }

        public void beforeActivityDestroy(Activity activity) {
        }

        public void afterActivityCreate(Activity activity) {
        }

        public void afterActivityResume(Activity activity) {
        }

        public void afterActivityPause(Activity activity) {
        }

        public void afterActivityDestroy(Activity activity) {
        }

        public void onSendBroadcast(Intent intent) {
        }
    };

    void afterActivityCreate(Activity activity);

    void afterActivityDestroy(Activity activity);

    void afterActivityPause(Activity activity);

    void afterActivityResume(Activity activity);

    void beforeActivityCreate(Activity activity);

    void beforeActivityDestroy(Activity activity);

    void beforeActivityPause(Activity activity);

    void beforeActivityResume(Activity activity);

    void onSendBroadcast(Intent intent);
}
