package com.lody.virtual.client.hook.proxies.window.session;

import android.os.IInterface;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.lody.virtual.client.hook.base.MethodInvocationProxy;
import com.lody.virtual.client.hook.base.MethodInvocationStub;

public class WindowSessionPatch extends MethodInvocationProxy<MethodInvocationStub<IInterface>> {
    public WindowSessionPatch(IInterface iInterface) {
        super(new MethodInvocationStub(iInterface));
    }

    public void onBindMethods() {
        addMethodProxy(new BaseMethodProxy(ProductAction.ACTION_ADD));
        addMethodProxy(new BaseMethodProxy("addToDisplay"));
        addMethodProxy(new BaseMethodProxy("addToDisplayWithoutInputChannel"));
        addMethodProxy(new BaseMethodProxy("addWithoutInputChannel"));
        addMethodProxy(new BaseMethodProxy("relayout"));
    }

    public void inject() {
    }

    public boolean isEnvBad() {
        return getInvocationStub().getProxyInterface() != null;
    }
}
