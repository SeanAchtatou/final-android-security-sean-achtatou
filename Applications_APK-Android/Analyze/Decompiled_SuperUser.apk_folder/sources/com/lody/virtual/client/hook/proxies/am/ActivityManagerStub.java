package com.lody.virtual.client.hook.proxies.am;

import android.app.ActivityManager;
import android.os.Build;
import android.os.IInterface;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.BinderInvocationStub;
import com.lody.virtual.client.hook.base.Inject;
import com.lody.virtual.client.hook.base.MethodInvocationProxy;
import com.lody.virtual.client.hook.base.MethodInvocationStub;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.base.ReplaceCallingPkgMethodProxy;
import com.lody.virtual.client.hook.base.ReplaceLastUidMethodProxy;
import com.lody.virtual.client.hook.base.ResultStaticMethodProxy;
import com.lody.virtual.client.hook.base.StaticMethodProxy;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.compat.BuildCompat;
import com.lody.virtual.helper.compat.ParceledListSliceCompat;
import com.lody.virtual.remote.AppTaskInfo;
import java.lang.reflect.Method;
import java.util.List;
import mirror.android.app.ActivityManagerNative;
import mirror.android.app.ActivityManagerOreo;
import mirror.android.app.IActivityManager;
import mirror.android.content.pm.ParceledListSlice;
import mirror.android.os.ServiceManager;
import mirror.android.util.Singleton;

@Inject(MethodProxies.class)
public class ActivityManagerStub extends MethodInvocationProxy<MethodInvocationStub<IInterface>> {
    public ActivityManagerStub() {
        super(new MethodInvocationStub(ActivityManagerNative.getDefault.call(new Object[0])));
    }

    public void inject() {
        if (BuildCompat.isOreo()) {
            Singleton.mInstance.set(ActivityManagerOreo.IActivityManagerSingleton.get(), getInvocationStub().getProxyInterface());
        } else if (ActivityManagerNative.gDefault.type() == IActivityManager.TYPE) {
            ActivityManagerNative.gDefault.set(getInvocationStub().getProxyInterface());
        } else if (ActivityManagerNative.gDefault.type() == Singleton.TYPE) {
            Singleton.mInstance.set(ActivityManagerNative.gDefault.get(), getInvocationStub().getProxyInterface());
        }
        BinderInvocationStub binderInvocationStub = new BinderInvocationStub((IInterface) getInvocationStub().getBaseInterface());
        binderInvocationStub.copyMethodProxies(getInvocationStub());
        ServiceManager.sCache.get().put(ServiceManagerNative.ACTIVITY, binderInvocationStub);
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        if (VirtualCore.get().isVAppProcess()) {
            addMethodProxy(new StaticMethodProxy("navigateUpTo") {
                public Object call(Object obj, Method method, Object... objArr) {
                    throw new RuntimeException("Call navigateUpTo!!!!");
                }
            });
            addMethodProxy(new ReplaceLastUidMethodProxy("checkPermissionWithToken"));
            addMethodProxy(new isUserRunning());
            addMethodProxy(new ResultStaticMethodProxy("updateConfiguration", 0));
            addMethodProxy(new ReplaceCallingPkgMethodProxy("setAppLockedVerifying"));
            addMethodProxy(new StaticMethodProxy("checkUriPermission") {
                public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
                    return 0;
                }
            });
            addMethodProxy(new StaticMethodProxy("getRecentTasks") {
                public Object call(Object obj, Method method, Object... objArr) {
                    Object invoke = method.invoke(obj, objArr);
                    for (ActivityManager.RecentTaskInfo recentTaskInfo : ParceledListSliceCompat.isReturnParceledListSlice(method) ? ParceledListSlice.getList.call(invoke, new Object[0]) : (List) invoke) {
                        AppTaskInfo taskInfo = VActivityManager.get().getTaskInfo(recentTaskInfo.id);
                        if (taskInfo != null) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                try {
                                    recentTaskInfo.topActivity = taskInfo.topActivity;
                                    recentTaskInfo.baseActivity = taskInfo.baseActivity;
                                } catch (Throwable th) {
                                }
                            }
                            try {
                                recentTaskInfo.origActivity = taskInfo.baseActivity;
                                recentTaskInfo.baseIntent = taskInfo.baseIntent;
                            } catch (Throwable th2) {
                            }
                        }
                    }
                    return invoke;
                }
            });
        }
    }

    public boolean isEnvBad() {
        return ActivityManagerNative.getDefault.call(new Object[0]) != getInvocationStub().getProxyInterface();
    }

    private class isUserRunning extends MethodProxy {
        private isUserRunning() {
        }

        public String getMethodName() {
            return "isUserRunning";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return Boolean.valueOf(((Integer) objArr[0]).intValue() == 0);
        }
    }
}
