package com.lody.virtual.client;

import android.os.Binder;
import android.os.Build;
import android.os.Process;
import com.duapps.ad.AdError;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.client.natives.NativeMethods;
import com.lody.virtual.helper.compat.BuildCompat;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.InstalledAppInfo;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NativeEngine {
    private static final String TAG = NativeEngine.class.getSimpleName();
    private static Map<String, InstalledAppInfo> sDexOverrideMap;
    private static boolean sFlag = false;

    private static native String nativeGetRedirectedPath(String str);

    private static native void nativeHookNative(Object obj, String str, boolean z, int i, int i2);

    private static native void nativeMark();

    private static native void nativeReadOnly(String str);

    private static native void nativeRedirect(String str, String str2);

    private static native String nativeRestoreRedirectedPath(String str);

    private static native void nativeStartUniformer(String str, int i, int i2);

    static {
        try {
            System.loadLibrary("va-native");
        } catch (Exception e2) {
            VLog.e(TAG, VLog.getStackTraceString(e2), new Object[0]);
        }
        NativeMethods.init();
    }

    public static void startDexOverride() {
        List<InstalledAppInfo> installedApps = VirtualCore.get().getInstalledApps(0);
        sDexOverrideMap = new HashMap(installedApps.size());
        for (InstalledAppInfo next : installedApps) {
            try {
                sDexOverrideMap.put(new File(next.apkPath).getCanonicalPath(), next);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static String getRedirectedPath(String str) {
        try {
            return nativeGetRedirectedPath(str);
        } catch (Throwable th) {
            VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
            return str;
        }
    }

    public static String restoreRedirectedPath(String str) {
        try {
            return nativeRestoreRedirectedPath(str);
        } catch (Throwable th) {
            VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
            return str;
        }
    }

    public static void redirectDirectory(String str, String str2) {
        if (!str.endsWith("/")) {
            str = str + "/";
        }
        if (!str2.endsWith("/")) {
            str2 = str2 + "/";
        }
        try {
            nativeRedirect(str, str2);
        } catch (Throwable th) {
            VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
        }
    }

    public static void redirectFile(String str, String str2) {
        if (str.endsWith("/")) {
            str = str.substring(0, str.length() - 1);
        }
        if (str2.endsWith("/")) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        try {
            nativeRedirect(str, str2);
        } catch (Throwable th) {
            VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
        }
    }

    public static void readOnly(String str) {
        try {
            nativeReadOnly(str);
        } catch (Throwable th) {
            VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
        }
    }

    public static void hook() {
        try {
            String format = String.format("/data/data/%s/lib/libva-native.so", VirtualCore.get().getHostPkg());
            if (!new File(format).exists()) {
                throw new RuntimeException("Unable to find the so.");
            }
            nativeStartUniformer(format, Build.VERSION.SDK_INT, BuildCompat.getPreviewSDKInt());
        } catch (Throwable th) {
            VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
        }
    }

    static void hookNative() {
        if (!sFlag) {
            try {
                nativeHookNative(new Method[]{NativeMethods.gOpenDexFileNative, NativeMethods.gCameraNativeSetup, NativeMethods.gAudioRecordNativeCheckPermission}, VirtualCore.get().getHostPkg(), VirtualRuntime.isArt(), Build.VERSION.SDK_INT, NativeMethods.gCameraMethodType);
            } catch (Throwable th) {
                VLog.e(TAG, VLog.getStackTraceString(th), new Object[0]);
            }
            sFlag = true;
        }
    }

    public static void onKillProcess(int i, int i2) {
        VLog.e(TAG, "killProcess: pid = %d, signal = %d.", Integer.valueOf(i), Integer.valueOf(i2));
        if (i == Process.myPid()) {
            VLog.e(TAG, VLog.getStackTraceString(new Throwable()), new Object[0]);
        }
    }

    public static int onGetCallingUid(int i) {
        int callingPid = Binder.getCallingPid();
        if (callingPid == Process.myPid()) {
            return VClientImpl.get().getBaseVUid();
        }
        if (callingPid == VirtualCore.get().getSystemPid()) {
            return AdError.NETWORK_ERROR_CODE;
        }
        int uidByPid = VActivityManager.get().getUidByPid(callingPid);
        if (uidByPid != -1) {
            return VUserHandle.getAppId(uidByPid);
        }
        VLog.d(TAG, "Unknown uid: " + callingPid, new Object[0]);
        return VClientImpl.get().getBaseVUid();
    }

    public static void onOpenDexFileNative(String[] strArr) {
        String str = strArr[0];
        String str2 = strArr[1];
        VLog.d(TAG, "DexOrJarPath = %s, OutputPath = %s.", str, str2);
        try {
            InstalledAppInfo installedAppInfo = sDexOverrideMap.get(new File(str).getCanonicalPath());
            if (installedAppInfo != null && !installedAppInfo.dependSystem) {
                strArr[1] = installedAppInfo.getOdexFile().getPath();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static int onGetUid(int i) {
        return VClientImpl.get().getBaseVUid();
    }
}
