package com.lody.virtual.client.hook.providers;

import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.actions.SearchIntents;
import com.lody.virtual.client.hook.base.MethodBox;
import com.lody.virtual.helper.utils.VLog;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import mirror.android.content.IContentProvider;

public class ProviderHook implements InvocationHandler {
    private static final Map<String, HookFetcher> PROVIDER_MAP = new HashMap();
    protected final Object mBase;

    public interface HookFetcher {
        ProviderHook fetch(boolean z, IInterface iInterface);
    }

    static {
        PROVIDER_MAP.put("settings", new HookFetcher() {
            public ProviderHook fetch(boolean z, IInterface iInterface) {
                return new SettingsProviderHook(iInterface);
            }
        });
        PROVIDER_MAP.put("downloads", new HookFetcher() {
            public ProviderHook fetch(boolean z, IInterface iInterface) {
                return new DownloadProviderHook(iInterface);
            }
        });
    }

    public ProviderHook(Object obj) {
        this.mBase = obj;
    }

    private static HookFetcher fetchHook(String str) {
        HookFetcher hookFetcher = PROVIDER_MAP.get(str);
        if (hookFetcher == null) {
            return new HookFetcher() {
                public ProviderHook fetch(boolean z, IInterface iInterface) {
                    if (z) {
                        return new ExternalProviderHook(iInterface);
                    }
                    return new InternalProviderHook(iInterface);
                }
            };
        }
        return hookFetcher;
    }

    private static IInterface createProxy(IInterface iInterface, ProviderHook providerHook) {
        if (iInterface == null || providerHook == null) {
            return null;
        }
        return (IInterface) Proxy.newProxyInstance(iInterface.getClass().getClassLoader(), new Class[]{IContentProvider.TYPE}, providerHook);
    }

    public static IInterface createProxy(boolean z, String str, IInterface iInterface) {
        HookFetcher fetchHook;
        IInterface createProxy;
        if (((iInterface instanceof Proxy) && (Proxy.getInvocationHandler(iInterface) instanceof ProviderHook)) || (fetchHook = fetchHook(str)) == null || (createProxy = createProxy(iInterface, fetchHook.fetch(z, iInterface))) == null) {
            return iInterface;
        }
        return createProxy;
    }

    public Bundle call(MethodBox methodBox, String str, String str2, Bundle bundle) {
        return (Bundle) methodBox.call();
    }

    public Uri insert(MethodBox methodBox, Uri uri, ContentValues contentValues) {
        return (Uri) methodBox.call();
    }

    public Cursor query(MethodBox methodBox, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return (Cursor) methodBox.call();
    }

    public String getType(MethodBox methodBox, Uri uri) {
        return (String) methodBox.call();
    }

    public int bulkInsert(MethodBox methodBox, Uri uri, ContentValues[] contentValuesArr) {
        return ((Integer) methodBox.call()).intValue();
    }

    public int delete(MethodBox methodBox, Uri uri, String str, String[] strArr) {
        return ((Integer) methodBox.call()).intValue();
    }

    public int update(MethodBox methodBox, Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return ((Integer) methodBox.call()).intValue();
    }

    public ParcelFileDescriptor openFile(MethodBox methodBox, Uri uri, String str) {
        return (ParcelFileDescriptor) methodBox.call();
    }

    public AssetFileDescriptor openAssetFile(MethodBox methodBox, Uri uri, String str) {
        return (AssetFileDescriptor) methodBox.call();
    }

    public Object invoke(Object obj, Method method, Object... objArr) {
        int i;
        try {
            processArgs(method, objArr);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        MethodBox methodBox = new MethodBox(method, this.mBase, objArr);
        if (Build.VERSION.SDK_INT >= 18) {
            i = 1;
        } else {
            i = 0;
        }
        try {
            String name = method.getName();
            if ("call".equals(name)) {
                return call(methodBox, (String) objArr[i], (String) objArr[i + 1], (Bundle) objArr[i + 2]);
            }
            if ("insert".equals(name)) {
                return insert(methodBox, (Uri) objArr[i], (ContentValues) objArr[i + 1]);
            }
            if ("getType".equals(name)) {
                return getType(methodBox, (Uri) objArr[0]);
            }
            if ("delete".equals(name)) {
                return Integer.valueOf(delete(methodBox, (Uri) objArr[0], (String) objArr[i + 1], (String[]) objArr[i + 2]));
            }
            if ("bulkInsert".equals(name)) {
                return Integer.valueOf(bulkInsert(methodBox, (Uri) objArr[i], (ContentValues[]) objArr[i + 1]));
            }
            if ("update".equals(name)) {
                return Integer.valueOf(update(methodBox, (Uri) objArr[i], (ContentValues) objArr[i + 1], (String) objArr[i + 2], (String[]) objArr[i + 3]));
            }
            if ("openFile".equals(name)) {
                return openFile(methodBox, (Uri) objArr[i], (String) objArr[i + 1]);
            }
            if ("openAssetFile".equals(name)) {
                return openAssetFile(methodBox, (Uri) objArr[i], (String) objArr[i + 1]);
            }
            if (SearchIntents.EXTRA_QUERY.equals(name)) {
                return query(methodBox, (Uri) objArr[i], (String[]) objArr[i + 1], (String) objArr[i + 2], (String[]) objArr[i + 3], (String) objArr[i + 4]);
            }
            return methodBox.call();
        } catch (Throwable th2) {
            VLog.d("ProviderHook", "call: %s (%s) with error", method.getName(), Arrays.toString(objArr));
            if (th2 instanceof InvocationTargetException) {
                throw th2.getCause();
            }
            throw th2;
        }
    }

    /* access modifiers changed from: protected */
    public void processArgs(Method method, Object... objArr) {
    }
}
