package com.lody.virtual.client.hook.base;

import android.os.IBinder;
import android.os.IInterface;
import mirror.RefStaticMethod;
import mirror.android.os.ServiceManager;

public abstract class BinderInvocationProxy extends MethodInvocationProxy<BinderInvocationStub> {
    protected String mServiceName;

    public BinderInvocationProxy(IInterface iInterface, String str) {
        this(new BinderInvocationStub(iInterface), str);
    }

    public BinderInvocationProxy(RefStaticMethod<IInterface> refStaticMethod, String str) {
        this(new BinderInvocationStub(refStaticMethod, ServiceManager.getService.call(str)), str);
    }

    public BinderInvocationProxy(Class<?> cls, String str) {
        this(new BinderInvocationStub(cls, ServiceManager.getService.call(str)), str);
    }

    public BinderInvocationProxy(BinderInvocationStub binderInvocationStub, String str) {
        super(binderInvocationStub);
        this.mServiceName = str;
    }

    public void inject() {
        ((BinderInvocationStub) getInvocationStub()).replaceService(this.mServiceName);
    }

    public boolean isEnvBad() {
        IBinder call = ServiceManager.getService.call(this.mServiceName);
        return (call == null || getInvocationStub() == call) ? false : true;
    }
}
