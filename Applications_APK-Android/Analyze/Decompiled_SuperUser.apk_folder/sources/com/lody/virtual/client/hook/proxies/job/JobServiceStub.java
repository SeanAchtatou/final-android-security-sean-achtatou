package com.lody.virtual.client.hook.proxies.job;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.ipc.VJobScheduler;
import java.lang.reflect.Method;
import mirror.android.app.job.IJobScheduler;

@TargetApi(21)
public class JobServiceStub extends BinderInvocationProxy {
    public JobServiceStub() {
        super(IJobScheduler.Stub.asInterface, "jobscheduler");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new schedule());
        addMethodProxy(new getAllPendingJobs());
        addMethodProxy(new cancelAll());
        addMethodProxy(new cancel());
    }

    private class schedule extends MethodProxy {
        private schedule() {
        }

        public String getMethodName() {
            return "schedule";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return Integer.valueOf(VJobScheduler.get().schedule((JobInfo) objArr[0]));
        }
    }

    private class getAllPendingJobs extends MethodProxy {
        private getAllPendingJobs() {
        }

        public String getMethodName() {
            return "getAllPendingJobs";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return VJobScheduler.get().getAllPendingJobs();
        }
    }

    private class cancelAll extends MethodProxy {
        private cancelAll() {
        }

        public String getMethodName() {
            return "cancelAll";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            VJobScheduler.get().cancelAll();
            return 0;
        }
    }

    private class cancel extends MethodProxy {
        private cancel() {
        }

        public String getMethodName() {
            return "cancel";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            VJobScheduler.get().cancel(((Integer) objArr[0]).intValue());
            return 0;
        }
    }
}
