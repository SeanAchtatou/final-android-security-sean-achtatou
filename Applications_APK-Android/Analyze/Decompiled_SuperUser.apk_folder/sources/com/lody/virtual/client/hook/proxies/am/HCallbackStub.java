package com.lody.virtual.client.hook.proxies.am;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.interfaces.IInjector;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.utils.ComponentUtils;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.remote.StubActivityRecord;
import mirror.android.app.ActivityManagerNative;
import mirror.android.app.ActivityThread;
import mirror.android.app.IActivityManager;

public class HCallbackStub implements Handler.Callback, IInjector {
    private static final int CREATE_SERVICE = ActivityThread.H.CREATE_SERVICE.get();
    private static final int LAUNCH_ACTIVITY = ActivityThread.H.LAUNCH_ACTIVITY.get();
    private static final int SCHEDULE_CRASH = (ActivityThread.H.SCHEDULE_CRASH != null ? ActivityThread.H.SCHEDULE_CRASH.get() : -1);
    private static final String TAG = HCallbackStub.class.getSimpleName();
    private static final HCallbackStub sCallback = new HCallbackStub();
    private boolean mCalling = false;
    private Handler.Callback otherCallback;

    private HCallbackStub() {
    }

    public static HCallbackStub getDefault() {
        return sCallback;
    }

    private static Handler getH() {
        return ActivityThread.mH.get(VirtualCore.mainThread());
    }

    private static Handler.Callback getHCallback() {
        try {
            return mirror.android.os.Handler.mCallback.get(getH());
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public boolean handleMessage(Message message) {
        if (!this.mCalling) {
            this.mCalling = true;
            try {
                if (LAUNCH_ACTIVITY == message.what) {
                    if (!handleLaunchActivity(message)) {
                        return true;
                    }
                } else if (CREATE_SERVICE == message.what) {
                    if (!VClientImpl.get().isBound()) {
                        ServiceInfo serviceInfo = (ServiceInfo) Reflect.on(message.obj).get("info");
                        VClientImpl.get().bindApplication(serviceInfo.packageName, serviceInfo.processName);
                    }
                } else if (SCHEDULE_CRASH == message.what) {
                    this.mCalling = false;
                    return true;
                }
                if (this.otherCallback != null) {
                    boolean handleMessage = this.otherCallback.handleMessage(message);
                    this.mCalling = false;
                    this.mCalling = false;
                    return handleMessage;
                }
                this.mCalling = false;
                this.mCalling = false;
            } finally {
                this.mCalling = false;
            }
        }
        return false;
    }

    private boolean handleLaunchActivity(Message message) {
        Object obj = message.obj;
        StubActivityRecord stubActivityRecord = new StubActivityRecord(ActivityThread.ActivityClientRecord.intent.get(obj));
        if (stubActivityRecord.intent == null) {
            return true;
        }
        Intent intent = stubActivityRecord.intent;
        ComponentName componentName = stubActivityRecord.caller;
        IBinder iBinder = ActivityThread.ActivityClientRecord.token.get(obj);
        ActivityInfo activityInfo = stubActivityRecord.info;
        if (VClientImpl.get().getToken() == null) {
            VActivityManager.get().processRestarted(activityInfo.packageName, activityInfo.processName, stubActivityRecord.userId);
            getH().sendMessageAtFrontOfQueue(Message.obtain(message));
            return false;
        } else if (!VClientImpl.get().isBound()) {
            VClientImpl.get().bindApplication(activityInfo.packageName, activityInfo.processName);
            getH().sendMessageAtFrontOfQueue(Message.obtain(message));
            return false;
        } else {
            VActivityManager.get().onActivityCreate(ComponentUtils.toComponentName(activityInfo), componentName, iBinder, activityInfo, intent, ComponentUtils.getTaskAffinity(activityInfo), IActivityManager.getTaskForActivity.call(ActivityManagerNative.getDefault.call(new Object[0]), iBinder, false).intValue(), activityInfo.launchMode, activityInfo.flags);
            intent.setExtrasClassLoader(VClientImpl.get().getClassLoader(activityInfo.applicationInfo));
            ActivityThread.ActivityClientRecord.intent.set(obj, intent);
            ActivityThread.ActivityClientRecord.activityInfo.set(obj, activityInfo);
            return true;
        }
    }

    public void inject() {
        this.otherCallback = getHCallback();
        mirror.android.os.Handler.mCallback.set(getH(), this);
    }

    public boolean isEnvBad() {
        boolean z;
        Handler.Callback hCallback = getHCallback();
        if (hCallback != this) {
            z = true;
        } else {
            z = false;
        }
        if (hCallback != null && z) {
            VLog.d(TAG, "HCallback has bad, other callback = " + hCallback, new Object[0]);
        }
        return z;
    }
}
