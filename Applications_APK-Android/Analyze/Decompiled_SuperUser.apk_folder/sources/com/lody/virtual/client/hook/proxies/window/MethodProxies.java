package com.lody.virtual.client.hook.proxies.window;

import android.os.IInterface;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.proxies.window.session.WindowSessionPatch;
import java.lang.reflect.Method;

class MethodProxies {
    MethodProxies() {
    }

    static class OpenSession extends BasePatchSession {
        OpenSession() {
        }

        public String getMethodName() {
            return "openSession";
        }
    }

    static class OverridePendingAppTransition extends BasePatchSession {
        OverridePendingAppTransition() {
        }

        public String getMethodName() {
            return "overridePendingAppTransition";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr[0] instanceof String) {
                objArr[0] = getHostPkg();
            }
            return super.call(obj, method, objArr);
        }
    }

    static class OverridePendingAppTransitionInPlace extends MethodProxy {
        OverridePendingAppTransitionInPlace() {
        }

        public String getMethodName() {
            return "overridePendingAppTransitionInPlace";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr[0] instanceof String) {
                objArr[0] = getHostPkg();
            }
            return method.invoke(obj, objArr);
        }
    }

    static class SetAppStartingWindow extends BasePatchSession {
        SetAppStartingWindow() {
        }

        public String getMethodName() {
            return "setAppStartingWindow";
        }
    }

    static abstract class BasePatchSession extends MethodProxy {
        BasePatchSession() {
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Object invoke = method.invoke(obj, objArr);
            if (invoke instanceof IInterface) {
                return proxySession((IInterface) invoke);
            }
            return invoke;
        }

        private Object proxySession(IInterface iInterface) {
            return new WindowSessionPatch(iInterface).getInvocationStub().getProxyInterface();
        }
    }
}
