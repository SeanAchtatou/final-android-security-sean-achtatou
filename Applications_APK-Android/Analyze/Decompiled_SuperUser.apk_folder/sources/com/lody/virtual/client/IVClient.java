package com.lody.virtual.client;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ProviderInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.PendingResultData;

public interface IVClient extends IInterface {
    IBinder acquireProviderClient(ProviderInfo providerInfo);

    IBinder createProxyService(ComponentName componentName, IBinder iBinder);

    void finishActivity(IBinder iBinder);

    IBinder getAppThread();

    String getDebugInfo();

    IBinder getToken();

    void scheduleNewIntent(String str, IBinder iBinder, Intent intent);

    void scheduleReceiver(String str, ComponentName componentName, Intent intent, PendingResultData pendingResultData);

    public static abstract class Stub extends Binder implements IVClient {
        private static final String DESCRIPTOR = "com.lody.virtual.client.IVClient";
        static final int TRANSACTION_acquireProviderClient = 5;
        static final int TRANSACTION_createProxyService = 4;
        static final int TRANSACTION_finishActivity = 3;
        static final int TRANSACTION_getAppThread = 6;
        static final int TRANSACTION_getDebugInfo = 8;
        static final int TRANSACTION_getToken = 7;
        static final int TRANSACTION_scheduleNewIntent = 2;
        static final int TRANSACTION_scheduleReceiver = 1;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IVClient asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IVClient)) {
                return new Proxy(iBinder);
            }
            return (IVClient) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            ProviderInfo providerInfo;
            ComponentName componentName;
            Intent intent;
            ComponentName componentName2;
            Intent intent2;
            PendingResultData pendingResultData;
            switch (i) {
                case 1:
                    parcel.enforceInterface(DESCRIPTOR);
                    String readString = parcel.readString();
                    if (parcel.readInt() != 0) {
                        componentName2 = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName2 = null;
                    }
                    if (parcel.readInt() != 0) {
                        intent2 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent2 = null;
                    }
                    if (parcel.readInt() != 0) {
                        pendingResultData = PendingResultData.CREATOR.createFromParcel(parcel);
                    } else {
                        pendingResultData = null;
                    }
                    scheduleReceiver(readString, componentName2, intent2, pendingResultData);
                    parcel2.writeNoException();
                    return true;
                case 2:
                    parcel.enforceInterface(DESCRIPTOR);
                    String readString2 = parcel.readString();
                    IBinder readStrongBinder = parcel.readStrongBinder();
                    if (parcel.readInt() != 0) {
                        intent = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent = null;
                    }
                    scheduleNewIntent(readString2, readStrongBinder, intent);
                    parcel2.writeNoException();
                    return true;
                case 3:
                    parcel.enforceInterface(DESCRIPTOR);
                    finishActivity(parcel.readStrongBinder());
                    parcel2.writeNoException();
                    return true;
                case 4:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        componentName = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName = null;
                    }
                    IBinder createProxyService = createProxyService(componentName, parcel.readStrongBinder());
                    parcel2.writeNoException();
                    parcel2.writeStrongBinder(createProxyService);
                    return true;
                case 5:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        providerInfo = (ProviderInfo) ProviderInfo.CREATOR.createFromParcel(parcel);
                    } else {
                        providerInfo = null;
                    }
                    IBinder acquireProviderClient = acquireProviderClient(providerInfo);
                    parcel2.writeNoException();
                    parcel2.writeStrongBinder(acquireProviderClient);
                    return true;
                case 6:
                    parcel.enforceInterface(DESCRIPTOR);
                    IBinder appThread = getAppThread();
                    parcel2.writeNoException();
                    parcel2.writeStrongBinder(appThread);
                    return true;
                case 7:
                    parcel.enforceInterface(DESCRIPTOR);
                    IBinder token = getToken();
                    parcel2.writeNoException();
                    parcel2.writeStrongBinder(token);
                    return true;
                case 8:
                    parcel.enforceInterface(DESCRIPTOR);
                    String debugInfo = getDebugInfo();
                    parcel2.writeNoException();
                    parcel2.writeString(debugInfo);
                    return true;
                case 1598968902:
                    parcel2.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        private static class Proxy implements IVClient {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void scheduleReceiver(String str, ComponentName componentName, Intent intent, PendingResultData pendingResultData) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (pendingResultData != null) {
                        obtain.writeInt(1);
                        pendingResultData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void scheduleNewIntent(String str, IBinder iBinder, Intent intent) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeStrongBinder(iBinder);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void finishActivity(IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder createProxyService(ComponentName componentName, IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder acquireProviderClient(ProviderInfo providerInfo) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (providerInfo != null) {
                        obtain.writeInt(1);
                        providerInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder getAppThread() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder getToken() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getDebugInfo() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
