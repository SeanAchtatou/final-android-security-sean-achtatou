package com.lody.virtual.client.hook.base;

import android.text.TextUtils;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.hook.base.LogInvocation;
import com.lody.virtual.client.hook.utils.MethodParameterUtils;
import com.lody.virtual.helper.utils.VLog;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class MethodInvocationStub<T> {
    /* access modifiers changed from: private */
    public static final String TAG = MethodInvocationStub.class.getSimpleName();
    /* access modifiers changed from: private */
    public T mBaseInterface;
    private String mIdentityName;
    private Map<String, MethodProxy> mInternalMethodProxies;
    /* access modifiers changed from: private */
    public LogInvocation.Condition mInvocationLoggingCondition;
    private T mProxyInterface;

    public Map<String, MethodProxy> getAllHooks() {
        return this.mInternalMethodProxies;
    }

    public MethodInvocationStub(T t, Class<?>... clsArr) {
        this.mInternalMethodProxies = new HashMap();
        this.mInvocationLoggingCondition = LogInvocation.Condition.NEVER;
        this.mBaseInterface = t;
        if (t != null) {
            this.mProxyInterface = Proxy.newProxyInstance(t.getClass().getClassLoader(), clsArr == null ? MethodParameterUtils.getAllInterface(t.getClass()) : clsArr, new HookInvocationHandler());
        } else {
            VLog.d(TAG, "Unable to build HookDelegate: %s.", getIdentityName());
        }
    }

    public LogInvocation.Condition getInvocationLoggingCondition() {
        return this.mInvocationLoggingCondition;
    }

    public void setInvocationLoggingCondition(LogInvocation.Condition condition) {
        this.mInvocationLoggingCondition = condition;
    }

    public void setIdentityName(String str) {
        this.mIdentityName = str;
    }

    public String getIdentityName() {
        if (this.mIdentityName != null) {
            return this.mIdentityName;
        }
        return getClass().getSimpleName();
    }

    public MethodInvocationStub(T t) {
        this(t, null);
    }

    public void copyMethodProxies(MethodInvocationStub methodInvocationStub) {
        this.mInternalMethodProxies.putAll(methodInvocationStub.getAllHooks());
    }

    public MethodProxy addMethodProxy(MethodProxy methodProxy) {
        if (methodProxy != null && !TextUtils.isEmpty(methodProxy.getMethodName())) {
            if (this.mInternalMethodProxies.containsKey(methodProxy.getMethodName())) {
                VLog.w(TAG, "The Hook(%s, %s) you added has been in existence.", methodProxy.getMethodName(), methodProxy.getClass().getName());
            } else {
                this.mInternalMethodProxies.put(methodProxy.getMethodName(), methodProxy);
            }
        }
        return methodProxy;
    }

    public MethodProxy removeMethodProxy(String str) {
        return this.mInternalMethodProxies.remove(str);
    }

    public void removeMethodProxy(MethodProxy methodProxy) {
        if (methodProxy != null) {
            removeMethodProxy(methodProxy.getMethodName());
        }
    }

    public void removeAllMethodProxies() {
        this.mInternalMethodProxies.clear();
    }

    public <H extends MethodProxy> H getMethodProxy(String str) {
        return (MethodProxy) this.mInternalMethodProxies.get(str);
    }

    public T getProxyInterface() {
        return this.mProxyInterface;
    }

    public T getBaseInterface() {
        return this.mBaseInterface;
    }

    public int getMethodProxiesCount() {
        return this.mInternalMethodProxies.size();
    }

    private class HookInvocationHandler implements InvocationHandler {
        private HookInvocationHandler() {
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v16, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: java.lang.String} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x010c  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object invoke(java.lang.Object r12, java.lang.reflect.Method r13, java.lang.Object[] r14) {
            /*
                r11 = this;
                r6 = 0
                r5 = 0
                r4 = 1
                com.lody.virtual.client.hook.base.MethodInvocationStub r1 = com.lody.virtual.client.hook.base.MethodInvocationStub.this
                java.lang.String r2 = r13.getName()
                com.lody.virtual.client.hook.base.MethodProxy r10 = r1.getMethodProxy(r2)
                if (r10 == 0) goto L_0x00c5
                boolean r1 = r10.isEnable()
                if (r1 == 0) goto L_0x00c5
                r3 = r4
            L_0x0016:
                com.lody.virtual.client.hook.base.MethodInvocationStub r1 = com.lody.virtual.client.hook.base.MethodInvocationStub.this
                com.lody.virtual.client.hook.base.LogInvocation$Condition r1 = r1.mInvocationLoggingCondition
                com.lody.virtual.client.hook.base.LogInvocation$Condition r2 = com.lody.virtual.client.hook.base.LogInvocation.Condition.NEVER
                if (r1 != r2) goto L_0x002a
                if (r10 == 0) goto L_0x00c8
                com.lody.virtual.client.hook.base.LogInvocation$Condition r1 = r10.getInvocationLoggingCondition()
                com.lody.virtual.client.hook.base.LogInvocation$Condition r2 = com.lody.virtual.client.hook.base.LogInvocation.Condition.NEVER
                if (r1 == r2) goto L_0x00c8
            L_0x002a:
                r9 = r4
            L_0x002b:
                if (r9 == 0) goto L_0x019b
                java.lang.String r1 = java.util.Arrays.toString(r14)
                int r2 = r1.length()
                int r2 = r2 + -1
                java.lang.String r1 = r1.substring(r4, r2)
                r8 = r1
            L_0x003c:
                if (r3 == 0) goto L_0x00cb
                com.lody.virtual.client.hook.base.MethodInvocationStub r1 = com.lody.virtual.client.hook.base.MethodInvocationStub.this     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                java.lang.Object r1 = r1.mBaseInterface     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                boolean r1 = r10.beforeCall(r1, r13, r14)     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                if (r1 == 0) goto L_0x00cb
                com.lody.virtual.client.hook.base.MethodInvocationStub r1 = com.lody.virtual.client.hook.base.MethodInvocationStub.this     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                java.lang.Object r1 = r1.mBaseInterface     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                java.lang.Object r1 = r10.call(r1, r13, r14)     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                com.lody.virtual.client.hook.base.MethodInvocationStub r2 = com.lody.virtual.client.hook.base.MethodInvocationStub.this     // Catch:{ Throwable -> 0x0192, all -> 0x018e }
                java.lang.Object r2 = r2.mBaseInterface     // Catch:{ Throwable -> 0x0192, all -> 0x018e }
                java.lang.Object r1 = r10.afterCall(r2, r13, r14, r1)     // Catch:{ Throwable -> 0x0192, all -> 0x018e }
            L_0x005e:
                if (r9 == 0) goto L_0x00c4
                com.lody.virtual.client.hook.base.MethodInvocationStub r2 = com.lody.virtual.client.hook.base.MethodInvocationStub.this
                com.lody.virtual.client.hook.base.LogInvocation$Condition r7 = r2.mInvocationLoggingCondition
                if (r6 == 0) goto L_0x00d6
                r2 = r4
            L_0x0069:
                int r2 = r7.getLogLevel(r3, r2)
                if (r10 == 0) goto L_0x0198
                com.lody.virtual.client.hook.base.LogInvocation$Condition r7 = r10.getInvocationLoggingCondition()
                if (r6 == 0) goto L_0x00d8
            L_0x0075:
                int r3 = r7.getLogLevel(r3, r4)
                int r2 = java.lang.Math.max(r2, r3)
                r3 = r2
            L_0x007e:
                if (r3 < 0) goto L_0x00c4
                if (r6 == 0) goto L_0x00da
                java.lang.String r2 = r6.toString()
            L_0x0086:
                java.lang.String r4 = com.lody.virtual.client.hook.base.MethodInvocationStub.TAG
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.Class r6 = r13.getDeclaringClass()
                java.lang.String r6 = r6.getSimpleName()
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "."
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = r13.getName()
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "("
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r5 = r5.append(r8)
                java.lang.String r6 = ") => "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r2 = r5.append(r2)
                java.lang.String r2 = r2.toString()
                android.util.Log.println(r3, r4, r2)
            L_0x00c4:
                return r1
            L_0x00c5:
                r3 = r5
                goto L_0x0016
            L_0x00c8:
                r9 = r5
                goto L_0x002b
            L_0x00cb:
                com.lody.virtual.client.hook.base.MethodInvocationStub r1 = com.lody.virtual.client.hook.base.MethodInvocationStub.this     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                java.lang.Object r1 = r1.mBaseInterface     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                java.lang.Object r1 = r13.invoke(r1, r14)     // Catch:{ Throwable -> 0x00ee, all -> 0x0189 }
                goto L_0x005e
            L_0x00d6:
                r2 = r5
                goto L_0x0069
            L_0x00d8:
                r4 = r5
                goto L_0x0075
            L_0x00da:
                java.lang.Class r2 = r13.getReturnType()
                java.lang.Class r4 = java.lang.Void.TYPE
                boolean r2 = r2.equals(r4)
                if (r2 == 0) goto L_0x00e9
                java.lang.String r2 = "void"
                goto L_0x0086
            L_0x00e9:
                java.lang.String r2 = java.lang.String.valueOf(r1)
                goto L_0x0086
            L_0x00ee:
                r2 = move-exception
            L_0x00ef:
                boolean r1 = r2 instanceof java.lang.reflect.InvocationTargetException     // Catch:{ all -> 0x0106 }
                if (r1 == 0) goto L_0x0105
                r0 = r2
                java.lang.reflect.InvocationTargetException r0 = (java.lang.reflect.InvocationTargetException) r0     // Catch:{ all -> 0x0106 }
                r1 = r0
                java.lang.Throwable r1 = r1.getTargetException()     // Catch:{ all -> 0x0106 }
                if (r1 == 0) goto L_0x0105
                r0 = r2
                java.lang.reflect.InvocationTargetException r0 = (java.lang.reflect.InvocationTargetException) r0     // Catch:{ all -> 0x0106 }
                r1 = r0
                java.lang.Throwable r2 = r1.getTargetException()     // Catch:{ all -> 0x0106 }
            L_0x0105:
                throw r2     // Catch:{ all -> 0x0106 }
            L_0x0106:
                r1 = move-exception
                r7 = r6
                r6 = r2
                r2 = r1
            L_0x010a:
                if (r9 == 0) goto L_0x0170
                com.lody.virtual.client.hook.base.MethodInvocationStub r1 = com.lody.virtual.client.hook.base.MethodInvocationStub.this
                com.lody.virtual.client.hook.base.LogInvocation$Condition r9 = r1.mInvocationLoggingCondition
                if (r6 == 0) goto L_0x0171
                r1 = r4
            L_0x0115:
                int r1 = r9.getLogLevel(r3, r1)
                if (r10 == 0) goto L_0x0196
                com.lody.virtual.client.hook.base.LogInvocation$Condition r9 = r10.getInvocationLoggingCondition()
                if (r6 == 0) goto L_0x0173
            L_0x0121:
                int r3 = r9.getLogLevel(r3, r4)
                int r1 = java.lang.Math.max(r1, r3)
                r3 = r1
            L_0x012a:
                if (r3 < 0) goto L_0x0170
                if (r6 == 0) goto L_0x0175
                java.lang.String r1 = r6.toString()
            L_0x0132:
                java.lang.String r4 = com.lody.virtual.client.hook.base.MethodInvocationStub.TAG
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.Class r6 = r13.getDeclaringClass()
                java.lang.String r6 = r6.getSimpleName()
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "."
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = r13.getName()
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "("
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r5 = r5.append(r8)
                java.lang.String r6 = ") => "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r1 = r5.append(r1)
                java.lang.String r1 = r1.toString()
                android.util.Log.println(r3, r4, r1)
            L_0x0170:
                throw r2
            L_0x0171:
                r1 = r5
                goto L_0x0115
            L_0x0173:
                r4 = r5
                goto L_0x0121
            L_0x0175:
                java.lang.Class r1 = r13.getReturnType()
                java.lang.Class r4 = java.lang.Void.TYPE
                boolean r1 = r1.equals(r4)
                if (r1 == 0) goto L_0x0184
                java.lang.String r1 = "void"
                goto L_0x0132
            L_0x0184:
                java.lang.String r1 = java.lang.String.valueOf(r7)
                goto L_0x0132
            L_0x0189:
                r1 = move-exception
                r2 = r1
                r7 = r6
                goto L_0x010a
            L_0x018e:
                r2 = move-exception
                r7 = r1
                goto L_0x010a
            L_0x0192:
                r2 = move-exception
                r6 = r1
                goto L_0x00ef
            L_0x0196:
                r3 = r1
                goto L_0x012a
            L_0x0198:
                r3 = r2
                goto L_0x007e
            L_0x019b:
                r8 = r6
                goto L_0x003c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.client.hook.base.MethodInvocationStub.HookInvocationHandler.invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
        }
    }

    private void dumpMethodProxies() {
        StringBuilder sb = new StringBuilder(50);
        sb.append("*********************");
        for (MethodProxy methodName : this.mInternalMethodProxies.values()) {
            sb.append(methodName.getMethodName()).append(ShellUtils.COMMAND_LINE_END);
        }
        sb.append("*********************");
        VLog.e(TAG, sb.toString(), new Object[0]);
    }
}
