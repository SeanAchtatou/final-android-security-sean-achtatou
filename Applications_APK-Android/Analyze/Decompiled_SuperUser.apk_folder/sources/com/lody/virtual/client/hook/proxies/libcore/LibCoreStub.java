package com.lody.virtual.client.hook.proxies.libcore;

import com.lody.virtual.client.hook.base.Inject;
import com.lody.virtual.client.hook.base.MethodInvocationProxy;
import com.lody.virtual.client.hook.base.MethodInvocationStub;
import com.lody.virtual.client.hook.base.ReplaceUidMethodProxy;
import mirror.libcore.io.ForwardingOs;
import mirror.libcore.io.Libcore;

@Inject(MethodProxies.class)
public class LibCoreStub extends MethodInvocationProxy<MethodInvocationStub<Object>> {
    public LibCoreStub() {
        super(new MethodInvocationStub(getOs()));
    }

    private static Object getOs() {
        Object obj;
        Object obj2 = Libcore.os.get();
        return (ForwardingOs.os == null || (obj = ForwardingOs.os.get(obj2)) == null) ? obj2 : obj;
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new ReplaceUidMethodProxy("chown", 1));
        addMethodProxy(new ReplaceUidMethodProxy("fchown", 1));
        addMethodProxy(new ReplaceUidMethodProxy("getpwuid", 0));
        addMethodProxy(new ReplaceUidMethodProxy("lchown", 1));
        addMethodProxy(new ReplaceUidMethodProxy("setuid", 0));
    }

    public void inject() {
        Libcore.os.set(getInvocationStub().getProxyInterface());
    }

    public boolean isEnvBad() {
        return getOs() != getInvocationStub().getProxyInterface();
    }
}
