package com.lody.virtual.client.core;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import com.lody.virtual.R;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.env.Constants;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.client.fixer.ContextFixer;
import com.lody.virtual.client.hook.delegate.ComponentDelegate;
import com.lody.virtual.client.hook.delegate.PhoneInfoDelegate;
import com.lody.virtual.client.hook.delegate.TaskDescriptionDelegate;
import com.lody.virtual.client.ipc.LocalProxyUtils;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.client.ipc.VPackageManager;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.compat.BundleCompat;
import com.lody.virtual.helper.utils.BitmapUtils;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.InstallResult;
import com.lody.virtual.remote.InstalledAppInfo;
import com.lody.virtual.server.IAppManager;
import com.lody.virtual.server.interfaces.IAppRequestListener;
import com.lody.virtual.server.interfaces.IPackageObserver;
import com.lody.virtual.server.interfaces.IUiCallback;
import dalvik.system.DexFile;
import java.util.List;
import mirror.android.app.ActivityThread;

public final class VirtualCore {
    public static final int GET_HIDDEN_APP = 1;
    @SuppressLint({"StaticFieldLeak"})
    private static VirtualCore gCore = new VirtualCore();
    private ComponentDelegate componentDelegate;
    private Context context;
    private PackageInfo hostPkgInfo;
    private String hostPkgName;
    private ConditionVariable initLock = new ConditionVariable();
    private boolean isStartUp;
    private IAppManager mService;
    private String mainProcessName;
    private Object mainThread;
    private final int myUid = Process.myUid();
    private PhoneInfoDelegate phoneInfoDelegate;
    private String processName;
    private ProcessType processType;
    private int systemPid;
    private TaskDescriptionDelegate taskDescriptionDelegate;
    private PackageManager unHookPackageManager;

    public interface AppRequestListener {
        void onRequestInstall(String str);

        void onRequestUninstall(String str);
    }

    public interface OnEmitShortcutListener {
        Bitmap getIcon(Bitmap bitmap);

        String getName(String str);
    }

    public static abstract class PackageObserver extends IPackageObserver.Stub {
    }

    private enum ProcessType {
        Server,
        VAppClient,
        Main,
        CHILD
    }

    public static abstract class UiCallback extends IUiCallback.Stub {
    }

    private VirtualCore() {
    }

    public static VirtualCore get() {
        return gCore;
    }

    public static PackageManager getPM() {
        return get().getPackageManager();
    }

    public static Object mainThread() {
        return get().mainThread;
    }

    public ConditionVariable getInitLock() {
        return this.initLock;
    }

    public int myUid() {
        return this.myUid;
    }

    public int myUserId() {
        return VUserHandle.getUserId(this.myUid);
    }

    public ComponentDelegate getComponentDelegate() {
        return this.componentDelegate == null ? ComponentDelegate.EMPTY : this.componentDelegate;
    }

    public void setComponentDelegate(ComponentDelegate componentDelegate2) {
        this.componentDelegate = componentDelegate2;
    }

    public PhoneInfoDelegate getPhoneInfoDelegate() {
        return this.phoneInfoDelegate;
    }

    public void setPhoneInfoDelegate(PhoneInfoDelegate phoneInfoDelegate2) {
        this.phoneInfoDelegate = phoneInfoDelegate2;
    }

    public void setCrashHandler(CrashHandler crashHandler) {
        VClientImpl.get().setCrashHandler(crashHandler);
    }

    public TaskDescriptionDelegate getTaskDescriptionDelegate() {
        return this.taskDescriptionDelegate;
    }

    public void setTaskDescriptionDelegate(TaskDescriptionDelegate taskDescriptionDelegate2) {
        this.taskDescriptionDelegate = taskDescriptionDelegate2;
    }

    public int[] getGids() {
        return this.hostPkgInfo.gids;
    }

    public Context getContext() {
        return this.context;
    }

    public PackageManager getPackageManager() {
        return this.context.getPackageManager();
    }

    public String getHostPkg() {
        return this.hostPkgName;
    }

    public PackageManager getUnHookPackageManager() {
        return this.unHookPackageManager;
    }

    public void startup(Context context2) {
        if (this.isStartUp) {
            return;
        }
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("VirtualCore.startup() must called in main thread.");
        }
        VASettings.STUB_CP_AUTHORITY = context2.getPackageName() + "." + VASettings.STUB_DEF_AUTHORITY;
        ServiceManagerNative.SERVICE_CP_AUTH = context2.getPackageName() + "." + ServiceManagerNative.SERVICE_DEF_AUTH;
        this.context = context2;
        this.mainThread = ActivityThread.currentActivityThread.call(new Object[0]);
        this.unHookPackageManager = context2.getPackageManager();
        this.hostPkgInfo = this.unHookPackageManager.getPackageInfo(context2.getPackageName(), 8);
        detectProcessType();
        InvocationStubManager instance = InvocationStubManager.getInstance();
        instance.init();
        instance.injectAll();
        ContextFixer.fixContext(context2);
        this.isStartUp = true;
        if (this.initLock != null) {
            this.initLock.open();
            this.initLock = null;
        }
    }

    public void waitForEngine() {
        ServiceManagerNative.ensureServerStarted();
    }

    public boolean isEngineLaunched() {
        String engineProcessName = getEngineProcessName();
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) this.context.getSystemService(ServiceManagerNative.ACTIVITY)).getRunningAppProcesses()) {
            if (runningAppProcessInfo.processName.endsWith(engineProcessName)) {
                return true;
            }
        }
        return false;
    }

    public String getEngineProcessName() {
        return this.context.getString(R.string.gc);
    }

    public void initialize(VirtualInitializer virtualInitializer) {
        if (virtualInitializer == null) {
            throw new IllegalStateException("Initializer = NULL");
        }
        switch (this.processType) {
            case Main:
                virtualInitializer.onMainProcess();
                return;
            case VAppClient:
                virtualInitializer.onVirtualProcess();
                return;
            case Server:
                virtualInitializer.onServerProcess();
                return;
            case CHILD:
                virtualInitializer.onChildProcess();
                return;
            default:
                return;
        }
    }

    private void detectProcessType() {
        this.hostPkgName = this.context.getApplicationInfo().packageName;
        this.mainProcessName = this.context.getApplicationInfo().processName;
        this.processName = ActivityThread.getProcessName.call(this.mainThread, new Object[0]);
        if (this.processName.equals(this.mainProcessName)) {
            this.processType = ProcessType.Main;
        } else if (this.processName.endsWith(Constants.SERVER_PROCESS_NAME)) {
            this.processType = ProcessType.Server;
        } else if (VActivityManager.get().isAppProcess(this.processName)) {
            this.processType = ProcessType.VAppClient;
        } else {
            this.processType = ProcessType.CHILD;
        }
        if (isVAppProcess()) {
            this.systemPid = VActivityManager.get().getSystemPid();
        }
    }

    private IAppManager getService() {
        if (this.mService == null || ((!get().isVAppProcess() && !this.mService.asBinder().isBinderAlive()) || (!get().isVAppProcess() && !this.mService.asBinder().pingBinder()))) {
            synchronized (this) {
                this.mService = (IAppManager) LocalProxyUtils.genProxy(IAppManager.class, getStubInterface());
            }
        }
        return this.mService;
    }

    private Object getStubInterface() {
        return IAppManager.Stub.asInterface(ServiceManagerNative.getService(ServiceManagerNative.APP));
    }

    public boolean isVAppProcess() {
        return ProcessType.VAppClient == this.processType;
    }

    public boolean isMainProcess() {
        return ProcessType.Main == this.processType;
    }

    public boolean isChildProcess() {
        return ProcessType.CHILD == this.processType;
    }

    public boolean isServerProcess() {
        return ProcessType.Server == this.processType;
    }

    public String getProcessName() {
        return this.processName;
    }

    public String getMainProcessName() {
        return this.mainProcessName;
    }

    public void preOpt(String str) {
        InstalledAppInfo installedAppInfo = getInstalledAppInfo(str, 0);
        if (installedAppInfo != null && !installedAppInfo.dependSystem && !installedAppInfo.skipDexOpt) {
            DexFile.loadDex(installedAppInfo.apkPath, installedAppInfo.getOdexFile().getPath(), 0).close();
        }
    }

    public boolean isAppRunning(String str, int i) {
        return VActivityManager.get().isAppRunning(str, i);
    }

    public InstallResult installPackage(String str, int i) {
        try {
            return getService().installPackage(str, i);
        } catch (RemoteException e2) {
            return (InstallResult) VirtualRuntime.crash(e2);
        }
    }

    public void addVisibleOutsidePackage(String str) {
        try {
            getService().addVisibleOutsidePackage(str);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void removeVisibleOutsidePackage(String str) {
        try {
            getService().removeVisibleOutsidePackage(str);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public boolean isOutsidePackageVisible(String str) {
        try {
            return getService().isOutsidePackageVisible(str);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public boolean isAppInstalled(String str) {
        try {
            return getService().isAppInstalled(str);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public boolean isPackageLaunchable(String str) {
        InstalledAppInfo installedAppInfo = getInstalledAppInfo(str, 0);
        if (installedAppInfo == null || getLaunchIntent(str, installedAppInfo.getInstalledUsers()[0]) == null) {
            return false;
        }
        return true;
    }

    public Intent getLaunchIntent(String str, int i) {
        List<ResolveInfo> list;
        VPackageManager vPackageManager = VPackageManager.get();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.INFO");
        intent.setPackage(str);
        List<ResolveInfo> queryIntentActivities = vPackageManager.queryIntentActivities(intent, intent.resolveType(this.context), 0, i);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 0) {
            intent.removeCategory("android.intent.category.INFO");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setPackage(str);
            list = vPackageManager.queryIntentActivities(intent, intent.resolveType(this.context), 0, i);
        } else {
            list = queryIntentActivities;
        }
        if (list == null || list.size() <= 0) {
            return null;
        }
        Intent intent2 = new Intent(intent);
        intent2.setFlags(268435456);
        intent2.setClassName(list.get(0).activityInfo.packageName, list.get(0).activityInfo.name);
        return intent2;
    }

    public boolean createShortcut(int i, String str, OnEmitShortcutListener onEmitShortcutListener) {
        return createShortcut(i, str, null, onEmitShortcutListener);
    }

    public boolean createShortcut(int i, String str, Intent intent, OnEmitShortcutListener onEmitShortcutListener) {
        Bitmap bitmap;
        InstalledAppInfo installedAppInfo = getInstalledAppInfo(str, 0);
        if (installedAppInfo == null) {
            return false;
        }
        ApplicationInfo applicationInfo = installedAppInfo.getApplicationInfo(i);
        PackageManager packageManager = this.context.getPackageManager();
        try {
            String charSequence = applicationInfo.loadLabel(packageManager).toString();
            Bitmap drawableToBitmap = BitmapUtils.drawableToBitmap(applicationInfo.loadIcon(packageManager));
            if (onEmitShortcutListener != null) {
                String name = onEmitShortcutListener.getName(charSequence);
                if (name == null) {
                    name = charSequence;
                }
                Bitmap icon = onEmitShortcutListener.getIcon(drawableToBitmap);
                if (icon != null) {
                    Bitmap bitmap2 = icon;
                    charSequence = name;
                    bitmap = bitmap2;
                } else {
                    charSequence = name;
                    bitmap = drawableToBitmap;
                }
            } else {
                bitmap = drawableToBitmap;
            }
            Intent launchIntent = getLaunchIntent(str, i);
            if (launchIntent == null) {
                return false;
            }
            Intent intent2 = new Intent();
            intent2.setClassName(getHostPkg(), Constants.SHORTCUT_PROXY_ACTIVITY_NAME);
            intent2.addCategory("android.intent.category.DEFAULT");
            if (intent != null) {
                intent2.putExtra("_VA_|_splash_", intent.toUri(0));
            }
            intent2.putExtra("_VA_|_intent_", launchIntent);
            intent2.putExtra("_VA_|_uri_", launchIntent.toUri(0));
            intent2.putExtra("_VA_|_user_id_", VUserHandle.myUserId());
            Intent intent3 = new Intent();
            intent3.putExtra("android.intent.extra.shortcut.INTENT", intent2);
            intent3.putExtra("android.intent.extra.shortcut.NAME", charSequence);
            intent3.putExtra("android.intent.extra.shortcut.ICON", bitmap);
            intent3.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            this.context.sendBroadcast(intent3);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public boolean removeShortcut(int i, String str, Intent intent, OnEmitShortcutListener onEmitShortcutListener) {
        String str2;
        InstalledAppInfo installedAppInfo = getInstalledAppInfo(str, 0);
        if (installedAppInfo == null) {
            return false;
        }
        try {
            String charSequence = installedAppInfo.getApplicationInfo(i).loadLabel(this.context.getPackageManager()).toString();
            if (onEmitShortcutListener == null || (str2 = onEmitShortcutListener.getName(charSequence)) == null) {
                str2 = charSequence;
            }
            Intent launchIntent = getLaunchIntent(str, i);
            if (launchIntent == null) {
                return false;
            }
            Intent intent2 = new Intent();
            intent2.setClassName(getHostPkg(), Constants.SHORTCUT_PROXY_ACTIVITY_NAME);
            intent2.addCategory("android.intent.category.DEFAULT");
            if (intent != null) {
                intent2.putExtra("_VA_|_splash_", intent.toUri(0));
            }
            intent2.putExtra("_VA_|_intent_", launchIntent);
            intent2.putExtra("_VA_|_uri_", launchIntent.toUri(0));
            intent2.putExtra("_VA_|_user_id_", VUserHandle.myUserId());
            Intent intent3 = new Intent();
            intent3.putExtra("android.intent.extra.shortcut.INTENT", intent2);
            intent3.putExtra("android.intent.extra.shortcut.NAME", str2);
            intent3.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
            this.context.sendBroadcast(intent3);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public void setUiCallback(Intent intent, IUiCallback iUiCallback) {
        if (iUiCallback != null) {
            Bundle bundle = new Bundle();
            BundleCompat.putBinder(bundle, "_VA_|_ui_callback_", iUiCallback.asBinder());
            intent.putExtra("_VA_|_sender_", bundle);
        }
    }

    public InstalledAppInfo getInstalledAppInfo(String str, int i) {
        try {
            return getService().getInstalledAppInfo(str, i);
        } catch (RemoteException e2) {
            return (InstalledAppInfo) VirtualRuntime.crash(e2);
        }
    }

    public int getInstalledAppCount() {
        try {
            return getService().getInstalledAppCount();
        } catch (RemoteException e2) {
            return ((Integer) VirtualRuntime.crash(e2)).intValue();
        }
    }

    public boolean isStartup() {
        return this.isStartUp;
    }

    public boolean uninstallPackageAsUser(String str, int i) {
        try {
            return getService().uninstallPackageAsUser(str, i);
        } catch (RemoteException e2) {
            return false;
        }
    }

    public boolean uninstallPackage(String str) {
        try {
            return getService().uninstallPackage(str);
        } catch (RemoteException e2) {
            return false;
        }
    }

    public Resources getResources(String str) {
        InstalledAppInfo installedAppInfo = getInstalledAppInfo(str, 0);
        if (installedAppInfo != null) {
            AssetManager newInstance = mirror.android.content.res.AssetManager.ctor.newInstance();
            mirror.android.content.res.AssetManager.addAssetPath.call(newInstance, installedAppInfo.apkPath);
            Resources resources = this.context.getResources();
            return new Resources(newInstance, resources.getDisplayMetrics(), resources.getConfiguration());
        }
        throw new Resources.NotFoundException(str);
    }

    public synchronized ActivityInfo resolveActivityInfo(Intent intent, int i) {
        ActivityInfo activityInfo;
        activityInfo = null;
        if (intent.getComponent() == null) {
            ResolveInfo resolveIntent = VPackageManager.get().resolveIntent(intent, intent.getType(), 0, i);
            if (!(resolveIntent == null || resolveIntent.activityInfo == null)) {
                activityInfo = resolveIntent.activityInfo;
                intent.setClassName(activityInfo.packageName, activityInfo.name);
            }
        } else {
            activityInfo = resolveActivityInfo(intent.getComponent(), i);
        }
        if (!(activityInfo == null || activityInfo.targetActivity == null)) {
            ComponentName componentName = new ComponentName(activityInfo.packageName, activityInfo.targetActivity);
            activityInfo = VPackageManager.get().getActivityInfo(componentName, 0, i);
            intent.setComponent(componentName);
        }
        return activityInfo;
    }

    public ActivityInfo resolveActivityInfo(ComponentName componentName, int i) {
        return VPackageManager.get().getActivityInfo(componentName, 0, i);
    }

    public ServiceInfo resolveServiceInfo(Intent intent, int i) {
        ResolveInfo resolveService = VPackageManager.get().resolveService(intent, intent.getType(), 0, i);
        if (resolveService != null) {
            return resolveService.serviceInfo;
        }
        return null;
    }

    public void killApp(String str, int i) {
        VActivityManager.get().killAppByPkg(str, i);
    }

    public void killAllApps() {
        VActivityManager.get().killAllApps();
    }

    public List<InstalledAppInfo> getInstalledApps(int i) {
        try {
            return getService().getInstalledApps(i);
        } catch (RemoteException e2) {
            return (List) VirtualRuntime.crash(e2);
        }
    }

    public List<InstalledAppInfo> getInstalledAppsAsUser(int i, int i2) {
        try {
            return getService().getInstalledAppsAsUser(i, i2);
        } catch (RemoteException e2) {
            return (List) VirtualRuntime.crash(e2);
        }
    }

    public void clearAppRequestListener() {
        try {
            getService().clearAppRequestListener();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void scanApps() {
        try {
            getService().scanApps();
        } catch (RemoteException e2) {
        }
    }

    public IAppRequestListener getAppRequestListener() {
        try {
            return getService().getAppRequestListener();
        } catch (RemoteException e2) {
            return (IAppRequestListener) VirtualRuntime.crash(e2);
        }
    }

    public void setAppRequestListener(final AppRequestListener appRequestListener) {
        try {
            getService().setAppRequestListener(new IAppRequestListener.Stub() {
                public void onRequestInstall(final String str) {
                    VirtualRuntime.getUIHandler().post(new Runnable() {
                        public void run() {
                            appRequestListener.onRequestInstall(str);
                        }
                    });
                }

                public void onRequestUninstall(final String str) {
                    VirtualRuntime.getUIHandler().post(new Runnable() {
                        public void run() {
                            appRequestListener.onRequestUninstall(str);
                        }
                    });
                }
            });
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public boolean isPackageLaunched(int i, String str) {
        try {
            return getService().isPackageLaunched(i, str);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public void setPackageHidden(int i, String str, boolean z) {
        try {
            getService().setPackageHidden(i, str, z);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public boolean installPackageAsUser(int i, String str) {
        try {
            return getService().installPackageAsUser(i, str);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public boolean isAppInstalledAsUser(int i, String str) {
        try {
            return getService().isAppInstalledAsUser(i, str);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public int[] getPackageInstalledUsers(String str) {
        try {
            return getService().getPackageInstalledUsers(str);
        } catch (RemoteException e2) {
            return (int[]) VirtualRuntime.crash(e2);
        }
    }

    public void registerObserver(IPackageObserver iPackageObserver) {
        try {
            getService().registerObserver(iPackageObserver);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void unregisterObserver(IPackageObserver iPackageObserver) {
        try {
            getService().unregisterObserver(iPackageObserver);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public boolean isOutsideInstalled(String str) {
        try {
            return this.unHookPackageManager.getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    public int getSystemPid() {
        return this.systemPid;
    }

    public static abstract class VirtualInitializer {
        public void onMainProcess() {
        }

        public void onVirtualProcess() {
        }

        public void onServerProcess() {
        }

        public void onChildProcess() {
        }
    }
}
