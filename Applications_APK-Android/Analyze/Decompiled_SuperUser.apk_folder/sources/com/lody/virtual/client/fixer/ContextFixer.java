package com.lody.virtual.client.fixer;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.DropBoxManager;
import com.lody.virtual.client.core.InvocationStubManager;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.BinderInvocationStub;
import com.lody.virtual.client.hook.proxies.dropbox.DropBoxManagerStub;
import com.lody.virtual.client.hook.proxies.graphics.GraphicsStatsStub;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.ReflectException;
import mirror.android.app.ContextImpl;
import mirror.android.app.ContextImplKitkat;
import mirror.android.content.ContentResolverJBMR2;

public class ContextFixer {
    private static final String TAG = ContextFixer.class.getSimpleName();

    public static void fixContext(Context context) {
        try {
            context.getPackageName();
            InvocationStubManager.getInstance().checkEnv(GraphicsStatsStub.class);
            int i = 0;
            while (true) {
                int i2 = i;
                Context context2 = context;
                if (context2 instanceof ContextWrapper) {
                    context = ((ContextWrapper) context2).getBaseContext();
                    i = i2 + 1;
                    if (i >= 10) {
                        return;
                    }
                } else {
                    ContextImpl.mPackageManager.set(context2, null);
                    try {
                        context2.getPackageManager();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                    if (VirtualCore.get().isVAppProcess()) {
                        DropBoxManager dropBoxManager = (DropBoxManager) context2.getSystemService("dropbox");
                        BinderInvocationStub binderInvocationStub = (BinderInvocationStub) InvocationStubManager.getInstance().getInvocationStub(DropBoxManagerStub.class);
                        if (binderInvocationStub != null) {
                            try {
                                Reflect.on(dropBoxManager).set("mService", binderInvocationStub.getProxyInterface());
                            } catch (ReflectException e2) {
                                e2.printStackTrace();
                            }
                        }
                        String hostPkg = VirtualCore.get().getHostPkg();
                        ContextImpl.mBasePackageName.set(context2, hostPkg);
                        if (Build.VERSION.SDK_INT >= 19) {
                            ContextImplKitkat.mOpPackageName.set(context2, hostPkg);
                        }
                        if (Build.VERSION.SDK_INT >= 18) {
                            ContentResolverJBMR2.mPackageName.set(context2.getContentResolver(), hostPkg);
                            return;
                        }
                        return;
                    }
                    return;
                }
            }
        } catch (Throwable th2) {
        }
    }
}
