package com.lody.virtual.client.hook.secondary;

import android.app.IServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.helper.collection.ArrayMap;
import com.lody.virtual.server.IBinderDelegateService;

public class ServiceConnectionDelegate extends IServiceConnection.Stub {
    private static final ArrayMap<IBinder, ServiceConnectionDelegate> DELEGATE_MAP = new ArrayMap<>();
    private IServiceConnection mConn;

    private ServiceConnectionDelegate(IServiceConnection iServiceConnection) {
        this.mConn = iServiceConnection;
    }

    public static ServiceConnectionDelegate getDelegate(IServiceConnection iServiceConnection) {
        if (iServiceConnection instanceof ServiceConnectionDelegate) {
            return (ServiceConnectionDelegate) iServiceConnection;
        }
        IBinder asBinder = iServiceConnection.asBinder();
        ServiceConnectionDelegate serviceConnectionDelegate = DELEGATE_MAP.get(asBinder);
        if (serviceConnectionDelegate == null) {
            serviceConnectionDelegate = new ServiceConnectionDelegate(iServiceConnection);
            DELEGATE_MAP.put(asBinder, serviceConnectionDelegate);
        }
        return serviceConnectionDelegate;
    }

    public static ServiceConnectionDelegate removeDelegate(IServiceConnection iServiceConnection) {
        return DELEGATE_MAP.remove(iServiceConnection.asBinder());
    }

    public void connected(ComponentName componentName, IBinder iBinder) {
        IBinder service;
        IBinderDelegateService asInterface = IBinderDelegateService.Stub.asInterface(iBinder);
        if (asInterface != null && (iBinder = ProxyServiceFactory.getProxyService(VClientImpl.get().getCurrentApplication(), (componentName = asInterface.getComponent()), (service = asInterface.getService()))) == null) {
            iBinder = service;
        }
        this.mConn.connected(componentName, iBinder);
    }
}
