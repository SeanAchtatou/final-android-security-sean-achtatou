package com.lody.virtual.client.hook.proxies.clipboard;

import android.os.Build;
import android.os.IInterface;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.BinderInvocationStub;
import com.lody.virtual.client.hook.base.ReplaceLastPkgMethodProxy;
import com.lody.virtual.helper.compat.BuildCompat;
import mirror.android.content.ClipboardManager;
import mirror.android.content.ClipboardManagerOreo;

public class ClipBoardStub extends BinderInvocationProxy {
    public ClipBoardStub() {
        super(getInterface(), "clipboard");
    }

    private static IInterface getInterface() {
        if (!BuildCompat.isOreo()) {
            return ClipboardManager.getService.call(new Object[0]);
        }
        return ClipboardManagerOreo.mService.get((android.content.ClipboardManager) VirtualCore.get().getContext().getSystemService("clipboard"));
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new ReplaceLastPkgMethodProxy("getPrimaryClip"));
        if (Build.VERSION.SDK_INT > 17) {
            addMethodProxy(new ReplaceLastPkgMethodProxy("setPrimaryClip"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("getPrimaryClipDescription"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("hasPrimaryClip"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("addPrimaryClipChangedListener"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("removePrimaryClipChangedListener"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("hasClipboardText"));
        }
    }

    public void inject() {
        super.inject();
        if (BuildCompat.isOreo()) {
            ClipboardManagerOreo.mService.set((android.content.ClipboardManager) VirtualCore.get().getContext().getSystemService("clipboard"), ((BinderInvocationStub) getInvocationStub()).getProxyInterface());
            return;
        }
        ClipboardManager.sService.set(((BinderInvocationStub) getInvocationStub()).getProxyInterface());
    }
}
