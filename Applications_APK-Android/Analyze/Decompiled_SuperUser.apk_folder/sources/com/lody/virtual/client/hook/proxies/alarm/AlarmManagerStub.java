package com.lody.virtual.client.hook.proxies.alarm;

import android.os.Build;
import android.os.WorkSource;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.helper.utils.ArrayUtils;
import java.lang.reflect.Method;
import mirror.android.app.IAlarmManager;

public class AlarmManagerStub extends BinderInvocationProxy {
    public AlarmManagerStub() {
        super(IAlarmManager.Stub.asInterface, "alarm");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new Set());
        addMethodProxy(new SetTime());
        addMethodProxy(new SetTimeZone());
    }

    private static class SetTimeZone extends MethodProxy {
        private SetTimeZone() {
        }

        public String getMethodName() {
            return "setTimeZone";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return null;
        }
    }

    private static class SetTime extends MethodProxy {
        private SetTime() {
        }

        public String getMethodName() {
            return "setTime";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (Build.VERSION.SDK_INT >= 21) {
                return false;
            }
            return null;
        }
    }

    private static class Set extends MethodProxy {
        private Set() {
        }

        public String getMethodName() {
            return "set";
        }

        public boolean beforeCall(Object obj, Method method, Object... objArr) {
            if (Build.VERSION.SDK_INT >= 24 && (objArr[0] instanceof String)) {
                objArr[0] = getHostPkg();
            }
            int indexOfFirst = ArrayUtils.indexOfFirst(objArr, WorkSource.class);
            if (indexOfFirst < 0) {
                return true;
            }
            objArr[indexOfFirst] = null;
            return true;
        }
    }
}
