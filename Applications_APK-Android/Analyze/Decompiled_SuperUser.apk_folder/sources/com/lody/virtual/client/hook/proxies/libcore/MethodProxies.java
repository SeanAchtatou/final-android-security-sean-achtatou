package com.lody.virtual.client.hook.proxies.libcore;

import com.lody.virtual.client.NativeEngine;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.helper.utils.Reflect;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import mirror.libcore.io.Os;

class MethodProxies {
    MethodProxies() {
    }

    static class Lstat extends Stat {
        Lstat() {
        }

        public String getMethodName() {
            return "lstat";
        }
    }

    static class Getpwnam extends MethodProxy {
        Getpwnam() {
        }

        public String getMethodName() {
            return "getpwnam";
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            if (obj2 != null) {
                Reflect on = Reflect.on(obj2);
                if (((Integer) on.get("pw_uid")).intValue() == VirtualCore.get().myUid()) {
                    on.set("pw_uid", Integer.valueOf(VClientImpl.get().getVUid()));
                }
            }
            return obj2;
        }
    }

    static class GetUid extends MethodProxy {
        GetUid() {
        }

        public String getMethodName() {
            return "getuid";
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            return Integer.valueOf(NativeEngine.onGetUid(((Integer) obj2).intValue()));
        }
    }

    static class GetsockoptUcred extends MethodProxy {
        GetsockoptUcred() {
        }

        public String getMethodName() {
            return "getsockoptUcred";
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            if (obj2 != null) {
                Reflect on = Reflect.on(obj2);
                if (((Integer) on.get("uid")).intValue() == VirtualCore.get().myUid()) {
                    on.set("uid", Integer.valueOf(getBaseVUid()));
                }
            }
            return obj2;
        }
    }

    static class Stat extends MethodProxy {
        private static Field st_uid;

        Stat() {
        }

        static {
            try {
                st_uid = Os.TYPE.getMethod("stat", String.class).getReturnType().getDeclaredField("st_uid");
                st_uid.setAccessible(true);
            } catch (Throwable th) {
                throw new IllegalStateException(th);
            }
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            if (((Integer) st_uid.get(obj2)).intValue() == VirtualCore.get().myUid()) {
                st_uid.set(obj2, Integer.valueOf(getBaseVUid()));
            }
            return obj2;
        }

        public String getMethodName() {
            return "stat";
        }
    }
}
