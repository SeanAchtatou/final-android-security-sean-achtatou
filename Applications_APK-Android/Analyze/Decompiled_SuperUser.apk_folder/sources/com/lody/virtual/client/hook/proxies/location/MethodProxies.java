package com.lody.virtual.client.hook.proxies.location;

import android.location.Location;
import android.location.LocationRequest;
import android.location.a;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.base.ReplaceLastPkgMethodProxy;
import com.lody.virtual.client.ipc.VirtualLocationManager;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.SchedulerTask;
import com.lody.virtual.remote.vloc.VLocation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import mirror.android.location.LocationRequestL;

public class MethodProxies {
    /* access modifiers changed from: private */
    public static Map<IBinder, UpdateLocationTask> tasks = new HashMap();

    /* access modifiers changed from: private */
    public static void fixLocationRequest(LocationRequest locationRequest) {
        if (locationRequest != null) {
            if (LocationRequestL.mHideFromAppOps != null) {
                LocationRequestL.mHideFromAppOps.set(locationRequest, false);
            }
            if (LocationRequestL.mWorkSource != null) {
                LocationRequestL.mWorkSource.set(locationRequest, null);
            }
        }
    }

    private static class AddGpsStatusListener extends ReplaceLastPkgMethodProxy {
        public AddGpsStatusListener() {
            super("addGpsStatusListener");
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (isFakeLocationEnable()) {
                return false;
            }
            return super.call(obj, method, objArr);
        }
    }

    private static class UpdateLocationTask extends SchedulerTask {
        private a listener;

        public UpdateLocationTask(a aVar) {
            super(VirtualRuntime.getUIHandler(), VASettings.LOCATION_UPDATE_PERIOD);
            this.listener = aVar;
        }

        public void run() {
            VLocation location = VirtualLocationManager.get().getLocation(MethodProxy.getAppUserId(), MethodProxy.getAppPkg());
            if (location != null) {
                Location createLocation = MethodProxies.createLocation(location);
                if (this.listener.asBinder().isBinderAlive()) {
                    try {
                        this.listener.a(createLocation);
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    private static class RequestLocationUpdates extends ReplaceLastPkgMethodProxy {
        public RequestLocationUpdates() {
            super("requestLocationUpdates");
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodProxies.fixLocationRequest((LocationRequest) objArr[0]);
            if (!isFakeLocationEnable()) {
                return super.call(obj, method, objArr);
            }
            a aVar = (a) objArr[1];
            UpdateLocationTask updateLocationTask = new UpdateLocationTask(aVar);
            MethodProxies.tasks.put(aVar.asBinder(), updateLocationTask);
            updateLocationTask.schedule();
            return 0;
        }
    }

    private static class RemoveUpdates extends ReplaceLastPkgMethodProxy {
        public RemoveUpdates() {
            super("removeUpdates");
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (!isFakeLocationEnable()) {
                return super.call(obj, method, objArr);
            }
            UpdateLocationTask updateLocationTask = (UpdateLocationTask) MethodProxies.tasks.get((IBinder) objArr[0]);
            if (updateLocationTask != null) {
                updateLocationTask.cancel();
            }
            return 0;
        }
    }

    private static class GetLastLocation extends ReplaceLastPkgMethodProxy {
        public GetLastLocation() {
            super("getLastLocation");
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (!isFakeLocationEnable()) {
                return super.call(obj, method, objArr);
            }
            MethodProxies.fixLocationRequest((LocationRequest) objArr[0]);
            VLocation location = VirtualLocationManager.get().getLocation(getAppUserId(), getAppPkg());
            if (location != null) {
                return MethodProxies.createLocation(location);
            }
            return null;
        }
    }

    private static class GetLastKnownLocation extends GetLastLocation {
        private GetLastKnownLocation() {
        }

        public String getMethodName() {
            return "getLastKnownLocation";
        }
    }

    private static class IsProviderEnabled extends MethodProxy {
        private IsProviderEnabled() {
        }

        public String getMethodName() {
            return "isProviderEnabled";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (!isFakeLocationEnable() || !"gps".equals((String) objArr[0])) {
                return super.call(obj, method, objArr);
            }
            return true;
        }
    }

    private static class getAllProviders extends MethodProxy {
        private getAllProviders() {
        }

        public String getMethodName() {
            return "getAllProviders";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (!isFakeLocationEnable()) {
                return super.call(obj, method, objArr);
            }
            return Arrays.asList("network", "gps");
        }
    }

    private static class GetBestProvider extends MethodProxy {
        private GetBestProvider() {
        }

        public String getMethodName() {
            return "getBestProvider";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (isFakeLocationEnable()) {
                return "network";
            }
            return super.call(obj, method, objArr);
        }
    }

    public static Location createLocation(VLocation vLocation) {
        Location location = new Location("gps");
        if (vLocation.accuracy == 0.0f) {
            location.setAccuracy(8.0f);
        } else {
            location.setAccuracy(vLocation.accuracy);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("satellites", 23);
        location.setBearing(vLocation.bearing);
        Reflect.on(location).call("setIsFromMockProvider", false);
        location.setLatitude(vLocation.latitude);
        location.setLongitude(vLocation.longitude);
        location.setSpeed(vLocation.speed);
        location.setTime(System.currentTimeMillis());
        location.setExtras(bundle);
        Reflect.on(location).call("setElapsedRealtimeNanos", Long.valueOf(SystemClock.elapsedRealtime()));
        return location;
    }
}
