package com.lody.virtual.client.hook.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.util.Log;
import com.lody.virtual.client.core.VirtualCore;
import java.io.FileDescriptor;
import java.lang.reflect.Method;
import mirror.RefStaticMethod;
import mirror.android.os.ServiceManager;

public class BinderInvocationStub extends MethodInvocationStub<IInterface> implements IBinder {
    private static final String TAG = BinderInvocationStub.class.getSimpleName();
    private IBinder mBaseBinder;

    public BinderInvocationStub(RefStaticMethod<IInterface> refStaticMethod, IBinder iBinder) {
        this(asInterface(refStaticMethod, iBinder));
    }

    public BinderInvocationStub(Class<?> cls, IBinder iBinder) {
        this(asInterface(cls, iBinder));
    }

    public BinderInvocationStub(IInterface iInterface) {
        super(iInterface);
        IBinder iBinder;
        if (getBaseInterface() != null) {
            iBinder = ((IInterface) getBaseInterface()).asBinder();
        } else {
            iBinder = null;
        }
        this.mBaseBinder = iBinder;
        addMethodProxy(new AsBinder());
    }

    private static IInterface asInterface(RefStaticMethod<IInterface> refStaticMethod, IBinder iBinder) {
        if (refStaticMethod == null || iBinder == null) {
            return null;
        }
        return refStaticMethod.call(iBinder);
    }

    private static IInterface asInterface(Class<?> cls, IBinder iBinder) {
        if (cls == null || iBinder == null) {
            return null;
        }
        try {
            return (IInterface) cls.getMethod("asInterface", IBinder.class).invoke(null, iBinder);
        } catch (Exception e2) {
            Log.d(TAG, "Could not create stub " + cls.getName() + ". Cause: " + e2);
            return null;
        }
    }

    public void replaceService(String str) {
        if (this.mBaseBinder != null) {
            ServiceManager.sCache.get().put(str, this);
        }
    }

    private final class AsBinder extends MethodProxy {
        private AsBinder() {
        }

        public String getMethodName() {
            return "asBinder";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return BinderInvocationStub.this;
        }
    }

    public String getInterfaceDescriptor() {
        return this.mBaseBinder.getInterfaceDescriptor();
    }

    public Context getContext() {
        return VirtualCore.get().getContext();
    }

    public boolean pingBinder() {
        return this.mBaseBinder.pingBinder();
    }

    public boolean isBinderAlive() {
        return this.mBaseBinder.isBinderAlive();
    }

    public IInterface queryLocalInterface(String str) {
        return (IInterface) getProxyInterface();
    }

    public void dump(FileDescriptor fileDescriptor, String[] strArr) {
        this.mBaseBinder.dump(fileDescriptor, strArr);
    }

    @TargetApi(13)
    public void dumpAsync(FileDescriptor fileDescriptor, String[] strArr) {
        this.mBaseBinder.dumpAsync(fileDescriptor, strArr);
    }

    public boolean transact(int i, Parcel parcel, Parcel parcel2, int i2) {
        return this.mBaseBinder.transact(i, parcel, parcel2, i2);
    }

    public void linkToDeath(IBinder.DeathRecipient deathRecipient, int i) {
        this.mBaseBinder.linkToDeath(deathRecipient, i);
    }

    public boolean unlinkToDeath(IBinder.DeathRecipient deathRecipient, int i) {
        return this.mBaseBinder.unlinkToDeath(deathRecipient, i);
    }

    public IBinder getBaseBinder() {
        return this.mBaseBinder;
    }
}
