package com.lody.virtual.client.stub;

import android.accounts.AuthenticatorDescription;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.lody.virtual.R;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VAccountManager;
import com.lody.virtual.helper.utils.VLog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ChooseAccountTypeActivity extends Activity {
    private static final String TAG = "AccountChooser";
    /* access modifiers changed from: private */
    public ArrayList<AuthInfo> mAuthenticatorInfosToDisplay;
    private HashMap<String, AuthInfo> mTypeToAuthenticatorInfo = new HashMap<>();

    public void onCreate(Bundle bundle) {
        HashSet hashSet;
        super.onCreate(bundle);
        String[] stringArrayExtra = getIntent().getStringArrayExtra(ChooseTypeAndAccountActivity.EXTRA_ALLOWABLE_ACCOUNT_TYPES_STRING_ARRAY);
        if (stringArrayExtra != null) {
            HashSet hashSet2 = new HashSet(stringArrayExtra.length);
            Collections.addAll(hashSet2, stringArrayExtra);
            hashSet = hashSet2;
        } else {
            hashSet = null;
        }
        buildTypeToAuthDescriptionMap();
        this.mAuthenticatorInfosToDisplay = new ArrayList<>(this.mTypeToAuthenticatorInfo.size());
        for (Map.Entry next : this.mTypeToAuthenticatorInfo.entrySet()) {
            String str = (String) next.getKey();
            AuthInfo authInfo = (AuthInfo) next.getValue();
            if (hashSet == null || hashSet.contains(str)) {
                this.mAuthenticatorInfosToDisplay.add(authInfo);
            }
        }
        if (this.mAuthenticatorInfosToDisplay.isEmpty()) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("errorMessage", "no allowable account types");
            setResult(-1, new Intent().putExtras(bundle2));
            finish();
        } else if (this.mAuthenticatorInfosToDisplay.size() == 1) {
            setResultAndFinish(this.mAuthenticatorInfosToDisplay.get(0).desc.type);
        } else {
            setContentView(R.layout.an);
            ListView listView = (ListView) findViewById(16908298);
            listView.setAdapter((ListAdapter) new AccountArrayAdapter(this, 17367043, this.mAuthenticatorInfosToDisplay));
            listView.setChoiceMode(0);
            listView.setTextFilterEnabled(false);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    ChooseAccountTypeActivity.this.setResultAndFinish(((AuthInfo) ChooseAccountTypeActivity.this.mAuthenticatorInfosToDisplay.get(i)).desc.type);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void setResultAndFinish(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("accountType", str);
        setResult(-1, new Intent().putExtras(bundle));
        VLog.v(TAG, "ChooseAccountTypeActivity.setResultAndFinish: selected account type " + str, new Object[0]);
        finish();
    }

    private void buildTypeToAuthDescriptionMap() {
        AuthenticatorDescription authenticatorDescription;
        String str;
        Drawable drawable;
        AuthenticatorDescription[] authenticatorTypes = VAccountManager.get().getAuthenticatorTypes();
        int length = authenticatorTypes.length;
        for (int i = 0; i < length; i++) {
            authenticatorDescription = authenticatorTypes[i];
            try {
                Resources resources = VirtualCore.get().getResources(authenticatorDescription.packageName);
                drawable = resources.getDrawable(authenticatorDescription.iconId);
                try {
                    CharSequence text = resources.getText(authenticatorDescription.labelId);
                    String charSequence = text.toString();
                    try {
                        str = text.toString();
                    } catch (Resources.NotFoundException e2) {
                    }
                } catch (Resources.NotFoundException e3) {
                    str = null;
                }
            } catch (Resources.NotFoundException e4) {
                drawable = null;
                str = null;
            }
            this.mTypeToAuthenticatorInfo.put(authenticatorDescription.type, new AuthInfo(authenticatorDescription, str, drawable));
        }
        return;
        VLog.w(TAG, "No icon resource for account type " + authenticatorDescription.type, new Object[0]);
        this.mTypeToAuthenticatorInfo.put(authenticatorDescription.type, new AuthInfo(authenticatorDescription, str, drawable));
    }

    private static class AuthInfo {
        final AuthenticatorDescription desc;
        final Drawable drawable;
        final String name;

        AuthInfo(AuthenticatorDescription authenticatorDescription, String str, Drawable drawable2) {
            this.desc = authenticatorDescription;
            this.name = str;
            this.drawable = drawable2;
        }
    }

    private static class ViewHolder {
        ImageView icon;
        TextView text;

        private ViewHolder() {
        }
    }

    private static class AccountArrayAdapter extends ArrayAdapter<AuthInfo> {
        private ArrayList<AuthInfo> mInfos;
        private LayoutInflater mLayoutInflater;

        AccountArrayAdapter(Context context, int i, ArrayList<AuthInfo> arrayList) {
            super(context, i, arrayList);
            this.mInfos = arrayList;
            this.mLayoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = this.mLayoutInflater.inflate(R.layout.am, (ViewGroup) null);
                viewHolder = new ViewHolder();
                viewHolder.text = (TextView) view.findViewById(R.id.fh);
                viewHolder.icon = (ImageView) view.findViewById(R.id.fg);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.text.setText(this.mInfos.get(i).name);
            viewHolder.icon.setImageDrawable(this.mInfos.get(i).drawable);
            return view;
        }
    }
}
