package com.lody.virtual.client.hook.proxies.input;

import android.view.inputmethod.EditorInfo;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.helper.utils.ArrayUtils;
import java.lang.reflect.Method;

class MethodProxies {
    MethodProxies() {
    }

    static class StartInput extends MethodProxy {
        StartInput() {
        }

        public String getMethodName() {
            return "startInput";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr.length > 2 && (objArr[2] instanceof EditorInfo)) {
                ((EditorInfo) objArr[2]).packageName = getHostPkg();
            }
            return method.invoke(obj, objArr);
        }
    }

    static class WindowGainedFocus extends MethodProxy {
        private int editorInfoIndex = -1;
        private Boolean noEditorInfo = null;

        WindowGainedFocus() {
        }

        public String getMethodName() {
            return "windowGainedFocus";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            EditorInfo editorInfo;
            if (this.noEditorInfo == null) {
                this.editorInfoIndex = ArrayUtils.indexOfFirst(objArr, EditorInfo.class);
                this.noEditorInfo = Boolean.valueOf(this.editorInfoIndex == -1);
            }
            if (!this.noEditorInfo.booleanValue() && (editorInfo = (EditorInfo) objArr[this.editorInfoIndex]) != null) {
                editorInfo.packageName = getHostPkg();
            }
            return method.invoke(obj, objArr);
        }
    }
}
