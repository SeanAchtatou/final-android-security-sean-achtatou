package com.lody.virtual.client.hook.utils;

import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.ArrayUtils;
import java.util.Arrays;
import java.util.HashSet;

public class MethodParameterUtils {
    public static String replaceFirstAppPkg(Object[] objArr) {
        int indexOfFirst;
        if (objArr == null || (indexOfFirst = ArrayUtils.indexOfFirst(objArr, String.class)) == -1) {
            return null;
        }
        String str = (String) objArr[indexOfFirst];
        objArr[indexOfFirst] = VirtualCore.get().getHostPkg();
        return str;
    }

    public static String replaceLastAppPkg(Object[] objArr) {
        int indexOfLast = ArrayUtils.indexOfLast(objArr, String.class);
        if (indexOfLast == -1) {
            return null;
        }
        String str = (String) objArr[indexOfLast];
        objArr[indexOfLast] = VirtualCore.get().getHostPkg();
        return str;
    }

    public static String replaceSequenceAppPkg(Object[] objArr, int i) {
        int indexOf = ArrayUtils.indexOf(objArr, String.class, i);
        if (indexOf == -1) {
            return null;
        }
        String str = (String) objArr[indexOf];
        objArr[indexOf] = VirtualCore.get().getHostPkg();
        return str;
    }

    public static Class<?>[] getAllInterface(Class cls) {
        HashSet hashSet = new HashSet();
        getAllInterfaces(cls, hashSet);
        Class<?>[] clsArr = new Class[hashSet.size()];
        hashSet.toArray(clsArr);
        return clsArr;
    }

    public static void getAllInterfaces(Class cls, HashSet<Class<?>> hashSet) {
        Class<?>[] interfaces = cls.getInterfaces();
        if (interfaces.length != 0) {
            hashSet.addAll(Arrays.asList(interfaces));
        }
        if (cls.getSuperclass() != Object.class) {
            getAllInterfaces(cls.getSuperclass(), hashSet);
        }
    }
}
