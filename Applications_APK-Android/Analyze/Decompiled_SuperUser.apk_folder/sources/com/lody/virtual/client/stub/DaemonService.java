package com.lody.virtual.client.stub;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

public class DaemonService extends Service {
    private static final int NOTIFY_ID = 1001;

    public static void startup(Context context) {
        context.startService(new Intent(context, DaemonService.class));
    }

    public void onDestroy() {
        super.onDestroy();
        startup(this);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, InnerService.class));
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }

    public static final class InnerService extends Service {
        public int onStartCommand(Intent intent, int i, int i2) {
            stopForeground(true);
            stopSelf();
            return super.onStartCommand(intent, i, i2);
        }

        public IBinder onBind(Intent intent) {
            return null;
        }
    }
}
