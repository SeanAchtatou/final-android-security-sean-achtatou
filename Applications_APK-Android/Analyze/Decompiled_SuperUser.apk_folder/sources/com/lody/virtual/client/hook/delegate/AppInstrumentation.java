package com.lody.virtual.client.hook.delegate;

import android.app.Activity;
import android.app.Application;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.RemoteException;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.fixer.ActivityFixer;
import com.lody.virtual.client.fixer.ContextFixer;
import com.lody.virtual.client.interfaces.IInjector;
import com.lody.virtual.client.ipc.ActivityClientRecord;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.compat.BundleCompat;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.server.interfaces.IUiCallback;
import mirror.android.app.ActivityThread;

public final class AppInstrumentation extends InstrumentationDelegate implements IInjector {
    private static final String TAG = AppInstrumentation.class.getSimpleName();
    private static AppInstrumentation gDefault;

    private AppInstrumentation(Instrumentation instrumentation) {
        super(instrumentation);
    }

    public static AppInstrumentation getDefault() {
        if (gDefault == null) {
            synchronized (AppInstrumentation.class) {
                if (gDefault == null) {
                    gDefault = create();
                }
            }
        }
        return gDefault;
    }

    private static AppInstrumentation create() {
        Instrumentation instrumentation = ActivityThread.mInstrumentation.get(VirtualCore.mainThread());
        if (instrumentation instanceof AppInstrumentation) {
            return (AppInstrumentation) instrumentation;
        }
        return new AppInstrumentation(instrumentation);
    }

    public void inject() {
        this.base = ActivityThread.mInstrumentation.get(VirtualCore.mainThread());
        ActivityThread.mInstrumentation.set(VirtualCore.mainThread(), this);
    }

    public boolean isEnvBad() {
        return !(ActivityThread.mInstrumentation.get(VirtualCore.mainThread()) instanceof AppInstrumentation);
    }

    public void callActivityOnCreate(Activity activity, Bundle bundle) {
        VirtualCore.get().getComponentDelegate().beforeActivityCreate(activity);
        ActivityClientRecord activityRecord = VActivityManager.get().getActivityRecord(mirror.android.app.Activity.mToken.get(activity));
        if (activityRecord != null) {
            activityRecord.activity = activity;
        }
        ContextFixer.fixContext(activity);
        ActivityFixer.fixActivity(activity);
        ActivityInfo activityInfo = null;
        if (activityRecord != null) {
            activityInfo = activityRecord.info;
        }
        if (activityInfo != null) {
            if (activityInfo.theme != 0) {
                activity.setTheme(activityInfo.theme);
            }
            if (activity.getRequestedOrientation() == -1 && activityInfo.screenOrientation != -1) {
                activity.setRequestedOrientation(activityInfo.screenOrientation);
            }
        }
        super.callActivityOnCreate(activity, bundle);
        VirtualCore.get().getComponentDelegate().afterActivityCreate(activity);
    }

    public void callActivityOnResume(Activity activity) {
        Bundle bundleExtra;
        IUiCallback asInterface;
        VirtualCore.get().getComponentDelegate().beforeActivityResume(activity);
        VActivityManager.get().onActivityResumed(activity);
        super.callActivityOnResume(activity);
        VirtualCore.get().getComponentDelegate().afterActivityResume(activity);
        Intent intent = activity.getIntent();
        if (intent != null && (bundleExtra = intent.getBundleExtra("_VA_|_sender_")) != null && (asInterface = IUiCallback.Stub.asInterface(BundleCompat.getBinder(bundleExtra, "_VA_|_ui_callback_"))) != null) {
            try {
                asInterface.onAppOpened(VClientImpl.get().getCurrentPackage(), VUserHandle.myUserId());
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void callActivityOnDestroy(Activity activity) {
        VirtualCore.get().getComponentDelegate().beforeActivityDestroy(activity);
        super.callActivityOnDestroy(activity);
        VirtualCore.get().getComponentDelegate().afterActivityDestroy(activity);
    }

    public void callActivityOnPause(Activity activity) {
        VirtualCore.get().getComponentDelegate().beforeActivityPause(activity);
        super.callActivityOnPause(activity);
        VirtualCore.get().getComponentDelegate().afterActivityPause(activity);
    }

    public void callApplicationOnCreate(Application application) {
        super.callApplicationOnCreate(application);
    }
}
