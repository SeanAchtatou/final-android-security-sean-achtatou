package com.lody.virtual.client.hook.proxies.account;

import android.accounts.Account;
import android.accounts.c;
import android.os.Bundle;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.ipc.VAccountManager;
import java.lang.reflect.Method;
import mirror.android.accounts.IAccountManager;

public class AccountManagerStub extends BinderInvocationProxy {
    /* access modifiers changed from: private */
    public static VAccountManager Mgr = VAccountManager.get();

    public AccountManagerStub() {
        super(IAccountManager.Stub.asInterface, ServiceManagerNative.ACCOUNT);
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new getPassword());
        addMethodProxy(new getUserData());
        addMethodProxy(new getAuthenticatorTypes());
        addMethodProxy(new getAccounts());
        addMethodProxy(new getAccountsForPackage());
        addMethodProxy(new getAccountsByTypeForPackage());
        addMethodProxy(new getAccountsAsUser());
        addMethodProxy(new hasFeatures());
        addMethodProxy(new getAccountsByFeatures());
        addMethodProxy(new addAccountExplicitly());
        addMethodProxy(new removeAccount());
        addMethodProxy(new removeAccountAsUser());
        addMethodProxy(new removeAccountExplicitly());
        addMethodProxy(new copyAccountToUser());
        addMethodProxy(new invalidateAuthToken());
        addMethodProxy(new peekAuthToken());
        addMethodProxy(new setAuthToken());
        addMethodProxy(new setPassword());
        addMethodProxy(new clearPassword());
        addMethodProxy(new setUserData());
        addMethodProxy(new updateAppPermission());
        addMethodProxy(new getAuthToken());
        addMethodProxy(new addAccount());
        addMethodProxy(new addAccountAsUser());
        addMethodProxy(new updateCredentials());
        addMethodProxy(new editProperties());
        addMethodProxy(new confirmCredentialsAsUser());
        addMethodProxy(new accountAuthenticated());
        addMethodProxy(new getAuthTokenLabel());
        addMethodProxy(new addSharedAccountAsUser());
        addMethodProxy(new getSharedAccountsAsUser());
        addMethodProxy(new removeSharedAccountAsUser());
        addMethodProxy(new renameAccount());
        addMethodProxy(new getPreviousName());
        addMethodProxy(new renameSharedAccountAsUser());
    }

    private static class getPassword extends MethodProxy {
        private getPassword() {
        }

        public String getMethodName() {
            return "getPassword";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.getPassword((Account) objArr[0]);
        }
    }

    private static class getUserData extends MethodProxy {
        private getUserData() {
        }

        public String getMethodName() {
            return "getUserData";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.getUserData((Account) objArr[0], (String) objArr[1]);
        }
    }

    private static class getAuthenticatorTypes extends MethodProxy {
        private getAuthenticatorTypes() {
        }

        public String getMethodName() {
            return "getAuthenticatorTypes";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.getAuthenticatorTypes();
        }
    }

    private static class getAccounts extends MethodProxy {
        private getAccounts() {
        }

        public String getMethodName() {
            return "getAccounts";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.getAccounts((String) objArr[0]);
        }
    }

    private static class getAccountsForPackage extends MethodProxy {
        private getAccountsForPackage() {
        }

        public String getMethodName() {
            return "getAccountsForPackage";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[0];
            return AccountManagerStub.Mgr.getAccounts(null);
        }
    }

    private static class getAccountsByTypeForPackage extends MethodProxy {
        private getAccountsByTypeForPackage() {
        }

        public String getMethodName() {
            return "getAccountsByTypeForPackage";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[1];
            return AccountManagerStub.Mgr.getAccounts((String) objArr[0]);
        }
    }

    private static class getAccountsAsUser extends MethodProxy {
        private getAccountsAsUser() {
        }

        public String getMethodName() {
            return "getAccountsAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.getAccounts((String) objArr[0]);
        }
    }

    private static class hasFeatures extends MethodProxy {
        private hasFeatures() {
        }

        public String getMethodName() {
            return "hasFeatures";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.hasFeatures((c) objArr[0], (Account) objArr[1], (String[]) objArr[2]);
            return 0;
        }
    }

    private static class getAccountsByFeatures extends MethodProxy {
        private getAccountsByFeatures() {
        }

        public String getMethodName() {
            return "getAccountsByFeatures";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.getAccountsByFeatures((c) objArr[0], (String) objArr[1], (String[]) objArr[2]);
            return 0;
        }
    }

    private static class addAccountExplicitly extends MethodProxy {
        private addAccountExplicitly() {
        }

        public String getMethodName() {
            return "addAccountExplicitly";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return Boolean.valueOf(AccountManagerStub.Mgr.addAccountExplicitly((Account) objArr[0], (String) objArr[1], (Bundle) objArr[2]));
        }
    }

    private static class removeAccount extends MethodProxy {
        private removeAccount() {
        }

        public String getMethodName() {
            return "removeAccount";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean booleanValue = ((Boolean) objArr[2]).booleanValue();
            AccountManagerStub.Mgr.removeAccount((c) objArr[0], (Account) objArr[1], booleanValue);
            return 0;
        }
    }

    private static class removeAccountAsUser extends MethodProxy {
        private removeAccountAsUser() {
        }

        public String getMethodName() {
            return "removeAccountAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean booleanValue = ((Boolean) objArr[2]).booleanValue();
            AccountManagerStub.Mgr.removeAccount((c) objArr[0], (Account) objArr[1], booleanValue);
            return 0;
        }
    }

    private static class removeAccountExplicitly extends MethodProxy {
        private removeAccountExplicitly() {
        }

        public String getMethodName() {
            return "removeAccountExplicitly";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return Boolean.valueOf(AccountManagerStub.Mgr.removeAccountExplicitly((Account) objArr[0]));
        }
    }

    private static class copyAccountToUser extends MethodProxy {
        private copyAccountToUser() {
        }

        public String getMethodName() {
            return "copyAccountToUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            c cVar = (c) objArr[0];
            Account account = (Account) objArr[1];
            ((Integer) objArr[2]).intValue();
            ((Integer) objArr[3]).intValue();
            method.invoke(obj, objArr);
            return 0;
        }
    }

    private static class invalidateAuthToken extends MethodProxy {
        private invalidateAuthToken() {
        }

        public String getMethodName() {
            return "invalidateAuthToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.invalidateAuthToken((String) objArr[0], (String) objArr[1]);
            return 0;
        }
    }

    private static class peekAuthToken extends MethodProxy {
        private peekAuthToken() {
        }

        public String getMethodName() {
            return "peekAuthToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.peekAuthToken((Account) objArr[0], (String) objArr[1]);
        }
    }

    private static class setAuthToken extends MethodProxy {
        private setAuthToken() {
        }

        public String getMethodName() {
            return "setAuthToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.setAuthToken((Account) objArr[0], (String) objArr[1], (String) objArr[2]);
            return 0;
        }
    }

    private static class setPassword extends MethodProxy {
        private setPassword() {
        }

        public String getMethodName() {
            return "setPassword";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.setPassword((Account) objArr[0], (String) objArr[1]);
            return 0;
        }
    }

    private static class clearPassword extends MethodProxy {
        private clearPassword() {
        }

        public String getMethodName() {
            return "clearPassword";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.clearPassword((Account) objArr[0]);
            return 0;
        }
    }

    private static class setUserData extends MethodProxy {
        private setUserData() {
        }

        public String getMethodName() {
            return "setUserData";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.setUserData((Account) objArr[0], (String) objArr[1], (String) objArr[2]);
            return 0;
        }
    }

    private static class updateAppPermission extends MethodProxy {
        private updateAppPermission() {
        }

        public String getMethodName() {
            return "updateAppPermission";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Account account = (Account) objArr[0];
            String str = (String) objArr[1];
            ((Integer) objArr[2]).intValue();
            ((Boolean) objArr[3]).booleanValue();
            method.invoke(obj, objArr);
            return 0;
        }
    }

    private static class getAuthToken extends MethodProxy {
        private getAuthToken() {
        }

        public String getMethodName() {
            return "getAuthToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.getAuthToken((c) objArr[0], (Account) objArr[1], (String) objArr[2], ((Boolean) objArr[3]).booleanValue(), ((Boolean) objArr[4]).booleanValue(), (Bundle) objArr[5]);
            return 0;
        }
    }

    private static class addAccount extends MethodProxy {
        private addAccount() {
        }

        public String getMethodName() {
            return "addAccount";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.addAccount((c) objArr[0], (String) objArr[1], (String) objArr[2], (String[]) objArr[3], ((Boolean) objArr[4]).booleanValue(), (Bundle) objArr[5]);
            return 0;
        }
    }

    private static class addAccountAsUser extends MethodProxy {
        private addAccountAsUser() {
        }

        public String getMethodName() {
            return "addAccountAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.addAccount((c) objArr[0], (String) objArr[1], (String) objArr[2], (String[]) objArr[3], ((Boolean) objArr[4]).booleanValue(), (Bundle) objArr[5]);
            return 0;
        }
    }

    private static class updateCredentials extends MethodProxy {
        private updateCredentials() {
        }

        public String getMethodName() {
            return "updateCredentials";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.updateCredentials((c) objArr[0], (Account) objArr[1], (String) objArr[2], ((Boolean) objArr[3]).booleanValue(), (Bundle) objArr[4]);
            return 0;
        }
    }

    private static class editProperties extends MethodProxy {
        private editProperties() {
        }

        public String getMethodName() {
            return "editProperties";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean booleanValue = ((Boolean) objArr[2]).booleanValue();
            AccountManagerStub.Mgr.editProperties((c) objArr[0], (String) objArr[1], booleanValue);
            return 0;
        }
    }

    private static class confirmCredentialsAsUser extends MethodProxy {
        private confirmCredentialsAsUser() {
        }

        public String getMethodName() {
            return "confirmCredentialsAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean booleanValue = ((Boolean) objArr[3]).booleanValue();
            AccountManagerStub.Mgr.confirmCredentials((c) objArr[0], (Account) objArr[1], (Bundle) objArr[2], booleanValue);
            return 0;
        }
    }

    private static class accountAuthenticated extends MethodProxy {
        private accountAuthenticated() {
        }

        public String getMethodName() {
            return "accountAuthenticated";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return Boolean.valueOf(AccountManagerStub.Mgr.accountAuthenticated((Account) objArr[0]));
        }
    }

    private static class getAuthTokenLabel extends MethodProxy {
        private getAuthTokenLabel() {
        }

        public String getMethodName() {
            return "getAuthTokenLabel";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.getAuthTokenLabel((c) objArr[0], (String) objArr[1], (String) objArr[2]);
            return 0;
        }
    }

    private static class addSharedAccountAsUser extends MethodProxy {
        private addSharedAccountAsUser() {
        }

        public String getMethodName() {
            return "addSharedAccountAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Account account = (Account) objArr[0];
            ((Integer) objArr[1]).intValue();
            return method.invoke(obj, objArr);
        }
    }

    private static class getSharedAccountsAsUser extends MethodProxy {
        private getSharedAccountsAsUser() {
        }

        public String getMethodName() {
            return "getSharedAccountsAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ((Integer) objArr[0]).intValue();
            return method.invoke(obj, objArr);
        }
    }

    private static class removeSharedAccountAsUser extends MethodProxy {
        private removeSharedAccountAsUser() {
        }

        public String getMethodName() {
            return "removeSharedAccountAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Account account = (Account) objArr[0];
            ((Integer) objArr[1]).intValue();
            return method.invoke(obj, objArr);
        }
    }

    private static class renameAccount extends MethodProxy {
        private renameAccount() {
        }

        public String getMethodName() {
            return "renameAccount";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            AccountManagerStub.Mgr.renameAccount((c) objArr[0], (Account) objArr[1], (String) objArr[2]);
            return 0;
        }
    }

    private static class getPreviousName extends MethodProxy {
        private getPreviousName() {
        }

        public String getMethodName() {
            return "getPreviousName";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return AccountManagerStub.Mgr.getPreviousName((Account) objArr[0]);
        }
    }

    private static class renameSharedAccountAsUser extends MethodProxy {
        private renameSharedAccountAsUser() {
        }

        public String getMethodName() {
            return "renameSharedAccountAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Account account = (Account) objArr[0];
            String str = (String) objArr[1];
            ((Integer) objArr[2]).intValue();
            return method.invoke(obj, objArr);
        }
    }
}
