package com.lody.virtual.client.hook.proxies.wifi;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.WorkSource;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.base.ReplaceCallingPkgMethodProxy;
import com.lody.virtual.client.hook.base.StaticMethodProxy;
import com.lody.virtual.helper.utils.ArrayUtils;
import com.lody.virtual.helper.utils.Reflect;
import java.lang.reflect.Method;
import java.util.ArrayList;
import mirror.android.net.wifi.IWifiManager;

public class WifiManagerStub extends BinderInvocationProxy {

    private class RemoveWorkSourceMethodProxy extends StaticMethodProxy {
        RemoveWorkSourceMethodProxy(String str) {
            super(str);
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int indexOfFirst = ArrayUtils.indexOfFirst(objArr, WorkSource.class);
            if (indexOfFirst >= 0) {
                objArr[indexOfFirst] = null;
            }
            return super.call(obj, method, objArr);
        }
    }

    public WifiManagerStub() {
        super(IWifiManager.Stub.asInterface, "wifi");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new GetConnectionInfo());
        addMethodProxy(new GetScanResults());
        addMethodProxy(new ReplaceCallingPkgMethodProxy("getBatchedScanResults"));
        addMethodProxy(new RemoveWorkSourceMethodProxy("acquireWifiLock"));
        addMethodProxy(new RemoveWorkSourceMethodProxy("updateWifiLockWorkSource"));
        if (Build.VERSION.SDK_INT > 21) {
            addMethodProxy(new RemoveWorkSourceMethodProxy("startLocationRestrictedScan"));
        }
        if (Build.VERSION.SDK_INT >= 19) {
            addMethodProxy(new RemoveWorkSourceMethodProxy("startScan"));
            addMethodProxy(new RemoveWorkSourceMethodProxy("requestBatchedScan"));
        }
        addMethodProxy(new MethodProxy() {
            public String getMethodName() {
                return "getWifiEnabledState";
            }

            public Object call(Object obj, Method method, Object... objArr) {
                if (isFakeLocationEnable()) {
                    return 1;
                }
                return super.call(obj, method, objArr);
            }
        });
    }

    private final class GetConnectionInfo extends MethodProxy {
        private GetConnectionInfo() {
        }

        public String getMethodName() {
            return "getConnectionInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            WifiInfo wifiInfo = (WifiInfo) method.invoke(obj, objArr);
            if (wifiInfo != null) {
                mirror.android.net.wifi.WifiInfo.mMacAddress.set(wifiInfo, getDeviceInfo().wifiMac);
            }
            return wifiInfo;
        }
    }

    private final class GetScanResults extends ReplaceCallingPkgMethodProxy {
        public GetScanResults() {
            super("getScanResults");
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (isFakeLocationEnable()) {
                new ArrayList(0);
            }
            return super.call(obj, method, objArr);
        }
    }

    private static ScanResult cloneScanResult(Parcelable parcelable) {
        Parcel obtain = Parcel.obtain();
        parcelable.writeToParcel(obtain, 0);
        obtain.setDataPosition(0);
        ScanResult scanResult = (ScanResult) Reflect.on(parcelable).field("CREATOR").call("createFromParcel", obtain).get();
        obtain.recycle();
        return scanResult;
    }
}
