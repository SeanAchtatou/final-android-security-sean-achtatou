package com.lody.virtual.client.hook.secondary;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import java.io.FileDescriptor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

abstract class StubBinder implements IBinder {
    private IBinder mBase;
    private ClassLoader mClassLoader;
    private IInterface mInterface;

    public abstract InvocationHandler createHandler(Class<?> cls, IInterface iInterface);

    StubBinder(ClassLoader classLoader, IBinder iBinder) {
        this.mClassLoader = classLoader;
        this.mBase = iBinder;
    }

    public String getInterfaceDescriptor() {
        return this.mBase.getInterfaceDescriptor();
    }

    public boolean pingBinder() {
        return this.mBase.pingBinder();
    }

    public boolean isBinderAlive() {
        return this.mBase.isBinderAlive();
    }

    public IInterface queryLocalInterface(String str) {
        Class<?> cls;
        IInterface iInterface;
        if (this.mInterface == null) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace == null || stackTrace.length <= 1) {
                return null;
            }
            IInterface iInterface2 = null;
            Class<?> cls2 = null;
            for (StackTraceElement stackTraceElement : stackTrace) {
                if (!stackTraceElement.isNativeMethod()) {
                    try {
                        Method declaredMethod = this.mClassLoader.loadClass(stackTraceElement.getClassName()).getDeclaredMethod(stackTraceElement.getMethodName(), IBinder.class);
                        if ((declaredMethod.getModifiers() & 8) != 0) {
                            declaredMethod.setAccessible(true);
                            Class<?> returnType = declaredMethod.getReturnType();
                            if (returnType.isInterface() && IInterface.class.isAssignableFrom(returnType)) {
                                try {
                                    iInterface = (IInterface) declaredMethod.invoke(null, this.mBase);
                                    cls = returnType;
                                    IInterface iInterface3 = iInterface;
                                    cls2 = cls;
                                    iInterface2 = iInterface3;
                                } catch (Exception e2) {
                                    cls2 = returnType;
                                }
                            }
                        }
                        IInterface iInterface4 = iInterface2;
                        cls = cls2;
                        iInterface = iInterface4;
                        IInterface iInterface32 = iInterface;
                        cls2 = cls;
                        iInterface2 = iInterface32;
                    } catch (Exception e3) {
                    }
                }
            }
            if (cls2 == null || iInterface2 == null) {
                return null;
            }
            this.mInterface = (IInterface) Proxy.newProxyInstance(this.mClassLoader, new Class[]{cls2}, createHandler(cls2, iInterface2));
        }
        return this.mInterface;
    }

    public void dump(FileDescriptor fileDescriptor, String[] strArr) {
        this.mBase.dump(fileDescriptor, strArr);
    }

    public void dumpAsync(FileDescriptor fileDescriptor, String[] strArr) {
        this.mBase.dumpAsync(fileDescriptor, strArr);
    }

    public boolean transact(int i, Parcel parcel, Parcel parcel2, int i2) {
        return this.mBase.transact(i, parcel, parcel2, i2);
    }

    public void linkToDeath(IBinder.DeathRecipient deathRecipient, int i) {
        this.mBase.linkToDeath(deathRecipient, i);
    }

    public boolean unlinkToDeath(IBinder.DeathRecipient deathRecipient, int i) {
        return this.mBase.unlinkToDeath(deathRecipient, i);
    }
}
