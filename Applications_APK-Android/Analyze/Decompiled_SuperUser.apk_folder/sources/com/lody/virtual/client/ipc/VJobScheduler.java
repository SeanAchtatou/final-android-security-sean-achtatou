package com.lody.virtual.client.ipc;

import android.app.job.JobInfo;
import android.os.RemoteException;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.server.IJobScheduler;
import java.util.List;

public class VJobScheduler {
    private static final VJobScheduler sInstance = new VJobScheduler();
    private IJobScheduler mRemote;

    public static VJobScheduler get() {
        return sInstance;
    }

    public IJobScheduler getRemote() {
        if (this.mRemote == null || (!this.mRemote.asBinder().isBinderAlive() && !VirtualCore.get().isVAppProcess())) {
            synchronized (this) {
                this.mRemote = (IJobScheduler) LocalProxyUtils.genProxy(IJobScheduler.class, getRemoteInterface());
            }
        }
        return this.mRemote;
    }

    private Object getRemoteInterface() {
        return IJobScheduler.Stub.asInterface(ServiceManagerNative.getService(ServiceManagerNative.JOB));
    }

    public int schedule(JobInfo jobInfo) {
        try {
            return getRemote().schedule(jobInfo);
        } catch (RemoteException e2) {
            return ((Integer) VirtualRuntime.crash(e2)).intValue();
        }
    }

    public List<JobInfo> getAllPendingJobs() {
        try {
            return getRemote().getAllPendingJobs();
        } catch (RemoteException e2) {
            return (List) VirtualRuntime.crash(e2);
        }
    }

    public void cancelAll() {
        try {
            getRemote().cancelAll();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void cancel(int i) {
        try {
            getRemote().cancel(i);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }
}
