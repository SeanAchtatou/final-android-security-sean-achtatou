package com.lody.virtual.client;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Instrumentation;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.Binder;
import android.os.Build;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.client.IVClient;
import com.lody.virtual.client.core.CrashHandler;
import com.lody.virtual.client.core.InvocationStubManager;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.SpecialComponentList;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.client.fixer.ContextFixer;
import com.lody.virtual.client.hook.delegate.AppInstrumentation;
import com.lody.virtual.client.hook.providers.ProviderHook;
import com.lody.virtual.client.hook.proxies.am.HCallbackStub;
import com.lody.virtual.client.hook.secondary.ProxyServiceFactory;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.client.ipc.VDeviceManager;
import com.lody.virtual.client.ipc.VPackageManager;
import com.lody.virtual.client.ipc.VirtualStorageManager;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.compat.BuildCompat;
import com.lody.virtual.helper.compat.StorageManagerCompat;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.InstalledAppInfo;
import com.lody.virtual.remote.PendingResultData;
import com.lody.virtual.remote.VDeviceInfo;
import com.taobao.android.dex.interpret.ARTUtils;
import com.taobao.android.runtime.DalvikUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import mirror.android.app.ActivityThread;
import mirror.android.app.ActivityThreadNMR1;
import mirror.android.app.ContextImpl;
import mirror.android.app.IActivityManager;
import mirror.android.app.LoadedApk;
import mirror.android.content.ContentProviderHolderOreo;
import mirror.android.providers.Settings;
import mirror.android.renderscript.RenderScriptCacheDir;
import mirror.android.view.HardwareRenderer;
import mirror.android.view.RenderScript;
import mirror.android.view.ThreadedRenderer;
import mirror.com.android.internal.content.ReferrerIntent;
import mirror.dalvik.system.VMRuntime;
import mirror.java.lang.ThreadGroup;
import mirror.java.lang.ThreadGroupN;

public final class VClientImpl extends IVClient.Stub {
    private static final int NEW_INTENT = 11;
    private static final int RECEIVER = 12;
    private static final String TAG = VClientImpl.class.getSimpleName();
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static final VClientImpl gClient = new VClientImpl();
    /* access modifiers changed from: private */
    public CrashHandler crashHandler;
    private VDeviceInfo deviceInfo;
    private AppBindData mBoundApplication;
    private final H mH = new H();
    private Application mInitialApplication;
    private Instrumentation mInstrumentation = AppInstrumentation.getDefault();
    private ConditionVariable mTempLock;
    private IBinder token;
    private int vuid;

    public static VClientImpl get() {
        return gClient;
    }

    public boolean isBound() {
        return this.mBoundApplication != null;
    }

    public VDeviceInfo getDeviceInfo() {
        return this.deviceInfo;
    }

    public Application getCurrentApplication() {
        return this.mInitialApplication;
    }

    public String getCurrentPackage() {
        if (this.mBoundApplication != null) {
            return this.mBoundApplication.appInfo.packageName;
        }
        return VPackageManager.get().getNameForUid(getVUid());
    }

    public ApplicationInfo getCurrentApplicationInfo() {
        if (this.mBoundApplication != null) {
            return this.mBoundApplication.appInfo;
        }
        return null;
    }

    public CrashHandler getCrashHandler() {
        return this.crashHandler;
    }

    public void setCrashHandler(CrashHandler crashHandler2) {
        this.crashHandler = crashHandler2;
    }

    public int getVUid() {
        return this.vuid;
    }

    public int getBaseVUid() {
        return VUserHandle.getAppId(this.vuid);
    }

    public ClassLoader getClassLoader(ApplicationInfo applicationInfo) {
        return createPackageContext(applicationInfo.packageName).getClassLoader();
    }

    private void sendMessage(int i, Object obj) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        this.mH.sendMessage(obtain);
    }

    public IBinder getAppThread() {
        return ActivityThread.getApplicationThread.call(VirtualCore.mainThread(), new Object[0]);
    }

    public IBinder getToken() {
        return this.token;
    }

    public void initProcess(IBinder iBinder, int i) {
        this.token = iBinder;
        this.vuid = i;
        this.deviceInfo = VDeviceManager.get().getDeviceInfo(VUserHandle.getUserId(i));
    }

    /* access modifiers changed from: private */
    public void handleNewIntent(NewIntentData newIntentData) {
        Intent intent;
        if (Build.VERSION.SDK_INT >= 22) {
            intent = ReferrerIntent.ctor.newInstance(newIntentData.intent, newIntentData.creator);
        } else {
            intent = newIntentData.intent;
        }
        if (ActivityThread.performNewIntents != null) {
            ActivityThread.performNewIntents.call(VirtualCore.mainThread(), newIntentData.token, Collections.singletonList(intent));
            return;
        }
        ActivityThreadNMR1.performNewIntents.call(VirtualCore.mainThread(), newIntentData.token, Collections.singletonList(intent), true);
    }

    public void bindApplication(final String str, final String str2) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            bindApplicationNoCheck(str, str2, new ConditionVariable());
            return;
        }
        final ConditionVariable conditionVariable = new ConditionVariable();
        VirtualRuntime.getUIHandler().post(new Runnable() {
            public void run() {
                VClientImpl.this.bindApplicationNoCheck(str, str2, conditionVariable);
                conditionVariable.open();
            }
        });
        conditionVariable.block();
    }

    /* access modifiers changed from: private */
    public void bindApplicationNoCheck(String str, String str2, ConditionVariable conditionVariable) {
        File cacheDir;
        if (str2 == null) {
            str2 = str;
        }
        this.mTempLock = conditionVariable;
        try {
            setupUncaughtHandler();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        try {
            fixInstalledProviders();
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
        ActivityThread.mInitialApplication.set(VirtualCore.mainThread(), null);
        AppBindData appBindData = new AppBindData();
        InstalledAppInfo installedAppInfo = VirtualCore.get().getInstalledAppInfo(str, 0);
        if (installedAppInfo == null) {
            new Exception("App not exist!").printStackTrace();
            Process.killProcess(0);
            System.exit(0);
        }
        if (!installedAppInfo.dependSystem && installedAppInfo.skipDexOpt) {
            VLog.d(TAG, "Dex opt skipped.", new Object[0]);
            if (VirtualRuntime.isArt()) {
                ARTUtils.a(VirtualCore.get().getContext());
                ARTUtils.a(false);
            } else {
                DalvikUtils.a();
                DalvikUtils.a(1);
            }
        }
        appBindData.appInfo = VPackageManager.get().getApplicationInfo(str, 0, VUserHandle.getUserId(this.vuid));
        appBindData.processName = str2;
        appBindData.providers = VPackageManager.get().queryContentProviders(str2, getVUid(), FileUtils.FileMode.MODE_IWUSR);
        Log.i(TAG, "Binding application " + appBindData.appInfo.packageName + " (" + appBindData.processName + ")");
        this.mBoundApplication = appBindData;
        VirtualRuntime.setupRuntime(appBindData.processName, appBindData.appInfo);
        int i = appBindData.appInfo.targetSdkVersion;
        if (i < 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(StrictMode.getThreadPolicy()).permitNetwork().build());
        }
        if (Build.VERSION.SDK_INT >= 24 && mirror.android.os.StrictMode.sVmPolicyMask != null) {
            mirror.android.os.StrictMode.sVmPolicyMask.set(0);
        }
        if (Build.VERSION.SDK_INT >= 21 && i < 21) {
            mirror.android.os.Message.updateCheckRecycle.call(Integer.valueOf(i));
        }
        if (VASettings.ENABLE_IO_REDIRECT) {
            startIOUniformer();
        }
        NativeEngine.hookNative();
        Object mainThread = VirtualCore.mainThread();
        NativeEngine.startDexOverride();
        Context createPackageContext = createPackageContext(appBindData.appInfo.packageName);
        System.setProperty("java.io.tmpdir", createPackageContext.getCacheDir().getAbsolutePath());
        if (Build.VERSION.SDK_INT >= 23) {
            cacheDir = createPackageContext.getCodeCacheDir();
        } else {
            cacheDir = createPackageContext.getCacheDir();
        }
        if (Build.VERSION.SDK_INT < 24) {
            if (HardwareRenderer.setupDiskCache != null) {
                HardwareRenderer.setupDiskCache.call(cacheDir);
            }
        } else if (ThreadedRenderer.setupDiskCache != null) {
            ThreadedRenderer.setupDiskCache.call(cacheDir);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (RenderScriptCacheDir.setupDiskCache != null) {
                RenderScriptCacheDir.setupDiskCache.call(cacheDir);
            }
        } else if (Build.VERSION.SDK_INT >= 16 && RenderScript.setupDiskCache != null) {
            RenderScript.setupDiskCache.call(cacheDir);
        }
        Object fixBoundApp = fixBoundApp(this.mBoundApplication);
        this.mBoundApplication.info = ContextImpl.mPackageInfo.get(createPackageContext);
        ActivityThread.AppBindData.info.set(fixBoundApp, appBindData.info);
        VMRuntime.setTargetSdkVersion.call(VMRuntime.getRuntime.call(new Object[0]), Integer.valueOf(appBindData.appInfo.targetSdkVersion));
        boolean isConflictingInstrumentation = SpecialComponentList.isConflictingInstrumentation(str);
        if (!isConflictingInstrumentation) {
            InvocationStubManager.getInstance().checkEnv(AppInstrumentation.class);
        }
        this.mInitialApplication = LoadedApk.makeApplication.call(appBindData.info, false, null);
        ActivityThread.mInitialApplication.set(mainThread, this.mInitialApplication);
        ContextFixer.fixContext(this.mInitialApplication);
        if (appBindData.providers != null) {
            installContentProviders(this.mInitialApplication, appBindData.providers);
        }
        if (conditionVariable != null) {
            conditionVariable.open();
            this.mTempLock = null;
        }
        try {
            this.mInstrumentation.callApplicationOnCreate(this.mInitialApplication);
            InvocationStubManager.getInstance().checkEnv(HCallbackStub.class);
            if (isConflictingInstrumentation) {
                InvocationStubManager.getInstance().checkEnv(AppInstrumentation.class);
            }
            Application application = ActivityThread.mInitialApplication.get(mainThread);
            if (application != null) {
                this.mInitialApplication = application;
            }
        } catch (Exception e2) {
            if (!this.mInstrumentation.onException(this.mInitialApplication, e2)) {
                throw new RuntimeException("Unable to create application " + this.mInitialApplication.getClass().getName() + ": " + e2.toString(), e2);
            }
        }
        VActivityManager.get().appDoneExecuting();
    }

    private void setupUncaughtHandler() {
        ThreadGroup threadGroup;
        ThreadGroup threadGroup2 = Thread.currentThread().getThreadGroup();
        while (true) {
            threadGroup = threadGroup2;
            if (threadGroup.getParent() == null) {
                break;
            }
            threadGroup2 = threadGroup.getParent();
        }
        RootThreadGroup rootThreadGroup = new RootThreadGroup(threadGroup);
        if (Build.VERSION.SDK_INT < 24) {
            List list = ThreadGroup.groups.get(threadGroup);
            synchronized (list) {
                ArrayList<ThreadGroup> arrayList = new ArrayList<>(list);
                arrayList.remove(rootThreadGroup);
                ThreadGroup.groups.set(rootThreadGroup, arrayList);
                list.clear();
                list.add(rootThreadGroup);
                ThreadGroup.groups.set(threadGroup, list);
                for (ThreadGroup threadGroup3 : arrayList) {
                    ThreadGroup.parent.set(threadGroup3, rootThreadGroup);
                }
            }
            return;
        }
        ThreadGroup[] threadGroupArr = ThreadGroupN.groups.get(threadGroup);
        synchronized (threadGroupArr) {
            ThreadGroup[] threadGroupArr2 = (ThreadGroup[]) threadGroupArr.clone();
            ThreadGroupN.groups.set(rootThreadGroup, threadGroupArr2);
            ThreadGroupN.groups.set(threadGroup, new ThreadGroup[]{rootThreadGroup});
            for (ThreadGroup threadGroup4 : threadGroupArr2) {
                ThreadGroupN.parent.set(threadGroup4, rootThreadGroup);
            }
            ThreadGroupN.ngroups.set(threadGroup, 1);
        }
    }

    @SuppressLint({"SdCardPath"})
    private void startIOUniformer() {
        ApplicationInfo applicationInfo = this.mBoundApplication.appInfo;
        int myUserId = VUserHandle.myUserId();
        String path = this.deviceInfo.getWifiFile(myUserId).getPath();
        NativeEngine.redirectDirectory("/sys/class/net/wlan0/address", path);
        NativeEngine.redirectDirectory("/sys/class/net/eth0/address", path);
        NativeEngine.redirectDirectory("/sys/class/net/wifi/address", path);
        NativeEngine.redirectDirectory("/data/data/" + applicationInfo.packageName, applicationInfo.dataDir);
        NativeEngine.redirectDirectory("/data/user/0/" + applicationInfo.packageName, applicationInfo.dataDir);
        if (Build.VERSION.SDK_INT >= 24) {
            NativeEngine.redirectDirectory("/data/user_de/0/" + applicationInfo.packageName, applicationInfo.dataDir);
        }
        String absolutePath = new File(VEnvironment.getDataAppPackageDirectory(applicationInfo.packageName), "lib").getAbsolutePath();
        NativeEngine.redirectDirectory(new File(VEnvironment.getUserSystemDirectory(myUserId), applicationInfo.packageName + "/lib").getAbsolutePath(), absolutePath);
        NativeEngine.redirectDirectory("/data/data/" + applicationInfo.packageName + "/lib/", absolutePath);
        NativeEngine.redirectDirectory("/data/user/0/" + applicationInfo.packageName + "/lib/", absolutePath);
        NativeEngine.readOnly(VEnvironment.getDataAppDirectory().getPath());
        VirtualStorageManager virtualStorageManager = VirtualStorageManager.get();
        String virtualStorage = virtualStorageManager.getVirtualStorage(applicationInfo.packageName, myUserId);
        if (virtualStorageManager.isVirtualStorageEnable(applicationInfo.packageName, myUserId) && virtualStorage != null) {
            File file = new File(virtualStorage);
            if (file.exists() || file.mkdirs()) {
                Iterator<String> it = getMountPoints().iterator();
                while (it.hasNext()) {
                    NativeEngine.redirectDirectory(it.next(), virtualStorage);
                }
            }
        }
        NativeEngine.hook();
    }

    @SuppressLint({"SdCardPath"})
    private HashSet<String> getMountPoints() {
        HashSet<String> hashSet = new HashSet<>(3);
        hashSet.add("/mnt/sdcard/");
        hashSet.add("/sdcard/");
        String[] allPoints = StorageManagerCompat.getAllPoints(VirtualCore.get().getContext());
        if (allPoints != null) {
            Collections.addAll(hashSet, allPoints);
        }
        return hashSet;
    }

    private Context createPackageContext(String str) {
        try {
            return VirtualCore.get().getContext().createPackageContext(str, 3);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            VirtualRuntime.crash(new RemoteException());
            throw new RuntimeException();
        }
    }

    private Object fixBoundApp(AppBindData appBindData) {
        Object obj = ActivityThread.mBoundApplication.get(VirtualCore.mainThread());
        ActivityThread.AppBindData.appInfo.set(obj, appBindData.appInfo);
        ActivityThread.AppBindData.processName.set(obj, appBindData.processName);
        ActivityThread.AppBindData.instrumentationName.set(obj, new ComponentName(appBindData.appInfo.packageName, Instrumentation.class.getName()));
        ActivityThread.AppBindData.providers.set(obj, appBindData.providers);
        return obj;
    }

    private void installContentProviders(Context context, List<ProviderInfo> list) {
        long clearCallingIdentity = Binder.clearCallingIdentity();
        Object mainThread = VirtualCore.mainThread();
        try {
            for (ProviderInfo next : list) {
                if (next.enabled) {
                    ActivityThread.installProvider(mainThread, context, next, null);
                }
            }
        } finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }

    public IBinder acquireProviderClient(ProviderInfo providerInfo) {
        ContentProviderClient contentProviderClient;
        IInterface iInterface;
        ContentProviderClient acquireContentProviderClient;
        if (this.mTempLock != null) {
            this.mTempLock.block();
        }
        if (!isBound()) {
            get().bindApplication(providerInfo.packageName, providerInfo.processName);
        }
        String[] split = providerInfo.authority.split(";");
        String str = split.length == 0 ? providerInfo.authority : split[0];
        ContentResolver contentResolver = VirtualCore.get().getContext().getContentResolver();
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                acquireContentProviderClient = contentResolver.acquireUnstableContentProviderClient(str);
            } else {
                acquireContentProviderClient = contentResolver.acquireContentProviderClient(str);
            }
            contentProviderClient = acquireContentProviderClient;
        } catch (Throwable th) {
            th.printStackTrace();
            contentProviderClient = null;
        }
        if (contentProviderClient != null) {
            iInterface = mirror.android.content.ContentProviderClient.mContentProvider.get(contentProviderClient);
            contentProviderClient.release();
        } else {
            iInterface = null;
        }
        if (iInterface != null) {
            return iInterface.asBinder();
        }
        return null;
    }

    private void fixInstalledProviders() {
        clearSettingProvider();
        for (Object next : ActivityThread.mProviderMap.get(VirtualCore.mainThread()).values()) {
            if (BuildCompat.isOreo()) {
                IInterface iInterface = ActivityThread.ProviderClientRecordJB.mProvider.get(next);
                Object obj = ActivityThread.ProviderClientRecordJB.mHolder.get(next);
                if (obj != null) {
                    ProviderInfo providerInfo = ContentProviderHolderOreo.info.get(obj);
                    if (!providerInfo.authority.startsWith(VASettings.STUB_CP_AUTHORITY)) {
                        IInterface createProxy = ProviderHook.createProxy(true, providerInfo.authority, iInterface);
                        ActivityThread.ProviderClientRecordJB.mProvider.set(next, createProxy);
                        ContentProviderHolderOreo.provider.set(obj, createProxy);
                    }
                }
            } else if (Build.VERSION.SDK_INT >= 16) {
                IInterface iInterface2 = ActivityThread.ProviderClientRecordJB.mProvider.get(next);
                Object obj2 = ActivityThread.ProviderClientRecordJB.mHolder.get(next);
                if (obj2 != null) {
                    ProviderInfo providerInfo2 = IActivityManager.ContentProviderHolder.info.get(obj2);
                    if (!providerInfo2.authority.startsWith(VASettings.STUB_CP_AUTHORITY)) {
                        IInterface createProxy2 = ProviderHook.createProxy(true, providerInfo2.authority, iInterface2);
                        ActivityThread.ProviderClientRecordJB.mProvider.set(next, createProxy2);
                        IActivityManager.ContentProviderHolder.provider.set(obj2, createProxy2);
                    }
                }
            } else {
                String str = ActivityThread.ProviderClientRecord.mName.get(next);
                IInterface iInterface3 = ActivityThread.ProviderClientRecord.mProvider.get(next);
                if (iInterface3 != null && !str.startsWith(VASettings.STUB_CP_AUTHORITY)) {
                    ActivityThread.ProviderClientRecord.mProvider.set(next, ProviderHook.createProxy(true, str, iInterface3));
                }
            }
        }
    }

    private void clearSettingProvider() {
        Object obj;
        Object obj2 = Settings.System.sNameValueCache.get();
        if (obj2 != null) {
            clearContentProvider(obj2);
        }
        Object obj3 = Settings.Secure.sNameValueCache.get();
        if (obj3 != null) {
            clearContentProvider(obj3);
        }
        if (Build.VERSION.SDK_INT >= 17 && Settings.Global.TYPE != null && (obj = Settings.Global.sNameValueCache.get()) != null) {
            clearContentProvider(obj);
        }
    }

    private static void clearContentProvider(Object obj) {
        if (BuildCompat.isOreo()) {
            Object obj2 = Settings.NameValueCacheOreo.mProviderHolder.get(obj);
            if (obj2 != null) {
                Settings.ContentProviderHolder.mContentProvider.set(obj2, null);
                return;
            }
            return;
        }
        Settings.NameValueCache.mContentProvider.set(obj, null);
    }

    public void finishActivity(IBinder iBinder) {
        VActivityManager.get().finishActivity(iBinder);
    }

    public void scheduleNewIntent(String str, IBinder iBinder, Intent intent) {
        NewIntentData newIntentData = new NewIntentData();
        newIntentData.creator = str;
        newIntentData.token = iBinder;
        newIntentData.intent = intent;
        sendMessage(11, newIntentData);
    }

    public void scheduleReceiver(String str, ComponentName componentName, Intent intent, PendingResultData pendingResultData) {
        ReceiverData receiverData = new ReceiverData();
        receiverData.resultData = pendingResultData;
        receiverData.intent = intent;
        receiverData.component = componentName;
        receiverData.processName = str;
        sendMessage(12, receiverData);
    }

    /* access modifiers changed from: private */
    public void handleReceiver(ReceiverData receiverData) {
        BroadcastReceiver.PendingResult build = receiverData.resultData.build();
        try {
            if (!isBound()) {
                bindApplication(receiverData.component.getPackageName(), receiverData.processName);
            }
            Context baseContext = this.mInitialApplication.getBaseContext();
            BroadcastReceiver broadcastReceiver = (BroadcastReceiver) baseContext.getClassLoader().loadClass(receiverData.component.getClassName()).newInstance();
            mirror.android.content.BroadcastReceiver.setPendingResult.call(broadcastReceiver, build);
            receiverData.intent.setExtrasClassLoader(baseContext.getClassLoader());
            broadcastReceiver.onReceive(ContextImpl.getReceiverRestrictedContext.call(baseContext, new Object[0]), receiverData.intent);
            if (mirror.android.content.BroadcastReceiver.getPendingResult.call(broadcastReceiver, new Object[0]) != null) {
                build.finish();
            }
            VActivityManager.get().broadcastFinish(receiverData.resultData);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new RuntimeException("Unable to start receiver " + receiverData.component + ": " + e2.toString(), e2);
        }
    }

    public IBinder createProxyService(ComponentName componentName, IBinder iBinder) {
        return ProxyServiceFactory.getProxyService(getCurrentApplication(), componentName, iBinder);
    }

    public String getDebugInfo() {
        return "process : " + VirtualRuntime.getProcessName() + ShellUtils.COMMAND_LINE_END + "initialPkg : " + VirtualRuntime.getInitialPackageName() + ShellUtils.COMMAND_LINE_END + "vuid : " + this.vuid;
    }

    private static class RootThreadGroup extends ThreadGroup {
        RootThreadGroup(ThreadGroup threadGroup) {
            super(threadGroup, "VA-Root");
        }

        public void uncaughtException(Thread thread, Throwable th) {
            CrashHandler access$600 = VClientImpl.gClient.crashHandler;
            if (access$600 != null) {
                access$600.handleUncaughtException(thread, th);
                return;
            }
            VLog.e("uncaught", th);
            System.exit(0);
        }
    }

    private final class NewIntentData {
        String creator;
        Intent intent;
        IBinder token;

        private NewIntentData() {
        }
    }

    private final class AppBindData {
        ApplicationInfo appInfo;
        Object info;
        String processName;
        List<ProviderInfo> providers;

        private AppBindData() {
        }
    }

    private final class ReceiverData {
        ComponentName component;
        Intent intent;
        String processName;
        PendingResultData resultData;

        private ReceiverData() {
        }
    }

    private class H extends Handler {
        private H() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 11:
                    VClientImpl.this.handleNewIntent((NewIntentData) message.obj);
                    return;
                case 12:
                    VClientImpl.this.handleReceiver((ReceiverData) message.obj);
                    return;
                default:
                    return;
            }
        }
    }
}
