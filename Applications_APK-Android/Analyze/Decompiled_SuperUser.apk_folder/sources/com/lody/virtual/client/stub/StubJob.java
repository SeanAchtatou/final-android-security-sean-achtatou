package com.lody.virtual.client.stub;

import android.annotation.TargetApi;
import android.app.Service;
import android.app.job.IJobCallback;
import android.app.job.IJobService;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.lody.virtual.client.core.InvocationStubManager;
import com.lody.virtual.client.hook.proxies.am.ActivityManagerStub;
import com.lody.virtual.helper.collection.SparseArray;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.server.job.VJobSchedulerService;
import java.util.Map;

@TargetApi(21)
public class StubJob extends Service {
    /* access modifiers changed from: private */
    public static final String TAG = StubJob.class.getSimpleName();
    /* access modifiers changed from: private */
    public final SparseArray<JobSession> mJobSessions = new SparseArray<>();
    /* access modifiers changed from: private */
    public JobScheduler mScheduler;
    private final IJobService mService = new IJobService.Stub() {
        public void startJob(JobParameters jobParameters) {
            boolean z;
            int jobId = jobParameters.getJobId();
            IJobCallback asInterface = IJobCallback.Stub.asInterface(mirror.android.app.job.JobParameters.callback.get(jobParameters));
            Map.Entry<VJobSchedulerService.JobId, VJobSchedulerService.JobConfig> findJobByVirtualJobId = VJobSchedulerService.get().findJobByVirtualJobId(jobId);
            if (findJobByVirtualJobId == null) {
                StubJob.this.emptyCallback(asInterface, jobId);
                StubJob.this.mScheduler.cancel(jobId);
                return;
            }
            VJobSchedulerService.JobId key = findJobByVirtualJobId.getKey();
            VJobSchedulerService.JobConfig value = findJobByVirtualJobId.getValue();
            synchronized (StubJob.this.mJobSessions) {
                if (((JobSession) StubJob.this.mJobSessions.get(jobId)) != null) {
                    StubJob.this.emptyCallback(asInterface, jobId);
                } else {
                    JobSession jobSession = new JobSession(jobId, asInterface, jobParameters);
                    mirror.android.app.job.JobParameters.callback.set(jobParameters, jobSession.asBinder());
                    mirror.android.app.job.JobParameters.jobId.set(jobParameters, key.clientJobId);
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(key.packageName, value.serviceName));
                    intent.putExtra("_VA_|_user_id_", VUserHandle.getUserId(key.vuid));
                    try {
                        z = StubJob.this.bindService(intent, jobSession, 0);
                    } catch (Throwable th) {
                        VLog.e(StubJob.TAG, th);
                        z = false;
                    }
                    if (z) {
                        StubJob.this.mJobSessions.put(jobId, jobSession);
                    } else {
                        StubJob.this.emptyCallback(asInterface, jobId);
                        StubJob.this.mScheduler.cancel(jobId);
                        VJobSchedulerService.get().cancel(jobId);
                    }
                }
            }
        }

        public void stopJob(JobParameters jobParameters) {
            int jobId = jobParameters.getJobId();
            synchronized (StubJob.this.mJobSessions) {
                JobSession jobSession = (JobSession) StubJob.this.mJobSessions.get(jobId);
                if (jobSession != null) {
                    jobSession.stopSession();
                }
            }
        }
    };

    /* access modifiers changed from: private */
    public void emptyCallback(IJobCallback iJobCallback, int i) {
        try {
            iJobCallback.acknowledgeStartMessage(i, false);
            iJobCallback.jobFinished(i, false);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void onCreate() {
        super.onCreate();
        InvocationStubManager.getInstance().checkEnv(ActivityManagerStub.class);
        this.mScheduler = (JobScheduler) getSystemService("jobscheduler");
    }

    public IBinder onBind(Intent intent) {
        return this.mService.asBinder();
    }

    private final class JobSession extends IJobCallback.Stub implements ServiceConnection {
        private IJobCallback clientCallback;
        private IJobService clientJobService;
        private int jobId;
        private JobParameters jobParams;

        JobSession(int i, IJobCallback iJobCallback, JobParameters jobParameters) {
            this.jobId = i;
            this.clientCallback = iJobCallback;
            this.jobParams = jobParameters;
        }

        public void acknowledgeStartMessage(int i, boolean z) {
            this.clientCallback.acknowledgeStartMessage(i, z);
        }

        public void acknowledgeStopMessage(int i, boolean z) {
            this.clientCallback.acknowledgeStopMessage(i, z);
        }

        public void jobFinished(int i, boolean z) {
            this.clientCallback.jobFinished(i, z);
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.clientJobService = IJobService.Stub.asInterface(iBinder);
            if (this.clientJobService == null) {
                StubJob.this.emptyCallback(this.clientCallback, this.jobId);
                stopSession();
                return;
            }
            try {
                this.clientJobService.startJob(this.jobParams);
            } catch (RemoteException e2) {
                forceFinishJob();
                e2.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }

        /* access modifiers changed from: package-private */
        public void forceFinishJob() {
            try {
                this.clientCallback.jobFinished(this.jobId, false);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            } finally {
                stopSession();
            }
        }

        /* access modifiers changed from: package-private */
        public void stopSession() {
            if (this.clientJobService != null) {
                try {
                    this.clientJobService.stopJob(this.jobParams);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            StubJob.this.mJobSessions.remove(this.jobId);
            StubJob.this.unbindService(this);
        }
    }
}
