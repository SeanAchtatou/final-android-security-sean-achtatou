package com.lody.virtual.client.hook.proxies.mount;

import android.os.Build;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.utils.MethodParameterUtils;
import java.io.File;
import java.lang.reflect.Method;

class MethodProxies {
    MethodProxies() {
    }

    static class GetVolumeList extends MethodProxy {
        GetVolumeList() {
        }

        public String getMethodName() {
            return "getVolumeList";
        }

        public boolean beforeCall(Object obj, Method method, Object... objArr) {
            if (objArr == null || objArr.length == 0) {
                return super.beforeCall(obj, method, objArr);
            }
            if (objArr[0] instanceof Integer) {
                objArr[0] = Integer.valueOf(getRealUid());
            }
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return super.beforeCall(obj, method, objArr);
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            return obj2;
        }
    }

    static class Mkdirs extends MethodProxy {
        Mkdirs() {
        }

        public String getMethodName() {
            return "mkdirs";
        }

        public boolean beforeCall(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return super.beforeCall(obj, method, objArr);
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str;
            if (Build.VERSION.SDK_INT < 19) {
                return super.call(obj, method, objArr);
            }
            if (objArr.length == 1) {
                str = (String) objArr[0];
            } else {
                str = (String) objArr[1];
            }
            File file = new File(str);
            if (file.exists() || file.mkdirs()) {
                return 0;
            }
            return -1;
        }
    }
}
