package com.lody.virtual.client.interfaces;

public interface IInjector {
    void inject();

    boolean isEnvBad();
}
