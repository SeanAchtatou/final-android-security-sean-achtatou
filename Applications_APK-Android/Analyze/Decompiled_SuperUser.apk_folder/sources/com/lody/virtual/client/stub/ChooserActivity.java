package com.lody.virtual.client.stub;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import com.lody.virtual.R;
import com.lody.virtual.client.env.Constants;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VUserHandle;

public class ChooserActivity extends ResolverActivity {
    public static final String ACTION = Intent.createChooser(new Intent(), "").getAction();
    public static final String EXTRA_DATA = "android.intent.extra.virtual.data";
    public static final String EXTRA_REQUEST_CODE = "android.intent.extra.virtual.request_code";
    public static final String EXTRA_WHO = "android.intent.extra.virtual.who";

    public static boolean check(Intent intent) {
        try {
            if (TextUtils.equals(ACTION, intent.getAction()) || TextUtils.equals("android.intent.action.CHOOSER", intent.getAction())) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"MissingSuperCall"})
    public void onCreate(Bundle bundle) {
        Intent[] intentArr;
        Intent intent = getIntent();
        int intExtra = intent.getIntExtra(Constants.EXTRA_USER_HANDLE, VUserHandle.getCallingUserId());
        this.mOptions = (Bundle) intent.getParcelableExtra(EXTRA_DATA);
        this.mResultWho = intent.getStringExtra(EXTRA_WHO);
        this.mRequestCode = intent.getIntExtra(EXTRA_REQUEST_CODE, 0);
        Parcelable parcelableExtra = intent.getParcelableExtra("android.intent.extra.INTENT");
        if (!(parcelableExtra instanceof Intent)) {
            VLog.w("ChooseActivity", "Target is not an intent: " + parcelableExtra, new Object[0]);
            finish();
            return;
        }
        Intent intent2 = (Intent) parcelableExtra;
        CharSequence charSequenceExtra = intent.getCharSequenceExtra("android.intent.extra.TITLE");
        if (charSequenceExtra == null) {
            charSequenceExtra = getString(R.string.g9);
        }
        Parcelable[] parcelableArrayExtra = intent.getParcelableArrayExtra("android.intent.extra.INITIAL_INTENTS");
        if (parcelableArrayExtra != null) {
            intentArr = new Intent[parcelableArrayExtra.length];
            for (int i = 0; i < parcelableArrayExtra.length; i++) {
                if (!(parcelableArrayExtra[i] instanceof Intent)) {
                    VLog.w("ChooseActivity", "Initial intent #" + i + " not an Intent: " + parcelableArrayExtra[i], new Object[0]);
                    finish();
                    return;
                }
                intentArr[i] = (Intent) parcelableArrayExtra[i];
            }
        } else {
            intentArr = null;
        }
        super.onCreate(bundle, intent2, charSequenceExtra, intentArr, null, false, intExtra);
    }
}
