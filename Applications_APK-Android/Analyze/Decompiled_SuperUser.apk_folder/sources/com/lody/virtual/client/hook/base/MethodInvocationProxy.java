package com.lody.virtual.client.hook.base;

import android.content.Context;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.MethodInvocationStub;
import com.lody.virtual.client.interfaces.IInjector;
import java.lang.reflect.Modifier;

public abstract class MethodInvocationProxy<T extends MethodInvocationStub> implements IInjector {
    protected T mInvocationStub;

    public abstract void inject();

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public MethodInvocationProxy(T r3) {
        /*
            r2 = this;
            r2.<init>()
            r2.mInvocationStub = r3
            r2.onBindMethods()
            r2.afterHookApply(r3)
            java.lang.Class r0 = r2.getClass()
            java.lang.Class<com.lody.virtual.client.hook.base.LogInvocation> r1 = com.lody.virtual.client.hook.base.LogInvocation.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.lody.virtual.client.hook.base.LogInvocation r0 = (com.lody.virtual.client.hook.base.LogInvocation) r0
            if (r0 == 0) goto L_0x0020
            com.lody.virtual.client.hook.base.LogInvocation$Condition r0 = r0.value()
            r3.setInvocationLoggingCondition(r0)
        L_0x0020:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.client.hook.base.MethodInvocationProxy.<init>(com.lody.virtual.client.hook.base.MethodInvocationStub):void");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        Inject inject;
        if (this.mInvocationStub != null && (inject = (Inject) getClass().getAnnotation(Inject.class)) != null) {
            for (Class<?> cls : inject.value().getDeclaredClasses()) {
                if (!Modifier.isAbstract(cls.getModifiers()) && MethodProxy.class.isAssignableFrom(cls) && cls.getAnnotation(SkipInject.class) == null) {
                    addMethodProxy(cls);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void addMethodProxy(java.lang.Class<?> r5) {
        /*
            r4 = this;
            java.lang.reflect.Constructor[] r0 = r5.getDeclaredConstructors()     // Catch:{ Throwable -> 0x0034 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ Throwable -> 0x0034 }
            boolean r1 = r0.isAccessible()     // Catch:{ Throwable -> 0x0034 }
            if (r1 != 0) goto L_0x0011
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ Throwable -> 0x0034 }
        L_0x0011:
            java.lang.Class[] r1 = r0.getParameterTypes()     // Catch:{ Throwable -> 0x0034 }
            int r1 = r1.length     // Catch:{ Throwable -> 0x0034 }
            if (r1 != 0) goto L_0x0027
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0034 }
            java.lang.Object r0 = r0.newInstance(r1)     // Catch:{ Throwable -> 0x0034 }
            com.lody.virtual.client.hook.base.MethodProxy r0 = (com.lody.virtual.client.hook.base.MethodProxy) r0     // Catch:{ Throwable -> 0x0034 }
        L_0x0021:
            T r1 = r4.mInvocationStub     // Catch:{ Throwable -> 0x0034 }
            r1.addMethodProxy(r0)     // Catch:{ Throwable -> 0x0034 }
            return
        L_0x0027:
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0034 }
            r2 = 0
            r1[r2] = r4     // Catch:{ Throwable -> 0x0034 }
            java.lang.Object r0 = r0.newInstance(r1)     // Catch:{ Throwable -> 0x0034 }
            com.lody.virtual.client.hook.base.MethodProxy r0 = (com.lody.virtual.client.hook.base.MethodProxy) r0     // Catch:{ Throwable -> 0x0034 }
            goto L_0x0021
        L_0x0034:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unable to instance Hook : "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = " : "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.client.hook.base.MethodInvocationProxy.addMethodProxy(java.lang.Class):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.lody.virtual.client.hook.base.MethodProxy addMethodProxy(com.lody.virtual.client.hook.base.MethodProxy r2) {
        /*
            r1 = this;
            T r0 = r1.mInvocationStub
            com.lody.virtual.client.hook.base.MethodProxy r0 = r0.addMethodProxy(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.client.hook.base.MethodInvocationProxy.addMethodProxy(com.lody.virtual.client.hook.base.MethodProxy):com.lody.virtual.client.hook.base.MethodProxy");
    }

    /* access modifiers changed from: protected */
    public void afterHookApply(T t) {
    }

    public Context getContext() {
        return VirtualCore.get().getContext();
    }

    public T getInvocationStub() {
        return this.mInvocationStub;
    }
}
