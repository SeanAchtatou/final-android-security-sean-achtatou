package com.lody.virtual.client.hook.proxies.appops;

import android.annotation.TargetApi;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.base.ReplaceLastPkgMethodProxy;
import com.lody.virtual.client.hook.base.StaticMethodProxy;
import java.lang.reflect.Method;
import mirror.com.android.internal.app.IAppOpsService;

@TargetApi(19)
public class AppOpsManagerStub extends BinderInvocationProxy {
    public AppOpsManagerStub() {
        super(IAppOpsService.Stub.asInterface, "appops");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new BaseMethodProxy("checkOperation", 1, 2));
        addMethodProxy(new BaseMethodProxy("noteOperation", 1, 2));
        addMethodProxy(new BaseMethodProxy("startOperation", 2, 3));
        addMethodProxy(new BaseMethodProxy("finishOperation", 2, 3));
        addMethodProxy(new BaseMethodProxy("startWatchingMode", -1, 1));
        addMethodProxy(new BaseMethodProxy("checkPackage", 0, 1));
        addMethodProxy(new BaseMethodProxy("getOpsForPackage", 0, 1));
        addMethodProxy(new BaseMethodProxy("setMode", 1, 2));
        addMethodProxy(new BaseMethodProxy("checkAudioOperation", 2, 3));
        addMethodProxy(new BaseMethodProxy("setAudioRestriction", 2, -1));
        addMethodProxy(new ReplaceLastPkgMethodProxy("resetAllModes"));
        addMethodProxy(new MethodProxy() {
            public String getMethodName() {
                return "noteProxyOperation";
            }

            public Object call(Object obj, Method method, Object... objArr) {
                return 0;
            }
        });
    }

    private class BaseMethodProxy extends StaticMethodProxy {
        final int pkgIndex;
        final int uidIndex;

        BaseMethodProxy(String str, int i, int i2) {
            super(str);
            this.pkgIndex = i2;
            this.uidIndex = i;
        }

        public boolean beforeCall(Object obj, Method method, Object... objArr) {
            if (this.pkgIndex != -1 && objArr.length > this.pkgIndex && (objArr[this.pkgIndex] instanceof String)) {
                objArr[this.pkgIndex] = getHostPkg();
            }
            if (this.uidIndex == -1 || !(objArr[this.uidIndex] instanceof Integer)) {
                return true;
            }
            objArr[this.uidIndex] = Integer.valueOf(getRealUid());
            return true;
        }
    }
}
