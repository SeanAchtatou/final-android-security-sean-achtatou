package com.lody.virtual.client.stub;

import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.accounts.c;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.helper.utils.VLog;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class AmsTask extends FutureTask<Bundle> implements AccountManagerFuture<Bundle> {
    final Activity mActivity;
    final AccountManagerCallback<Bundle> mCallback;
    final Handler mHandler;
    protected final c mResponse = new Response();

    public abstract void doWork();

    public AmsTask(Activity activity, Handler handler, AccountManagerCallback<Bundle> accountManagerCallback) {
        super(new Callable<Bundle>() {
            public Bundle call() {
                throw new IllegalStateException("this should never be called");
            }
        });
        this.mHandler = handler;
        this.mCallback = accountManagerCallback;
        this.mActivity = activity;
    }

    public final AccountManagerFuture<Bundle> start() {
        try {
            doWork();
        } catch (RemoteException e2) {
            setException(e2);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void set(Bundle bundle) {
        if (bundle == null) {
            VLog.e("AccountManager", "the bundle must not be null", new Exception());
        }
        super.set((Object) bundle);
    }

    private Bundle internalGetResult(Long l, TimeUnit timeUnit) {
        Bundle bundle;
        if (l == null) {
            try {
                bundle = (Bundle) get();
                cancel(true);
            } catch (CancellationException e2) {
                throw new OperationCanceledException();
            } catch (TimeoutException e3) {
                cancel(true);
                throw new OperationCanceledException();
            } catch (InterruptedException e4) {
                cancel(true);
                throw new OperationCanceledException();
            } catch (ExecutionException e5) {
                Throwable cause = e5.getCause();
                if (cause instanceof IOException) {
                    throw ((IOException) cause);
                } else if (cause instanceof UnsupportedOperationException) {
                    throw new AuthenticatorException(cause);
                } else if (cause instanceof AuthenticatorException) {
                    throw ((AuthenticatorException) cause);
                } else if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else if (cause instanceof Error) {
                    throw ((Error) cause);
                } else {
                    throw new IllegalStateException(cause);
                }
            } catch (Throwable th) {
                cancel(true);
                throw th;
            }
        } else {
            bundle = (Bundle) get(l.longValue(), timeUnit);
            cancel(true);
        }
        return bundle;
    }

    public Bundle getResult() {
        return internalGetResult(null, null);
    }

    public Bundle getResult(long j, TimeUnit timeUnit) {
        return internalGetResult(Long.valueOf(j), timeUnit);
    }

    /* access modifiers changed from: protected */
    public void done() {
        if (this.mCallback != null) {
            postToHandler(this.mHandler, this.mCallback, this);
        }
    }

    private class Response extends c.a {
        private Response() {
        }

        public void onResult(Bundle bundle) {
            Intent intent = (Intent) bundle.getParcelable("intent");
            if (intent != null && AmsTask.this.mActivity != null) {
                AmsTask.this.mActivity.startActivity(intent);
            } else if (bundle.getBoolean("retry")) {
                try {
                    AmsTask.this.doWork();
                } catch (RemoteException e2) {
                    throw new RuntimeException(e2);
                }
            } else {
                AmsTask.this.set(bundle);
            }
        }

        public void onError(int i, String str) {
            if (i == 4 || i == 100 || i == 101) {
                AmsTask.this.cancel(true);
            } else {
                AmsTask.this.setException(AmsTask.this.convertErrorToException(i, str));
            }
        }
    }

    /* access modifiers changed from: private */
    public Exception convertErrorToException(int i, String str) {
        if (i == 3) {
            return new IOException(str);
        }
        if (i == 6) {
            return new UnsupportedOperationException(str);
        }
        if (i == 5) {
            return new AuthenticatorException(str);
        }
        if (i == 7) {
            return new IllegalArgumentException(str);
        }
        return new AuthenticatorException(str);
    }

    private void postToHandler(Handler handler, final AccountManagerCallback<Bundle> accountManagerCallback, final AccountManagerFuture<Bundle> accountManagerFuture) {
        if (handler == null) {
            handler = VirtualRuntime.getUIHandler();
        }
        handler.post(new Runnable() {
            public void run() {
                accountManagerCallback.run(accountManagerFuture);
            }
        });
    }
}
