package com.lody.virtual.client.hook.proxies.search;

import android.annotation.TargetApi;
import android.content.ComponentName;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.base.StaticMethodProxy;
import java.lang.reflect.Method;
import mirror.android.app.ISearchManager;

@TargetApi(17)
public class SearchManagerStub extends BinderInvocationProxy {
    public SearchManagerStub() {
        super(ISearchManager.Stub.asInterface, FirebaseAnalytics.Event.SEARCH);
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new StaticMethodProxy("launchLegacyAssist"));
        addMethodProxy(new GetSearchableInfo());
    }

    private static class GetSearchableInfo extends MethodProxy {
        private GetSearchableInfo() {
        }

        public String getMethodName() {
            return "getSearchableInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ComponentName componentName = (ComponentName) objArr[0];
            if (componentName == null || VirtualCore.getPM().getActivityInfo(componentName, 0) == null) {
                return method.invoke(obj, objArr);
            }
            return null;
        }
    }
}
