package com.lody.virtual.client.hook.proxies.restriction;

import android.annotation.TargetApi;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.ReplaceCallingPkgMethodProxy;
import mirror.android.content.IRestrictionsManager;

@TargetApi(21)
public class RestrictionStub extends BinderInvocationProxy {
    public RestrictionStub() {
        super(IRestrictionsManager.Stub.asInterface, "restrictions");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new ReplaceCallingPkgMethodProxy("getApplicationRestrictions"));
        addMethodProxy(new ReplaceCallingPkgMethodProxy("notifyPermissionResponse"));
        addMethodProxy(new ReplaceCallingPkgMethodProxy("requestPermission"));
    }
}
