package com.lody.virtual.client.hook.proxies.location;

import android.os.Build;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.Inject;
import com.lody.virtual.client.hook.base.LogInvocation;
import com.lody.virtual.client.hook.base.ReplaceLastPkgMethodProxy;
import mirror.android.location.ILocationManager;

@Inject(MethodProxies.class)
@LogInvocation(LogInvocation.Condition.ALWAYS)
public class LocationManagerStub extends BinderInvocationProxy {
    public LocationManagerStub() {
        super(ILocationManager.Stub.asInterface, FirebaseAnalytics.Param.LOCATION);
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        if (Build.VERSION.SDK_INT >= 23) {
            addMethodProxy(new ReplaceLastPkgMethodProxy("addTestProvider"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("removeTestProvider"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("setTestProviderLocation"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("clearTestProviderLocation"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("setTestProviderEnabled"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("clearTestProviderEnabled"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("setTestProviderStatus"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("clearTestProviderStatus"));
        }
        if (Build.VERSION.SDK_INT >= 21) {
            addMethodProxy(new ReplaceLastPkgMethodProxy("addGpsMeasurementsListener"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("addGpsNavigationMessageListener"));
        }
        if (Build.VERSION.SDK_INT >= 17) {
            addMethodProxy(new ReplaceLastPkgMethodProxy("requestGeofence"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("removeGeofence"));
        }
        if (Build.VERSION.SDK_INT == 16 && TextUtils.equals(Build.VERSION.RELEASE, "4.1.2")) {
            addMethodProxy(new ReplaceLastPkgMethodProxy("requestLocationUpdatesPI"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("removeUpdatesPI"));
            addMethodProxy(new ReplaceLastPkgMethodProxy("addProximityAlert"));
        }
    }
}
