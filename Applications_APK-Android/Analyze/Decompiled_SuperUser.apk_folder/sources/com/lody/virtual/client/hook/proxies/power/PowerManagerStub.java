package com.lody.virtual.client.hook.proxies.power;

import com.lody.virtual.client.hook.base.BinderInvocationProxy;
import com.lody.virtual.client.hook.base.ReplaceLastPkgMethodProxy;
import com.lody.virtual.client.hook.base.ReplaceSequencePkgMethodProxy;
import com.lody.virtual.client.hook.base.ResultStaticMethodProxy;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import mirror.android.os.IPowerManager;

public class PowerManagerStub extends BinderInvocationProxy {
    public PowerManagerStub() {
        super(IPowerManager.Stub.asInterface, "power");
    }

    /* access modifiers changed from: protected */
    public void onBindMethods() {
        super.onBindMethods();
        addMethodProxy(new ReplaceSequencePkgMethodProxy("acquireWakeLock", 2) {
            public Object call(Object obj, Method method, Object... objArr) {
                try {
                    return super.call(obj, method, objArr);
                } catch (InvocationTargetException e2) {
                    return PowerManagerStub.this.onHandleError(e2);
                }
            }
        });
        addMethodProxy(new ReplaceLastPkgMethodProxy("acquireWakeLockWithUid") {
            public Object call(Object obj, Method method, Object... objArr) {
                try {
                    return super.call(obj, method, objArr);
                } catch (InvocationTargetException e2) {
                    return PowerManagerStub.this.onHandleError(e2);
                }
            }
        });
        addMethodProxy(new ResultStaticMethodProxy("updateWakeLockWorkSource", 0));
    }

    /* access modifiers changed from: private */
    public Object onHandleError(InvocationTargetException invocationTargetException) {
        if (invocationTargetException.getCause() instanceof SecurityException) {
            return 0;
        }
        throw invocationTargetException.getCause();
    }
}
