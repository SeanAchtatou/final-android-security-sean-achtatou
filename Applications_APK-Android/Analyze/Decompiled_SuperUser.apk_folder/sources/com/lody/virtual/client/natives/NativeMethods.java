package com.lody.virtual.client.natives;

import android.hardware.Camera;
import android.media.AudioRecord;
import android.os.Build;
import dalvik.system.DexFile;
import java.lang.reflect.Method;

public class NativeMethods {
    public static Method gAudioRecordNativeCheckPermission;
    public static int gCameraMethodType;
    public static Method gCameraNativeSetup;
    public static Method gOpenDexFileNative;

    public static void init() {
        String str = Build.VERSION.SDK_INT >= 19 ? "openDexFileNative" : "openDexFile";
        Method[] declaredMethods = DexFile.class.getDeclaredMethods();
        int length = declaredMethods.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Method method = declaredMethods[i];
            if (method.getName().equals(str)) {
                gOpenDexFileNative = method;
                break;
            }
            i++;
        }
        if (gOpenDexFileNative == null) {
            throw new RuntimeException("Unable to find method : " + str);
        }
        gOpenDexFileNative.setAccessible(true);
        gCameraMethodType = -1;
        try {
            gCameraNativeSetup = Camera.class.getDeclaredMethod("native_setup", Object.class, Integer.TYPE, String.class);
            gCameraMethodType = 1;
        } catch (NoSuchMethodException e2) {
        }
        if (gCameraNativeSetup == null) {
            try {
                gCameraNativeSetup = Camera.class.getDeclaredMethod("native_setup", Object.class, Integer.TYPE, Integer.TYPE, String.class);
                gCameraMethodType = 2;
            } catch (NoSuchMethodException e3) {
            }
        }
        if (gCameraNativeSetup == null) {
            try {
                gCameraNativeSetup = Camera.class.getDeclaredMethod("native_setup", Object.class, Integer.TYPE, Integer.TYPE, String.class, Boolean.TYPE);
                gCameraMethodType = 3;
            } catch (NoSuchMethodException e4) {
            }
        }
        if (gCameraNativeSetup == null) {
            try {
                gCameraNativeSetup = Camera.class.getDeclaredMethod("native_setup", Object.class, Integer.TYPE, String.class, Boolean.TYPE);
                gCameraMethodType = 4;
            } catch (NoSuchMethodException e5) {
            }
        }
        if (gCameraNativeSetup != null) {
            gCameraNativeSetup.setAccessible(true);
        }
        for (Method method2 : AudioRecord.class.getDeclaredMethods()) {
            if (method2.getName().equals("native_check_permission") && method2.getParameterTypes().length == 1 && method2.getParameterTypes()[0] == String.class) {
                gAudioRecordNativeCheckPermission = method2;
                method2.setAccessible(true);
                return;
            }
        }
    }
}
