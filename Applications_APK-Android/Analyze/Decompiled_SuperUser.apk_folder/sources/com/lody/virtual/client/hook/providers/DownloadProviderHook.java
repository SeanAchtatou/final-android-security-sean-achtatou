package com.lody.virtual.client.hook.providers;

import android.content.ContentValues;
import android.net.Uri;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.MethodBox;

class DownloadProviderHook extends ExternalProviderHook {
    private static final String COLUMN_COOKIE_DATA = "cookiedata";
    private static final String COLUMN_IS_PUBLIC_API = "is_public_api";
    private static final String COLUMN_NOTIFICATION_CLASS = "notificationclass";
    private static final String COLUMN_NOTIFICATION_PACKAGE = "notificationpackage";
    private static final String COLUMN_OTHER_UID = "otheruid";
    private static final String[] ENFORCE_REMOVE_COLUMNS = {COLUMN_OTHER_UID, COLUMN_NOTIFICATION_CLASS};
    private static final String INSERT_KEY_PREFIX = "http_header_";
    private static final String TAG = DownloadProviderHook.class.getSimpleName();

    DownloadProviderHook(Object obj) {
        super(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public Uri insert(MethodBox methodBox, Uri uri, ContentValues contentValues) {
        if (contentValues.containsKey(COLUMN_NOTIFICATION_PACKAGE)) {
            contentValues.put(COLUMN_NOTIFICATION_PACKAGE, VirtualCore.get().getHostPkg());
        }
        if (contentValues.containsKey(COLUMN_COOKIE_DATA)) {
            String asString = contentValues.getAsString(COLUMN_COOKIE_DATA);
            contentValues.remove(COLUMN_COOKIE_DATA);
            int i = 0;
            while (contentValues.containsKey(INSERT_KEY_PREFIX + i)) {
                i++;
            }
            contentValues.put(INSERT_KEY_PREFIX + i, "Cookie: " + asString);
        }
        if (!contentValues.containsKey(COLUMN_IS_PUBLIC_API)) {
            contentValues.put(COLUMN_IS_PUBLIC_API, (Boolean) true);
        }
        for (String str : ENFORCE_REMOVE_COLUMNS) {
            if (contentValues.containsKey(str)) {
                contentValues.remove(str);
            }
        }
        return super.insert(methodBox, uri, contentValues);
    }
}
