package com.lody.virtual.client.stub;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.R;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VUserHandle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ResolverActivity extends Activity implements AdapterView.OnItemClickListener {
    private static final boolean DEBUG = false;
    private static final String TAG = "ResolverActivity";
    private AlertDialog dialog;
    /* access modifiers changed from: private */
    public ResolveListAdapter mAdapter;
    private Button mAlwaysButton;
    /* access modifiers changed from: private */
    public boolean mAlwaysUseOption;
    private int mIconDpi;
    /* access modifiers changed from: private */
    public int mIconSize;
    private int mLastSelected = -1;
    private int mLaunchedFromUid;
    private ListView mListView;
    private int mMaxColumns;
    private Button mOnceButton;
    protected Bundle mOptions;
    /* access modifiers changed from: private */
    public PackageManager mPm;
    private boolean mRegistered;
    protected int mRequestCode;
    protected String mResultWho;
    /* access modifiers changed from: private */
    public boolean mShowExtended;

    private Intent makeMyIntent() {
        Intent intent = new Intent(getIntent());
        intent.setComponent(null);
        intent.setFlags(intent.getFlags() & -8388609);
        return intent;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"MissingSuperCall"})
    public void onCreate(Bundle bundle) {
        int i;
        Intent makeMyIntent = makeMyIntent();
        Set<String> categories = makeMyIntent.getCategories();
        if (!"android.intent.action.MAIN".equals(makeMyIntent.getAction()) || categories == null || categories.size() != 1 || !categories.contains("android.intent.category.HOME")) {
            i = R.string.g9;
        } else {
            i = R.string.g9;
        }
        onCreate(bundle, makeMyIntent, getResources().getText(i), null, null, true, VUserHandle.getCallingUserId());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle, Intent intent, CharSequence charSequence, Intent[] intentArr, List<ResolveInfo> list, boolean z, int i) {
        super.onCreate(bundle);
        this.mLaunchedFromUid = i;
        this.mPm = getPackageManager();
        this.mAlwaysUseOption = z;
        this.mMaxColumns = getResources().getInteger(R.integer.f4467config_maxResolverActivityColumns);
        this.mRegistered = true;
        ActivityManager activityManager = (ActivityManager) getSystemService(ServiceManagerNative.ACTIVITY);
        this.mIconDpi = activityManager.getLauncherLargeIconDensity();
        this.mIconSize = activityManager.getLauncherLargeIconSize();
        this.mAdapter = new ResolveListAdapter(this, intent, intentArr, list, this.mLaunchedFromUid);
        int count = this.mAdapter.getCount();
        if (Build.VERSION.SDK_INT >= 17 && this.mLaunchedFromUid < 0) {
            finish();
        } else if (count == 1) {
            startSelected(0, false);
            this.mRegistered = false;
            finish();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(charSequence);
            if (count > 1) {
                this.mListView = new ListView(this);
                this.mListView.setAdapter((ListAdapter) this.mAdapter);
                this.mListView.setOnItemClickListener(this);
                this.mListView.setOnItemLongClickListener(new ItemLongClickListener());
                builder.setView(this.mListView);
                if (z) {
                    this.mListView.setChoiceMode(1);
                }
            } else {
                builder.setMessage(R.string.ge);
            }
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    ResolverActivity.this.finish();
                }
            });
            this.dialog = builder.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    @TargetApi(15)
    public Drawable getIcon(Resources resources, int i) {
        try {
            return resources.getDrawableForDensity(i, this.mIconDpi);
        } catch (Resources.NotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Drawable loadIconForResolveInfo(ResolveInfo resolveInfo) {
        Drawable icon;
        Drawable icon2;
        try {
            if (resolveInfo.resolvePackageName != null && resolveInfo.icon != 0 && (icon2 = getIcon(this.mPm.getResourcesForApplication(resolveInfo.resolvePackageName), resolveInfo.icon)) != null) {
                return icon2;
            }
            int iconResource = resolveInfo.getIconResource();
            if (!(iconResource == 0 || (icon = getIcon(this.mPm.getResourcesForApplication(resolveInfo.activityInfo.packageName), iconResource)) == null)) {
                return icon;
            }
            return resolveInfo.loadIcon(this.mPm);
        } catch (PackageManager.NameNotFoundException e2) {
            VLog.e(TAG, "Couldn't find resources for package\n" + VLog.getStackTraceString(e2), new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (!this.mRegistered) {
            this.mRegistered = true;
        }
        this.mAdapter.handlePackagesChanged();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mRegistered) {
            this.mRegistered = false;
        }
        if ((getIntent().getFlags() & 268435456) != 0 && !isChangingConfigurations()) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (this.mAlwaysUseOption) {
            int checkedItemPosition = this.mListView.getCheckedItemPosition();
            boolean z = checkedItemPosition != -1;
            this.mLastSelected = checkedItemPosition;
            this.mAlwaysButton.setEnabled(z);
            this.mOnceButton.setEnabled(z);
            if (z) {
                this.mListView.setSelection(checkedItemPosition);
            }
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        int checkedItemPosition = this.mListView.getCheckedItemPosition();
        boolean z = checkedItemPosition != -1;
        if (!this.mAlwaysUseOption || (z && this.mLastSelected == checkedItemPosition)) {
            startSelected(i, false);
            return;
        }
        this.mAlwaysButton.setEnabled(z);
        this.mOnceButton.setEnabled(z);
        if (z) {
            this.mListView.smoothScrollToPosition(checkedItemPosition);
        }
        this.mLastSelected = checkedItemPosition;
    }

    /* access modifiers changed from: package-private */
    public void startSelected(int i, boolean z) {
        if (!isFinishing()) {
            onIntentSelected(this.mAdapter.resolveInfoForPosition(i), this.mAdapter.intentForPosition(i), z);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onIntentSelected(ResolveInfo resolveInfo, Intent intent, boolean z) {
        IntentFilter intentFilter;
        int i;
        String resolveType;
        if (this.mAlwaysUseOption && this.mAdapter.mOrigResolveList != null) {
            IntentFilter intentFilter2 = new IntentFilter();
            if (intent.getAction() != null) {
                intentFilter2.addAction(intent.getAction());
            }
            Set<String> categories = intent.getCategories();
            if (categories != null) {
                for (String addCategory : categories) {
                    intentFilter2.addCategory(addCategory);
                }
            }
            intentFilter2.addCategory("android.intent.category.DEFAULT");
            int i2 = resolveInfo.match & 268369920;
            Uri data = intent.getData();
            if (i2 != 6291456 || (resolveType = intent.resolveType(this)) == null) {
                intentFilter = intentFilter2;
            } else {
                try {
                    intentFilter2.addDataType(resolveType);
                    intentFilter = intentFilter2;
                } catch (IntentFilter.MalformedMimeTypeException e2) {
                    VLog.w(TAG, "mimeType\n" + VLog.getStackTraceString(e2), new Object[0]);
                    intentFilter = null;
                }
            }
            if (data != null && data.getScheme() != null && (i2 != 6291456 || (!"file".equals(data.getScheme()) && !FirebaseAnalytics.Param.CONTENT.equals(data.getScheme())))) {
                intentFilter.addDataScheme(data.getScheme());
                if (Build.VERSION.SDK_INT >= 19) {
                    Iterator<PatternMatcher> schemeSpecificPartsIterator = resolveInfo.filter.schemeSpecificPartsIterator();
                    if (schemeSpecificPartsIterator != null) {
                        String schemeSpecificPart = data.getSchemeSpecificPart();
                        while (true) {
                            if (schemeSpecificPart == null || !schemeSpecificPartsIterator.hasNext()) {
                                break;
                            }
                            PatternMatcher next = schemeSpecificPartsIterator.next();
                            if (next.match(schemeSpecificPart)) {
                                intentFilter.addDataSchemeSpecificPart(next.getPath(), next.getType());
                                break;
                            }
                        }
                    }
                    Iterator<IntentFilter.AuthorityEntry> authoritiesIterator = resolveInfo.filter.authoritiesIterator();
                    if (authoritiesIterator != null) {
                        while (true) {
                            if (!authoritiesIterator.hasNext()) {
                                break;
                            }
                            IntentFilter.AuthorityEntry next2 = authoritiesIterator.next();
                            if (next2.match(data) >= 0) {
                                int port = next2.getPort();
                                intentFilter.addDataAuthority(next2.getHost(), port >= 0 ? Integer.toString(port) : null);
                            }
                        }
                    }
                    Iterator<PatternMatcher> pathsIterator = resolveInfo.filter.pathsIterator();
                    if (pathsIterator != null) {
                        String path = data.getPath();
                        while (true) {
                            if (path == null || !pathsIterator.hasNext()) {
                                break;
                            }
                            PatternMatcher next3 = pathsIterator.next();
                            if (next3.match(path)) {
                                intentFilter.addDataPath(next3.getPath(), next3.getType());
                                break;
                            }
                        }
                    }
                }
            }
            if (intentFilter != null) {
                int size = this.mAdapter.mOrigResolveList.size();
                ComponentName[] componentNameArr = new ComponentName[size];
                int i3 = 0;
                int i4 = 0;
                while (i3 < size) {
                    ResolveInfo resolveInfo2 = this.mAdapter.mOrigResolveList.get(i3);
                    componentNameArr[i3] = new ComponentName(resolveInfo2.activityInfo.packageName, resolveInfo2.activityInfo.name);
                    if (resolveInfo2.match > i4) {
                        i = resolveInfo2.match;
                    } else {
                        i = i4;
                    }
                    i3++;
                    i4 = i;
                }
                if (z) {
                    getPackageManager().addPreferredActivity(intentFilter, i4, componentNameArr, intent.getComponent());
                } else {
                    try {
                        Reflect.on(VClientImpl.get().getCurrentApplication().getPackageManager()).call("setLastChosenActivity", intent, intent.resolveTypeIfNeeded(getContentResolver()), 65536, intentFilter, Integer.valueOf(i4), intent.getComponent());
                    } catch (Exception e3) {
                        VLog.d(TAG, "Error calling setLastChosenActivity\n" + VLog.getStackTraceString(e3), new Object[0]);
                    }
                }
            }
        }
        if (intent != null) {
            ActivityInfo resolveActivityInfo = VirtualCore.get().resolveActivityInfo(intent, this.mLaunchedFromUid);
            if (resolveActivityInfo == null) {
                startActivity(intent);
            } else {
                VActivityManager.get().startActivity(intent, resolveActivityInfo, null, this.mOptions, this.mResultWho, this.mRequestCode, this.mLaunchedFromUid);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void showAppDetails(ResolveInfo resolveInfo) {
        startActivity(new Intent().setAction("android.settings.APPLICATION_DETAILS_SETTINGS").setData(Uri.fromParts(ServiceManagerNative.PACKAGE, resolveInfo.activityInfo.packageName, null)).addFlags(524288));
    }

    static class ViewHolder {
        public ImageView icon;
        public TextView text;
        public TextView text2;

        public ViewHolder(View view) {
            this.text = (TextView) view.findViewById(R.id.text1);
            this.text2 = (TextView) view.findViewById(R.id.text2);
            this.icon = (ImageView) view.findViewById(R.id.icon);
        }
    }

    private final class DisplayResolveInfo {
        Drawable displayIcon;
        CharSequence displayLabel;
        CharSequence extendedInfo;
        Intent origIntent;
        ResolveInfo ri;

        DisplayResolveInfo(ResolveInfo resolveInfo, CharSequence charSequence, CharSequence charSequence2, Intent intent) {
            this.ri = resolveInfo;
            this.displayLabel = charSequence;
            this.extendedInfo = charSequence2;
            this.origIntent = intent;
        }
    }

    private final class ResolveListAdapter extends BaseAdapter {
        private final List<ResolveInfo> mBaseResolveList;
        private final LayoutInflater mInflater;
        private int mInitialHighlight = -1;
        private final Intent[] mInitialIntents;
        private final Intent mIntent;
        private ResolveInfo mLastChosen;
        private final int mLaunchedFromUid;
        List<DisplayResolveInfo> mList;
        List<ResolveInfo> mOrigResolveList;

        public ResolveListAdapter(Context context, Intent intent, Intent[] intentArr, List<ResolveInfo> list, int i) {
            this.mIntent = new Intent(intent);
            this.mInitialIntents = intentArr;
            this.mBaseResolveList = list;
            this.mLaunchedFromUid = i;
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mList = new ArrayList();
            rebuildList();
        }

        public void handlePackagesChanged() {
            getCount();
            rebuildList();
            notifyDataSetChanged();
            if (getCount() == 0) {
                ResolverActivity.this.finish();
            }
        }

        public int getInitialHighlight() {
            return this.mInitialHighlight;
        }

        private void rebuildList() {
            int i;
            List<ResolveInfo> list;
            int size;
            String str;
            int i2;
            this.mList.clear();
            if (this.mBaseResolveList != null) {
                List<ResolveInfo> list2 = this.mBaseResolveList;
                this.mOrigResolveList = null;
                list = list2;
            } else {
                PackageManager access$100 = ResolverActivity.this.mPm;
                Intent intent = this.mIntent;
                if (ResolverActivity.this.mAlwaysUseOption) {
                    i = 64;
                } else {
                    i = 0;
                }
                List<ResolveInfo> queryIntentActivities = access$100.queryIntentActivities(intent, i | 65536);
                this.mOrigResolveList = queryIntentActivities;
                list = queryIntentActivities;
            }
            if (list != null && (size = list.size()) > 0) {
                ResolveInfo resolveInfo = list.get(0);
                int i3 = 1;
                while (i3 < size) {
                    ResolveInfo resolveInfo2 = list.get(i3);
                    if (resolveInfo.priority == resolveInfo2.priority && resolveInfo.isDefault == resolveInfo2.isDefault) {
                        i2 = size;
                    } else {
                        i2 = size;
                        while (i3 < i2) {
                            if (this.mOrigResolveList == list) {
                                this.mOrigResolveList = new ArrayList(this.mOrigResolveList);
                            }
                            list.remove(i3);
                            i2--;
                        }
                    }
                    i3++;
                    size = i2;
                }
                if (size > 1) {
                    Collections.sort(list, new ResolveInfo.DisplayNameComparator(ResolverActivity.this.mPm));
                }
                if (this.mInitialIntents != null) {
                    for (Intent intent2 : this.mInitialIntents) {
                        if (intent2 != null) {
                            ActivityInfo resolveActivityInfo = intent2.resolveActivityInfo(ResolverActivity.this.getPackageManager(), 0);
                            if (resolveActivityInfo == null) {
                                VLog.w(ResolverActivity.TAG, "No activity found for " + intent2, new Object[0]);
                            } else {
                                ResolveInfo resolveInfo3 = new ResolveInfo();
                                resolveInfo3.activityInfo = resolveActivityInfo;
                                if (intent2 instanceof LabeledIntent) {
                                    LabeledIntent labeledIntent = (LabeledIntent) intent2;
                                    resolveInfo3.resolvePackageName = labeledIntent.getSourcePackage();
                                    resolveInfo3.labelRes = labeledIntent.getLabelResource();
                                    resolveInfo3.nonLocalizedLabel = labeledIntent.getNonLocalizedLabel();
                                    resolveInfo3.icon = labeledIntent.getIconResource();
                                }
                                this.mList.add(new DisplayResolveInfo(resolveInfo3, resolveInfo3.loadLabel(ResolverActivity.this.getPackageManager()), null, intent2));
                            }
                        }
                    }
                }
                ResolveInfo resolveInfo4 = list.get(0);
                CharSequence loadLabel = resolveInfo4.loadLabel(ResolverActivity.this.mPm);
                boolean unused = ResolverActivity.this.mShowExtended = false;
                int i4 = 0;
                ResolveInfo resolveInfo5 = resolveInfo4;
                for (int i5 = 1; i5 < size; i5++) {
                    if (loadLabel == null) {
                        loadLabel = resolveInfo5.activityInfo.packageName;
                    }
                    ResolveInfo resolveInfo6 = list.get(i5);
                    CharSequence loadLabel2 = resolveInfo6.loadLabel(ResolverActivity.this.mPm);
                    if (loadLabel2 == null) {
                        str = resolveInfo6.activityInfo.packageName;
                    } else {
                        str = loadLabel2;
                    }
                    if (!str.equals(loadLabel)) {
                        processGroup(list, i4, i5 - 1, resolveInfo5, loadLabel);
                        loadLabel = str;
                        i4 = i5;
                        resolveInfo5 = resolveInfo6;
                    }
                }
                processGroup(list, i4, size - 1, resolveInfo5, loadLabel);
            }
        }

        private void processGroup(List<ResolveInfo> list, int i, int i2, ResolveInfo resolveInfo, CharSequence charSequence) {
            boolean z;
            boolean z2;
            if ((i2 - i) + 1 == 1) {
                if (this.mLastChosen != null && this.mLastChosen.activityInfo.packageName.equals(resolveInfo.activityInfo.packageName) && this.mLastChosen.activityInfo.name.equals(resolveInfo.activityInfo.name)) {
                    this.mInitialHighlight = this.mList.size();
                }
                this.mList.add(new DisplayResolveInfo(resolveInfo, charSequence, null, null));
                return;
            }
            boolean unused = ResolverActivity.this.mShowExtended = true;
            boolean z3 = false;
            CharSequence loadLabel = resolveInfo.activityInfo.applicationInfo.loadLabel(ResolverActivity.this.mPm);
            if (loadLabel == null) {
                z3 = true;
            }
            if (!z3) {
                HashSet hashSet = new HashSet();
                hashSet.add(loadLabel);
                int i3 = i + 1;
                while (true) {
                    if (i3 > i2) {
                        z2 = z3;
                        break;
                    }
                    CharSequence loadLabel2 = list.get(i3).activityInfo.applicationInfo.loadLabel(ResolverActivity.this.mPm);
                    if (loadLabel2 == null || hashSet.contains(loadLabel2)) {
                        z2 = true;
                    } else {
                        hashSet.add(loadLabel2);
                        i3++;
                    }
                }
                z2 = true;
                hashSet.clear();
                z = z2;
            } else {
                z = z3;
            }
            while (i <= i2) {
                ResolveInfo resolveInfo2 = list.get(i);
                if (this.mLastChosen != null && this.mLastChosen.activityInfo.packageName.equals(resolveInfo2.activityInfo.packageName) && this.mLastChosen.activityInfo.name.equals(resolveInfo2.activityInfo.name)) {
                    this.mInitialHighlight = this.mList.size();
                }
                if (z) {
                    this.mList.add(new DisplayResolveInfo(resolveInfo2, charSequence, resolveInfo2.activityInfo.packageName, null));
                } else {
                    this.mList.add(new DisplayResolveInfo(resolveInfo2, charSequence, resolveInfo2.activityInfo.applicationInfo.loadLabel(ResolverActivity.this.mPm), null));
                }
                i++;
            }
        }

        public ResolveInfo resolveInfoForPosition(int i) {
            return this.mList.get(i).ri;
        }

        public Intent intentForPosition(int i) {
            DisplayResolveInfo displayResolveInfo = this.mList.get(i);
            Intent intent = new Intent(displayResolveInfo.origIntent != null ? displayResolveInfo.origIntent : this.mIntent);
            intent.addFlags(50331648);
            ActivityInfo activityInfo = displayResolveInfo.ri.activityInfo;
            intent.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
            return intent;
        }

        public int getCount() {
            return this.mList.size();
        }

        public Object getItem(int i) {
            return this.mList.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.mInflater.inflate(R.layout.ci, viewGroup, false);
                ViewHolder viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
                ViewGroup.LayoutParams layoutParams = viewHolder.icon.getLayoutParams();
                int access$300 = ResolverActivity.this.mIconSize;
                layoutParams.height = access$300;
                layoutParams.width = access$300;
            }
            bindView(view, this.mList.get(i));
            return view;
        }

        private final void bindView(View view, DisplayResolveInfo displayResolveInfo) {
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            viewHolder.text.setText(displayResolveInfo.displayLabel);
            if (ResolverActivity.this.mShowExtended) {
                viewHolder.text2.setVisibility(0);
                viewHolder.text2.setText(displayResolveInfo.extendedInfo);
            } else {
                viewHolder.text2.setVisibility(8);
            }
            if (displayResolveInfo.displayIcon == null) {
                new LoadIconTask().execute(displayResolveInfo);
            }
            viewHolder.icon.setImageDrawable(displayResolveInfo.displayIcon);
        }
    }

    class ItemLongClickListener implements AdapterView.OnItemLongClickListener {
        ItemLongClickListener() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
            ResolverActivity.this.showAppDetails(ResolverActivity.this.mAdapter.resolveInfoForPosition(i));
            return true;
        }
    }

    class LoadIconTask extends AsyncTask<DisplayResolveInfo, Void, DisplayResolveInfo> {
        LoadIconTask() {
        }

        /* access modifiers changed from: protected */
        public DisplayResolveInfo doInBackground(DisplayResolveInfo... displayResolveInfoArr) {
            DisplayResolveInfo displayResolveInfo = displayResolveInfoArr[0];
            if (displayResolveInfo.displayIcon == null) {
                displayResolveInfo.displayIcon = ResolverActivity.this.loadIconForResolveInfo(displayResolveInfo.ri);
            }
            return displayResolveInfo;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(DisplayResolveInfo displayResolveInfo) {
            ResolverActivity.this.mAdapter.notifyDataSetChanged();
        }
    }
}
