package com.lody.virtual.client.hook.proxies.pm;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDeleteObserver2;
import android.content.pm.IPackageInstallerCallback;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Environment;
import android.os.IInterface;
import android.os.Process;
import android.util.Log;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.utils.MethodParameterUtils;
import com.lody.virtual.client.ipc.VPackageManager;
import com.lody.virtual.helper.collection.ArraySet;
import com.lody.virtual.helper.compat.ParceledListSliceCompat;
import com.lody.virtual.helper.utils.ArrayUtils;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.server.IPackageInstaller;
import com.lody.virtual.server.pm.installer.SessionInfo;
import com.lody.virtual.server.pm.installer.SessionParams;
import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import mirror.android.content.pm.ParceledListSlice;

class MethodProxies {
    MethodProxies() {
    }

    static class IsPackageAvailable extends MethodProxy {
        IsPackageAvailable() {
        }

        public String getMethodName() {
            return "isPackageAvailable";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (isAppPkg((String) objArr[0])) {
                return true;
            }
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetInstallerPackageName extends MethodProxy {
        GetInstallerPackageName() {
        }

        public String getMethodName() {
            return "getInstallerPackageName";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return "com.android.vending";
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPreferredActivities extends MethodProxy {
        GetPreferredActivities() {
        }

        public String getMethodName() {
            return "getPreferredActivities";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceLastAppPkg(objArr);
            return method.invoke(obj, objArr);
        }
    }

    static class GetComponentEnabledSetting extends MethodProxy {
        GetComponentEnabledSetting() {
        }

        public String getMethodName() {
            return "getComponentEnabledSetting";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (((ComponentName) objArr[0]) != null) {
                return 1;
            }
            return method.invoke(obj, objArr);
        }
    }

    static class RemovePackageFromPreferred extends MethodProxy {
        RemovePackageFromPreferred() {
        }

        public String getMethodName() {
            return "removePackageFromPreferred";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }
    }

    static class GetServiceInfo extends MethodProxy {
        GetServiceInfo() {
        }

        public String getMethodName() {
            return "getServiceInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[1]).intValue();
            int myUserId = VUserHandle.myUserId();
            ServiceInfo serviceInfo = VPackageManager.get().getServiceInfo((ComponentName) objArr[0], intValue, myUserId);
            if (serviceInfo != null) {
                return serviceInfo;
            }
            ServiceInfo serviceInfo2 = (ServiceInfo) method.invoke(obj, objArr);
            if (serviceInfo2 == null || !isVisiblePackage(serviceInfo2.applicationInfo)) {
                return null;
            }
            return serviceInfo2;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackageUid extends MethodProxy {
        GetPackageUid() {
        }

        public String getMethodName() {
            return "getPackageUid";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[0];
            if (str.equals(getHostPkg())) {
                return method.invoke(obj, objArr);
            }
            return Integer.valueOf(VUserHandle.getAppId(VPackageManager.get().getPackageUid(str, 0)));
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetActivityInfo extends MethodProxy {
        GetActivityInfo() {
        }

        public String getMethodName() {
            return "getActivityInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ComponentName componentName = (ComponentName) objArr[0];
            if (getHostPkg().equals(componentName.getPackageName())) {
                return method.invoke(obj, objArr);
            }
            int myUserId = VUserHandle.myUserId();
            ActivityInfo activityInfo = VPackageManager.get().getActivityInfo(componentName, ((Integer) objArr[1]).intValue(), myUserId);
            if (activityInfo != null) {
                return activityInfo;
            }
            ActivityInfo activityInfo2 = (ActivityInfo) method.invoke(obj, objArr);
            if (activityInfo2 == null || !isVisiblePackage(activityInfo2.applicationInfo)) {
                return null;
            }
            return activityInfo2;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackageUidEtc extends GetPackageUid {
        GetPackageUidEtc() {
        }

        public String getMethodName() {
            return super.getMethodName() + "Etc";
        }
    }

    static class GetPackageInstaller extends MethodProxy {
        GetPackageInstaller() {
        }

        public boolean isEnable() {
            return isAppProcess();
        }

        public String getMethodName() {
            return "getPackageInstaller";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            IInterface iInterface = (IInterface) method.invoke(obj, objArr);
            final IPackageInstaller packageInstaller = VPackageManager.get().getPackageInstaller();
            return Proxy.newProxyInstance(iInterface.getClass().getClassLoader(), iInterface.getClass().getInterfaces(), new InvocationHandler() {
                public Object invoke(Object obj, Method method, Object[] objArr) {
                    String name = method.getName();
                    char c2 = 65535;
                    switch (name.hashCode()) {
                        case -663066834:
                            if (name.equals("getSessionInfo")) {
                                c2 = 5;
                                break;
                            }
                            break;
                        case -652885011:
                            if (name.equals("updateSessionAppIcon")) {
                                c2 = 1;
                                break;
                            }
                            break;
                        case -403218424:
                            if (name.equals("registerCallback")) {
                                c2 = 8;
                                break;
                            }
                            break;
                        case -93516191:
                            if (name.equals("abandonSession")) {
                                c2 = 3;
                                break;
                            }
                            break;
                        case -63461894:
                            if (name.equals("createSession")) {
                                c2 = 0;
                                break;
                            }
                            break;
                        case 938656808:
                            if (name.equals("getAllSessions")) {
                                c2 = 6;
                                break;
                            }
                            break;
                        case 1170196863:
                            if (name.equals("setPermissionsResult")) {
                                c2 = 10;
                                break;
                            }
                            break;
                        case 1238099456:
                            if (name.equals("updateSessionAppLabel")) {
                                c2 = 2;
                                break;
                            }
                            break;
                        case 1568181855:
                            if (name.equals("getMySessions")) {
                                c2 = 7;
                                break;
                            }
                            break;
                        case 1738611873:
                            if (name.equals("unregisterCallback")) {
                                c2 = 9;
                                break;
                            }
                            break;
                        case 1788161260:
                            if (name.equals("openSession")) {
                                c2 = 4;
                                break;
                            }
                            break;
                    }
                    switch (c2) {
                        case 0:
                            return Integer.valueOf(packageInstaller.createSession(SessionParams.create((PackageInstaller.SessionParams) objArr[0]), (String) objArr[1], VUserHandle.myUserId()));
                        case 1:
                            packageInstaller.updateSessionAppIcon(((Integer) objArr[0]).intValue(), (Bitmap) objArr[1]);
                            return 0;
                        case 2:
                            packageInstaller.updateSessionAppLabel(((Integer) objArr[0]).intValue(), (String) objArr[1]);
                            return 0;
                        case 3:
                            packageInstaller.abandonSession(((Integer) objArr[0]).intValue());
                            return 0;
                        case 4:
                            return packageInstaller.openSession(((Integer) objArr[0]).intValue());
                        case 5:
                            SessionInfo sessionInfo = packageInstaller.getSessionInfo(((Integer) objArr[0]).intValue());
                            if (sessionInfo != null) {
                                return sessionInfo.alloc();
                            }
                            return null;
                        case 6:
                            return ParceledListSliceCompat.create(packageInstaller.getAllSessions(((Integer) objArr[0]).intValue()).getList());
                        case 7:
                            return ParceledListSliceCompat.create(packageInstaller.getMySessions((String) objArr[0], ((Integer) objArr[1]).intValue()).getList());
                        case 8:
                            packageInstaller.registerCallback((IPackageInstallerCallback) objArr[0], VUserHandle.myUserId());
                            return 0;
                        case 9:
                            packageInstaller.unregisterCallback((IPackageInstallerCallback) objArr[0]);
                            return 0;
                        case 10:
                            packageInstaller.setPermissionsResult(((Integer) objArr[0]).intValue(), ((Boolean) objArr[1]).booleanValue());
                            return 0;
                        default:
                            throw new RuntimeException("Not support PackageInstaller method : " + method.getName());
                    }
                }
            });
        }
    }

    static class FreeStorageAndNotify extends MethodProxy {
        FreeStorageAndNotify() {
        }

        public String getMethodName() {
            return "freeStorageAndNotify";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr[objArr.length - 1] instanceof IPackageDataObserver) {
                ((IPackageDataObserver) objArr[objArr.length - 1]).onRemoveCompleted(null, true);
            }
            return 0;
        }
    }

    static class GetPackageGids extends MethodProxy {
        GetPackageGids() {
        }

        public String getMethodName() {
            return "getPackageGids";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class RevokeRuntimePermission extends MethodProxy {
        RevokeRuntimePermission() {
        }

        public String getMethodName() {
            return "revokeRuntimePermission";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class ClearPackagePreferredActivities extends MethodProxy {
        ClearPackagePreferredActivities() {
        }

        public String getMethodName() {
            return "clearPackagePreferredActivities";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }
    }

    static class ResolveContentProvider extends MethodProxy {
        ResolveContentProvider() {
        }

        public String getMethodName() {
            return "resolveContentProvider";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ProviderInfo resolveContentProvider = VPackageManager.get().resolveContentProvider((String) objArr[0], ((Integer) objArr[1]).intValue(), VUserHandle.myUserId());
            if (resolveContentProvider != null || (resolveContentProvider = (ProviderInfo) method.invoke(obj, objArr)) == null || isVisiblePackage(resolveContentProvider.applicationInfo)) {
            }
            return resolveContentProvider;
        }
    }

    static class QueryIntentServices extends MethodProxy {
        QueryIntentServices() {
        }

        public String getMethodName() {
            return "queryIntentServices";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean isReturnParceledListSlice = ParceledListSliceCompat.isReturnParceledListSlice(method);
            List<ResolveInfo> queryIntentServices = VPackageManager.get().queryIntentServices((Intent) objArr[0], (String) objArr[1], ((Integer) objArr[2]).intValue(), VUserHandle.myUserId());
            Object invoke = method.invoke(obj, objArr);
            if (invoke != null) {
                List call = isReturnParceledListSlice ? ParceledListSlice.getList.call(invoke, new Object[0]) : (List) invoke;
                if (call != null) {
                    Iterator it = call.iterator();
                    while (it.hasNext()) {
                        ResolveInfo resolveInfo = (ResolveInfo) it.next();
                        if (resolveInfo == null || resolveInfo.serviceInfo == null || !isVisiblePackage(resolveInfo.serviceInfo.applicationInfo)) {
                            it.remove();
                        }
                    }
                    queryIntentServices.addAll(call);
                }
            }
            if (isReturnParceledListSlice) {
                return ParceledListSliceCompat.create(queryIntentServices);
            }
            return queryIntentServices;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPermissions extends MethodProxy {
        GetPermissions() {
        }

        public String getMethodName() {
            return "getPermissions";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return method.invoke(obj, objArr);
        }
    }

    static class IsPackageForzen extends MethodProxy {
        IsPackageForzen() {
        }

        public String getMethodName() {
            return "isPackageForzen";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return false;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackageGidsEtc extends GetPackageGids {
        GetPackageGidsEtc() {
        }

        public String getMethodName() {
            return super.getMethodName() + "Etc";
        }
    }

    static class QueryIntentActivities extends MethodProxy {
        QueryIntentActivities() {
        }

        public String getMethodName() {
            return "queryIntentActivities";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Log.e("tag", "QueryIntentActivities");
            boolean isReturnParceledListSlice = ParceledListSliceCompat.isReturnParceledListSlice(method);
            int myUserId = VUserHandle.myUserId();
            List<ResolveInfo> queryIntentActivities = VPackageManager.get().queryIntentActivities((Intent) objArr[0], (String) objArr[1], ((Integer) objArr[2]).intValue(), myUserId);
            List<ResolveInfo> queryIntentActivities2 = VirtualCore.get().getUnHookPackageManager().queryIntentActivities((Intent) objArr[0], myUserId);
            if (queryIntentActivities2 != null) {
                queryIntentActivities.addAll(queryIntentActivities2);
            }
            if (isReturnParceledListSlice) {
                return ParceledListSliceCompat.create(queryIntentActivities);
            }
            return queryIntentActivities;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class ResolveService extends MethodProxy {
        ResolveService() {
        }

        public String getMethodName() {
            return "resolveService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[2]).intValue();
            int myUserId = VUserHandle.myUserId();
            ResolveInfo resolveService = VPackageManager.get().resolveService((Intent) objArr[0], (String) objArr[1], intValue, myUserId);
            if (resolveService == null) {
                return (ResolveInfo) method.invoke(obj, objArr);
            }
            return resolveService;
        }
    }

    static class ClearPackagePersistentPreferredActivities extends MethodProxy {
        ClearPackagePersistentPreferredActivities() {
        }

        public String getMethodName() {
            return "clearPackagePersistentPreferredActivities";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }
    }

    static class GetPermissionGroupInfo extends MethodProxy {
        GetPermissionGroupInfo() {
        }

        public String getMethodName() {
            return "getPermissionGroupInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            PermissionGroupInfo permissionGroupInfo = VPackageManager.get().getPermissionGroupInfo((String) objArr[0], ((Integer) objArr[1]).intValue());
            return permissionGroupInfo != null ? permissionGroupInfo : super.call(obj, method, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static final class GetPackageInfo extends MethodProxy {
        GetPackageInfo() {
        }

        public String getMethodName() {
            return "getPackageInfo";
        }

        public boolean beforeCall(Object obj, Method method, Object... objArr) {
            return (objArr == null || objArr[0] == null) ? false : true;
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[1]).intValue();
            int myUserId = VUserHandle.myUserId();
            PackageInfo packageInfo = VPackageManager.get().getPackageInfo((String) objArr[0], intValue, myUserId);
            if (packageInfo == null) {
                PackageInfo packageInfo2 = (PackageInfo) method.invoke(obj, objArr);
                if (packageInfo2 == null || !isVisiblePackage(packageInfo2.applicationInfo)) {
                    return null;
                }
                return packageInfo2;
            } else if (!"com.kg.apk".equals(packageInfo.packageName)) {
                return packageInfo;
            } else {
                packageInfo.versionCode = 430;
                packageInfo.versionName = "4.3.0";
                return packageInfo;
            }
        }
    }

    static class DeleteApplicationCacheFiles extends MethodProxy {
        DeleteApplicationCacheFiles() {
        }

        public String getMethodName() {
            return "deleteApplicationCacheFiles";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return method.invoke(obj, objArr);
        }
    }

    static class SetApplicationBlockedSettingAsUser extends MethodProxy {
        SetApplicationBlockedSettingAsUser() {
        }

        public String getMethodName() {
            return "setApplicationBlockedSettingAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetApplicationEnabledSetting extends MethodProxy {
        GetApplicationEnabledSetting() {
        }

        public String getMethodName() {
            return "getApplicationEnabledSetting";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }
    }

    static class AddPackageToPreferred extends MethodProxy {
        AddPackageToPreferred() {
        }

        public String getMethodName() {
            return "addPackageToPreferred";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return 0;
        }
    }

    static class CheckPermission extends MethodProxy {
        CheckPermission() {
        }

        public String getMethodName() {
            return "checkPermission";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int myUserId = VUserHandle.myUserId();
            return Integer.valueOf(VPackageManager.get().checkPermission((String) objArr[0], (String) objArr[1], myUserId));
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            return super.afterCall(obj, method, objArr, obj2);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackagesForUid extends MethodProxy {
        GetPackagesForUid() {
        }

        public String getMethodName() {
            return "getPackagesForUid";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[0]).intValue();
            int callingUid = Binder.getCallingUid();
            if (intValue == VirtualCore.get().myUid()) {
                intValue = getBaseVUid();
            }
            String[] packagesForUid = VPackageManager.get().getPackagesForUid(callingUid);
            String[] packagesForUid2 = VPackageManager.get().getPackagesForUid(intValue);
            String[] packagesForUid3 = VPackageManager.get().getPackagesForUid(Process.myUid());
            ArraySet arraySet = new ArraySet(2);
            if (packagesForUid != null && packagesForUid.length > 0) {
                arraySet.addAll(Arrays.asList(packagesForUid));
            }
            if (packagesForUid2 != null && packagesForUid2.length > 0) {
                arraySet.addAll(Arrays.asList(packagesForUid2));
            }
            if (packagesForUid3 != null && packagesForUid3.length > 0) {
                arraySet.addAll(Arrays.asList(packagesForUid3));
            }
            return arraySet.toArray(new String[arraySet.size()]);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class QueryContentProviders extends MethodProxy {
        QueryContentProviders() {
        }

        public String getMethodName() {
            return "queryContentProviders";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[2]).intValue();
            List<ProviderInfo> queryContentProviders = VPackageManager.get().queryContentProviders((String) objArr[0], intValue, 0);
            if (ParceledListSliceCompat.isReturnParceledListSlice(method)) {
                return ParceledListSliceCompat.create(queryContentProviders);
            }
            return queryContentProviders;
        }
    }

    static class SetApplicationEnabledSetting extends MethodProxy {
        SetApplicationEnabledSetting() {
        }

        public String getMethodName() {
            return "setApplicationEnabledSetting";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    @SuppressLint({"PackageManagerGetSignatures"})
    static class CheckSignatures extends MethodProxy {
        CheckSignatures() {
        }

        public String getMethodName() {
            return "checkSignatures";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr.length == 2 && (objArr[0] instanceof String) && (objArr[1] instanceof String)) {
                PackageManager pm = VirtualCore.getPM();
                String str = (String) objArr[0];
                String str2 = (String) objArr[1];
                try {
                    PackageInfo packageInfo = pm.getPackageInfo(str, 64);
                    PackageInfo packageInfo2 = pm.getPackageInfo(str2, 64);
                    Signature[] signatureArr = packageInfo.signatures;
                    Signature[] signatureArr2 = packageInfo2.signatures;
                    if (ArrayUtils.isEmpty(signatureArr)) {
                        if (!ArrayUtils.isEmpty(signatureArr2)) {
                            return -1;
                        }
                        return 1;
                    } else if (ArrayUtils.isEmpty(signatureArr2)) {
                        return -2;
                    } else {
                        if (Arrays.equals(signatureArr, signatureArr2)) {
                            return 0;
                        }
                        return -3;
                    }
                } catch (Throwable th) {
                }
            }
            return method.invoke(obj, objArr);
        }
    }

    static class checkUidSignatures extends MethodProxy {
        checkUidSignatures() {
        }

        public String getMethodName() {
            return "checkUidSignatures";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ((Integer) objArr[0]).intValue();
            ((Integer) objArr[1]).intValue();
            return 0;
        }
    }

    static class getNameForUid extends MethodProxy {
        getNameForUid() {
        }

        public String getMethodName() {
            return "getNameForUid";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return VPackageManager.get().getNameForUid(((Integer) objArr[0]).intValue());
        }
    }

    static class DeletePackage extends MethodProxy {
        DeletePackage() {
        }

        public String getMethodName() {
            return "deletePackage";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[0];
            try {
                VirtualCore.get().uninstallPackage(str);
                IPackageDeleteObserver2 iPackageDeleteObserver2 = (IPackageDeleteObserver2) objArr[1];
                if (iPackageDeleteObserver2 != null) {
                    iPackageDeleteObserver2.onPackageDeleted(str, 0, "done.");
                }
            } catch (Throwable th) {
            }
            return 0;
        }
    }

    static class ActivitySupportsIntent extends MethodProxy {
        ActivitySupportsIntent() {
        }

        public String getMethodName() {
            return "activitySupportsIntent";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return Boolean.valueOf(VPackageManager.get().activitySupportsIntent((ComponentName) objArr[0], (Intent) objArr[1], (String) objArr[2]));
        }
    }

    static class ResolveIntent extends MethodProxy {
        ResolveIntent() {
        }

        public String getMethodName() {
            return "resolveIntent";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[2]).intValue();
            int myUserId = VUserHandle.myUserId();
            ResolveInfo resolveIntent = VPackageManager.get().resolveIntent((Intent) objArr[0], (String) objArr[1], intValue, myUserId);
            if (resolveIntent == null) {
                return (ResolveInfo) method.invoke(obj, objArr);
            }
            return resolveIntent;
        }
    }

    static class GetApplicationInfo extends MethodProxy {
        GetApplicationInfo() {
        }

        public String getMethodName() {
            return "getApplicationInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[0];
            int intValue = ((Integer) objArr[1]).intValue();
            int intValue2 = ((Integer) objArr[1]).intValue();
            if (getHostPkg().equals(str)) {
                return method.invoke(obj, objArr);
            }
            ApplicationInfo applicationInfo = VPackageManager.get().getApplicationInfo(str, intValue2, VUserHandle.myUserId());
            if (applicationInfo == null) {
                ApplicationInfo applicationInfo2 = (ApplicationInfo) method.invoke(obj, objArr);
                if (applicationInfo2 == null || !isVisiblePackage(applicationInfo2)) {
                    return null;
                }
                return applicationInfo2;
            } else if (intValue != 0) {
                return applicationInfo;
            } else {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/" + VirtualCore.get().getContext().getPackageName() + "/cache", "fix_plug.apk");
                if (!file.exists()) {
                    return applicationInfo;
                }
                applicationInfo.sourceDir = file.getAbsolutePath();
                return applicationInfo;
            }
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetProviderInfo extends MethodProxy {
        GetProviderInfo() {
        }

        public String getMethodName() {
            return "getProviderInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ComponentName componentName = (ComponentName) objArr[0];
            int intValue = ((Integer) objArr[1]).intValue();
            if (getHostPkg().equals(componentName.getPackageName())) {
                return method.invoke(obj, objArr);
            }
            ProviderInfo providerInfo = VPackageManager.get().getProviderInfo(componentName, intValue, VUserHandle.myUserId());
            if (providerInfo != null) {
                return providerInfo;
            }
            ProviderInfo providerInfo2 = (ProviderInfo) method.invoke(obj, objArr);
            if (providerInfo2 == null || !isVisiblePackage(providerInfo2.applicationInfo)) {
                return null;
            }
            return providerInfo2;
        }
    }

    static class SetComponentEnabledSetting extends MethodProxy {
        SetComponentEnabledSetting() {
        }

        public String getMethodName() {
            return "setComponentEnabledSetting";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetInstalledApplications extends MethodProxy {
        GetInstalledApplications() {
        }

        public String getMethodName() {
            return "getInstalledApplications";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            List<ApplicationInfo> installedApplications = VPackageManager.get().getInstalledApplications(((Integer) objArr[0]).intValue(), VUserHandle.myUserId());
            if (ParceledListSliceCompat.isReturnParceledListSlice(method)) {
                return ParceledListSliceCompat.create(installedApplications);
            }
            return installedApplications;
        }
    }

    static class GetInstalledPackages extends MethodProxy {
        GetInstalledPackages() {
        }

        public String getMethodName() {
            return "getInstalledPackages";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int intValue = ((Integer) objArr[0]).intValue();
            int myUserId = VUserHandle.myUserId();
            List<PackageInfo> installedPackages = VirtualCore.get().getUnHookPackageManager().getInstalledPackages(intValue);
            installedPackages.addAll(VPackageManager.get().getInstalledPackages(intValue, myUserId));
            if (ParceledListSliceCompat.isReturnParceledListSlice(method)) {
                return ParceledListSliceCompat.create(installedPackages);
            }
            return installedPackages;
        }
    }

    static class QueryIntentReceivers extends MethodProxy {
        QueryIntentReceivers() {
        }

        public String getMethodName() {
            return "queryIntentReceivers";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean isReturnParceledListSlice = ParceledListSliceCompat.isReturnParceledListSlice(method);
            List<ResolveInfo> queryIntentReceivers = VPackageManager.get().queryIntentReceivers((Intent) objArr[0], (String) objArr[1], ((Integer) objArr[2]).intValue(), VUserHandle.myUserId());
            Object invoke = method.invoke(obj, objArr);
            List call = isReturnParceledListSlice ? ParceledListSlice.getList.call(invoke, new Object[0]) : (List) invoke;
            if (call != null) {
                Iterator it = call.iterator();
                while (it.hasNext()) {
                    ResolveInfo resolveInfo = (ResolveInfo) it.next();
                    if (resolveInfo == null || resolveInfo.activityInfo == null || !isVisiblePackage(resolveInfo.activityInfo.applicationInfo)) {
                        it.remove();
                    }
                }
                queryIntentReceivers.addAll(call);
            }
            if (isReturnParceledListSlice) {
                return ParceledListSliceCompat.create(queryIntentReceivers);
            }
            return queryIntentReceivers;
        }
    }

    static class GetReceiverInfo extends MethodProxy {
        GetReceiverInfo() {
        }

        public String getMethodName() {
            return "getReceiverInfo";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ComponentName componentName = (ComponentName) objArr[0];
            if (getHostPkg().equals(componentName.getPackageName())) {
                return method.invoke(obj, objArr);
            }
            ActivityInfo receiverInfo = VPackageManager.get().getReceiverInfo(componentName, ((Integer) objArr[1]).intValue(), 0);
            if (receiverInfo != null) {
                return receiverInfo;
            }
            ActivityInfo activityInfo = (ActivityInfo) method.invoke(obj, objArr);
            if (activityInfo == null || !isVisiblePackage(activityInfo.applicationInfo)) {
                return null;
            }
            return activityInfo;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    @TargetApi(17)
    static class GetPermissionFlags extends MethodProxy {
        GetPermissionFlags() {
        }

        public String getMethodName() {
            return "getPermissionFlags";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return method.invoke(obj, objArr);
        }
    }

    static class SetPackageStoppedState extends MethodProxy {
        SetPackageStoppedState() {
        }

        public String getMethodName() {
            return "setPackageStoppedState";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    @TargetApi(19)
    static class QueryIntentContentProviders extends MethodProxy {
        QueryIntentContentProviders() {
        }

        public String getMethodName() {
            return "queryIntentContentProviders";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean isReturnParceledListSlice = ParceledListSliceCompat.isReturnParceledListSlice(method);
            List<ResolveInfo> queryIntentContentProviders = VPackageManager.get().queryIntentContentProviders((Intent) objArr[0], (String) objArr[1], ((Integer) objArr[2]).intValue(), VUserHandle.myUserId());
            Object invoke = method.invoke(obj, objArr);
            List call = isReturnParceledListSlice ? ParceledListSlice.getList.call(invoke, new Object[0]) : (List) invoke;
            if (call != null) {
                Iterator it = call.iterator();
                while (it.hasNext()) {
                    ResolveInfo resolveInfo = (ResolveInfo) it.next();
                    if (resolveInfo == null || resolveInfo.providerInfo == null || !isVisiblePackage(resolveInfo.providerInfo.applicationInfo)) {
                        it.remove();
                    }
                }
                queryIntentContentProviders.addAll(call);
            }
            if (ParceledListSliceCompat.isReturnParceledListSlice(method)) {
                return ParceledListSliceCompat.create(queryIntentContentProviders);
            }
            return queryIntentContentProviders;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetApplicationBlockedSettingAsUser extends MethodProxy {
        GetApplicationBlockedSettingAsUser() {
        }

        public String getMethodName() {
            return "getApplicationBlockedSettingAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }
    }
}
