package com.lody.virtual.client.ipc;

import android.accounts.Account;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorDescription;
import android.accounts.c;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.client.stub.AmsTask;
import com.lody.virtual.helper.compat.AccountManagerCompat;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.server.IAccountManager;
import io.fabric.sdk.android.services.common.a;

public class VAccountManager {
    private static VAccountManager sMgr = new VAccountManager();
    private IAccountManager mRemote;

    public static VAccountManager get() {
        return sMgr;
    }

    public IAccountManager getRemote() {
        if (this.mRemote == null || (!this.mRemote.asBinder().isBinderAlive() && !VirtualCore.get().isVAppProcess())) {
            synchronized (VAccountManager.class) {
                this.mRemote = (IAccountManager) LocalProxyUtils.genProxy(IAccountManager.class, getStubInterface());
            }
        }
        return this.mRemote;
    }

    private Object getStubInterface() {
        return IAccountManager.Stub.asInterface(ServiceManagerNative.getService(ServiceManagerNative.ACCOUNT));
    }

    public AuthenticatorDescription[] getAuthenticatorTypes() {
        try {
            return getRemote().getAuthenticatorTypes(VUserHandle.myUserId());
        } catch (RemoteException e2) {
            return (AuthenticatorDescription[]) VirtualRuntime.crash(e2);
        }
    }

    public void removeAccount(c cVar, Account account, boolean z) {
        try {
            getRemote().removeAccount(VUserHandle.myUserId(), cVar, account, z);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void getAuthToken(c cVar, Account account, String str, boolean z, boolean z2, Bundle bundle) {
        try {
            getRemote().getAuthToken(VUserHandle.myUserId(), cVar, account, str, z, z2, bundle);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public boolean addAccountExplicitly(Account account, String str, Bundle bundle) {
        try {
            return getRemote().addAccountExplicitly(VUserHandle.myUserId(), account, str, bundle);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public Account[] getAccounts(int i, String str) {
        try {
            return getRemote().getAccounts(i, str);
        } catch (RemoteException e2) {
            return (Account[]) VirtualRuntime.crash(e2);
        }
    }

    public Account[] getAccounts(String str) {
        try {
            return getRemote().getAccounts(VUserHandle.myUserId(), str);
        } catch (RemoteException e2) {
            return (Account[]) VirtualRuntime.crash(e2);
        }
    }

    public String peekAuthToken(Account account, String str) {
        try {
            return getRemote().peekAuthToken(VUserHandle.myUserId(), account, str);
        } catch (RemoteException e2) {
            return (String) VirtualRuntime.crash(e2);
        }
    }

    public String getPreviousName(Account account) {
        try {
            return getRemote().getPreviousName(VUserHandle.myUserId(), account);
        } catch (RemoteException e2) {
            return (String) VirtualRuntime.crash(e2);
        }
    }

    public void hasFeatures(c cVar, Account account, String[] strArr) {
        try {
            getRemote().hasFeatures(VUserHandle.myUserId(), cVar, account, strArr);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public boolean accountAuthenticated(Account account) {
        try {
            return getRemote().accountAuthenticated(VUserHandle.myUserId(), account);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public void clearPassword(Account account) {
        try {
            getRemote().clearPassword(VUserHandle.myUserId(), account);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void renameAccount(c cVar, Account account, String str) {
        try {
            getRemote().renameAccount(VUserHandle.myUserId(), cVar, account, str);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void setPassword(Account account, String str) {
        try {
            getRemote().setPassword(VUserHandle.myUserId(), account, str);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void addAccount(int i, c cVar, String str, String str2, String[] strArr, boolean z, Bundle bundle) {
        try {
            getRemote().addAccount(i, cVar, str, str2, strArr, z, bundle);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void addAccount(c cVar, String str, String str2, String[] strArr, boolean z, Bundle bundle) {
        try {
            getRemote().addAccount(VUserHandle.myUserId(), cVar, str, str2, strArr, z, bundle);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void updateCredentials(c cVar, Account account, String str, boolean z, Bundle bundle) {
        try {
            getRemote().updateCredentials(VUserHandle.myUserId(), cVar, account, str, z, bundle);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public boolean removeAccountExplicitly(Account account) {
        try {
            return getRemote().removeAccountExplicitly(VUserHandle.myUserId(), account);
        } catch (RemoteException e2) {
            return ((Boolean) VirtualRuntime.crash(e2)).booleanValue();
        }
    }

    public void setUserData(Account account, String str, String str2) {
        try {
            getRemote().setUserData(VUserHandle.myUserId(), account, str, str2);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void editProperties(c cVar, String str, boolean z) {
        try {
            getRemote().editProperties(VUserHandle.myUserId(), cVar, str, z);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void getAuthTokenLabel(c cVar, String str, String str2) {
        try {
            getRemote().getAuthTokenLabel(VUserHandle.myUserId(), cVar, str, str2);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void confirmCredentials(c cVar, Account account, Bundle bundle, boolean z) {
        try {
            getRemote().confirmCredentials(VUserHandle.myUserId(), cVar, account, bundle, z);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void invalidateAuthToken(String str, String str2) {
        try {
            getRemote().invalidateAuthToken(VUserHandle.myUserId(), str, str2);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void getAccountsByFeatures(c cVar, String str, String[] strArr) {
        try {
            getRemote().getAccountsByFeatures(VUserHandle.myUserId(), cVar, str, strArr);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void setAuthToken(Account account, String str, String str2) {
        try {
            getRemote().setAuthToken(VUserHandle.myUserId(), account, str, str2);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public Object getPassword(Account account) {
        try {
            return getRemote().getPassword(VUserHandle.myUserId(), account);
        } catch (RemoteException e2) {
            return VirtualRuntime.crash(e2);
        }
    }

    public String getUserData(Account account, String str) {
        try {
            return getRemote().getUserData(VUserHandle.myUserId(), account, str);
        } catch (RemoteException e2) {
            return (String) VirtualRuntime.crash(e2);
        }
    }

    public AccountManagerFuture<Bundle> addAccount(int i, String str, String str2, String[] strArr, Bundle bundle, Activity activity, AccountManagerCallback<Bundle> accountManagerCallback, Handler handler) {
        if (str == null) {
            throw new IllegalArgumentException("accountType is null");
        }
        final Bundle bundle2 = new Bundle();
        if (bundle != null) {
            bundle2.putAll(bundle);
        }
        bundle2.putString(AccountManagerCompat.KEY_ANDROID_PACKAGE_NAME, a.ANDROID_CLIENT_TYPE);
        final int i2 = i;
        final String str3 = str;
        final String str4 = str2;
        final String[] strArr2 = strArr;
        final Activity activity2 = activity;
        return new AmsTask(activity, handler, accountManagerCallback) {
            public void doWork() {
                VAccountManager.this.addAccount(i2, this.mResponse, str3, str4, strArr2, activity2 != null, bundle2);
            }
        }.start();
    }
}
