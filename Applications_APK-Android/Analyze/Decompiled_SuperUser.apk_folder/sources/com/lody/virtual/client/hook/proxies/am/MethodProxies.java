package com.lody.virtual.client.hook.proxies.am;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Application;
import android.app.IServiceConnection;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Intent;
import android.content.a;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.TypedValue;
import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.badger.BadgerManager;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.Constants;
import com.lody.virtual.client.env.SpecialComponentList;
import com.lody.virtual.client.hook.base.MethodProxy;
import com.lody.virtual.client.hook.delegate.TaskDescriptionDelegate;
import com.lody.virtual.client.hook.providers.ProviderHook;
import com.lody.virtual.client.hook.secondary.ServiceConnectionDelegate;
import com.lody.virtual.client.hook.utils.MethodParameterUtils;
import com.lody.virtual.client.ipc.ActivityClientRecord;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.client.ipc.VPackageManager;
import com.lody.virtual.client.stub.ChooserActivity;
import com.lody.virtual.client.stub.StubPendingActivity;
import com.lody.virtual.client.stub.StubPendingReceiver;
import com.lody.virtual.client.stub.StubPendingService;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.compat.BuildCompat;
import com.lody.virtual.helper.utils.ArrayUtils;
import com.lody.virtual.helper.utils.BitmapUtils;
import com.lody.virtual.helper.utils.ComponentUtils;
import com.lody.virtual.helper.utils.DrawableUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.AppTaskInfo;
import com.lody.virtual.server.interfaces.IAppRequestListener;
import java.io.File;
import java.lang.reflect.Method;
import java.util.List;
import java.util.WeakHashMap;
import mirror.android.app.IActivityManager;
import mirror.android.content.ContentProviderHolderOreo;
import mirror.android.content.IIntentReceiver;
import mirror.android.content.IIntentReceiverJB;
import mirror.android.content.pm.UserInfo;

class MethodProxies {
    MethodProxies() {
    }

    static class ForceStopPackage extends MethodProxy {
        ForceStopPackage() {
        }

        public String getMethodName() {
            return "forceStopPackage";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int myUserId = VUserHandle.myUserId();
            VActivityManager.get().killAppByPkg((String) objArr[0], myUserId);
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class CrashApplication extends MethodProxy {
        CrashApplication() {
        }

        public String getMethodName() {
            return "crashApplication";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class AddPackageDependency extends MethodProxy {
        AddPackageDependency() {
        }

        public String getMethodName() {
            return "addPackageDependency";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackageForToken extends MethodProxy {
        GetPackageForToken() {
        }

        public String getMethodName() {
            return "getPackageForToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String packageForToken = VActivityManager.get().getPackageForToken((IBinder) objArr[0]);
            return packageForToken != null ? packageForToken : super.call(obj, method, objArr);
        }
    }

    static class UnbindService extends MethodProxy {
        UnbindService() {
        }

        public String getMethodName() {
            return "unbindService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ServiceConnectionDelegate removeDelegate = ServiceConnectionDelegate.removeDelegate((IServiceConnection) objArr[0]);
            if (removeDelegate == null) {
                return method.invoke(obj, objArr);
            }
            return Boolean.valueOf(VActivityManager.get().unbindService(removeDelegate));
        }

        public boolean isEnable() {
            return isAppProcess() || isServerProcess();
        }
    }

    static class GetContentProviderExternal extends GetContentProvider {
        GetContentProviderExternal() {
        }

        public String getMethodName() {
            return "getContentProviderExternal";
        }

        public int getProviderNameIndex() {
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class StartVoiceActivity extends StartActivity {
        StartVoiceActivity() {
        }

        public String getMethodName() {
            return "startVoiceActivity";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return super.call(obj, method, objArr);
        }
    }

    static class UnstableProviderDied extends MethodProxy {
        UnstableProviderDied() {
        }

        public String getMethodName() {
            return "unstableProviderDied";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr[0] == null) {
                return 0;
            }
            return method.invoke(obj, objArr);
        }
    }

    static class PeekService extends MethodProxy {
        PeekService() {
        }

        public String getMethodName() {
            return "peekService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceLastAppPkg(objArr);
            return VActivityManager.get().peekService((Intent) objArr[0], (String) objArr[1]);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackageAskScreenCompat extends MethodProxy {
        GetPackageAskScreenCompat() {
        }

        public String getMethodName() {
            return "getPackageAskScreenCompat";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (Build.VERSION.SDK_INT >= 15 && objArr.length > 0 && (objArr[0] instanceof String)) {
                objArr[0] = getHostPkg();
            }
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetIntentSender extends MethodProxy {
        GetIntentSender() {
        }

        public String getMethodName() {
            return "getIntentSender";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[1];
            String[] strArr = (String[]) objArr[6];
            int intValue = ((Integer) objArr[0]).intValue();
            int intValue2 = ((Integer) objArr[7]).intValue();
            if (objArr[5] instanceof Intent[]) {
                Intent[] intentArr = (Intent[]) objArr[5];
                if (intentArr.length > 0) {
                    Intent intent = intentArr[intentArr.length - 1];
                    if (strArr != null && strArr.length > 0) {
                        intent.setDataAndType(intent.getData(), strArr[strArr.length - 1]);
                    }
                    Intent redirectIntentSender = redirectIntentSender(intValue, str, intent);
                    if (redirectIntentSender != null) {
                        objArr[5] = new Intent[]{redirectIntentSender};
                    }
                }
            }
            objArr[7] = Integer.valueOf(intValue2);
            objArr[1] = getHostPkg();
            if (objArr[objArr.length - 1] instanceof Integer) {
                objArr[objArr.length - 1] = 0;
            }
            IInterface iInterface = (IInterface) method.invoke(obj, objArr);
            if (!(iInterface == null || str == null)) {
                VActivityManager.get().addPendingIntent(iInterface.asBinder(), str);
            }
            return iInterface;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        private Intent redirectIntentSender(int i, String str, Intent intent) {
            Intent cloneFilter = intent.cloneFilter();
            switch (i) {
                case 1:
                    cloneFilter.setClass(getHostContext(), StubPendingReceiver.class);
                    break;
                case 2:
                    if (VirtualCore.get().resolveActivityInfo(intent, VUserHandle.myUserId()) != null) {
                        cloneFilter.setClass(getHostContext(), StubPendingActivity.class);
                        cloneFilter.addFlags(268435456);
                        break;
                    }
                    break;
                case 3:
                default:
                    return null;
                case 4:
                    if (VirtualCore.get().resolveServiceInfo(intent, VUserHandle.myUserId()) != null) {
                        cloneFilter.setClass(getHostContext(), StubPendingService.class);
                        break;
                    }
                    break;
            }
            cloneFilter.putExtra("_VA_|_user_id_", VUserHandle.myUserId());
            cloneFilter.putExtra("_VA_|_intent_", intent);
            cloneFilter.putExtra("_VA_|_creator_", str);
            cloneFilter.putExtra("_VA_|_from_inner_", true);
            return cloneFilter;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class StartActivity extends MethodProxy {
        private static final String SCHEME_FILE = "file";
        private static final String SCHEME_PACKAGE = "package";

        StartActivity() {
        }

        public String getMethodName() {
            return "startActivity";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            IBinder iBinder;
            int i;
            String str;
            ActivityClientRecord activityRecord;
            int indexOfObject = ArrayUtils.indexOfObject(objArr, Intent.class, 1);
            if (indexOfObject < 0) {
                return -1;
            }
            int indexOfObject2 = ArrayUtils.indexOfObject(objArr, IBinder.class, 2);
            Intent intent = (Intent) objArr[indexOfObject];
            intent.setDataAndType(intent.getData(), (String) objArr[indexOfObject + 1]);
            if (indexOfObject2 >= 0) {
                iBinder = (IBinder) objArr[indexOfObject2];
            } else {
                iBinder = null;
            }
            int myUserId = VUserHandle.myUserId();
            if (ComponentUtils.isStubComponent(intent)) {
                return method.invoke(obj, objArr);
            }
            if ("android.intent.action.INSTALL_PACKAGE".equals(intent.getAction()) || ("android.intent.action.VIEW".equals(intent.getAction()) && "application/vnd.android.package-archive".equals(intent.getType()))) {
                if (handleInstallRequest(intent)) {
                    return 0;
                }
            } else if (("android.intent.action.UNINSTALL_PACKAGE".equals(intent.getAction()) || "android.intent.action.DELETE".equals(intent.getAction())) && "package".equals(intent.getScheme()) && handleUninstallRequest(intent)) {
                return 0;
            }
            Bundle bundle = (Bundle) ArrayUtils.getFirst(objArr, Bundle.class);
            if (iBinder != null) {
                i = ((Integer) objArr[indexOfObject2 + 2]).intValue();
                str = (String) objArr[indexOfObject2 + 1];
            } else {
                i = 0;
                str = null;
            }
            if (ChooserActivity.check(intent)) {
                intent.setComponent(new ComponentName(getHostContext(), ChooserActivity.class));
                intent.putExtra(Constants.EXTRA_USER_HANDLE, myUserId);
                intent.putExtra(ChooserActivity.EXTRA_DATA, bundle);
                intent.putExtra(ChooserActivity.EXTRA_WHO, str);
                intent.putExtra(ChooserActivity.EXTRA_REQUEST_CODE, i);
                return method.invoke(obj, objArr);
            }
            if (Build.VERSION.SDK_INT >= 18) {
                objArr[indexOfObject - 1] = getHostPkg();
            }
            if (!(intent.getScheme() == null || !intent.getScheme().equals("package") || intent.getData() == null || intent.getAction() == null || !intent.getAction().startsWith("android.settings."))) {
                intent.setData(Uri.parse("package:" + getHostPkg()));
            }
            ActivityInfo resolveActivityInfo = VirtualCore.get().resolveActivityInfo(intent, myUserId);
            if (resolveActivityInfo == null) {
                VLog.e("VActivityManager", "Unable to resolve activityInfo : " + intent, new Object[0]);
                if (intent.getPackage() == null || !isAppPkg(intent.getPackage())) {
                    return method.invoke(obj, objArr);
                }
                return -1;
            }
            int startActivity = VActivityManager.get().startActivity(intent, resolveActivityInfo, iBinder, bundle, str, i, VUserHandle.myUserId());
            if (!(startActivity == 0 || iBinder == null || i <= 0)) {
                VActivityManager.get().sendActivityResult(iBinder, str, i);
            }
            if (!(iBinder == null || (activityRecord = VActivityManager.get().getActivityRecord(iBinder)) == null || activityRecord.activity == null)) {
                try {
                    TypedValue typedValue = new TypedValue();
                    Resources.Theme newTheme = activityRecord.activity.getResources().newTheme();
                    newTheme.applyStyle(resolveActivityInfo.getThemeResource(), true);
                    if (newTheme.resolveAttribute(16842926, typedValue, true)) {
                        TypedArray obtainStyledAttributes = newTheme.obtainStyledAttributes(typedValue.data, new int[]{16842936, 16842937});
                        activityRecord.activity.overridePendingTransition(obtainStyledAttributes.getResourceId(0, 0), obtainStyledAttributes.getResourceId(1, 0));
                        obtainStyledAttributes.recycle();
                    }
                } catch (Throwable th) {
                }
            }
            return Integer.valueOf(startActivity);
        }

        private boolean handleInstallRequest(Intent intent) {
            IAppRequestListener appRequestListener = VirtualCore.get().getAppRequestListener();
            if (appRequestListener != null) {
                Uri data = intent.getData();
                if (SCHEME_FILE.equals(data.getScheme())) {
                    try {
                        appRequestListener.onRequestInstall(new File(data.getPath()).getPath());
                        return true;
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            return false;
        }

        private boolean handleUninstallRequest(Intent intent) {
            IAppRequestListener appRequestListener = VirtualCore.get().getAppRequestListener();
            if (appRequestListener != null) {
                Uri data = intent.getData();
                if ("package".equals(data.getScheme())) {
                    try {
                        appRequestListener.onRequestUninstall(data.getSchemeSpecificPart());
                        return true;
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            return false;
        }
    }

    static class StartActivities extends MethodProxy {
        StartActivities() {
        }

        public String getMethodName() {
            return "startActivities";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Intent[] intentArr = (Intent[]) ArrayUtils.getFirst(objArr, Intent[].class);
            String[] strArr = (String[]) ArrayUtils.getFirst(objArr, String[].class);
            IBinder iBinder = null;
            int indexOfObject = ArrayUtils.indexOfObject(objArr, IBinder.class, 2);
            if (indexOfObject != -1) {
                iBinder = (IBinder) objArr[indexOfObject];
            }
            return Integer.valueOf(VActivityManager.get().startActivities(intentArr, strArr, iBinder, (Bundle) ArrayUtils.getFirst(objArr, Bundle.class), VUserHandle.myUserId()));
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class FinishActivity extends MethodProxy {
        FinishActivity() {
        }

        public String getMethodName() {
            return "finishActivity";
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            IBinder iBinder = (IBinder) objArr[0];
            ActivityClientRecord activityRecord = VActivityManager.get().getActivityRecord(iBinder);
            if (!(VActivityManager.get().onActivityDestroy(iBinder) || activityRecord == null || activityRecord.activity == null || activityRecord.info.getThemeResource() == 0)) {
                try {
                    TypedValue typedValue = new TypedValue();
                    Resources.Theme newTheme = activityRecord.activity.getResources().newTheme();
                    newTheme.applyStyle(activityRecord.info.getThemeResource(), true);
                    if (newTheme.resolveAttribute(16842926, typedValue, true)) {
                        TypedArray obtainStyledAttributes = newTheme.obtainStyledAttributes(typedValue.data, new int[]{16842938, 16842939});
                        activityRecord.activity.overridePendingTransition(obtainStyledAttributes.getResourceId(0, 0), obtainStyledAttributes.getResourceId(1, 0));
                        obtainStyledAttributes.recycle();
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            return super.afterCall(obj, method, objArr, obj2);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetCallingPackage extends MethodProxy {
        GetCallingPackage() {
        }

        public String getMethodName() {
            return "getCallingPackage";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return VActivityManager.get().getCallingPackage((IBinder) objArr[0]);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPackageForIntentSender extends MethodProxy {
        GetPackageForIntentSender() {
        }

        public String getMethodName() {
            return "getPackageForIntentSender";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String packageForIntentSender;
            IInterface iInterface = (IInterface) objArr[0];
            return (iInterface == null || (packageForIntentSender = VActivityManager.get().getPackageForIntentSender(iInterface.asBinder())) == null) ? super.call(obj, method, objArr) : packageForIntentSender;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class PublishContentProviders extends MethodProxy {
        PublishContentProviders() {
        }

        public String getMethodName() {
            return "publishContentProviders";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetServices extends MethodProxy {
        GetServices() {
        }

        public String getMethodName() {
            return "getServices";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return VActivityManager.get().getServices(((Integer) objArr[0]).intValue(), ((Integer) objArr[1]).intValue()).getList();
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GrantUriPermissionFromOwner extends MethodProxy {
        GrantUriPermissionFromOwner() {
        }

        public String getMethodName() {
            return "grantUriPermissionFromOwner";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class SetServiceForeground extends MethodProxy {
        SetServiceForeground() {
        }

        public String getMethodName() {
            return "setServiceForeground";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean z;
            boolean z2;
            ComponentName componentName = (ComponentName) objArr[0];
            IBinder iBinder = (IBinder) objArr[1];
            int intValue = ((Integer) objArr[2]).intValue();
            Notification notification = (Notification) objArr[3];
            if (objArr[4] instanceof Boolean) {
                z = ((Boolean) objArr[4]).booleanValue();
            } else if (Build.VERSION.SDK_INT < 24 || !(objArr[4] instanceof Integer)) {
                VLog.e(getClass().getSimpleName(), "Unknown flag : " + objArr[4], new Object[0]);
                z = false;
            } else {
                if ((((Integer) objArr[4]).intValue() & 1) != 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                z = z2;
            }
            VActivityManager.get().setServiceForeground(componentName, iBinder, intValue, notification, z);
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class UpdateDeviceOwner extends MethodProxy {
        UpdateDeviceOwner() {
        }

        public String getMethodName() {
            return "updateDeviceOwner";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetIntentForIntentSender extends MethodProxy {
        GetIntentForIntentSender() {
        }

        public String getMethodName() {
            return "getIntentForIntentSender";
        }

        public Object afterCall(Object obj, Method method, Object[] objArr, Object obj2) {
            Intent intent = (Intent) super.afterCall(obj, method, objArr, obj2);
            if (intent == null || !intent.hasExtra("_VA_|_intent_")) {
                return intent;
            }
            return intent.getParcelableExtra("_VA_|_intent_");
        }
    }

    static class UnbindFinished extends MethodProxy {
        UnbindFinished() {
        }

        public String getMethodName() {
            return "unbindFinished";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            boolean booleanValue = ((Boolean) objArr[2]).booleanValue();
            VActivityManager.get().unbindFinished((IBinder) objArr[0], (Intent) objArr[1], booleanValue);
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class StartActivityIntentSender extends MethodProxy {
        StartActivityIntentSender() {
        }

        public String getMethodName() {
            return "startActivityIntentSender";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return super.call(obj, method, objArr);
        }
    }

    static class BindService extends MethodProxy {
        BindService() {
        }

        public String getMethodName() {
            return "bindService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            IInterface iInterface = (IInterface) objArr[0];
            IBinder iBinder = (IBinder) objArr[1];
            Intent intent = (Intent) objArr[2];
            String str = (String) objArr[3];
            IServiceConnection iServiceConnection = (IServiceConnection) objArr[4];
            int intValue = ((Integer) objArr[5]).intValue();
            int myUserId = VUserHandle.myUserId();
            if (isServerProcess()) {
                myUserId = intent.getIntExtra("_VA_|_user_id_", VUserHandle.USER_NULL);
            }
            if (myUserId == -10000) {
                return method.invoke(obj, objArr);
            }
            ServiceInfo resolveServiceInfo = VirtualCore.get().resolveServiceInfo(intent, myUserId);
            if (resolveServiceInfo == null) {
                return method.invoke(obj, objArr);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                intent.setComponent(new ComponentName(resolveServiceInfo.packageName, resolveServiceInfo.name));
            }
            return Integer.valueOf(VActivityManager.get().bindService(iInterface.asBinder(), iBinder, intent, str, ServiceConnectionDelegate.getDelegate(iServiceConnection), intValue, myUserId));
        }

        public boolean isEnable() {
            return isAppProcess() || isServerProcess();
        }
    }

    static class StartService extends MethodProxy {
        StartService() {
        }

        public String getMethodName() {
            return "startService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Intent intent;
            int i;
            IInterface iInterface = (IInterface) objArr[0];
            Intent intent2 = (Intent) objArr[1];
            String str = (String) objArr[2];
            if (intent2.getComponent() != null && getHostPkg().equals(intent2.getComponent().getPackageName())) {
                return method.invoke(obj, objArr);
            }
            int myUserId = VUserHandle.myUserId();
            if (intent2.getBooleanExtra("_VA_|_from_inner_", false)) {
                int intExtra = intent2.getIntExtra("_VA_|_user_id_", myUserId);
                intent = (Intent) intent2.getParcelableExtra("_VA_|_intent_");
                i = intExtra;
            } else if (isServerProcess()) {
                intent = intent2;
                i = intent2.getIntExtra("_VA_|_user_id_", VUserHandle.USER_NULL);
            } else {
                int i2 = myUserId;
                intent = intent2;
                i = i2;
            }
            intent.setDataAndType(intent.getData(), str);
            if (VirtualCore.get().resolveServiceInfo(intent, VUserHandle.myUserId()) != null) {
                return VActivityManager.get().startService(iInterface, intent, str, i);
            }
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess() || isServerProcess();
        }
    }

    static class StartActivityAndWait extends StartActivity {
        StartActivityAndWait() {
        }

        public String getMethodName() {
            return "startActivityAndWait";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return super.call(obj, method, objArr);
        }
    }

    static class PublishService extends MethodProxy {
        PublishService() {
        }

        public String getMethodName() {
            return "publishService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            IBinder iBinder = (IBinder) objArr[0];
            if (!VActivityManager.get().isVAServiceToken(iBinder)) {
                return method.invoke(obj, objArr);
            }
            VActivityManager.get().publishService(iBinder, (Intent) objArr[1], (IBinder) objArr[2]);
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetRunningAppProcesses extends MethodProxy {
        GetRunningAppProcesses() {
        }

        public String getMethodName() {
            return "getRunningAppProcesses";
        }

        public synchronized Object call(Object obj, Method method, Object... objArr) {
            List<ActivityManager.RunningAppProcessInfo> list;
            list = (List) method.invoke(obj, objArr);
            if (list != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : list) {
                    if (VActivityManager.get().isAppPid(runningAppProcessInfo.pid)) {
                        List<String> processPkgList = VActivityManager.get().getProcessPkgList(runningAppProcessInfo.pid);
                        String appProcessName = VActivityManager.get().getAppProcessName(runningAppProcessInfo.pid);
                        if (appProcessName != null) {
                            runningAppProcessInfo.processName = appProcessName;
                        }
                        runningAppProcessInfo.pkgList = (String[]) processPkgList.toArray(new String[processPkgList.size()]);
                        runningAppProcessInfo.uid = VUserHandle.getAppId(VActivityManager.get().getUidByPid(runningAppProcessInfo.pid));
                    }
                }
            }
            return list;
        }
    }

    static class SetPackageAskScreenCompat extends MethodProxy {
        SetPackageAskScreenCompat() {
        }

        public String getMethodName() {
            return "setPackageAskScreenCompat";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (Build.VERSION.SDK_INT >= 15 && objArr.length > 0 && (objArr[0] instanceof String)) {
                objArr[0] = getHostPkg();
            }
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetCallingActivity extends MethodProxy {
        GetCallingActivity() {
        }

        public String getMethodName() {
            return "getCallingActivity";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return VActivityManager.get().getCallingActivity((IBinder) objArr[0]);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetCurrentUser extends MethodProxy {
        GetCurrentUser() {
        }

        public String getMethodName() {
            return "getCurrentUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            try {
                return UserInfo.ctor.newInstance(0, ServiceManagerNative.USER, 1);
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }
    }

    static class KillApplicationProcess extends MethodProxy {
        KillApplicationProcess() {
        }

        public String getMethodName() {
            return "killApplicationProcess";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            if (objArr.length <= 1 || !(objArr[0] instanceof String) || !(objArr[1] instanceof Integer)) {
                return method.invoke(obj, objArr);
            }
            int intValue = ((Integer) objArr[1]).intValue();
            VActivityManager.get().killApplicationProcess((String) objArr[0], intValue);
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class StartActivityAsUser extends StartActivity {
        StartActivityAsUser() {
        }

        public String getMethodName() {
            return "startActivityAsUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return super.call(obj, method, objArr);
        }
    }

    static class CheckPermission extends MethodProxy {
        CheckPermission() {
        }

        public String getMethodName() {
            return "checkPermission";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            String str = (String) objArr[0];
            if (SpecialComponentList.isWhitePermission(str)) {
                return 0;
            }
            if (str.startsWith("com.google")) {
                return 0;
            }
            objArr[objArr.length - 1] = Integer.valueOf(getRealUid());
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class StartActivityAsCaller extends StartActivity {
        StartActivityAsCaller() {
        }

        public String getMethodName() {
            return "startActivityAsCaller";
        }
    }

    static class HandleIncomingUser extends MethodProxy {
        HandleIncomingUser() {
        }

        public String getMethodName() {
            return "handleIncomingUser";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int length = objArr.length - 1;
            if (objArr[length] instanceof String) {
                objArr[length] = getHostPkg();
            }
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetTasks extends MethodProxy {
        GetTasks() {
        }

        public String getMethodName() {
            return "getTasks";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            List<ActivityManager.RunningTaskInfo> list = (List) method.invoke(obj, objArr);
            for (ActivityManager.RunningTaskInfo runningTaskInfo : list) {
                AppTaskInfo taskInfo = VActivityManager.get().getTaskInfo(runningTaskInfo.id);
                if (taskInfo != null) {
                    runningTaskInfo.topActivity = taskInfo.topActivity;
                    runningTaskInfo.baseActivity = taskInfo.baseActivity;
                }
            }
            return list;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetPersistedUriPermissions extends MethodProxy {
        GetPersistedUriPermissions() {
        }

        public String getMethodName() {
            return "getPersistedUriPermissions";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class RegisterReceiver extends MethodProxy {
        private static final int IDX_IIntentReceiver = (Build.VERSION.SDK_INT >= 15 ? 2 : 1);
        private static final int IDX_IntentFilter;
        private static final int IDX_RequiredPermission;
        /* access modifiers changed from: private */
        public WeakHashMap<IBinder, a> mProxyIIntentReceivers = new WeakHashMap<>();

        RegisterReceiver() {
        }

        static {
            int i;
            int i2 = 3;
            if (Build.VERSION.SDK_INT >= 15) {
                i = 4;
            } else {
                i = 3;
            }
            IDX_RequiredPermission = i;
            if (Build.VERSION.SDK_INT < 15) {
                i2 = 2;
            }
            IDX_IntentFilter = i2;
        }

        public String getMethodName() {
            return "registerReceiver";
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0030, code lost:
            r2 = r0.asBinder();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object call(java.lang.Object r5, java.lang.reflect.Method r6, java.lang.Object... r7) {
            /*
                r4 = this;
                com.lody.virtual.client.hook.utils.MethodParameterUtils.replaceFirstAppPkg(r7)
                int r0 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IDX_RequiredPermission
                r1 = 0
                r7[r0] = r1
                int r0 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IDX_IntentFilter
                r0 = r7[r0]
                android.content.IntentFilter r0 = (android.content.IntentFilter) r0
                com.lody.virtual.client.env.SpecialComponentList.protectIntentFilter(r0)
                int r0 = r7.length
                int r1 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IDX_IIntentReceiver
                if (r0 <= r1) goto L_0x006a
                java.lang.Class<android.content.a> r0 = android.content.a.class
                int r1 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IDX_IIntentReceiver
                r1 = r7[r1]
                boolean r0 = r0.isInstance(r1)
                if (r0 == 0) goto L_0x006a
                int r0 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IDX_IIntentReceiver
                r0 = r7[r0]
                android.os.IInterface r0 = (android.os.IInterface) r0
                java.lang.Class<com.lody.virtual.client.hook.proxies.am.MethodProxies$RegisterReceiver$IIntentReceiverProxy> r1 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IIntentReceiverProxy.class
                boolean r1 = r1.isInstance(r0)
                if (r1 != 0) goto L_0x006a
                android.os.IBinder r2 = r0.asBinder()
                if (r2 == 0) goto L_0x006a
                com.lody.virtual.client.hook.proxies.am.MethodProxies$RegisterReceiver$1 r1 = new com.lody.virtual.client.hook.proxies.am.MethodProxies$RegisterReceiver$1
                r1.<init>(r2)
                r3 = 0
                r2.linkToDeath(r1, r3)
                java.util.WeakHashMap<android.os.IBinder, android.content.a> r1 = r4.mProxyIIntentReceivers
                java.lang.Object r1 = r1.get(r2)
                android.content.a r1 = (android.content.a) r1
                if (r1 != 0) goto L_0x0053
                com.lody.virtual.client.hook.proxies.am.MethodProxies$RegisterReceiver$IIntentReceiverProxy r1 = new com.lody.virtual.client.hook.proxies.am.MethodProxies$RegisterReceiver$IIntentReceiverProxy
                r1.<init>(r0)
                java.util.WeakHashMap<android.os.IBinder, android.content.a> r3 = r4.mProxyIIntentReceivers
                r3.put(r2, r1)
            L_0x0053:
                mirror.RefObject<java.lang.ref.WeakReference> r2 = mirror.android.app.LoadedApk.ReceiverDispatcher.InnerReceiver.mDispatcher
                java.lang.Object r0 = r2.get(r0)
                java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0
                if (r0 == 0) goto L_0x006a
                mirror.RefObject<android.content.a> r2 = mirror.android.app.LoadedApk.ReceiverDispatcher.mIIntentReceiver
                java.lang.Object r0 = r0.get()
                r2.set(r0, r1)
                int r0 = com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.IDX_IIntentReceiver
                r7[r0] = r1
            L_0x006a:
                java.lang.Object r0 = r6.invoke(r5, r7)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.client.hook.proxies.am.MethodProxies.RegisterReceiver.call(java.lang.Object, java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
        }

        public boolean isEnable() {
            return isAppProcess();
        }

        private static class IIntentReceiverProxy extends a.C0004a {
            IInterface mOld;

            IIntentReceiverProxy(IInterface iInterface) {
                this.mOld = iInterface;
            }

            public void performReceive(Intent intent, int i, String str, Bundle bundle, boolean z, boolean z2, int i2) {
                Intent intent2;
                if (accept(intent)) {
                    if (intent.hasExtra("_VA_|_intent_")) {
                        intent2 = (Intent) intent.getParcelableExtra("_VA_|_intent_");
                    } else {
                        intent2 = intent;
                    }
                    SpecialComponentList.unprotectIntent(intent2);
                    if (Build.VERSION.SDK_INT > 16) {
                        IIntentReceiverJB.performReceive.call(this.mOld, intent2, Integer.valueOf(i), str, bundle, Boolean.valueOf(z), Boolean.valueOf(z2), Integer.valueOf(i2));
                        return;
                    }
                    IIntentReceiver.performReceive.call(this.mOld, intent2, Integer.valueOf(i), str, bundle, Boolean.valueOf(z), Boolean.valueOf(z2));
                }
            }

            private boolean accept(Intent intent) {
                boolean z = false;
                int intExtra = intent.getIntExtra("_VA_|_uid_", -1);
                if (intExtra == -1) {
                    int intExtra2 = intent.getIntExtra("_VA_|_user_id_", -1);
                    if (intExtra2 == -1 || intExtra2 == VUserHandle.myUserId()) {
                        z = true;
                    }
                    return z;
                } else if (VClientImpl.get().getVUid() == intExtra) {
                    return true;
                } else {
                    return false;
                }
            }

            public void performReceive(Intent intent, int i, String str, Bundle bundle, boolean z, boolean z2) {
                performReceive(intent, i, str, bundle, z, z2, 0);
            }
        }
    }

    static class StopService extends MethodProxy {
        StopService() {
        }

        public String getMethodName() {
            return "stopService";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ResolveInfo resolveService;
            IInterface iInterface = (IInterface) objArr[0];
            Intent intent = (Intent) objArr[1];
            String str = (String) objArr[2];
            intent.setDataAndType(intent.getData(), str);
            ComponentName component = intent.getComponent();
            PackageManager pm = VirtualCore.getPM();
            if (!(component != null || (resolveService = pm.resolveService(intent, 0)) == null || resolveService.serviceInfo == null)) {
                component = new ComponentName(resolveService.serviceInfo.packageName, resolveService.serviceInfo.name);
            }
            if (component == null || getHostPkg().equals(component.getPackageName())) {
                return method.invoke(obj, objArr);
            }
            return Integer.valueOf(VActivityManager.get().stopService(iInterface, intent, str));
        }

        public boolean isEnable() {
            return isAppProcess() || isServerProcess();
        }
    }

    static class GetContentProvider extends MethodProxy {
        GetContentProvider() {
        }

        public String getMethodName() {
            return "getContentProvider";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            int providerNameIndex = getProviderNameIndex();
            int myUserId = VUserHandle.myUserId();
            ProviderInfo resolveContentProvider = VPackageManager.get().resolveContentProvider((String) objArr[providerNameIndex], 0, myUserId);
            if (resolveContentProvider == null || !resolveContentProvider.enabled || !isAppPkg(resolveContentProvider.packageName)) {
                Object invoke = method.invoke(obj, objArr);
                if (invoke == null) {
                    return null;
                }
                if (BuildCompat.isOreo()) {
                    IInterface iInterface = ContentProviderHolderOreo.provider.get(invoke);
                    ProviderInfo providerInfo = ContentProviderHolderOreo.info.get(invoke);
                    if (iInterface != null) {
                        iInterface = ProviderHook.createProxy(true, providerInfo.authority, iInterface);
                    }
                    ContentProviderHolderOreo.provider.set(invoke, iInterface);
                } else {
                    IInterface iInterface2 = IActivityManager.ContentProviderHolder.provider.get(invoke);
                    ProviderInfo providerInfo2 = IActivityManager.ContentProviderHolder.info.get(invoke);
                    if (iInterface2 != null) {
                        iInterface2 = ProviderHook.createProxy(true, providerInfo2.authority, iInterface2);
                    }
                    IActivityManager.ContentProviderHolder.provider.set(invoke, iInterface2);
                }
                return invoke;
            }
            int initProcess = VActivityManager.get().initProcess(resolveContentProvider.packageName, resolveContentProvider.processName, myUserId);
            if (initProcess == -1) {
                return null;
            }
            objArr[providerNameIndex] = VASettings.getStubAuthority(initProcess);
            Object invoke2 = method.invoke(obj, objArr);
            if (invoke2 == null) {
                return null;
            }
            if (BuildCompat.isOreo()) {
                IInterface iInterface3 = ContentProviderHolderOreo.provider.get(invoke2);
                if (iInterface3 != null) {
                    iInterface3 = VActivityManager.get().acquireProviderClient(myUserId, resolveContentProvider);
                }
                ContentProviderHolderOreo.provider.set(invoke2, iInterface3);
                ContentProviderHolderOreo.info.set(invoke2, resolveContentProvider);
            } else {
                IInterface iInterface4 = IActivityManager.ContentProviderHolder.provider.get(invoke2);
                if (iInterface4 != null) {
                    iInterface4 = VActivityManager.get().acquireProviderClient(myUserId, resolveContentProvider);
                }
                IActivityManager.ContentProviderHolder.provider.set(invoke2, iInterface4);
                IActivityManager.ContentProviderHolder.info.set(invoke2, resolveContentProvider);
            }
            return invoke2;
        }

        public int getProviderNameIndex() {
            return 1;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    @TargetApi(21)
    static class SetTaskDescription extends MethodProxy {
        SetTaskDescription() {
        }

        public String getMethodName() {
            return "setTaskDescription";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Application currentApplication;
            String str;
            Bitmap bitmap;
            Drawable loadIcon;
            ActivityManager.TaskDescription taskDescription = (ActivityManager.TaskDescription) objArr[1];
            String label = taskDescription.getLabel();
            Bitmap icon = taskDescription.getIcon();
            if ((label == null || icon == null) && (currentApplication = VClientImpl.get().getCurrentApplication()) != null) {
                if (label == null) {
                    try {
                        str = currentApplication.getApplicationInfo().loadLabel(currentApplication.getPackageManager()).toString();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                } else {
                    str = label;
                }
                if (icon != null || (loadIcon = currentApplication.getApplicationInfo().loadIcon(currentApplication.getPackageManager())) == null) {
                    bitmap = icon;
                } else {
                    bitmap = DrawableUtils.drawableToBitMap(loadIcon);
                }
                taskDescription = new ActivityManager.TaskDescription(str, bitmap, taskDescription.getPrimaryColor());
            }
            TaskDescriptionDelegate taskDescriptionDelegate = VirtualCore.get().getTaskDescriptionDelegate();
            if (taskDescriptionDelegate != null) {
                taskDescription = taskDescriptionDelegate.getTaskDescription(taskDescription);
            }
            objArr[1] = taskDescription;
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class StopServiceToken extends MethodProxy {
        StopServiceToken() {
        }

        public String getMethodName() {
            return "stopServiceToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            ComponentName componentName = (ComponentName) objArr[0];
            IBinder iBinder = (IBinder) objArr[1];
            if (!VActivityManager.get().isVAServiceToken(iBinder)) {
                return method.invoke(obj, objArr);
            }
            int intValue = ((Integer) objArr[2]).intValue();
            if (componentName != null) {
                return Boolean.valueOf(VActivityManager.get().stopServiceToken(componentName, iBinder, intValue));
            }
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess() || isServerProcess();
        }
    }

    static class StartActivityWithConfig extends StartActivity {
        StartActivityWithConfig() {
        }

        public String getMethodName() {
            return "startActivityWithConfig";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return super.call(obj, method, objArr);
        }
    }

    static class StartNextMatchingActivity extends StartActivity {
        StartNextMatchingActivity() {
        }

        public String getMethodName() {
            return "startNextMatchingActivity";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return false;
        }
    }

    static class BroadcastIntent extends MethodProxy {
        BroadcastIntent() {
        }

        public String getMethodName() {
            return "broadcastIntent";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            Intent intent = (Intent) objArr[1];
            intent.setDataAndType(intent.getData(), (String) objArr[2]);
            if (VirtualCore.get().getComponentDelegate() != null) {
                VirtualCore.get().getComponentDelegate().onSendBroadcast(intent);
            }
            Intent handleIntent = handleIntent(intent);
            if (handleIntent == null) {
                return 0;
            }
            objArr[1] = handleIntent;
            if ((objArr[7] instanceof String) || (objArr[7] instanceof String[])) {
                objArr[7] = null;
            }
            return method.invoke(obj, objArr);
        }

        private Intent handleIntent(Intent intent) {
            String action = intent.getAction();
            if ("android.intent.action.CREATE_SHORTCUT".equals(action) || "com.android.launcher.action.INSTALL_SHORTCUT".equals(action)) {
                if (VASettings.ENABLE_INNER_SHORTCUT) {
                    return handleInstallShortcutIntent(intent);
                }
                return null;
            } else if ("com.android.launcher.action.UNINSTALL_SHORTCUT".equals(action)) {
                handleUninstallShortcutIntent(intent);
                return intent;
            } else if (!BadgerManager.handleBadger(intent)) {
                return ComponentUtils.redirectBroadcastIntent(intent, VUserHandle.myUserId());
            } else {
                return null;
            }
        }

        private Intent handleInstallShortcutIntent(Intent intent) {
            ComponentName resolveActivity;
            Bitmap drawableToBitmap;
            Intent intent2 = (Intent) intent.getParcelableExtra("android.intent.extra.shortcut.INTENT");
            if (!(intent2 == null || (resolveActivity = intent2.resolveActivity(VirtualCore.getPM())) == null)) {
                String packageName = resolveActivity.getPackageName();
                Intent intent3 = new Intent();
                intent3.setClassName(getHostPkg(), Constants.SHORTCUT_PROXY_ACTIVITY_NAME);
                intent3.addCategory("android.intent.category.DEFAULT");
                intent3.putExtra("_VA_|_intent_", intent2);
                intent3.putExtra("_VA_|_uri_", intent2.toUri(0));
                intent3.putExtra("_VA_|_user_id_", VUserHandle.myUserId());
                intent.removeExtra("android.intent.extra.shortcut.INTENT");
                intent.putExtra("android.intent.extra.shortcut.INTENT", intent3);
                Intent.ShortcutIconResource shortcutIconResource = (Intent.ShortcutIconResource) intent.getParcelableExtra("android.intent.extra.shortcut.ICON_RESOURCE");
                if (shortcutIconResource != null && !TextUtils.equals(shortcutIconResource.packageName, getHostPkg())) {
                    try {
                        Resources resources = VirtualCore.get().getResources(packageName);
                        int identifier = resources.getIdentifier(shortcutIconResource.resourceName, "drawable", packageName);
                        if (identifier > 0 && (drawableToBitmap = BitmapUtils.drawableToBitmap(resources.getDrawable(identifier))) != null) {
                            intent.removeExtra("android.intent.extra.shortcut.ICON_RESOURCE");
                            intent.putExtra("android.intent.extra.shortcut.ICON", drawableToBitmap);
                        }
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
            return intent;
        }

        private void handleUninstallShortcutIntent(Intent intent) {
            Intent intent2 = (Intent) intent.getParcelableExtra("android.intent.extra.shortcut.INTENT");
            if (intent2 != null && intent2.resolveActivity(getPM()) != null) {
                Intent intent3 = new Intent();
                intent3.putExtra("_VA_|_uri_", intent2.toUri(0));
                intent3.setClassName(getHostPkg(), Constants.SHORTCUT_PROXY_ACTIVITY_NAME);
                intent3.removeExtra("android.intent.extra.shortcut.INTENT");
                intent.putExtra("android.intent.extra.shortcut.INTENT", intent3);
            }
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class GetActivityClassForToken extends MethodProxy {
        GetActivityClassForToken() {
        }

        public String getMethodName() {
            return "getActivityClassForToken";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            return VActivityManager.get().getActivityForToken((IBinder) objArr[0]);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class CheckGrantUriPermission extends MethodProxy {
        CheckGrantUriPermission() {
        }

        public String getMethodName() {
            return "checkGrantUriPermission";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            MethodParameterUtils.replaceFirstAppPkg(objArr);
            return method.invoke(obj, objArr);
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }

    static class ServiceDoneExecuting extends MethodProxy {
        ServiceDoneExecuting() {
        }

        public String getMethodName() {
            return "serviceDoneExecuting";
        }

        public Object call(Object obj, Method method, Object... objArr) {
            IBinder iBinder = (IBinder) objArr[0];
            if (!VActivityManager.get().isVAServiceToken(iBinder)) {
                return method.invoke(obj, objArr);
            }
            VActivityManager.get().serviceDoneExecuting(iBinder, ((Integer) objArr[1]).intValue(), ((Integer) objArr[2]).intValue(), ((Integer) objArr[3]).intValue());
            return 0;
        }

        public boolean isEnable() {
            return isAppProcess();
        }
    }
}
