package com.lody.virtual.client.ipc;

import android.os.RemoteException;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.remote.vloc.VCell;
import com.lody.virtual.remote.vloc.VLocation;
import com.lody.virtual.server.IVirtualLocationManager;
import java.util.List;

public class VirtualLocationManager {
    private static final VirtualLocationManager sInstance = new VirtualLocationManager();
    private IVirtualLocationManager mRemote;

    public static VirtualLocationManager get() {
        return sInstance;
    }

    public IVirtualLocationManager getRemote() {
        if (this.mRemote == null || (!this.mRemote.asBinder().isBinderAlive() && !VirtualCore.get().isVAppProcess())) {
            synchronized (this) {
                this.mRemote = (IVirtualLocationManager) LocalProxyUtils.genProxy(IVirtualLocationManager.class, getRemoteInterface());
            }
        }
        return this.mRemote;
    }

    private Object getRemoteInterface() {
        return IVirtualLocationManager.Stub.asInterface(ServiceManagerNative.getService(ServiceManagerNative.VIRTUAL_LOC));
    }

    public int getMode(int i, String str) {
        try {
            return getRemote().getMode(i, str);
        } catch (RemoteException e2) {
            return ((Integer) VirtualRuntime.crash(e2)).intValue();
        }
    }

    public void setMode(int i, String str, int i2) {
        try {
            getRemote().setMode(i, str, i2);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void setCell(int i, String str, VCell vCell) {
        try {
            getRemote().setCell(i, str, vCell);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void setAllCell(int i, String str, List<VCell> list) {
        try {
            getRemote().setAllCell(i, str, list);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void setNeighboringCell(int i, String str, List<VCell> list) {
        try {
            getRemote().setNeighboringCell(i, str, list);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public VCell getCell(int i, String str) {
        try {
            return getRemote().getCell(i, str);
        } catch (RemoteException e2) {
            return (VCell) VirtualRuntime.crash(e2);
        }
    }

    public List<VCell> getAllCell(int i, String str) {
        try {
            return getRemote().getAllCell(i, str);
        } catch (RemoteException e2) {
            return (List) VirtualRuntime.crash(e2);
        }
    }

    public List<VCell> getNeighboringCell(int i, String str) {
        try {
            return getRemote().getNeighboringCell(i, str);
        } catch (RemoteException e2) {
            return (List) VirtualRuntime.crash(e2);
        }
    }

    public void setGlobalCell(VCell vCell) {
        try {
            getRemote().setGlobalCell(vCell);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void setGlobalAllCell(List<VCell> list) {
        try {
            getRemote().setGlobalAllCell(list);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void setGlobalNeighboringCell(List<VCell> list) {
        try {
            getRemote().setGlobalNeighboringCell(list);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public void setLocation(int i, String str, VLocation vLocation) {
        try {
            getRemote().setLocation(i, str, vLocation);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public VLocation getLocation(int i, String str) {
        try {
            return getRemote().getLocation(i, str);
        } catch (RemoteException e2) {
            return (VLocation) VirtualRuntime.crash(e2);
        }
    }

    public void setGlobalLocation(VLocation vLocation) {
        try {
            getRemote().setGlobalLocation(vLocation);
        } catch (RemoteException e2) {
            VirtualRuntime.crash(e2);
        }
    }

    public VLocation getGlobalLocation() {
        try {
            return getRemote().getGlobalLocation();
        } catch (RemoteException e2) {
            return (VLocation) VirtualRuntime.crash(e2);
        }
    }
}
