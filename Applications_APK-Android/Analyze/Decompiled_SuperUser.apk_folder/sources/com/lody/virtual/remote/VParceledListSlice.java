package com.lody.virtual.remote;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class VParceledListSlice<T extends Parcelable> implements Parcelable {
    public static final Parcelable.ClassLoaderCreator<VParceledListSlice> CREATOR = new Parcelable.ClassLoaderCreator<VParceledListSlice>() {
        public VParceledListSlice createFromParcel(Parcel parcel) {
            return new VParceledListSlice(parcel, null);
        }

        public VParceledListSlice createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new VParceledListSlice(parcel, classLoader);
        }

        public VParceledListSlice[] newArray(int i) {
            return new VParceledListSlice[i];
        }
    };
    /* access modifiers changed from: private */
    public static boolean DEBUG = false;
    private static final int MAX_FIRST_IPC_SIZE = 131072;
    private static final int MAX_IPC_SIZE = 262144;
    /* access modifiers changed from: private */
    public static String TAG = "ParceledListSlice";
    /* access modifiers changed from: private */
    public final List<T> mList;

    public VParceledListSlice(List<T> list) {
        this.mList = list;
    }

    private VParceledListSlice(Parcel parcel, ClassLoader classLoader) {
        int i = 0;
        int readInt = parcel.readInt();
        this.mList = new ArrayList(readInt);
        if (DEBUG) {
            Log.d(TAG, "Retrieving " + readInt + " items");
        }
        if (readInt > 0) {
            Class<?> cls = null;
            while (i < readInt && parcel.readInt() != 0) {
                Parcelable readParcelable = parcel.readParcelable(classLoader);
                if (cls == null) {
                    cls = readParcelable.getClass();
                } else {
                    verifySameType(cls, readParcelable.getClass());
                }
                this.mList.add(readParcelable);
                if (DEBUG) {
                    Log.d(TAG, "Read inline #" + i + ": " + ((Object) this.mList.get(this.mList.size() - 1)));
                }
                i++;
            }
            if (i < readInt) {
                IBinder readStrongBinder = parcel.readStrongBinder();
                while (i < readInt) {
                    if (DEBUG) {
                        Log.d(TAG, "Reading more @" + i + " of " + readInt + ": retriever=" + readStrongBinder);
                    }
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    obtain.writeInt(i);
                    try {
                        readStrongBinder.transact(1, obtain, obtain2, 0);
                        while (i < readInt && obtain2.readInt() != 0) {
                            Parcelable readParcelable2 = obtain2.readParcelable(classLoader);
                            verifySameType(cls, readParcelable2.getClass());
                            this.mList.add(readParcelable2);
                            if (DEBUG) {
                                Log.d(TAG, "Read extra #" + i + ": " + ((Object) this.mList.get(this.mList.size() - 1)));
                            }
                            i++;
                        }
                        obtain2.recycle();
                        obtain.recycle();
                    } catch (RemoteException e2) {
                        Log.w(TAG, "Failure retrieving array; only received " + i + " of " + readInt, e2);
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void verifySameType(Class<?> cls, Class<?> cls2) {
        if (!cls2.equals(cls)) {
            throw new IllegalArgumentException("Can't unparcel type " + cls2.getName() + " in list of type " + cls.getName());
        }
    }

    public List<T> getList() {
        return this.mList;
    }

    public int describeContents() {
        int i = 0;
        for (int i2 = 0; i2 < this.mList.size(); i2++) {
            i |= ((Parcelable) this.mList.get(i2)).describeContents();
        }
        return i;
    }

    public void writeToParcel(Parcel parcel, final int i) {
        final int size = this.mList.size();
        parcel.writeInt(size);
        if (DEBUG) {
            Log.d(TAG, "Writing " + size + " items");
        }
        if (size > 0) {
            final Class<?> cls = ((Parcelable) this.mList.get(0)).getClass();
            int i2 = 0;
            while (i2 < size && parcel.dataSize() < MAX_FIRST_IPC_SIZE) {
                parcel.writeInt(1);
                Parcelable parcelable = (Parcelable) this.mList.get(i2);
                verifySameType(cls, parcelable.getClass());
                parcel.writeParcelable(parcelable, i);
                if (DEBUG) {
                    Log.d(TAG, "Wrote inline #" + i2 + ": " + ((Object) this.mList.get(i2)));
                }
                i2++;
            }
            if (i2 < size) {
                parcel.writeInt(0);
                AnonymousClass2 r0 = new Binder() {
                    /* access modifiers changed from: protected */
                    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
                        int i3;
                        if (i != 1) {
                            return super.onTransact(i, parcel, parcel2, i2);
                        }
                        int readInt = parcel.readInt();
                        if (VParceledListSlice.DEBUG) {
                            Log.d(VParceledListSlice.TAG, "Writing more @" + readInt + " of " + size);
                        }
                        while (true) {
                            i3 = readInt;
                            if (i3 < size && parcel2.dataSize() < VParceledListSlice.MAX_IPC_SIZE) {
                                parcel2.writeInt(1);
                                Parcelable parcelable = (Parcelable) VParceledListSlice.this.mList.get(i3);
                                VParceledListSlice.verifySameType(cls, parcelable.getClass());
                                parcel2.writeParcelable(parcelable, i);
                                if (VParceledListSlice.DEBUG) {
                                    Log.d(VParceledListSlice.TAG, "Wrote extra #" + i3 + ": " + VParceledListSlice.this.mList.get(i3));
                                }
                                readInt = i3 + 1;
                            }
                        }
                        if (i3 < size) {
                            if (VParceledListSlice.DEBUG) {
                                Log.d(VParceledListSlice.TAG, "Breaking @" + i3 + " of " + size);
                            }
                            parcel2.writeInt(0);
                        }
                        return true;
                    }
                };
                if (DEBUG) {
                    Log.d(TAG, "Breaking @" + i2 + " of " + size + ": retriever=" + r0);
                }
                parcel.writeStrongBinder(r0);
            }
        }
    }
}
