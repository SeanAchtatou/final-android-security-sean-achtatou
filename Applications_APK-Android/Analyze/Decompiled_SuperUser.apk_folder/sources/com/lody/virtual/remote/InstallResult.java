package com.lody.virtual.remote;

import android.os.Parcel;
import android.os.Parcelable;

public class InstallResult implements Parcelable {
    public static final Parcelable.Creator<InstallResult> CREATOR = new Parcelable.Creator<InstallResult>() {
        public InstallResult createFromParcel(Parcel parcel) {
            return new InstallResult(parcel);
        }

        public InstallResult[] newArray(int i) {
            return new InstallResult[i];
        }
    };
    public String error;
    public boolean isSuccess;
    public boolean isUpdate;
    public String packageName;

    public InstallResult() {
    }

    protected InstallResult(Parcel parcel) {
        boolean z;
        boolean z2 = true;
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.isSuccess = z;
        this.isUpdate = parcel.readByte() == 0 ? false : z2;
        this.packageName = parcel.readString();
        this.error = parcel.readString();
    }

    public static InstallResult makeFailure(String str) {
        InstallResult installResult = new InstallResult();
        installResult.error = str;
        return installResult;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        int i3 = 1;
        if (this.isSuccess) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        parcel.writeByte((byte) i2);
        if (!this.isUpdate) {
            i3 = 0;
        }
        parcel.writeByte((byte) i3);
        parcel.writeString(this.packageName);
        parcel.writeString(this.error);
    }

    public int describeContents() {
        return 0;
    }
}
