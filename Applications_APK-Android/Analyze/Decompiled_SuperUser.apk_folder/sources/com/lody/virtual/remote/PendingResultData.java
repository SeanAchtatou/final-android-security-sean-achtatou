package com.lody.virtual.remote;

import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import mirror.android.content.BroadcastReceiver;

public class PendingResultData implements Parcelable {
    public static final Parcelable.Creator<PendingResultData> CREATOR = new Parcelable.Creator<PendingResultData>() {
        public PendingResultData createFromParcel(Parcel parcel) {
            return new PendingResultData(parcel);
        }

        public PendingResultData[] newArray(int i) {
            return new PendingResultData[i];
        }
    };
    public boolean mAbortBroadcast;
    public boolean mFinished;
    public int mFlags;
    public boolean mInitialStickyHint;
    public boolean mOrderedHint;
    public int mResultCode;
    public String mResultData;
    public Bundle mResultExtras;
    public int mSendingUser;
    public IBinder mToken;
    public int mType;

    public PendingResultData(BroadcastReceiver.PendingResult pendingResult) {
        if (BroadcastReceiver.PendingResultMNC.ctor != null) {
            this.mType = BroadcastReceiver.PendingResultMNC.mType.get(pendingResult);
            this.mOrderedHint = BroadcastReceiver.PendingResultMNC.mOrderedHint.get(pendingResult);
            this.mInitialStickyHint = BroadcastReceiver.PendingResultMNC.mInitialStickyHint.get(pendingResult);
            this.mToken = BroadcastReceiver.PendingResultMNC.mToken.get(pendingResult);
            this.mSendingUser = BroadcastReceiver.PendingResultMNC.mSendingUser.get(pendingResult);
            this.mFlags = BroadcastReceiver.PendingResultMNC.mFlags.get(pendingResult);
            this.mResultCode = BroadcastReceiver.PendingResultMNC.mResultCode.get(pendingResult);
            this.mResultData = BroadcastReceiver.PendingResultMNC.mResultData.get(pendingResult);
            this.mResultExtras = BroadcastReceiver.PendingResultMNC.mResultExtras.get(pendingResult);
            this.mAbortBroadcast = BroadcastReceiver.PendingResultMNC.mAbortBroadcast.get(pendingResult);
            this.mFinished = BroadcastReceiver.PendingResultMNC.mFinished.get(pendingResult);
        } else if (BroadcastReceiver.PendingResultJBMR1.ctor != null) {
            this.mType = BroadcastReceiver.PendingResultJBMR1.mType.get(pendingResult);
            this.mOrderedHint = BroadcastReceiver.PendingResultJBMR1.mOrderedHint.get(pendingResult);
            this.mInitialStickyHint = BroadcastReceiver.PendingResultJBMR1.mInitialStickyHint.get(pendingResult);
            this.mToken = BroadcastReceiver.PendingResultJBMR1.mToken.get(pendingResult);
            this.mSendingUser = BroadcastReceiver.PendingResultJBMR1.mSendingUser.get(pendingResult);
            this.mResultCode = BroadcastReceiver.PendingResultJBMR1.mResultCode.get(pendingResult);
            this.mResultData = BroadcastReceiver.PendingResultJBMR1.mResultData.get(pendingResult);
            this.mResultExtras = BroadcastReceiver.PendingResultJBMR1.mResultExtras.get(pendingResult);
            this.mAbortBroadcast = BroadcastReceiver.PendingResultJBMR1.mAbortBroadcast.get(pendingResult);
            this.mFinished = BroadcastReceiver.PendingResultJBMR1.mFinished.get(pendingResult);
        } else {
            this.mType = BroadcastReceiver.PendingResult.mType.get(pendingResult);
            this.mOrderedHint = BroadcastReceiver.PendingResult.mOrderedHint.get(pendingResult);
            this.mInitialStickyHint = BroadcastReceiver.PendingResult.mInitialStickyHint.get(pendingResult);
            this.mToken = BroadcastReceiver.PendingResult.mToken.get(pendingResult);
            this.mResultCode = BroadcastReceiver.PendingResult.mResultCode.get(pendingResult);
            this.mResultData = BroadcastReceiver.PendingResult.mResultData.get(pendingResult);
            this.mResultExtras = BroadcastReceiver.PendingResult.mResultExtras.get(pendingResult);
            this.mAbortBroadcast = BroadcastReceiver.PendingResult.mAbortBroadcast.get(pendingResult);
            this.mFinished = BroadcastReceiver.PendingResult.mFinished.get(pendingResult);
        }
    }

    protected PendingResultData(Parcel parcel) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        this.mType = parcel.readInt();
        this.mOrderedHint = parcel.readByte() != 0;
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.mInitialStickyHint = z;
        this.mToken = parcel.readStrongBinder();
        this.mSendingUser = parcel.readInt();
        this.mFlags = parcel.readInt();
        this.mResultCode = parcel.readInt();
        this.mResultData = parcel.readString();
        this.mResultExtras = parcel.readBundle();
        if (parcel.readByte() != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.mAbortBroadcast = z2;
        this.mFinished = parcel.readByte() == 0 ? false : z3;
    }

    public BroadcastReceiver.PendingResult build() {
        if (BroadcastReceiver.PendingResultMNC.ctor != null) {
            return BroadcastReceiver.PendingResultMNC.ctor.newInstance(Integer.valueOf(this.mResultCode), this.mResultData, this.mResultExtras, Integer.valueOf(this.mType), Boolean.valueOf(this.mOrderedHint), Boolean.valueOf(this.mInitialStickyHint), this.mToken, Integer.valueOf(this.mSendingUser), Integer.valueOf(this.mFlags));
        } else if (BroadcastReceiver.PendingResultJBMR1.ctor != null) {
            return BroadcastReceiver.PendingResultJBMR1.ctor.newInstance(Integer.valueOf(this.mResultCode), this.mResultData, this.mResultExtras, Integer.valueOf(this.mType), Boolean.valueOf(this.mOrderedHint), Boolean.valueOf(this.mInitialStickyHint), this.mToken, Integer.valueOf(this.mSendingUser));
        } else {
            return BroadcastReceiver.PendingResult.ctor.newInstance(Integer.valueOf(this.mResultCode), this.mResultData, this.mResultExtras, Integer.valueOf(this.mType), Boolean.valueOf(this.mOrderedHint), Boolean.valueOf(this.mInitialStickyHint), this.mToken);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        byte b2;
        byte b3;
        byte b4 = 1;
        parcel.writeInt(this.mType);
        parcel.writeByte(this.mOrderedHint ? (byte) 1 : 0);
        if (this.mInitialStickyHint) {
            b2 = 1;
        } else {
            b2 = 0;
        }
        parcel.writeByte(b2);
        parcel.writeStrongBinder(this.mToken);
        parcel.writeInt(this.mSendingUser);
        parcel.writeInt(this.mFlags);
        parcel.writeInt(this.mResultCode);
        parcel.writeString(this.mResultData);
        parcel.writeBundle(this.mResultExtras);
        if (this.mAbortBroadcast) {
            b3 = 1;
        } else {
            b3 = 0;
        }
        parcel.writeByte(b3);
        if (!this.mFinished) {
            b4 = 0;
        }
        parcel.writeByte(b4);
    }

    public void finish() {
        try {
            build().finish();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
