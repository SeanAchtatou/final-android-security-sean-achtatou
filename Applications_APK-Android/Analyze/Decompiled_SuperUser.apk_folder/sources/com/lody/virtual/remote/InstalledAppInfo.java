package com.lody.virtual.remote;

import android.content.pm.ApplicationInfo;
import android.os.Parcel;
import android.os.Parcelable;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VPackageManager;
import com.lody.virtual.os.VEnvironment;
import java.io.File;

public final class InstalledAppInfo implements Parcelable {
    public static final Parcelable.Creator<InstalledAppInfo> CREATOR = new Parcelable.Creator<InstalledAppInfo>() {
        public InstalledAppInfo createFromParcel(Parcel parcel) {
            return new InstalledAppInfo(parcel);
        }

        public InstalledAppInfo[] newArray(int i) {
            return new InstalledAppInfo[i];
        }
    };
    public String apkPath;
    public int appId;
    public boolean dependSystem;
    public String libPath;
    public String packageName;
    public boolean skipDexOpt;

    public InstalledAppInfo(String str, String str2, String str3, boolean z, boolean z2, int i) {
        this.packageName = str;
        this.apkPath = str2;
        this.libPath = str3;
        this.dependSystem = z;
        this.skipDexOpt = z2;
        this.appId = i;
    }

    public File getOdexFile() {
        return VEnvironment.getOdexFile(this.packageName);
    }

    public ApplicationInfo getApplicationInfo(int i) {
        return VPackageManager.get().getApplicationInfo(this.packageName, 0, i);
    }

    public int[] getInstalledUsers() {
        return VirtualCore.get().getPackageInstalledUsers(this.packageName);
    }

    public boolean isLaunched(int i) {
        return VirtualCore.get().isPackageLaunched(i, this.packageName);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        byte b2;
        byte b3 = 1;
        parcel.writeString(this.packageName);
        parcel.writeString(this.apkPath);
        parcel.writeString(this.libPath);
        if (this.dependSystem) {
            b2 = 1;
        } else {
            b2 = 0;
        }
        parcel.writeByte(b2);
        if (!this.skipDexOpt) {
            b3 = 0;
        }
        parcel.writeByte(b3);
        parcel.writeInt(this.appId);
    }

    protected InstalledAppInfo(Parcel parcel) {
        boolean z;
        boolean z2 = true;
        this.packageName = parcel.readString();
        this.apkPath = parcel.readString();
        this.libPath = parcel.readString();
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.dependSystem = z;
        this.skipDexOpt = parcel.readByte() == 0 ? false : z2;
        this.appId = parcel.readInt();
    }
}
