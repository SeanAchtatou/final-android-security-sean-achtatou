package com.lody.virtual.remote;

import android.os.Parcel;
import android.os.Parcelable;
import com.lody.virtual.os.VUserHandle;

public class BadgerInfo implements Parcelable {
    public static final Parcelable.Creator<BadgerInfo> CREATOR = new Parcelable.Creator<BadgerInfo>() {
        public BadgerInfo createFromParcel(Parcel parcel) {
            return new BadgerInfo(parcel);
        }

        public BadgerInfo[] newArray(int i) {
            return new BadgerInfo[i];
        }
    };
    public int badgerCount;
    public String className;
    public String packageName;
    public int userId;

    public BadgerInfo() {
        this.userId = VUserHandle.myUserId();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.userId);
        parcel.writeString(this.packageName);
        parcel.writeInt(this.badgerCount);
        parcel.writeString(this.className);
    }

    protected BadgerInfo(Parcel parcel) {
        this.userId = parcel.readInt();
        this.packageName = parcel.readString();
        this.badgerCount = parcel.readInt();
        this.className = parcel.readString();
    }
}
