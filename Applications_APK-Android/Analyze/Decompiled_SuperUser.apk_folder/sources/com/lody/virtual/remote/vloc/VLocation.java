package com.lody.virtual.remote.vloc;

import android.os.Parcel;
import android.os.Parcelable;

public class VLocation implements Parcelable {
    public static final Parcelable.Creator<VLocation> CREATOR = new Parcelable.Creator<VLocation>() {
        public VLocation createFromParcel(Parcel parcel) {
            return new VLocation(parcel);
        }

        public VLocation[] newArray(int i) {
            return new VLocation[i];
        }
    };
    public float accuracy = 0.0f;
    public double altitude = 0.0d;
    public float bearing;
    public double latitude = 0.0d;
    public double longitude = 0.0d;
    public float speed;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.latitude);
        parcel.writeDouble(this.longitude);
        parcel.writeDouble(this.altitude);
        parcel.writeFloat(this.accuracy);
        parcel.writeFloat(this.speed);
        parcel.writeFloat(this.bearing);
    }

    public VLocation() {
    }

    public VLocation(Parcel parcel) {
        this.latitude = parcel.readDouble();
        this.longitude = parcel.readDouble();
        this.altitude = parcel.readDouble();
        this.accuracy = parcel.readFloat();
        this.speed = parcel.readFloat();
        this.bearing = parcel.readFloat();
    }
}
