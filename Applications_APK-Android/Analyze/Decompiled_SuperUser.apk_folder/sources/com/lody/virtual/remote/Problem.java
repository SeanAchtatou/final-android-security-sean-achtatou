package com.lody.virtual.remote;

import android.os.Parcel;
import android.os.Parcelable;

public class Problem implements Parcelable {
    public static final Parcelable.Creator<Problem> CREATOR = new Parcelable.Creator<Problem>() {
        public Problem createFromParcel(Parcel parcel) {
            return new Problem(parcel);
        }

        public Problem[] newArray(int i) {
            return new Problem[i];
        }
    };

    /* renamed from: e  reason: collision with root package name */
    public Throwable f4468e;

    public Problem(Throwable th) {
        this.f4468e = th;
    }

    protected Problem(Parcel parcel) {
        this.f4468e = (Throwable) parcel.readSerializable();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.f4468e);
    }
}
