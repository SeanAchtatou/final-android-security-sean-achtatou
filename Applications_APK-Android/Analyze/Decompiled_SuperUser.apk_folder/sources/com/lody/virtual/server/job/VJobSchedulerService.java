package com.lody.virtual.server.job;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.text.TextUtils;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VJobScheduler;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.utils.Singleton;
import com.lody.virtual.os.VBinder;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.server.IJobScheduler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

@TargetApi(21)
public class VJobSchedulerService extends IJobScheduler.Stub {
    private static final int JOB_FILE_VERSION = 1;
    private static final String TAG = VJobScheduler.class.getSimpleName();
    private static final Singleton<VJobSchedulerService> gDefault = new Singleton<VJobSchedulerService>() {
        /* access modifiers changed from: protected */
        public VJobSchedulerService create() {
            return new VJobSchedulerService();
        }
    };
    private int mGlobalJobId;
    private final ComponentName mJobProxyComponent;
    private final Map<JobId, JobConfig> mJobStore;
    private final JobScheduler mScheduler;

    private VJobSchedulerService() {
        this.mJobStore = new HashMap();
        this.mScheduler = (JobScheduler) VirtualCore.get().getContext().getSystemService("jobscheduler");
        this.mJobProxyComponent = new ComponentName(VirtualCore.get().getHostPkg(), VASettings.STUB_JOB);
        readJobs();
    }

    public static VJobSchedulerService get() {
        return gDefault.get();
    }

    public static final class JobId implements Parcelable {
        public static final Parcelable.Creator<JobId> CREATOR = new Parcelable.Creator<JobId>() {
            public JobId createFromParcel(Parcel parcel) {
                return new JobId(parcel);
            }

            public JobId[] newArray(int i) {
                return new JobId[i];
            }
        };
        public int clientJobId;
        public String packageName;
        public int vuid;

        JobId(int i, String str, int i2) {
            this.vuid = i;
            this.packageName = str;
            this.clientJobId = i2;
        }

        JobId(Parcel parcel) {
            this.vuid = parcel.readInt();
            this.packageName = parcel.readString();
            this.clientJobId = parcel.readInt();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            JobId jobId = (JobId) obj;
            if (this.vuid == jobId.vuid && this.clientJobId == jobId.clientJobId && TextUtils.equals(this.packageName, jobId.packageName)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((this.packageName != null ? this.packageName.hashCode() : 0) + (this.vuid * 31)) * 31) + this.clientJobId;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.vuid);
            parcel.writeString(this.packageName);
            parcel.writeInt(this.clientJobId);
        }
    }

    public static final class JobConfig implements Parcelable {
        public static final Parcelable.Creator<JobConfig> CREATOR = new Parcelable.Creator<JobConfig>() {
            public JobConfig createFromParcel(Parcel parcel) {
                return new JobConfig(parcel);
            }

            public JobConfig[] newArray(int i) {
                return new JobConfig[i];
            }
        };
        public PersistableBundle extras;
        public String serviceName;
        public int virtualJobId;

        JobConfig(int i, String str, PersistableBundle persistableBundle) {
            this.virtualJobId = i;
            this.serviceName = str;
            this.extras = persistableBundle;
        }

        JobConfig(Parcel parcel) {
            this.virtualJobId = parcel.readInt();
            this.serviceName = parcel.readString();
            this.extras = (PersistableBundle) parcel.readParcelable(PersistableBundle.class.getClassLoader());
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.virtualJobId);
            parcel.writeString(this.serviceName);
            parcel.writeParcelable(this.extras, i);
        }
    }

    public int schedule(JobInfo jobInfo) {
        int callingUid = VBinder.getCallingUid();
        int id = jobInfo.getId();
        ComponentName service = jobInfo.getService();
        JobId jobId = new JobId(callingUid, service.getPackageName(), id);
        JobConfig jobConfig = this.mJobStore.get(jobId);
        if (jobConfig == null) {
            int i = this.mGlobalJobId;
            this.mGlobalJobId = i + 1;
            jobConfig = new JobConfig(i, service.getClassName(), jobInfo.getExtras());
            this.mJobStore.put(jobId, jobConfig);
        } else {
            jobConfig.serviceName = service.getClassName();
            jobConfig.extras = jobInfo.getExtras();
        }
        saveJobs();
        mirror.android.app.job.JobInfo.jobId.set(jobInfo, jobConfig.virtualJobId);
        mirror.android.app.job.JobInfo.service.set(jobInfo, this.mJobProxyComponent);
        return this.mScheduler.schedule(jobInfo);
    }

    private void saveJobs() {
        File jobConfigFile = VEnvironment.getJobConfigFile();
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInt(1);
            obtain.writeInt(this.mJobStore.size());
            for (Map.Entry next : this.mJobStore.entrySet()) {
                ((JobId) next.getKey()).writeToParcel(obtain, 0);
                ((JobConfig) next.getValue()).writeToParcel(obtain, 0);
            }
            FileOutputStream fileOutputStream = new FileOutputStream(jobConfigFile);
            fileOutputStream.write(obtain.marshall());
            fileOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            obtain.recycle();
        }
    }

    private void readJobs() {
        File jobConfigFile = VEnvironment.getJobConfigFile();
        if (jobConfigFile.exists()) {
            Parcel obtain = Parcel.obtain();
            try {
                FileInputStream fileInputStream = new FileInputStream(jobConfigFile);
                byte[] bArr = new byte[((int) jobConfigFile.length())];
                int read = fileInputStream.read(bArr);
                fileInputStream.close();
                if (read != bArr.length) {
                    throw new IOException("Unable to read job config.");
                }
                obtain.unmarshall(bArr, 0, bArr.length);
                obtain.setDataPosition(0);
                int readInt = obtain.readInt();
                if (readInt != 1) {
                    throw new IOException("Bad version of job file: " + readInt);
                }
                if (!this.mJobStore.isEmpty()) {
                    this.mJobStore.clear();
                }
                int readInt2 = obtain.readInt();
                for (int i = 0; i < readInt2; i++) {
                    JobId jobId = new JobId(obtain);
                    JobConfig jobConfig = new JobConfig(obtain);
                    this.mJobStore.put(jobId, jobConfig);
                    this.mGlobalJobId = Math.max(this.mGlobalJobId, jobConfig.virtualJobId);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            } finally {
                obtain.recycle();
            }
        }
    }

    public void cancel(int i) {
        boolean z;
        int callingUid = VBinder.getCallingUid();
        synchronized (this.mJobStore) {
            Iterator<Map.Entry<JobId, JobConfig>> it = this.mJobStore.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                Map.Entry next = it.next();
                JobId jobId = (JobId) next.getKey();
                JobConfig jobConfig = (JobConfig) next.getValue();
                if (jobId.vuid == callingUid && jobId.clientJobId == i) {
                    this.mScheduler.cancel(jobConfig.virtualJobId);
                    it.remove();
                    z = true;
                    break;
                }
            }
            if (z) {
                saveJobs();
            }
        }
    }

    public void cancelAll() {
        boolean z;
        int callingUid = VBinder.getCallingUid();
        synchronized (this.mJobStore) {
            Iterator<Map.Entry<JobId, JobConfig>> it = this.mJobStore.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                Map.Entry next = it.next();
                if (((JobId) next.getKey()).vuid == callingUid) {
                    this.mScheduler.cancel(((JobConfig) next.getValue()).virtualJobId);
                    z = true;
                    it.remove();
                    break;
                }
            }
            if (z) {
                saveJobs();
            }
        }
    }

    public List<JobInfo> getAllPendingJobs() {
        int callingUid = VBinder.getCallingUid();
        List<JobInfo> allPendingJobs = this.mScheduler.getAllPendingJobs();
        synchronized (this.mJobStore) {
            ListIterator<JobInfo> listIterator = allPendingJobs.listIterator();
            while (listIterator.hasNext()) {
                JobInfo next = listIterator.next();
                if (!VASettings.STUB_JOB.equals(next.getService().getClassName())) {
                    listIterator.remove();
                } else {
                    Map.Entry<JobId, JobConfig> findJobByVirtualJobId = findJobByVirtualJobId(next.getId());
                    if (findJobByVirtualJobId == null) {
                        listIterator.remove();
                    } else {
                        JobId key = findJobByVirtualJobId.getKey();
                        JobConfig value = findJobByVirtualJobId.getValue();
                        if (key.vuid != callingUid) {
                            listIterator.remove();
                        } else {
                            mirror.android.app.job.JobInfo.jobId.set(next, key.clientJobId);
                            mirror.android.app.job.JobInfo.service.set(next, new ComponentName(key.packageName, value.serviceName));
                        }
                    }
                }
            }
        }
        return allPendingJobs;
    }

    public Map.Entry<JobId, JobConfig> findJobByVirtualJobId(int i) {
        Map.Entry<JobId, JobConfig> entry;
        synchronized (this.mJobStore) {
            Iterator<Map.Entry<JobId, JobConfig>> it = this.mJobStore.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    entry = null;
                    break;
                }
                entry = it.next();
                if (entry.getValue().virtualJobId == i) {
                    break;
                }
            }
        }
        return entry;
    }
}
