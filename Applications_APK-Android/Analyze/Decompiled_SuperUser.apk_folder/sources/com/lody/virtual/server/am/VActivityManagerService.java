package com.lody.virtual.server.am;

import android.app.ActivityManager;
import android.app.IServiceConnection;
import android.app.IStopUserCallback;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import com.lody.virtual.client.IVClient;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.Constants;
import com.lody.virtual.client.env.SpecialComponentList;
import com.lody.virtual.client.ipc.ProviderCall;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.ipc.VNotificationManager;
import com.lody.virtual.client.stub.ChooseTypeAndAccountActivity;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.collection.ArrayMap;
import com.lody.virtual.helper.collection.SparseArray;
import com.lody.virtual.helper.compat.ApplicationThreadCompat;
import com.lody.virtual.helper.compat.BundleCompat;
import com.lody.virtual.helper.compat.IApplicationThreadCompat;
import com.lody.virtual.helper.utils.ComponentUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VBinder;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.AppTaskInfo;
import com.lody.virtual.remote.BadgerInfo;
import com.lody.virtual.remote.PendingIntentData;
import com.lody.virtual.remote.PendingResultData;
import com.lody.virtual.remote.VParceledListSlice;
import com.lody.virtual.server.IActivityManager;
import com.lody.virtual.server.am.ServiceRecord;
import com.lody.virtual.server.interfaces.IProcessObserver;
import com.lody.virtual.server.pm.PackageCacheManager;
import com.lody.virtual.server.pm.PackageSetting;
import com.lody.virtual.server.pm.VAppManagerService;
import com.lody.virtual.server.pm.VPackageManagerService;
import com.lody.virtual.server.secondary.BinderDelegateService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class VActivityManagerService extends IActivityManager.Stub {
    private static final boolean BROADCAST_NOT_STARTED_PKG = false;
    private static final String TAG = VActivityManagerService.class.getSimpleName();
    private static final AtomicReference<VActivityManagerService> sService = new AtomicReference<>();
    private ActivityManager am = ((ActivityManager) VirtualCore.get().getContext().getSystemService(ServiceManagerNative.ACTIVITY));
    private final Set<ServiceRecord> mHistory = new HashSet();
    private final ActivityStack mMainStack = new ActivityStack(this);
    private final PendingIntents mPendingIntents = new PendingIntents();
    private final SparseArray<ProcessRecord> mPidsSelfLocked = new SparseArray<>();
    private final ProcessMap<ProcessRecord> mProcessNames = new ProcessMap<>();
    private NotificationManager nm = ((NotificationManager) VirtualCore.get().getContext().getSystemService(ServiceManagerNative.NOTIFICATION));

    public static VActivityManagerService get() {
        return sService.get();
    }

    public static void systemReady(Context context) {
        new VActivityManagerService().onCreate(context);
    }

    private static ServiceInfo resolveServiceInfo(Intent intent, int i) {
        ServiceInfo resolveServiceInfo;
        if (intent == null || (resolveServiceInfo = VirtualCore.get().resolveServiceInfo(intent, i)) == null) {
            return null;
        }
        return resolveServiceInfo;
    }

    public void onCreate(Context context) {
        PackageInfo packageInfo;
        AttributeCache.init(context);
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 137);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            throw new RuntimeException("Unable to found PackageInfo : " + context.getPackageName());
        }
        sService.set(this);
    }

    public int startActivity(Intent intent, ActivityInfo activityInfo, IBinder iBinder, Bundle bundle, String str, int i, int i2) {
        int startActivityLocked;
        synchronized (this) {
            startActivityLocked = this.mMainStack.startActivityLocked(i2, intent, activityInfo, iBinder, bundle, str, i);
        }
        return startActivityLocked;
    }

    public int startActivities(Intent[] intentArr, String[] strArr, IBinder iBinder, Bundle bundle, int i) {
        int startActivitiesLocked;
        synchronized (this) {
            ActivityInfo[] activityInfoArr = new ActivityInfo[intentArr.length];
            int i2 = 0;
            while (true) {
                if (i2 >= intentArr.length) {
                    startActivitiesLocked = this.mMainStack.startActivitiesLocked(i, intentArr, activityInfoArr, strArr, iBinder, bundle);
                    break;
                }
                ActivityInfo resolveActivityInfo = VirtualCore.get().resolveActivityInfo(intentArr[i2], i);
                if (resolveActivityInfo == null) {
                    startActivitiesLocked = -1;
                    break;
                }
                activityInfoArr[i2] = resolveActivityInfo;
                i2++;
            }
        }
        return startActivitiesLocked;
    }

    public String getPackageForIntentSender(IBinder iBinder) {
        PendingIntentData pendingIntent = this.mPendingIntents.getPendingIntent(iBinder);
        if (pendingIntent != null) {
            return pendingIntent.creator;
        }
        return null;
    }

    public PendingIntentData getPendingIntent(IBinder iBinder) {
        return this.mPendingIntents.getPendingIntent(iBinder);
    }

    public void addPendingIntent(IBinder iBinder, String str) {
        this.mPendingIntents.addPendingIntent(iBinder, str);
    }

    public void removePendingIntent(IBinder iBinder) {
        this.mPendingIntents.removePendingIntent(iBinder);
    }

    public int getSystemPid() {
        return VirtualCore.get().myUid();
    }

    public void onActivityCreated(ComponentName componentName, ComponentName componentName2, IBinder iBinder, Intent intent, String str, int i, int i2, int i3) {
        ProcessRecord findProcessLocked = findProcessLocked(Binder.getCallingPid());
        if (findProcessLocked != null) {
            this.mMainStack.onActivityCreated(findProcessLocked, componentName, componentName2, iBinder, intent, str, i, i2, i3);
        }
    }

    public void onActivityResumed(int i, IBinder iBinder) {
        this.mMainStack.onActivityResumed(i, iBinder);
    }

    public boolean onActivityDestroyed(int i, IBinder iBinder) {
        return this.mMainStack.onActivityDestroyed(i, iBinder) != null;
    }

    public AppTaskInfo getTaskInfo(int i) {
        return this.mMainStack.getTaskInfo(i);
    }

    public String getPackageForToken(int i, IBinder iBinder) {
        return this.mMainStack.getPackageForToken(i, iBinder);
    }

    public ComponentName getActivityClassForToken(int i, IBinder iBinder) {
        return this.mMainStack.getActivityClassForToken(i, iBinder);
    }

    private void processDead(ProcessRecord processRecord) {
        synchronized (this.mHistory) {
            Iterator<ServiceRecord> it = this.mHistory.iterator();
            while (it.hasNext()) {
                ServiceRecord next = it.next();
                if (next.process != null && next.process.pid == processRecord.pid) {
                    it.remove();
                }
            }
            this.mMainStack.processDied(processRecord);
        }
    }

    public IBinder acquireProviderClient(int i, ProviderInfo providerInfo) {
        ProcessRecord findProcessLocked;
        ProcessRecord startProcessIfNeedLocked;
        synchronized (this.mPidsSelfLocked) {
            findProcessLocked = findProcessLocked(VBinder.getCallingPid());
        }
        if (findProcessLocked == null) {
            throw new SecurityException("Who are you?");
        }
        String str = providerInfo.processName;
        synchronized (this) {
            startProcessIfNeedLocked = startProcessIfNeedLocked(str, i, providerInfo.packageName);
        }
        if (startProcessIfNeedLocked != null && startProcessIfNeedLocked.client.asBinder().isBinderAlive()) {
            try {
                return startProcessIfNeedLocked.client.acquireProviderClient(providerInfo);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public ComponentName getCallingActivity(int i, IBinder iBinder) {
        return this.mMainStack.getCallingActivity(i, iBinder);
    }

    public String getCallingPackage(int i, IBinder iBinder) {
        return this.mMainStack.getCallingPackage(i, iBinder);
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        try {
            return super.onTransact(i, parcel, parcel2, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    private void addRecord(ServiceRecord serviceRecord) {
        this.mHistory.add(serviceRecord);
    }

    private ServiceRecord findRecordLocked(int i, ServiceInfo serviceInfo) {
        ServiceRecord serviceRecord;
        synchronized (this.mHistory) {
            Iterator<ServiceRecord> it = this.mHistory.iterator();
            while (true) {
                if (!it.hasNext()) {
                    serviceRecord = null;
                    break;
                }
                serviceRecord = it.next();
                if ((serviceRecord.process == null || serviceRecord.process.userId == i) && ComponentUtils.isSameComponent(serviceInfo, serviceRecord.serviceInfo)) {
                    break;
                }
            }
        }
        return serviceRecord;
    }

    private ServiceRecord findRecordLocked(IServiceConnection iServiceConnection) {
        ServiceRecord serviceRecord;
        synchronized (this.mHistory) {
            Iterator<ServiceRecord> it = this.mHistory.iterator();
            while (true) {
                if (!it.hasNext()) {
                    serviceRecord = null;
                    break;
                }
                serviceRecord = it.next();
                if (serviceRecord.containConnection(iServiceConnection)) {
                    break;
                }
            }
        }
        return serviceRecord;
    }

    public ComponentName startService(IBinder iBinder, Intent intent, String str, int i) {
        ComponentName startServiceCommon;
        synchronized (this) {
            startServiceCommon = startServiceCommon(intent, true, i);
        }
        return startServiceCommon;
    }

    private ComponentName startServiceCommon(Intent intent, boolean z, int i) {
        boolean z2 = false;
        ServiceInfo resolveServiceInfo = resolveServiceInfo(intent, i);
        if (resolveServiceInfo == null) {
            return null;
        }
        ProcessRecord startProcessIfNeedLocked = startProcessIfNeedLocked(ComponentUtils.getProcessName(resolveServiceInfo), i, resolveServiceInfo.packageName);
        if (startProcessIfNeedLocked == null) {
            VLog.e(TAG, "Unable to start new Process for : " + ComponentUtils.toComponentName(resolveServiceInfo), new Object[0]);
            return null;
        }
        IInterface iInterface = startProcessIfNeedLocked.appThread;
        ServiceRecord findRecordLocked = findRecordLocked(i, resolveServiceInfo);
        if (findRecordLocked == null) {
            findRecordLocked = new ServiceRecord();
            findRecordLocked.startId = 0;
            findRecordLocked.activeSince = SystemClock.elapsedRealtime();
            findRecordLocked.process = startProcessIfNeedLocked;
            findRecordLocked.serviceInfo = resolveServiceInfo;
            try {
                IApplicationThreadCompat.scheduleCreateService(iInterface, findRecordLocked, findRecordLocked.serviceInfo, 0);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
            addRecord(findRecordLocked);
        }
        findRecordLocked.lastActivityTime = SystemClock.uptimeMillis();
        if (z) {
            findRecordLocked.startId++;
            if (resolveServiceInfo.applicationInfo != null && resolveServiceInfo.applicationInfo.targetSdkVersion < 5) {
                z2 = true;
            }
            try {
                IApplicationThreadCompat.scheduleServiceArgs(iInterface, findRecordLocked, z2, findRecordLocked.startId, 0, intent);
            } catch (RemoteException e3) {
                e3.printStackTrace();
            }
        }
        return ComponentUtils.toComponentName(resolveServiceInfo);
    }

    public int stopService(IBinder iBinder, Intent intent, String str, int i) {
        int i2 = 0;
        synchronized (this) {
            ServiceInfo resolveServiceInfo = resolveServiceInfo(intent, i);
            if (resolveServiceInfo != null) {
                ServiceRecord findRecordLocked = findRecordLocked(i, resolveServiceInfo);
                if (findRecordLocked != null) {
                    stopServiceCommon(findRecordLocked, ComponentUtils.toComponentName(resolveServiceInfo));
                    i2 = 1;
                }
            }
        }
        return i2;
    }

    public boolean stopServiceToken(ComponentName componentName, IBinder iBinder, int i, int i2) {
        boolean z;
        synchronized (this) {
            ServiceRecord serviceRecord = (ServiceRecord) iBinder;
            if (serviceRecord == null || !(serviceRecord.startId == i || i == -1)) {
                z = false;
            } else {
                stopServiceCommon(serviceRecord, componentName);
                z = true;
            }
        }
        return z;
    }

    private void stopServiceCommon(ServiceRecord serviceRecord, ComponentName componentName) {
        for (ServiceRecord.IntentBindRecord next : serviceRecord.bindings) {
            for (IServiceConnection connected : next.connections) {
                try {
                    connected.connected(componentName, null);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                IApplicationThreadCompat.scheduleUnbindService(serviceRecord.process.appThread, serviceRecord, next.intent);
            } catch (RemoteException e3) {
                e3.printStackTrace();
            }
        }
        try {
            IApplicationThreadCompat.scheduleStopService(serviceRecord.process.appThread, serviceRecord);
        } catch (RemoteException e4) {
            e4.printStackTrace();
        }
        this.mHistory.remove(serviceRecord);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int bindService(android.os.IBinder r7, android.os.IBinder r8, android.content.Intent r9, java.lang.String r10, android.app.IServiceConnection r11, int r12, int r13) {
        /*
            r6 = this;
            r1 = 1
            r0 = 0
            monitor-enter(r6)
            android.content.pm.ServiceInfo r4 = resolveServiceInfo(r9, r13)     // Catch:{ all -> 0x0024 }
            if (r4 != 0) goto L_0x000b
            monitor-exit(r6)     // Catch:{ all -> 0x0024 }
        L_0x000a:
            return r0
        L_0x000b:
            com.lody.virtual.server.am.ServiceRecord r2 = r6.findRecordLocked(r13, r4)     // Catch:{ all -> 0x0024 }
            if (r2 != 0) goto L_0x0027
            r3 = r1
        L_0x0012:
            if (r3 == 0) goto L_0x0020
            r3 = r12 & 1
            if (r3 == 0) goto L_0x0020
            r2 = 0
            r6.startServiceCommon(r9, r2, r13)     // Catch:{ all -> 0x0024 }
            com.lody.virtual.server.am.ServiceRecord r2 = r6.findRecordLocked(r13, r4)     // Catch:{ all -> 0x0024 }
        L_0x0020:
            if (r2 != 0) goto L_0x0029
            monitor-exit(r6)     // Catch:{ all -> 0x0024 }
            goto L_0x000a
        L_0x0024:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0024 }
            throw r0
        L_0x0027:
            r3 = r0
            goto L_0x0012
        L_0x0029:
            com.lody.virtual.server.am.ServiceRecord$IntentBindRecord r3 = r2.peekBinding(r9)     // Catch:{ all -> 0x0024 }
            if (r3 == 0) goto L_0x0069
            android.os.IBinder r0 = r3.binder     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x0069
            android.os.IBinder r0 = r3.binder     // Catch:{ all -> 0x0024 }
            boolean r0 = r0.isBinderAlive()     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x0069
            boolean r0 = r3.doRebind     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x0048
            com.lody.virtual.server.am.ProcessRecord r0 = r2.process     // Catch:{ RemoteException -> 0x0064 }
            android.os.IInterface r0 = r0.appThread     // Catch:{ RemoteException -> 0x0064 }
            r4 = 1
            r5 = 0
            com.lody.virtual.helper.compat.IApplicationThreadCompat.scheduleBindService(r0, r2, r9, r4, r5)     // Catch:{ RemoteException -> 0x0064 }
        L_0x0048:
            android.content.ComponentName r0 = new android.content.ComponentName     // Catch:{ all -> 0x0024 }
            android.content.pm.ServiceInfo r4 = r2.serviceInfo     // Catch:{ all -> 0x0024 }
            java.lang.String r4 = r4.packageName     // Catch:{ all -> 0x0024 }
            android.content.pm.ServiceInfo r5 = r2.serviceInfo     // Catch:{ all -> 0x0024 }
            java.lang.String r5 = r5.name     // Catch:{ all -> 0x0024 }
            r0.<init>(r4, r5)     // Catch:{ all -> 0x0024 }
            r6.connectService(r11, r0, r3)     // Catch:{ all -> 0x0024 }
        L_0x0058:
            long r4 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0024 }
            r2.lastActivityTime = r4     // Catch:{ all -> 0x0024 }
            r2.addToBoundIntent(r9, r11)     // Catch:{ all -> 0x0024 }
            monitor-exit(r6)     // Catch:{ all -> 0x0024 }
            r0 = r1
            goto L_0x000a
        L_0x0064:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0024 }
            goto L_0x0048
        L_0x0069:
            com.lody.virtual.server.am.ProcessRecord r0 = r2.process     // Catch:{ RemoteException -> 0x0073 }
            android.os.IInterface r0 = r0.appThread     // Catch:{ RemoteException -> 0x0073 }
            r3 = 0
            r4 = 0
            com.lody.virtual.helper.compat.IApplicationThreadCompat.scheduleBindService(r0, r2, r9, r3, r4)     // Catch:{ RemoteException -> 0x0073 }
            goto L_0x0058
        L_0x0073:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0024 }
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.am.VActivityManagerService.bindService(android.os.IBinder, android.os.IBinder, android.content.Intent, java.lang.String, android.app.IServiceConnection, int, int):int");
    }

    public boolean unbindService(IServiceConnection iServiceConnection, int i) {
        boolean z;
        synchronized (this) {
            ServiceRecord findRecordLocked = findRecordLocked(iServiceConnection);
            if (findRecordLocked == null) {
                z = false;
            } else {
                for (ServiceRecord.IntentBindRecord next : findRecordLocked.bindings) {
                    if (next.containConnection(iServiceConnection)) {
                        next.removeConnection(iServiceConnection);
                        try {
                            IApplicationThreadCompat.scheduleUnbindService(findRecordLocked.process.appThread, findRecordLocked, next.intent);
                        } catch (RemoteException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                if (findRecordLocked.startId <= 0 && findRecordLocked.getConnectionCount() <= 0) {
                    try {
                        IApplicationThreadCompat.scheduleStopService(findRecordLocked.process.appThread, findRecordLocked);
                    } catch (RemoteException e3) {
                        e3.printStackTrace();
                    }
                    if (Build.VERSION.SDK_INT < 21) {
                        this.mHistory.remove(findRecordLocked);
                    }
                }
                z = true;
            }
        }
        return z;
    }

    public void unbindFinished(IBinder iBinder, Intent intent, boolean z, int i) {
        ServiceRecord.IntentBindRecord peekBinding;
        synchronized (this) {
            ServiceRecord serviceRecord = (ServiceRecord) iBinder;
            if (!(serviceRecord == null || (peekBinding = serviceRecord.peekBinding(intent)) == null)) {
                peekBinding.doRebind = z;
            }
        }
    }

    public boolean isVAServiceToken(IBinder iBinder) {
        return iBinder instanceof ServiceRecord;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void serviceDoneExecuting(android.os.IBinder r2, int r3, int r4, int r5, int r6) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.lody.virtual.server.am.ServiceRecord r2 = (com.lody.virtual.server.am.ServiceRecord) r2     // Catch:{ all -> 0x0011 }
            if (r2 != 0) goto L_0x0007
            monitor-exit(r1)     // Catch:{ all -> 0x0011 }
        L_0x0006:
            return
        L_0x0007:
            r0 = 2
            if (r0 != r3) goto L_0x000f
            java.util.Set<com.lody.virtual.server.am.ServiceRecord> r0 = r1.mHistory     // Catch:{ all -> 0x0011 }
            r0.remove(r2)     // Catch:{ all -> 0x0011 }
        L_0x000f:
            monitor-exit(r1)     // Catch:{ all -> 0x0011 }
            goto L_0x0006
        L_0x0011:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0011 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.am.VActivityManagerService.serviceDoneExecuting(android.os.IBinder, int, int, int, int):void");
    }

    public IBinder peekService(Intent intent, String str, int i) {
        ServiceRecord.IntentBindRecord peekBinding;
        IBinder iBinder = null;
        synchronized (this) {
            ServiceInfo resolveServiceInfo = resolveServiceInfo(intent, i);
            if (resolveServiceInfo != null) {
                ServiceRecord findRecordLocked = findRecordLocked(i, resolveServiceInfo);
                if (findRecordLocked != null && (peekBinding = findRecordLocked.peekBinding(intent)) != null) {
                    iBinder = peekBinding.binder;
                }
            }
        }
        return iBinder;
    }

    public void publishService(IBinder iBinder, Intent intent, IBinder iBinder2, int i) {
        ServiceRecord.IntentBindRecord peekBinding;
        synchronized (this) {
            ServiceRecord serviceRecord = (ServiceRecord) iBinder;
            if (!(serviceRecord == null || (peekBinding = serviceRecord.peekBinding(intent)) == null)) {
                peekBinding.binder = iBinder2;
                for (IServiceConnection connectService : peekBinding.connections) {
                    connectService(connectService, ComponentUtils.toComponentName(serviceRecord.serviceInfo), peekBinding);
                }
            }
        }
    }

    private void connectService(IServiceConnection iServiceConnection, ComponentName componentName, ServiceRecord.IntentBindRecord intentBindRecord) {
        try {
            iServiceConnection.connected(componentName, new BinderDelegateService(componentName, intentBindRecord.binder));
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public VParceledListSlice<ActivityManager.RunningServiceInfo> getServices(int i, int i2, int i3) {
        VParceledListSlice<ActivityManager.RunningServiceInfo> vParceledListSlice;
        synchronized (this.mHistory) {
            ArrayList arrayList = new ArrayList(this.mHistory.size());
            for (ServiceRecord next : this.mHistory) {
                if (next.process.userId == i3) {
                    ActivityManager.RunningServiceInfo runningServiceInfo = new ActivityManager.RunningServiceInfo();
                    runningServiceInfo.uid = next.process.vuid;
                    runningServiceInfo.pid = next.process.pid;
                    ProcessRecord findProcessLocked = findProcessLocked(next.process.pid);
                    if (findProcessLocked != null) {
                        runningServiceInfo.process = findProcessLocked.processName;
                        runningServiceInfo.clientPackage = findProcessLocked.info.packageName;
                    }
                    runningServiceInfo.activeSince = next.activeSince;
                    runningServiceInfo.lastActivityTime = next.lastActivityTime;
                    runningServiceInfo.clientCount = next.getClientCount();
                    runningServiceInfo.service = ComponentUtils.toComponentName(next.serviceInfo);
                    runningServiceInfo.started = next.startId > 0;
                    arrayList.add(runningServiceInfo);
                }
            }
            vParceledListSlice = new VParceledListSlice<>(arrayList);
        }
        return vParceledListSlice;
    }

    public void setServiceForeground(ComponentName componentName, IBinder iBinder, int i, Notification notification, boolean z, int i2) {
        ServiceRecord serviceRecord = (ServiceRecord) iBinder;
        if (serviceRecord == null) {
            return;
        }
        if (i != 0) {
            if (notification == null) {
                throw new IllegalArgumentException("null notification");
            }
            if (serviceRecord.foregroundId != i) {
                if (serviceRecord.foregroundId != 0) {
                    cancelNotification(i2, serviceRecord.foregroundId, serviceRecord.serviceInfo.packageName);
                }
                serviceRecord.foregroundId = i;
            }
            serviceRecord.foregroundNoti = notification;
            postNotification(i2, i, serviceRecord.serviceInfo.packageName, notification);
        } else if (z) {
            cancelNotification(i2, serviceRecord.foregroundId, serviceRecord.serviceInfo.packageName);
            serviceRecord.foregroundId = 0;
            serviceRecord.foregroundNoti = null;
        }
    }

    private void cancelNotification(int i, int i2, String str) {
        int dealNotificationId = VNotificationManager.get().dealNotificationId(i2, str, null, i);
        this.nm.cancel(VNotificationManager.get().dealNotificationTag(dealNotificationId, str, null, i), dealNotificationId);
    }

    private void postNotification(int i, int i2, String str, Notification notification) {
        int dealNotificationId = VNotificationManager.get().dealNotificationId(i2, str, null, i);
        String dealNotificationTag = VNotificationManager.get().dealNotificationTag(dealNotificationId, str, null, i);
        VNotificationManager.get().dealNotification(dealNotificationId, notification, str);
        VNotificationManager.get().addNotification(dealNotificationId, dealNotificationTag, str, i);
        try {
            this.nm.notify(dealNotificationTag, dealNotificationId, notification);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void processRestarted(String str, String str2, int i) {
        int callingPid = getCallingPid();
        int uid = VUserHandle.getUid(i, VAppManagerService.get().getAppId(str));
        synchronized (this) {
            if (findProcessLocked(callingPid) == null) {
                ApplicationInfo applicationInfo = VPackageManagerService.get().getApplicationInfo(str, 0, i);
                applicationInfo.flags |= 4;
                int parseVPid = parseVPid(getProcessName(callingPid));
                if (parseVPid != -1) {
                    performStartProcessLocked(uid, parseVPid, applicationInfo, str2);
                }
            }
        }
    }

    private int parseVPid(String str) {
        String str2 = VirtualCore.get().getHostPkg() + ":p";
        if (str != null && str.startsWith(str2)) {
            try {
                return Integer.parseInt(str.substring(str2.length()));
            } catch (NumberFormatException e2) {
            }
        }
        return -1;
    }

    private String getProcessName(int i) {
        for (ActivityManager.RunningAppProcessInfo next : this.am.getRunningAppProcesses()) {
            if (next.pid == i) {
                return next.processName;
            }
        }
        return null;
    }

    private void attachClient(int i, final IBinder iBinder) {
        IInterface iInterface;
        ProcessRecord processRecord;
        final ProcessRecord processRecord2 = null;
        IVClient asInterface = IVClient.Stub.asInterface(iBinder);
        if (asInterface == null) {
            Process.killProcess(i);
            return;
        }
        try {
            iInterface = ApplicationThreadCompat.asInterface(asInterface.getAppThread());
        } catch (RemoteException e2) {
            iInterface = null;
        }
        if (iInterface == null) {
            Process.killProcess(i);
            return;
        }
        try {
            IBinder token = asInterface.getToken();
            if (token instanceof ProcessRecord) {
                processRecord = (ProcessRecord) token;
            } else {
                processRecord = null;
            }
            processRecord2 = processRecord;
        } catch (RemoteException e3) {
        }
        if (processRecord2 == null) {
            Process.killProcess(i);
            return;
        }
        try {
            iBinder.linkToDeath(new IBinder.DeathRecipient() {
                public void binderDied() {
                    iBinder.unlinkToDeath(this, 0);
                    VActivityManagerService.this.onProcessDead(processRecord2);
                }
            }, 0);
        } catch (RemoteException e4) {
            e4.printStackTrace();
        }
        processRecord2.client = asInterface;
        processRecord2.appThread = iInterface;
        processRecord2.pid = i;
        synchronized (this.mProcessNames) {
            this.mProcessNames.put(processRecord2.processName, processRecord2.vuid, processRecord2);
            this.mPidsSelfLocked.put(processRecord2.pid, processRecord2);
        }
    }

    /* access modifiers changed from: private */
    public void onProcessDead(ProcessRecord processRecord) {
        this.mProcessNames.remove(processRecord.processName, processRecord.vuid);
        this.mPidsSelfLocked.remove(processRecord.pid);
        processDead(processRecord);
        processRecord.lock.open();
    }

    public int getFreeStubCount() {
        return VASettings.STUB_COUNT - this.mPidsSelfLocked.size();
    }

    public int initProcess(String str, String str2, int i) {
        int i2;
        synchronized (this) {
            ProcessRecord startProcessIfNeedLocked = startProcessIfNeedLocked(str2, i, str);
            i2 = startProcessIfNeedLocked != null ? startProcessIfNeedLocked.vpid : -1;
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public ProcessRecord startProcessIfNeedLocked(String str, int i, String str2) {
        if (get().getFreeStubCount() < 3) {
            killAllApps();
        }
        PackageSetting setting = PackageCacheManager.getSetting(str2);
        ApplicationInfo applicationInfo = VPackageManagerService.get().getApplicationInfo(str2, 0, i);
        if (setting == null || applicationInfo == null) {
            return null;
        }
        if (!setting.isLaunched(i)) {
            sendFirstLaunchBroadcast(setting, i);
            setting.setLaunched(i, true);
            VAppManagerService.get().savePersistenceData();
        }
        int uid = VUserHandle.getUid(i, setting.appId);
        ProcessRecord processRecord = this.mProcessNames.get(str, uid);
        if (processRecord != null && processRecord.client.asBinder().isBinderAlive()) {
            return processRecord;
        }
        int queryFreeStubProcessLocked = queryFreeStubProcessLocked();
        if (queryFreeStubProcessLocked == -1) {
            return null;
        }
        ProcessRecord performStartProcessLocked = performStartProcessLocked(uid, queryFreeStubProcessLocked, applicationInfo, str);
        if (performStartProcessLocked == null) {
            return performStartProcessLocked;
        }
        performStartProcessLocked.pkgList.add(applicationInfo.packageName);
        return performStartProcessLocked;
    }

    private void sendFirstLaunchBroadcast(PackageSetting packageSetting, int i) {
        Intent intent = new Intent("android.intent.action.PACKAGE_FIRST_LAUNCH", Uri.fromParts(ServiceManagerNative.PACKAGE, packageSetting.packageName, null));
        intent.setPackage(packageSetting.packageName);
        intent.putExtra("android.intent.extra.UID", VUserHandle.getUid(packageSetting.appId, i));
        intent.putExtra(Constants.EXTRA_USER_HANDLE, i);
        sendBroadcastAsUser(intent, null);
    }

    public int getUidByPid(int i) {
        synchronized (this.mPidsSelfLocked) {
            ProcessRecord findProcessLocked = findProcessLocked(i);
            if (findProcessLocked == null) {
                return Process.myUid();
            }
            int i2 = findProcessLocked.vuid;
            return i2;
        }
    }

    private ProcessRecord performStartProcessLocked(int i, int i2, ApplicationInfo applicationInfo, String str) {
        ProcessRecord processRecord = new ProcessRecord(applicationInfo, str, i, i2);
        Bundle bundle = new Bundle();
        BundleCompat.putBinder(bundle, "_VA_|_binder_", processRecord);
        bundle.putInt("_VA_|_vuid_", i);
        bundle.putString("_VA_|_process_", str);
        bundle.putString("_VA_|_pkg_", applicationInfo.packageName);
        Bundle call = ProviderCall.call(VASettings.getStubAuthority(i2), "_VA_|_init_process_", null, bundle);
        if (call == null) {
            return null;
        }
        attachClient(call.getInt("_VA_|_pid_"), BundleCompat.getBinder(call, "_VA_|_client_"));
        return processRecord;
    }

    private int queryFreeStubProcessLocked() {
        boolean z;
        for (int i = 0; i < VASettings.STUB_COUNT; i++) {
            int size = this.mPidsSelfLocked.size();
            while (true) {
                int i2 = size - 1;
                if (size <= 0) {
                    z = false;
                    break;
                } else if (this.mPidsSelfLocked.valueAt(i2).vpid == i) {
                    z = true;
                    break;
                } else {
                    size = i2;
                }
            }
            if (!z) {
                return i;
            }
        }
        return -1;
    }

    public boolean isAppProcess(String str) {
        return parseVPid(str) != -1;
    }

    public boolean isAppPid(int i) {
        boolean z;
        synchronized (this.mPidsSelfLocked) {
            z = findProcessLocked(i) != null;
        }
        return z;
    }

    public String getAppProcessName(int i) {
        synchronized (this.mPidsSelfLocked) {
            ProcessRecord processRecord = this.mPidsSelfLocked.get(i);
            if (processRecord == null) {
                return null;
            }
            String str = processRecord.processName;
            return str;
        }
    }

    public List<String> getProcessPkgList(int i) {
        synchronized (this.mPidsSelfLocked) {
            ProcessRecord processRecord = this.mPidsSelfLocked.get(i);
            if (processRecord == null) {
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList(processRecord.pkgList);
            return arrayList;
        }
    }

    public void killAllApps() {
        synchronized (this.mPidsSelfLocked) {
            for (int i = 0; i < this.mPidsSelfLocked.size(); i++) {
                Process.killProcess(this.mPidsSelfLocked.valueAt(i).pid);
            }
        }
    }

    public void killAppByPkg(String str, int i) {
        synchronized (this.mProcessNames) {
            ArrayMap<String, SparseArray<ProcessRecord>> map = this.mProcessNames.getMap();
            int size = map.size();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    SparseArray valueAt = map.valueAt(i2);
                    for (int i3 = 0; i3 < valueAt.size(); i3++) {
                        ProcessRecord processRecord = (ProcessRecord) valueAt.valueAt(i3);
                        if ((i == -1 || processRecord.userId == i) && processRecord.pkgList.contains(str)) {
                            Process.killProcess(processRecord.pid);
                        }
                    }
                    size = i2;
                }
            }
        }
    }

    public boolean isAppRunning(String str, int i) {
        boolean z;
        synchronized (this.mPidsSelfLocked) {
            int size = this.mPidsSelfLocked.size();
            while (true) {
                int i2 = size - 1;
                if (size <= 0) {
                    z = false;
                    break;
                }
                ProcessRecord valueAt = this.mPidsSelfLocked.valueAt(i2);
                if (valueAt.userId == i && valueAt.info.packageName.equals(str)) {
                    z = true;
                    break;
                }
                size = i2;
            }
        }
        return z;
    }

    public void killApplicationProcess(String str, int i) {
        synchronized (this.mProcessNames) {
            ProcessRecord processRecord = this.mProcessNames.get(str, i);
            if (processRecord != null) {
                Process.killProcess(processRecord.pid);
            }
        }
    }

    public void dump() {
    }

    public void registerProcessObserver(IProcessObserver iProcessObserver) {
    }

    public void unregisterProcessObserver(IProcessObserver iProcessObserver) {
    }

    public String getInitialPackage(int i) {
        String str;
        synchronized (this.mPidsSelfLocked) {
            ProcessRecord processRecord = this.mPidsSelfLocked.get(i);
            if (processRecord != null) {
                str = processRecord.info.packageName;
            } else {
                str = null;
            }
        }
        return str;
    }

    public void handleApplicationCrash() {
    }

    public void appDoneExecuting() {
        synchronized (this.mPidsSelfLocked) {
            ProcessRecord processRecord = this.mPidsSelfLocked.get(VBinder.getCallingPid());
            if (processRecord != null) {
                processRecord.doneExecuting = true;
                processRecord.lock.open();
            }
        }
    }

    public ProcessRecord findProcessLocked(int i) {
        return this.mPidsSelfLocked.get(i);
    }

    public ProcessRecord findProcessLocked(String str, int i) {
        return this.mProcessNames.get(str, i);
    }

    public int stopUser(int i, IStopUserCallback.Stub stub) {
        synchronized (this.mPidsSelfLocked) {
            int size = this.mPidsSelfLocked.size();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    ProcessRecord valueAt = this.mPidsSelfLocked.valueAt(i2);
                    if (valueAt.userId == i) {
                        Process.killProcess(valueAt.pid);
                    }
                    size = i2;
                }
            }
        }
        try {
            stub.userStopped(i);
            return 0;
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public void sendOrderedBroadcastAsUser(Intent intent, VUserHandle vUserHandle, String str, BroadcastReceiver broadcastReceiver, Handler handler, int i, String str2, Bundle bundle) {
        Context context = VirtualCore.get().getContext();
        if (vUserHandle != null) {
            intent.putExtra("_VA_|_user_id_", vUserHandle.getIdentifier());
        }
        context.sendOrderedBroadcast(intent, null, broadcastReceiver, handler, i, str2, bundle);
    }

    public void sendBroadcastAsUser(Intent intent, VUserHandle vUserHandle) {
        SpecialComponentList.protectIntent(intent);
        Context context = VirtualCore.get().getContext();
        if (vUserHandle != null) {
            intent.putExtra("_VA_|_user_id_", vUserHandle.getIdentifier());
        }
        context.sendBroadcast(intent);
    }

    public boolean bindServiceAsUser(Intent intent, ServiceConnection serviceConnection, int i, VUserHandle vUserHandle) {
        Intent intent2 = new Intent(intent);
        if (vUserHandle != null) {
            intent2.putExtra("_VA_|_user_id_", vUserHandle.getIdentifier());
        }
        return VirtualCore.get().getContext().bindService(intent2, serviceConnection, i);
    }

    public void sendBroadcastAsUser(Intent intent, VUserHandle vUserHandle, String str) {
        SpecialComponentList.protectIntent(intent);
        Context context = VirtualCore.get().getContext();
        if (vUserHandle != null) {
            intent.putExtra("_VA_|_user_id_", vUserHandle.getIdentifier());
        }
        context.sendBroadcast(intent);
    }

    /* access modifiers changed from: package-private */
    public boolean handleStaticBroadcast(int i, ActivityInfo activityInfo, Intent intent, PendingResultData pendingResultData) {
        Intent intent2 = (Intent) intent.getParcelableExtra("_VA_|_intent_");
        ComponentName componentName = (ComponentName) intent.getParcelableExtra("_VA_|_component_");
        int intExtra = intent.getIntExtra("_VA_|_user_id_", VUserHandle.USER_NULL);
        if (intent2 == null) {
            return false;
        }
        if (intExtra >= 0) {
            return handleUserBroadcast(VUserHandle.getUid(intExtra, i), activityInfo, componentName, intent2, pendingResultData);
        }
        VLog.w(TAG, "Sent a broadcast without userId " + intent2, new Object[0]);
        return false;
    }

    private boolean handleUserBroadcast(int i, ActivityInfo activityInfo, ComponentName componentName, Intent intent, PendingResultData pendingResultData) {
        if (componentName != null && !ComponentUtils.toComponentName(activityInfo).equals(componentName)) {
            return false;
        }
        String unprotectAction = SpecialComponentList.unprotectAction(intent.getAction());
        if (unprotectAction != null) {
            intent.setAction(unprotectAction);
        }
        handleStaticBroadcastAsUser(i, activityInfo, intent, pendingResultData);
        return true;
    }

    private void handleStaticBroadcastAsUser(int i, ActivityInfo activityInfo, Intent intent, PendingResultData pendingResultData) {
        synchronized (this) {
            ProcessRecord findProcessLocked = findProcessLocked(activityInfo.processName, i);
            if (!(findProcessLocked == null || findProcessLocked.appThread == null)) {
                performScheduleReceiver(findProcessLocked.client, i, activityInfo, intent, pendingResultData);
            }
        }
    }

    private void performScheduleReceiver(IVClient iVClient, int i, ActivityInfo activityInfo, Intent intent, PendingResultData pendingResultData) {
        ComponentName componentName = ComponentUtils.toComponentName(activityInfo);
        BroadcastSystem.get().broadcastSent(i, activityInfo, pendingResultData);
        try {
            iVClient.scheduleReceiver(activityInfo.processName, componentName, intent, pendingResultData);
        } catch (Throwable th) {
            if (pendingResultData != null) {
                pendingResultData.finish();
            }
        }
    }

    public void broadcastFinish(PendingResultData pendingResultData) {
        BroadcastSystem.get().broadcastFinish(pendingResultData);
    }

    public void notifyBadgerChange(BadgerInfo badgerInfo) {
        Intent intent = new Intent(VASettings.ACTION_BADGER_CHANGE);
        intent.putExtra(ChooseTypeAndAccountActivity.KEY_USER_ID, badgerInfo.userId);
        intent.putExtra("packageName", badgerInfo.packageName);
        intent.putExtra("badgerCount", badgerInfo.badgerCount);
        VirtualCore.get().getContext().sendBroadcast(intent);
    }
}
