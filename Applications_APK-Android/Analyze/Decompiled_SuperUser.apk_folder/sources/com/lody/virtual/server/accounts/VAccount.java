package com.lody.virtual.server.accounts;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;

public class VAccount implements Parcelable {
    public static final Parcelable.Creator<VAccount> CREATOR = new Parcelable.Creator<VAccount>() {
        public VAccount createFromParcel(Parcel parcel) {
            return new VAccount(parcel);
        }

        public VAccount[] newArray(int i) {
            return new VAccount[i];
        }
    };
    public Map<String, String> authTokens;
    public long lastAuthenticatedTime;
    public String name;
    public String password;
    public String previousName;
    public String type;
    public Map<String, String> userDatas;
    public int userId;

    public VAccount(int i, Account account) {
        this.userId = i;
        this.name = account.name;
        this.type = account.type;
        this.authTokens = new HashMap();
        this.userDatas = new HashMap();
    }

    public VAccount(Parcel parcel) {
        this.userId = parcel.readInt();
        this.name = parcel.readString();
        this.previousName = parcel.readString();
        this.type = parcel.readString();
        this.password = parcel.readString();
        this.lastAuthenticatedTime = parcel.readLong();
        int readInt = parcel.readInt();
        this.authTokens = new HashMap(readInt);
        for (int i = 0; i < readInt; i++) {
            this.authTokens.put(parcel.readString(), parcel.readString());
        }
        int readInt2 = parcel.readInt();
        this.userDatas = new HashMap(readInt2);
        for (int i2 = 0; i2 < readInt2; i2++) {
            this.userDatas.put(parcel.readString(), parcel.readString());
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.userId);
        parcel.writeString(this.name);
        parcel.writeString(this.previousName);
        parcel.writeString(this.type);
        parcel.writeString(this.password);
        parcel.writeLong(this.lastAuthenticatedTime);
        parcel.writeInt(this.authTokens.size());
        for (Map.Entry next : this.authTokens.entrySet()) {
            parcel.writeString((String) next.getKey());
            parcel.writeString((String) next.getValue());
        }
        parcel.writeInt(this.userDatas.size());
        for (Map.Entry next2 : this.userDatas.entrySet()) {
            parcel.writeString((String) next2.getKey());
            parcel.writeString((String) next2.getValue());
        }
    }
}
