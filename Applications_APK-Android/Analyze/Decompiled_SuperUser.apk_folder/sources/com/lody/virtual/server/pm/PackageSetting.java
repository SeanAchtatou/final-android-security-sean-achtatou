package com.lody.virtual.server.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.lody.virtual.remote.InstalledAppInfo;

public class PackageSetting implements Parcelable {
    public static final Parcelable.Creator<PackageSetting> CREATOR = new Parcelable.Creator<PackageSetting>() {
        public PackageSetting createFromParcel(Parcel parcel) {
            return new PackageSetting(parcel);
        }

        public PackageSetting[] newArray(int i) {
            return new PackageSetting[i];
        }
    };
    private static final PackageUserState DEFAULT_USER_STATE = new PackageUserState();
    public String apkPath;
    public int appId;
    public boolean dependSystem;
    public long firstInstallTime;
    public long lastUpdateTime;
    public String libPath;
    public String packageName;
    public boolean skipDexOpt;
    private SparseArray<PackageUserState> userState = new SparseArray<>();

    public PackageSetting() {
    }

    protected PackageSetting(Parcel parcel) {
        boolean z = true;
        this.packageName = parcel.readString();
        this.apkPath = parcel.readString();
        this.libPath = parcel.readString();
        this.dependSystem = parcel.readByte() != 0;
        this.appId = parcel.readInt();
        this.userState = parcel.readSparseArray(PackageUserState.class.getClassLoader());
        this.skipDexOpt = parcel.readByte() == 0 ? false : z;
    }

    public InstalledAppInfo getAppInfo() {
        return new InstalledAppInfo(this.packageName, this.apkPath, this.libPath, this.dependSystem, this.skipDexOpt, this.appId);
    }

    /* access modifiers changed from: package-private */
    public PackageUserState modifyUserState(int i) {
        PackageUserState packageUserState = this.userState.get(i);
        if (packageUserState != null) {
            return packageUserState;
        }
        PackageUserState packageUserState2 = new PackageUserState();
        this.userState.put(i, packageUserState2);
        return packageUserState2;
    }

    /* access modifiers changed from: package-private */
    public void setUserState(int i, boolean z, boolean z2, boolean z3) {
        PackageUserState modifyUserState = modifyUserState(i);
        modifyUserState.launched = z;
        modifyUserState.hidden = z2;
        modifyUserState.installed = z3;
    }

    /* access modifiers changed from: package-private */
    public PackageUserState readUserState(int i) {
        PackageUserState packageUserState = this.userState.get(i);
        return packageUserState != null ? packageUserState : DEFAULT_USER_STATE;
    }

    /* access modifiers changed from: package-private */
    public void removeUser(int i) {
        this.userState.delete(i);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        byte b2 = 1;
        parcel.writeString(this.packageName);
        parcel.writeString(this.apkPath);
        parcel.writeString(this.libPath);
        parcel.writeByte(this.dependSystem ? (byte) 1 : 0);
        parcel.writeInt(this.appId);
        parcel.writeSparseArray(this.userState);
        if (!this.skipDexOpt) {
            b2 = 0;
        }
        parcel.writeByte(b2);
    }

    public boolean isLaunched(int i) {
        return readUserState(i).launched;
    }

    public boolean isHidden(int i) {
        return readUserState(i).hidden;
    }

    public boolean isInstalled(int i) {
        return readUserState(i).installed;
    }

    public void setLaunched(int i, boolean z) {
        modifyUserState(i).launched = z;
    }

    public void setHidden(int i, boolean z) {
        modifyUserState(i).hidden = z;
    }

    public void setInstalled(int i, boolean z) {
        modifyUserState(i).installed = z;
    }
}
