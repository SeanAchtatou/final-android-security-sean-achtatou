package com.lody.virtual.server.am;

import android.content.Intent;
import com.lody.virtual.remote.AppTaskInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class TaskRecord {
    public final List<ActivityRecord> activities = Collections.synchronizedList(new ArrayList());
    public String affinity;
    public int taskId;
    public Intent taskRoot;
    public int userId;

    TaskRecord(int i, int i2, String str, Intent intent) {
        this.taskId = i;
        this.userId = i2;
        this.affinity = str;
        this.taskRoot = intent;
    }

    /* access modifiers changed from: package-private */
    public AppTaskInfo getAppTaskInfo() {
        int size = this.activities.size();
        if (size <= 0) {
            return null;
        }
        return new AppTaskInfo(this.taskId, this.taskRoot, this.taskRoot.getComponent(), this.activities.get(size - 1).component);
    }

    public boolean isFinishing() {
        boolean z = true;
        Iterator<ActivityRecord> it = this.activities.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            if (!it.next().marked) {
                z = false;
            } else {
                z = z2;
            }
        }
    }
}
