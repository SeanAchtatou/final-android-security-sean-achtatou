package com.lody.virtual.server.notification;

import android.app.NotificationManager;
import android.content.Context;
import android.text.TextUtils;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.server.INotificationManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class VNotificationManagerService extends INotificationManager.Stub {
    static final String TAG = NotificationCompat.class.getSimpleName();
    private static final AtomicReference<VNotificationManagerService> gService = new AtomicReference<>();
    private Context mContext;
    private final List<String> mDisables = new ArrayList();
    private NotificationManager mNotificationManager;
    private final HashMap<String, List<NotificationInfo>> mNotifications = new HashMap<>();

    private VNotificationManagerService(Context context) {
        this.mContext = context;
        this.mNotificationManager = (NotificationManager) context.getSystemService(ServiceManagerNative.NOTIFICATION);
    }

    public static void systemReady(Context context) {
        gService.set(new VNotificationManagerService(context));
    }

    public static VNotificationManagerService get() {
        return gService.get();
    }

    public int dealNotificationId(int i, String str, String str2, int i2) {
        return i;
    }

    public String dealNotificationTag(int i, String str, String str2, int i2) {
        if (TextUtils.equals(this.mContext.getPackageName(), str)) {
            return str2;
        }
        if (str2 == null) {
            return str + "@" + i2;
        }
        return str + ":" + str2 + "@" + i2;
    }

    public boolean areNotificationsEnabledForPackage(String str, int i) {
        return !this.mDisables.contains(new StringBuilder().append(str).append(":").append(i).toString());
    }

    public void setNotificationsEnabledForPackage(String str, boolean z, int i) {
        String str2 = str + ":" + i;
        if (z) {
            if (this.mDisables.contains(str2)) {
                this.mDisables.remove(str2);
            }
        } else if (!this.mDisables.contains(str2)) {
            this.mDisables.add(str2);
        }
    }

    public void addNotification(int i, String str, String str2, int i2) {
        NotificationInfo notificationInfo = new NotificationInfo(i, str, str2, i2);
        synchronized (this.mNotifications) {
            List list = this.mNotifications.get(str2);
            if (list == null) {
                list = new ArrayList();
                this.mNotifications.put(str2, list);
            }
            if (!list.contains(notificationInfo)) {
                list.add(notificationInfo);
            }
        }
    }

    public void cancelAllNotification(String str, int i) {
        ArrayList<NotificationInfo> arrayList = new ArrayList<>();
        synchronized (this.mNotifications) {
            List list = this.mNotifications.get(str);
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    NotificationInfo notificationInfo = (NotificationInfo) list.get(size);
                    if (notificationInfo.userId == i) {
                        arrayList.add(notificationInfo);
                        list.remove(size);
                    }
                }
            }
        }
        for (NotificationInfo notificationInfo2 : arrayList) {
            VLog.d(TAG, "cancel " + notificationInfo2.tag + " " + notificationInfo2.id, new Object[0]);
            this.mNotificationManager.cancel(notificationInfo2.tag, notificationInfo2.id);
        }
    }

    private static class NotificationInfo {
        int id;
        String packageName;
        String tag;
        int userId;

        NotificationInfo(int i, String str, String str2, int i2) {
            this.id = i;
            this.tag = str;
            this.packageName = str2;
            this.userId = i2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof NotificationInfo)) {
                return super.equals(obj);
            }
            NotificationInfo notificationInfo = (NotificationInfo) obj;
            return notificationInfo.id == this.id && TextUtils.equals(notificationInfo.tag, this.tag) && TextUtils.equals(this.packageName, notificationInfo.packageName) && notificationInfo.userId == this.userId;
        }
    }
}
