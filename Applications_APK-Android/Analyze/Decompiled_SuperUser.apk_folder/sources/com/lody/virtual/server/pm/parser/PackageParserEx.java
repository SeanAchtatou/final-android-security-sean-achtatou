package com.lody.virtual.server.pm.parser;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageParser;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.Constants;
import com.lody.virtual.client.fixer.ComponentFixer;
import com.lody.virtual.helper.collection.ArrayMap;
import com.lody.virtual.helper.compat.PackageParserCompat;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.server.pm.PackageSetting;
import com.lody.virtual.server.pm.PackageUserState;
import com.lody.virtual.server.pm.parser.VPackage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import mirror.android.content.pm.ApplicationInfoL;
import mirror.android.content.pm.ApplicationInfoN;
import mirror.android.content.pm.PackageParser;

public class PackageParserEx {
    private static final String TAG = PackageParserEx.class.getSimpleName();
    private static final ArrayMap<String, String[]> sSharedLibCache = new ArrayMap<>();

    public static VPackage parsePackage(File file) {
        PackageParser createParser = PackageParserCompat.createParser(file);
        PackageParser.Package parsePackage = PackageParserCompat.parsePackage(createParser, file, 0);
        if (!parsePackage.requestedPermissions.contains("android.permission.FAKE_PACKAGE_SIGNATURE") || parsePackage.mAppMetaData == null || !parsePackage.mAppMetaData.containsKey(Constants.FEATURE_FAKE_SIGNATURE)) {
            PackageParserCompat.collectCertificates(createParser, parsePackage, 1);
        } else {
            parsePackage.mSignatures = new Signature[]{new Signature(parsePackage.mAppMetaData.getString(Constants.FEATURE_FAKE_SIGNATURE))};
            VLog.d(TAG, "Using fake-signature feature on : " + parsePackage.packageName, new Object[0]);
        }
        return buildPackageCache(parsePackage);
    }

    /* JADX INFO: finally extract failed */
    public static VPackage readPackageCache(String str) {
        Parcel obtain = Parcel.obtain();
        try {
            FileInputStream fileInputStream = new FileInputStream(VEnvironment.getPackageCacheFile(str));
            byte[] byteArray = FileUtils.toByteArray(fileInputStream);
            fileInputStream.close();
            obtain.unmarshall(byteArray, 0, byteArray.length);
            obtain.setDataPosition(0);
            if (obtain.readInt() != 4) {
                throw new IllegalStateException("Invalid version.");
            }
            VPackage vPackage = new VPackage(obtain);
            addOwner(vPackage);
            obtain.recycle();
            return vPackage;
        } catch (Exception e2) {
            e2.printStackTrace();
            obtain.recycle();
            return null;
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }

    public static void readSignature(VPackage vPackage) {
        File signatureFile = VEnvironment.getSignatureFile(vPackage.packageName);
        if (signatureFile.exists()) {
            Parcel obtain = Parcel.obtain();
            try {
                FileInputStream fileInputStream = new FileInputStream(signatureFile);
                byte[] byteArray = FileUtils.toByteArray(fileInputStream);
                fileInputStream.close();
                obtain.unmarshall(byteArray, 0, byteArray.length);
                obtain.setDataPosition(0);
                vPackage.mSignatures = (Signature[]) obtain.createTypedArray(Signature.CREATOR);
            } catch (IOException e2) {
                e2.printStackTrace();
            } finally {
                obtain.recycle();
            }
        }
    }

    public static void savePackageCache(VPackage vPackage) {
        String str = vPackage.packageName;
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInt(4);
            vPackage.writeToParcel(obtain, 0);
            FileOutputStream fileOutputStream = new FileOutputStream(VEnvironment.getPackageCacheFile(str));
            fileOutputStream.write(obtain.marshall());
            fileOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            obtain.recycle();
        }
        Signature[] signatureArr = vPackage.mSignatures;
        if (signatureArr != null) {
            File signatureFile = VEnvironment.getSignatureFile(str);
            if (signatureFile.exists() && !signatureFile.delete()) {
                VLog.w(TAG, "Unable to delete the signatures of " + str, new Object[0]);
            }
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain2.writeTypedArray(signatureArr, 0);
                FileUtils.writeParcelToFile(obtain2, signatureFile);
            } catch (IOException e3) {
                e3.printStackTrace();
            } finally {
                obtain2.recycle();
            }
        }
    }

    private static VPackage buildPackageCache(PackageParser.Package packageR) {
        List list;
        VPackage vPackage = new VPackage();
        vPackage.activities = new ArrayList<>(packageR.activities.size());
        vPackage.services = new ArrayList<>(packageR.services.size());
        vPackage.receivers = new ArrayList<>(packageR.receivers.size());
        vPackage.providers = new ArrayList<>(packageR.providers.size());
        vPackage.instrumentation = new ArrayList<>(packageR.instrumentation.size());
        vPackage.permissions = new ArrayList<>(packageR.permissions.size());
        vPackage.permissionGroups = new ArrayList<>(packageR.permissionGroups.size());
        Iterator<PackageParser.Activity> it = packageR.activities.iterator();
        while (it.hasNext()) {
            vPackage.activities.add(new VPackage.ActivityComponent(it.next()));
        }
        Iterator<PackageParser.Service> it2 = packageR.services.iterator();
        while (it2.hasNext()) {
            vPackage.services.add(new VPackage.ServiceComponent(it2.next()));
        }
        Iterator<PackageParser.Activity> it3 = packageR.receivers.iterator();
        while (it3.hasNext()) {
            vPackage.receivers.add(new VPackage.ActivityComponent(it3.next()));
        }
        Iterator<PackageParser.Provider> it4 = packageR.providers.iterator();
        while (it4.hasNext()) {
            vPackage.providers.add(new VPackage.ProviderComponent(it4.next()));
        }
        Iterator<PackageParser.Instrumentation> it5 = packageR.instrumentation.iterator();
        while (it5.hasNext()) {
            vPackage.instrumentation.add(new VPackage.InstrumentationComponent(it5.next()));
        }
        vPackage.requestedPermissions = new ArrayList<>(packageR.requestedPermissions.size());
        vPackage.requestedPermissions.addAll(packageR.requestedPermissions);
        if (!(PackageParser.Package.protectedBroadcasts == null || (list = PackageParser.Package.protectedBroadcasts.get(packageR)) == null)) {
            vPackage.protectedBroadcasts = new ArrayList<>(list);
            vPackage.protectedBroadcasts.addAll(list);
        }
        vPackage.applicationInfo = packageR.applicationInfo;
        vPackage.mSignatures = packageR.mSignatures;
        vPackage.mAppMetaData = packageR.mAppMetaData;
        vPackage.packageName = packageR.packageName;
        vPackage.mPreferredOrder = packageR.mPreferredOrder;
        vPackage.mVersionName = packageR.mVersionName;
        vPackage.mSharedUserId = packageR.mSharedUserId;
        vPackage.mSharedUserLabel = packageR.mSharedUserLabel;
        vPackage.usesLibraries = packageR.usesLibraries;
        vPackage.mVersionCode = packageR.mVersionCode;
        vPackage.mAppMetaData = packageR.mAppMetaData;
        vPackage.configPreferences = packageR.configPreferences;
        vPackage.reqFeatures = packageR.reqFeatures;
        addOwner(vPackage);
        return vPackage;
    }

    public static void initApplicationInfoBase(PackageSetting packageSetting, VPackage vPackage) {
        ApplicationInfo applicationInfo = vPackage.applicationInfo;
        applicationInfo.flags |= 4;
        if (TextUtils.isEmpty(applicationInfo.processName)) {
            applicationInfo.processName = applicationInfo.packageName;
        }
        applicationInfo.enabled = true;
        applicationInfo.nativeLibraryDir = packageSetting.libPath;
        applicationInfo.uid = packageSetting.appId;
        applicationInfo.name = ComponentFixer.fixComponentClassName(packageSetting.packageName, applicationInfo.name);
        applicationInfo.publicSourceDir = packageSetting.apkPath;
        applicationInfo.sourceDir = packageSetting.apkPath;
        if (Build.VERSION.SDK_INT >= 21) {
            applicationInfo.splitSourceDirs = new String[]{packageSetting.apkPath};
            applicationInfo.splitPublicSourceDirs = applicationInfo.splitSourceDirs;
            ApplicationInfoL.scanSourceDir.set(applicationInfo, applicationInfo.dataDir);
            ApplicationInfoL.scanPublicSourceDir.set(applicationInfo, applicationInfo.dataDir);
            ApplicationInfoL.primaryCpuAbi.set(applicationInfo, ApplicationInfoL.primaryCpuAbi.get(VirtualCore.get().getContext().getApplicationInfo()));
        }
        if (packageSetting.dependSystem) {
            String[] strArr = sSharedLibCache.get(packageSetting.packageName);
            if (strArr == null) {
                try {
                    String[] strArr2 = VirtualCore.get().getUnHookPackageManager().getApplicationInfo(packageSetting.packageName, FileUtils.FileMode.MODE_ISGID).sharedLibraryFiles;
                    if (strArr2 == null) {
                        try {
                            strArr = new String[0];
                        } catch (PackageManager.NameNotFoundException e2) {
                            PackageManager.NameNotFoundException nameNotFoundException = e2;
                            strArr = strArr2;
                            e = nameNotFoundException;
                            e.printStackTrace();
                            applicationInfo.sharedLibraryFiles = strArr;
                        }
                    } else {
                        strArr = strArr2;
                    }
                    sSharedLibCache.put(packageSetting.packageName, strArr);
                } catch (PackageManager.NameNotFoundException e3) {
                    e = e3;
                    e.printStackTrace();
                    applicationInfo.sharedLibraryFiles = strArr;
                }
            }
            applicationInfo.sharedLibraryFiles = strArr;
        }
    }

    private static void initApplicationAsUser(ApplicationInfo applicationInfo, int i) {
        applicationInfo.dataDir = VEnvironment.getDataUserPackageDirectory(i, applicationInfo.packageName).getPath();
        if (Build.VERSION.SDK_INT >= 21) {
            ApplicationInfoL.scanSourceDir.set(applicationInfo, applicationInfo.dataDir);
            ApplicationInfoL.scanPublicSourceDir.set(applicationInfo, applicationInfo.dataDir);
        }
        if (Build.VERSION.SDK_INT >= 24) {
            ApplicationInfoN.deviceEncryptedDataDir.set(applicationInfo, applicationInfo.dataDir);
            ApplicationInfoN.deviceProtectedDataDir.set(applicationInfo, applicationInfo.dataDir);
            ApplicationInfoN.credentialEncryptedDataDir.set(applicationInfo, applicationInfo.dataDir);
            ApplicationInfoN.credentialProtectedDataDir.set(applicationInfo, applicationInfo.dataDir);
        }
    }

    private static void addOwner(VPackage vPackage) {
        Iterator<VPackage.ActivityComponent> it = vPackage.activities.iterator();
        while (it.hasNext()) {
            VPackage.ActivityComponent next = it.next();
            next.owner = vPackage;
            Iterator it2 = next.intents.iterator();
            while (it2.hasNext()) {
                ((VPackage.ActivityIntentInfo) it2.next()).activity = next;
            }
        }
        Iterator<VPackage.ServiceComponent> it3 = vPackage.services.iterator();
        while (it3.hasNext()) {
            VPackage.ServiceComponent next2 = it3.next();
            next2.owner = vPackage;
            Iterator it4 = next2.intents.iterator();
            while (it4.hasNext()) {
                ((VPackage.ServiceIntentInfo) it4.next()).service = next2;
            }
        }
        Iterator<VPackage.ActivityComponent> it5 = vPackage.receivers.iterator();
        while (it5.hasNext()) {
            VPackage.ActivityComponent next3 = it5.next();
            next3.owner = vPackage;
            Iterator it6 = next3.intents.iterator();
            while (it6.hasNext()) {
                ((VPackage.ActivityIntentInfo) it6.next()).activity = next3;
            }
        }
        Iterator<VPackage.ProviderComponent> it7 = vPackage.providers.iterator();
        while (it7.hasNext()) {
            VPackage.ProviderComponent next4 = it7.next();
            next4.owner = vPackage;
            Iterator it8 = next4.intents.iterator();
            while (it8.hasNext()) {
                ((VPackage.ProviderIntentInfo) it8.next()).provider = next4;
            }
        }
        Iterator<VPackage.InstrumentationComponent> it9 = vPackage.instrumentation.iterator();
        while (it9.hasNext()) {
            it9.next().owner = vPackage;
        }
        Iterator<VPackage.PermissionComponent> it10 = vPackage.permissions.iterator();
        while (it10.hasNext()) {
            it10.next().owner = vPackage;
        }
        Iterator<VPackage.PermissionGroupComponent> it11 = vPackage.permissionGroups.iterator();
        while (it11.hasNext()) {
            it11.next().owner = vPackage;
        }
    }

    public static PackageInfo generatePackageInfo(VPackage vPackage, int i, long j, long j2, PackageUserState packageUserState, int i2) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int i3;
        if (!checkUseInstalledOrHidden(packageUserState, i)) {
            return null;
        }
        if (vPackage.mSignatures == null) {
            readSignature(vPackage);
        }
        PackageInfo packageInfo = new PackageInfo();
        packageInfo.packageName = vPackage.packageName;
        packageInfo.versionCode = vPackage.mVersionCode;
        packageInfo.sharedUserLabel = vPackage.mSharedUserLabel;
        packageInfo.versionName = vPackage.mVersionName;
        packageInfo.sharedUserId = vPackage.mSharedUserId;
        packageInfo.sharedUserLabel = vPackage.mSharedUserLabel;
        packageInfo.applicationInfo = generateApplicationInfo(vPackage, i, packageUserState, i2);
        packageInfo.firstInstallTime = j;
        packageInfo.lastUpdateTime = j2;
        if (vPackage.requestedPermissions != null && !vPackage.requestedPermissions.isEmpty()) {
            String[] strArr = new String[vPackage.requestedPermissions.size()];
            vPackage.requestedPermissions.toArray(strArr);
            packageInfo.requestedPermissions = strArr;
        }
        if ((i & FileUtils.FileMode.MODE_IRUSR) != 0) {
            packageInfo.gids = PackageParserCompat.GIDS;
        }
        if ((i & 16384) != 0) {
            int size6 = vPackage.configPreferences != null ? vPackage.configPreferences.size() : 0;
            if (size6 > 0) {
                packageInfo.configPreferences = new ConfigurationInfo[size6];
                vPackage.configPreferences.toArray(packageInfo.configPreferences);
            }
            if (vPackage.reqFeatures != null) {
                i3 = vPackage.reqFeatures.size();
            } else {
                i3 = 0;
            }
            if (i3 > 0) {
                packageInfo.reqFeatures = new FeatureInfo[i3];
                vPackage.reqFeatures.toArray(packageInfo.reqFeatures);
            }
        }
        if ((i & 1) != 0 && (size5 = vPackage.activities.size()) > 0) {
            ActivityInfo[] activityInfoArr = new ActivityInfo[size5];
            int i4 = 0;
            int i5 = 0;
            while (i4 < size5) {
                activityInfoArr[i5] = generateActivityInfo(vPackage.activities.get(i4), i, packageUserState, i2);
                i4++;
                i5++;
            }
            packageInfo.activities = activityInfoArr;
        }
        if ((i & 2) != 0 && (size4 = vPackage.receivers.size()) > 0) {
            ActivityInfo[] activityInfoArr2 = new ActivityInfo[size4];
            int i6 = 0;
            int i7 = 0;
            while (i6 < size4) {
                activityInfoArr2[i7] = generateActivityInfo(vPackage.receivers.get(i6), i, packageUserState, i2);
                i6++;
                i7++;
            }
            packageInfo.receivers = activityInfoArr2;
        }
        if ((i & 4) != 0 && (size3 = vPackage.services.size()) > 0) {
            ServiceInfo[] serviceInfoArr = new ServiceInfo[size3];
            int i8 = 0;
            int i9 = 0;
            while (i8 < size3) {
                serviceInfoArr[i9] = generateServiceInfo(vPackage.services.get(i8), i, packageUserState, i2);
                i8++;
                i9++;
            }
            packageInfo.services = serviceInfoArr;
        }
        if ((i & 8) != 0 && (size2 = vPackage.providers.size()) > 0) {
            ProviderInfo[] providerInfoArr = new ProviderInfo[size2];
            int i10 = 0;
            int i11 = 0;
            while (i10 < size2) {
                providerInfoArr[i11] = generateProviderInfo(vPackage.providers.get(i10), i, packageUserState, i2);
                i10++;
                i11++;
            }
            packageInfo.providers = providerInfoArr;
        }
        if ((i & 16) != 0 && (size = vPackage.instrumentation.size()) > 0) {
            packageInfo.instrumentation = new InstrumentationInfo[size];
            for (int i12 = 0; i12 < size; i12++) {
                packageInfo.instrumentation[i12] = generateInstrumentationInfo(vPackage.instrumentation.get(i12), i);
            }
        }
        if ((i & 64) != 0) {
            int length = vPackage.mSignatures != null ? vPackage.mSignatures.length : 0;
            if (length > 0) {
                packageInfo.signatures = new Signature[length];
                System.arraycopy(vPackage.mSignatures, 0, packageInfo.signatures, 0, length);
            }
        }
        return packageInfo;
    }

    public static ApplicationInfo generateApplicationInfo(VPackage vPackage, int i, PackageUserState packageUserState, int i2) {
        ApplicationInfo applicationInfo = null;
        if (vPackage != null && checkUseInstalledOrHidden(packageUserState, i)) {
            applicationInfo = new ApplicationInfo(vPackage.applicationInfo);
            if ((i & FileUtils.FileMode.MODE_IWUSR) != 0) {
                applicationInfo.metaData = vPackage.mAppMetaData;
            }
            if (applicationInfo.metaData == null) {
                applicationInfo.metaData = new Bundle();
            }
            applicationInfo.metaData.putString("CHANNEL", getChannelName(VirtualCore.get().getContext()));
            initApplicationAsUser(applicationInfo, i2);
        }
        return applicationInfo;
    }

    private static String getChannelName(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), FileUtils.FileMode.MODE_IWUSR);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("CHANNEL");
            }
            return "PCKingoRoot";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "PCKingoRoot";
        }
    }

    public static ActivityInfo generateActivityInfo(VPackage.ActivityComponent activityComponent, int i, PackageUserState packageUserState, int i2) {
        ActivityInfo activityInfo = null;
        if (activityComponent != null && checkUseInstalledOrHidden(packageUserState, i)) {
            activityInfo = new ActivityInfo(activityComponent.info);
            if (!((i & FileUtils.FileMode.MODE_IWUSR) == 0 || activityComponent.metaData == null)) {
                activityInfo.metaData = activityComponent.metaData;
            }
            activityInfo.applicationInfo = generateApplicationInfo(activityComponent.owner, i, packageUserState, i2);
        }
        return activityInfo;
    }

    public static ServiceInfo generateServiceInfo(VPackage.ServiceComponent serviceComponent, int i, PackageUserState packageUserState, int i2) {
        ServiceInfo serviceInfo = null;
        if (serviceComponent != null && checkUseInstalledOrHidden(packageUserState, i)) {
            serviceInfo = new ServiceInfo(serviceComponent.info);
            if (!((i & FileUtils.FileMode.MODE_IWUSR) == 0 || serviceComponent.metaData == null)) {
                serviceInfo.metaData = serviceComponent.metaData;
            }
            serviceInfo.applicationInfo = generateApplicationInfo(serviceComponent.owner, i, packageUserState, i2);
        }
        return serviceInfo;
    }

    public static ProviderInfo generateProviderInfo(VPackage.ProviderComponent providerComponent, int i, PackageUserState packageUserState, int i2) {
        if (providerComponent == null || !checkUseInstalledOrHidden(packageUserState, i)) {
            return null;
        }
        ProviderInfo providerInfo = new ProviderInfo(providerComponent.info);
        if (!((i & FileUtils.FileMode.MODE_IWUSR) == 0 || providerComponent.metaData == null)) {
            providerInfo.metaData = providerComponent.metaData;
        }
        if ((i & FileUtils.FileMode.MODE_ISUID) == 0) {
            providerInfo.uriPermissionPatterns = null;
        }
        providerInfo.applicationInfo = generateApplicationInfo(providerComponent.owner, i, packageUserState, i2);
        return providerInfo;
    }

    public static InstrumentationInfo generateInstrumentationInfo(VPackage.InstrumentationComponent instrumentationComponent, int i) {
        if (instrumentationComponent == null) {
            return null;
        }
        if ((i & FileUtils.FileMode.MODE_IWUSR) == 0) {
            return instrumentationComponent.info;
        }
        InstrumentationInfo instrumentationInfo = new InstrumentationInfo(instrumentationComponent.info);
        instrumentationInfo.metaData = instrumentationComponent.metaData;
        return instrumentationInfo;
    }

    public static PermissionInfo generatePermissionInfo(VPackage.PermissionComponent permissionComponent, int i) {
        if (permissionComponent == null) {
            return null;
        }
        if ((i & FileUtils.FileMode.MODE_IWUSR) == 0) {
            return permissionComponent.info;
        }
        PermissionInfo permissionInfo = new PermissionInfo(permissionComponent.info);
        permissionInfo.metaData = permissionComponent.metaData;
        return permissionInfo;
    }

    public static PermissionGroupInfo generatePermissionGroupInfo(VPackage.PermissionGroupComponent permissionGroupComponent, int i) {
        if (permissionGroupComponent == null) {
            return null;
        }
        if ((i & FileUtils.FileMode.MODE_IWUSR) == 0) {
            return permissionGroupComponent.info;
        }
        PermissionGroupInfo permissionGroupInfo = new PermissionGroupInfo(permissionGroupComponent.info);
        permissionGroupInfo.metaData = permissionGroupComponent.metaData;
        return permissionGroupInfo;
    }

    private static boolean checkUseInstalledOrHidden(PackageUserState packageUserState, int i) {
        return (packageUserState.installed && !packageUserState.hidden) || (i & 8192) != 0;
    }
}
