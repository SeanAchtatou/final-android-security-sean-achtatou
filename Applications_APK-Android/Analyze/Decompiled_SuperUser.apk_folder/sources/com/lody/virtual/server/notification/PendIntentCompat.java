package com.lody.virtual.server.notification;

import android.app.PendingIntent;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.VLog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PendIntentCompat {
    private Map<Integer, PendingIntent> clickIntents;
    private RemoteViews mRemoteViews;

    PendIntentCompat(RemoteViews remoteViews) {
        this.mRemoteViews = remoteViews;
    }

    public int findPendIntents() {
        if (this.clickIntents == null) {
            this.clickIntents = getClickIntents(this.mRemoteViews);
        }
        return this.clickIntents.size();
    }

    public void setPendIntent(RemoteViews remoteViews, View view, View view2) {
        int i;
        if (findPendIntents() > 0) {
            ArrayList arrayList = new ArrayList();
            VLog.v(NotificationCompat.TAG, "start find intent", new Object[0]);
            int i2 = 0;
            for (Map.Entry next : this.clickIntents.entrySet()) {
                View findViewById = view2.findViewById(((Integer) next.getKey()).intValue());
                if (findViewById != null) {
                    arrayList.add(new RectInfo(getRect(findViewById), (PendingIntent) next.getValue(), i2));
                    i = i2 + 1;
                } else {
                    i = i2;
                }
                i2 = i;
            }
            VLog.v(NotificationCompat.TAG, "find:" + arrayList, new Object[0]);
            if (view instanceof ViewGroup) {
                setIntentByViewGroup(remoteViews, (ViewGroup) view, arrayList);
            }
        }
    }

    private Rect getRect(View view) {
        Rect rect = new Rect();
        rect.top = view.getTop();
        rect.left = view.getLeft();
        rect.right = view.getRight();
        rect.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        if (parent != null && (parent instanceof ViewGroup)) {
            Rect rect2 = getRect((ViewGroup) parent);
            rect.top += rect2.top;
            rect.left += rect2.left;
            rect.right += rect2.left;
            rect.bottom = rect2.top + rect.bottom;
        }
        return rect;
    }

    private void setIntentByViewGroup(RemoteViews remoteViews, ViewGroup viewGroup, List<RectInfo> list) {
        RectInfo findIntent;
        int childCount = viewGroup.getChildCount();
        viewGroup.getHitRect(new Rect());
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof ViewGroup) {
                setIntentByViewGroup(remoteViews, (ViewGroup) childAt, list);
            } else if (((childAt instanceof TextView) || (childAt instanceof ImageView)) && (findIntent = findIntent(getRect(childAt), list)) != null) {
                remoteViews.setOnClickPendingIntent(childAt.getId(), findIntent.mPendingIntent);
            }
        }
    }

    private RectInfo findIntent(Rect rect, List<RectInfo> list) {
        int i;
        int i2 = 0;
        RectInfo rectInfo = null;
        for (RectInfo next : list) {
            int overlapArea = getOverlapArea(rect, next.rect);
            if (overlapArea > i2) {
                if (overlapArea == 0) {
                    Log.w("PendingIntentCompat", "find two:" + next.rect);
                }
                i = overlapArea;
            } else {
                next = rectInfo;
                i = i2;
            }
            i2 = i;
            rectInfo = next;
        }
        return rectInfo;
    }

    private int getOverlapArea(Rect rect, Rect rect2) {
        Rect rect3 = new Rect();
        rect3.left = Math.max(rect.left, rect2.left);
        rect3.top = Math.max(rect.top, rect2.top);
        rect3.right = Math.min(rect.right, rect2.right);
        rect3.bottom = Math.min(rect.bottom, rect2.bottom);
        if (rect3.left >= rect3.right || rect3.top >= rect3.bottom) {
            return 0;
        }
        return (rect3.bottom - rect3.top) * (rect3.right - rect3.left);
    }

    private Map<Integer, PendingIntent> getClickIntents(RemoteViews remoteViews) {
        Object obj;
        String simpleName;
        HashMap hashMap = new HashMap();
        if (remoteViews == null) {
            return hashMap;
        }
        try {
            obj = Reflect.on(remoteViews).get("mActions");
        } catch (Exception e2) {
            e2.printStackTrace();
            obj = null;
        }
        if (obj == null) {
            return hashMap;
        }
        if (obj instanceof Collection) {
            for (Object next : (Collection) obj) {
                if (next != null) {
                    try {
                        simpleName = (String) Reflect.on(next).call("getActionName").get();
                    } catch (Exception e3) {
                        simpleName = next.getClass().getSimpleName();
                    }
                    if ("SetOnClickPendingIntent".equalsIgnoreCase(simpleName)) {
                        hashMap.put(Integer.valueOf(((Integer) Reflect.on(next).get("viewId")).intValue()), (PendingIntent) Reflect.on(next).get("pendingIntent"));
                    }
                }
            }
        }
        return hashMap;
    }

    class RectInfo {
        int index;
        PendingIntent mPendingIntent;
        Rect rect;

        public RectInfo(Rect rect2, PendingIntent pendingIntent, int i) {
            this.rect = rect2;
            this.mPendingIntent = pendingIntent;
            this.index = i;
        }

        public String toString() {
            return "RectInfo{rect=" + this.rect + '}';
        }
    }
}
