package com.lody.virtual.server.am;

import android.os.IBinder;
import android.os.RemoteException;
import com.lody.virtual.remote.PendingIntentData;
import java.util.HashMap;
import java.util.Map;

public final class PendingIntents {
    /* access modifiers changed from: private */
    public final Map<IBinder, PendingIntentData> mLruHistory = new HashMap();

    /* access modifiers changed from: package-private */
    public final PendingIntentData getPendingIntent(IBinder iBinder) {
        PendingIntentData pendingIntentData;
        synchronized (this.mLruHistory) {
            pendingIntentData = this.mLruHistory.get(iBinder);
        }
        return pendingIntentData;
    }

    /* access modifiers changed from: package-private */
    public final void addPendingIntent(final IBinder iBinder, String str) {
        synchronized (this.mLruHistory) {
            try {
                iBinder.linkToDeath(new IBinder.DeathRecipient() {
                    public void binderDied() {
                        iBinder.unlinkToDeath(this, 0);
                        PendingIntents.this.mLruHistory.remove(iBinder);
                    }
                }, 0);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
            PendingIntentData pendingIntentData = this.mLruHistory.get(iBinder);
            if (pendingIntentData == null) {
                this.mLruHistory.put(iBinder, new PendingIntentData(str, iBinder));
            } else {
                pendingIntentData.creator = str;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void removePendingIntent(IBinder iBinder) {
        synchronized (this.mLruHistory) {
            this.mLruHistory.remove(iBinder);
        }
    }
}
