package com.lody.virtual.server.pm;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.collection.IntArray;
import com.lody.virtual.helper.compat.NativeLibraryHelperCompat;
import com.lody.virtual.helper.utils.ArrayUtils;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.InstallResult;
import com.lody.virtual.remote.InstalledAppInfo;
import com.lody.virtual.server.IAppManager;
import com.lody.virtual.server.accounts.VAccountManagerService;
import com.lody.virtual.server.am.BroadcastSystem;
import com.lody.virtual.server.am.UidSystem;
import com.lody.virtual.server.am.VActivityManagerService;
import com.lody.virtual.server.interfaces.IAppRequestListener;
import com.lody.virtual.server.interfaces.IPackageObserver;
import com.lody.virtual.server.pm.parser.PackageParserEx;
import com.lody.virtual.server.pm.parser.VPackage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class VAppManagerService extends IAppManager.Stub {
    private static final String TAG = VAppManagerService.class.getSimpleName();
    private static final AtomicReference<VAppManagerService> sService = new AtomicReference<>();
    /* access modifiers changed from: private */
    public IAppRequestListener mAppRequestListener;
    private boolean mBooting;
    private final PackagePersistenceLayer mPersistenceLayer = new PackagePersistenceLayer(this);
    private RemoteCallbackList<IPackageObserver> mRemoteCallbackList = new RemoteCallbackList<>();
    private final UidSystem mUidSystem = new UidSystem();
    private final Set<String> mVisibleOutsidePackages = new HashSet();

    public static VAppManagerService get() {
        return sService.get();
    }

    public static void systemReady() {
        VEnvironment.systemReady();
        VAppManagerService vAppManagerService = new VAppManagerService();
        vAppManagerService.mUidSystem.initUidList();
        sService.set(vAppManagerService);
    }

    public boolean isBooting() {
        return this.mBooting;
    }

    public void scanApps() {
        if (!this.mBooting) {
            synchronized (this) {
                this.mBooting = true;
                this.mPersistenceLayer.read();
                PrivilegeAppOptimizer.get().performOptimizeAllApps();
                this.mBooting = false;
            }
        }
    }

    private void cleanUpResidualFiles(PackageSetting packageSetting) {
        FileUtils.deleteDir(VEnvironment.getDataAppPackageDirectory(packageSetting.packageName));
        for (int dataUserPackageDirectory : VUserManagerService.get().getUserIds()) {
            FileUtils.deleteDir(VEnvironment.getDataUserPackageDirectory(dataUserPackageDirectory, packageSetting.packageName));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void loadPackage(PackageSetting packageSetting) {
        if (!loadPackageInnerLocked(packageSetting)) {
            cleanUpResidualFiles(packageSetting);
        }
    }

    private boolean loadPackageInnerLocked(PackageSetting packageSetting) {
        VPackage vPackage;
        if (packageSetting.dependSystem && !VirtualCore.get().isOutsideInstalled(packageSetting.packageName)) {
            return false;
        }
        File packageCacheFile = VEnvironment.getPackageCacheFile(packageSetting.packageName);
        try {
            vPackage = PackageParserEx.readPackageCache(packageSetting.packageName);
        } catch (Throwable th) {
            th.printStackTrace();
            vPackage = null;
        }
        if (vPackage == null || vPackage.packageName == null) {
            return false;
        }
        chmodPackageDictionary(packageCacheFile);
        PackageCacheManager.put(vPackage, packageSetting);
        BroadcastSystem.get().startApp(vPackage);
        return true;
    }

    public boolean isOutsidePackageVisible(String str) {
        return str != null && this.mVisibleOutsidePackages.contains(str);
    }

    public void addVisibleOutsidePackage(String str) {
        if (str != null) {
            this.mVisibleOutsidePackages.add(str);
        }
    }

    public void removeVisibleOutsidePackage(String str) {
        if (str != null) {
            this.mVisibleOutsidePackages.remove(str);
        }
    }

    public InstallResult installPackage(String str, int i) {
        return installPackage(str, i, true);
    }

    public synchronized InstallResult installPackage(String str, int i, boolean z) {
        InstallResult makeFailure;
        VPackage vPackage;
        boolean z2;
        File file;
        PackageSetting packageSetting;
        long currentTimeMillis = System.currentTimeMillis();
        if (str == null) {
            makeFailure = InstallResult.makeFailure("path = NULL");
        } else {
            boolean z3 = (i & 64) != 0;
            File file2 = new File(str);
            if (!file2.exists() || !file2.isFile()) {
                makeFailure = InstallResult.makeFailure("Package File is not exist.");
            } else {
                try {
                    vPackage = PackageParserEx.parsePackage(file2);
                } catch (Throwable th) {
                    th.printStackTrace();
                    vPackage = null;
                }
                if (vPackage != null) {
                    if (vPackage.packageName != null) {
                        InstallResult installResult = new InstallResult();
                        installResult.packageName = vPackage.packageName;
                        VPackage vPackage2 = PackageCacheManager.get(vPackage.packageName);
                        PackageSetting packageSetting2 = vPackage2 != null ? (PackageSetting) vPackage2.mExtras : null;
                        if (vPackage2 != null) {
                            if ((i & 16) != 0) {
                                installResult.isUpdate = true;
                                makeFailure = installResult;
                            } else if (!canUpdate(vPackage2, vPackage, i)) {
                                makeFailure = InstallResult.makeFailure("Not allowed to update the package.");
                            } else {
                                installResult.isUpdate = true;
                            }
                        }
                        File dataAppPackageDirectory = VEnvironment.getDataAppPackageDirectory(vPackage.packageName);
                        File file3 = new File(dataAppPackageDirectory, "lib");
                        if (installResult.isUpdate) {
                            FileUtils.deleteDir(file3);
                            VEnvironment.getOdexFile(vPackage.packageName).delete();
                            VActivityManagerService.get().killAppByPkg(vPackage.packageName, -1);
                        }
                        if (file3.exists() || file3.mkdirs()) {
                            boolean z4 = (i & 32) != 0 && VirtualCore.get().isOutsideInstalled(vPackage.packageName);
                            if (packageSetting2 == null || !packageSetting2.dependSystem) {
                                z2 = z4;
                            } else {
                                z2 = false;
                            }
                            NativeLibraryHelperCompat.copyNativeBinaries(new File(str), file3);
                            if (!z2) {
                                file = new File(dataAppPackageDirectory, "base.apk");
                                File parentFile = file.getParentFile();
                                if (!parentFile.exists() && !parentFile.mkdirs()) {
                                    VLog.w(TAG, "Warning: unable to create folder : " + file.getPath(), new Object[0]);
                                } else if (file.exists() && !file.delete()) {
                                    VLog.w(TAG, "Warning: unable to delete file : " + file.getPath(), new Object[0]);
                                }
                                try {
                                    FileUtils.copyFile(file2, file);
                                } catch (IOException e2) {
                                    file.delete();
                                    makeFailure = InstallResult.makeFailure("Unable to copy the package file.");
                                }
                            } else {
                                file = file2;
                            }
                            if (vPackage2 != null) {
                                PackageCacheManager.remove(vPackage.packageName);
                            }
                            chmodPackageDictionary(file);
                            if (packageSetting2 != null) {
                                packageSetting = packageSetting2;
                            } else {
                                packageSetting = new PackageSetting();
                            }
                            packageSetting.skipDexOpt = z3;
                            packageSetting.dependSystem = z2;
                            packageSetting.apkPath = file.getPath();
                            packageSetting.libPath = file3.getPath();
                            packageSetting.packageName = vPackage.packageName;
                            packageSetting.appId = VUserHandle.getAppId(this.mUidSystem.getOrCreateUid(vPackage));
                            if (installResult.isUpdate) {
                                packageSetting.lastUpdateTime = currentTimeMillis;
                            } else {
                                packageSetting.firstInstallTime = currentTimeMillis;
                                packageSetting.lastUpdateTime = currentTimeMillis;
                                int[] userIds = VUserManagerService.get().getUserIds();
                                int length = userIds.length;
                                for (int i2 = 0; i2 < length; i2++) {
                                    int i3 = userIds[i2];
                                    packageSetting.setUserState(i3, false, false, i3 == 0);
                                }
                            }
                            PackageParserEx.savePackageCache(vPackage);
                            PackageCacheManager.put(vPackage, packageSetting);
                            this.mPersistenceLayer.save();
                            BroadcastSystem.get().startApp(vPackage);
                            if (z) {
                                notifyAppInstalled(packageSetting, -1);
                            }
                            installResult.isSuccess = true;
                            makeFailure = installResult;
                        } else {
                            makeFailure = InstallResult.makeFailure("Unable to create lib dir.");
                        }
                    }
                }
                makeFailure = InstallResult.makeFailure("Unable to parse the package.");
            }
        }
        return makeFailure;
    }

    public synchronized boolean installPackageAsUser(int i, String str) {
        PackageSetting setting;
        boolean z = true;
        synchronized (this) {
            if (!VUserManagerService.get().exists(i) || (setting = PackageCacheManager.getSetting(str)) == null || setting.isInstalled(i)) {
                z = false;
            } else {
                setting.setInstalled(i, true);
                notifyAppInstalled(setting, i);
                this.mPersistenceLayer.save();
            }
        }
        return z;
    }

    private void chmodPackageDictionary(File file) {
        try {
            if (Build.VERSION.SDK_INT >= 21 && !FileUtils.isSymlink(file)) {
                FileUtils.chmod(file.getParentFile().getAbsolutePath(), FileUtils.FileMode.MODE_755);
                FileUtils.chmod(file.getAbsolutePath(), FileUtils.FileMode.MODE_755);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private boolean canUpdate(VPackage vPackage, VPackage vPackage2, int i) {
        if ((i & 8) != 0 && vPackage.mVersionCode < vPackage2.mVersionCode) {
            return true;
        }
        if ((i & 2) != 0) {
            return false;
        }
        if ((i & 4) == 0) {
            return false;
        }
        return true;
    }

    public synchronized boolean uninstallPackage(String str) {
        boolean z;
        PackageSetting setting = PackageCacheManager.getSetting(str);
        if (setting != null) {
            uninstallPackageFully(setting);
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized boolean uninstallPackageAsUser(String str, int i) {
        boolean z = false;
        synchronized (this) {
            if (VUserManagerService.get().exists(i)) {
                PackageSetting setting = PackageCacheManager.getSetting(str);
                if (setting != null) {
                    int[] packageInstalledUsers = getPackageInstalledUsers(str);
                    if (ArrayUtils.contains(packageInstalledUsers, i)) {
                        if (packageInstalledUsers.length == 1) {
                            uninstallPackageFully(setting);
                        } else {
                            VActivityManagerService.get().killAppByPkg(str, i);
                            setting.setInstalled(i, false);
                            notifyAppUninstalled(setting, i);
                            this.mPersistenceLayer.save();
                            FileUtils.deleteDir(VEnvironment.getDataUserPackageDirectory(i, str));
                        }
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    private void uninstallPackageFully(PackageSetting packageSetting) {
        String str = packageSetting.packageName;
        try {
            BroadcastSystem.get().stopApp(str);
            VActivityManagerService.get().killAppByPkg(str, -1);
            VEnvironment.getPackageResourcePath(str).delete();
            FileUtils.deleteDir(VEnvironment.getDataAppPackageDirectory(str));
            VEnvironment.getOdexFile(str).delete();
            for (int dataUserPackageDirectory : VUserManagerService.get().getUserIds()) {
                FileUtils.deleteDir(VEnvironment.getDataUserPackageDirectory(dataUserPackageDirectory, str));
            }
            PackageCacheManager.remove(str);
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            notifyAppUninstalled(packageSetting, -1);
        }
    }

    public int[] getPackageInstalledUsers(String str) {
        PackageSetting setting = PackageCacheManager.getSetting(str);
        if (setting == null) {
            return new int[0];
        }
        IntArray intArray = new IntArray(5);
        for (int i : VUserManagerService.get().getUserIds()) {
            if (setting.readUserState(i).installed) {
                intArray.add(i);
            }
        }
        return intArray.getAll();
    }

    public List<InstalledAppInfo> getInstalledApps(int i) {
        ArrayList arrayList = new ArrayList(getInstalledAppCount());
        for (VPackage vPackage : PackageCacheManager.PACKAGE_CACHE.values()) {
            arrayList.add(((PackageSetting) vPackage.mExtras).getAppInfo());
        }
        return arrayList;
    }

    public List<InstalledAppInfo> getInstalledAppsAsUser(int i, int i2) {
        ArrayList arrayList = new ArrayList(getInstalledAppCount());
        for (VPackage vPackage : PackageCacheManager.PACKAGE_CACHE.values()) {
            PackageSetting packageSetting = (PackageSetting) vPackage.mExtras;
            boolean isInstalled = packageSetting.isInstalled(i);
            if ((i2 & 1) == 0 && packageSetting.isHidden(i)) {
                isInstalled = false;
            }
            if (isInstalled) {
                arrayList.add(packageSetting.getAppInfo());
            }
        }
        return arrayList;
    }

    public int getInstalledAppCount() {
        return PackageCacheManager.PACKAGE_CACHE.size();
    }

    public boolean isAppInstalled(String str) {
        return str != null && PackageCacheManager.PACKAGE_CACHE.containsKey(str);
    }

    public boolean isAppInstalledAsUser(int i, String str) {
        PackageSetting setting;
        if (str == null || !VUserManagerService.get().exists(i) || (setting = PackageCacheManager.getSetting(str)) == null) {
            return false;
        }
        return setting.isInstalled(i);
    }

    private void notifyAppInstalled(PackageSetting packageSetting, int i) {
        String str = packageSetting.packageName;
        int beginBroadcast = this.mRemoteCallbackList.beginBroadcast();
        while (true) {
            int i2 = beginBroadcast - 1;
            if (beginBroadcast > 0) {
                if (i == -1) {
                    try {
                        sendInstalledBroadcast(str);
                        this.mRemoteCallbackList.getBroadcastItem(i2).onPackageInstalled(str);
                        this.mRemoteCallbackList.getBroadcastItem(i2).onPackageInstalledAsUser(0, str);
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                        beginBroadcast = i2;
                    }
                } else {
                    this.mRemoteCallbackList.getBroadcastItem(i2).onPackageInstalledAsUser(i, str);
                }
                beginBroadcast = i2;
            } else {
                this.mRemoteCallbackList.finishBroadcast();
                VAccountManagerService.get().refreshAuthenticatorCache(null);
                return;
            }
        }
    }

    private void notifyAppUninstalled(PackageSetting packageSetting, int i) {
        String str = packageSetting.packageName;
        int beginBroadcast = this.mRemoteCallbackList.beginBroadcast();
        while (true) {
            int i2 = beginBroadcast - 1;
            if (beginBroadcast > 0) {
                if (i == -1) {
                    try {
                        sendUninstalledBroadcast(str);
                        this.mRemoteCallbackList.getBroadcastItem(i2).onPackageUninstalled(str);
                        this.mRemoteCallbackList.getBroadcastItem(i2).onPackageUninstalledAsUser(0, str);
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                        beginBroadcast = i2;
                    }
                } else {
                    this.mRemoteCallbackList.getBroadcastItem(i2).onPackageUninstalledAsUser(i, str);
                }
                beginBroadcast = i2;
            } else {
                this.mRemoteCallbackList.finishBroadcast();
                VAccountManagerService.get().refreshAuthenticatorCache(null);
                return;
            }
        }
    }

    private void sendInstalledBroadcast(String str) {
        Intent intent = new Intent("android.intent.action.PACKAGE_ADDED");
        intent.setData(Uri.parse("package:" + str));
        VActivityManagerService.get().sendBroadcastAsUser(intent, VUserHandle.ALL);
    }

    private void sendUninstalledBroadcast(String str) {
        Intent intent = new Intent("android.intent.action.PACKAGE_REMOVED");
        intent.setData(Uri.parse("package:" + str));
        VActivityManagerService.get().sendBroadcastAsUser(intent, VUserHandle.ALL);
    }

    public void registerObserver(IPackageObserver iPackageObserver) {
        try {
            this.mRemoteCallbackList.register(iPackageObserver);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void unregisterObserver(IPackageObserver iPackageObserver) {
        try {
            this.mRemoteCallbackList.unregister(iPackageObserver);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public IAppRequestListener getAppRequestListener() {
        return this.mAppRequestListener;
    }

    public void setAppRequestListener(final IAppRequestListener iAppRequestListener) {
        this.mAppRequestListener = iAppRequestListener;
        if (iAppRequestListener != null) {
            try {
                iAppRequestListener.asBinder().linkToDeath(new IBinder.DeathRecipient() {
                    public void binderDied() {
                        iAppRequestListener.asBinder().unlinkToDeath(this, 0);
                        IAppRequestListener unused = VAppManagerService.this.mAppRequestListener = null;
                    }
                }, 0);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void clearAppRequestListener() {
        this.mAppRequestListener = null;
    }

    public InstalledAppInfo getInstalledAppInfo(String str, int i) {
        InstalledAppInfo installedAppInfo;
        synchronized (PackageCacheManager.class) {
            if (str != null) {
                PackageSetting setting = PackageCacheManager.getSetting(str);
                if (setting != null) {
                    installedAppInfo = setting.getAppInfo();
                }
            }
            installedAppInfo = null;
        }
        return installedAppInfo;
    }

    public boolean isPackageLaunched(int i, String str) {
        PackageSetting setting = PackageCacheManager.getSetting(str);
        return setting != null && setting.isLaunched(i);
    }

    public void setPackageHidden(int i, String str, boolean z) {
        PackageSetting setting = PackageCacheManager.getSetting(str);
        if (setting != null && VUserManagerService.get().exists(i)) {
            setting.setHidden(i, z);
            this.mPersistenceLayer.save();
        }
    }

    public int getAppId(String str) {
        PackageSetting setting = PackageCacheManager.getSetting(str);
        if (setting != null) {
            return setting.appId;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void restoreFactoryState() {
        VLog.w(TAG, "Warning: Restore the factory state...", new Object[0]);
        VEnvironment.getDalvikCacheDirectory().delete();
        VEnvironment.getUserSystemDirectory().delete();
        VEnvironment.getDataAppDirectory().delete();
    }

    public void savePersistenceData() {
        this.mPersistenceLayer.save();
    }
}
