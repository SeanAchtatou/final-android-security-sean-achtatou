package com.lody.virtual.server;

import android.content.IntentSender;
import android.content.pm.IPackageInstallerCallback;
import android.content.pm.IPackageInstallerSession;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.VParceledListSlice;
import com.lody.virtual.server.pm.installer.SessionInfo;
import com.lody.virtual.server.pm.installer.SessionParams;

public interface IPackageInstaller extends IInterface {
    void abandonSession(int i);

    int createSession(SessionParams sessionParams, String str, int i);

    VParceledListSlice getAllSessions(int i);

    VParceledListSlice getMySessions(String str, int i);

    SessionInfo getSessionInfo(int i);

    IPackageInstallerSession openSession(int i);

    void registerCallback(IPackageInstallerCallback iPackageInstallerCallback, int i);

    void setPermissionsResult(int i, boolean z);

    void uninstall(String str, String str2, int i, IntentSender intentSender, int i2);

    void unregisterCallback(IPackageInstallerCallback iPackageInstallerCallback);

    void updateSessionAppIcon(int i, Bitmap bitmap);

    void updateSessionAppLabel(int i, String str);

    public static abstract class Stub extends Binder implements IPackageInstaller {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IPackageInstaller";
        static final int TRANSACTION_abandonSession = 4;
        static final int TRANSACTION_createSession = 1;
        static final int TRANSACTION_getAllSessions = 7;
        static final int TRANSACTION_getMySessions = 8;
        static final int TRANSACTION_getSessionInfo = 6;
        static final int TRANSACTION_openSession = 5;
        static final int TRANSACTION_registerCallback = 9;
        static final int TRANSACTION_setPermissionsResult = 12;
        static final int TRANSACTION_uninstall = 11;
        static final int TRANSACTION_unregisterCallback = 10;
        static final int TRANSACTION_updateSessionAppIcon = 2;
        static final int TRANSACTION_updateSessionAppLabel = 3;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IPackageInstaller asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IPackageInstaller)) {
                return new Proxy(iBinder);
            }
            return (IPackageInstaller) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r0v0 */
        /* JADX WARN: Type inference failed for: r0v25, types: [android.os.IBinder] */
        /* JADX WARN: Type inference failed for: r0v31, types: [android.graphics.Bitmap] */
        /* JADX WARN: Type inference failed for: r0v35, types: [com.lody.virtual.server.pm.installer.SessionParams] */
        /* JADX WARN: Type inference failed for: r0v40 */
        /* JADX WARN: Type inference failed for: r0v41 */
        /* JADX WARN: Type inference failed for: r0v42 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r8, android.os.Parcel r9, android.os.Parcel r10, int r11) {
            /*
                r7 = this;
                r0 = 0
                r1 = 0
                r6 = 1
                switch(r8) {
                    case 1: goto L_0x0011;
                    case 2: goto L_0x0037;
                    case 3: goto L_0x0055;
                    case 4: goto L_0x0069;
                    case 5: goto L_0x0079;
                    case 6: goto L_0x0094;
                    case 7: goto L_0x00b3;
                    case 8: goto L_0x00d2;
                    case 9: goto L_0x00f5;
                    case 10: goto L_0x010e;
                    case 11: goto L_0x0123;
                    case 12: goto L_0x0152;
                    case 1598968902: goto L_0x000b;
                    default: goto L_0x0006;
                }
            L_0x0006:
                boolean r6 = super.onTransact(r8, r9, r10, r11)
            L_0x000a:
                return r6
            L_0x000b:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r10.writeString(r0)
                goto L_0x000a
            L_0x0011:
                java.lang.String r1 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r1)
                int r1 = r9.readInt()
                if (r1 == 0) goto L_0x0024
                android.os.Parcelable$Creator<com.lody.virtual.server.pm.installer.SessionParams> r0 = com.lody.virtual.server.pm.installer.SessionParams.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r9)
                com.lody.virtual.server.pm.installer.SessionParams r0 = (com.lody.virtual.server.pm.installer.SessionParams) r0
            L_0x0024:
                java.lang.String r1 = r9.readString()
                int r2 = r9.readInt()
                int r0 = r7.createSession(r0, r1, r2)
                r10.writeNoException()
                r10.writeInt(r0)
                goto L_0x000a
            L_0x0037:
                java.lang.String r1 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r1)
                int r1 = r9.readInt()
                int r2 = r9.readInt()
                if (r2 == 0) goto L_0x004e
                android.os.Parcelable$Creator r0 = android.graphics.Bitmap.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r9)
                android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            L_0x004e:
                r7.updateSessionAppIcon(r1, r0)
                r10.writeNoException()
                goto L_0x000a
            L_0x0055:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                int r0 = r9.readInt()
                java.lang.String r1 = r9.readString()
                r7.updateSessionAppLabel(r0, r1)
                r10.writeNoException()
                goto L_0x000a
            L_0x0069:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                int r0 = r9.readInt()
                r7.abandonSession(r0)
                r10.writeNoException()
                goto L_0x000a
            L_0x0079:
                java.lang.String r1 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r1)
                int r1 = r9.readInt()
                android.content.pm.IPackageInstallerSession r1 = r7.openSession(r1)
                r10.writeNoException()
                if (r1 == 0) goto L_0x008f
                android.os.IBinder r0 = r1.asBinder()
            L_0x008f:
                r10.writeStrongBinder(r0)
                goto L_0x000a
            L_0x0094:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                int r0 = r9.readInt()
                com.lody.virtual.server.pm.installer.SessionInfo r0 = r7.getSessionInfo(r0)
                r10.writeNoException()
                if (r0 == 0) goto L_0x00ae
                r10.writeInt(r6)
                r0.writeToParcel(r10, r6)
                goto L_0x000a
            L_0x00ae:
                r10.writeInt(r1)
                goto L_0x000a
            L_0x00b3:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                int r0 = r9.readInt()
                com.lody.virtual.remote.VParceledListSlice r0 = r7.getAllSessions(r0)
                r10.writeNoException()
                if (r0 == 0) goto L_0x00cd
                r10.writeInt(r6)
                r0.writeToParcel(r10, r6)
                goto L_0x000a
            L_0x00cd:
                r10.writeInt(r1)
                goto L_0x000a
            L_0x00d2:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                java.lang.String r0 = r9.readString()
                int r2 = r9.readInt()
                com.lody.virtual.remote.VParceledListSlice r0 = r7.getMySessions(r0, r2)
                r10.writeNoException()
                if (r0 == 0) goto L_0x00f0
                r10.writeInt(r6)
                r0.writeToParcel(r10, r6)
                goto L_0x000a
            L_0x00f0:
                r10.writeInt(r1)
                goto L_0x000a
            L_0x00f5:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                android.os.IBinder r0 = r9.readStrongBinder()
                android.content.pm.IPackageInstallerCallback r0 = android.content.pm.IPackageInstallerCallback.Stub.asInterface(r0)
                int r1 = r9.readInt()
                r7.registerCallback(r0, r1)
                r10.writeNoException()
                goto L_0x000a
            L_0x010e:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                android.os.IBinder r0 = r9.readStrongBinder()
                android.content.pm.IPackageInstallerCallback r0 = android.content.pm.IPackageInstallerCallback.Stub.asInterface(r0)
                r7.unregisterCallback(r0)
                r10.writeNoException()
                goto L_0x000a
            L_0x0123:
                java.lang.String r1 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r1)
                java.lang.String r1 = r9.readString()
                java.lang.String r2 = r9.readString()
                int r3 = r9.readInt()
                int r4 = r9.readInt()
                if (r4 == 0) goto L_0x0150
                android.os.Parcelable$Creator r0 = android.content.IntentSender.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r9)
                android.content.IntentSender r0 = (android.content.IntentSender) r0
                r4 = r0
            L_0x0143:
                int r5 = r9.readInt()
                r0 = r7
                r0.uninstall(r1, r2, r3, r4, r5)
                r10.writeNoException()
                goto L_0x000a
            L_0x0150:
                r4 = r0
                goto L_0x0143
            L_0x0152:
                java.lang.String r0 = "com.lody.virtual.server.IPackageInstaller"
                r9.enforceInterface(r0)
                int r2 = r9.readInt()
                int r0 = r9.readInt()
                if (r0 == 0) goto L_0x016a
                r0 = r6
            L_0x0162:
                r7.setPermissionsResult(r2, r0)
                r10.writeNoException()
                goto L_0x000a
            L_0x016a:
                r0 = r1
                goto L_0x0162
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.IPackageInstaller.Stub.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }

        private static class Proxy implements IPackageInstaller {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public int createSession(SessionParams sessionParams, String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (sessionParams != null) {
                        obtain.writeInt(1);
                        sessionParams.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void updateSessionAppIcon(int i, Bitmap bitmap) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (bitmap != null) {
                        obtain.writeInt(1);
                        bitmap.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void updateSessionAppLabel(int i, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void abandonSession(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IPackageInstallerSession openSession(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return IPackageInstallerSession.Stub.asInterface(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public SessionInfo getSessionInfo(int i) {
                SessionInfo sessionInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        sessionInfo = SessionInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        sessionInfo = null;
                    }
                    return sessionInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VParceledListSlice getAllSessions(int i) {
                VParceledListSlice vParceledListSlice;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vParceledListSlice = VParceledListSlice.CREATOR.createFromParcel(obtain2);
                    } else {
                        vParceledListSlice = null;
                    }
                    return vParceledListSlice;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VParceledListSlice getMySessions(String str, int i) {
                VParceledListSlice vParceledListSlice;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vParceledListSlice = VParceledListSlice.CREATOR.createFromParcel(obtain2);
                    } else {
                        vParceledListSlice = null;
                    }
                    return vParceledListSlice;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void registerCallback(IPackageInstallerCallback iPackageInstallerCallback, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iPackageInstallerCallback != null ? iPackageInstallerCallback.asBinder() : null);
                    obtain.writeInt(i);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterCallback(IPackageInstallerCallback iPackageInstallerCallback) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iPackageInstallerCallback != null ? iPackageInstallerCallback.asBinder() : null);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void uninstall(String str, String str2, int i, IntentSender intentSender, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    if (intentSender != null) {
                        obtain.writeInt(1);
                        intentSender.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i2);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setPermissionsResult(int i, boolean z) {
                int i2 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (z) {
                        i2 = 1;
                    }
                    obtain.writeInt(i2);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
