package com.lody.virtual.server.pm.installer;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.IPackageInstallerCallback;
import android.content.pm.IPackageInstallerSession;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.SparseArray;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.compat.ObjectsCompat;
import com.lody.virtual.helper.utils.Singleton;
import com.lody.virtual.os.VBinder;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.VParceledListSlice;
import com.lody.virtual.server.IPackageInstaller;
import com.lody.virtual.server.pm.VAppManagerService;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

@TargetApi(21)
public class VPackageInstallerService extends IPackageInstaller.Stub {
    private static final long MAX_ACTIVE_SESSIONS = 1024;
    private static final String TAG = "PackageInstaller";
    private static final Singleton<VPackageInstallerService> gDefault = new Singleton<VPackageInstallerService>() {
        /* access modifiers changed from: protected */
        public VPackageInstallerService create() {
            return new VPackageInstallerService();
        }
    };
    /* access modifiers changed from: private */
    public final Callbacks mCallbacks;
    private Context mContext;
    /* access modifiers changed from: private */
    public final Handler mInstallHandler;
    private final HandlerThread mInstallThread;
    private final InternalCallback mInternalCallback;
    private final Random mRandom;
    /* access modifiers changed from: private */
    public final SparseArray<PackageInstallerSession> mSessions;

    private VPackageInstallerService() {
        this.mRandom = new SecureRandom();
        this.mSessions = new SparseArray<>();
        this.mInternalCallback = new InternalCallback();
        this.mContext = VirtualCore.get().getContext();
        this.mInstallThread = new HandlerThread(TAG);
        this.mInstallThread.start();
        this.mInstallHandler = new Handler(this.mInstallThread.getLooper());
        this.mCallbacks = new Callbacks(this.mInstallThread.getLooper());
    }

    public static VPackageInstallerService get() {
        return gDefault.get();
    }

    private static int getSessionCount(SparseArray<PackageInstallerSession> sparseArray, int i) {
        int i2;
        int size = sparseArray.size();
        int i3 = 0;
        int i4 = 0;
        while (i3 < size) {
            if (sparseArray.valueAt(i3).installerUid == i) {
                i2 = i4 + 1;
            } else {
                i2 = i4;
            }
            i3++;
            i4 = i2;
        }
        return i4;
    }

    public int createSession(SessionParams sessionParams, String str, int i) {
        try {
            return createSessionInternal(sessionParams, str, i);
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private int createSessionInternal(SessionParams sessionParams, String str, int i) {
        int allocateSessionIdLocked;
        PackageInstallerSession packageInstallerSession;
        int callingUid = VBinder.getCallingUid();
        synchronized (this.mSessions) {
            if (((long) getSessionCount(this.mSessions, callingUid)) >= MAX_ACTIVE_SESSIONS) {
                throw new IllegalStateException("Too many active sessions for UID " + callingUid);
            }
            allocateSessionIdLocked = allocateSessionIdLocked();
            packageInstallerSession = new PackageInstallerSession(this.mInternalCallback, this.mContext, this.mInstallHandler.getLooper(), str, allocateSessionIdLocked, i, callingUid, sessionParams, VEnvironment.getPackageInstallerStageDir());
        }
        this.mCallbacks.notifySessionCreated(packageInstallerSession.sessionId, packageInstallerSession.userId);
        return allocateSessionIdLocked;
    }

    public void updateSessionAppIcon(int i, Bitmap bitmap) {
        synchronized (this.mSessions) {
            PackageInstallerSession packageInstallerSession = this.mSessions.get(i);
            if (packageInstallerSession == null || !isCallingUidOwner(packageInstallerSession)) {
                throw new SecurityException("Caller has no access to session " + i);
            }
            packageInstallerSession.params.appIcon = bitmap;
            packageInstallerSession.params.appIconLastModified = -1;
            this.mInternalCallback.onSessionBadgingChanged(packageInstallerSession);
        }
    }

    public void updateSessionAppLabel(int i, String str) {
        synchronized (this.mSessions) {
            PackageInstallerSession packageInstallerSession = this.mSessions.get(i);
            if (packageInstallerSession == null || !isCallingUidOwner(packageInstallerSession)) {
                throw new SecurityException("Caller has no access to session " + i);
            }
            packageInstallerSession.params.appLabel = str;
            this.mInternalCallback.onSessionBadgingChanged(packageInstallerSession);
        }
    }

    public void abandonSession(int i) {
        synchronized (this.mSessions) {
            PackageInstallerSession packageInstallerSession = this.mSessions.get(i);
            if (packageInstallerSession == null || !isCallingUidOwner(packageInstallerSession)) {
                throw new SecurityException("Caller has no access to session " + i);
            }
            packageInstallerSession.abandon();
        }
    }

    public IPackageInstallerSession openSession(int i) {
        try {
            return openSessionInternal(i);
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private IPackageInstallerSession openSessionInternal(int i) {
        PackageInstallerSession packageInstallerSession;
        synchronized (this.mSessions) {
            packageInstallerSession = this.mSessions.get(i);
            if (packageInstallerSession == null || !isCallingUidOwner(packageInstallerSession)) {
                throw new SecurityException("Caller has no access to session " + i);
            }
            packageInstallerSession.open();
        }
        return packageInstallerSession;
    }

    public SessionInfo getSessionInfo(int i) {
        SessionInfo generateInfo;
        synchronized (this.mSessions) {
            PackageInstallerSession packageInstallerSession = this.mSessions.get(i);
            generateInfo = packageInstallerSession != null ? packageInstallerSession.generateInfo() : null;
        }
        return generateInfo;
    }

    public VParceledListSlice getAllSessions(int i) {
        ArrayList arrayList = new ArrayList();
        synchronized (this.mSessions) {
            for (int i2 = 0; i2 < this.mSessions.size(); i2++) {
                PackageInstallerSession valueAt = this.mSessions.valueAt(i2);
                if (valueAt.userId == i) {
                    arrayList.add(valueAt.generateInfo());
                }
            }
        }
        return new VParceledListSlice(arrayList);
    }

    public VParceledListSlice getMySessions(String str, int i) {
        ArrayList arrayList = new ArrayList();
        synchronized (this.mSessions) {
            for (int i2 = 0; i2 < this.mSessions.size(); i2++) {
                PackageInstallerSession valueAt = this.mSessions.valueAt(i2);
                if (ObjectsCompat.equals(valueAt.installerPackageName, str) && valueAt.userId == i) {
                    arrayList.add(valueAt.generateInfo());
                }
            }
        }
        return new VParceledListSlice(arrayList);
    }

    public void registerCallback(IPackageInstallerCallback iPackageInstallerCallback, int i) {
        this.mCallbacks.register(iPackageInstallerCallback, i);
    }

    public void unregisterCallback(IPackageInstallerCallback iPackageInstallerCallback) {
        this.mCallbacks.unregister(iPackageInstallerCallback);
    }

    public void uninstall(String str, String str2, int i, IntentSender intentSender, int i2) {
        int i3 = 1;
        int i4 = 0;
        boolean uninstallPackage = VAppManagerService.get().uninstallPackage(str);
        if (intentSender != null) {
            Intent intent = new Intent();
            intent.putExtra("android.content.pm.extra.PACKAGE_NAME", str);
            if (!uninstallPackage) {
                i4 = 1;
            }
            intent.putExtra("android.content.pm.extra.STATUS", i4);
            intent.putExtra("android.content.pm.extra.STATUS_MESSAGE", PackageHelper.deleteStatusToString(uninstallPackage));
            if (!uninstallPackage) {
                i3 = -1;
            }
            intent.putExtra("android.content.pm.extra.LEGACY_STATUS", i3);
            try {
                intentSender.sendIntent(this.mContext, 0, intent, null, null);
            } catch (IntentSender.SendIntentException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void setPermissionsResult(int i, boolean z) {
        synchronized (this.mSessions) {
            PackageInstallerSession packageInstallerSession = this.mSessions.get(i);
            if (packageInstallerSession != null) {
                packageInstallerSession.setPermissionsResult(z);
            }
        }
    }

    private boolean isCallingUidOwner(PackageInstallerSession packageInstallerSession) {
        return true;
    }

    private int allocateSessionIdLocked() {
        int i = 0;
        while (true) {
            int nextInt = this.mRandom.nextInt(2147483646) + 1;
            if (this.mSessions.get(nextInt) == null) {
                return nextInt;
            }
            int i2 = i + 1;
            if (i >= 32) {
                throw new IllegalStateException("Failed to allocate session ID");
            }
            i = i2;
        }
    }

    private static class Callbacks extends Handler {
        private static final int MSG_SESSION_ACTIVE_CHANGED = 3;
        private static final int MSG_SESSION_BADGING_CHANGED = 2;
        private static final int MSG_SESSION_CREATED = 1;
        private static final int MSG_SESSION_FINISHED = 5;
        private static final int MSG_SESSION_PROGRESS_CHANGED = 4;
        private final RemoteCallbackList<IPackageInstallerCallback> mCallbacks = new RemoteCallbackList<>();

        public Callbacks(Looper looper) {
            super(looper);
        }

        public void register(IPackageInstallerCallback iPackageInstallerCallback, int i) {
            this.mCallbacks.register(iPackageInstallerCallback, new VUserHandle(i));
        }

        public void unregister(IPackageInstallerCallback iPackageInstallerCallback) {
            this.mCallbacks.unregister(iPackageInstallerCallback);
        }

        public void handleMessage(Message message) {
            int i = message.arg2;
            int beginBroadcast = this.mCallbacks.beginBroadcast();
            for (int i2 = 0; i2 < beginBroadcast; i2++) {
                IPackageInstallerCallback broadcastItem = this.mCallbacks.getBroadcastItem(i2);
                if (i == ((VUserHandle) this.mCallbacks.getBroadcastCookie(i2)).getIdentifier()) {
                    try {
                        invokeCallback(broadcastItem, message);
                    } catch (RemoteException e2) {
                    }
                }
            }
            this.mCallbacks.finishBroadcast();
        }

        private void invokeCallback(IPackageInstallerCallback iPackageInstallerCallback, Message message) {
            int i = message.arg1;
            switch (message.what) {
                case 1:
                    iPackageInstallerCallback.onSessionCreated(i);
                    return;
                case 2:
                    iPackageInstallerCallback.onSessionBadgingChanged(i);
                    return;
                case 3:
                    iPackageInstallerCallback.onSessionActiveChanged(i, ((Boolean) message.obj).booleanValue());
                    return;
                case 4:
                    iPackageInstallerCallback.onSessionProgressChanged(i, ((Float) message.obj).floatValue());
                    return;
                case 5:
                    iPackageInstallerCallback.onSessionFinished(i, ((Boolean) message.obj).booleanValue());
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: private */
        public void notifySessionCreated(int i, int i2) {
            obtainMessage(1, i, i2).sendToTarget();
        }

        /* access modifiers changed from: private */
        public void notifySessionBadgingChanged(int i, int i2) {
            obtainMessage(2, i, i2).sendToTarget();
        }

        /* access modifiers changed from: private */
        public void notifySessionActiveChanged(int i, int i2, boolean z) {
            obtainMessage(3, i, i2, Boolean.valueOf(z)).sendToTarget();
        }

        /* access modifiers changed from: private */
        public void notifySessionProgressChanged(int i, int i2, float f2) {
            obtainMessage(4, i, i2, Float.valueOf(f2)).sendToTarget();
        }

        public void notifySessionFinished(int i, int i2, boolean z) {
            obtainMessage(5, i, i2, Boolean.valueOf(z)).sendToTarget();
        }
    }

    static class PackageInstallObserverAdapter extends PackageInstallObserver {
        private final Context mContext;
        private final int mSessionId;
        private final IntentSender mTarget;
        private final int mUserId;

        PackageInstallObserverAdapter(Context context, IntentSender intentSender, int i, int i2) {
            this.mContext = context;
            this.mTarget = intentSender;
            this.mSessionId = i;
            this.mUserId = i2;
        }

        public void onUserActionRequired(Intent intent) {
            Intent intent2 = new Intent();
            intent2.putExtra("android.content.pm.extra.SESSION_ID", this.mSessionId);
            intent2.putExtra("android.content.pm.extra.STATUS", -1);
            intent2.putExtra("android.intent.extra.INTENT", intent);
            try {
                this.mTarget.sendIntent(this.mContext, 0, intent2, null, null);
            } catch (IntentSender.SendIntentException e2) {
            }
        }

        public void onPackageInstalled(String str, int i, String str2, Bundle bundle) {
            Intent intent = new Intent();
            intent.putExtra("android.content.pm.extra.PACKAGE_NAME", str);
            intent.putExtra("android.content.pm.extra.SESSION_ID", this.mSessionId);
            intent.putExtra("android.content.pm.extra.STATUS", PackageHelper.installStatusToPublicStatus(i));
            intent.putExtra("android.content.pm.extra.STATUS_MESSAGE", PackageHelper.installStatusToString(i, str2));
            intent.putExtra("android.content.pm.extra.LEGACY_STATUS", i);
            if (bundle != null) {
                String string = bundle.getString("android.content.pm.extra.FAILURE_EXISTING_PACKAGE");
                if (!TextUtils.isEmpty(string)) {
                    intent.putExtra("android.content.pm.extra.OTHER_PACKAGE_NAME", string);
                }
            }
            try {
                this.mTarget.sendIntent(this.mContext, 0, intent, null, null);
            } catch (IntentSender.SendIntentException e2) {
            }
        }
    }

    class InternalCallback {
        InternalCallback() {
        }

        public void onSessionBadgingChanged(PackageInstallerSession packageInstallerSession) {
            VPackageInstallerService.this.mCallbacks.notifySessionBadgingChanged(packageInstallerSession.sessionId, packageInstallerSession.userId);
        }

        public void onSessionActiveChanged(PackageInstallerSession packageInstallerSession, boolean z) {
            VPackageInstallerService.this.mCallbacks.notifySessionActiveChanged(packageInstallerSession.sessionId, packageInstallerSession.userId, z);
        }

        public void onSessionProgressChanged(PackageInstallerSession packageInstallerSession, float f2) {
            VPackageInstallerService.this.mCallbacks.notifySessionProgressChanged(packageInstallerSession.sessionId, packageInstallerSession.userId, f2);
        }

        public void onSessionFinished(final PackageInstallerSession packageInstallerSession, boolean z) {
            VPackageInstallerService.this.mCallbacks.notifySessionFinished(packageInstallerSession.sessionId, packageInstallerSession.userId, z);
            VPackageInstallerService.this.mInstallHandler.post(new Runnable() {
                public void run() {
                    synchronized (VPackageInstallerService.this.mSessions) {
                        VPackageInstallerService.this.mSessions.remove(packageInstallerSession.sessionId);
                    }
                }
            });
        }

        public void onSessionPrepared(PackageInstallerSession packageInstallerSession) {
        }

        public void onSessionSealedBlocking(PackageInstallerSession packageInstallerSession) {
        }
    }
}
