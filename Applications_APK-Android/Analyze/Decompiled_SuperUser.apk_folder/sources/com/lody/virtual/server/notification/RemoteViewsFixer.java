package com.lody.virtual.server.notification;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import com.lody.virtual.R;
import com.lody.virtual.helper.utils.Reflect;
import com.lody.virtual.helper.utils.VLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class RemoteViewsFixer {
    private static final String TAG = NotificationCompat.TAG;
    private boolean init = false;
    private final HashMap<String, Bitmap> mImages = new HashMap<>();
    private NotificationCompat mNotificationCompat;
    private final WidthCompat mWidthCompat = new WidthCompat();
    private int notification_max_height;
    private int notification_mid_height;
    private int notification_min_height;
    private int notification_padding;
    private int notification_panel_width;
    private int notification_side_padding;

    RemoteViewsFixer(NotificationCompat notificationCompat) {
        this.mNotificationCompat = notificationCompat;
    }

    /* access modifiers changed from: package-private */
    public View toView(Context context, RemoteViews remoteViews, boolean z, boolean z2) {
        try {
            return createView(context, remoteViews, z, z2);
        } catch (Throwable th) {
            VLog.w(TAG, "toView 2", th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap createBitmap(View view) {
        if (view == null) {
            return null;
        }
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        return view.getDrawingCache();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View apply(Context context, RemoteViews remoteViews) {
        View view = null;
        try {
            view = LayoutInflater.from(context).inflate(remoteViews.getLayoutId(), (ViewGroup) null, false);
            try {
                Reflect.on(view).call("setTagInternal", Reflect.on("com.android.internal.R$id").get("widget_frame"), Integer.valueOf(remoteViews.getLayoutId()));
            } catch (Exception e2) {
                VLog.w(TAG, "setTagInternal", e2);
            }
        } catch (Exception e3) {
            VLog.w(TAG, "inflate", e3);
        }
        if (view != null) {
            ArrayList arrayList = (ArrayList) Reflect.on(remoteViews).get("mActions");
            if (arrayList != null) {
                VLog.d(TAG, "apply actions:" + arrayList.size(), new Object[0]);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    try {
                        Reflect.on(it.next()).call("apply", view, null, null);
                    } catch (Exception e4) {
                        VLog.w(TAG, "apply action", e4);
                    }
                }
            }
        } else {
            VLog.e(TAG, "create views", new Object[0]);
        }
        return view;
    }

    private View createView(Context context, RemoteViews remoteViews, boolean z, boolean z2) {
        int i;
        if (remoteViews == null) {
            return null;
        }
        Context hostContext = this.mNotificationCompat.getHostContext();
        init(hostContext);
        VLog.v(TAG, "createView:big=" + z + ",system=" + z2, new Object[0]);
        int i2 = z ? this.notification_max_height : this.notification_min_height;
        int notificationWidth = this.mWidthCompat.getNotificationWidth(hostContext, this.notification_panel_width, i2, this.notification_side_padding);
        VLog.v(TAG, "createView:getNotificationWidth=" + notificationWidth, new Object[0]);
        FrameLayout frameLayout = new FrameLayout(context);
        VLog.v(TAG, "createView:apply", new Object[0]);
        View apply = apply(context, remoteViews);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 16;
        frameLayout.addView(apply, layoutParams);
        if (apply instanceof ViewGroup) {
            VLog.v(TAG, "createView:fixTextView", new Object[0]);
            fixTextView((ViewGroup) apply);
        }
        if (z2) {
            i = 1073741824;
        } else {
            i = z ? Integer.MIN_VALUE : 1073741824;
        }
        VLog.v(TAG, "createView:layout", new Object[0]);
        frameLayout.layout(0, 0, notificationWidth, i2);
        frameLayout.measure(View.MeasureSpec.makeMeasureSpec(notificationWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(i2, i));
        frameLayout.layout(0, 0, notificationWidth, frameLayout.getMeasuredHeight());
        VLog.v(TAG, "notification:systemId=" + z2 + ",max=%d/%d, szie=%d/%d", Integer.valueOf(notificationWidth), Integer.valueOf(i2), Integer.valueOf(frameLayout.getMeasuredWidth()), Integer.valueOf(frameLayout.getMeasuredHeight()));
        return frameLayout;
    }

    private void fixTextView(ViewGroup viewGroup) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof TextView) {
                TextView textView = (TextView) childAt;
                if (isSingleLine(textView)) {
                    textView.setSingleLine(false);
                    textView.setMaxLines(1);
                }
            } else if (childAt instanceof ViewGroup) {
                fixTextView((ViewGroup) childAt);
            }
        }
    }

    private boolean isSingleLine(TextView textView) {
        try {
            return ((Boolean) Reflect.on(textView).get("mSingleLine")).booleanValue();
        } catch (Exception e2) {
            return (textView.getInputType() & 131072) != 0;
        }
    }

    public RemoteViews makeRemoteViews(String str, Context context, RemoteViews remoteViews, boolean z, boolean z2) {
        int i;
        Bitmap bitmap;
        if (remoteViews == null) {
            return null;
        }
        PendIntentCompat pendIntentCompat = new PendIntentCompat(remoteViews);
        if (!z2 || pendIntentCompat.findPendIntents() <= 0) {
            i = R.layout.av;
        } else {
            i = R.layout.au;
        }
        VLog.v(TAG, "createviews id = " + i, new Object[0]);
        RemoteViews remoteViews2 = new RemoteViews(this.mNotificationCompat.getHostContext().getPackageName(), i);
        VLog.v(TAG, "remoteViews to view", new Object[0]);
        View view = toView(context, remoteViews, z, false);
        VLog.v(TAG, "start createBitmap", new Object[0]);
        Bitmap createBitmap = createBitmap(view);
        if (createBitmap == null) {
            VLog.e(TAG, "bmp is null,contentView=" + remoteViews, new Object[0]);
        } else {
            VLog.v(TAG, "bmp w=" + createBitmap.getWidth() + ",h=" + createBitmap.getHeight(), new Object[0]);
        }
        synchronized (this.mImages) {
            bitmap = this.mImages.get(str);
        }
        if (bitmap != null && !bitmap.isRecycled()) {
            VLog.v(TAG, "recycle " + str, new Object[0]);
            bitmap.recycle();
        }
        remoteViews2.setImageViewBitmap(R.id.ft, createBitmap);
        VLog.v(TAG, "createview " + str, new Object[0]);
        synchronized (this.mImages) {
            this.mImages.put(str, createBitmap);
        }
        if (z2 && i == R.layout.au) {
            VLog.v(TAG, "start setPendIntent", new Object[0]);
            try {
                pendIntentCompat.setPendIntent(remoteViews2, toView(this.mNotificationCompat.getHostContext(), remoteViews2, z, false), view);
            } catch (Exception e2) {
                VLog.e(TAG, "setPendIntent error", e2);
            }
        }
        return remoteViews2;
    }

    private void init(Context context) {
        if (!this.init) {
            this.init = true;
            if (this.notification_panel_width == 0) {
                Context context2 = null;
                try {
                    context2 = context.createPackageContext("com.android.systemui", 2);
                } catch (PackageManager.NameNotFoundException e2) {
                }
                if (Build.VERSION.SDK_INT <= 19) {
                    this.notification_side_padding = 0;
                } else {
                    this.notification_side_padding = getDimem(context, context2, "notification_side_padding", R.dimen.fe);
                }
                this.notification_panel_width = getDimem(context, context2, "notification_panel_width", R.dimen.fb);
                if (this.notification_panel_width <= 0) {
                    this.notification_panel_width = context.getResources().getDisplayMetrics().widthPixels;
                }
                this.notification_min_height = getDimem(context, context2, "notification_min_height", R.dimen.f_);
                this.notification_max_height = getDimem(context, context2, "notification_max_height", R.dimen.f7);
                this.notification_mid_height = getDimem(context, context2, "notification_mid_height", R.dimen.f9);
                this.notification_padding = getDimem(context, context2, "notification_padding", R.dimen.fa);
            }
        }
    }

    private int getDimem(Context context, Context context2, String str, int i) {
        int identifier;
        if (!(context2 == null || (identifier = context2.getResources().getIdentifier(str, "dimen", "com.android.systemui")) == 0)) {
            try {
                return Math.round(context2.getResources().getDimension(identifier));
            } catch (Exception e2) {
            }
        }
        if (i == 0) {
            return 0;
        }
        return Math.round(context.getResources().getDimension(i));
    }
}
