package com.lody.virtual.server.am;

import android.app.IServiceConnection;
import android.app.Notification;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ServiceRecord extends Binder {
    public long activeSince;
    public final List<IntentBindRecord> bindings = new ArrayList();
    public int foregroundId;
    public Notification foregroundNoti;
    public long lastActivityTime;
    public ProcessRecord process;
    public ServiceInfo serviceInfo;
    public int startId;

    public boolean containConnection(IServiceConnection iServiceConnection) {
        for (IntentBindRecord containConnection : this.bindings) {
            if (containConnection.containConnection(iServiceConnection)) {
                return true;
            }
        }
        return false;
    }

    public int getClientCount() {
        return this.bindings.size();
    }

    /* access modifiers changed from: package-private */
    public int getConnectionCount() {
        int i;
        synchronized (this.bindings) {
            i = 0;
            for (IntentBindRecord intentBindRecord : this.bindings) {
                i = intentBindRecord.connections.size() + i;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public IntentBindRecord peekBinding(Intent intent) {
        synchronized (this.bindings) {
            for (IntentBindRecord next : this.bindings) {
                if (next.intent.filterEquals(intent)) {
                    return next;
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void addToBoundIntent(Intent intent, IServiceConnection iServiceConnection) {
        IntentBindRecord peekBinding = peekBinding(intent);
        if (peekBinding == null) {
            peekBinding = new IntentBindRecord();
            peekBinding.intent = intent;
            synchronized (this.bindings) {
                this.bindings.add(peekBinding);
            }
        }
        peekBinding.addConnection(iServiceConnection);
    }

    public static class IntentBindRecord {
        public IBinder binder;
        public final List<IServiceConnection> connections = Collections.synchronizedList(new ArrayList());
        public boolean doRebind = false;
        Intent intent;

        public boolean containConnection(IServiceConnection iServiceConnection) {
            for (IServiceConnection asBinder : this.connections) {
                if (asBinder.asBinder() == iServiceConnection.asBinder()) {
                    return true;
                }
            }
            return false;
        }

        public void addConnection(IServiceConnection iServiceConnection) {
            if (!containConnection(iServiceConnection)) {
                this.connections.add(iServiceConnection);
                try {
                    iServiceConnection.asBinder().linkToDeath(new DeathRecipient(this, iServiceConnection), 0);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
        }

        public void removeConnection(IServiceConnection iServiceConnection) {
            synchronized (this.connections) {
                Iterator<IServiceConnection> it = this.connections.iterator();
                while (it.hasNext()) {
                    if (it.next().asBinder() == iServiceConnection.asBinder()) {
                        it.remove();
                    }
                }
            }
        }
    }

    private static class DeathRecipient implements IBinder.DeathRecipient {
        private final IntentBindRecord bindRecord;
        private final IServiceConnection connection;

        private DeathRecipient(IntentBindRecord intentBindRecord, IServiceConnection iServiceConnection) {
            this.bindRecord = intentBindRecord;
            this.connection = iServiceConnection;
        }

        public void binderDied() {
            this.bindRecord.removeConnection(this.connection);
            this.connection.asBinder().unlinkToDeath(this, 0);
        }
    }
}
