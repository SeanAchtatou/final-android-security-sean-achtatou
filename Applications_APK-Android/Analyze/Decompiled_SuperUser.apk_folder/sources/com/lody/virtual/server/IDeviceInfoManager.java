package com.lody.virtual.server;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.VDeviceInfo;

public interface IDeviceInfoManager extends IInterface {
    VDeviceInfo getDeviceInfo(int i);

    public static abstract class Stub extends Binder implements IDeviceInfoManager {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IDeviceInfoManager";
        static final int TRANSACTION_getDeviceInfo = 1;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IDeviceInfoManager asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IDeviceInfoManager)) {
                return new Proxy(iBinder);
            }
            return (IDeviceInfoManager) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            switch (i) {
                case 1:
                    parcel.enforceInterface(DESCRIPTOR);
                    VDeviceInfo deviceInfo = getDeviceInfo(parcel.readInt());
                    parcel2.writeNoException();
                    if (deviceInfo != null) {
                        parcel2.writeInt(1);
                        deviceInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 1598968902:
                    parcel2.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        private static class Proxy implements IDeviceInfoManager {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public VDeviceInfo getDeviceInfo(int i) {
                VDeviceInfo vDeviceInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vDeviceInfo = VDeviceInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        vDeviceInfo = null;
                    }
                    return vDeviceInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
