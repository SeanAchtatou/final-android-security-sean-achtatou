package com.lody.virtual.server.notification;

import android.app.Notification;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import com.lody.virtual.client.core.VirtualCore;

class NotificationCompatCompatV14 extends NotificationCompat {
    private final RemoteViewsFixer mRemoteViewsFixer = new RemoteViewsFixer(this);

    NotificationCompatCompatV14() {
    }

    private RemoteViewsFixer getRemoteViewsFixer() {
        return this.mRemoteViewsFixer;
    }

    public boolean dealNotification(int i, Notification notification, String str) {
        Context appContext = getAppContext(str);
        if (appContext == null) {
            return false;
        }
        if (VirtualCore.get().isOutsideInstalled(str)) {
            getNotificationFixer().fixIconImage(appContext.getResources(), notification.contentView, false, notification);
            if (Build.VERSION.SDK_INT >= 16) {
                getNotificationFixer().fixIconImage(appContext.getResources(), notification.bigContentView, false, notification);
            }
            notification.icon = getHostContext().getApplicationInfo().icon;
            return true;
        }
        if (notification.tickerView != null) {
            if (isSystemLayout(notification.tickerView)) {
                getNotificationFixer().fixRemoteViewActions(appContext, false, notification.tickerView);
            } else {
                notification.tickerView = getRemoteViewsFixer().makeRemoteViews(i + ":tickerView", appContext, notification.tickerView, false, false);
            }
        }
        if (notification.contentView != null) {
            if (isSystemLayout(notification.contentView)) {
                getNotificationFixer().fixIconImage(appContext.getResources(), notification.contentView, getNotificationFixer().fixRemoteViewActions(appContext, false, notification.contentView), notification);
            } else {
                notification.contentView = getRemoteViewsFixer().makeRemoteViews(i + ":contentView", appContext, notification.contentView, false, true);
            }
        }
        if (Build.VERSION.SDK_INT >= 16 && notification.bigContentView != null) {
            if (isSystemLayout(notification.bigContentView)) {
                getNotificationFixer().fixRemoteViewActions(appContext, false, notification.bigContentView);
            } else {
                notification.bigContentView = getRemoteViewsFixer().makeRemoteViews(i + ":bigContentView", appContext, notification.bigContentView, true, true);
            }
        }
        if (Build.VERSION.SDK_INT >= 21 && notification.headsUpContentView != null) {
            if (isSystemLayout(notification.headsUpContentView)) {
                getNotificationFixer().fixIconImage(appContext.getResources(), notification.contentView, getNotificationFixer().fixRemoteViewActions(appContext, false, notification.headsUpContentView), notification);
            } else {
                notification.headsUpContentView = getRemoteViewsFixer().makeRemoteViews(i + ":headsUpContentView", appContext, notification.headsUpContentView, false, false);
            }
        }
        notification.icon = getHostContext().getApplicationInfo().icon;
        return true;
    }

    /* access modifiers changed from: package-private */
    public Context getAppContext(String str) {
        try {
            return getHostContext().createPackageContext(str, 3);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
