package com.lody.virtual.server.accounts;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.content.SyncAdapterType;
import android.content.SyncRequest;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.Xml;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.server.accounts.VSyncRecord;
import com.lody.virtual.server.pm.VAppManagerService;
import com.lody.virtual.server.pm.VPackageManagerService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mirror.android.content.SyncAdapterTypeN;
import mirror.com.android.internal.R_Hide;

public class VContentService {
    private final Map<String, SyncAdapterInfo> mAppSyncAdapterInfos = new HashMap();
    private Context mContext;
    private final SparseArray<Map<VSyncRecord.SyncRecordKey, VSyncRecord>> mRecords = new SparseArray<>();

    private class SyncAdapterInfo {
        SyncAdapterType adapterType;
        ServiceInfo serviceInfo;

        SyncAdapterInfo(SyncAdapterType syncAdapterType, ServiceInfo serviceInfo2) {
            this.adapterType = syncAdapterType;
            this.serviceInfo = serviceInfo2;
        }
    }

    public void refreshServiceCache(String str) {
        Intent intent = new Intent("android.content.SyncAdapter");
        if (str != null) {
            intent.setPackage(str);
        }
        generateServicesMap(VPackageManagerService.get().queryIntentServices(intent, null, FileUtils.FileMode.MODE_IWUSR, 0), this.mAppSyncAdapterInfos, new RegisteredServicesParser());
    }

    public void syncAsUser(SyncRequest syncRequest, int i) {
        HashMap hashMap;
        Account account = mirror.android.content.SyncRequest.mAccountToSync.get(syncRequest);
        String str = mirror.android.content.SyncRequest.mAuthority.get(syncRequest);
        Bundle bundle = mirror.android.content.SyncRequest.mExtras.get(syncRequest);
        boolean z = mirror.android.content.SyncRequest.mIsPeriodic.get(syncRequest);
        long j = mirror.android.content.SyncRequest.mSyncRunTimeSecs.get(syncRequest);
        if (isAccountExist(i, account, str)) {
            VSyncRecord.SyncRecordKey syncRecordKey = new VSyncRecord.SyncRecordKey(account, str);
            VSyncRecord.SyncExtras syncExtras = new VSyncRecord.SyncExtras(bundle);
            int isSyncableAsUser = getIsSyncableAsUser(account, str, i);
            synchronized (this.mRecords) {
                Map map = this.mRecords.get(i);
                if (map == null) {
                    HashMap hashMap2 = new HashMap();
                    this.mRecords.put(i, hashMap2);
                    hashMap = hashMap2;
                } else {
                    hashMap = map;
                }
                VSyncRecord vSyncRecord = (VSyncRecord) hashMap.get(syncRecordKey);
                if (vSyncRecord == null) {
                    vSyncRecord = new VSyncRecord(i, account, str);
                    hashMap.put(syncRecordKey, vSyncRecord);
                }
                if (isSyncableAsUser < 0) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putBoolean("initialize", true);
                    vSyncRecord.extras.add(new VSyncRecord.SyncExtras(bundle2));
                }
                if (z) {
                    vSyncRecord.configs.put(syncExtras, new VSyncRecord.PeriodicSyncConfig(j));
                } else {
                    vSyncRecord.extras.add(syncExtras);
                }
            }
        }
    }

    private boolean isAccountExist(int i, Account account, String str) {
        boolean z;
        synchronized (this.mAppSyncAdapterInfos) {
            SyncAdapterInfo syncAdapterInfo = this.mAppSyncAdapterInfos.get(account.type + "/" + str);
            z = syncAdapterInfo != null && VAppManagerService.get().isAppInstalled(syncAdapterInfo.serviceInfo.packageName);
        }
        return z;
    }

    public int getIsSyncableAsUser(Account account, String str, int i) {
        VSyncRecord.SyncRecordKey syncRecordKey = new VSyncRecord.SyncRecordKey(account, str);
        synchronized (this.mRecords) {
            Map map = this.mRecords.get(i);
            if (map == null) {
                return -1;
            }
            VSyncRecord vSyncRecord = (VSyncRecord) map.get(syncRecordKey);
            if (vSyncRecord == null) {
                return -1;
            }
            int i2 = vSyncRecord.syncable;
            return i2;
        }
    }

    private void generateServicesMap(List<ResolveInfo> list, Map<String, SyncAdapterInfo> map, RegisteredServicesParser registeredServicesParser) {
        int next;
        SyncAdapterType parseSyncAdapterType;
        for (ResolveInfo next2 : list) {
            XmlResourceParser parser = registeredServicesParser.getParser(this.mContext, next2.serviceInfo, "android.content.SyncAdapter");
            if (parser != null) {
                try {
                    AttributeSet asAttributeSet = Xml.asAttributeSet(parser);
                    do {
                        next = parser.next();
                        if (next == 1) {
                            break;
                        }
                    } while (next != 2);
                    if ("sync-adapter".equals(parser.getName()) && (parseSyncAdapterType = parseSyncAdapterType(registeredServicesParser.getResources(this.mContext, next2.serviceInfo.applicationInfo), asAttributeSet)) != null) {
                        map.put(parseSyncAdapterType.accountType + "/" + parseSyncAdapterType.authority, new SyncAdapterInfo(parseSyncAdapterType, next2.serviceInfo));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    private SyncAdapterType parseSyncAdapterType(Resources resources, AttributeSet attributeSet) {
        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, R_Hide.styleable.SyncAdapter.get());
        try {
            String string = obtainAttributes.getString(R_Hide.styleable.SyncAdapter_contentAuthority.get());
            String string2 = obtainAttributes.getString(R_Hide.styleable.SyncAdapter_accountType.get());
            if (string == null || string2 == null) {
                obtainAttributes.recycle();
                return null;
            }
            boolean z = obtainAttributes.getBoolean(R_Hide.styleable.SyncAdapter_userVisible.get(), true);
            boolean z2 = obtainAttributes.getBoolean(R_Hide.styleable.SyncAdapter_supportsUploading.get(), true);
            boolean z3 = obtainAttributes.getBoolean(R_Hide.styleable.SyncAdapter_isAlwaysSyncable.get(), true);
            boolean z4 = obtainAttributes.getBoolean(R_Hide.styleable.SyncAdapter_allowParallelSyncs.get(), true);
            String string3 = obtainAttributes.getString(R_Hide.styleable.SyncAdapter_settingsActivity.get());
            if (SyncAdapterTypeN.ctor != null) {
                SyncAdapterType newInstance = SyncAdapterTypeN.ctor.newInstance(string, string2, Boolean.valueOf(z), Boolean.valueOf(z2), Boolean.valueOf(z3), Boolean.valueOf(z4), string3, null);
                obtainAttributes.recycle();
                return newInstance;
            }
            SyncAdapterType newInstance2 = mirror.android.content.SyncAdapterType.ctor.newInstance(string, string2, Boolean.valueOf(z), Boolean.valueOf(z2), Boolean.valueOf(z3), Boolean.valueOf(z4), string3);
            obtainAttributes.recycle();
            return newInstance2;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }
}
