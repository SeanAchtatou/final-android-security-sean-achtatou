package com.lody.virtual.server.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.widget.RemoteViews;
import com.lody.virtual.helper.utils.Reflect;
import mirror.com.android.internal.R_Hide;

class NotificationFixer {
    private static final String TAG = NotificationCompat.TAG;
    private NotificationCompat mNotificationCompat;

    NotificationFixer(NotificationCompat notificationCompat) {
        this.mNotificationCompat = notificationCompat;
    }

    private static void fixNotificationIcon(Context context, Notification notification, Notification.Builder builder) {
        Bitmap drawableToBitMap;
        Bitmap drawableToBitMap2;
        if (Build.VERSION.SDK_INT < 23) {
            builder.setSmallIcon(notification.icon);
            builder.setLargeIcon(notification.largeIcon);
            return;
        }
        Icon smallIcon = notification.getSmallIcon();
        if (!(smallIcon == null || (drawableToBitMap2 = drawableToBitMap(smallIcon.loadDrawable(context))) == null)) {
            builder.setSmallIcon(Icon.createWithBitmap(drawableToBitMap2));
        }
        Icon largeIcon = notification.getLargeIcon();
        if (largeIcon != null && (drawableToBitMap = drawableToBitMap(largeIcon.loadDrawable(context))) != null) {
            builder.setLargeIcon(Icon.createWithBitmap(drawableToBitMap));
        }
    }

    private static Bitmap drawableToBitMap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    /* access modifiers changed from: package-private */
    public void fixIcon(Icon icon, Context context, boolean z) {
        if (icon == null || mirror.android.graphics.drawable.Icon.mType.get(icon).intValue() != 2) {
            return;
        }
        if (z) {
            mirror.android.graphics.drawable.Icon.mObj1.set(icon, context.getResources());
            mirror.android.graphics.drawable.Icon.mString1.set(icon, context.getPackageName());
            return;
        }
        mirror.android.graphics.drawable.Icon.mObj1.set(icon, drawableToBitMap(icon.loadDrawable(context)));
        mirror.android.graphics.drawable.Icon.mString1.set(icon, null);
        mirror.android.graphics.drawable.Icon.mType.set(icon, 1);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(21)
    public void fixNotificationRemoteViews(Context context, Notification notification) {
        Notification.Builder builder;
        try {
            builder = (Notification.Builder) Reflect.on((Class<?>) Notification.Builder.class).create(context, notification).get();
        } catch (Exception e2) {
            builder = null;
        }
        if (builder != null) {
            Notification build = builder.build();
            if (notification.tickerView == null) {
                notification.tickerView = build.tickerView;
            }
            if (notification.contentView == null) {
                notification.contentView = build.contentView;
            }
            if (notification.bigContentView == null) {
                notification.bigContentView = build.bigContentView;
            }
            if (notification.headsUpContentView == null) {
                notification.headsUpContentView = build.headsUpContentView;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0103  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean fixRemoteViewActions(android.content.Context r15, boolean r16, android.widget.RemoteViews r17) {
        /*
            r14 = this;
            r5 = 0
            if (r17 == 0) goto L_0x01af
            mirror.RefStaticInt r1 = mirror.com.android.internal.R_Hide.id.icon
            int r7 = r1.get()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            com.lody.virtual.helper.utils.Reflect r1 = com.lody.virtual.helper.utils.Reflect.on(r17)
            java.lang.String r2 = "mActions"
            java.lang.Object r1 = r1.get(r2)
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            if (r1 == 0) goto L_0x019a
            int r2 = r1.size()
            int r2 = r2 + -1
            r6 = r2
        L_0x0023:
            if (r6 < 0) goto L_0x017e
            java.lang.Object r9 = r1.get(r6)
            if (r9 != 0) goto L_0x0031
            r3 = r5
        L_0x002c:
            int r2 = r6 + -1
            r6 = r2
            r5 = r3
            goto L_0x0023
        L_0x0031:
            java.lang.Class r2 = r9.getClass()
            java.lang.String r2 = r2.getSimpleName()
            java.lang.String r3 = "TextViewDrawableAction"
            boolean r2 = r2.endsWith(r3)
            if (r2 == 0) goto L_0x0046
            r1.remove(r9)
            r3 = r5
            goto L_0x002c
        L_0x0046:
            boolean r2 = com.lody.virtual.server.notification.ReflectionActionCompat.isInstance(r9)
            if (r2 != 0) goto L_0x005c
            java.lang.Class r2 = r9.getClass()
            java.lang.String r2 = r2.getSimpleName()
            java.lang.String r3 = "ReflectionAction"
            boolean r2 = r2.endsWith(r3)
            if (r2 == 0) goto L_0x01b6
        L_0x005c:
            com.lody.virtual.helper.utils.Reflect r2 = com.lody.virtual.helper.utils.Reflect.on(r9)
            java.lang.String r3 = "viewId"
            java.lang.Object r2 = r2.get(r3)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r10 = r2.intValue()
            com.lody.virtual.helper.utils.Reflect r2 = com.lody.virtual.helper.utils.Reflect.on(r9)
            java.lang.String r3 = "methodName"
            java.lang.Object r2 = r2.get(r3)
            java.lang.String r2 = (java.lang.String) r2
            com.lody.virtual.helper.utils.Reflect r3 = com.lody.virtual.helper.utils.Reflect.on(r9)
            java.lang.String r4 = "type"
            java.lang.Object r3 = r3.get(r4)
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r11 = r3.intValue()
            com.lody.virtual.helper.utils.Reflect r3 = com.lody.virtual.helper.utils.Reflect.on(r9)
            java.lang.String r4 = "value"
            java.lang.Object r4 = r3.get(r4)
            if (r5 != 0) goto L_0x01b3
            if (r10 != r7) goto L_0x0101
            r5 = 1
        L_0x0097:
            if (r5 == 0) goto L_0x01b3
            r3 = 4
            if (r11 != r3) goto L_0x01b0
            r3 = r4
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r3 = r3.intValue()
            if (r3 != 0) goto L_0x01b0
            r5 = 0
            r3 = r5
        L_0x00a7:
            if (r3 == 0) goto L_0x00d8
            java.lang.String r5 = com.lody.virtual.server.notification.NotificationFixer.TAG
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "find icon "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r2)
            java.lang.String r13 = " type="
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r11)
            java.lang.String r13 = ", value="
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r4)
            java.lang.String r12 = r12.toString()
            r13 = 0
            java.lang.Object[] r13 = new java.lang.Object[r13]
            com.lody.virtual.helper.utils.VLog.v(r5, r12, r13)
        L_0x00d8:
            java.lang.String r5 = "setImageResource"
            boolean r5 = r2.equals(r5)
            if (r5 == 0) goto L_0x0103
            com.lody.virtual.server.notification.NotificationFixer$BitmapReflectionAction r2 = new com.lody.virtual.server.notification.NotificationFixer$BitmapReflectionAction
            java.lang.String r5 = "setImageBitmap"
            android.content.res.Resources r11 = r15.getResources()
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            android.graphics.drawable.Drawable r4 = r11.getDrawable(r4)
            android.graphics.Bitmap r4 = drawableToBitMap(r4)
            r2.<init>(r10, r5, r4)
            r8.add(r2)
            r1.remove(r9)
            goto L_0x002c
        L_0x0101:
            r5 = 0
            goto L_0x0097
        L_0x0103:
            java.lang.String r5 = "setText"
            boolean r5 = r2.equals(r5)
            if (r5 == 0) goto L_0x0136
            r5 = 4
            if (r11 != r5) goto L_0x0136
            com.lody.virtual.helper.utils.Reflect r2 = com.lody.virtual.helper.utils.Reflect.on(r9)
            java.lang.String r5 = "type"
            r10 = 9
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            r2.set(r5, r10)
            com.lody.virtual.helper.utils.Reflect r2 = com.lody.virtual.helper.utils.Reflect.on(r9)
            java.lang.String r5 = "value"
            android.content.res.Resources r9 = r15.getResources()
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            java.lang.String r4 = r9.getString(r4)
            r2.set(r5, r4)
            goto L_0x002c
        L_0x0136:
            java.lang.String r5 = "setLabelFor"
            boolean r5 = r2.equals(r5)
            if (r5 == 0) goto L_0x0143
            r1.remove(r9)
            goto L_0x002c
        L_0x0143:
            java.lang.String r5 = "setBackgroundResource"
            boolean r5 = r2.equals(r5)
            if (r5 == 0) goto L_0x0150
            r1.remove(r9)
            goto L_0x002c
        L_0x0150:
            java.lang.String r5 = "setImageURI"
            boolean r2 = r2.equals(r5)
            if (r2 == 0) goto L_0x016b
            android.net.Uri r4 = (android.net.Uri) r4
            java.lang.String r2 = r4.getScheme()
            java.lang.String r4 = "http"
            boolean r2 = r2.startsWith(r4)
            if (r2 != 0) goto L_0x002c
            r1.remove(r9)
            goto L_0x002c
        L_0x016b:
            int r2 = android.os.Build.VERSION.SDK_INT
            r5 = 23
            if (r2 < r5) goto L_0x002c
            boolean r2 = r4 instanceof android.graphics.drawable.Icon
            if (r2 == 0) goto L_0x002c
            android.graphics.drawable.Icon r4 = (android.graphics.drawable.Icon) r4
            r0 = r16
            r14.fixIcon(r4, r15, r0)
            goto L_0x002c
        L_0x017e:
            java.util.Iterator r2 = r8.iterator()
        L_0x0182:
            boolean r1 = r2.hasNext()
            if (r1 == 0) goto L_0x019a
            java.lang.Object r1 = r2.next()
            com.lody.virtual.server.notification.NotificationFixer$BitmapReflectionAction r1 = (com.lody.virtual.server.notification.NotificationFixer.BitmapReflectionAction) r1
            int r3 = r1.viewId
            java.lang.String r4 = r1.methodName
            android.graphics.Bitmap r1 = r1.bitmap
            r0 = r17
            r0.setBitmap(r3, r4, r1)
            goto L_0x0182
        L_0x019a:
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 21
            if (r1 >= r2) goto L_0x01af
            mirror.RefObject<java.lang.String> r1 = mirror.android.widget.RemoteViews.mPackage
            com.lody.virtual.client.core.VirtualCore r2 = com.lody.virtual.client.core.VirtualCore.get()
            java.lang.String r2 = r2.getHostPkg()
            r0 = r17
            r1.set(r0, r2)
        L_0x01af:
            return r5
        L_0x01b0:
            r3 = r5
            goto L_0x00a7
        L_0x01b3:
            r3 = r5
            goto L_0x00d8
        L_0x01b6:
            r3 = r5
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.notification.NotificationFixer.fixRemoteViewActions(android.content.Context, boolean, android.widget.RemoteViews):boolean");
    }

    /* access modifiers changed from: package-private */
    public void fixIconImage(Resources resources, RemoteViews remoteViews, boolean z, Notification notification) {
        if (remoteViews != null && this.mNotificationCompat.isSystemLayout(remoteViews)) {
            try {
                int i = R_Hide.id.icon.get();
                if (!z && notification.largeIcon == null) {
                    Drawable drawable = resources.getDrawable(notification.icon);
                    drawable.setLevel(notification.iconLevel);
                    remoteViews.setImageViewBitmap(i, drawableToBitMap(drawable));
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    remoteViews.setInt(i, "setBackgroundColor", 0);
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    remoteViews.setViewPadding(i, 0, 0, 0, 0);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static class BitmapReflectionAction {
        Bitmap bitmap;
        String methodName;
        int viewId;

        BitmapReflectionAction(int i, String str, Bitmap bitmap2) {
            this.viewId = i;
            this.methodName = str;
            this.bitmap = bitmap2;
        }
    }
}
