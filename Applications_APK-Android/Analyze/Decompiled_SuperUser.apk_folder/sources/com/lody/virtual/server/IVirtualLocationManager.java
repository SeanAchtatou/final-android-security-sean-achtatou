package com.lody.virtual.server;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.vloc.VCell;
import com.lody.virtual.remote.vloc.VLocation;
import java.util.List;

public interface IVirtualLocationManager extends IInterface {
    List<VCell> getAllCell(int i, String str);

    VCell getCell(int i, String str);

    VLocation getGlobalLocation();

    VLocation getLocation(int i, String str);

    int getMode(int i, String str);

    List<VCell> getNeighboringCell(int i, String str);

    void setAllCell(int i, String str, List<VCell> list);

    void setCell(int i, String str, VCell vCell);

    void setGlobalAllCell(List<VCell> list);

    void setGlobalCell(VCell vCell);

    void setGlobalLocation(VLocation vLocation);

    void setGlobalNeighboringCell(List<VCell> list);

    void setLocation(int i, String str, VLocation vLocation);

    void setMode(int i, String str, int i2);

    void setNeighboringCell(int i, String str, List<VCell> list);

    public static abstract class Stub extends Binder implements IVirtualLocationManager {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IVirtualLocationManager";
        static final int TRANSACTION_getAllCell = 10;
        static final int TRANSACTION_getCell = 9;
        static final int TRANSACTION_getGlobalLocation = 15;
        static final int TRANSACTION_getLocation = 13;
        static final int TRANSACTION_getMode = 1;
        static final int TRANSACTION_getNeighboringCell = 11;
        static final int TRANSACTION_setAllCell = 4;
        static final int TRANSACTION_setCell = 3;
        static final int TRANSACTION_setGlobalAllCell = 7;
        static final int TRANSACTION_setGlobalCell = 6;
        static final int TRANSACTION_setGlobalLocation = 14;
        static final int TRANSACTION_setGlobalNeighboringCell = 8;
        static final int TRANSACTION_setLocation = 12;
        static final int TRANSACTION_setMode = 2;
        static final int TRANSACTION_setNeighboringCell = 5;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IVirtualLocationManager asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IVirtualLocationManager)) {
                return new Proxy(iBinder);
            }
            return (IVirtualLocationManager) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.lody.virtual.remote.vloc.VLocation} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: com.lody.virtual.remote.vloc.VLocation} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v38, resolved type: com.lody.virtual.remote.vloc.VCell} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v49, resolved type: com.lody.virtual.remote.vloc.VCell} */
        /* JADX WARN: Type inference failed for: r0v0 */
        /* JADX WARN: Type inference failed for: r0v64 */
        /* JADX WARN: Type inference failed for: r0v65 */
        /* JADX WARN: Type inference failed for: r0v66 */
        /* JADX WARN: Type inference failed for: r0v67 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r6, android.os.Parcel r7, android.os.Parcel r8, int r9) {
            /*
                r5 = this;
                r3 = 0
                r0 = 0
                r1 = 1
                switch(r6) {
                    case 1: goto L_0x0012;
                    case 2: goto L_0x002b;
                    case 3: goto L_0x0044;
                    case 4: goto L_0x0067;
                    case 5: goto L_0x0082;
                    case 6: goto L_0x009e;
                    case 7: goto L_0x00ba;
                    case 8: goto L_0x00ce;
                    case 9: goto L_0x00e2;
                    case 10: goto L_0x0105;
                    case 11: goto L_0x011f;
                    case 12: goto L_0x0139;
                    case 13: goto L_0x015d;
                    case 14: goto L_0x0180;
                    case 15: goto L_0x019c;
                    case 1598968902: goto L_0x000b;
                    default: goto L_0x0006;
                }
            L_0x0006:
                boolean r0 = super.onTransact(r6, r7, r8, r9)
            L_0x000a:
                return r0
            L_0x000b:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r8.writeString(r0)
                r0 = r1
                goto L_0x000a
            L_0x0012:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                int r0 = r5.getMode(r0, r2)
                r8.writeNoException()
                r8.writeInt(r0)
                r0 = r1
                goto L_0x000a
            L_0x002b:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                int r3 = r7.readInt()
                r5.setMode(r0, r2, r3)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x0044:
                java.lang.String r2 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r2)
                int r2 = r7.readInt()
                java.lang.String r3 = r7.readString()
                int r4 = r7.readInt()
                if (r4 == 0) goto L_0x005f
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VCell> r0 = com.lody.virtual.remote.vloc.VCell.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r7)
                com.lody.virtual.remote.vloc.VCell r0 = (com.lody.virtual.remote.vloc.VCell) r0
            L_0x005f:
                r5.setCell(r2, r3, r0)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x0067:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VCell> r3 = com.lody.virtual.remote.vloc.VCell.CREATOR
                java.util.ArrayList r3 = r7.createTypedArrayList(r3)
                r5.setAllCell(r0, r2, r3)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x0082:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VCell> r3 = com.lody.virtual.remote.vloc.VCell.CREATOR
                java.util.ArrayList r3 = r7.createTypedArrayList(r3)
                r5.setNeighboringCell(r0, r2, r3)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x009e:
                java.lang.String r2 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r2)
                int r2 = r7.readInt()
                if (r2 == 0) goto L_0x00b1
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VCell> r0 = com.lody.virtual.remote.vloc.VCell.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r7)
                com.lody.virtual.remote.vloc.VCell r0 = (com.lody.virtual.remote.vloc.VCell) r0
            L_0x00b1:
                r5.setGlobalCell(r0)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x00ba:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VCell> r0 = com.lody.virtual.remote.vloc.VCell.CREATOR
                java.util.ArrayList r0 = r7.createTypedArrayList(r0)
                r5.setGlobalAllCell(r0)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x00ce:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VCell> r0 = com.lody.virtual.remote.vloc.VCell.CREATOR
                java.util.ArrayList r0 = r7.createTypedArrayList(r0)
                r5.setGlobalNeighboringCell(r0)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x00e2:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                com.lody.virtual.remote.vloc.VCell r0 = r5.getCell(r0, r2)
                r8.writeNoException()
                if (r0 == 0) goto L_0x0101
                r8.writeInt(r1)
                r0.writeToParcel(r8, r1)
            L_0x00fe:
                r0 = r1
                goto L_0x000a
            L_0x0101:
                r8.writeInt(r3)
                goto L_0x00fe
            L_0x0105:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                java.util.List r0 = r5.getAllCell(r0, r2)
                r8.writeNoException()
                r8.writeTypedList(r0)
                r0 = r1
                goto L_0x000a
            L_0x011f:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                java.util.List r0 = r5.getNeighboringCell(r0, r2)
                r8.writeNoException()
                r8.writeTypedList(r0)
                r0 = r1
                goto L_0x000a
            L_0x0139:
                java.lang.String r2 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r2)
                int r2 = r7.readInt()
                java.lang.String r3 = r7.readString()
                int r4 = r7.readInt()
                if (r4 == 0) goto L_0x0154
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VLocation> r0 = com.lody.virtual.remote.vloc.VLocation.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r7)
                com.lody.virtual.remote.vloc.VLocation r0 = (com.lody.virtual.remote.vloc.VLocation) r0
            L_0x0154:
                r5.setLocation(r2, r3, r0)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x015d:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                int r0 = r7.readInt()
                java.lang.String r2 = r7.readString()
                com.lody.virtual.remote.vloc.VLocation r0 = r5.getLocation(r0, r2)
                r8.writeNoException()
                if (r0 == 0) goto L_0x017c
                r8.writeInt(r1)
                r0.writeToParcel(r8, r1)
            L_0x0179:
                r0 = r1
                goto L_0x000a
            L_0x017c:
                r8.writeInt(r3)
                goto L_0x0179
            L_0x0180:
                java.lang.String r2 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r2)
                int r2 = r7.readInt()
                if (r2 == 0) goto L_0x0193
                android.os.Parcelable$Creator<com.lody.virtual.remote.vloc.VLocation> r0 = com.lody.virtual.remote.vloc.VLocation.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r7)
                com.lody.virtual.remote.vloc.VLocation r0 = (com.lody.virtual.remote.vloc.VLocation) r0
            L_0x0193:
                r5.setGlobalLocation(r0)
                r8.writeNoException()
                r0 = r1
                goto L_0x000a
            L_0x019c:
                java.lang.String r0 = "com.lody.virtual.server.IVirtualLocationManager"
                r7.enforceInterface(r0)
                com.lody.virtual.remote.vloc.VLocation r0 = r5.getGlobalLocation()
                r8.writeNoException()
                if (r0 == 0) goto L_0x01b3
                r8.writeInt(r1)
                r0.writeToParcel(r8, r1)
            L_0x01b0:
                r0 = r1
                goto L_0x000a
            L_0x01b3:
                r8.writeInt(r3)
                goto L_0x01b0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.IVirtualLocationManager.Stub.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }

        private static class Proxy implements IVirtualLocationManager {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public int getMode(int i, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setMode(int i, String str, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeInt(i2);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setCell(int i, String str, VCell vCell) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (vCell != null) {
                        obtain.writeInt(1);
                        vCell.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setAllCell(int i, String str, List<VCell> list) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setNeighboringCell(int i, String str, List<VCell> list) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setGlobalCell(VCell vCell) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (vCell != null) {
                        obtain.writeInt(1);
                        vCell.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setGlobalAllCell(List<VCell> list) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setGlobalNeighboringCell(List<VCell> list) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeTypedList(list);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VCell getCell(int i, String str) {
                VCell vCell;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vCell = VCell.CREATOR.createFromParcel(obtain2);
                    } else {
                        vCell = null;
                    }
                    return vCell;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<VCell> getAllCell(int i, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(VCell.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<VCell> getNeighboringCell(int i, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(VCell.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setLocation(int i, String str, VLocation vLocation) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (vLocation != null) {
                        obtain.writeInt(1);
                        vLocation.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VLocation getLocation(int i, String str) {
                VLocation vLocation;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vLocation = VLocation.CREATOR.createFromParcel(obtain2);
                    } else {
                        vLocation = null;
                    }
                    return vLocation;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setGlobalLocation(VLocation vLocation) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (vLocation != null) {
                        obtain.writeInt(1);
                        vLocation.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VLocation getGlobalLocation() {
                VLocation vLocation;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vLocation = VLocation.CREATOR.createFromParcel(obtain2);
                    } else {
                        vLocation = null;
                    }
                    return vLocation;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
