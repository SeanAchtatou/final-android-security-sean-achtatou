package com.lody.virtual.server.am;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.SparseArray;
import java.util.HashMap;
import java.util.WeakHashMap;

public final class AttributeCache {
    private static AttributeCache sInstance = null;
    private final Configuration mConfiguration = new Configuration();
    private final Context mContext;
    private final WeakHashMap<String, Package> mPackages = new WeakHashMap<>();

    public AttributeCache(Context context) {
        this.mContext = context;
    }

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new AttributeCache(context);
        }
    }

    public static AttributeCache instance() {
        return sInstance;
    }

    public void removePackage(String str) {
        synchronized (this) {
            this.mPackages.remove(str);
        }
    }

    public void updateConfiguration(Configuration configuration) {
        synchronized (this) {
            if ((this.mConfiguration.updateFrom(configuration) & -1073741985) != 0) {
                this.mPackages.clear();
            }
        }
    }

    public Entry get(String str, int i, int[] iArr) {
        Package packageR;
        HashMap hashMap;
        Entry entry;
        synchronized (this) {
            Package packageR2 = this.mPackages.get(str);
            if (packageR2 != null) {
                HashMap hashMap2 = (HashMap) packageR2.mMap.get(i);
                if (hashMap2 != null && (entry = (Entry) hashMap2.get(iArr)) != null) {
                    return entry;
                }
                HashMap hashMap3 = hashMap2;
                packageR = packageR2;
                hashMap = hashMap3;
            } else {
                try {
                    Context createPackageContext = this.mContext.createPackageContext(str, 3);
                    if (createPackageContext == null) {
                        return null;
                    }
                    Package packageR3 = new Package(createPackageContext);
                    this.mPackages.put(str, packageR3);
                    packageR = packageR3;
                    hashMap = null;
                } catch (PackageManager.NameNotFoundException e2) {
                    return null;
                }
            }
            if (hashMap == null) {
                hashMap = new HashMap();
                packageR.mMap.put(i, hashMap);
            }
            try {
                Entry entry2 = new Entry(packageR.context, packageR.context.obtainStyledAttributes(i, iArr));
                hashMap.put(iArr, entry2);
                return entry2;
            } catch (Resources.NotFoundException e3) {
                return null;
            }
        }
    }

    public static final class Package {
        public final Context context;
        /* access modifiers changed from: private */
        public final SparseArray<HashMap<int[], Entry>> mMap = new SparseArray<>();

        public Package(Context context2) {
            this.context = context2;
        }
    }

    public static final class Entry {
        public final TypedArray array;
        public final Context context;

        public Entry(Context context2, TypedArray typedArray) {
            this.context = context2;
            this.array = typedArray;
        }
    }
}
