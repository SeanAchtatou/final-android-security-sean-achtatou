package com.lody.virtual.server.vs;

import android.util.SparseArray;
import com.lody.virtual.server.IVirtualStorageService;
import com.lody.virtual.server.pm.VUserManagerService;
import java.util.HashMap;

public class VirtualStorageService extends IVirtualStorageService.Stub {
    private static final VirtualStorageService sService = new VirtualStorageService();
    private final SparseArray<HashMap<String, VSConfig>> mConfigs = new SparseArray<>();
    private final VSPersistenceLayer mLayer = new VSPersistenceLayer(this);

    public static VirtualStorageService get() {
        return sService;
    }

    private VirtualStorageService() {
        this.mLayer.read();
    }

    /* access modifiers changed from: package-private */
    public SparseArray<HashMap<String, VSConfig>> getConfigs() {
        return this.mConfigs;
    }

    public void setVirtualStorage(String str, int i, String str2) {
        checkUserId(i);
        synchronized (this.mConfigs) {
            getOrCreateVSConfigLocked(str, i).vsPath = str2;
            this.mLayer.save();
        }
    }

    private VSConfig getOrCreateVSConfigLocked(String str, int i) {
        HashMap hashMap;
        HashMap hashMap2 = this.mConfigs.get(i);
        if (hashMap2 == null) {
            HashMap hashMap3 = new HashMap();
            this.mConfigs.put(i, hashMap3);
            hashMap = hashMap3;
        } else {
            hashMap = hashMap2;
        }
        VSConfig vSConfig = (VSConfig) hashMap.get(str);
        if (vSConfig != null) {
            return vSConfig;
        }
        VSConfig vSConfig2 = new VSConfig();
        vSConfig2.enable = true;
        hashMap.put(str, vSConfig2);
        return vSConfig2;
    }

    public String getVirtualStorage(String str, int i) {
        String str2;
        checkUserId(i);
        synchronized (this.mConfigs) {
            str2 = getOrCreateVSConfigLocked(str, i).vsPath;
        }
        return str2;
    }

    public void setVirtualStorageState(String str, int i, boolean z) {
        checkUserId(i);
        synchronized (this.mConfigs) {
            getOrCreateVSConfigLocked(str, i).enable = z;
            this.mLayer.save();
        }
    }

    public boolean isVirtualStorageEnable(String str, int i) {
        boolean z;
        checkUserId(i);
        synchronized (this.mConfigs) {
            z = getOrCreateVSConfigLocked(str, i).enable;
        }
        return z;
    }

    private void checkUserId(int i) {
        if (!VUserManagerService.get().exists(i)) {
            throw new IllegalStateException("Invalid userId " + i);
        }
    }
}
