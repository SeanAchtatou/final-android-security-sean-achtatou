package com.lody.virtual.server.pm.installer;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.IPackageInstallObserver2;
import android.content.pm.IPackageInstallerSession;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.text.TextUtils;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.server.pm.installer.VPackageInstallerService;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@TargetApi(21)
public class PackageInstallerSession extends IPackageInstallerSession.Stub {
    public static final int INSTALL_FAILED_ABORTED = -115;
    public static final int INSTALL_FAILED_INTERNAL_ERROR = -110;
    public static final int INSTALL_FAILED_INVALID_APK = -2;
    public static final int INSTALL_SUCCEEDED = 1;
    private static final int MSG_COMMIT = 0;
    private static final String REMOVE_SPLIT_MARKER_EXTENSION = ".removed";
    private static final String TAG = "PackageInstaller";
    final String installerPackageName;
    final int installerUid;
    private final AtomicInteger mActiveCount = new AtomicInteger();
    private ArrayList<FileBridge> mBridges = new ArrayList<>();
    private final VPackageInstallerService.InternalCallback mCallback;
    private float mClientProgress = 0.0f;
    private final Context mContext;
    private boolean mDestroyed = false;
    private String mFinalMessage;
    private int mFinalStatus;
    private final Handler mHandler;
    private final Handler.Callback mHandlerCallback = new Handler.Callback() {
        public boolean handleMessage(Message message) {
            synchronized (PackageInstallerSession.this.mLock) {
                if (message.obj != null) {
                    IPackageInstallObserver2 unused = PackageInstallerSession.this.mRemoteObserver = (IPackageInstallObserver2) message.obj;
                }
                try {
                    PackageInstallerSession.this.commitLocked();
                } catch (PackageManagerException e2) {
                    String completeMessage = PackageInstallerSession.getCompleteMessage(e2);
                    VLog.e(PackageInstallerSession.TAG, "Commit of session " + PackageInstallerSession.this.sessionId + " failed: " + completeMessage, new Object[0]);
                    PackageInstallerSession.this.destroyInternal();
                    PackageInstallerSession.this.dispatchSessionFinished(e2.error, completeMessage, null);
                }
            }
            return true;
        }
    };
    private float mInternalProgress = 0.0f;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    private String mPackageName;
    private boolean mPermissionsAccepted;
    private boolean mPrepared = false;
    private float mProgress = 0.0f;
    /* access modifiers changed from: private */
    public IPackageInstallObserver2 mRemoteObserver;
    private float mReportedProgress = -1.0f;
    private File mResolvedBaseFile;
    private File mResolvedStageDir;
    private final List<File> mResolvedStagedFiles = new ArrayList();
    private boolean mSealed = false;
    final SessionParams params;
    final int sessionId;
    final File stageDir;
    final int userId;

    public PackageInstallerSession(VPackageInstallerService.InternalCallback internalCallback, Context context, Looper looper, String str, int i, int i2, int i3, SessionParams sessionParams, File file) {
        this.mCallback = internalCallback;
        this.mContext = context;
        this.mHandler = new Handler(looper, this.mHandlerCallback);
        this.installerPackageName = str;
        this.sessionId = i;
        this.userId = i2;
        this.installerUid = i3;
        this.mPackageName = sessionParams.appPackageName;
        this.params = sessionParams;
        this.stageDir = file;
    }

    public SessionInfo generateInfo() {
        SessionInfo sessionInfo = new SessionInfo();
        synchronized (this.mLock) {
            sessionInfo.sessionId = this.sessionId;
            sessionInfo.installerPackageName = this.installerPackageName;
            sessionInfo.resolvedBaseCodePath = this.mResolvedBaseFile != null ? this.mResolvedBaseFile.getAbsolutePath() : null;
            sessionInfo.progress = this.mProgress;
            sessionInfo.sealed = this.mSealed;
            sessionInfo.active = this.mActiveCount.get() > 0;
            sessionInfo.mode = this.params.mode;
            sessionInfo.sizeBytes = this.params.sizeBytes;
            sessionInfo.appPackageName = this.params.appPackageName;
            sessionInfo.appIcon = this.params.appIcon;
            sessionInfo.appLabel = this.params.appLabel;
        }
        return sessionInfo;
    }

    /* access modifiers changed from: private */
    public void commitLocked() {
        if (this.mDestroyed) {
            throw new PackageManagerException(-110, "Session destroyed");
        } else if (!this.mSealed) {
            throw new PackageManagerException(-110, "Session not sealed");
        } else {
            try {
                resolveStageDir();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            validateInstallLocked();
            this.mInternalProgress = 0.5f;
            computeProgressLocked(true);
            new IPackageInstallObserver2.Stub() {
                public void onUserActionRequired(Intent intent) {
                    throw new IllegalStateException();
                }

                public void onPackageInstalled(String str, int i, String str2, Bundle bundle) {
                    PackageInstallerSession.this.destroyInternal();
                    PackageInstallerSession.this.dispatchSessionFinished(i, str2, bundle);
                }
            };
        }
    }

    private void validateInstallLocked() {
        this.mResolvedBaseFile = null;
        this.mResolvedStagedFiles.clear();
        File[] listFiles = this.mResolvedStageDir.listFiles();
        if (listFiles == null || listFiles.length == 0) {
            throw new PackageManagerException(-2, "No packages staged");
        }
        for (File file : listFiles) {
            if (!file.isDirectory()) {
                File file2 = new File(this.mResolvedStageDir, "base.apk");
                if (!file.equals(file2)) {
                    file.renameTo(file2);
                }
                this.mResolvedBaseFile = file2;
                this.mResolvedStagedFiles.add(file2);
            }
        }
        if (this.mResolvedBaseFile == null) {
            throw new PackageManagerException(-2, "Full install must include a base package");
        }
    }

    public void setClientProgress(float f2) {
        synchronized (this.mLock) {
            boolean z = this.mClientProgress == 0.0f;
            this.mClientProgress = f2;
            computeProgressLocked(z);
        }
    }

    private static float constrain(float f2, float f3, float f4) {
        if (f2 < f3) {
            return f3;
        }
        return f2 > f4 ? f4 : f2;
    }

    private void computeProgressLocked(boolean z) {
        this.mProgress = constrain(this.mClientProgress * 0.8f, 0.0f, 0.8f) + constrain(this.mInternalProgress * 0.2f, 0.0f, 0.2f);
        if (z || ((double) Math.abs(this.mProgress - this.mReportedProgress)) >= 0.01d) {
            this.mReportedProgress = this.mProgress;
            this.mCallback.onSessionProgressChanged(this, this.mProgress);
        }
    }

    public void addClientProgress(float f2) {
        synchronized (this.mLock) {
            setClientProgress(this.mClientProgress + f2);
        }
    }

    public String[] getNames() {
        assertPreparedAndNotSealed("getNames");
        try {
            return resolveStageDir().list();
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private File resolveStageDir() {
        File file;
        synchronized (this.mLock) {
            if (this.mResolvedStageDir == null && this.stageDir != null) {
                this.mResolvedStageDir = this.stageDir;
                if (!this.stageDir.exists()) {
                    this.stageDir.mkdirs();
                }
            }
            file = this.mResolvedStageDir;
        }
        return file;
    }

    public ParcelFileDescriptor openWrite(String str, long j, long j2) {
        try {
            return openWriteInternal(str, j, j2);
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private void assertPreparedAndNotSealed(String str) {
        synchronized (this.mLock) {
            if (!this.mPrepared) {
                throw new IllegalStateException(str + " before prepared");
            } else if (this.mSealed) {
                throw new SecurityException(str + " not allowed after commit");
            }
        }
    }

    private ParcelFileDescriptor openWriteInternal(String str, long j, long j2) {
        FileBridge fileBridge;
        synchronized (this.mLock) {
            assertPreparedAndNotSealed("openWrite");
            fileBridge = new FileBridge();
            this.mBridges.add(fileBridge);
        }
        try {
            FileDescriptor open = Os.open(new File(resolveStageDir(), str).getAbsolutePath(), OsConstants.O_CREAT | OsConstants.O_WRONLY, 420);
            if (j2 > 0) {
                Os.posix_fallocate(open, 0, j2);
            }
            if (j > 0) {
                Os.lseek(open, j, OsConstants.SEEK_SET);
            }
            fileBridge.setTargetFile(open);
            fileBridge.start();
            return ParcelFileDescriptor.dup(fileBridge.getClientSocket());
        } catch (ErrnoException e2) {
            throw new IOException(e2);
        }
    }

    public ParcelFileDescriptor openRead(String str) {
        try {
            return openReadInternal(str);
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private ParcelFileDescriptor openReadInternal(String str) {
        assertPreparedAndNotSealed("openRead");
        try {
            if (FileUtils.isValidExtFilename(str)) {
                return ParcelFileDescriptor.dup(Os.open(new File(resolveStageDir(), str).getAbsolutePath(), OsConstants.O_RDONLY, 0));
            }
            throw new IllegalArgumentException("Invalid name: " + str);
        } catch (ErrnoException e2) {
            throw new IOException(e2);
        }
    }

    public void removeSplit(String str) {
        if (TextUtils.isEmpty(this.params.appPackageName)) {
            throw new IllegalStateException("Must specify package name to remove a split");
        }
        try {
            createRemoveSplitMarker(str);
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private void createRemoveSplitMarker(String str) {
        try {
            String str2 = str + REMOVE_SPLIT_MARKER_EXTENSION;
            if (!FileUtils.isValidExtFilename(str2)) {
                throw new IllegalArgumentException("Invalid marker: " + str2);
            }
            File file = new File(resolveStageDir(), str2);
            file.createNewFile();
            Os.chmod(file.getAbsolutePath(), 0);
        } catch (ErrnoException e2) {
            throw new IOException(e2);
        }
    }

    public void close() {
        if (this.mActiveCount.decrementAndGet() == 0) {
            this.mCallback.onSessionActiveChanged(this, false);
        }
    }

    public void commit(IntentSender intentSender) {
        boolean z;
        synchronized (this.mLock) {
            z = this.mSealed;
            if (!this.mSealed) {
                Iterator<FileBridge> it = this.mBridges.iterator();
                while (it.hasNext()) {
                    if (!it.next().isClosed()) {
                        throw new SecurityException("Files still open");
                    }
                }
                this.mSealed = true;
            }
            this.mClientProgress = 1.0f;
            computeProgressLocked(true);
        }
        if (!z) {
            this.mCallback.onSessionSealedBlocking(this);
        }
        this.mActiveCount.incrementAndGet();
        this.mHandler.obtainMessage(0, new VPackageInstallerService.PackageInstallObserverAdapter(this.mContext, intentSender, this.sessionId, this.userId).getBinder()).sendToTarget();
    }

    public void abandon() {
        destroyInternal();
        dispatchSessionFinished(-115, "Session was abandoned", null);
    }

    /* access modifiers changed from: private */
    public void destroyInternal() {
        synchronized (this.mLock) {
            this.mSealed = true;
            this.mDestroyed = true;
            Iterator<FileBridge> it = this.mBridges.iterator();
            while (it.hasNext()) {
                it.next().forceClose();
            }
        }
        if (this.stageDir != null) {
            FileUtils.deleteDir(this.stageDir.getAbsolutePath());
        }
    }

    /* access modifiers changed from: private */
    public void dispatchSessionFinished(int i, String str, Bundle bundle) {
        boolean z = true;
        this.mFinalStatus = i;
        this.mFinalMessage = str;
        if (this.mRemoteObserver != null) {
            try {
                this.mRemoteObserver.onPackageInstalled(this.mPackageName, i, str, bundle);
            } catch (RemoteException e2) {
            }
        }
        if (i != 1) {
            z = false;
        }
        this.mCallback.onSessionFinished(this, z);
    }

    /* access modifiers changed from: package-private */
    public void setPermissionsResult(boolean z) {
        if (!this.mSealed) {
            throw new SecurityException("Must be sealed to accept permissions");
        } else if (z) {
            synchronized (this.mLock) {
                this.mPermissionsAccepted = true;
            }
            this.mHandler.obtainMessage(0).sendToTarget();
        } else {
            destroyInternal();
            dispatchSessionFinished(-115, "User rejected permissions", null);
        }
    }

    public void open() {
        if (this.mActiveCount.getAndIncrement() == 0) {
            this.mCallback.onSessionActiveChanged(this, true);
        }
        synchronized (this.mLock) {
            if (!this.mPrepared) {
                if (this.stageDir == null) {
                    throw new IllegalArgumentException("Exactly one of stageDir or stageCid stage must be set");
                }
                this.mPrepared = true;
                this.mCallback.onSessionPrepared(this);
            }
        }
    }

    public static String getCompleteMessage(Throwable th) {
        StringBuilder sb = new StringBuilder();
        sb.append(th.getMessage());
        while (true) {
            th = th.getCause();
            if (th == null) {
                return sb.toString();
            }
            sb.append(": ").append(th.getMessage());
        }
    }

    private class PackageManagerException extends Exception {
        public final int error;

        PackageManagerException(int i, String str) {
            super(str);
            this.error = i;
        }
    }
}
