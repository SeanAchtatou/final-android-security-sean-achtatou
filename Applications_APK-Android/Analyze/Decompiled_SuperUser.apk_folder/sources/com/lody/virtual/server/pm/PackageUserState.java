package com.lody.virtual.server.pm;

import android.os.Parcel;
import android.os.Parcelable;

public class PackageUserState implements Parcelable {
    public static final Parcelable.Creator<PackageUserState> CREATOR = new Parcelable.Creator<PackageUserState>() {
        public PackageUserState createFromParcel(Parcel parcel) {
            return new PackageUserState(parcel);
        }

        public PackageUserState[] newArray(int i) {
            return new PackageUserState[i];
        }
    };
    public boolean hidden;
    public boolean installed;
    public boolean launched;

    public PackageUserState() {
        this.installed = false;
        this.launched = true;
        this.hidden = false;
    }

    protected PackageUserState(Parcel parcel) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        if (parcel.readByte() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.launched = z;
        if (parcel.readByte() != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.hidden = z2;
        this.installed = parcel.readByte() == 0 ? false : z3;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        byte b2;
        byte b3;
        byte b4 = 1;
        if (this.launched) {
            b2 = 1;
        } else {
            b2 = 0;
        }
        parcel.writeByte(b2);
        if (this.hidden) {
            b3 = 1;
        } else {
            b3 = 0;
        }
        parcel.writeByte(b3);
        if (!this.installed) {
            b4 = 0;
        }
        parcel.writeByte(b4);
    }
}
