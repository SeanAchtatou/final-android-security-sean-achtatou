package com.lody.virtual.server.device;

import android.os.Parcel;
import com.lody.virtual.helper.PersistenceLayer;
import com.lody.virtual.helper.collection.SparseArray;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.remote.VDeviceInfo;

public class DeviceInfoPersistenceLayer extends PersistenceLayer {
    private VDeviceManagerService mService;

    public DeviceInfoPersistenceLayer(VDeviceManagerService vDeviceManagerService) {
        super(VEnvironment.getDeviceInfoFile());
        this.mService = vDeviceManagerService;
    }

    public int getCurrentVersion() {
        return 1;
    }

    public void writeMagic(Parcel parcel) {
    }

    public boolean verifyMagic(Parcel parcel) {
        return true;
    }

    public void writePersistenceData(Parcel parcel) {
        SparseArray<VDeviceInfo> deviceInfos = this.mService.getDeviceInfos();
        int size = deviceInfos.size();
        parcel.writeInt(size);
        for (int i = 0; i < size; i++) {
            parcel.writeInt(deviceInfos.keyAt(i));
            deviceInfos.valueAt(i).writeToParcel(parcel, 0);
        }
    }

    public void readPersistenceData(Parcel parcel) {
        SparseArray<VDeviceInfo> deviceInfos = this.mService.getDeviceInfos();
        deviceInfos.clear();
        int readInt = parcel.readInt();
        while (true) {
            int i = readInt - 1;
            if (readInt > 0) {
                deviceInfos.put(parcel.readInt(), new VDeviceInfo(parcel));
                readInt = i;
            } else {
                return;
            }
        }
    }

    public boolean onVersionConflict(int i, int i2) {
        return false;
    }

    public void onPersistenceFileDamage() {
        getPersistenceFile().delete();
    }
}
