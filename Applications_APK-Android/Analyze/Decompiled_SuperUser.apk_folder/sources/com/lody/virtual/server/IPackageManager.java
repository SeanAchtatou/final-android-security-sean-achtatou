package com.lody.virtual.server;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.VParceledListSlice;
import com.lody.virtual.server.IPackageInstaller;
import java.util.List;

public interface IPackageManager extends IInterface {
    boolean activitySupportsIntent(ComponentName componentName, Intent intent, String str);

    int checkPermission(String str, String str2, int i);

    ActivityInfo getActivityInfo(ComponentName componentName, int i, int i2);

    List<PermissionGroupInfo> getAllPermissionGroups(int i);

    ApplicationInfo getApplicationInfo(String str, int i, int i2);

    VParceledListSlice getInstalledApplications(int i, int i2);

    VParceledListSlice getInstalledPackages(int i, int i2);

    String getNameForUid(int i);

    PackageInfo getPackageInfo(String str, int i, int i2);

    IPackageInstaller getPackageInstaller();

    int getPackageUid(String str, int i);

    String[] getPackagesForUid(int i);

    PermissionGroupInfo getPermissionGroupInfo(String str, int i);

    PermissionInfo getPermissionInfo(String str, int i);

    ProviderInfo getProviderInfo(ComponentName componentName, int i, int i2);

    ActivityInfo getReceiverInfo(ComponentName componentName, int i, int i2);

    ServiceInfo getServiceInfo(ComponentName componentName, int i, int i2);

    List<String> getSharedLibraries(String str);

    VParceledListSlice queryContentProviders(String str, int i, int i2);

    List<ResolveInfo> queryIntentActivities(Intent intent, String str, int i, int i2);

    List<ResolveInfo> queryIntentContentProviders(Intent intent, String str, int i, int i2);

    List<ResolveInfo> queryIntentReceivers(Intent intent, String str, int i, int i2);

    List<ResolveInfo> queryIntentServices(Intent intent, String str, int i, int i2);

    List<PermissionInfo> queryPermissionsByGroup(String str, int i);

    List<String> querySharedPackages(String str);

    ProviderInfo resolveContentProvider(String str, int i, int i2);

    ResolveInfo resolveIntent(Intent intent, String str, int i, int i2);

    ResolveInfo resolveService(Intent intent, String str, int i, int i2);

    public static abstract class Stub extends Binder implements IPackageManager {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IPackageManager";
        static final int TRANSACTION_activitySupportsIntent = 7;
        static final int TRANSACTION_checkPermission = 4;
        static final int TRANSACTION_getActivityInfo = 6;
        static final int TRANSACTION_getAllPermissionGroups = 22;
        static final int TRANSACTION_getApplicationInfo = 24;
        static final int TRANSACTION_getInstalledApplications = 18;
        static final int TRANSACTION_getInstalledPackages = 17;
        static final int TRANSACTION_getNameForUid = 27;
        static final int TRANSACTION_getPackageInfo = 5;
        static final int TRANSACTION_getPackageInstaller = 28;
        static final int TRANSACTION_getPackageUid = 1;
        static final int TRANSACTION_getPackagesForUid = 2;
        static final int TRANSACTION_getPermissionGroupInfo = 21;
        static final int TRANSACTION_getPermissionInfo = 19;
        static final int TRANSACTION_getProviderInfo = 10;
        static final int TRANSACTION_getReceiverInfo = 8;
        static final int TRANSACTION_getServiceInfo = 9;
        static final int TRANSACTION_getSharedLibraries = 3;
        static final int TRANSACTION_queryContentProviders = 25;
        static final int TRANSACTION_queryIntentActivities = 12;
        static final int TRANSACTION_queryIntentContentProviders = 16;
        static final int TRANSACTION_queryIntentReceivers = 13;
        static final int TRANSACTION_queryIntentServices = 15;
        static final int TRANSACTION_queryPermissionsByGroup = 20;
        static final int TRANSACTION_querySharedPackages = 26;
        static final int TRANSACTION_resolveContentProvider = 23;
        static final int TRANSACTION_resolveIntent = 11;
        static final int TRANSACTION_resolveService = 14;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IPackageManager asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IPackageManager)) {
                return new Proxy(iBinder);
            }
            return (IPackageManager) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            Intent intent;
            Intent intent2;
            Intent intent3;
            Intent intent4;
            Intent intent5;
            Intent intent6;
            ComponentName componentName;
            ComponentName componentName2;
            ComponentName componentName3;
            ComponentName componentName4;
            Intent intent7;
            int i3;
            ComponentName componentName5;
            IBinder iBinder = null;
            switch (i) {
                case 1:
                    parcel.enforceInterface(DESCRIPTOR);
                    int packageUid = getPackageUid(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeInt(packageUid);
                    return true;
                case 2:
                    parcel.enforceInterface(DESCRIPTOR);
                    String[] packagesForUid = getPackagesForUid(parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeStringArray(packagesForUid);
                    return true;
                case 3:
                    parcel.enforceInterface(DESCRIPTOR);
                    List<String> sharedLibraries = getSharedLibraries(parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeStringList(sharedLibraries);
                    return true;
                case 4:
                    parcel.enforceInterface(DESCRIPTOR);
                    int checkPermission = checkPermission(parcel.readString(), parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeInt(checkPermission);
                    return true;
                case 5:
                    parcel.enforceInterface(DESCRIPTOR);
                    PackageInfo packageInfo = getPackageInfo(parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (packageInfo != null) {
                        parcel2.writeInt(1);
                        packageInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 6:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        componentName5 = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName5 = null;
                    }
                    ActivityInfo activityInfo = getActivityInfo(componentName5, parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (activityInfo != null) {
                        parcel2.writeInt(1);
                        activityInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 7:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        componentName4 = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName4 = null;
                    }
                    if (parcel.readInt() != 0) {
                        intent7 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent7 = null;
                    }
                    boolean activitySupportsIntent = activitySupportsIntent(componentName4, intent7, parcel.readString());
                    parcel2.writeNoException();
                    if (activitySupportsIntent) {
                        i3 = 1;
                    } else {
                        i3 = 0;
                    }
                    parcel2.writeInt(i3);
                    return true;
                case 8:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        componentName3 = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName3 = null;
                    }
                    ActivityInfo receiverInfo = getReceiverInfo(componentName3, parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (receiverInfo != null) {
                        parcel2.writeInt(1);
                        receiverInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 9:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        componentName2 = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName2 = null;
                    }
                    ServiceInfo serviceInfo = getServiceInfo(componentName2, parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (serviceInfo != null) {
                        parcel2.writeInt(1);
                        serviceInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 10:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        componentName = (ComponentName) ComponentName.CREATOR.createFromParcel(parcel);
                    } else {
                        componentName = null;
                    }
                    ProviderInfo providerInfo = getProviderInfo(componentName, parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (providerInfo != null) {
                        parcel2.writeInt(1);
                        providerInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 11:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        intent6 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent6 = null;
                    }
                    ResolveInfo resolveIntent = resolveIntent(intent6, parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (resolveIntent != null) {
                        parcel2.writeInt(1);
                        resolveIntent.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 12:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        intent5 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent5 = null;
                    }
                    List<ResolveInfo> queryIntentActivities = queryIntentActivities(intent5, parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(queryIntentActivities);
                    return true;
                case 13:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        intent4 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent4 = null;
                    }
                    List<ResolveInfo> queryIntentReceivers = queryIntentReceivers(intent4, parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(queryIntentReceivers);
                    return true;
                case 14:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        intent3 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent3 = null;
                    }
                    ResolveInfo resolveService = resolveService(intent3, parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (resolveService != null) {
                        parcel2.writeInt(1);
                        resolveService.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 15:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        intent2 = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent2 = null;
                    }
                    List<ResolveInfo> queryIntentServices = queryIntentServices(intent2, parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(queryIntentServices);
                    return true;
                case 16:
                    parcel.enforceInterface(DESCRIPTOR);
                    if (parcel.readInt() != 0) {
                        intent = (Intent) Intent.CREATOR.createFromParcel(parcel);
                    } else {
                        intent = null;
                    }
                    List<ResolveInfo> queryIntentContentProviders = queryIntentContentProviders(intent, parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(queryIntentContentProviders);
                    return true;
                case 17:
                    parcel.enforceInterface(DESCRIPTOR);
                    VParceledListSlice installedPackages = getInstalledPackages(parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (installedPackages != null) {
                        parcel2.writeInt(1);
                        installedPackages.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 18:
                    parcel.enforceInterface(DESCRIPTOR);
                    VParceledListSlice installedApplications = getInstalledApplications(parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (installedApplications != null) {
                        parcel2.writeInt(1);
                        installedApplications.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 19:
                    parcel.enforceInterface(DESCRIPTOR);
                    PermissionInfo permissionInfo = getPermissionInfo(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    if (permissionInfo != null) {
                        parcel2.writeInt(1);
                        permissionInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 20:
                    parcel.enforceInterface(DESCRIPTOR);
                    List<PermissionInfo> queryPermissionsByGroup = queryPermissionsByGroup(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(queryPermissionsByGroup);
                    return true;
                case 21:
                    parcel.enforceInterface(DESCRIPTOR);
                    PermissionGroupInfo permissionGroupInfo = getPermissionGroupInfo(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    if (permissionGroupInfo != null) {
                        parcel2.writeInt(1);
                        permissionGroupInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 22:
                    parcel.enforceInterface(DESCRIPTOR);
                    List<PermissionGroupInfo> allPermissionGroups = getAllPermissionGroups(parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(allPermissionGroups);
                    return true;
                case 23:
                    parcel.enforceInterface(DESCRIPTOR);
                    ProviderInfo resolveContentProvider = resolveContentProvider(parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (resolveContentProvider != null) {
                        parcel2.writeInt(1);
                        resolveContentProvider.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 24:
                    parcel.enforceInterface(DESCRIPTOR);
                    ApplicationInfo applicationInfo = getApplicationInfo(parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (applicationInfo != null) {
                        parcel2.writeInt(1);
                        applicationInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 25:
                    parcel.enforceInterface(DESCRIPTOR);
                    VParceledListSlice queryContentProviders = queryContentProviders(parcel.readString(), parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (queryContentProviders != null) {
                        parcel2.writeInt(1);
                        queryContentProviders.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 26:
                    parcel.enforceInterface(DESCRIPTOR);
                    List<String> querySharedPackages = querySharedPackages(parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeStringList(querySharedPackages);
                    return true;
                case 27:
                    parcel.enforceInterface(DESCRIPTOR);
                    String nameForUid = getNameForUid(parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeString(nameForUid);
                    return true;
                case 28:
                    parcel.enforceInterface(DESCRIPTOR);
                    IPackageInstaller packageInstaller = getPackageInstaller();
                    parcel2.writeNoException();
                    if (packageInstaller != null) {
                        iBinder = packageInstaller.asBinder();
                    }
                    parcel2.writeStrongBinder(iBinder);
                    return true;
                case 1598968902:
                    parcel2.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        private static class Proxy implements IPackageManager {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public int getPackageUid(String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String[] getPackagesForUid(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createStringArray();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<String> getSharedLibraries(String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createStringArrayList();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int checkPermission(String str, String str2, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public PackageInfo getPackageInfo(String str, int i, int i2) {
                PackageInfo packageInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        packageInfo = (PackageInfo) PackageInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        packageInfo = null;
                    }
                    return packageInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ActivityInfo getActivityInfo(ComponentName componentName, int i, int i2) {
                ActivityInfo activityInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        activityInfo = (ActivityInfo) ActivityInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        activityInfo = null;
                    }
                    return activityInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean activitySupportsIntent(ComponentName componentName, Intent intent, String str) {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ActivityInfo getReceiverInfo(ComponentName componentName, int i, int i2) {
                ActivityInfo activityInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        activityInfo = (ActivityInfo) ActivityInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        activityInfo = null;
                    }
                    return activityInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ServiceInfo getServiceInfo(ComponentName componentName, int i, int i2) {
                ServiceInfo serviceInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        serviceInfo = (ServiceInfo) ServiceInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        serviceInfo = null;
                    }
                    return serviceInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ProviderInfo getProviderInfo(ComponentName componentName, int i, int i2) {
                ProviderInfo providerInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        providerInfo = (ProviderInfo) ProviderInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        providerInfo = null;
                    }
                    return providerInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ResolveInfo resolveIntent(Intent intent, String str, int i, int i2) {
                ResolveInfo resolveInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        resolveInfo = (ResolveInfo) ResolveInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        resolveInfo = null;
                    }
                    return resolveInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<ResolveInfo> queryIntentActivities(Intent intent, String str, int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(ResolveInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<ResolveInfo> queryIntentReceivers(Intent intent, String str, int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(ResolveInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ResolveInfo resolveService(Intent intent, String str, int i, int i2) {
                ResolveInfo resolveInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        resolveInfo = (ResolveInfo) ResolveInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        resolveInfo = null;
                    }
                    return resolveInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<ResolveInfo> queryIntentServices(Intent intent, String str, int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(ResolveInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<ResolveInfo> queryIntentContentProviders(Intent intent, String str, int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(ResolveInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VParceledListSlice getInstalledPackages(int i, int i2) {
                VParceledListSlice vParceledListSlice;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vParceledListSlice = VParceledListSlice.CREATOR.createFromParcel(obtain2);
                    } else {
                        vParceledListSlice = null;
                    }
                    return vParceledListSlice;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VParceledListSlice getInstalledApplications(int i, int i2) {
                VParceledListSlice vParceledListSlice;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vParceledListSlice = VParceledListSlice.CREATOR.createFromParcel(obtain2);
                    } else {
                        vParceledListSlice = null;
                    }
                    return vParceledListSlice;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public PermissionInfo getPermissionInfo(String str, int i) {
                PermissionInfo permissionInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        permissionInfo = (PermissionInfo) PermissionInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        permissionInfo = null;
                    }
                    return permissionInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<PermissionInfo> queryPermissionsByGroup(String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(PermissionInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public PermissionGroupInfo getPermissionGroupInfo(String str, int i) {
                PermissionGroupInfo permissionGroupInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        permissionGroupInfo = (PermissionGroupInfo) PermissionGroupInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        permissionGroupInfo = null;
                    }
                    return permissionGroupInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<PermissionGroupInfo> getAllPermissionGroups(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(PermissionGroupInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ProviderInfo resolveContentProvider(String str, int i, int i2) {
                ProviderInfo providerInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        providerInfo = (ProviderInfo) ProviderInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        providerInfo = null;
                    }
                    return providerInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ApplicationInfo getApplicationInfo(String str, int i, int i2) {
                ApplicationInfo applicationInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        applicationInfo = (ApplicationInfo) ApplicationInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        applicationInfo = null;
                    }
                    return applicationInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VParceledListSlice queryContentProviders(String str, int i, int i2) {
                VParceledListSlice vParceledListSlice;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vParceledListSlice = VParceledListSlice.CREATOR.createFromParcel(obtain2);
                    } else {
                        vParceledListSlice = null;
                    }
                    return vParceledListSlice;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<String> querySharedPackages(String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createStringArrayList();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getNameForUid(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IPackageInstaller getPackageInstaller() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                    return IPackageInstaller.Stub.asInterface(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
