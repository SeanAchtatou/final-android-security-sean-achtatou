package com.lody.virtual.server.pm;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.server.pm.parser.VPackage;
import com.lody.virtual.server.pm.parser.VPackage.IntentInfo;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class IntentResolver<F extends VPackage.IntentInfo, R> {
    private static final String TAG = "IntentResolver";
    private static final Comparator sResolvePrioritySorter = new Comparator() {
        public int compare(Object obj, Object obj2) {
            int priority;
            int priority2;
            if (obj instanceof IntentFilter) {
                priority = ((IntentFilter) obj).getPriority();
                priority2 = ((IntentFilter) obj2).getPriority();
            } else if (!(obj instanceof ResolveInfo)) {
                return 0;
            } else {
                ResolveInfo resolveInfo = (ResolveInfo) obj;
                ResolveInfo resolveInfo2 = (ResolveInfo) obj2;
                priority = resolveInfo.filter == null ? 0 : resolveInfo.filter.getPriority();
                priority2 = resolveInfo2.filter == null ? 0 : resolveInfo2.filter.getPriority();
            }
            if (priority > priority2) {
                return -1;
            }
            if (priority < priority2) {
                return 1;
            }
            return 0;
        }
    };
    private HashMap<String, F[]> mActionToFilter = new HashMap<>();
    private HashMap<String, F[]> mBaseTypeToFilter = new HashMap<>();
    private HashSet<F> mFilters = new HashSet<>();
    private HashMap<String, F[]> mSchemeToFilter = new HashMap<>();
    private HashMap<String, F[]> mTypeToFilter = new HashMap<>();
    private HashMap<String, F[]> mTypedActionToFilter = new HashMap<>();
    private HashMap<String, F[]> mWildTypeToFilter = new HashMap<>();

    /* access modifiers changed from: protected */
    public abstract boolean isPackageForFilter(String str, VPackage.IntentInfo intentInfo);

    /* access modifiers changed from: protected */
    public abstract F[] newArray(int i);

    private static FastImmutableArraySet<String> getFastIntentCategories(Intent intent) {
        Set<String> categories = intent.getCategories();
        if (categories == null) {
            return null;
        }
        return new FastImmutableArraySet<>(categories.toArray(new String[categories.size()]));
    }

    public void addFilter(F f2) {
        this.mFilters.add(f2);
        int register_intent_filter = register_intent_filter(f2, f2.filter.schemesIterator(), this.mSchemeToFilter, "      Scheme: ");
        int register_mime_types = register_mime_types(f2, "      Type: ");
        if (register_intent_filter == 0 && register_mime_types == 0) {
            register_intent_filter(f2, f2.filter.actionsIterator(), this.mActionToFilter, "      Action: ");
        }
        if (register_mime_types != 0) {
            register_intent_filter(f2, f2.filter.actionsIterator(), this.mTypedActionToFilter, "      TypedAction: ");
        }
    }

    private boolean filterEquals(IntentFilter intentFilter, IntentFilter intentFilter2) {
        int countDataSchemes;
        int countActions = intentFilter.countActions();
        if (countActions != intentFilter2.countActions()) {
            return false;
        }
        for (int i = 0; i < countActions; i++) {
            if (!intentFilter2.hasAction(intentFilter.getAction(i))) {
                return false;
            }
        }
        int countCategories = intentFilter.countCategories();
        if (countCategories != intentFilter2.countCategories()) {
            return false;
        }
        for (int i2 = 0; i2 < countCategories; i2++) {
            if (!intentFilter2.hasCategory(intentFilter.getCategory(i2))) {
                return false;
            }
        }
        if (intentFilter.countDataTypes() != intentFilter2.countDataTypes() || (countDataSchemes = intentFilter.countDataSchemes()) != intentFilter2.countDataSchemes()) {
            return false;
        }
        for (int i3 = 0; i3 < countDataSchemes; i3++) {
            if (!intentFilter2.hasDataScheme(intentFilter.getDataScheme(i3))) {
                return false;
            }
        }
        if (intentFilter.countDataAuthorities() != intentFilter2.countDataAuthorities() || intentFilter.countDataPaths() != intentFilter2.countDataPaths()) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 19 || intentFilter.countDataSchemeSpecificParts() == intentFilter2.countDataSchemeSpecificParts()) {
            return true;
        }
        return false;
    }

    private ArrayList<F> collectFilters(F[] fArr, IntentFilter intentFilter) {
        F f2;
        if (fArr == null) {
            return null;
        }
        ArrayList<F> arrayList = null;
        int i = 0;
        while (i < fArr.length && (f2 = fArr[i]) != null) {
            if (filterEquals(f2.filter, intentFilter)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(f2);
            }
            i++;
        }
        return arrayList;
    }

    public ArrayList<F> findFilters(IntentFilter intentFilter) {
        if (intentFilter.countDataSchemes() == 1) {
            return collectFilters((VPackage.IntentInfo[]) this.mSchemeToFilter.get(intentFilter.getDataScheme(0)), intentFilter);
        }
        if (intentFilter.countDataTypes() != 0 && intentFilter.countActions() == 1) {
            return collectFilters((VPackage.IntentInfo[]) this.mTypedActionToFilter.get(intentFilter.getAction(0)), intentFilter);
        }
        if (intentFilter.countDataTypes() == 0 && intentFilter.countDataSchemes() == 0 && intentFilter.countActions() == 1) {
            return collectFilters((VPackage.IntentInfo[]) this.mActionToFilter.get(intentFilter.getAction(0)), intentFilter);
        }
        ArrayList<F> arrayList = null;
        Iterator<F> it = this.mFilters.iterator();
        while (it.hasNext()) {
            VPackage.IntentInfo intentInfo = (VPackage.IntentInfo) it.next();
            if (filterEquals(intentInfo.filter, intentFilter)) {
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }
                arrayList.add(intentInfo);
            }
        }
        return arrayList;
    }

    public void removeFilter(F f2) {
        removeFilterInternal(f2);
        this.mFilters.remove(f2);
    }

    /* access modifiers changed from: package-private */
    public void removeFilterInternal(F f2) {
        int unregister_intent_filter = unregister_intent_filter(f2, f2.filter.schemesIterator(), this.mSchemeToFilter, "      Scheme: ");
        int unregister_mime_types = unregister_mime_types(f2, "      Type: ");
        if (unregister_intent_filter == 0 && unregister_mime_types == 0) {
            unregister_intent_filter(f2, f2.filter.actionsIterator(), this.mActionToFilter, "      Action: ");
        }
        if (unregister_mime_types != 0) {
            unregister_intent_filter(f2, f2.filter.actionsIterator(), this.mTypedActionToFilter, "      TypedAction: ");
        }
    }

    public Iterator<F> filterIterator() {
        return new IteratorWrapper(this.mFilters.iterator());
    }

    public Set<F> filterSet() {
        return Collections.unmodifiableSet(this.mFilters);
    }

    public List<R> queryIntentFromList(Intent intent, String str, boolean z, ArrayList<F[]> arrayList, int i) {
        ArrayList arrayList2 = new ArrayList();
        FastImmutableArraySet<String> fastIntentCategories = getFastIntentCategories(intent);
        String scheme = intent.getScheme();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            buildResolveList(intent, fastIntentCategories, z, str, scheme, (VPackage.IntentInfo[]) arrayList.get(i2), arrayList2, i);
        }
        sortResults(arrayList2);
        return arrayList2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<R> queryIntent(android.content.Intent r13, java.lang.String r14, boolean r15, int r16) {
        /*
            r12 = this;
            java.lang.String r5 = r13.getScheme()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r6 = 0
            r2 = 0
            r1 = 0
            r3 = 0
            if (r14 == 0) goto L_0x00d7
            r0 = 47
            int r0 = r14.indexOf(r0)
            if (r0 <= 0) goto L_0x00d7
            r4 = 0
            java.lang.String r4 = r14.substring(r4, r0)
            java.lang.String r8 = "*"
            boolean r8 = r4.equals(r8)
            if (r8 != 0) goto L_0x00bf
            int r1 = r14.length()
            int r2 = r0 + 2
            if (r1 != r2) goto L_0x0036
            int r0 = r0 + 1
            char r0 = r14.charAt(r0)
            r1 = 42
            if (r0 == r1) goto L_0x00ad
        L_0x0036:
            java.util.HashMap<java.lang.String, F[]> r0 = r12.mTypeToFilter
            java.lang.Object r0 = r0.get(r14)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r0 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r0
            java.util.HashMap<java.lang.String, F[]> r1 = r12.mWildTypeToFilter
            java.lang.Object r1 = r1.get(r4)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r1 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r1
            r2 = r0
        L_0x0047:
            java.util.HashMap<java.lang.String, F[]> r0 = r12.mWildTypeToFilter
            java.lang.String r4 = "*"
            java.lang.Object r0 = r0.get(r4)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r0 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r0
            r10 = r0
            r11 = r1
            r6 = r2
        L_0x0054:
            if (r5 == 0) goto L_0x00d5
            java.util.HashMap<java.lang.String, F[]> r0 = r12.mSchemeToFilter
            java.lang.Object r0 = r0.get(r5)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r0 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r0
            r9 = r0
        L_0x005f:
            if (r14 != 0) goto L_0x0076
            if (r5 != 0) goto L_0x0076
            java.lang.String r0 = r13.getAction()
            if (r0 == 0) goto L_0x0076
            java.util.HashMap<java.lang.String, F[]> r0 = r12.mActionToFilter
            java.lang.String r1 = r13.getAction()
            java.lang.Object r0 = r0.get(r1)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r0 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r0
            r6 = r0
        L_0x0076:
            com.lody.virtual.server.pm.FastImmutableArraySet r2 = getFastIntentCategories(r13)
            if (r6 == 0) goto L_0x0085
            r0 = r12
            r1 = r13
            r3 = r15
            r4 = r14
            r8 = r16
            r0.buildResolveList(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0085:
            if (r11 == 0) goto L_0x0091
            r0 = r12
            r1 = r13
            r3 = r15
            r4 = r14
            r6 = r11
            r8 = r16
            r0.buildResolveList(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0091:
            if (r10 == 0) goto L_0x009d
            r0 = r12
            r1 = r13
            r3 = r15
            r4 = r14
            r6 = r10
            r8 = r16
            r0.buildResolveList(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x009d:
            if (r9 == 0) goto L_0x00a9
            r0 = r12
            r1 = r13
            r3 = r15
            r4 = r14
            r6 = r9
            r8 = r16
            r0.buildResolveList(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x00a9:
            r12.sortResults(r7)
            return r7
        L_0x00ad:
            java.util.HashMap<java.lang.String, F[]> r0 = r12.mBaseTypeToFilter
            java.lang.Object r0 = r0.get(r4)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r0 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r0
            java.util.HashMap<java.lang.String, F[]> r1 = r12.mWildTypeToFilter
            java.lang.Object r1 = r1.get(r4)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r1 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r1
            r2 = r0
            goto L_0x0047
        L_0x00bf:
            java.lang.String r0 = r13.getAction()
            if (r0 == 0) goto L_0x00d7
            java.util.HashMap<java.lang.String, F[]> r0 = r12.mTypedActionToFilter
            java.lang.String r4 = r13.getAction()
            java.lang.Object r0 = r0.get(r4)
            com.lody.virtual.server.pm.parser.VPackage$IntentInfo[] r0 = (com.lody.virtual.server.pm.parser.VPackage.IntentInfo[]) r0
            r10 = r1
            r11 = r2
            r6 = r0
            goto L_0x0054
        L_0x00d5:
            r9 = r3
            goto L_0x005f
        L_0x00d7:
            r10 = r1
            r11 = r2
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.IntentResolver.queryIntent(android.content.Intent, java.lang.String, boolean, int):java.util.List");
    }

    /* access modifiers changed from: protected */
    public boolean allowFilterResult(VPackage.IntentInfo intentInfo, List list) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isFilterStopped(VPackage.IntentInfo intentInfo) {
        return false;
    }

    /* access modifiers changed from: protected */
    public R newResult(VPackage.IntentInfo intentInfo, int i, int i2) {
        return intentInfo;
    }

    /* access modifiers changed from: protected */
    public void sortResults(List<R> list) {
        Collections.sort(list, sResolvePrioritySorter);
    }

    /* access modifiers changed from: protected */
    public void dumpFilter(PrintWriter printWriter, String str, VPackage.IntentInfo intentInfo) {
        printWriter.print(str);
        printWriter.println(intentInfo);
    }

    /* access modifiers changed from: protected */
    public Object filterToLabel(VPackage.IntentInfo intentInfo) {
        return "IntentFilter";
    }

    /* access modifiers changed from: protected */
    public void dumpFilterLabel(PrintWriter printWriter, String str, Object obj, int i) {
        printWriter.print(str);
        printWriter.print(obj);
        printWriter.print(": ");
        printWriter.println(i);
    }

    private void addFilter(HashMap<String, F[]> hashMap, String str, F f2) {
        VPackage.IntentInfo[] intentInfoArr = (VPackage.IntentInfo[]) hashMap.get(str);
        if (intentInfoArr == null) {
            VPackage.IntentInfo[] newArray = newArray(2);
            hashMap.put(str, newArray);
            newArray[0] = f2;
            return;
        }
        int length = intentInfoArr.length;
        int i = length;
        while (i > 0 && intentInfoArr[i - 1] == null) {
            i--;
        }
        if (i < length) {
            intentInfoArr[i] = f2;
            return;
        }
        VPackage.IntentInfo[] newArray2 = newArray((length * 3) / 2);
        System.arraycopy(intentInfoArr, 0, newArray2, 0, length);
        newArray2[length] = f2;
        hashMap.put(str, newArray2);
    }

    private int register_mime_types(F f2, String str) {
        String str2;
        Iterator<String> typesIterator = f2.filter.typesIterator();
        if (typesIterator == null) {
            return 0;
        }
        int i = 0;
        while (typesIterator.hasNext()) {
            String next = typesIterator.next();
            i++;
            int indexOf = next.indexOf(47);
            if (indexOf > 0) {
                str2 = next;
                next = next.substring(0, indexOf).intern();
            } else {
                str2 = next + "/*";
            }
            addFilter(this.mTypeToFilter, str2, f2);
            if (indexOf > 0) {
                addFilter(this.mBaseTypeToFilter, next, f2);
            } else {
                addFilter(this.mWildTypeToFilter, next, f2);
            }
        }
        return i;
    }

    private int unregister_mime_types(F f2, String str) {
        String str2;
        Iterator<String> typesIterator = f2.filter.typesIterator();
        if (typesIterator == null) {
            return 0;
        }
        int i = 0;
        while (typesIterator.hasNext()) {
            String next = typesIterator.next();
            i++;
            int indexOf = next.indexOf(47);
            if (indexOf > 0) {
                str2 = next;
                next = next.substring(0, indexOf).intern();
            } else {
                str2 = next + "/*";
            }
            remove_all_objects(this.mTypeToFilter, str2, f2);
            if (indexOf > 0) {
                remove_all_objects(this.mBaseTypeToFilter, next, f2);
            } else {
                remove_all_objects(this.mWildTypeToFilter, next, f2);
            }
        }
        return i;
    }

    private int register_intent_filter(F f2, Iterator<String> it, HashMap<String, F[]> hashMap, String str) {
        if (it == null) {
            return 0;
        }
        int i = 0;
        while (it.hasNext()) {
            i++;
            addFilter(hashMap, it.next(), f2);
        }
        return i;
    }

    private int unregister_intent_filter(F f2, Iterator<String> it, HashMap<String, F[]> hashMap, String str) {
        if (it == null) {
            return 0;
        }
        int i = 0;
        while (it.hasNext()) {
            i++;
            remove_all_objects(hashMap, it.next(), f2);
        }
        return i;
    }

    private void remove_all_objects(HashMap<String, F[]> hashMap, String str, Object obj) {
        VPackage.IntentInfo[] intentInfoArr = (VPackage.IntentInfo[]) hashMap.get(str);
        if (intentInfoArr != null) {
            int length = intentInfoArr.length - 1;
            while (length >= 0 && intentInfoArr[length] == null) {
                length--;
            }
            for (int i = length; i >= 0; i--) {
                if (intentInfoArr[i] == obj) {
                    int i2 = length - i;
                    if (i2 > 0) {
                        System.arraycopy(intentInfoArr, i + 1, intentInfoArr, i, i2);
                    }
                    intentInfoArr[length] = null;
                    length--;
                }
            }
            if (length < 0) {
                hashMap.remove(str);
            } else if (length < intentInfoArr.length / 2) {
                VPackage.IntentInfo[] newArray = newArray(length + 2);
                System.arraycopy(intentInfoArr, 0, newArray, 0, length + 1);
                hashMap.put(str, newArray);
            }
        }
    }

    private void buildResolveList(Intent intent, FastImmutableArraySet<String> fastImmutableArraySet, boolean z, String str, String str2, F[] fArr, List<R> list, int i) {
        int i2;
        boolean z2;
        String action = intent.getAction();
        Uri data = intent.getData();
        String str3 = intent.getPackage();
        if (fArr != null) {
            i2 = fArr.length;
        } else {
            i2 = 0;
        }
        boolean z3 = false;
        int i3 = 0;
        while (i3 < i2) {
            F f2 = fArr[i3];
            if (f2 == null) {
                break;
            }
            if (str3 != null && !isPackageForFilter(str3, f2)) {
                z2 = z3;
            } else if (!allowFilterResult(f2, list)) {
                z2 = z3;
            } else {
                int match = f2.filter.match(action, str, str2, data, fastImmutableArraySet, TAG);
                if (match < 0) {
                    z2 = z3;
                } else if (!z || f2.filter.hasCategory("android.intent.category.DEFAULT")) {
                    Object newResult = newResult(f2, match, i);
                    if (newResult != null) {
                        list.add(newResult);
                    }
                    z2 = z3;
                } else {
                    z2 = true;
                }
            }
            i3++;
            z3 = z2;
        }
        if (!z3) {
            return;
        }
        if (list.size() == 0) {
            VLog.w(TAG, "resolveIntent failed: found match, but none with CATEGORY_DEFAULT", new Object[0]);
        } else if (list.size() > 1) {
            VLog.w(TAG, "resolveIntent: multiple matches, only some with CATEGORY_DEFAULT", new Object[0]);
        }
    }

    private class IteratorWrapper implements Iterator<F> {
        private F mCur;
        private Iterator<F> mI;

        IteratorWrapper(Iterator<F> it) {
            this.mI = it;
        }

        public boolean hasNext() {
            return this.mI.hasNext();
        }

        public F next() {
            F f2 = (VPackage.IntentInfo) this.mI.next();
            this.mCur = f2;
            return f2;
        }

        public void remove() {
            if (this.mCur != null) {
                IntentResolver.this.removeFilterInternal(this.mCur);
            }
            this.mI.remove();
        }
    }
}
