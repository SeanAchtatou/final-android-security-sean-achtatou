package com.lody.virtual.server;

import android.accounts.Account;
import android.accounts.AuthenticatorDescription;
import android.accounts.c;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public interface IAccountManager extends IInterface {
    boolean accountAuthenticated(int i, Account account);

    void addAccount(int i, c cVar, String str, String str2, String[] strArr, boolean z, Bundle bundle);

    boolean addAccountExplicitly(int i, Account account, String str, Bundle bundle);

    void clearPassword(int i, Account account);

    void confirmCredentials(int i, c cVar, Account account, Bundle bundle, boolean z);

    void editProperties(int i, c cVar, String str, boolean z);

    Account[] getAccounts(int i, String str);

    void getAccountsByFeatures(int i, c cVar, String str, String[] strArr);

    void getAuthToken(int i, c cVar, Account account, String str, boolean z, boolean z2, Bundle bundle);

    void getAuthTokenLabel(int i, c cVar, String str, String str2);

    AuthenticatorDescription[] getAuthenticatorTypes(int i);

    String getPassword(int i, Account account);

    String getPreviousName(int i, Account account);

    String getUserData(int i, Account account, String str);

    void hasFeatures(int i, c cVar, Account account, String[] strArr);

    void invalidateAuthToken(int i, String str, String str2);

    String peekAuthToken(int i, Account account, String str);

    void removeAccount(int i, c cVar, Account account, boolean z);

    boolean removeAccountExplicitly(int i, Account account);

    void renameAccount(int i, c cVar, Account account, String str);

    void setAuthToken(int i, Account account, String str, String str2);

    void setPassword(int i, Account account, String str);

    void setUserData(int i, Account account, String str, String str2);

    void updateCredentials(int i, c cVar, Account account, String str, boolean z, Bundle bundle);

    public static abstract class Stub extends Binder implements IAccountManager {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IAccountManager";
        static final int TRANSACTION_accountAuthenticated = 22;
        static final int TRANSACTION_addAccount = 16;
        static final int TRANSACTION_addAccountExplicitly = 17;
        static final int TRANSACTION_clearPassword = 21;
        static final int TRANSACTION_confirmCredentials = 15;
        static final int TRANSACTION_editProperties = 11;
        static final int TRANSACTION_getAccounts = 4;
        static final int TRANSACTION_getAccountsByFeatures = 2;
        static final int TRANSACTION_getAuthToken = 5;
        static final int TRANSACTION_getAuthTokenLabel = 12;
        static final int TRANSACTION_getAuthenticatorTypes = 1;
        static final int TRANSACTION_getPassword = 14;
        static final int TRANSACTION_getPreviousName = 3;
        static final int TRANSACTION_getUserData = 13;
        static final int TRANSACTION_hasFeatures = 9;
        static final int TRANSACTION_invalidateAuthToken = 23;
        static final int TRANSACTION_peekAuthToken = 24;
        static final int TRANSACTION_removeAccount = 20;
        static final int TRANSACTION_removeAccountExplicitly = 18;
        static final int TRANSACTION_renameAccount = 19;
        static final int TRANSACTION_setAuthToken = 7;
        static final int TRANSACTION_setPassword = 6;
        static final int TRANSACTION_setUserData = 8;
        static final int TRANSACTION_updateCredentials = 10;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IAccountManager asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IAccountManager)) {
                return new Proxy(iBinder);
            }
            return (IAccountManager) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            Account account;
            Account account2;
            Account account3;
            Account account4;
            Account account5;
            Account account6;
            Account account7;
            Bundle bundle;
            Account account8;
            Bundle bundle2;
            boolean z;
            Account account9;
            Account account10;
            Account account11;
            boolean z2;
            Bundle bundle3;
            Account account12;
            Account account13;
            Account account14;
            Account account15;
            Account account16;
            boolean z3;
            Account account17;
            boolean z4 = false;
            Bundle bundle4 = null;
            switch (i) {
                case 1:
                    parcel.enforceInterface(DESCRIPTOR);
                    AuthenticatorDescription[] authenticatorTypes = getAuthenticatorTypes(parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedArray(authenticatorTypes, 1);
                    return true;
                case 2:
                    parcel.enforceInterface(DESCRIPTOR);
                    getAccountsByFeatures(parcel.readInt(), c.a.asInterface(parcel.readStrongBinder()), parcel.readString(), parcel.createStringArray());
                    parcel2.writeNoException();
                    return true;
                case 3:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account17 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account17 = null;
                    }
                    String previousName = getPreviousName(readInt, account17);
                    parcel2.writeNoException();
                    parcel2.writeString(previousName);
                    return true;
                case 4:
                    parcel.enforceInterface(DESCRIPTOR);
                    Account[] accounts = getAccounts(parcel.readInt(), parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeTypedArray(accounts, 1);
                    return true;
                case 5:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt2 = parcel.readInt();
                    c asInterface = c.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account16 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account16 = null;
                    }
                    String readString = parcel.readString();
                    if (parcel.readInt() != 0) {
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                    if (parcel.readInt() != 0) {
                        z4 = true;
                    }
                    if (parcel.readInt() != 0) {
                        bundle4 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    }
                    getAuthToken(readInt2, asInterface, account16, readString, z3, z4, bundle4);
                    parcel2.writeNoException();
                    return true;
                case 6:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt3 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account15 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account15 = null;
                    }
                    setPassword(readInt3, account15, parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 7:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt4 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account14 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account14 = null;
                    }
                    setAuthToken(readInt4, account14, parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 8:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt5 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account13 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account13 = null;
                    }
                    setUserData(readInt5, account13, parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 9:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt6 = parcel.readInt();
                    c asInterface2 = c.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account12 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account12 = null;
                    }
                    hasFeatures(readInt6, asInterface2, account12, parcel.createStringArray());
                    parcel2.writeNoException();
                    return true;
                case 10:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt7 = parcel.readInt();
                    c asInterface3 = c.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account11 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account11 = null;
                    }
                    String readString2 = parcel.readString();
                    if (parcel.readInt() != 0) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    if (parcel.readInt() != 0) {
                        bundle3 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle3 = null;
                    }
                    updateCredentials(readInt7, asInterface3, account11, readString2, z2, bundle3);
                    parcel2.writeNoException();
                    return true;
                case 11:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt8 = parcel.readInt();
                    c asInterface4 = c.a.asInterface(parcel.readStrongBinder());
                    String readString3 = parcel.readString();
                    if (parcel.readInt() != 0) {
                        z4 = true;
                    }
                    editProperties(readInt8, asInterface4, readString3, z4);
                    parcel2.writeNoException();
                    return true;
                case 12:
                    parcel.enforceInterface(DESCRIPTOR);
                    getAuthTokenLabel(parcel.readInt(), c.a.asInterface(parcel.readStrongBinder()), parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 13:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt9 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account10 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account10 = null;
                    }
                    String userData = getUserData(readInt9, account10, parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeString(userData);
                    return true;
                case 14:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt10 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account9 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account9 = null;
                    }
                    String password = getPassword(readInt10, account9);
                    parcel2.writeNoException();
                    parcel2.writeString(password);
                    return true;
                case 15:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt11 = parcel.readInt();
                    c asInterface5 = c.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account8 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account8 = null;
                    }
                    if (parcel.readInt() != 0) {
                        bundle2 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle2 = null;
                    }
                    if (parcel.readInt() != 0) {
                        z = true;
                    } else {
                        z = false;
                    }
                    confirmCredentials(readInt11, asInterface5, account8, bundle2, z);
                    parcel2.writeNoException();
                    return true;
                case 16:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt12 = parcel.readInt();
                    c asInterface6 = c.a.asInterface(parcel.readStrongBinder());
                    String readString4 = parcel.readString();
                    String readString5 = parcel.readString();
                    String[] createStringArray = parcel.createStringArray();
                    if (parcel.readInt() != 0) {
                        z4 = true;
                    }
                    if (parcel.readInt() != 0) {
                        bundle4 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    }
                    addAccount(readInt12, asInterface6, readString4, readString5, createStringArray, z4, bundle4);
                    parcel2.writeNoException();
                    return true;
                case 17:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt13 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account7 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account7 = null;
                    }
                    String readString6 = parcel.readString();
                    if (parcel.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                    } else {
                        bundle = null;
                    }
                    boolean addAccountExplicitly = addAccountExplicitly(readInt13, account7, readString6, bundle);
                    parcel2.writeNoException();
                    if (addAccountExplicitly) {
                        z4 = true;
                    }
                    parcel2.writeInt(z4 ? 1 : 0);
                    return true;
                case 18:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt14 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account6 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account6 = null;
                    }
                    boolean removeAccountExplicitly = removeAccountExplicitly(readInt14, account6);
                    parcel2.writeNoException();
                    if (removeAccountExplicitly) {
                        z4 = true;
                    }
                    parcel2.writeInt(z4 ? 1 : 0);
                    return true;
                case 19:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt15 = parcel.readInt();
                    c asInterface7 = c.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account5 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account5 = null;
                    }
                    renameAccount(readInt15, asInterface7, account5, parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 20:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt16 = parcel.readInt();
                    c asInterface8 = c.a.asInterface(parcel.readStrongBinder());
                    if (parcel.readInt() != 0) {
                        account4 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account4 = null;
                    }
                    if (parcel.readInt() != 0) {
                        z4 = true;
                    }
                    removeAccount(readInt16, asInterface8, account4, z4);
                    parcel2.writeNoException();
                    return true;
                case 21:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt17 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account3 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account3 = null;
                    }
                    clearPassword(readInt17, account3);
                    parcel2.writeNoException();
                    return true;
                case 22:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt18 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account2 = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account2 = null;
                    }
                    boolean accountAuthenticated = accountAuthenticated(readInt18, account2);
                    parcel2.writeNoException();
                    if (accountAuthenticated) {
                        z4 = true;
                    }
                    parcel2.writeInt(z4 ? 1 : 0);
                    return true;
                case 23:
                    parcel.enforceInterface(DESCRIPTOR);
                    invalidateAuthToken(parcel.readInt(), parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 24:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt19 = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        account = (Account) Account.CREATOR.createFromParcel(parcel);
                    } else {
                        account = null;
                    }
                    String peekAuthToken = peekAuthToken(readInt19, account, parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeString(peekAuthToken);
                    return true;
                case 1598968902:
                    parcel2.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        private static class Proxy implements IAccountManager {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public AuthenticatorDescription[] getAuthenticatorTypes(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return (AuthenticatorDescription[]) obtain2.createTypedArray(AuthenticatorDescription.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void getAccountsByFeatures(int i, c cVar, String str, String[] strArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeStringArray(strArr);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getPreviousName(int i, Account account) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Account[] getAccounts(int i, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return (Account[]) obtain2.createTypedArray(Account.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void getAuthToken(int i, c cVar, Account account, String str, boolean z, boolean z2, Bundle bundle) {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setPassword(int i, Account account, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setAuthToken(int i, Account account, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setUserData(int i, Account account, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void hasFeatures(int i, c cVar, Account account, String[] strArr) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStringArray(strArr);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void updateCredentials(int i, c cVar, Account account, String str, boolean z, Bundle bundle) {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (!z) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void editProperties(int i, c cVar, String str, boolean z) {
                int i2 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i2 = 1;
                    }
                    obtain.writeInt(i2);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void getAuthTokenLabel(int i, c cVar, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getUserData(int i, Account account, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getPassword(int i, Account account) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void confirmCredentials(int i, c cVar, Account account, Bundle bundle, boolean z) {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!z) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void addAccount(int i, c cVar, String str, String str2, String[] strArr, boolean z, Bundle bundle) {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    if (!z) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean addAccountExplicitly(int i, Account account, String str, Bundle bundle) {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean removeAccountExplicitly(int i, Account account) {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void renameAccount(int i, c cVar, Account account, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void removeAccount(int i, c cVar, Account account, boolean z) {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(cVar != null ? cVar.asBinder() : null);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!z) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void clearPassword(int i, Account account) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean accountAuthenticated(int i, Account account) {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void invalidateAuthToken(int i, String str, String str2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.mRemote.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String peekAuthToken(int i, Account account, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (account != null) {
                        obtain.writeInt(1);
                        account.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    this.mRemote.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
