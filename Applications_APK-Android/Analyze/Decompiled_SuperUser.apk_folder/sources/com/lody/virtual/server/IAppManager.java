package com.lody.virtual.server;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.InstallResult;
import com.lody.virtual.remote.InstalledAppInfo;
import com.lody.virtual.server.interfaces.IAppRequestListener;
import com.lody.virtual.server.interfaces.IPackageObserver;
import java.util.List;

public interface IAppManager extends IInterface {
    void addVisibleOutsidePackage(String str);

    void clearAppRequestListener();

    IAppRequestListener getAppRequestListener();

    int getInstalledAppCount();

    InstalledAppInfo getInstalledAppInfo(String str, int i);

    List<InstalledAppInfo> getInstalledApps(int i);

    List<InstalledAppInfo> getInstalledAppsAsUser(int i, int i2);

    int[] getPackageInstalledUsers(String str);

    InstallResult installPackage(String str, int i);

    boolean installPackageAsUser(int i, String str);

    boolean isAppInstalled(String str);

    boolean isAppInstalledAsUser(int i, String str);

    boolean isOutsidePackageVisible(String str);

    boolean isPackageLaunched(int i, String str);

    void registerObserver(IPackageObserver iPackageObserver);

    void removeVisibleOutsidePackage(String str);

    void scanApps();

    void setAppRequestListener(IAppRequestListener iAppRequestListener);

    void setPackageHidden(int i, String str, boolean z);

    boolean uninstallPackage(String str);

    boolean uninstallPackageAsUser(String str, int i);

    void unregisterObserver(IPackageObserver iPackageObserver);

    public static abstract class Stub extends Binder implements IAppManager {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IAppManager";
        static final int TRANSACTION_addVisibleOutsidePackage = 3;
        static final int TRANSACTION_clearAppRequestListener = 21;
        static final int TRANSACTION_getAppRequestListener = 22;
        static final int TRANSACTION_getInstalledAppCount = 15;
        static final int TRANSACTION_getInstalledAppInfo = 6;
        static final int TRANSACTION_getInstalledApps = 13;
        static final int TRANSACTION_getInstalledAppsAsUser = 14;
        static final int TRANSACTION_getPackageInstalledUsers = 1;
        static final int TRANSACTION_installPackage = 7;
        static final int TRANSACTION_installPackageAsUser = 10;
        static final int TRANSACTION_isAppInstalled = 16;
        static final int TRANSACTION_isAppInstalledAsUser = 17;
        static final int TRANSACTION_isOutsidePackageVisible = 5;
        static final int TRANSACTION_isPackageLaunched = 8;
        static final int TRANSACTION_registerObserver = 18;
        static final int TRANSACTION_removeVisibleOutsidePackage = 4;
        static final int TRANSACTION_scanApps = 2;
        static final int TRANSACTION_setAppRequestListener = 20;
        static final int TRANSACTION_setPackageHidden = 9;
        static final int TRANSACTION_uninstallPackage = 12;
        static final int TRANSACTION_uninstallPackageAsUser = 11;
        static final int TRANSACTION_unregisterObserver = 19;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IAppManager asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IAppManager)) {
                return new Proxy(iBinder);
            }
            return (IAppManager) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            boolean z = false;
            switch (i) {
                case 1:
                    parcel.enforceInterface(DESCRIPTOR);
                    int[] packageInstalledUsers = getPackageInstalledUsers(parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeIntArray(packageInstalledUsers);
                    return true;
                case 2:
                    parcel.enforceInterface(DESCRIPTOR);
                    scanApps();
                    parcel2.writeNoException();
                    return true;
                case 3:
                    parcel.enforceInterface(DESCRIPTOR);
                    addVisibleOutsidePackage(parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 4:
                    parcel.enforceInterface(DESCRIPTOR);
                    removeVisibleOutsidePackage(parcel.readString());
                    parcel2.writeNoException();
                    return true;
                case 5:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean isOutsidePackageVisible = isOutsidePackageVisible(parcel.readString());
                    parcel2.writeNoException();
                    if (isOutsidePackageVisible) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 6:
                    parcel.enforceInterface(DESCRIPTOR);
                    InstalledAppInfo installedAppInfo = getInstalledAppInfo(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    if (installedAppInfo != null) {
                        parcel2.writeInt(1);
                        installedAppInfo.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 7:
                    parcel.enforceInterface(DESCRIPTOR);
                    InstallResult installPackage = installPackage(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    if (installPackage != null) {
                        parcel2.writeInt(1);
                        installPackage.writeToParcel(parcel2, 1);
                        return true;
                    }
                    parcel2.writeInt(0);
                    return true;
                case 8:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean isPackageLaunched = isPackageLaunched(parcel.readInt(), parcel.readString());
                    parcel2.writeNoException();
                    if (isPackageLaunched) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 9:
                    parcel.enforceInterface(DESCRIPTOR);
                    int readInt = parcel.readInt();
                    String readString = parcel.readString();
                    if (parcel.readInt() != 0) {
                        z = true;
                    }
                    setPackageHidden(readInt, readString, z);
                    parcel2.writeNoException();
                    return true;
                case 10:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean installPackageAsUser = installPackageAsUser(parcel.readInt(), parcel.readString());
                    parcel2.writeNoException();
                    if (installPackageAsUser) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 11:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean uninstallPackageAsUser = uninstallPackageAsUser(parcel.readString(), parcel.readInt());
                    parcel2.writeNoException();
                    if (uninstallPackageAsUser) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 12:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean uninstallPackage = uninstallPackage(parcel.readString());
                    parcel2.writeNoException();
                    if (uninstallPackage) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 13:
                    parcel.enforceInterface(DESCRIPTOR);
                    List<InstalledAppInfo> installedApps = getInstalledApps(parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(installedApps);
                    return true;
                case 14:
                    parcel.enforceInterface(DESCRIPTOR);
                    List<InstalledAppInfo> installedAppsAsUser = getInstalledAppsAsUser(parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeTypedList(installedAppsAsUser);
                    return true;
                case 15:
                    parcel.enforceInterface(DESCRIPTOR);
                    int installedAppCount = getInstalledAppCount();
                    parcel2.writeNoException();
                    parcel2.writeInt(installedAppCount);
                    return true;
                case 16:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean isAppInstalled = isAppInstalled(parcel.readString());
                    parcel2.writeNoException();
                    if (isAppInstalled) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 17:
                    parcel.enforceInterface(DESCRIPTOR);
                    boolean isAppInstalledAsUser = isAppInstalledAsUser(parcel.readInt(), parcel.readString());
                    parcel2.writeNoException();
                    if (isAppInstalledAsUser) {
                        z = true;
                    }
                    parcel2.writeInt(z ? 1 : 0);
                    return true;
                case 18:
                    parcel.enforceInterface(DESCRIPTOR);
                    registerObserver(IPackageObserver.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case 19:
                    parcel.enforceInterface(DESCRIPTOR);
                    unregisterObserver(IPackageObserver.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case 20:
                    parcel.enforceInterface(DESCRIPTOR);
                    setAppRequestListener(IAppRequestListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    return true;
                case 21:
                    parcel.enforceInterface(DESCRIPTOR);
                    clearAppRequestListener();
                    parcel2.writeNoException();
                    return true;
                case 22:
                    parcel.enforceInterface(DESCRIPTOR);
                    IAppRequestListener appRequestListener = getAppRequestListener();
                    parcel2.writeNoException();
                    parcel2.writeStrongBinder(appRequestListener != null ? appRequestListener.asBinder() : null);
                    return true;
                case 1598968902:
                    parcel2.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }

        private static class Proxy implements IAppManager {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public int[] getPackageInstalledUsers(String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createIntArray();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void scanApps() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void addVisibleOutsidePackage(String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void removeVisibleOutsidePackage(String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isOutsidePackageVisible(String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public InstalledAppInfo getInstalledAppInfo(String str, int i) {
                InstalledAppInfo installedAppInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        installedAppInfo = InstalledAppInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        installedAppInfo = null;
                    }
                    return installedAppInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public InstallResult installPackage(String str, int i) {
                InstallResult installResult;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        installResult = InstallResult.CREATOR.createFromParcel(obtain2);
                    } else {
                        installResult = null;
                    }
                    return installResult;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isPackageLaunched(int i, String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setPackageHidden(int i, String str, boolean z) {
                int i2 = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (z) {
                        i2 = 1;
                    }
                    obtain.writeInt(i2);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean installPackageAsUser(int i, String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean uninstallPackageAsUser(String str, int i) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean uninstallPackage(String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<InstalledAppInfo> getInstalledApps(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(InstalledAppInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<InstalledAppInfo> getInstalledAppsAsUser(int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(InstalledAppInfo.CREATOR);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getInstalledAppCount() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isAppInstalled(String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isAppInstalledAsUser(int i, String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void registerObserver(IPackageObserver iPackageObserver) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iPackageObserver != null ? iPackageObserver.asBinder() : null);
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterObserver(IPackageObserver iPackageObserver) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iPackageObserver != null ? iPackageObserver.asBinder() : null);
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setAppRequestListener(IAppRequestListener iAppRequestListener) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iAppRequestListener != null ? iAppRequestListener.asBinder() : null);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void clearAppRequestListener() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IAppRequestListener getAppRequestListener() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                    return IAppRequestListener.Stub.asInterface(obtain2.readStrongBinder());
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
