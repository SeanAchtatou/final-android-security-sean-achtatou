package com.lody.virtual.server;

import android.app.IServiceConnection;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ProviderInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.lody.virtual.remote.AppTaskInfo;
import com.lody.virtual.remote.BadgerInfo;
import com.lody.virtual.remote.PendingIntentData;
import com.lody.virtual.remote.PendingResultData;
import com.lody.virtual.remote.VParceledListSlice;
import com.lody.virtual.server.interfaces.IProcessObserver;
import java.util.List;

public interface IActivityManager extends IInterface {
    IBinder acquireProviderClient(int i, ProviderInfo providerInfo);

    void addPendingIntent(IBinder iBinder, String str);

    void appDoneExecuting();

    int bindService(IBinder iBinder, IBinder iBinder2, Intent intent, String str, IServiceConnection iServiceConnection, int i, int i2);

    void broadcastFinish(PendingResultData pendingResultData);

    void dump();

    ComponentName getActivityClassForToken(int i, IBinder iBinder);

    String getAppProcessName(int i);

    ComponentName getCallingActivity(int i, IBinder iBinder);

    String getCallingPackage(int i, IBinder iBinder);

    int getFreeStubCount();

    String getInitialPackage(int i);

    String getPackageForIntentSender(IBinder iBinder);

    String getPackageForToken(int i, IBinder iBinder);

    PendingIntentData getPendingIntent(IBinder iBinder);

    List<String> getProcessPkgList(int i);

    VParceledListSlice getServices(int i, int i2, int i3);

    int getSystemPid();

    AppTaskInfo getTaskInfo(int i);

    int getUidByPid(int i);

    void handleApplicationCrash();

    int initProcess(String str, String str2, int i);

    boolean isAppPid(int i);

    boolean isAppProcess(String str);

    boolean isAppRunning(String str, int i);

    boolean isVAServiceToken(IBinder iBinder);

    void killAllApps();

    void killAppByPkg(String str, int i);

    void killApplicationProcess(String str, int i);

    void notifyBadgerChange(BadgerInfo badgerInfo);

    void onActivityCreated(ComponentName componentName, ComponentName componentName2, IBinder iBinder, Intent intent, String str, int i, int i2, int i3);

    boolean onActivityDestroyed(int i, IBinder iBinder);

    void onActivityResumed(int i, IBinder iBinder);

    IBinder peekService(Intent intent, String str, int i);

    void processRestarted(String str, String str2, int i);

    void publishService(IBinder iBinder, Intent intent, IBinder iBinder2, int i);

    void registerProcessObserver(IProcessObserver iProcessObserver);

    void removePendingIntent(IBinder iBinder);

    void serviceDoneExecuting(IBinder iBinder, int i, int i2, int i3, int i4);

    void setServiceForeground(ComponentName componentName, IBinder iBinder, int i, Notification notification, boolean z, int i2);

    int startActivities(Intent[] intentArr, String[] strArr, IBinder iBinder, Bundle bundle, int i);

    int startActivity(Intent intent, ActivityInfo activityInfo, IBinder iBinder, Bundle bundle, String str, int i, int i2);

    ComponentName startService(IBinder iBinder, Intent intent, String str, int i);

    int stopService(IBinder iBinder, Intent intent, String str, int i);

    boolean stopServiceToken(ComponentName componentName, IBinder iBinder, int i, int i2);

    void unbindFinished(IBinder iBinder, Intent intent, boolean z, int i);

    boolean unbindService(IServiceConnection iServiceConnection, int i);

    void unregisterProcessObserver(IProcessObserver iProcessObserver);

    public static abstract class Stub extends Binder implements IActivityManager {
        private static final String DESCRIPTOR = "com.lody.virtual.server.IActivityManager";
        static final int TRANSACTION_acquireProviderClient = 41;
        static final int TRANSACTION_addPendingIntent = 43;
        static final int TRANSACTION_appDoneExecuting = 18;
        static final int TRANSACTION_bindService = 34;
        static final int TRANSACTION_broadcastFinish = 47;
        static final int TRANSACTION_dump = 13;
        static final int TRANSACTION_getActivityClassForToken = 24;
        static final int TRANSACTION_getAppProcessName = 8;
        static final int TRANSACTION_getCallingActivity = 26;
        static final int TRANSACTION_getCallingPackage = 25;
        static final int TRANSACTION_getFreeStubCount = 2;
        static final int TRANSACTION_getInitialPackage = 16;
        static final int TRANSACTION_getPackageForIntentSender = 45;
        static final int TRANSACTION_getPackageForToken = 28;
        static final int TRANSACTION_getPendingIntent = 42;
        static final int TRANSACTION_getProcessPkgList = 9;
        static final int TRANSACTION_getServices = 40;
        static final int TRANSACTION_getSystemPid = 3;
        static final int TRANSACTION_getTaskInfo = 27;
        static final int TRANSACTION_getUidByPid = 4;
        static final int TRANSACTION_handleApplicationCrash = 17;
        static final int TRANSACTION_initProcess = 1;
        static final int TRANSACTION_isAppPid = 7;
        static final int TRANSACTION_isAppProcess = 5;
        static final int TRANSACTION_isAppRunning = 6;
        static final int TRANSACTION_isVAServiceToken = 29;
        static final int TRANSACTION_killAllApps = 10;
        static final int TRANSACTION_killAppByPkg = 11;
        static final int TRANSACTION_killApplicationProcess = 12;
        static final int TRANSACTION_notifyBadgerChange = 48;
        static final int TRANSACTION_onActivityCreated = 21;
        static final int TRANSACTION_onActivityDestroyed = 23;
        static final int TRANSACTION_onActivityResumed = 22;
        static final int TRANSACTION_peekService = 38;
        static final int TRANSACTION_processRestarted = 46;
        static final int TRANSACTION_publishService = 39;
        static final int TRANSACTION_registerProcessObserver = 14;
        static final int TRANSACTION_removePendingIntent = 44;
        static final int TRANSACTION_serviceDoneExecuting = 37;
        static final int TRANSACTION_setServiceForeground = 33;
        static final int TRANSACTION_startActivities = 19;
        static final int TRANSACTION_startActivity = 20;
        static final int TRANSACTION_startService = 30;
        static final int TRANSACTION_stopService = 31;
        static final int TRANSACTION_stopServiceToken = 32;
        static final int TRANSACTION_unbindFinished = 36;
        static final int TRANSACTION_unbindService = 35;
        static final int TRANSACTION_unregisterProcessObserver = 15;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IActivityManager asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IActivityManager)) {
                return new Proxy(iBinder);
            }
            return (IActivityManager) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        /* JADX WARN: Type inference failed for: r4v0 */
        /* JADX WARN: Type inference failed for: r4v3, types: [android.app.Notification] */
        /* JADX WARN: Type inference failed for: r4v4 */
        /* JADX WARN: Type inference failed for: r4v5, types: [android.content.Intent] */
        /* JADX WARN: Type inference failed for: r4v6 */
        /* JADX WARN: Type inference failed for: r4v7, types: [android.os.Bundle] */
        /* JADX WARN: Type inference failed for: r4v8 */
        /* JADX WARN: Type inference failed for: r4v9, types: [android.os.Bundle] */
        /* JADX WARN: Type inference failed for: r4v10 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTransact(int r11, android.os.Parcel r12, android.os.Parcel r13, int r14) {
            /*
                r10 = this;
                r5 = 0
                r4 = 0
                r9 = 1
                switch(r11) {
                    case 1: goto L_0x0011;
                    case 2: goto L_0x002d;
                    case 3: goto L_0x003d;
                    case 4: goto L_0x004d;
                    case 5: goto L_0x0061;
                    case 6: goto L_0x007a;
                    case 7: goto L_0x0096;
                    case 8: goto L_0x00ae;
                    case 9: goto L_0x00c3;
                    case 10: goto L_0x00d8;
                    case 11: goto L_0x00e5;
                    case 12: goto L_0x00fa;
                    case 13: goto L_0x010f;
                    case 14: goto L_0x011c;
                    case 15: goto L_0x0131;
                    case 16: goto L_0x0146;
                    case 17: goto L_0x015b;
                    case 18: goto L_0x0168;
                    case 19: goto L_0x0175;
                    case 20: goto L_0x01aa;
                    case 21: goto L_0x01fd;
                    case 22: goto L_0x0250;
                    case 23: goto L_0x0265;
                    case 24: goto L_0x0281;
                    case 25: goto L_0x02a4;
                    case 26: goto L_0x02bd;
                    case 27: goto L_0x02e0;
                    case 28: goto L_0x02ff;
                    case 29: goto L_0x0318;
                    case 30: goto L_0x0330;
                    case 31: goto L_0x0367;
                    case 32: goto L_0x0394;
                    case 33: goto L_0x03c4;
                    case 34: goto L_0x0405;
                    case 35: goto L_0x0444;
                    case 36: goto L_0x0464;
                    case 37: goto L_0x0490;
                    case 38: goto L_0x04b2;
                    case 39: goto L_0x04db;
                    case 40: goto L_0x0504;
                    case 41: goto L_0x052b;
                    case 42: goto L_0x0550;
                    case 43: goto L_0x056f;
                    case 44: goto L_0x0584;
                    case 45: goto L_0x0595;
                    case 46: goto L_0x05aa;
                    case 47: goto L_0x05c3;
                    case 48: goto L_0x05e0;
                    case 1598968902: goto L_0x000b;
                    default: goto L_0x0006;
                }
            L_0x0006:
                boolean r9 = super.onTransact(r11, r12, r13, r14)
            L_0x000a:
                return r9
            L_0x000b:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r13.writeString(r0)
                goto L_0x000a
            L_0x0011:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                java.lang.String r0 = r12.readString()
                java.lang.String r1 = r12.readString()
                int r2 = r12.readInt()
                int r0 = r10.initProcess(r0, r1, r2)
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x002d:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r10.getFreeStubCount()
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x003d:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r10.getSystemPid()
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x004d:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                int r0 = r10.getUidByPid(r0)
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x0061:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                java.lang.String r0 = r12.readString()
                boolean r0 = r10.isAppProcess(r0)
                r13.writeNoException()
                if (r0 == 0) goto L_0x0078
                r0 = r9
            L_0x0074:
                r13.writeInt(r0)
                goto L_0x000a
            L_0x0078:
                r0 = r5
                goto L_0x0074
            L_0x007a:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                java.lang.String r0 = r12.readString()
                int r1 = r12.readInt()
                boolean r0 = r10.isAppRunning(r0, r1)
                r13.writeNoException()
                if (r0 == 0) goto L_0x0091
                r5 = r9
            L_0x0091:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x0096:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                boolean r0 = r10.isAppPid(r0)
                r13.writeNoException()
                if (r0 == 0) goto L_0x00a9
                r5 = r9
            L_0x00a9:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x00ae:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                java.lang.String r0 = r10.getAppProcessName(r0)
                r13.writeNoException()
                r13.writeString(r0)
                goto L_0x000a
            L_0x00c3:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                java.util.List r0 = r10.getProcessPkgList(r0)
                r13.writeNoException()
                r13.writeStringList(r0)
                goto L_0x000a
            L_0x00d8:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                r10.killAllApps()
                r13.writeNoException()
                goto L_0x000a
            L_0x00e5:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                java.lang.String r0 = r12.readString()
                int r1 = r12.readInt()
                r10.killAppByPkg(r0, r1)
                r13.writeNoException()
                goto L_0x000a
            L_0x00fa:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                java.lang.String r0 = r12.readString()
                int r1 = r12.readInt()
                r10.killApplicationProcess(r0, r1)
                r13.writeNoException()
                goto L_0x000a
            L_0x010f:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                r10.dump()
                r13.writeNoException()
                goto L_0x000a
            L_0x011c:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                com.lody.virtual.server.interfaces.IProcessObserver r0 = com.lody.virtual.server.interfaces.IProcessObserver.Stub.asInterface(r0)
                r10.registerProcessObserver(r0)
                r13.writeNoException()
                goto L_0x000a
            L_0x0131:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                com.lody.virtual.server.interfaces.IProcessObserver r0 = com.lody.virtual.server.interfaces.IProcessObserver.Stub.asInterface(r0)
                r10.unregisterProcessObserver(r0)
                r13.writeNoException()
                goto L_0x000a
            L_0x0146:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                java.lang.String r0 = r10.getInitialPackage(r0)
                r13.writeNoException()
                r13.writeString(r0)
                goto L_0x000a
            L_0x015b:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                r10.handleApplicationCrash()
                r13.writeNoException()
                goto L_0x000a
            L_0x0168:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                r10.appDoneExecuting()
                r13.writeNoException()
                goto L_0x000a
            L_0x0175:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object[] r1 = r12.createTypedArray(r0)
                android.content.Intent[] r1 = (android.content.Intent[]) r1
                java.lang.String[] r2 = r12.createStringArray()
                android.os.IBinder r3 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0199
                android.os.Parcelable$Creator r0 = android.os.Bundle.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.os.Bundle r0 = (android.os.Bundle) r0
                r4 = r0
            L_0x0199:
                int r5 = r12.readInt()
                r0 = r10
                int r0 = r0.startActivities(r1, r2, r3, r4, r5)
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x01aa:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x01f9
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
                r1 = r0
            L_0x01be:
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x01fb
                android.os.Parcelable$Creator r0 = android.content.pm.ActivityInfo.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.pm.ActivityInfo r0 = (android.content.pm.ActivityInfo) r0
                r2 = r0
            L_0x01cd:
                android.os.IBinder r3 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x01e0
                android.os.Parcelable$Creator r0 = android.os.Bundle.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.os.Bundle r0 = (android.os.Bundle) r0
                r4 = r0
            L_0x01e0:
                java.lang.String r5 = r12.readString()
                int r6 = r12.readInt()
                int r7 = r12.readInt()
                r0 = r10
                int r0 = r0.startActivity(r1, r2, r3, r4, r5, r6, r7)
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x01f9:
                r1 = r4
                goto L_0x01be
            L_0x01fb:
                r2 = r4
                goto L_0x01cd
            L_0x01fd:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x024c
                android.os.Parcelable$Creator r0 = android.content.ComponentName.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.ComponentName r0 = (android.content.ComponentName) r0
                r1 = r0
            L_0x0211:
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x024e
                android.os.Parcelable$Creator r0 = android.content.ComponentName.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.ComponentName r0 = (android.content.ComponentName) r0
                r2 = r0
            L_0x0220:
                android.os.IBinder r3 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0233
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
                r4 = r0
            L_0x0233:
                java.lang.String r5 = r12.readString()
                int r6 = r12.readInt()
                int r7 = r12.readInt()
                int r8 = r12.readInt()
                r0 = r10
                r0.onActivityCreated(r1, r2, r3, r4, r5, r6, r7, r8)
                r13.writeNoException()
                goto L_0x000a
            L_0x024c:
                r1 = r4
                goto L_0x0211
            L_0x024e:
                r2 = r4
                goto L_0x0220
            L_0x0250:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                android.os.IBinder r1 = r12.readStrongBinder()
                r10.onActivityResumed(r0, r1)
                r13.writeNoException()
                goto L_0x000a
            L_0x0265:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                android.os.IBinder r1 = r12.readStrongBinder()
                boolean r0 = r10.onActivityDestroyed(r0, r1)
                r13.writeNoException()
                if (r0 == 0) goto L_0x027c
                r5 = r9
            L_0x027c:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x0281:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                android.os.IBinder r1 = r12.readStrongBinder()
                android.content.ComponentName r0 = r10.getActivityClassForToken(r0, r1)
                r13.writeNoException()
                if (r0 == 0) goto L_0x029f
                r13.writeInt(r9)
                r0.writeToParcel(r13, r9)
                goto L_0x000a
            L_0x029f:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x02a4:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                android.os.IBinder r1 = r12.readStrongBinder()
                java.lang.String r0 = r10.getCallingPackage(r0, r1)
                r13.writeNoException()
                r13.writeString(r0)
                goto L_0x000a
            L_0x02bd:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                android.os.IBinder r1 = r12.readStrongBinder()
                android.content.ComponentName r0 = r10.getCallingActivity(r0, r1)
                r13.writeNoException()
                if (r0 == 0) goto L_0x02db
                r13.writeInt(r9)
                r0.writeToParcel(r13, r9)
                goto L_0x000a
            L_0x02db:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x02e0:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                com.lody.virtual.remote.AppTaskInfo r0 = r10.getTaskInfo(r0)
                r13.writeNoException()
                if (r0 == 0) goto L_0x02fa
                r13.writeInt(r9)
                r0.writeToParcel(r13, r9)
                goto L_0x000a
            L_0x02fa:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x02ff:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                android.os.IBinder r1 = r12.readStrongBinder()
                java.lang.String r0 = r10.getPackageForToken(r0, r1)
                r13.writeNoException()
                r13.writeString(r0)
                goto L_0x000a
            L_0x0318:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                boolean r0 = r10.isVAServiceToken(r0)
                r13.writeNoException()
                if (r0 == 0) goto L_0x032b
                r5 = r9
            L_0x032b:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x0330:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r1 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0360
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
            L_0x0347:
                java.lang.String r2 = r12.readString()
                int r3 = r12.readInt()
                android.content.ComponentName r0 = r10.startService(r1, r0, r2, r3)
                r13.writeNoException()
                if (r0 == 0) goto L_0x0362
                r13.writeInt(r9)
                r0.writeToParcel(r13, r9)
                goto L_0x000a
            L_0x0360:
                r0 = r4
                goto L_0x0347
            L_0x0362:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x0367:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r1 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0392
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
            L_0x037e:
                java.lang.String r2 = r12.readString()
                int r3 = r12.readInt()
                int r0 = r10.stopService(r1, r0, r2, r3)
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x0392:
                r0 = r4
                goto L_0x037e
            L_0x0394:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x03c2
                android.os.Parcelable$Creator r0 = android.content.ComponentName.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.ComponentName r0 = (android.content.ComponentName) r0
            L_0x03a7:
                android.os.IBinder r1 = r12.readStrongBinder()
                int r2 = r12.readInt()
                int r3 = r12.readInt()
                boolean r0 = r10.stopServiceToken(r0, r1, r2, r3)
                r13.writeNoException()
                if (r0 == 0) goto L_0x03bd
                r5 = r9
            L_0x03bd:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x03c2:
                r0 = r4
                goto L_0x03a7
            L_0x03c4:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0403
                android.os.Parcelable$Creator r0 = android.content.ComponentName.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.ComponentName r0 = (android.content.ComponentName) r0
                r1 = r0
            L_0x03d8:
                android.os.IBinder r2 = r12.readStrongBinder()
                int r3 = r12.readInt()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x03ef
                android.os.Parcelable$Creator r0 = android.app.Notification.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.app.Notification r0 = (android.app.Notification) r0
                r4 = r0
            L_0x03ef:
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x03f6
                r5 = r9
            L_0x03f6:
                int r6 = r12.readInt()
                r0 = r10
                r0.setServiceForeground(r1, r2, r3, r4, r5, r6)
                r13.writeNoException()
                goto L_0x000a
            L_0x0403:
                r1 = r4
                goto L_0x03d8
            L_0x0405:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r1 = r12.readStrongBinder()
                android.os.IBinder r2 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0442
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
                r3 = r0
            L_0x0421:
                java.lang.String r4 = r12.readString()
                android.os.IBinder r0 = r12.readStrongBinder()
                android.app.IServiceConnection r5 = android.app.IServiceConnection.Stub.asInterface(r0)
                int r6 = r12.readInt()
                int r7 = r12.readInt()
                r0 = r10
                int r0 = r0.bindService(r1, r2, r3, r4, r5, r6, r7)
                r13.writeNoException()
                r13.writeInt(r0)
                goto L_0x000a
            L_0x0442:
                r3 = r4
                goto L_0x0421
            L_0x0444:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                android.app.IServiceConnection r0 = android.app.IServiceConnection.Stub.asInterface(r0)
                int r1 = r12.readInt()
                boolean r0 = r10.unbindService(r0, r1)
                r13.writeNoException()
                if (r0 == 0) goto L_0x045f
                r5 = r9
            L_0x045f:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x0464:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r1 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x048e
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
            L_0x047b:
                int r2 = r12.readInt()
                if (r2 == 0) goto L_0x0482
                r5 = r9
            L_0x0482:
                int r2 = r12.readInt()
                r10.unbindFinished(r1, r0, r5, r2)
                r13.writeNoException()
                goto L_0x000a
            L_0x048e:
                r0 = r4
                goto L_0x047b
            L_0x0490:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r1 = r12.readStrongBinder()
                int r2 = r12.readInt()
                int r3 = r12.readInt()
                int r4 = r12.readInt()
                int r5 = r12.readInt()
                r0 = r10
                r0.serviceDoneExecuting(r1, r2, r3, r4, r5)
                r13.writeNoException()
                goto L_0x000a
            L_0x04b2:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x04d9
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
            L_0x04c5:
                java.lang.String r1 = r12.readString()
                int r2 = r12.readInt()
                android.os.IBinder r0 = r10.peekService(r0, r1, r2)
                r13.writeNoException()
                r13.writeStrongBinder(r0)
                goto L_0x000a
            L_0x04d9:
                r0 = r4
                goto L_0x04c5
            L_0x04db:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r1 = r12.readStrongBinder()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x0502
                android.os.Parcelable$Creator r0 = android.content.Intent.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.Intent r0 = (android.content.Intent) r0
            L_0x04f2:
                android.os.IBinder r2 = r12.readStrongBinder()
                int r3 = r12.readInt()
                r10.publishService(r1, r0, r2, r3)
                r13.writeNoException()
                goto L_0x000a
            L_0x0502:
                r0 = r4
                goto L_0x04f2
            L_0x0504:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                int r1 = r12.readInt()
                int r2 = r12.readInt()
                com.lody.virtual.remote.VParceledListSlice r0 = r10.getServices(r0, r1, r2)
                r13.writeNoException()
                if (r0 == 0) goto L_0x0526
                r13.writeInt(r9)
                r0.writeToParcel(r13, r9)
                goto L_0x000a
            L_0x0526:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x052b:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r1 = r12.readInt()
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x054e
                android.os.Parcelable$Creator r0 = android.content.pm.ProviderInfo.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                android.content.pm.ProviderInfo r0 = (android.content.pm.ProviderInfo) r0
            L_0x0542:
                android.os.IBinder r0 = r10.acquireProviderClient(r1, r0)
                r13.writeNoException()
                r13.writeStrongBinder(r0)
                goto L_0x000a
            L_0x054e:
                r0 = r4
                goto L_0x0542
            L_0x0550:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                com.lody.virtual.remote.PendingIntentData r0 = r10.getPendingIntent(r0)
                r13.writeNoException()
                if (r0 == 0) goto L_0x056a
                r13.writeInt(r9)
                r0.writeToParcel(r13, r9)
                goto L_0x000a
            L_0x056a:
                r13.writeInt(r5)
                goto L_0x000a
            L_0x056f:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                java.lang.String r1 = r12.readString()
                r10.addPendingIntent(r0, r1)
                r13.writeNoException()
                goto L_0x000a
            L_0x0584:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                r10.removePendingIntent(r0)
                r13.writeNoException()
                goto L_0x000a
            L_0x0595:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                android.os.IBinder r0 = r12.readStrongBinder()
                java.lang.String r0 = r10.getPackageForIntentSender(r0)
                r13.writeNoException()
                r13.writeString(r0)
                goto L_0x000a
            L_0x05aa:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                java.lang.String r0 = r12.readString()
                java.lang.String r1 = r12.readString()
                int r2 = r12.readInt()
                r10.processRestarted(r0, r1, r2)
                r13.writeNoException()
                goto L_0x000a
            L_0x05c3:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x05de
                android.os.Parcelable$Creator<com.lody.virtual.remote.PendingResultData> r0 = com.lody.virtual.remote.PendingResultData.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                com.lody.virtual.remote.PendingResultData r0 = (com.lody.virtual.remote.PendingResultData) r0
            L_0x05d6:
                r10.broadcastFinish(r0)
                r13.writeNoException()
                goto L_0x000a
            L_0x05de:
                r0 = r4
                goto L_0x05d6
            L_0x05e0:
                java.lang.String r0 = "com.lody.virtual.server.IActivityManager"
                r12.enforceInterface(r0)
                int r0 = r12.readInt()
                if (r0 == 0) goto L_0x05fb
                android.os.Parcelable$Creator<com.lody.virtual.remote.BadgerInfo> r0 = com.lody.virtual.remote.BadgerInfo.CREATOR
                java.lang.Object r0 = r0.createFromParcel(r12)
                com.lody.virtual.remote.BadgerInfo r0 = (com.lody.virtual.remote.BadgerInfo) r0
            L_0x05f3:
                r10.notifyBadgerChange(r0)
                r13.writeNoException()
                goto L_0x000a
            L_0x05fb:
                r0 = r4
                goto L_0x05f3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.IActivityManager.Stub.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
        }

        private static class Proxy implements IActivityManager {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public int initProcess(String str, String str2, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getFreeStubCount() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getSystemPid() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getUidByPid(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isAppProcess(String str) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isAppRunning(String str, int i) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isAppPid(int i) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getAppProcessName(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<String> getProcessPkgList(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createStringArrayList();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void killAllApps() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void killAppByPkg(String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void killApplicationProcess(String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void dump() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void registerProcessObserver(IProcessObserver iProcessObserver) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iProcessObserver != null ? iProcessObserver.asBinder() : null);
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterProcessObserver(IProcessObserver iProcessObserver) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iProcessObserver != null ? iProcessObserver.asBinder() : null);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getInitialPackage(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void handleApplicationCrash() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void appDoneExecuting() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int startActivities(Intent[] intentArr, String[] strArr, IBinder iBinder, Bundle bundle, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeTypedArray(intentArr, 0);
                    obtain.writeStringArray(strArr);
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int startActivity(Intent intent, ActivityInfo activityInfo, IBinder iBinder, Bundle bundle, String str, int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (activityInfo != null) {
                        obtain.writeInt(1);
                        activityInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onActivityCreated(ComponentName componentName, ComponentName componentName2, IBinder iBinder, Intent intent, String str, int i, int i2, int i3) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (componentName2 != null) {
                        obtain.writeInt(1);
                        componentName2.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.mRemote.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onActivityResumed(int i, IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean onActivityDestroyed(int i, IBinder iBinder) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ComponentName getActivityClassForToken(int i, IBinder iBinder) {
                ComponentName componentName;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        componentName = (ComponentName) ComponentName.CREATOR.createFromParcel(obtain2);
                    } else {
                        componentName = null;
                    }
                    return componentName;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getCallingPackage(int i, IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ComponentName getCallingActivity(int i, IBinder iBinder) {
                ComponentName componentName;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        componentName = (ComponentName) ComponentName.CREATOR.createFromParcel(obtain2);
                    } else {
                        componentName = null;
                    }
                    return componentName;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public AppTaskInfo getTaskInfo(int i) {
                AppTaskInfo appTaskInfo;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    this.mRemote.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        appTaskInfo = AppTaskInfo.CREATOR.createFromParcel(obtain2);
                    } else {
                        appTaskInfo = null;
                    }
                    return appTaskInfo;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getPackageForToken(int i, IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean isVAServiceToken(IBinder iBinder) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(29, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ComponentName startService(IBinder iBinder, Intent intent, String str, int i) {
                ComponentName componentName;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(30, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        componentName = (ComponentName) ComponentName.CREATOR.createFromParcel(obtain2);
                    } else {
                        componentName = null;
                    }
                    return componentName;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int stopService(IBinder iBinder, Intent intent, String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(31, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean stopServiceToken(ComponentName componentName, IBinder iBinder, int i, int i2) {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(32, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void setServiceForeground(ComponentName componentName, IBinder iBinder, int i, Notification notification, boolean z, int i2) {
                int i3 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (componentName != null) {
                        obtain.writeInt(1);
                        componentName.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeInt(i);
                    if (notification != null) {
                        obtain.writeInt(1);
                        notification.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!z) {
                        i3 = 0;
                    }
                    obtain.writeInt(i3);
                    obtain.writeInt(i2);
                    this.mRemote.transact(33, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int bindService(IBinder iBinder, IBinder iBinder2, Intent intent, String str, IServiceConnection iServiceConnection, int i, int i2) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeStrongBinder(iBinder2);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeStrongBinder(iServiceConnection != null ? iServiceConnection.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.mRemote.transact(34, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean unbindService(IServiceConnection iServiceConnection, int i) {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iServiceConnection != null ? iServiceConnection.asBinder() : null);
                    obtain.writeInt(i);
                    this.mRemote.transact(35, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unbindFinished(IBinder iBinder, Intent intent, boolean z, int i) {
                int i2 = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!z) {
                        i2 = 0;
                    }
                    obtain.writeInt(i2);
                    obtain.writeInt(i);
                    this.mRemote.transact(36, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void serviceDoneExecuting(IBinder iBinder, int i, int i2, int i3, int i4) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeInt(i4);
                    this.mRemote.transact(37, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder peekService(Intent intent, String str, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    this.mRemote.transact(38, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void publishService(IBinder iBinder, Intent intent, IBinder iBinder2, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    if (intent != null) {
                        obtain.writeInt(1);
                        intent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder2);
                    obtain.writeInt(i);
                    this.mRemote.transact(39, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public VParceledListSlice getServices(int i, int i2, int i3) {
                VParceledListSlice vParceledListSlice;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    this.mRemote.transact(40, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        vParceledListSlice = VParceledListSlice.CREATOR.createFromParcel(obtain2);
                    } else {
                        vParceledListSlice = null;
                    }
                    return vParceledListSlice;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder acquireProviderClient(int i, ProviderInfo providerInfo) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeInt(i);
                    if (providerInfo != null) {
                        obtain.writeInt(1);
                        providerInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(41, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public PendingIntentData getPendingIntent(IBinder iBinder) {
                PendingIntentData pendingIntentData;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(42, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        pendingIntentData = PendingIntentData.CREATOR.createFromParcel(obtain2);
                    } else {
                        pendingIntentData = null;
                    }
                    return pendingIntentData;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void addPendingIntent(IBinder iBinder, String str) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.mRemote.transact(43, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void removePendingIntent(IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(44, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getPackageForIntentSender(IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(45, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void processRestarted(String str, String str2, int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    this.mRemote.transact(46, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void broadcastFinish(PendingResultData pendingResultData) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (pendingResultData != null) {
                        obtain.writeInt(1);
                        pendingResultData.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(47, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void notifyBadgerChange(BadgerInfo badgerInfo) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    if (badgerInfo != null) {
                        obtain.writeInt(1);
                        badgerInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(48, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
