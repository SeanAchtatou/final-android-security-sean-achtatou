package com.lody.virtual.server.pm.installer;

import android.annotation.TargetApi;
import android.content.pm.PackageInstaller;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import mirror.android.content.pm.PackageInstaller;

@TargetApi(21)
public class SessionParams implements Parcelable {
    public static final Parcelable.Creator<SessionParams> CREATOR = new Parcelable.Creator<SessionParams>() {
        public SessionParams createFromParcel(Parcel parcel) {
            return new SessionParams(parcel);
        }

        public SessionParams[] newArray(int i) {
            return new SessionParams[i];
        }
    };
    public static final int MODE_FULL_INSTALL = 1;
    public static final int MODE_INHERIT_EXISTING = 2;
    public static final int MODE_INVALID = -1;
    public String abiOverride;
    public Bitmap appIcon;
    public long appIconLastModified = -1;
    public String appLabel;
    public String appPackageName;
    public String[] grantedRuntimePermissions;
    public int installFlags;
    public int installLocation = 1;
    public int mode = -1;
    public Uri originatingUri;
    public Uri referrerUri;
    public long sizeBytes = -1;
    public String volumeUuid;

    public SessionParams(int i) {
        this.mode = i;
    }

    public PackageInstaller.SessionParams build() {
        if (Build.VERSION.SDK_INT >= 23) {
            PackageInstaller.SessionParams sessionParams = new PackageInstaller.SessionParams(this.mode);
            PackageInstaller.SessionParamsMarshmallow.installFlags.set(sessionParams, this.installFlags);
            PackageInstaller.SessionParamsMarshmallow.installLocation.set(sessionParams, this.installLocation);
            PackageInstaller.SessionParamsMarshmallow.sizeBytes.set(sessionParams, this.sizeBytes);
            PackageInstaller.SessionParamsMarshmallow.appPackageName.set(sessionParams, this.appPackageName);
            PackageInstaller.SessionParamsMarshmallow.appIcon.set(sessionParams, this.appIcon);
            PackageInstaller.SessionParamsMarshmallow.appLabel.set(sessionParams, this.appLabel);
            PackageInstaller.SessionParamsMarshmallow.appIconLastModified.set(sessionParams, this.appIconLastModified);
            PackageInstaller.SessionParamsMarshmallow.originatingUri.set(sessionParams, this.originatingUri);
            PackageInstaller.SessionParamsMarshmallow.referrerUri.set(sessionParams, this.referrerUri);
            PackageInstaller.SessionParamsMarshmallow.abiOverride.set(sessionParams, this.abiOverride);
            PackageInstaller.SessionParamsMarshmallow.volumeUuid.set(sessionParams, this.volumeUuid);
            PackageInstaller.SessionParamsMarshmallow.grantedRuntimePermissions.set(sessionParams, this.grantedRuntimePermissions);
            return sessionParams;
        }
        PackageInstaller.SessionParams sessionParams2 = new PackageInstaller.SessionParams(this.mode);
        PackageInstaller.SessionParamsLOLLIPOP.installFlags.set(sessionParams2, this.installFlags);
        PackageInstaller.SessionParamsLOLLIPOP.installLocation.set(sessionParams2, this.installLocation);
        PackageInstaller.SessionParamsLOLLIPOP.sizeBytes.set(sessionParams2, this.sizeBytes);
        PackageInstaller.SessionParamsLOLLIPOP.appPackageName.set(sessionParams2, this.appPackageName);
        PackageInstaller.SessionParamsLOLLIPOP.appIcon.set(sessionParams2, this.appIcon);
        PackageInstaller.SessionParamsLOLLIPOP.appLabel.set(sessionParams2, this.appLabel);
        PackageInstaller.SessionParamsLOLLIPOP.appIconLastModified.set(sessionParams2, this.appIconLastModified);
        PackageInstaller.SessionParamsLOLLIPOP.originatingUri.set(sessionParams2, this.originatingUri);
        PackageInstaller.SessionParamsLOLLIPOP.referrerUri.set(sessionParams2, this.referrerUri);
        PackageInstaller.SessionParamsLOLLIPOP.abiOverride.set(sessionParams2, this.abiOverride);
        return sessionParams2;
    }

    public static SessionParams create(PackageInstaller.SessionParams sessionParams) {
        if (Build.VERSION.SDK_INT >= 23) {
            SessionParams sessionParams2 = new SessionParams(PackageInstaller.SessionParamsMarshmallow.mode.get(sessionParams));
            sessionParams2.installFlags = PackageInstaller.SessionParamsMarshmallow.installFlags.get(sessionParams);
            sessionParams2.installLocation = PackageInstaller.SessionParamsMarshmallow.installLocation.get(sessionParams);
            sessionParams2.sizeBytes = PackageInstaller.SessionParamsMarshmallow.sizeBytes.get(sessionParams);
            sessionParams2.appPackageName = PackageInstaller.SessionParamsMarshmallow.appPackageName.get(sessionParams);
            sessionParams2.appIcon = PackageInstaller.SessionParamsMarshmallow.appIcon.get(sessionParams);
            sessionParams2.appLabel = PackageInstaller.SessionParamsMarshmallow.appLabel.get(sessionParams);
            sessionParams2.appIconLastModified = PackageInstaller.SessionParamsMarshmallow.appIconLastModified.get(sessionParams);
            sessionParams2.originatingUri = PackageInstaller.SessionParamsMarshmallow.originatingUri.get(sessionParams);
            sessionParams2.referrerUri = PackageInstaller.SessionParamsMarshmallow.referrerUri.get(sessionParams);
            sessionParams2.abiOverride = PackageInstaller.SessionParamsMarshmallow.abiOverride.get(sessionParams);
            sessionParams2.volumeUuid = PackageInstaller.SessionParamsMarshmallow.volumeUuid.get(sessionParams);
            sessionParams2.grantedRuntimePermissions = PackageInstaller.SessionParamsMarshmallow.grantedRuntimePermissions.get(sessionParams);
            return sessionParams2;
        }
        SessionParams sessionParams3 = new SessionParams(PackageInstaller.SessionParamsLOLLIPOP.mode.get(sessionParams));
        sessionParams3.installFlags = PackageInstaller.SessionParamsLOLLIPOP.installFlags.get(sessionParams);
        sessionParams3.installLocation = PackageInstaller.SessionParamsLOLLIPOP.installLocation.get(sessionParams);
        sessionParams3.sizeBytes = PackageInstaller.SessionParamsLOLLIPOP.sizeBytes.get(sessionParams);
        sessionParams3.appPackageName = PackageInstaller.SessionParamsLOLLIPOP.appPackageName.get(sessionParams);
        sessionParams3.appIcon = PackageInstaller.SessionParamsLOLLIPOP.appIcon.get(sessionParams);
        sessionParams3.appLabel = PackageInstaller.SessionParamsLOLLIPOP.appLabel.get(sessionParams);
        sessionParams3.appIconLastModified = PackageInstaller.SessionParamsLOLLIPOP.appIconLastModified.get(sessionParams);
        sessionParams3.originatingUri = PackageInstaller.SessionParamsLOLLIPOP.originatingUri.get(sessionParams);
        sessionParams3.referrerUri = PackageInstaller.SessionParamsLOLLIPOP.referrerUri.get(sessionParams);
        sessionParams3.abiOverride = PackageInstaller.SessionParamsLOLLIPOP.abiOverride.get(sessionParams);
        return sessionParams3;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mode);
        parcel.writeInt(this.installFlags);
        parcel.writeInt(this.installLocation);
        parcel.writeLong(this.sizeBytes);
        parcel.writeString(this.appPackageName);
        parcel.writeParcelable(this.appIcon, i);
        parcel.writeString(this.appLabel);
        parcel.writeLong(this.appIconLastModified);
        parcel.writeParcelable(this.originatingUri, i);
        parcel.writeParcelable(this.referrerUri, i);
        parcel.writeString(this.abiOverride);
        parcel.writeString(this.volumeUuid);
        parcel.writeStringArray(this.grantedRuntimePermissions);
    }

    protected SessionParams(Parcel parcel) {
        this.mode = parcel.readInt();
        this.installFlags = parcel.readInt();
        this.installLocation = parcel.readInt();
        this.sizeBytes = parcel.readLong();
        this.appPackageName = parcel.readString();
        this.appIcon = (Bitmap) parcel.readParcelable(Bitmap.class.getClassLoader());
        this.appLabel = parcel.readString();
        this.appIconLastModified = parcel.readLong();
        this.originatingUri = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.referrerUri = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.abiOverride = parcel.readString();
        this.volumeUuid = parcel.readString();
        this.grantedRuntimePermissions = parcel.createStringArray();
    }
}
