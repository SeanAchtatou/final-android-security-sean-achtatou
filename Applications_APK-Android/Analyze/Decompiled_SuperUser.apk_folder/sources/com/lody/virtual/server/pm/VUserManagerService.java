package com.lody.virtual.server.pm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.util.SparseArray;
import com.kingouser.com.util.ShellUtils;
import com.lody.virtual.R;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.Constants;
import com.lody.virtual.helper.utils.ArrayUtils;
import com.lody.virtual.helper.utils.AtomicFile;
import com.lody.virtual.helper.utils.FastXmlSerializer;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VBinder;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.os.VUserInfo;
import com.lody.virtual.os.VUserManager;
import com.lody.virtual.server.IUserManager;
import com.lody.virtual.server.am.VActivityManagerService;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class VUserManagerService extends IUserManager.Stub {
    private static final String ATTR_CREATION_TIME = "created";
    private static final String ATTR_FLAGS = "flags";
    private static final String ATTR_ICON_PATH = "icon";
    private static final String ATTR_ID = "id";
    private static final String ATTR_LAST_LOGGED_IN_TIME = "lastLoggedIn";
    private static final String ATTR_NEXT_SERIAL_NO = "nextSerialNumber";
    private static final String ATTR_PARTIAL = "partial";
    private static final String ATTR_SERIAL_NO = "serialNumber";
    private static final String ATTR_USER_VERSION = "version";
    private static final boolean DBG = false;
    private static final long EPOCH_PLUS_30_YEARS = 946080000000L;
    private static final String LOG_TAG = "VUserManagerService";
    private static final int MIN_USER_ID = 1;
    private static final String TAG_NAME = "name";
    private static final String TAG_USER = "user";
    private static final String TAG_USERS = "users";
    private static final String USER_INFO_DIR = ("system" + File.separator + TAG_USERS);
    private static final String USER_LIST_FILENAME = "userlist.xml";
    private static final String USER_PHOTO_FILENAME = "photo.png";
    private static final int USER_VERSION = 1;
    private static VUserManagerService sInstance;
    private final File mBaseUserPath;
    private final Context mContext;
    private boolean mGuestEnabled;
    /* access modifiers changed from: private */
    public final Object mInstallLock;
    private int mNextSerialNumber;
    private int mNextUserId;
    /* access modifiers changed from: private */
    public final Object mPackagesLock;
    private final VPackageManagerService mPm;
    private HashSet<Integer> mRemovingUserIds;
    private int[] mUserIds;
    private final File mUserListFile;
    private int mUserVersion;
    private SparseArray<VUserInfo> mUsers;
    private final File mUsersDir;

    VUserManagerService(Context context, VPackageManagerService vPackageManagerService, Object obj, Object obj2) {
        this(context, vPackageManagerService, obj, obj2, VEnvironment.getDataDirectory(), new File(VEnvironment.getDataDirectory(), "user"));
    }

    private VUserManagerService(Context context, VPackageManagerService vPackageManagerService, Object obj, Object obj2, File file, File file2) {
        this.mUsers = new SparseArray<>();
        this.mRemovingUserIds = new HashSet<>();
        this.mNextUserId = 1;
        this.mUserVersion = 0;
        this.mContext = context;
        this.mPm = vPackageManagerService;
        this.mInstallLock = obj;
        this.mPackagesLock = obj2;
        synchronized (this.mInstallLock) {
            synchronized (this.mPackagesLock) {
                this.mUsersDir = new File(file, USER_INFO_DIR);
                this.mUsersDir.mkdirs();
                new File(this.mUsersDir, "0").mkdirs();
                this.mBaseUserPath = file2;
                this.mUserListFile = new File(this.mUsersDir, USER_LIST_FILENAME);
                readUserListLocked();
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < this.mUsers.size(); i++) {
                    VUserInfo valueAt = this.mUsers.valueAt(i);
                    if (valueAt.partial && i != 0) {
                        arrayList.add(valueAt);
                    }
                }
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    VUserInfo vUserInfo = (VUserInfo) arrayList.get(i2);
                    VLog.w(LOG_TAG, "Removing partially created user #" + i2 + " (name=" + vUserInfo.name + ")", new Object[0]);
                    removeUserStateLocked(vUserInfo.id);
                }
                sInstance = this;
            }
        }
    }

    public static VUserManagerService get() {
        VUserManagerService vUserManagerService;
        synchronized (VUserManagerService.class) {
            vUserManagerService = sInstance;
        }
        return vUserManagerService;
    }

    private static void checkManageUsersPermission(String str) {
        if (VBinder.getCallingUid() != VirtualCore.get().myUid()) {
            throw new SecurityException("You need MANAGE_USERS permission to: " + str);
        }
    }

    public List<VUserInfo> getUsers(boolean z) {
        ArrayList arrayList;
        synchronized (this.mPackagesLock) {
            arrayList = new ArrayList(this.mUsers.size());
            for (int i = 0; i < this.mUsers.size(); i++) {
                VUserInfo valueAt = this.mUsers.valueAt(i);
                if (!valueAt.partial && (!z || !this.mRemovingUserIds.contains(Integer.valueOf(valueAt.id)))) {
                    arrayList.add(valueAt);
                }
            }
        }
        return arrayList;
    }

    public VUserInfo getUserInfo(int i) {
        VUserInfo userInfoLocked;
        synchronized (this.mPackagesLock) {
            userInfoLocked = getUserInfoLocked(i);
        }
        return userInfoLocked;
    }

    private VUserInfo getUserInfoLocked(int i) {
        VUserInfo vUserInfo = this.mUsers.get(i);
        if (vUserInfo == null || !vUserInfo.partial || this.mRemovingUserIds.contains(Integer.valueOf(i))) {
            return vUserInfo;
        }
        VLog.w(LOG_TAG, "getUserInfo: unknown user #" + i, new Object[0]);
        return null;
    }

    public boolean exists(int i) {
        boolean contains;
        synchronized (this.mPackagesLock) {
            contains = ArrayUtils.contains(this.mUserIds, i);
        }
        return contains;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        if (r0 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0047, code lost:
        sendUserInfoChangedBroadcast(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setUserName(int r5, java.lang.String r6) {
        /*
            r4 = this;
            r1 = 0
            java.lang.String r0 = "rename users"
            checkManageUsersPermission(r0)
            java.lang.Object r2 = r4.mPackagesLock
            monitor-enter(r2)
            android.util.SparseArray<com.lody.virtual.os.VUserInfo> r0 = r4.mUsers     // Catch:{ all -> 0x004b }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x004b }
            com.lody.virtual.os.VUserInfo r0 = (com.lody.virtual.os.VUserInfo) r0     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0017
            boolean r3 = r0.partial     // Catch:{ all -> 0x004b }
            if (r3 == 0) goto L_0x0034
        L_0x0017:
            java.lang.String r0 = "VUserManagerService"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x004b }
            r1.<init>()     // Catch:{ all -> 0x004b }
            java.lang.String r3 = "setUserName: unknown user #"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x004b }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x004b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x004b }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x004b }
            com.lody.virtual.helper.utils.VLog.w(r0, r1, r3)     // Catch:{ all -> 0x004b }
            monitor-exit(r2)     // Catch:{ all -> 0x004b }
        L_0x0033:
            return
        L_0x0034:
            if (r6 == 0) goto L_0x004e
            java.lang.String r3 = r0.name     // Catch:{ all -> 0x004b }
            boolean r3 = r6.equals(r3)     // Catch:{ all -> 0x004b }
            if (r3 != 0) goto L_0x004e
            r0.name = r6     // Catch:{ all -> 0x004b }
            r4.writeUserLocked(r0)     // Catch:{ all -> 0x004b }
            r0 = 1
        L_0x0044:
            monitor-exit(r2)     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0033
            r4.sendUserInfoChangedBroadcast(r5)
            goto L_0x0033
        L_0x004b:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004b }
            throw r0
        L_0x004e:
            r0 = r1
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VUserManagerService.setUserName(int, java.lang.String):void");
    }

    public void setUserIcon(int i, Bitmap bitmap) {
        checkManageUsersPermission("update users");
        synchronized (this.mPackagesLock) {
            VUserInfo vUserInfo = this.mUsers.get(i);
            if (vUserInfo == null || vUserInfo.partial) {
                VLog.w(LOG_TAG, "setUserIcon: unknown user #" + i, new Object[0]);
                return;
            }
            writeBitmapLocked(vUserInfo, bitmap);
            writeUserLocked(vUserInfo);
            sendUserInfoChangedBroadcast(i);
        }
    }

    private void sendUserInfoChangedBroadcast(int i) {
        Intent intent = new Intent(Constants.ACTION_USER_INFO_CHANGED);
        intent.putExtra(Constants.EXTRA_USER_HANDLE, i);
        intent.addFlags(1073741824);
        VActivityManagerService.get().sendBroadcastAsUser(intent, new VUserHandle(i));
    }

    public Bitmap getUserIcon(int i) {
        synchronized (this.mPackagesLock) {
            VUserInfo vUserInfo = this.mUsers.get(i);
            if (vUserInfo == null || vUserInfo.partial) {
                VLog.w(LOG_TAG, "getUserIcon: unknown user #" + i, new Object[0]);
                return null;
            } else if (vUserInfo.iconPath == null) {
                return null;
            } else {
                Bitmap decodeFile = BitmapFactory.decodeFile(vUserInfo.iconPath);
                return decodeFile;
            }
        }
    }

    public boolean isGuestEnabled() {
        boolean z;
        synchronized (this.mPackagesLock) {
            z = this.mGuestEnabled;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setGuestEnabled(boolean r5) {
        /*
            r4 = this;
            java.lang.String r0 = "enable guest users"
            checkManageUsersPermission(r0)
            java.lang.Object r2 = r4.mPackagesLock
            monitor-enter(r2)
            boolean r0 = r4.mGuestEnabled     // Catch:{ all -> 0x0041 }
            if (r0 == r5) goto L_0x003f
            r4.mGuestEnabled = r5     // Catch:{ all -> 0x0041 }
            r0 = 0
            r1 = r0
        L_0x0010:
            android.util.SparseArray<com.lody.virtual.os.VUserInfo> r0 = r4.mUsers     // Catch:{ all -> 0x0041 }
            int r0 = r0.size()     // Catch:{ all -> 0x0041 }
            if (r1 >= r0) goto L_0x0037
            android.util.SparseArray<com.lody.virtual.os.VUserInfo> r0 = r4.mUsers     // Catch:{ all -> 0x0041 }
            java.lang.Object r0 = r0.valueAt(r1)     // Catch:{ all -> 0x0041 }
            com.lody.virtual.os.VUserInfo r0 = (com.lody.virtual.os.VUserInfo) r0     // Catch:{ all -> 0x0041 }
            boolean r3 = r0.partial     // Catch:{ all -> 0x0041 }
            if (r3 != 0) goto L_0x0033
            boolean r3 = r0.isGuest()     // Catch:{ all -> 0x0041 }
            if (r3 == 0) goto L_0x0033
            if (r5 != 0) goto L_0x0031
            int r0 = r0.id     // Catch:{ all -> 0x0041 }
            r4.removeUser(r0)     // Catch:{ all -> 0x0041 }
        L_0x0031:
            monitor-exit(r2)     // Catch:{ all -> 0x0041 }
        L_0x0032:
            return
        L_0x0033:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0010
        L_0x0037:
            if (r5 == 0) goto L_0x003f
            java.lang.String r0 = "Guest"
            r1 = 4
            r4.createUser(r0, r1)     // Catch:{ all -> 0x0041 }
        L_0x003f:
            monitor-exit(r2)     // Catch:{ all -> 0x0041 }
            goto L_0x0032
        L_0x0041:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0041 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VUserManagerService.setGuestEnabled(boolean):void");
    }

    public void wipeUser(int i) {
        checkManageUsersPermission("wipe user");
    }

    public void makeInitialized(int i) {
        checkManageUsersPermission("makeInitialized");
        synchronized (this.mPackagesLock) {
            VUserInfo vUserInfo = this.mUsers.get(i);
            if (vUserInfo == null || vUserInfo.partial) {
                VLog.w(LOG_TAG, "makeInitialized: unknown user #" + i, new Object[0]);
            }
            if ((vUserInfo.flags & 16) == 0) {
                vUserInfo.flags |= 16;
                writeUserLocked(vUserInfo);
            }
        }
    }

    private boolean isUserLimitReachedLocked() {
        return this.mUsers.size() >= VUserManager.getMaxSupportedUsers();
    }

    private void writeBitmapLocked(VUserInfo vUserInfo, Bitmap bitmap) {
        try {
            File file = new File(this.mUsersDir, Integer.toString(vUserInfo.id));
            File file2 = new File(file, USER_PHOTO_FILENAME);
            if (!file.exists()) {
                file.mkdir();
            }
            Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.PNG;
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            if (bitmap.compress(compressFormat, 100, fileOutputStream)) {
                vUserInfo.iconPath = file2.getAbsolutePath();
            }
            try {
                fileOutputStream.close();
            } catch (IOException e2) {
            }
        } catch (FileNotFoundException e3) {
            VLog.w(LOG_TAG, "Error setting photo for user ", e3);
        }
    }

    public int[] getUserIds() {
        int[] iArr;
        synchronized (this.mPackagesLock) {
            iArr = this.mUserIds;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public int[] getUserIdsLPr() {
        return this.mUserIds;
    }

    private void readUserList() {
        synchronized (this.mPackagesLock) {
            readUserListLocked();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        fallbackToSingleUserLocked();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00be, code lost:
        if (r0 != null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c5, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c6, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        fallbackToSingleUserLocked();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00e2, code lost:
        if (r0 != null) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00e9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00ea, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00f9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00fa, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x00fe, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00ff, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ba A[ExcHandler: IOException (e java.io.IOException), Splitter:B:4:0x0019] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00de A[ExcHandler: XmlPullParserException (e org.xmlpull.v1.XmlPullParserException), Splitter:B:4:0x0019] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f5 A[SYNTHETIC, Splitter:B:69:0x00f5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readUserListLocked() {
        /*
            r8 = this;
            r1 = 0
            r6 = 2
            r5 = 1
            r0 = 0
            r8.mGuestEnabled = r1
            java.io.File r1 = r8.mUserListFile
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x0012
            r8.fallbackToSingleUserLocked()
        L_0x0011:
            return
        L_0x0012:
            com.lody.virtual.helper.utils.AtomicFile r1 = new com.lody.virtual.helper.utils.AtomicFile
            java.io.File r2 = r8.mUserListFile
            r1.<init>(r2)
            java.io.FileInputStream r0 = r1.openRead()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de, all -> 0x00ef }
            org.xmlpull.v1.XmlPullParser r1 = android.util.Xml.newPullParser()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            r2 = 0
            r1.setInput(r0, r2)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
        L_0x0025:
            int r2 = r1.next()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == r6) goto L_0x002d
            if (r2 != r5) goto L_0x0025
        L_0x002d:
            if (r2 == r6) goto L_0x0047
            java.lang.String r1 = "VUserManagerService"
            java.lang.String r2 = "Unable to read user list"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            com.lody.virtual.helper.utils.VLog.e(r1, r2, r3)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            r8.fallbackToSingleUserLocked()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r0 == 0) goto L_0x0011
            r0.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x0011
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0011
        L_0x0047:
            r2 = -1
            r8.mNextSerialNumber = r2     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            java.lang.String r2 = r1.getName()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            java.lang.String r3 = "users"
            boolean r2 = r2.equals(r3)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == 0) goto L_0x0074
            r2 = 0
            java.lang.String r3 = "nextSerialNumber"
            java.lang.String r2 = r1.getAttributeValue(r2, r3)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == 0) goto L_0x0065
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            r8.mNextSerialNumber = r2     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
        L_0x0065:
            r2 = 0
            java.lang.String r3 = "version"
            java.lang.String r2 = r1.getAttributeValue(r2, r3)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == 0) goto L_0x0074
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            r8.mUserVersion = r2     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
        L_0x0074:
            int r2 = r1.next()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == r5) goto L_0x00cb
            if (r2 != r6) goto L_0x0074
            java.lang.String r2 = r1.getName()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            java.lang.String r3 = "user"
            boolean r2 = r2.equals(r3)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == 0) goto L_0x0074
            r2 = 0
            java.lang.String r3 = "id"
            java.lang.String r2 = r1.getAttributeValue(r2, r3)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            com.lody.virtual.os.VUserInfo r2 = r8.readUser(r2)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r2 == 0) goto L_0x0074
            android.util.SparseArray<com.lody.virtual.os.VUserInfo> r3 = r8.mUsers     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            int r4 = r2.id     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            r3.put(r4, r2)     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            boolean r3 = r2.isGuest()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r3 == 0) goto L_0x00a9
            r3 = 1
            r8.mGuestEnabled = r3     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
        L_0x00a9:
            int r3 = r8.mNextSerialNumber     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r3 < 0) goto L_0x00b3
            int r3 = r8.mNextSerialNumber     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            int r4 = r2.id     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r3 > r4) goto L_0x0074
        L_0x00b3:
            int r2 = r2.id     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            int r2 = r2 + 1
            r8.mNextSerialNumber = r2     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            goto L_0x0074
        L_0x00ba:
            r1 = move-exception
            r8.fallbackToSingleUserLocked()     // Catch:{ all -> 0x00fe }
            if (r0 == 0) goto L_0x0011
            r0.close()     // Catch:{ IOException -> 0x00c5 }
            goto L_0x0011
        L_0x00c5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0011
        L_0x00cb:
            r8.updateUserIdsLocked()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            r8.upgradeIfNecessary()     // Catch:{ IOException -> 0x00ba, XmlPullParserException -> 0x00de }
            if (r0 == 0) goto L_0x0011
            r0.close()     // Catch:{ IOException -> 0x00d8 }
            goto L_0x0011
        L_0x00d8:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0011
        L_0x00de:
            r1 = move-exception
            r8.fallbackToSingleUserLocked()     // Catch:{ all -> 0x00fe }
            if (r0 == 0) goto L_0x0011
            r0.close()     // Catch:{ IOException -> 0x00e9 }
            goto L_0x0011
        L_0x00e9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0011
        L_0x00ef:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f8:
            throw r0
        L_0x00f9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00f8
        L_0x00fe:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00f3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VUserManagerService.readUserListLocked():void");
    }

    private void upgradeIfNecessary() {
        int i = this.mUserVersion;
        if (i < 1) {
            VUserInfo vUserInfo = this.mUsers.get(0);
            if ("Primary".equals(vUserInfo.name)) {
                vUserInfo.name = "Admin";
                writeUserLocked(vUserInfo);
            }
            i = 1;
        }
        if (i < 1) {
            VLog.w(LOG_TAG, "User version " + this.mUserVersion + " didn't upgrade as expected to " + 1, new Object[0]);
            return;
        }
        this.mUserVersion = i;
        writeUserListLocked();
    }

    private void fallbackToSingleUserLocked() {
        VUserInfo vUserInfo = new VUserInfo(0, this.mContext.getResources().getString(R.string.gg), null, 19);
        this.mUsers.put(0, vUserInfo);
        this.mNextSerialNumber = 1;
        updateUserIdsLocked();
        writeUserListLocked();
        writeUserLocked(vUserInfo);
    }

    private void writeUserLocked(VUserInfo vUserInfo) {
        AtomicFile atomicFile = new AtomicFile(new File(this.mUsersDir, vUserInfo.id + ".xml"));
        try {
            FileOutputStream startWrite = atomicFile.startWrite();
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(startWrite);
            FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
            fastXmlSerializer.setOutput(bufferedOutputStream, "utf-8");
            fastXmlSerializer.startDocument(null, true);
            fastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            fastXmlSerializer.startTag(null, "user");
            fastXmlSerializer.attribute(null, ATTR_ID, Integer.toString(vUserInfo.id));
            fastXmlSerializer.attribute(null, ATTR_SERIAL_NO, Integer.toString(vUserInfo.serialNumber));
            fastXmlSerializer.attribute(null, ATTR_FLAGS, Integer.toString(vUserInfo.flags));
            fastXmlSerializer.attribute(null, ATTR_CREATION_TIME, Long.toString(vUserInfo.creationTime));
            fastXmlSerializer.attribute(null, ATTR_LAST_LOGGED_IN_TIME, Long.toString(vUserInfo.lastLoggedInTime));
            if (vUserInfo.iconPath != null) {
                fastXmlSerializer.attribute(null, ATTR_ICON_PATH, vUserInfo.iconPath);
            }
            if (vUserInfo.partial) {
                fastXmlSerializer.attribute(null, ATTR_PARTIAL, "true");
            }
            fastXmlSerializer.startTag(null, TAG_NAME);
            fastXmlSerializer.text(vUserInfo.name);
            fastXmlSerializer.endTag(null, TAG_NAME);
            fastXmlSerializer.endTag(null, "user");
            fastXmlSerializer.endDocument();
            atomicFile.finishWrite(startWrite);
        } catch (Exception e2) {
            VLog.e(LOG_TAG, "Error writing user info " + vUserInfo.id + ShellUtils.COMMAND_LINE_END + e2, new Object[0]);
            atomicFile.failWrite(null);
        }
    }

    private void writeUserListLocked() {
        FileOutputStream fileOutputStream = null;
        AtomicFile atomicFile = new AtomicFile(this.mUserListFile);
        try {
            FileOutputStream startWrite = atomicFile.startWrite();
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(startWrite);
                FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
                fastXmlSerializer.setOutput(bufferedOutputStream, "utf-8");
                fastXmlSerializer.startDocument(null, true);
                fastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                fastXmlSerializer.startTag(null, TAG_USERS);
                fastXmlSerializer.attribute(null, ATTR_NEXT_SERIAL_NO, Integer.toString(this.mNextSerialNumber));
                fastXmlSerializer.attribute(null, ATTR_USER_VERSION, Integer.toString(this.mUserVersion));
                for (int i = 0; i < this.mUsers.size(); i++) {
                    fastXmlSerializer.startTag(null, "user");
                    fastXmlSerializer.attribute(null, ATTR_ID, Integer.toString(this.mUsers.valueAt(i).id));
                    fastXmlSerializer.endTag(null, "user");
                }
                fastXmlSerializer.endTag(null, TAG_USERS);
                fastXmlSerializer.endDocument();
                atomicFile.finishWrite(startWrite);
            } catch (Exception e2) {
                fileOutputStream = startWrite;
                atomicFile.failWrite(fileOutputStream);
                VLog.e(LOG_TAG, "Error writing user list", new Object[0]);
            }
        } catch (Exception e3) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0121 A[SYNTHETIC, Splitter:B:56:0x0121] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x012f A[SYNTHETIC, Splitter:B:62:0x012f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.lody.virtual.os.VUserInfo readUser(int r18) {
        /*
            r17 = this;
            r11 = 0
            r10 = 0
            r5 = 0
            r8 = 0
            r6 = 0
            r4 = 0
            r2 = 0
            com.lody.virtual.helper.utils.AtomicFile r3 = new com.lody.virtual.helper.utils.AtomicFile     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.io.File r12 = new java.io.File     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            r0 = r17
            java.io.File r13 = r0.mUsersDir     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            r14.<init>()     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.lang.String r15 = java.lang.Integer.toString(r18)     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.lang.String r15 = ".xml"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.lang.String r14 = r14.toString()     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            r12.<init>(r13, r14)     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            r3.<init>(r12)     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            java.io.FileInputStream r3 = r3.openRead()     // Catch:{ IOException -> 0x0114, XmlPullParserException -> 0x011d, all -> 0x0127 }
            org.xmlpull.v1.XmlPullParser r13 = android.util.Xml.newPullParser()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2 = 0
            r13.setInput(r3, r2)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
        L_0x003a:
            int r2 = r13.next()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r12 = 2
            if (r2 == r12) goto L_0x0044
            r12 = 1
            if (r2 != r12) goto L_0x003a
        L_0x0044:
            r12 = 2
            if (r2 == r12) goto L_0x006b
            java.lang.String r2 = "VUserManagerService"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r4.<init>()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r5 = "Unable to read user "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r0 = r18
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            com.lody.virtual.helper.utils.VLog.e(r2, r4, r5)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2 = 0
            if (r3 == 0) goto L_0x006a
            r3.close()     // Catch:{ IOException -> 0x0133 }
        L_0x006a:
            return r2
        L_0x006b:
            java.lang.String r2 = r13.getName()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r12 = "user"
            boolean r2 = r2.equals(r12)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            if (r2 == 0) goto L_0x0145
            java.lang.String r2 = "id"
            r5 = -1
            r0 = r17
            int r2 = r0.readIntAttribute(r13, r2, r5)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r0 = r18
            if (r2 == r0) goto L_0x0097
            java.lang.String r2 = "VUserManagerService"
            java.lang.String r4 = "User id does not match the file name"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            com.lody.virtual.helper.utils.VLog.e(r2, r4, r5)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2 = 0
            if (r3 == 0) goto L_0x006a
            r3.close()     // Catch:{ IOException -> 0x0095 }
            goto L_0x006a
        L_0x0095:
            r3 = move-exception
            goto L_0x006a
        L_0x0097:
            java.lang.String r2 = "serialNumber"
            r0 = r17
            r1 = r18
            int r11 = r0.readIntAttribute(r13, r2, r1)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r2 = "flags"
            r5 = 0
            r0 = r17
            int r12 = r0.readIntAttribute(r13, r2, r5)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2 = 0
            java.lang.String r5 = "icon"
            java.lang.String r5 = r13.getAttributeValue(r2, r5)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r2 = "created"
            r6 = 0
            r0 = r17
            long r8 = r0.readLongAttribute(r13, r2, r6)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r2 = "lastLoggedIn"
            r6 = 0
            r0 = r17
            long r6 = r0.readLongAttribute(r13, r2, r6)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2 = 0
            java.lang.String r14 = "partial"
            java.lang.String r2 = r13.getAttributeValue(r2, r14)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r14 = "true"
            boolean r2 = r14.equals(r2)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            if (r2 == 0) goto L_0x0143
            r2 = 1
        L_0x00d5:
            int r4 = r13.next()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r14 = 2
            if (r4 == r14) goto L_0x00df
            r14 = 1
            if (r4 != r14) goto L_0x00d5
        L_0x00df:
            r14 = 2
            if (r4 != r14) goto L_0x0141
            java.lang.String r4 = r13.getName()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            java.lang.String r14 = "name"
            boolean r4 = r4.equals(r14)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            if (r4 == 0) goto L_0x0141
            int r4 = r13.next()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r14 = 4
            if (r4 != r14) goto L_0x0141
            java.lang.String r4 = r13.getText()     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r10 = r4
            r4 = r2
        L_0x00fb:
            com.lody.virtual.os.VUserInfo r2 = new com.lody.virtual.os.VUserInfo     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r0 = r18
            r2.<init>(r0, r10, r5, r12)     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2.serialNumber = r11     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2.creationTime = r8     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2.lastLoggedInTime = r6     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            r2.partial = r4     // Catch:{ IOException -> 0x013e, XmlPullParserException -> 0x013c, all -> 0x013a }
            if (r3 == 0) goto L_0x006a
            r3.close()     // Catch:{ IOException -> 0x0111 }
            goto L_0x006a
        L_0x0111:
            r3 = move-exception
            goto L_0x006a
        L_0x0114:
            r3 = move-exception
        L_0x0115:
            if (r2 == 0) goto L_0x011a
            r2.close()     // Catch:{ IOException -> 0x0136 }
        L_0x011a:
            r2 = 0
            goto L_0x006a
        L_0x011d:
            r3 = move-exception
            r3 = r2
        L_0x011f:
            if (r3 == 0) goto L_0x011a
            r3.close()     // Catch:{ IOException -> 0x0125 }
            goto L_0x011a
        L_0x0125:
            r2 = move-exception
            goto L_0x011a
        L_0x0127:
            r3 = move-exception
            r16 = r3
            r3 = r2
            r2 = r16
        L_0x012d:
            if (r3 == 0) goto L_0x0132
            r3.close()     // Catch:{ IOException -> 0x0138 }
        L_0x0132:
            throw r2
        L_0x0133:
            r3 = move-exception
            goto L_0x006a
        L_0x0136:
            r2 = move-exception
            goto L_0x011a
        L_0x0138:
            r3 = move-exception
            goto L_0x0132
        L_0x013a:
            r2 = move-exception
            goto L_0x012d
        L_0x013c:
            r2 = move-exception
            goto L_0x011f
        L_0x013e:
            r2 = move-exception
            r2 = r3
            goto L_0x0115
        L_0x0141:
            r4 = r2
            goto L_0x00fb
        L_0x0143:
            r2 = r4
            goto L_0x00d5
        L_0x0145:
            r12 = r11
            r11 = r18
            goto L_0x00fb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VUserManagerService.readUser(int):com.lody.virtual.os.VUserInfo");
    }

    private int readIntAttribute(XmlPullParser xmlPullParser, String str, int i) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return i;
        }
        try {
            return Integer.parseInt(attributeValue);
        } catch (NumberFormatException e2) {
            return i;
        }
    }

    private long readLongAttribute(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return j;
        }
        try {
            return Long.parseLong(attributeValue);
        } catch (NumberFormatException e2) {
            return j;
        }
    }

    public VUserInfo createUser(String str, int i) {
        VUserInfo vUserInfo = null;
        checkManageUsersPermission("Only the system can create users");
        long clearCallingIdentity = Binder.clearCallingIdentity();
        try {
            synchronized (this.mInstallLock) {
                synchronized (this.mPackagesLock) {
                    if (!isUserLimitReachedLocked()) {
                        int nextAvailableIdLocked = getNextAvailableIdLocked();
                        vUserInfo = new VUserInfo(nextAvailableIdLocked, str, null, i);
                        File file = new File(this.mBaseUserPath, Integer.toString(nextAvailableIdLocked));
                        int i2 = this.mNextSerialNumber;
                        this.mNextSerialNumber = i2 + 1;
                        vUserInfo.serialNumber = i2;
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis <= EPOCH_PLUS_30_YEARS) {
                            currentTimeMillis = 0;
                        }
                        vUserInfo.creationTime = currentTimeMillis;
                        vUserInfo.partial = true;
                        VEnvironment.getUserSystemDirectory(vUserInfo.id).mkdirs();
                        this.mUsers.put(nextAvailableIdLocked, vUserInfo);
                        writeUserListLocked();
                        writeUserLocked(vUserInfo);
                        this.mPm.createNewUser(nextAvailableIdLocked, file);
                        vUserInfo.partial = false;
                        writeUserLocked(vUserInfo);
                        updateUserIdsLocked();
                        Intent intent = new Intent(Constants.ACTION_USER_ADDED);
                        intent.putExtra(Constants.EXTRA_USER_HANDLE, vUserInfo.id);
                        VActivityManagerService.get().sendBroadcastAsUser(intent, VUserHandle.ALL, null);
                        Binder.restoreCallingIdentity(clearCallingIdentity);
                    }
                }
            }
            return vUserInfo;
        } finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        if (com.lody.virtual.server.am.VActivityManagerService.get().stopUser(r7, new com.lody.virtual.server.pm.VUserManagerService.AnonymousClass1(r6)) != 0) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean removeUser(int r7) {
        /*
            r6 = this;
            r1 = 1
            r2 = 0
            java.lang.String r0 = "Only the system can remove users"
            checkManageUsersPermission(r0)
            java.lang.Object r3 = r6.mPackagesLock
            monitor-enter(r3)
            android.util.SparseArray<com.lody.virtual.os.VUserInfo> r0 = r6.mUsers     // Catch:{ all -> 0x003a }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ all -> 0x003a }
            com.lody.virtual.os.VUserInfo r0 = (com.lody.virtual.os.VUserInfo) r0     // Catch:{ all -> 0x003a }
            if (r7 == 0) goto L_0x0016
            if (r0 != 0) goto L_0x0019
        L_0x0016:
            monitor-exit(r3)     // Catch:{ all -> 0x003a }
            r0 = r2
        L_0x0018:
            return r0
        L_0x0019:
            java.util.HashSet<java.lang.Integer> r4 = r6.mRemovingUserIds     // Catch:{ all -> 0x003a }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x003a }
            r4.add(r5)     // Catch:{ all -> 0x003a }
            r4 = 1
            r0.partial = r4     // Catch:{ all -> 0x003a }
            r6.writeUserLocked(r0)     // Catch:{ all -> 0x003a }
            monitor-exit(r3)     // Catch:{ all -> 0x003a }
            com.lody.virtual.server.am.VActivityManagerService r0 = com.lody.virtual.server.am.VActivityManagerService.get()
            com.lody.virtual.server.pm.VUserManagerService$1 r3 = new com.lody.virtual.server.pm.VUserManagerService$1
            r3.<init>()
            int r0 = r0.stopUser(r7, r3)
            if (r0 != 0) goto L_0x003d
            r0 = r1
            goto L_0x0018
        L_0x003a:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x003a }
            throw r0
        L_0x003d:
            r0 = r2
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VUserManagerService.removeUser(int):boolean");
    }

    /* access modifiers changed from: package-private */
    public void finishRemoveUser(final int i) {
        long clearCallingIdentity = Binder.clearCallingIdentity();
        try {
            Intent intent = new Intent(Constants.ACTION_USER_REMOVED);
            intent.putExtra(Constants.EXTRA_USER_HANDLE, i);
            VActivityManagerService.get().sendOrderedBroadcastAsUser(intent, VUserHandle.ALL, null, new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    new Thread() {
                        public void run() {
                            synchronized (VUserManagerService.this.mInstallLock) {
                                synchronized (VUserManagerService.this.mPackagesLock) {
                                    VUserManagerService.this.removeUserStateLocked(i);
                                }
                            }
                        }
                    }.start();
                }
            }, null, -1, null, null);
        } finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }

    /* access modifiers changed from: private */
    public void removeUserStateLocked(int i) {
        this.mPm.cleanUpUser(i);
        this.mUsers.remove(i);
        this.mRemovingUserIds.remove(Integer.valueOf(i));
        new AtomicFile(new File(this.mUsersDir, i + ".xml")).delete();
        writeUserListLocked();
        updateUserIdsLocked();
        removeDirectoryRecursive(VEnvironment.getUserSystemDirectory(i));
    }

    private void removeDirectoryRecursive(File file) {
        if (file.isDirectory()) {
            for (String file2 : file.list()) {
                removeDirectoryRecursive(new File(file, file2));
            }
        }
        file.delete();
    }

    public int getUserSerialNumber(int i) {
        int i2;
        synchronized (this.mPackagesLock) {
            if (!exists(i)) {
                i2 = -1;
            } else {
                i2 = getUserInfoLocked(i).serialNumber;
            }
        }
        return i2;
    }

    public int getUserHandle(int i) {
        int i2;
        synchronized (this.mPackagesLock) {
            int[] iArr = this.mUserIds;
            int length = iArr.length;
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    i2 = -1;
                    break;
                }
                i2 = iArr[i3];
                if (getUserInfoLocked(i2).serialNumber == i) {
                    break;
                }
                i3++;
            }
        }
        return i2;
    }

    private void updateUserIdsLocked() {
        int i;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < this.mUsers.size(); i4++) {
            if (!this.mUsers.valueAt(i4).partial) {
                i3++;
            }
        }
        int[] iArr = new int[i3];
        int i5 = 0;
        while (i2 < this.mUsers.size()) {
            if (!this.mUsers.valueAt(i2).partial) {
                i = i5 + 1;
                iArr[i5] = this.mUsers.keyAt(i2);
            } else {
                i = i5;
            }
            i2++;
            i5 = i;
        }
        this.mUserIds = iArr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void userForeground(int r7) {
        /*
            r6 = this;
            java.lang.Object r1 = r6.mPackagesLock
            monitor-enter(r1)
            android.util.SparseArray<com.lody.virtual.os.VUserInfo> r0 = r6.mUsers     // Catch:{ all -> 0x0042 }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ all -> 0x0042 }
            com.lody.virtual.os.VUserInfo r0 = (com.lody.virtual.os.VUserInfo) r0     // Catch:{ all -> 0x0042 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0015
            boolean r4 = r0.partial     // Catch:{ all -> 0x0042 }
            if (r4 == 0) goto L_0x0032
        L_0x0015:
            java.lang.String r0 = "VUserManagerService"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0042 }
            r2.<init>()     // Catch:{ all -> 0x0042 }
            java.lang.String r3 = "userForeground: unknown user #"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0042 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x0042 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0042 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0042 }
            com.lody.virtual.helper.utils.VLog.w(r0, r2, r3)     // Catch:{ all -> 0x0042 }
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
        L_0x0031:
            return
        L_0x0032:
            r4 = 946080000000(0xdc46c32800, double:4.674256262175E-312)
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0040
            r0.lastLoggedInTime = r2     // Catch:{ all -> 0x0042 }
            r6.writeUserLocked(r0)     // Catch:{ all -> 0x0042 }
        L_0x0040:
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            goto L_0x0031
        L_0x0042:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0042 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VUserManagerService.userForeground(int):void");
    }

    private int getNextAvailableIdLocked() {
        int i;
        synchronized (this.mPackagesLock) {
            i = this.mNextUserId;
            while (i < Integer.MAX_VALUE && (this.mUsers.indexOfKey(i) >= 0 || this.mRemovingUserIds.contains(Integer.valueOf(i)))) {
                i++;
            }
            this.mNextUserId = i + 1;
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }
}
