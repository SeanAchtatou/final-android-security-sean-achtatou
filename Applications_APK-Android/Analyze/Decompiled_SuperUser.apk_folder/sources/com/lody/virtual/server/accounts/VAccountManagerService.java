package com.lody.virtual.server.accounts;

import android.accounts.Account;
import android.accounts.AuthenticatorDescription;
import android.accounts.a;
import android.accounts.b;
import android.accounts.c;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.Xml;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.compat.AccountManagerCompat;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VBinder;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.server.IAccountManager;
import com.lody.virtual.server.am.VActivityManagerService;
import com.lody.virtual.server.pm.VPackageManagerService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import mirror.com.android.internal.R_Hide;

public class VAccountManagerService extends IAccountManager.Stub {
    private static final long CHECK_IN_TIME = 43200000;
    /* access modifiers changed from: private */
    public static final String TAG = VAccountManagerService.class.getSimpleName();
    private static final AtomicReference<VAccountManagerService> sInstance = new AtomicReference<>();
    /* access modifiers changed from: private */
    public final SparseArray<List<VAccount>> accountsByUserId = new SparseArray<>();
    /* access modifiers changed from: private */
    public final LinkedList<AuthTokenRecord> authTokenRecords = new LinkedList<>();
    private final AuthenticatorCache cache = new AuthenticatorCache();
    private long lastAccountChangeTime = 0;
    /* access modifiers changed from: private */
    public Context mContext = VirtualCore.get().getContext();
    /* access modifiers changed from: private */
    public final LinkedHashMap<String, Session> mSessions = new LinkedHashMap<>();

    public static VAccountManagerService get() {
        return sInstance.get();
    }

    public static void systemReady() {
        VAccountManagerService vAccountManagerService = new VAccountManagerService();
        vAccountManagerService.readAllAccounts();
        sInstance.set(vAccountManagerService);
    }

    private static AuthenticatorDescription parseAuthenticatorDescription(Resources resources, String str, AttributeSet attributeSet) {
        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, R_Hide.styleable.AccountAuthenticator.get());
        try {
            String string = obtainAttributes.getString(R_Hide.styleable.AccountAuthenticator_accountType.get());
            int resourceId = obtainAttributes.getResourceId(R_Hide.styleable.AccountAuthenticator_label.get(), 0);
            int resourceId2 = obtainAttributes.getResourceId(R_Hide.styleable.AccountAuthenticator_icon.get(), 0);
            int resourceId3 = obtainAttributes.getResourceId(R_Hide.styleable.AccountAuthenticator_smallIcon.get(), 0);
            int resourceId4 = obtainAttributes.getResourceId(R_Hide.styleable.AccountAuthenticator_accountPreferences.get(), 0);
            boolean z = obtainAttributes.getBoolean(R_Hide.styleable.AccountAuthenticator_customTokens.get(), false);
            if (TextUtils.isEmpty(string)) {
                return null;
            }
            AuthenticatorDescription authenticatorDescription = new AuthenticatorDescription(string, str, resourceId, resourceId2, resourceId3, resourceId4, z);
            obtainAttributes.recycle();
            return authenticatorDescription;
        } finally {
            obtainAttributes.recycle();
        }
    }

    public AuthenticatorDescription[] getAuthenticatorTypes(int i) {
        AuthenticatorDescription[] authenticatorDescriptionArr;
        synchronized (this.cache) {
            authenticatorDescriptionArr = new AuthenticatorDescription[this.cache.authenticators.size()];
            int i2 = 0;
            for (AuthenticatorInfo authenticatorInfo : this.cache.authenticators.values()) {
                authenticatorDescriptionArr[i2] = authenticatorInfo.desc;
                i2++;
            }
        }
        return authenticatorDescriptionArr;
    }

    public void getAccountsByFeatures(int i, c cVar, String str, String[] strArr) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (str == null) {
            throw new IllegalArgumentException("accountType is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(str);
            if (authenticatorInfo == null) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArray("accounts", new Account[0]);
                try {
                    cVar.onResult(bundle);
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else if (strArr == null || strArr.length == 0) {
                Bundle bundle2 = new Bundle();
                bundle2.putParcelableArray("accounts", getAccounts(i, str));
                try {
                    cVar.onResult(bundle2);
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
            } else {
                new GetAccountsByTypeAndFeatureSession(cVar, i, authenticatorInfo, strArr).bind();
            }
        }
    }

    public final String getPreviousName(int i, Account account) {
        String str;
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        }
        synchronized (this.accountsByUserId) {
            str = null;
            VAccount account2 = getAccount(i, account);
            if (account2 != null) {
                str = account2.previousName;
            }
        }
        return str;
    }

    public Account[] getAccounts(int i, String str) {
        List<Account> accountList = getAccountList(i, str);
        return (Account[]) accountList.toArray(new Account[accountList.size()]);
    }

    private List<Account> getAccountList(int i, String str) {
        ArrayList arrayList;
        synchronized (this.accountsByUserId) {
            arrayList = new ArrayList();
            List<VAccount> list = this.accountsByUserId.get(i);
            if (list != null) {
                for (VAccount vAccount : list) {
                    if (str == null || vAccount.type.equals(str)) {
                        arrayList.add(new Account(vAccount.name, vAccount.type));
                    }
                }
            }
        }
        return arrayList;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        try {
            return super.onTransact(i, parcel, parcel2, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public final void getAuthToken(int i, c cVar, Account account, String str, boolean z, boolean z2, Bundle bundle) {
        String customAuthToken;
        VAccount account2;
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (account == null) {
            try {
                VLog.w(TAG, "getAuthToken called with null account", new Object[0]);
                cVar.onError(7, "account is null");
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        } else if (str == null) {
            VLog.w(TAG, "getAuthToken called with null authTokenType", new Object[0]);
            cVar.onError(7, "authTokenType is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(account.type);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
            } else {
                final String string = bundle.getString(AccountManagerCompat.KEY_ANDROID_PACKAGE_NAME);
                final boolean z3 = authenticatorInfo.desc.customTokens;
                bundle.putInt("callerUid", VBinder.getCallingUid());
                bundle.putInt("callerPid", Binder.getCallingPid());
                if (z) {
                    bundle.putBoolean(AccountManagerCompat.KEY_NOTIFY_ON_FAILURE, true);
                }
                if (!z3) {
                    synchronized (this.accountsByUserId) {
                        account2 = getAccount(i, account);
                    }
                    String str2 = account2 != null ? account2.authTokens.get(str) : null;
                    if (str2 != null) {
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("authtoken", str2);
                        bundle2.putString("authAccount", account.name);
                        bundle2.putString("accountType", account.type);
                        onResult(cVar, bundle2);
                        return;
                    }
                }
                if (!z3 || (customAuthToken = getCustomAuthToken(i, account, str, string)) == null) {
                    final Account account3 = account;
                    final String str3 = str;
                    final Bundle bundle3 = bundle;
                    final boolean z4 = z;
                    final int i2 = i;
                    new Session(cVar, i, authenticatorInfo, z2, false, account.name) {
                        /* access modifiers changed from: protected */
                        public String toDebugString(long j) {
                            return super.toDebugString(j) + ", getAuthToken" + ", " + account3 + ", authTokenType " + str3 + ", loginOptions " + bundle3 + ", notifyOnAuthFailure " + z4;
                        }

                        public void run() {
                            this.mAuthenticator.a(this, account3, str3, bundle3);
                        }

                        public void onResult(Bundle bundle) {
                            if (bundle != null) {
                                String string = bundle.getString("authtoken");
                                if (string != null) {
                                    String string2 = bundle.getString("authAccount");
                                    String string3 = bundle.getString("accountType");
                                    if (TextUtils.isEmpty(string3) || TextUtils.isEmpty(string2)) {
                                        onError(5, "the type and name should not be empty");
                                        return;
                                    }
                                    if (!z3) {
                                        synchronized (VAccountManagerService.this.accountsByUserId) {
                                            if (VAccountManagerService.this.getAccount(i2, string2, string3) == null) {
                                                Object obj = (List) VAccountManagerService.this.accountsByUserId.get(i2);
                                                if (obj == null) {
                                                    obj = new ArrayList();
                                                    VAccountManagerService.this.accountsByUserId.put(i2, obj);
                                                }
                                                obj.add(new VAccount(i2, new Account(string2, string3)));
                                                VAccountManagerService.this.saveAllAccounts();
                                            }
                                        }
                                    }
                                    long j = bundle.getLong(AccountManagerCompat.KEY_CUSTOM_TOKEN_EXPIRY, 0);
                                    if (z3 && j > System.currentTimeMillis()) {
                                        AuthTokenRecord authTokenRecord = new AuthTokenRecord(i2, account3, str3, string, string, j);
                                        synchronized (VAccountManagerService.this.authTokenRecords) {
                                            VAccountManagerService.this.authTokenRecords.remove(authTokenRecord);
                                            VAccountManagerService.this.authTokenRecords.add(authTokenRecord);
                                        }
                                    }
                                }
                                if (((Intent) bundle.getParcelable("intent")) == null || !z4 || !z3) {
                                }
                            }
                            super.onResult(bundle);
                        }
                    }.bind();
                    return;
                }
                Bundle bundle4 = new Bundle();
                bundle4.putString("authtoken", customAuthToken);
                bundle4.putString("authAccount", account.name);
                bundle4.putString("accountType", account.type);
                onResult(cVar, bundle4);
            }
        }
    }

    public void setPassword(int i, Account account, String str) {
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        }
        setPasswordInternal(i, account, str);
    }

    private void setPasswordInternal(int i, Account account, String str) {
        synchronized (this.accountsByUserId) {
            VAccount account2 = getAccount(i, account);
            if (account2 != null) {
                account2.password = str;
                account2.authTokens.clear();
                saveAllAccounts();
                synchronized (this.authTokenRecords) {
                    Iterator<AuthTokenRecord> it = this.authTokenRecords.iterator();
                    while (it.hasNext()) {
                        AuthTokenRecord next = it.next();
                        if (next.userId == i && next.account.equals(account)) {
                            it.remove();
                        }
                    }
                }
                sendAccountsChangedBroadcast(i);
            }
        }
    }

    public void setAuthToken(int i, Account account, String str, String str2) {
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else if (str == null) {
            throw new IllegalArgumentException("authTokenType is null");
        } else {
            synchronized (this.accountsByUserId) {
                VAccount account2 = getAccount(i, account);
                if (account2 != null) {
                    account2.authTokens.put(str, str2);
                    saveAllAccounts();
                }
            }
        }
    }

    public void setUserData(int i, Account account, String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("key is null");
        } else if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else {
            VAccount account2 = getAccount(i, account);
            if (account2 != null) {
                synchronized (this.accountsByUserId) {
                    account2.userDatas.put(str, str2);
                    saveAllAccounts();
                }
            }
        }
    }

    public void hasFeatures(int i, c cVar, Account account, String[] strArr) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else if (strArr == null) {
            throw new IllegalArgumentException("features is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(account.type);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final Account account2 = account;
                final String[] strArr2 = strArr;
                new Session(cVar, i, authenticatorInfo, false, true, account.name) {
                    public void run() {
                        try {
                            this.mAuthenticator.a(this, account2, strArr2);
                        } catch (RemoteException e2) {
                            onError(1, "remote exception");
                        }
                    }

                    public void onResult(Bundle bundle) {
                        c responseAndClose = getResponseAndClose();
                        if (responseAndClose == null) {
                            return;
                        }
                        if (bundle == null) {
                            try {
                                responseAndClose.onError(5, "null bundle");
                            } catch (RemoteException e2) {
                                Log.v(VAccountManagerService.TAG, "failure while notifying response", e2);
                            }
                        } else {
                            Log.v(VAccountManagerService.TAG, getClass().getSimpleName() + " calling onResult() on response " + responseAndClose);
                            Bundle bundle2 = new Bundle();
                            bundle2.putBoolean("booleanResult", bundle.getBoolean("booleanResult", false));
                            responseAndClose.onResult(bundle2);
                        }
                    }
                }.bind();
            }
        }
    }

    public void updateCredentials(int i, c cVar, Account account, String str, boolean z, Bundle bundle) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else if (str == null) {
            throw new IllegalArgumentException("authTokenType is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(account.type);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final Account account2 = account;
                final String str2 = str;
                final Bundle bundle2 = bundle;
                new Session(cVar, i, authenticatorInfo, z, false, account.name) {
                    public void run() {
                        this.mAuthenticator.b(this, account2, str2, bundle2);
                    }

                    /* access modifiers changed from: protected */
                    public String toDebugString(long j) {
                        if (bundle2 != null) {
                            bundle2.keySet();
                        }
                        return super.toDebugString(j) + ", updateCredentials" + ", " + account2 + ", authTokenType " + str2 + ", loginOptions " + bundle2;
                    }
                }.bind();
            }
        }
    }

    public String getPassword(int i, Account account) {
        String str;
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        }
        synchronized (this.accountsByUserId) {
            VAccount account2 = getAccount(i, account);
            if (account2 != null) {
                str = account2.password;
            } else {
                str = null;
            }
        }
        return str;
    }

    public String getUserData(int i, Account account, String str) {
        String str2;
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else if (str == null) {
            throw new IllegalArgumentException("key is null");
        } else {
            synchronized (this.accountsByUserId) {
                VAccount account2 = getAccount(i, account);
                if (account2 != null) {
                    str2 = account2.userDatas.get(str);
                } else {
                    str2 = null;
                }
            }
            return str2;
        }
    }

    public void editProperties(int i, c cVar, String str, boolean z) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (str == null) {
            throw new IllegalArgumentException("accountType is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(str);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final String str2 = str;
                new Session(cVar, i, authenticatorInfo, z, true, null) {
                    public void run() {
                        this.mAuthenticator.b(this, this.mAuthenticatorInfo.desc.type);
                    }

                    /* access modifiers changed from: protected */
                    public String toDebugString(long j) {
                        return super.toDebugString(j) + ", editProperties" + ", accountType " + str2;
                    }
                }.bind();
            }
        }
    }

    public void getAuthTokenLabel(int i, c cVar, String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("accountType is null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("authTokenType is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(str);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final String str3 = str2;
                new Session(cVar, i, authenticatorInfo, false, false, null) {
                    public void run() {
                        this.mAuthenticator.a(this, str3);
                    }

                    public void onResult(Bundle bundle) {
                        if (bundle != null) {
                            String string = bundle.getString("authTokenLabelKey");
                            Bundle bundle2 = new Bundle();
                            bundle2.putString("authTokenLabelKey", string);
                            super.onResult(bundle2);
                            return;
                        }
                        super.onResult(null);
                    }
                }.bind();
            }
        }
    }

    public void confirmCredentials(int i, c cVar, Account account, Bundle bundle, boolean z) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(account.type);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final Account account2 = account;
                final Bundle bundle2 = bundle;
                new Session(cVar, i, authenticatorInfo, z, true, account.name, true, true) {
                    public void run() {
                        this.mAuthenticator.a(this, account2, bundle2);
                    }
                }.bind();
            }
        }
    }

    public void addAccount(int i, c cVar, String str, String str2, String[] strArr, boolean z, Bundle bundle) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (str == null) {
            throw new IllegalArgumentException("accountType is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(str);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final String str3 = str2;
                final String[] strArr2 = strArr;
                final Bundle bundle2 = bundle;
                final String str4 = str;
                new Session(cVar, i, authenticatorInfo, z, true, null, false, true) {
                    public void run() {
                        this.mAuthenticator.a(this, this.mAuthenticatorInfo.desc.type, str3, strArr2, bundle2);
                    }

                    /* access modifiers changed from: protected */
                    public String toDebugString(long j) {
                        return super.toDebugString(j) + ", addAccount" + ", accountType " + str4 + ", requiredFeatures " + (strArr2 != null ? TextUtils.join(",", strArr2) : null);
                    }
                }.bind();
            }
        }
    }

    public boolean addAccountExplicitly(int i, Account account, String str, Bundle bundle) {
        if (account != null) {
            return insertAccountIntoDatabase(i, account, str, bundle);
        }
        throw new IllegalArgumentException("account is null");
    }

    public boolean removeAccountExplicitly(int i, Account account) {
        return account != null && removeAccountInternal(i, account);
    }

    public void renameAccount(int i, c cVar, Account account, String str) {
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        }
        Account renameAccountInternal = renameAccountInternal(i, account, str);
        Bundle bundle = new Bundle();
        bundle.putString("authAccount", renameAccountInternal.name);
        bundle.putString("accountType", renameAccountInternal.type);
        try {
            cVar.onResult(bundle);
        } catch (RemoteException e2) {
            Log.w(TAG, e2.getMessage());
        }
    }

    public void removeAccount(int i, c cVar, Account account, boolean z) {
        if (cVar == null) {
            throw new IllegalArgumentException("response is null");
        } else if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else {
            AuthenticatorInfo authenticatorInfo = getAuthenticatorInfo(account.type);
            if (authenticatorInfo == null) {
                try {
                    cVar.onError(7, "account.type does not exist");
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                final Account account2 = account;
                final int i2 = i;
                new Session(cVar, i, authenticatorInfo, z, true, account.name) {
                    /* access modifiers changed from: protected */
                    public String toDebugString(long j) {
                        return super.toDebugString(j) + ", removeAccount" + ", account " + account2;
                    }

                    public void run() {
                        this.mAuthenticator.a(this, account2);
                    }

                    public void onResult(Bundle bundle) {
                        if (bundle != null && bundle.containsKey("booleanResult") && !bundle.containsKey("intent")) {
                            boolean z = bundle.getBoolean("booleanResult");
                            if (z) {
                                boolean unused = VAccountManagerService.this.removeAccountInternal(i2, account2);
                            }
                            c responseAndClose = getResponseAndClose();
                            if (responseAndClose != null) {
                                Log.v(VAccountManagerService.TAG, getClass().getSimpleName() + " calling onResult() on response " + responseAndClose);
                                Bundle bundle2 = new Bundle();
                                bundle2.putBoolean("booleanResult", z);
                                try {
                                    responseAndClose.onResult(bundle2);
                                } catch (RemoteException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                        super.onResult(bundle);
                    }
                }.bind();
            }
        }
    }

    public void clearPassword(int i, Account account) {
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        }
        setPasswordInternal(i, account, null);
    }

    /* access modifiers changed from: private */
    public boolean removeAccountInternal(int i, Account account) {
        List list = this.accountsByUserId.get(i);
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                VAccount vAccount = (VAccount) it.next();
                if (i == vAccount.userId && TextUtils.equals(vAccount.name, account.name) && TextUtils.equals(account.type, vAccount.type)) {
                    it.remove();
                    saveAllAccounts();
                    sendAccountsChangedBroadcast(i);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean accountAuthenticated(int i, Account account) {
        boolean z;
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        }
        synchronized (this.accountsByUserId) {
            VAccount account2 = getAccount(i, account);
            if (account2 != null) {
                account2.lastAuthenticatedTime = System.currentTimeMillis();
                saveAllAccounts();
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void invalidateAuthToken(int i, String str, String str2) {
        boolean z;
        if (str == null) {
            throw new IllegalArgumentException("accountType is null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("authToken is null");
        } else {
            synchronized (this.accountsByUserId) {
                List<VAccount> list = this.accountsByUserId.get(i);
                if (list != null) {
                    boolean z2 = false;
                    for (VAccount vAccount : list) {
                        if (vAccount.type.equals(str)) {
                            vAccount.authTokens.values().remove(str2);
                            z = true;
                        } else {
                            z = z2;
                        }
                        z2 = z;
                    }
                    if (z2) {
                        saveAllAccounts();
                    }
                }
                synchronized (this.authTokenRecords) {
                    Iterator<AuthTokenRecord> it = this.authTokenRecords.iterator();
                    while (it.hasNext()) {
                        AuthTokenRecord next = it.next();
                        if (next.userId == i && next.authTokenType.equals(str) && next.authToken.equals(str2)) {
                            it.remove();
                        }
                    }
                }
            }
        }
    }

    private Account renameAccountInternal(int i, Account account, String str) {
        synchronized (this.accountsByUserId) {
            VAccount account2 = getAccount(i, account);
            if (account2 == null) {
                return account;
            }
            account2.previousName = account2.name;
            account2.name = str;
            saveAllAccounts();
            Account account3 = new Account(account2.name, account2.type);
            synchronized (this.authTokenRecords) {
                Iterator<AuthTokenRecord> it = this.authTokenRecords.iterator();
                while (it.hasNext()) {
                    AuthTokenRecord next = it.next();
                    if (next.userId == i && next.account.equals(account)) {
                        next.account = account3;
                    }
                }
            }
            sendAccountsChangedBroadcast(i);
            return account3;
        }
    }

    public String peekAuthToken(int i, Account account, String str) {
        String str2;
        if (account == null) {
            throw new IllegalArgumentException("account is null");
        } else if (str == null) {
            throw new IllegalArgumentException("authTokenType is null");
        } else {
            synchronized (this.accountsByUserId) {
                VAccount account2 = getAccount(i, account);
                if (account2 != null) {
                    str2 = account2.authTokens.get(str);
                } else {
                    str2 = null;
                }
            }
            return str2;
        }
    }

    private String getCustomAuthToken(int i, Account account, String str, String str2) {
        String str3;
        AuthTokenRecord authTokenRecord = new AuthTokenRecord(i, account, str, str2);
        String str4 = null;
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.authTokenRecords) {
            Iterator<AuthTokenRecord> it = this.authTokenRecords.iterator();
            while (it.hasNext()) {
                AuthTokenRecord next = it.next();
                if (next.expiryEpochMillis > 0 && next.expiryEpochMillis < currentTimeMillis) {
                    it.remove();
                    str3 = str4;
                } else if (authTokenRecord.equals(next)) {
                    str3 = authTokenRecord.authToken;
                } else {
                    str3 = str4;
                }
                str4 = str3;
            }
        }
        return str4;
    }

    private void onResult(c cVar, Bundle bundle) {
        try {
            cVar.onResult(bundle);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    private AuthenticatorInfo getAuthenticatorInfo(String str) {
        AuthenticatorInfo authenticatorInfo;
        synchronized (this.cache) {
            authenticatorInfo = str == null ? null : this.cache.authenticators.get(str);
        }
        return authenticatorInfo;
    }

    private VAccount getAccount(int i, Account account) {
        return getAccount(i, account.name, account.type);
    }

    private boolean insertAccountIntoDatabase(int i, Account account, String str, Bundle bundle) {
        if (account == null) {
            return false;
        }
        synchronized (this.accountsByUserId) {
            VAccount vAccount = new VAccount(i, account);
            vAccount.password = str;
            if (bundle != null) {
                for (String next : bundle.keySet()) {
                    Object obj = bundle.get(next);
                    if (obj instanceof String) {
                        vAccount.userDatas.put(next, (String) obj);
                    }
                }
            }
            Object obj2 = this.accountsByUserId.get(i);
            if (obj2 == null) {
                obj2 = new ArrayList();
                this.accountsByUserId.put(i, obj2);
            }
            obj2.add(vAccount);
            saveAllAccounts();
            sendAccountsChangedBroadcast(vAccount.userId);
        }
        return true;
    }

    private void sendAccountsChangedBroadcast(int i) {
        VActivityManagerService.get().sendBroadcastAsUser(new Intent("android.accounts.LOGIN_ACCOUNTS_CHANGED"), new VUserHandle(i));
        broadcastCheckInNowIfNeed(i);
    }

    private void broadcastCheckInNowIfNeed(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        if (Math.abs(currentTimeMillis - this.lastAccountChangeTime) > CHECK_IN_TIME) {
            this.lastAccountChangeTime = currentTimeMillis;
            saveAllAccounts();
            VActivityManagerService.get().sendBroadcastAsUser(new Intent("android.server.checkin.CHECKIN_NOW"), new VUserHandle(i));
        }
    }

    /* access modifiers changed from: private */
    public void saveAllAccounts() {
        File accountConfigFile = VEnvironment.getAccountConfigFile();
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInt(1);
            ArrayList<VAccount> arrayList = new ArrayList<>();
            for (int i = 0; i < this.accountsByUserId.size(); i++) {
                List valueAt = this.accountsByUserId.valueAt(i);
                if (valueAt != null) {
                    arrayList.addAll(valueAt);
                }
            }
            obtain.writeInt(arrayList.size());
            for (VAccount writeToParcel : arrayList) {
                writeToParcel.writeToParcel(obtain, 0);
            }
            obtain.writeLong(this.lastAccountChangeTime);
            FileOutputStream fileOutputStream = new FileOutputStream(accountConfigFile);
            fileOutputStream.write(obtain.marshall());
            fileOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        obtain.recycle();
    }

    private void readAllAccounts() {
        boolean z;
        boolean z2 = false;
        File accountConfigFile = VEnvironment.getAccountConfigFile();
        refreshAuthenticatorCache(null);
        if (accountConfigFile.exists()) {
            this.accountsByUserId.clear();
            Parcel obtain = Parcel.obtain();
            try {
                FileInputStream fileInputStream = new FileInputStream(accountConfigFile);
                byte[] bArr = new byte[((int) accountConfigFile.length())];
                int read = fileInputStream.read(bArr);
                fileInputStream.close();
                if (read != bArr.length) {
                    throw new IOException(String.format(Locale.ENGLISH, "Expect length %d, but got %d.", Integer.valueOf(bArr.length), Integer.valueOf(read)));
                }
                obtain.unmarshall(bArr, 0, bArr.length);
                obtain.setDataPosition(0);
                obtain.readInt();
                int readInt = obtain.readInt();
                while (true) {
                    int i = readInt - 1;
                    if (readInt <= 0) {
                        break;
                    }
                    VAccount vAccount = new VAccount(obtain);
                    VLog.d(TAG, "Reading account : " + vAccount.type, new Object[0]);
                    if (this.cache.authenticators.get(vAccount.type) != null) {
                        Object obj = this.accountsByUserId.get(vAccount.userId);
                        if (obj == null) {
                            obj = new ArrayList();
                            this.accountsByUserId.put(vAccount.userId, obj);
                        }
                        obj.add(vAccount);
                        z = z2;
                    } else {
                        z = true;
                    }
                    z2 = z;
                    readInt = i;
                }
                this.lastAccountChangeTime = obtain.readLong();
                if (z2) {
                    saveAllAccounts();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            } finally {
                obtain.recycle();
            }
        }
    }

    /* access modifiers changed from: private */
    public VAccount getAccount(int i, String str, String str2) {
        List<VAccount> list = this.accountsByUserId.get(i);
        if (list != null) {
            for (VAccount vAccount : list) {
                if (TextUtils.equals(vAccount.name, str) && TextUtils.equals(vAccount.type, str2)) {
                    return vAccount;
                }
            }
        }
        return null;
    }

    public void refreshAuthenticatorCache(String str) {
        this.cache.authenticators.clear();
        Intent intent = new Intent("android.accounts.AccountAuthenticator");
        if (str != null) {
            intent.setPackage(str);
        }
        generateServicesMap(VPackageManagerService.get().queryIntentServices(intent, null, FileUtils.FileMode.MODE_IWUSR, 0), this.cache.authenticators, new RegisteredServicesParser());
    }

    private void generateServicesMap(List<ResolveInfo> list, Map<String, AuthenticatorInfo> map, RegisteredServicesParser registeredServicesParser) {
        int next;
        AuthenticatorDescription parseAuthenticatorDescription;
        for (ResolveInfo next2 : list) {
            XmlResourceParser parser = registeredServicesParser.getParser(this.mContext, next2.serviceInfo, "android.accounts.AccountAuthenticator");
            if (parser != null) {
                try {
                    AttributeSet asAttributeSet = Xml.asAttributeSet(parser);
                    do {
                        next = parser.next();
                        if (next == 1) {
                            break;
                        }
                    } while (next != 2);
                    if ("account-authenticator".equals(parser.getName()) && (parseAuthenticatorDescription = parseAuthenticatorDescription(registeredServicesParser.getResources(this.mContext, next2.serviceInfo.applicationInfo), next2.serviceInfo.packageName, asAttributeSet)) != null) {
                        map.put(parseAuthenticatorDescription.type, new AuthenticatorInfo(parseAuthenticatorDescription, next2.serviceInfo));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    static final class AuthTokenRecord {
        public Account account;
        public String authToken;
        /* access modifiers changed from: private */
        public String authTokenType;
        public long expiryEpochMillis;
        private String packageName;
        public int userId;

        AuthTokenRecord(int i, Account account2, String str, String str2, String str3, long j) {
            this.userId = i;
            this.account = account2;
            this.authTokenType = str;
            this.packageName = str2;
            this.authToken = str3;
            this.expiryEpochMillis = j;
        }

        AuthTokenRecord(int i, Account account2, String str, String str2) {
            this.userId = i;
            this.account = account2;
            this.authTokenType = str;
            this.packageName = str2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AuthTokenRecord authTokenRecord = (AuthTokenRecord) obj;
            if (this.userId != authTokenRecord.userId || !this.account.equals(authTokenRecord.account) || !this.authTokenType.equals(authTokenRecord.authTokenType) || !this.packageName.equals(authTokenRecord.packageName)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (((((this.userId * 31) + this.account.hashCode()) * 31) + this.authTokenType.hashCode()) * 31) + this.packageName.hashCode();
        }
    }

    private final class AuthenticatorInfo {
        final AuthenticatorDescription desc;
        final ServiceInfo serviceInfo;

        AuthenticatorInfo(AuthenticatorDescription authenticatorDescription, ServiceInfo serviceInfo2) {
            this.desc = authenticatorDescription;
            this.serviceInfo = serviceInfo2;
        }
    }

    private final class AuthenticatorCache {
        final Map<String, AuthenticatorInfo> authenticators;

        private AuthenticatorCache() {
            this.authenticators = new HashMap();
        }
    }

    private abstract class Session extends b.a implements ServiceConnection, IBinder.DeathRecipient {
        private String mAccountName;
        private boolean mAuthDetailsRequired;
        a mAuthenticator;
        final AuthenticatorInfo mAuthenticatorInfo;
        private long mCreationTime;
        private boolean mExpectActivityLaunch;
        private int mNumErrors;
        private int mNumRequestContinued;
        public int mNumResults;
        private c mResponse;
        private final boolean mStripAuthTokenFromResult;
        private boolean mUpdateLastAuthenticatedTime;
        final int mUserId;

        public abstract void run();

        Session(c cVar, int i, AuthenticatorInfo authenticatorInfo, boolean z, boolean z2, String str, boolean z3, boolean z4) {
            if (authenticatorInfo == null) {
                throw new IllegalArgumentException("accountType is null");
            }
            this.mStripAuthTokenFromResult = z2;
            this.mResponse = cVar;
            this.mUserId = i;
            this.mAuthenticatorInfo = authenticatorInfo;
            this.mExpectActivityLaunch = z;
            this.mCreationTime = SystemClock.elapsedRealtime();
            this.mAccountName = str;
            this.mAuthDetailsRequired = z3;
            this.mUpdateLastAuthenticatedTime = z4;
            synchronized (VAccountManagerService.this.mSessions) {
                VAccountManagerService.this.mSessions.put(toString(), this);
            }
            if (cVar != null) {
                try {
                    cVar.asBinder().linkToDeath(this, 0);
                } catch (RemoteException e2) {
                    this.mResponse = null;
                    binderDied();
                }
            }
        }

        Session(VAccountManagerService vAccountManagerService, c cVar, int i, AuthenticatorInfo authenticatorInfo, boolean z, boolean z2, String str) {
            this(cVar, i, authenticatorInfo, z, z2, str, false, false);
        }

        /* access modifiers changed from: package-private */
        public c getResponseAndClose() {
            if (this.mResponse == null) {
                return null;
            }
            c cVar = this.mResponse;
            close();
            return cVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
            unbind();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
            if (r3.mResponse == null) goto L_0x002b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
            r3.mResponse.asBinder().unlinkToDeath(r3, 0);
            r3.mResponse = null;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void close() {
            /*
                r3 = this;
                com.lody.virtual.server.accounts.VAccountManagerService r0 = com.lody.virtual.server.accounts.VAccountManagerService.this
                java.util.LinkedHashMap r1 = r0.mSessions
                monitor-enter(r1)
                com.lody.virtual.server.accounts.VAccountManagerService r0 = com.lody.virtual.server.accounts.VAccountManagerService.this     // Catch:{ all -> 0x002f }
                java.util.LinkedHashMap r0 = r0.mSessions     // Catch:{ all -> 0x002f }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x002f }
                java.lang.Object r0 = r0.remove(r2)     // Catch:{ all -> 0x002f }
                if (r0 != 0) goto L_0x0019
                monitor-exit(r1)     // Catch:{ all -> 0x002f }
            L_0x0018:
                return
            L_0x0019:
                monitor-exit(r1)     // Catch:{ all -> 0x002f }
                android.accounts.c r0 = r3.mResponse
                if (r0 == 0) goto L_0x002b
                android.accounts.c r0 = r3.mResponse
                android.os.IBinder r0 = r0.asBinder()
                r1 = 0
                r0.unlinkToDeath(r3, r1)
                r0 = 0
                r3.mResponse = r0
            L_0x002b:
                r3.unbind()
                goto L_0x0018
            L_0x002f:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x002f }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.accounts.VAccountManagerService.Session.close():void");
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.mAuthenticator = a.C0000a.a(iBinder);
            try {
                run();
            } catch (RemoteException e2) {
                onError(1, "remote exception");
            }
        }

        public void onRequestContinued() {
            this.mNumRequestContinued++;
        }

        public void onError(int i, String str) {
            this.mNumErrors++;
            c responseAndClose = getResponseAndClose();
            if (responseAndClose != null) {
                Log.v(VAccountManagerService.TAG, getClass().getSimpleName() + " calling onError() on response " + responseAndClose);
                try {
                    responseAndClose.onError(i, str);
                } catch (RemoteException e2) {
                    Log.v(VAccountManagerService.TAG, "Session.onError: caught RemoteException while responding", e2);
                }
            } else {
                Log.v(VAccountManagerService.TAG, "Session.onError: already closed");
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            this.mAuthenticator = null;
            c responseAndClose = getResponseAndClose();
            if (responseAndClose != null) {
                try {
                    responseAndClose.onError(1, "disconnected");
                } catch (RemoteException e2) {
                    Log.v(VAccountManagerService.TAG, "Session.onServiceDisconnected: caught RemoteException while responding", e2);
                }
            }
        }

        public void onResult(Bundle bundle) {
            c responseAndClose;
            boolean z = true;
            this.mNumResults++;
            if (bundle != null) {
                boolean z2 = bundle.getBoolean("booleanResult", false);
                boolean z3 = bundle.containsKey("authAccount") && bundle.containsKey("accountType");
                if (!this.mUpdateLastAuthenticatedTime || (!z2 && !z3)) {
                    z = false;
                }
                if (z || this.mAuthDetailsRequired) {
                    synchronized (VAccountManagerService.this.accountsByUserId) {
                        VAccount access$200 = VAccountManagerService.this.getAccount(this.mUserId, this.mAccountName, this.mAuthenticatorInfo.desc.type);
                        if (z && access$200 != null) {
                            access$200.lastAuthenticatedTime = System.currentTimeMillis();
                            VAccountManagerService.this.saveAllAccounts();
                        }
                        if (this.mAuthDetailsRequired) {
                            long j = -1;
                            if (access$200 != null) {
                                j = access$200.lastAuthenticatedTime;
                            }
                            bundle.putLong(AccountManagerCompat.KEY_LAST_AUTHENTICATED_TIME, j);
                        }
                    }
                }
            }
            if (bundle == null || !TextUtils.isEmpty(bundle.getString("authtoken"))) {
            }
            Intent intent = null;
            if (bundle != null) {
                intent = (Intent) bundle.getParcelable("intent");
            }
            if (!this.mExpectActivityLaunch || bundle == null || !bundle.containsKey("intent")) {
                responseAndClose = getResponseAndClose();
            } else {
                responseAndClose = this.mResponse;
            }
            if (responseAndClose == null) {
                return;
            }
            if (bundle == null) {
                try {
                    Log.v(VAccountManagerService.TAG, getClass().getSimpleName() + " calling onError() on response " + responseAndClose);
                    responseAndClose.onError(5, "null bundle returned");
                } catch (RemoteException e2) {
                    Log.v(VAccountManagerService.TAG, "failure while notifying response", e2);
                }
            } else {
                if (this.mStripAuthTokenFromResult) {
                    bundle.remove("authtoken");
                }
                Log.v(VAccountManagerService.TAG, getClass().getSimpleName() + " calling onResult() on response " + responseAndClose);
                if (bundle.getInt("errorCode", -1) <= 0 || intent != null) {
                    responseAndClose.onResult(bundle);
                } else {
                    responseAndClose.onError(bundle.getInt("errorCode"), bundle.getString("errorMessage"));
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void bind() {
            Log.v(VAccountManagerService.TAG, "initiating bind to authenticator type " + this.mAuthenticatorInfo.desc.type);
            Intent intent = new Intent();
            intent.setAction("android.accounts.AccountAuthenticator");
            intent.setClassName(this.mAuthenticatorInfo.serviceInfo.packageName, this.mAuthenticatorInfo.serviceInfo.name);
            intent.putExtra("_VA_|_user_id_", this.mUserId);
            if (!VAccountManagerService.this.mContext.bindService(intent, this, 1)) {
                Log.d(VAccountManagerService.TAG, "bind attempt failed for " + toDebugString());
                onError(1, "bind failure");
            }
        }

        /* access modifiers changed from: protected */
        public String toDebugString() {
            return toDebugString(SystemClock.elapsedRealtime());
        }

        /* access modifiers changed from: protected */
        public String toDebugString(long j) {
            return "Session: expectLaunch " + this.mExpectActivityLaunch + ", connected " + (this.mAuthenticator != null) + ", stats (" + this.mNumResults + "/" + this.mNumRequestContinued + "/" + this.mNumErrors + ")" + ", lifetime " + (((double) (j - this.mCreationTime)) / 1000.0d);
        }

        private void unbind() {
            if (this.mAuthenticator != null) {
                this.mAuthenticator = null;
                VAccountManagerService.this.mContext.unbindService(this);
            }
        }

        public void binderDied() {
            this.mResponse = null;
            close();
        }
    }

    private class GetAccountsByTypeAndFeatureSession extends Session {
        private volatile Account[] mAccountsOfType = null;
        private volatile ArrayList<Account> mAccountsWithFeatures = null;
        private volatile int mCurrentAccount = 0;
        private final String[] mFeatures;

        public GetAccountsByTypeAndFeatureSession(c cVar, int i, AuthenticatorInfo authenticatorInfo, String[] strArr) {
            super(VAccountManagerService.this, cVar, i, authenticatorInfo, false, true, null);
            this.mFeatures = strArr;
        }

        public void run() {
            this.mAccountsOfType = VAccountManagerService.this.getAccounts(this.mUserId, this.mAuthenticatorInfo.desc.type);
            this.mAccountsWithFeatures = new ArrayList<>(this.mAccountsOfType.length);
            this.mCurrentAccount = 0;
            checkAccount();
        }

        public void checkAccount() {
            if (this.mCurrentAccount >= this.mAccountsOfType.length) {
                sendResult();
                return;
            }
            a aVar = this.mAuthenticator;
            if (aVar == null) {
                Log.v(VAccountManagerService.TAG, "checkAccount: aborting session since we are no longer connected to the authenticator, " + toDebugString());
                return;
            }
            try {
                aVar.a(this, this.mAccountsOfType[this.mCurrentAccount], this.mFeatures);
            } catch (RemoteException e2) {
                onError(1, "remote exception");
            }
        }

        public void onResult(Bundle bundle) {
            this.mNumResults++;
            if (bundle == null) {
                onError(5, "null bundle");
                return;
            }
            if (bundle.getBoolean("booleanResult", false)) {
                this.mAccountsWithFeatures.add(this.mAccountsOfType[this.mCurrentAccount]);
            }
            this.mCurrentAccount++;
            checkAccount();
        }

        public void sendResult() {
            c responseAndClose = getResponseAndClose();
            if (responseAndClose != null) {
                try {
                    Account[] accountArr = new Account[this.mAccountsWithFeatures.size()];
                    for (int i = 0; i < accountArr.length; i++) {
                        accountArr[i] = this.mAccountsWithFeatures.get(i);
                    }
                    if (Log.isLoggable(VAccountManagerService.TAG, 2)) {
                        Log.v(VAccountManagerService.TAG, getClass().getSimpleName() + " calling onResult() on response " + responseAndClose);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArray("accounts", accountArr);
                    responseAndClose.onResult(bundle);
                } catch (RemoteException e2) {
                    Log.v(VAccountManagerService.TAG, "failure while notifying response", e2);
                }
            }
        }

        /* access modifiers changed from: protected */
        public String toDebugString(long j) {
            return super.toDebugString(j) + ", getAccountsByTypeAndFeatures" + ", " + (this.mFeatures != null ? TextUtils.join(",", this.mFeatures) : null);
        }
    }
}
