package com.lody.virtual.server.am;

import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.server.pm.parser.VPackage;
import io.fabric.sdk.android.services.common.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class UidSystem {
    private static final String TAG = UidSystem.class.getSimpleName();
    private int mFreeUid = a.DEFAULT_TIMEOUT;
    private final HashMap<String, Integer> mSharedUserIdMap = new HashMap<>();

    public void initUidList() {
        this.mSharedUserIdMap.clear();
        if (!loadUidList(VEnvironment.getUidListFile())) {
            loadUidList(VEnvironment.getBakUidListFile());
        }
    }

    private boolean loadUidList(File file) {
        if (!file.exists()) {
            return false;
        }
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
            this.mFreeUid = objectInputStream.readInt();
            this.mSharedUserIdMap.putAll((HashMap) objectInputStream.readObject());
            objectInputStream.close();
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    private void save() {
        File uidListFile = VEnvironment.getUidListFile();
        File bakUidListFile = VEnvironment.getBakUidListFile();
        if (uidListFile.exists()) {
            if (bakUidListFile.exists() && !bakUidListFile.delete()) {
                VLog.w(TAG, "Warning: Unable to delete the expired file --\n " + bakUidListFile.getPath(), new Object[0]);
            }
            try {
                FileUtils.copyFile(uidListFile, bakUidListFile);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(uidListFile));
            objectOutputStream.writeInt(this.mFreeUid);
            objectOutputStream.writeObject(this.mSharedUserIdMap);
            objectOutputStream.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public int getOrCreateUid(VPackage vPackage) {
        String str;
        String str2 = vPackage.mSharedUserId;
        if (str2 == null) {
            str = vPackage.packageName;
        } else {
            str = str2;
        }
        Integer num = this.mSharedUserIdMap.get(str);
        if (num != null) {
            return num.intValue();
        }
        int i = this.mFreeUid + 1;
        this.mFreeUid = i;
        this.mSharedUserIdMap.put(str, Integer.valueOf(i));
        save();
        return i;
    }
}
