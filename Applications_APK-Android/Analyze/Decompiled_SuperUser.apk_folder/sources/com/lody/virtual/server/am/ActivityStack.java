package com.lody.virtual.server.am;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.SparseArray;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.env.VirtualRuntime;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.helper.utils.ArrayUtils;
import com.lody.virtual.helper.utils.ClassUtils;
import com.lody.virtual.helper.utils.ComponentUtils;
import com.lody.virtual.remote.AppTaskInfo;
import com.lody.virtual.remote.StubActivityRecord;
import io.fabric.sdk.android.services.common.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import mirror.android.app.ActivityManagerNative;
import mirror.android.app.ActivityThread;
import mirror.android.app.IActivityManager;
import mirror.android.app.IApplicationThread;

class ActivityStack {
    private final ActivityManager mAM;
    private final SparseArray<TaskRecord> mHistory = new SparseArray<>();
    private final VActivityManagerService mService;

    private enum ReuseTarget {
        CURRENT,
        AFFINITY,
        DOCUMENT,
        MULTIPLE
    }

    ActivityStack(VActivityManagerService vActivityManagerService) {
        this.mService = vActivityManagerService;
        this.mAM = (ActivityManager) VirtualCore.get().getContext().getSystemService(ServiceManagerNative.ACTIVITY);
    }

    private static void removeFlags(Intent intent, int i) {
        intent.setFlags(intent.getFlags() & (i ^ -1));
    }

    private static boolean containFlags(Intent intent, int i) {
        return (intent.getFlags() & i) != 0;
    }

    private static ActivityRecord topActivityInTask(TaskRecord taskRecord) {
        ActivityRecord activityRecord;
        synchronized (taskRecord.activities) {
            int size = taskRecord.activities.size() - 1;
            while (true) {
                if (size < 0) {
                    activityRecord = null;
                    break;
                }
                activityRecord = taskRecord.activities.get(size);
                if (!activityRecord.marked) {
                    break;
                }
                size--;
            }
        }
        return activityRecord;
    }

    private void deliverNewIntentLocked(ActivityRecord activityRecord, ActivityRecord activityRecord2, Intent intent) {
        if (activityRecord2 != null) {
            try {
                activityRecord2.process.client.scheduleNewIntent(activityRecord != null ? activityRecord.component.getPackageName() : a.ANDROID_CLIENT_TYPE, activityRecord2.token, intent);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            } catch (NullPointerException e3) {
                e3.printStackTrace();
            }
        }
    }

    private TaskRecord findTaskByAffinityLocked(int i, String str) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.mHistory.size()) {
                return null;
            }
            TaskRecord valueAt = this.mHistory.valueAt(i3);
            if (i == valueAt.userId && str.equals(valueAt.affinity)) {
                return valueAt;
            }
            i2 = i3 + 1;
        }
    }

    private TaskRecord findTaskByIntentLocked(int i, Intent intent) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.mHistory.size()) {
                return null;
            }
            TaskRecord valueAt = this.mHistory.valueAt(i3);
            if (i == valueAt.userId && valueAt.taskRoot != null && intent.getComponent().equals(valueAt.taskRoot.getComponent())) {
                return valueAt;
            }
            i2 = i3 + 1;
        }
    }

    private ActivityRecord findActivityByToken(int i, IBinder iBinder) {
        ActivityRecord activityRecord = null;
        if (iBinder != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.mHistory.size()) {
                    break;
                }
                TaskRecord valueAt = this.mHistory.valueAt(i3);
                if (valueAt.userId == i) {
                    synchronized (valueAt.activities) {
                        for (ActivityRecord next : valueAt.activities) {
                            if (next.token != iBinder) {
                                next = activityRecord;
                            }
                            activityRecord = next;
                        }
                    }
                }
                i2 = i3 + 1;
            }
        }
        return activityRecord;
    }

    private boolean markTaskByClearTarget(TaskRecord taskRecord, ClearTarget clearTarget, ComponentName componentName) {
        int i;
        boolean z;
        boolean z2 = false;
        synchronized (taskRecord.activities) {
            switch (clearTarget) {
                case TASK:
                    for (ActivityRecord activityRecord : taskRecord.activities) {
                        activityRecord.marked = true;
                        z2 = true;
                    }
                    break;
                case SPEC_ACTIVITY:
                    for (ActivityRecord next : taskRecord.activities) {
                        if (next.component.equals(componentName)) {
                            next.marked = true;
                            z = true;
                        } else {
                            z = z2;
                        }
                        z2 = z;
                    }
                    break;
                case TOP:
                    int size = taskRecord.activities.size();
                    while (true) {
                        i = size - 1;
                        if (size > 0) {
                            if (taskRecord.activities.get(i).component.equals(componentName)) {
                                z2 = true;
                            } else {
                                size = i;
                            }
                        }
                    }
                    if (z2) {
                        int i2 = i;
                        while (true) {
                            int i3 = i2 + 1;
                            if (i2 >= taskRecord.activities.size() - 1) {
                                break;
                            } else {
                                taskRecord.activities.get(i3).marked = true;
                                i2 = i3;
                            }
                        }
                    }
                    break;
            }
        }
        return z2;
    }

    private void optimizeTasksLocked() {
        boolean z;
        ArrayList arrayList = new ArrayList(this.mAM.getRecentTasks(Integer.MAX_VALUE, 3));
        int size = this.mHistory.size();
        while (true) {
            int i = size - 1;
            if (size > 0) {
                TaskRecord valueAt = this.mHistory.valueAt(i);
                ListIterator listIterator = arrayList.listIterator();
                while (true) {
                    if (listIterator.hasNext()) {
                        if (((ActivityManager.RecentTaskInfo) listIterator.next()).id == valueAt.taskId) {
                            z = true;
                            listIterator.remove();
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    this.mHistory.removeAt(i);
                }
                size = i;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int startActivitiesLocked(int i, Intent[] intentArr, ActivityInfo[] activityInfoArr, String[] strArr, IBinder iBinder, Bundle bundle) {
        TaskRecord taskRecord;
        optimizeTasksLocked();
        ReuseTarget reuseTarget = ReuseTarget.CURRENT;
        Intent intent = intentArr[0];
        ActivityInfo activityInfo = activityInfoArr[0];
        ActivityRecord findActivityByToken = findActivityByToken(i, iBinder);
        if (findActivityByToken != null && findActivityByToken.launchMode == 3) {
            intent.addFlags(268435456);
        }
        if (containFlags(intent, 67108864)) {
            removeFlags(intent, 131072);
        }
        if (containFlags(intent, 32768) && !containFlags(intent, 268435456)) {
            removeFlags(intent, 32768);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            switch (activityInfo.documentLaunchMode) {
                case 1:
                    reuseTarget = ReuseTarget.DOCUMENT;
                    break;
                case 2:
                    reuseTarget = ReuseTarget.MULTIPLE;
                    break;
            }
        }
        if (containFlags(intent, 268435456)) {
            reuseTarget = containFlags(intent, 134217728) ? ReuseTarget.MULTIPLE : ReuseTarget.AFFINITY;
        } else if (activityInfo.launchMode == 2) {
            reuseTarget = containFlags(intent, 134217728) ? ReuseTarget.MULTIPLE : ReuseTarget.AFFINITY;
        }
        if (findActivityByToken == null && reuseTarget == ReuseTarget.CURRENT) {
            reuseTarget = ReuseTarget.AFFINITY;
        }
        String taskAffinity = ComponentUtils.getTaskAffinity(activityInfo);
        if (reuseTarget == ReuseTarget.AFFINITY) {
            taskRecord = findTaskByAffinityLocked(i, taskAffinity);
        } else if (reuseTarget == ReuseTarget.CURRENT) {
            taskRecord = findActivityByToken.task;
        } else if (reuseTarget == ReuseTarget.DOCUMENT) {
            taskRecord = findTaskByIntentLocked(i, intent);
        } else {
            taskRecord = null;
        }
        Intent[] startActivitiesProcess = startActivitiesProcess(i, intentArr, activityInfoArr, findActivityByToken);
        if (taskRecord == null) {
            realStartActivitiesLocked(null, startActivitiesProcess, strArr, bundle);
            return 0;
        }
        ActivityRecord activityRecord = topActivityInTask(taskRecord);
        if (activityRecord == null) {
            return 0;
        }
        realStartActivitiesLocked(activityRecord.token, startActivitiesProcess, strArr, bundle);
        return 0;
    }

    private Intent[] startActivitiesProcess(int i, Intent[] intentArr, ActivityInfo[] activityInfoArr, ActivityRecord activityRecord) {
        Intent[] intentArr2 = new Intent[intentArr.length];
        for (int i2 = 0; i2 < intentArr.length; i2++) {
            intentArr2[i2] = startActivityProcess(i, activityRecord, intentArr[i2], activityInfoArr[i2]);
        }
        return intentArr2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x018d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int startActivityLocked(int r19, android.content.Intent r20, android.content.pm.ActivityInfo r21, android.os.IBinder r22, android.os.Bundle r23, java.lang.String r24, int r25) {
        /*
            r18 = this;
            r18.optimizeTasksLocked()
            r0 = r18
            r1 = r19
            r2 = r22
            com.lody.virtual.server.am.ActivityRecord r12 = r0.findActivityByToken(r1, r2)
            if (r12 == 0) goto L_0x00c6
            com.lody.virtual.server.am.TaskRecord r5 = r12.task
        L_0x0011:
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r7 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.CURRENT
            com.lody.virtual.server.am.ActivityStack$ClearTarget r6 = com.lody.virtual.server.am.ActivityStack.ClearTarget.NOTHING
            r8 = 67108864(0x4000000, float:1.5046328E-36)
            r0 = r20
            boolean r9 = containFlags(r0, r8)
            r8 = 32768(0x8000, float:4.5918E-41)
            r0 = r20
            boolean r13 = containFlags(r0, r8)
            android.content.ComponentName r8 = r20.getComponent()
            if (r8 != 0) goto L_0x003e
            android.content.ComponentName r8 = new android.content.ComponentName
            r0 = r21
            java.lang.String r10 = r0.packageName
            r0 = r21
            java.lang.String r11 = r0.name
            r8.<init>(r10, r11)
            r0 = r20
            r0.setComponent(r8)
        L_0x003e:
            if (r12 == 0) goto L_0x004c
            int r8 = r12.launchMode
            r10 = 3
            if (r8 != r10) goto L_0x004c
            r8 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = r20
            r0.addFlags(r8)
        L_0x004c:
            if (r9 == 0) goto L_0x0057
            r6 = 131072(0x20000, float:1.83671E-40)
            r0 = r20
            removeFlags(r0, r6)
            com.lody.virtual.server.am.ActivityStack$ClearTarget r6 = com.lody.virtual.server.am.ActivityStack.ClearTarget.TOP
        L_0x0057:
            if (r13 == 0) goto L_0x0065
            r8 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = r20
            boolean r8 = containFlags(r0, r8)
            if (r8 == 0) goto L_0x00c9
            com.lody.virtual.server.am.ActivityStack$ClearTarget r6 = com.lody.virtual.server.am.ActivityStack.ClearTarget.TASK
        L_0x0065:
            int r8 = android.os.Build.VERSION.SDK_INT
            r10 = 21
            if (r8 < r10) goto L_0x0072
            r0 = r21
            int r8 = r0.documentLaunchMode
            switch(r8) {
                case 1: goto L_0x00d2;
                case 2: goto L_0x00d7;
                default: goto L_0x0072;
            }
        L_0x0072:
            r8 = 0
            r0 = r21
            int r10 = r0.launchMode
            switch(r10) {
                case 1: goto L_0x00da;
                case 2: goto L_0x00fb;
                case 3: goto L_0x0119;
                default: goto L_0x007a;
            }
        L_0x007a:
            r10 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r20
            boolean r10 = containFlags(r0, r10)
            if (r10 == 0) goto L_0x01c5
            r8 = 1
            r16 = r8
            r8 = r9
            r9 = r7
            r7 = r16
        L_0x008b:
            com.lody.virtual.server.am.ActivityStack$ClearTarget r10 = com.lody.virtual.server.am.ActivityStack.ClearTarget.NOTHING
            if (r6 != r10) goto L_0x01c2
            r10 = 131072(0x20000, float:1.83671E-40)
            r0 = r20
            boolean r10 = containFlags(r0, r10)
            if (r10 == 0) goto L_0x01c2
            com.lody.virtual.server.am.ActivityStack$ClearTarget r6 = com.lody.virtual.server.am.ActivityStack.ClearTarget.SPEC_ACTIVITY
            r11 = r6
        L_0x009c:
            if (r5 != 0) goto L_0x00a4
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r6 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.CURRENT
            if (r9 != r6) goto L_0x00a4
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r9 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.AFFINITY
        L_0x00a4:
            java.lang.String r10 = com.lody.virtual.helper.utils.ComponentUtils.getTaskAffinity(r21)
            r6 = 0
            int[] r14 = com.lody.virtual.server.am.ActivityStack.AnonymousClass2.$SwitchMap$com$lody$virtual$server$am$ActivityStack$ReuseTarget
            int r9 = r9.ordinal()
            r9 = r14[r9]
            switch(r9) {
                case 1: goto L_0x0126;
                case 2: goto L_0x012f;
                case 3: goto L_0x0134;
                default: goto L_0x00b4;
            }
        L_0x00b4:
            r10 = 0
            if (r6 != 0) goto L_0x0137
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            r4 = r23
            r0.startActivityInNewTaskLocked(r1, r2, r3, r4)
        L_0x00c4:
            r5 = 0
            return r5
        L_0x00c6:
            r5 = 0
            goto L_0x0011
        L_0x00c9:
            r8 = 32768(0x8000, float:4.5918E-41)
            r0 = r20
            removeFlags(r0, r8)
            goto L_0x0065
        L_0x00d2:
            com.lody.virtual.server.am.ActivityStack$ClearTarget r6 = com.lody.virtual.server.am.ActivityStack.ClearTarget.TASK
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r7 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.DOCUMENT
            goto L_0x0072
        L_0x00d7:
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r7 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.MULTIPLE
            goto L_0x0072
        L_0x00da:
            r8 = 1
            r10 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = r20
            boolean r10 = containFlags(r0, r10)
            if (r10 == 0) goto L_0x01c5
            r7 = 134217728(0x8000000, float:3.85186E-34)
            r0 = r20
            boolean r7 = containFlags(r0, r7)
            if (r7 == 0) goto L_0x00f8
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r7 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.MULTIPLE
        L_0x00f1:
            r16 = r8
            r8 = r9
            r9 = r7
            r7 = r16
            goto L_0x008b
        L_0x00f8:
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r7 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.AFFINITY
            goto L_0x00f1
        L_0x00fb:
            r7 = 0
            com.lody.virtual.server.am.ActivityStack$ClearTarget r9 = com.lody.virtual.server.am.ActivityStack.ClearTarget.TOP
            r6 = 134217728(0x8000000, float:3.85186E-34)
            r0 = r20
            boolean r6 = containFlags(r0, r6)
            if (r6 == 0) goto L_0x0116
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r6 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.MULTIPLE
        L_0x010a:
            r16 = r8
            r8 = r7
            r7 = r16
            r17 = r9
            r9 = r6
            r6 = r17
            goto L_0x008b
        L_0x0116:
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r6 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.AFFINITY
            goto L_0x010a
        L_0x0119:
            r6 = 0
            com.lody.virtual.server.am.ActivityStack$ClearTarget r7 = com.lody.virtual.server.am.ActivityStack.ClearTarget.TOP
            com.lody.virtual.server.am.ActivityStack$ReuseTarget r9 = com.lody.virtual.server.am.ActivityStack.ReuseTarget.AFFINITY
            r16 = r8
            r8 = r6
            r6 = r7
            r7 = r16
            goto L_0x008b
        L_0x0126:
            r0 = r18
            r1 = r19
            com.lody.virtual.server.am.TaskRecord r6 = r0.findTaskByAffinityLocked(r1, r10)
            goto L_0x00b4
        L_0x012f:
            com.lody.virtual.server.am.TaskRecord r6 = r18.findTaskByIntentLocked(r19, r20)
            goto L_0x00b4
        L_0x0134:
            r6 = r5
            goto L_0x00b4
        L_0x0137:
            r9 = 0
            r0 = r18
            android.app.ActivityManager r5 = r0.mAM
            int r14 = r6.taskId
            r15 = 0
            r5.moveTaskToFront(r14, r15)
            if (r13 != 0) goto L_0x01b7
            if (r8 != 0) goto L_0x01b7
            android.content.Intent r5 = r6.taskRoot
            r0 = r20
            boolean r5 = com.lody.virtual.helper.utils.ComponentUtils.isSameIntent(r0, r5)
            if (r5 == 0) goto L_0x01b7
            r5 = 1
        L_0x0151:
            boolean r13 = r11.deliverIntent
            if (r13 != 0) goto L_0x0157
            if (r7 == 0) goto L_0x01bf
        L_0x0157:
            android.content.ComponentName r10 = r20.getComponent()
            r0 = r18
            boolean r10 = r0.markTaskByClearTarget(r6, r11, r10)
            com.lody.virtual.server.am.ActivityRecord r11 = topActivityInTask(r6)
            if (r8 == 0) goto L_0x0170
            if (r7 != 0) goto L_0x0170
            if (r11 == 0) goto L_0x0170
            if (r10 == 0) goto L_0x0170
            r7 = 1
            r11.marked = r7
        L_0x0170:
            if (r11 == 0) goto L_0x01bc
            boolean r7 = r11.marked
            if (r7 != 0) goto L_0x01bc
            android.content.ComponentName r7 = r11.component
            android.content.ComponentName r8 = r20.getComponent()
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x01bc
            r0 = r18
            r1 = r20
            r0.deliverNewIntentLocked(r12, r11, r1)
            r7 = 1
            r8 = r10
        L_0x018b:
            if (r8 == 0) goto L_0x0196
            r0 = r18
            android.util.SparseArray<com.lody.virtual.server.am.TaskRecord> r8 = r0.mHistory
            monitor-enter(r8)
            r18.scheduleFinishMarkedActivityLocked()     // Catch:{ all -> 0x01b9 }
            monitor-exit(r8)     // Catch:{ all -> 0x01b9 }
        L_0x0196:
            if (r5 != 0) goto L_0x00c4
            if (r7 != 0) goto L_0x00c4
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r21
            android.content.Intent r7 = r0.startActivityProcess(r1, r12, r2, r3)
            if (r7 == 0) goto L_0x00c4
            r5 = r18
            r8 = r21
            r9 = r24
            r10 = r25
            r11 = r23
            r5.startActivityFromSourceTask(r6, r7, r8, r9, r10, r11)
            goto L_0x00c4
        L_0x01b7:
            r5 = 0
            goto L_0x0151
        L_0x01b9:
            r5 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x01b9 }
            throw r5
        L_0x01bc:
            r7 = r9
            r8 = r10
            goto L_0x018b
        L_0x01bf:
            r7 = r9
            r8 = r10
            goto L_0x018b
        L_0x01c2:
            r11 = r6
            goto L_0x009c
        L_0x01c5:
            r16 = r8
            r8 = r9
            r9 = r7
            r7 = r16
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.am.ActivityStack.startActivityLocked(int, android.content.Intent, android.content.pm.ActivityInfo, android.os.IBinder, android.os.Bundle, java.lang.String, int):int");
    }

    private void startActivityInNewTaskLocked(int i, Intent intent, ActivityInfo activityInfo, Bundle bundle) {
        Intent startActivityProcess = startActivityProcess(i, null, intent, activityInfo);
        if (startActivityProcess != null) {
            startActivityProcess.addFlags(268435456);
            startActivityProcess.addFlags(134217728);
            startActivityProcess.addFlags(2097152);
            if (Build.VERSION.SDK_INT < 21) {
                startActivityProcess.addFlags(524288);
            } else {
                startActivityProcess.addFlags(524288);
            }
            if (Build.VERSION.SDK_INT >= 16) {
                VirtualCore.get().getContext().startActivity(startActivityProcess, bundle);
            } else {
                VirtualCore.get().getContext().startActivity(startActivityProcess);
            }
        }
    }

    private void scheduleFinishMarkedActivityLocked() {
        int size = this.mHistory.size();
        while (true) {
            int i = size - 1;
            if (size > 0) {
                for (final ActivityRecord next : this.mHistory.valueAt(i).activities) {
                    if (next.marked) {
                        VirtualRuntime.getUIHandler().post(new Runnable() {
                            public void run() {
                                try {
                                    next.process.client.finishActivity(next.token);
                                } catch (RemoteException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        });
                    }
                }
                size = i;
            } else {
                return;
            }
        }
    }

    private void startActivityFromSourceTask(TaskRecord taskRecord, Intent intent, ActivityInfo activityInfo, String str, int i, Bundle bundle) {
        ActivityRecord activityRecord = taskRecord.activities.isEmpty() ? null : taskRecord.activities.get(taskRecord.activities.size() - 1);
        if (activityRecord != null && startActivityProcess(taskRecord.userId, activityRecord, intent, activityInfo) != null) {
            realStartActivityLocked(activityRecord.token, intent, str, i, bundle);
        }
    }

    private void realStartActivitiesLocked(IBinder iBinder, Intent[] intentArr, String[] strArr, Bundle bundle) {
        Class<?>[] paramList = IActivityManager.startActivities.paramList();
        Object[] objArr = new Object[paramList.length];
        if (paramList[0] == IApplicationThread.TYPE) {
            objArr[0] = ActivityThread.getApplicationThread.call(VirtualCore.mainThread(), new Object[0]);
        }
        int protoIndexOf = ArrayUtils.protoIndexOf(paramList, String.class);
        int protoIndexOf2 = ArrayUtils.protoIndexOf(paramList, Intent[].class);
        int protoIndexOf3 = ArrayUtils.protoIndexOf(paramList, IBinder.class, 2);
        int protoIndexOf4 = ArrayUtils.protoIndexOf(paramList, Bundle.class);
        int i = protoIndexOf2 + 1;
        if (protoIndexOf != -1) {
            objArr[protoIndexOf] = VirtualCore.get().getHostPkg();
        }
        objArr[protoIndexOf2] = intentArr;
        objArr[protoIndexOf3] = iBinder;
        objArr[i] = strArr;
        objArr[protoIndexOf4] = bundle;
        ClassUtils.fixArgs(paramList, objArr);
        IActivityManager.startActivities.call(ActivityManagerNative.getDefault.call(new Object[0]), objArr);
    }

    private void realStartActivityLocked(IBinder iBinder, Intent intent, String str, int i, Bundle bundle) {
        Class<?>[] paramList = IActivityManager.startActivity.paramList();
        Object[] objArr = new Object[paramList.length];
        if (paramList[0] == IApplicationThread.TYPE) {
            objArr[0] = ActivityThread.getApplicationThread.call(VirtualCore.mainThread(), new Object[0]);
        }
        int protoIndexOf = ArrayUtils.protoIndexOf(paramList, Intent.class);
        int protoIndexOf2 = ArrayUtils.protoIndexOf(paramList, IBinder.class, 2);
        int protoIndexOf3 = ArrayUtils.protoIndexOf(paramList, Bundle.class);
        int i2 = protoIndexOf + 1;
        objArr[protoIndexOf] = intent;
        objArr[protoIndexOf2] = iBinder;
        objArr[protoIndexOf2 + 1] = str;
        objArr[protoIndexOf2 + 2] = Integer.valueOf(i);
        if (protoIndexOf3 != -1) {
            objArr[protoIndexOf3] = bundle;
        }
        objArr[i2] = intent.getType();
        if (Build.VERSION.SDK_INT >= 18) {
            objArr[protoIndexOf - 1] = VirtualCore.get().getHostPkg();
        }
        ClassUtils.fixArgs(paramList, objArr);
        IActivityManager.startActivity.call(ActivityManagerNative.getDefault.call(new Object[0]), objArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String fetchStubActivity(int r10, android.content.pm.ActivityInfo r11) {
        /*
            r9 = this;
            r1 = 0
            mirror.RefStaticObject<int[]> r0 = mirror.com.android.internal.R_Hide.styleable.Window     // Catch:{ Throwable -> 0x0078 }
            java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x0078 }
            int[] r0 = (int[]) r0     // Catch:{ Throwable -> 0x0078 }
            mirror.RefStaticInt r2 = mirror.com.android.internal.R_Hide.styleable.Window_windowIsTranslucent     // Catch:{ Throwable -> 0x0078 }
            int r2 = r2.get()     // Catch:{ Throwable -> 0x0078 }
            mirror.RefStaticInt r3 = mirror.com.android.internal.R_Hide.styleable.Window_windowIsFloating     // Catch:{ Throwable -> 0x0078 }
            int r3 = r3.get()     // Catch:{ Throwable -> 0x0078 }
            mirror.RefStaticInt r4 = mirror.com.android.internal.R_Hide.styleable.Window_windowShowWallpaper     // Catch:{ Throwable -> 0x0078 }
            int r4 = r4.get()     // Catch:{ Throwable -> 0x0078 }
            com.lody.virtual.client.core.VirtualCore r5 = com.lody.virtual.client.core.VirtualCore.get()     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r6 = r11.packageName     // Catch:{ Throwable -> 0x0078 }
            android.content.res.Resources r5 = r5.getResources(r6)     // Catch:{ Throwable -> 0x0078 }
            if (r5 == 0) goto L_0x0050
            android.content.res.Resources$Theme r5 = r5.newTheme()     // Catch:{ Throwable -> 0x0078 }
            int r6 = r11.theme     // Catch:{ Throwable -> 0x0078 }
            android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r0)     // Catch:{ Throwable -> 0x0078 }
            if (r5 == 0) goto L_0x0093
            r0 = 0
            boolean r0 = r5.getBoolean(r4, r0)     // Catch:{ Throwable -> 0x0078 }
            r4 = 0
            boolean r2 = r5.getBoolean(r2, r4)     // Catch:{ Throwable -> 0x0087 }
            r4 = 0
            boolean r3 = r5.getBoolean(r3, r4)     // Catch:{ Throwable -> 0x008d }
        L_0x0042:
            if (r3 != 0) goto L_0x0048
            if (r2 != 0) goto L_0x0048
            if (r0 == 0) goto L_0x0049
        L_0x0048:
            r1 = 1
        L_0x0049:
            if (r1 == 0) goto L_0x0082
            java.lang.String r0 = com.lody.virtual.client.stub.VASettings.getStubDialogName(r10)
        L_0x004f:
            return r0
        L_0x0050:
            com.lody.virtual.server.am.AttributeCache r5 = com.lody.virtual.server.am.AttributeCache.instance()     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r6 = r11.packageName     // Catch:{ Throwable -> 0x0078 }
            int r7 = r11.theme     // Catch:{ Throwable -> 0x0078 }
            com.lody.virtual.server.am.AttributeCache$Entry r5 = r5.get(r6, r7, r0)     // Catch:{ Throwable -> 0x0078 }
            if (r5 == 0) goto L_0x0093
            android.content.res.TypedArray r0 = r5.array     // Catch:{ Throwable -> 0x0078 }
            if (r0 == 0) goto L_0x0093
            android.content.res.TypedArray r0 = r5.array     // Catch:{ Throwable -> 0x0078 }
            r6 = 0
            boolean r0 = r0.getBoolean(r4, r6)     // Catch:{ Throwable -> 0x0078 }
            android.content.res.TypedArray r4 = r5.array     // Catch:{ Throwable -> 0x0087 }
            r6 = 0
            boolean r2 = r4.getBoolean(r2, r6)     // Catch:{ Throwable -> 0x0087 }
            android.content.res.TypedArray r4 = r5.array     // Catch:{ Throwable -> 0x008d }
            r5 = 0
            boolean r3 = r4.getBoolean(r3, r5)     // Catch:{ Throwable -> 0x008d }
            goto L_0x0042
        L_0x0078:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x007b:
            r0.printStackTrace()
            r0 = r2
            r2 = r3
            r3 = r1
            goto L_0x0042
        L_0x0082:
            java.lang.String r0 = com.lody.virtual.client.stub.VASettings.getStubActivityName(r10)
            goto L_0x004f
        L_0x0087:
            r2 = move-exception
            r3 = r1
            r8 = r0
            r0 = r2
            r2 = r8
            goto L_0x007b
        L_0x008d:
            r3 = move-exception
            r8 = r3
            r3 = r2
            r2 = r0
            r0 = r8
            goto L_0x007b
        L_0x0093:
            r0 = r1
            r2 = r1
            r3 = r1
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.am.ActivityStack.fetchStubActivity(int, android.content.pm.ActivityInfo):java.lang.String");
    }

    private Intent startActivityProcess(int i, ActivityRecord activityRecord, Intent intent, ActivityInfo activityInfo) {
        ComponentName componentName;
        Intent intent2 = new Intent(intent);
        ProcessRecord startProcessIfNeedLocked = this.mService.startProcessIfNeedLocked(activityInfo.processName, i, activityInfo.packageName);
        if (startProcessIfNeedLocked == null) {
            return null;
        }
        Intent intent3 = new Intent();
        intent3.setClassName(VirtualCore.get().getHostPkg(), fetchStubActivity(startProcessIfNeedLocked.vpid, activityInfo));
        ComponentName component = intent2.getComponent();
        if (component == null) {
            component = ComponentUtils.toComponentName(activityInfo);
        }
        intent3.setType(component.flattenToString());
        if (activityRecord != null) {
            componentName = activityRecord.component;
        } else {
            componentName = null;
        }
        new StubActivityRecord(intent2, activityInfo, componentName, i).saveToIntent(intent3);
        return intent3;
    }

    /* access modifiers changed from: package-private */
    public void onActivityCreated(ProcessRecord processRecord, ComponentName componentName, ComponentName componentName2, IBinder iBinder, Intent intent, String str, int i, int i2, int i3) {
        TaskRecord taskRecord;
        synchronized (this.mHistory) {
            optimizeTasksLocked();
            TaskRecord taskRecord2 = this.mHistory.get(i);
            if (taskRecord2 == null) {
                taskRecord = new TaskRecord(i, processRecord.userId, str, intent);
                this.mHistory.put(i, taskRecord);
            } else {
                taskRecord = taskRecord2;
            }
            ActivityRecord activityRecord = new ActivityRecord(taskRecord, componentName, componentName2, iBinder, processRecord.userId, processRecord, i2, i3, str);
            synchronized (taskRecord.activities) {
                taskRecord.activities.add(activityRecord);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onActivityResumed(int i, IBinder iBinder) {
        synchronized (this.mHistory) {
            optimizeTasksLocked();
            ActivityRecord findActivityByToken = findActivityByToken(i, iBinder);
            if (findActivityByToken != null) {
                synchronized (findActivityByToken.task.activities) {
                    findActivityByToken.task.activities.remove(findActivityByToken);
                    findActivityByToken.task.activities.add(findActivityByToken);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ActivityRecord onActivityDestroyed(int i, IBinder iBinder) {
        ActivityRecord findActivityByToken;
        synchronized (this.mHistory) {
            optimizeTasksLocked();
            findActivityByToken = findActivityByToken(i, iBinder);
            if (findActivityByToken != null) {
                synchronized (findActivityByToken.task.activities) {
                    findActivityByToken.task.activities.remove(findActivityByToken);
                }
            }
        }
        return findActivityByToken;
    }

    /* access modifiers changed from: package-private */
    public void processDied(ProcessRecord processRecord) {
        synchronized (this.mHistory) {
            optimizeTasksLocked();
            int size = this.mHistory.size();
            while (true) {
                int i = size - 1;
                if (size > 0) {
                    TaskRecord valueAt = this.mHistory.valueAt(i);
                    synchronized (valueAt.activities) {
                        Iterator<ActivityRecord> it = valueAt.activities.iterator();
                        while (it.hasNext()) {
                            if (it.next().process.pid == processRecord.pid) {
                                it.remove();
                                if (valueAt.activities.isEmpty()) {
                                    this.mHistory.remove(valueAt.taskId);
                                }
                            }
                        }
                    }
                    size = i;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String getPackageForToken(int i, IBinder iBinder) {
        String str;
        synchronized (this.mHistory) {
            ActivityRecord findActivityByToken = findActivityByToken(i, iBinder);
            if (findActivityByToken != null) {
                str = findActivityByToken.component.getPackageName();
            } else {
                str = null;
            }
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public ComponentName getCallingActivity(int i, IBinder iBinder) {
        ComponentName componentName;
        synchronized (this.mHistory) {
            ActivityRecord findActivityByToken = findActivityByToken(i, iBinder);
            if (findActivityByToken != null) {
                componentName = findActivityByToken.caller != null ? findActivityByToken.caller : findActivityByToken.component;
            } else {
                componentName = null;
            }
        }
        return componentName;
    }

    public String getCallingPackage(int i, IBinder iBinder) {
        String str;
        synchronized (this.mHistory) {
            ActivityRecord findActivityByToken = findActivityByToken(i, iBinder);
            if (findActivityByToken != null) {
                str = findActivityByToken.caller != null ? findActivityByToken.caller.getPackageName() : a.ANDROID_CLIENT_TYPE;
            } else {
                str = a.ANDROID_CLIENT_TYPE;
            }
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public AppTaskInfo getTaskInfo(int i) {
        AppTaskInfo appTaskInfo;
        synchronized (this.mHistory) {
            TaskRecord taskRecord = this.mHistory.get(i);
            if (taskRecord != null) {
                appTaskInfo = taskRecord.getAppTaskInfo();
            } else {
                appTaskInfo = null;
            }
        }
        return appTaskInfo;
    }

    /* access modifiers changed from: package-private */
    public ComponentName getActivityClassForToken(int i, IBinder iBinder) {
        ComponentName componentName;
        synchronized (this.mHistory) {
            ActivityRecord findActivityByToken = findActivityByToken(i, iBinder);
            if (findActivityByToken != null) {
                componentName = findActivityByToken.component;
            } else {
                componentName = null;
            }
        }
        return componentName;
    }

    private enum ClearTarget {
        NOTHING,
        SPEC_ACTIVITY,
        TASK(true),
        TOP(true);
        
        boolean deliverIntent;

        private ClearTarget(boolean z) {
            this.deliverIntent = z;
        }
    }
}
