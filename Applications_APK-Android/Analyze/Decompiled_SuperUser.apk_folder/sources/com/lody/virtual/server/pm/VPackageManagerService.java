package com.lody.virtual.server.pm;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.Parcel;
import android.text.TextUtils;
import android.util.Log;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.ServiceManagerNative;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.compat.ObjectsCompat;
import com.lody.virtual.os.VUserHandle;
import com.lody.virtual.remote.VParceledListSlice;
import com.lody.virtual.server.IPackageInstaller;
import com.lody.virtual.server.IPackageManager;
import com.lody.virtual.server.pm.installer.VPackageInstallerService;
import com.lody.virtual.server.pm.parser.PackageParserEx;
import com.lody.virtual.server.pm.parser.VPackage;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class VPackageManagerService extends IPackageManager.Stub {
    static final String TAG = "PackageManager";
    private static final AtomicReference<VPackageManagerService> gService = new AtomicReference<>();
    private static final Comparator<ProviderInfo> sProviderInitOrderSorter = new Comparator<ProviderInfo>() {
        public int compare(ProviderInfo providerInfo, ProviderInfo providerInfo2) {
            int i = providerInfo.initOrder;
            int i2 = providerInfo2.initOrder;
            if (i > i2) {
                return -1;
            }
            return i < i2 ? 1 : 0;
        }
    };
    static final Comparator<ResolveInfo> sResolvePrioritySorter = new Comparator<ResolveInfo>() {
        public int compare(ResolveInfo resolveInfo, ResolveInfo resolveInfo2) {
            int i = resolveInfo.priority;
            int i2 = resolveInfo2.priority;
            if (i == i2) {
                int i3 = resolveInfo.preferredOrder;
                int i4 = resolveInfo2.preferredOrder;
                if (i3 != i4) {
                    if (i3 <= i4) {
                        return 1;
                    }
                    return -1;
                } else if (resolveInfo.isDefault == resolveInfo2.isDefault) {
                    int i5 = resolveInfo.match;
                    int i6 = resolveInfo2.match;
                    if (i5 == i6) {
                        return 0;
                    }
                    if (i5 <= i6) {
                        return 1;
                    }
                    return -1;
                } else if (!resolveInfo.isDefault) {
                    return 1;
                } else {
                    return -1;
                }
            } else if (i > i2) {
                return -1;
            } else {
                return 1;
            }
        }
    };
    private final ActivityIntentResolver mActivities = new ActivityIntentResolver();
    private final Map<String, VPackage> mPackages;
    private final HashMap<String, VPackage.PermissionGroupComponent> mPermissionGroups;
    private final HashMap<String, VPackage.PermissionComponent> mPermissions;
    private final ProviderIntentResolver mProviders;
    private final HashMap<String, VPackage.ProviderComponent> mProvidersByAuthority;
    private final HashMap<ComponentName, VPackage.ProviderComponent> mProvidersByComponent;
    private final ActivityIntentResolver mReceivers = new ActivityIntentResolver();
    private final ResolveInfo mResolveInfo;
    private final ServiceIntentResolver mServices = new ServiceIntentResolver();

    public VPackageManagerService() {
        ProviderIntentResolver providerIntentResolver = null;
        this.mProviders = Build.VERSION.SDK_INT >= 19 ? new ProviderIntentResolver() : providerIntentResolver;
        this.mProvidersByComponent = new HashMap<>();
        this.mPermissions = new HashMap<>();
        this.mPermissionGroups = new HashMap<>();
        this.mProvidersByAuthority = new HashMap<>();
        this.mPackages = PackageCacheManager.PACKAGE_CACHE;
        Intent intent = new Intent();
        intent.setClassName(VirtualCore.get().getHostPkg(), VASettings.RESOLVER_ACTIVITY);
        this.mResolveInfo = VirtualCore.get().getUnHookPackageManager().resolveActivity(intent, 0);
    }

    public static void systemReady() {
        VPackageManagerService vPackageManagerService = new VPackageManagerService();
        new VUserManagerService(VirtualCore.get().getContext(), vPackageManagerService, new char[0], vPackageManagerService.mPackages);
        gService.set(vPackageManagerService);
    }

    public static VPackageManagerService get() {
        return gService.get();
    }

    /* access modifiers changed from: package-private */
    public void analyzePackageLocked(VPackage vPackage) {
        int size = vPackage.activities.size();
        for (int i = 0; i < size; i++) {
            VPackage.ActivityComponent activityComponent = vPackage.activities.get(i);
            if (activityComponent.info.processName == null) {
                activityComponent.info.processName = activityComponent.info.packageName;
            }
            this.mActivities.addActivity(activityComponent, ServiceManagerNative.ACTIVITY);
        }
        int size2 = vPackage.services.size();
        for (int i2 = 0; i2 < size2; i2++) {
            VPackage.ServiceComponent serviceComponent = vPackage.services.get(i2);
            if (serviceComponent.info.processName == null) {
                serviceComponent.info.processName = serviceComponent.info.packageName;
            }
            this.mServices.addService(serviceComponent);
        }
        int size3 = vPackage.receivers.size();
        for (int i3 = 0; i3 < size3; i3++) {
            VPackage.ActivityComponent activityComponent2 = vPackage.receivers.get(i3);
            if (activityComponent2.info.processName == null) {
                activityComponent2.info.processName = activityComponent2.info.packageName;
            }
            this.mReceivers.addActivity(activityComponent2, "receiver");
        }
        int size4 = vPackage.providers.size();
        for (int i4 = 0; i4 < size4; i4++) {
            VPackage.ProviderComponent providerComponent = vPackage.providers.get(i4);
            if (providerComponent.info.processName == null) {
                providerComponent.info.processName = providerComponent.info.packageName;
            }
            if (Build.VERSION.SDK_INT >= 19) {
                this.mProviders.addProvider(providerComponent);
            }
            for (String str : providerComponent.info.authority.split(";")) {
                if (!this.mProvidersByAuthority.containsKey(str)) {
                    this.mProvidersByAuthority.put(str, providerComponent);
                }
            }
            this.mProvidersByComponent.put(providerComponent.getComponentName(), providerComponent);
        }
        int size5 = vPackage.permissions.size();
        for (int i5 = 0; i5 < size5; i5++) {
            VPackage.PermissionComponent permissionComponent = vPackage.permissions.get(i5);
            this.mPermissions.put(permissionComponent.className, permissionComponent);
        }
        int size6 = vPackage.permissionGroups.size();
        for (int i6 = 0; i6 < size6; i6++) {
            VPackage.PermissionGroupComponent permissionGroupComponent = vPackage.permissionGroups.get(i6);
            this.mPermissionGroups.put(permissionGroupComponent.className, permissionGroupComponent);
        }
    }

    /* access modifiers changed from: package-private */
    public void deletePackageLocked(String str) {
        VPackage vPackage = this.mPackages.get(str);
        if (vPackage != null) {
            int size = vPackage.activities.size();
            for (int i = 0; i < size; i++) {
                this.mActivities.removeActivity(vPackage.activities.get(i), ServiceManagerNative.ACTIVITY);
            }
            int size2 = vPackage.services.size();
            for (int i2 = 0; i2 < size2; i2++) {
                this.mServices.removeService(vPackage.services.get(i2));
            }
            int size3 = vPackage.receivers.size();
            for (int i3 = 0; i3 < size3; i3++) {
                this.mReceivers.removeActivity(vPackage.receivers.get(i3), "receiver");
            }
            int size4 = vPackage.providers.size();
            for (int i4 = 0; i4 < size4; i4++) {
                VPackage.ProviderComponent providerComponent = vPackage.providers.get(i4);
                if (Build.VERSION.SDK_INT >= 19) {
                    this.mProviders.removeProvider(providerComponent);
                }
                for (String remove : providerComponent.info.authority.split(";")) {
                    this.mProvidersByAuthority.remove(remove);
                }
                this.mProvidersByComponent.remove(providerComponent.getComponentName());
            }
            int size5 = vPackage.permissions.size();
            for (int i5 = 0; i5 < size5; i5++) {
                this.mPermissions.remove(vPackage.permissions.get(i5).className);
            }
            int size6 = vPackage.permissionGroups.size();
            for (int i6 = 0; i6 < size6; i6++) {
                this.mPermissionGroups.remove(vPackage.permissionGroups.get(i6).className);
            }
        }
    }

    public List<String> getSharedLibraries(String str) {
        ArrayList<String> arrayList;
        synchronized (this.mPackages) {
            VPackage vPackage = this.mPackages.get(str);
            if (vPackage != null) {
                arrayList = vPackage.usesLibraries;
            } else {
                arrayList = null;
            }
        }
        return arrayList;
    }

    public int checkPermission(String str, String str2, int i) {
        if ("android.permission.INTERACT_ACROSS_USERS".equals(str) || "android.permission.INTERACT_ACROSS_USERS_FULL".equals(str)) {
            return -1;
        }
        return VirtualCore.get().getPackageManager().checkPermission(str, VirtualCore.get().getHostPkg());
    }

    public PackageInfo getPackageInfo(String str, int i, int i2) {
        checkUserId(i2);
        synchronized (this.mPackages) {
            VPackage vPackage = this.mPackages.get(str);
            if (vPackage == null) {
                return null;
            }
            PackageInfo generatePackageInfo = generatePackageInfo(vPackage, (PackageSetting) vPackage.mExtras, i, i2);
            return generatePackageInfo;
        }
    }

    private PackageInfo generatePackageInfo(VPackage vPackage, PackageSetting packageSetting, int i, int i2) {
        PackageInfo generatePackageInfo = PackageParserEx.generatePackageInfo(vPackage, updateFlagsNought(i), packageSetting.firstInstallTime, packageSetting.lastUpdateTime, packageSetting.readUserState(i2), i2);
        if (generatePackageInfo != null) {
            return generatePackageInfo;
        }
        return null;
    }

    private int updateFlagsNought(int i) {
        if (Build.VERSION.SDK_INT >= 24 && (i & 786432) == 0) {
            return i | 786432;
        }
        return i;
    }

    private void checkUserId(int i) {
        if (!VUserManagerService.get().exists(i)) {
            throw new SecurityException("Invalid userId " + i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.pm.ActivityInfo getActivityInfo(android.content.ComponentName r6, int r7, int r8) {
        /*
            r5 = this;
            r5.checkUserId(r8)
            int r2 = r5.updateFlagsNought(r7)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r3 = r5.mPackages
            monitor-enter(r3)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r0 = r5.mPackages     // Catch:{ all -> 0x003b }
            java.lang.String r1 = r6.getPackageName()     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage r0 = (com.lody.virtual.server.pm.parser.VPackage) r0     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0038
            java.lang.Object r0 = r0.mExtras     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.PackageSetting r0 = (com.lody.virtual.server.pm.PackageSetting) r0     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.VPackageManagerService$ActivityIntentResolver r1 = r5.mActivities     // Catch:{ all -> 0x003b }
            java.util.HashMap r1 = r1.mActivities     // Catch:{ all -> 0x003b }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage$ActivityComponent r1 = (com.lody.virtual.server.pm.parser.VPackage.ActivityComponent) r1     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0038
            com.lody.virtual.server.pm.PackageUserState r4 = r0.readUserState(r8)     // Catch:{ all -> 0x003b }
            android.content.pm.ActivityInfo r1 = com.lody.virtual.server.pm.parser.PackageParserEx.generateActivityInfo(r1, r2, r4, r8)     // Catch:{ all -> 0x003b }
            com.lody.virtual.client.fixer.ComponentFixer.fixComponentInfo(r0, r1, r8)     // Catch:{ all -> 0x003b }
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = r1
        L_0x0037:
            return r0
        L_0x0038:
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = 0
            goto L_0x0037
        L_0x003b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VPackageManagerService.getActivityInfo(android.content.ComponentName, int, int):android.content.pm.ActivityInfo");
    }

    public boolean activitySupportsIntent(ComponentName componentName, Intent intent, String str) {
        synchronized (this.mPackages) {
            VPackage.ActivityComponent activityComponent = (VPackage.ActivityComponent) this.mActivities.mActivities.get(componentName);
            if (activityComponent == null) {
                return false;
            }
            for (int i = 0; i < activityComponent.intents.size(); i++) {
                if (((VPackage.ActivityIntentInfo) activityComponent.intents.get(i)).filter.match(intent.getAction(), str, intent.getScheme(), intent.getData(), intent.getCategories(), TAG) >= 0) {
                    return true;
                }
            }
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.pm.ActivityInfo getReceiverInfo(android.content.ComponentName r6, int r7, int r8) {
        /*
            r5 = this;
            r5.checkUserId(r8)
            int r2 = r5.updateFlagsNought(r7)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r3 = r5.mPackages
            monitor-enter(r3)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r0 = r5.mPackages     // Catch:{ all -> 0x003b }
            java.lang.String r1 = r6.getPackageName()     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage r0 = (com.lody.virtual.server.pm.parser.VPackage) r0     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0038
            java.lang.Object r0 = r0.mExtras     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.PackageSetting r0 = (com.lody.virtual.server.pm.PackageSetting) r0     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.VPackageManagerService$ActivityIntentResolver r1 = r5.mReceivers     // Catch:{ all -> 0x003b }
            java.util.HashMap r1 = r1.mActivities     // Catch:{ all -> 0x003b }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage$ActivityComponent r1 = (com.lody.virtual.server.pm.parser.VPackage.ActivityComponent) r1     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0038
            com.lody.virtual.server.pm.PackageUserState r4 = r0.readUserState(r8)     // Catch:{ all -> 0x003b }
            android.content.pm.ActivityInfo r1 = com.lody.virtual.server.pm.parser.PackageParserEx.generateActivityInfo(r1, r2, r4, r8)     // Catch:{ all -> 0x003b }
            com.lody.virtual.client.fixer.ComponentFixer.fixComponentInfo(r0, r1, r8)     // Catch:{ all -> 0x003b }
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = r1
        L_0x0037:
            return r0
        L_0x0038:
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = 0
            goto L_0x0037
        L_0x003b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VPackageManagerService.getReceiverInfo(android.content.ComponentName, int, int):android.content.pm.ActivityInfo");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.pm.ServiceInfo getServiceInfo(android.content.ComponentName r6, int r7, int r8) {
        /*
            r5 = this;
            r5.checkUserId(r8)
            int r2 = r5.updateFlagsNought(r7)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r3 = r5.mPackages
            monitor-enter(r3)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r0 = r5.mPackages     // Catch:{ all -> 0x003b }
            java.lang.String r1 = r6.getPackageName()     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage r0 = (com.lody.virtual.server.pm.parser.VPackage) r0     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0038
            java.lang.Object r0 = r0.mExtras     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.PackageSetting r0 = (com.lody.virtual.server.pm.PackageSetting) r0     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.VPackageManagerService$ServiceIntentResolver r1 = r5.mServices     // Catch:{ all -> 0x003b }
            java.util.HashMap r1 = r1.mServices     // Catch:{ all -> 0x003b }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage$ServiceComponent r1 = (com.lody.virtual.server.pm.parser.VPackage.ServiceComponent) r1     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0038
            com.lody.virtual.server.pm.PackageUserState r4 = r0.readUserState(r8)     // Catch:{ all -> 0x003b }
            android.content.pm.ServiceInfo r1 = com.lody.virtual.server.pm.parser.PackageParserEx.generateServiceInfo(r1, r2, r4, r8)     // Catch:{ all -> 0x003b }
            com.lody.virtual.client.fixer.ComponentFixer.fixComponentInfo(r0, r1, r8)     // Catch:{ all -> 0x003b }
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = r1
        L_0x0037:
            return r0
        L_0x0038:
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = 0
            goto L_0x0037
        L_0x003b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VPackageManagerService.getServiceInfo(android.content.ComponentName, int, int):android.content.pm.ServiceInfo");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.pm.ProviderInfo getProviderInfo(android.content.ComponentName r6, int r7, int r8) {
        /*
            r5 = this;
            r5.checkUserId(r8)
            int r2 = r5.updateFlagsNought(r7)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r3 = r5.mPackages
            monitor-enter(r3)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r0 = r5.mPackages     // Catch:{ all -> 0x0037 }
            java.lang.String r1 = r6.getPackageName()     // Catch:{ all -> 0x0037 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0037 }
            com.lody.virtual.server.pm.parser.VPackage r0 = (com.lody.virtual.server.pm.parser.VPackage) r0     // Catch:{ all -> 0x0037 }
            if (r0 == 0) goto L_0x0034
            java.lang.Object r0 = r0.mExtras     // Catch:{ all -> 0x0037 }
            com.lody.virtual.server.pm.PackageSetting r0 = (com.lody.virtual.server.pm.PackageSetting) r0     // Catch:{ all -> 0x0037 }
            java.util.HashMap<android.content.ComponentName, com.lody.virtual.server.pm.parser.VPackage$ProviderComponent> r1 = r5.mProvidersByComponent     // Catch:{ all -> 0x0037 }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x0037 }
            com.lody.virtual.server.pm.parser.VPackage$ProviderComponent r1 = (com.lody.virtual.server.pm.parser.VPackage.ProviderComponent) r1     // Catch:{ all -> 0x0037 }
            if (r1 == 0) goto L_0x0034
            com.lody.virtual.server.pm.PackageUserState r4 = r0.readUserState(r8)     // Catch:{ all -> 0x0037 }
            android.content.pm.ProviderInfo r1 = com.lody.virtual.server.pm.parser.PackageParserEx.generateProviderInfo(r1, r2, r4, r8)     // Catch:{ all -> 0x0037 }
            com.lody.virtual.client.fixer.ComponentFixer.fixComponentInfo(r0, r1, r8)     // Catch:{ all -> 0x0037 }
            monitor-exit(r3)     // Catch:{ all -> 0x0037 }
            r0 = r1
        L_0x0033:
            return r0
        L_0x0034:
            monitor-exit(r3)     // Catch:{ all -> 0x0037 }
            r0 = 0
            goto L_0x0033
        L_0x0037:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0037 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VPackageManagerService.getProviderInfo(android.content.ComponentName, int, int):android.content.pm.ProviderInfo");
    }

    public ResolveInfo resolveIntent(Intent intent, String str, int i, int i2) {
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        return chooseBestActivity(intent, str, updateFlagsNought, queryIntentActivities(intent, str, updateFlagsNought, 0));
    }

    private ResolveInfo chooseBestActivity(Intent intent, String str, int i, List<ResolveInfo> list) {
        if (list != null) {
            int size = list.size();
            if (size == 1) {
                return list.get(0);
            }
            if (size > 1) {
                ResolveInfo resolveInfo = list.get(0);
                ResolveInfo resolveInfo2 = list.get(1);
                if (resolveInfo.priority != resolveInfo2.priority || resolveInfo.preferredOrder != resolveInfo2.preferredOrder || resolveInfo.isDefault != resolveInfo2.isDefault) {
                    return list.get(0);
                }
                ResolveInfo findPreferredActivity = findPreferredActivity(intent, str, i, list, resolveInfo.priority);
                if (findPreferredActivity == null) {
                    return list.get(0);
                }
                return findPreferredActivity;
            }
        }
        return null;
    }

    private ResolveInfo findPreferredActivity(Intent intent, String str, int i, List<ResolveInfo> list, int i2) {
        return null;
    }

    public List<ResolveInfo> queryIntentActivities(Intent intent, String str, int i, int i2) {
        ComponentName componentName;
        Intent intent2;
        List<ResolveInfo> emptyList;
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        ComponentName component = intent.getComponent();
        if (component != null || Build.VERSION.SDK_INT < 15 || intent.getSelector() == null) {
            componentName = component;
            intent2 = intent;
        } else {
            Intent selector = intent.getSelector();
            componentName = selector.getComponent();
            intent2 = selector;
        }
        if (componentName != null) {
            emptyList = new ArrayList<>(1);
            ActivityInfo activityInfo = getActivityInfo(componentName, updateFlagsNought, i2);
            if (activityInfo != null) {
                ResolveInfo resolveInfo = new ResolveInfo();
                resolveInfo.activityInfo = activityInfo;
                emptyList.add(resolveInfo);
            }
        } else {
            synchronized (this.mPackages) {
                String str2 = intent2.getPackage();
                if (str2 == null) {
                    emptyList = this.mActivities.queryIntent(intent2, str, updateFlagsNought, i2);
                } else {
                    VPackage vPackage = this.mPackages.get(str2);
                    if (vPackage != null) {
                        emptyList = this.mActivities.queryIntentForPackage(intent2, str, updateFlagsNought, vPackage.activities, i2);
                    } else {
                        emptyList = Collections.emptyList();
                    }
                }
            }
        }
        return emptyList;
    }

    public List<ResolveInfo> queryIntentReceivers(Intent intent, String str, int i, int i2) {
        ComponentName componentName;
        Intent intent2;
        List<ResolveInfo> emptyList;
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        ComponentName component = intent.getComponent();
        if (component != null || Build.VERSION.SDK_INT < 15 || intent.getSelector() == null) {
            componentName = component;
            intent2 = intent;
        } else {
            Intent selector = intent.getSelector();
            componentName = selector.getComponent();
            intent2 = selector;
        }
        if (componentName != null) {
            emptyList = new ArrayList<>(1);
            ActivityInfo receiverInfo = getReceiverInfo(componentName, updateFlagsNought, i2);
            if (receiverInfo != null) {
                ResolveInfo resolveInfo = new ResolveInfo();
                resolveInfo.activityInfo = receiverInfo;
                emptyList.add(resolveInfo);
            }
        } else {
            synchronized (this.mPackages) {
                String str2 = intent2.getPackage();
                if (str2 == null) {
                    emptyList = this.mReceivers.queryIntent(intent2, str, updateFlagsNought, i2);
                } else {
                    VPackage vPackage = this.mPackages.get(str2);
                    if (vPackage != null) {
                        emptyList = this.mReceivers.queryIntentForPackage(intent2, str, updateFlagsNought, vPackage.receivers, i2);
                    } else {
                        emptyList = Collections.emptyList();
                    }
                }
            }
        }
        return emptyList;
    }

    public ResolveInfo resolveService(Intent intent, String str, int i, int i2) {
        checkUserId(i2);
        List<ResolveInfo> queryIntentServices = queryIntentServices(intent, str, updateFlagsNought(i), i2);
        if (queryIntentServices == null || queryIntentServices.size() < 1) {
            return null;
        }
        return queryIntentServices.get(0);
    }

    public List<ResolveInfo> queryIntentServices(Intent intent, String str, int i, int i2) {
        ComponentName componentName;
        Intent intent2;
        List<ResolveInfo> emptyList;
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        ComponentName component = intent.getComponent();
        if (component != null || Build.VERSION.SDK_INT < 15 || intent.getSelector() == null) {
            componentName = component;
            intent2 = intent;
        } else {
            Intent selector = intent.getSelector();
            componentName = selector.getComponent();
            intent2 = selector;
        }
        if (componentName != null) {
            emptyList = new ArrayList<>(1);
            ServiceInfo serviceInfo = getServiceInfo(componentName, updateFlagsNought, i2);
            if (serviceInfo != null) {
                ResolveInfo resolveInfo = new ResolveInfo();
                resolveInfo.serviceInfo = serviceInfo;
                emptyList.add(resolveInfo);
            }
        } else {
            synchronized (this.mPackages) {
                String str2 = intent2.getPackage();
                if (str2 == null) {
                    emptyList = this.mServices.queryIntent(intent2, str, updateFlagsNought, i2);
                } else {
                    VPackage vPackage = this.mPackages.get(str2);
                    if (vPackage != null) {
                        emptyList = this.mServices.queryIntentForPackage(intent2, str, updateFlagsNought, vPackage.services, i2);
                    } else {
                        emptyList = Collections.emptyList();
                    }
                }
            }
        }
        return emptyList;
    }

    @TargetApi(19)
    public List<ResolveInfo> queryIntentContentProviders(Intent intent, String str, int i, int i2) {
        ComponentName componentName;
        Intent intent2;
        List<ResolveInfo> emptyList;
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        ComponentName component = intent.getComponent();
        if (component != null || Build.VERSION.SDK_INT < 15 || intent.getSelector() == null) {
            componentName = component;
            intent2 = intent;
        } else {
            Intent selector = intent.getSelector();
            componentName = selector.getComponent();
            intent2 = selector;
        }
        if (componentName != null) {
            emptyList = new ArrayList<>(1);
            ProviderInfo providerInfo = getProviderInfo(componentName, updateFlagsNought, i2);
            if (providerInfo != null) {
                ResolveInfo resolveInfo = new ResolveInfo();
                resolveInfo.providerInfo = providerInfo;
                emptyList.add(resolveInfo);
            }
        } else {
            synchronized (this.mPackages) {
                String str2 = intent2.getPackage();
                if (str2 == null) {
                    emptyList = this.mProviders.queryIntent(intent2, str, updateFlagsNought, i2);
                } else {
                    VPackage vPackage = this.mPackages.get(str2);
                    if (vPackage != null) {
                        emptyList = this.mProviders.queryIntentForPackage(intent2, str, updateFlagsNought, vPackage.providers, i2);
                    } else {
                        emptyList = Collections.emptyList();
                    }
                }
            }
        }
        return emptyList;
    }

    public VParceledListSlice<ProviderInfo> queryContentProviders(String str, int i, int i2) {
        int userId = VUserHandle.getUserId(i);
        checkUserId(userId);
        int updateFlagsNought = updateFlagsNought(i2);
        ArrayList arrayList = new ArrayList(3);
        synchronized (this.mPackages) {
            for (VPackage.ProviderComponent next : this.mProvidersByComponent.values()) {
                PackageSetting packageSetting = (PackageSetting) next.owner.mExtras;
                if (str == null || (packageSetting.appId == VUserHandle.getAppId(i) && next.info.processName.equals(str))) {
                    arrayList.add(PackageParserEx.generateProviderInfo(next, updateFlagsNought, packageSetting.readUserState(userId), userId));
                }
            }
        }
        if (!arrayList.isEmpty()) {
            Collections.sort(arrayList, sProviderInitOrderSorter);
        }
        return new VParceledListSlice<>(arrayList);
    }

    public VParceledListSlice<PackageInfo> getInstalledPackages(int i, int i2) {
        checkUserId(i2);
        ArrayList arrayList = new ArrayList(this.mPackages.size());
        synchronized (this.mPackages) {
            for (VPackage next : this.mPackages.values()) {
                PackageInfo generatePackageInfo = generatePackageInfo(next, (PackageSetting) next.mExtras, i, i2);
                if (generatePackageInfo != null) {
                    arrayList.add(generatePackageInfo);
                }
            }
        }
        return new VParceledListSlice<>(arrayList);
    }

    public VParceledListSlice<ApplicationInfo> getInstalledApplications(int i, int i2) {
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        ArrayList arrayList = new ArrayList(this.mPackages.size());
        synchronized (this.mPackages) {
            for (VPackage next : this.mPackages.values()) {
                arrayList.add(PackageParserEx.generateApplicationInfo(next, updateFlagsNought, ((PackageSetting) next.mExtras).readUserState(i2), i2));
            }
        }
        return new VParceledListSlice<>(arrayList);
    }

    public PermissionInfo getPermissionInfo(String str, int i) {
        synchronized (this.mPackages) {
            VPackage.PermissionComponent permissionComponent = this.mPermissions.get(str);
            if (permissionComponent == null) {
                return null;
            }
            PermissionInfo permissionInfo = new PermissionInfo(permissionComponent.info);
            return permissionInfo;
        }
    }

    public List<PermissionInfo> queryPermissionsByGroup(String str, int i) {
        synchronized (this.mPackages) {
        }
        return null;
    }

    public PermissionGroupInfo getPermissionGroupInfo(String str, int i) {
        synchronized (this.mPackages) {
            VPackage.PermissionGroupComponent permissionGroupComponent = this.mPermissionGroups.get(str);
            if (permissionGroupComponent == null) {
                return null;
            }
            PermissionGroupInfo permissionGroupInfo = new PermissionGroupInfo(permissionGroupComponent.info);
            return permissionGroupInfo;
        }
    }

    public List<PermissionGroupInfo> getAllPermissionGroups(int i) {
        ArrayList arrayList;
        synchronized (this.mPackages) {
            arrayList = new ArrayList(this.mPermissionGroups.size());
            for (VPackage.PermissionGroupComponent permissionGroupComponent : this.mPermissionGroups.values()) {
                arrayList.add(new PermissionGroupInfo(permissionGroupComponent.info));
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.pm.ProviderInfo resolveContentProvider(java.lang.String r5, int r6, int r7) {
        /*
            r4 = this;
            r4.checkUserId(r7)
            int r2 = r4.updateFlagsNought(r6)
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r3 = r4.mPackages
            monitor-enter(r3)
            java.util.HashMap<java.lang.String, com.lody.virtual.server.pm.parser.VPackage$ProviderComponent> r0 = r4.mProvidersByAuthority     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage$ProviderComponent r0 = (com.lody.virtual.server.pm.parser.VPackage.ProviderComponent) r0     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0038
            com.lody.virtual.server.pm.parser.VPackage r1 = r0.owner     // Catch:{ all -> 0x003b }
            java.lang.Object r1 = r1.mExtras     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.PackageSetting r1 = (com.lody.virtual.server.pm.PackageSetting) r1     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.PackageUserState r1 = r1.readUserState(r7)     // Catch:{ all -> 0x003b }
            android.content.pm.ProviderInfo r1 = com.lody.virtual.server.pm.parser.PackageParserEx.generateProviderInfo(r0, r2, r1, r7)     // Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0038
            java.util.Map<java.lang.String, com.lody.virtual.server.pm.parser.VPackage> r0 = r4.mPackages     // Catch:{ all -> 0x003b }
            java.lang.String r2 = r1.packageName     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.parser.VPackage r0 = (com.lody.virtual.server.pm.parser.VPackage) r0     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.mExtras     // Catch:{ all -> 0x003b }
            com.lody.virtual.server.pm.PackageSetting r0 = (com.lody.virtual.server.pm.PackageSetting) r0     // Catch:{ all -> 0x003b }
            com.lody.virtual.client.fixer.ComponentFixer.fixComponentInfo(r0, r1, r7)     // Catch:{ all -> 0x003b }
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = r1
        L_0x0037:
            return r0
        L_0x0038:
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            r0 = 0
            goto L_0x0037
        L_0x003b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x003b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lody.virtual.server.pm.VPackageManagerService.resolveContentProvider(java.lang.String, int, int):android.content.pm.ProviderInfo");
    }

    public ApplicationInfo getApplicationInfo(String str, int i, int i2) {
        checkUserId(i2);
        int updateFlagsNought = updateFlagsNought(i);
        synchronized (this.mPackages) {
            VPackage vPackage = this.mPackages.get(str);
            if (vPackage == null) {
                return null;
            }
            ApplicationInfo generateApplicationInfo = PackageParserEx.generateApplicationInfo(vPackage, updateFlagsNought, ((PackageSetting) vPackage.mExtras).readUserState(i2), i2);
            return generateApplicationInfo;
        }
    }

    public String[] getPackagesForUid(int i) {
        String[] strArr;
        int userId = VUserHandle.getUserId(i);
        checkUserId(userId);
        synchronized (this) {
            ArrayList arrayList = new ArrayList(2);
            for (VPackage next : this.mPackages.values()) {
                if (VUserHandle.getUid(userId, ((PackageSetting) next.mExtras).appId) == i) {
                    arrayList.add(next.packageName);
                }
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        return strArr;
    }

    public int getPackageUid(String str, int i) {
        int i2;
        checkUserId(i);
        synchronized (this.mPackages) {
            VPackage vPackage = this.mPackages.get(str);
            if (vPackage != null) {
                i2 = VUserHandle.getUid(i, ((PackageSetting) vPackage.mExtras).appId);
            } else {
                i2 = -1;
            }
        }
        return i2;
    }

    public String getNameForUid(int i) {
        String str;
        int appId = VUserHandle.getAppId(i);
        synchronized (this.mPackages) {
            Iterator<VPackage> it = this.mPackages.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    str = null;
                    break;
                }
                PackageSetting packageSetting = (PackageSetting) it.next().mExtras;
                if (packageSetting.appId == appId) {
                    str = packageSetting.packageName;
                    break;
                }
            }
        }
        return str;
    }

    public List<String> querySharedPackages(String str) {
        synchronized (this.mPackages) {
            VPackage vPackage = this.mPackages.get(str);
            if (vPackage == null || vPackage.mSharedUserId == null) {
                List<String> list = Collections.EMPTY_LIST;
                return list;
            }
            ArrayList arrayList = new ArrayList();
            for (VPackage next : this.mPackages.values()) {
                if (TextUtils.equals(next.mSharedUserId, vPackage.mSharedUserId)) {
                    arrayList.add(next.packageName);
                }
            }
            return arrayList;
        }
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        try {
            return super.onTransact(i, parcel, parcel2, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public IPackageInstaller getPackageInstaller() {
        return VPackageInstallerService.get();
    }

    /* access modifiers changed from: package-private */
    public void createNewUser(int i, File file) {
        for (VPackage vPackage : this.mPackages.values()) {
            ((PackageSetting) vPackage.mExtras).modifyUserState(i);
        }
    }

    /* access modifiers changed from: package-private */
    public void cleanUpUser(int i) {
        for (VPackage vPackage : this.mPackages.values()) {
            ((PackageSetting) vPackage.mExtras).removeUser(i);
        }
    }

    private final class ActivityIntentResolver extends IntentResolver<VPackage.ActivityIntentInfo, ResolveInfo> {
        /* access modifiers changed from: private */
        public final HashMap<ComponentName, VPackage.ActivityComponent> mActivities;
        private int mFlags;

        private ActivityIntentResolver() {
            this.mActivities = new HashMap<>();
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ boolean allowFilterResult(VPackage.IntentInfo intentInfo, List list) {
            return allowFilterResult((VPackage.ActivityIntentInfo) intentInfo, (List<ResolveInfo>) list);
        }

        public List<ResolveInfo> queryIntent(Intent intent, String str, boolean z, int i) {
            this.mFlags = z ? 65536 : 0;
            return super.queryIntent(intent, str, z, i);
        }

        /* access modifiers changed from: package-private */
        public List<ResolveInfo> queryIntent(Intent intent, String str, int i, int i2) {
            this.mFlags = i;
            return super.queryIntent(intent, str, (65536 & i) != 0, i2);
        }

        /* access modifiers changed from: package-private */
        public List<ResolveInfo> queryIntentForPackage(Intent intent, String str, int i, ArrayList<VPackage.ActivityComponent> arrayList, int i2) {
            if (arrayList == null) {
                return null;
            }
            this.mFlags = i;
            boolean z = (65536 & i) != 0;
            int size = arrayList.size();
            ArrayList arrayList2 = new ArrayList(size);
            for (int i3 = 0; i3 < size; i3++) {
                ArrayList arrayList3 = arrayList.get(i3).intents;
                if (arrayList3 != null && arrayList3.size() > 0) {
                    VPackage.ActivityIntentInfo[] activityIntentInfoArr = new VPackage.ActivityIntentInfo[arrayList3.size()];
                    arrayList3.toArray(activityIntentInfoArr);
                    arrayList2.add(activityIntentInfoArr);
                }
            }
            return super.queryIntentFromList(intent, str, z, arrayList2, i2);
        }

        public final void addActivity(VPackage.ActivityComponent activityComponent, String str) {
            this.mActivities.put(activityComponent.getComponentName(), activityComponent);
            int size = activityComponent.intents.size();
            for (int i = 0; i < size; i++) {
                VPackage.ActivityIntentInfo activityIntentInfo = (VPackage.ActivityIntentInfo) activityComponent.intents.get(i);
                if (activityIntentInfo.filter.getPriority() > 0 && ServiceManagerNative.ACTIVITY.equals(str)) {
                    activityIntentInfo.filter.setPriority(0);
                    Log.w(VPackageManagerService.TAG, "Package " + activityComponent.info.applicationInfo.packageName + " has activity " + activityComponent.className + " with priority > 0, forcing to 0");
                }
                addFilter(activityIntentInfo);
            }
        }

        public final void removeActivity(VPackage.ActivityComponent activityComponent, String str) {
            this.mActivities.remove(activityComponent.getComponentName());
            int size = activityComponent.intents.size();
            for (int i = 0; i < size; i++) {
                removeFilter((VPackage.ActivityIntentInfo) activityComponent.intents.get(i));
            }
        }

        /* access modifiers changed from: protected */
        public boolean allowFilterResult(VPackage.ActivityIntentInfo activityIntentInfo, List<ResolveInfo> list) {
            ActivityInfo activityInfo = activityIntentInfo.activity.info;
            for (int size = list.size() - 1; size >= 0; size--) {
                ActivityInfo activityInfo2 = list.get(size).activityInfo;
                if (ObjectsCompat.equals(activityInfo2.name, activityInfo.name) && ObjectsCompat.equals(activityInfo2.packageName, activityInfo.packageName)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public VPackage.ActivityIntentInfo[] newArray(int i) {
            return new VPackage.ActivityIntentInfo[i];
        }

        /* access modifiers changed from: protected */
        public boolean isFilterStopped(VPackage.ActivityIntentInfo activityIntentInfo) {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean isPackageForFilter(String str, VPackage.ActivityIntentInfo activityIntentInfo) {
            return str.equals(activityIntentInfo.activity.owner.packageName);
        }

        /* access modifiers changed from: protected */
        public ResolveInfo newResult(VPackage.ActivityIntentInfo activityIntentInfo, int i, int i2) {
            VPackage.ActivityComponent activityComponent = activityIntentInfo.activity;
            ActivityInfo generateActivityInfo = PackageParserEx.generateActivityInfo(activityComponent, this.mFlags, ((PackageSetting) activityComponent.owner.mExtras).readUserState(i2), i2);
            if (generateActivityInfo == null) {
                return null;
            }
            ResolveInfo resolveInfo = new ResolveInfo();
            resolveInfo.activityInfo = generateActivityInfo;
            if ((this.mFlags & 64) != 0) {
                resolveInfo.filter = activityIntentInfo.filter;
            }
            resolveInfo.priority = activityIntentInfo.filter.getPriority();
            resolveInfo.preferredOrder = activityComponent.owner.mPreferredOrder;
            resolveInfo.match = i;
            resolveInfo.isDefault = activityIntentInfo.hasDefault;
            resolveInfo.labelRes = activityIntentInfo.labelRes;
            resolveInfo.nonLocalizedLabel = activityIntentInfo.nonLocalizedLabel;
            resolveInfo.icon = activityIntentInfo.icon;
            return resolveInfo;
        }

        /* access modifiers changed from: protected */
        public void sortResults(List<ResolveInfo> list) {
            Collections.sort(list, VPackageManagerService.sResolvePrioritySorter);
        }

        /* access modifiers changed from: protected */
        public void dumpFilter(PrintWriter printWriter, String str, VPackage.ActivityIntentInfo activityIntentInfo) {
        }

        /* access modifiers changed from: protected */
        public Object filterToLabel(VPackage.ActivityIntentInfo activityIntentInfo) {
            return activityIntentInfo.activity;
        }

        /* access modifiers changed from: protected */
        public void dumpFilterLabel(PrintWriter printWriter, String str, Object obj, int i) {
        }
    }

    private final class ServiceIntentResolver extends IntentResolver<VPackage.ServiceIntentInfo, ResolveInfo> {
        private int mFlags;
        /* access modifiers changed from: private */
        public final HashMap<ComponentName, VPackage.ServiceComponent> mServices;

        private ServiceIntentResolver() {
            this.mServices = new HashMap<>();
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ boolean allowFilterResult(VPackage.IntentInfo intentInfo, List list) {
            return allowFilterResult((VPackage.ServiceIntentInfo) intentInfo, (List<ResolveInfo>) list);
        }

        public List<ResolveInfo> queryIntent(Intent intent, String str, boolean z, int i) {
            this.mFlags = z ? 65536 : 0;
            return super.queryIntent(intent, str, z, i);
        }

        public List<ResolveInfo> queryIntent(Intent intent, String str, int i, int i2) {
            this.mFlags = i;
            return super.queryIntent(intent, str, (65536 & i) != 0, i2);
        }

        public List<ResolveInfo> queryIntentForPackage(Intent intent, String str, int i, ArrayList<VPackage.ServiceComponent> arrayList, int i2) {
            if (arrayList == null) {
                return null;
            }
            this.mFlags = i;
            boolean z = (65536 & i) != 0;
            int size = arrayList.size();
            ArrayList arrayList2 = new ArrayList(size);
            for (int i3 = 0; i3 < size; i3++) {
                ArrayList arrayList3 = arrayList.get(i3).intents;
                if (arrayList3 != null && arrayList3.size() > 0) {
                    VPackage.ServiceIntentInfo[] serviceIntentInfoArr = new VPackage.ServiceIntentInfo[arrayList3.size()];
                    arrayList3.toArray(serviceIntentInfoArr);
                    arrayList2.add(serviceIntentInfoArr);
                }
            }
            return super.queryIntentFromList(intent, str, z, arrayList2, i2);
        }

        public final void addService(VPackage.ServiceComponent serviceComponent) {
            this.mServices.put(serviceComponent.getComponentName(), serviceComponent);
            int size = serviceComponent.intents.size();
            for (int i = 0; i < size; i++) {
                addFilter((VPackage.ServiceIntentInfo) serviceComponent.intents.get(i));
            }
        }

        public final void removeService(VPackage.ServiceComponent serviceComponent) {
            this.mServices.remove(serviceComponent.getComponentName());
            int size = serviceComponent.intents.size();
            for (int i = 0; i < size; i++) {
                removeFilter((VPackage.ServiceIntentInfo) serviceComponent.intents.get(i));
            }
        }

        /* access modifiers changed from: protected */
        public boolean allowFilterResult(VPackage.ServiceIntentInfo serviceIntentInfo, List<ResolveInfo> list) {
            ServiceInfo serviceInfo = serviceIntentInfo.service.info;
            for (int size = list.size() - 1; size >= 0; size--) {
                ServiceInfo serviceInfo2 = list.get(size).serviceInfo;
                if (ObjectsCompat.equals(serviceInfo2.name, serviceInfo.name) && ObjectsCompat.equals(serviceInfo2.packageName, serviceInfo.packageName)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public VPackage.ServiceIntentInfo[] newArray(int i) {
            return new VPackage.ServiceIntentInfo[i];
        }

        /* access modifiers changed from: protected */
        public boolean isFilterStopped(VPackage.ServiceIntentInfo serviceIntentInfo) {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean isPackageForFilter(String str, VPackage.ServiceIntentInfo serviceIntentInfo) {
            return str.equals(serviceIntentInfo.service.owner.packageName);
        }

        /* access modifiers changed from: protected */
        public ResolveInfo newResult(VPackage.ServiceIntentInfo serviceIntentInfo, int i, int i2) {
            VPackage.ServiceComponent serviceComponent = serviceIntentInfo.service;
            ServiceInfo generateServiceInfo = PackageParserEx.generateServiceInfo(serviceComponent, this.mFlags, ((PackageSetting) serviceComponent.owner.mExtras).readUserState(i2), i2);
            if (generateServiceInfo == null) {
                return null;
            }
            ResolveInfo resolveInfo = new ResolveInfo();
            resolveInfo.serviceInfo = generateServiceInfo;
            if ((this.mFlags & 64) != 0) {
                resolveInfo.filter = serviceIntentInfo.filter;
            }
            resolveInfo.priority = serviceIntentInfo.filter.getPriority();
            resolveInfo.preferredOrder = serviceComponent.owner.mPreferredOrder;
            resolveInfo.match = i;
            resolveInfo.isDefault = serviceIntentInfo.hasDefault;
            resolveInfo.labelRes = serviceIntentInfo.labelRes;
            resolveInfo.nonLocalizedLabel = serviceIntentInfo.nonLocalizedLabel;
            resolveInfo.icon = serviceIntentInfo.icon;
            return resolveInfo;
        }

        /* access modifiers changed from: protected */
        public void sortResults(List<ResolveInfo> list) {
            Collections.sort(list, VPackageManagerService.sResolvePrioritySorter);
        }

        /* access modifiers changed from: protected */
        public void dumpFilter(PrintWriter printWriter, String str, VPackage.ServiceIntentInfo serviceIntentInfo) {
        }

        /* access modifiers changed from: protected */
        public Object filterToLabel(VPackage.ServiceIntentInfo serviceIntentInfo) {
            return serviceIntentInfo.service;
        }

        /* access modifiers changed from: protected */
        public void dumpFilterLabel(PrintWriter printWriter, String str, Object obj, int i) {
        }
    }
}
