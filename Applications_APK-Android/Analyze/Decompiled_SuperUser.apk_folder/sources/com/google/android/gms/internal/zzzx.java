package com.google.android.gms.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import android.util.Log;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.zzt;
import com.google.android.gms.internal.zzaad;
import com.google.android.gms.internal.zzaax;
import com.google.android.gms.internal.zzabh;
import com.google.android.gms.tasks.TaskCompletionSource;

public abstract class zzzx {
    public final int zzakD;

    private static abstract class zza extends zzzx {
        protected final TaskCompletionSource<Void> zzazE;

        public zza(int i, TaskCompletionSource<Void> taskCompletionSource) {
            super(i);
            this.zzazE = taskCompletionSource;
        }

        public void zza(zzaal zzaal, boolean z) {
        }

        public final void zza(zzaax.zza<?> zza) {
            try {
                zzb(zza);
            } catch (DeadObjectException e2) {
                zzz(zzzx.zza(e2));
                throw e2;
            } catch (RemoteException e3) {
                zzz(zzzx.zza(e3));
            }
        }

        /* access modifiers changed from: protected */
        public abstract void zzb(zzaax.zza<?> zza);

        public void zzz(Status status) {
            this.zzazE.trySetException(new com.google.android.gms.common.api.zza(status));
        }
    }

    public static class zzb<A extends zzaad.zza<? extends Result, Api.zzb>> extends zzzx {
        protected final A zzazF;

        public zzb(int i, A a2) {
            super(i);
            this.zzazF = a2;
        }

        public void zza(zzaal zzaal, boolean z) {
            zzaal.zza((zzaaf<? extends Result>) this.zzazF, z);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void zza(com.google.android.gms.internal.zzaax.zza<?> r3) {
            /*
                r2 = this;
                A r0 = r2.zzazF
                com.google.android.gms.common.api.Api$zze r1 = r3.zzvU()
                r0.zzb(r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzzx.zzb.zza(com.google.android.gms.internal.zzaax$zza):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void zzz(com.google.android.gms.common.api.Status r2) {
            /*
                r1 = this;
                A r0 = r1.zzazF
                r0.zzB(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzzx.zzb.zzz(com.google.android.gms.common.api.Status):void");
        }
    }

    public static final class zzc extends zza {
        public final zzabm<Api.zzb, ?> zzazG;
        public final zzabz<Api.zzb, ?> zzazH;

        public zzc(zzabn zzabn, TaskCompletionSource<Void> taskCompletionSource) {
            super(3, taskCompletionSource);
            this.zzazG = zzabn.zzazG;
            this.zzazH = zzabn.zzazH;
        }

        public /* bridge */ /* synthetic */ void zza(zzaal zzaal, boolean z) {
            super.zza(zzaal, z);
        }

        public void zzb(zzaax.zza<?> zza) {
            if (this.zzazG.zzwW() != null) {
                zza.zzwI().put(this.zzazG.zzwW(), new zzabn(this.zzazG, this.zzazH));
            }
        }

        public /* bridge */ /* synthetic */ void zzz(Status status) {
            super.zzz(status);
        }
    }

    public static final class zzd<TResult> extends zzzx {
        private final TaskCompletionSource<TResult> zzazE;
        private final zzabv<Api.zzb, TResult> zzazI;
        private final zzabs zzazJ;

        public zzd(int i, zzabv<Api.zzb, TResult> zzabv, TaskCompletionSource<TResult> taskCompletionSource, zzabs zzabs) {
            super(i);
            this.zzazE = taskCompletionSource;
            this.zzazI = zzabv;
            this.zzazJ = zzabs;
        }

        public void zza(zzaal zzaal, boolean z) {
            zzaal.zza(this.zzazE, z);
        }

        public void zza(zzaax.zza<?> zza) {
            try {
                this.zzazI.zza(zza.zzvU(), this.zzazE);
            } catch (DeadObjectException e2) {
                throw e2;
            } catch (RemoteException e3) {
                zzz(zzzx.zza(e3));
            }
        }

        public void zzz(Status status) {
            this.zzazE.trySetException(this.zzazJ.zzA(status));
        }
    }

    public static final class zze extends zza {
        public final zzabh.zzb<?> zzazK;

        public zze(zzabh.zzb<?> zzb, TaskCompletionSource<Void> taskCompletionSource) {
            super(4, taskCompletionSource);
            this.zzazK = zzb;
        }

        public /* bridge */ /* synthetic */ void zza(zzaal zzaal, boolean z) {
            super.zza(zzaal, z);
        }

        public void zzb(zzaax.zza<?> zza) {
            zzabn remove = zza.zzwI().remove(this.zzazK);
            if (remove != null) {
                remove.zzazG.zzwX();
                return;
            }
            Log.wtf("UnregisterListenerTask", "Received call to unregister a listener without a matching registration call.", new Exception());
            this.zzazE.trySetException(new com.google.android.gms.common.api.zza(Status.zzazz));
        }

        public /* bridge */ /* synthetic */ void zzz(Status status) {
            super.zzz(status);
        }
    }

    public zzzx(int i) {
        this.zzakD = i;
    }

    /* access modifiers changed from: private */
    public static Status zza(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (zzt.zzzh() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    public abstract void zza(zzaal zzaal, boolean z);

    public abstract void zza(zzaax.zza<?> zza2);

    public abstract void zzz(Status status);
}
