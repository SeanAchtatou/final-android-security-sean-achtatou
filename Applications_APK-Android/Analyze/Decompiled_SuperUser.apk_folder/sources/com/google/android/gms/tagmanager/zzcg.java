package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.TextUtils;
import com.duapps.ad.AdError;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.tagmanager.zzde;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

class zzcg implements zzaw {
    /* access modifiers changed from: private */
    public static final String zzaeu = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL,'%s' INTEGER NOT NULL);", "gtm_hits", "hit_id", "hit_time", "hit_url", "hit_first_send_time");
    /* access modifiers changed from: private */
    public final Context mContext;
    private final zzb zzbGV;
    private volatile zzad zzbGW;
    private final zzax zzbGX;
    /* access modifiers changed from: private */
    public final String zzbGY;
    private long zzbGZ;
    private final int zzbHa;
    /* access modifiers changed from: private */
    public zze zzuP;

    class zza implements zzde.zza {
        zza() {
        }

        public void zza(zzas zzas) {
            zzcg.this.zzv(zzas.zzQP());
        }

        public void zzb(zzas zzas) {
            zzcg.this.zzv(zzas.zzQP());
            zzbo.v(new StringBuilder(57).append("Permanent failure dispatching hitId: ").append(zzas.zzQP()).toString());
        }

        public void zzc(zzas zzas) {
            long zzQQ = zzas.zzQQ();
            if (zzQQ == 0) {
                zzcg.this.zzj(zzas.zzQP(), zzcg.this.zzuP.currentTimeMillis());
            } else if (zzQQ + 14400000 < zzcg.this.zzuP.currentTimeMillis()) {
                zzcg.this.zzv(zzas.zzQP());
                zzbo.v(new StringBuilder(47).append("Giving up on failed hitId: ").append(zzas.zzQP()).toString());
            }
        }
    }

    class zzb extends SQLiteOpenHelper {
        private boolean zzbHc;
        private long zzbHd = 0;

        zzb(Context context, String str) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x004d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean zza(java.lang.String r11, android.database.sqlite.SQLiteDatabase r12) {
            /*
                r10 = this;
                r8 = 0
                r9 = 0
                java.lang.String r1 = "SQLITE_MASTER"
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                r0 = 0
                java.lang.String r3 = "name"
                r2[r0] = r3     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                java.lang.String r3 = "name=?"
                r0 = 1
                java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                r0 = 0
                r4[r0] = r11     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r12
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0056, all -> 0x0053 }
                if (r1 == 0) goto L_0x0025
                r1.close()
            L_0x0025:
                return r0
            L_0x0026:
                r0 = move-exception
                r0 = r9
            L_0x0028:
                java.lang.String r2 = "Error querying for table "
                java.lang.String r1 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0048 }
                int r3 = r1.length()     // Catch:{ all -> 0x0048 }
                if (r3 == 0) goto L_0x0042
                java.lang.String r1 = r2.concat(r1)     // Catch:{ all -> 0x0048 }
            L_0x0038:
                com.google.android.gms.tagmanager.zzbo.zzbh(r1)     // Catch:{ all -> 0x0048 }
                if (r0 == 0) goto L_0x0040
                r0.close()
            L_0x0040:
                r0 = r8
                goto L_0x0025
            L_0x0042:
                java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x0048 }
                r1.<init>(r2)     // Catch:{ all -> 0x0048 }
                goto L_0x0038
            L_0x0048:
                r1 = move-exception
                r9 = r0
                r0 = r1
            L_0x004b:
                if (r9 == 0) goto L_0x0050
                r9.close()
            L_0x0050:
                throw r0
            L_0x0051:
                r0 = move-exception
                goto L_0x004b
            L_0x0053:
                r0 = move-exception
                r9 = r1
                goto L_0x004b
            L_0x0056:
                r0 = move-exception
                r0 = r1
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzcg.zzb.zza(java.lang.String, android.database.sqlite.SQLiteDatabase):boolean");
        }

        /* JADX INFO: finally extract failed */
        private void zzc(SQLiteDatabase sQLiteDatabase) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM gtm_hits WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_time") || !hashSet.remove("hit_first_send_time")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.zzbHc || this.zzbHd + 3600000 <= zzcg.this.zzuP.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.zzbHc = true;
                this.zzbHd = zzcg.this.zzuP.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e2) {
                    zzcg.this.mContext.getDatabasePath(zzcg.this.zzbGY).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.zzbHc = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            zzan.zzca(sQLiteDatabase.getPath());
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (Build.VERSION.SDK_INT < 15) {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!zza("gtm_hits", sQLiteDatabase)) {
                sQLiteDatabase.execSQL(zzcg.zzaeu);
            } else {
                zzc(sQLiteDatabase);
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    zzcg(zzax zzax, Context context) {
        this(zzax, context, "gtm_urls.db", AdError.SERVER_ERROR_CODE);
    }

    zzcg(zzax zzax, Context context, String str, int i) {
        this.mContext = context.getApplicationContext();
        this.zzbGY = str;
        this.zzbGX = zzax;
        this.zzuP = zzi.zzzc();
        this.zzbGV = new zzb(this.mContext, this.zzbGY);
        this.zzbGW = new zzde(this.mContext, new zza());
        this.zzbGZ = 0;
        this.zzbHa = i;
    }

    private void zzRc() {
        int zzRd = (zzRd() - this.zzbHa) + 1;
        if (zzRd > 0) {
            List<String> zznG = zznG(zzRd);
            zzbo.v(new StringBuilder(51).append("Store full, deleting ").append(zznG.size()).append(" hits to make room.").toString());
            zzh((String[]) zznG.toArray(new String[0]));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void zzh(long j, String str) {
        SQLiteDatabase zzhe = zzhe("Error opening database for putHit");
        if (zzhe != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_time", Long.valueOf(j));
            contentValues.put("hit_url", str);
            contentValues.put("hit_first_send_time", (Integer) 0);
            try {
                zzhe.insert("gtm_hits", null, contentValues);
                this.zzbGX.zzaR(false);
            } catch (SQLiteException e2) {
                zzbo.zzbh("Error storing hit");
            }
        }
    }

    private SQLiteDatabase zzhe(String str) {
        try {
            return this.zzbGV.getWritableDatabase();
        } catch (SQLiteException e2) {
            zzbo.zzbh(str);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void zzj(long j, long j2) {
        SQLiteDatabase zzhe = zzhe("Error opening database for getNumStoredHits.");
        if (zzhe != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_first_send_time", Long.valueOf(j2));
            try {
                zzhe.update("gtm_hits", contentValues, "hit_id=?", new String[]{String.valueOf(j)});
            } catch (SQLiteException e2) {
                zzbo.zzbh(new StringBuilder(69).append("Error setting HIT_FIRST_DISPATCH_TIME for hitId: ").append(j).toString());
                zzv(j);
            }
        }
    }

    /* access modifiers changed from: private */
    public void zzv(long j) {
        zzh(new String[]{String.valueOf(j)});
    }

    public void dispatch() {
        zzbo.v("GTM Dispatch running...");
        if (this.zzbGW.zzQH()) {
            List<zzas> zznH = zznH(40);
            if (zznH.isEmpty()) {
                zzbo.v("...nothing to dispatch");
                this.zzbGX.zzaR(true);
                return;
            }
            this.zzbGW.zzP(zznH);
            if (zzRe() > 0) {
                zzdc.zzRA().dispatch();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int zzRd() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase zzhe = zzhe("Error opening database for getNumStoredHits.");
        if (zzhe != null) {
            try {
                Cursor rawQuery = zzhe.rawQuery("SELECT COUNT(*) from gtm_hits", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e2) {
                zzbo.zzbh("Error getting numStoredHits");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int zzRe() {
        /*
            r10 = this;
            r8 = 0
            r9 = 0
            java.lang.String r0 = "Error opening database for getNumStoredHits."
            android.database.sqlite.SQLiteDatabase r0 = r10.zzhe(r0)
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return r8
        L_0x000b:
            java.lang.String r1 = "gtm_hits"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            r3 = 0
            java.lang.String r4 = "hit_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            r3 = 1
            java.lang.String r4 = "hit_first_send_time"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            java.lang.String r3 = "hit_first_send_time=0"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            int r0 = r1.getCount()     // Catch:{ SQLiteException -> 0x004b, all -> 0x0044 }
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            r8 = r0
            goto L_0x000a
        L_0x002f:
            r0 = move-exception
            r0 = r9
        L_0x0031:
            java.lang.String r1 = "Error getting num untried hits"
            com.google.android.gms.tagmanager.zzbo.zzbh(r1)     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x004e
            r0.close()
            r0 = r8
            goto L_0x002d
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r9 == 0) goto L_0x0043
            r9.close()
        L_0x0043:
            throw r0
        L_0x0044:
            r0 = move-exception
            r9 = r1
            goto L_0x003e
        L_0x0047:
            r1 = move-exception
            r9 = r0
            r0 = r1
            goto L_0x003e
        L_0x004b:
            r0 = move-exception
            r0 = r1
            goto L_0x0031
        L_0x004e:
            r0 = r8
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzcg.zzRe():int");
    }

    public void zzg(long j, String str) {
        zzot();
        zzRc();
        zzh(j, str);
    }

    /* access modifiers changed from: package-private */
    public void zzh(String[] strArr) {
        SQLiteDatabase zzhe;
        boolean z = true;
        if (strArr != null && strArr.length != 0 && (zzhe = zzhe("Error opening database for deleteHits.")) != null) {
            try {
                zzhe.delete("gtm_hits", String.format("HIT_ID in (%s)", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))), strArr);
                zzax zzax = this.zzbGX;
                if (zzRd() != 0) {
                    z = false;
                }
                zzax.zzaR(z);
            } catch (SQLiteException e2) {
                zzbo.zzbh("Error deleting hits");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<String> zznG(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            zzbo.zzbh("Invalid maxHits specified. Skipping");
            return arrayList;
        }
        SQLiteDatabase zzhe = zzhe("Error opening database for peekHitIds.");
        if (zzhe == null) {
            return arrayList;
        }
        try {
            cursor = zzhe.query("gtm_hits", new String[]{"hit_id"}, null, null, null, null, String.format("%s ASC", "hit_id"), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    String valueOf = String.valueOf(e.getMessage());
                    zzbo.zzbh(valueOf.length() != 0 ? "Error in peekHits fetching hitIds: ".concat(valueOf) : new String("Error in peekHits fetching hitIds: "));
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00de, code lost:
        r3 = "Error in peekHits fetching hitIds: ".concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e7, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r3 = new java.lang.String("Error in peekHits fetching hitIds: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f6, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0174, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0175, code lost:
        r12 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0178, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0179, code lost:
        r3 = r2;
        r4 = r13;
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00de A[Catch:{ all -> 0x00f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ec A[SYNTHETIC, Splitter:B:40:0x00ec] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0174 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.google.android.gms.tagmanager.zzas> zznH(int r17) {
        /*
            r16 = this;
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r2 = "Error opening database for peekHits"
            r0 = r16
            android.database.sqlite.SQLiteDatabase r2 = r0.zzhe(r2)
            if (r2 != 0) goto L_0x0011
            r2 = r11
        L_0x0010:
            return r2
        L_0x0011:
            r12 = 0
            java.lang.String r3 = "gtm_hits"
            r4 = 3
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            r5 = 0
            java.lang.String r6 = "hit_id"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            r5 = 1
            java.lang.String r6 = "hit_time"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            r5 = 2
            java.lang.String r6 = "hit_first_send_time"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "%s ASC"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            r13 = 0
            java.lang.String r14 = "hit_id"
            r10[r13] = r14     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            java.lang.String r9 = java.lang.String.format(r9, r10)     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            java.lang.String r10 = java.lang.Integer.toString(r17)     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            android.database.Cursor r13 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x00ca, all -> 0x0172 }
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0178, all -> 0x0174 }
            r12.<init>()     // Catch:{ SQLiteException -> 0x0178, all -> 0x0174 }
            boolean r3 = r13.moveToFirst()     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            if (r3 == 0) goto L_0x0068
        L_0x004b:
            com.google.android.gms.tagmanager.zzas r3 = new com.google.android.gms.tagmanager.zzas     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            r4 = 0
            long r4 = r13.getLong(r4)     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            r6 = 1
            long r6 = r13.getLong(r6)     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            r8 = 2
            long r8 = r13.getLong(r8)     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            r3.<init>(r4, r6, r8)     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            r12.add(r3)     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            boolean r3 = r13.moveToNext()     // Catch:{ SQLiteException -> 0x017e, all -> 0x0174 }
            if (r3 != 0) goto L_0x004b
        L_0x0068:
            if (r13 == 0) goto L_0x006d
            r13.close()
        L_0x006d:
            r11 = 0
            java.lang.String r3 = "gtm_hits"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0170 }
            r5 = 0
            java.lang.String r6 = "hit_id"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0170 }
            r5 = 1
            java.lang.String r6 = "hit_url"
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0170 }
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "%s ASC"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ SQLiteException -> 0x0170 }
            r14 = 0
            java.lang.String r15 = "hit_id"
            r10[r14] = r15     // Catch:{ SQLiteException -> 0x0170 }
            java.lang.String r9 = java.lang.String.format(r9, r10)     // Catch:{ SQLiteException -> 0x0170 }
            java.lang.String r10 = java.lang.Integer.toString(r17)     // Catch:{ SQLiteException -> 0x0170 }
            android.database.Cursor r3 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0170 }
            boolean r2 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            if (r2 == 0) goto L_0x00c2
            r4 = r11
        L_0x009e:
            r0 = r3
            android.database.sqlite.SQLiteCursor r0 = (android.database.sqlite.SQLiteCursor) r0     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            r2 = r0
            android.database.CursorWindow r2 = r2.getWindow()     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            int r2 = r2.getNumRows()     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            if (r2 <= 0) goto L_0x00fa
            java.lang.Object r2 = r12.get(r4)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            com.google.android.gms.tagmanager.zzas r2 = (com.google.android.gms.tagmanager.zzas) r2     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            r5 = 1
            java.lang.String r5 = r3.getString(r5)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            r2.zzhi(r5)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
        L_0x00ba:
            int r2 = r4 + 1
            boolean r4 = r3.moveToNext()     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            if (r4 != 0) goto L_0x0184
        L_0x00c2:
            if (r3 == 0) goto L_0x00c7
            r3.close()
        L_0x00c7:
            r2 = r12
            goto L_0x0010
        L_0x00ca:
            r2 = move-exception
            r3 = r2
            r4 = r12
            r2 = r11
        L_0x00ce:
            java.lang.String r5 = "Error in peekHits fetching hitIds: "
            java.lang.String r3 = r3.getMessage()     // Catch:{ all -> 0x00f2 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x00f2 }
            int r6 = r3.length()     // Catch:{ all -> 0x00f2 }
            if (r6 == 0) goto L_0x00ec
            java.lang.String r3 = r5.concat(r3)     // Catch:{ all -> 0x00f2 }
        L_0x00e2:
            com.google.android.gms.tagmanager.zzbo.zzbh(r3)     // Catch:{ all -> 0x00f2 }
            if (r4 == 0) goto L_0x0010
            r4.close()
            goto L_0x0010
        L_0x00ec:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00f2 }
            r3.<init>(r5)     // Catch:{ all -> 0x00f2 }
            goto L_0x00e2
        L_0x00f2:
            r2 = move-exception
            r12 = r4
        L_0x00f4:
            if (r12 == 0) goto L_0x00f9
            r12.close()
        L_0x00f9:
            throw r2
        L_0x00fa:
            java.lang.String r5 = "HitString for hitId %d too large.  Hit will be deleted."
            r2 = 1
            java.lang.Object[] r6 = new java.lang.Object[r2]     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            r7 = 0
            java.lang.Object r2 = r12.get(r4)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            com.google.android.gms.tagmanager.zzas r2 = (com.google.android.gms.tagmanager.zzas) r2     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            long r8 = r2.zzQP()     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            java.lang.Long r2 = java.lang.Long.valueOf(r8)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            java.lang.String r2 = java.lang.String.format(r5, r6)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            com.google.android.gms.tagmanager.zzbo.zzbh(r2)     // Catch:{ SQLiteException -> 0x0118, all -> 0x016d }
            goto L_0x00ba
        L_0x0118:
            r2 = move-exception
            r13 = r3
        L_0x011a:
            java.lang.String r3 = "Error in peekHits fetching hit url: "
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x0161 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0161 }
            int r4 = r2.length()     // Catch:{ all -> 0x0161 }
            if (r4 == 0) goto L_0x015b
            java.lang.String r2 = r3.concat(r2)     // Catch:{ all -> 0x0161 }
        L_0x012e:
            com.google.android.gms.tagmanager.zzbo.zzbh(r2)     // Catch:{ all -> 0x0161 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0161 }
            r3.<init>()     // Catch:{ all -> 0x0161 }
            r4 = 0
            java.util.Iterator r5 = r12.iterator()     // Catch:{ all -> 0x0161 }
        L_0x013b:
            boolean r2 = r5.hasNext()     // Catch:{ all -> 0x0161 }
            if (r2 == 0) goto L_0x0153
            java.lang.Object r2 = r5.next()     // Catch:{ all -> 0x0161 }
            com.google.android.gms.tagmanager.zzas r2 = (com.google.android.gms.tagmanager.zzas) r2     // Catch:{ all -> 0x0161 }
            java.lang.String r6 = r2.zzQR()     // Catch:{ all -> 0x0161 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0161 }
            if (r6 == 0) goto L_0x0169
            if (r4 == 0) goto L_0x0168
        L_0x0153:
            if (r13 == 0) goto L_0x0158
            r13.close()
        L_0x0158:
            r2 = r3
            goto L_0x0010
        L_0x015b:
            java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x0161 }
            r2.<init>(r3)     // Catch:{ all -> 0x0161 }
            goto L_0x012e
        L_0x0161:
            r2 = move-exception
        L_0x0162:
            if (r13 == 0) goto L_0x0167
            r13.close()
        L_0x0167:
            throw r2
        L_0x0168:
            r4 = 1
        L_0x0169:
            r3.add(r2)     // Catch:{ all -> 0x0161 }
            goto L_0x013b
        L_0x016d:
            r2 = move-exception
            r13 = r3
            goto L_0x0162
        L_0x0170:
            r2 = move-exception
            goto L_0x011a
        L_0x0172:
            r2 = move-exception
            goto L_0x00f4
        L_0x0174:
            r2 = move-exception
            r12 = r13
            goto L_0x00f4
        L_0x0178:
            r2 = move-exception
            r3 = r2
            r4 = r13
            r2 = r11
            goto L_0x00ce
        L_0x017e:
            r2 = move-exception
            r3 = r2
            r4 = r13
            r2 = r12
            goto L_0x00ce
        L_0x0184:
            r4 = r2
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzcg.zznH(int):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public int zzot() {
        boolean z = true;
        long currentTimeMillis = this.zzuP.currentTimeMillis();
        if (currentTimeMillis <= this.zzbGZ + 86400000) {
            return 0;
        }
        this.zzbGZ = currentTimeMillis;
        SQLiteDatabase zzhe = zzhe("Error opening database for deleteStaleHits.");
        if (zzhe == null) {
            return 0;
        }
        int delete = zzhe.delete("gtm_hits", "HIT_TIME < ?", new String[]{Long.toString(this.zzuP.currentTimeMillis() - 2592000000L)});
        zzax zzax = this.zzbGX;
        if (zzRd() != 0) {
            z = false;
        }
        zzax.zzaR(z);
        return delete;
    }
}
