package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzcy extends zzam {
    private static final String ID = zzah.RUNTIME_VERSION.toString();

    public zzcy() {
        super(ID, new String[0]);
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        return zzdl.zzS(65833898L);
    }
}
