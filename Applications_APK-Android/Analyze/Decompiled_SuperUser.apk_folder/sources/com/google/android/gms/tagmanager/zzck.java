package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzck extends zzam {
    private static final String ID = zzah.RANDOM.toString();
    private static final String zzbHo = zzai.MIN.toString();
    private static final String zzbHp = zzai.MAX.toString();

    public zzck() {
        super(ID, new String[0]);
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        double d2;
        double d3;
        zzak.zza zza = map.get(zzbHo);
        zzak.zza zza2 = map.get(zzbHp);
        if (!(zza == null || zza == zzdl.zzRT() || zza2 == null || zza2 == zzdl.zzRT())) {
            zzdk zzf = zzdl.zzf(zza);
            zzdk zzf2 = zzdl.zzf(zza2);
            if (!(zzf == zzdl.zzRR() || zzf2 == zzdl.zzRR())) {
                double doubleValue = zzf.doubleValue();
                d2 = zzf2.doubleValue();
                if (doubleValue <= d2) {
                    d3 = doubleValue;
                    return zzdl.zzS(Long.valueOf(Math.round(((d2 - d3) * Math.random()) + d3)));
                }
            }
        }
        d2 = 2.147483647E9d;
        d3 = 0.0d;
        return zzdl.zzS(Long.valueOf(Math.round(((d2 - d3) * Math.random()) + d3)));
    }
}
