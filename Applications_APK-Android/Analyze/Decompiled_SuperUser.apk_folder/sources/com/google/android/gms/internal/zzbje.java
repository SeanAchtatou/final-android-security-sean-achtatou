package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzi;
import java.util.HashMap;
import java.util.Map;

public class zzbje {
    private final Context mContext;
    private String zzbFB;
    Map<String, Object> zzbLX;
    private final Map<String, Object> zzbLY;
    private final zzbjg zzbNj;
    private final zze zzuP;

    public zzbje(Context context) {
        this(context, new HashMap(), new zzbjg(context), zzi.zzzc());
    }

    zzbje(Context context, Map<String, Object> map, zzbjg zzbjg, zze zze) {
        this.zzbFB = null;
        this.zzbLX = new HashMap();
        this.mContext = context;
        this.zzuP = zze;
        this.zzbNj = zzbjg;
        this.zzbLY = map;
    }

    public void zzig(String str) {
        this.zzbFB = str;
    }
}
