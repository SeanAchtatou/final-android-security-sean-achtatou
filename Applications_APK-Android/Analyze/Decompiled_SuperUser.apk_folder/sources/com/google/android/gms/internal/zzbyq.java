package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InterruptedIOException;

public class zzbyq extends zzbzc {
    /* access modifiers changed from: protected */
    public IOException newTimeoutException(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* access modifiers changed from: protected */
    public void timedOut() {
    }
}
