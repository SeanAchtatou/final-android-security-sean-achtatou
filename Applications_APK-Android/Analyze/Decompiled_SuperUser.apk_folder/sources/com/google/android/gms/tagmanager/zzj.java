package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class zzj extends zzdj {
    private static final String ID = zzah.ARBITRARY_PIXEL.toString();
    private static final String URL = zzai.URL.toString();
    private static final String zzbER = zzai.ADDITIONAL_PARAMS.toString();
    private static final String zzbES = zzai.UNREPEATABLE.toString();
    static final String zzbET;
    private static final Set<String> zzbEU = new HashSet();
    private final Context mContext;
    private final zza zzbEV;

    public interface zza {
        zzat zzQe();
    }

    static {
        String str = ID;
        zzbET = new StringBuilder(String.valueOf(str).length() + 17).append("gtm_").append(str).append("_unrepeatable").toString();
    }

    public zzj(final Context context) {
        this(context, new zza() {
            public zzat zzQe() {
                return zzaa.zzbT(context);
            }
        });
    }

    zzj(Context context, zza zza2) {
        super(ID, URL);
        this.zzbEV = zza2;
        this.mContext = context;
    }

    private synchronized boolean zzgO(String str) {
        boolean z = true;
        synchronized (this) {
            if (!zzgQ(str)) {
                if (zzgP(str)) {
                    zzbEU.add(str);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    public void zzab(Map<String, zzak.zza> map) {
        String zze = map.get(zzbES) != null ? zzdl.zze(map.get(zzbES)) : null;
        if (zze == null || !zzgO(zze)) {
            Uri.Builder buildUpon = Uri.parse(zzdl.zze(map.get(URL))).buildUpon();
            zzak.zza zza2 = map.get(zzbER);
            if (zza2 != null) {
                Object zzj = zzdl.zzj(zza2);
                if (!(zzj instanceof List)) {
                    String valueOf = String.valueOf(buildUpon.build().toString());
                    zzbo.e(valueOf.length() != 0 ? "ArbitraryPixel: additional params not a list: not sending partial hit: ".concat(valueOf) : new String("ArbitraryPixel: additional params not a list: not sending partial hit: "));
                    return;
                }
                for (Object next : (List) zzj) {
                    if (!(next instanceof Map)) {
                        String valueOf2 = String.valueOf(buildUpon.build().toString());
                        zzbo.e(valueOf2.length() != 0 ? "ArbitraryPixel: additional params contains non-map: not sending partial hit: ".concat(valueOf2) : new String("ArbitraryPixel: additional params contains non-map: not sending partial hit: "));
                        return;
                    }
                    for (Map.Entry entry : ((Map) next).entrySet()) {
                        buildUpon.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
                    }
                }
            }
            String uri = buildUpon.build().toString();
            this.zzbEV.zzQe().zzhf(uri);
            String valueOf3 = String.valueOf(uri);
            zzbo.v(valueOf3.length() != 0 ? "ArbitraryPixel: url = ".concat(valueOf3) : new String("ArbitraryPixel: url = "));
            if (zze != null) {
                synchronized (zzj.class) {
                    zzbEU.add(zze);
                    zzdd.zzd(this.mContext, zzbET, zze, "true");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzgP(String str) {
        return this.mContext.getSharedPreferences(zzbET, 0).contains(str);
    }

    /* access modifiers changed from: package-private */
    public boolean zzgQ(String str) {
        return zzbEU.contains(str);
    }
}
