package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzatt;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class zzaul extends zzauh {
    /* access modifiers changed from: private */
    public final zza zzbvD;
    /* access modifiers changed from: private */
    public zzatt zzbvE;
    private Boolean zzbvF;
    private final zzatk zzbvG;
    private final zzauo zzbvH;
    private final List<Runnable> zzbvI = new ArrayList();
    private final zzatk zzbvJ;

    protected class zza implements ServiceConnection, zzf.zzb, zzf.zzc {
        /* access modifiers changed from: private */
        public volatile boolean zzbvR;
        private volatile zzatw zzbvS;

        protected zza() {
        }

        public void onConnected(Bundle bundle) {
            zzac.zzdj("MeasurementServiceConnection.onConnected");
            synchronized (this) {
                try {
                    final zzatt zzatt = (zzatt) this.zzbvS.zzxD();
                    this.zzbvS = null;
                    zzaul.this.zzKk().zzm(new Runnable() {
                        public void run() {
                            synchronized (zza.this) {
                                boolean unused = zza.this.zzbvR = false;
                                if (!zzaul.this.isConnected()) {
                                    zzaul.this.zzKl().zzMe().log("Connected to remote service");
                                    zzaul.this.zza(zzatt);
                                }
                            }
                        }
                    });
                } catch (DeadObjectException | IllegalStateException e2) {
                    this.zzbvS = null;
                    this.zzbvR = false;
                }
            }
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
            zzac.zzdj("MeasurementServiceConnection.onConnectionFailed");
            zzatx zzMv = zzaul.this.zzbqb.zzMv();
            if (zzMv != null) {
                zzMv.zzMb().zzj("Service connection failed", connectionResult);
            }
            synchronized (this) {
                this.zzbvR = false;
                this.zzbvS = null;
            }
            zzaul.this.zzKk().zzm(new Runnable() {
                public void run() {
                    zzatt unused = zzaul.this.zzbvE = (zzatt) null;
                    zzaul.this.zzNc();
                }
            });
        }

        public void onConnectionSuspended(int i) {
            zzac.zzdj("MeasurementServiceConnection.onConnectionSuspended");
            zzaul.this.zzKl().zzMe().log("Service connection suspended");
            zzaul.this.zzKk().zzm(new Runnable() {
                public void run() {
                    zzaul zzaul = zzaul.this;
                    Context context = zzaul.this.getContext();
                    zzaul.this.zzKn().zzLh();
                    zzaul.onServiceDisconnected(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementService"));
                }
            });
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            zzac.zzdj("MeasurementServiceConnection.onServiceConnected");
            synchronized (this) {
                if (iBinder == null) {
                    this.zzbvR = false;
                    zzaul.this.zzKl().zzLZ().log("Service connected with null binder");
                    return;
                }
                final zzatt zzatt = null;
                try {
                    String interfaceDescriptor = iBinder.getInterfaceDescriptor();
                    if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                        zzatt = zzatt.zza.zzes(iBinder);
                        zzaul.this.zzKl().zzMf().log("Bound to IMeasurementService interface");
                    } else {
                        zzaul.this.zzKl().zzLZ().zzj("Got binder with a wrong descriptor", interfaceDescriptor);
                    }
                } catch (RemoteException e2) {
                    zzaul.this.zzKl().zzLZ().log("Service connect failed to get IMeasurementService");
                }
                if (zzatt == null) {
                    this.zzbvR = false;
                    try {
                        com.google.android.gms.common.stats.zza.zzyJ().zza(zzaul.this.getContext(), zzaul.this.zzbvD);
                    } catch (IllegalArgumentException e3) {
                    }
                } else {
                    zzaul.this.zzKk().zzm(new Runnable() {
                        public void run() {
                            synchronized (zza.this) {
                                boolean unused = zza.this.zzbvR = false;
                                if (!zzaul.this.isConnected()) {
                                    zzaul.this.zzKl().zzMf().log("Connected to service");
                                    zzaul.this.zza(zzatt);
                                }
                            }
                        }
                    });
                }
            }
        }

        public void onServiceDisconnected(final ComponentName componentName) {
            zzac.zzdj("MeasurementServiceConnection.onServiceDisconnected");
            zzaul.this.zzKl().zzMe().log("Service disconnected");
            zzaul.this.zzKk().zzm(new Runnable() {
                public void run() {
                    zzaul.this.onServiceDisconnected(componentName);
                }
            });
        }

        public void zzNd() {
            zzaul.this.zzmR();
            Context context = zzaul.this.getContext();
            synchronized (this) {
                if (this.zzbvR) {
                    zzaul.this.zzKl().zzMf().log("Connection attempt already in progress");
                } else if (this.zzbvS != null) {
                    zzaul.this.zzKl().zzMf().log("Already awaiting connection attempt");
                } else {
                    this.zzbvS = new zzatw(context, Looper.getMainLooper(), this, this);
                    zzaul.this.zzKl().zzMf().log("Connecting to remote service");
                    this.zzbvR = true;
                    this.zzbvS.zzxz();
                }
            }
        }

        public void zzz(Intent intent) {
            zzaul.this.zzmR();
            Context context = zzaul.this.getContext();
            com.google.android.gms.common.stats.zza zzyJ = com.google.android.gms.common.stats.zza.zzyJ();
            synchronized (this) {
                if (this.zzbvR) {
                    zzaul.this.zzKl().zzMf().log("Connection attempt already in progress");
                    return;
                }
                this.zzbvR = true;
                zzyJ.zza(context, intent, zzaul.this.zzbvD, 129);
            }
        }
    }

    protected zzaul(zzaue zzaue) {
        super(zzaue);
        this.zzbvH = new zzauo(zzaue.zznR());
        this.zzbvD = new zza();
        this.zzbvG = new zzatk(zzaue) {
            public void run() {
                zzaul.this.zzop();
            }
        };
        this.zzbvJ = new zzatk(zzaue) {
            public void run() {
                zzaul.this.zzKl().zzMb().log("Tasks have been queued for a long time");
            }
        };
    }

    /* access modifiers changed from: private */
    public void onServiceDisconnected(ComponentName componentName) {
        zzmR();
        if (this.zzbvE != null) {
            this.zzbvE = null;
            zzKl().zzMf().zzj("Disconnected from device MeasurementService", componentName);
            zzNb();
        }
    }

    private boolean zzMZ() {
        zzKn().zzLh();
        List<ResolveInfo> queryIntentServices = getContext().getPackageManager().queryIntentServices(new Intent().setClassName(getContext(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
        return queryIntentServices != null && queryIntentServices.size() > 0;
    }

    private void zzNb() {
        zzmR();
        zzoD();
    }

    /* access modifiers changed from: private */
    public void zzNc() {
        zzmR();
        zzKl().zzMf().zzj("Processing queued up service tasks", Integer.valueOf(this.zzbvI.size()));
        for (Runnable zzm : this.zzbvI) {
            zzKk().zzm(zzm);
        }
        this.zzbvI.clear();
        this.zzbvJ.cancel();
    }

    private void zzo(Runnable runnable) {
        zzmR();
        if (isConnected()) {
            runnable.run();
        } else if (((long) this.zzbvI.size()) >= zzKn().zzLn()) {
            zzKl().zzLZ().log("Discarding data. Max runnable queue size reached");
        } else {
            this.zzbvI.add(runnable);
            this.zzbvJ.zzy(60000);
            zzoD();
        }
    }

    /* access modifiers changed from: private */
    public void zzoo() {
        zzmR();
        this.zzbvH.start();
        this.zzbvG.zzy(zzKn().zzpq());
    }

    /* access modifiers changed from: private */
    public void zzop() {
        zzmR();
        if (isConnected()) {
            zzKl().zzMf().log("Inactivity, disconnecting from the service");
            disconnect();
        }
    }

    public void disconnect() {
        zzmR();
        zzob();
        try {
            com.google.android.gms.common.stats.zza.zzyJ().zza(getContext(), this.zzbvD);
        } catch (IllegalArgumentException | IllegalStateException e2) {
        }
        this.zzbvE = null;
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public boolean isConnected() {
        zzmR();
        zzob();
        return this.zzbvE != null;
    }

    public /* bridge */ /* synthetic */ void zzJV() {
        super.zzJV();
    }

    public /* bridge */ /* synthetic */ void zzJW() {
        super.zzJW();
    }

    public /* bridge */ /* synthetic */ void zzJX() {
        super.zzJX();
    }

    public /* bridge */ /* synthetic */ zzatb zzJY() {
        return super.zzJY();
    }

    public /* bridge */ /* synthetic */ zzatf zzJZ() {
        return super.zzJZ();
    }

    public /* bridge */ /* synthetic */ zzauj zzKa() {
        return super.zzKa();
    }

    public /* bridge */ /* synthetic */ zzatu zzKb() {
        return super.zzKb();
    }

    public /* bridge */ /* synthetic */ zzatl zzKc() {
        return super.zzKc();
    }

    public /* bridge */ /* synthetic */ zzaul zzKd() {
        return super.zzKd();
    }

    public /* bridge */ /* synthetic */ zzauk zzKe() {
        return super.zzKe();
    }

    public /* bridge */ /* synthetic */ zzatv zzKf() {
        return super.zzKf();
    }

    public /* bridge */ /* synthetic */ zzatj zzKg() {
        return super.zzKg();
    }

    public /* bridge */ /* synthetic */ zzaut zzKh() {
        return super.zzKh();
    }

    public /* bridge */ /* synthetic */ zzauc zzKi() {
        return super.zzKi();
    }

    public /* bridge */ /* synthetic */ zzaun zzKj() {
        return super.zzKj();
    }

    public /* bridge */ /* synthetic */ zzaud zzKk() {
        return super.zzKk();
    }

    public /* bridge */ /* synthetic */ zzatx zzKl() {
        return super.zzKl();
    }

    public /* bridge */ /* synthetic */ zzaua zzKm() {
        return super.zzKm();
    }

    public /* bridge */ /* synthetic */ zzati zzKn() {
        return super.zzKn();
    }

    /* access modifiers changed from: protected */
    public void zzMT() {
        zzmR();
        zzob();
        zzo(new Runnable() {
            public void run() {
                zzatt zzd = zzaul.this.zzbvE;
                if (zzd == null) {
                    zzaul.this.zzKl().zzLZ().log("Discarding data. Failed to send app launch");
                    return;
                }
                try {
                    zzd.zza(zzaul.this.zzKb().zzfD(zzaul.this.zzKl().zzMg()));
                    zzaul.this.zza(zzd, (com.google.android.gms.common.internal.safeparcel.zza) null);
                    zzaul.this.zzoo();
                } catch (RemoteException e2) {
                    zzaul.this.zzKl().zzLZ().zzj("Failed to send app launch to the service", e2);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zzMY() {
        zzmR();
        zzob();
        zzo(new Runnable() {
            public void run() {
                zzatt zzd = zzaul.this.zzbvE;
                if (zzd == null) {
                    zzaul.this.zzKl().zzLZ().log("Failed to send measurementEnabled to service");
                    return;
                }
                try {
                    zzd.zzb(zzaul.this.zzKb().zzfD(zzaul.this.zzKl().zzMg()));
                    zzaul.this.zzoo();
                } catch (RemoteException e2) {
                    zzaul.this.zzKl().zzLZ().zzj("Failed to send measurementEnabled to the service", e2);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean zzNa() {
        zzmR();
        zzob();
        zzKn().zzLh();
        zzKl().zzMf().log("Checking service availability");
        switch (zze.zzuY().isGooglePlayServicesAvailable(getContext())) {
            case 0:
                zzKl().zzMf().log("Service available");
                return true;
            case 1:
                zzKl().zzMf().log("Service missing");
                return false;
            case 2:
                zzKl().zzMe().log("Service container out of date");
                return true;
            case 3:
                zzKl().zzMb().log("Service disabled");
                return false;
            case 9:
                zzKl().zzMb().log("Service invalid");
                return false;
            case 18:
                zzKl().zzMb().log("Service updating");
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void zza(zzatt zzatt) {
        zzmR();
        zzac.zzw(zzatt);
        this.zzbvE = zzatt;
        zzoo();
        zzNc();
    }

    /* access modifiers changed from: package-private */
    public void zza(zzatt zzatt, com.google.android.gms.common.internal.safeparcel.zza zza2) {
        zzmR();
        zzJW();
        zzob();
        int i = Build.VERSION.SDK_INT;
        zzKn().zzLh();
        ArrayList<com.google.android.gms.common.internal.safeparcel.zza> arrayList = new ArrayList<>();
        zzKn().zzLq();
        int i2 = 100;
        for (int i3 = 0; i3 < 1001 && i2 == 100; i3++) {
            List<com.google.android.gms.common.internal.safeparcel.zza> zzlD = zzKf().zzlD(100);
            if (zzlD != null) {
                arrayList.addAll(zzlD);
                i2 = zzlD.size();
            } else {
                i2 = 0;
            }
            if (zza2 != null && i2 < 100) {
                arrayList.add(zza2);
            }
            for (com.google.android.gms.common.internal.safeparcel.zza zza3 : arrayList) {
                if (zza3 instanceof zzatq) {
                    try {
                        zzatt.zza((zzatq) zza3, zzKb().zzfD(zzKl().zzMg()));
                    } catch (RemoteException e2) {
                        zzKl().zzLZ().zzj("Failed to send event to the service", e2);
                    }
                } else if (zza3 instanceof zzauq) {
                    try {
                        zzatt.zza((zzauq) zza3, zzKb().zzfD(zzKl().zzMg()));
                    } catch (RemoteException e3) {
                        zzKl().zzLZ().zzj("Failed to send attribute to the service", e3);
                    }
                } else if (zza3 instanceof zzatg) {
                    try {
                        zzatt.zza((zzatg) zza3, zzKb().zzfD(zzKl().zzMg()));
                    } catch (RemoteException e4) {
                        zzKl().zzLZ().zzj("Failed to send conditional property to the service", e4);
                    }
                } else {
                    zzKl().zzLZ().log("Discarding data. Unrecognized parcel type.");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void zza(final AppMeasurement.zzf zzf) {
        zzmR();
        zzob();
        zzo(new Runnable() {
            public void run() {
                zzatt zzd = zzaul.this.zzbvE;
                if (zzd == null) {
                    zzaul.this.zzKl().zzLZ().log("Failed to send current screen to service");
                    return;
                }
                try {
                    if (zzf == null) {
                        zzd.zza(0, (String) null, (String) null, zzaul.this.getContext().getPackageName());
                    } else {
                        zzd.zza(zzf.zzbqg, zzf.zzbqe, zzf.zzbqf, zzaul.this.getContext().getPackageName());
                    }
                    zzaul.this.zzoo();
                } catch (RemoteException e2) {
                    zzaul.this.zzKl().zzLZ().zzj("Failed to send current screen to the service", e2);
                }
            }
        });
    }

    public void zza(final AtomicReference<String> atomicReference) {
        zzmR();
        zzob();
        zzo(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                synchronized (atomicReference) {
                    try {
                        zzatt zzd = zzaul.this.zzbvE;
                        if (zzd == null) {
                            zzaul.this.zzKl().zzLZ().log("Failed to get app instance id");
                            atomicReference.notify();
                            return;
                        }
                        atomicReference.set(zzd.zzc(zzaul.this.zzKb().zzfD(null)));
                        zzaul.this.zzoo();
                        atomicReference.notify();
                    } catch (RemoteException e2) {
                        zzaul.this.zzKl().zzLZ().zzj("Failed to get app instance id", e2);
                        atomicReference.notify();
                    } catch (Throwable th) {
                        atomicReference.notify();
                        throw th;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zza(AtomicReference<List<zzatg>> atomicReference, String str, String str2, String str3) {
        zzmR();
        zzob();
        final AtomicReference<List<zzatg>> atomicReference2 = atomicReference;
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        zzo(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                synchronized (atomicReference2) {
                    try {
                        zzatt zzd = zzaul.this.zzbvE;
                        if (zzd == null) {
                            zzaul.this.zzKl().zzLZ().zzd("Failed to get conditional properties", zzatx.zzfE(str4), str5, str6);
                            atomicReference2.set(Collections.emptyList());
                            atomicReference2.notify();
                            return;
                        }
                        if (TextUtils.isEmpty(str4)) {
                            atomicReference2.set(zzd.zza(str5, str6, zzaul.this.zzKb().zzfD(zzaul.this.zzKl().zzMg())));
                        } else {
                            atomicReference2.set(zzd.zzn(str4, str5, str6));
                        }
                        zzaul.this.zzoo();
                        atomicReference2.notify();
                    } catch (RemoteException e2) {
                        zzaul.this.zzKl().zzLZ().zzd("Failed to get conditional properties", zzatx.zzfE(str4), str5, e2);
                        atomicReference2.set(Collections.emptyList());
                        atomicReference2.notify();
                    } catch (Throwable th) {
                        atomicReference2.notify();
                        throw th;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zza(AtomicReference<List<zzauq>> atomicReference, String str, String str2, String str3, boolean z) {
        zzmR();
        zzob();
        final AtomicReference<List<zzauq>> atomicReference2 = atomicReference;
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        final boolean z2 = z;
        zzo(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                synchronized (atomicReference2) {
                    try {
                        zzatt zzd = zzaul.this.zzbvE;
                        if (zzd == null) {
                            zzaul.this.zzKl().zzLZ().zzd("Failed to get user properties", zzatx.zzfE(str4), str5, str6);
                            atomicReference2.set(Collections.emptyList());
                            atomicReference2.notify();
                            return;
                        }
                        if (TextUtils.isEmpty(str4)) {
                            atomicReference2.set(zzd.zza(str5, str6, z2, zzaul.this.zzKb().zzfD(zzaul.this.zzKl().zzMg())));
                        } else {
                            atomicReference2.set(zzd.zza(str4, str5, str6, z2));
                        }
                        zzaul.this.zzoo();
                        atomicReference2.notify();
                    } catch (RemoteException e2) {
                        zzaul.this.zzKl().zzLZ().zzd("Failed to get user properties", zzatx.zzfE(str4), str5, e2);
                        atomicReference2.set(Collections.emptyList());
                        atomicReference2.notify();
                    } catch (Throwable th) {
                        atomicReference2.notify();
                        throw th;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zza(final AtomicReference<List<zzauq>> atomicReference, final boolean z) {
        zzmR();
        zzob();
        zzo(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                synchronized (atomicReference) {
                    try {
                        zzatt zzd = zzaul.this.zzbvE;
                        if (zzd == null) {
                            zzaul.this.zzKl().zzLZ().log("Failed to get user properties");
                            atomicReference.notify();
                            return;
                        }
                        atomicReference.set(zzd.zza(zzaul.this.zzKb().zzfD(null), z));
                        zzaul.this.zzoo();
                        atomicReference.notify();
                    } catch (RemoteException e2) {
                        zzaul.this.zzKl().zzLZ().zzj("Failed to get user properties", e2);
                        atomicReference.notify();
                    } catch (Throwable th) {
                        atomicReference.notify();
                        throw th;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zzb(final zzauq zzauq) {
        zzmR();
        zzob();
        int i = Build.VERSION.SDK_INT;
        zzKn().zzLh();
        final boolean z = zzKf().zza(zzauq);
        zzo(new Runnable() {
            public void run() {
                zzatt zzd = zzaul.this.zzbvE;
                if (zzd == null) {
                    zzaul.this.zzKl().zzLZ().log("Discarding data. Failed to set user attribute");
                    return;
                }
                zzaul.this.zza(zzd, z ? null : zzauq);
                zzaul.this.zzoo();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zzc(zzatq zzatq, String str) {
        zzac.zzw(zzatq);
        zzmR();
        zzob();
        int i = Build.VERSION.SDK_INT;
        zzKn().zzLh();
        final boolean z = zzKf().zza(zzatq);
        final zzatq zzatq2 = zzatq;
        final String str2 = str;
        zzo(new Runnable(true) {
            public void run() {
                zzatt zzd = zzaul.this.zzbvE;
                if (zzd == null) {
                    zzaul.this.zzKl().zzLZ().log("Discarding data. Failed to send event to service");
                    return;
                }
                if (true) {
                    zzaul.this.zza(zzd, z ? null : zzatq2);
                } else {
                    try {
                        if (TextUtils.isEmpty(str2)) {
                            zzd.zza(zzatq2, zzaul.this.zzKb().zzfD(zzaul.this.zzKl().zzMg()));
                        } else {
                            zzd.zza(zzatq2, str2, zzaul.this.zzKl().zzMg());
                        }
                    } catch (RemoteException e2) {
                        zzaul.this.zzKl().zzLZ().zzj("Failed to send event to the service", e2);
                    }
                }
                zzaul.this.zzoo();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zzf(zzatg zzatg) {
        zzac.zzw(zzatg);
        zzmR();
        zzob();
        zzKn().zzLh();
        final boolean z = zzKf().zzc(zzatg);
        final zzatg zzatg2 = new zzatg(zzatg);
        final zzatg zzatg3 = zzatg;
        zzo(new Runnable(true) {
            public void run() {
                zzatt zzd = zzaul.this.zzbvE;
                if (zzd == null) {
                    zzaul.this.zzKl().zzLZ().log("Discarding data. Failed to send conditional user property to service");
                    return;
                }
                if (true) {
                    zzaul.this.zza(zzd, z ? null : zzatg2);
                } else {
                    try {
                        if (TextUtils.isEmpty(zzatg3.packageName)) {
                            zzd.zza(zzatg2, zzaul.this.zzKb().zzfD(zzaul.this.zzKl().zzMg()));
                        } else {
                            zzd.zzb(zzatg2);
                        }
                    } catch (RemoteException e2) {
                        zzaul.this.zzKl().zzLZ().zzj("Failed to send conditional user property to the service", e2);
                    }
                }
                zzaul.this.zzoo();
            }
        });
    }

    public /* bridge */ /* synthetic */ void zzmR() {
        super.zzmR();
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
    }

    public /* bridge */ /* synthetic */ com.google.android.gms.common.util.zze zznR() {
        return super.zznR();
    }

    /* access modifiers changed from: package-private */
    public void zzoD() {
        zzmR();
        zzob();
        if (!isConnected()) {
            if (this.zzbvF == null) {
                this.zzbvF = zzKm().zzMn();
                if (this.zzbvF == null) {
                    zzKl().zzMf().log("State of service unknown");
                    this.zzbvF = Boolean.valueOf(zzNa());
                    zzKm().zzaJ(this.zzbvF.booleanValue());
                }
            }
            if (this.zzbvF.booleanValue()) {
                zzKl().zzMf().log("Using measurement service");
                this.zzbvD.zzNd();
            } else if (zzMZ()) {
                zzKl().zzMf().log("Using local app measurement service");
                Intent intent = new Intent("com.google.android.gms.measurement.START");
                Context context = getContext();
                zzKn().zzLh();
                intent.setComponent(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementService"));
                this.zzbvD.zzz(intent);
            } else {
                zzKl().zzLZ().log("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }
}
