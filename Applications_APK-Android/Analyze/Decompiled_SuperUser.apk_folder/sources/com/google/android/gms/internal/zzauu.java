package com.google.android.gms.internal;

public interface zzauu {

    public static final class zza extends zzbyd<zza> {
        private static volatile zza[] zzbwm;
        public Integer zzbwn;
        public zze[] zzbwo;
        public zzb[] zzbwp;

        public zza() {
            zzNm();
        }

        public static zza[] zzNl() {
            if (zzbwm == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbwm == null) {
                        zzbwm = new zza[0];
                    }
                }
            }
            return zzbwm;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.zzbwn == null) {
                if (zza.zzbwn != null) {
                    return false;
                }
            } else if (!this.zzbwn.equals(zza.zzbwn)) {
                return false;
            }
            if (!zzbyh.equals(this.zzbwo, zza.zzbwo) || !zzbyh.equals(this.zzbwp, zza.zzbwp)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zza.zzcwC == null || zza.zzcwC.isEmpty() : this.zzcwC.equals(zza.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((((this.zzbwn == null ? 0 : this.zzbwn.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + zzbyh.hashCode(this.zzbwo)) * 31) + zzbyh.hashCode(this.zzbwp)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzG */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbwn = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 18:
                        int zzb = zzbym.zzb(zzbyb, 18);
                        int length = this.zzbwo == null ? 0 : this.zzbwo.length;
                        zze[] zzeArr = new zze[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbwo, 0, zzeArr, 0, length);
                        }
                        while (length < zzeArr.length - 1) {
                            zzeArr[length] = new zze();
                            zzbyb.zza(zzeArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzeArr[length] = new zze();
                        zzbyb.zza(zzeArr[length]);
                        this.zzbwo = zzeArr;
                        break;
                    case 26:
                        int zzb2 = zzbym.zzb(zzbyb, 26);
                        int length2 = this.zzbwp == null ? 0 : this.zzbwp.length;
                        zzb[] zzbArr = new zzb[(zzb2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzbwp, 0, zzbArr, 0, length2);
                        }
                        while (length2 < zzbArr.length - 1) {
                            zzbArr[length2] = new zzb();
                            zzbyb.zza(zzbArr[length2]);
                            zzbyb.zzaeW();
                            length2++;
                        }
                        zzbArr[length2] = new zzb();
                        zzbyb.zza(zzbArr[length2]);
                        this.zzbwp = zzbArr;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zza zzNm() {
            this.zzbwn = null;
            this.zzbwo = zze.zzNs();
            this.zzbwp = zzb.zzNn();
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwn != null) {
                zzbyc.zzJ(1, this.zzbwn.intValue());
            }
            if (this.zzbwo != null && this.zzbwo.length > 0) {
                for (zze zze : this.zzbwo) {
                    if (zze != null) {
                        zzbyc.zza(2, zze);
                    }
                }
            }
            if (this.zzbwp != null && this.zzbwp.length > 0) {
                for (zzb zzb : this.zzbwp) {
                    if (zzb != null) {
                        zzbyc.zza(3, zzb);
                    }
                }
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwn != null) {
                zzu += zzbyc.zzL(1, this.zzbwn.intValue());
            }
            if (this.zzbwo != null && this.zzbwo.length > 0) {
                int i = zzu;
                for (zze zze : this.zzbwo) {
                    if (zze != null) {
                        i += zzbyc.zzc(2, zze);
                    }
                }
                zzu = i;
            }
            if (this.zzbwp != null && this.zzbwp.length > 0) {
                for (zzb zzb : this.zzbwp) {
                    if (zzb != null) {
                        zzu += zzbyc.zzc(3, zzb);
                    }
                }
            }
            return zzu;
        }
    }

    public static final class zzb extends zzbyd<zzb> {
        private static volatile zzb[] zzbwq;
        public Integer zzbwr;
        public String zzbws;
        public zzc[] zzbwt;
        public Boolean zzbwu;
        public zzd zzbwv;

        public zzb() {
            zzNo();
        }

        public static zzb[] zzNn() {
            if (zzbwq == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbwq == null) {
                        zzbwq = new zzb[0];
                    }
                }
            }
            return zzbwq;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            if (this.zzbwr == null) {
                if (zzb.zzbwr != null) {
                    return false;
                }
            } else if (!this.zzbwr.equals(zzb.zzbwr)) {
                return false;
            }
            if (this.zzbws == null) {
                if (zzb.zzbws != null) {
                    return false;
                }
            } else if (!this.zzbws.equals(zzb.zzbws)) {
                return false;
            }
            if (!zzbyh.equals(this.zzbwt, zzb.zzbwt)) {
                return false;
            }
            if (this.zzbwu == null) {
                if (zzb.zzbwu != null) {
                    return false;
                }
            } else if (!this.zzbwu.equals(zzb.zzbwu)) {
                return false;
            }
            if (this.zzbwv == null) {
                if (zzb.zzbwv != null) {
                    return false;
                }
            } else if (!this.zzbwv.equals(zzb.zzbwv)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzb.zzcwC == null || zzb.zzcwC.isEmpty() : this.zzcwC.equals(zzb.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwv == null ? 0 : this.zzbwv.hashCode()) + (((this.zzbwu == null ? 0 : this.zzbwu.hashCode()) + (((((this.zzbws == null ? 0 : this.zzbws.hashCode()) + (((this.zzbwr == null ? 0 : this.zzbwr.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31) + zzbyh.hashCode(this.zzbwt)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzH */
        public zzb zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbwr = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 18:
                        this.zzbws = zzbyb.readString();
                        break;
                    case 26:
                        int zzb = zzbym.zzb(zzbyb, 26);
                        int length = this.zzbwt == null ? 0 : this.zzbwt.length;
                        zzc[] zzcArr = new zzc[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbwt, 0, zzcArr, 0, length);
                        }
                        while (length < zzcArr.length - 1) {
                            zzcArr[length] = new zzc();
                            zzbyb.zza(zzcArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzcArr[length] = new zzc();
                        zzbyb.zza(zzcArr[length]);
                        this.zzbwt = zzcArr;
                        break;
                    case 32:
                        this.zzbwu = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 42:
                        if (this.zzbwv == null) {
                            this.zzbwv = new zzd();
                        }
                        zzbyb.zza(this.zzbwv);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zzb zzNo() {
            this.zzbwr = null;
            this.zzbws = null;
            this.zzbwt = zzc.zzNp();
            this.zzbwu = null;
            this.zzbwv = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwr != null) {
                zzbyc.zzJ(1, this.zzbwr.intValue());
            }
            if (this.zzbws != null) {
                zzbyc.zzq(2, this.zzbws);
            }
            if (this.zzbwt != null && this.zzbwt.length > 0) {
                for (zzc zzc : this.zzbwt) {
                    if (zzc != null) {
                        zzbyc.zza(3, zzc);
                    }
                }
            }
            if (this.zzbwu != null) {
                zzbyc.zzg(4, this.zzbwu.booleanValue());
            }
            if (this.zzbwv != null) {
                zzbyc.zza(5, this.zzbwv);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwr != null) {
                zzu += zzbyc.zzL(1, this.zzbwr.intValue());
            }
            if (this.zzbws != null) {
                zzu += zzbyc.zzr(2, this.zzbws);
            }
            if (this.zzbwt != null && this.zzbwt.length > 0) {
                int i = zzu;
                for (zzc zzc : this.zzbwt) {
                    if (zzc != null) {
                        i += zzbyc.zzc(3, zzc);
                    }
                }
                zzu = i;
            }
            if (this.zzbwu != null) {
                zzu += zzbyc.zzh(4, this.zzbwu.booleanValue());
            }
            return this.zzbwv != null ? zzu + zzbyc.zzc(5, this.zzbwv) : zzu;
        }
    }

    public static final class zzc extends zzbyd<zzc> {
        private static volatile zzc[] zzbww;
        public String zzbwA;
        public zzf zzbwx;
        public zzd zzbwy;
        public Boolean zzbwz;

        public zzc() {
            zzNq();
        }

        public static zzc[] zzNp() {
            if (zzbww == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbww == null) {
                        zzbww = new zzc[0];
                    }
                }
            }
            return zzbww;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzc)) {
                return false;
            }
            zzc zzc = (zzc) obj;
            if (this.zzbwx == null) {
                if (zzc.zzbwx != null) {
                    return false;
                }
            } else if (!this.zzbwx.equals(zzc.zzbwx)) {
                return false;
            }
            if (this.zzbwy == null) {
                if (zzc.zzbwy != null) {
                    return false;
                }
            } else if (!this.zzbwy.equals(zzc.zzbwy)) {
                return false;
            }
            if (this.zzbwz == null) {
                if (zzc.zzbwz != null) {
                    return false;
                }
            } else if (!this.zzbwz.equals(zzc.zzbwz)) {
                return false;
            }
            if (this.zzbwA == null) {
                if (zzc.zzbwA != null) {
                    return false;
                }
            } else if (!this.zzbwA.equals(zzc.zzbwA)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzc.zzcwC == null || zzc.zzcwC.isEmpty() : this.zzcwC.equals(zzc.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwA == null ? 0 : this.zzbwA.hashCode()) + (((this.zzbwz == null ? 0 : this.zzbwz.hashCode()) + (((this.zzbwy == null ? 0 : this.zzbwy.hashCode()) + (((this.zzbwx == null ? 0 : this.zzbwx.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzI */
        public zzc zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        if (this.zzbwx == null) {
                            this.zzbwx = new zzf();
                        }
                        zzbyb.zza(this.zzbwx);
                        break;
                    case 18:
                        if (this.zzbwy == null) {
                            this.zzbwy = new zzd();
                        }
                        zzbyb.zza(this.zzbwy);
                        break;
                    case 24:
                        this.zzbwz = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 34:
                        this.zzbwA = zzbyb.readString();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zzc zzNq() {
            this.zzbwx = null;
            this.zzbwy = null;
            this.zzbwz = null;
            this.zzbwA = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwx != null) {
                zzbyc.zza(1, this.zzbwx);
            }
            if (this.zzbwy != null) {
                zzbyc.zza(2, this.zzbwy);
            }
            if (this.zzbwz != null) {
                zzbyc.zzg(3, this.zzbwz.booleanValue());
            }
            if (this.zzbwA != null) {
                zzbyc.zzq(4, this.zzbwA);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwx != null) {
                zzu += zzbyc.zzc(1, this.zzbwx);
            }
            if (this.zzbwy != null) {
                zzu += zzbyc.zzc(2, this.zzbwy);
            }
            if (this.zzbwz != null) {
                zzu += zzbyc.zzh(3, this.zzbwz.booleanValue());
            }
            return this.zzbwA != null ? zzu + zzbyc.zzr(4, this.zzbwA) : zzu;
        }
    }

    public static final class zzd extends zzbyd<zzd> {
        public Integer zzbwB;
        public Boolean zzbwC;
        public String zzbwD;
        public String zzbwE;
        public String zzbwF;

        public zzd() {
            zzNr();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzd)) {
                return false;
            }
            zzd zzd = (zzd) obj;
            if (this.zzbwB == null) {
                if (zzd.zzbwB != null) {
                    return false;
                }
            } else if (!this.zzbwB.equals(zzd.zzbwB)) {
                return false;
            }
            if (this.zzbwC == null) {
                if (zzd.zzbwC != null) {
                    return false;
                }
            } else if (!this.zzbwC.equals(zzd.zzbwC)) {
                return false;
            }
            if (this.zzbwD == null) {
                if (zzd.zzbwD != null) {
                    return false;
                }
            } else if (!this.zzbwD.equals(zzd.zzbwD)) {
                return false;
            }
            if (this.zzbwE == null) {
                if (zzd.zzbwE != null) {
                    return false;
                }
            } else if (!this.zzbwE.equals(zzd.zzbwE)) {
                return false;
            }
            if (this.zzbwF == null) {
                if (zzd.zzbwF != null) {
                    return false;
                }
            } else if (!this.zzbwF.equals(zzd.zzbwF)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzd.zzcwC == null || zzd.zzcwC.isEmpty() : this.zzcwC.equals(zzd.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwF == null ? 0 : this.zzbwF.hashCode()) + (((this.zzbwE == null ? 0 : this.zzbwE.hashCode()) + (((this.zzbwD == null ? 0 : this.zzbwD.hashCode()) + (((this.zzbwC == null ? 0 : this.zzbwC.hashCode()) + (((this.zzbwB == null ? 0 : this.zzbwB.intValue()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzJ */
        public zzd zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                this.zzbwB = Integer.valueOf(zzafa);
                                continue;
                        }
                    case 16:
                        this.zzbwC = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 26:
                        this.zzbwD = zzbyb.readString();
                        break;
                    case 34:
                        this.zzbwE = zzbyb.readString();
                        break;
                    case 42:
                        this.zzbwF = zzbyb.readString();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zzd zzNr() {
            this.zzbwC = null;
            this.zzbwD = null;
            this.zzbwE = null;
            this.zzbwF = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwB != null) {
                zzbyc.zzJ(1, this.zzbwB.intValue());
            }
            if (this.zzbwC != null) {
                zzbyc.zzg(2, this.zzbwC.booleanValue());
            }
            if (this.zzbwD != null) {
                zzbyc.zzq(3, this.zzbwD);
            }
            if (this.zzbwE != null) {
                zzbyc.zzq(4, this.zzbwE);
            }
            if (this.zzbwF != null) {
                zzbyc.zzq(5, this.zzbwF);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwB != null) {
                zzu += zzbyc.zzL(1, this.zzbwB.intValue());
            }
            if (this.zzbwC != null) {
                zzu += zzbyc.zzh(2, this.zzbwC.booleanValue());
            }
            if (this.zzbwD != null) {
                zzu += zzbyc.zzr(3, this.zzbwD);
            }
            if (this.zzbwE != null) {
                zzu += zzbyc.zzr(4, this.zzbwE);
            }
            return this.zzbwF != null ? zzu + zzbyc.zzr(5, this.zzbwF) : zzu;
        }
    }

    public static final class zze extends zzbyd<zze> {
        private static volatile zze[] zzbwG;
        public String zzbwH;
        public zzc zzbwI;
        public Integer zzbwr;

        public zze() {
            zzNt();
        }

        public static zze[] zzNs() {
            if (zzbwG == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbwG == null) {
                        zzbwG = new zze[0];
                    }
                }
            }
            return zzbwG;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zze)) {
                return false;
            }
            zze zze = (zze) obj;
            if (this.zzbwr == null) {
                if (zze.zzbwr != null) {
                    return false;
                }
            } else if (!this.zzbwr.equals(zze.zzbwr)) {
                return false;
            }
            if (this.zzbwH == null) {
                if (zze.zzbwH != null) {
                    return false;
                }
            } else if (!this.zzbwH.equals(zze.zzbwH)) {
                return false;
            }
            if (this.zzbwI == null) {
                if (zze.zzbwI != null) {
                    return false;
                }
            } else if (!this.zzbwI.equals(zze.zzbwI)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zze.zzcwC == null || zze.zzcwC.isEmpty() : this.zzcwC.equals(zze.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwI == null ? 0 : this.zzbwI.hashCode()) + (((this.zzbwH == null ? 0 : this.zzbwH.hashCode()) + (((this.zzbwr == null ? 0 : this.zzbwr.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzK */
        public zze zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbwr = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 18:
                        this.zzbwH = zzbyb.readString();
                        break;
                    case 26:
                        if (this.zzbwI == null) {
                            this.zzbwI = new zzc();
                        }
                        zzbyb.zza(this.zzbwI);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zze zzNt() {
            this.zzbwr = null;
            this.zzbwH = null;
            this.zzbwI = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwr != null) {
                zzbyc.zzJ(1, this.zzbwr.intValue());
            }
            if (this.zzbwH != null) {
                zzbyc.zzq(2, this.zzbwH);
            }
            if (this.zzbwI != null) {
                zzbyc.zza(3, this.zzbwI);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwr != null) {
                zzu += zzbyc.zzL(1, this.zzbwr.intValue());
            }
            if (this.zzbwH != null) {
                zzu += zzbyc.zzr(2, this.zzbwH);
            }
            return this.zzbwI != null ? zzu + zzbyc.zzc(3, this.zzbwI) : zzu;
        }
    }

    public static final class zzf extends zzbyd<zzf> {
        public Integer zzbwJ;
        public String zzbwK;
        public Boolean zzbwL;
        public String[] zzbwM;

        public zzf() {
            zzNu();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzf)) {
                return false;
            }
            zzf zzf = (zzf) obj;
            if (this.zzbwJ == null) {
                if (zzf.zzbwJ != null) {
                    return false;
                }
            } else if (!this.zzbwJ.equals(zzf.zzbwJ)) {
                return false;
            }
            if (this.zzbwK == null) {
                if (zzf.zzbwK != null) {
                    return false;
                }
            } else if (!this.zzbwK.equals(zzf.zzbwK)) {
                return false;
            }
            if (this.zzbwL == null) {
                if (zzf.zzbwL != null) {
                    return false;
                }
            } else if (!this.zzbwL.equals(zzf.zzbwL)) {
                return false;
            }
            if (zzbyh.equals(this.zzbwM, zzf.zzbwM)) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzf.zzcwC == null || zzf.zzcwC.isEmpty() : this.zzcwC.equals(zzf.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((this.zzbwL == null ? 0 : this.zzbwL.hashCode()) + (((this.zzbwK == null ? 0 : this.zzbwK.hashCode()) + (((this.zzbwJ == null ? 0 : this.zzbwJ.intValue()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31) + zzbyh.hashCode(this.zzbwM)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzL */
        public zzf zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                this.zzbwJ = Integer.valueOf(zzafa);
                                continue;
                        }
                    case 18:
                        this.zzbwK = zzbyb.readString();
                        break;
                    case 24:
                        this.zzbwL = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 34:
                        int zzb = zzbym.zzb(zzbyb, 34);
                        int length = this.zzbwM == null ? 0 : this.zzbwM.length;
                        String[] strArr = new String[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbwM, 0, strArr, 0, length);
                        }
                        while (length < strArr.length - 1) {
                            strArr[length] = zzbyb.readString();
                            zzbyb.zzaeW();
                            length++;
                        }
                        strArr[length] = zzbyb.readString();
                        this.zzbwM = strArr;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zzf zzNu() {
            this.zzbwK = null;
            this.zzbwL = null;
            this.zzbwM = zzbym.EMPTY_STRING_ARRAY;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwJ != null) {
                zzbyc.zzJ(1, this.zzbwJ.intValue());
            }
            if (this.zzbwK != null) {
                zzbyc.zzq(2, this.zzbwK);
            }
            if (this.zzbwL != null) {
                zzbyc.zzg(3, this.zzbwL.booleanValue());
            }
            if (this.zzbwM != null && this.zzbwM.length > 0) {
                for (String str : this.zzbwM) {
                    if (str != null) {
                        zzbyc.zzq(4, str);
                    }
                }
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwJ != null) {
                zzu += zzbyc.zzL(1, this.zzbwJ.intValue());
            }
            if (this.zzbwK != null) {
                zzu += zzbyc.zzr(2, this.zzbwK);
            }
            if (this.zzbwL != null) {
                zzu += zzbyc.zzh(3, this.zzbwL.booleanValue());
            }
            if (this.zzbwM == null || this.zzbwM.length <= 0) {
                return zzu;
            }
            int i = 0;
            int i2 = 0;
            for (String str : this.zzbwM) {
                if (str != null) {
                    i2++;
                    i += zzbyc.zzku(str);
                }
            }
            return zzu + i + (i2 * 1);
        }
    }
}
