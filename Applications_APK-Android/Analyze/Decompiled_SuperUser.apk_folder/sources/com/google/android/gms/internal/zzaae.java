package com.google.android.gms.internal;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.internal.zzaaz;

public abstract class zzaae extends zzabe implements DialogInterface.OnCancelListener {
    protected boolean mStarted;
    /* access modifiers changed from: private */
    public ConnectionResult zzaAa;
    /* access modifiers changed from: private */
    public int zzaAb;
    private final Handler zzaAc;
    protected boolean zzazZ;
    protected final GoogleApiAvailability zzazn;

    private class zza implements Runnable {
        private zza() {
        }

        public void run() {
            if (zzaae.this.mStarted) {
                if (zzaae.this.zzaAa.hasResolution()) {
                    zzaae.this.zzaCR.startActivityForResult(GoogleApiActivity.zzb(zzaae.this.getActivity(), zzaae.this.zzaAa.getResolution(), zzaae.this.zzaAb, false), 1);
                } else if (zzaae.this.zzazn.isUserResolvableError(zzaae.this.zzaAa.getErrorCode())) {
                    zzaae.this.zzazn.zza(zzaae.this.getActivity(), zzaae.this.zzaCR, zzaae.this.zzaAa.getErrorCode(), 2, zzaae.this);
                } else if (zzaae.this.zzaAa.getErrorCode() == 18) {
                    final Dialog zza = zzaae.this.zzazn.zza(zzaae.this.getActivity(), zzaae.this);
                    zzaae.this.zzazn.zza(zzaae.this.getActivity().getApplicationContext(), new zzaaz.zza() {
                        public void zzvE() {
                            zzaae.this.zzvD();
                            if (zza.isShowing()) {
                                zza.dismiss();
                            }
                        }
                    });
                } else {
                    zzaae.this.zza(zzaae.this.zzaAa, zzaae.this.zzaAb);
                }
            }
        }
    }

    protected zzaae(zzabf zzabf) {
        this(zzabf, GoogleApiAvailability.getInstance());
    }

    zzaae(zzabf zzabf, GoogleApiAvailability googleApiAvailability) {
        super(zzabf);
        this.zzaAb = -1;
        this.zzaAc = new Handler(Looper.getMainLooper());
        this.zzazn = googleApiAvailability;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onActivityResult(int i, int i2, Intent intent) {
        boolean z = true;
        switch (i) {
            case 1:
                if (i2 != -1) {
                    if (i2 == 0) {
                        this.zzaAa = new ConnectionResult(intent != null ? intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13) : 13, null);
                    }
                    z = false;
                    break;
                }
                break;
            case 2:
                int isGooglePlayServicesAvailable = this.zzazn.isGooglePlayServicesAvailable(getActivity());
                if (isGooglePlayServicesAvailable != 0) {
                    z = false;
                }
                if (this.zzaAa.getErrorCode() == 18 && isGooglePlayServicesAvailable == 18) {
                    return;
                }
            default:
                z = false;
                break;
        }
        if (z) {
            zzvD();
        } else {
            zza(this.zzaAa, this.zzaAb);
        }
    }

    public void onCancel(DialogInterface dialogInterface) {
        zza(new ConnectionResult(13, null), this.zzaAb);
        zzvD();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.zzazZ = bundle.getBoolean("resolving_error", false);
            if (this.zzazZ) {
                this.zzaAb = bundle.getInt("failed_client_id", -1);
                this.zzaAa = new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution"));
            }
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("resolving_error", this.zzazZ);
        if (this.zzazZ) {
            bundle.putInt("failed_client_id", this.zzaAb);
            bundle.putInt("failed_status", this.zzaAa.getErrorCode());
            bundle.putParcelable("failed_resolution", this.zzaAa.getResolution());
        }
    }

    public void onStart() {
        super.onStart();
        this.mStarted = true;
    }

    public void onStop() {
        super.onStop();
        this.mStarted = false;
    }

    /* access modifiers changed from: protected */
    public abstract void zza(ConnectionResult connectionResult, int i);

    public void zzb(ConnectionResult connectionResult, int i) {
        if (!this.zzazZ) {
            this.zzazZ = true;
            this.zzaAb = i;
            this.zzaAa = connectionResult;
            this.zzaAc.post(new zza());
        }
    }

    /* access modifiers changed from: protected */
    public void zzvD() {
        this.zzaAb = -1;
        this.zzazZ = false;
        this.zzaAa = null;
        zzvx();
    }

    /* access modifiers changed from: protected */
    public abstract void zzvx();
}
