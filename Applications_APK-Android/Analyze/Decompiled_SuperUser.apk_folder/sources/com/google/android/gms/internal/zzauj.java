package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzf;
import com.google.android.gms.internal.zzauk;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

public class zzauj extends zzauh {
    protected zza zzbuY;
    private AppMeasurement.zzb zzbuZ;
    private final Set<AppMeasurement.zzc> zzbva = new CopyOnWriteArraySet();
    private boolean zzbvb;
    private String zzbvc = null;
    private String zzbvd = null;

    @TargetApi(14)
    private class zza implements Application.ActivityLifecycleCallbacks {
        private zza() {
        }

        private boolean zzfR(String str) {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            zzauj.this.zzd("auto", "_ldl", str);
            return true;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            Uri data;
            try {
                zzauj.this.zzKl().zzMf().log("onActivityCreated");
                Intent intent = activity.getIntent();
                if (!(intent == null || (data = intent.getData()) == null || !data.isHierarchical())) {
                    if (bundle == null) {
                        Bundle zzu = zzauj.this.zzKh().zzu(data);
                        String str = zzauj.this.zzKh().zzA(intent) ? "gs" : "auto";
                        if (zzu != null) {
                            zzauj.this.zze(str, "_cmp", zzu);
                        }
                    }
                    String queryParameter = data.getQueryParameter("referrer");
                    if (!TextUtils.isEmpty(queryParameter)) {
                        if (!(queryParameter.contains("gclid") && (queryParameter.contains("utm_campaign") || queryParameter.contains("utm_source") || queryParameter.contains("utm_medium") || queryParameter.contains("utm_term") || queryParameter.contains("utm_content")))) {
                            zzauj.this.zzKl().zzMe().log("Activity created with data 'referrer' param without gclid and at least one utm field");
                            return;
                        } else {
                            zzauj.this.zzKl().zzMe().zzj("Activity created with referrer", queryParameter);
                            zzfR(queryParameter);
                        }
                    } else {
                        return;
                    }
                }
            } catch (Throwable th) {
                zzauj.this.zzKl().zzLZ().zzj("Throwable caught in onActivityCreated", th);
            }
            zzauj.this.zzKe().onActivityCreated(activity, bundle);
        }

        public void onActivityDestroyed(Activity activity) {
            zzauj.this.zzKe().onActivityDestroyed(activity);
        }

        public void onActivityPaused(Activity activity) {
            zzauj.this.zzKe().onActivityPaused(activity);
            zzauj.this.zzKj().zzNg();
        }

        public void onActivityResumed(Activity activity) {
            zzauj.this.zzKe().onActivityResumed(activity);
            zzauj.this.zzKj().zzNe();
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            zzauj.this.zzKe().onActivitySaveInstanceState(activity, bundle);
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    protected zzauj(zzaue zzaue) {
        super(zzaue);
    }

    private Bundle zzM(Bundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        Bundle bundle2 = new Bundle(bundle);
        for (String next : bundle2.keySet()) {
            Object obj = bundle2.get(next);
            if (obj instanceof Bundle) {
                bundle2.putBundle(next, new Bundle((Bundle) obj));
            } else if (obj instanceof Parcelable[]) {
                Parcelable[] parcelableArr = (Parcelable[]) obj;
                for (int i = 0; i < parcelableArr.length; i++) {
                    if (parcelableArr[i] instanceof Bundle) {
                        parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                    }
                }
            } else if (obj instanceof ArrayList) {
                ArrayList arrayList = (ArrayList) obj;
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    Object obj2 = arrayList.get(i2);
                    if (obj2 instanceof Bundle) {
                        arrayList.set(i2, new Bundle((Bundle) obj2));
                    }
                }
            }
        }
        return bundle2;
    }

    private void zzMU() {
        try {
            zzf(Class.forName(zzMV()));
        } catch (ClassNotFoundException e2) {
            zzKl().zzMd().log("Tag Manager is not found and thus will not be used");
        }
    }

    private String zzMV() {
        return "com.google.android.gms.tagmanager.TagManagerService";
    }

    private void zza(final AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        long currentTimeMillis = zznR().currentTimeMillis();
        zzac.zzw(conditionalUserProperty);
        zzac.zzdr(conditionalUserProperty.mName);
        zzac.zzdr(conditionalUserProperty.mOrigin);
        zzac.zzw(conditionalUserProperty.mValue);
        conditionalUserProperty.mCreationTimestamp = currentTimeMillis;
        String str = conditionalUserProperty.mName;
        Object obj = conditionalUserProperty.mValue;
        if (zzKh().zzfX(str) != 0) {
            zzKl().zzLZ().zzj("Invalid conditional user property name", str);
        } else if (zzKh().zzl(str, obj) != 0) {
            zzKl().zzLZ().zze("Invalid conditional user property value", str, obj);
        } else {
            Object zzm = zzKh().zzm(str, obj);
            if (zzm == null) {
                zzKl().zzLZ().zze("Unable to normalize conditional user property value", str, obj);
                return;
            }
            conditionalUserProperty.mValue = zzm;
            long j = conditionalUserProperty.mTriggerTimeout;
            if (j > zzKn().zzLb() || j < 1) {
                zzKl().zzLZ().zze("Invalid conditional user property timeout", str, Long.valueOf(j));
                return;
            }
            long j2 = conditionalUserProperty.mTimeToLive;
            if (j2 > zzKn().zzLc() || j2 < 1) {
                zzKl().zzLZ().zze("Invalid conditional user property time to live", str, Long.valueOf(j2));
            } else {
                zzKk().zzm(new Runnable() {
                    public void run() {
                        zzauj.this.zzb(conditionalUserProperty);
                    }
                });
            }
        }
    }

    private void zza(String str, String str2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        zza(str, str2, zznR().currentTimeMillis(), bundle, z, z2, z3, str3);
    }

    /* access modifiers changed from: private */
    public void zza(String str, String str2, Object obj, long j) {
        zzac.zzdr(str);
        zzac.zzdr(str2);
        zzmR();
        zzJW();
        zzob();
        if (!this.zzbqb.isEnabled()) {
            zzKl().zzMe().log("User property not set since app measurement is disabled");
        } else if (this.zzbqb.zzMu()) {
            zzKl().zzMe().zze("Setting user property (FE)", str2, obj);
            zzKd().zzb(new zzauq(str2, j, obj, str));
        }
    }

    private void zza(String str, String str2, String str3, Bundle bundle) {
        long currentTimeMillis = zznR().currentTimeMillis();
        zzac.zzdr(str2);
        final AppMeasurement.ConditionalUserProperty conditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
        conditionalUserProperty.mAppId = str;
        conditionalUserProperty.mName = str2;
        conditionalUserProperty.mCreationTimestamp = currentTimeMillis;
        if (str3 != null) {
            conditionalUserProperty.mExpiredEventName = str3;
            conditionalUserProperty.mExpiredEventParams = bundle;
        }
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauj.this.zzc(conditionalUserProperty);
            }
        });
    }

    /* access modifiers changed from: private */
    public void zzaL(boolean z) {
        zzmR();
        zzJW();
        zzob();
        zzKl().zzMe().zzj("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        zzKm().setMeasurementEnabled(z);
        zzKd().zzMY();
    }

    private Map<String, Object> zzb(String str, String str2, String str3, boolean z) {
        final AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            final String str4 = str;
            final String str5 = str2;
            final String str6 = str3;
            final boolean z2 = z;
            this.zzbqb.zzKk().zzm(new Runnable() {
                public void run() {
                    zzauj.this.zzbqb.zzKd().zza(atomicReference, str4, str5, str6, z2);
                }
            });
            try {
                atomicReference.wait(5000);
            } catch (InterruptedException e2) {
                zzKl().zzMb().zzj("Interrupted waiting for get user properties", e2);
            }
        }
        List<zzauq> list = (List) atomicReference.get();
        if (list == null) {
            zzKl().zzMb().log("Timed out waiting for get user properties");
            return Collections.emptyMap();
        }
        ArrayMap arrayMap = new ArrayMap(list.size());
        for (zzauq zzauq : list) {
            arrayMap.put(zzauq.name, zzauq.getValue());
        }
        return arrayMap;
    }

    /* access modifiers changed from: private */
    public void zzb(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        zzmR();
        zzob();
        zzac.zzw(conditionalUserProperty);
        zzac.zzdr(conditionalUserProperty.mName);
        zzac.zzdr(conditionalUserProperty.mOrigin);
        zzac.zzw(conditionalUserProperty.mValue);
        if (!this.zzbqb.isEnabled()) {
            zzKl().zzMe().log("Conditional property not sent since Firebase Analytics is disabled");
            return;
        }
        zzauq zzauq = new zzauq(conditionalUserProperty.mName, conditionalUserProperty.mTriggeredTimestamp, conditionalUserProperty.mValue, conditionalUserProperty.mOrigin);
        try {
            zzatq zza2 = zzKh().zza(conditionalUserProperty.mTriggeredEventName, conditionalUserProperty.mTriggeredEventParams, conditionalUserProperty.mOrigin, 0, true, false);
            zzKd().zzf(new zzatg(conditionalUserProperty.mAppId, conditionalUserProperty.mOrigin, zzauq, conditionalUserProperty.mCreationTimestamp, false, conditionalUserProperty.mTriggerEventName, zzKh().zza(conditionalUserProperty.mTimedOutEventName, conditionalUserProperty.mTimedOutEventParams, conditionalUserProperty.mOrigin, 0, true, false), conditionalUserProperty.mTriggerTimeout, zza2, conditionalUserProperty.mTimeToLive, zzKh().zza(conditionalUserProperty.mExpiredEventName, conditionalUserProperty.mExpiredEventParams, conditionalUserProperty.mOrigin, 0, true, false)));
        } catch (IllegalArgumentException e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.zzaut.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauu$zzc):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zze):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zza[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzb[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzc[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzg[]):void
      com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.Object, boolean):int
      com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(java.lang.String, android.os.Bundle, java.util.List<java.lang.String>, boolean, boolean):android.os.Bundle
     arg types: [java.lang.String, android.os.Bundle, java.util.List, boolean, int]
     candidates:
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, java.lang.String, java.lang.String, int):void
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.String, int, java.lang.Object, boolean):boolean
      com.google.android.gms.internal.zzaut.zza(java.lang.String, android.os.Bundle, java.util.List<java.lang.String>, boolean, boolean):android.os.Bundle */
    /* access modifiers changed from: private */
    public void zzb(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        int length;
        zzac.zzdr(str);
        zzac.zzdr(str2);
        zzac.zzw(bundle);
        zzmR();
        zzob();
        if (!this.zzbqb.isEnabled()) {
            zzKl().zzMe().log("Event not sent since app measurement is disabled");
            return;
        }
        if (!this.zzbvb) {
            this.zzbvb = true;
            zzMU();
        }
        boolean equals = "am".equals(str);
        boolean zzgd = zzaut.zzgd(str2);
        if (z && this.zzbuZ != null && !zzgd && !equals) {
            zzKl().zzMe().zze("Passing event to registered event handler (FE)", str2, bundle);
            this.zzbuZ.zzb(str, str2, bundle, j);
        } else if (this.zzbqb.zzMu()) {
            int zzfV = zzKh().zzfV(str2);
            if (zzfV != 0) {
                this.zzbqb.zzKh().zza(zzfV, "_ev", zzKh().zza(str2, zzKn().zzKM(), true), str2 != null ? str2.length() : 0);
                return;
            }
            List zzx = zzf.zzx("_o");
            Bundle zza2 = zzKh().zza(str2, bundle, (List<String>) zzx, z3, true);
            ArrayList arrayList = new ArrayList();
            arrayList.add(zza2);
            long nextLong = zzKm().zzMh().nextLong();
            int i = 0;
            String[] strArr = (String[]) zza2.keySet().toArray(new String[bundle.size()]);
            Arrays.sort(strArr);
            int length2 = strArr.length;
            int i2 = 0;
            while (i2 < length2) {
                String str4 = strArr[i2];
                Bundle[] zzH = zzKh().zzH(zza2.get(str4));
                if (zzH == null) {
                    length = i;
                } else {
                    zza2.putInt(str4, zzH.length);
                    int i3 = 0;
                    while (true) {
                        int i4 = i3;
                        if (i4 >= zzH.length) {
                            break;
                        }
                        Bundle zza3 = zzKh().zza("_ep", zzH[i4], (List<String>) zzx, z3, false);
                        zza3.putString("_en", str2);
                        zza3.putLong("_eid", nextLong);
                        zza3.putString("_gn", str4);
                        zza3.putInt("_ll", zzH.length);
                        zza3.putInt("_i", i4);
                        arrayList.add(zza3);
                        i3 = i4 + 1;
                    }
                    length = zzH.length + i;
                }
                i2++;
                i = length;
            }
            if (i != 0) {
                zza2.putLong("_eid", nextLong);
                zza2.putInt("_epc", i);
            }
            zzKn().zzLh();
            zzauk.zza zzMW = zzKe().zzMW();
            if (zzMW != null && !zza2.containsKey("_sc")) {
                zzMW.zzbvC = true;
            }
            int i5 = 0;
            while (true) {
                int i6 = i5;
                if (i6 < arrayList.size()) {
                    Bundle bundle2 = (Bundle) arrayList.get(i6);
                    String str5 = i6 != 0 ? "_ep" : str2;
                    bundle2.putString("_o", str);
                    if (!bundle2.containsKey("_sc")) {
                        zzauk.zza(zzMW, bundle2);
                    }
                    Bundle zzN = z2 ? zzKh().zzN(bundle2) : bundle2;
                    zzKl().zzMe().zze("Logging event (FE)", str2, zzN);
                    zzKd().zzc(new zzatq(str5, new zzato(zzN), str, j), str3);
                    if (!equals) {
                        for (AppMeasurement.zzc zzc : this.zzbva) {
                            zzc.zzc(str, str2, new Bundle(zzN), j);
                        }
                    }
                    i5 = i6 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void zzc(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        zzmR();
        zzob();
        zzac.zzw(conditionalUserProperty);
        zzac.zzdr(conditionalUserProperty.mName);
        if (!this.zzbqb.isEnabled()) {
            zzKl().zzMe().log("Conditional property not cleared since Firebase Analytics is disabled");
            return;
        }
        zzauq zzauq = new zzauq(conditionalUserProperty.mName, 0, null, null);
        try {
            zzKd().zzf(new zzatg(conditionalUserProperty.mAppId, conditionalUserProperty.mOrigin, zzauq, conditionalUserProperty.mCreationTimestamp, conditionalUserProperty.mActive, conditionalUserProperty.mTriggerEventName, null, conditionalUserProperty.mTriggerTimeout, null, conditionalUserProperty.mTimeToLive, zzKh().zza(conditionalUserProperty.mExpiredEventName, conditionalUserProperty.mExpiredEventParams, conditionalUserProperty.mOrigin, conditionalUserProperty.mCreationTimestamp, true, false)));
        } catch (IllegalArgumentException e2) {
        }
    }

    private List<AppMeasurement.ConditionalUserProperty> zzo(String str, String str2, String str3) {
        final AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            final String str4 = str;
            final String str5 = str2;
            final String str6 = str3;
            this.zzbqb.zzKk().zzm(new Runnable() {
                public void run() {
                    zzauj.this.zzbqb.zzKd().zza(atomicReference, str4, str5, str6);
                }
            });
            try {
                atomicReference.wait(5000);
            } catch (InterruptedException e2) {
                zzKl().zzMb().zze("Interrupted waiting for get conditional user properties", str, e2);
            }
        }
        List<zzatg> list = (List) atomicReference.get();
        if (list == null) {
            zzKl().zzMb().zzj("Timed out waiting for get conditional user properties", str);
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (zzatg zzatg : list) {
            AppMeasurement.ConditionalUserProperty conditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
            conditionalUserProperty.mAppId = str;
            conditionalUserProperty.mOrigin = str2;
            conditionalUserProperty.mCreationTimestamp = zzatg.zzbqY;
            conditionalUserProperty.mName = zzatg.zzbqX.name;
            conditionalUserProperty.mValue = zzatg.zzbqX.getValue();
            conditionalUserProperty.mActive = zzatg.zzbqZ;
            conditionalUserProperty.mTriggerEventName = zzatg.zzbra;
            if (zzatg.zzbrb != null) {
                conditionalUserProperty.mTimedOutEventName = zzatg.zzbrb.name;
                if (zzatg.zzbrb.zzbrH != null) {
                    conditionalUserProperty.mTimedOutEventParams = zzatg.zzbrb.zzbrH.zzLW();
                }
            }
            conditionalUserProperty.mTriggerTimeout = zzatg.zzbrc;
            if (zzatg.zzbrd != null) {
                conditionalUserProperty.mTriggeredEventName = zzatg.zzbrd.name;
                if (zzatg.zzbrd.zzbrH != null) {
                    conditionalUserProperty.mTriggeredEventParams = zzatg.zzbrd.zzbrH.zzLW();
                }
            }
            conditionalUserProperty.mTriggeredTimestamp = zzatg.zzbqX.zzbwf;
            conditionalUserProperty.mTimeToLive = zzatg.zzbre;
            if (zzatg.zzbrf != null) {
                conditionalUserProperty.mExpiredEventName = zzatg.zzbrf.name;
                if (zzatg.zzbrf.zzbrH != null) {
                    conditionalUserProperty.mExpiredEventParams = zzatg.zzbrf.zzbrH.zzLW();
                }
            }
            arrayList.add(conditionalUserProperty);
        }
        return arrayList;
    }

    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        zzJW();
        zza((String) null, str, str2, bundle);
    }

    public void clearConditionalUserPropertyAs(String str, String str2, String str3, Bundle bundle) {
        zzac.zzdr(str);
        zzJV();
        zza(str, str2, str3, bundle);
    }

    public Task<String> getAppInstanceId() {
        try {
            String zzMm = zzKm().zzMm();
            return zzMm != null ? Tasks.forResult(zzMm) : Tasks.call(zzKk().zzMs(), new Callable<String>() {
                /* renamed from: zzbY */
                public String call() {
                    String zzMm = zzauj.this.zzKm().zzMm();
                    if (zzMm == null) {
                        zzMm = zzauj.this.zzKa().zzar(120000);
                        if (zzMm == null) {
                            throw new TimeoutException();
                        }
                        zzauj.this.zzKm().zzfJ(zzMm);
                    }
                    return zzMm;
                }
            });
        } catch (Exception e2) {
            zzKl().zzMb().log("Failed to schedule task for getAppInstanceId");
            return Tasks.forException(e2);
        }
    }

    public List<AppMeasurement.ConditionalUserProperty> getConditionalUserProperties(String str, String str2) {
        zzJW();
        return zzo(null, str, str2);
    }

    public List<AppMeasurement.ConditionalUserProperty> getConditionalUserPropertiesAs(String str, String str2, String str3) {
        zzac.zzdr(str);
        zzJV();
        return zzo(str, str2, str3);
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public int getMaxUserProperties(String str) {
        zzac.zzdr(str);
        return zzKn().zzKZ();
    }

    public Map<String, Object> getUserProperties(String str, String str2, boolean z) {
        zzJW();
        return zzb(null, str, str2, z);
    }

    public Map<String, Object> getUserPropertiesAs(String str, String str2, String str3, boolean z) {
        zzac.zzdr(str);
        zzJV();
        return zzb(str, str2, str3, z);
    }

    public void setConditionalUserProperty(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        zzac.zzw(conditionalUserProperty);
        zzJW();
        AppMeasurement.ConditionalUserProperty conditionalUserProperty2 = new AppMeasurement.ConditionalUserProperty(conditionalUserProperty);
        if (!TextUtils.isEmpty(conditionalUserProperty2.mAppId)) {
            zzKl().zzMb().log("Package name should be null when calling setConditionalUserProperty");
        }
        conditionalUserProperty2.mAppId = null;
        zza(conditionalUserProperty2);
    }

    public void setConditionalUserPropertyAs(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        zzac.zzw(conditionalUserProperty);
        zzac.zzdr(conditionalUserProperty.mAppId);
        zzJV();
        zza(new AppMeasurement.ConditionalUserProperty(conditionalUserProperty));
    }

    public void setMeasurementEnabled(final boolean z) {
        zzob();
        zzJW();
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauj.this.zzaL(z);
            }
        });
    }

    public void setMinimumSessionDuration(final long j) {
        zzJW();
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauj.this.zzKm().zzbtn.set(j);
                zzauj.this.zzKl().zzMe().zzj("Minimum session duration set", Long.valueOf(j));
            }
        });
    }

    public void setSessionTimeoutDuration(final long j) {
        zzJW();
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauj.this.zzKm().zzbto.set(j);
                zzauj.this.zzKl().zzMe().zzj("Session timeout duration set", Long.valueOf(j));
            }
        });
    }

    public /* bridge */ /* synthetic */ void zzJV() {
        super.zzJV();
    }

    public /* bridge */ /* synthetic */ void zzJW() {
        super.zzJW();
    }

    public /* bridge */ /* synthetic */ void zzJX() {
        super.zzJX();
    }

    public /* bridge */ /* synthetic */ zzatb zzJY() {
        return super.zzJY();
    }

    public /* bridge */ /* synthetic */ zzatf zzJZ() {
        return super.zzJZ();
    }

    public /* bridge */ /* synthetic */ zzauj zzKa() {
        return super.zzKa();
    }

    public /* bridge */ /* synthetic */ zzatu zzKb() {
        return super.zzKb();
    }

    public /* bridge */ /* synthetic */ zzatl zzKc() {
        return super.zzKc();
    }

    public /* bridge */ /* synthetic */ zzaul zzKd() {
        return super.zzKd();
    }

    public /* bridge */ /* synthetic */ zzauk zzKe() {
        return super.zzKe();
    }

    public /* bridge */ /* synthetic */ zzatv zzKf() {
        return super.zzKf();
    }

    public /* bridge */ /* synthetic */ zzatj zzKg() {
        return super.zzKg();
    }

    public /* bridge */ /* synthetic */ zzaut zzKh() {
        return super.zzKh();
    }

    public /* bridge */ /* synthetic */ zzauc zzKi() {
        return super.zzKi();
    }

    public /* bridge */ /* synthetic */ zzaun zzKj() {
        return super.zzKj();
    }

    public /* bridge */ /* synthetic */ zzaud zzKk() {
        return super.zzKk();
    }

    public /* bridge */ /* synthetic */ zzatx zzKl() {
        return super.zzKl();
    }

    public /* bridge */ /* synthetic */ zzaua zzKm() {
        return super.zzKm();
    }

    public /* bridge */ /* synthetic */ zzati zzKn() {
        return super.zzKn();
    }

    @TargetApi(14)
    public void zzMS() {
        if (getContext().getApplicationContext() instanceof Application) {
            Application application = (Application) getContext().getApplicationContext();
            if (this.zzbuY == null) {
                this.zzbuY = new zza();
            }
            application.unregisterActivityLifecycleCallbacks(this.zzbuY);
            application.registerActivityLifecycleCallbacks(this.zzbuY);
            zzKl().zzMf().log("Registered activity lifecycle callback");
        }
    }

    public void zzMT() {
        zzmR();
        zzJW();
        zzob();
        if (this.zzbqb.zzMu()) {
            zzKd().zzMT();
            String zzMp = zzKm().zzMp();
            if (!TextUtils.isEmpty(zzMp) && !zzMp.equals(zzKc().zzLS())) {
                Bundle bundle = new Bundle();
                bundle.putString("_po", zzMp);
                zze("auto", "_ou", bundle);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    public void zza(AppMeasurement.zzb zzb) {
        zzmR();
        zzJW();
        zzob();
        if (!(zzb == null || zzb == this.zzbuZ)) {
            zzac.zza(this.zzbuZ == null, (Object) "EventInterceptor already set.");
        }
        this.zzbuZ = zzb;
    }

    public void zza(AppMeasurement.zzc zzc) {
        zzJW();
        zzob();
        zzac.zzw(zzc);
        if (!this.zzbva.add(zzc)) {
            zzKl().zzMb().log("OnEventListener already registered");
        }
    }

    /* access modifiers changed from: protected */
    public void zza(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        final Bundle zzM = zzM(bundle);
        final String str4 = str;
        final String str5 = str2;
        final long j2 = j;
        final boolean z4 = z;
        final boolean z5 = z2;
        final boolean z6 = z3;
        final String str6 = str3;
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauj.this.zzb(str4, str5, j2, zzM, z4, z5, z6, str6);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void zza(String str, String str2, long j, Object obj) {
        final String str3 = str;
        final String str4 = str2;
        final Object obj2 = obj;
        final long j2 = j;
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauj.this.zza(str3, str4, obj2, j2);
            }
        });
    }

    public void zza(String str, String str2, Bundle bundle, boolean z) {
        zzJW();
        zza(str, str2, bundle, true, this.zzbuZ == null || zzaut.zzgd(str2), z, null);
    }

    public List<zzauq> zzaM(final boolean z) {
        zzJW();
        zzob();
        zzKl().zzMe().log("Fetching user attributes (FE)");
        final AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            this.zzbqb.zzKk().zzm(new Runnable() {
                public void run() {
                    zzauj.this.zzKd().zza(atomicReference, z);
                }
            });
            try {
                atomicReference.wait(5000);
            } catch (InterruptedException e2) {
                zzKl().zzMb().zzj("Interrupted waiting for get user properties", e2);
            }
        }
        List<zzauq> list = (List) atomicReference.get();
        if (list != null) {
            return list;
        }
        zzKl().zzMb().log("Timed out waiting for get user properties");
        return Collections.emptyList();
    }

    /* access modifiers changed from: package-private */
    public String zzar(long j) {
        if (zzKk().zzMr()) {
            zzKl().zzLZ().log("Cannot retrieve app instance id from analytics worker thread");
            return null;
        } else if (zzKk().zzbc()) {
            zzKl().zzLZ().log("Cannot retrieve app instance id from main thread");
            return null;
        } else {
            long elapsedRealtime = zznR().elapsedRealtime();
            String zzas = zzas(j);
            long elapsedRealtime2 = zznR().elapsedRealtime() - elapsedRealtime;
            return (zzas != null || elapsedRealtime2 >= j) ? zzas : zzas(j - elapsedRealtime2);
        }
    }

    /* access modifiers changed from: package-private */
    public String zzas(long j) {
        final AtomicReference atomicReference = new AtomicReference();
        synchronized (atomicReference) {
            zzKk().zzm(new Runnable() {
                public void run() {
                    zzauj.this.zzKd().zza(atomicReference);
                }
            });
            try {
                atomicReference.wait(j);
            } catch (InterruptedException e2) {
                zzKl().zzMb().log("Interrupted waiting for app instance id");
                return null;
            }
        }
        return (String) atomicReference.get();
    }

    public void zzd(String str, String str2, Bundle bundle, long j) {
        zzJW();
        zza(str, str2, j, bundle, false, true, true, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.zzaut.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauu$zzc):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zze):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zza[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzb[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzc[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzg[]):void
      com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.Object, boolean):int
      com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String */
    public void zzd(String str, String str2, Object obj) {
        int i = 0;
        zzac.zzdr(str);
        long currentTimeMillis = zznR().currentTimeMillis();
        int zzfX = zzKh().zzfX(str2);
        if (zzfX != 0) {
            String zza2 = zzKh().zza(str2, zzKn().zzKN(), true);
            if (str2 != null) {
                i = str2.length();
            }
            this.zzbqb.zzKh().zza(zzfX, "_ev", zza2, i);
        } else if (obj != null) {
            int zzl = zzKh().zzl(str2, obj);
            if (zzl != 0) {
                String zza3 = zzKh().zza(str2, zzKn().zzKN(), true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i = String.valueOf(obj).length();
                }
                this.zzbqb.zzKh().zza(zzl, "_ev", zza3, i);
                return;
            }
            Object zzm = zzKh().zzm(str2, obj);
            if (zzm != null) {
                zza(str, str2, currentTimeMillis, zzm);
            }
        } else {
            zza(str, str2, currentTimeMillis, (Object) null);
        }
    }

    public void zze(String str, String str2, Bundle bundle) {
        zzJW();
        zza(str, str2, bundle, true, this.zzbuZ == null || zzaut.zzgd(str2), false, null);
    }

    public void zzf(Class<?> cls) {
        try {
            cls.getDeclaredMethod("initialize", Context.class).invoke(null, getContext());
        } catch (Exception e2) {
            zzKl().zzMb().zzj("Failed to invoke Tag Manager's initialize() method", e2);
        }
    }

    public synchronized String zzfQ(String str) {
        String str2;
        zzob();
        zzJW();
        if (str == null || !str.equals(this.zzbvd)) {
            String zzar = zzar(30000);
            if (zzar == null) {
                str2 = null;
            } else {
                this.zzbvd = str;
                this.zzbvc = zzar;
                str2 = this.zzbvc;
            }
        } else {
            str2 = this.zzbvc;
        }
        return str2;
    }

    public /* bridge */ /* synthetic */ void zzmR() {
        super.zzmR();
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
    }

    public /* bridge */ /* synthetic */ zze zznR() {
        return super.zznR();
    }
}
