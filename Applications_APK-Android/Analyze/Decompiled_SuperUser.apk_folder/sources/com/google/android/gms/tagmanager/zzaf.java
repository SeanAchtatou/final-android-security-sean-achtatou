package com.google.android.gms.tagmanager;

import android.util.Base64;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzaf extends zzam {
    private static final String ID = zzah.ENCODE.toString();
    private static final String zzbGi = zzai.ARG0.toString();
    private static final String zzbGj = zzai.NO_PADDING.toString();
    private static final String zzbGk = zzai.INPUT_FORMAT.toString();
    private static final String zzbGl = zzai.OUTPUT_FORMAT.toString();

    public zzaf() {
        super(ID, zzbGi);
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        byte[] decode;
        String encodeToString;
        zzak.zza zza = map.get(zzbGi);
        if (zza == null || zza == zzdl.zzRT()) {
            return zzdl.zzRT();
        }
        String zze = zzdl.zze(zza);
        zzak.zza zza2 = map.get(zzbGk);
        String zze2 = zza2 == null ? "text" : zzdl.zze(zza2);
        zzak.zza zza3 = map.get(zzbGl);
        String zze3 = zza3 == null ? "base16" : zzdl.zze(zza3);
        int i = 2;
        zzak.zza zza4 = map.get(zzbGj);
        if (zza4 != null && zzdl.zzi(zza4).booleanValue()) {
            i = 3;
        }
        try {
            if ("text".equals(zze2)) {
                decode = zze.getBytes();
            } else if ("base16".equals(zze2)) {
                decode = zzk.zzgR(zze);
            } else if ("base64".equals(zze2)) {
                decode = Base64.decode(zze, i);
            } else if ("base64url".equals(zze2)) {
                decode = Base64.decode(zze, i | 8);
            } else {
                String valueOf = String.valueOf(zze2);
                zzbo.e(valueOf.length() != 0 ? "Encode: unknown input format: ".concat(valueOf) : new String("Encode: unknown input format: "));
                return zzdl.zzRT();
            }
            if ("base16".equals(zze3)) {
                encodeToString = zzk.zzq(decode);
            } else if ("base64".equals(zze3)) {
                encodeToString = Base64.encodeToString(decode, i);
            } else if ("base64url".equals(zze3)) {
                encodeToString = Base64.encodeToString(decode, i | 8);
            } else {
                String valueOf2 = String.valueOf(zze3);
                zzbo.e(valueOf2.length() != 0 ? "Encode: unknown output format: ".concat(valueOf2) : new String("Encode: unknown output format: "));
                return zzdl.zzRT();
            }
            return zzdl.zzS(encodeToString);
        } catch (IllegalArgumentException e2) {
            zzbo.e("Encode: invalid input:");
            return zzdl.zzRT();
        }
    }
}
