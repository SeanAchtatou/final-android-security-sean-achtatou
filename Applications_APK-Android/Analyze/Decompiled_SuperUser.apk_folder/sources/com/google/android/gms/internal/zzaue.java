package com.google.android.gms.internal;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzatj;
import com.google.android.gms.internal.zzatx;
import com.google.android.gms.internal.zzaty;
import com.google.android.gms.internal.zzauv;
import com.google.android.gms.internal.zzauw;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.lody.virtual.helper.utils.FileUtils;
import io.fabric.sdk.android.services.common.a;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzaue {
    private static volatile zzaue zzbub;
    private final Context mContext;
    private final boolean zzadP;
    private FileLock zzbuA;
    private FileChannel zzbuB;
    private List<Long> zzbuC;
    private int zzbuD;
    private int zzbuE;
    private long zzbuF = -1;
    protected long zzbuG;
    private final long zzbuH;
    private final zzati zzbuc;
    private final zzaua zzbud;
    private final zzatx zzbue;
    private final zzaud zzbuf;
    private final zzaun zzbug;
    private final zzauc zzbuh;
    private final AppMeasurement zzbui;
    private final FirebaseAnalytics zzbuj;
    private final zzaut zzbuk;
    private final zzatj zzbul;
    private final zzatv zzbum;
    private final zzaty zzbun;
    private final zzauk zzbuo;
    private final zzaul zzbup;
    private final zzatl zzbuq;
    private final zzauj zzbur;
    private final zzatu zzbus;
    private final zzatz zzbut;
    private final zzaup zzbuu;
    private final zzatf zzbuv;
    private final zzatb zzbuw;
    private boolean zzbux;
    private Boolean zzbuy;
    private long zzbuz;
    private final zze zzuP;

    private class zza implements zzatj.zzb {
        zzauw.zze zzbuJ;
        List<Long> zzbuK;
        long zzbuL;
        List<zzauw.zzb> zzth;

        private zza() {
        }

        private long zza(zzauw.zzb zzb) {
            return ((zzb.zzbxc.longValue() / 1000) / 60) / 60;
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty() {
            return this.zzth == null || this.zzth.isEmpty();
        }

        public boolean zza(long j, zzauw.zzb zzb) {
            zzac.zzw(zzb);
            if (this.zzth == null) {
                this.zzth = new ArrayList();
            }
            if (this.zzbuK == null) {
                this.zzbuK = new ArrayList();
            }
            if (this.zzth.size() > 0 && zza(this.zzth.get(0)) != zza(zzb)) {
                return false;
            }
            long zzafB = this.zzbuL + ((long) zzb.zzafB());
            if (zzafB >= ((long) zzaue.this.zzKn().zzLo())) {
                return false;
            }
            this.zzbuL = zzafB;
            this.zzth.add(zzb);
            this.zzbuK.add(Long.valueOf(j));
            return this.zzth.size() < zzaue.this.zzKn().zzLp();
        }

        public void zzb(zzauw.zze zze) {
            zzac.zzw(zze);
            this.zzbuJ = zze;
        }
    }

    zzaue(zzaui zzaui) {
        zzac.zzw(zzaui);
        this.mContext = zzaui.mContext;
        this.zzuP = zzaui.zzn(this);
        this.zzbuH = this.zzuP.currentTimeMillis();
        this.zzbuc = zzaui.zza(this);
        zzaua zzb = zzaui.zzb(this);
        zzb.initialize();
        this.zzbud = zzb;
        zzatx zzc = zzaui.zzc(this);
        zzc.initialize();
        this.zzbue = zzc;
        zzKl().zzMd().zzj("App measurement is starting up, version", Long.valueOf(zzKn().zzKv()));
        zzKn().zzLh();
        zzKl().zzMd().log("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        zzaut zzj = zzaui.zzj(this);
        zzj.initialize();
        this.zzbuk = zzj;
        zzatl zzq = zzaui.zzq(this);
        zzq.initialize();
        this.zzbuq = zzq;
        zzatu zzr = zzaui.zzr(this);
        zzr.initialize();
        this.zzbus = zzr;
        zzKn().zzLh();
        String zzke = zzr.zzke();
        if (zzKh().zzge(zzke)) {
            zzKl().zzMd().log("Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.");
        } else {
            zzatx.zza zzMd = zzKl().zzMd();
            String valueOf = String.valueOf(zzke);
            zzMd.log(valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app "));
        }
        zzKl().zzMe().log("Debug-level message logging enabled");
        zzatj zzk = zzaui.zzk(this);
        zzk.initialize();
        this.zzbul = zzk;
        zzatv zzl = zzaui.zzl(this);
        zzl.initialize();
        this.zzbum = zzl;
        zzatf zzu = zzaui.zzu(this);
        zzu.initialize();
        this.zzbuv = zzu;
        this.zzbuw = zzaui.zzv(this);
        zzaty zzm = zzaui.zzm(this);
        zzm.initialize();
        this.zzbun = zzm;
        zzauk zzo = zzaui.zzo(this);
        zzo.initialize();
        this.zzbuo = zzo;
        zzaul zzp = zzaui.zzp(this);
        zzp.initialize();
        this.zzbup = zzp;
        zzauj zzi = zzaui.zzi(this);
        zzi.initialize();
        this.zzbur = zzi;
        zzaup zzt = zzaui.zzt(this);
        zzt.initialize();
        this.zzbuu = zzt;
        this.zzbut = zzaui.zzs(this);
        this.zzbui = zzaui.zzh(this);
        this.zzbuj = zzaui.zzg(this);
        zzaun zze = zzaui.zze(this);
        zze.initialize();
        this.zzbug = zze;
        zzauc zzf = zzaui.zzf(this);
        zzf.initialize();
        this.zzbuh = zzf;
        zzaud zzd = zzaui.zzd(this);
        zzd.initialize();
        this.zzbuf = zzd;
        if (this.zzbuD != this.zzbuE) {
            zzKl().zzLZ().zze("Not all components initialized", Integer.valueOf(this.zzbuD), Integer.valueOf(this.zzbuE));
        }
        this.zzadP = true;
        this.zzbuc.zzLh();
        if (this.mContext.getApplicationContext() instanceof Application) {
            int i = Build.VERSION.SDK_INT;
            zzKa().zzMS();
        } else {
            zzKl().zzMb().log("Application context is not an Application");
        }
        this.zzbuf.zzm(new Runnable() {
            public void run() {
                zzaue.this.start();
            }
        });
    }

    private boolean zzMJ() {
        zzmR();
        zzob();
        return zzKg().zzLK() || !TextUtils.isEmpty(zzKg().zzLE());
    }

    private void zzMK() {
        zzmR();
        zzob();
        if (zzMO()) {
            if (this.zzbuG > 0) {
                long abs = 3600000 - Math.abs(zznR().elapsedRealtime() - this.zzbuG);
                if (abs > 0) {
                    zzKl().zzMf().zzj("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    zzMA().unregister();
                    zzMB().cancel();
                    return;
                }
                this.zzbuG = 0;
            }
            if (!zzMu() || !zzMJ()) {
                zzMA().unregister();
                zzMB().cancel();
                return;
            }
            long zzML = zzML();
            if (zzML == 0) {
                zzMA().unregister();
                zzMB().cancel();
            } else if (!zzMz().zzqa()) {
                zzMA().zzpX();
                zzMB().cancel();
            } else {
                long j = zzKm().zzbtd.get();
                long zzLt = zzKn().zzLt();
                if (!zzKh().zzh(j, zzLt)) {
                    zzML = Math.max(zzML, j + zzLt);
                }
                zzMA().unregister();
                long currentTimeMillis = zzML - zznR().currentTimeMillis();
                if (currentTimeMillis <= 0) {
                    currentTimeMillis = zzKn().zzLx();
                    zzKm().zzbtb.set(zznR().currentTimeMillis());
                }
                zzKl().zzMf().zzj("Upload scheduled in approximately ms", Long.valueOf(currentTimeMillis));
                zzMB().zzy(currentTimeMillis);
            }
        }
    }

    private long zzML() {
        long zzLu;
        long currentTimeMillis = zznR().currentTimeMillis();
        long zzLA = zzKn().zzLA();
        boolean z = zzKg().zzLL() || zzKg().zzLF();
        if (z) {
            String zzLD = zzKn().zzLD();
            zzLu = (TextUtils.isEmpty(zzLD) || ".none.".equals(zzLD)) ? zzKn().zzLv() : zzKn().zzLw();
        } else {
            zzLu = zzKn().zzLu();
        }
        long j = zzKm().zzbtb.get();
        long j2 = zzKm().zzbtc.get();
        long max = Math.max(zzKg().zzLI(), zzKg().zzLJ());
        if (max == 0) {
            return 0;
        }
        long abs = currentTimeMillis - Math.abs(max - currentTimeMillis);
        long abs2 = currentTimeMillis - Math.abs(j2 - currentTimeMillis);
        long max2 = Math.max(currentTimeMillis - Math.abs(j - currentTimeMillis), abs2);
        long j3 = abs + zzLA;
        if (z && max2 > 0) {
            j3 = Math.min(abs, max2) + zzLu;
        }
        if (!zzKh().zzh(max2, zzLu)) {
            j3 = max2 + zzLu;
        }
        if (abs2 == 0 || abs2 < abs) {
            return j3;
        }
        for (int i = 0; i < zzKn().zzLC(); i++) {
            j3 += ((long) (1 << i)) * zzKn().zzLB();
            if (j3 > abs2) {
                return j3;
            }
        }
        return 0;
    }

    private void zza(zzaug zzaug) {
        if (zzaug == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    private void zza(zzauh zzauh) {
        if (zzauh == null) {
            throw new IllegalStateException("Component not created");
        } else if (!zzauh.isInitialized()) {
            throw new IllegalStateException("Component not initialized");
        }
    }

    private boolean zza(zzatm zzatm) {
        if (zzatm.zzbrA == null) {
            return false;
        }
        Iterator<String> it = zzatm.zzbrA.iterator();
        while (it.hasNext()) {
            if ("_r".equals(it.next())) {
                return true;
            }
        }
        return zzKi().zzab(zzatm.mAppId, zzatm.mName) && zzKg().zza(zzMG(), zzatm.mAppId, false, false, false, false, false).zzbrs < ((long) zzKn().zzfl(zzatm.mAppId));
    }

    private zzauw.zza[] zza(String str, zzauw.zzg[] zzgArr, zzauw.zzb[] zzbArr) {
        zzac.zzdr(str);
        return zzJZ().zza(str, zzbArr, zzgArr);
    }

    public static zzaue zzbM(Context context) {
        zzac.zzw(context);
        zzac.zzw(context.getApplicationContext());
        if (zzbub == null) {
            synchronized (zzaue.class) {
                if (zzbub == null) {
                    zzbub = new zzaui(context).zzMR();
                }
            }
        }
        return zzbub;
    }

    private void zzf(zzatd zzatd) {
        boolean z = true;
        zzmR();
        zzob();
        zzac.zzw(zzatd);
        zzac.zzdr(zzatd.packageName);
        zzatc zzfu = zzKg().zzfu(zzatd.packageName);
        String zzfH = zzKm().zzfH(zzatd.packageName);
        boolean z2 = false;
        if (zzfu == null) {
            zzatc zzatc = new zzatc(this, zzatd.packageName);
            zzatc.zzfd(zzKm().zzMi());
            zzatc.zzff(zzfH);
            zzfu = zzatc;
            z2 = true;
        } else if (!zzfH.equals(zzfu.zzKp())) {
            zzfu.zzff(zzfH);
            zzfu.zzfd(zzKm().zzMi());
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzatd.zzbqK) && !zzatd.zzbqK.equals(zzfu.getGmpAppId())) {
            zzfu.zzfe(zzatd.zzbqK);
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzatd.zzbqS) && !zzatd.zzbqS.equals(zzfu.zzKq())) {
            zzfu.zzfg(zzatd.zzbqS);
            z2 = true;
        }
        if (!(zzatd.zzbqM == 0 || zzatd.zzbqM == zzfu.zzKv())) {
            zzfu.zzab(zzatd.zzbqM);
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzatd.zzbhN) && !zzatd.zzbhN.equals(zzfu.zzmZ())) {
            zzfu.setAppVersion(zzatd.zzbhN);
            z2 = true;
        }
        if (zzatd.zzbqR != zzfu.zzKt()) {
            zzfu.zzaa(zzatd.zzbqR);
            z2 = true;
        }
        if (zzatd.zzbqL != null && !zzatd.zzbqL.equals(zzfu.zzKu())) {
            zzfu.zzfh(zzatd.zzbqL);
            z2 = true;
        }
        if (zzatd.zzbqN != zzfu.zzKw()) {
            zzfu.zzac(zzatd.zzbqN);
            z2 = true;
        }
        if (zzatd.zzbqP != zzfu.zzKx()) {
            zzfu.setMeasurementEnabled(zzatd.zzbqP);
            z2 = true;
        }
        if (!TextUtils.isEmpty(zzatd.zzbqO) && !zzatd.zzbqO.equals(zzfu.zzKI())) {
            zzfu.zzfi(zzatd.zzbqO);
            z2 = true;
        }
        if (zzatd.zzbqT != zzfu.zzuW()) {
            zzfu.zzam(zzatd.zzbqT);
        } else {
            z = z2;
        }
        if (z) {
            zzKg().zza(zzfu);
        }
    }

    private boolean zzl(String str, long j) {
        boolean z;
        int i;
        boolean z2;
        int i2;
        boolean z3;
        boolean z4;
        zzKg().beginTransaction();
        try {
            zza zza2 = new zza();
            zzKg().zza(str, j, this.zzbuF, zza2);
            if (!zza2.isEmpty()) {
                boolean z5 = false;
                zzauw.zze zze = zza2.zzbuJ;
                zze.zzbxj = new zzauw.zzb[zza2.zzth.size()];
                int i3 = 0;
                int i4 = 0;
                while (i4 < zza2.zzth.size()) {
                    if (zzKi().zzaa(zza2.zzbuJ.zzaS, zza2.zzth.get(i4).name)) {
                        zzKl().zzMb().zze("Dropping blacklisted raw event. appId", zzatx.zzfE(zza2.zzbuJ.zzaS), zza2.zzth.get(i4).name);
                        if ((zzKh().zzgg(zza2.zzbuJ.zzaS) || zzKh().zzgh(zza2.zzbuJ.zzaS)) || "_err".equals(zza2.zzth.get(i4).name)) {
                            i = i3;
                            z2 = z5;
                        } else {
                            zzKh().zza(11, "_ev", zza2.zzth.get(i4).name, 0);
                            i = i3;
                            z2 = z5;
                        }
                    } else {
                        boolean zzab = zzKi().zzab(zza2.zzbuJ.zzaS, zza2.zzth.get(i4).name);
                        if (zzab || zzKh().zzgi(zza2.zzth.get(i4).name)) {
                            boolean z6 = false;
                            boolean z7 = false;
                            if (zza2.zzth.get(i4).zzbxb == null) {
                                zza2.zzth.get(i4).zzbxb = new zzauw.zzc[0];
                            }
                            zzauw.zzc[] zzcArr = zza2.zzth.get(i4).zzbxb;
                            int length = zzcArr.length;
                            int i5 = 0;
                            while (i5 < length) {
                                zzauw.zzc zzc = zzcArr[i5];
                                if ("_c".equals(zzc.name)) {
                                    zzc.zzbxf = 1L;
                                    z6 = true;
                                    z4 = z7;
                                } else if ("_r".equals(zzc.name)) {
                                    zzc.zzbxf = 1L;
                                    z4 = true;
                                } else {
                                    z4 = z7;
                                }
                                i5++;
                                z7 = z4;
                            }
                            if (!z6 && zzab) {
                                zzKl().zzMf().zzj("Marking event as conversion", zza2.zzth.get(i4).name);
                                zzauw.zzc[] zzcArr2 = (zzauw.zzc[]) Arrays.copyOf(zza2.zzth.get(i4).zzbxb, zza2.zzth.get(i4).zzbxb.length + 1);
                                zzauw.zzc zzc2 = new zzauw.zzc();
                                zzc2.name = "_c";
                                zzc2.zzbxf = 1L;
                                zzcArr2[zzcArr2.length - 1] = zzc2;
                                zza2.zzth.get(i4).zzbxb = zzcArr2;
                            }
                            if (!z7) {
                                zzKl().zzMf().zzj("Marking event as real-time", zza2.zzth.get(i4).name);
                                zzauw.zzc[] zzcArr3 = (zzauw.zzc[]) Arrays.copyOf(zza2.zzth.get(i4).zzbxb, zza2.zzth.get(i4).zzbxb.length + 1);
                                zzauw.zzc zzc3 = new zzauw.zzc();
                                zzc3.name = "_r";
                                zzc3.zzbxf = 1L;
                                zzcArr3[zzcArr3.length - 1] = zzc3;
                                zza2.zzth.get(i4).zzbxb = zzcArr3;
                            }
                            boolean z8 = true;
                            if (zzKg().zza(zzMG(), zza2.zzbuJ.zzaS, false, false, false, false, true).zzbrs > ((long) zzKn().zzfl(zza2.zzbuJ.zzaS))) {
                                zzauw.zzb zzb = zza2.zzth.get(i4);
                                int i6 = 0;
                                while (true) {
                                    if (i6 >= zzb.zzbxb.length) {
                                        break;
                                    } else if ("_r".equals(zzb.zzbxb[i6].name)) {
                                        zzauw.zzc[] zzcArr4 = new zzauw.zzc[(zzb.zzbxb.length - 1)];
                                        if (i6 > 0) {
                                            System.arraycopy(zzb.zzbxb, 0, zzcArr4, 0, i6);
                                        }
                                        if (i6 < zzcArr4.length) {
                                            System.arraycopy(zzb.zzbxb, i6 + 1, zzcArr4, i6, zzcArr4.length - i6);
                                        }
                                        zzb.zzbxb = zzcArr4;
                                    } else {
                                        i6++;
                                    }
                                }
                                z8 = z5;
                            }
                            if (zzaut.zzfT(zza2.zzth.get(i4).name) && zzab && zzKg().zza(zzMG(), zza2.zzbuJ.zzaS, false, false, true, false, false).zzbrq > ((long) zzKn().zzfk(zza2.zzbuJ.zzaS))) {
                                zzKl().zzMb().zzj("Too many conversions. Not logging as conversion. appId", zzatx.zzfE(zza2.zzbuJ.zzaS));
                                zzauw.zzb zzb2 = zza2.zzth.get(i4);
                                boolean z9 = false;
                                zzauw.zzc zzc4 = null;
                                zzauw.zzc[] zzcArr5 = zzb2.zzbxb;
                                int length2 = zzcArr5.length;
                                int i7 = 0;
                                while (i7 < length2) {
                                    zzauw.zzc zzc5 = zzcArr5[i7];
                                    if ("_c".equals(zzc5.name)) {
                                        z3 = z9;
                                    } else if ("_err".equals(zzc5.name)) {
                                        zzauw.zzc zzc6 = zzc4;
                                        z3 = true;
                                        zzc5 = zzc6;
                                    } else {
                                        zzc5 = zzc4;
                                        z3 = z9;
                                    }
                                    i7++;
                                    z9 = z3;
                                    zzc4 = zzc5;
                                }
                                if (z9 && zzc4 != null) {
                                    zzauw.zzc[] zzcArr6 = new zzauw.zzc[(zzb2.zzbxb.length - 1)];
                                    int i8 = 0;
                                    zzauw.zzc[] zzcArr7 = zzb2.zzbxb;
                                    int length3 = zzcArr7.length;
                                    int i9 = 0;
                                    while (i9 < length3) {
                                        zzauw.zzc zzc7 = zzcArr7[i9];
                                        if (zzc7 != zzc4) {
                                            i2 = i8 + 1;
                                            zzcArr6[i8] = zzc7;
                                        } else {
                                            i2 = i8;
                                        }
                                        i9++;
                                        i8 = i2;
                                    }
                                    zzb2.zzbxb = zzcArr6;
                                    z = z8;
                                } else if (zzc4 != null) {
                                    zzc4.name = "_err";
                                    zzc4.zzbxf = 10L;
                                    z = z8;
                                } else {
                                    zzKl().zzLZ().zzj("Did not find conversion parameter. appId", zzatx.zzfE(zza2.zzbuJ.zzaS));
                                }
                            }
                            z = z8;
                        } else {
                            z = z5;
                        }
                        zze.zzbxj[i3] = zza2.zzth.get(i4);
                        i = i3 + 1;
                        z2 = z;
                    }
                    i4++;
                    i3 = i;
                    z5 = z2;
                }
                if (i3 < zza2.zzth.size()) {
                    zze.zzbxj = (zzauw.zzb[]) Arrays.copyOf(zze.zzbxj, i3);
                }
                zze.zzbxC = zza(zza2.zzbuJ.zzaS, zza2.zzbuJ.zzbxk, zze.zzbxj);
                zze.zzbxm = Long.MAX_VALUE;
                zze.zzbxn = Long.MIN_VALUE;
                for (zzauw.zzb zzb3 : zze.zzbxj) {
                    if (zzb3.zzbxc.longValue() < zze.zzbxm.longValue()) {
                        zze.zzbxm = zzb3.zzbxc;
                    }
                    if (zzb3.zzbxc.longValue() > zze.zzbxn.longValue()) {
                        zze.zzbxn = zzb3.zzbxc;
                    }
                }
                String str2 = zza2.zzbuJ.zzaS;
                zzatc zzfu = zzKg().zzfu(str2);
                if (zzfu == null) {
                    zzKl().zzLZ().zzj("Bundling raw events w/o app info. appId", zzatx.zzfE(zza2.zzbuJ.zzaS));
                } else if (zze.zzbxj.length > 0) {
                    long zzKs = zzfu.zzKs();
                    zze.zzbxp = zzKs != 0 ? Long.valueOf(zzKs) : null;
                    long zzKr = zzfu.zzKr();
                    if (zzKr != 0) {
                        zzKs = zzKr;
                    }
                    zze.zzbxo = zzKs != 0 ? Long.valueOf(zzKs) : null;
                    zzfu.zzKB();
                    zze.zzbxA = Integer.valueOf((int) zzfu.zzKy());
                    zzfu.zzY(zze.zzbxm.longValue());
                    zzfu.zzZ(zze.zzbxn.longValue());
                    zze.zzbqO = zzfu.zzKJ();
                    zzKg().zza(zzfu);
                }
                if (zze.zzbxj.length > 0) {
                    zzKn().zzLh();
                    zzauv.zzb zzfL = zzKi().zzfL(zza2.zzbuJ.zzaS);
                    if (zzfL != null && zzfL.zzbwQ != null) {
                        zze.zzbxH = zzfL.zzbwQ;
                    } else if (TextUtils.isEmpty(zza2.zzbuJ.zzbqK)) {
                        zze.zzbxH = -1L;
                    } else {
                        zzKl().zzMb().zzj("Did not find measurement config or missing version info. appId", zzatx.zzfE(zza2.zzbuJ.zzaS));
                    }
                    zzKg().zza(zze, z5);
                }
                zzKg().zzJ(zza2.zzbuK);
                zzKg().zzfB(str2);
                zzKg().setTransactionSuccessful();
                return zze.zzbxj.length > 0;
            }
            zzKg().setTransactionSuccessful();
            zzKg().endTransaction();
            return false;
        } finally {
            zzKg().endTransaction();
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    public boolean isEnabled() {
        boolean z = false;
        zzmR();
        zzob();
        if (zzKn().zzLi()) {
            return false;
        }
        Boolean zzLj = zzKn().zzLj();
        if (zzLj != null) {
            z = zzLj.booleanValue();
        } else if (!zzKn().zzwR()) {
            z = true;
        }
        return zzKm().zzaK(z);
    }

    /* access modifiers changed from: protected */
    public void start() {
        zzmR();
        zzKg().zzLG();
        if (zzKm().zzbtb.get() == 0) {
            zzKm().zzbtb.set(zznR().currentTimeMillis());
        }
        if (zzMu()) {
            zzKn().zzLh();
            if (!TextUtils.isEmpty(zzKb().getGmpAppId())) {
                String zzMl = zzKm().zzMl();
                if (zzMl == null) {
                    zzKm().zzfI(zzKb().getGmpAppId());
                } else if (!zzMl.equals(zzKb().getGmpAppId())) {
                    zzKl().zzMd().log("Rechecking which service to use due to a GMP App Id change");
                    zzKm().zzMo();
                    this.zzbup.disconnect();
                    this.zzbup.zzoD();
                    zzKm().zzfI(zzKb().getGmpAppId());
                }
            }
            zzKn().zzLh();
            if (!TextUtils.isEmpty(zzKb().getGmpAppId())) {
                zzKa().zzMT();
            }
        } else if (isEnabled()) {
            if (!zzKh().zzbW("android.permission.INTERNET")) {
                zzKl().zzLZ().log("App is missing INTERNET permission");
            }
            if (!zzKh().zzbW("android.permission.ACCESS_NETWORK_STATE")) {
                zzKl().zzLZ().log("App is missing ACCESS_NETWORK_STATE permission");
            }
            zzKn().zzLh();
            if (!zzadg.zzbi(getContext()).zzzx()) {
                if (!zzaub.zzi(getContext(), false)) {
                    zzKl().zzLZ().log("AppMeasurementReceiver not registered/enabled");
                }
                if (!zzaum.zzj(getContext(), false)) {
                    zzKl().zzLZ().log("AppMeasurementService not registered/enabled");
                }
            }
            zzKl().zzLZ().log("Uploading is not possible. App measurement disabled");
        }
        zzMK();
    }

    /* access modifiers changed from: package-private */
    public void zzJV() {
        zzKn().zzLh();
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* access modifiers changed from: package-private */
    public void zzJW() {
        zzKn().zzLh();
    }

    public zzatb zzJY() {
        zza(this.zzbuw);
        return this.zzbuw;
    }

    public zzatf zzJZ() {
        zza((zzauh) this.zzbuv);
        return this.zzbuv;
    }

    /* access modifiers changed from: protected */
    public void zzK(List<Long> list) {
        zzac.zzaw(!list.isEmpty());
        if (this.zzbuC != null) {
            zzKl().zzLZ().log("Set uploading progress before finishing the previous upload");
        } else {
            this.zzbuC = new ArrayList(list);
        }
    }

    public zzauj zzKa() {
        zza((zzauh) this.zzbur);
        return this.zzbur;
    }

    public zzatu zzKb() {
        zza((zzauh) this.zzbus);
        return this.zzbus;
    }

    public zzatl zzKc() {
        zza((zzauh) this.zzbuq);
        return this.zzbuq;
    }

    public zzaul zzKd() {
        zza((zzauh) this.zzbup);
        return this.zzbup;
    }

    public zzauk zzKe() {
        zza((zzauh) this.zzbuo);
        return this.zzbuo;
    }

    public zzatv zzKf() {
        zza((zzauh) this.zzbum);
        return this.zzbum;
    }

    public zzatj zzKg() {
        zza((zzauh) this.zzbul);
        return this.zzbul;
    }

    public zzaut zzKh() {
        zza((zzaug) this.zzbuk);
        return this.zzbuk;
    }

    public zzauc zzKi() {
        zza((zzauh) this.zzbuh);
        return this.zzbuh;
    }

    public zzaun zzKj() {
        zza((zzauh) this.zzbug);
        return this.zzbug;
    }

    public zzaud zzKk() {
        zza((zzauh) this.zzbuf);
        return this.zzbuf;
    }

    public zzatx zzKl() {
        zza((zzauh) this.zzbue);
        return this.zzbue;
    }

    public zzaua zzKm() {
        zza((zzaug) this.zzbud);
        return this.zzbud;
    }

    public zzati zzKn() {
        return this.zzbuc;
    }

    public zzatz zzMA() {
        if (this.zzbut != null) {
            return this.zzbut;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    public zzaup zzMB() {
        zza((zzauh) this.zzbuu);
        return this.zzbuu;
    }

    /* access modifiers changed from: package-private */
    public FileChannel zzMC() {
        return this.zzbuB;
    }

    /* access modifiers changed from: package-private */
    public void zzMD() {
        zzmR();
        zzob();
        if (zzMO() && zzME()) {
            zzy(zza(zzMC()), zzKb().zzLX());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzME() {
        zzmR();
        try {
            this.zzbuB = new RandomAccessFile(new File(getContext().getFilesDir(), this.zzbul.zzow()), "rw").getChannel();
            this.zzbuA = this.zzbuB.tryLock();
            if (this.zzbuA != null) {
                zzKl().zzMf().log("Storage concurrent access okay");
                return true;
            }
            zzKl().zzLZ().log("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e2) {
            zzKl().zzLZ().zzj("Failed to acquire storage lock", e2);
        } catch (IOException e3) {
            zzKl().zzLZ().zzj("Failed to access storage lock file", e3);
        }
    }

    /* access modifiers changed from: package-private */
    public long zzMF() {
        return this.zzbuH;
    }

    /* access modifiers changed from: package-private */
    public long zzMG() {
        return ((((zznR().currentTimeMillis() + zzKm().zzMj()) / 1000) / 60) / 60) / 24;
    }

    /* access modifiers changed from: protected */
    public boolean zzMH() {
        zzmR();
        return this.zzbuC != null;
    }

    public void zzMI() {
        zzatc zzfu;
        String str;
        List<Pair<zzauw.zze, Long>> list;
        zzmR();
        zzob();
        zzKn().zzLh();
        Boolean zzMn = zzKm().zzMn();
        if (zzMn == null) {
            zzKl().zzMb().log("Upload data called on the client side before use of service was decided");
        } else if (zzMn.booleanValue()) {
            zzKl().zzLZ().log("Upload called in the client side when service should be used");
        } else if (this.zzbuG > 0) {
            zzMK();
        } else if (zzMH()) {
            zzKl().zzMb().log("Uploading requested multiple times");
        } else if (!zzMz().zzqa()) {
            zzKl().zzMb().log("Network not connected, ignoring upload request");
            zzMK();
        } else {
            long currentTimeMillis = zznR().currentTimeMillis();
            zzaq(currentTimeMillis - zzKn().zzLs());
            long j = zzKm().zzbtb.get();
            if (j != 0) {
                zzKl().zzMe().zzj("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(currentTimeMillis - j)));
            }
            String zzLE = zzKg().zzLE();
            if (!TextUtils.isEmpty(zzLE)) {
                if (this.zzbuF == -1) {
                    this.zzbuF = zzKg().zzLM();
                }
                List<Pair<zzauw.zze, Long>> zzn = zzKg().zzn(zzLE, zzKn().zzfq(zzLE), zzKn().zzfr(zzLE));
                if (!zzn.isEmpty()) {
                    Iterator<Pair<zzauw.zze, Long>> it = zzn.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            str = null;
                            break;
                        }
                        zzauw.zze zze = (zzauw.zze) it.next().first;
                        if (!TextUtils.isEmpty(zze.zzbxw)) {
                            str = zze.zzbxw;
                            break;
                        }
                    }
                    if (str != null) {
                        int i = 0;
                        while (true) {
                            if (i >= zzn.size()) {
                                break;
                            }
                            zzauw.zze zze2 = (zzauw.zze) zzn.get(i).first;
                            if (!TextUtils.isEmpty(zze2.zzbxw) && !zze2.zzbxw.equals(str)) {
                                list = zzn.subList(0, i);
                                break;
                            }
                            i++;
                        }
                    }
                    list = zzn;
                    zzauw.zzd zzd = new zzauw.zzd();
                    zzd.zzbxg = new zzauw.zze[list.size()];
                    ArrayList arrayList = new ArrayList(list.size());
                    for (int i2 = 0; i2 < zzd.zzbxg.length; i2++) {
                        zzd.zzbxg[i2] = (zzauw.zze) list.get(i2).first;
                        arrayList.add((Long) list.get(i2).second);
                        zzd.zzbxg[i2].zzbxv = Long.valueOf(zzKn().zzKv());
                        zzd.zzbxg[i2].zzbxl = Long.valueOf(currentTimeMillis);
                        zzd.zzbxg[i2].zzbxB = Boolean.valueOf(zzKn().zzLh());
                    }
                    String zzb = zzKl().zzak(2) ? zzaut.zzb(zzd) : null;
                    byte[] zza2 = zzKh().zza(zzd);
                    String zzLr = zzKn().zzLr();
                    try {
                        URL url = new URL(zzLr);
                        zzK(arrayList);
                        zzKm().zzbtc.set(currentTimeMillis);
                        String str2 = "?";
                        if (zzd.zzbxg.length > 0) {
                            str2 = zzd.zzbxg[0].zzaS;
                        }
                        zzKl().zzMf().zzd("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(zza2.length), zzb);
                        zzMz().zza(zzLE, url, zza2, null, new zzaty.zza() {
                            public void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
                                zzaue.this.zza(i, th, bArr);
                            }
                        });
                    } catch (MalformedURLException e2) {
                        zzKl().zzLZ().zze("Failed to parse upload URL. Not uploading. appId", zzatx.zzfE(zzLE), zzLr);
                    }
                }
            } else {
                this.zzbuF = -1;
                String zzao = zzKg().zzao(currentTimeMillis - zzKn().zzLs());
                if (!TextUtils.isEmpty(zzao) && (zzfu = zzKg().zzfu(zzao)) != null) {
                    zzb(zzfu);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzMM() {
        this.zzbuE++;
    }

    /* access modifiers changed from: package-private */
    public void zzMN() {
        zzmR();
        zzob();
        if (!this.zzbux) {
            zzKl().zzMd().log("This instance being marked as an uploader");
            zzMD();
        }
        this.zzbux = true;
    }

    /* access modifiers changed from: package-private */
    public boolean zzMO() {
        zzmR();
        zzob();
        return this.zzbux;
    }

    /* access modifiers changed from: protected */
    public boolean zzMu() {
        boolean z = false;
        zzob();
        zzmR();
        if (this.zzbuy == null || this.zzbuz == 0 || (this.zzbuy != null && !this.zzbuy.booleanValue() && Math.abs(zznR().elapsedRealtime() - this.zzbuz) > 1000)) {
            this.zzbuz = zznR().elapsedRealtime();
            zzKn().zzLh();
            if (zzKh().zzbW("android.permission.INTERNET") && zzKh().zzbW("android.permission.ACCESS_NETWORK_STATE") && (zzadg.zzbi(getContext()).zzzx() || (zzaub.zzi(getContext(), false) && zzaum.zzj(getContext(), false)))) {
                z = true;
            }
            this.zzbuy = Boolean.valueOf(z);
            if (this.zzbuy.booleanValue()) {
                this.zzbuy = Boolean.valueOf(zzKh().zzga(zzKb().getGmpAppId()));
            }
        }
        return this.zzbuy.booleanValue();
    }

    public zzatx zzMv() {
        if (this.zzbue == null || !this.zzbue.isInitialized()) {
            return null;
        }
        return this.zzbue;
    }

    /* access modifiers changed from: package-private */
    public zzaud zzMw() {
        return this.zzbuf;
    }

    public AppMeasurement zzMx() {
        return this.zzbui;
    }

    public FirebaseAnalytics zzMy() {
        return this.zzbuj;
    }

    public zzaty zzMz() {
        zza((zzauh) this.zzbun);
        return this.zzbun;
    }

    public void zzV(boolean z) {
        zzMK();
    }

    /* access modifiers changed from: package-private */
    public int zza(FileChannel fileChannel) {
        zzmR();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzKl().zzLZ().log("Bad chanel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read == 4) {
                allocate.flip();
                return allocate.getInt();
            } else if (read == -1) {
                return 0;
            } else {
                zzKl().zzMb().zzj("Unexpected data length. Bytes read", Integer.valueOf(read));
                return 0;
            }
        } catch (IOException e2) {
            zzKl().zzLZ().zzj("Failed to read from channel", e2);
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void zza(int i, Throwable th, byte[] bArr) {
        boolean z = false;
        zzmR();
        zzob();
        if (bArr == null) {
            bArr = new byte[0];
        }
        List<Long> list = this.zzbuC;
        this.zzbuC = null;
        if ((i == 200 || i == 204) && th == null) {
            try {
                zzKm().zzbtb.set(zznR().currentTimeMillis());
                zzKm().zzbtc.set(0);
                zzMK();
                zzKl().zzMf().zze("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                zzKg().beginTransaction();
                for (Long longValue : list) {
                    zzKg().zzan(longValue.longValue());
                }
                zzKg().setTransactionSuccessful();
                zzKg().endTransaction();
                if (!zzMz().zzqa() || !zzMJ()) {
                    this.zzbuF = -1;
                    zzMK();
                } else {
                    zzMI();
                }
                this.zzbuG = 0;
            } catch (SQLiteException e2) {
                zzKl().zzLZ().zzj("Database error while trying to delete uploaded bundles", e2);
                this.zzbuG = zznR().elapsedRealtime();
                zzKl().zzMf().zzj("Disable upload, time", Long.valueOf(this.zzbuG));
            } catch (Throwable th2) {
                zzKg().endTransaction();
                throw th2;
            }
        } else {
            zzKl().zzMf().zze("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            zzKm().zzbtc.set(zznR().currentTimeMillis());
            if (i == 503 || i == 429) {
                z = true;
            }
            if (z) {
                zzKm().zzbtd.set(zznR().currentTimeMillis());
            }
            zzMK();
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzatd zzatd, long j) {
        zzatc zzfu = zzKg().zzfu(zzatd.packageName);
        if (!(zzfu == null || zzfu.getGmpAppId() == null || zzfu.getGmpAppId().equals(zzatd.zzbqK))) {
            zzKl().zzMb().zzj("New GMP App Id passed in. Removing cached database data. appId", zzatx.zzfE(zzfu.zzke()));
            zzKg().zzfz(zzfu.zzke());
            zzfu = null;
        }
        if (zzfu != null && zzfu.zzmZ() != null && !zzfu.zzmZ().equals(zzatd.zzbhN)) {
            Bundle bundle = new Bundle();
            bundle.putString("_pv", zzfu.zzmZ());
            zzb(new zzatq("_au", new zzato(bundle), "auto", j), zzatd);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzatm zzatm, zzatd zzatd) {
        zzmR();
        zzob();
        zzac.zzw(zzatm);
        zzac.zzw(zzatd);
        zzac.zzdr(zzatm.mAppId);
        zzac.zzaw(zzatm.mAppId.equals(zzatd.packageName));
        zzauw.zze zze = new zzauw.zze();
        zze.zzbxi = 1;
        zze.zzbxq = a.ANDROID_CLIENT_TYPE;
        zze.zzaS = zzatd.packageName;
        zze.zzbqL = zzatd.zzbqL;
        zze.zzbhN = zzatd.zzbhN;
        zze.zzbxD = Integer.valueOf((int) zzatd.zzbqR);
        zze.zzbxu = Long.valueOf(zzatd.zzbqM);
        zze.zzbqK = zzatd.zzbqK;
        zze.zzbxz = zzatd.zzbqN == 0 ? null : Long.valueOf(zzatd.zzbqN);
        Pair<String, Boolean> zzfG = zzKm().zzfG(zzatd.packageName);
        if (!TextUtils.isEmpty((CharSequence) zzfG.first)) {
            zze.zzbxw = (String) zzfG.first;
            zze.zzbxx = (Boolean) zzfG.second;
        } else if (!zzKc().zzbL(this.mContext)) {
            String string = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
            if (string == null) {
                zzKl().zzMb().zzj("null secure ID. appId", zzatx.zzfE(zze.zzaS));
                string = "null";
            } else if (string.isEmpty()) {
                zzKl().zzMb().zzj("empty secure ID. appId", zzatx.zzfE(zze.zzaS));
            }
            zze.zzbxG = string;
        }
        zze.zzbxr = zzKc().zzkN();
        zze.zzbb = zzKc().zzLS();
        zze.zzbxt = Integer.valueOf((int) zzKc().zzLT());
        zze.zzbxs = zzKc().zzLU();
        zze.zzbxv = null;
        zze.zzbxl = null;
        zze.zzbxm = null;
        zze.zzbxn = null;
        zze.zzbxI = Long.valueOf(zzatd.zzbqT);
        zzatc zzfu = zzKg().zzfu(zzatd.packageName);
        if (zzfu == null) {
            zzfu = new zzatc(this, zzatd.packageName);
            zzfu.zzfd(zzKm().zzMi());
            zzfu.zzfg(zzatd.zzbqS);
            zzfu.zzfe(zzatd.zzbqK);
            zzfu.zzff(zzKm().zzfH(zzatd.packageName));
            zzfu.zzad(0);
            zzfu.zzY(0);
            zzfu.zzZ(0);
            zzfu.setAppVersion(zzatd.zzbhN);
            zzfu.zzaa(zzatd.zzbqR);
            zzfu.zzfh(zzatd.zzbqL);
            zzfu.zzab(zzatd.zzbqM);
            zzfu.zzac(zzatd.zzbqN);
            zzfu.setMeasurementEnabled(zzatd.zzbqP);
            zzfu.zzam(zzatd.zzbqT);
            zzKg().zza(zzfu);
        }
        zze.zzbxy = zzfu.getAppInstanceId();
        zze.zzbqS = zzfu.zzKq();
        List<zzaus> zzft = zzKg().zzft(zzatd.packageName);
        zze.zzbxk = new zzauw.zzg[zzft.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < zzft.size()) {
                zzauw.zzg zzg = new zzauw.zzg();
                zze.zzbxk[i2] = zzg;
                zzg.name = zzft.get(i2).mName;
                zzg.zzbxM = Long.valueOf(zzft.get(i2).zzbwj);
                zzKh().zza(zzg, zzft.get(i2).mValue);
                i = i2 + 1;
            } else {
                try {
                    break;
                } catch (IOException e2) {
                    zzKl().zzLZ().zze("Data loss. Failed to insert raw event metadata. appId", zzatx.zzfE(zze.zzaS), e2);
                    return;
                }
            }
        }
        if (zzKg().zza(zzatm, zzKg().zza(zze), zza(zzatm))) {
            this.zzbuG = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zza(int i, FileChannel fileChannel) {
        zzmR();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzKl().zzLZ().log("Bad chanel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() == 4) {
                return true;
            }
            zzKl().zzLZ().zzj("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            return true;
        } catch (IOException e2) {
            zzKl().zzLZ().zzj("Failed to write to channel", e2);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.internal.zzaut.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauu$zzc):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zze):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zza[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzb[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzc[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzg[]):void
      com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.Object, boolean):int
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void */
    public byte[] zza(zzatq zzatq, String str) {
        long j;
        zzob();
        zzmR();
        zzJV();
        zzac.zzw(zzatq);
        zzac.zzdr(str);
        zzauw.zzd zzd = new zzauw.zzd();
        zzKg().beginTransaction();
        try {
            zzatc zzfu = zzKg().zzfu(str);
            if (zzfu == null) {
                zzKl().zzMe().zzj("Log and bundle not available. package_name", str);
                return new byte[0];
            } else if (!zzfu.zzKx()) {
                zzKl().zzMe().zzj("Log and bundle disabled. package_name", str);
                byte[] bArr = new byte[0];
                zzKg().endTransaction();
                return bArr;
            } else {
                zzauw.zze zze = new zzauw.zze();
                zzd.zzbxg = new zzauw.zze[]{zze};
                zze.zzbxi = 1;
                zze.zzbxq = a.ANDROID_CLIENT_TYPE;
                zze.zzaS = zzfu.zzke();
                zze.zzbqL = zzfu.zzKu();
                zze.zzbhN = zzfu.zzmZ();
                zze.zzbxD = Integer.valueOf((int) zzfu.zzKt());
                zze.zzbxu = Long.valueOf(zzfu.zzKv());
                zze.zzbqK = zzfu.getGmpAppId();
                zze.zzbxz = Long.valueOf(zzfu.zzKw());
                Pair<String, Boolean> zzfG = zzKm().zzfG(zzfu.zzke());
                if (!TextUtils.isEmpty((CharSequence) zzfG.first)) {
                    zze.zzbxw = (String) zzfG.first;
                    zze.zzbxx = (Boolean) zzfG.second;
                }
                zze.zzbxr = zzKc().zzkN();
                zze.zzbb = zzKc().zzLS();
                zze.zzbxt = Integer.valueOf((int) zzKc().zzLT());
                zze.zzbxs = zzKc().zzLU();
                zze.zzbxy = zzfu.getAppInstanceId();
                zze.zzbqS = zzfu.zzKq();
                List<zzaus> zzft = zzKg().zzft(zzfu.zzke());
                zze.zzbxk = new zzauw.zzg[zzft.size()];
                for (int i = 0; i < zzft.size(); i++) {
                    zzauw.zzg zzg = new zzauw.zzg();
                    zze.zzbxk[i] = zzg;
                    zzg.name = zzft.get(i).mName;
                    zzg.zzbxM = Long.valueOf(zzft.get(i).zzbwj);
                    zzKh().zza(zzg, zzft.get(i).mValue);
                }
                Bundle zzLW = zzatq.zzbrH.zzLW();
                if ("_iap".equals(zzatq.name)) {
                    zzLW.putLong("_c", 1);
                    zzKl().zzMe().log("Marking in-app purchase as real-time");
                    zzLW.putLong("_r", 1);
                }
                zzLW.putString("_o", zzatq.zzbqW);
                if (zzKh().zzge(zze.zzaS)) {
                    zzKh().zza(zzLW, "_dbg", (Object) 1L);
                    zzKh().zza(zzLW, "_r", (Object) 1L);
                }
                zzatn zzQ = zzKg().zzQ(str, zzatq.name);
                if (zzQ == null) {
                    zzKg().zza(new zzatn(str, zzatq.name, 1, 0, zzatq.zzbrI));
                    j = 0;
                } else {
                    j = zzQ.zzbrD;
                    zzKg().zza(zzQ.zzap(zzatq.zzbrI).zzLV());
                }
                zzatm zzatm = new zzatm(this, zzatq.zzbqW, str, zzatq.name, zzatq.zzbrI, j, zzLW);
                zzauw.zzb zzb = new zzauw.zzb();
                zze.zzbxj = new zzauw.zzb[]{zzb};
                zzb.zzbxc = Long.valueOf(zzatm.zzaxb);
                zzb.name = zzatm.mName;
                zzb.zzbxd = Long.valueOf(zzatm.zzbrz);
                zzb.zzbxb = new zzauw.zzc[zzatm.zzbrA.size()];
                Iterator<String> it = zzatm.zzbrA.iterator();
                int i2 = 0;
                while (it.hasNext()) {
                    String next = it.next();
                    zzauw.zzc zzc = new zzauw.zzc();
                    zzb.zzbxb[i2] = zzc;
                    zzc.name = next;
                    zzKh().zza(zzc, zzatm.zzbrA.get(next));
                    i2++;
                }
                zze.zzbxC = zza(zzfu.zzke(), zze.zzbxk, zze.zzbxj);
                zze.zzbxm = zzb.zzbxc;
                zze.zzbxn = zzb.zzbxc;
                long zzKs = zzfu.zzKs();
                zze.zzbxp = zzKs != 0 ? Long.valueOf(zzKs) : null;
                long zzKr = zzfu.zzKr();
                if (zzKr != 0) {
                    zzKs = zzKr;
                }
                zze.zzbxo = zzKs != 0 ? Long.valueOf(zzKs) : null;
                zzfu.zzKB();
                zze.zzbxA = Integer.valueOf((int) zzfu.zzKy());
                zze.zzbxv = Long.valueOf(zzKn().zzKv());
                zze.zzbxl = Long.valueOf(zznR().currentTimeMillis());
                zze.zzbxB = Boolean.TRUE;
                zzfu.zzY(zze.zzbxm.longValue());
                zzfu.zzZ(zze.zzbxn.longValue());
                zzKg().zza(zzfu);
                zzKg().setTransactionSuccessful();
                zzKg().endTransaction();
                try {
                    byte[] bArr2 = new byte[zzd.zzafB()];
                    zzbyc zzah = zzbyc.zzah(bArr2);
                    zzd.zza(zzah);
                    zzah.zzafo();
                    return zzKh().zzk(bArr2);
                } catch (IOException e2) {
                    zzKl().zzLZ().zze("Data loss. Failed to bundle and serialize. appId", zzatx.zzfE(str), e2);
                    return null;
                }
            }
        } finally {
            zzKg().endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzaq(long j) {
        return zzl(null, j);
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzatc zzatc) {
        ArrayMap arrayMap = null;
        if (TextUtils.isEmpty(zzatc.getGmpAppId())) {
            zzb(zzatc.zzke(), 204, null, null, null);
            return;
        }
        String zzP = zzKn().zzP(zzatc.getGmpAppId(), zzatc.getAppInstanceId());
        try {
            URL url = new URL(zzP);
            zzKl().zzMf().zzj("Fetching remote configuration", zzatc.zzke());
            zzauv.zzb zzfL = zzKi().zzfL(zzatc.zzke());
            String zzfM = zzKi().zzfM(zzatc.zzke());
            if (zzfL != null && !TextUtils.isEmpty(zzfM)) {
                arrayMap = new ArrayMap();
                arrayMap.put("If-Modified-Since", zzfM);
            }
            zzMz().zza(zzatc.zzke(), url, arrayMap, new zzaty.zza() {
                public void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
                    zzaue.this.zzb(str, i, th, bArr, map);
                }
            });
        } catch (MalformedURLException e2) {
            zzKl().zzLZ().zze("Failed to parse config URL. Not fetching. appId", zzatx.zzfE(zzatc.zzke()), zzP);
        }
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzatd zzatd, long j) {
        zzmR();
        zzob();
        Bundle bundle = new Bundle();
        bundle.putLong("_c", 1);
        bundle.putLong("_r", 1);
        zzb(new zzatq("_v", new zzato(bundle), "auto", j), zzatd);
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzatg zzatg, zzatd zzatd) {
        boolean z;
        zzac.zzw(zzatg);
        zzac.zzdr(zzatg.packageName);
        zzac.zzw(zzatg.zzbqW);
        zzac.zzw(zzatg.zzbqX);
        zzac.zzdr(zzatg.zzbqX.name);
        zzmR();
        zzob();
        if (!TextUtils.isEmpty(zzatd.zzbqK)) {
            if (!zzatd.zzbqP) {
                zzf(zzatd);
                return;
            }
            zzatg zzatg2 = new zzatg(zzatg);
            zzKg().beginTransaction();
            try {
                zzatg zzT = zzKg().zzT(zzatg2.packageName, zzatg2.zzbqX.name);
                if (zzT != null && zzT.zzbqZ) {
                    zzatg2.zzbqW = zzT.zzbqW;
                    zzatg2.zzbqY = zzT.zzbqY;
                    zzatg2.zzbra = zzT.zzbra;
                    zzatg2.zzbrd = zzT.zzbrd;
                    z = false;
                } else if (TextUtils.isEmpty(zzatg2.zzbra)) {
                    zzauq zzauq = zzatg2.zzbqX;
                    zzatg2.zzbqX = new zzauq(zzauq.name, zzatg2.zzbqY, zzauq.getValue(), zzauq.zzbqW);
                    zzatg2.zzbqZ = true;
                    z = true;
                } else {
                    z = false;
                }
                if (zzatg2.zzbqZ) {
                    zzauq zzauq2 = zzatg2.zzbqX;
                    zzaus zzaus = new zzaus(zzatg2.packageName, zzatg2.zzbqW, zzauq2.name, zzauq2.zzbwf, zzauq2.getValue());
                    if (zzKg().zza(zzaus)) {
                        zzKl().zzMe().zzd("User property updated immediately", zzatg2.packageName, zzaus.mName, zzaus.mValue);
                    } else {
                        zzKl().zzLZ().zzd("(2)Too many active user properties, ignoring", zzatx.zzfE(zzatg2.packageName), zzaus.mName, zzaus.mValue);
                    }
                    if (z && zzatg2.zzbrd != null) {
                        zzc(new zzatq(zzatg2.zzbrd, zzatg2.zzbqY), zzatd);
                    }
                }
                if (zzKg().zza(zzatg2)) {
                    zzKl().zzMe().zzd("Conditional property added", zzatg2.packageName, zzatg2.zzbqX.name, zzatg2.zzbqX.getValue());
                } else {
                    zzKl().zzLZ().zzd("Too many conditional properties, ignoring", zzatx.zzfE(zzatg2.packageName), zzatg2.zzbqX.name, zzatg2.zzbqX.getValue());
                }
                zzKg().setTransactionSuccessful();
            } finally {
                zzKg().endTransaction();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzatq zzatq, zzatd zzatd) {
        zzac.zzw(zzatd);
        zzac.zzdr(zzatd.packageName);
        zzmR();
        zzob();
        String str = zzatd.packageName;
        long j = zzatq.zzbrI;
        if (zzKh().zzd(zzatq, zzatd)) {
            if (!zzatd.zzbqP) {
                zzf(zzatd);
                return;
            }
            zzKg().beginTransaction();
            try {
                for (zzatg next : zzKg().zzh(str, j)) {
                    if (next != null) {
                        zzKl().zzMe().zzd("User property timed out", next.packageName, next.zzbqX.name, next.zzbqX.getValue());
                        if (next.zzbrb != null) {
                            zzc(new zzatq(next.zzbrb, j), zzatd);
                        }
                        zzKg().zzU(str, next.zzbqX.name);
                    }
                }
                List<zzatg> zzi = zzKg().zzi(str, j);
                ArrayList<zzatq> arrayList = new ArrayList<>(zzi.size());
                for (zzatg next2 : zzi) {
                    if (next2 != null) {
                        zzKl().zzMe().zzd("User property expired", next2.packageName, next2.zzbqX.name, next2.zzbqX.getValue());
                        zzKg().zzR(str, next2.zzbqX.name);
                        if (next2.zzbrf != null) {
                            arrayList.add(next2.zzbrf);
                        }
                        zzKg().zzU(str, next2.zzbqX.name);
                    }
                }
                for (zzatq zzatq2 : arrayList) {
                    zzc(new zzatq(zzatq2, j), zzatd);
                }
                List<zzatg> zzc = zzKg().zzc(str, zzatq.name, j);
                ArrayList<zzatq> arrayList2 = new ArrayList<>(zzc.size());
                for (zzatg next3 : zzc) {
                    if (next3 != null) {
                        zzauq zzauq = next3.zzbqX;
                        zzaus zzaus = new zzaus(next3.packageName, next3.zzbqW, zzauq.name, j, zzauq.getValue());
                        if (zzKg().zza(zzaus)) {
                            zzKl().zzMe().zzd("User property triggered", next3.packageName, zzaus.mName, zzaus.mValue);
                        } else {
                            zzKl().zzLZ().zzd("Too many active user properties, ignoring", zzatx.zzfE(next3.packageName), zzaus.mName, zzaus.mValue);
                        }
                        if (next3.zzbrd != null) {
                            arrayList2.add(next3.zzbrd);
                        }
                        next3.zzbqX = new zzauq(zzaus);
                        next3.zzbqZ = true;
                        zzKg().zza(next3);
                    }
                }
                zzc(zzatq, zzatd);
                for (zzatq zzatq3 : arrayList2) {
                    zzc(new zzatq(zzatq3, j), zzatd);
                }
                zzKg().setTransactionSuccessful();
            } finally {
                zzKg().endTransaction();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzatd.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int]
     candidates:
      com.google.android.gms.internal.zzatd.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int):void
      com.google.android.gms.internal.zzatd.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int):void */
    /* access modifiers changed from: package-private */
    public void zzb(zzatq zzatq, String str) {
        zzatc zzfu = zzKg().zzfu(str);
        if (zzfu == null || TextUtils.isEmpty(zzfu.zzmZ())) {
            zzKl().zzMe().zzj("No app data available; dropping event", str);
            return;
        }
        try {
            String str2 = zzadg.zzbi(getContext()).getPackageInfo(str, 0).versionName;
            if (zzfu.zzmZ() != null && !zzfu.zzmZ().equals(str2)) {
                zzKl().zzMb().zzj("App version does not match; dropping event. appId", zzatx.zzfE(str));
                return;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            if (!"_ui".equals(zzatq.name)) {
                zzKl().zzMb().zzj("Could not find package. appId", zzatx.zzfE(str));
            }
        }
        zzatq zzatq2 = zzatq;
        zzb(zzatq2, new zzatd(str, zzfu.getGmpAppId(), zzfu.zzmZ(), zzfu.zzKt(), zzfu.zzKu(), zzfu.zzKv(), zzfu.zzKw(), (String) null, zzfu.zzKx(), false, zzfu.zzKq(), zzfu.zzuW(), 0L, 0));
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzauh zzauh) {
        this.zzbuD++;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.zzaut.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauu$zzc):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zze):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zza[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzb[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzc[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzg[]):void
      com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.Object, boolean):int
      com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public void zzb(zzauq zzauq, zzatd zzatd) {
        int i = 0;
        zzmR();
        zzob();
        if (!TextUtils.isEmpty(zzatd.zzbqK)) {
            if (!zzatd.zzbqP) {
                zzf(zzatd);
                return;
            }
            int zzfX = zzKh().zzfX(zzauq.name);
            if (zzfX != 0) {
                String zza2 = zzKh().zza(zzauq.name, zzKn().zzKN(), true);
                if (zzauq.name != null) {
                    i = zzauq.name.length();
                }
                zzKh().zza(zzfX, "_ev", zza2, i);
                return;
            }
            int zzl = zzKh().zzl(zzauq.name, zzauq.getValue());
            if (zzl != 0) {
                String zza3 = zzKh().zza(zzauq.name, zzKn().zzKN(), true);
                Object value = zzauq.getValue();
                if (value != null && ((value instanceof String) || (value instanceof CharSequence))) {
                    i = String.valueOf(value).length();
                }
                zzKh().zza(zzl, "_ev", zza3, i);
                return;
            }
            Object zzm = zzKh().zzm(zzauq.name, zzauq.getValue());
            if (zzm != null) {
                zzaus zzaus = new zzaus(zzatd.packageName, zzauq.zzbqW, zzauq.name, zzauq.zzbwf, zzm);
                zzKl().zzMe().zze("Setting user property", zzaus.mName, zzm);
                zzKg().beginTransaction();
                try {
                    zzf(zzatd);
                    boolean zza4 = zzKg().zza(zzaus);
                    zzKg().setTransactionSuccessful();
                    if (zza4) {
                        zzKl().zzMe().zze("User property set", zzaus.mName, zzaus.mValue);
                    } else {
                        zzKl().zzLZ().zze("Too many unique user properties are set. Ignoring user property", zzaus.mName, zzaus.mValue);
                        zzKh().zza(9, (String) null, (String) null, 0);
                    }
                } finally {
                    zzKg().endTransaction();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzb(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        boolean z = false;
        zzmR();
        zzob();
        zzac.zzdr(str);
        if (bArr == null) {
            bArr = new byte[0];
        }
        zzKg().beginTransaction();
        try {
            zzatc zzfu = zzKg().zzfu(str);
            boolean z2 = (i == 200 || i == 204 || i == 304) && th == null;
            if (zzfu == null) {
                zzKl().zzMb().zzj("App does not exist in onConfigFetched. appId", zzatx.zzfE(str));
            } else if (z2 || i == 404) {
                List list = map != null ? map.get("Last-Modified") : null;
                String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
                if (i == 404 || i == 304) {
                    if (zzKi().zzfL(str) == null && !zzKi().zzb(str, null, null)) {
                        zzKg().endTransaction();
                        return;
                    }
                } else if (!zzKi().zzb(str, bArr, str2)) {
                    zzKg().endTransaction();
                    return;
                }
                zzfu.zzae(zznR().currentTimeMillis());
                zzKg().zza(zzfu);
                if (i == 404) {
                    zzKl().zzMc().zzj("Config not found. Using empty config. appId", str);
                } else {
                    zzKl().zzMf().zze("Successfully fetched config. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                }
                if (!zzMz().zzqa() || !zzMJ()) {
                    zzMK();
                } else {
                    zzMI();
                }
            } else {
                zzfu.zzaf(zznR().currentTimeMillis());
                zzKg().zza(zzfu);
                zzKl().zzMf().zze("Fetching config failed. code, error", Integer.valueOf(i), th);
                zzKi().zzfN(str);
                zzKm().zzbtc.set(zznR().currentTimeMillis());
                if (i == 503 || i == 429) {
                    z = true;
                }
                if (z) {
                    zzKm().zzbtd.set(zznR().currentTimeMillis());
                }
                zzMK();
            }
            zzKg().setTransactionSuccessful();
        } finally {
            zzKg().endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    public void zzc(zzatd zzatd, long j) {
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        zzmR();
        zzob();
        zzatc zzfu = zzKg().zzfu(zzatd.packageName);
        if (zzfu != null && TextUtils.isEmpty(zzfu.getGmpAppId()) && zzatd != null && !TextUtils.isEmpty(zzatd.zzbqK)) {
            zzfu.zzae(0);
            zzKg().zza(zzfu);
        }
        Bundle bundle = new Bundle();
        bundle.putLong("_c", 1);
        bundle.putLong("_r", 1);
        bundle.putLong("_uwa", 0);
        bundle.putLong("_pfo", 0);
        bundle.putLong("_sys", 0);
        bundle.putLong("_sysu", 0);
        if (getContext().getPackageManager() == null) {
            zzKl().zzLZ().zzj("PackageManager is null, first open report might be inaccurate. appId", zzatx.zzfE(zzatd.packageName));
        } else {
            try {
                packageInfo = zzadg.zzbi(getContext()).getPackageInfo(zzatd.packageName, 0);
            } catch (PackageManager.NameNotFoundException e2) {
                zzKl().zzLZ().zze("Package info is null, first open report might be inaccurate. appId", zzatx.zzfE(zzatd.packageName), e2);
                packageInfo = null;
            }
            if (!(packageInfo == null || packageInfo.firstInstallTime == 0 || packageInfo.firstInstallTime == packageInfo.lastUpdateTime)) {
                bundle.putLong("_uwa", 1);
            }
            try {
                applicationInfo = zzadg.zzbi(getContext()).getApplicationInfo(zzatd.packageName, 0);
            } catch (PackageManager.NameNotFoundException e3) {
                zzKl().zzLZ().zze("Application info is null, first open report might be inaccurate. appId", zzatx.zzfE(zzatd.packageName), e3);
                applicationInfo = null;
            }
            if (applicationInfo != null) {
                if ((applicationInfo.flags & 1) != 0) {
                    bundle.putLong("_sys", 1);
                }
                if ((applicationInfo.flags & FileUtils.FileMode.MODE_IWUSR) != 0) {
                    bundle.putLong("_sysu", 1);
                }
            }
        }
        long zzfA = zzKg().zzfA(zzatd.packageName);
        if (zzfA >= 0) {
            bundle.putLong("_pfo", zzfA);
        }
        zzb(new zzatq("_f", new zzato(bundle), "auto", j), zzatd);
    }

    /* access modifiers changed from: package-private */
    public void zzc(zzatg zzatg, zzatd zzatd) {
        zzac.zzw(zzatg);
        zzac.zzdr(zzatg.packageName);
        zzac.zzw(zzatg.zzbqX);
        zzac.zzdr(zzatg.zzbqX.name);
        zzmR();
        zzob();
        if (!TextUtils.isEmpty(zzatd.zzbqK)) {
            if (!zzatd.zzbqP) {
                zzf(zzatd);
                return;
            }
            zzKg().beginTransaction();
            try {
                zzf(zzatd);
                zzatg zzT = zzKg().zzT(zzatg.packageName, zzatg.zzbqX.name);
                if (zzT != null) {
                    zzKl().zzMe().zze("Removing conditional user property", zzatg.packageName, zzatg.zzbqX.name);
                    zzKg().zzU(zzatg.packageName, zzatg.zzbqX.name);
                    if (zzT.zzbqZ) {
                        zzKg().zzR(zzatg.packageName, zzatg.zzbqX.name);
                    }
                    if (zzatg.zzbrf != null) {
                        Bundle bundle = null;
                        if (zzatg.zzbrf.zzbrH != null) {
                            bundle = zzatg.zzbrf.zzbrH.zzLW();
                        }
                        zzc(zzKh().zza(zzatg.zzbrf.name, bundle, zzT.zzbqW, zzatg.zzbrf.zzbrI, true, false), zzatd);
                    }
                } else {
                    zzKl().zzMb().zze("Conditional user property doesn't exist", zzatx.zzfE(zzatg.packageName), zzatg.zzbqX.name);
                }
                zzKg().setTransactionSuccessful();
            } finally {
                zzKg().endTransaction();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.internal.zzaut.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauu$zzc):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zze):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zza[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzb[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzc[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzg[]):void
      com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.Object, boolean):int
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:64:0x0259=Splitter:B:64:0x0259, B:93:0x034f=Splitter:B:93:0x034f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zzc(com.google.android.gms.internal.zzatq r19, com.google.android.gms.internal.zzatd r20) {
        /*
            r18 = this;
            com.google.android.gms.common.internal.zzac.zzw(r20)
            r0 = r20
            java.lang.String r2 = r0.packageName
            com.google.android.gms.common.internal.zzac.zzdr(r2)
            long r16 = java.lang.System.nanoTime()
            r18.zzmR()
            r18.zzob()
            r0 = r20
            java.lang.String r3 = r0.packageName
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()
            r0 = r19
            r1 = r20
            boolean r2 = r2.zzd(r0, r1)
            if (r2 != 0) goto L_0x0027
        L_0x0026:
            return
        L_0x0027:
            r0 = r20
            boolean r2 = r0.zzbqP
            if (r2 != 0) goto L_0x0035
            r0 = r18
            r1 = r20
            r0.zzf(r1)
            goto L_0x0026
        L_0x0035:
            com.google.android.gms.internal.zzauc r2 = r18.zzKi()
            r0 = r19
            java.lang.String r4 = r0.name
            boolean r2 = r2.zzaa(r3, r4)
            if (r2 == 0) goto L_0x00d3
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzMb()
            java.lang.String r4 = "Dropping blacklisted event. appId"
            java.lang.Object r5 = com.google.android.gms.internal.zzatx.zzfE(r3)
            r0 = r19
            java.lang.String r6 = r0.name
            r2.zze(r4, r5, r6)
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()
            boolean r2 = r2.zzgg(r3)
            if (r2 != 0) goto L_0x006c
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()
            boolean r2 = r2.zzgh(r3)
            if (r2 == 0) goto L_0x00d1
        L_0x006c:
            r2 = 1
        L_0x006d:
            if (r2 != 0) goto L_0x008b
            java.lang.String r4 = "_err"
            r0 = r19
            java.lang.String r5 = r0.name
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x008b
            com.google.android.gms.internal.zzaut r4 = r18.zzKh()
            r5 = 11
            java.lang.String r6 = "_ev"
            r0 = r19
            java.lang.String r7 = r0.name
            r8 = 0
            r4.zza(r5, r6, r7, r8)
        L_0x008b:
            if (r2 == 0) goto L_0x0026
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            com.google.android.gms.internal.zzatc r2 = r2.zzfu(r3)
            if (r2 == 0) goto L_0x0026
            long r4 = r2.zzKA()
            long r6 = r2.zzKz()
            long r4 = java.lang.Math.max(r4, r6)
            com.google.android.gms.common.util.zze r3 = r18.zznR()
            long r6 = r3.currentTimeMillis()
            long r4 = r6 - r4
            long r4 = java.lang.Math.abs(r4)
            com.google.android.gms.internal.zzati r3 = r18.zzKn()
            long r6 = r3.zzLm()
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x0026
            com.google.android.gms.internal.zzatx r3 = r18.zzKl()
            com.google.android.gms.internal.zzatx$zza r3 = r3.zzMe()
            java.lang.String r4 = "Fetching config for blacklisted app"
            r3.log(r4)
            r0 = r18
            r0.zzb(r2)
            goto L_0x0026
        L_0x00d1:
            r2 = 0
            goto L_0x006d
        L_0x00d3:
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()
            r4 = 2
            boolean r2 = r2.zzak(r4)
            if (r2 == 0) goto L_0x00ed
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzMf()
            java.lang.String r4 = "Logging event"
            r0 = r19
            r2.zzj(r4, r0)
        L_0x00ed:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.beginTransaction()
            r0 = r19
            com.google.android.gms.internal.zzato r2 = r0.zzbrH     // Catch:{ all -> 0x028e }
            android.os.Bundle r14 = r2.zzLW()     // Catch:{ all -> 0x028e }
            r0 = r18
            r1 = r20
            r0.zzf(r1)     // Catch:{ all -> 0x028e }
            java.lang.String r2 = "_iap"
            r0 = r19
            java.lang.String r4 = r0.name     // Catch:{ all -> 0x028e }
            boolean r2 = r2.equals(r4)     // Catch:{ all -> 0x028e }
            if (r2 != 0) goto L_0x011b
            java.lang.String r2 = "ecommerce_purchase"
            r0 = r19
            java.lang.String r4 = r0.name     // Catch:{ all -> 0x028e }
            boolean r2 = r2.equals(r4)     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x01e6
        L_0x011b:
            java.lang.String r2 = "currency"
            java.lang.String r2 = r14.getString(r2)     // Catch:{ all -> 0x028e }
            java.lang.String r4 = "ecommerce_purchase"
            r0 = r19
            java.lang.String r5 = r0.name     // Catch:{ all -> 0x028e }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x028e }
            if (r4 == 0) goto L_0x027e
            java.lang.String r4 = "value"
            double r4 = r14.getDouble(r4)     // Catch:{ all -> 0x028e }
            r6 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r4 = r4 * r6
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x014c
            java.lang.String r4 = "value"
            long r4 = r14.getLong(r4)     // Catch:{ all -> 0x028e }
            double r4 = (double) r4     // Catch:{ all -> 0x028e }
            r6 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r4 = r4 * r6
        L_0x014c:
            r6 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 > 0) goto L_0x0259
            r6 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 < 0) goto L_0x0259
            long r4 = java.lang.Math.round(r4)     // Catch:{ all -> 0x028e }
            r8 = r4
        L_0x015d:
            boolean r4 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x028e }
            if (r4 != 0) goto L_0x01e6
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ all -> 0x028e }
            java.lang.String r2 = r2.toUpperCase(r4)     // Catch:{ all -> 0x028e }
            java.lang.String r4 = "[A-Z]{3}"
            boolean r4 = r2.matches(r4)     // Catch:{ all -> 0x028e }
            if (r4 == 0) goto L_0x01e6
            java.lang.String r4 = "_ltv_"
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x028e }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x028e }
            int r5 = r2.length()     // Catch:{ all -> 0x028e }
            if (r5 == 0) goto L_0x0287
            java.lang.String r5 = r4.concat(r2)     // Catch:{ all -> 0x028e }
        L_0x0185:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaus r2 = r2.zzS(r3, r5)     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x0195
            java.lang.Object r4 = r2.mValue     // Catch:{ all -> 0x028e }
            boolean r4 = r4 instanceof java.lang.Long     // Catch:{ all -> 0x028e }
            if (r4 != 0) goto L_0x0297
        L_0x0195:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzati r4 = r18.zzKn()     // Catch:{ all -> 0x028e }
            int r4 = r4.zzfn(r3)     // Catch:{ all -> 0x028e }
            int r4 = r4 + -1
            r2.zzz(r3, r4)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaus r2 = new com.google.android.gms.internal.zzaus     // Catch:{ all -> 0x028e }
            r0 = r19
            java.lang.String r4 = r0.zzbqW     // Catch:{ all -> 0x028e }
            com.google.android.gms.common.util.zze r6 = r18.zznR()     // Catch:{ all -> 0x028e }
            long r6 = r6.currentTimeMillis()     // Catch:{ all -> 0x028e }
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x028e }
            r2.<init>(r3, r4, r5, r6, r8)     // Catch:{ all -> 0x028e }
        L_0x01bb:
            com.google.android.gms.internal.zzatj r4 = r18.zzKg()     // Catch:{ all -> 0x028e }
            boolean r4 = r4.zza(r2)     // Catch:{ all -> 0x028e }
            if (r4 != 0) goto L_0x01e6
            com.google.android.gms.internal.zzatx r4 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r4 = r4.zzLZ()     // Catch:{ all -> 0x028e }
            java.lang.String r5 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r6 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            java.lang.String r7 = r2.mName     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.mValue     // Catch:{ all -> 0x028e }
            r4.zzd(r5, r6, r7, r2)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            r4 = 9
            r5 = 0
            r6 = 0
            r7 = 0
            r2.zza(r4, r5, r6, r7)     // Catch:{ all -> 0x028e }
        L_0x01e6:
            r0 = r19
            java.lang.String r2 = r0.name     // Catch:{ all -> 0x028e }
            boolean r10 = com.google.android.gms.internal.zzaut.zzfT(r2)     // Catch:{ all -> 0x028e }
            java.lang.String r2 = "_err"
            r0 = r19
            java.lang.String r4 = r0.name     // Catch:{ all -> 0x028e }
            boolean r12 = r2.equals(r4)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r5 = r18.zzKg()     // Catch:{ all -> 0x028e }
            long r6 = r18.zzMG()     // Catch:{ all -> 0x028e }
            r9 = 1
            r11 = 0
            r13 = 0
            r8 = r3
            com.google.android.gms.internal.zzatj$zza r2 = r5.zza(r6, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x028e }
            long r4 = r2.zzbrp     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzati r6 = r18.zzKn()     // Catch:{ all -> 0x028e }
            long r6 = r6.zzKV()     // Catch:{ all -> 0x028e }
            long r4 = r4 - r6
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x02b7
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 % r6
            r6 = 1
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x0239
            com.google.android.gms.internal.zzatx r4 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r4 = r4.zzLZ()     // Catch:{ all -> 0x028e }
            java.lang.String r5 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r3 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            long r6 = r2.zzbrp     // Catch:{ all -> 0x028e }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x028e }
            r4.zze(r5, r3, r2)     // Catch:{ all -> 0x028e }
        L_0x0239:
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            r3 = 16
            java.lang.String r4 = "_ev"
            r0 = r19
            java.lang.String r5 = r0.name     // Catch:{ all -> 0x028e }
            r6 = 0
            r2.zza(r3, r4, r5, r6)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.endTransaction()
            goto L_0x0026
        L_0x0259:
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzMb()     // Catch:{ all -> 0x028e }
            java.lang.String r6 = "Data lost. Currency value is too big. appId"
            java.lang.Object r3 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ all -> 0x028e }
            r2.zze(r6, r3, r4)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.endTransaction()
            goto L_0x0026
        L_0x027e:
            java.lang.String r4 = "value"
            long r4 = r14.getLong(r4)     // Catch:{ all -> 0x028e }
            r8 = r4
            goto L_0x015d
        L_0x0287:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x028e }
            r5.<init>(r4)     // Catch:{ all -> 0x028e }
            goto L_0x0185
        L_0x028e:
            r2 = move-exception
            com.google.android.gms.internal.zzatj r3 = r18.zzKg()
            r3.endTransaction()
            throw r2
        L_0x0297:
            java.lang.Object r2 = r2.mValue     // Catch:{ all -> 0x028e }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x028e }
            long r10 = r2.longValue()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaus r2 = new com.google.android.gms.internal.zzaus     // Catch:{ all -> 0x028e }
            r0 = r19
            java.lang.String r4 = r0.zzbqW     // Catch:{ all -> 0x028e }
            com.google.android.gms.common.util.zze r6 = r18.zznR()     // Catch:{ all -> 0x028e }
            long r6 = r6.currentTimeMillis()     // Catch:{ all -> 0x028e }
            long r8 = r8 + r10
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x028e }
            r2.<init>(r3, r4, r5, r6, r8)     // Catch:{ all -> 0x028e }
            goto L_0x01bb
        L_0x02b7:
            if (r10 == 0) goto L_0x030a
            long r4 = r2.zzbro     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzati r6 = r18.zzKn()     // Catch:{ all -> 0x028e }
            long r6 = r6.zzKW()     // Catch:{ all -> 0x028e }
            long r4 = r4 - r6
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x030a
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 % r6
            r6 = 1
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x02ea
            com.google.android.gms.internal.zzatx r4 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r4 = r4.zzLZ()     // Catch:{ all -> 0x028e }
            java.lang.String r5 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r3 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            long r6 = r2.zzbro     // Catch:{ all -> 0x028e }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x028e }
            r4.zze(r5, r3, r2)     // Catch:{ all -> 0x028e }
        L_0x02ea:
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            r3 = 16
            java.lang.String r4 = "_ev"
            r0 = r19
            java.lang.String r5 = r0.name     // Catch:{ all -> 0x028e }
            r6 = 0
            r2.zza(r3, r4, r5, r6)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.endTransaction()
            goto L_0x0026
        L_0x030a:
            if (r12 == 0) goto L_0x034f
            long r4 = r2.zzbrr     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzati r6 = r18.zzKn()     // Catch:{ all -> 0x028e }
            r0 = r20
            java.lang.String r7 = r0.packageName     // Catch:{ all -> 0x028e }
            int r6 = r6.zzfj(r7)     // Catch:{ all -> 0x028e }
            long r6 = (long) r6     // Catch:{ all -> 0x028e }
            long r4 = r4 - r6
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x034f
            r6 = 1
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x033f
            com.google.android.gms.internal.zzatx r4 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r4 = r4.zzLZ()     // Catch:{ all -> 0x028e }
            java.lang.String r5 = "Too many error events logged. appId, count"
            java.lang.Object r3 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            long r6 = r2.zzbrr     // Catch:{ all -> 0x028e }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x028e }
            r4.zze(r5, r3, r2)     // Catch:{ all -> 0x028e }
        L_0x033f:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.endTransaction()
            goto L_0x0026
        L_0x034f:
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            java.lang.String r4 = "_o"
            r0 = r19
            java.lang.String r5 = r0.zzbqW     // Catch:{ all -> 0x028e }
            r2.zza(r14, r4, r5)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            boolean r2 = r2.zzge(r3)     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x0384
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            java.lang.String r4 = "_dbg"
            r6 = 1
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x028e }
            r2.zza(r14, r4, r5)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            java.lang.String r4 = "_r"
            r6 = 1
            java.lang.Long r5 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x028e }
            r2.zza(r14, r4, r5)     // Catch:{ all -> 0x028e }
        L_0x0384:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            long r4 = r2.zzfv(r3)     // Catch:{ all -> 0x028e }
            r6 = 0
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x03a7
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzMb()     // Catch:{ all -> 0x028e }
            java.lang.String r6 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r7 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x028e }
            r2.zze(r6, r7, r4)     // Catch:{ all -> 0x028e }
        L_0x03a7:
            com.google.android.gms.internal.zzatm r5 = new com.google.android.gms.internal.zzatm     // Catch:{ all -> 0x028e }
            r0 = r19
            java.lang.String r7 = r0.zzbqW     // Catch:{ all -> 0x028e }
            r0 = r19
            java.lang.String r9 = r0.name     // Catch:{ all -> 0x028e }
            r0 = r19
            long r10 = r0.zzbrI     // Catch:{ all -> 0x028e }
            r12 = 0
            r6 = r18
            r8 = r3
            r5.<init>(r6, r7, r8, r9, r10, r12, r14)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            java.lang.String r4 = r5.mName     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatn r2 = r2.zzQ(r3, r4)     // Catch:{ all -> 0x028e }
            if (r2 != 0) goto L_0x0478
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            long r6 = r2.zzfC(r3)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzati r2 = r18.zzKn()     // Catch:{ all -> 0x028e }
            r2.zzKU()     // Catch:{ all -> 0x028e }
            r8 = 500(0x1f4, double:2.47E-321)
            int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r2 < 0) goto L_0x0412
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzLZ()     // Catch:{ all -> 0x028e }
            java.lang.String r4 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r3 = com.google.android.gms.internal.zzatx.zzfE(r3)     // Catch:{ all -> 0x028e }
            java.lang.String r5 = r5.mName     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzati r6 = r18.zzKn()     // Catch:{ all -> 0x028e }
            int r6 = r6.zzKU()     // Catch:{ all -> 0x028e }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x028e }
            r2.zzd(r4, r3, r5, r6)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzaut r2 = r18.zzKh()     // Catch:{ all -> 0x028e }
            r3 = 8
            r4 = 0
            r5 = 0
            r6 = 0
            r2.zza(r3, r4, r5, r6)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.endTransaction()
            goto L_0x0026
        L_0x0412:
            com.google.android.gms.internal.zzatn r7 = new com.google.android.gms.internal.zzatn     // Catch:{ all -> 0x028e }
            java.lang.String r9 = r5.mName     // Catch:{ all -> 0x028e }
            r10 = 0
            r12 = 0
            long r14 = r5.zzaxb     // Catch:{ all -> 0x028e }
            r8 = r3
            r7.<init>(r8, r9, r10, r12, r14)     // Catch:{ all -> 0x028e }
        L_0x0420:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            r2.zza(r7)     // Catch:{ all -> 0x028e }
            r0 = r18
            r1 = r20
            r0.zza(r5, r1)     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()     // Catch:{ all -> 0x028e }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()     // Catch:{ all -> 0x028e }
            r3 = 2
            boolean r2 = r2.zzak(r3)     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x044d
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzMf()     // Catch:{ all -> 0x028e }
            java.lang.String r3 = "Event recorded"
            r2.zzj(r3, r5)     // Catch:{ all -> 0x028e }
        L_0x044d:
            com.google.android.gms.internal.zzatj r2 = r18.zzKg()
            r2.endTransaction()
            r18.zzMK()
            com.google.android.gms.internal.zzatx r2 = r18.zzKl()
            com.google.android.gms.internal.zzatx$zza r2 = r2.zzMf()
            java.lang.String r3 = "Background event processing time, ms"
            long r4 = java.lang.System.nanoTime()
            long r4 = r4 - r16
            r6 = 500000(0x7a120, double:2.47033E-318)
            long r4 = r4 + r6
            r6 = 1000000(0xf4240, double:4.940656E-318)
            long r4 = r4 / r6
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r2.zzj(r3, r4)
            goto L_0x0026
        L_0x0478:
            long r6 = r2.zzbrD     // Catch:{ all -> 0x028e }
            r0 = r18
            com.google.android.gms.internal.zzatm r5 = r5.zza(r0, r6)     // Catch:{ all -> 0x028e }
            long r6 = r5.zzaxb     // Catch:{ all -> 0x028e }
            com.google.android.gms.internal.zzatn r7 = r2.zzap(r6)     // Catch:{ all -> 0x028e }
            goto L_0x0420
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaue.zzc(com.google.android.gms.internal.zzatq, com.google.android.gms.internal.zzatd):void");
    }

    /* access modifiers changed from: package-private */
    public void zzc(zzauq zzauq, zzatd zzatd) {
        zzmR();
        zzob();
        if (!TextUtils.isEmpty(zzatd.zzbqK)) {
            if (!zzatd.zzbqP) {
                zzf(zzatd);
                return;
            }
            zzKl().zzMe().zzj("Removing user property", zzauq.name);
            zzKg().beginTransaction();
            try {
                zzf(zzatd);
                zzKg().zzR(zzatd.packageName, zzauq.name);
                zzKg().setTransactionSuccessful();
                zzKl().zzMe().zzj("User property removed", zzauq.name);
            } finally {
                zzKg().endTransaction();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzd(zzatd zzatd) {
        zzmR();
        zzob();
        zzac.zzdr(zzatd.packageName);
        zzf(zzatd);
    }

    /* access modifiers changed from: package-private */
    public void zzd(zzatd zzatd, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("_et", 1);
        zzb(new zzatq("_e", new zzato(bundle), "auto", j), zzatd);
    }

    /* access modifiers changed from: package-private */
    public void zzd(zzatg zzatg) {
        zzatd zzfO = zzfO(zzatg.packageName);
        if (zzfO != null) {
            zzb(zzatg, zzfO);
        }
    }

    public void zze(zzatd zzatd) {
        zzmR();
        zzob();
        zzac.zzw(zzatd);
        zzac.zzdr(zzatd.packageName);
        if (!TextUtils.isEmpty(zzatd.zzbqK)) {
            if (!zzatd.zzbqP) {
                zzf(zzatd);
                return;
            }
            long j = zzatd.zzbqU;
            if (j == 0) {
                j = zznR().currentTimeMillis();
            }
            int i = zzatd.zzbqV;
            if (!(i == 0 || i == 1)) {
                zzKl().zzMb().zze("Incorrect app type, assuming installed app. appId, appType", zzatx.zzfE(zzatd.packageName), Integer.valueOf(i));
                i = 0;
            }
            zzKg().beginTransaction();
            try {
                zza(zzatd, j);
                zzf(zzatd);
                zzatn zzatn = null;
                if (i == 0) {
                    zzatn = zzKg().zzQ(zzatd.packageName, "_f");
                } else if (i == 1) {
                    zzatn = zzKg().zzQ(zzatd.packageName, "_v");
                }
                if (zzatn == null) {
                    long j2 = (1 + (j / 3600000)) * 3600000;
                    if (i == 0) {
                        zzb(new zzauq("_fot", j, Long.valueOf(j2), "auto"), zzatd);
                        zzc(zzatd, j);
                    } else if (i == 1) {
                        zzb(new zzauq("_fvt", j, Long.valueOf(j2), "auto"), zzatd);
                        zzb(zzatd, j);
                    }
                    zzd(zzatd, j);
                } else if (zzatd.zzbqQ) {
                    zze(zzatd, j);
                }
                zzKg().setTransactionSuccessful();
            } finally {
                zzKg().endTransaction();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zze(zzatd zzatd, long j) {
        zzb(new zzatq("_cd", new zzato(new Bundle()), "auto", j), zzatd);
    }

    /* access modifiers changed from: package-private */
    public void zze(zzatg zzatg) {
        zzatd zzfO = zzfO(zzatg.packageName);
        if (zzfO != null) {
            zzc(zzatg, zzfO);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzatd.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int]
     candidates:
      com.google.android.gms.internal.zzatd.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int):void
      com.google.android.gms.internal.zzatd.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int):void */
    /* access modifiers changed from: package-private */
    public zzatd zzfO(String str) {
        zzatc zzfu = zzKg().zzfu(str);
        if (zzfu == null || TextUtils.isEmpty(zzfu.zzmZ())) {
            zzKl().zzMe().zzj("No app data available; dropping", str);
            return null;
        }
        try {
            String str2 = zzadg.zzbi(getContext()).getPackageInfo(str, 0).versionName;
            if (zzfu.zzmZ() != null && !zzfu.zzmZ().equals(str2)) {
                zzKl().zzMb().zzj("App version does not match; dropping. appId", zzatx.zzfE(str));
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
        }
        return new zzatd(str, zzfu.getGmpAppId(), zzfu.zzmZ(), zzfu.zzKt(), zzfu.zzKu(), zzfu.zzKv(), zzfu.zzKw(), (String) null, zzfu.zzKx(), false, zzfu.zzKq(), zzfu.zzuW(), 0L, 0);
    }

    public String zzfP(final String str) {
        try {
            return (String) zzKk().zzd(new Callable<String>() {
                /* renamed from: zzbY */
                public String call() {
                    zzatc zzfu = zzaue.this.zzKg().zzfu(str);
                    if (zzfu != null) {
                        return zzfu.getAppInstanceId();
                    }
                    zzaue.this.zzKl().zzMb().log("App info was null when attempting to get app instance id");
                    return null;
                }
            }).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            zzKl().zzLZ().zze("Failed to get app instance id. appId", zzatx.zzfE(str), e2);
            return null;
        }
    }

    public void zzmR() {
        zzKk().zzmR();
    }

    public zze zznR() {
        return this.zzuP;
    }

    /* access modifiers changed from: package-private */
    public void zzob() {
        if (!this.zzadP) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzy(int i, int i2) {
        zzmR();
        if (i > i2) {
            zzKl().zzLZ().zze("Panic: can't downgrade version. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
            return false;
        }
        if (i < i2) {
            if (zza(i2, zzMC())) {
                zzKl().zzMf().zze("Storage version upgraded. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
            } else {
                zzKl().zzLZ().zze("Storage version upgrade failed. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
                return false;
            }
        }
        return true;
    }
}
