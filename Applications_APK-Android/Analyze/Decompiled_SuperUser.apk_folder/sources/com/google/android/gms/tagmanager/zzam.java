package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzak;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

abstract class zzam {
    private final Set<String> zzbGm;
    private final String zzbGn;

    public zzam(String str, String... strArr) {
        this.zzbGn = str;
        this.zzbGm = new HashSet(strArr.length);
        for (String add : strArr) {
            this.zzbGm.add(add);
        }
    }

    public String zzQN() {
        return this.zzbGn;
    }

    public Set<String> zzQO() {
        return this.zzbGm;
    }

    public abstract boolean zzQd();

    public abstract zzak.zza zzZ(Map<String, zzak.zza> map);

    /* access modifiers changed from: package-private */
    public boolean zzf(Set<String> set) {
        return set.containsAll(this.zzbGm);
    }
}
