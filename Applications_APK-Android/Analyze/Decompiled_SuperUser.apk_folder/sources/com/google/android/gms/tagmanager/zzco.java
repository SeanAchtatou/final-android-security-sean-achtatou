package com.google.android.gms.tagmanager;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzco extends zzam {
    private static final String ID = zzah.RESOLUTION.toString();
    private final Context mContext;

    public zzco(Context context) {
        super(ID, new String[0]);
        this.mContext = context;
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        return zzdl.zzS(new StringBuilder(23).append(i).append("x").append(displayMetrics.heightPixels).toString());
    }
}
