package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzak extends zzam {
    private static final String ID = zzah.EVENT.toString();
    private final zzcx zzbEZ;

    public zzak(zzcx zzcx) {
        super(ID, new String[0]);
        this.zzbEZ = zzcx;
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        String zzRq = this.zzbEZ.zzRq();
        return zzRq == null ? zzdl.zzRT() : zzdl.zzS(zzRq);
    }
}
