package com.google.android.gms.internal;

public interface zzak {

    public static final class zza extends zzbyd<zza> {
        private static volatile zza[] zzlt;
        public String string;
        public int type;
        public boolean zzlA;
        public zza[] zzlB;
        public int[] zzlC;
        public boolean zzlD;
        public zza[] zzlu;
        public zza[] zzlv;
        public zza[] zzlw;
        public String zzlx;
        public String zzly;
        public long zzlz;

        public zza() {
            zzM();
        }

        public static zza[] zzL() {
            if (zzlt == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzlt == null) {
                        zzlt = new zza[0];
                    }
                }
            }
            return zzlt;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.type != zza.type) {
                return false;
            }
            if (this.string == null) {
                if (zza.string != null) {
                    return false;
                }
            } else if (!this.string.equals(zza.string)) {
                return false;
            }
            if (!zzbyh.equals(this.zzlu, zza.zzlu) || !zzbyh.equals(this.zzlv, zza.zzlv) || !zzbyh.equals(this.zzlw, zza.zzlw)) {
                return false;
            }
            if (this.zzlx == null) {
                if (zza.zzlx != null) {
                    return false;
                }
            } else if (!this.zzlx.equals(zza.zzlx)) {
                return false;
            }
            if (this.zzly == null) {
                if (zza.zzly != null) {
                    return false;
                }
            } else if (!this.zzly.equals(zza.zzly)) {
                return false;
            }
            if (this.zzlz == zza.zzlz && this.zzlA == zza.zzlA && zzbyh.equals(this.zzlB, zza.zzlB) && zzbyh.equals(this.zzlC, zza.zzlC) && this.zzlD == zza.zzlD) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zza.zzcwC == null || zza.zzcwC.isEmpty() : this.zzcwC.equals(zza.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            int i = 1231;
            int i2 = 0;
            int hashCode = ((((((this.zzlA ? 1231 : 1237) + (((((this.zzly == null ? 0 : this.zzly.hashCode()) + (((this.zzlx == null ? 0 : this.zzlx.hashCode()) + (((((((((this.string == null ? 0 : this.string.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + this.type) * 31)) * 31) + zzbyh.hashCode(this.zzlu)) * 31) + zzbyh.hashCode(this.zzlv)) * 31) + zzbyh.hashCode(this.zzlw)) * 31)) * 31)) * 31) + ((int) (this.zzlz ^ (this.zzlz >>> 32)))) * 31)) * 31) + zzbyh.hashCode(this.zzlB)) * 31) + zzbyh.hashCode(this.zzlC)) * 31;
            if (!this.zzlD) {
                i = 1237;
            }
            int i3 = (hashCode + i) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i2 = this.zzcwC.hashCode();
            }
            return i3 + i2;
        }

        public zza zzM() {
            this.type = 1;
            this.string = "";
            this.zzlu = zzL();
            this.zzlv = zzL();
            this.zzlw = zzL();
            this.zzlx = "";
            this.zzly = "";
            this.zzlz = 0;
            this.zzlA = false;
            this.zzlB = zzL();
            this.zzlC = zzbym.zzcwQ;
            this.zzlD = false;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            zzbyc.zzJ(1, this.type);
            if (this.string != null && !this.string.equals("")) {
                zzbyc.zzq(2, this.string);
            }
            if (this.zzlu != null && this.zzlu.length > 0) {
                for (zza zza : this.zzlu) {
                    if (zza != null) {
                        zzbyc.zza(3, zza);
                    }
                }
            }
            if (this.zzlv != null && this.zzlv.length > 0) {
                for (zza zza2 : this.zzlv) {
                    if (zza2 != null) {
                        zzbyc.zza(4, zza2);
                    }
                }
            }
            if (this.zzlw != null && this.zzlw.length > 0) {
                for (zza zza3 : this.zzlw) {
                    if (zza3 != null) {
                        zzbyc.zza(5, zza3);
                    }
                }
            }
            if (this.zzlx != null && !this.zzlx.equals("")) {
                zzbyc.zzq(6, this.zzlx);
            }
            if (this.zzly != null && !this.zzly.equals("")) {
                zzbyc.zzq(7, this.zzly);
            }
            if (this.zzlz != 0) {
                zzbyc.zzb(8, this.zzlz);
            }
            if (this.zzlD) {
                zzbyc.zzg(9, this.zzlD);
            }
            if (this.zzlC != null && this.zzlC.length > 0) {
                for (int zzJ : this.zzlC) {
                    zzbyc.zzJ(10, zzJ);
                }
            }
            if (this.zzlB != null && this.zzlB.length > 0) {
                for (zza zza4 : this.zzlB) {
                    if (zza4 != null) {
                        zzbyc.zza(11, zza4);
                    }
                }
            }
            if (this.zzlA) {
                zzbyc.zzg(12, this.zzlA);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu() + zzbyc.zzL(1, this.type);
            if (this.string != null && !this.string.equals("")) {
                zzu += zzbyc.zzr(2, this.string);
            }
            if (this.zzlu != null && this.zzlu.length > 0) {
                int i = zzu;
                for (zza zza : this.zzlu) {
                    if (zza != null) {
                        i += zzbyc.zzc(3, zza);
                    }
                }
                zzu = i;
            }
            if (this.zzlv != null && this.zzlv.length > 0) {
                int i2 = zzu;
                for (zza zza2 : this.zzlv) {
                    if (zza2 != null) {
                        i2 += zzbyc.zzc(4, zza2);
                    }
                }
                zzu = i2;
            }
            if (this.zzlw != null && this.zzlw.length > 0) {
                int i3 = zzu;
                for (zza zza3 : this.zzlw) {
                    if (zza3 != null) {
                        i3 += zzbyc.zzc(5, zza3);
                    }
                }
                zzu = i3;
            }
            if (this.zzlx != null && !this.zzlx.equals("")) {
                zzu += zzbyc.zzr(6, this.zzlx);
            }
            if (this.zzly != null && !this.zzly.equals("")) {
                zzu += zzbyc.zzr(7, this.zzly);
            }
            if (this.zzlz != 0) {
                zzu += zzbyc.zzf(8, this.zzlz);
            }
            if (this.zzlD) {
                zzu += zzbyc.zzh(9, this.zzlD);
            }
            if (this.zzlC != null && this.zzlC.length > 0) {
                int i4 = 0;
                for (int zzrl : this.zzlC) {
                    i4 += zzbyc.zzrl(zzrl);
                }
                zzu = zzu + i4 + (this.zzlC.length * 1);
            }
            if (this.zzlB != null && this.zzlB.length > 0) {
                for (zza zza4 : this.zzlB) {
                    if (zza4 != null) {
                        zzu += zzbyc.zzc(11, zza4);
                    }
                }
            }
            return this.zzlA ? zzu + zzbyc.zzh(12, this.zzlA) : zzu;
        }

        /* renamed from: zzx */
        public zza zzb(zzbyb zzbyb) {
            int i;
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                this.type = zzafa;
                                continue;
                        }
                    case 18:
                        this.string = zzbyb.readString();
                        break;
                    case 26:
                        int zzb = zzbym.zzb(zzbyb, 26);
                        int length = this.zzlu == null ? 0 : this.zzlu.length;
                        zza[] zzaArr = new zza[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzlu, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new zza();
                            zzbyb.zza(zzaArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzaArr[length] = new zza();
                        zzbyb.zza(zzaArr[length]);
                        this.zzlu = zzaArr;
                        break;
                    case 34:
                        int zzb2 = zzbym.zzb(zzbyb, 34);
                        int length2 = this.zzlv == null ? 0 : this.zzlv.length;
                        zza[] zzaArr2 = new zza[(zzb2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzlv, 0, zzaArr2, 0, length2);
                        }
                        while (length2 < zzaArr2.length - 1) {
                            zzaArr2[length2] = new zza();
                            zzbyb.zza(zzaArr2[length2]);
                            zzbyb.zzaeW();
                            length2++;
                        }
                        zzaArr2[length2] = new zza();
                        zzbyb.zza(zzaArr2[length2]);
                        this.zzlv = zzaArr2;
                        break;
                    case 42:
                        int zzb3 = zzbym.zzb(zzbyb, 42);
                        int length3 = this.zzlw == null ? 0 : this.zzlw.length;
                        zza[] zzaArr3 = new zza[(zzb3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzlw, 0, zzaArr3, 0, length3);
                        }
                        while (length3 < zzaArr3.length - 1) {
                            zzaArr3[length3] = new zza();
                            zzbyb.zza(zzaArr3[length3]);
                            zzbyb.zzaeW();
                            length3++;
                        }
                        zzaArr3[length3] = new zza();
                        zzbyb.zza(zzaArr3[length3]);
                        this.zzlw = zzaArr3;
                        break;
                    case 50:
                        this.zzlx = zzbyb.readString();
                        break;
                    case 58:
                        this.zzly = zzbyb.readString();
                        break;
                    case 64:
                        this.zzlz = zzbyb.zzaeZ();
                        break;
                    case 72:
                        this.zzlD = zzbyb.zzafc();
                        break;
                    case 80:
                        int zzb4 = zzbym.zzb(zzbyb, 80);
                        int[] iArr = new int[zzb4];
                        int i2 = 0;
                        int i3 = 0;
                        while (i2 < zzb4) {
                            if (i2 != 0) {
                                zzbyb.zzaeW();
                            }
                            int zzafa2 = zzbyb.zzafa();
                            switch (zzafa2) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    i = i3 + 1;
                                    iArr[i3] = zzafa2;
                                    break;
                                default:
                                    i = i3;
                                    break;
                            }
                            i2++;
                            i3 = i;
                        }
                        if (i3 != 0) {
                            int length4 = this.zzlC == null ? 0 : this.zzlC.length;
                            if (length4 != 0 || i3 != iArr.length) {
                                int[] iArr2 = new int[(length4 + i3)];
                                if (length4 != 0) {
                                    System.arraycopy(this.zzlC, 0, iArr2, 0, length4);
                                }
                                System.arraycopy(iArr, 0, iArr2, length4, i3);
                                this.zzlC = iArr2;
                                break;
                            } else {
                                this.zzlC = iArr;
                                break;
                            }
                        } else {
                            break;
                        }
                    case 82:
                        int zzrf = zzbyb.zzrf(zzbyb.zzaff());
                        int position = zzbyb.getPosition();
                        int i4 = 0;
                        while (zzbyb.zzafk() > 0) {
                            switch (zzbyb.zzafa()) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case 17:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            zzbyb.zzrh(position);
                            int length5 = this.zzlC == null ? 0 : this.zzlC.length;
                            int[] iArr3 = new int[(i4 + length5)];
                            if (length5 != 0) {
                                System.arraycopy(this.zzlC, 0, iArr3, 0, length5);
                            }
                            while (zzbyb.zzafk() > 0) {
                                int zzafa3 = zzbyb.zzafa();
                                switch (zzafa3) {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                    case 14:
                                    case 15:
                                    case 16:
                                    case 17:
                                        iArr3[length5] = zzafa3;
                                        length5++;
                                        break;
                                }
                            }
                            this.zzlC = iArr3;
                        }
                        zzbyb.zzrg(zzrf);
                        break;
                    case 90:
                        int zzb5 = zzbym.zzb(zzbyb, 90);
                        int length6 = this.zzlB == null ? 0 : this.zzlB.length;
                        zza[] zzaArr4 = new zza[(zzb5 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.zzlB, 0, zzaArr4, 0, length6);
                        }
                        while (length6 < zzaArr4.length - 1) {
                            zzaArr4[length6] = new zza();
                            zzbyb.zza(zzaArr4[length6]);
                            zzbyb.zzaeW();
                            length6++;
                        }
                        zzaArr4[length6] = new zza();
                        zzbyb.zza(zzaArr4[length6]);
                        this.zzlB = zzaArr4;
                        break;
                    case 96:
                        this.zzlA = zzbyb.zzafc();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }
    }
}
