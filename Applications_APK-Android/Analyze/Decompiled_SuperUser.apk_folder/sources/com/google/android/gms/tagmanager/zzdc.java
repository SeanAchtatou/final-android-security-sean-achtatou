package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

class zzdc extends zzdb {
    /* access modifiers changed from: private */
    public static final Object zzbId = new Object();
    private static zzdc zzbIp;
    /* access modifiers changed from: private */
    public boolean connected = true;
    /* access modifiers changed from: private */
    public Context zzbIe;
    /* access modifiers changed from: private */
    public zzaw zzbIf;
    private volatile zzau zzbIg;
    /* access modifiers changed from: private */
    public int zzbIh = 1800000;
    private boolean zzbIi = true;
    private boolean zzbIj = false;
    private boolean zzbIk = true;
    private zzax zzbIl = new zzax() {
        public void zzaR(boolean z) {
            zzdc.this.zze(z, zzdc.this.connected);
        }
    };
    private zza zzbIm;
    private zzbt zzbIn;
    private boolean zzbIo = false;

    public interface zza {
        void cancel();

        void zzRF();

        void zzy(long j);
    }

    private class zzb implements zza {
        private Handler handler;

        private zzb() {
            this.handler = new Handler(zzdc.this.zzbIe.getMainLooper(), new Handler.Callback() {
                public boolean handleMessage(Message message) {
                    if (1 == message.what && zzdc.zzbId.equals(message.obj)) {
                        zzdc.this.dispatch();
                        if (!zzdc.this.isPowerSaveMode()) {
                            zzb.this.zzy((long) zzdc.this.zzbIh);
                        }
                    }
                    return true;
                }
            });
        }

        private Message obtainMessage() {
            return this.handler.obtainMessage(1, zzdc.zzbId);
        }

        public void cancel() {
            this.handler.removeMessages(1, zzdc.zzbId);
        }

        public void zzRF() {
            this.handler.removeMessages(1, zzdc.zzbId);
            this.handler.sendMessage(obtainMessage());
        }

        public void zzy(long j) {
            this.handler.removeMessages(1, zzdc.zzbId);
            this.handler.sendMessageDelayed(obtainMessage(), j);
        }
    }

    private zzdc() {
    }

    /* access modifiers changed from: private */
    public boolean isPowerSaveMode() {
        return this.zzbIo || !this.connected || this.zzbIh <= 0;
    }

    public static zzdc zzRA() {
        if (zzbIp == null) {
            zzbIp = new zzdc();
        }
        return zzbIp;
    }

    private void zzRB() {
        this.zzbIn = new zzbt(this);
        this.zzbIn.zzcb(this.zzbIe);
    }

    private void zzRC() {
        this.zzbIm = new zzb();
        if (this.zzbIh > 0) {
            this.zzbIm.zzy((long) this.zzbIh);
        }
    }

    private void zzoH() {
        if (isPowerSaveMode()) {
            this.zzbIm.cancel();
            zzbo.v("PowerSaveMode initiated.");
            return;
        }
        this.zzbIm.zzy((long) this.zzbIh);
        zzbo.v("PowerSaveMode terminated.");
    }

    public synchronized void dispatch() {
        if (!this.zzbIj) {
            zzbo.v("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.zzbIi = true;
        } else {
            this.zzbIg.zzp(new Runnable() {
                public void run() {
                    zzdc.this.zzbIf.dispatch();
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized zzaw zzRD() {
        if (this.zzbIf == null) {
            if (this.zzbIe == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.zzbIf = new zzcg(this.zzbIl, this.zzbIe);
        }
        if (this.zzbIm == null) {
            zzRC();
        }
        this.zzbIj = true;
        if (this.zzbIi) {
            dispatch();
            this.zzbIi = false;
        }
        if (this.zzbIn == null && this.zzbIk) {
            zzRB();
        }
        return this.zzbIf;
    }

    /* access modifiers changed from: package-private */
    public synchronized void zza(Context context, zzau zzau) {
        if (this.zzbIe == null) {
            this.zzbIe = context.getApplicationContext();
            if (this.zzbIg == null) {
                this.zzbIg = zzau;
            }
        }
    }

    public synchronized void zzaS(boolean z) {
        zze(this.zzbIo, z);
    }

    /* access modifiers changed from: package-private */
    public synchronized void zze(boolean z, boolean z2) {
        boolean isPowerSaveMode = isPowerSaveMode();
        this.zzbIo = z;
        this.connected = z2;
        if (isPowerSaveMode() != isPowerSaveMode) {
            zzoH();
        }
    }

    public synchronized void zznO() {
        if (!isPowerSaveMode()) {
            this.zzbIm.zzRF();
        }
    }
}
