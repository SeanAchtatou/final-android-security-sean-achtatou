package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzaq extends zzam {
    private static final String ID = zzah.GTM_VERSION.toString();

    public zzaq() {
        super(ID, new String[0]);
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        return zzdl.zzS("4.00");
    }
}
