package com.google.android.gms.internal;

import com.google.android.gms.internal.zzak;

public interface zzaj {

    public static final class zza extends zzbyd<zza> {
        public int level;
        public int zzkn;
        public int zzko;

        public zza() {
            zzw();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.level == zza.level && this.zzkn == zza.zzkn && this.zzko == zza.zzko) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zza.zzcwC == null || zza.zzcwC.isEmpty() : this.zzcwC.equals(zza.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((((((getClass().getName().hashCode() + 527) * 31) + this.level) * 31) + this.zzkn) * 31) + this.zzko) * 31);
        }

        public void zza(zzbyc zzbyc) {
            if (this.level != 1) {
                zzbyc.zzJ(1, this.level);
            }
            if (this.zzkn != 0) {
                zzbyc.zzJ(2, this.zzkn);
            }
            if (this.zzko != 0) {
                zzbyc.zzJ(3, this.zzko);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzn */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 1:
                            case 2:
                            case 3:
                                this.level = zzafa;
                                continue;
                        }
                    case 16:
                        this.zzkn = zzbyb.zzafa();
                        break;
                    case 24:
                        this.zzko = zzbyb.zzafa();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.level != 1) {
                zzu += zzbyc.zzL(1, this.level);
            }
            if (this.zzkn != 0) {
                zzu += zzbyc.zzL(2, this.zzkn);
            }
            return this.zzko != 0 ? zzu + zzbyc.zzL(3, this.zzko) : zzu;
        }

        public zza zzw() {
            this.level = 1;
            this.zzkn = 0;
            this.zzko = 0;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }
    }

    public static final class zzb extends zzbyd<zzb> {
        private static volatile zzb[] zzkp;
        public int name;
        public int[] zzkq;
        public int zzkr;
        public boolean zzks;
        public boolean zzkt;

        public zzb() {
            zzy();
        }

        public static zzb[] zzx() {
            if (zzkp == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzkp == null) {
                        zzkp = new zzb[0];
                    }
                }
            }
            return zzkp;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            if (zzbyh.equals(this.zzkq, zzb.zzkq) && this.zzkr == zzb.zzkr && this.name == zzb.name && this.zzks == zzb.zzks && this.zzkt == zzb.zzkt) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzb.zzcwC == null || zzb.zzcwC.isEmpty() : this.zzcwC.equals(zzb.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            int i = 1231;
            int hashCode = ((this.zzks ? 1231 : 1237) + ((((((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzkq)) * 31) + this.zzkr) * 31) + this.name) * 31)) * 31;
            if (!this.zzkt) {
                i = 1237;
            }
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((hashCode + i) * 31);
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzkt) {
                zzbyc.zzg(1, this.zzkt);
            }
            zzbyc.zzJ(2, this.zzkr);
            if (this.zzkq != null && this.zzkq.length > 0) {
                for (int zzJ : this.zzkq) {
                    zzbyc.zzJ(3, zzJ);
                }
            }
            if (this.name != 0) {
                zzbyc.zzJ(4, this.name);
            }
            if (this.zzks) {
                zzbyc.zzg(6, this.zzks);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzo */
        public zzb zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzkt = zzbyb.zzafc();
                        break;
                    case 16:
                        this.zzkr = zzbyb.zzafa();
                        break;
                    case 24:
                        int zzb = zzbym.zzb(zzbyb, 24);
                        int length = this.zzkq == null ? 0 : this.zzkq.length;
                        int[] iArr = new int[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzkq, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length++;
                        }
                        iArr[length] = zzbyb.zzafa();
                        this.zzkq = iArr;
                        break;
                    case 26:
                        int zzrf = zzbyb.zzrf(zzbyb.zzaff());
                        int position = zzbyb.getPosition();
                        int i = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i++;
                        }
                        zzbyb.zzrh(position);
                        int length2 = this.zzkq == null ? 0 : this.zzkq.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzkq, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = zzbyb.zzafa();
                            length2++;
                        }
                        this.zzkq = iArr2;
                        zzbyb.zzrg(zzrf);
                        break;
                    case 32:
                        this.name = zzbyb.zzafa();
                        break;
                    case 48:
                        this.zzks = zzbyb.zzafc();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int i;
            int i2 = 0;
            int zzu = super.zzu();
            if (this.zzkt) {
                zzu += zzbyc.zzh(1, this.zzkt);
            }
            int zzL = zzbyc.zzL(2, this.zzkr) + zzu;
            if (this.zzkq == null || this.zzkq.length <= 0) {
                i = zzL;
            } else {
                for (int zzrl : this.zzkq) {
                    i2 += zzbyc.zzrl(zzrl);
                }
                i = zzL + i2 + (this.zzkq.length * 1);
            }
            if (this.name != 0) {
                i += zzbyc.zzL(4, this.name);
            }
            return this.zzks ? i + zzbyc.zzh(6, this.zzks) : i;
        }

        public zzb zzy() {
            this.zzkq = zzbym.zzcwQ;
            this.zzkr = 0;
            this.name = 0;
            this.zzks = false;
            this.zzkt = false;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }
    }

    public static final class zzc extends zzbyd<zzc> {
        private static volatile zzc[] zzku;
        public String zzaB;
        public long zzkv;
        public long zzkw;
        public boolean zzkx;
        public long zzky;

        public zzc() {
            zzA();
        }

        public static zzc[] zzz() {
            if (zzku == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzku == null) {
                        zzku = new zzc[0];
                    }
                }
            }
            return zzku;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzc)) {
                return false;
            }
            zzc zzc = (zzc) obj;
            if (this.zzaB == null) {
                if (zzc.zzaB != null) {
                    return false;
                }
            } else if (!this.zzaB.equals(zzc.zzaB)) {
                return false;
            }
            if (this.zzkv == zzc.zzkv && this.zzkw == zzc.zzkw && this.zzkx == zzc.zzkx && this.zzky == zzc.zzky) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzc.zzcwC == null || zzc.zzcwC.isEmpty() : this.zzcwC.equals(zzc.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((this.zzkx ? 1231 : 1237) + (((((((this.zzaB == null ? 0 : this.zzaB.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + ((int) (this.zzkv ^ (this.zzkv >>> 32)))) * 31) + ((int) (this.zzkw ^ (this.zzkw >>> 32)))) * 31)) * 31) + ((int) (this.zzky ^ (this.zzky >>> 32)))) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzc zzA() {
            this.zzaB = "";
            this.zzkv = 0;
            this.zzkw = 2147483647L;
            this.zzkx = false;
            this.zzky = 0;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzaB != null && !this.zzaB.equals("")) {
                zzbyc.zzq(1, this.zzaB);
            }
            if (this.zzkv != 0) {
                zzbyc.zzb(2, this.zzkv);
            }
            if (this.zzkw != 2147483647L) {
                zzbyc.zzb(3, this.zzkw);
            }
            if (this.zzkx) {
                zzbyc.zzg(4, this.zzkx);
            }
            if (this.zzky != 0) {
                zzbyc.zzb(5, this.zzky);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzp */
        public zzc zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzaB = zzbyb.readString();
                        break;
                    case 16:
                        this.zzkv = zzbyb.zzaeZ();
                        break;
                    case 24:
                        this.zzkw = zzbyb.zzaeZ();
                        break;
                    case 32:
                        this.zzkx = zzbyb.zzafc();
                        break;
                    case 40:
                        this.zzky = zzbyb.zzaeZ();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzaB != null && !this.zzaB.equals("")) {
                zzu += zzbyc.zzr(1, this.zzaB);
            }
            if (this.zzkv != 0) {
                zzu += zzbyc.zzf(2, this.zzkv);
            }
            if (this.zzkw != 2147483647L) {
                zzu += zzbyc.zzf(3, this.zzkw);
            }
            if (this.zzkx) {
                zzu += zzbyc.zzh(4, this.zzkx);
            }
            return this.zzky != 0 ? zzu + zzbyc.zzf(5, this.zzky) : zzu;
        }
    }

    public static final class zzd extends zzbyd<zzd> {
        public zzak.zza[] zzkA;
        public zzc[] zzkB;
        public zzak.zza[] zzkz;

        public zzd() {
            zzB();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzd)) {
                return false;
            }
            zzd zzd = (zzd) obj;
            if (!zzbyh.equals(this.zzkz, zzd.zzkz) || !zzbyh.equals(this.zzkA, zzd.zzkA) || !zzbyh.equals(this.zzkB, zzd.zzkB)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzd.zzcwC == null || zzd.zzcwC.isEmpty() : this.zzcwC.equals(zzd.zzcwC);
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzkz)) * 31) + zzbyh.hashCode(this.zzkA)) * 31) + zzbyh.hashCode(this.zzkB)) * 31);
        }

        public zzd zzB() {
            this.zzkz = zzak.zza.zzL();
            this.zzkA = zzak.zza.zzL();
            this.zzkB = zzc.zzz();
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzkz != null && this.zzkz.length > 0) {
                for (zzak.zza zza : this.zzkz) {
                    if (zza != null) {
                        zzbyc.zza(1, zza);
                    }
                }
            }
            if (this.zzkA != null && this.zzkA.length > 0) {
                for (zzak.zza zza2 : this.zzkA) {
                    if (zza2 != null) {
                        zzbyc.zza(2, zza2);
                    }
                }
            }
            if (this.zzkB != null && this.zzkB.length > 0) {
                for (zzc zzc : this.zzkB) {
                    if (zzc != null) {
                        zzbyc.zza(3, zzc);
                    }
                }
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzq */
        public zzd zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        int zzb = zzbym.zzb(zzbyb, 10);
                        int length = this.zzkz == null ? 0 : this.zzkz.length;
                        zzak.zza[] zzaArr = new zzak.zza[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzkz, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new zzak.zza();
                            zzbyb.zza(zzaArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzaArr[length] = new zzak.zza();
                        zzbyb.zza(zzaArr[length]);
                        this.zzkz = zzaArr;
                        break;
                    case 18:
                        int zzb2 = zzbym.zzb(zzbyb, 18);
                        int length2 = this.zzkA == null ? 0 : this.zzkA.length;
                        zzak.zza[] zzaArr2 = new zzak.zza[(zzb2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzkA, 0, zzaArr2, 0, length2);
                        }
                        while (length2 < zzaArr2.length - 1) {
                            zzaArr2[length2] = new zzak.zza();
                            zzbyb.zza(zzaArr2[length2]);
                            zzbyb.zzaeW();
                            length2++;
                        }
                        zzaArr2[length2] = new zzak.zza();
                        zzbyb.zza(zzaArr2[length2]);
                        this.zzkA = zzaArr2;
                        break;
                    case 26:
                        int zzb3 = zzbym.zzb(zzbyb, 26);
                        int length3 = this.zzkB == null ? 0 : this.zzkB.length;
                        zzc[] zzcArr = new zzc[(zzb3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzkB, 0, zzcArr, 0, length3);
                        }
                        while (length3 < zzcArr.length - 1) {
                            zzcArr[length3] = new zzc();
                            zzbyb.zza(zzcArr[length3]);
                            zzbyb.zzaeW();
                            length3++;
                        }
                        zzcArr[length3] = new zzc();
                        zzbyb.zza(zzcArr[length3]);
                        this.zzkB = zzcArr;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzkz != null && this.zzkz.length > 0) {
                int i = zzu;
                for (zzak.zza zza : this.zzkz) {
                    if (zza != null) {
                        i += zzbyc.zzc(1, zza);
                    }
                }
                zzu = i;
            }
            if (this.zzkA != null && this.zzkA.length > 0) {
                int i2 = zzu;
                for (zzak.zza zza2 : this.zzkA) {
                    if (zza2 != null) {
                        i2 += zzbyc.zzc(2, zza2);
                    }
                }
                zzu = i2;
            }
            if (this.zzkB != null && this.zzkB.length > 0) {
                for (zzc zzc : this.zzkB) {
                    if (zzc != null) {
                        zzu += zzbyc.zzc(3, zzc);
                    }
                }
            }
            return zzu;
        }
    }

    public static final class zze extends zzbyd<zze> {
        private static volatile zze[] zzkC;
        public int key;
        public int value;

        public zze() {
            zzD();
        }

        public static zze[] zzC() {
            if (zzkC == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzkC == null) {
                        zzkC = new zze[0];
                    }
                }
            }
            return zzkC;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zze)) {
                return false;
            }
            zze zze = (zze) obj;
            if (this.key == zze.key && this.value == zze.value) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zze.zzcwC == null || zze.zzcwC.isEmpty() : this.zzcwC.equals(zze.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((((getClass().getName().hashCode() + 527) * 31) + this.key) * 31) + this.value) * 31);
        }

        public zze zzD() {
            this.key = 0;
            this.value = 0;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            zzbyc.zzJ(1, this.key);
            zzbyc.zzJ(2, this.value);
            super.zza(zzbyc);
        }

        /* renamed from: zzr */
        public zze zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.key = zzbyb.zzafa();
                        break;
                    case 16:
                        this.value = zzbyb.zzafa();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            return super.zzu() + zzbyc.zzL(1, this.key) + zzbyc.zzL(2, this.value);
        }
    }

    public static final class zzf extends zzbyd<zzf> {
        public String version;
        public String[] zzkD;
        public String[] zzkE;
        public zzak.zza[] zzkF;
        public zze[] zzkG;
        public zzb[] zzkH;
        public zzb[] zzkI;
        public zzb[] zzkJ;
        public zzg[] zzkK;
        public String zzkL;
        public String zzkM;
        public String zzkN;
        public zza zzkO;
        public float zzkP;
        public boolean zzkQ;
        public String[] zzkR;
        public int zzkS;

        public zzf() {
            zzE();
        }

        public static zzf zzf(byte[] bArr) {
            return (zzf) zzbyj.zza(new zzf(), bArr);
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzf)) {
                return false;
            }
            zzf zzf = (zzf) obj;
            if (!zzbyh.equals(this.zzkD, zzf.zzkD) || !zzbyh.equals(this.zzkE, zzf.zzkE) || !zzbyh.equals(this.zzkF, zzf.zzkF) || !zzbyh.equals(this.zzkG, zzf.zzkG) || !zzbyh.equals(this.zzkH, zzf.zzkH) || !zzbyh.equals(this.zzkI, zzf.zzkI) || !zzbyh.equals(this.zzkJ, zzf.zzkJ) || !zzbyh.equals(this.zzkK, zzf.zzkK)) {
                return false;
            }
            if (this.zzkL == null) {
                if (zzf.zzkL != null) {
                    return false;
                }
            } else if (!this.zzkL.equals(zzf.zzkL)) {
                return false;
            }
            if (this.zzkM == null) {
                if (zzf.zzkM != null) {
                    return false;
                }
            } else if (!this.zzkM.equals(zzf.zzkM)) {
                return false;
            }
            if (this.zzkN == null) {
                if (zzf.zzkN != null) {
                    return false;
                }
            } else if (!this.zzkN.equals(zzf.zzkN)) {
                return false;
            }
            if (this.version == null) {
                if (zzf.version != null) {
                    return false;
                }
            } else if (!this.version.equals(zzf.version)) {
                return false;
            }
            if (this.zzkO == null) {
                if (zzf.zzkO != null) {
                    return false;
                }
            } else if (!this.zzkO.equals(zzf.zzkO)) {
                return false;
            }
            if (Float.floatToIntBits(this.zzkP) == Float.floatToIntBits(zzf.zzkP) && this.zzkQ == zzf.zzkQ && zzbyh.equals(this.zzkR, zzf.zzkR) && this.zzkS == zzf.zzkS) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzf.zzcwC == null || zzf.zzcwC.isEmpty() : this.zzcwC.equals(zzf.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((((this.zzkQ ? 1231 : 1237) + (((((this.zzkO == null ? 0 : this.zzkO.hashCode()) + (((this.version == null ? 0 : this.version.hashCode()) + (((this.zzkN == null ? 0 : this.zzkN.hashCode()) + (((this.zzkM == null ? 0 : this.zzkM.hashCode()) + (((this.zzkL == null ? 0 : this.zzkL.hashCode()) + ((((((((((((((((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzkD)) * 31) + zzbyh.hashCode(this.zzkE)) * 31) + zzbyh.hashCode(this.zzkF)) * 31) + zzbyh.hashCode(this.zzkG)) * 31) + zzbyh.hashCode(this.zzkH)) * 31) + zzbyh.hashCode(this.zzkI)) * 31) + zzbyh.hashCode(this.zzkJ)) * 31) + zzbyh.hashCode(this.zzkK)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Float.floatToIntBits(this.zzkP)) * 31)) * 31) + zzbyh.hashCode(this.zzkR)) * 31) + this.zzkS) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzf zzE() {
            this.zzkD = zzbym.EMPTY_STRING_ARRAY;
            this.zzkE = zzbym.EMPTY_STRING_ARRAY;
            this.zzkF = zzak.zza.zzL();
            this.zzkG = zze.zzC();
            this.zzkH = zzb.zzx();
            this.zzkI = zzb.zzx();
            this.zzkJ = zzb.zzx();
            this.zzkK = zzg.zzF();
            this.zzkL = "";
            this.zzkM = "";
            this.zzkN = "0";
            this.version = "";
            this.zzkO = null;
            this.zzkP = 0.0f;
            this.zzkQ = false;
            this.zzkR = zzbym.EMPTY_STRING_ARRAY;
            this.zzkS = 0;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzkE != null && this.zzkE.length > 0) {
                for (String str : this.zzkE) {
                    if (str != null) {
                        zzbyc.zzq(1, str);
                    }
                }
            }
            if (this.zzkF != null && this.zzkF.length > 0) {
                for (zzak.zza zza : this.zzkF) {
                    if (zza != null) {
                        zzbyc.zza(2, zza);
                    }
                }
            }
            if (this.zzkG != null && this.zzkG.length > 0) {
                for (zze zze : this.zzkG) {
                    if (zze != null) {
                        zzbyc.zza(3, zze);
                    }
                }
            }
            if (this.zzkH != null && this.zzkH.length > 0) {
                for (zzb zzb : this.zzkH) {
                    if (zzb != null) {
                        zzbyc.zza(4, zzb);
                    }
                }
            }
            if (this.zzkI != null && this.zzkI.length > 0) {
                for (zzb zzb2 : this.zzkI) {
                    if (zzb2 != null) {
                        zzbyc.zza(5, zzb2);
                    }
                }
            }
            if (this.zzkJ != null && this.zzkJ.length > 0) {
                for (zzb zzb3 : this.zzkJ) {
                    if (zzb3 != null) {
                        zzbyc.zza(6, zzb3);
                    }
                }
            }
            if (this.zzkK != null && this.zzkK.length > 0) {
                for (zzg zzg : this.zzkK) {
                    if (zzg != null) {
                        zzbyc.zza(7, zzg);
                    }
                }
            }
            if (this.zzkL != null && !this.zzkL.equals("")) {
                zzbyc.zzq(9, this.zzkL);
            }
            if (this.zzkM != null && !this.zzkM.equals("")) {
                zzbyc.zzq(10, this.zzkM);
            }
            if (this.zzkN != null && !this.zzkN.equals("0")) {
                zzbyc.zzq(12, this.zzkN);
            }
            if (this.version != null && !this.version.equals("")) {
                zzbyc.zzq(13, this.version);
            }
            if (this.zzkO != null) {
                zzbyc.zza(14, this.zzkO);
            }
            if (Float.floatToIntBits(this.zzkP) != Float.floatToIntBits(0.0f)) {
                zzbyc.zzc(15, this.zzkP);
            }
            if (this.zzkR != null && this.zzkR.length > 0) {
                for (String str2 : this.zzkR) {
                    if (str2 != null) {
                        zzbyc.zzq(16, str2);
                    }
                }
            }
            if (this.zzkS != 0) {
                zzbyc.zzJ(17, this.zzkS);
            }
            if (this.zzkQ) {
                zzbyc.zzg(18, this.zzkQ);
            }
            if (this.zzkD != null && this.zzkD.length > 0) {
                for (String str3 : this.zzkD) {
                    if (str3 != null) {
                        zzbyc.zzq(19, str3);
                    }
                }
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzs */
        public zzf zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        int zzb = zzbym.zzb(zzbyb, 10);
                        int length = this.zzkE == null ? 0 : this.zzkE.length;
                        String[] strArr = new String[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzkE, 0, strArr, 0, length);
                        }
                        while (length < strArr.length - 1) {
                            strArr[length] = zzbyb.readString();
                            zzbyb.zzaeW();
                            length++;
                        }
                        strArr[length] = zzbyb.readString();
                        this.zzkE = strArr;
                        break;
                    case 18:
                        int zzb2 = zzbym.zzb(zzbyb, 18);
                        int length2 = this.zzkF == null ? 0 : this.zzkF.length;
                        zzak.zza[] zzaArr = new zzak.zza[(zzb2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzkF, 0, zzaArr, 0, length2);
                        }
                        while (length2 < zzaArr.length - 1) {
                            zzaArr[length2] = new zzak.zza();
                            zzbyb.zza(zzaArr[length2]);
                            zzbyb.zzaeW();
                            length2++;
                        }
                        zzaArr[length2] = new zzak.zza();
                        zzbyb.zza(zzaArr[length2]);
                        this.zzkF = zzaArr;
                        break;
                    case 26:
                        int zzb3 = zzbym.zzb(zzbyb, 26);
                        int length3 = this.zzkG == null ? 0 : this.zzkG.length;
                        zze[] zzeArr = new zze[(zzb3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzkG, 0, zzeArr, 0, length3);
                        }
                        while (length3 < zzeArr.length - 1) {
                            zzeArr[length3] = new zze();
                            zzbyb.zza(zzeArr[length3]);
                            zzbyb.zzaeW();
                            length3++;
                        }
                        zzeArr[length3] = new zze();
                        zzbyb.zza(zzeArr[length3]);
                        this.zzkG = zzeArr;
                        break;
                    case 34:
                        int zzb4 = zzbym.zzb(zzbyb, 34);
                        int length4 = this.zzkH == null ? 0 : this.zzkH.length;
                        zzb[] zzbArr = new zzb[(zzb4 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.zzkH, 0, zzbArr, 0, length4);
                        }
                        while (length4 < zzbArr.length - 1) {
                            zzbArr[length4] = new zzb();
                            zzbyb.zza(zzbArr[length4]);
                            zzbyb.zzaeW();
                            length4++;
                        }
                        zzbArr[length4] = new zzb();
                        zzbyb.zza(zzbArr[length4]);
                        this.zzkH = zzbArr;
                        break;
                    case 42:
                        int zzb5 = zzbym.zzb(zzbyb, 42);
                        int length5 = this.zzkI == null ? 0 : this.zzkI.length;
                        zzb[] zzbArr2 = new zzb[(zzb5 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.zzkI, 0, zzbArr2, 0, length5);
                        }
                        while (length5 < zzbArr2.length - 1) {
                            zzbArr2[length5] = new zzb();
                            zzbyb.zza(zzbArr2[length5]);
                            zzbyb.zzaeW();
                            length5++;
                        }
                        zzbArr2[length5] = new zzb();
                        zzbyb.zza(zzbArr2[length5]);
                        this.zzkI = zzbArr2;
                        break;
                    case 50:
                        int zzb6 = zzbym.zzb(zzbyb, 50);
                        int length6 = this.zzkJ == null ? 0 : this.zzkJ.length;
                        zzb[] zzbArr3 = new zzb[(zzb6 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.zzkJ, 0, zzbArr3, 0, length6);
                        }
                        while (length6 < zzbArr3.length - 1) {
                            zzbArr3[length6] = new zzb();
                            zzbyb.zza(zzbArr3[length6]);
                            zzbyb.zzaeW();
                            length6++;
                        }
                        zzbArr3[length6] = new zzb();
                        zzbyb.zza(zzbArr3[length6]);
                        this.zzkJ = zzbArr3;
                        break;
                    case 58:
                        int zzb7 = zzbym.zzb(zzbyb, 58);
                        int length7 = this.zzkK == null ? 0 : this.zzkK.length;
                        zzg[] zzgArr = new zzg[(zzb7 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.zzkK, 0, zzgArr, 0, length7);
                        }
                        while (length7 < zzgArr.length - 1) {
                            zzgArr[length7] = new zzg();
                            zzbyb.zza(zzgArr[length7]);
                            zzbyb.zzaeW();
                            length7++;
                        }
                        zzgArr[length7] = new zzg();
                        zzbyb.zza(zzgArr[length7]);
                        this.zzkK = zzgArr;
                        break;
                    case 74:
                        this.zzkL = zzbyb.readString();
                        break;
                    case 82:
                        this.zzkM = zzbyb.readString();
                        break;
                    case 98:
                        this.zzkN = zzbyb.readString();
                        break;
                    case 106:
                        this.version = zzbyb.readString();
                        break;
                    case 114:
                        if (this.zzkO == null) {
                            this.zzkO = new zza();
                        }
                        zzbyb.zza(this.zzkO);
                        break;
                    case 125:
                        this.zzkP = zzbyb.readFloat();
                        break;
                    case 130:
                        int zzb8 = zzbym.zzb(zzbyb, 130);
                        int length8 = this.zzkR == null ? 0 : this.zzkR.length;
                        String[] strArr2 = new String[(zzb8 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.zzkR, 0, strArr2, 0, length8);
                        }
                        while (length8 < strArr2.length - 1) {
                            strArr2[length8] = zzbyb.readString();
                            zzbyb.zzaeW();
                            length8++;
                        }
                        strArr2[length8] = zzbyb.readString();
                        this.zzkR = strArr2;
                        break;
                    case 136:
                        this.zzkS = zzbyb.zzafa();
                        break;
                    case 144:
                        this.zzkQ = zzbyb.zzafc();
                        break;
                    case 154:
                        int zzb9 = zzbym.zzb(zzbyb, 154);
                        int length9 = this.zzkD == null ? 0 : this.zzkD.length;
                        String[] strArr3 = new String[(zzb9 + length9)];
                        if (length9 != 0) {
                            System.arraycopy(this.zzkD, 0, strArr3, 0, length9);
                        }
                        while (length9 < strArr3.length - 1) {
                            strArr3[length9] = zzbyb.readString();
                            zzbyb.zzaeW();
                            length9++;
                        }
                        strArr3[length9] = zzbyb.readString();
                        this.zzkD = strArr3;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int i;
            int zzu = super.zzu();
            if (this.zzkE == null || this.zzkE.length <= 0) {
                i = zzu;
            } else {
                int i2 = 0;
                int i3 = 0;
                for (String str : this.zzkE) {
                    if (str != null) {
                        i3++;
                        i2 += zzbyc.zzku(str);
                    }
                }
                i = zzu + i2 + (i3 * 1);
            }
            if (this.zzkF != null && this.zzkF.length > 0) {
                int i4 = i;
                for (zzak.zza zza : this.zzkF) {
                    if (zza != null) {
                        i4 += zzbyc.zzc(2, zza);
                    }
                }
                i = i4;
            }
            if (this.zzkG != null && this.zzkG.length > 0) {
                int i5 = i;
                for (zze zze : this.zzkG) {
                    if (zze != null) {
                        i5 += zzbyc.zzc(3, zze);
                    }
                }
                i = i5;
            }
            if (this.zzkH != null && this.zzkH.length > 0) {
                int i6 = i;
                for (zzb zzb : this.zzkH) {
                    if (zzb != null) {
                        i6 += zzbyc.zzc(4, zzb);
                    }
                }
                i = i6;
            }
            if (this.zzkI != null && this.zzkI.length > 0) {
                int i7 = i;
                for (zzb zzb2 : this.zzkI) {
                    if (zzb2 != null) {
                        i7 += zzbyc.zzc(5, zzb2);
                    }
                }
                i = i7;
            }
            if (this.zzkJ != null && this.zzkJ.length > 0) {
                int i8 = i;
                for (zzb zzb3 : this.zzkJ) {
                    if (zzb3 != null) {
                        i8 += zzbyc.zzc(6, zzb3);
                    }
                }
                i = i8;
            }
            if (this.zzkK != null && this.zzkK.length > 0) {
                int i9 = i;
                for (zzg zzg : this.zzkK) {
                    if (zzg != null) {
                        i9 += zzbyc.zzc(7, zzg);
                    }
                }
                i = i9;
            }
            if (this.zzkL != null && !this.zzkL.equals("")) {
                i += zzbyc.zzr(9, this.zzkL);
            }
            if (this.zzkM != null && !this.zzkM.equals("")) {
                i += zzbyc.zzr(10, this.zzkM);
            }
            if (this.zzkN != null && !this.zzkN.equals("0")) {
                i += zzbyc.zzr(12, this.zzkN);
            }
            if (this.version != null && !this.version.equals("")) {
                i += zzbyc.zzr(13, this.version);
            }
            if (this.zzkO != null) {
                i += zzbyc.zzc(14, this.zzkO);
            }
            if (Float.floatToIntBits(this.zzkP) != Float.floatToIntBits(0.0f)) {
                i += zzbyc.zzd(15, this.zzkP);
            }
            if (this.zzkR != null && this.zzkR.length > 0) {
                int i10 = 0;
                int i11 = 0;
                for (String str2 : this.zzkR) {
                    if (str2 != null) {
                        i11++;
                        i10 += zzbyc.zzku(str2);
                    }
                }
                i = i + i10 + (i11 * 2);
            }
            if (this.zzkS != 0) {
                i += zzbyc.zzL(17, this.zzkS);
            }
            if (this.zzkQ) {
                i += zzbyc.zzh(18, this.zzkQ);
            }
            if (this.zzkD == null || this.zzkD.length <= 0) {
                return i;
            }
            int i12 = 0;
            int i13 = 0;
            for (String str3 : this.zzkD) {
                if (str3 != null) {
                    i13++;
                    i12 += zzbyc.zzku(str3);
                }
            }
            return i + i12 + (i13 * 2);
        }
    }

    public static final class zzg extends zzbyd<zzg> {
        private static volatile zzg[] zzkT;
        public int[] zzkU;
        public int[] zzkV;
        public int[] zzkW;
        public int[] zzkX;
        public int[] zzkY;
        public int[] zzkZ;
        public int[] zzla;
        public int[] zzlb;
        public int[] zzlc;
        public int[] zzld;

        public zzg() {
            zzG();
        }

        public static zzg[] zzF() {
            if (zzkT == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzkT == null) {
                        zzkT = new zzg[0];
                    }
                }
            }
            return zzkT;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzg)) {
                return false;
            }
            zzg zzg = (zzg) obj;
            if (!zzbyh.equals(this.zzkU, zzg.zzkU) || !zzbyh.equals(this.zzkV, zzg.zzkV) || !zzbyh.equals(this.zzkW, zzg.zzkW) || !zzbyh.equals(this.zzkX, zzg.zzkX) || !zzbyh.equals(this.zzkY, zzg.zzkY) || !zzbyh.equals(this.zzkZ, zzg.zzkZ) || !zzbyh.equals(this.zzla, zzg.zzla) || !zzbyh.equals(this.zzlb, zzg.zzlb) || !zzbyh.equals(this.zzlc, zzg.zzlc) || !zzbyh.equals(this.zzld, zzg.zzld)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzg.zzcwC == null || zzg.zzcwC.isEmpty() : this.zzcwC.equals(zzg.zzcwC);
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((((((((((((((((((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzkU)) * 31) + zzbyh.hashCode(this.zzkV)) * 31) + zzbyh.hashCode(this.zzkW)) * 31) + zzbyh.hashCode(this.zzkX)) * 31) + zzbyh.hashCode(this.zzkY)) * 31) + zzbyh.hashCode(this.zzkZ)) * 31) + zzbyh.hashCode(this.zzla)) * 31) + zzbyh.hashCode(this.zzlb)) * 31) + zzbyh.hashCode(this.zzlc)) * 31) + zzbyh.hashCode(this.zzld)) * 31);
        }

        public zzg zzG() {
            this.zzkU = zzbym.zzcwQ;
            this.zzkV = zzbym.zzcwQ;
            this.zzkW = zzbym.zzcwQ;
            this.zzkX = zzbym.zzcwQ;
            this.zzkY = zzbym.zzcwQ;
            this.zzkZ = zzbym.zzcwQ;
            this.zzla = zzbym.zzcwQ;
            this.zzlb = zzbym.zzcwQ;
            this.zzlc = zzbym.zzcwQ;
            this.zzld = zzbym.zzcwQ;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzkU != null && this.zzkU.length > 0) {
                for (int zzJ : this.zzkU) {
                    zzbyc.zzJ(1, zzJ);
                }
            }
            if (this.zzkV != null && this.zzkV.length > 0) {
                for (int zzJ2 : this.zzkV) {
                    zzbyc.zzJ(2, zzJ2);
                }
            }
            if (this.zzkW != null && this.zzkW.length > 0) {
                for (int zzJ3 : this.zzkW) {
                    zzbyc.zzJ(3, zzJ3);
                }
            }
            if (this.zzkX != null && this.zzkX.length > 0) {
                for (int zzJ4 : this.zzkX) {
                    zzbyc.zzJ(4, zzJ4);
                }
            }
            if (this.zzkY != null && this.zzkY.length > 0) {
                for (int zzJ5 : this.zzkY) {
                    zzbyc.zzJ(5, zzJ5);
                }
            }
            if (this.zzkZ != null && this.zzkZ.length > 0) {
                for (int zzJ6 : this.zzkZ) {
                    zzbyc.zzJ(6, zzJ6);
                }
            }
            if (this.zzla != null && this.zzla.length > 0) {
                for (int zzJ7 : this.zzla) {
                    zzbyc.zzJ(7, zzJ7);
                }
            }
            if (this.zzlb != null && this.zzlb.length > 0) {
                for (int zzJ8 : this.zzlb) {
                    zzbyc.zzJ(8, zzJ8);
                }
            }
            if (this.zzlc != null && this.zzlc.length > 0) {
                for (int zzJ9 : this.zzlc) {
                    zzbyc.zzJ(9, zzJ9);
                }
            }
            if (this.zzld != null && this.zzld.length > 0) {
                for (int zzJ10 : this.zzld) {
                    zzbyc.zzJ(10, zzJ10);
                }
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzt */
        public zzg zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzb = zzbym.zzb(zzbyb, 8);
                        int length = this.zzkU == null ? 0 : this.zzkU.length;
                        int[] iArr = new int[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzkU, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length++;
                        }
                        iArr[length] = zzbyb.zzafa();
                        this.zzkU = iArr;
                        break;
                    case 10:
                        int zzrf = zzbyb.zzrf(zzbyb.zzaff());
                        int position = zzbyb.getPosition();
                        int i = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i++;
                        }
                        zzbyb.zzrh(position);
                        int length2 = this.zzkU == null ? 0 : this.zzkU.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzkU, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = zzbyb.zzafa();
                            length2++;
                        }
                        this.zzkU = iArr2;
                        zzbyb.zzrg(zzrf);
                        break;
                    case 16:
                        int zzb2 = zzbym.zzb(zzbyb, 16);
                        int length3 = this.zzkV == null ? 0 : this.zzkV.length;
                        int[] iArr3 = new int[(zzb2 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzkV, 0, iArr3, 0, length3);
                        }
                        while (length3 < iArr3.length - 1) {
                            iArr3[length3] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length3++;
                        }
                        iArr3[length3] = zzbyb.zzafa();
                        this.zzkV = iArr3;
                        break;
                    case 18:
                        int zzrf2 = zzbyb.zzrf(zzbyb.zzaff());
                        int position2 = zzbyb.getPosition();
                        int i2 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i2++;
                        }
                        zzbyb.zzrh(position2);
                        int length4 = this.zzkV == null ? 0 : this.zzkV.length;
                        int[] iArr4 = new int[(i2 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.zzkV, 0, iArr4, 0, length4);
                        }
                        while (length4 < iArr4.length) {
                            iArr4[length4] = zzbyb.zzafa();
                            length4++;
                        }
                        this.zzkV = iArr4;
                        zzbyb.zzrg(zzrf2);
                        break;
                    case 24:
                        int zzb3 = zzbym.zzb(zzbyb, 24);
                        int length5 = this.zzkW == null ? 0 : this.zzkW.length;
                        int[] iArr5 = new int[(zzb3 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.zzkW, 0, iArr5, 0, length5);
                        }
                        while (length5 < iArr5.length - 1) {
                            iArr5[length5] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length5++;
                        }
                        iArr5[length5] = zzbyb.zzafa();
                        this.zzkW = iArr5;
                        break;
                    case 26:
                        int zzrf3 = zzbyb.zzrf(zzbyb.zzaff());
                        int position3 = zzbyb.getPosition();
                        int i3 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i3++;
                        }
                        zzbyb.zzrh(position3);
                        int length6 = this.zzkW == null ? 0 : this.zzkW.length;
                        int[] iArr6 = new int[(i3 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.zzkW, 0, iArr6, 0, length6);
                        }
                        while (length6 < iArr6.length) {
                            iArr6[length6] = zzbyb.zzafa();
                            length6++;
                        }
                        this.zzkW = iArr6;
                        zzbyb.zzrg(zzrf3);
                        break;
                    case 32:
                        int zzb4 = zzbym.zzb(zzbyb, 32);
                        int length7 = this.zzkX == null ? 0 : this.zzkX.length;
                        int[] iArr7 = new int[(zzb4 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.zzkX, 0, iArr7, 0, length7);
                        }
                        while (length7 < iArr7.length - 1) {
                            iArr7[length7] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length7++;
                        }
                        iArr7[length7] = zzbyb.zzafa();
                        this.zzkX = iArr7;
                        break;
                    case 34:
                        int zzrf4 = zzbyb.zzrf(zzbyb.zzaff());
                        int position4 = zzbyb.getPosition();
                        int i4 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i4++;
                        }
                        zzbyb.zzrh(position4);
                        int length8 = this.zzkX == null ? 0 : this.zzkX.length;
                        int[] iArr8 = new int[(i4 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.zzkX, 0, iArr8, 0, length8);
                        }
                        while (length8 < iArr8.length) {
                            iArr8[length8] = zzbyb.zzafa();
                            length8++;
                        }
                        this.zzkX = iArr8;
                        zzbyb.zzrg(zzrf4);
                        break;
                    case 40:
                        int zzb5 = zzbym.zzb(zzbyb, 40);
                        int length9 = this.zzkY == null ? 0 : this.zzkY.length;
                        int[] iArr9 = new int[(zzb5 + length9)];
                        if (length9 != 0) {
                            System.arraycopy(this.zzkY, 0, iArr9, 0, length9);
                        }
                        while (length9 < iArr9.length - 1) {
                            iArr9[length9] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length9++;
                        }
                        iArr9[length9] = zzbyb.zzafa();
                        this.zzkY = iArr9;
                        break;
                    case 42:
                        int zzrf5 = zzbyb.zzrf(zzbyb.zzaff());
                        int position5 = zzbyb.getPosition();
                        int i5 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i5++;
                        }
                        zzbyb.zzrh(position5);
                        int length10 = this.zzkY == null ? 0 : this.zzkY.length;
                        int[] iArr10 = new int[(i5 + length10)];
                        if (length10 != 0) {
                            System.arraycopy(this.zzkY, 0, iArr10, 0, length10);
                        }
                        while (length10 < iArr10.length) {
                            iArr10[length10] = zzbyb.zzafa();
                            length10++;
                        }
                        this.zzkY = iArr10;
                        zzbyb.zzrg(zzrf5);
                        break;
                    case 48:
                        int zzb6 = zzbym.zzb(zzbyb, 48);
                        int length11 = this.zzkZ == null ? 0 : this.zzkZ.length;
                        int[] iArr11 = new int[(zzb6 + length11)];
                        if (length11 != 0) {
                            System.arraycopy(this.zzkZ, 0, iArr11, 0, length11);
                        }
                        while (length11 < iArr11.length - 1) {
                            iArr11[length11] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length11++;
                        }
                        iArr11[length11] = zzbyb.zzafa();
                        this.zzkZ = iArr11;
                        break;
                    case 50:
                        int zzrf6 = zzbyb.zzrf(zzbyb.zzaff());
                        int position6 = zzbyb.getPosition();
                        int i6 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i6++;
                        }
                        zzbyb.zzrh(position6);
                        int length12 = this.zzkZ == null ? 0 : this.zzkZ.length;
                        int[] iArr12 = new int[(i6 + length12)];
                        if (length12 != 0) {
                            System.arraycopy(this.zzkZ, 0, iArr12, 0, length12);
                        }
                        while (length12 < iArr12.length) {
                            iArr12[length12] = zzbyb.zzafa();
                            length12++;
                        }
                        this.zzkZ = iArr12;
                        zzbyb.zzrg(zzrf6);
                        break;
                    case 56:
                        int zzb7 = zzbym.zzb(zzbyb, 56);
                        int length13 = this.zzla == null ? 0 : this.zzla.length;
                        int[] iArr13 = new int[(zzb7 + length13)];
                        if (length13 != 0) {
                            System.arraycopy(this.zzla, 0, iArr13, 0, length13);
                        }
                        while (length13 < iArr13.length - 1) {
                            iArr13[length13] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length13++;
                        }
                        iArr13[length13] = zzbyb.zzafa();
                        this.zzla = iArr13;
                        break;
                    case 58:
                        int zzrf7 = zzbyb.zzrf(zzbyb.zzaff());
                        int position7 = zzbyb.getPosition();
                        int i7 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i7++;
                        }
                        zzbyb.zzrh(position7);
                        int length14 = this.zzla == null ? 0 : this.zzla.length;
                        int[] iArr14 = new int[(i7 + length14)];
                        if (length14 != 0) {
                            System.arraycopy(this.zzla, 0, iArr14, 0, length14);
                        }
                        while (length14 < iArr14.length) {
                            iArr14[length14] = zzbyb.zzafa();
                            length14++;
                        }
                        this.zzla = iArr14;
                        zzbyb.zzrg(zzrf7);
                        break;
                    case 64:
                        int zzb8 = zzbym.zzb(zzbyb, 64);
                        int length15 = this.zzlb == null ? 0 : this.zzlb.length;
                        int[] iArr15 = new int[(zzb8 + length15)];
                        if (length15 != 0) {
                            System.arraycopy(this.zzlb, 0, iArr15, 0, length15);
                        }
                        while (length15 < iArr15.length - 1) {
                            iArr15[length15] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length15++;
                        }
                        iArr15[length15] = zzbyb.zzafa();
                        this.zzlb = iArr15;
                        break;
                    case 66:
                        int zzrf8 = zzbyb.zzrf(zzbyb.zzaff());
                        int position8 = zzbyb.getPosition();
                        int i8 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i8++;
                        }
                        zzbyb.zzrh(position8);
                        int length16 = this.zzlb == null ? 0 : this.zzlb.length;
                        int[] iArr16 = new int[(i8 + length16)];
                        if (length16 != 0) {
                            System.arraycopy(this.zzlb, 0, iArr16, 0, length16);
                        }
                        while (length16 < iArr16.length) {
                            iArr16[length16] = zzbyb.zzafa();
                            length16++;
                        }
                        this.zzlb = iArr16;
                        zzbyb.zzrg(zzrf8);
                        break;
                    case 72:
                        int zzb9 = zzbym.zzb(zzbyb, 72);
                        int length17 = this.zzlc == null ? 0 : this.zzlc.length;
                        int[] iArr17 = new int[(zzb9 + length17)];
                        if (length17 != 0) {
                            System.arraycopy(this.zzlc, 0, iArr17, 0, length17);
                        }
                        while (length17 < iArr17.length - 1) {
                            iArr17[length17] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length17++;
                        }
                        iArr17[length17] = zzbyb.zzafa();
                        this.zzlc = iArr17;
                        break;
                    case 74:
                        int zzrf9 = zzbyb.zzrf(zzbyb.zzaff());
                        int position9 = zzbyb.getPosition();
                        int i9 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i9++;
                        }
                        zzbyb.zzrh(position9);
                        int length18 = this.zzlc == null ? 0 : this.zzlc.length;
                        int[] iArr18 = new int[(i9 + length18)];
                        if (length18 != 0) {
                            System.arraycopy(this.zzlc, 0, iArr18, 0, length18);
                        }
                        while (length18 < iArr18.length) {
                            iArr18[length18] = zzbyb.zzafa();
                            length18++;
                        }
                        this.zzlc = iArr18;
                        zzbyb.zzrg(zzrf9);
                        break;
                    case 80:
                        int zzb10 = zzbym.zzb(zzbyb, 80);
                        int length19 = this.zzld == null ? 0 : this.zzld.length;
                        int[] iArr19 = new int[(zzb10 + length19)];
                        if (length19 != 0) {
                            System.arraycopy(this.zzld, 0, iArr19, 0, length19);
                        }
                        while (length19 < iArr19.length - 1) {
                            iArr19[length19] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length19++;
                        }
                        iArr19[length19] = zzbyb.zzafa();
                        this.zzld = iArr19;
                        break;
                    case 82:
                        int zzrf10 = zzbyb.zzrf(zzbyb.zzaff());
                        int position10 = zzbyb.getPosition();
                        int i10 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i10++;
                        }
                        zzbyb.zzrh(position10);
                        int length20 = this.zzld == null ? 0 : this.zzld.length;
                        int[] iArr20 = new int[(i10 + length20)];
                        if (length20 != 0) {
                            System.arraycopy(this.zzld, 0, iArr20, 0, length20);
                        }
                        while (length20 < iArr20.length) {
                            iArr20[length20] = zzbyb.zzafa();
                            length20++;
                        }
                        this.zzld = iArr20;
                        zzbyb.zzrg(zzrf10);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int i;
            int zzu = super.zzu();
            if (this.zzkU == null || this.zzkU.length <= 0) {
                i = zzu;
            } else {
                int i2 = 0;
                for (int zzrl : this.zzkU) {
                    i2 += zzbyc.zzrl(zzrl);
                }
                i = zzu + i2 + (this.zzkU.length * 1);
            }
            if (this.zzkV != null && this.zzkV.length > 0) {
                int i3 = 0;
                for (int zzrl2 : this.zzkV) {
                    i3 += zzbyc.zzrl(zzrl2);
                }
                i = i + i3 + (this.zzkV.length * 1);
            }
            if (this.zzkW != null && this.zzkW.length > 0) {
                int i4 = 0;
                for (int zzrl3 : this.zzkW) {
                    i4 += zzbyc.zzrl(zzrl3);
                }
                i = i + i4 + (this.zzkW.length * 1);
            }
            if (this.zzkX != null && this.zzkX.length > 0) {
                int i5 = 0;
                for (int zzrl4 : this.zzkX) {
                    i5 += zzbyc.zzrl(zzrl4);
                }
                i = i + i5 + (this.zzkX.length * 1);
            }
            if (this.zzkY != null && this.zzkY.length > 0) {
                int i6 = 0;
                for (int zzrl5 : this.zzkY) {
                    i6 += zzbyc.zzrl(zzrl5);
                }
                i = i + i6 + (this.zzkY.length * 1);
            }
            if (this.zzkZ != null && this.zzkZ.length > 0) {
                int i7 = 0;
                for (int zzrl6 : this.zzkZ) {
                    i7 += zzbyc.zzrl(zzrl6);
                }
                i = i + i7 + (this.zzkZ.length * 1);
            }
            if (this.zzla != null && this.zzla.length > 0) {
                int i8 = 0;
                for (int zzrl7 : this.zzla) {
                    i8 += zzbyc.zzrl(zzrl7);
                }
                i = i + i8 + (this.zzla.length * 1);
            }
            if (this.zzlb != null && this.zzlb.length > 0) {
                int i9 = 0;
                for (int zzrl8 : this.zzlb) {
                    i9 += zzbyc.zzrl(zzrl8);
                }
                i = i + i9 + (this.zzlb.length * 1);
            }
            if (this.zzlc != null && this.zzlc.length > 0) {
                int i10 = 0;
                for (int zzrl9 : this.zzlc) {
                    i10 += zzbyc.zzrl(zzrl9);
                }
                i = i + i10 + (this.zzlc.length * 1);
            }
            if (this.zzld == null || this.zzld.length <= 0) {
                return i;
            }
            int i11 = 0;
            for (int zzrl10 : this.zzld) {
                i11 += zzbyc.zzrl(zzrl10);
            }
            return i + i11 + (this.zzld.length * 1);
        }
    }

    public static final class zzh extends zzbyd<zzh> {
        public static final zzbye<zzak.zza, zzh> zzle = zzbye.zza(11, zzh.class, 810);
        private static final zzh[] zzlf = new zzh[0];
        public int[] zzlg;
        public int[] zzlh;
        public int[] zzli;
        public int zzlj;
        public int[] zzlk;
        public int zzll;
        public int zzlm;

        public zzh() {
            zzH();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzh)) {
                return false;
            }
            zzh zzh = (zzh) obj;
            if (!zzbyh.equals(this.zzlg, zzh.zzlg) || !zzbyh.equals(this.zzlh, zzh.zzlh) || !zzbyh.equals(this.zzli, zzh.zzli) || this.zzlj != zzh.zzlj || !zzbyh.equals(this.zzlk, zzh.zzlk) || this.zzll != zzh.zzll || this.zzlm != zzh.zzlm) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzh.zzcwC == null || zzh.zzcwC.isEmpty() : this.zzcwC.equals(zzh.zzcwC);
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((((((((((((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzlg)) * 31) + zzbyh.hashCode(this.zzlh)) * 31) + zzbyh.hashCode(this.zzli)) * 31) + this.zzlj) * 31) + zzbyh.hashCode(this.zzlk)) * 31) + this.zzll) * 31) + this.zzlm) * 31);
        }

        public zzh zzH() {
            this.zzlg = zzbym.zzcwQ;
            this.zzlh = zzbym.zzcwQ;
            this.zzli = zzbym.zzcwQ;
            this.zzlj = 0;
            this.zzlk = zzbym.zzcwQ;
            this.zzll = 0;
            this.zzlm = 0;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzlg != null && this.zzlg.length > 0) {
                for (int zzJ : this.zzlg) {
                    zzbyc.zzJ(1, zzJ);
                }
            }
            if (this.zzlh != null && this.zzlh.length > 0) {
                for (int zzJ2 : this.zzlh) {
                    zzbyc.zzJ(2, zzJ2);
                }
            }
            if (this.zzli != null && this.zzli.length > 0) {
                for (int zzJ3 : this.zzli) {
                    zzbyc.zzJ(3, zzJ3);
                }
            }
            if (this.zzlj != 0) {
                zzbyc.zzJ(4, this.zzlj);
            }
            if (this.zzlk != null && this.zzlk.length > 0) {
                for (int zzJ4 : this.zzlk) {
                    zzbyc.zzJ(5, zzJ4);
                }
            }
            if (this.zzll != 0) {
                zzbyc.zzJ(6, this.zzll);
            }
            if (this.zzlm != 0) {
                zzbyc.zzJ(7, this.zzlm);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int i;
            int zzu = super.zzu();
            if (this.zzlg == null || this.zzlg.length <= 0) {
                i = zzu;
            } else {
                int i2 = 0;
                for (int zzrl : this.zzlg) {
                    i2 += zzbyc.zzrl(zzrl);
                }
                i = zzu + i2 + (this.zzlg.length * 1);
            }
            if (this.zzlh != null && this.zzlh.length > 0) {
                int i3 = 0;
                for (int zzrl2 : this.zzlh) {
                    i3 += zzbyc.zzrl(zzrl2);
                }
                i = i + i3 + (this.zzlh.length * 1);
            }
            if (this.zzli != null && this.zzli.length > 0) {
                int i4 = 0;
                for (int zzrl3 : this.zzli) {
                    i4 += zzbyc.zzrl(zzrl3);
                }
                i = i + i4 + (this.zzli.length * 1);
            }
            if (this.zzlj != 0) {
                i += zzbyc.zzL(4, this.zzlj);
            }
            if (this.zzlk != null && this.zzlk.length > 0) {
                int i5 = 0;
                for (int zzrl4 : this.zzlk) {
                    i5 += zzbyc.zzrl(zzrl4);
                }
                i = i + i5 + (this.zzlk.length * 1);
            }
            if (this.zzll != 0) {
                i += zzbyc.zzL(6, this.zzll);
            }
            return this.zzlm != 0 ? i + zzbyc.zzL(7, this.zzlm) : i;
        }

        /* renamed from: zzu */
        public zzh zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzb = zzbym.zzb(zzbyb, 8);
                        int length = this.zzlg == null ? 0 : this.zzlg.length;
                        int[] iArr = new int[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzlg, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length++;
                        }
                        iArr[length] = zzbyb.zzafa();
                        this.zzlg = iArr;
                        break;
                    case 10:
                        int zzrf = zzbyb.zzrf(zzbyb.zzaff());
                        int position = zzbyb.getPosition();
                        int i = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i++;
                        }
                        zzbyb.zzrh(position);
                        int length2 = this.zzlg == null ? 0 : this.zzlg.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzlg, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = zzbyb.zzafa();
                            length2++;
                        }
                        this.zzlg = iArr2;
                        zzbyb.zzrg(zzrf);
                        break;
                    case 16:
                        int zzb2 = zzbym.zzb(zzbyb, 16);
                        int length3 = this.zzlh == null ? 0 : this.zzlh.length;
                        int[] iArr3 = new int[(zzb2 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzlh, 0, iArr3, 0, length3);
                        }
                        while (length3 < iArr3.length - 1) {
                            iArr3[length3] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length3++;
                        }
                        iArr3[length3] = zzbyb.zzafa();
                        this.zzlh = iArr3;
                        break;
                    case 18:
                        int zzrf2 = zzbyb.zzrf(zzbyb.zzaff());
                        int position2 = zzbyb.getPosition();
                        int i2 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i2++;
                        }
                        zzbyb.zzrh(position2);
                        int length4 = this.zzlh == null ? 0 : this.zzlh.length;
                        int[] iArr4 = new int[(i2 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.zzlh, 0, iArr4, 0, length4);
                        }
                        while (length4 < iArr4.length) {
                            iArr4[length4] = zzbyb.zzafa();
                            length4++;
                        }
                        this.zzlh = iArr4;
                        zzbyb.zzrg(zzrf2);
                        break;
                    case 24:
                        int zzb3 = zzbym.zzb(zzbyb, 24);
                        int length5 = this.zzli == null ? 0 : this.zzli.length;
                        int[] iArr5 = new int[(zzb3 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.zzli, 0, iArr5, 0, length5);
                        }
                        while (length5 < iArr5.length - 1) {
                            iArr5[length5] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length5++;
                        }
                        iArr5[length5] = zzbyb.zzafa();
                        this.zzli = iArr5;
                        break;
                    case 26:
                        int zzrf3 = zzbyb.zzrf(zzbyb.zzaff());
                        int position3 = zzbyb.getPosition();
                        int i3 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i3++;
                        }
                        zzbyb.zzrh(position3);
                        int length6 = this.zzli == null ? 0 : this.zzli.length;
                        int[] iArr6 = new int[(i3 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.zzli, 0, iArr6, 0, length6);
                        }
                        while (length6 < iArr6.length) {
                            iArr6[length6] = zzbyb.zzafa();
                            length6++;
                        }
                        this.zzli = iArr6;
                        zzbyb.zzrg(zzrf3);
                        break;
                    case 32:
                        this.zzlj = zzbyb.zzafa();
                        break;
                    case 40:
                        int zzb4 = zzbym.zzb(zzbyb, 40);
                        int length7 = this.zzlk == null ? 0 : this.zzlk.length;
                        int[] iArr7 = new int[(zzb4 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.zzlk, 0, iArr7, 0, length7);
                        }
                        while (length7 < iArr7.length - 1) {
                            iArr7[length7] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length7++;
                        }
                        iArr7[length7] = zzbyb.zzafa();
                        this.zzlk = iArr7;
                        break;
                    case 42:
                        int zzrf4 = zzbyb.zzrf(zzbyb.zzaff());
                        int position4 = zzbyb.getPosition();
                        int i4 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i4++;
                        }
                        zzbyb.zzrh(position4);
                        int length8 = this.zzlk == null ? 0 : this.zzlk.length;
                        int[] iArr8 = new int[(i4 + length8)];
                        if (length8 != 0) {
                            System.arraycopy(this.zzlk, 0, iArr8, 0, length8);
                        }
                        while (length8 < iArr8.length) {
                            iArr8[length8] = zzbyb.zzafa();
                            length8++;
                        }
                        this.zzlk = iArr8;
                        zzbyb.zzrg(zzrf4);
                        break;
                    case 48:
                        this.zzll = zzbyb.zzafa();
                        break;
                    case 56:
                        this.zzlm = zzbyb.zzafa();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }
    }

    public static final class zzi extends zzbyd<zzi> {
        private static volatile zzi[] zzln;
        public String name;
        public zzak.zza zzlo;
        public zzd zzlp;

        public zzi() {
            zzJ();
        }

        public static zzi[] zzI() {
            if (zzln == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzln == null) {
                        zzln = new zzi[0];
                    }
                }
            }
            return zzln;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzi)) {
                return false;
            }
            zzi zzi = (zzi) obj;
            if (this.name == null) {
                if (zzi.name != null) {
                    return false;
                }
            } else if (!this.name.equals(zzi.name)) {
                return false;
            }
            if (this.zzlo == null) {
                if (zzi.zzlo != null) {
                    return false;
                }
            } else if (!this.zzlo.equals(zzi.zzlo)) {
                return false;
            }
            if (this.zzlp == null) {
                if (zzi.zzlp != null) {
                    return false;
                }
            } else if (!this.zzlp.equals(zzi.zzlp)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzi.zzcwC == null || zzi.zzcwC.isEmpty() : this.zzcwC.equals(zzi.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzlp == null ? 0 : this.zzlp.hashCode()) + (((this.zzlo == null ? 0 : this.zzlo.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzi zzJ() {
            this.name = "";
            this.zzlo = null;
            this.zzlp = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.name != null && !this.name.equals("")) {
                zzbyc.zzq(1, this.name);
            }
            if (this.zzlo != null) {
                zzbyc.zza(2, this.zzlo);
            }
            if (this.zzlp != null) {
                zzbyc.zza(3, this.zzlp);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.name != null && !this.name.equals("")) {
                zzu += zzbyc.zzr(1, this.name);
            }
            if (this.zzlo != null) {
                zzu += zzbyc.zzc(2, this.zzlo);
            }
            return this.zzlp != null ? zzu + zzbyc.zzc(3, this.zzlp) : zzu;
        }

        /* renamed from: zzv */
        public zzi zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.name = zzbyb.readString();
                        break;
                    case 18:
                        if (this.zzlo == null) {
                            this.zzlo = new zzak.zza();
                        }
                        zzbyb.zza(this.zzlo);
                        break;
                    case 26:
                        if (this.zzlp == null) {
                            this.zzlp = new zzd();
                        }
                        zzbyb.zza(this.zzlp);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }
    }

    public static final class zzj extends zzbyd<zzj> {
        public zzi[] zzlq;
        public zzf zzlr;
        public String zzls;

        public zzj() {
            zzK();
        }

        public static zzj zzg(byte[] bArr) {
            return (zzj) zzbyj.zza(new zzj(), bArr);
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzj)) {
                return false;
            }
            zzj zzj = (zzj) obj;
            if (!zzbyh.equals(this.zzlq, zzj.zzlq)) {
                return false;
            }
            if (this.zzlr == null) {
                if (zzj.zzlr != null) {
                    return false;
                }
            } else if (!this.zzlr.equals(zzj.zzlr)) {
                return false;
            }
            if (this.zzls == null) {
                if (zzj.zzls != null) {
                    return false;
                }
            } else if (!this.zzls.equals(zzj.zzls)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzj.zzcwC == null || zzj.zzcwC.isEmpty() : this.zzcwC.equals(zzj.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzls == null ? 0 : this.zzls.hashCode()) + (((this.zzlr == null ? 0 : this.zzlr.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzlq)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzj zzK() {
            this.zzlq = zzi.zzI();
            this.zzlr = null;
            this.zzls = "";
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzlq != null && this.zzlq.length > 0) {
                for (zzi zzi : this.zzlq) {
                    if (zzi != null) {
                        zzbyc.zza(1, zzi);
                    }
                }
            }
            if (this.zzlr != null) {
                zzbyc.zza(2, this.zzlr);
            }
            if (this.zzls != null && !this.zzls.equals("")) {
                zzbyc.zzq(3, this.zzls);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzlq != null && this.zzlq.length > 0) {
                for (zzi zzi : this.zzlq) {
                    if (zzi != null) {
                        zzu += zzbyc.zzc(1, zzi);
                    }
                }
            }
            if (this.zzlr != null) {
                zzu += zzbyc.zzc(2, this.zzlr);
            }
            return (this.zzls == null || this.zzls.equals("")) ? zzu : zzu + zzbyc.zzr(3, this.zzls);
        }

        /* renamed from: zzw */
        public zzj zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        int zzb = zzbym.zzb(zzbyb, 10);
                        int length = this.zzlq == null ? 0 : this.zzlq.length;
                        zzi[] zziArr = new zzi[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzlq, 0, zziArr, 0, length);
                        }
                        while (length < zziArr.length - 1) {
                            zziArr[length] = new zzi();
                            zzbyb.zza(zziArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zziArr[length] = new zzi();
                        zzbyb.zza(zziArr[length]);
                        this.zzlq = zziArr;
                        break;
                    case 18:
                        if (this.zzlr == null) {
                            this.zzlr = new zzf();
                        }
                        zzbyb.zza(this.zzlr);
                        break;
                    case 26:
                        this.zzls = zzbyb.readString();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }
    }
}
