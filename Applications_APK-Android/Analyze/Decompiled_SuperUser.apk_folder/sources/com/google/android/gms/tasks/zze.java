package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

class zze<TResult> implements zzf<TResult> {
    private final Executor zzbFP;
    /* access modifiers changed from: private */
    public OnSuccessListener<? super TResult> zzbNB;
    /* access modifiers changed from: private */
    public final Object zzrJ = new Object();

    public zze(Executor executor, OnSuccessListener<? super TResult> onSuccessListener) {
        this.zzbFP = executor;
        this.zzbNB = onSuccessListener;
    }

    public void cancel() {
        synchronized (this.zzrJ) {
            this.zzbNB = null;
        }
    }

    public void onComplete(final Task<TResult> task) {
        if (task.isSuccessful()) {
            synchronized (this.zzrJ) {
                if (this.zzbNB != null) {
                    this.zzbFP.execute(new Runnable() {
                        public void run() {
                            synchronized (zze.this.zzrJ) {
                                if (zze.this.zzbNB != null) {
                                    zze.this.zzbNB.onSuccess(task.getResult());
                                }
                            }
                        }
                    });
                }
            }
        }
    }
}
