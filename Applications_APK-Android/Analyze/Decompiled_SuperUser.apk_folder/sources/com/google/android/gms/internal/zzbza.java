package com.google.android.gms.internal;

import java.io.Closeable;
import java.io.Flushable;

public interface zzbza extends Closeable, Flushable {
    void close();

    void flush();

    void write(zzbyr zzbyr, long j);
}
