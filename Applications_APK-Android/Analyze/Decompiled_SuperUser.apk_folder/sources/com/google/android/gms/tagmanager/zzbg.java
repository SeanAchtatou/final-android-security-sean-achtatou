package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class zzbg extends zzam {
    private static final String ID = zzah.JOINER.toString();
    private static final String zzbGA = zzai.ITEM_SEPARATOR.toString();
    private static final String zzbGB = zzai.KEY_VALUE_SEPARATOR.toString();
    private static final String zzbGC = zzai.ESCAPE.toString();
    private static final String zzbGi = zzai.ARG0.toString();

    private enum zza {
        NONE,
        URL,
        BACKSLASH
    }

    public zzbg() {
        super(ID, zzbGi);
    }

    private String zza(String str, zza zza2, Set<Character> set) {
        switch (zza2) {
            case URL:
                try {
                    return zzdp.zzhD(str);
                } catch (UnsupportedEncodingException e2) {
                    zzbo.zzb("Joiner: unsupported encoding", e2);
                    return str;
                }
            case BACKSLASH:
                String replace = str.replace("\\", "\\\\");
                Iterator<Character> it = set.iterator();
                while (true) {
                    String str2 = replace;
                    if (!it.hasNext()) {
                        return str2;
                    }
                    String ch = it.next().toString();
                    String valueOf = String.valueOf(ch);
                    replace = str2.replace(ch, valueOf.length() != 0 ? "\\".concat(valueOf) : new String("\\"));
                }
            default:
                return str;
        }
    }

    private void zza(StringBuilder sb, String str, zza zza2, Set<Character> set) {
        sb.append(zza(str, zza2, set));
    }

    private void zza(Set<Character> set, String str) {
        for (int i = 0; i < str.length(); i++) {
            set.add(Character.valueOf(str.charAt(i)));
        }
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        HashSet hashSet;
        zza zza2;
        zzak.zza zza3 = map.get(zzbGi);
        if (zza3 == null) {
            return zzdl.zzRT();
        }
        zzak.zza zza4 = map.get(zzbGA);
        String zze = zza4 != null ? zzdl.zze(zza4) : "";
        zzak.zza zza5 = map.get(zzbGB);
        String zze2 = zza5 != null ? zzdl.zze(zza5) : "=";
        zza zza6 = zza.NONE;
        zzak.zza zza7 = map.get(zzbGC);
        if (zza7 != null) {
            String zze3 = zzdl.zze(zza7);
            if ("url".equals(zze3)) {
                zza2 = zza.URL;
                hashSet = null;
            } else if ("backslash".equals(zze3)) {
                zza2 = zza.BACKSLASH;
                hashSet = new HashSet();
                zza(hashSet, zze);
                zza(hashSet, zze2);
                hashSet.remove('\\');
            } else {
                String valueOf = String.valueOf(zze3);
                zzbo.e(valueOf.length() != 0 ? "Joiner: unsupported escape type: ".concat(valueOf) : new String("Joiner: unsupported escape type: "));
                return zzdl.zzRT();
            }
        } else {
            hashSet = null;
            zza2 = zza6;
        }
        StringBuilder sb = new StringBuilder();
        switch (zza3.type) {
            case 2:
                boolean z = true;
                zzak.zza[] zzaArr = zza3.zzlu;
                int length = zzaArr.length;
                int i = 0;
                while (i < length) {
                    zzak.zza zza8 = zzaArr[i];
                    if (!z) {
                        sb.append(zze);
                    }
                    zza(sb, zzdl.zze(zza8), zza2, hashSet);
                    i++;
                    z = false;
                }
                break;
            case 3:
                for (int i2 = 0; i2 < zza3.zzlv.length; i2++) {
                    if (i2 > 0) {
                        sb.append(zze);
                    }
                    String zze4 = zzdl.zze(zza3.zzlv[i2]);
                    String zze5 = zzdl.zze(zza3.zzlw[i2]);
                    zza(sb, zze4, zza2, hashSet);
                    sb.append(zze2);
                    zza(sb, zze5, zza2, hashSet);
                }
                break;
            default:
                zza(sb, zzdl.zze(zza3), zza2, hashSet);
                break;
        }
        return zzdl.zzS(sb.toString());
    }
}
