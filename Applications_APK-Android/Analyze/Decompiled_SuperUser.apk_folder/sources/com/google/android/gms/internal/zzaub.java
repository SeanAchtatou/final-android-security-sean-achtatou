package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.measurement.AppMeasurement;

public final class zzaub {
    private final zza zzbtA;

    public interface zza {
        void doStartService(Context context, Intent intent);
    }

    public zzaub(zza zza2) {
        zzac.zzw(zza2);
        this.zzbtA = zza2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.zzaut.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauu$zzc):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zze):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zza[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzb[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzc[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzauw$zzg[]):void
      com.google.android.gms.internal.zzaut.zza(java.lang.String, java.lang.Object, boolean):int
      com.google.android.gms.internal.zzaut.zza(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.internal.zzaut.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.zzaut.zza(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean zzi(Context context, boolean z) {
        zzac.zzw(context);
        return zzaut.zza(context, z ? "com.google.android.gms.measurement.PackageMeasurementReceiver" : "com.google.android.gms.measurement.AppMeasurementReceiver", false);
    }

    public void onReceive(Context context, Intent intent) {
        final zzaue zzbM = zzaue.zzbM(context);
        final zzatx zzKl = zzbM.zzKl();
        if (intent == null) {
            zzKl.zzMb().log("Receiver called with null intent");
            return;
        }
        zzbM.zzKn().zzLh();
        String action = intent.getAction();
        zzKl.zzMf().zzj("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            zzaum.zzj(context, false);
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            this.zzbtA.doStartService(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                zzKl.zzMf().log("Install referrer extras are null");
                return;
            }
            zzKl.zzMd().zzj("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            final Bundle zzu = zzbM.zzKh().zzu(Uri.parse(stringExtra));
            if (zzu == null) {
                zzKl.zzMf().log("No campaign defined in install referrer broadcast");
                return;
            }
            final long longExtra = 1000 * intent.getLongExtra("referrer_timestamp_seconds", 0);
            if (longExtra == 0) {
                zzKl.zzMb().log("Install referrer is missing timestamp");
            }
            final Context context2 = context;
            zzbM.zzKk().zzm(new Runnable(this) {
                public void run() {
                    zzaus zzS = zzbM.zzKg().zzS(zzbM.zzKb().zzke(), "_fot");
                    long longValue = (zzS == null || !(zzS.mValue instanceof Long)) ? 0 : ((Long) zzS.mValue).longValue();
                    long j = longExtra;
                    long j2 = (longValue <= 0 || (j < longValue && j > 0)) ? j : longValue - 1;
                    if (j2 > 0) {
                        zzu.putLong("click_timestamp", j2);
                    }
                    AppMeasurement.getInstance(context2).logEventInternal("auto", "_cmp", zzu);
                    zzKl.zzMf().log("Install campaign recorded");
                }
            });
        }
    }
}
