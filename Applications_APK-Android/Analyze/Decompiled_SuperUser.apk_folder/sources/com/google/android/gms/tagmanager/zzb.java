package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzb extends zzam {
    private static final String ID = zzah.ADVERTISER_ID.toString();
    private final zza zzbEO;

    public zzb(Context context) {
        this(zza.zzbS(context));
    }

    zzb(zza zza) {
        super(ID, new String[0]);
        this.zzbEO = zza;
        this.zzbEO.zzPX();
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        String zzPX = this.zzbEO.zzPX();
        return zzPX == null ? zzdl.zzRT() : zzdl.zzS(zzPX);
    }
}
