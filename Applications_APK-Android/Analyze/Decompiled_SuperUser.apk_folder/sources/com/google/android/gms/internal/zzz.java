package com.google.android.gms.internal;

import java.util.Map;
import org.apache.http.HttpResponse;

public interface zzz {
    HttpResponse zza(zzl<?> zzl, Map<String, String> map);
}
