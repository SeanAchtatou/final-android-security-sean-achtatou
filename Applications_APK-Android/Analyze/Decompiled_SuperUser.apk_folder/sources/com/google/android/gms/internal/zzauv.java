package com.google.android.gms.internal;

import com.google.android.gms.internal.zzauu;

public interface zzauv {

    public static final class zza extends zzbyd<zza> {
        private static volatile zza[] zzbwN;
        public String name;
        public Boolean zzbwO;
        public Boolean zzbwP;

        public zza() {
            zzNw();
        }

        public static zza[] zzNv() {
            if (zzbwN == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbwN == null) {
                        zzbwN = new zza[0];
                    }
                }
            }
            return zzbwN;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.name == null) {
                if (zza.name != null) {
                    return false;
                }
            } else if (!this.name.equals(zza.name)) {
                return false;
            }
            if (this.zzbwO == null) {
                if (zza.zzbwO != null) {
                    return false;
                }
            } else if (!this.zzbwO.equals(zza.zzbwO)) {
                return false;
            }
            if (this.zzbwP == null) {
                if (zza.zzbwP != null) {
                    return false;
                }
            } else if (!this.zzbwP.equals(zza.zzbwP)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zza.zzcwC == null || zza.zzcwC.isEmpty() : this.zzcwC.equals(zza.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwP == null ? 0 : this.zzbwP.hashCode()) + (((this.zzbwO == null ? 0 : this.zzbwO.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzM */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.name = zzbyb.readString();
                        break;
                    case 16:
                        this.zzbwO = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 24:
                        this.zzbwP = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zza zzNw() {
            this.name = null;
            this.zzbwO = null;
            this.zzbwP = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.name != null) {
                zzbyc.zzq(1, this.name);
            }
            if (this.zzbwO != null) {
                zzbyc.zzg(2, this.zzbwO.booleanValue());
            }
            if (this.zzbwP != null) {
                zzbyc.zzg(3, this.zzbwP.booleanValue());
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.name != null) {
                zzu += zzbyc.zzr(1, this.name);
            }
            if (this.zzbwO != null) {
                zzu += zzbyc.zzh(2, this.zzbwO.booleanValue());
            }
            return this.zzbwP != null ? zzu + zzbyc.zzh(3, this.zzbwP.booleanValue()) : zzu;
        }
    }

    public static final class zzb extends zzbyd<zzb> {
        public String zzbqK;
        public Long zzbwQ;
        public Integer zzbwR;
        public zzc[] zzbwS;
        public zza[] zzbwT;
        public zzauu.zza[] zzbwU;

        public zzb() {
            zzNx();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            if (this.zzbwQ == null) {
                if (zzb.zzbwQ != null) {
                    return false;
                }
            } else if (!this.zzbwQ.equals(zzb.zzbwQ)) {
                return false;
            }
            if (this.zzbqK == null) {
                if (zzb.zzbqK != null) {
                    return false;
                }
            } else if (!this.zzbqK.equals(zzb.zzbqK)) {
                return false;
            }
            if (this.zzbwR == null) {
                if (zzb.zzbwR != null) {
                    return false;
                }
            } else if (!this.zzbwR.equals(zzb.zzbwR)) {
                return false;
            }
            if (!zzbyh.equals(this.zzbwS, zzb.zzbwS) || !zzbyh.equals(this.zzbwT, zzb.zzbwT) || !zzbyh.equals(this.zzbwU, zzb.zzbwU)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzb.zzcwC == null || zzb.zzcwC.isEmpty() : this.zzcwC.equals(zzb.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((((((this.zzbwR == null ? 0 : this.zzbwR.hashCode()) + (((this.zzbqK == null ? 0 : this.zzbqK.hashCode()) + (((this.zzbwQ == null ? 0 : this.zzbwQ.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31) + zzbyh.hashCode(this.zzbwS)) * 31) + zzbyh.hashCode(this.zzbwT)) * 31) + zzbyh.hashCode(this.zzbwU)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        /* renamed from: zzN */
        public zzb zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbwQ = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 18:
                        this.zzbqK = zzbyb.readString();
                        break;
                    case 24:
                        this.zzbwR = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 34:
                        int zzb = zzbym.zzb(zzbyb, 34);
                        int length = this.zzbwS == null ? 0 : this.zzbwS.length;
                        zzc[] zzcArr = new zzc[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbwS, 0, zzcArr, 0, length);
                        }
                        while (length < zzcArr.length - 1) {
                            zzcArr[length] = new zzc();
                            zzbyb.zza(zzcArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzcArr[length] = new zzc();
                        zzbyb.zza(zzcArr[length]);
                        this.zzbwS = zzcArr;
                        break;
                    case 42:
                        int zzb2 = zzbym.zzb(zzbyb, 42);
                        int length2 = this.zzbwT == null ? 0 : this.zzbwT.length;
                        zza[] zzaArr = new zza[(zzb2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzbwT, 0, zzaArr, 0, length2);
                        }
                        while (length2 < zzaArr.length - 1) {
                            zzaArr[length2] = new zza();
                            zzbyb.zza(zzaArr[length2]);
                            zzbyb.zzaeW();
                            length2++;
                        }
                        zzaArr[length2] = new zza();
                        zzbyb.zza(zzaArr[length2]);
                        this.zzbwT = zzaArr;
                        break;
                    case 50:
                        int zzb3 = zzbym.zzb(zzbyb, 50);
                        int length3 = this.zzbwU == null ? 0 : this.zzbwU.length;
                        zzauu.zza[] zzaArr2 = new zzauu.zza[(zzb3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzbwU, 0, zzaArr2, 0, length3);
                        }
                        while (length3 < zzaArr2.length - 1) {
                            zzaArr2[length3] = new zzauu.zza();
                            zzbyb.zza(zzaArr2[length3]);
                            zzbyb.zzaeW();
                            length3++;
                        }
                        zzaArr2[length3] = new zzauu.zza();
                        zzbyb.zza(zzaArr2[length3]);
                        this.zzbwU = zzaArr2;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public zzb zzNx() {
            this.zzbwQ = null;
            this.zzbqK = null;
            this.zzbwR = null;
            this.zzbwS = zzc.zzNy();
            this.zzbwT = zza.zzNv();
            this.zzbwU = zzauu.zza.zzNl();
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwQ != null) {
                zzbyc.zzb(1, this.zzbwQ.longValue());
            }
            if (this.zzbqK != null) {
                zzbyc.zzq(2, this.zzbqK);
            }
            if (this.zzbwR != null) {
                zzbyc.zzJ(3, this.zzbwR.intValue());
            }
            if (this.zzbwS != null && this.zzbwS.length > 0) {
                for (zzc zzc : this.zzbwS) {
                    if (zzc != null) {
                        zzbyc.zza(4, zzc);
                    }
                }
            }
            if (this.zzbwT != null && this.zzbwT.length > 0) {
                for (zza zza : this.zzbwT) {
                    if (zza != null) {
                        zzbyc.zza(5, zza);
                    }
                }
            }
            if (this.zzbwU != null && this.zzbwU.length > 0) {
                for (zzauu.zza zza2 : this.zzbwU) {
                    if (zza2 != null) {
                        zzbyc.zza(6, zza2);
                    }
                }
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwQ != null) {
                zzu += zzbyc.zzf(1, this.zzbwQ.longValue());
            }
            if (this.zzbqK != null) {
                zzu += zzbyc.zzr(2, this.zzbqK);
            }
            if (this.zzbwR != null) {
                zzu += zzbyc.zzL(3, this.zzbwR.intValue());
            }
            if (this.zzbwS != null && this.zzbwS.length > 0) {
                int i = zzu;
                for (zzc zzc : this.zzbwS) {
                    if (zzc != null) {
                        i += zzbyc.zzc(4, zzc);
                    }
                }
                zzu = i;
            }
            if (this.zzbwT != null && this.zzbwT.length > 0) {
                int i2 = zzu;
                for (zza zza : this.zzbwT) {
                    if (zza != null) {
                        i2 += zzbyc.zzc(5, zza);
                    }
                }
                zzu = i2;
            }
            if (this.zzbwU != null && this.zzbwU.length > 0) {
                for (zzauu.zza zza2 : this.zzbwU) {
                    if (zza2 != null) {
                        zzu += zzbyc.zzc(6, zza2);
                    }
                }
            }
            return zzu;
        }
    }

    public static final class zzc extends zzbyd<zzc> {
        private static volatile zzc[] zzbwV;
        public String value;
        public String zzaB;

        public zzc() {
            zzNz();
        }

        public static zzc[] zzNy() {
            if (zzbwV == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbwV == null) {
                        zzbwV = new zzc[0];
                    }
                }
            }
            return zzbwV;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzc)) {
                return false;
            }
            zzc zzc = (zzc) obj;
            if (this.zzaB == null) {
                if (zzc.zzaB != null) {
                    return false;
                }
            } else if (!this.zzaB.equals(zzc.zzaB)) {
                return false;
            }
            if (this.value == null) {
                if (zzc.value != null) {
                    return false;
                }
            } else if (!this.value.equals(zzc.value)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzc.zzcwC == null || zzc.zzcwC.isEmpty() : this.zzcwC.equals(zzc.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.value == null ? 0 : this.value.hashCode()) + (((this.zzaB == null ? 0 : this.zzaB.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzc zzNz() {
            this.zzaB = null;
            this.value = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzO */
        public zzc zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzaB = zzbyb.readString();
                        break;
                    case 18:
                        this.value = zzbyb.readString();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzaB != null) {
                zzbyc.zzq(1, this.zzaB);
            }
            if (this.value != null) {
                zzbyc.zzq(2, this.value);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzaB != null) {
                zzu += zzbyc.zzr(1, this.zzaB);
            }
            return this.value != null ? zzu + zzbyc.zzr(2, this.value) : zzu;
        }
    }
}
