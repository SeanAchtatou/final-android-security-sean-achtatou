package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.zzaaf;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzbjd;
import com.google.android.gms.internal.zzbje;
import com.google.android.gms.internal.zzbjf;
import com.google.android.gms.tagmanager.zzbn;
import com.google.android.gms.tagmanager.zzcj;
import com.google.android.gms.tagmanager.zzo;

public class zzp extends zzaaf<ContainerHolder> {
    private final Context mContext;
    private final String zzbEX;
    /* access modifiers changed from: private */
    public long zzbFc;
    private final TagManager zzbFj;
    private final zzd zzbFm;
    /* access modifiers changed from: private */
    public final zzcl zzbFn;
    private final int zzbFo;
    /* access modifiers changed from: private */
    public final zzq zzbFp;
    private zzf zzbFq;
    private zzbje zzbFr;
    /* access modifiers changed from: private */
    public volatile zzo zzbFs;
    /* access modifiers changed from: private */
    public volatile boolean zzbFt;
    /* access modifiers changed from: private */
    public zzaj.zzj zzbFu;
    private String zzbFv;
    private zze zzbFw;
    private zza zzbFx;
    private final Looper zzrs;
    /* access modifiers changed from: private */
    public final com.google.android.gms.common.util.zze zzuP;

    /* renamed from: com.google.android.gms.tagmanager.zzp$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    interface zza {
        boolean zzb(Container container);
    }

    private class zzb implements zzbn<zzbjd.zza> {
        private zzb() {
        }

        /* synthetic */ zzb(zzp zzp, AnonymousClass1 r2) {
            this();
        }

        /* renamed from: zza */
        public void onSuccess(zzbjd.zza zza) {
            zzaj.zzj zzj;
            if (zza.zzbNi != null) {
                zzj = zza.zzbNi;
            } else {
                zzaj.zzf zzf = zza.zzlr;
                zzj = new zzaj.zzj();
                zzj.zzlr = zzf;
                zzj.zzlq = null;
                zzj.zzls = zzf.version;
            }
            zzp.this.zza(zzj, zza.zzbNh, true);
        }

        public void zza(zzbn.zza zza) {
            if (!zzp.this.zzbFt) {
                zzp.this.zzaz(0);
            }
        }
    }

    private class zzc implements zzbn<zzaj.zzj> {
        private zzc() {
        }

        /* synthetic */ zzc(zzp zzp, AnonymousClass1 r2) {
            this();
        }

        public void zza(zzbn.zza zza) {
            if (zza == zzbn.zza.SERVER_UNAVAILABLE_ERROR) {
                zzp.this.zzbFp.zzQu();
            }
            synchronized (zzp.this) {
                if (!zzp.this.isReady()) {
                    if (zzp.this.zzbFs != null) {
                        zzp.this.zzb(zzp.this.zzbFs);
                    } else {
                        zzp.this.zzb(zzp.this.zzc(Status.zzazA));
                    }
                }
            }
            zzp.this.zzaz(zzp.this.zzbFp.zzQt());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* renamed from: zzb */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onSuccess(com.google.android.gms.internal.zzaj.zzj r6) {
            /*
                r5 = this;
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this
                com.google.android.gms.tagmanager.zzq r0 = r0.zzbFp
                r0.zzQv()
                com.google.android.gms.tagmanager.zzp r1 = com.google.android.gms.tagmanager.zzp.this
                monitor-enter(r1)
                com.google.android.gms.internal.zzaj$zzf r0 = r6.zzlr     // Catch:{ all -> 0x0077 }
                if (r0 != 0) goto L_0x003a
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                com.google.android.gms.internal.zzaj$zzj r0 = r0.zzbFu     // Catch:{ all -> 0x0077 }
                com.google.android.gms.internal.zzaj$zzf r0 = r0.zzlr     // Catch:{ all -> 0x0077 }
                if (r0 != 0) goto L_0x0030
                java.lang.String r0 = "Current resource is null; network resource is also null"
                com.google.android.gms.tagmanager.zzbo.e(r0)     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzq r0 = r0.zzbFp     // Catch:{ all -> 0x0077 }
                long r2 = r0.zzQt()     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                r0.zzaz(r2)     // Catch:{ all -> 0x0077 }
                monitor-exit(r1)     // Catch:{ all -> 0x0077 }
            L_0x002f:
                return
            L_0x0030:
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                com.google.android.gms.internal.zzaj$zzj r0 = r0.zzbFu     // Catch:{ all -> 0x0077 }
                com.google.android.gms.internal.zzaj$zzf r0 = r0.zzlr     // Catch:{ all -> 0x0077 }
                r6.zzlr = r0     // Catch:{ all -> 0x0077 }
            L_0x003a:
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzp r2 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                com.google.android.gms.common.util.zze r2 = r2.zzuP     // Catch:{ all -> 0x0077 }
                long r2 = r2.currentTimeMillis()     // Catch:{ all -> 0x0077 }
                r4 = 0
                r0.zza(r6, r2, r4)     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                long r2 = r0.zzbFc     // Catch:{ all -> 0x0077 }
                r0 = 58
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0077 }
                r4.<init>(r0)     // Catch:{ all -> 0x0077 }
                java.lang.String r0 = "setting refresh time to current time: "
                java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0077 }
                java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x0077 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzbo.v(r0)     // Catch:{ all -> 0x0077 }
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                boolean r0 = r0.zzQp()     // Catch:{ all -> 0x0077 }
                if (r0 != 0) goto L_0x0075
                com.google.android.gms.tagmanager.zzp r0 = com.google.android.gms.tagmanager.zzp.this     // Catch:{ all -> 0x0077 }
                r0.zza(r6)     // Catch:{ all -> 0x0077 }
            L_0x0075:
                monitor-exit(r1)     // Catch:{ all -> 0x0077 }
                goto L_0x002f
            L_0x0077:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0077 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzp.zzc.onSuccess(com.google.android.gms.internal.zzaj$zzj):void");
        }
    }

    private class zzd implements zzo.zza {
        private zzd() {
        }

        /* synthetic */ zzd(zzp zzp, AnonymousClass1 r2) {
            this();
        }

        public String zzQj() {
            return zzp.this.zzQj();
        }

        public void zzQl() {
            if (zzp.this.zzbFn.zzpV()) {
                zzp.this.zzaz(0);
            }
        }

        public void zzgW(String str) {
            zzp.this.zzgW(str);
        }
    }

    interface zze extends Releasable {
        void zza(zzbn<zzaj.zzj> zzbn);

        void zzf(long j, String str);

        void zzgZ(String str);
    }

    interface zzf extends Releasable {
        void zzQr();

        void zza(zzbn<zzbjd.zza> zzbn);

        void zzb(zzbjd.zza zza);

        zzbjf.zzc zznz(int i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzp(Context context, TagManager tagManager, Looper looper, String str, int i, zzf zzf2, zze zze2, zzbje zzbje, com.google.android.gms.common.util.zze zze3, zzcl zzcl, zzq zzq) {
        super(looper == null ? Looper.getMainLooper() : looper);
        this.mContext = context;
        this.zzbFj = tagManager;
        this.zzrs = looper == null ? Looper.getMainLooper() : looper;
        this.zzbEX = str;
        this.zzbFo = i;
        this.zzbFq = zzf2;
        this.zzbFw = zze2;
        this.zzbFr = zzbje;
        this.zzbFm = new zzd(this, null);
        this.zzbFu = new zzaj.zzj();
        this.zzuP = zze3;
        this.zzbFn = zzcl;
        this.zzbFp = zzq;
        if (zzQp()) {
            zzgW(zzcj.zzRg().zzRi());
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzp(android.content.Context r19, com.google.android.gms.tagmanager.TagManager r20, android.os.Looper r21, java.lang.String r22, int r23, com.google.android.gms.tagmanager.zzt r24) {
        /*
            r18 = this;
            com.google.android.gms.tagmanager.zzcv r4 = new com.google.android.gms.tagmanager.zzcv
            r0 = r19
            r1 = r22
            r4.<init>(r0, r1)
            com.google.android.gms.tagmanager.zzcu r16 = new com.google.android.gms.tagmanager.zzcu
            r0 = r16
            r1 = r19
            r2 = r22
            r3 = r24
            r0.<init>(r1, r2, r3)
            com.google.android.gms.internal.zzbje r14 = new com.google.android.gms.internal.zzbje
            r0 = r19
            r14.<init>(r0)
            com.google.android.gms.common.util.zze r15 = com.google.android.gms.common.util.zzi.zzzc()
            com.google.android.gms.tagmanager.zzbm r5 = new com.google.android.gms.tagmanager.zzbm
            r6 = 1
            r7 = 5
            r8 = 900000(0xdbba0, double:4.44659E-318)
            r10 = 5000(0x1388, double:2.4703E-320)
            java.lang.String r12 = "refreshing"
            com.google.android.gms.common.util.zze r13 = com.google.android.gms.common.util.zzi.zzzc()
            r5.<init>(r6, r7, r8, r10, r12, r13)
            com.google.android.gms.tagmanager.zzq r17 = new com.google.android.gms.tagmanager.zzq
            r0 = r17
            r1 = r19
            r2 = r22
            r0.<init>(r1, r2)
            r6 = r18
            r7 = r19
            r8 = r20
            r9 = r21
            r10 = r22
            r11 = r23
            r12 = r4
            r13 = r16
            r16 = r5
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r0 = r18
            com.google.android.gms.internal.zzbje r4 = r0.zzbFr
            java.lang.String r5 = r24.zzQx()
            r4.zzig(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzp.<init>(android.content.Context, com.google.android.gms.tagmanager.TagManager, android.os.Looper, java.lang.String, int, com.google.android.gms.tagmanager.zzt):void");
    }

    /* access modifiers changed from: private */
    public boolean zzQp() {
        zzcj zzRg = zzcj.zzRg();
        return (zzRg.zzRh() == zzcj.zza.CONTAINER || zzRg.zzRh() == zzcj.zza.CONTAINER_DEBUG) && this.zzbEX.equals(zzRg.getContainerId());
    }

    /* access modifiers changed from: private */
    public synchronized void zza(zzaj.zzj zzj) {
        if (this.zzbFq != null) {
            zzbjd.zza zza2 = new zzbjd.zza();
            zza2.zzbNh = this.zzbFc;
            zza2.zzlr = new zzaj.zzf();
            zza2.zzbNi = zzj;
            this.zzbFq.zzb(zza2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: private */
    public synchronized void zza(zzaj.zzj zzj, long j, boolean z) {
        if (z) {
            boolean z2 = this.zzbFt;
        }
        if (!isReady() || this.zzbFs != null) {
            this.zzbFu = zzj;
            this.zzbFc = j;
            long zzQs = this.zzbFp.zzQs();
            zzaz(Math.max(0L, Math.min(zzQs, (this.zzbFc + zzQs) - this.zzuP.currentTimeMillis())));
            Container container = new Container(this.mContext, this.zzbFj.getDataLayer(), this.zzbEX, j, zzj);
            if (this.zzbFs == null) {
                this.zzbFs = new zzo(this.zzbFj, this.zzrs, container, this.zzbFm);
            } else {
                this.zzbFs.zza(container);
            }
            if (!isReady() && this.zzbFx.zzb(container)) {
                zzb(this.zzbFs);
            }
        }
    }

    private void zzaQ(final boolean z) {
        this.zzbFq.zza(new zzb(this, null));
        this.zzbFw.zza(new zzc(this, null));
        zzbjf.zzc zznz = this.zzbFq.zznz(this.zzbFo);
        if (zznz != null) {
            this.zzbFs = new zzo(this.zzbFj, this.zzrs, new Container(this.mContext, this.zzbFj.getDataLayer(), this.zzbEX, 0, zznz), this.zzbFm);
        }
        this.zzbFx = new zza() {
            private Long zzbFz;

            private long zzQq() {
                if (this.zzbFz == null) {
                    this.zzbFz = Long.valueOf(zzp.this.zzbFp.zzQs());
                }
                return this.zzbFz.longValue();
            }

            public boolean zzb(Container container) {
                return z ? container.getLastRefreshTime() + zzQq() >= zzp.this.zzuP.currentTimeMillis() : !container.isDefault();
            }
        };
        if (zzQp()) {
            this.zzbFw.zzf(0, "");
        } else {
            this.zzbFq.zzQr();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void zzaz(long j) {
        if (this.zzbFw == null) {
            zzbo.zzbh("Refresh requested, but no network load scheduler.");
        } else {
            this.zzbFw.zzf(j, this.zzbFu.zzls);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized String zzQj() {
        return this.zzbFv;
    }

    public void zzQm() {
        zzbjf.zzc zznz = this.zzbFq.zznz(this.zzbFo);
        if (zznz != null) {
            zzb(new zzo(this.zzbFj, this.zzrs, new Container(this.mContext, this.zzbFj.getDataLayer(), this.zzbEX, 0, zznz), new zzo.zza() {
                public String zzQj() {
                    return zzp.this.zzQj();
                }

                public void zzQl() {
                    zzbo.zzbh("Refresh ignored: container loaded as default only.");
                }

                public void zzgW(String str) {
                    zzp.this.zzgW(str);
                }
            }));
        } else {
            zzbo.e("Default was requested, but no default container was found");
            zzb(zzc(new Status(10, "Default was requested, but no default container was found", null)));
        }
        this.zzbFw = null;
        this.zzbFq = null;
    }

    public void zzQn() {
        zzaQ(false);
    }

    public void zzQo() {
        zzaQ(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzbN */
    public ContainerHolder zzc(Status status) {
        if (this.zzbFs != null) {
            return this.zzbFs;
        }
        if (status == Status.zzazA) {
            zzbo.e("timer expired: setting result to failure");
        }
        return new zzo(status);
    }

    /* access modifiers changed from: package-private */
    public synchronized void zzgW(String str) {
        this.zzbFv = str;
        if (this.zzbFw != null) {
            this.zzbFw.zzgZ(str);
        }
    }
}
