package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzak;
import com.google.android.gms.internal.zzbjf;
import com.google.android.gms.tagmanager.zzm;
import com.google.android.gms.tagmanager.zzu;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class zzcx {
    private static final zzce<zzak.zza> zzbHF = new zzce<>(zzdl.zzRT(), true);
    private final DataLayer zzbEY;
    private final zzbjf.zzc zzbHG;
    private final zzaj zzbHH;
    private final Map<String, zzam> zzbHI;
    private final Map<String, zzam> zzbHJ;
    private final Map<String, zzam> zzbHK;
    private final zzl<zzbjf.zza, zzce<zzak.zza>> zzbHL;
    private final zzl<String, zzb> zzbHM;
    private final Set<zzbjf.zze> zzbHN;
    private final Map<String, zzc> zzbHO;
    private volatile String zzbHP;
    private int zzbHQ;

    interface zza {
        void zza(zzbjf.zze zze, Set<zzbjf.zza> set, Set<zzbjf.zza> set2, zzcs zzcs);
    }

    private static class zzb {
        private zzce<zzak.zza> zzbHV;
        private zzak.zza zzbHW;

        public zzb(zzce<zzak.zza> zzce, zzak.zza zza) {
            this.zzbHV = zzce;
            this.zzbHW = zza;
        }

        public int getSize() {
            return (this.zzbHW == null ? 0 : this.zzbHW.zzafA()) + this.zzbHV.getObject().zzafA();
        }

        public zzce<zzak.zza> zzRs() {
            return this.zzbHV;
        }

        public zzak.zza zzRt() {
            return this.zzbHW;
        }
    }

    private static class zzc {
        private final Set<zzbjf.zze> zzbHN = new HashSet();
        private final Map<zzbjf.zze, List<zzbjf.zza>> zzbHX = new HashMap();
        private final Map<zzbjf.zze, List<zzbjf.zza>> zzbHY = new HashMap();
        private final Map<zzbjf.zze, List<String>> zzbHZ = new HashMap();
        private final Map<zzbjf.zze, List<String>> zzbIa = new HashMap();
        private zzbjf.zza zzbIb;

        public Set<zzbjf.zze> zzRu() {
            return this.zzbHN;
        }

        public Map<zzbjf.zze, List<zzbjf.zza>> zzRv() {
            return this.zzbHX;
        }

        public Map<zzbjf.zze, List<String>> zzRw() {
            return this.zzbHZ;
        }

        public Map<zzbjf.zze, List<String>> zzRx() {
            return this.zzbIa;
        }

        public Map<zzbjf.zze, List<zzbjf.zza>> zzRy() {
            return this.zzbHY;
        }

        public zzbjf.zza zzRz() {
            return this.zzbIb;
        }

        public void zza(zzbjf.zze zze) {
            this.zzbHN.add(zze);
        }

        public void zza(zzbjf.zze zze, zzbjf.zza zza) {
            Object obj = this.zzbHX.get(zze);
            if (obj == null) {
                obj = new ArrayList();
                this.zzbHX.put(zze, obj);
            }
            obj.add(zza);
        }

        public void zza(zzbjf.zze zze, String str) {
            Object obj = this.zzbHZ.get(zze);
            if (obj == null) {
                obj = new ArrayList();
                this.zzbHZ.put(zze, obj);
            }
            obj.add(str);
        }

        public void zzb(zzbjf.zza zza) {
            this.zzbIb = zza;
        }

        public void zzb(zzbjf.zze zze, zzbjf.zza zza) {
            Object obj = this.zzbHY.get(zze);
            if (obj == null) {
                obj = new ArrayList();
                this.zzbHY.put(zze, obj);
            }
            obj.add(zza);
        }

        public void zzb(zzbjf.zze zze, String str) {
            Object obj = this.zzbIa.get(zze);
            if (obj == null) {
                obj = new ArrayList();
                this.zzbIa.put(zze, obj);
            }
            obj.add(str);
        }
    }

    public zzcx(Context context, zzbjf.zzc zzc2, DataLayer dataLayer, zzu.zza zza2, zzu.zza zza3, zzaj zzaj) {
        if (zzc2 == null) {
            throw new NullPointerException("resource cannot be null");
        }
        this.zzbHG = zzc2;
        this.zzbHN = new HashSet(zzc2.zzSW());
        this.zzbEY = dataLayer;
        this.zzbHH = zzaj;
        this.zzbHL = new zzm().zza(1048576, new zzm.zza<zzbjf.zza, zzce<zzak.zza>>(this) {
            /* renamed from: zza */
            public int sizeOf(zzbjf.zza zza, zzce<zzak.zza> zzce) {
                return zzce.getObject().zzafA();
            }
        });
        this.zzbHM = new zzm().zza(1048576, new zzm.zza<String, zzb>(this) {
            /* renamed from: zza */
            public int sizeOf(String str, zzb zzb) {
                return str.length() + zzb.getSize();
            }
        });
        this.zzbHI = new HashMap();
        zzb(new zzj(context));
        zzb(new zzu(zza3));
        zzb(new zzy(dataLayer));
        zzb(new zzdm(context, dataLayer));
        this.zzbHJ = new HashMap();
        zzc(new zzs());
        zzc(new zzag());
        zzc(new zzah());
        zzc(new zzao());
        zzc(new zzap());
        zzc(new zzbk());
        zzc(new zzbl());
        zzc(new zzcn());
        zzc(new zzdf());
        this.zzbHK = new HashMap();
        zza(new zzb(context));
        zza(new zzc(context));
        zza(new zze(context));
        zza(new zzf(context));
        zza(new zzg(context));
        zza(new zzh(context));
        zza(new zzi(context));
        zza(new zzn());
        zza(new zzr(this.zzbHG.getVersion()));
        zza(new zzu(zza2));
        zza(new zzw(dataLayer));
        zza(new zzab(context));
        zza(new zzac());
        zza(new zzaf());
        zza(new zzak(this));
        zza(new zzaq());
        zza(new zzar());
        zza(new zzbe(context));
        zza(new zzbg());
        zza(new zzbj());
        zza(new zzbq());
        zza(new zzbs(context));
        zza(new zzcf());
        zza(new zzch());
        zza(new zzck());
        zza(new zzcm());
        zza(new zzco(context));
        zza(new zzcy());
        zza(new zzcz());
        zza(new zzdh());
        zza(new zzdn());
        this.zzbHO = new HashMap();
        for (zzbjf.zze next : this.zzbHN) {
            for (int i = 0; i < next.zzTE().size(); i++) {
                zzbjf.zza zza4 = next.zzTE().get(i);
                zzc zzh = zzh(this.zzbHO, zza(zza4));
                zzh.zza(next);
                zzh.zza(next, zza4);
                zzh.zza(next, "Unknown");
            }
            for (int i2 = 0; i2 < next.zzTF().size(); i2++) {
                zzbjf.zza zza5 = next.zzTF().get(i2);
                zzc zzh2 = zzh(this.zzbHO, zza(zza5));
                zzh2.zza(next);
                zzh2.zzb(next, zza5);
                zzh2.zzb(next, "Unknown");
            }
        }
        for (Map.Entry next2 : this.zzbHG.zzTB().entrySet()) {
            for (zzbjf.zza zza6 : (List) next2.getValue()) {
                if (!zzdl.zzi(zza6.zzSY().get(zzai.NOT_DEFAULT_MACRO.toString())).booleanValue()) {
                    zzh(this.zzbHO, (String) next2.getKey()).zzb(zza6);
                }
            }
        }
    }

    private String zzRr() {
        if (this.zzbHQ <= 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toString(this.zzbHQ));
        for (int i = 2; i < this.zzbHQ; i++) {
            sb.append(' ');
        }
        sb.append(": ");
        return sb.toString();
    }

    private zzce<zzak.zza> zza(zzak.zza zza2, Set<String> set, zzdo zzdo) {
        if (!zza2.zzlD) {
            return new zzce<>(zza2, true);
        }
        switch (zza2.type) {
            case 2:
                zzak.zza zzm = zzbjf.zzm(zza2);
                zzm.zzlu = new zzak.zza[zza2.zzlu.length];
                for (int i = 0; i < zza2.zzlu.length; i++) {
                    zzce<zzak.zza> zza3 = zza(zza2.zzlu[i], set, zzdo.zznC(i));
                    if (zza3 == zzbHF) {
                        return zzbHF;
                    }
                    zzm.zzlu[i] = zza3.getObject();
                }
                return new zzce<>(zzm, false);
            case 3:
                zzak.zza zzm2 = zzbjf.zzm(zza2);
                if (zza2.zzlv.length != zza2.zzlw.length) {
                    String valueOf = String.valueOf(zza2.toString());
                    zzbo.e(valueOf.length() != 0 ? "Invalid serving value: ".concat(valueOf) : new String("Invalid serving value: "));
                    return zzbHF;
                }
                zzm2.zzlv = new zzak.zza[zza2.zzlv.length];
                zzm2.zzlw = new zzak.zza[zza2.zzlv.length];
                for (int i2 = 0; i2 < zza2.zzlv.length; i2++) {
                    zzce<zzak.zza> zza4 = zza(zza2.zzlv[i2], set, zzdo.zznD(i2));
                    zzce<zzak.zza> zza5 = zza(zza2.zzlw[i2], set, zzdo.zznE(i2));
                    if (zza4 == zzbHF || zza5 == zzbHF) {
                        return zzbHF;
                    }
                    zzm2.zzlv[i2] = zza4.getObject();
                    zzm2.zzlw[i2] = zza5.getObject();
                }
                return new zzce<>(zzm2, false);
            case 4:
                if (set.contains(zza2.zzlx)) {
                    String valueOf2 = String.valueOf(zza2.zzlx);
                    String valueOf3 = String.valueOf(set.toString());
                    zzbo.e(new StringBuilder(String.valueOf(valueOf2).length() + 79 + String.valueOf(valueOf3).length()).append("Macro cycle detected.  Current macro reference: ").append(valueOf2).append(".  Previous macro references: ").append(valueOf3).append(".").toString());
                    return zzbHF;
                }
                set.add(zza2.zzlx);
                zzce<zzak.zza> zza6 = zzdp.zza(zza(zza2.zzlx, set, zzdo.zzRa()), zza2.zzlC);
                set.remove(zza2.zzlx);
                return zza6;
            case 5:
            case 6:
            default:
                zzbo.e(new StringBuilder(25).append("Unknown type: ").append(zza2.type).toString());
                return zzbHF;
            case 7:
                zzak.zza zzm3 = zzbjf.zzm(zza2);
                zzm3.zzlB = new zzak.zza[zza2.zzlB.length];
                for (int i3 = 0; i3 < zza2.zzlB.length; i3++) {
                    zzce<zzak.zza> zza7 = zza(zza2.zzlB[i3], set, zzdo.zznF(i3));
                    if (zza7 == zzbHF) {
                        return zzbHF;
                    }
                    zzm3.zzlB[i3] = zza7.getObject();
                }
                return new zzce<>(zzm3, false);
        }
    }

    private zzce<zzak.zza> zza(String str, Set<String> set, zzbr zzbr) {
        zzbjf.zza zza2;
        this.zzbHQ++;
        zzb zzb2 = this.zzbHM.get(str);
        if (zzb2 != null) {
            zza(zzb2.zzRt(), set);
            this.zzbHQ--;
            return zzb2.zzRs();
        }
        zzc zzc2 = this.zzbHO.get(str);
        if (zzc2 == null) {
            String valueOf = String.valueOf(zzRr());
            zzbo.e(new StringBuilder(String.valueOf(valueOf).length() + 15 + String.valueOf(str).length()).append(valueOf).append("Invalid macro: ").append(str).toString());
            this.zzbHQ--;
            return zzbHF;
        }
        zzce<Set<zzbjf.zza>> zza3 = zza(str, zzc2.zzRu(), zzc2.zzRv(), zzc2.zzRw(), zzc2.zzRy(), zzc2.zzRx(), set, zzbr.zzQB());
        if (zza3.getObject().isEmpty()) {
            zza2 = zzc2.zzRz();
        } else {
            if (zza3.getObject().size() > 1) {
                String valueOf2 = String.valueOf(zzRr());
                zzbo.zzbh(new StringBuilder(String.valueOf(valueOf2).length() + 37 + String.valueOf(str).length()).append(valueOf2).append("Multiple macros active for macroName ").append(str).toString());
            }
            zza2 = (zzbjf.zza) zza3.getObject().iterator().next();
        }
        if (zza2 == null) {
            this.zzbHQ--;
            return zzbHF;
        }
        zzce<zzak.zza> zza4 = zza(this.zzbHK, zza2, set, zzbr.zzQS());
        zzce<zzak.zza> zzce = zza4 == zzbHF ? zzbHF : new zzce<>(zza4.getObject(), zza3.zzRb() && zza4.zzRb());
        zzak.zza zzRt = zza2.zzRt();
        if (zzce.zzRb()) {
            this.zzbHM.zzh(str, new zzb(zzce, zzRt));
        }
        zza(zzRt, set);
        this.zzbHQ--;
        return zzce;
    }

    private zzce<zzak.zza> zza(Map<String, zzam> map, zzbjf.zza zza2, Set<String> set, zzcp zzcp) {
        boolean z;
        boolean z2 = true;
        zzak.zza zza3 = zza2.zzSY().get(zzai.FUNCTION.toString());
        if (zza3 == null) {
            zzbo.e("No function id in properties");
            return zzbHF;
        }
        String str = zza3.zzly;
        zzam zzam = map.get(str);
        if (zzam == null) {
            zzbo.e(String.valueOf(str).concat(" has no backing implementation."));
            return zzbHF;
        }
        zzce<zzak.zza> zzce = this.zzbHL.get(zza2);
        if (zzce != null) {
            return zzce;
        }
        HashMap hashMap = new HashMap();
        boolean z3 = true;
        for (Map.Entry next : zza2.zzSY().entrySet()) {
            zzce<zzak.zza> zza4 = zza((zzak.zza) next.getValue(), set, zzcp.zzhm((String) next.getKey()).zzd((zzak.zza) next.getValue()));
            if (zza4 == zzbHF) {
                return zzbHF;
            }
            if (zza4.zzRb()) {
                zza2.zza((String) next.getKey(), zza4.getObject());
                z = z3;
            } else {
                z = false;
            }
            hashMap.put((String) next.getKey(), zza4.getObject());
            z3 = z;
        }
        if (!zzam.zzf(hashMap.keySet())) {
            String valueOf = String.valueOf(zzam.zzQO());
            String valueOf2 = String.valueOf(hashMap.keySet());
            zzbo.e(new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length()).append("Incorrect keys for function ").append(str).append(" required ").append(valueOf).append(" had ").append(valueOf2).toString());
            return zzbHF;
        }
        if (!z3 || !zzam.zzQd()) {
            z2 = false;
        }
        zzce<zzak.zza> zzce2 = new zzce<>(zzam.zzZ(hashMap), z2);
        if (!z2) {
            return zzce2;
        }
        this.zzbHL.zzh(zza2, zzce2);
        return zzce2;
    }

    private zzce<Set<zzbjf.zza>> zza(Set<zzbjf.zze> set, Set<String> set2, zza zza2, zzcw zzcw) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        boolean z = true;
        for (zzbjf.zze next : set) {
            zzcs zzQZ = zzcw.zzQZ();
            zzce<Boolean> zza3 = zza(next, set2, zzQZ);
            if (zza3.getObject().booleanValue()) {
                zza2.zza(next, hashSet, hashSet2, zzQZ);
            }
            z = z && zza3.zzRb();
        }
        hashSet.removeAll(hashSet2);
        return new zzce<>(hashSet, z);
    }

    private static String zza(zzbjf.zza zza2) {
        return zzdl.zze(zza2.zzSY().get(zzai.INSTANCE_NAME.toString()));
    }

    private void zza(zzak.zza zza2, Set<String> set) {
        zzce<zzak.zza> zza3;
        if (zza2 != null && (zza3 = zza(zza2, set, new zzcc())) != zzbHF) {
            Object zzj = zzdl.zzj(zza3.getObject());
            if (zzj instanceof Map) {
                this.zzbEY.push((Map) zzj);
            } else if (zzj instanceof List) {
                for (Object next : (List) zzj) {
                    if (next instanceof Map) {
                        this.zzbEY.push((Map) next);
                    } else {
                        zzbo.zzbh("pushAfterEvaluate: value not a Map");
                    }
                }
            } else {
                zzbo.zzbh("pushAfterEvaluate: value not a Map or List");
            }
        }
    }

    private static void zza(Map<String, zzam> map, zzam zzam) {
        if (map.containsKey(zzam.zzQN())) {
            String valueOf = String.valueOf(zzam.zzQN());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Duplicate function type name: ".concat(valueOf) : new String("Duplicate function type name: "));
        } else {
            map.put(zzam.zzQN(), zzam);
        }
    }

    private static zzc zzh(Map<String, zzc> map, String str) {
        zzc zzc2 = map.get(str);
        if (zzc2 != null) {
            return zzc2;
        }
        zzc zzc3 = new zzc();
        map.put(str, zzc3);
        return zzc3;
    }

    public synchronized void zzQ(List<zzaj.zzi> list) {
        for (zzaj.zzi next : list) {
            if (next.name == null || !next.name.startsWith("gaExperiment:")) {
                String valueOf = String.valueOf(next);
                zzbo.v(new StringBuilder(String.valueOf(valueOf).length() + 22).append("Ignored supplemental: ").append(valueOf).toString());
            } else {
                zzal.zza(this.zzbEY, next);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized String zzRq() {
        return this.zzbHP;
    }

    /* access modifiers changed from: package-private */
    public zzce<Boolean> zza(zzbjf.zza zza2, Set<String> set, zzcp zzcp) {
        zzce<zzak.zza> zza3 = zza(this.zzbHJ, zza2, set, zzcp);
        Boolean zzi = zzdl.zzi(zza3.getObject());
        zzdl.zzS(zzi);
        return new zzce<>(zzi, zza3.zzRb());
    }

    /* access modifiers changed from: package-private */
    public zzce<Boolean> zza(zzbjf.zze zze, Set<String> set, zzcs zzcs) {
        boolean z;
        boolean z2 = true;
        for (zzbjf.zza zza2 : zze.zzTb()) {
            zzce<Boolean> zza3 = zza(zza2, set, zzcs.zzQT());
            if (zza3.getObject().booleanValue()) {
                zzdl.zzS(false);
                return new zzce<>(false, zza3.zzRb());
            }
            z2 = z && zza3.zzRb();
        }
        for (zzbjf.zza zza4 : zze.zzTa()) {
            zzce<Boolean> zza5 = zza(zza4, set, zzcs.zzQU());
            if (!zza5.getObject().booleanValue()) {
                zzdl.zzS(false);
                return new zzce<>(false, zza5.zzRb());
            }
            z = z && zza5.zzRb();
        }
        zzdl.zzS(true);
        return new zzce<>(true, z);
    }

    /* access modifiers changed from: package-private */
    public zzce<Set<zzbjf.zza>> zza(String str, Set<zzbjf.zze> set, Map<zzbjf.zze, List<zzbjf.zza>> map, Map<zzbjf.zze, List<String>> map2, Map<zzbjf.zze, List<zzbjf.zza>> map3, Map<zzbjf.zze, List<String>> map4, Set<String> set2, zzcw zzcw) {
        final Map<zzbjf.zze, List<zzbjf.zza>> map5 = map;
        final Map<zzbjf.zze, List<String>> map6 = map2;
        final Map<zzbjf.zze, List<zzbjf.zza>> map7 = map3;
        final Map<zzbjf.zze, List<String>> map8 = map4;
        return zza(set, set2, new zza(this) {
            public void zza(zzbjf.zze zze, Set<zzbjf.zza> set, Set<zzbjf.zza> set2, zzcs zzcs) {
                List list = (List) map5.get(zze);
                map6.get(zze);
                if (list != null) {
                    set.addAll(list);
                    zzcs.zzQV();
                }
                List list2 = (List) map7.get(zze);
                map8.get(zze);
                if (list2 != null) {
                    set2.addAll(list2);
                    zzcs.zzQW();
                }
            }
        }, zzcw);
    }

    /* access modifiers changed from: package-private */
    public zzce<Set<zzbjf.zza>> zza(Set<zzbjf.zze> set, zzcw zzcw) {
        return zza(set, new HashSet(), new zza(this) {
            public void zza(zzbjf.zze zze, Set<zzbjf.zza> set, Set<zzbjf.zza> set2, zzcs zzcs) {
                set.addAll(zze.zzTc());
                set2.addAll(zze.zzTd());
                zzcs.zzQX();
                zzcs.zzQY();
            }
        }, zzcw);
    }

    /* access modifiers changed from: package-private */
    public void zza(zzam zzam) {
        zza(this.zzbHK, zzam);
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzam zzam) {
        zza(this.zzbHI, zzam);
    }

    /* access modifiers changed from: package-private */
    public void zzc(zzam zzam) {
        zza(this.zzbHJ, zzam);
    }

    public synchronized void zzgU(String str) {
        zzhr(str);
        zzv zzQM = this.zzbHH.zzhh(str).zzQM();
        for (zzbjf.zza zza2 : zza(this.zzbHN, zzQM.zzQB()).getObject()) {
            zza(this.zzbHI, zza2, new HashSet(), zzQM.zzQA());
        }
        zzhr(null);
    }

    public zzce<zzak.zza> zzhq(String str) {
        this.zzbHQ = 0;
        return zza(str, new HashSet(), this.zzbHH.zzhg(str).zzQL());
    }

    /* access modifiers changed from: package-private */
    public synchronized void zzhr(String str) {
        this.zzbHP = str;
    }
}
