package com.google.android.gms.internal;

import com.google.android.gms.internal.zzbyd;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class zzbye<M extends zzbyd<M>, T> {
    public final int tag;
    protected final int type;
    protected final Class<T> zzckL;
    protected final boolean zzcwD;

    private zzbye(int i, Class<T> cls, int i2, boolean z) {
        this.type = i;
        this.zzckL = cls;
        this.tag = i2;
        this.zzcwD = z;
    }

    public static <M extends zzbyd<M>, T extends zzbyj> zzbye<M, T> zza(int i, Class<T> cls, long j) {
        return new zzbye<>(i, cls, (int) j, false);
    }

    private T zzae(List<zzbyl> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            zzbyl zzbyl = list.get(i);
            if (zzbyl.zzbyc.length != 0) {
                zza(zzbyl, arrayList);
            }
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        T cast = this.zzckL.cast(Array.newInstance(this.zzckL.getComponentType(), size));
        for (int i2 = 0; i2 < size; i2++) {
            Array.set(cast, i2, arrayList.get(i2));
        }
        return cast;
    }

    private T zzaf(List<zzbyl> list) {
        if (list.isEmpty()) {
            return null;
        }
        return this.zzckL.cast(zzaU(zzbyb.zzag(list.get(list.size() - 1).zzbyc)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzbye)) {
            return false;
        }
        zzbye zzbye = (zzbye) obj;
        return this.type == zzbye.type && this.zzckL == zzbye.zzckL && this.tag == zzbye.tag && this.zzcwD == zzbye.zzcwD;
    }

    public int hashCode() {
        return (this.zzcwD ? 1 : 0) + ((((((this.type + 1147) * 31) + this.zzckL.hashCode()) * 31) + this.tag) * 31);
    }

    /* access modifiers changed from: protected */
    public void zza(zzbyl zzbyl, List<Object> list) {
        list.add(zzaU(zzbyb.zzag(zzbyl.zzbyc)));
    }

    /* access modifiers changed from: package-private */
    public void zza(Object obj, zzbyc zzbyc) {
        if (this.zzcwD) {
            zzc(obj, zzbyc);
        } else {
            zzb(obj, zzbyc);
        }
    }

    /* access modifiers changed from: protected */
    public Object zzaU(zzbyb zzbyb) {
        Class componentType = this.zzcwD ? this.zzckL.getComponentType() : this.zzckL;
        try {
            switch (this.type) {
                case 10:
                    zzbyj zzbyj = (zzbyj) componentType.newInstance();
                    zzbyb.zza(zzbyj, zzbym.zzrx(this.tag));
                    return zzbyj;
                case 11:
                    zzbyj zzbyj2 = (zzbyj) componentType.newInstance();
                    zzbyb.zza(zzbyj2);
                    return zzbyj2;
                default:
                    throw new IllegalArgumentException(new StringBuilder(24).append("Unknown type ").append(this.type).toString());
            }
        } catch (InstantiationException e2) {
            String valueOf = String.valueOf(componentType);
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 33).append("Error creating instance of class ").append(valueOf).toString(), e2);
        } catch (IllegalAccessException e3) {
            String valueOf2 = String.valueOf(componentType);
            throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf2).length() + 33).append("Error creating instance of class ").append(valueOf2).toString(), e3);
        } catch (IOException e4) {
            throw new IllegalArgumentException("Error reading extension field", e4);
        }
    }

    /* access modifiers changed from: package-private */
    public int zzaV(Object obj) {
        return this.zzcwD ? zzaW(obj) : zzaX(obj);
    }

    /* access modifiers changed from: protected */
    public int zzaW(Object obj) {
        int i = 0;
        int length = Array.getLength(obj);
        for (int i2 = 0; i2 < length; i2++) {
            if (Array.get(obj, i2) != null) {
                i += zzaX(Array.get(obj, i2));
            }
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public int zzaX(Object obj) {
        int zzrx = zzbym.zzrx(this.tag);
        switch (this.type) {
            case 10:
                return zzbyc.zzb(zzrx, (zzbyj) obj);
            case 11:
                return zzbyc.zzc(zzrx, (zzbyj) obj);
            default:
                throw new IllegalArgumentException(new StringBuilder(24).append("Unknown type ").append(this.type).toString());
        }
    }

    /* access modifiers changed from: package-private */
    public final T zzad(List<zzbyl> list) {
        if (list == null) {
            return null;
        }
        return this.zzcwD ? zzae(list) : zzaf(list);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void zzb(Object obj, zzbyc zzbyc) {
        try {
            zzbyc.zzrp(this.tag);
            switch (this.type) {
                case 10:
                    int zzrx = zzbym.zzrx(this.tag);
                    zzbyc.zzb((zzbyj) obj);
                    zzbyc.zzN(zzrx, 4);
                    return;
                case 11:
                    zzbyc.zzc((zzbyj) obj);
                    return;
                default:
                    throw new IllegalArgumentException(new StringBuilder(24).append("Unknown type ").append(this.type).toString());
            }
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
        throw new IllegalStateException(e2);
    }

    /* access modifiers changed from: protected */
    public void zzc(Object obj, zzbyc zzbyc) {
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            Object obj2 = Array.get(obj, i);
            if (obj2 != null) {
                zzb(obj2, zzbyc);
            }
        }
    }
}
