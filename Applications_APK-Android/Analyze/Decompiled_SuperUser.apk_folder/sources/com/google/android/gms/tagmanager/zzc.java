package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzc extends zzam {
    private static final String ID = zzah.ADVERTISING_TRACKING_ENABLED.toString();
    private final zza zzbEO;

    public zzc(Context context) {
        this(zza.zzbS(context));
    }

    zzc(zza zza) {
        super(ID, new String[0]);
        this.zzbEO = zza;
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        return zzdl.zzS(Boolean.valueOf(!this.zzbEO.isLimitAdTrackingEnabled()));
    }
}
