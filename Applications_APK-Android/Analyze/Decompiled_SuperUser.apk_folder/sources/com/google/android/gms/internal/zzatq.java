package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.zzac;

public final class zzatq extends zza {
    public static final Parcelable.Creator<zzatq> CREATOR = new zzatr();
    public final String name;
    public final String zzbqW;
    public final zzato zzbrH;
    public final long zzbrI;

    zzatq(zzatq zzatq, long j) {
        zzac.zzw(zzatq);
        this.name = zzatq.name;
        this.zzbrH = zzatq.zzbrH;
        this.zzbqW = zzatq.zzbqW;
        this.zzbrI = j;
    }

    public zzatq(String str, zzato zzato, String str2, long j) {
        this.name = str;
        this.zzbrH = zzato;
        this.zzbqW = str2;
        this.zzbrI = j;
    }

    public String toString() {
        String str = this.zzbqW;
        String str2 = this.name;
        String valueOf = String.valueOf(this.zzbrH);
        return new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("origin=").append(str).append(",name=").append(str2).append(",params=").append(valueOf).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzatr.zza(this, parcel, i);
    }
}
