package com.google.android.gms.internal;

import com.lody.virtual.helper.utils.FileUtils;

public interface zzauw {

    public static final class zza extends zzbyd<zza> {
        private static volatile zza[] zzbwW;
        public zzf zzbwX;
        public zzf zzbwY;
        public Boolean zzbwZ;
        public Integer zzbwn;

        public zza() {
            zzNB();
        }

        public static zza[] zzNA() {
            if (zzbwW == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbwW == null) {
                        zzbwW = new zza[0];
                    }
                }
            }
            return zzbwW;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.zzbwn == null) {
                if (zza.zzbwn != null) {
                    return false;
                }
            } else if (!this.zzbwn.equals(zza.zzbwn)) {
                return false;
            }
            if (this.zzbwX == null) {
                if (zza.zzbwX != null) {
                    return false;
                }
            } else if (!this.zzbwX.equals(zza.zzbwX)) {
                return false;
            }
            if (this.zzbwY == null) {
                if (zza.zzbwY != null) {
                    return false;
                }
            } else if (!this.zzbwY.equals(zza.zzbwY)) {
                return false;
            }
            if (this.zzbwZ == null) {
                if (zza.zzbwZ != null) {
                    return false;
                }
            } else if (!this.zzbwZ.equals(zza.zzbwZ)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zza.zzcwC == null || zza.zzcwC.isEmpty() : this.zzcwC.equals(zza.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwZ == null ? 0 : this.zzbwZ.hashCode()) + (((this.zzbwY == null ? 0 : this.zzbwY.hashCode()) + (((this.zzbwX == null ? 0 : this.zzbwX.hashCode()) + (((this.zzbwn == null ? 0 : this.zzbwn.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zza zzNB() {
            this.zzbwn = null;
            this.zzbwX = null;
            this.zzbwY = null;
            this.zzbwZ = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzP */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbwn = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 18:
                        if (this.zzbwX == null) {
                            this.zzbwX = new zzf();
                        }
                        zzbyb.zza(this.zzbwX);
                        break;
                    case 26:
                        if (this.zzbwY == null) {
                            this.zzbwY = new zzf();
                        }
                        zzbyb.zza(this.zzbwY);
                        break;
                    case 32:
                        this.zzbwZ = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbwn != null) {
                zzbyc.zzJ(1, this.zzbwn.intValue());
            }
            if (this.zzbwX != null) {
                zzbyc.zza(2, this.zzbwX);
            }
            if (this.zzbwY != null) {
                zzbyc.zza(3, this.zzbwY);
            }
            if (this.zzbwZ != null) {
                zzbyc.zzg(4, this.zzbwZ.booleanValue());
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbwn != null) {
                zzu += zzbyc.zzL(1, this.zzbwn.intValue());
            }
            if (this.zzbwX != null) {
                zzu += zzbyc.zzc(2, this.zzbwX);
            }
            if (this.zzbwY != null) {
                zzu += zzbyc.zzc(3, this.zzbwY);
            }
            return this.zzbwZ != null ? zzu + zzbyc.zzh(4, this.zzbwZ.booleanValue()) : zzu;
        }
    }

    public static final class zzb extends zzbyd<zzb> {
        private static volatile zzb[] zzbxa;
        public Integer count;
        public String name;
        public zzc[] zzbxb;
        public Long zzbxc;
        public Long zzbxd;

        public zzb() {
            zzND();
        }

        public static zzb[] zzNC() {
            if (zzbxa == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbxa == null) {
                        zzbxa = new zzb[0];
                    }
                }
            }
            return zzbxa;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzb)) {
                return false;
            }
            zzb zzb = (zzb) obj;
            if (!zzbyh.equals(this.zzbxb, zzb.zzbxb)) {
                return false;
            }
            if (this.name == null) {
                if (zzb.name != null) {
                    return false;
                }
            } else if (!this.name.equals(zzb.name)) {
                return false;
            }
            if (this.zzbxc == null) {
                if (zzb.zzbxc != null) {
                    return false;
                }
            } else if (!this.zzbxc.equals(zzb.zzbxc)) {
                return false;
            }
            if (this.zzbxd == null) {
                if (zzb.zzbxd != null) {
                    return false;
                }
            } else if (!this.zzbxd.equals(zzb.zzbxd)) {
                return false;
            }
            if (this.count == null) {
                if (zzb.count != null) {
                    return false;
                }
            } else if (!this.count.equals(zzb.count)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzb.zzcwC == null || zzb.zzcwC.isEmpty() : this.zzcwC.equals(zzb.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.count == null ? 0 : this.count.hashCode()) + (((this.zzbxd == null ? 0 : this.zzbxd.hashCode()) + (((this.zzbxc == null ? 0 : this.zzbxc.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzbxb)) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzb zzND() {
            this.zzbxb = zzc.zzNE();
            this.name = null;
            this.zzbxc = null;
            this.zzbxd = null;
            this.count = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzQ */
        public zzb zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        int zzb = zzbym.zzb(zzbyb, 10);
                        int length = this.zzbxb == null ? 0 : this.zzbxb.length;
                        zzc[] zzcArr = new zzc[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbxb, 0, zzcArr, 0, length);
                        }
                        while (length < zzcArr.length - 1) {
                            zzcArr[length] = new zzc();
                            zzbyb.zza(zzcArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzcArr[length] = new zzc();
                        zzbyb.zza(zzcArr[length]);
                        this.zzbxb = zzcArr;
                        break;
                    case 18:
                        this.name = zzbyb.readString();
                        break;
                    case 24:
                        this.zzbxc = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 32:
                        this.zzbxd = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 40:
                        this.count = Integer.valueOf(zzbyb.zzafa());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbxb != null && this.zzbxb.length > 0) {
                for (zzc zzc : this.zzbxb) {
                    if (zzc != null) {
                        zzbyc.zza(1, zzc);
                    }
                }
            }
            if (this.name != null) {
                zzbyc.zzq(2, this.name);
            }
            if (this.zzbxc != null) {
                zzbyc.zzb(3, this.zzbxc.longValue());
            }
            if (this.zzbxd != null) {
                zzbyc.zzb(4, this.zzbxd.longValue());
            }
            if (this.count != null) {
                zzbyc.zzJ(5, this.count.intValue());
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbxb != null && this.zzbxb.length > 0) {
                for (zzc zzc : this.zzbxb) {
                    if (zzc != null) {
                        zzu += zzbyc.zzc(1, zzc);
                    }
                }
            }
            if (this.name != null) {
                zzu += zzbyc.zzr(2, this.name);
            }
            if (this.zzbxc != null) {
                zzu += zzbyc.zzf(3, this.zzbxc.longValue());
            }
            if (this.zzbxd != null) {
                zzu += zzbyc.zzf(4, this.zzbxd.longValue());
            }
            return this.count != null ? zzu + zzbyc.zzL(5, this.count.intValue()) : zzu;
        }
    }

    public static final class zzc extends zzbyd<zzc> {
        private static volatile zzc[] zzbxe;
        public String name;
        public String zzaGV;
        public Float zzbwh;
        public Double zzbwi;
        public Long zzbxf;

        public zzc() {
            zzNF();
        }

        public static zzc[] zzNE() {
            if (zzbxe == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbxe == null) {
                        zzbxe = new zzc[0];
                    }
                }
            }
            return zzbxe;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzc)) {
                return false;
            }
            zzc zzc = (zzc) obj;
            if (this.name == null) {
                if (zzc.name != null) {
                    return false;
                }
            } else if (!this.name.equals(zzc.name)) {
                return false;
            }
            if (this.zzaGV == null) {
                if (zzc.zzaGV != null) {
                    return false;
                }
            } else if (!this.zzaGV.equals(zzc.zzaGV)) {
                return false;
            }
            if (this.zzbxf == null) {
                if (zzc.zzbxf != null) {
                    return false;
                }
            } else if (!this.zzbxf.equals(zzc.zzbxf)) {
                return false;
            }
            if (this.zzbwh == null) {
                if (zzc.zzbwh != null) {
                    return false;
                }
            } else if (!this.zzbwh.equals(zzc.zzbwh)) {
                return false;
            }
            if (this.zzbwi == null) {
                if (zzc.zzbwi != null) {
                    return false;
                }
            } else if (!this.zzbwi.equals(zzc.zzbwi)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzc.zzcwC == null || zzc.zzcwC.isEmpty() : this.zzcwC.equals(zzc.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwi == null ? 0 : this.zzbwi.hashCode()) + (((this.zzbwh == null ? 0 : this.zzbwh.hashCode()) + (((this.zzbxf == null ? 0 : this.zzbxf.hashCode()) + (((this.zzaGV == null ? 0 : this.zzaGV.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzc zzNF() {
            this.name = null;
            this.zzaGV = null;
            this.zzbxf = null;
            this.zzbwh = null;
            this.zzbwi = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzR */
        public zzc zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.name = zzbyb.readString();
                        break;
                    case 18:
                        this.zzaGV = zzbyb.readString();
                        break;
                    case 24:
                        this.zzbxf = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 37:
                        this.zzbwh = Float.valueOf(zzbyb.readFloat());
                        break;
                    case 41:
                        this.zzbwi = Double.valueOf(zzbyb.readDouble());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.name != null) {
                zzbyc.zzq(1, this.name);
            }
            if (this.zzaGV != null) {
                zzbyc.zzq(2, this.zzaGV);
            }
            if (this.zzbxf != null) {
                zzbyc.zzb(3, this.zzbxf.longValue());
            }
            if (this.zzbwh != null) {
                zzbyc.zzc(4, this.zzbwh.floatValue());
            }
            if (this.zzbwi != null) {
                zzbyc.zza(5, this.zzbwi.doubleValue());
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.name != null) {
                zzu += zzbyc.zzr(1, this.name);
            }
            if (this.zzaGV != null) {
                zzu += zzbyc.zzr(2, this.zzaGV);
            }
            if (this.zzbxf != null) {
                zzu += zzbyc.zzf(3, this.zzbxf.longValue());
            }
            if (this.zzbwh != null) {
                zzu += zzbyc.zzd(4, this.zzbwh.floatValue());
            }
            return this.zzbwi != null ? zzu + zzbyc.zzb(5, this.zzbwi.doubleValue()) : zzu;
        }
    }

    public static final class zzd extends zzbyd<zzd> {
        public zze[] zzbxg;

        public zzd() {
            zzNG();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzd)) {
                return false;
            }
            zzd zzd = (zzd) obj;
            if (zzbyh.equals(this.zzbxg, zzd.zzbxg)) {
                return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzd.zzcwC == null || zzd.zzcwC.isEmpty() : this.zzcwC.equals(zzd.zzcwC);
            }
            return false;
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzbxg)) * 31);
        }

        public zzd zzNG() {
            this.zzbxg = zze.zzNH();
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzS */
        public zzd zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        int zzb = zzbym.zzb(zzbyb, 10);
                        int length = this.zzbxg == null ? 0 : this.zzbxg.length;
                        zze[] zzeArr = new zze[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbxg, 0, zzeArr, 0, length);
                        }
                        while (length < zzeArr.length - 1) {
                            zzeArr[length] = new zze();
                            zzbyb.zza(zzeArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzeArr[length] = new zze();
                        zzbyb.zza(zzeArr[length]);
                        this.zzbxg = zzeArr;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbxg != null && this.zzbxg.length > 0) {
                for (zze zze : this.zzbxg) {
                    if (zze != null) {
                        zzbyc.zza(1, zze);
                    }
                }
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbxg != null && this.zzbxg.length > 0) {
                for (zze zze : this.zzbxg) {
                    if (zze != null) {
                        zzu += zzbyc.zzc(1, zze);
                    }
                }
            }
            return zzu;
        }
    }

    public static final class zze extends zzbyd<zze> {
        private static volatile zze[] zzbxh;
        public String zzaS;
        public String zzbb;
        public String zzbhN;
        public String zzbqK;
        public String zzbqL;
        public String zzbqO;
        public String zzbqS;
        public Integer zzbxA;
        public Boolean zzbxB;
        public zza[] zzbxC;
        public Integer zzbxD;
        public Integer zzbxE;
        public Integer zzbxF;
        public String zzbxG;
        public Long zzbxH;
        public Long zzbxI;
        public Integer zzbxi;
        public zzb[] zzbxj;
        public zzg[] zzbxk;
        public Long zzbxl;
        public Long zzbxm;
        public Long zzbxn;
        public Long zzbxo;
        public Long zzbxp;
        public String zzbxq;
        public String zzbxr;
        public String zzbxs;
        public Integer zzbxt;
        public Long zzbxu;
        public Long zzbxv;
        public String zzbxw;
        public Boolean zzbxx;
        public String zzbxy;
        public Long zzbxz;

        public zze() {
            zzNI();
        }

        public static zze[] zzNH() {
            if (zzbxh == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbxh == null) {
                        zzbxh = new zze[0];
                    }
                }
            }
            return zzbxh;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zze)) {
                return false;
            }
            zze zze = (zze) obj;
            if (this.zzbxi == null) {
                if (zze.zzbxi != null) {
                    return false;
                }
            } else if (!this.zzbxi.equals(zze.zzbxi)) {
                return false;
            }
            if (!zzbyh.equals(this.zzbxj, zze.zzbxj) || !zzbyh.equals(this.zzbxk, zze.zzbxk)) {
                return false;
            }
            if (this.zzbxl == null) {
                if (zze.zzbxl != null) {
                    return false;
                }
            } else if (!this.zzbxl.equals(zze.zzbxl)) {
                return false;
            }
            if (this.zzbxm == null) {
                if (zze.zzbxm != null) {
                    return false;
                }
            } else if (!this.zzbxm.equals(zze.zzbxm)) {
                return false;
            }
            if (this.zzbxn == null) {
                if (zze.zzbxn != null) {
                    return false;
                }
            } else if (!this.zzbxn.equals(zze.zzbxn)) {
                return false;
            }
            if (this.zzbxo == null) {
                if (zze.zzbxo != null) {
                    return false;
                }
            } else if (!this.zzbxo.equals(zze.zzbxo)) {
                return false;
            }
            if (this.zzbxp == null) {
                if (zze.zzbxp != null) {
                    return false;
                }
            } else if (!this.zzbxp.equals(zze.zzbxp)) {
                return false;
            }
            if (this.zzbxq == null) {
                if (zze.zzbxq != null) {
                    return false;
                }
            } else if (!this.zzbxq.equals(zze.zzbxq)) {
                return false;
            }
            if (this.zzbb == null) {
                if (zze.zzbb != null) {
                    return false;
                }
            } else if (!this.zzbb.equals(zze.zzbb)) {
                return false;
            }
            if (this.zzbxr == null) {
                if (zze.zzbxr != null) {
                    return false;
                }
            } else if (!this.zzbxr.equals(zze.zzbxr)) {
                return false;
            }
            if (this.zzbxs == null) {
                if (zze.zzbxs != null) {
                    return false;
                }
            } else if (!this.zzbxs.equals(zze.zzbxs)) {
                return false;
            }
            if (this.zzbxt == null) {
                if (zze.zzbxt != null) {
                    return false;
                }
            } else if (!this.zzbxt.equals(zze.zzbxt)) {
                return false;
            }
            if (this.zzbqL == null) {
                if (zze.zzbqL != null) {
                    return false;
                }
            } else if (!this.zzbqL.equals(zze.zzbqL)) {
                return false;
            }
            if (this.zzaS == null) {
                if (zze.zzaS != null) {
                    return false;
                }
            } else if (!this.zzaS.equals(zze.zzaS)) {
                return false;
            }
            if (this.zzbhN == null) {
                if (zze.zzbhN != null) {
                    return false;
                }
            } else if (!this.zzbhN.equals(zze.zzbhN)) {
                return false;
            }
            if (this.zzbxu == null) {
                if (zze.zzbxu != null) {
                    return false;
                }
            } else if (!this.zzbxu.equals(zze.zzbxu)) {
                return false;
            }
            if (this.zzbxv == null) {
                if (zze.zzbxv != null) {
                    return false;
                }
            } else if (!this.zzbxv.equals(zze.zzbxv)) {
                return false;
            }
            if (this.zzbxw == null) {
                if (zze.zzbxw != null) {
                    return false;
                }
            } else if (!this.zzbxw.equals(zze.zzbxw)) {
                return false;
            }
            if (this.zzbxx == null) {
                if (zze.zzbxx != null) {
                    return false;
                }
            } else if (!this.zzbxx.equals(zze.zzbxx)) {
                return false;
            }
            if (this.zzbxy == null) {
                if (zze.zzbxy != null) {
                    return false;
                }
            } else if (!this.zzbxy.equals(zze.zzbxy)) {
                return false;
            }
            if (this.zzbxz == null) {
                if (zze.zzbxz != null) {
                    return false;
                }
            } else if (!this.zzbxz.equals(zze.zzbxz)) {
                return false;
            }
            if (this.zzbxA == null) {
                if (zze.zzbxA != null) {
                    return false;
                }
            } else if (!this.zzbxA.equals(zze.zzbxA)) {
                return false;
            }
            if (this.zzbqO == null) {
                if (zze.zzbqO != null) {
                    return false;
                }
            } else if (!this.zzbqO.equals(zze.zzbqO)) {
                return false;
            }
            if (this.zzbqK == null) {
                if (zze.zzbqK != null) {
                    return false;
                }
            } else if (!this.zzbqK.equals(zze.zzbqK)) {
                return false;
            }
            if (this.zzbxB == null) {
                if (zze.zzbxB != null) {
                    return false;
                }
            } else if (!this.zzbxB.equals(zze.zzbxB)) {
                return false;
            }
            if (!zzbyh.equals(this.zzbxC, zze.zzbxC)) {
                return false;
            }
            if (this.zzbqS == null) {
                if (zze.zzbqS != null) {
                    return false;
                }
            } else if (!this.zzbqS.equals(zze.zzbqS)) {
                return false;
            }
            if (this.zzbxD == null) {
                if (zze.zzbxD != null) {
                    return false;
                }
            } else if (!this.zzbxD.equals(zze.zzbxD)) {
                return false;
            }
            if (this.zzbxE == null) {
                if (zze.zzbxE != null) {
                    return false;
                }
            } else if (!this.zzbxE.equals(zze.zzbxE)) {
                return false;
            }
            if (this.zzbxF == null) {
                if (zze.zzbxF != null) {
                    return false;
                }
            } else if (!this.zzbxF.equals(zze.zzbxF)) {
                return false;
            }
            if (this.zzbxG == null) {
                if (zze.zzbxG != null) {
                    return false;
                }
            } else if (!this.zzbxG.equals(zze.zzbxG)) {
                return false;
            }
            if (this.zzbxH == null) {
                if (zze.zzbxH != null) {
                    return false;
                }
            } else if (!this.zzbxH.equals(zze.zzbxH)) {
                return false;
            }
            if (this.zzbxI == null) {
                if (zze.zzbxI != null) {
                    return false;
                }
            } else if (!this.zzbxI.equals(zze.zzbxI)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zze.zzcwC == null || zze.zzcwC.isEmpty() : this.zzcwC.equals(zze.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbxI == null ? 0 : this.zzbxI.hashCode()) + (((this.zzbxH == null ? 0 : this.zzbxH.hashCode()) + (((this.zzbxG == null ? 0 : this.zzbxG.hashCode()) + (((this.zzbxF == null ? 0 : this.zzbxF.hashCode()) + (((this.zzbxE == null ? 0 : this.zzbxE.hashCode()) + (((this.zzbxD == null ? 0 : this.zzbxD.hashCode()) + (((this.zzbqS == null ? 0 : this.zzbqS.hashCode()) + (((((this.zzbxB == null ? 0 : this.zzbxB.hashCode()) + (((this.zzbqK == null ? 0 : this.zzbqK.hashCode()) + (((this.zzbqO == null ? 0 : this.zzbqO.hashCode()) + (((this.zzbxA == null ? 0 : this.zzbxA.hashCode()) + (((this.zzbxz == null ? 0 : this.zzbxz.hashCode()) + (((this.zzbxy == null ? 0 : this.zzbxy.hashCode()) + (((this.zzbxx == null ? 0 : this.zzbxx.hashCode()) + (((this.zzbxw == null ? 0 : this.zzbxw.hashCode()) + (((this.zzbxv == null ? 0 : this.zzbxv.hashCode()) + (((this.zzbxu == null ? 0 : this.zzbxu.hashCode()) + (((this.zzbhN == null ? 0 : this.zzbhN.hashCode()) + (((this.zzaS == null ? 0 : this.zzaS.hashCode()) + (((this.zzbqL == null ? 0 : this.zzbqL.hashCode()) + (((this.zzbxt == null ? 0 : this.zzbxt.hashCode()) + (((this.zzbxs == null ? 0 : this.zzbxs.hashCode()) + (((this.zzbxr == null ? 0 : this.zzbxr.hashCode()) + (((this.zzbb == null ? 0 : this.zzbb.hashCode()) + (((this.zzbxq == null ? 0 : this.zzbxq.hashCode()) + (((this.zzbxp == null ? 0 : this.zzbxp.hashCode()) + (((this.zzbxo == null ? 0 : this.zzbxo.hashCode()) + (((this.zzbxn == null ? 0 : this.zzbxn.hashCode()) + (((this.zzbxm == null ? 0 : this.zzbxm.hashCode()) + (((this.zzbxl == null ? 0 : this.zzbxl.hashCode()) + (((((((this.zzbxi == null ? 0 : this.zzbxi.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31) + zzbyh.hashCode(this.zzbxj)) * 31) + zzbyh.hashCode(this.zzbxk)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + zzbyh.hashCode(this.zzbxC)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zze zzNI() {
            this.zzbxi = null;
            this.zzbxj = zzb.zzNC();
            this.zzbxk = zzg.zzNK();
            this.zzbxl = null;
            this.zzbxm = null;
            this.zzbxn = null;
            this.zzbxo = null;
            this.zzbxp = null;
            this.zzbxq = null;
            this.zzbb = null;
            this.zzbxr = null;
            this.zzbxs = null;
            this.zzbxt = null;
            this.zzbqL = null;
            this.zzaS = null;
            this.zzbhN = null;
            this.zzbxu = null;
            this.zzbxv = null;
            this.zzbxw = null;
            this.zzbxx = null;
            this.zzbxy = null;
            this.zzbxz = null;
            this.zzbxA = null;
            this.zzbqO = null;
            this.zzbqK = null;
            this.zzbxB = null;
            this.zzbxC = zza.zzNA();
            this.zzbqS = null;
            this.zzbxD = null;
            this.zzbxE = null;
            this.zzbxF = null;
            this.zzbxG = null;
            this.zzbxH = null;
            this.zzbxI = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzT */
        public zze zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbxi = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 18:
                        int zzb = zzbym.zzb(zzbyb, 18);
                        int length = this.zzbxj == null ? 0 : this.zzbxj.length;
                        zzb[] zzbArr = new zzb[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbxj, 0, zzbArr, 0, length);
                        }
                        while (length < zzbArr.length - 1) {
                            zzbArr[length] = new zzb();
                            zzbyb.zza(zzbArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzbArr[length] = new zzb();
                        zzbyb.zza(zzbArr[length]);
                        this.zzbxj = zzbArr;
                        break;
                    case 26:
                        int zzb2 = zzbym.zzb(zzbyb, 26);
                        int length2 = this.zzbxk == null ? 0 : this.zzbxk.length;
                        zzg[] zzgArr = new zzg[(zzb2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzbxk, 0, zzgArr, 0, length2);
                        }
                        while (length2 < zzgArr.length - 1) {
                            zzgArr[length2] = new zzg();
                            zzbyb.zza(zzgArr[length2]);
                            zzbyb.zzaeW();
                            length2++;
                        }
                        zzgArr[length2] = new zzg();
                        zzbyb.zza(zzgArr[length2]);
                        this.zzbxk = zzgArr;
                        break;
                    case 32:
                        this.zzbxl = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 40:
                        this.zzbxm = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 48:
                        this.zzbxn = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 56:
                        this.zzbxp = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 66:
                        this.zzbxq = zzbyb.readString();
                        break;
                    case 74:
                        this.zzbb = zzbyb.readString();
                        break;
                    case 82:
                        this.zzbxr = zzbyb.readString();
                        break;
                    case 90:
                        this.zzbxs = zzbyb.readString();
                        break;
                    case 96:
                        this.zzbxt = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 106:
                        this.zzbqL = zzbyb.readString();
                        break;
                    case 114:
                        this.zzaS = zzbyb.readString();
                        break;
                    case 130:
                        this.zzbhN = zzbyb.readString();
                        break;
                    case 136:
                        this.zzbxu = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 144:
                        this.zzbxv = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 154:
                        this.zzbxw = zzbyb.readString();
                        break;
                    case 160:
                        this.zzbxx = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 170:
                        this.zzbxy = zzbyb.readString();
                        break;
                    case 176:
                        this.zzbxz = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 184:
                        this.zzbxA = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 194:
                        this.zzbqO = zzbyb.readString();
                        break;
                    case 202:
                        this.zzbqK = zzbyb.readString();
                        break;
                    case 208:
                        this.zzbxo = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 224:
                        this.zzbxB = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 234:
                        int zzb3 = zzbym.zzb(zzbyb, 234);
                        int length3 = this.zzbxC == null ? 0 : this.zzbxC.length;
                        zza[] zzaArr = new zza[(zzb3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzbxC, 0, zzaArr, 0, length3);
                        }
                        while (length3 < zzaArr.length - 1) {
                            zzaArr[length3] = new zza();
                            zzbyb.zza(zzaArr[length3]);
                            zzbyb.zzaeW();
                            length3++;
                        }
                        zzaArr[length3] = new zza();
                        zzbyb.zza(zzaArr[length3]);
                        this.zzbxC = zzaArr;
                        break;
                    case 242:
                        this.zzbqS = zzbyb.readString();
                        break;
                    case 248:
                        this.zzbxD = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case FileUtils.FileMode.MODE_IRUSR /*256*/:
                        this.zzbxE = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 264:
                        this.zzbxF = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 274:
                        this.zzbxG = zzbyb.readString();
                        break;
                    case 280:
                        this.zzbxH = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 288:
                        this.zzbxI = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbxi != null) {
                zzbyc.zzJ(1, this.zzbxi.intValue());
            }
            if (this.zzbxj != null && this.zzbxj.length > 0) {
                for (zzb zzb : this.zzbxj) {
                    if (zzb != null) {
                        zzbyc.zza(2, zzb);
                    }
                }
            }
            if (this.zzbxk != null && this.zzbxk.length > 0) {
                for (zzg zzg : this.zzbxk) {
                    if (zzg != null) {
                        zzbyc.zza(3, zzg);
                    }
                }
            }
            if (this.zzbxl != null) {
                zzbyc.zzb(4, this.zzbxl.longValue());
            }
            if (this.zzbxm != null) {
                zzbyc.zzb(5, this.zzbxm.longValue());
            }
            if (this.zzbxn != null) {
                zzbyc.zzb(6, this.zzbxn.longValue());
            }
            if (this.zzbxp != null) {
                zzbyc.zzb(7, this.zzbxp.longValue());
            }
            if (this.zzbxq != null) {
                zzbyc.zzq(8, this.zzbxq);
            }
            if (this.zzbb != null) {
                zzbyc.zzq(9, this.zzbb);
            }
            if (this.zzbxr != null) {
                zzbyc.zzq(10, this.zzbxr);
            }
            if (this.zzbxs != null) {
                zzbyc.zzq(11, this.zzbxs);
            }
            if (this.zzbxt != null) {
                zzbyc.zzJ(12, this.zzbxt.intValue());
            }
            if (this.zzbqL != null) {
                zzbyc.zzq(13, this.zzbqL);
            }
            if (this.zzaS != null) {
                zzbyc.zzq(14, this.zzaS);
            }
            if (this.zzbhN != null) {
                zzbyc.zzq(16, this.zzbhN);
            }
            if (this.zzbxu != null) {
                zzbyc.zzb(17, this.zzbxu.longValue());
            }
            if (this.zzbxv != null) {
                zzbyc.zzb(18, this.zzbxv.longValue());
            }
            if (this.zzbxw != null) {
                zzbyc.zzq(19, this.zzbxw);
            }
            if (this.zzbxx != null) {
                zzbyc.zzg(20, this.zzbxx.booleanValue());
            }
            if (this.zzbxy != null) {
                zzbyc.zzq(21, this.zzbxy);
            }
            if (this.zzbxz != null) {
                zzbyc.zzb(22, this.zzbxz.longValue());
            }
            if (this.zzbxA != null) {
                zzbyc.zzJ(23, this.zzbxA.intValue());
            }
            if (this.zzbqO != null) {
                zzbyc.zzq(24, this.zzbqO);
            }
            if (this.zzbqK != null) {
                zzbyc.zzq(25, this.zzbqK);
            }
            if (this.zzbxo != null) {
                zzbyc.zzb(26, this.zzbxo.longValue());
            }
            if (this.zzbxB != null) {
                zzbyc.zzg(28, this.zzbxB.booleanValue());
            }
            if (this.zzbxC != null && this.zzbxC.length > 0) {
                for (zza zza : this.zzbxC) {
                    if (zza != null) {
                        zzbyc.zza(29, zza);
                    }
                }
            }
            if (this.zzbqS != null) {
                zzbyc.zzq(30, this.zzbqS);
            }
            if (this.zzbxD != null) {
                zzbyc.zzJ(31, this.zzbxD.intValue());
            }
            if (this.zzbxE != null) {
                zzbyc.zzJ(32, this.zzbxE.intValue());
            }
            if (this.zzbxF != null) {
                zzbyc.zzJ(33, this.zzbxF.intValue());
            }
            if (this.zzbxG != null) {
                zzbyc.zzq(34, this.zzbxG);
            }
            if (this.zzbxH != null) {
                zzbyc.zzb(35, this.zzbxH.longValue());
            }
            if (this.zzbxI != null) {
                zzbyc.zzb(36, this.zzbxI.longValue());
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbxi != null) {
                zzu += zzbyc.zzL(1, this.zzbxi.intValue());
            }
            if (this.zzbxj != null && this.zzbxj.length > 0) {
                int i = zzu;
                for (zzb zzb : this.zzbxj) {
                    if (zzb != null) {
                        i += zzbyc.zzc(2, zzb);
                    }
                }
                zzu = i;
            }
            if (this.zzbxk != null && this.zzbxk.length > 0) {
                int i2 = zzu;
                for (zzg zzg : this.zzbxk) {
                    if (zzg != null) {
                        i2 += zzbyc.zzc(3, zzg);
                    }
                }
                zzu = i2;
            }
            if (this.zzbxl != null) {
                zzu += zzbyc.zzf(4, this.zzbxl.longValue());
            }
            if (this.zzbxm != null) {
                zzu += zzbyc.zzf(5, this.zzbxm.longValue());
            }
            if (this.zzbxn != null) {
                zzu += zzbyc.zzf(6, this.zzbxn.longValue());
            }
            if (this.zzbxp != null) {
                zzu += zzbyc.zzf(7, this.zzbxp.longValue());
            }
            if (this.zzbxq != null) {
                zzu += zzbyc.zzr(8, this.zzbxq);
            }
            if (this.zzbb != null) {
                zzu += zzbyc.zzr(9, this.zzbb);
            }
            if (this.zzbxr != null) {
                zzu += zzbyc.zzr(10, this.zzbxr);
            }
            if (this.zzbxs != null) {
                zzu += zzbyc.zzr(11, this.zzbxs);
            }
            if (this.zzbxt != null) {
                zzu += zzbyc.zzL(12, this.zzbxt.intValue());
            }
            if (this.zzbqL != null) {
                zzu += zzbyc.zzr(13, this.zzbqL);
            }
            if (this.zzaS != null) {
                zzu += zzbyc.zzr(14, this.zzaS);
            }
            if (this.zzbhN != null) {
                zzu += zzbyc.zzr(16, this.zzbhN);
            }
            if (this.zzbxu != null) {
                zzu += zzbyc.zzf(17, this.zzbxu.longValue());
            }
            if (this.zzbxv != null) {
                zzu += zzbyc.zzf(18, this.zzbxv.longValue());
            }
            if (this.zzbxw != null) {
                zzu += zzbyc.zzr(19, this.zzbxw);
            }
            if (this.zzbxx != null) {
                zzu += zzbyc.zzh(20, this.zzbxx.booleanValue());
            }
            if (this.zzbxy != null) {
                zzu += zzbyc.zzr(21, this.zzbxy);
            }
            if (this.zzbxz != null) {
                zzu += zzbyc.zzf(22, this.zzbxz.longValue());
            }
            if (this.zzbxA != null) {
                zzu += zzbyc.zzL(23, this.zzbxA.intValue());
            }
            if (this.zzbqO != null) {
                zzu += zzbyc.zzr(24, this.zzbqO);
            }
            if (this.zzbqK != null) {
                zzu += zzbyc.zzr(25, this.zzbqK);
            }
            if (this.zzbxo != null) {
                zzu += zzbyc.zzf(26, this.zzbxo.longValue());
            }
            if (this.zzbxB != null) {
                zzu += zzbyc.zzh(28, this.zzbxB.booleanValue());
            }
            if (this.zzbxC != null && this.zzbxC.length > 0) {
                for (zza zza : this.zzbxC) {
                    if (zza != null) {
                        zzu += zzbyc.zzc(29, zza);
                    }
                }
            }
            if (this.zzbqS != null) {
                zzu += zzbyc.zzr(30, this.zzbqS);
            }
            if (this.zzbxD != null) {
                zzu += zzbyc.zzL(31, this.zzbxD.intValue());
            }
            if (this.zzbxE != null) {
                zzu += zzbyc.zzL(32, this.zzbxE.intValue());
            }
            if (this.zzbxF != null) {
                zzu += zzbyc.zzL(33, this.zzbxF.intValue());
            }
            if (this.zzbxG != null) {
                zzu += zzbyc.zzr(34, this.zzbxG);
            }
            if (this.zzbxH != null) {
                zzu += zzbyc.zzf(35, this.zzbxH.longValue());
            }
            return this.zzbxI != null ? zzu + zzbyc.zzf(36, this.zzbxI.longValue()) : zzu;
        }
    }

    public static final class zzf extends zzbyd<zzf> {
        public long[] zzbxJ;
        public long[] zzbxK;

        public zzf() {
            zzNJ();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzf)) {
                return false;
            }
            zzf zzf = (zzf) obj;
            if (!zzbyh.equals(this.zzbxJ, zzf.zzbxJ) || !zzbyh.equals(this.zzbxK, zzf.zzbxK)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzf.zzcwC == null || zzf.zzcwC.isEmpty() : this.zzcwC.equals(zzf.zzcwC);
        }

        public int hashCode() {
            return ((this.zzcwC == null || this.zzcwC.isEmpty()) ? 0 : this.zzcwC.hashCode()) + ((((((getClass().getName().hashCode() + 527) * 31) + zzbyh.hashCode(this.zzbxJ)) * 31) + zzbyh.hashCode(this.zzbxK)) * 31);
        }

        public zzf zzNJ() {
            this.zzbxJ = zzbym.zzcwR;
            this.zzbxK = zzbym.zzcwR;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzU */
        public zzf zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        int zzb = zzbym.zzb(zzbyb, 8);
                        int length = this.zzbxJ == null ? 0 : this.zzbxJ.length;
                        long[] jArr = new long[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbxJ, 0, jArr, 0, length);
                        }
                        while (length < jArr.length - 1) {
                            jArr[length] = zzbyb.zzaeY();
                            zzbyb.zzaeW();
                            length++;
                        }
                        jArr[length] = zzbyb.zzaeY();
                        this.zzbxJ = jArr;
                        break;
                    case 10:
                        int zzrf = zzbyb.zzrf(zzbyb.zzaff());
                        int position = zzbyb.getPosition();
                        int i = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzaeY();
                            i++;
                        }
                        zzbyb.zzrh(position);
                        int length2 = this.zzbxJ == null ? 0 : this.zzbxJ.length;
                        long[] jArr2 = new long[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzbxJ, 0, jArr2, 0, length2);
                        }
                        while (length2 < jArr2.length) {
                            jArr2[length2] = zzbyb.zzaeY();
                            length2++;
                        }
                        this.zzbxJ = jArr2;
                        zzbyb.zzrg(zzrf);
                        break;
                    case 16:
                        int zzb2 = zzbym.zzb(zzbyb, 16);
                        int length3 = this.zzbxK == null ? 0 : this.zzbxK.length;
                        long[] jArr3 = new long[(zzb2 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.zzbxK, 0, jArr3, 0, length3);
                        }
                        while (length3 < jArr3.length - 1) {
                            jArr3[length3] = zzbyb.zzaeY();
                            zzbyb.zzaeW();
                            length3++;
                        }
                        jArr3[length3] = zzbyb.zzaeY();
                        this.zzbxK = jArr3;
                        break;
                    case 18:
                        int zzrf2 = zzbyb.zzrf(zzbyb.zzaff());
                        int position2 = zzbyb.getPosition();
                        int i2 = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzaeY();
                            i2++;
                        }
                        zzbyb.zzrh(position2);
                        int length4 = this.zzbxK == null ? 0 : this.zzbxK.length;
                        long[] jArr4 = new long[(i2 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.zzbxK, 0, jArr4, 0, length4);
                        }
                        while (length4 < jArr4.length) {
                            jArr4[length4] = zzbyb.zzaeY();
                            length4++;
                        }
                        this.zzbxK = jArr4;
                        zzbyb.zzrg(zzrf2);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbxJ != null && this.zzbxJ.length > 0) {
                for (long zza : this.zzbxJ) {
                    zzbyc.zza(1, zza);
                }
            }
            if (this.zzbxK != null && this.zzbxK.length > 0) {
                for (long zza2 : this.zzbxK) {
                    zzbyc.zza(2, zza2);
                }
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int i;
            int zzu = super.zzu();
            if (this.zzbxJ == null || this.zzbxJ.length <= 0) {
                i = zzu;
            } else {
                int i2 = 0;
                for (long zzbp : this.zzbxJ) {
                    i2 += zzbyc.zzbp(zzbp);
                }
                i = zzu + i2 + (this.zzbxJ.length * 1);
            }
            if (this.zzbxK == null || this.zzbxK.length <= 0) {
                return i;
            }
            int i3 = 0;
            for (long zzbp2 : this.zzbxK) {
                i3 += zzbyc.zzbp(zzbp2);
            }
            return i + i3 + (this.zzbxK.length * 1);
        }
    }

    public static final class zzg extends zzbyd<zzg> {
        private static volatile zzg[] zzbxL;
        public String name;
        public String zzaGV;
        public Float zzbwh;
        public Double zzbwi;
        public Long zzbxM;
        public Long zzbxf;

        public zzg() {
            zzNL();
        }

        public static zzg[] zzNK() {
            if (zzbxL == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzbxL == null) {
                        zzbxL = new zzg[0];
                    }
                }
            }
            return zzbxL;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zzg)) {
                return false;
            }
            zzg zzg = (zzg) obj;
            if (this.zzbxM == null) {
                if (zzg.zzbxM != null) {
                    return false;
                }
            } else if (!this.zzbxM.equals(zzg.zzbxM)) {
                return false;
            }
            if (this.name == null) {
                if (zzg.name != null) {
                    return false;
                }
            } else if (!this.name.equals(zzg.name)) {
                return false;
            }
            if (this.zzaGV == null) {
                if (zzg.zzaGV != null) {
                    return false;
                }
            } else if (!this.zzaGV.equals(zzg.zzaGV)) {
                return false;
            }
            if (this.zzbxf == null) {
                if (zzg.zzbxf != null) {
                    return false;
                }
            } else if (!this.zzbxf.equals(zzg.zzbxf)) {
                return false;
            }
            if (this.zzbwh == null) {
                if (zzg.zzbwh != null) {
                    return false;
                }
            } else if (!this.zzbwh.equals(zzg.zzbwh)) {
                return false;
            }
            if (this.zzbwi == null) {
                if (zzg.zzbwi != null) {
                    return false;
                }
            } else if (!this.zzbwi.equals(zzg.zzbwi)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zzg.zzcwC == null || zzg.zzcwC.isEmpty() : this.zzcwC.equals(zzg.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbwi == null ? 0 : this.zzbwi.hashCode()) + (((this.zzbwh == null ? 0 : this.zzbwh.hashCode()) + (((this.zzbxf == null ? 0 : this.zzbxf.hashCode()) + (((this.zzaGV == null ? 0 : this.zzaGV.hashCode()) + (((this.name == null ? 0 : this.name.hashCode()) + (((this.zzbxM == null ? 0 : this.zzbxM.hashCode()) + ((getClass().getName().hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zzg zzNL() {
            this.zzbxM = null;
            this.name = null;
            this.zzaGV = null;
            this.zzbxf = null;
            this.zzbwh = null;
            this.zzbwi = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzV */
        public zzg zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbxM = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 18:
                        this.name = zzbyb.readString();
                        break;
                    case 26:
                        this.zzaGV = zzbyb.readString();
                        break;
                    case 32:
                        this.zzbxf = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 45:
                        this.zzbwh = Float.valueOf(zzbyb.readFloat());
                        break;
                    case 49:
                        this.zzbwi = Double.valueOf(zzbyb.readDouble());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbxM != null) {
                zzbyc.zzb(1, this.zzbxM.longValue());
            }
            if (this.name != null) {
                zzbyc.zzq(2, this.name);
            }
            if (this.zzaGV != null) {
                zzbyc.zzq(3, this.zzaGV);
            }
            if (this.zzbxf != null) {
                zzbyc.zzb(4, this.zzbxf.longValue());
            }
            if (this.zzbwh != null) {
                zzbyc.zzc(5, this.zzbwh.floatValue());
            }
            if (this.zzbwi != null) {
                zzbyc.zza(6, this.zzbwi.doubleValue());
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbxM != null) {
                zzu += zzbyc.zzf(1, this.zzbxM.longValue());
            }
            if (this.name != null) {
                zzu += zzbyc.zzr(2, this.name);
            }
            if (this.zzaGV != null) {
                zzu += zzbyc.zzr(3, this.zzaGV);
            }
            if (this.zzbxf != null) {
                zzu += zzbyc.zzf(4, this.zzbxf.longValue());
            }
            if (this.zzbwh != null) {
                zzu += zzbyc.zzd(5, this.zzbwh.floatValue());
            }
            return this.zzbwi != null ? zzu + zzbyc.zzb(6, this.zzbwi.doubleValue()) : zzu;
        }
    }
}
