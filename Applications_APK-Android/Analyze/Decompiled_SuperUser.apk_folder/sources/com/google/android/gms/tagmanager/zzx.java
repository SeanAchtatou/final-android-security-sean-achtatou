package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.text.TextUtils;
import com.duapps.ad.AdError;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzi;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class zzx implements DataLayer.zzc {
    /* access modifiers changed from: private */
    public static final String zzbFO = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", "datalayer", "ID", "key", FirebaseAnalytics.Param.VALUE, "expires");
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Executor zzbFP;
    private zza zzbFQ;
    private int zzbFR;
    private zze zzuP;

    class zza extends SQLiteOpenHelper {
        zza(Context context, String str) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x004d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean zza(java.lang.String r11, android.database.sqlite.SQLiteDatabase r12) {
            /*
                r10 = this;
                r8 = 0
                r9 = 0
                java.lang.String r1 = "SQLITE_MASTER"
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                r0 = 0
                java.lang.String r3 = "name"
                r2[r0] = r3     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                java.lang.String r3 = "name=?"
                r0 = 1
                java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                r0 = 0
                r4[r0] = r11     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                r5 = 0
                r6 = 0
                r7 = 0
                r0 = r12
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0026, all -> 0x0051 }
                boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0056, all -> 0x0053 }
                if (r1 == 0) goto L_0x0025
                r1.close()
            L_0x0025:
                return r0
            L_0x0026:
                r0 = move-exception
                r0 = r9
            L_0x0028:
                java.lang.String r2 = "Error querying for table "
                java.lang.String r1 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0048 }
                int r3 = r1.length()     // Catch:{ all -> 0x0048 }
                if (r3 == 0) goto L_0x0042
                java.lang.String r1 = r2.concat(r1)     // Catch:{ all -> 0x0048 }
            L_0x0038:
                com.google.android.gms.tagmanager.zzbo.zzbh(r1)     // Catch:{ all -> 0x0048 }
                if (r0 == 0) goto L_0x0040
                r0.close()
            L_0x0040:
                r0 = r8
                goto L_0x0025
            L_0x0042:
                java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x0048 }
                r1.<init>(r2)     // Catch:{ all -> 0x0048 }
                goto L_0x0038
            L_0x0048:
                r1 = move-exception
                r9 = r0
                r0 = r1
            L_0x004b:
                if (r9 == 0) goto L_0x0050
                r9.close()
            L_0x0050:
                throw r0
            L_0x0051:
                r0 = move-exception
                goto L_0x004b
            L_0x0053:
                r0 = move-exception
                r9 = r1
                goto L_0x004b
            L_0x0056:
                r0 = move-exception
                r0 = r1
                goto L_0x0028
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzx.zza.zza(java.lang.String, android.database.sqlite.SQLiteDatabase):boolean");
        }

        /* JADX INFO: finally extract failed */
        private void zzc(SQLiteDatabase sQLiteDatabase) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM datalayer WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("key") || !hashSet.remove(FirebaseAnalytics.Param.VALUE) || !hashSet.remove("ID") || !hashSet.remove("expires")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = super.getWritableDatabase();
            } catch (SQLiteException e2) {
                zzx.this.mContext.getDatabasePath("google_tagmanager.db").delete();
            }
            return sQLiteDatabase == null ? super.getWritableDatabase() : sQLiteDatabase;
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            zzan.zzca(sQLiteDatabase.getPath());
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (Build.VERSION.SDK_INT < 15) {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!zza("datalayer", sQLiteDatabase)) {
                sQLiteDatabase.execSQL(zzx.zzbFO);
            } else {
                zzc(sQLiteDatabase);
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    private static class zzb {
        final String zzAX;
        final byte[] zzbFX;

        zzb(String str, byte[] bArr) {
            this.zzAX = str;
            this.zzbFX = bArr;
        }

        public String toString() {
            String str = this.zzAX;
            return new StringBuilder(String.valueOf(str).length() + 54).append("KeyAndSerialized: key = ").append(str).append(" serialized hash = ").append(Arrays.hashCode(this.zzbFX)).toString();
        }
    }

    public zzx(Context context) {
        this(context, zzi.zzzc(), "google_tagmanager.db", AdError.SERVER_ERROR_CODE, Executors.newSingleThreadExecutor());
    }

    zzx(Context context, zze zze, String str, int i, Executor executor) {
        this.mContext = context;
        this.zzuP = zze;
        this.zzbFR = i;
        this.zzbFP = executor;
        this.zzbFQ = new zza(this.mContext, str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x002c A[SYNTHETIC, Splitter:B:18:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] zzK(java.lang.Object r6) {
        /*
            r5 = this;
            r0 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0019, all -> 0x0026 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0019, all -> 0x0026 }
            r1.writeObject(r6)     // Catch:{ IOException -> 0x0037, all -> 0x0035 }
            byte[] r0 = r2.toByteArray()     // Catch:{ IOException -> 0x0037, all -> 0x0035 }
            r1.close()     // Catch:{ IOException -> 0x0039 }
            r2.close()     // Catch:{ IOException -> 0x0039 }
        L_0x0018:
            return r0
        L_0x0019:
            r1 = move-exception
            r1 = r0
        L_0x001b:
            if (r1 == 0) goto L_0x0020
            r1.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0020:
            r2.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x0018
        L_0x0024:
            r1 = move-exception
            goto L_0x0018
        L_0x0026:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0033 }
        L_0x002f:
            r2.close()     // Catch:{ IOException -> 0x0033 }
        L_0x0032:
            throw r0
        L_0x0033:
            r1 = move-exception
            goto L_0x0032
        L_0x0035:
            r0 = move-exception
            goto L_0x002a
        L_0x0037:
            r3 = move-exception
            goto L_0x001b
        L_0x0039:
            r1 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzx.zzK(java.lang.Object):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0027 A[SYNTHETIC, Splitter:B:18:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0036 A[SYNTHETIC, Splitter:B:25:0x0036] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object zzL(byte[] r6) {
        /*
            r5 = this;
            r0 = 0
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r6)
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0016, ClassNotFoundException -> 0x0023, all -> 0x0030 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0016, ClassNotFoundException -> 0x0023, all -> 0x0030 }
            java.lang.Object r0 = r1.readObject()     // Catch:{ IOException -> 0x0043, ClassNotFoundException -> 0x0041, all -> 0x003f }
            r1.close()     // Catch:{ IOException -> 0x0045 }
            r2.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0015:
            return r0
        L_0x0016:
            r1 = move-exception
            r1 = r0
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0021 }
        L_0x001d:
            r2.close()     // Catch:{ IOException -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            goto L_0x0015
        L_0x0023:
            r1 = move-exception
            r1 = r0
        L_0x0025:
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x002e }
        L_0x002a:
            r2.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0015
        L_0x002e:
            r1 = move-exception
            goto L_0x0015
        L_0x0030:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x0039:
            r2.close()     // Catch:{ IOException -> 0x003d }
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            goto L_0x003c
        L_0x003f:
            r0 = move-exception
            goto L_0x0034
        L_0x0041:
            r3 = move-exception
            goto L_0x0025
        L_0x0043:
            r3 = move-exception
            goto L_0x0018
        L_0x0045:
            r1 = move-exception
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzx.zzL(byte[]):java.lang.Object");
    }

    private List<DataLayer.zza> zzN(List<zzb> list) {
        ArrayList arrayList = new ArrayList();
        for (zzb next : list) {
            arrayList.add(new DataLayer.zza(next.zzAX, zzL(next.zzbFX)));
        }
        return arrayList;
    }

    private List<zzb> zzO(List<DataLayer.zza> list) {
        ArrayList arrayList = new ArrayList();
        for (DataLayer.zza next : list) {
            arrayList.add(new zzb(next.zzAX, zzK(next.mValue)));
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public List<DataLayer.zza> zzQC() {
        try {
            zzaA(this.zzuP.currentTimeMillis());
            return zzN(zzQD());
        } finally {
            zzQF();
        }
    }

    /* JADX INFO: finally extract failed */
    private List<zzb> zzQD() {
        SQLiteDatabase zzhe = zzhe("Error opening database for loadSerialized.");
        ArrayList arrayList = new ArrayList();
        if (zzhe == null) {
            return arrayList;
        }
        Cursor query = zzhe.query("datalayer", new String[]{"key", FirebaseAnalytics.Param.VALUE}, null, null, null, null, "ID", null);
        while (query.moveToNext()) {
            try {
                arrayList.add(new zzb(query.getString(0), query.getBlob(1)));
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        return arrayList;
    }

    private int zzQE() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase zzhe = zzhe("Error opening database for getNumStoredEntries.");
        if (zzhe != null) {
            try {
                Cursor rawQuery = zzhe.rawQuery("SELECT COUNT(*) from datalayer", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e2) {
                zzbo.zzbh("Error getting numStoredEntries");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    private void zzQF() {
        try {
            this.zzbFQ.close();
        } catch (SQLiteException e2) {
        }
    }

    private void zzaA(long j) {
        SQLiteDatabase zzhe = zzhe("Error opening database for deleteOlderThan.");
        if (zzhe != null) {
            try {
                zzbo.v(new StringBuilder(33).append("Deleted ").append(zzhe.delete("datalayer", "expires <= ?", new String[]{Long.toString(j)})).append(" expired items").toString());
            } catch (SQLiteException e2) {
                zzbo.zzbh("Error deleting old entries.");
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void zzb(List<zzb> list, long j) {
        try {
            long currentTimeMillis = this.zzuP.currentTimeMillis();
            zzaA(currentTimeMillis);
            zznA(list.size());
            zzc(list, currentTimeMillis + j);
            zzQF();
        } catch (Throwable th) {
            zzQF();
            throw th;
        }
    }

    private void zzc(List<zzb> list, long j) {
        SQLiteDatabase zzhe = zzhe("Error opening database for writeEntryToDatabase.");
        if (zzhe != null) {
            for (zzb next : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("expires", Long.valueOf(j));
                contentValues.put("key", next.zzAX);
                contentValues.put(FirebaseAnalytics.Param.VALUE, next.zzbFX);
                zzhe.insert("datalayer", null, contentValues);
            }
        }
    }

    private void zzg(String[] strArr) {
        SQLiteDatabase zzhe;
        if (strArr != null && strArr.length != 0 && (zzhe = zzhe("Error opening database for deleteEntries.")) != null) {
            try {
                zzhe.delete("datalayer", String.format("%s in (%s)", "ID", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))), strArr);
            } catch (SQLiteException e2) {
                String valueOf = String.valueOf(Arrays.toString(strArr));
                zzbo.zzbh(valueOf.length() != 0 ? "Error deleting entries ".concat(valueOf) : new String("Error deleting entries "));
            }
        }
    }

    /* access modifiers changed from: private */
    public void zzhd(String str) {
        SQLiteDatabase zzhe = zzhe("Error opening database for clearKeysWithPrefix.");
        if (zzhe != null) {
            try {
                zzbo.v(new StringBuilder(25).append("Cleared ").append(zzhe.delete("datalayer", "key = ? OR key LIKE ?", new String[]{str, String.valueOf(str).concat(".%")})).append(" items").toString());
            } catch (SQLiteException e2) {
                String valueOf = String.valueOf(e2);
                zzbo.zzbh(new StringBuilder(String.valueOf(str).length() + 44 + String.valueOf(valueOf).length()).append("Error deleting entries with key prefix: ").append(str).append(" (").append(valueOf).append(").").toString());
            } finally {
                zzQF();
            }
        }
    }

    private SQLiteDatabase zzhe(String str) {
        try {
            return this.zzbFQ.getWritableDatabase();
        } catch (SQLiteException e2) {
            zzbo.zzbh(str);
            return null;
        }
    }

    private void zznA(int i) {
        int zzQE = (zzQE() - this.zzbFR) + i;
        if (zzQE > 0) {
            List<String> zznB = zznB(zzQE);
            zzbo.zzbg(new StringBuilder(64).append("DataLayer store full, deleting ").append(zznB.size()).append(" entries to make room.").toString());
            zzg((String[]) zznB.toArray(new String[0]));
        }
    }

    private List<String> zznB(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            zzbo.zzbh("Invalid maxEntries specified. Skipping.");
            return arrayList;
        }
        SQLiteDatabase zzhe = zzhe("Error opening database for peekEntryIds.");
        if (zzhe == null) {
            return arrayList;
        }
        try {
            cursor = zzhe.query("datalayer", new String[]{"ID"}, null, null, null, null, String.format("%s ASC", "ID"), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    String valueOf = String.valueOf(e.getMessage());
                    zzbo.zzbh(valueOf.length() != 0 ? "Error in peekEntries fetching entryIds: ".concat(valueOf) : new String("Error in peekEntries fetching entryIds: "));
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    public void zza(final DataLayer.zzc.zza zza2) {
        this.zzbFP.execute(new Runnable() {
            public void run() {
                zza2.zzM(zzx.this.zzQC());
            }
        });
    }

    public void zza(List<DataLayer.zza> list, final long j) {
        final List<zzb> zzO = zzO(list);
        this.zzbFP.execute(new Runnable() {
            public void run() {
                zzx.this.zzb(zzO, j);
            }
        });
    }

    public void zzhc(final String str) {
        this.zzbFP.execute(new Runnable() {
            public void run() {
                zzx.this.zzhd(str);
            }
        });
    }
}
