package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import io.fabric.sdk.android.services.common.a;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

class zzde implements zzad {
    private final Context mContext;
    private final String zzIA;
    private final zzb zzbIs;
    private final zza zzbIt;

    public interface zza {
        void zza(zzas zzas);

        void zzb(zzas zzas);

        void zzc(zzas zzas);
    }

    interface zzb {
        HttpURLConnection zzd(URL url);
    }

    zzde(Context context, zza zza2) {
        this(new zzb() {
            public HttpURLConnection zzd(URL url) {
                return (HttpURLConnection) url.openConnection();
            }
        }, context, zza2);
    }

    zzde(zzb zzb2, Context context, zza zza2) {
        this.zzbIs = zzb2;
        this.mContext = context.getApplicationContext();
        this.zzbIt = zza2;
        this.zzIA = zza("GoogleTagManager", "4.00", Build.VERSION.RELEASE, zzc(Locale.getDefault()), Build.MODEL, Build.ID);
    }

    static String zzc(Locale locale) {
        if (locale == null || locale.getLanguage() == null || locale.getLanguage().length() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(locale.getLanguage().toLowerCase());
        if (!(locale.getCountry() == null || locale.getCountry().length() == 0)) {
            sb.append("-").append(locale.getCountry().toLowerCase());
        }
        return sb.toString();
    }

    public void zzP(List<zzas> list) {
        IOException iOException;
        boolean z;
        boolean z2;
        boolean z3;
        InputStream inputStream;
        Throwable th;
        int min = Math.min(list.size(), 40);
        boolean z4 = true;
        int i = 0;
        while (i < min) {
            zzas zzas = list.get(i);
            URL zzd = zzd(zzas);
            if (zzd == null) {
                zzbo.zzbh("No destination: discarding hit.");
                this.zzbIt.zzb(zzas);
                z2 = z4;
            } else {
                try {
                    HttpURLConnection zzd2 = this.zzbIs.zzd(zzd);
                    if (z4) {
                        try {
                            zzbt.zzcc(this.mContext);
                            z4 = false;
                        } catch (Throwable th2) {
                            Throwable th3 = th2;
                            inputStream = null;
                            z3 = z4;
                            th = th3;
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                } catch (IOException e2) {
                                    z = z3;
                                    iOException = e2;
                                    String valueOf = String.valueOf(iOException.getClass().getSimpleName());
                                    zzbo.zzbh(valueOf.length() != 0 ? "Exception sending hit: ".concat(valueOf) : new String("Exception sending hit: "));
                                    zzbo.zzbh(iOException.getMessage());
                                    this.zzbIt.zzc(zzas);
                                    z2 = z;
                                    i++;
                                    z4 = z2;
                                }
                            }
                            zzd2.disconnect();
                            throw th;
                        }
                    }
                    zzd2.setRequestProperty(a.HEADER_USER_AGENT, this.zzIA);
                    int responseCode = zzd2.getResponseCode();
                    InputStream inputStream2 = zzd2.getInputStream();
                    if (responseCode != 200) {
                        try {
                            zzbo.zzbh(new StringBuilder(25).append("Bad response: ").append(responseCode).toString());
                            this.zzbIt.zzc(zzas);
                        } catch (Throwable th4) {
                            Throwable th5 = th4;
                            inputStream = inputStream2;
                            z3 = z4;
                            th = th5;
                        }
                    } else {
                        this.zzbIt.zza(zzas);
                    }
                    if (inputStream2 != null) {
                        inputStream2.close();
                    }
                    zzd2.disconnect();
                    z2 = z4;
                } catch (IOException e3) {
                    iOException = e3;
                    z = z4;
                }
            }
            i++;
            z4 = z2;
        }
    }

    public boolean zzQH() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        zzbo.v("...no network connectivity");
        return false;
    }

    /* access modifiers changed from: package-private */
    public String zza(String str, String str2, String str3, String str4, String str5, String str6) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", str, str2, str3, str4, str5, str6);
    }

    /* access modifiers changed from: package-private */
    public URL zzd(zzas zzas) {
        try {
            return new URL(zzas.zzQR());
        } catch (MalformedURLException e2) {
            zzbo.e("Error trying to parse the GTM url.");
            return null;
        }
    }
}
