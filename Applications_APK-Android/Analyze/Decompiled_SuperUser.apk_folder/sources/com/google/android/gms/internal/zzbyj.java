package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzbyj {
    protected volatile int zzcwL = -1;

    public static final <T extends zzbyj> T zza(zzbyj zzbyj, byte[] bArr) {
        return zzb(zzbyj, bArr, 0, bArr.length);
    }

    public static final void zza(zzbyj zzbyj, byte[] bArr, int i, int i2) {
        try {
            zzbyc zzc = zzbyc.zzc(bArr, i, i2);
            zzbyj.zza(zzc);
            zzc.zzafo();
        } catch (IOException e2) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e2);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.google.android.gms.internal.zzbyj> T zzb(T r2, byte[] r3, int r4, int r5) {
        /*
            com.google.android.gms.internal.zzbyb r0 = com.google.android.gms.internal.zzbyb.zzb(r3, r4, r5)     // Catch:{ zzbyi -> 0x000c, IOException -> 0x000e }
            r2.zzb(r0)     // Catch:{ zzbyi -> 0x000c, IOException -> 0x000e }
            r1 = 0
            r0.zzrc(r1)     // Catch:{ zzbyi -> 0x000c, IOException -> 0x000e }
            return r2
        L_0x000c:
            r0 = move-exception
            throw r0
        L_0x000e:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Reading from a byte array threw an IOException (should never happen)."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbyj.zzb(com.google.android.gms.internal.zzbyj, byte[], int, int):com.google.android.gms.internal.zzbyj");
    }

    public static final byte[] zzf(zzbyj zzbyj) {
        byte[] bArr = new byte[zzbyj.zzafB()];
        zza(zzbyj, bArr, 0, bArr.length);
        return bArr;
    }

    public String toString() {
        return zzbyk.zzg(this);
    }

    public void zza(zzbyc zzbyc) {
    }

    public int zzafA() {
        if (this.zzcwL < 0) {
            zzafB();
        }
        return this.zzcwL;
    }

    public int zzafB() {
        int zzu = zzu();
        this.zzcwL = zzu;
        return zzu;
    }

    /* renamed from: zzafq */
    public zzbyj clone() {
        return (zzbyj) super.clone();
    }

    public abstract zzbyj zzb(zzbyb zzbyb);

    /* access modifiers changed from: protected */
    public int zzu() {
        return 0;
    }
}
