package com.google.android.gms.internal;

public interface zzbyp {

    public static final class zza extends zzbyd<zza> {
        private static volatile zza[] zzcxF;
        public String zzcxG;

        public zza() {
            zzafR();
        }

        public static zza[] zzafQ() {
            if (zzcxF == null) {
                synchronized (zzbyh.zzcwK) {
                    if (zzcxF == null) {
                        zzcxF = new zza[0];
                    }
                }
            }
            return zzcxF;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzcxG != null && !this.zzcxG.equals("")) {
                zzbyc.zzq(1, this.zzcxG);
            }
            super.zza(zzbyc);
        }

        public zza zzafR() {
            this.zzcxG = "";
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzbc */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzcxG = zzbyb.readString();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            return (this.zzcxG == null || this.zzcxG.equals("")) ? zzu : zzu + zzbyc.zzr(1, this.zzcxG);
        }
    }

    public static final class zzb extends zzbyd<zzb> {
        public String zzcxG;
        public String zzcxH;
        public long zzcxI;
        public String zzcxJ;
        public int zzcxK;
        public int zzcxL;
        public String zzcxM;
        public String zzcxN;
        public String zzcxO;
        public String zzcxP;
        public String zzcxQ;
        public int zzcxR;
        public zza[] zzcxS;

        public zzb() {
            zzafS();
        }

        public static zzb zzal(byte[] bArr) {
            return (zzb) zzbyj.zza(new zzb(), bArr);
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzcxG != null && !this.zzcxG.equals("")) {
                zzbyc.zzq(1, this.zzcxG);
            }
            if (this.zzcxH != null && !this.zzcxH.equals("")) {
                zzbyc.zzq(2, this.zzcxH);
            }
            if (this.zzcxI != 0) {
                zzbyc.zzb(3, this.zzcxI);
            }
            if (this.zzcxJ != null && !this.zzcxJ.equals("")) {
                zzbyc.zzq(4, this.zzcxJ);
            }
            if (this.zzcxK != 0) {
                zzbyc.zzJ(5, this.zzcxK);
            }
            if (this.zzcxL != 0) {
                zzbyc.zzJ(6, this.zzcxL);
            }
            if (this.zzcxM != null && !this.zzcxM.equals("")) {
                zzbyc.zzq(7, this.zzcxM);
            }
            if (this.zzcxN != null && !this.zzcxN.equals("")) {
                zzbyc.zzq(8, this.zzcxN);
            }
            if (this.zzcxO != null && !this.zzcxO.equals("")) {
                zzbyc.zzq(9, this.zzcxO);
            }
            if (this.zzcxP != null && !this.zzcxP.equals("")) {
                zzbyc.zzq(10, this.zzcxP);
            }
            if (this.zzcxQ != null && !this.zzcxQ.equals("")) {
                zzbyc.zzq(11, this.zzcxQ);
            }
            if (this.zzcxR != 0) {
                zzbyc.zzJ(12, this.zzcxR);
            }
            if (this.zzcxS != null && this.zzcxS.length > 0) {
                for (zza zza : this.zzcxS) {
                    if (zza != null) {
                        zzbyc.zza(13, zza);
                    }
                }
            }
            super.zza(zzbyc);
        }

        public zzb zzafS() {
            this.zzcxG = "";
            this.zzcxH = "";
            this.zzcxI = 0;
            this.zzcxJ = "";
            this.zzcxK = 0;
            this.zzcxL = 0;
            this.zzcxM = "";
            this.zzcxN = "";
            this.zzcxO = "";
            this.zzcxP = "";
            this.zzcxQ = "";
            this.zzcxR = 0;
            this.zzcxS = zza.zzafQ();
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzbd */
        public zzb zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzcxG = zzbyb.readString();
                        break;
                    case 18:
                        this.zzcxH = zzbyb.readString();
                        break;
                    case 24:
                        this.zzcxI = zzbyb.zzaeZ();
                        break;
                    case 34:
                        this.zzcxJ = zzbyb.readString();
                        break;
                    case 40:
                        this.zzcxK = zzbyb.zzafa();
                        break;
                    case 48:
                        this.zzcxL = zzbyb.zzafa();
                        break;
                    case 58:
                        this.zzcxM = zzbyb.readString();
                        break;
                    case 66:
                        this.zzcxN = zzbyb.readString();
                        break;
                    case 74:
                        this.zzcxO = zzbyb.readString();
                        break;
                    case 82:
                        this.zzcxP = zzbyb.readString();
                        break;
                    case 90:
                        this.zzcxQ = zzbyb.readString();
                        break;
                    case 96:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 0:
                            case 1:
                            case 2:
                                this.zzcxR = zzafa;
                                continue;
                        }
                    case 106:
                        int zzb = zzbym.zzb(zzbyb, 106);
                        int length = this.zzcxS == null ? 0 : this.zzcxS.length;
                        zza[] zzaArr = new zza[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzcxS, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new zza();
                            zzbyb.zza(zzaArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzaArr[length] = new zza();
                        zzbyb.zza(zzaArr[length]);
                        this.zzcxS = zzaArr;
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzcxG != null && !this.zzcxG.equals("")) {
                zzu += zzbyc.zzr(1, this.zzcxG);
            }
            if (this.zzcxH != null && !this.zzcxH.equals("")) {
                zzu += zzbyc.zzr(2, this.zzcxH);
            }
            if (this.zzcxI != 0) {
                zzu += zzbyc.zzf(3, this.zzcxI);
            }
            if (this.zzcxJ != null && !this.zzcxJ.equals("")) {
                zzu += zzbyc.zzr(4, this.zzcxJ);
            }
            if (this.zzcxK != 0) {
                zzu += zzbyc.zzL(5, this.zzcxK);
            }
            if (this.zzcxL != 0) {
                zzu += zzbyc.zzL(6, this.zzcxL);
            }
            if (this.zzcxM != null && !this.zzcxM.equals("")) {
                zzu += zzbyc.zzr(7, this.zzcxM);
            }
            if (this.zzcxN != null && !this.zzcxN.equals("")) {
                zzu += zzbyc.zzr(8, this.zzcxN);
            }
            if (this.zzcxO != null && !this.zzcxO.equals("")) {
                zzu += zzbyc.zzr(9, this.zzcxO);
            }
            if (this.zzcxP != null && !this.zzcxP.equals("")) {
                zzu += zzbyc.zzr(10, this.zzcxP);
            }
            if (this.zzcxQ != null && !this.zzcxQ.equals("")) {
                zzu += zzbyc.zzr(11, this.zzcxQ);
            }
            if (this.zzcxR != 0) {
                zzu += zzbyc.zzL(12, this.zzcxR);
            }
            if (this.zzcxS == null || this.zzcxS.length <= 0) {
                return zzu;
            }
            int i = zzu;
            for (zza zza : this.zzcxS) {
                if (zza != null) {
                    i += zzbyc.zzc(13, zza);
                }
            }
            return i;
        }
    }
}
