package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzw extends zzam {
    private static final String ID = zzah.CUSTOM_VAR.toString();
    private static final String NAME = zzai.NAME.toString();
    private static final String zzbFN = zzai.DEFAULT_VALUE.toString();
    private final DataLayer zzbEY;

    public zzw(DataLayer dataLayer) {
        super(ID, NAME);
        this.zzbEY = dataLayer;
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        Object obj = this.zzbEY.get(zzdl.zze(map.get(NAME)));
        if (obj != null) {
            return zzdl.zzS(obj);
        }
        zzak.zza zza = map.get(zzbFN);
        return zza != null ? zza : zzdl.zzRT();
    }
}
