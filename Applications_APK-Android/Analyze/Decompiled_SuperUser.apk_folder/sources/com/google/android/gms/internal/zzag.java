package com.google.android.gms.internal;

import com.duapps.ad.AdError;
import com.lody.virtual.helper.utils.FileUtils;

public interface zzag {

    public static final class zza extends zzbyd<zza> {
        public String zzaN = null;
        public String zzaP = null;
        public String zzaQ = null;
        public String zzaR = null;
        public Long zzbA = null;
        public Long zzbB = null;
        public Long zzbC = null;
        public zzb zzbD;
        public Long zzbE = null;
        public Long zzbF = null;
        public Long zzbG = null;
        public Long zzbH = null;
        public Long zzbI = null;
        public Long zzbJ = null;
        public Integer zzbK = null;
        public Integer zzbL = null;
        public Long zzbM = null;
        public Long zzbN = null;
        public Long zzbO = null;
        public Long zzbP = null;
        public Long zzbQ = null;
        public Integer zzbR = null;
        public C0074zza zzbS;
        public C0074zza[] zzbT = C0074zza.zzv();
        public zzb zzbU;
        public Long zzbV = null;
        public String zzbW = null;
        public Integer zzbX = null;
        public Boolean zzbY = null;
        public String zzbZ = null;
        public String zzba = null;
        public String zzbb = null;
        public Long zzbc = null;
        public Long zzbd = null;
        public Long zzbe = null;
        public Long zzbf = null;
        public Long zzbg = null;
        public Long zzbh = null;
        public Long zzbi = null;
        public Long zzbj = null;
        public Long zzbk = null;
        public Long zzbl = null;
        public String zzbm = null;
        public Long zzbn = null;
        public Long zzbo = null;
        public Long zzbp = null;
        public Long zzbq = null;
        public Long zzbr = null;
        public Long zzbs = null;
        public Long zzbt = null;
        public Long zzbu = null;
        public Long zzbv = null;
        public String zzbw = null;
        public Long zzbx = null;
        public Long zzby = null;
        public Long zzbz = null;
        public Long zzca = null;
        public zze zzcb;

        /* renamed from: com.google.android.gms.internal.zzag$zza$zza  reason: collision with other inner class name */
        public static final class C0074zza extends zzbyd<C0074zza> {
            private static volatile C0074zza[] zzcc;
            public Long zzbn = null;
            public Long zzbo = null;
            public Long zzcd = null;
            public Long zzce = null;
            public Long zzcf = null;
            public Long zzcg = null;
            public Integer zzch = null;
            public Long zzci = null;
            public Long zzcj = null;
            public Long zzck = null;
            public Integer zzcl = null;
            public Long zzcm = null;

            public C0074zza() {
                this.zzcwL = -1;
            }

            public static C0074zza[] zzv() {
                if (zzcc == null) {
                    synchronized (zzbyh.zzcwK) {
                        if (zzcc == null) {
                            zzcc = new C0074zza[0];
                        }
                    }
                }
                return zzcc;
            }

            public void zza(zzbyc zzbyc) {
                if (this.zzbn != null) {
                    zzbyc.zzb(1, this.zzbn.longValue());
                }
                if (this.zzbo != null) {
                    zzbyc.zzb(2, this.zzbo.longValue());
                }
                if (this.zzcd != null) {
                    zzbyc.zzb(3, this.zzcd.longValue());
                }
                if (this.zzce != null) {
                    zzbyc.zzb(4, this.zzce.longValue());
                }
                if (this.zzcf != null) {
                    zzbyc.zzb(5, this.zzcf.longValue());
                }
                if (this.zzcg != null) {
                    zzbyc.zzb(6, this.zzcg.longValue());
                }
                if (this.zzch != null) {
                    zzbyc.zzJ(7, this.zzch.intValue());
                }
                if (this.zzci != null) {
                    zzbyc.zzb(8, this.zzci.longValue());
                }
                if (this.zzcj != null) {
                    zzbyc.zzb(9, this.zzcj.longValue());
                }
                if (this.zzck != null) {
                    zzbyc.zzb(10, this.zzck.longValue());
                }
                if (this.zzcl != null) {
                    zzbyc.zzJ(11, this.zzcl.intValue());
                }
                if (this.zzcm != null) {
                    zzbyc.zzb(12, this.zzcm.longValue());
                }
                super.zza(zzbyc);
            }

            /* renamed from: zzg */
            public C0074zza zzb(zzbyb zzbyb) {
                while (true) {
                    int zzaeW = zzbyb.zzaeW();
                    switch (zzaeW) {
                        case 0:
                            break;
                        case 8:
                            this.zzbn = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 16:
                            this.zzbo = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 24:
                            this.zzcd = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 32:
                            this.zzce = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 40:
                            this.zzcf = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 48:
                            this.zzcg = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 56:
                            int zzafa = zzbyb.zzafa();
                            switch (zzafa) {
                                case 0:
                                case 1:
                                case 2:
                                case AdError.NETWORK_ERROR_CODE:
                                    this.zzch = Integer.valueOf(zzafa);
                                    continue;
                            }
                        case 64:
                            this.zzci = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 72:
                            this.zzcj = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 80:
                            this.zzck = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 88:
                            int zzafa2 = zzbyb.zzafa();
                            switch (zzafa2) {
                                case 0:
                                case 1:
                                case 2:
                                case AdError.NETWORK_ERROR_CODE:
                                    this.zzcl = Integer.valueOf(zzafa2);
                                    continue;
                            }
                        case 96:
                            this.zzcm = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        default:
                            if (super.zza(zzbyb, zzaeW)) {
                                break;
                            } else {
                                break;
                            }
                    }
                }
                return this;
            }

            /* access modifiers changed from: protected */
            public int zzu() {
                int zzu = super.zzu();
                if (this.zzbn != null) {
                    zzu += zzbyc.zzf(1, this.zzbn.longValue());
                }
                if (this.zzbo != null) {
                    zzu += zzbyc.zzf(2, this.zzbo.longValue());
                }
                if (this.zzcd != null) {
                    zzu += zzbyc.zzf(3, this.zzcd.longValue());
                }
                if (this.zzce != null) {
                    zzu += zzbyc.zzf(4, this.zzce.longValue());
                }
                if (this.zzcf != null) {
                    zzu += zzbyc.zzf(5, this.zzcf.longValue());
                }
                if (this.zzcg != null) {
                    zzu += zzbyc.zzf(6, this.zzcg.longValue());
                }
                if (this.zzch != null) {
                    zzu += zzbyc.zzL(7, this.zzch.intValue());
                }
                if (this.zzci != null) {
                    zzu += zzbyc.zzf(8, this.zzci.longValue());
                }
                if (this.zzcj != null) {
                    zzu += zzbyc.zzf(9, this.zzcj.longValue());
                }
                if (this.zzck != null) {
                    zzu += zzbyc.zzf(10, this.zzck.longValue());
                }
                if (this.zzcl != null) {
                    zzu += zzbyc.zzL(11, this.zzcl.intValue());
                }
                return this.zzcm != null ? zzu + zzbyc.zzf(12, this.zzcm.longValue()) : zzu;
            }
        }

        public static final class zzb extends zzbyd<zzb> {
            public Long zzbP = null;
            public Long zzbQ = null;
            public Long zzcn = null;

            public zzb() {
                this.zzcwL = -1;
            }

            public void zza(zzbyc zzbyc) {
                if (this.zzbP != null) {
                    zzbyc.zzb(1, this.zzbP.longValue());
                }
                if (this.zzbQ != null) {
                    zzbyc.zzb(2, this.zzbQ.longValue());
                }
                if (this.zzcn != null) {
                    zzbyc.zzb(3, this.zzcn.longValue());
                }
                super.zza(zzbyc);
            }

            /* renamed from: zzh */
            public zzb zzb(zzbyb zzbyb) {
                while (true) {
                    int zzaeW = zzbyb.zzaeW();
                    switch (zzaeW) {
                        case 0:
                            break;
                        case 8:
                            this.zzbP = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 16:
                            this.zzbQ = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        case 24:
                            this.zzcn = Long.valueOf(zzbyb.zzaeZ());
                            break;
                        default:
                            if (super.zza(zzbyb, zzaeW)) {
                                break;
                            } else {
                                break;
                            }
                    }
                }
                return this;
            }

            /* access modifiers changed from: protected */
            public int zzu() {
                int zzu = super.zzu();
                if (this.zzbP != null) {
                    zzu += zzbyc.zzf(1, this.zzbP.longValue());
                }
                if (this.zzbQ != null) {
                    zzu += zzbyc.zzf(2, this.zzbQ.longValue());
                }
                return this.zzcn != null ? zzu + zzbyc.zzf(3, this.zzcn.longValue()) : zzu;
            }
        }

        public zza() {
            this.zzcwL = -1;
        }

        public static zza zzd(byte[] bArr) {
            return (zza) zzbyj.zza(new zza(), bArr);
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzbb != null) {
                zzbyc.zzq(1, this.zzbb);
            }
            if (this.zzba != null) {
                zzbyc.zzq(2, this.zzba);
            }
            if (this.zzbc != null) {
                zzbyc.zzb(3, this.zzbc.longValue());
            }
            if (this.zzbd != null) {
                zzbyc.zzb(4, this.zzbd.longValue());
            }
            if (this.zzbe != null) {
                zzbyc.zzb(5, this.zzbe.longValue());
            }
            if (this.zzbf != null) {
                zzbyc.zzb(6, this.zzbf.longValue());
            }
            if (this.zzbg != null) {
                zzbyc.zzb(7, this.zzbg.longValue());
            }
            if (this.zzbh != null) {
                zzbyc.zzb(8, this.zzbh.longValue());
            }
            if (this.zzbi != null) {
                zzbyc.zzb(9, this.zzbi.longValue());
            }
            if (this.zzbj != null) {
                zzbyc.zzb(10, this.zzbj.longValue());
            }
            if (this.zzbk != null) {
                zzbyc.zzb(11, this.zzbk.longValue());
            }
            if (this.zzbl != null) {
                zzbyc.zzb(12, this.zzbl.longValue());
            }
            if (this.zzbm != null) {
                zzbyc.zzq(13, this.zzbm);
            }
            if (this.zzbn != null) {
                zzbyc.zzb(14, this.zzbn.longValue());
            }
            if (this.zzbo != null) {
                zzbyc.zzb(15, this.zzbo.longValue());
            }
            if (this.zzbp != null) {
                zzbyc.zzb(16, this.zzbp.longValue());
            }
            if (this.zzbq != null) {
                zzbyc.zzb(17, this.zzbq.longValue());
            }
            if (this.zzbr != null) {
                zzbyc.zzb(18, this.zzbr.longValue());
            }
            if (this.zzbs != null) {
                zzbyc.zzb(19, this.zzbs.longValue());
            }
            if (this.zzbt != null) {
                zzbyc.zzb(20, this.zzbt.longValue());
            }
            if (this.zzbV != null) {
                zzbyc.zzb(21, this.zzbV.longValue());
            }
            if (this.zzbu != null) {
                zzbyc.zzb(22, this.zzbu.longValue());
            }
            if (this.zzbv != null) {
                zzbyc.zzb(23, this.zzbv.longValue());
            }
            if (this.zzbW != null) {
                zzbyc.zzq(24, this.zzbW);
            }
            if (this.zzca != null) {
                zzbyc.zzb(25, this.zzca.longValue());
            }
            if (this.zzbX != null) {
                zzbyc.zzJ(26, this.zzbX.intValue());
            }
            if (this.zzaN != null) {
                zzbyc.zzq(27, this.zzaN);
            }
            if (this.zzbY != null) {
                zzbyc.zzg(28, this.zzbY.booleanValue());
            }
            if (this.zzbw != null) {
                zzbyc.zzq(29, this.zzbw);
            }
            if (this.zzbZ != null) {
                zzbyc.zzq(30, this.zzbZ);
            }
            if (this.zzbx != null) {
                zzbyc.zzb(31, this.zzbx.longValue());
            }
            if (this.zzby != null) {
                zzbyc.zzb(32, this.zzby.longValue());
            }
            if (this.zzbz != null) {
                zzbyc.zzb(33, this.zzbz.longValue());
            }
            if (this.zzaP != null) {
                zzbyc.zzq(34, this.zzaP);
            }
            if (this.zzbA != null) {
                zzbyc.zzb(35, this.zzbA.longValue());
            }
            if (this.zzbB != null) {
                zzbyc.zzb(36, this.zzbB.longValue());
            }
            if (this.zzbC != null) {
                zzbyc.zzb(37, this.zzbC.longValue());
            }
            if (this.zzbD != null) {
                zzbyc.zza(38, this.zzbD);
            }
            if (this.zzbE != null) {
                zzbyc.zzb(39, this.zzbE.longValue());
            }
            if (this.zzbF != null) {
                zzbyc.zzb(40, this.zzbF.longValue());
            }
            if (this.zzbG != null) {
                zzbyc.zzb(41, this.zzbG.longValue());
            }
            if (this.zzbH != null) {
                zzbyc.zzb(42, this.zzbH.longValue());
            }
            if (this.zzbT != null && this.zzbT.length > 0) {
                for (C0074zza zza : this.zzbT) {
                    if (zza != null) {
                        zzbyc.zza(43, zza);
                    }
                }
            }
            if (this.zzbI != null) {
                zzbyc.zzb(44, this.zzbI.longValue());
            }
            if (this.zzbJ != null) {
                zzbyc.zzb(45, this.zzbJ.longValue());
            }
            if (this.zzaQ != null) {
                zzbyc.zzq(46, this.zzaQ);
            }
            if (this.zzaR != null) {
                zzbyc.zzq(47, this.zzaR);
            }
            if (this.zzbK != null) {
                zzbyc.zzJ(48, this.zzbK.intValue());
            }
            if (this.zzbL != null) {
                zzbyc.zzJ(49, this.zzbL.intValue());
            }
            if (this.zzbS != null) {
                zzbyc.zza(50, this.zzbS);
            }
            if (this.zzbM != null) {
                zzbyc.zzb(51, this.zzbM.longValue());
            }
            if (this.zzbN != null) {
                zzbyc.zzb(52, this.zzbN.longValue());
            }
            if (this.zzbO != null) {
                zzbyc.zzb(53, this.zzbO.longValue());
            }
            if (this.zzbP != null) {
                zzbyc.zzb(54, this.zzbP.longValue());
            }
            if (this.zzbQ != null) {
                zzbyc.zzb(55, this.zzbQ.longValue());
            }
            if (this.zzbR != null) {
                zzbyc.zzJ(56, this.zzbR.intValue());
            }
            if (this.zzbU != null) {
                zzbyc.zza(57, this.zzbU);
            }
            if (this.zzcb != null) {
                zzbyc.zza(201, this.zzcb);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzf */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzbb = zzbyb.readString();
                        break;
                    case 18:
                        this.zzba = zzbyb.readString();
                        break;
                    case 24:
                        this.zzbc = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 32:
                        this.zzbd = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 40:
                        this.zzbe = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 48:
                        this.zzbf = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 56:
                        this.zzbg = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 64:
                        this.zzbh = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 72:
                        this.zzbi = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 80:
                        this.zzbj = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 88:
                        this.zzbk = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 96:
                        this.zzbl = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 106:
                        this.zzbm = zzbyb.readString();
                        break;
                    case 112:
                        this.zzbn = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 120:
                        this.zzbo = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case FileUtils.FileMode.MODE_IWUSR /*128*/:
                        this.zzbp = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 136:
                        this.zzbq = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 144:
                        this.zzbr = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 152:
                        this.zzbs = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 160:
                        this.zzbt = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 168:
                        this.zzbV = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 176:
                        this.zzbu = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 184:
                        this.zzbv = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 194:
                        this.zzbW = zzbyb.readString();
                        break;
                    case 200:
                        this.zzca = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 208:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                this.zzbX = Integer.valueOf(zzafa);
                                continue;
                        }
                    case 218:
                        this.zzaN = zzbyb.readString();
                        break;
                    case 224:
                        this.zzbY = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 234:
                        this.zzbw = zzbyb.readString();
                        break;
                    case 242:
                        this.zzbZ = zzbyb.readString();
                        break;
                    case 248:
                        this.zzbx = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case FileUtils.FileMode.MODE_IRUSR /*256*/:
                        this.zzby = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 264:
                        this.zzbz = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 274:
                        this.zzaP = zzbyb.readString();
                        break;
                    case 280:
                        this.zzbA = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 288:
                        this.zzbB = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 296:
                        this.zzbC = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 306:
                        if (this.zzbD == null) {
                            this.zzbD = new zzb();
                        }
                        zzbyb.zza(this.zzbD);
                        break;
                    case 312:
                        this.zzbE = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 320:
                        this.zzbF = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 328:
                        this.zzbG = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 336:
                        this.zzbH = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 346:
                        int zzb2 = zzbym.zzb(zzbyb, 346);
                        int length = this.zzbT == null ? 0 : this.zzbT.length;
                        C0074zza[] zzaArr = new C0074zza[(zzb2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzbT, 0, zzaArr, 0, length);
                        }
                        while (length < zzaArr.length - 1) {
                            zzaArr[length] = new C0074zza();
                            zzbyb.zza(zzaArr[length]);
                            zzbyb.zzaeW();
                            length++;
                        }
                        zzaArr[length] = new C0074zza();
                        zzbyb.zza(zzaArr[length]);
                        this.zzbT = zzaArr;
                        break;
                    case 352:
                        this.zzbI = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 360:
                        this.zzbJ = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 370:
                        this.zzaQ = zzbyb.readString();
                        break;
                    case 378:
                        this.zzaR = zzbyb.readString();
                        break;
                    case 384:
                        int zzafa2 = zzbyb.zzafa();
                        switch (zzafa2) {
                            case 0:
                            case 1:
                            case 2:
                            case AdError.NETWORK_ERROR_CODE:
                                this.zzbK = Integer.valueOf(zzafa2);
                                continue;
                        }
                    case 392:
                        int zzafa3 = zzbyb.zzafa();
                        switch (zzafa3) {
                            case 0:
                            case 1:
                            case 2:
                            case AdError.NETWORK_ERROR_CODE:
                                this.zzbL = Integer.valueOf(zzafa3);
                                continue;
                        }
                    case 402:
                        if (this.zzbS == null) {
                            this.zzbS = new C0074zza();
                        }
                        zzbyb.zza(this.zzbS);
                        break;
                    case 408:
                        this.zzbM = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 416:
                        this.zzbN = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 424:
                        this.zzbO = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 432:
                        this.zzbP = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 440:
                        this.zzbQ = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 448:
                        int zzafa4 = zzbyb.zzafa();
                        switch (zzafa4) {
                            case 0:
                            case 1:
                            case 2:
                            case AdError.NETWORK_ERROR_CODE:
                                this.zzbR = Integer.valueOf(zzafa4);
                                continue;
                        }
                    case 458:
                        if (this.zzbU == null) {
                            this.zzbU = new zzb();
                        }
                        zzbyb.zza(this.zzbU);
                        break;
                    case 1610:
                        if (this.zzcb == null) {
                            this.zzcb = new zze();
                        }
                        zzbyb.zza(this.zzcb);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzbb != null) {
                zzu += zzbyc.zzr(1, this.zzbb);
            }
            if (this.zzba != null) {
                zzu += zzbyc.zzr(2, this.zzba);
            }
            if (this.zzbc != null) {
                zzu += zzbyc.zzf(3, this.zzbc.longValue());
            }
            if (this.zzbd != null) {
                zzu += zzbyc.zzf(4, this.zzbd.longValue());
            }
            if (this.zzbe != null) {
                zzu += zzbyc.zzf(5, this.zzbe.longValue());
            }
            if (this.zzbf != null) {
                zzu += zzbyc.zzf(6, this.zzbf.longValue());
            }
            if (this.zzbg != null) {
                zzu += zzbyc.zzf(7, this.zzbg.longValue());
            }
            if (this.zzbh != null) {
                zzu += zzbyc.zzf(8, this.zzbh.longValue());
            }
            if (this.zzbi != null) {
                zzu += zzbyc.zzf(9, this.zzbi.longValue());
            }
            if (this.zzbj != null) {
                zzu += zzbyc.zzf(10, this.zzbj.longValue());
            }
            if (this.zzbk != null) {
                zzu += zzbyc.zzf(11, this.zzbk.longValue());
            }
            if (this.zzbl != null) {
                zzu += zzbyc.zzf(12, this.zzbl.longValue());
            }
            if (this.zzbm != null) {
                zzu += zzbyc.zzr(13, this.zzbm);
            }
            if (this.zzbn != null) {
                zzu += zzbyc.zzf(14, this.zzbn.longValue());
            }
            if (this.zzbo != null) {
                zzu += zzbyc.zzf(15, this.zzbo.longValue());
            }
            if (this.zzbp != null) {
                zzu += zzbyc.zzf(16, this.zzbp.longValue());
            }
            if (this.zzbq != null) {
                zzu += zzbyc.zzf(17, this.zzbq.longValue());
            }
            if (this.zzbr != null) {
                zzu += zzbyc.zzf(18, this.zzbr.longValue());
            }
            if (this.zzbs != null) {
                zzu += zzbyc.zzf(19, this.zzbs.longValue());
            }
            if (this.zzbt != null) {
                zzu += zzbyc.zzf(20, this.zzbt.longValue());
            }
            if (this.zzbV != null) {
                zzu += zzbyc.zzf(21, this.zzbV.longValue());
            }
            if (this.zzbu != null) {
                zzu += zzbyc.zzf(22, this.zzbu.longValue());
            }
            if (this.zzbv != null) {
                zzu += zzbyc.zzf(23, this.zzbv.longValue());
            }
            if (this.zzbW != null) {
                zzu += zzbyc.zzr(24, this.zzbW);
            }
            if (this.zzca != null) {
                zzu += zzbyc.zzf(25, this.zzca.longValue());
            }
            if (this.zzbX != null) {
                zzu += zzbyc.zzL(26, this.zzbX.intValue());
            }
            if (this.zzaN != null) {
                zzu += zzbyc.zzr(27, this.zzaN);
            }
            if (this.zzbY != null) {
                zzu += zzbyc.zzh(28, this.zzbY.booleanValue());
            }
            if (this.zzbw != null) {
                zzu += zzbyc.zzr(29, this.zzbw);
            }
            if (this.zzbZ != null) {
                zzu += zzbyc.zzr(30, this.zzbZ);
            }
            if (this.zzbx != null) {
                zzu += zzbyc.zzf(31, this.zzbx.longValue());
            }
            if (this.zzby != null) {
                zzu += zzbyc.zzf(32, this.zzby.longValue());
            }
            if (this.zzbz != null) {
                zzu += zzbyc.zzf(33, this.zzbz.longValue());
            }
            if (this.zzaP != null) {
                zzu += zzbyc.zzr(34, this.zzaP);
            }
            if (this.zzbA != null) {
                zzu += zzbyc.zzf(35, this.zzbA.longValue());
            }
            if (this.zzbB != null) {
                zzu += zzbyc.zzf(36, this.zzbB.longValue());
            }
            if (this.zzbC != null) {
                zzu += zzbyc.zzf(37, this.zzbC.longValue());
            }
            if (this.zzbD != null) {
                zzu += zzbyc.zzc(38, this.zzbD);
            }
            if (this.zzbE != null) {
                zzu += zzbyc.zzf(39, this.zzbE.longValue());
            }
            if (this.zzbF != null) {
                zzu += zzbyc.zzf(40, this.zzbF.longValue());
            }
            if (this.zzbG != null) {
                zzu += zzbyc.zzf(41, this.zzbG.longValue());
            }
            if (this.zzbH != null) {
                zzu += zzbyc.zzf(42, this.zzbH.longValue());
            }
            if (this.zzbT != null && this.zzbT.length > 0) {
                int i = zzu;
                for (C0074zza zza : this.zzbT) {
                    if (zza != null) {
                        i += zzbyc.zzc(43, zza);
                    }
                }
                zzu = i;
            }
            if (this.zzbI != null) {
                zzu += zzbyc.zzf(44, this.zzbI.longValue());
            }
            if (this.zzbJ != null) {
                zzu += zzbyc.zzf(45, this.zzbJ.longValue());
            }
            if (this.zzaQ != null) {
                zzu += zzbyc.zzr(46, this.zzaQ);
            }
            if (this.zzaR != null) {
                zzu += zzbyc.zzr(47, this.zzaR);
            }
            if (this.zzbK != null) {
                zzu += zzbyc.zzL(48, this.zzbK.intValue());
            }
            if (this.zzbL != null) {
                zzu += zzbyc.zzL(49, this.zzbL.intValue());
            }
            if (this.zzbS != null) {
                zzu += zzbyc.zzc(50, this.zzbS);
            }
            if (this.zzbM != null) {
                zzu += zzbyc.zzf(51, this.zzbM.longValue());
            }
            if (this.zzbN != null) {
                zzu += zzbyc.zzf(52, this.zzbN.longValue());
            }
            if (this.zzbO != null) {
                zzu += zzbyc.zzf(53, this.zzbO.longValue());
            }
            if (this.zzbP != null) {
                zzu += zzbyc.zzf(54, this.zzbP.longValue());
            }
            if (this.zzbQ != null) {
                zzu += zzbyc.zzf(55, this.zzbQ.longValue());
            }
            if (this.zzbR != null) {
                zzu += zzbyc.zzL(56, this.zzbR.intValue());
            }
            if (this.zzbU != null) {
                zzu += zzbyc.zzc(57, this.zzbU);
            }
            return this.zzcb != null ? zzu + zzbyc.zzc(201, this.zzcb) : zzu;
        }
    }

    public static final class zzb extends zzbyd<zzb> {
        public Long zzco = null;
        public Integer zzcp = null;
        public Boolean zzcq = null;
        public int[] zzcr = zzbym.zzcwQ;
        public Long zzcs = null;

        public zzb() {
            this.zzcwL = -1;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzco != null) {
                zzbyc.zzb(1, this.zzco.longValue());
            }
            if (this.zzcp != null) {
                zzbyc.zzJ(2, this.zzcp.intValue());
            }
            if (this.zzcq != null) {
                zzbyc.zzg(3, this.zzcq.booleanValue());
            }
            if (this.zzcr != null && this.zzcr.length > 0) {
                for (int zzJ : this.zzcr) {
                    zzbyc.zzJ(4, zzJ);
                }
            }
            if (this.zzcs != null) {
                zzbyc.zza(5, this.zzcs.longValue());
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzi */
        public zzb zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzco = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 16:
                        this.zzcp = Integer.valueOf(zzbyb.zzafa());
                        break;
                    case 24:
                        this.zzcq = Boolean.valueOf(zzbyb.zzafc());
                        break;
                    case 32:
                        int zzb = zzbym.zzb(zzbyb, 32);
                        int length = this.zzcr == null ? 0 : this.zzcr.length;
                        int[] iArr = new int[(zzb + length)];
                        if (length != 0) {
                            System.arraycopy(this.zzcr, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = zzbyb.zzafa();
                            zzbyb.zzaeW();
                            length++;
                        }
                        iArr[length] = zzbyb.zzafa();
                        this.zzcr = iArr;
                        break;
                    case 34:
                        int zzrf = zzbyb.zzrf(zzbyb.zzaff());
                        int position = zzbyb.getPosition();
                        int i = 0;
                        while (zzbyb.zzafk() > 0) {
                            zzbyb.zzafa();
                            i++;
                        }
                        zzbyb.zzrh(position);
                        int length2 = this.zzcr == null ? 0 : this.zzcr.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.zzcr, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = zzbyb.zzafa();
                            length2++;
                        }
                        this.zzcr = iArr2;
                        zzbyb.zzrg(zzrf);
                        break;
                    case 40:
                        this.zzcs = Long.valueOf(zzbyb.zzaeY());
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzco != null) {
                zzu += zzbyc.zzf(1, this.zzco.longValue());
            }
            if (this.zzcp != null) {
                zzu += zzbyc.zzL(2, this.zzcp.intValue());
            }
            if (this.zzcq != null) {
                zzu += zzbyc.zzh(3, this.zzcq.booleanValue());
            }
            if (this.zzcr != null && this.zzcr.length > 0) {
                int i = 0;
                for (int zzrl : this.zzcr) {
                    i += zzbyc.zzrl(zzrl);
                }
                zzu = zzu + i + (this.zzcr.length * 1);
            }
            return this.zzcs != null ? zzu + zzbyc.zze(5, this.zzcs.longValue()) : zzu;
        }
    }

    public static final class zzc extends zzbyd<zzc> {
        public byte[] zzct = null;
        public byte[] zzcu = null;

        public zzc() {
            this.zzcwL = -1;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzct != null) {
                zzbyc.zzb(1, this.zzct);
            }
            if (this.zzcu != null) {
                zzbyc.zzb(2, this.zzcu);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzj */
        public zzc zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzct = zzbyb.readBytes();
                        break;
                    case 18:
                        this.zzcu = zzbyb.readBytes();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzct != null) {
                zzu += zzbyc.zzc(1, this.zzct);
            }
            return this.zzcu != null ? zzu + zzbyc.zzc(2, this.zzcu) : zzu;
        }
    }

    public static final class zzd extends zzbyd<zzd> {
        public byte[] data = null;
        public byte[] zzcv = null;
        public byte[] zzcw = null;
        public byte[] zzcx = null;

        public zzd() {
            this.zzcwL = -1;
        }

        public static zzd zze(byte[] bArr) {
            return (zzd) zzbyj.zza(new zzd(), bArr);
        }

        public void zza(zzbyc zzbyc) {
            if (this.data != null) {
                zzbyc.zzb(1, this.data);
            }
            if (this.zzcv != null) {
                zzbyc.zzb(2, this.zzcv);
            }
            if (this.zzcw != null) {
                zzbyc.zzb(3, this.zzcw);
            }
            if (this.zzcx != null) {
                zzbyc.zzb(4, this.zzcx);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzk */
        public zzd zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.data = zzbyb.readBytes();
                        break;
                    case 18:
                        this.zzcv = zzbyb.readBytes();
                        break;
                    case 26:
                        this.zzcw = zzbyb.readBytes();
                        break;
                    case 34:
                        this.zzcx = zzbyb.readBytes();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.data != null) {
                zzu += zzbyc.zzc(1, this.data);
            }
            if (this.zzcv != null) {
                zzu += zzbyc.zzc(2, this.zzcv);
            }
            if (this.zzcw != null) {
                zzu += zzbyc.zzc(3, this.zzcw);
            }
            return this.zzcx != null ? zzu + zzbyc.zzc(4, this.zzcx) : zzu;
        }
    }

    public static final class zze extends zzbyd<zze> {
        public Long zzco = null;
        public String zzcy = null;
        public byte[] zzcz = null;

        public zze() {
            this.zzcwL = -1;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzco != null) {
                zzbyc.zzb(1, this.zzco.longValue());
            }
            if (this.zzcy != null) {
                zzbyc.zzq(3, this.zzcy);
            }
            if (this.zzcz != null) {
                zzbyc.zzb(4, this.zzcz);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzl */
        public zze zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzco = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 26:
                        this.zzcy = zzbyb.readString();
                        break;
                    case 34:
                        this.zzcz = zzbyb.readBytes();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzco != null) {
                zzu += zzbyc.zzf(1, this.zzco.longValue());
            }
            if (this.zzcy != null) {
                zzu += zzbyc.zzr(3, this.zzcy);
            }
            return this.zzcz != null ? zzu + zzbyc.zzc(4, this.zzcz) : zzu;
        }
    }

    public static final class zzf extends zzbyd<zzf> {
        public byte[][] zzcA = zzbym.zzcwV;
        public Integer zzcB = null;
        public Integer zzcC = null;
        public byte[] zzcv = null;

        public zzf() {
            this.zzcwL = -1;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzcA != null && this.zzcA.length > 0) {
                for (byte[] bArr : this.zzcA) {
                    if (bArr != null) {
                        zzbyc.zzb(1, bArr);
                    }
                }
            }
            if (this.zzcv != null) {
                zzbyc.zzb(2, this.zzcv);
            }
            if (this.zzcB != null) {
                zzbyc.zzJ(3, this.zzcB.intValue());
            }
            if (this.zzcC != null) {
                zzbyc.zzJ(4, this.zzcC.intValue());
            }
            super.zza(zzbyc);
        }

        /* renamed from: zzm */
        public zzf zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        int zzb = zzbym.zzb(zzbyb, 10);
                        int length = this.zzcA == null ? 0 : this.zzcA.length;
                        byte[][] bArr = new byte[(zzb + length)][];
                        if (length != 0) {
                            System.arraycopy(this.zzcA, 0, bArr, 0, length);
                        }
                        while (length < bArr.length - 1) {
                            bArr[length] = zzbyb.readBytes();
                            zzbyb.zzaeW();
                            length++;
                        }
                        bArr[length] = zzbyb.readBytes();
                        this.zzcA = bArr;
                        break;
                    case 18:
                        this.zzcv = zzbyb.readBytes();
                        break;
                    case 24:
                        int zzafa = zzbyb.zzafa();
                        switch (zzafa) {
                            case 0:
                            case 1:
                                this.zzcB = Integer.valueOf(zzafa);
                                continue;
                        }
                    case 32:
                        int zzafa2 = zzbyb.zzafa();
                        switch (zzafa2) {
                            case 0:
                            case 1:
                                this.zzcC = Integer.valueOf(zzafa2);
                                continue;
                        }
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int i;
            int zzu = super.zzu();
            if (this.zzcA == null || this.zzcA.length <= 0) {
                i = zzu;
            } else {
                int i2 = 0;
                int i3 = 0;
                for (byte[] bArr : this.zzcA) {
                    if (bArr != null) {
                        i3++;
                        i2 += zzbyc.zzaj(bArr);
                    }
                }
                i = zzu + i2 + (i3 * 1);
            }
            if (this.zzcv != null) {
                i += zzbyc.zzc(2, this.zzcv);
            }
            if (this.zzcB != null) {
                i += zzbyc.zzL(3, this.zzcB.intValue());
            }
            return this.zzcC != null ? i + zzbyc.zzL(4, this.zzcC.intValue()) : i;
        }
    }
}
