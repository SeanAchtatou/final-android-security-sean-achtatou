package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import com.duapps.ad.AdError;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.util.zzu;
import com.google.android.gms.common.zze;
import com.google.android.gms.internal.zzats;
import com.lody.virtual.helper.utils.FileUtils;
import com.lody.virtual.os.VUserHandle;
import io.fabric.sdk.android.services.common.a;
import java.lang.reflect.InvocationTargetException;

public class zzati extends zzaug {
    static final String zzbrg = String.valueOf(zze.GOOGLE_PLAY_SERVICES_VERSION_CODE / AdError.NETWORK_ERROR_CODE).replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");
    private Boolean zzaeZ;

    zzati(zzaue zzaue) {
        super(zzaue);
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public /* bridge */ /* synthetic */ void zzJV() {
        super.zzJV();
    }

    public /* bridge */ /* synthetic */ void zzJW() {
        super.zzJW();
    }

    public /* bridge */ /* synthetic */ void zzJX() {
        super.zzJX();
    }

    public /* bridge */ /* synthetic */ zzatb zzJY() {
        return super.zzJY();
    }

    public /* bridge */ /* synthetic */ zzatf zzJZ() {
        return super.zzJZ();
    }

    /* access modifiers changed from: package-private */
    public String zzKK() {
        return zzats.zzbrP.get();
    }

    public int zzKL() {
        return 25;
    }

    public int zzKM() {
        return 40;
    }

    public int zzKN() {
        return 24;
    }

    /* access modifiers changed from: package-private */
    public int zzKO() {
        return 40;
    }

    /* access modifiers changed from: package-private */
    public int zzKP() {
        return 100;
    }

    /* access modifiers changed from: package-private */
    public int zzKQ() {
        return FileUtils.FileMode.MODE_IRUSR;
    }

    /* access modifiers changed from: package-private */
    public int zzKR() {
        return AdError.NETWORK_ERROR_CODE;
    }

    public int zzKS() {
        return 36;
    }

    public int zzKT() {
        return FileUtils.FileMode.MODE_ISUID;
    }

    /* access modifiers changed from: package-private */
    public int zzKU() {
        return 500;
    }

    public long zzKV() {
        return (long) zzats.zzbrZ.get().intValue();
    }

    public long zzKW() {
        return (long) zzats.zzbsb.get().intValue();
    }

    /* access modifiers changed from: package-private */
    public int zzKX() {
        return 25;
    }

    /* access modifiers changed from: package-private */
    public int zzKY() {
        return AdError.NETWORK_ERROR_CODE;
    }

    /* access modifiers changed from: package-private */
    public int zzKZ() {
        return 25;
    }

    public /* bridge */ /* synthetic */ zzauj zzKa() {
        return super.zzKa();
    }

    public /* bridge */ /* synthetic */ zzatu zzKb() {
        return super.zzKb();
    }

    public /* bridge */ /* synthetic */ zzatl zzKc() {
        return super.zzKc();
    }

    public /* bridge */ /* synthetic */ zzaul zzKd() {
        return super.zzKd();
    }

    public /* bridge */ /* synthetic */ zzauk zzKe() {
        return super.zzKe();
    }

    public /* bridge */ /* synthetic */ zzatv zzKf() {
        return super.zzKf();
    }

    public /* bridge */ /* synthetic */ zzatj zzKg() {
        return super.zzKg();
    }

    public /* bridge */ /* synthetic */ zzaut zzKh() {
        return super.zzKh();
    }

    public /* bridge */ /* synthetic */ zzauc zzKi() {
        return super.zzKi();
    }

    public /* bridge */ /* synthetic */ zzaun zzKj() {
        return super.zzKj();
    }

    public /* bridge */ /* synthetic */ zzaud zzKk() {
        return super.zzKk();
    }

    public /* bridge */ /* synthetic */ zzatx zzKl() {
        return super.zzKl();
    }

    public /* bridge */ /* synthetic */ zzaua zzKm() {
        return super.zzKm();
    }

    public /* bridge */ /* synthetic */ zzati zzKn() {
        return super.zzKn();
    }

    public long zzKv() {
        return 10260;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLA() {
        return Math.max(0L, zzats.zzbsp.get().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLB() {
        return Math.max(0L, zzats.zzbsq.get().longValue());
    }

    public int zzLC() {
        return Math.min(20, Math.max(0, zzats.zzbsr.get().intValue()));
    }

    public String zzLD() {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, "debug.firebase.analytics.app", "");
        } catch (ClassNotFoundException e2) {
            zzKl().zzLZ().zzj("Could not find SystemProperties class", e2);
        } catch (NoSuchMethodException e3) {
            zzKl().zzLZ().zzj("Could not find SystemProperties.get() method", e3);
        } catch (IllegalAccessException e4) {
            zzKl().zzLZ().zzj("Could not access SystemProperties.get()", e4);
        } catch (InvocationTargetException e5) {
            zzKl().zzLZ().zzj("SystemProperties.get() threw an exception", e5);
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public int zzLa() {
        return AdError.NETWORK_ERROR_CODE;
    }

    /* access modifiers changed from: package-private */
    public long zzLb() {
        return 15552000000L;
    }

    /* access modifiers changed from: package-private */
    public long zzLc() {
        return 15552000000L;
    }

    /* access modifiers changed from: package-private */
    public long zzLd() {
        return 3600000;
    }

    /* access modifiers changed from: package-private */
    public long zzLe() {
        return 60000;
    }

    /* access modifiers changed from: package-private */
    public long zzLf() {
        return 61000;
    }

    /* access modifiers changed from: package-private */
    public String zzLg() {
        return "google_app_measurement_local.db";
    }

    public boolean zzLh() {
        return false;
    }

    public boolean zzLi() {
        Boolean zzfp = zzfp("firebase_analytics_collection_deactivated");
        return zzfp != null && zzfp.booleanValue();
    }

    public Boolean zzLj() {
        return zzfp("firebase_analytics_collection_enabled");
    }

    public long zzLk() {
        return zzats.zzbss.get().longValue();
    }

    public long zzLl() {
        return zzats.zzbsn.get().longValue();
    }

    public long zzLm() {
        return zzats.zzbso.get().longValue();
    }

    public long zzLn() {
        return 1000;
    }

    public int zzLo() {
        return Math.max(0, zzats.zzbrX.get().intValue());
    }

    public int zzLp() {
        return Math.max(1, zzats.zzbrY.get().intValue());
    }

    public int zzLq() {
        return VUserHandle.PER_USER_RANGE;
    }

    public String zzLr() {
        return zzats.zzbsf.get();
    }

    public long zzLs() {
        return zzats.zzbrS.get().longValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLt() {
        return Math.max(0L, zzats.zzbsg.get().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLu() {
        return Math.max(0L, zzats.zzbsi.get().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLv() {
        return Math.max(0L, zzats.zzbsj.get().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLw() {
        return Math.max(0L, zzats.zzbsk.get().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLx() {
        return Math.max(0L, zzats.zzbsl.get().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzLy() {
        return Math.max(0L, zzats.zzbsm.get().longValue());
    }

    public long zzLz() {
        return zzats.zzbsh.get().longValue();
    }

    public String zzP(String str, String str2) {
        Uri.Builder builder = new Uri.Builder();
        Uri.Builder encodedAuthority = builder.scheme(zzats.zzbrT.get()).encodedAuthority(zzats.zzbrU.get());
        String valueOf = String.valueOf(str);
        encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", str2).appendQueryParameter("platform", a.ANDROID_CLIENT_TYPE).appendQueryParameter("gmp_version", String.valueOf(10260L));
        return builder.build().toString();
    }

    public long zza(String str, zzats.zza<Long> zza) {
        if (str == null) {
            return zza.get().longValue();
        }
        String zzZ = zzKi().zzZ(str, zza.getKey());
        if (TextUtils.isEmpty(zzZ)) {
            return zza.get().longValue();
        }
        try {
            return zza.get(Long.valueOf(Long.valueOf(zzZ).longValue())).longValue();
        } catch (NumberFormatException e2) {
            return zza.get().longValue();
        }
    }

    public int zzb(String str, zzats.zza<Integer> zza) {
        if (str == null) {
            return zza.get().intValue();
        }
        String zzZ = zzKi().zzZ(str, zza.getKey());
        if (TextUtils.isEmpty(zzZ)) {
            return zza.get().intValue();
        }
        try {
            return zza.get(Integer.valueOf(Integer.valueOf(zzZ).intValue())).intValue();
        } catch (NumberFormatException e2) {
            return zza.get().intValue();
        }
    }

    public int zzfj(String str) {
        return Math.max(0, Math.min(1000000, zzb(str, zzats.zzbsa)));
    }

    public int zzfk(String str) {
        return zzb(str, zzats.zzbsc);
    }

    public int zzfl(String str) {
        return zzb(str, zzats.zzbsd);
    }

    /* access modifiers changed from: package-private */
    public long zzfm(String str) {
        return zza(str, zzats.zzbrQ);
    }

    /* access modifiers changed from: package-private */
    public int zzfn(String str) {
        return zzb(str, zzats.zzbst);
    }

    /* access modifiers changed from: package-private */
    public int zzfo(String str) {
        return Math.max(0, Math.min((int) AdError.SERVER_ERROR_CODE, zzb(str, zzats.zzbsu)));
    }

    /* access modifiers changed from: package-private */
    public Boolean zzfp(String str) {
        zzac.zzdr(str);
        try {
            if (getContext().getPackageManager() == null) {
                zzKl().zzLZ().log("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo applicationInfo = zzadg.zzbi(getContext()).getApplicationInfo(getContext().getPackageName(), FileUtils.FileMode.MODE_IWUSR);
            if (applicationInfo == null) {
                zzKl().zzLZ().log("Failed to load metadata: ApplicationInfo is null");
                return null;
            } else if (applicationInfo.metaData == null) {
                zzKl().zzLZ().log("Failed to load metadata: Metadata bundle is null");
                return null;
            } else if (applicationInfo.metaData.containsKey(str)) {
                return Boolean.valueOf(applicationInfo.metaData.getBoolean(str));
            } else {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            zzKl().zzLZ().zzj("Failed to load metadata: Package name not found", e2);
            return null;
        }
    }

    public int zzfq(String str) {
        return zzb(str, zzats.zzbrV);
    }

    public int zzfr(String str) {
        return Math.max(0, zzb(str, zzats.zzbrW));
    }

    public int zzfs(String str) {
        return Math.max(0, Math.min(1000000, zzb(str, zzats.zzbse)));
    }

    public /* bridge */ /* synthetic */ void zzmR() {
        super.zzmR();
    }

    public /* bridge */ /* synthetic */ com.google.android.gms.common.util.zze zznR() {
        return super.zznR();
    }

    public boolean zzoW() {
        if (this.zzaeZ == null) {
            synchronized (this) {
                if (this.zzaeZ == null) {
                    ApplicationInfo applicationInfo = getContext().getApplicationInfo();
                    String zzzr = zzu.zzzr();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.zzaeZ = Boolean.valueOf(str != null && str.equals(zzzr));
                    }
                    if (this.zzaeZ == null) {
                        this.zzaeZ = Boolean.TRUE;
                        zzKl().zzLZ().log("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.zzaeZ.booleanValue();
    }

    /* access modifiers changed from: package-private */
    public long zzpq() {
        return zzats.zzbsv.get().longValue();
    }

    public String zzpv() {
        return "google_app_measurement.db";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public long zzpz() {
        return Math.max(0L, zzats.zzbrR.get().longValue());
    }

    public boolean zzwR() {
        return zzaba.zzwR();
    }
}
