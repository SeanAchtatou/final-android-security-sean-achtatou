package com.google.android.gms.internal;

import com.google.android.gms.tagmanager.zzbo;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

class zzbjh implements zzbji {
    private HttpURLConnection zzbMs;
    private InputStream zzbMt = null;

    zzbjh() {
    }

    private InputStream zzd(HttpURLConnection httpURLConnection) {
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            return httpURLConnection.getInputStream();
        }
        String sb = new StringBuilder(25).append("Bad response: ").append(responseCode).toString();
        if (responseCode == 404) {
            throw new FileNotFoundException(sb);
        } else if (responseCode == 503) {
            throw new zzbjk(sb);
        } else {
            throw new IOException(sb);
        }
    }

    private void zze(HttpURLConnection httpURLConnection) {
        try {
            if (this.zzbMt != null) {
                this.zzbMt.close();
            }
        } catch (IOException e2) {
            IOException iOException = e2;
            String valueOf = String.valueOf(iOException.getMessage());
            zzbo.zzb(valueOf.length() != 0 ? "HttpUrlConnectionNetworkClient: Error when closing http input stream: ".concat(valueOf) : new String("HttpUrlConnectionNetworkClient: Error when closing http input stream: "), iOException);
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }

    public void close() {
        zze(this.zzbMs);
    }

    public InputStream zzhX(String str) {
        this.zzbMs = zzhY(str);
        this.zzbMt = zzd(this.zzbMs);
        return this.zzbMt;
    }

    /* access modifiers changed from: package-private */
    public HttpURLConnection zzhY(String str) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setReadTimeout(20000);
        httpURLConnection.setConnectTimeout(20000);
        return httpURLConnection;
    }
}
