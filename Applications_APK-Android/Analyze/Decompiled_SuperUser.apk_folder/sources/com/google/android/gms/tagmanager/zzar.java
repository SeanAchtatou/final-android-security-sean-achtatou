package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

class zzar extends zzam {
    private static final String ID = zzah.HASH.toString();
    private static final String zzbGi = zzai.ARG0.toString();
    private static final String zzbGk = zzai.INPUT_FORMAT.toString();
    private static final String zzbGo = zzai.ALGORITHM.toString();

    public zzar() {
        super(ID, zzbGi);
    }

    private byte[] zzf(String str, byte[] bArr) {
        MessageDigest instance = MessageDigest.getInstance(str);
        instance.update(bArr);
        return instance.digest();
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        byte[] zzgR;
        zzak.zza zza = map.get(zzbGi);
        if (zza == null || zza == zzdl.zzRT()) {
            return zzdl.zzRT();
        }
        String zze = zzdl.zze(zza);
        zzak.zza zza2 = map.get(zzbGo);
        String zze2 = zza2 == null ? "MD5" : zzdl.zze(zza2);
        zzak.zza zza3 = map.get(zzbGk);
        String zze3 = zza3 == null ? "text" : zzdl.zze(zza3);
        if ("text".equals(zze3)) {
            zzgR = zze.getBytes();
        } else if ("base16".equals(zze3)) {
            zzgR = zzk.zzgR(zze);
        } else {
            String valueOf = String.valueOf(zze3);
            zzbo.e(valueOf.length() != 0 ? "Hash: unknown input format: ".concat(valueOf) : new String("Hash: unknown input format: "));
            return zzdl.zzRT();
        }
        try {
            return zzdl.zzS(zzk.zzq(zzf(zze2, zzgR)));
        } catch (NoSuchAlgorithmException e2) {
            String valueOf2 = String.valueOf(zze2);
            zzbo.e(valueOf2.length() != 0 ? "Hash: unknown algorithm: ".concat(valueOf2) : new String("Hash: unknown algorithm: "));
            return zzdl.zzRT();
        }
    }
}
