package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

class zzd<TResult> implements zzf<TResult> {
    private final Executor zzbFP;
    /* access modifiers changed from: private */
    public OnFailureListener zzbNz;
    /* access modifiers changed from: private */
    public final Object zzrJ = new Object();

    public zzd(Executor executor, OnFailureListener onFailureListener) {
        this.zzbFP = executor;
        this.zzbNz = onFailureListener;
    }

    public void cancel() {
        synchronized (this.zzrJ) {
            this.zzbNz = null;
        }
    }

    public void onComplete(final Task<TResult> task) {
        if (!task.isSuccessful()) {
            synchronized (this.zzrJ) {
                if (this.zzbNz != null) {
                    this.zzbFP.execute(new Runnable() {
                        public void run() {
                            synchronized (zzd.this.zzrJ) {
                                if (zzd.this.zzbNz != null) {
                                    zzd.this.zzbNz.onFailure(task.getException());
                                }
                            }
                        }
                    });
                }
            }
        }
    }
}
