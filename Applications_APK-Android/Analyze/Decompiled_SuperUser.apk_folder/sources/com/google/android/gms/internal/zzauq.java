package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.zzac;

public class zzauq extends zza {
    public static final Parcelable.Creator<zzauq> CREATOR = new zzaur();
    public final String name;
    public final int versionCode;
    public final String zzaGV;
    public final String zzbqW;
    public final long zzbwf;
    public final Long zzbwg;
    public final Float zzbwh;
    public final Double zzbwi;

    zzauq(int i, String str, long j, Long l, Float f2, String str2, String str3, Double d2) {
        Double d3 = null;
        this.versionCode = i;
        this.name = str;
        this.zzbwf = j;
        this.zzbwg = l;
        this.zzbwh = null;
        if (i == 1) {
            this.zzbwi = f2 != null ? Double.valueOf(f2.doubleValue()) : d3;
        } else {
            this.zzbwi = d2;
        }
        this.zzaGV = str2;
        this.zzbqW = str3;
    }

    zzauq(zzaus zzaus) {
        this(zzaus.mName, zzaus.zzbwj, zzaus.mValue, zzaus.mOrigin);
    }

    zzauq(String str, long j, Object obj, String str2) {
        zzac.zzdr(str);
        this.versionCode = 2;
        this.name = str;
        this.zzbwf = j;
        this.zzbqW = str2;
        if (obj == null) {
            this.zzbwg = null;
            this.zzbwh = null;
            this.zzbwi = null;
            this.zzaGV = null;
        } else if (obj instanceof Long) {
            this.zzbwg = (Long) obj;
            this.zzbwh = null;
            this.zzbwi = null;
            this.zzaGV = null;
        } else if (obj instanceof String) {
            this.zzbwg = null;
            this.zzbwh = null;
            this.zzbwi = null;
            this.zzaGV = (String) obj;
        } else if (obj instanceof Double) {
            this.zzbwg = null;
            this.zzbwh = null;
            this.zzbwi = (Double) obj;
            this.zzaGV = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    public Object getValue() {
        if (this.zzbwg != null) {
            return this.zzbwg;
        }
        if (this.zzbwi != null) {
            return this.zzbwi;
        }
        if (this.zzaGV != null) {
            return this.zzaGV;
        }
        return null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzaur.zza(this, parcel, i);
    }
}
