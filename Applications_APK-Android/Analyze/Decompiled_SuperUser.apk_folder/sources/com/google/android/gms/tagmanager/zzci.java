package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.Map;
import java.util.Set;

public abstract class zzci extends zzam {
    private static final String zzbGi = zzai.ARG0.toString();
    private static final String zzbHf = zzai.ARG1.toString();

    public zzci(String str) {
        super(str, zzbGi, zzbHf);
    }

    public /* bridge */ /* synthetic */ String zzQN() {
        return super.zzQN();
    }

    public /* bridge */ /* synthetic */ Set zzQO() {
        return super.zzQO();
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        for (zzak.zza zza : map.values()) {
            if (zza == zzdl.zzRT()) {
                return zzdl.zzS(false);
            }
        }
        zzak.zza zza2 = map.get(zzbGi);
        zzak.zza zza3 = map.get(zzbHf);
        return zzdl.zzS(Boolean.valueOf((zza2 == null || zza3 == null) ? false : zza(zza2, zza3, map)));
    }

    /* access modifiers changed from: protected */
    public abstract boolean zza(zzak.zza zza, zzak.zza zza2, Map<String, zzak.zza> map);
}
