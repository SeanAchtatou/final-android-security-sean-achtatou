package com.google.android.gms.tagmanager;

import android.text.TextUtils;

class zzas {
    private final long zzagi;
    private final long zzbGp;
    private final long zzbGq;
    private String zzbGr;

    zzas(long j, long j2, long j3) {
        this.zzbGp = j;
        this.zzagi = j2;
        this.zzbGq = j3;
    }

    /* access modifiers changed from: package-private */
    public long zzQP() {
        return this.zzbGp;
    }

    /* access modifiers changed from: package-private */
    public long zzQQ() {
        return this.zzbGq;
    }

    /* access modifiers changed from: package-private */
    public String zzQR() {
        return this.zzbGr;
    }

    /* access modifiers changed from: package-private */
    public void zzhi(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim())) {
            this.zzbGr = str;
        }
    }
}
