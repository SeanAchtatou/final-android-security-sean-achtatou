package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzak;
import java.util.Map;

public class zzah extends zzdg {
    private static final String ID = com.google.android.gms.internal.zzah.EQUALS.toString();

    public zzah() {
        super(ID);
    }

    /* access modifiers changed from: protected */
    public boolean zza(String str, String str2, Map<String, zzak.zza> map) {
        return str.equals(str2);
    }
}
