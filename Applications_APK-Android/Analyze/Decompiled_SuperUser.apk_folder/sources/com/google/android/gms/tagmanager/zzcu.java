package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.tagmanager.zzp;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class zzcu implements zzp.zze {
    private boolean mClosed;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final String zzbEX;
    private String zzbFv;
    private final zza zzbHA;
    private ScheduledFuture<?> zzbHB;
    private zzbn<zzaj.zzj> zzbHw;
    private zzt zzbHx;
    private final ScheduledExecutorService zzbHz;

    interface zza {
        zzct zza(zzt zzt);
    }

    interface zzb {
        ScheduledExecutorService zzRn();
    }

    public zzcu(Context context, String str, zzt zzt) {
        this(context, str, zzt, null, null);
    }

    zzcu(Context context, String str, zzt zzt, zzb zzb2, zza zza2) {
        this.zzbHx = zzt;
        this.mContext = context;
        this.zzbEX = str;
        this.zzbHz = (zzb2 == null ? new zzb(this) {
            public ScheduledExecutorService zzRn() {
                return Executors.newSingleThreadScheduledExecutor();
            }
        } : zzb2).zzRn();
        if (zza2 == null) {
            this.zzbHA = new zza() {
                public zzct zza(zzt zzt) {
                    return new zzct(zzcu.this.mContext, zzcu.this.zzbEX, zzt);
                }
            };
        } else {
            this.zzbHA = zza2;
        }
    }

    private synchronized void zzRm() {
        if (this.mClosed) {
            throw new IllegalStateException("called method after closed");
        }
    }

    private zzct zzhp(String str) {
        zzct zza2 = this.zzbHA.zza(this.zzbHx);
        zza2.zza(this.zzbHw);
        zza2.zzgZ(this.zzbFv);
        zza2.zzho(str);
        return zza2;
    }

    public synchronized void release() {
        zzRm();
        if (this.zzbHB != null) {
            this.zzbHB.cancel(false);
        }
        this.zzbHz.shutdown();
        this.mClosed = true;
    }

    public synchronized void zza(zzbn<zzaj.zzj> zzbn) {
        zzRm();
        this.zzbHw = zzbn;
    }

    public synchronized void zzf(long j, String str) {
        String str2 = this.zzbEX;
        zzbo.v(new StringBuilder(String.valueOf(str2).length() + 55).append("loadAfterDelay: containerId=").append(str2).append(" delay=").append(j).toString());
        zzRm();
        if (this.zzbHw == null) {
            throw new IllegalStateException("callback must be set before loadAfterDelay() is called.");
        }
        if (this.zzbHB != null) {
            this.zzbHB.cancel(false);
        }
        this.zzbHB = this.zzbHz.schedule(zzhp(str), j, TimeUnit.MILLISECONDS);
    }

    public synchronized void zzgZ(String str) {
        zzRm();
        this.zzbFv = str;
    }
}
