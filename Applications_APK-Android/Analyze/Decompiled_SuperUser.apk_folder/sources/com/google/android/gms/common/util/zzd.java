package com.google.android.gms.common.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.gms.internal.zzadg;
import com.lody.virtual.helper.utils.FileUtils;

public class zzd {
    public static int zzD(Context context, String str) {
        return zzc(zzE(context, str));
    }

    public static PackageInfo zzE(Context context, String str) {
        try {
            return zzadg.zzbi(context).getPackageInfo(str, FileUtils.FileMode.MODE_IWUSR);
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    public static boolean zzF(Context context, String str) {
        "com.google.android.gms".equals(str);
        try {
            return (zzadg.zzbi(context).getApplicationInfo(str, 0).flags & 2097152) != 0;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    public static int zzc(PackageInfo packageInfo) {
        Bundle bundle;
        if (packageInfo == null || packageInfo.applicationInfo == null || (bundle = packageInfo.applicationInfo.metaData) == null) {
            return -1;
        }
        return bundle.getInt("com.google.android.gms.version", -1);
    }
}
