package com.google.android.gms.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.zzac;

public class zzaad {

    public static abstract class zza<R extends Result, A extends Api.zzb> extends zzaaf<R> implements zzb<R> {
        private final Api<?> zzaxf;
        private final Api.zzc<A> zzazY;

        @Deprecated
        protected zza(Api.zzc<A> zzc, GoogleApiClient googleApiClient) {
            super((GoogleApiClient) zzac.zzb(googleApiClient, "GoogleApiClient must not be null"));
            this.zzazY = (Api.zzc) zzac.zzw(zzc);
            this.zzaxf = null;
        }

        protected zza(Api<?> api, GoogleApiClient googleApiClient) {
            super((GoogleApiClient) zzac.zzb(googleApiClient, "GoogleApiClient must not be null"));
            this.zzazY = api.zzvg();
            this.zzaxf = api;
        }

        private void zzc(RemoteException remoteException) {
            zzB(new Status(8, remoteException.getLocalizedMessage(), null));
        }

        public final Api<?> getApi() {
            return this.zzaxf;
        }

        public /* synthetic */ void setResult(Object obj) {
            super.zzb((Result) obj);
        }

        public final void zzB(Status status) {
            zzac.zzb(!status.isSuccess(), "Failed result must not be success");
            zzb(zzc(status));
        }

        /* access modifiers changed from: protected */
        public abstract void zza(A a2);

        public final void zzb(A a2) {
            try {
                zza((Api.zzb) a2);
            } catch (DeadObjectException e2) {
                zzc(e2);
                throw e2;
            } catch (RemoteException e3) {
                zzc(e3);
            }
        }

        public final Api.zzc<A> zzvg() {
            return this.zzazY;
        }
    }

    public interface zzb<R> {
        void setResult(R r);

        void zzB(Status status);
    }
}
