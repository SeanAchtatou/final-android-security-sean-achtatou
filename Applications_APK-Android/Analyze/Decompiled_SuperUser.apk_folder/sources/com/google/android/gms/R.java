package com.google.android.gms;

public final class R {

    public static final class attr {

        /* renamed from: buttonSize */
        public static final int i6 = 2130772296;

        /* renamed from: circleCrop */
        public static final int ga = 2130772227;

        /* renamed from: colorScheme */
        public static final int i7 = 2130772297;

        /* renamed from: imageAspectRatio */
        public static final int g_ = 2130772226;

        /* renamed from: imageAspectRatioAdjust */
        public static final int g9 = 2130772225;

        /* renamed from: scopeUris */
        public static final int i8 = 2130772298;
    }

    public static final class color {

        /* renamed from: common_google_signin_btn_text_dark */
        public static final int em = 2131558600;

        /* renamed from: common_google_signin_btn_text_dark_default */
        public static final int ao = 2131558451;

        /* renamed from: common_google_signin_btn_text_dark_disabled */
        public static final int ap = 2131558452;

        /* renamed from: common_google_signin_btn_text_dark_focused */
        public static final int aq = 2131558453;

        /* renamed from: common_google_signin_btn_text_dark_pressed */
        public static final int ar = 2131558454;

        /* renamed from: common_google_signin_btn_text_light */
        public static final int en = 2131558601;

        /* renamed from: common_google_signin_btn_text_light_default */
        public static final int as = 2131558455;

        /* renamed from: common_google_signin_btn_text_light_disabled */
        public static final int at = 2131558456;

        /* renamed from: common_google_signin_btn_text_light_focused */
        public static final int au = 2131558457;

        /* renamed from: common_google_signin_btn_text_light_pressed */
        public static final int av = 2131558458;

        /* renamed from: common_google_signin_btn_tint */
        public static final int eo = 2131558602;
    }

    public static final class drawable {

        /* renamed from: common_full_open_on_phone */
        public static final int f5 = 2130837632;

        /* renamed from: common_google_signin_btn_icon_dark */
        public static final int bb = 2130837633;

        /* renamed from: common_google_signin_btn_icon_dark_focused */
        public static final int bc = 2130837634;

        /* renamed from: common_google_signin_btn_icon_dark_normal */
        public static final int bd = 2130837635;

        /* renamed from: common_google_signin_btn_icon_dark_normal_background */
        public static final int ef = 2130837636;

        /* renamed from: common_google_signin_btn_icon_disabled */
        public static final int be = 2130837637;

        /* renamed from: common_google_signin_btn_icon_light */
        public static final int bf = 2130837638;

        /* renamed from: common_google_signin_btn_icon_light_focused */
        public static final int bg = 2130837639;

        /* renamed from: common_google_signin_btn_icon_light_normal */
        public static final int bh = 2130837640;

        /* renamed from: common_google_signin_btn_icon_light_normal_background */
        public static final int eg = 2130837641;

        /* renamed from: common_google_signin_btn_text_dark */
        public static final int bi = 2130837642;

        /* renamed from: common_google_signin_btn_text_dark_focused */
        public static final int bj = 2130837643;

        /* renamed from: common_google_signin_btn_text_dark_normal */
        public static final int bk = 2130837644;

        /* renamed from: common_google_signin_btn_text_dark_normal_background */
        public static final int eh = 2130837645;

        /* renamed from: common_google_signin_btn_text_disabled */
        public static final int bl = 2130837646;

        /* renamed from: common_google_signin_btn_text_light */
        public static final int bm = 2130837647;

        /* renamed from: common_google_signin_btn_text_light_focused */
        public static final int bn = 2130837648;

        /* renamed from: common_google_signin_btn_text_light_normal */
        public static final int bo = 2130837649;

        /* renamed from: common_google_signin_btn_text_light_normal_background */
        public static final int ei = 2130837650;

        /* renamed from: googleg_disabled_color_18 */
        public static final int ej = 2130837676;

        /* renamed from: googleg_standard_color_18 */
        public static final int ek = 2130837677;
    }

    public static final class id {

        /* renamed from: adjust_height */
        public static final int aw = 2131623995;

        /* renamed from: adjust_width */
        public static final int ax = 2131623996;
        public static final int auto = 2131623974;
        public static final int center = 2131623976;

        /* renamed from: dark */
        public static final int b8 = 2131624007;

        /* renamed from: icon_only */
        public static final int b5 = 2131624004;

        /* renamed from: light */
        public static final int b9 = 2131624008;
        public static final int none = 2131623957;
        public static final int normal = 2131623953;
        public static final int radio = 2131624039;

        /* renamed from: seek_bar */
        public static final int ej = 2131624130;

        /* renamed from: standard */
        public static final int b6 = 2131624005;
        public static final int text = 2131624101;
        public static final int text1 = 2131624395;
        public static final int text2 = 2131624344;

        /* renamed from: toolbar */
        public static final int cu = 2131624067;

        /* renamed from: wide */
        public static final int b7 = 2131624006;
        public static final int wrap_content = 2131623973;
    }

    public static final class integer {

        /* renamed from: google_play_services_version  reason: collision with root package name */
        public static final int f3918google_play_services_version = 2131492871;
    }

    public static final class string {

        /* renamed from: common_google_play_services_enable_button */
        public static final int t = 2131296275;

        /* renamed from: common_google_play_services_enable_text */
        public static final int u = 2131296276;

        /* renamed from: common_google_play_services_enable_title */
        public static final int v = 2131296277;

        /* renamed from: common_google_play_services_install_button */
        public static final int w = 2131296278;

        /* renamed from: common_google_play_services_install_text */
        public static final int x = 2131296279;

        /* renamed from: common_google_play_services_install_title */
        public static final int y = 2131296280;

        /* renamed from: common_google_play_services_notification_ticker */
        public static final int z = 2131296281;
        public static final int common_google_play_services_unknown_issue = 2131296282;

        /* renamed from: common_google_play_services_unsupported_text */
        public static final int a1 = 2131296283;

        /* renamed from: common_google_play_services_update_button */
        public static final int a2 = 2131296284;

        /* renamed from: common_google_play_services_update_text */
        public static final int a3 = 2131296285;

        /* renamed from: common_google_play_services_update_title */
        public static final int a4 = 2131296286;

        /* renamed from: common_google_play_services_updating_text */
        public static final int a5 = 2131296287;

        /* renamed from: common_google_play_services_wear_update_text */
        public static final int a6 = 2131296288;

        /* renamed from: common_open_on_phone */
        public static final int a7 = 2131296289;

        /* renamed from: common_signin_button_text */
        public static final int a8 = 2131296290;

        /* renamed from: common_signin_button_text_long */
        public static final int a9 = 2131296291;
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.kingouser.com.R.attr.g9, com.kingouser.com.R.attr.g_, com.kingouser.com.R.attr.ga};
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
        public static final int[] SignInButton = {com.kingouser.com.R.attr.i6, com.kingouser.com.R.attr.i7, com.kingouser.com.R.attr.i8};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
