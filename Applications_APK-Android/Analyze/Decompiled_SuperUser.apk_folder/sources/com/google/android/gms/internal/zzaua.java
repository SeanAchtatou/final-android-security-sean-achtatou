package com.google.android.gms.internal;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.internal.zzac;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Locale;

class zzaua extends zzauh {
    static final Pair<String, Long> zzbsZ = new Pair<>("", 0L);
    /* access modifiers changed from: private */
    public SharedPreferences zzagD;
    public final zzc zzbta = new zzc("health_monitor", zzKn().zzpz());
    public final zzb zzbtb = new zzb("last_upload", 0);
    public final zzb zzbtc = new zzb("last_upload_attempt", 0);
    public final zzb zzbtd = new zzb("backoff", 0);
    public final zzb zzbte = new zzb("last_delete_stale", 0);
    public final zzb zzbtf = new zzb("midnight_offset", 0);
    private String zzbtg;
    private boolean zzbth;
    private long zzbti;
    private String zzbtj;
    private long zzbtk;
    private final Object zzbtl = new Object();
    private SecureRandom zzbtm;
    public final zzb zzbtn = new zzb("time_before_start", 10000);
    public final zzb zzbto = new zzb("session_timeout", 1800000);
    public final zza zzbtp = new zza("start_new_session", true);
    public final zzb zzbtq = new zzb("last_pause_time", 0);
    public final zzb zzbtr = new zzb("time_active", 0);
    public boolean zzbts;

    public final class zza {
        private final String zzAX;
        private boolean zzayS;
        private final boolean zzbtt;
        private boolean zzbtu;

        public zza(String str, boolean z) {
            zzac.zzdr(str);
            this.zzAX = str;
            this.zzbtt = z;
        }

        private void zzMq() {
            if (!this.zzbtu) {
                this.zzbtu = true;
                this.zzayS = zzaua.this.zzagD.getBoolean(this.zzAX, this.zzbtt);
            }
        }

        public boolean get() {
            zzMq();
            return this.zzayS;
        }

        public void set(boolean z) {
            SharedPreferences.Editor edit = zzaua.this.zzagD.edit();
            edit.putBoolean(this.zzAX, z);
            edit.apply();
            this.zzayS = z;
        }
    }

    public final class zzb {
        private final String zzAX;
        private long zzadd;
        private boolean zzbtu;
        private final long zzbtw;

        public zzb(String str, long j) {
            zzac.zzdr(str);
            this.zzAX = str;
            this.zzbtw = j;
        }

        private void zzMq() {
            if (!this.zzbtu) {
                this.zzbtu = true;
                this.zzadd = zzaua.this.zzagD.getLong(this.zzAX, this.zzbtw);
            }
        }

        public long get() {
            zzMq();
            return this.zzadd;
        }

        public void set(long j) {
            SharedPreferences.Editor edit = zzaua.this.zzagD.edit();
            edit.putLong(this.zzAX, j);
            edit.apply();
            this.zzadd = j;
        }
    }

    public final class zzc {
        private final long zzagH;
        final String zzbtx;
        private final String zzbty;
        private final String zzbtz;

        private zzc(String str, long j) {
            zzac.zzdr(str);
            zzac.zzaw(j > 0);
            this.zzbtx = String.valueOf(str).concat(":start");
            this.zzbty = String.valueOf(str).concat(":count");
            this.zzbtz = String.valueOf(str).concat(":value");
            this.zzagH = j;
        }

        private void zzqk() {
            zzaua.this.zzmR();
            long currentTimeMillis = zzaua.this.zznR().currentTimeMillis();
            SharedPreferences.Editor edit = zzaua.this.zzagD.edit();
            edit.remove(this.zzbty);
            edit.remove(this.zzbtz);
            edit.putLong(this.zzbtx, currentTimeMillis);
            edit.apply();
        }

        private long zzql() {
            zzaua.this.zzmR();
            long zzqn = zzqn();
            if (zzqn != 0) {
                return Math.abs(zzqn - zzaua.this.zznR().currentTimeMillis());
            }
            zzqk();
            return 0;
        }

        private long zzqn() {
            return zzaua.this.zzMk().getLong(this.zzbtx, 0);
        }

        public void zzcc(String str) {
            zzk(str, 1);
        }

        public void zzk(String str, long j) {
            zzaua.this.zzmR();
            if (zzqn() == 0) {
                zzqk();
            }
            if (str == null) {
                str = "";
            }
            long j2 = zzaua.this.zzagD.getLong(this.zzbty, 0);
            if (j2 <= 0) {
                SharedPreferences.Editor edit = zzaua.this.zzagD.edit();
                edit.putString(this.zzbtz, str);
                edit.putLong(this.zzbty, j);
                edit.apply();
                return;
            }
            boolean z = (zzaua.this.zzMh().nextLong() & Long.MAX_VALUE) < (Long.MAX_VALUE / (j2 + j)) * j;
            SharedPreferences.Editor edit2 = zzaua.this.zzagD.edit();
            if (z) {
                edit2.putString(this.zzbtz, str);
            }
            edit2.putLong(this.zzbty, j2 + j);
            edit2.apply();
        }

        public Pair<String, Long> zzqm() {
            zzaua.this.zzmR();
            long zzql = zzql();
            if (zzql < this.zzagH) {
                return null;
            }
            if (zzql > this.zzagH * 2) {
                zzqk();
                return null;
            }
            String string = zzaua.this.zzMk().getString(this.zzbtz, null);
            long j = zzaua.this.zzMk().getLong(this.zzbty, 0);
            zzqk();
            return (string == null || j <= 0) ? zzaua.zzbsZ : new Pair<>(string, Long.valueOf(j));
        }
    }

    zzaua(zzaue zzaue) {
        super(zzaue);
    }

    /* access modifiers changed from: private */
    public SharedPreferences zzMk() {
        zzmR();
        zzob();
        return this.zzagD;
    }

    /* access modifiers changed from: package-private */
    public void setMeasurementEnabled(boolean z) {
        zzmR();
        zzKl().zzMf().zzj("Setting measurementEnabled", Boolean.valueOf(z));
        SharedPreferences.Editor edit = zzMk().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public String zzKq() {
        zzmR();
        try {
            return com.google.firebase.iid.zzc.zzabN().getId();
        } catch (IllegalStateException e2) {
            zzKl().zzMb().log("Failed to retrieve Firebase Instance Id");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public SecureRandom zzMh() {
        zzmR();
        if (this.zzbtm == null) {
            this.zzbtm = new SecureRandom();
        }
        return this.zzbtm;
    }

    /* access modifiers changed from: package-private */
    public String zzMi() {
        byte[] bArr = new byte[16];
        zzMh().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new BigInteger(1, bArr));
    }

    /* access modifiers changed from: package-private */
    public long zzMj() {
        zzob();
        zzmR();
        long j = this.zzbtf.get();
        if (j != 0) {
            return j;
        }
        long nextInt = (long) (zzMh().nextInt(86400000) + 1);
        this.zzbtf.set(nextInt);
        return nextInt;
    }

    /* access modifiers changed from: package-private */
    public String zzMl() {
        zzmR();
        return zzMk().getString("gmp_app_id", null);
    }

    /* access modifiers changed from: package-private */
    public String zzMm() {
        String str;
        synchronized (this.zzbtl) {
            str = Math.abs(zznR().elapsedRealtime() - this.zzbtk) < 1000 ? this.zzbtj : null;
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public Boolean zzMn() {
        zzmR();
        if (!zzMk().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(zzMk().getBoolean("use_service", false));
    }

    /* access modifiers changed from: package-private */
    public void zzMo() {
        boolean z = true;
        zzmR();
        zzKl().zzMf().log("Clearing collection preferences.");
        boolean contains = zzMk().contains("measurement_enabled");
        if (contains) {
            z = zzaK(true);
        }
        SharedPreferences.Editor edit = zzMk().edit();
        edit.clear();
        edit.apply();
        if (contains) {
            setMeasurementEnabled(z);
        }
    }

    /* access modifiers changed from: protected */
    public String zzMp() {
        zzmR();
        String string = zzMk().getString("previous_os_version", null);
        String zzLS = zzKc().zzLS();
        if (!TextUtils.isEmpty(zzLS) && !zzLS.equals(string)) {
            SharedPreferences.Editor edit = zzMk().edit();
            edit.putString("previous_os_version", zzLS);
            edit.apply();
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    public void zzaJ(boolean z) {
        zzmR();
        zzKl().zzMf().zzj("Setting useService", Boolean.valueOf(z));
        SharedPreferences.Editor edit = zzMk().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public boolean zzaK(boolean z) {
        zzmR();
        return zzMk().getBoolean("measurement_enabled", z);
    }

    /* access modifiers changed from: package-private */
    public Pair<String, Boolean> zzfG(String str) {
        zzmR();
        long elapsedRealtime = zznR().elapsedRealtime();
        if (this.zzbtg != null && elapsedRealtime < this.zzbti) {
            return new Pair<>(this.zzbtg, Boolean.valueOf(this.zzbth));
        }
        this.zzbti = elapsedRealtime + zzKn().zzfm(str);
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(getContext());
            this.zzbtg = advertisingIdInfo.getId();
            if (this.zzbtg == null) {
                this.zzbtg = "";
            }
            this.zzbth = advertisingIdInfo.isLimitAdTrackingEnabled();
        } catch (Throwable th) {
            zzKl().zzMe().zzj("Unable to get advertising id", th);
            this.zzbtg = "";
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair<>(this.zzbtg, Boolean.valueOf(this.zzbth));
    }

    /* access modifiers changed from: package-private */
    public String zzfH(String str) {
        zzmR();
        String str2 = (String) zzfG(str).first;
        MessageDigest zzch = zzaut.zzch("MD5");
        if (zzch == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, zzch.digest(str2.getBytes())));
    }

    /* access modifiers changed from: package-private */
    public void zzfI(String str) {
        zzmR();
        SharedPreferences.Editor edit = zzMk().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public void zzfJ(String str) {
        synchronized (this.zzbtl) {
            this.zzbtj = str;
            this.zzbtk = zznR().elapsedRealtime();
        }
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
        this.zzagD = getContext().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.zzbts = this.zzagD.getBoolean("has_been_opened", false);
        if (!this.zzbts) {
            SharedPreferences.Editor edit = this.zzagD.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
    }
}
