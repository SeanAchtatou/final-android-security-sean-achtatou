package com.google.android.gms.internal;

public final class zzbyr implements zzbys, zzbyt, Cloneable {
    private static final byte[] zzcxT = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
    long zzaA;
    zzbyx zzcxU;

    public void close() {
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzbyr)) {
            return false;
        }
        zzbyr zzbyr = (zzbyr) obj;
        if (this.zzaA != zzbyr.zzaA) {
            return false;
        }
        if (this.zzaA == 0) {
            return true;
        }
        zzbyx zzbyx = this.zzcxU;
        zzbyx zzbyx2 = zzbyr.zzcxU;
        int i = zzbyx.pos;
        int i2 = zzbyx2.pos;
        while (j < this.zzaA) {
            long min = (long) Math.min(zzbyx.limit - i, zzbyx2.limit - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                byte b2 = zzbyx.data[i];
                int i5 = i2 + 1;
                if (b2 != zzbyx2.data[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == zzbyx.limit) {
                zzbyx = zzbyx.zzcyc;
                i = zzbyx.pos;
            }
            if (i2 == zzbyx2.limit) {
                zzbyx2 = zzbyx2.zzcyc;
                i2 = zzbyx2.pos;
            }
            j += min;
        }
        return true;
    }

    public void flush() {
    }

    public int hashCode() {
        zzbyx zzbyx = this.zzcxU;
        if (zzbyx == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = zzbyx.pos;
            int i3 = zzbyx.limit;
            while (i2 < i3) {
                i2++;
                i = zzbyx.data[i2] + (i * 31);
            }
            zzbyx = zzbyx.zzcyc;
        } while (zzbyx != this.zzcxU);
        return i;
    }

    public long read(zzbyr zzbyr, long j) {
        if (zzbyr == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.zzaA == 0) {
            return -1;
        } else {
            if (j > this.zzaA) {
                j = this.zzaA;
            }
            zzbyr.write(this, j);
            return j;
        }
    }

    public String toString() {
        return zzafU().toString();
    }

    public void write(zzbyr zzbyr, long j) {
        if (zzbyr == null) {
            throw new IllegalArgumentException("source == null");
        } else if (zzbyr == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            zzbzd.checkOffsetAndCount(zzbyr.zzaA, 0, j);
            while (j > 0) {
                if (j < ((long) (zzbyr.zzcxU.limit - zzbyr.zzcxU.pos))) {
                    zzbyx zzbyx = this.zzcxU != null ? this.zzcxU.zzcyd : null;
                    if (zzbyx != null && zzbyx.zzcyb) {
                        if ((((long) zzbyx.limit) + j) - ((long) (zzbyx.zzcya ? 0 : zzbyx.pos)) <= 8192) {
                            zzbyr.zzcxU.zza(zzbyx, (int) j);
                            zzbyr.zzaA -= j;
                            this.zzaA += j;
                            return;
                        }
                    }
                    zzbyr.zzcxU = zzbyr.zzcxU.zzrz((int) j);
                }
                zzbyx zzbyx2 = zzbyr.zzcxU;
                long j2 = (long) (zzbyx2.limit - zzbyx2.pos);
                zzbyr.zzcxU = zzbyx2.zzafX();
                if (this.zzcxU == null) {
                    this.zzcxU = zzbyx2;
                    zzbyx zzbyx3 = this.zzcxU;
                    zzbyx zzbyx4 = this.zzcxU;
                    zzbyx zzbyx5 = this.zzcxU;
                    zzbyx4.zzcyd = zzbyx5;
                    zzbyx3.zzcyc = zzbyx5;
                } else {
                    this.zzcxU.zzcyd.zza(zzbyx2).zzafY();
                }
                zzbyr.zzaA -= j2;
                this.zzaA += j2;
                j -= j2;
            }
        }
    }

    /* renamed from: zzafT */
    public zzbyr clone() {
        zzbyr zzbyr = new zzbyr();
        if (this.zzaA == 0) {
            return zzbyr;
        }
        zzbyr.zzcxU = new zzbyx(this.zzcxU);
        zzbyx zzbyx = zzbyr.zzcxU;
        zzbyx zzbyx2 = zzbyr.zzcxU;
        zzbyx zzbyx3 = zzbyr.zzcxU;
        zzbyx2.zzcyd = zzbyx3;
        zzbyx.zzcyc = zzbyx3;
        for (zzbyx zzbyx4 = this.zzcxU.zzcyc; zzbyx4 != this.zzcxU; zzbyx4 = zzbyx4.zzcyc) {
            zzbyr.zzcxU.zzcyd.zza(new zzbyx(zzbyx4));
        }
        zzbyr.zzaA = this.zzaA;
        return zzbyr;
    }

    public zzbyu zzafU() {
        if (this.zzaA <= 2147483647L) {
            return zzry((int) this.zzaA);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.zzaA);
    }

    public zzbyu zzry(int i) {
        return i == 0 ? zzbyu.zzcxW : new zzbyz(this, i);
    }
}
