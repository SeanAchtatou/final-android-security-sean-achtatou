package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zzch extends zzam {
    private static final String ID = zzah.PLATFORM.toString();
    private static final zzak.zza zzbHe = zzdl.zzS("Android");

    public zzch() {
        super(ID, new String[0]);
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        return zzbHe;
    }
}
