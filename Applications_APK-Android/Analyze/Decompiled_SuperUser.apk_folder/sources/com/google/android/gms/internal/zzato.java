package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import java.util.Iterator;

public class zzato extends zza implements Iterable<String> {
    public static final Parcelable.Creator<zzato> CREATOR = new zzatp();
    /* access modifiers changed from: private */
    public final Bundle zzbrE;

    zzato(Bundle bundle) {
        this.zzbrE = bundle;
    }

    /* access modifiers changed from: package-private */
    public Object get(String str) {
        return this.zzbrE.get(str);
    }

    public Iterator<String> iterator() {
        return new Iterator<String>() {
            Iterator<String> zzbrF = zzato.this.zzbrE.keySet().iterator();

            public boolean hasNext() {
                return this.zzbrF.hasNext();
            }

            public String next() {
                return this.zzbrF.next();
            }

            public void remove() {
                throw new UnsupportedOperationException("Remove not supported");
            }
        };
    }

    public int size() {
        return this.zzbrE.size();
    }

    public String toString() {
        return this.zzbrE.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        zzatp.zza(this, parcel, i);
    }

    public Bundle zzLW() {
        return new Bundle(this.zzbrE);
    }
}
