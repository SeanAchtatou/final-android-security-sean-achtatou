package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzak;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class zzbj extends zzam {
    private static final String ID = zzah.LANGUAGE.toString();

    public zzbj() {
        super(ID, new String[0]);
    }

    public /* bridge */ /* synthetic */ String zzQN() {
        return super.zzQN();
    }

    public /* bridge */ /* synthetic */ Set zzQO() {
        return super.zzQO();
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        Locale locale = Locale.getDefault();
        if (locale == null) {
            return zzdl.zzRT();
        }
        String language = locale.getLanguage();
        return language == null ? zzdl.zzRT() : zzdl.zzS(language.toLowerCase());
    }
}
