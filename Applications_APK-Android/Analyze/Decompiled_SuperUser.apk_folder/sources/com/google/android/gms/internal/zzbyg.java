package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class zzbyg implements Cloneable {
    private Object value;
    private zzbye<?, ?> zzcwI;
    private List<zzbyl> zzcwJ = new ArrayList();

    zzbyg() {
    }

    private byte[] toByteArray() {
        byte[] bArr = new byte[zzu()];
        zza(zzbyc.zzah(bArr));
        return bArr;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzbyg)) {
            return false;
        }
        zzbyg zzbyg = (zzbyg) obj;
        if (this.value == null || zzbyg.value == null) {
            if (this.zzcwJ != null && zzbyg.zzcwJ != null) {
                return this.zzcwJ.equals(zzbyg.zzcwJ);
            }
            try {
                return Arrays.equals(toByteArray(), zzbyg.toByteArray());
            } catch (IOException e2) {
                throw new IllegalStateException(e2);
            }
        } else if (this.zzcwI == zzbyg.zzcwI) {
            return !this.zzcwI.zzckL.isArray() ? this.value.equals(zzbyg.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zzbyg.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zzbyg.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zzbyg.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zzbyg.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zzbyg.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zzbyg.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zzbyg.value);
        } else {
            return false;
        }
    }

    public int hashCode() {
        try {
            return Arrays.hashCode(toByteArray()) + 527;
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzbyc zzbyc) {
        if (this.value != null) {
            this.zzcwI.zza(this.value, zzbyc);
            return;
        }
        for (zzbyl zza : this.zzcwJ) {
            zza.zza(zzbyc);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzbyl zzbyl) {
        this.zzcwJ.add(zzbyl);
    }

    /* renamed from: zzafs */
    public final zzbyg clone() {
        int i = 0;
        zzbyg zzbyg = new zzbyg();
        try {
            zzbyg.zzcwI = this.zzcwI;
            if (this.zzcwJ == null) {
                zzbyg.zzcwJ = null;
            } else {
                zzbyg.zzcwJ.addAll(this.zzcwJ);
            }
            if (this.value != null) {
                if (this.value instanceof zzbyj) {
                    zzbyg.value = (zzbyj) ((zzbyj) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    zzbyg.value = ((byte[]) this.value).clone();
                } else if (this.value instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.value;
                    byte[][] bArr2 = new byte[bArr.length][];
                    zzbyg.value = bArr2;
                    for (int i2 = 0; i2 < bArr.length; i2++) {
                        bArr2[i2] = (byte[]) bArr[i2].clone();
                    }
                } else if (this.value instanceof boolean[]) {
                    zzbyg.value = ((boolean[]) this.value).clone();
                } else if (this.value instanceof int[]) {
                    zzbyg.value = ((int[]) this.value).clone();
                } else if (this.value instanceof long[]) {
                    zzbyg.value = ((long[]) this.value).clone();
                } else if (this.value instanceof float[]) {
                    zzbyg.value = ((float[]) this.value).clone();
                } else if (this.value instanceof double[]) {
                    zzbyg.value = ((double[]) this.value).clone();
                } else if (this.value instanceof zzbyj[]) {
                    zzbyj[] zzbyjArr = (zzbyj[]) this.value;
                    zzbyj[] zzbyjArr2 = new zzbyj[zzbyjArr.length];
                    zzbyg.value = zzbyjArr2;
                    while (true) {
                        int i3 = i;
                        if (i3 >= zzbyjArr.length) {
                            break;
                        }
                        zzbyjArr2[i3] = (zzbyj) zzbyjArr[i3].clone();
                        i = i3 + 1;
                    }
                }
            }
            return zzbyg;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.gms.internal.zzbye<?, ?>, java.lang.Object, com.google.android.gms.internal.zzbye<?, T>, com.google.android.gms.internal.zzbye] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    <T> T zzb(com.google.android.gms.internal.zzbye<?, T> r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.value
            if (r0 == 0) goto L_0x0014
            com.google.android.gms.internal.zzbye<?, ?> r0 = r2.zzcwI
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x0021
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Tried to getExtension with a different Extension."
            r0.<init>(r1)
            throw r0
        L_0x0014:
            r2.zzcwI = r3
            java.util.List<com.google.android.gms.internal.zzbyl> r0 = r2.zzcwJ
            java.lang.Object r0 = r3.zzad(r0)
            r2.value = r0
            r0 = 0
            r2.zzcwJ = r0
        L_0x0021:
            java.lang.Object r0 = r2.value
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzbyg.zzb(com.google.android.gms.internal.zzbye):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public int zzu() {
        int i = 0;
        if (this.value != null) {
            return this.zzcwI.zzaV(this.value);
        }
        Iterator<zzbyl> it = this.zzcwJ.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().zzu() + i2;
        }
    }
}
