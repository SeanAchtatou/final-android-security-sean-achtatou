package com.google.android.gms.tagmanager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataLayer {
    public static final String EVENT_KEY = "event";
    public static final Object OBJECT_NOT_PRESENT = new Object();
    static final String[] zzbFE = "gtm.lifetime".toString().split("\\.");
    private static final Pattern zzbFF = Pattern.compile("(\\d+)\\s*([smhd]?)");
    private final ConcurrentHashMap<zzb, Integer> zzbFG;
    private final Map<String, Object> zzbFH;
    private final ReentrantLock zzbFI;
    private final LinkedList<Map<String, Object>> zzbFJ;
    private final zzc zzbFK;
    /* access modifiers changed from: private */
    public final CountDownLatch zzbFL;

    static final class zza {
        public final Object mValue;
        public final String zzAX;

        zza(String str, Object obj) {
            this.zzAX = str;
            this.mValue = obj;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            return this.zzAX.equals(zza.zzAX) && this.mValue.equals(zza.mValue);
        }

        public int hashCode() {
            return Arrays.hashCode(new Integer[]{Integer.valueOf(this.zzAX.hashCode()), Integer.valueOf(this.mValue.hashCode())});
        }

        public String toString() {
            String str = this.zzAX;
            String valueOf = String.valueOf(this.mValue.toString());
            return new StringBuilder(String.valueOf(str).length() + 13 + String.valueOf(valueOf).length()).append("Key: ").append(str).append(" value: ").append(valueOf).toString();
        }
    }

    interface zzb {
        void zzaa(Map<String, Object> map);
    }

    interface zzc {

        public interface zza {
            void zzM(List<zza> list);
        }

        void zza(zza zza2);

        void zza(List<zza> list, long j);

        void zzhc(String str);
    }

    DataLayer() {
        this(new zzc() {
            public void zza(zzc.zza zza) {
                zza.zzM(new ArrayList());
            }

            public void zza(List<zza> list, long j) {
            }

            public void zzhc(String str) {
            }
        });
    }

    DataLayer(zzc zzc2) {
        this.zzbFK = zzc2;
        this.zzbFG = new ConcurrentHashMap<>();
        this.zzbFH = new HashMap();
        this.zzbFI = new ReentrantLock();
        this.zzbFJ = new LinkedList<>();
        this.zzbFL = new CountDownLatch(1);
        zzQy();
    }

    public static List<Object> listOf(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        for (Object add : objArr) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public static Map<String, Object> mapOf(Object... objArr) {
        if (objArr.length % 2 != 0) {
            throw new IllegalArgumentException("expected even number of key-value pairs");
        }
        HashMap hashMap = new HashMap();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length) {
                return hashMap;
            }
            if (!(objArr[i2] instanceof String)) {
                String valueOf = String.valueOf(objArr[i2]);
                throw new IllegalArgumentException(new StringBuilder(String.valueOf(valueOf).length() + 21).append("key is not a string: ").append(valueOf).toString());
            }
            hashMap.put((String) objArr[i2], objArr[i2 + 1]);
            i = i2 + 2;
        }
    }

    private void zzQy() {
        this.zzbFK.zza(new zzc.zza() {
            public void zzM(List<zza> list) {
                for (zza next : list) {
                    DataLayer.this.zzac(DataLayer.this.zzn(next.zzAX, next.mValue));
                }
                DataLayer.this.zzbFL.countDown();
            }
        });
    }

    private void zzQz() {
        int i = 0;
        while (true) {
            int i2 = i;
            Map poll = this.zzbFJ.poll();
            if (poll != null) {
                zzah(poll);
                i = i2 + 1;
                if (i > 500) {
                    this.zzbFJ.clear();
                    throw new RuntimeException("Seems like an infinite loop of pushing to the data layer");
                }
            } else {
                return;
            }
        }
    }

    private void zza(Map<String, Object> map, String str, Collection<zza> collection) {
        for (Map.Entry next : map.entrySet()) {
            String str2 = str.length() == 0 ? "" : ".";
            String str3 = (String) next.getKey();
            String sb = new StringBuilder(String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str3).length()).append(str).append(str2).append(str3).toString();
            if (next.getValue() instanceof Map) {
                zza((Map) next.getValue(), sb, collection);
            } else if (!sb.equals("gtm.lifetime")) {
                collection.add(new zza(sb, next.getValue()));
            }
        }
    }

    /* access modifiers changed from: private */
    public void zzac(Map<String, Object> map) {
        this.zzbFI.lock();
        try {
            this.zzbFJ.offer(map);
            if (this.zzbFI.getHoldCount() == 1) {
                zzQz();
            }
            zzad(map);
        } finally {
            this.zzbFI.unlock();
        }
    }

    private void zzad(Map<String, Object> map) {
        Long zzae = zzae(map);
        if (zzae != null) {
            this.zzbFK.zza(zzag(map), zzae.longValue());
        }
    }

    private Long zzae(Map<String, Object> map) {
        Object zzaf = zzaf(map);
        if (zzaf == null) {
            return null;
        }
        return zzhb(zzaf.toString());
    }

    private Object zzaf(Map<String, Object> map) {
        String[] strArr = zzbFE;
        int length = strArr.length;
        int i = 0;
        Object obj = map;
        while (i < length) {
            String str = strArr[i];
            if (!(obj instanceof Map)) {
                return null;
            }
            i++;
            obj = ((Map) obj).get(str);
        }
        return obj;
    }

    private List<zza> zzag(Map<String, Object> map) {
        ArrayList arrayList = new ArrayList();
        zza(map, "", arrayList);
        return arrayList;
    }

    private void zzah(Map<String, Object> map) {
        synchronized (this.zzbFH) {
            for (String next : map.keySet()) {
                zzd(zzn(next, map.get(next)), this.zzbFH);
            }
        }
        zzai(map);
    }

    private void zzai(Map<String, Object> map) {
        for (zzb zzaa : this.zzbFG.keySet()) {
            zzaa.zzaa(map);
        }
    }

    static Long zzhb(String str) {
        long j;
        Matcher matcher = zzbFF.matcher(str);
        if (!matcher.matches()) {
            String valueOf = String.valueOf(str);
            zzbo.zzbg(valueOf.length() != 0 ? "unknown _lifetime: ".concat(valueOf) : new String("unknown _lifetime: "));
            return null;
        }
        try {
            j = Long.parseLong(matcher.group(1));
        } catch (NumberFormatException e2) {
            String valueOf2 = String.valueOf(str);
            zzbo.zzbh(valueOf2.length() != 0 ? "illegal number in _lifetime value: ".concat(valueOf2) : new String("illegal number in _lifetime value: "));
            j = 0;
        }
        if (j <= 0) {
            String valueOf3 = String.valueOf(str);
            zzbo.zzbg(valueOf3.length() != 0 ? "non-positive _lifetime: ".concat(valueOf3) : new String("non-positive _lifetime: "));
            return null;
        }
        String group = matcher.group(2);
        if (group.length() == 0) {
            return Long.valueOf(j);
        }
        switch (group.charAt(0)) {
            case 'd':
                return Long.valueOf(j * 1000 * 60 * 60 * 24);
            case 'h':
                return Long.valueOf(j * 1000 * 60 * 60);
            case 'm':
                return Long.valueOf(j * 1000 * 60);
            case 's':
                return Long.valueOf(j * 1000);
            default:
                String valueOf4 = String.valueOf(str);
                zzbo.zzbh(valueOf4.length() != 0 ? "unknown units in _lifetime: ".concat(valueOf4) : new String("unknown units in _lifetime: "));
                return null;
        }
    }

    public Object get(String str) {
        synchronized (this.zzbFH) {
            Object obj = this.zzbFH;
            String[] split = str.split("\\.");
            int length = split.length;
            Object obj2 = obj;
            int i = 0;
            while (i < length) {
                String str2 = split[i];
                if (!(obj2 instanceof Map)) {
                    return null;
                }
                Object obj3 = ((Map) obj2).get(str2);
                if (obj3 == null) {
                    return null;
                }
                i++;
                obj2 = obj3;
            }
            return obj2;
        }
    }

    public void push(String str, Object obj) {
        push(zzn(str, obj));
    }

    public void push(Map<String, Object> map) {
        try {
            this.zzbFL.await();
        } catch (InterruptedException e2) {
            zzbo.zzbh("DataLayer.push: unexpected InterruptedException");
        }
        zzac(map);
    }

    public void pushEvent(String str, Map<String, Object> map) {
        HashMap hashMap = new HashMap(map);
        hashMap.put(EVENT_KEY, str);
        push(hashMap);
    }

    public String toString() {
        String sb;
        synchronized (this.zzbFH) {
            StringBuilder sb2 = new StringBuilder();
            for (Map.Entry next : this.zzbFH.entrySet()) {
                sb2.append(String.format("{\n\tKey: %s\n\tValue: %s\n}\n", next.getKey(), next.getValue()));
            }
            sb = sb2.toString();
        }
        return sb;
    }

    /* access modifiers changed from: package-private */
    public void zza(zzb zzb2) {
        this.zzbFG.put(zzb2, 0);
    }

    /* access modifiers changed from: package-private */
    public void zzb(List<Object> list, List<Object> list2) {
        while (list2.size() < list.size()) {
            list2.add(null);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                Object obj = list.get(i2);
                if (obj instanceof List) {
                    if (!(list2.get(i2) instanceof List)) {
                        list2.set(i2, new ArrayList());
                    }
                    zzb((List) obj, (List) list2.get(i2));
                } else if (obj instanceof Map) {
                    if (!(list2.get(i2) instanceof Map)) {
                        list2.set(i2, new HashMap());
                    }
                    zzd((Map) obj, (Map) list2.get(i2));
                } else if (obj != OBJECT_NOT_PRESENT) {
                    list2.set(i2, obj);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzd(Map<String, Object> map, Map<String, Object> map2) {
        for (String next : map.keySet()) {
            Object obj = map.get(next);
            if (obj instanceof List) {
                if (!(map2.get(next) instanceof List)) {
                    map2.put(next, new ArrayList());
                }
                zzb((List) obj, (List) map2.get(next));
            } else if (obj instanceof Map) {
                if (!(map2.get(next) instanceof Map)) {
                    map2.put(next, new HashMap());
                }
                zzd((Map) obj, (Map) map2.get(next));
            } else {
                map2.put(next, obj);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzha(String str) {
        push(str, null);
        this.zzbFK.zzhc(str);
    }

    /* access modifiers changed from: package-private */
    public Map<String, Object> zzn(String str, Object obj) {
        HashMap hashMap = new HashMap();
        String[] split = str.toString().split("\\.");
        int i = 0;
        HashMap hashMap2 = hashMap;
        while (i < split.length - 1) {
            HashMap hashMap3 = new HashMap();
            hashMap2.put(split[i], hashMap3);
            i++;
            hashMap2 = hashMap3;
        }
        hashMap2.put(split[split.length - 1], obj);
        return hashMap;
    }
}
