package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

class zza<TResult, TContinuationResult> implements zzf<TResult> {
    private final Executor zzbFP;
    /* access modifiers changed from: private */
    public final Continuation<TResult, TContinuationResult> zzbNs;
    /* access modifiers changed from: private */
    public final zzh<TContinuationResult> zzbNt;

    public zza(Executor executor, Continuation<TResult, TContinuationResult> continuation, zzh<TContinuationResult> zzh) {
        this.zzbFP = executor;
        this.zzbNs = continuation;
        this.zzbNt = zzh;
    }

    public void cancel() {
        throw new UnsupportedOperationException();
    }

    public void onComplete(final Task<TResult> task) {
        this.zzbFP.execute(new Runnable() {
            public void run() {
                try {
                    zza.this.zzbNt.setResult(zza.this.zzbNs.then(task));
                } catch (RuntimeExecutionException e2) {
                    if (e2.getCause() instanceof Exception) {
                        zza.this.zzbNt.setException((Exception) e2.getCause());
                    } else {
                        zza.this.zzbNt.setException(e2);
                    }
                } catch (Exception e3) {
                    zza.this.zzbNt.setException(e3);
                }
            }
        });
    }
}
