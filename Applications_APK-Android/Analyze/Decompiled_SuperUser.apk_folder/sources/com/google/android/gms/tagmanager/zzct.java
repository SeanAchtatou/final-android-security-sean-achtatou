package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzbjf;
import com.google.android.gms.internal.zzbji;
import com.google.android.gms.internal.zzbjj;
import com.google.android.gms.internal.zzbjk;
import com.google.android.gms.tagmanager.zzbn;
import com.google.android.gms.tagmanager.zzcj;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

class zzct implements Runnable {
    private final Context mContext;
    private final String zzbEX;
    private volatile String zzbFv;
    private final zzbjj zzbHu;
    private final String zzbHv;
    private zzbn<zzaj.zzj> zzbHw;
    private volatile zzt zzbHx;
    private volatile String zzbHy;

    zzct(Context context, String str, zzbjj zzbjj, zzt zzt) {
        this.mContext = context;
        this.zzbHu = zzbjj;
        this.zzbEX = str;
        this.zzbHx = zzt;
        String valueOf = String.valueOf("/r?id=");
        String valueOf2 = String.valueOf(str);
        this.zzbHv = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        this.zzbFv = this.zzbHv;
        this.zzbHy = null;
    }

    public zzct(Context context, String str, zzt zzt) {
        this(context, str, new zzbjj(), zzt);
    }

    private boolean zzRj() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        zzbo.v("...no network connectivity");
        return false;
    }

    private void zzRk() {
        if (!zzRj()) {
            this.zzbHw.zza(zzbn.zza.NOT_AVAILABLE);
            return;
        }
        zzbo.v("Start loading resource from network ...");
        String zzRl = zzRl();
        zzbji zzTH = this.zzbHu.zzTH();
        InputStream inputStream = null;
        try {
            inputStream = zzTH.zzhX(zzRl);
        } catch (FileNotFoundException e2) {
            String str = this.zzbEX;
            zzbo.zzbh(new StringBuilder(String.valueOf(zzRl).length() + 79 + String.valueOf(str).length()).append("No data is retrieved from the given url: ").append(zzRl).append(". Make sure container_id: ").append(str).append(" is correct.").toString());
            this.zzbHw.zza(zzbn.zza.SERVER_ERROR);
            zzTH.close();
            return;
        } catch (zzbjk e3) {
            String valueOf = String.valueOf(zzRl);
            zzbo.zzbh(valueOf.length() != 0 ? "Error when loading resource for url: ".concat(valueOf) : new String("Error when loading resource for url: "));
            this.zzbHw.zza(zzbn.zza.SERVER_UNAVAILABLE_ERROR);
        } catch (IOException e4) {
            String valueOf2 = String.valueOf(e4.getMessage());
            zzbo.zzc(new StringBuilder(String.valueOf(zzRl).length() + 40 + String.valueOf(valueOf2).length()).append("Error when loading resources from url: ").append(zzRl).append(" ").append(valueOf2).toString(), e4);
            this.zzbHw.zza(zzbn.zza.IO_ERROR);
            zzTH.close();
            return;
        } catch (Throwable th) {
            zzTH.close();
            throw th;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            zzbjf.zzc(inputStream, byteArrayOutputStream);
            zzaj.zzj zzg = zzaj.zzj.zzg(byteArrayOutputStream.toByteArray());
            String valueOf3 = String.valueOf(zzg);
            zzbo.v(new StringBuilder(String.valueOf(valueOf3).length() + 43).append("Successfully loaded supplemented resource: ").append(valueOf3).toString());
            if (zzg.zzlr == null && zzg.zzlq.length == 0) {
                String valueOf4 = String.valueOf(this.zzbEX);
                zzbo.v(valueOf4.length() != 0 ? "No change for container: ".concat(valueOf4) : new String("No change for container: "));
            }
            this.zzbHw.onSuccess(zzg);
            zzTH.close();
            zzbo.v("Load resource from network finished.");
        } catch (IOException e5) {
            String valueOf5 = String.valueOf(e5.getMessage());
            zzbo.zzc(new StringBuilder(String.valueOf(zzRl).length() + 51 + String.valueOf(valueOf5).length()).append("Error when parsing downloaded resources from url: ").append(zzRl).append(" ").append(valueOf5).toString(), e5);
            this.zzbHw.zza(zzbn.zza.SERVER_ERROR);
            zzTH.close();
        }
    }

    public void run() {
        if (this.zzbHw == null) {
            throw new IllegalStateException("callback must be set before execute");
        }
        zzRk();
    }

    /* access modifiers changed from: package-private */
    public String zzRl() {
        String valueOf = String.valueOf(this.zzbHx.zzQx());
        String str = this.zzbFv;
        String valueOf2 = String.valueOf("&v=a65833898");
        String sb = new StringBuilder(String.valueOf(valueOf).length() + String.valueOf(str).length() + String.valueOf(valueOf2).length()).append(valueOf).append(str).append(valueOf2).toString();
        if (this.zzbHy != null && !this.zzbHy.trim().equals("")) {
            String valueOf3 = String.valueOf(sb);
            String valueOf4 = String.valueOf("&pv=");
            String str2 = this.zzbHy;
            sb = new StringBuilder(String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length() + String.valueOf(str2).length()).append(valueOf3).append(valueOf4).append(str2).toString();
        }
        if (!zzcj.zzRg().zzRh().equals(zzcj.zza.CONTAINER_DEBUG)) {
            return sb;
        }
        String valueOf5 = String.valueOf(sb);
        String valueOf6 = String.valueOf("&gtm_debug=x");
        return valueOf6.length() != 0 ? valueOf5.concat(valueOf6) : new String(valueOf5);
    }

    /* access modifiers changed from: package-private */
    public void zza(zzbn<zzaj.zzj> zzbn) {
        this.zzbHw = zzbn;
    }

    /* access modifiers changed from: package-private */
    public void zzgZ(String str) {
        if (str == null) {
            this.zzbFv = this.zzbHv;
            return;
        }
        String valueOf = String.valueOf(str);
        zzbo.zzbf(valueOf.length() != 0 ? "Setting CTFE URL path: ".concat(valueOf) : new String("Setting CTFE URL path: "));
        this.zzbFv = str;
    }

    /* access modifiers changed from: package-private */
    public void zzho(String str) {
        String valueOf = String.valueOf(str);
        zzbo.zzbf(valueOf.length() != 0 ? "Setting previous container version: ".concat(valueOf) : new String("Setting previous container version: "));
        this.zzbHy = str;
    }
}
