package com.google.android.gms.tagmanager;

class zzce<T> {
    private final T zzbGT;
    private final boolean zzbGU;

    zzce(T t, boolean z) {
        this.zzbGT = t;
        this.zzbGU = z;
    }

    public T getObject() {
        return this.zzbGT;
    }

    public boolean zzRb() {
        return this.zzbGU;
    }
}
