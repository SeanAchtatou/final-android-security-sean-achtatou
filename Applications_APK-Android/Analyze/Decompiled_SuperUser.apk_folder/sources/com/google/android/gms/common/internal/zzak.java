package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.a;
import android.util.AttributeSet;
import android.widget.Button;
import com.google.android.gms.R;

public final class zzak extends Button {
    public zzak(Context context) {
        this(context, null);
    }

    public zzak(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842824);
    }

    private void zza(Resources resources) {
        setTypeface(Typeface.DEFAULT_BOLD);
        setTextSize(14.0f);
        float f2 = resources.getDisplayMetrics().density;
        setMinHeight((int) ((f2 * 48.0f) + 0.5f));
        setMinWidth((int) ((f2 * 48.0f) + 0.5f));
    }

    private void zzb(Resources resources, int i, int i2) {
        Drawable g2 = a.g(resources.getDrawable(zze(i, zzg(i2, R.drawable.bb, R.drawable.bf, R.drawable.bf), zzg(i2, R.drawable.bi, R.drawable.bm, R.drawable.bm))));
        a.a(g2, resources.getColorStateList(R.color.eo));
        a.a(g2, PorterDuff.Mode.SRC_ATOP);
        setBackgroundDrawable(g2);
    }

    private void zzc(Resources resources, int i, int i2) {
        setTextColor((ColorStateList) zzac.zzw(resources.getColorStateList(zzg(i2, R.color.em, R.color.en, R.color.en))));
        switch (i) {
            case 0:
                setText(resources.getString(R.string.a8));
                break;
            case 1:
                setText(resources.getString(R.string.a9));
                break;
            case 2:
                setText((CharSequence) null);
                break;
            default:
                throw new IllegalStateException(new StringBuilder(32).append("Unknown button size: ").append(i).toString());
        }
        setTransformationMethod(null);
    }

    private int zze(int i, int i2, int i3) {
        switch (i) {
            case 0:
            case 1:
                return i3;
            case 2:
                return i2;
            default:
                throw new IllegalStateException(new StringBuilder(32).append("Unknown button size: ").append(i).toString());
        }
    }

    private int zzg(int i, int i2, int i3, int i4) {
        switch (i) {
            case 0:
                return i2;
            case 1:
                return i3;
            case 2:
                return i4;
            default:
                throw new IllegalStateException(new StringBuilder(33).append("Unknown color scheme: ").append(i).toString());
        }
    }

    public void zza(Resources resources, int i, int i2) {
        zza(resources);
        zzb(resources, i, i2);
        zzc(resources, i, i2);
    }
}
