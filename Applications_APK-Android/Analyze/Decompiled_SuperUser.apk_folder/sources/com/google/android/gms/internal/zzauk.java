package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class zzauk extends zzauh {
    protected zza zzbvp;
    private volatile AppMeasurement.zzf zzbvq;
    private AppMeasurement.zzf zzbvr;
    private long zzbvs;
    private final Map<Activity, zza> zzbvt = new ArrayMap();
    private final CopyOnWriteArrayList<AppMeasurement.zzd> zzbvu = new CopyOnWriteArrayList<>();
    private boolean zzbvv;
    private AppMeasurement.zzf zzbvw;
    private String zzbvx;

    static class zza extends AppMeasurement.zzf {
        public boolean zzbvC;

        public zza(zza zza) {
            this.zzbqe = zza.zzbqe;
            this.zzbqf = zza.zzbqf;
            this.zzbqg = zza.zzbqg;
            this.zzbvC = zza.zzbvC;
        }

        public zza(String str, String str2, long j) {
            this.zzbqe = str;
            this.zzbqf = str2;
            this.zzbqg = j;
            this.zzbvC = false;
        }
    }

    public zzauk(zzaue zzaue) {
        super(zzaue);
    }

    private void zza(Activity activity, zza zza2, final boolean z) {
        boolean z2;
        boolean z3 = true;
        AppMeasurement.zzf zzf = this.zzbvq != null ? this.zzbvq : (this.zzbvr == null || Math.abs(zznR().elapsedRealtime() - this.zzbvs) >= 1000) ? null : this.zzbvr;
        AppMeasurement.zzf zzf2 = zzf != null ? new AppMeasurement.zzf(zzf) : null;
        this.zzbvv = true;
        try {
            Iterator<AppMeasurement.zzd> it = this.zzbvu.iterator();
            while (it.hasNext()) {
                try {
                    z2 = it.next().zza(zzf2, zza2) & z3;
                } catch (Exception e2) {
                    zzKl().zzLZ().zzj("onScreenChangeCallback threw exception", e2);
                    z2 = z3;
                }
                z3 = z2;
            }
        } catch (Exception e3) {
            zzKl().zzLZ().zzj("onScreenChangeCallback loop threw exception", e3);
        } finally {
            this.zzbvv = false;
        }
        if (z3) {
            if (zza2.zzbqf == null) {
                zza2.zzbqf = zzfS(activity.getClass().getCanonicalName());
            }
            final zza zza3 = new zza(zza2);
            this.zzbvr = this.zzbvq;
            this.zzbvs = zznR().elapsedRealtime();
            this.zzbvq = zza3;
            zzKk().zzm(new Runnable() {
                public void run() {
                    if (z && zzauk.this.zzbvp != null) {
                        zzauk.this.zza(zzauk.this.zzbvp);
                    }
                    zzauk.this.zzbvp = zza3;
                    zzauk.this.zzKd().zza(zza3);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void zza(zza zza2) {
        zzJY().zzW(zznR().elapsedRealtime());
        if (zzKj().zzaN(zza2.zzbvC)) {
            zza2.zzbvC = false;
        }
    }

    public static void zza(AppMeasurement.zzf zzf, Bundle bundle) {
        if (bundle != null && zzf != null && !bundle.containsKey("_sc")) {
            if (zzf.zzbqe != null) {
                bundle.putString("_sn", zzf.zzbqe);
            }
            bundle.putString("_sc", zzf.zzbqf);
            bundle.putLong("_si", zzf.zzbqg);
        }
    }

    static String zzfS(String str) {
        String[] split = str.split("\\.");
        if (split.length == 0) {
            return str.substring(0, 36);
        }
        String str2 = split[split.length - 1];
        return str2.length() > 36 ? str2.substring(0, 36) : str2;
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.firebase.analytics.screen_service")) != null) {
            zza zzv = zzv(activity);
            zzv.zzbqg = bundle2.getLong("id");
            zzv.zzbqe = bundle2.getString("name");
            zzv.zzbqf = bundle2.getString("referrer_name");
        }
    }

    public void onActivityDestroyed(Activity activity) {
        this.zzbvt.remove(activity);
    }

    public void onActivityPaused(Activity activity) {
        final zza zzv = zzv(activity);
        this.zzbvr = this.zzbvq;
        this.zzbvs = zznR().elapsedRealtime();
        this.zzbvq = null;
        zzKk().zzm(new Runnable() {
            public void run() {
                zzauk.this.zza(zzv);
                zzauk.this.zzbvp = null;
                zzauk.this.zzKd().zza((AppMeasurement.zzf) null);
            }
        });
    }

    public void onActivityResumed(Activity activity) {
        zza(activity, zzv(activity), false);
        zzJY().zzJU();
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        zza zza2;
        if (bundle != null && (zza2 = this.zzbvt.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", zza2.zzbqg);
            bundle2.putString("name", zza2.zzbqe);
            bundle2.putString("referrer_name", zza2.zzbqf);
            bundle.putBundle("com.google.firebase.analytics.screen_service", bundle2);
        }
    }

    public void registerOnScreenChangeCallback(AppMeasurement.zzd zzd) {
        zzJW();
        if (zzd == null) {
            zzKl().zzMb().log("Attempting to register null OnScreenChangeCallback");
            return;
        }
        this.zzbvu.remove(zzd);
        this.zzbvu.add(zzd);
    }

    public void setCurrentScreen(Activity activity, String str, String str2) {
        int i = Build.VERSION.SDK_INT;
        if (activity == null) {
            zzKl().zzMb().log("setCurrentScreen must be called with a non-null activity");
        } else if (!zzKk().zzbc()) {
            zzKl().zzMb().log("setCurrentScreen must be called from the main thread");
        } else if (this.zzbvv) {
            zzKl().zzMb().log("Cannot call setCurrentScreen from onScreenChangeCallback");
        } else if (this.zzbvq == null) {
            zzKl().zzMb().log("setCurrentScreen cannot be called while no activity active");
        } else if (this.zzbvt.get(activity) == null) {
            zzKl().zzMb().log("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = zzfS(activity.getClass().getCanonicalName());
            }
            boolean equals = this.zzbvq.zzbqf.equals(str2);
            boolean z = (this.zzbvq.zzbqe == null && str == null) || (this.zzbvq.zzbqe != null && this.zzbvq.zzbqe.equals(str));
            if (equals && z) {
                zzKl().zzMc().log("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() < 1 || str.length() > zzKn().zzKP())) {
                zzKl().zzMb().zzj("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() >= 1 && str2.length() <= zzKn().zzKP())) {
                zzKl().zzMf().zze("Setting current screen to name, class", str == null ? "null" : str, str2);
                zza zza2 = new zza(str, str2, zzKh().zzNk());
                this.zzbvt.put(activity, zza2);
                zza(activity, zza2, true);
            } else {
                zzKl().zzMb().zzj("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    public void unregisterOnScreenChangeCallback(AppMeasurement.zzd zzd) {
        zzJW();
        this.zzbvu.remove(zzd);
    }

    public /* bridge */ /* synthetic */ void zzJV() {
        super.zzJV();
    }

    public /* bridge */ /* synthetic */ void zzJW() {
        super.zzJW();
    }

    public /* bridge */ /* synthetic */ void zzJX() {
        super.zzJX();
    }

    public /* bridge */ /* synthetic */ zzatb zzJY() {
        return super.zzJY();
    }

    public /* bridge */ /* synthetic */ zzatf zzJZ() {
        return super.zzJZ();
    }

    public /* bridge */ /* synthetic */ zzauj zzKa() {
        return super.zzKa();
    }

    public /* bridge */ /* synthetic */ zzatu zzKb() {
        return super.zzKb();
    }

    public /* bridge */ /* synthetic */ zzatl zzKc() {
        return super.zzKc();
    }

    public /* bridge */ /* synthetic */ zzaul zzKd() {
        return super.zzKd();
    }

    public /* bridge */ /* synthetic */ zzauk zzKe() {
        return super.zzKe();
    }

    public /* bridge */ /* synthetic */ zzatv zzKf() {
        return super.zzKf();
    }

    public /* bridge */ /* synthetic */ zzatj zzKg() {
        return super.zzKg();
    }

    public /* bridge */ /* synthetic */ zzaut zzKh() {
        return super.zzKh();
    }

    public /* bridge */ /* synthetic */ zzauc zzKi() {
        return super.zzKi();
    }

    public /* bridge */ /* synthetic */ zzaun zzKj() {
        return super.zzKj();
    }

    public /* bridge */ /* synthetic */ zzaud zzKk() {
        return super.zzKk();
    }

    public /* bridge */ /* synthetic */ zzatx zzKl() {
        return super.zzKl();
    }

    public /* bridge */ /* synthetic */ zzaua zzKm() {
        return super.zzKm();
    }

    public /* bridge */ /* synthetic */ zzati zzKn() {
        return super.zzKn();
    }

    public zza zzMW() {
        zzob();
        zzmR();
        return this.zzbvp;
    }

    public AppMeasurement.zzf zzMX() {
        zzJW();
        AppMeasurement.zzf zzf = this.zzbvq;
        if (zzf == null) {
            return null;
        }
        return new AppMeasurement.zzf(zzf);
    }

    public void zza(String str, AppMeasurement.zzf zzf) {
        zzmR();
        synchronized (this) {
            if (this.zzbvx == null || this.zzbvx.equals(str) || zzf != null) {
                this.zzbvx = str;
                this.zzbvw = zzf;
            }
        }
    }

    public /* bridge */ /* synthetic */ void zzmR() {
        super.zzmR();
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
    }

    public /* bridge */ /* synthetic */ zze zznR() {
        return super.zznR();
    }

    /* access modifiers changed from: package-private */
    public zza zzv(Activity activity) {
        zzac.zzw(activity);
        zza zza2 = this.zzbvt.get(activity);
        if (zza2 != null) {
            return zza2;
        }
        zza zza3 = new zza(null, zzfS(activity.getClass().getCanonicalName()), zzKh().zzNk());
        this.zzbvt.put(activity, zza3);
        return zza3;
    }
}
