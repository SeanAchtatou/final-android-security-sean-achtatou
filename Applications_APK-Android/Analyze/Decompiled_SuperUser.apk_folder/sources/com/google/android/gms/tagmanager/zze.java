package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.Map;

class zze extends zzam {
    private static final String ID = zzah.ADWORDS_CLICK_REFERRER.toString();
    private static final String zzbEP = zzai.COMPONENT.toString();
    private static final String zzbEQ = zzai.CONVERSION_ID.toString();
    private final Context zzqn;

    public zze(Context context) {
        super(ID, zzbEQ);
        this.zzqn = context;
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        zzak.zza zza = map.get(zzbEQ);
        if (zza == null) {
            return zzdl.zzRT();
        }
        String zze = zzdl.zze(zza);
        zzak.zza zza2 = map.get(zzbEP);
        String zzr = zzbf.zzr(this.zzqn, zze, zza2 != null ? zzdl.zze(zza2) : null);
        return zzr != null ? zzdl.zzS(zzr) : zzdl.zzRT();
    }
}
