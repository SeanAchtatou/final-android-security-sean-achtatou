package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.common.util.zzi;
import java.io.IOException;

public class zza {
    private static Object zzbEL = new Object();
    private static zza zzbEM;
    private volatile boolean mClosed;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Thread zzXh;
    private volatile AdvertisingIdClient.Info zzadB;
    private volatile long zzbEF;
    private volatile long zzbEG;
    private volatile long zzbEH;
    private volatile long zzbEI;
    private final Object zzbEJ;
    private C0082zza zzbEK;
    private final zze zzuP;

    /* renamed from: com.google.android.gms.tagmanager.zza$zza  reason: collision with other inner class name */
    public interface C0082zza {
        AdvertisingIdClient.Info zzQc();
    }

    private zza(Context context) {
        this(context, null, zzi.zzzc());
    }

    public zza(Context context, C0082zza zza, zze zze) {
        this.zzbEF = 900000;
        this.zzbEG = 30000;
        this.mClosed = false;
        this.zzbEJ = new Object();
        this.zzbEK = new C0082zza() {
            public AdvertisingIdClient.Info zzQc() {
                try {
                    return AdvertisingIdClient.getAdvertisingIdInfo(zza.this.mContext);
                } catch (IllegalStateException e2) {
                    zzbo.zzc("IllegalStateException getting Advertising Id Info", e2);
                    return null;
                } catch (GooglePlayServicesRepairableException e3) {
                    zzbo.zzc("GooglePlayServicesRepairableException getting Advertising Id Info", e3);
                    return null;
                } catch (IOException e4) {
                    zzbo.zzc("IOException getting Ad Id Info", e4);
                    return null;
                } catch (GooglePlayServicesNotAvailableException e5) {
                    zzbo.zzc("GooglePlayServicesNotAvailableException getting Advertising Id Info", e5);
                    return null;
                } catch (Exception e6) {
                    zzbo.zzc("Unknown exception. Could not get the Advertising Id Info.", e6);
                    return null;
                }
            }
        };
        this.zzuP = zze;
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        if (zza != null) {
            this.zzbEK = zza;
        }
        this.zzbEH = this.zzuP.currentTimeMillis();
        this.zzXh = new Thread(new Runnable() {
            public void run() {
                zza.this.zzQb();
            }
        });
    }

    private void zzPY() {
        synchronized (this) {
            try {
                zzPZ();
                wait(500);
            } catch (InterruptedException e2) {
            }
        }
    }

    private void zzPZ() {
        if (this.zzuP.currentTimeMillis() - this.zzbEH > this.zzbEG) {
            synchronized (this.zzbEJ) {
                this.zzbEJ.notify();
            }
            this.zzbEH = this.zzuP.currentTimeMillis();
        }
    }

    private void zzQa() {
        if (this.zzuP.currentTimeMillis() - this.zzbEI > 3600000) {
            this.zzadB = null;
        }
    }

    /* access modifiers changed from: private */
    public void zzQb() {
        Process.setThreadPriority(10);
        while (true) {
            boolean z = this.mClosed;
            AdvertisingIdClient.Info zzQc = this.zzbEK.zzQc();
            if (zzQc != null) {
                this.zzadB = zzQc;
                this.zzbEI = this.zzuP.currentTimeMillis();
                zzbo.zzbg("Obtained fresh AdvertisingId info from GmsCore.");
            }
            synchronized (this) {
                notifyAll();
            }
            try {
                synchronized (this.zzbEJ) {
                    this.zzbEJ.wait(this.zzbEF);
                }
            } catch (InterruptedException e2) {
                zzbo.zzbg("sleep interrupted in AdvertiserDataPoller thread; continuing");
            }
        }
    }

    public static zza zzbS(Context context) {
        if (zzbEM == null) {
            synchronized (zzbEL) {
                if (zzbEM == null) {
                    zzbEM = new zza(context);
                    zzbEM.start();
                }
            }
        }
        return zzbEM;
    }

    public boolean isLimitAdTrackingEnabled() {
        if (this.zzadB == null) {
            zzPY();
        } else {
            zzPZ();
        }
        zzQa();
        if (this.zzadB == null) {
            return true;
        }
        return this.zzadB.isLimitAdTrackingEnabled();
    }

    public void start() {
        this.zzXh.start();
    }

    public String zzPX() {
        if (this.zzadB == null) {
            zzPY();
        } else {
            zzPZ();
        }
        zzQa();
        if (this.zzadB == null) {
            return null;
        }
        return this.zzadB.getId();
    }
}
