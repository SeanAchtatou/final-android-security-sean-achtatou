package com.google.android.gms.tagmanager;

import android.content.Context;

public class zzaa implements zzat {
    private static final Object zzbEL = new Object();
    private static zzaa zzbFZ;
    private zzcl zzbFn;
    private zzau zzbGa;

    private zzaa(Context context) {
        this(zzav.zzca(context), new zzda());
    }

    zzaa(zzau zzau, zzcl zzcl) {
        this.zzbGa = zzau;
        this.zzbFn = zzcl;
    }

    public static zzat zzbT(Context context) {
        zzaa zzaa;
        synchronized (zzbEL) {
            if (zzbFZ == null) {
                zzbFZ = new zzaa(context);
            }
            zzaa = zzbFZ;
        }
        return zzaa;
    }

    public boolean zzhf(String str) {
        if (!this.zzbFn.zzpV()) {
            zzbo.zzbh("Too many urls sent too quickly with the TagManagerSender, rate limiting invoked.");
            return false;
        }
        this.zzbGa.zzhj(str);
        return true;
    }
}
