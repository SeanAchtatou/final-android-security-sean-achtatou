package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzak;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzdl {
    private static final Object zzbIF = null;
    private static Long zzbIG = new Long(0);
    private static Double zzbIH = new Double(0.0d);
    private static zzdk zzbII = zzdk.zzaB(0);
    private static String zzbIJ = new String("");
    private static Boolean zzbIK = new Boolean(false);
    private static List<Object> zzbIL = new ArrayList(0);
    private static Map<Object, Object> zzbIM = new HashMap();
    private static zzak.zza zzbIN = zzS(zzbIJ);

    private static double getDouble(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).doubleValue();
        }
        zzbo.e("getDouble received non-Number");
        return 0.0d;
    }

    public static String zzN(Object obj) {
        return obj == null ? zzbIJ : obj.toString();
    }

    public static zzdk zzO(Object obj) {
        return obj instanceof zzdk ? (zzdk) obj : zzU(obj) ? zzdk.zzaB(zzV(obj)) : zzT(obj) ? zzdk.zza(Double.valueOf(getDouble(obj))) : zzhx(zzN(obj));
    }

    public static Long zzP(Object obj) {
        return zzU(obj) ? Long.valueOf(zzV(obj)) : zzhy(zzN(obj));
    }

    public static Double zzQ(Object obj) {
        return zzT(obj) ? Double.valueOf(getDouble(obj)) : zzhz(zzN(obj));
    }

    public static Boolean zzR(Object obj) {
        return obj instanceof Boolean ? (Boolean) obj : zzhA(zzN(obj));
    }

    public static Object zzRN() {
        return null;
    }

    public static Long zzRO() {
        return zzbIG;
    }

    public static Double zzRP() {
        return zzbIH;
    }

    public static Boolean zzRQ() {
        return zzbIK;
    }

    public static zzdk zzRR() {
        return zzbII;
    }

    public static String zzRS() {
        return zzbIJ;
    }

    public static zzak.zza zzRT() {
        return zzbIN;
    }

    public static zzak.zza zzS(Object obj) {
        boolean z = false;
        zzak.zza zza = new zzak.zza();
        if (obj instanceof zzak.zza) {
            return (zzak.zza) obj;
        }
        if (obj instanceof String) {
            zza.type = 1;
            zza.string = (String) obj;
        } else if (obj instanceof List) {
            zza.type = 2;
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            boolean z2 = false;
            for (Object zzS : list) {
                zzak.zza zzS2 = zzS(zzS);
                if (zzS2 == zzbIN) {
                    return zzbIN;
                }
                boolean z3 = z2 || zzS2.zzlD;
                arrayList.add(zzS2);
                z2 = z3;
            }
            zza.zzlu = (zzak.zza[]) arrayList.toArray(new zzak.zza[0]);
            z = z2;
        } else if (obj instanceof Map) {
            zza.type = 3;
            Set<Map.Entry> entrySet = ((Map) obj).entrySet();
            ArrayList arrayList2 = new ArrayList(entrySet.size());
            ArrayList arrayList3 = new ArrayList(entrySet.size());
            boolean z4 = false;
            for (Map.Entry entry : entrySet) {
                zzak.zza zzS3 = zzS(entry.getKey());
                zzak.zza zzS4 = zzS(entry.getValue());
                if (zzS3 == zzbIN || zzS4 == zzbIN) {
                    return zzbIN;
                }
                boolean z5 = z4 || zzS3.zzlD || zzS4.zzlD;
                arrayList2.add(zzS3);
                arrayList3.add(zzS4);
                z4 = z5;
            }
            zza.zzlv = (zzak.zza[]) arrayList2.toArray(new zzak.zza[0]);
            zza.zzlw = (zzak.zza[]) arrayList3.toArray(new zzak.zza[0]);
            z = z4;
        } else if (zzT(obj)) {
            zza.type = 1;
            zza.string = obj.toString();
        } else if (zzU(obj)) {
            zza.type = 6;
            zza.zzlz = zzV(obj);
        } else if (obj instanceof Boolean) {
            zza.type = 8;
            zza.zzlA = ((Boolean) obj).booleanValue();
        } else {
            String valueOf = String.valueOf(obj == null ? "null" : obj.getClass().toString());
            zzbo.e(valueOf.length() != 0 ? "Converting to Value from unknown object type: ".concat(valueOf) : new String("Converting to Value from unknown object type: "));
            return zzbIN;
        }
        zza.zzlD = z;
        return zza;
    }

    private static boolean zzT(Object obj) {
        return (obj instanceof Double) || (obj instanceof Float) || ((obj instanceof zzdk) && ((zzdk) obj).zzRI());
    }

    private static boolean zzU(Object obj) {
        return (obj instanceof Byte) || (obj instanceof Short) || (obj instanceof Integer) || (obj instanceof Long) || ((obj instanceof zzdk) && ((zzdk) obj).zzRJ());
    }

    private static long zzV(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).longValue();
        }
        zzbo.e("getInt64 received non-Number");
        return 0;
    }

    public static String zze(zzak.zza zza) {
        return zzN(zzj(zza));
    }

    public static zzdk zzf(zzak.zza zza) {
        return zzO(zzj(zza));
    }

    public static Long zzg(zzak.zza zza) {
        return zzP(zzj(zza));
    }

    public static Double zzh(zzak.zza zza) {
        return zzQ(zzj(zza));
    }

    private static Boolean zzhA(String str) {
        return "true".equalsIgnoreCase(str) ? Boolean.TRUE : "false".equalsIgnoreCase(str) ? Boolean.FALSE : zzbIK;
    }

    public static zzak.zza zzhw(String str) {
        zzak.zza zza = new zzak.zza();
        zza.type = 5;
        zza.zzly = str;
        return zza;
    }

    private static zzdk zzhx(String str) {
        try {
            return zzdk.zzhv(str);
        } catch (NumberFormatException e2) {
            zzbo.e(new StringBuilder(String.valueOf(str).length() + 33).append("Failed to convert '").append(str).append("' to a number.").toString());
            return zzbII;
        }
    }

    private static Long zzhy(String str) {
        zzdk zzhx = zzhx(str);
        return zzhx == zzbII ? zzbIG : Long.valueOf(zzhx.longValue());
    }

    private static Double zzhz(String str) {
        zzdk zzhx = zzhx(str);
        return zzhx == zzbII ? zzbIH : Double.valueOf(zzhx.doubleValue());
    }

    public static Boolean zzi(zzak.zza zza) {
        return zzR(zzj(zza));
    }

    public static Object zzj(zzak.zza zza) {
        int i = 0;
        if (zza == null) {
            return null;
        }
        switch (zza.type) {
            case 1:
                return zza.string;
            case 2:
                ArrayList arrayList = new ArrayList(zza.zzlu.length);
                zzak.zza[] zzaArr = zza.zzlu;
                int length = zzaArr.length;
                while (i < length) {
                    Object zzj = zzj(zzaArr[i]);
                    if (zzj == null) {
                        return null;
                    }
                    arrayList.add(zzj);
                    i++;
                }
                return arrayList;
            case 3:
                if (zza.zzlv.length != zza.zzlw.length) {
                    String valueOf = String.valueOf(zza.toString());
                    zzbo.e(valueOf.length() != 0 ? "Converting an invalid value to object: ".concat(valueOf) : new String("Converting an invalid value to object: "));
                    return null;
                }
                HashMap hashMap = new HashMap(zza.zzlw.length);
                while (i < zza.zzlv.length) {
                    Object zzj2 = zzj(zza.zzlv[i]);
                    Object zzj3 = zzj(zza.zzlw[i]);
                    if (zzj2 == null || zzj3 == null) {
                        return null;
                    }
                    hashMap.put(zzj2, zzj3);
                    i++;
                }
                return hashMap;
            case 4:
                zzbo.e("Trying to convert a macro reference to object");
                return null;
            case 5:
                zzbo.e("Trying to convert a function id to object");
                return null;
            case 6:
                return Long.valueOf(zza.zzlz);
            case 7:
                StringBuffer stringBuffer = new StringBuffer();
                zzak.zza[] zzaArr2 = zza.zzlB;
                int length2 = zzaArr2.length;
                while (i < length2) {
                    String zze = zze(zzaArr2[i]);
                    if (zze == zzbIJ) {
                        return null;
                    }
                    stringBuffer.append(zze);
                    i++;
                }
                return stringBuffer.toString();
            case 8:
                return Boolean.valueOf(zza.zzlA);
            default:
                zzbo.e(new StringBuilder(46).append("Failed to convert a value of type: ").append(zza.type).toString());
                return null;
        }
    }
}
