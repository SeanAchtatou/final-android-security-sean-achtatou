package com.google.android.gms.common.util;

import android.os.ParcelFileDescriptor;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class zzp {
    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
            }
        }
    }

    public static long zza(InputStream inputStream, OutputStream outputStream) {
        return zza(inputStream, outputStream, false);
    }

    public static long zza(InputStream inputStream, OutputStream outputStream, boolean z) {
        return zza(inputStream, outputStream, z, FileUtils.FileMode.MODE_ISGID);
    }

    public static long zza(InputStream inputStream, OutputStream outputStream, boolean z, int i) {
        byte[] bArr = new byte[i];
        long j = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, i);
                if (read == -1) {
                    break;
                }
                j += (long) read;
                outputStream.write(bArr, 0, read);
            } finally {
                if (z) {
                    closeQuietly(inputStream);
                    closeQuietly(outputStream);
                }
            }
        }
        return j;
    }

    public static void zza(ParcelFileDescriptor parcelFileDescriptor) {
        if (parcelFileDescriptor != null) {
            try {
                parcelFileDescriptor.close();
            } catch (IOException e2) {
            }
        }
    }

    public static byte[] zza(InputStream inputStream, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        zza(inputStream, byteArrayOutputStream, z);
        return byteArrayOutputStream.toByteArray();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.util.zzp.zza(java.io.InputStream, boolean):byte[]
     arg types: [java.io.InputStream, int]
     candidates:
      com.google.android.gms.common.util.zzp.zza(java.io.InputStream, java.io.OutputStream):long
      com.google.android.gms.common.util.zzp.zza(java.io.InputStream, boolean):byte[] */
    public static byte[] zzj(InputStream inputStream) {
        return zza(inputStream, true);
    }
}
