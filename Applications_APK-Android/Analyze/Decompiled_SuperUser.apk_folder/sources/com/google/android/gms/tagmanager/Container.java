package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzbjf;
import com.google.android.gms.tagmanager.zzcj;
import com.google.android.gms.tagmanager.zzu;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Container {
    private final Context mContext;
    private final String zzbEX;
    private final DataLayer zzbEY;
    private zzcx zzbEZ;
    private Map<String, FunctionCallMacroCallback> zzbFa = new HashMap();
    private Map<String, FunctionCallTagCallback> zzbFb = new HashMap();
    private volatile long zzbFc;
    private volatile String zzbFd = "";

    public interface FunctionCallMacroCallback {
        Object getValue(String str, Map<String, Object> map);
    }

    public interface FunctionCallTagCallback {
        void execute(String str, Map<String, Object> map);
    }

    private class zza implements zzu.zza {
        private zza() {
        }

        public Object zze(String str, Map<String, Object> map) {
            FunctionCallMacroCallback zzgS = Container.this.zzgS(str);
            if (zzgS == null) {
                return null;
            }
            return zzgS.getValue(str, map);
        }
    }

    private class zzb implements zzu.zza {
        private zzb() {
        }

        public Object zze(String str, Map<String, Object> map) {
            FunctionCallTagCallback zzgT = Container.this.zzgT(str);
            if (zzgT != null) {
                zzgT.execute(str, map);
            }
            return zzdl.zzRS();
        }
    }

    Container(Context context, DataLayer dataLayer, String str, long j, zzaj.zzj zzj) {
        this.mContext = context;
        this.zzbEY = dataLayer;
        this.zzbEX = str;
        this.zzbFc = j;
        zza(zzj.zzlr);
        if (zzj.zzlq != null) {
            zza(zzj.zzlq);
        }
    }

    Container(Context context, DataLayer dataLayer, String str, long j, zzbjf.zzc zzc) {
        this.mContext = context;
        this.zzbEY = dataLayer;
        this.zzbEX = str;
        this.zzbFc = j;
        zza(zzc);
    }

    private synchronized zzcx zzQi() {
        return this.zzbEZ;
    }

    private void zza(zzaj.zzf zzf) {
        if (zzf == null) {
            throw new NullPointerException();
        }
        try {
            zza(zzbjf.zzb(zzf));
        } catch (zzbjf.zzg e2) {
            String valueOf = String.valueOf(zzf);
            String valueOf2 = String.valueOf(e2.toString());
            zzbo.e(new StringBuilder(String.valueOf(valueOf).length() + 46 + String.valueOf(valueOf2).length()).append("Not loading resource: ").append(valueOf).append(" because it is invalid: ").append(valueOf2).toString());
        }
    }

    private void zza(zzbjf.zzc zzc) {
        this.zzbFd = zzc.getVersion();
        zzbjf.zzc zzc2 = zzc;
        zza(new zzcx(this.mContext, zzc2, this.zzbEY, new zza(), new zzb(), zzgV(this.zzbFd)));
        if (getBoolean("_gtm.loadEventEnabled")) {
            this.zzbEY.pushEvent("gtm.load", DataLayer.mapOf("gtm.id", this.zzbEX));
        }
    }

    private synchronized void zza(zzcx zzcx) {
        this.zzbEZ = zzcx;
    }

    private void zza(zzaj.zzi[] zziArr) {
        ArrayList arrayList = new ArrayList();
        for (zzaj.zzi add : zziArr) {
            arrayList.add(add);
        }
        zzQi().zzQ(arrayList);
    }

    public boolean getBoolean(String str) {
        zzcx zzQi = zzQi();
        if (zzQi == null) {
            zzbo.e("getBoolean called for closed container.");
            return zzdl.zzRQ().booleanValue();
        }
        try {
            return zzdl.zzi(zzQi.zzhq(str).getObject()).booleanValue();
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzbo.e(new StringBuilder(String.valueOf(valueOf).length() + 66).append("Calling getBoolean() threw an exception: ").append(valueOf).append(" Returning default value.").toString());
            return zzdl.zzRQ().booleanValue();
        }
    }

    public String getContainerId() {
        return this.zzbEX;
    }

    public double getDouble(String str) {
        zzcx zzQi = zzQi();
        if (zzQi == null) {
            zzbo.e("getDouble called for closed container.");
            return zzdl.zzRP().doubleValue();
        }
        try {
            return zzdl.zzh(zzQi.zzhq(str).getObject()).doubleValue();
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzbo.e(new StringBuilder(String.valueOf(valueOf).length() + 65).append("Calling getDouble() threw an exception: ").append(valueOf).append(" Returning default value.").toString());
            return zzdl.zzRP().doubleValue();
        }
    }

    public long getLastRefreshTime() {
        return this.zzbFc;
    }

    public long getLong(String str) {
        zzcx zzQi = zzQi();
        if (zzQi == null) {
            zzbo.e("getLong called for closed container.");
            return zzdl.zzRO().longValue();
        }
        try {
            return zzdl.zzg(zzQi.zzhq(str).getObject()).longValue();
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzbo.e(new StringBuilder(String.valueOf(valueOf).length() + 63).append("Calling getLong() threw an exception: ").append(valueOf).append(" Returning default value.").toString());
            return zzdl.zzRO().longValue();
        }
    }

    public String getString(String str) {
        zzcx zzQi = zzQi();
        if (zzQi == null) {
            zzbo.e("getString called for closed container.");
            return zzdl.zzRS();
        }
        try {
            return zzdl.zze(zzQi.zzhq(str).getObject());
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzbo.e(new StringBuilder(String.valueOf(valueOf).length() + 65).append("Calling getString() threw an exception: ").append(valueOf).append(" Returning default value.").toString());
            return zzdl.zzRS();
        }
    }

    public boolean isDefault() {
        return getLastRefreshTime() == 0;
    }

    public void registerFunctionCallMacroCallback(String str, FunctionCallMacroCallback functionCallMacroCallback) {
        if (functionCallMacroCallback == null) {
            throw new NullPointerException("Macro handler must be non-null");
        }
        synchronized (this.zzbFa) {
            this.zzbFa.put(str, functionCallMacroCallback);
        }
    }

    public void registerFunctionCallTagCallback(String str, FunctionCallTagCallback functionCallTagCallback) {
        if (functionCallTagCallback == null) {
            throw new NullPointerException("Tag callback must be non-null");
        }
        synchronized (this.zzbFb) {
            this.zzbFb.put(str, functionCallTagCallback);
        }
    }

    /* access modifiers changed from: package-private */
    public void release() {
        this.zzbEZ = null;
    }

    public void unregisterFunctionCallMacroCallback(String str) {
        synchronized (this.zzbFa) {
            this.zzbFa.remove(str);
        }
    }

    public void unregisterFunctionCallTagCallback(String str) {
        synchronized (this.zzbFb) {
            this.zzbFb.remove(str);
        }
    }

    public String zzQh() {
        return this.zzbFd;
    }

    /* access modifiers changed from: package-private */
    public FunctionCallMacroCallback zzgS(String str) {
        FunctionCallMacroCallback functionCallMacroCallback;
        synchronized (this.zzbFa) {
            functionCallMacroCallback = this.zzbFa.get(str);
        }
        return functionCallMacroCallback;
    }

    public FunctionCallTagCallback zzgT(String str) {
        FunctionCallTagCallback functionCallTagCallback;
        synchronized (this.zzbFb) {
            functionCallTagCallback = this.zzbFb.get(str);
        }
        return functionCallTagCallback;
    }

    public void zzgU(String str) {
        zzQi().zzgU(str);
    }

    /* access modifiers changed from: package-private */
    public zzaj zzgV(String str) {
        zzcj.zzRg().zzRh().equals(zzcj.zza.CONTAINER_DEBUG);
        return new zzbw();
    }
}
