package com.google.android.gms.tagmanager;

public final class zzbo {
    static zzbp zzbGQ = new zzz();
    static int zzbGR;

    public static void e(String str) {
        zzbGQ.e(str);
    }

    public static int getLogLevel() {
        return zzbGR;
    }

    public static void setLogLevel(int i) {
        zzbGR = i;
        zzbGQ.setLogLevel(i);
    }

    public static void v(String str) {
        zzbGQ.v(str);
    }

    public static void zzb(String str, Throwable th) {
        zzbGQ.zzb(str, th);
    }

    public static void zzbf(String str) {
        zzbGQ.zzbf(str);
    }

    public static void zzbg(String str) {
        zzbGQ.zzbg(str);
    }

    public static void zzbh(String str) {
        zzbGQ.zzbh(str);
    }

    public static void zzc(String str, Throwable th) {
        zzbGQ.zzc(str, th);
    }
}
