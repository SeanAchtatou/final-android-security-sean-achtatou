package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzac;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class zzsk extends zzsa {
    private volatile String zzadh;
    private Future<String> zzaeS;

    protected zzsk(zzsc zzsc) {
        super(zzsc);
    }

    /* access modifiers changed from: private */
    public String zzoT() {
        String zzoU = zzoU();
        try {
            return !zzx(zznU().getContext(), zzoU) ? "0" : zzoU;
        } catch (Exception e2) {
            zze("Error saving clientId file", e2);
            return "0";
        }
    }

    private boolean zzx(Context context, String str) {
        zzac.zzdr(str);
        zzac.zzdk("ClientId should be saved from worker thread");
        FileOutputStream fileOutputStream = null;
        try {
            zza("Storing clientId", str);
            fileOutputStream = context.openFileOutput("gaClientId", 0);
            fileOutputStream.write(str.getBytes());
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e2) {
                    zze("Failed to close clientId writing stream", e2);
                }
            }
            return true;
        } catch (FileNotFoundException e3) {
            zze("Error creating clientId file", e3);
            if (fileOutputStream == null) {
                return false;
            }
            try {
                fileOutputStream.close();
                return false;
            } catch (IOException e4) {
                zze("Failed to close clientId writing stream", e4);
                return false;
            }
        } catch (IOException e5) {
            zze("Error writing to clientId file", e5);
            if (fileOutputStream == null) {
                return false;
            }
            try {
                fileOutputStream.close();
                return false;
            } catch (IOException e6) {
                zze("Failed to close clientId writing stream", e6);
                return false;
            }
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e7) {
                    zze("Failed to close clientId writing stream", e7);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0075 A[SYNTHETIC, Splitter:B:34:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008e A[SYNTHETIC, Splitter:B:44:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x009e A[SYNTHETIC, Splitter:B:51:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String zzap(android.content.Context r7) {
        /*
            r6 = this;
            r0 = 0
            java.lang.String r1 = "ClientId should be loaded from worker thread"
            com.google.android.gms.common.internal.zzac.zzdk(r1)
            java.lang.String r1 = "gaClientId"
            java.io.FileInputStream r2 = r7.openFileInput(r1)     // Catch:{ FileNotFoundException -> 0x0071, IOException -> 0x0080, all -> 0x0099 }
            r1 = 36
            byte[] r3 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            r1 = 0
            r4 = 36
            int r4 = r2.read(r3, r1, r4)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            int r1 = r2.available()     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            if (r1 <= 0) goto L_0x0037
            java.lang.String r1 = "clientId file seems corrupted, deleting it."
            r6.zzbS(r1)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            r2.close()     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            java.lang.String r1 = "gaClientId"
            r7.deleteFile(r1)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0030 }
        L_0x002f:
            return r0
        L_0x0030:
            r1 = move-exception
            java.lang.String r2 = "Failed to close client id reading stream"
            r6.zze(r2, r1)
            goto L_0x002f
        L_0x0037:
            r1 = 14
            if (r4 >= r1) goto L_0x0055
            java.lang.String r1 = "clientId file is empty, deleting it."
            r6.zzbS(r1)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            r2.close()     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            java.lang.String r1 = "gaClientId"
            r7.deleteFile(r1)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x004e }
            goto L_0x002f
        L_0x004e:
            r1 = move-exception
            java.lang.String r2 = "Failed to close client id reading stream"
            r6.zze(r2, r1)
            goto L_0x002f
        L_0x0055:
            r2.close()     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            java.lang.String r1 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            r5 = 0
            r1.<init>(r3, r5, r4)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            java.lang.String r3 = "Read client id from disk"
            r6.zza(r3, r1)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x00ab }
            if (r2 == 0) goto L_0x0068
            r2.close()     // Catch:{ IOException -> 0x006a }
        L_0x0068:
            r0 = r1
            goto L_0x002f
        L_0x006a:
            r0 = move-exception
            java.lang.String r2 = "Failed to close client id reading stream"
            r6.zze(r2, r0)
            goto L_0x0068
        L_0x0071:
            r1 = move-exception
            r1 = r0
        L_0x0073:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0079 }
            goto L_0x002f
        L_0x0079:
            r1 = move-exception
            java.lang.String r2 = "Failed to close client id reading stream"
            r6.zze(r2, r1)
            goto L_0x002f
        L_0x0080:
            r1 = move-exception
            r2 = r0
        L_0x0082:
            java.lang.String r3 = "Error reading client id file, deleting it"
            r6.zze(r3, r1)     // Catch:{ all -> 0x00a9 }
            java.lang.String r1 = "gaClientId"
            r7.deleteFile(r1)     // Catch:{ all -> 0x00a9 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x002f
        L_0x0092:
            r1 = move-exception
            java.lang.String r2 = "Failed to close client id reading stream"
            r6.zze(r2, r1)
            goto L_0x002f
        L_0x0099:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x009c:
            if (r2 == 0) goto L_0x00a1
            r2.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x00a1:
            throw r0
        L_0x00a2:
            r1 = move-exception
            java.lang.String r2 = "Failed to close client id reading stream"
            r6.zze(r2, r1)
            goto L_0x00a1
        L_0x00a9:
            r0 = move-exception
            goto L_0x009c
        L_0x00ab:
            r1 = move-exception
            goto L_0x0082
        L_0x00ad:
            r1 = move-exception
            r1 = r2
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzsk.zzap(android.content.Context):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
    }

    public String zzoQ() {
        String str;
        zzob();
        synchronized (this) {
            if (this.zzadh == null) {
                this.zzaeS = zznU().zzc(new Callable<String>() {
                    /* renamed from: zzbY */
                    public String call() {
                        return zzsk.this.zzoS();
                    }
                });
            }
            if (this.zzaeS != null) {
                try {
                    this.zzadh = this.zzaeS.get();
                } catch (InterruptedException e2) {
                    zzd("ClientId loading or generation was interrupted", e2);
                    this.zzadh = "0";
                } catch (ExecutionException e3) {
                    zze("Failed to load or generate client id", e3);
                    this.zzadh = "0";
                }
                if (this.zzadh == null) {
                    this.zzadh = "0";
                }
                zza("Loaded clientId", this.zzadh);
                this.zzaeS = null;
            }
            str = this.zzadh;
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public String zzoR() {
        synchronized (this) {
            this.zzadh = null;
            this.zzaeS = zznU().zzc(new Callable<String>() {
                /* renamed from: zzbY */
                public String call() {
                    return zzsk.this.zzoT();
                }
            });
        }
        return zzoQ();
    }

    /* access modifiers changed from: package-private */
    public String zzoS() {
        String zzap = zzap(zznU().getContext());
        return zzap == null ? zzoT() : zzap;
    }

    /* access modifiers changed from: protected */
    public String zzoU() {
        return UUID.randomUUID().toString().toLowerCase();
    }
}
