package com.google.android.gms.internal;

public interface zzaf {

    public static final class zza extends zzbyd<zza> {
        public String stackTrace = null;
        public String zzaS = null;
        public Long zzaT = null;
        public String zzaU = null;
        public String zzaV = null;
        public Long zzaW = null;
        public Long zzaX = null;
        public String zzaY = null;
        public Long zzaZ = null;
        public String zzba = null;

        public zza() {
            this.zzcwL = -1;
        }

        public void zza(zzbyc zzbyc) {
            if (this.zzaS != null) {
                zzbyc.zzq(1, this.zzaS);
            }
            if (this.zzaT != null) {
                zzbyc.zzb(2, this.zzaT.longValue());
            }
            if (this.stackTrace != null) {
                zzbyc.zzq(3, this.stackTrace);
            }
            if (this.zzaU != null) {
                zzbyc.zzq(4, this.zzaU);
            }
            if (this.zzaV != null) {
                zzbyc.zzq(5, this.zzaV);
            }
            if (this.zzaW != null) {
                zzbyc.zzb(6, this.zzaW.longValue());
            }
            if (this.zzaX != null) {
                zzbyc.zzb(7, this.zzaX.longValue());
            }
            if (this.zzaY != null) {
                zzbyc.zzq(8, this.zzaY);
            }
            if (this.zzaZ != null) {
                zzbyc.zzb(9, this.zzaZ.longValue());
            }
            if (this.zzba != null) {
                zzbyc.zzq(10, this.zzba);
            }
            super.zza(zzbyc);
        }

        /* renamed from: zze */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 10:
                        this.zzaS = zzbyb.readString();
                        break;
                    case 16:
                        this.zzaT = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 26:
                        this.stackTrace = zzbyb.readString();
                        break;
                    case 34:
                        this.zzaU = zzbyb.readString();
                        break;
                    case 42:
                        this.zzaV = zzbyb.readString();
                        break;
                    case 48:
                        this.zzaW = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 56:
                        this.zzaX = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 66:
                        this.zzaY = zzbyb.readString();
                        break;
                    case 72:
                        this.zzaZ = Long.valueOf(zzbyb.zzaeZ());
                        break;
                    case 82:
                        this.zzba = zzbyb.readString();
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu();
            if (this.zzaS != null) {
                zzu += zzbyc.zzr(1, this.zzaS);
            }
            if (this.zzaT != null) {
                zzu += zzbyc.zzf(2, this.zzaT.longValue());
            }
            if (this.stackTrace != null) {
                zzu += zzbyc.zzr(3, this.stackTrace);
            }
            if (this.zzaU != null) {
                zzu += zzbyc.zzr(4, this.zzaU);
            }
            if (this.zzaV != null) {
                zzu += zzbyc.zzr(5, this.zzaV);
            }
            if (this.zzaW != null) {
                zzu += zzbyc.zzf(6, this.zzaW.longValue());
            }
            if (this.zzaX != null) {
                zzu += zzbyc.zzf(7, this.zzaX.longValue());
            }
            if (this.zzaY != null) {
                zzu += zzbyc.zzr(8, this.zzaY);
            }
            if (this.zzaZ != null) {
                zzu += zzbyc.zzf(9, this.zzaZ.longValue());
            }
            return this.zzba != null ? zzu + zzbyc.zzr(10, this.zzba) : zzu;
        }
    }
}
