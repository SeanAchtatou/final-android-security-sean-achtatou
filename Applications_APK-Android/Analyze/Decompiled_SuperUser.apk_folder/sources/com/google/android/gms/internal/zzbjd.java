package com.google.android.gms.internal;

import com.google.android.gms.internal.zzaj;

public interface zzbjd {

    public static final class zza extends zzbyd<zza> {
        public long zzbNh;
        public zzaj.zzj zzbNi;
        public zzaj.zzf zzlr;

        public zza() {
            zzTx();
        }

        public static zza zzQ(byte[] bArr) {
            return (zza) zzbyj.zza(new zza(), bArr);
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof zza)) {
                return false;
            }
            zza zza = (zza) obj;
            if (this.zzbNh != zza.zzbNh) {
                return false;
            }
            if (this.zzlr == null) {
                if (zza.zzlr != null) {
                    return false;
                }
            } else if (!this.zzlr.equals(zza.zzlr)) {
                return false;
            }
            if (this.zzbNi == null) {
                if (zza.zzbNi != null) {
                    return false;
                }
            } else if (!this.zzbNi.equals(zza.zzbNi)) {
                return false;
            }
            return (this.zzcwC == null || this.zzcwC.isEmpty()) ? zza.zzcwC == null || zza.zzcwC.isEmpty() : this.zzcwC.equals(zza.zzcwC);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.zzbNi == null ? 0 : this.zzbNi.hashCode()) + (((this.zzlr == null ? 0 : this.zzlr.hashCode()) + ((((getClass().getName().hashCode() + 527) * 31) + ((int) (this.zzbNh ^ (this.zzbNh >>> 32)))) * 31)) * 31)) * 31;
            if (this.zzcwC != null && !this.zzcwC.isEmpty()) {
                i = this.zzcwC.hashCode();
            }
            return hashCode + i;
        }

        public zza zzTx() {
            this.zzbNh = 0;
            this.zzlr = null;
            this.zzbNi = null;
            this.zzcwC = null;
            this.zzcwL = -1;
            return this;
        }

        /* renamed from: zzW */
        public zza zzb(zzbyb zzbyb) {
            while (true) {
                int zzaeW = zzbyb.zzaeW();
                switch (zzaeW) {
                    case 0:
                        break;
                    case 8:
                        this.zzbNh = zzbyb.zzaeZ();
                        break;
                    case 18:
                        if (this.zzlr == null) {
                            this.zzlr = new zzaj.zzf();
                        }
                        zzbyb.zza(this.zzlr);
                        break;
                    case 26:
                        if (this.zzbNi == null) {
                            this.zzbNi = new zzaj.zzj();
                        }
                        zzbyb.zza(this.zzbNi);
                        break;
                    default:
                        if (super.zza(zzbyb, zzaeW)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void zza(zzbyc zzbyc) {
            zzbyc.zzb(1, this.zzbNh);
            if (this.zzlr != null) {
                zzbyc.zza(2, this.zzlr);
            }
            if (this.zzbNi != null) {
                zzbyc.zza(3, this.zzbNi);
            }
            super.zza(zzbyc);
        }

        /* access modifiers changed from: protected */
        public int zzu() {
            int zzu = super.zzu() + zzbyc.zzf(1, this.zzbNh);
            if (this.zzlr != null) {
                zzu += zzbyc.zzc(2, this.zzlr);
            }
            return this.zzbNi != null ? zzu + zzbyc.zzc(3, this.zzbNi) : zzu;
        }
    }
}
