package com.google.android.gms.tasks;

import com.google.android.gms.common.internal.zzac;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class Tasks {

    private static final class zza implements zzb {
        private final CountDownLatch zztj;

        private zza() {
            this.zztj = new CountDownLatch(1);
        }

        public void await() {
            this.zztj.await();
        }

        public boolean await(long j, TimeUnit timeUnit) {
            return this.zztj.await(j, timeUnit);
        }

        public void onFailure(Exception exc) {
            this.zztj.countDown();
        }

        public void onSuccess(Object obj) {
            this.zztj.countDown();
        }
    }

    interface zzb extends OnFailureListener, OnSuccessListener<Object> {
    }

    private static final class zzc implements zzb {
        private final zzh<Void> zzbNF;
        private Exception zzbNK;
        private final int zzbNM;
        private int zzbNN;
        private int zzbNO;
        private final Object zzrJ = new Object();

        public zzc(int i, zzh<Void> zzh) {
            this.zzbNM = i;
            this.zzbNF = zzh;
        }

        private void zzTL() {
            if (this.zzbNN + this.zzbNO != this.zzbNM) {
                return;
            }
            if (this.zzbNK == null) {
                this.zzbNF.setResult(null);
                return;
            }
            zzh<Void> zzh = this.zzbNF;
            int i = this.zzbNO;
            zzh.setException(new ExecutionException(new StringBuilder(54).append(i).append(" out of ").append(this.zzbNM).append(" underlying tasks failed").toString(), this.zzbNK));
        }

        public void onFailure(Exception exc) {
            synchronized (this.zzrJ) {
                this.zzbNO++;
                this.zzbNK = exc;
                zzTL();
            }
        }

        public void onSuccess(Object obj) {
            synchronized (this.zzrJ) {
                this.zzbNN++;
                zzTL();
            }
        }
    }

    private Tasks() {
    }

    public static <TResult> TResult await(Task<TResult> task) {
        zzac.zzye();
        zzac.zzb(task, "Task must not be null");
        if (task.isComplete()) {
            return zzb(task);
        }
        zza zza2 = new zza();
        zza(task, zza2);
        zza2.await();
        return zzb(task);
    }

    public static <TResult> TResult await(Task<TResult> task, long j, TimeUnit timeUnit) {
        zzac.zzye();
        zzac.zzb(task, "Task must not be null");
        zzac.zzb(timeUnit, "TimeUnit must not be null");
        if (task.isComplete()) {
            return zzb(task);
        }
        zza zza2 = new zza();
        zza(task, zza2);
        if (zza2.await(j, timeUnit)) {
            return zzb(task);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    public static <TResult> Task<TResult> call(Callable<TResult> callable) {
        return call(TaskExecutors.MAIN_THREAD, callable);
    }

    public static <TResult> Task<TResult> call(Executor executor, final Callable<TResult> callable) {
        zzac.zzb(executor, "Executor must not be null");
        zzac.zzb(callable, "Callback must not be null");
        final zzh zzh = new zzh();
        executor.execute(new Runnable() {
            public void run() {
                try {
                    zzh.this.setResult(callable.call());
                } catch (Exception e2) {
                    zzh.this.setException(e2);
                }
            }
        });
        return zzh;
    }

    public static <TResult> Task<TResult> forException(Exception exc) {
        zzh zzh = new zzh();
        zzh.setException(exc);
        return zzh;
    }

    public static <TResult> Task<TResult> forResult(TResult tresult) {
        zzh zzh = new zzh();
        zzh.setResult(tresult);
        return zzh;
    }

    public static Task<Void> whenAll(Collection<? extends Task<?>> collection) {
        if (collection.isEmpty()) {
            return forResult(null);
        }
        for (Task task : collection) {
            if (task == null) {
                throw new NullPointerException("null tasks are not accepted");
            }
        }
        zzh zzh = new zzh();
        zzc zzc2 = new zzc(collection.size(), zzh);
        for (Task zza2 : collection) {
            zza(zza2, zzc2);
        }
        return zzh;
    }

    public static Task<Void> whenAll(Task<?>... taskArr) {
        return taskArr.length == 0 ? forResult(null) : whenAll(Arrays.asList(taskArr));
    }

    private static void zza(Task<?> task, zzb zzb2) {
        task.addOnSuccessListener(TaskExecutors.zzbNG, zzb2);
        task.addOnFailureListener(TaskExecutors.zzbNG, zzb2);
    }

    private static <TResult> TResult zzb(Task<TResult> task) {
        if (task.isSuccessful()) {
            return task.getResult();
        }
        throw new ExecutionException(task.getException());
    }
}
