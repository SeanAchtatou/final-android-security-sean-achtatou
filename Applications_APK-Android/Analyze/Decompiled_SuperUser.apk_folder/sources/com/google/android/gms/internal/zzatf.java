package com.google.android.gms.internal;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.internal.zzauu;
import com.google.android.gms.internal.zzauw;
import com.google.android.gms.measurement.AppMeasurement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

class zzatf extends zzauh {
    zzatf(zzaue zzaue) {
        super(zzaue);
    }

    private Boolean zza(zzauu.zzb zzb, zzauw.zzb zzb2, long j) {
        Boolean zza;
        if (zzb.zzbwv != null) {
            Boolean zza2 = zza(j, zzb.zzbwv);
            if (zza2 == null) {
                return null;
            }
            if (!zza2.booleanValue()) {
                return false;
            }
        }
        HashSet hashSet = new HashSet();
        for (zzauu.zzc zzc : zzb.zzbwt) {
            if (TextUtils.isEmpty(zzc.zzbwA)) {
                zzKl().zzMb().zzj("null or empty param name in filter. event", zzb2.name);
                return null;
            }
            hashSet.add(zzc.zzbwA);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (zzauw.zzc zzc2 : zzb2.zzbxb) {
            if (hashSet.contains(zzc2.name)) {
                if (zzc2.zzbxf != null) {
                    arrayMap.put(zzc2.name, zzc2.zzbxf);
                } else if (zzc2.zzbwi != null) {
                    arrayMap.put(zzc2.name, zzc2.zzbwi);
                } else if (zzc2.zzaGV != null) {
                    arrayMap.put(zzc2.name, zzc2.zzaGV);
                } else {
                    zzKl().zzMb().zze("Unknown value for param. event, param", zzb2.name, zzc2.name);
                    return null;
                }
            }
        }
        for (zzauu.zzc zzc3 : zzb.zzbwt) {
            boolean equals = Boolean.TRUE.equals(zzc3.zzbwz);
            String str = zzc3.zzbwA;
            if (TextUtils.isEmpty(str)) {
                zzKl().zzMb().zzj("Event has empty param name. event", zzb2.name);
                return null;
            }
            Object obj = arrayMap.get(str);
            if (obj instanceof Long) {
                if (zzc3.zzbwy == null) {
                    zzKl().zzMb().zze("No number filter for long param. event, param", zzb2.name, str);
                    return null;
                }
                Boolean zza3 = zza(((Long) obj).longValue(), zzc3.zzbwy);
                if (zza3 == null) {
                    return null;
                }
                if ((!zza3.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof Double) {
                if (zzc3.zzbwy == null) {
                    zzKl().zzMb().zze("No number filter for double param. event, param", zzb2.name, str);
                    return null;
                }
                Boolean zza4 = zza(((Double) obj).doubleValue(), zzc3.zzbwy);
                if (zza4 == null) {
                    return null;
                }
                if ((!zza4.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof String) {
                if (zzc3.zzbwx != null) {
                    zza = zza((String) obj, zzc3.zzbwx);
                } else if (zzc3.zzbwy == null) {
                    zzKl().zzMb().zze("No filter for String param. event, param", zzb2.name, str);
                    return null;
                } else if (zzaut.zzgf((String) obj)) {
                    zza = zza((String) obj, zzc3.zzbwy);
                } else {
                    zzKl().zzMb().zze("Invalid param value for number filter. event, param", zzb2.name, str);
                    return null;
                }
                if (zza == null) {
                    return null;
                }
                if ((!zza.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj == null) {
                zzKl().zzMf().zze("Missing param for filter. event, param", zzb2.name, str);
                return false;
            } else {
                zzKl().zzMb().zze("Unknown param type. event, param", zzb2.name, str);
                return null;
            }
        }
        return true;
    }

    private Boolean zza(zzauu.zze zze, zzauw.zzg zzg) {
        zzauu.zzc zzc = zze.zzbwI;
        if (zzc == null) {
            zzKl().zzMb().zzj("Missing property filter. property", zzg.name);
            return null;
        }
        boolean equals = Boolean.TRUE.equals(zzc.zzbwz);
        if (zzg.zzbxf != null) {
            if (zzc.zzbwy != null) {
                return zza(zza(zzg.zzbxf.longValue(), zzc.zzbwy), equals);
            }
            zzKl().zzMb().zzj("No number filter for long property. property", zzg.name);
            return null;
        } else if (zzg.zzbwi != null) {
            if (zzc.zzbwy != null) {
                return zza(zza(zzg.zzbwi.doubleValue(), zzc.zzbwy), equals);
            }
            zzKl().zzMb().zzj("No number filter for double property. property", zzg.name);
            return null;
        } else if (zzg.zzaGV == null) {
            zzKl().zzMb().zzj("User property has no value, property", zzg.name);
            return null;
        } else if (zzc.zzbwx != null) {
            return zza(zza(zzg.zzaGV, zzc.zzbwx), equals);
        } else {
            if (zzc.zzbwy == null) {
                zzKl().zzMb().zzj("No string or number filter defined. property", zzg.name);
                return null;
            } else if (zzaut.zzgf(zzg.zzaGV)) {
                return zza(zza(zzg.zzaGV, zzc.zzbwy), equals);
            } else {
                zzKl().zzMb().zze("Invalid user property value for Numeric number filter. property, value", zzg.name, zzg.zzaGV);
                return null;
            }
        }
    }

    static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    private Boolean zza(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    private Boolean zza(BigDecimal bigDecimal, int i, BigDecimal bigDecimal2, BigDecimal bigDecimal3, BigDecimal bigDecimal4, double d2) {
        boolean z = true;
        if (bigDecimal == null) {
            return null;
        }
        if (i == 4) {
            if (bigDecimal3 == null || bigDecimal4 == null) {
                return null;
            }
        } else if (bigDecimal2 == null) {
            return null;
        }
        switch (i) {
            case 1:
                if (bigDecimal.compareTo(bigDecimal2) != -1) {
                    z = false;
                }
                return Boolean.valueOf(z);
            case 2:
                if (bigDecimal.compareTo(bigDecimal2) != 1) {
                    z = false;
                }
                return Boolean.valueOf(z);
            case 3:
                if (d2 != 0.0d) {
                    if (!(bigDecimal.compareTo(bigDecimal2.subtract(new BigDecimal(d2).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal2.add(new BigDecimal(d2).multiply(new BigDecimal(2)))) == -1)) {
                        z = false;
                    }
                    return Boolean.valueOf(z);
                }
                if (bigDecimal.compareTo(bigDecimal2) != 0) {
                    z = false;
                }
                return Boolean.valueOf(z);
            case 4:
                if (bigDecimal.compareTo(bigDecimal3) == -1 || bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = false;
                }
                return Boolean.valueOf(z);
            default:
                return null;
        }
    }

    private List<String> zza(String[] strArr, boolean z) {
        if (z) {
            return Arrays.asList(strArr);
        }
        ArrayList arrayList = new ArrayList();
        for (String upperCase : strArr) {
            arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
        }
        return arrayList;
    }

    public Boolean zza(double d2, zzauu.zzd zzd) {
        try {
            return zza(new BigDecimal(d2), zzd, Math.ulp(d2));
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    public Boolean zza(long j, zzauu.zzd zzd) {
        try {
            return zza(new BigDecimal(j), zzd, 0.0d);
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    public Boolean zza(String str, zzauu.zzd zzd) {
        if (!zzaut.zzgf(str)) {
            return null;
        }
        try {
            return zza(new BigDecimal(str), zzd, 0.0d);
        } catch (NumberFormatException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Boolean zza(String str, zzauu.zzf zzf) {
        String str2 = null;
        zzac.zzw(zzf);
        if (str == null || zzf.zzbwJ == null || zzf.zzbwJ.intValue() == 0) {
            return null;
        }
        if (zzf.zzbwJ.intValue() == 6) {
            if (zzf.zzbwM == null || zzf.zzbwM.length == 0) {
                return null;
            }
        } else if (zzf.zzbwK == null) {
            return null;
        }
        int intValue = zzf.zzbwJ.intValue();
        boolean z = zzf.zzbwL != null && zzf.zzbwL.booleanValue();
        String upperCase = (z || intValue == 1 || intValue == 6) ? zzf.zzbwK : zzf.zzbwK.toUpperCase(Locale.ENGLISH);
        List<String> zza = zzf.zzbwM == null ? null : zza(zzf.zzbwM, z);
        if (intValue == 1) {
            str2 = upperCase;
        }
        return zza(str, intValue, z, upperCase, zza, str2);
    }

    /* access modifiers changed from: package-private */
    public Boolean zza(BigDecimal bigDecimal, zzauu.zzd zzd, double d2) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        zzac.zzw(zzd);
        if (zzd.zzbwB == null || zzd.zzbwB.intValue() == 0) {
            return null;
        }
        if (zzd.zzbwB.intValue() == 4) {
            if (zzd.zzbwE == null || zzd.zzbwF == null) {
                return null;
            }
        } else if (zzd.zzbwD == null) {
            return null;
        }
        int intValue = zzd.zzbwB.intValue();
        if (zzd.zzbwB.intValue() == 4) {
            if (!zzaut.zzgf(zzd.zzbwE) || !zzaut.zzgf(zzd.zzbwF)) {
                return null;
            }
            try {
                bigDecimal4 = new BigDecimal(zzd.zzbwE);
                bigDecimal3 = new BigDecimal(zzd.zzbwF);
                bigDecimal2 = null;
            } catch (NumberFormatException e2) {
                return null;
            }
        } else if (!zzaut.zzgf(zzd.zzbwD)) {
            return null;
        } else {
            try {
                bigDecimal2 = new BigDecimal(zzd.zzbwD);
                bigDecimal3 = null;
                bigDecimal4 = null;
            } catch (NumberFormatException e3) {
                return null;
            }
        }
        return zza(bigDecimal, intValue, bigDecimal2, bigDecimal4, bigDecimal3, d2);
    }

    /* access modifiers changed from: package-private */
    public void zza(String str, zzauu.zza[] zzaArr) {
        zzac.zzw(zzaArr);
        for (zzauu.zza zza : zzaArr) {
            for (zzauu.zzb zzb : zza.zzbwp) {
                String str2 = AppMeasurement.zza.zzbqc.get(zzb.zzbws);
                if (str2 != null) {
                    zzb.zzbws = str2;
                }
                for (zzauu.zzc zzc : zzb.zzbwt) {
                    String str3 = AppMeasurement.zze.zzbqd.get(zzc.zzbwA);
                    if (str3 != null) {
                        zzc.zzbwA = str3;
                    }
                }
            }
            for (zzauu.zze zze : zza.zzbwo) {
                String str4 = AppMeasurement.zzg.zzbqh.get(zze.zzbwH);
                if (str4 != null) {
                    zze.zzbwH = str4;
                }
            }
        }
        zzKg().zzb(str, zzaArr);
    }

    /* access modifiers changed from: package-private */
    public zzauw.zza[] zza(String str, zzauw.zzb[] zzbArr, zzauw.zzg[] zzgArr) {
        Map map;
        zzauu.zze zze;
        zzatn zzLV;
        Map map2;
        zzac.zzdr(str);
        HashSet hashSet = new HashSet();
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        Map<Integer, zzauw.zzf> zzfy = zzKg().zzfy(str);
        if (zzfy != null) {
            for (Integer intValue : zzfy.keySet()) {
                int intValue2 = intValue.intValue();
                zzauw.zzf zzf = zzfy.get(Integer.valueOf(intValue2));
                BitSet bitSet = (BitSet) arrayMap2.get(Integer.valueOf(intValue2));
                BitSet bitSet2 = (BitSet) arrayMap3.get(Integer.valueOf(intValue2));
                if (bitSet == null) {
                    bitSet = new BitSet();
                    arrayMap2.put(Integer.valueOf(intValue2), bitSet);
                    bitSet2 = new BitSet();
                    arrayMap3.put(Integer.valueOf(intValue2), bitSet2);
                }
                for (int i = 0; i < zzf.zzbxJ.length * 64; i++) {
                    if (zzaut.zza(zzf.zzbxJ, i)) {
                        zzKl().zzMf().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue2), Integer.valueOf(i));
                        bitSet2.set(i);
                        if (zzaut.zza(zzf.zzbxK, i)) {
                            bitSet.set(i);
                        }
                    }
                }
                zzauw.zza zza = new zzauw.zza();
                arrayMap.put(Integer.valueOf(intValue2), zza);
                zza.zzbwZ = false;
                zza.zzbwY = zzf;
                zza.zzbwX = new zzauw.zzf();
                zza.zzbwX.zzbxK = zzaut.zza(bitSet);
                zza.zzbwX.zzbxJ = zzaut.zza(bitSet2);
            }
        }
        if (zzbArr != null) {
            ArrayMap arrayMap4 = new ArrayMap();
            int length = zzbArr.length;
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= length) {
                    break;
                }
                zzauw.zzb zzb = zzbArr[i3];
                zzatn zzQ = zzKg().zzQ(str, zzb.name);
                if (zzQ == null) {
                    zzKl().zzMb().zze("Event aggregate wasn't created during raw event logging. appId, event", zzatx.zzfE(str), zzb.name);
                    zzLV = new zzatn(str, zzb.name, 1, 1, zzb.zzbxc.longValue());
                } else {
                    zzLV = zzQ.zzLV();
                }
                zzKg().zza(zzLV);
                long j = zzLV.zzbrB;
                Map map3 = (Map) arrayMap4.get(zzb.name);
                if (map3 == null) {
                    Map zzV = zzKg().zzV(str, zzb.name);
                    if (zzV == null) {
                        zzV = new ArrayMap();
                    }
                    arrayMap4.put(zzb.name, zzV);
                    map2 = zzV;
                } else {
                    map2 = map3;
                }
                for (Integer intValue3 : map2.keySet()) {
                    int intValue4 = intValue3.intValue();
                    if (hashSet.contains(Integer.valueOf(intValue4))) {
                        zzKl().zzMf().zzj("Skipping failed audience ID", Integer.valueOf(intValue4));
                    } else {
                        zzauw.zza zza2 = (zzauw.zza) arrayMap.get(Integer.valueOf(intValue4));
                        BitSet bitSet3 = (BitSet) arrayMap2.get(Integer.valueOf(intValue4));
                        BitSet bitSet4 = (BitSet) arrayMap3.get(Integer.valueOf(intValue4));
                        if (zza2 == null) {
                            zzauw.zza zza3 = new zzauw.zza();
                            arrayMap.put(Integer.valueOf(intValue4), zza3);
                            zza3.zzbwZ = true;
                            bitSet3 = new BitSet();
                            arrayMap2.put(Integer.valueOf(intValue4), bitSet3);
                            bitSet4 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue4), bitSet4);
                        }
                        for (zzauu.zzb zzb2 : (List) map2.get(Integer.valueOf(intValue4))) {
                            if (zzKl().zzak(2)) {
                                zzKl().zzMf().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(intValue4), zzb2.zzbwr, zzb2.zzbws);
                                zzKl().zzMf().zzj("Filter definition", zzaut.zza(zzb2));
                            }
                            if (zzb2.zzbwr == null || zzb2.zzbwr.intValue() > 256) {
                                zzKl().zzMb().zze("Invalid event filter ID. appId, id", zzatx.zzfE(str), String.valueOf(zzb2.zzbwr));
                            } else if (bitSet3.get(zzb2.zzbwr.intValue())) {
                                zzKl().zzMf().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue4), zzb2.zzbwr);
                            } else {
                                Boolean zza4 = zza(zzb2, zzb, j);
                                zzKl().zzMf().zzj("Event filter result", zza4 == null ? "null" : zza4);
                                if (zza4 == null) {
                                    hashSet.add(Integer.valueOf(intValue4));
                                } else {
                                    bitSet4.set(zzb2.zzbwr.intValue());
                                    if (zza4.booleanValue()) {
                                        bitSet3.set(zzb2.zzbwr.intValue());
                                    }
                                }
                            }
                        }
                    }
                }
                i2 = i3 + 1;
            }
        }
        if (zzgArr != null) {
            ArrayMap arrayMap5 = new ArrayMap();
            for (zzauw.zzg zzg : zzgArr) {
                Map map4 = (Map) arrayMap5.get(zzg.name);
                if (map4 == null) {
                    Map zzW = zzKg().zzW(str, zzg.name);
                    if (zzW == null) {
                        zzW = new ArrayMap();
                    }
                    arrayMap5.put(zzg.name, zzW);
                    map = zzW;
                } else {
                    map = map4;
                }
                for (Integer intValue5 : map.keySet()) {
                    int intValue6 = intValue5.intValue();
                    if (hashSet.contains(Integer.valueOf(intValue6))) {
                        zzKl().zzMf().zzj("Skipping failed audience ID", Integer.valueOf(intValue6));
                    } else {
                        zzauw.zza zza5 = (zzauw.zza) arrayMap.get(Integer.valueOf(intValue6));
                        BitSet bitSet5 = (BitSet) arrayMap2.get(Integer.valueOf(intValue6));
                        BitSet bitSet6 = (BitSet) arrayMap3.get(Integer.valueOf(intValue6));
                        if (zza5 == null) {
                            zzauw.zza zza6 = new zzauw.zza();
                            arrayMap.put(Integer.valueOf(intValue6), zza6);
                            zza6.zzbwZ = true;
                            bitSet5 = new BitSet();
                            arrayMap2.put(Integer.valueOf(intValue6), bitSet5);
                            bitSet6 = new BitSet();
                            arrayMap3.put(Integer.valueOf(intValue6), bitSet6);
                        }
                        Iterator it = ((List) map.get(Integer.valueOf(intValue6))).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            zze = (zzauu.zze) it.next();
                            if (zzKl().zzak(2)) {
                                zzKl().zzMf().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(intValue6), zze.zzbwr, zze.zzbwH);
                                zzKl().zzMf().zzj("Filter definition", zzaut.zza(zze));
                            }
                            if (zze.zzbwr == null || zze.zzbwr.intValue() > 256) {
                                zzKl().zzMb().zze("Invalid property filter ID. appId, id", zzatx.zzfE(str), String.valueOf(zze.zzbwr));
                                hashSet.add(Integer.valueOf(intValue6));
                            } else if (bitSet5.get(zze.zzbwr.intValue())) {
                                zzKl().zzMf().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue6), zze.zzbwr);
                            } else {
                                Boolean zza7 = zza(zze, zzg);
                                zzKl().zzMf().zzj("Property filter result", zza7 == null ? "null" : zza7);
                                if (zza7 == null) {
                                    hashSet.add(Integer.valueOf(intValue6));
                                } else {
                                    bitSet6.set(zze.zzbwr.intValue());
                                    if (zza7.booleanValue()) {
                                        bitSet5.set(zze.zzbwr.intValue());
                                    }
                                }
                            }
                        }
                        zzKl().zzMb().zze("Invalid property filter ID. appId, id", zzatx.zzfE(str), String.valueOf(zze.zzbwr));
                        hashSet.add(Integer.valueOf(intValue6));
                    }
                }
            }
        }
        zzauw.zza[] zzaArr = new zzauw.zza[arrayMap2.size()];
        int i4 = 0;
        for (Integer intValue7 : arrayMap2.keySet()) {
            int intValue8 = intValue7.intValue();
            if (!hashSet.contains(Integer.valueOf(intValue8))) {
                zzauw.zza zza8 = (zzauw.zza) arrayMap.get(Integer.valueOf(intValue8));
                if (zza8 == null) {
                    zza8 = new zzauw.zza();
                }
                zzauw.zza zza9 = zza8;
                zzaArr[i4] = zza9;
                zza9.zzbwn = Integer.valueOf(intValue8);
                zza9.zzbwX = new zzauw.zzf();
                zza9.zzbwX.zzbxK = zzaut.zza((BitSet) arrayMap2.get(Integer.valueOf(intValue8)));
                zza9.zzbwX.zzbxJ = zzaut.zza((BitSet) arrayMap3.get(Integer.valueOf(intValue8)));
                zzKg().zza(str, intValue8, zza9.zzbwX);
                i4++;
            }
        }
        return (zzauw.zza[]) Arrays.copyOf(zzaArr, i4);
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
    }
}
