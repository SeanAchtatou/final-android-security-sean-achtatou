package com.google.android.gms.internal;

final class zzbyx {
    final byte[] data;
    int limit;
    int pos;
    boolean zzcya;
    boolean zzcyb;
    zzbyx zzcyc;
    zzbyx zzcyd;

    zzbyx() {
        this.data = new byte[8192];
        this.zzcyb = true;
        this.zzcya = false;
    }

    zzbyx(zzbyx zzbyx) {
        this(zzbyx.data, zzbyx.pos, zzbyx.limit);
        zzbyx.zzcya = true;
    }

    zzbyx(byte[] bArr, int i, int i2) {
        this.data = bArr;
        this.pos = i;
        this.limit = i2;
        this.zzcyb = false;
        this.zzcya = true;
    }

    public zzbyx zza(zzbyx zzbyx) {
        zzbyx.zzcyd = this;
        zzbyx.zzcyc = this.zzcyc;
        this.zzcyc.zzcyd = zzbyx;
        this.zzcyc = zzbyx;
        return zzbyx;
    }

    public void zza(zzbyx zzbyx, int i) {
        if (!zzbyx.zzcyb) {
            throw new IllegalArgumentException();
        }
        if (zzbyx.limit + i > 8192) {
            if (zzbyx.zzcya) {
                throw new IllegalArgumentException();
            } else if ((zzbyx.limit + i) - zzbyx.pos > 8192) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(zzbyx.data, zzbyx.pos, zzbyx.data, 0, zzbyx.limit - zzbyx.pos);
                zzbyx.limit -= zzbyx.pos;
                zzbyx.pos = 0;
            }
        }
        System.arraycopy(this.data, this.pos, zzbyx.data, zzbyx.limit, i);
        zzbyx.limit += i;
        this.pos += i;
    }

    public zzbyx zzafX() {
        zzbyx zzbyx = this.zzcyc != this ? this.zzcyc : null;
        this.zzcyd.zzcyc = this.zzcyc;
        this.zzcyc.zzcyd = this.zzcyd;
        this.zzcyc = null;
        this.zzcyd = null;
        return zzbyx;
    }

    public void zzafY() {
        if (this.zzcyd == this) {
            throw new IllegalStateException();
        } else if (this.zzcyd.zzcyb) {
            int i = this.limit - this.pos;
            if (i <= (this.zzcyd.zzcya ? 0 : this.zzcyd.pos) + (8192 - this.zzcyd.limit)) {
                zza(this.zzcyd, i);
                zzafX();
                zzbyy.zzb(this);
            }
        }
    }

    public zzbyx zzrz(int i) {
        zzbyx zzafZ;
        if (i <= 0 || i > this.limit - this.pos) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            zzafZ = new zzbyx(this);
        } else {
            zzafZ = zzbyy.zzafZ();
            System.arraycopy(this.data, this.pos, zzafZ.data, 0, i);
        }
        zzafZ.limit = zzafZ.pos + i;
        this.pos += i;
        this.zzcyd.zza(zzafZ);
        return zzafZ;
    }
}
