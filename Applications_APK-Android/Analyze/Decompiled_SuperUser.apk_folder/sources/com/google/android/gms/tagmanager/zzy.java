package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.List;
import java.util.Map;

class zzy extends zzdj {
    private static final String ID = zzah.DATA_LAYER_WRITE.toString();
    private static final String VALUE = zzai.VALUE.toString();
    private static final String zzbFY = zzai.CLEAR_PERSISTENT_DATA_LAYER_PREFIX.toString();
    private final DataLayer zzbEY;

    public zzy(DataLayer dataLayer) {
        super(ID, VALUE);
        this.zzbEY = dataLayer;
    }

    private void zza(zzak.zza zza) {
        String zze;
        if (zza != null && zza != zzdl.zzRN() && (zze = zzdl.zze(zza)) != zzdl.zzRS()) {
            this.zzbEY.zzha(zze);
        }
    }

    private void zzb(zzak.zza zza) {
        if (zza != null && zza != zzdl.zzRN()) {
            Object zzj = zzdl.zzj(zza);
            if (zzj instanceof List) {
                for (Object next : (List) zzj) {
                    if (next instanceof Map) {
                        this.zzbEY.push((Map) next);
                    }
                }
            }
        }
    }

    public void zzab(Map<String, zzak.zza> map) {
        zzb(map.get(VALUE));
        zza(map.get(zzbFY));
    }
}
