package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.duapps.ad.AdError;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.common.internal.safeparcel.zzc;

public class zze implements Parcelable.Creator<DataHolder> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.String[], boolean):void
     arg types: [android.os.Parcel, int, java.lang.String[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.String[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, android.database.CursorWindow[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzc.zza(android.os.Parcel, int, android.os.Bundle, boolean):void */
    static void zza(DataHolder dataHolder, Parcel parcel, int i) {
        int zzaZ = zzc.zzaZ(parcel);
        zzc.zza(parcel, 1, dataHolder.zzxl(), false);
        zzc.zza(parcel, 2, (Parcelable[]) dataHolder.zzxm(), i, false);
        zzc.zzc(parcel, 3, dataHolder.getStatusCode());
        zzc.zza(parcel, 4, dataHolder.zzxf(), false);
        zzc.zzc(parcel, AdError.NETWORK_ERROR_CODE, dataHolder.zzaiI);
        zzc.zzJ(parcel, zzaZ);
    }

    /* renamed from: zzaO */
    public DataHolder createFromParcel(Parcel parcel) {
        int i = 0;
        Bundle bundle = null;
        int zzaY = zzb.zzaY(parcel);
        CursorWindow[] cursorWindowArr = null;
        String[] strArr = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzaY) {
            int zzaX = zzb.zzaX(parcel);
            switch (zzb.zzdc(zzaX)) {
                case 1:
                    strArr = zzb.zzC(parcel, zzaX);
                    break;
                case 2:
                    cursorWindowArr = (CursorWindow[]) zzb.zzb(parcel, zzaX, CursorWindow.CREATOR);
                    break;
                case 3:
                    i = zzb.zzg(parcel, zzaX);
                    break;
                case 4:
                    bundle = zzb.zzs(parcel, zzaX);
                    break;
                case AdError.NETWORK_ERROR_CODE:
                    i2 = zzb.zzg(parcel, zzaX);
                    break;
                default:
                    zzb.zzb(parcel, zzaX);
                    break;
            }
        }
        if (parcel.dataPosition() != zzaY) {
            throw new zzb.zza(new StringBuilder(37).append("Overread allowed size end=").append(zzaY).toString(), parcel);
        }
        DataHolder dataHolder = new DataHolder(i2, strArr, cursorWindowArr, i, bundle);
        dataHolder.zzxk();
        return dataHolder;
    }

    /* renamed from: zzcL */
    public DataHolder[] newArray(int i) {
        return new DataHolder[i];
    }
}
