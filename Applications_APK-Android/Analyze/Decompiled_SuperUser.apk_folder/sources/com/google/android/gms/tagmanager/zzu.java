package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.HashMap;
import java.util.Map;

class zzu extends zzam {
    private static final String ID = zzah.FUNCTION_CALL.toString();
    private static final String zzbER = zzai.ADDITIONAL_PARAMS.toString();
    private static final String zzbFC = zzai.FUNCTION_CALL_NAME.toString();
    private final zza zzbFD;

    public interface zza {
        Object zze(String str, Map<String, Object> map);
    }

    public zzu(zza zza2) {
        super(ID, zzbFC);
        this.zzbFD = zza2;
    }

    public boolean zzQd() {
        return false;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        String zze = zzdl.zze(map.get(zzbFC));
        HashMap hashMap = new HashMap();
        zzak.zza zza2 = map.get(zzbER);
        if (zza2 != null) {
            Object zzj = zzdl.zzj(zza2);
            if (!(zzj instanceof Map)) {
                zzbo.zzbh("FunctionCallMacro: expected ADDITIONAL_PARAMS to be a map.");
                return zzdl.zzRT();
            }
            for (Map.Entry entry : ((Map) zzj).entrySet()) {
                hashMap.put(entry.getKey().toString(), entry.getValue());
            }
        }
        try {
            return zzdl.zzS(this.zzbFD.zze(zze, hashMap));
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzbo.zzbh(new StringBuilder(String.valueOf(zze).length() + 34 + String.valueOf(valueOf).length()).append("Custom macro/tag ").append(zze).append(" threw exception ").append(valueOf).toString());
            return zzdl.zzRT();
        }
    }
}
