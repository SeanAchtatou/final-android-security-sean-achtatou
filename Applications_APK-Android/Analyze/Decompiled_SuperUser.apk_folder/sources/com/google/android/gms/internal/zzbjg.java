package com.google.android.gms.internal;

import android.content.Context;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class zzbjg {
    public static final Integer zzbNq = 0;
    public static final Integer zzbNr = 1;
    private final Context mContext;
    private final ExecutorService zzbtK;

    public zzbjg(Context context) {
        this(context, Executors.newSingleThreadExecutor());
    }

    zzbjg(Context context, ExecutorService executorService) {
        this.mContext = context;
        this.zzbtK = executorService;
    }
}
