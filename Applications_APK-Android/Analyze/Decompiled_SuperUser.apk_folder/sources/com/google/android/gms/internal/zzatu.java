package com.google.android.gms.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.zze;

public class zzatu extends zzauh {
    private String mAppId;
    private String zzVX;
    private String zzacL;
    private String zzacM;
    private String zzbqv;
    private long zzbqz;
    private int zzbsw;
    private long zzbsx;
    private int zzbsy;

    zzatu(zzaue zzaue) {
        super(zzaue);
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    /* access modifiers changed from: package-private */
    public String getGmpAppId() {
        zzob();
        return this.zzVX;
    }

    public /* bridge */ /* synthetic */ void zzJV() {
        super.zzJV();
    }

    public /* bridge */ /* synthetic */ void zzJW() {
        super.zzJW();
    }

    public /* bridge */ /* synthetic */ void zzJX() {
        super.zzJX();
    }

    public /* bridge */ /* synthetic */ zzatb zzJY() {
        return super.zzJY();
    }

    public /* bridge */ /* synthetic */ zzatf zzJZ() {
        return super.zzJZ();
    }

    public /* bridge */ /* synthetic */ zzauj zzKa() {
        return super.zzKa();
    }

    public /* bridge */ /* synthetic */ zzatu zzKb() {
        return super.zzKb();
    }

    public /* bridge */ /* synthetic */ zzatl zzKc() {
        return super.zzKc();
    }

    public /* bridge */ /* synthetic */ zzaul zzKd() {
        return super.zzKd();
    }

    public /* bridge */ /* synthetic */ zzauk zzKe() {
        return super.zzKe();
    }

    public /* bridge */ /* synthetic */ zzatv zzKf() {
        return super.zzKf();
    }

    public /* bridge */ /* synthetic */ zzatj zzKg() {
        return super.zzKg();
    }

    public /* bridge */ /* synthetic */ zzaut zzKh() {
        return super.zzKh();
    }

    public /* bridge */ /* synthetic */ zzauc zzKi() {
        return super.zzKi();
    }

    public /* bridge */ /* synthetic */ zzaun zzKj() {
        return super.zzKj();
    }

    public /* bridge */ /* synthetic */ zzaud zzKk() {
        return super.zzKk();
    }

    public /* bridge */ /* synthetic */ zzatx zzKl() {
        return super.zzKl();
    }

    public /* bridge */ /* synthetic */ zzaua zzKm() {
        return super.zzKm();
    }

    public /* bridge */ /* synthetic */ zzati zzKn() {
        return super.zzKn();
    }

    /* access modifiers changed from: package-private */
    public String zzKu() {
        zzob();
        return this.zzbqv;
    }

    /* access modifiers changed from: package-private */
    public long zzKv() {
        return zzKn().zzKv();
    }

    /* access modifiers changed from: package-private */
    public long zzKw() {
        zzob();
        zzmR();
        if (this.zzbsx == 0) {
            this.zzbsx = this.zzbqb.zzKh().zzM(getContext(), getContext().getPackageName());
        }
        return this.zzbsx;
    }

    /* access modifiers changed from: package-private */
    public int zzLX() {
        zzob();
        return this.zzbsw;
    }

    /* access modifiers changed from: package-private */
    public int zzLY() {
        zzob();
        return this.zzbsy;
    }

    /* access modifiers changed from: protected */
    public void zzbw(Status status) {
        if (status == null) {
            zzKl().zzLZ().log("GoogleService failed to initialize (no status)");
        } else {
            zzKl().zzLZ().zze("GoogleService failed to initialize, status", Integer.valueOf(status.getStatusCode()), status.getStatusMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public zzatd zzfD(String str) {
        zzmR();
        return new zzatd(zzke(), getGmpAppId(), zzmZ(), (long) zzLX(), zzKu(), zzKv(), zzKw(), str, this.zzbqb.isEnabled(), !zzKm().zzbts, zzKm().zzKq(), zzuW(), this.zzbqb.zzMF(), zzLY());
    }

    /* access modifiers changed from: package-private */
    public String zzke() {
        zzob();
        return this.mAppId;
    }

    public /* bridge */ /* synthetic */ void zzmR() {
        super.zzmR();
    }

    /* access modifiers changed from: protected */
    public void zzmS() {
        boolean z;
        int i = 1;
        String str = "unknown";
        String str2 = "Unknown";
        int i2 = Integer.MIN_VALUE;
        String str3 = "Unknown";
        String packageName = getContext().getPackageName();
        PackageManager packageManager = getContext().getPackageManager();
        if (packageManager == null) {
            zzKl().zzLZ().zzj("PackageManager is null, app identity information might be inaccurate. appId", zzatx.zzfE(packageName));
        } else {
            try {
                str = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException e2) {
                zzKl().zzLZ().zzj("Error retrieving app installer package name. appId", zzatx.zzfE(packageName));
            }
            if (str == null) {
                str = "manual_install";
            } else if ("com.android.vending".equals(str)) {
                str = "";
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    if (!TextUtils.isEmpty(applicationLabel)) {
                        str3 = applicationLabel.toString();
                    }
                    str2 = packageInfo.versionName;
                    i2 = packageInfo.versionCode;
                }
            } catch (PackageManager.NameNotFoundException e3) {
                zzKl().zzLZ().zze("Error retrieving package info. appId, appName", zzatx.zzfE(packageName), str3);
            }
        }
        this.mAppId = packageName;
        this.zzbqv = str;
        this.zzacM = str2;
        this.zzbsw = i2;
        this.zzacL = str3;
        this.zzbsx = 0;
        zzKn().zzLh();
        Status zzaQ = zzaba.zzaQ(getContext());
        boolean z2 = zzaQ != null && zzaQ.isSuccess();
        if (!z2) {
            zzbw(zzaQ);
        }
        if (z2) {
            Boolean zzLj = zzKn().zzLj();
            if (zzKn().zzLi()) {
                zzKl().zzMd().log("Collection disabled with firebase_analytics_collection_deactivated=1");
                z = false;
            } else if (zzLj != null && !zzLj.booleanValue()) {
                zzKl().zzMd().log("Collection disabled with firebase_analytics_collection_enabled=0");
                z = false;
            } else if (zzLj != null || !zzKn().zzwR()) {
                zzKl().zzMf().log("Collection enabled");
                z = true;
            } else {
                zzKl().zzMd().log("Collection disabled with google_app_measurement_enable=0");
                z = false;
            }
        } else {
            z = false;
        }
        this.zzVX = "";
        this.zzbqz = 0;
        zzKn().zzLh();
        try {
            String zzwQ = zzaba.zzwQ();
            if (TextUtils.isEmpty(zzwQ)) {
                zzwQ = "";
            }
            this.zzVX = zzwQ;
            if (z) {
                zzKl().zzMf().zze("App package, google app id", this.mAppId, this.zzVX);
            }
        } catch (IllegalStateException e4) {
            zzKl().zzLZ().zze("getGoogleAppId or isMeasurementEnabled failed with exception. appId", zzatx.zzfE(packageName), e4);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            if (!zzade.zzbg(getContext())) {
                i = 0;
            }
            this.zzbsy = i;
            return;
        }
        this.zzbsy = 0;
    }

    /* access modifiers changed from: package-private */
    public String zzmZ() {
        zzob();
        return this.zzacM;
    }

    public /* bridge */ /* synthetic */ zze zznR() {
        return super.zznR();
    }

    /* access modifiers changed from: package-private */
    public long zzuW() {
        zzob();
        return 0;
    }
}
