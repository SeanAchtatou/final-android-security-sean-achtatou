package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzak;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class zzdp {
    private static zzce<zzak.zza> zza(zzce<zzak.zza> zzce) {
        try {
            return new zzce<>(zzdl.zzS(zzhD(zzdl.zze(zzce.getObject()))), zzce.zzRb());
        } catch (UnsupportedEncodingException e2) {
            zzbo.zzb("Escape URI: unsupported encoding", e2);
            return zzce;
        }
    }

    private static zzce<zzak.zza> zza(zzce<zzak.zza> zzce, int i) {
        if (!zzl(zzce.getObject())) {
            zzbo.e("Escaping can only be applied to strings.");
            return zzce;
        }
        switch (i) {
            case 12:
                return zza(zzce);
            default:
                zzbo.e(new StringBuilder(39).append("Unsupported Value Escaping: ").append(i).toString());
                return zzce;
        }
    }

    static zzce<zzak.zza> zza(zzce<zzak.zza> zzce, int... iArr) {
        for (int zza : iArr) {
            zzce = zza(zzce, zza);
        }
        return zzce;
    }

    static String zzhD(String str) {
        return URLEncoder.encode(str, "UTF-8").replaceAll("\\+", "%20");
    }

    private static boolean zzl(zzak.zza zza) {
        return zzdl.zzj(zza) instanceof String;
    }
}
