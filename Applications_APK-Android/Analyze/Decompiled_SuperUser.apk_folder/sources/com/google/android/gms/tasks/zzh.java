package com.google.android.gms.tasks;

import android.app.Activity;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.internal.zzabe;
import com.google.android.gms.internal.zzabf;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

final class zzh<TResult> extends Task<TResult> {
    private final zzg<TResult> zzbNH = new zzg<>();
    private boolean zzbNI;
    private TResult zzbNJ;
    private Exception zzbNK;
    private final Object zzrJ = new Object();

    private static class zza extends zzabe {
        private final List<WeakReference<zzf<?>>> mListeners = new ArrayList();

        private zza(zzabf zzabf) {
            super(zzabf);
            this.zzaCR.zza("TaskOnStopCallback", this);
        }

        public static zza zzw(Activity activity) {
            zzabf zzs = zzs(activity);
            zza zza = (zza) zzs.zza("TaskOnStopCallback", zza.class);
            return zza == null ? new zza(zzs) : zza;
        }

        public void onStop() {
            synchronized (this.mListeners) {
                for (WeakReference<zzf<?>> weakReference : this.mListeners) {
                    zzf zzf = (zzf) weakReference.get();
                    if (zzf != null) {
                        zzf.cancel();
                    }
                }
                this.mListeners.clear();
            }
        }

        public <T> void zzb(zzf<T> zzf) {
            synchronized (this.mListeners) {
                this.mListeners.add(new WeakReference(zzf));
            }
        }
    }

    zzh() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    private void zzTI() {
        zzac.zza(this.zzbNI, (Object) "Task is not yet complete");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    private void zzTJ() {
        zzac.zza(!this.zzbNI, (Object) "Task is already complete");
    }

    private void zzTK() {
        synchronized (this.zzrJ) {
            if (this.zzbNI) {
                this.zzbNH.zza(this);
            }
        }
    }

    public Task<TResult> addOnCompleteListener(Activity activity, OnCompleteListener<TResult> onCompleteListener) {
        zzc zzc = new zzc(TaskExecutors.MAIN_THREAD, onCompleteListener);
        this.zzbNH.zza(zzc);
        zza.zzw(activity).zzb(zzc);
        zzTK();
        return this;
    }

    public Task<TResult> addOnCompleteListener(OnCompleteListener<TResult> onCompleteListener) {
        return addOnCompleteListener(TaskExecutors.MAIN_THREAD, onCompleteListener);
    }

    public Task<TResult> addOnCompleteListener(Executor executor, OnCompleteListener<TResult> onCompleteListener) {
        this.zzbNH.zza(new zzc(executor, onCompleteListener));
        zzTK();
        return this;
    }

    public Task<TResult> addOnFailureListener(Activity activity, OnFailureListener onFailureListener) {
        zzd zzd = new zzd(TaskExecutors.MAIN_THREAD, onFailureListener);
        this.zzbNH.zza(zzd);
        zza.zzw(activity).zzb(zzd);
        zzTK();
        return this;
    }

    public Task<TResult> addOnFailureListener(OnFailureListener onFailureListener) {
        return addOnFailureListener(TaskExecutors.MAIN_THREAD, onFailureListener);
    }

    public Task<TResult> addOnFailureListener(Executor executor, OnFailureListener onFailureListener) {
        this.zzbNH.zza(new zzd(executor, onFailureListener));
        zzTK();
        return this;
    }

    public Task<TResult> addOnSuccessListener(Activity activity, OnSuccessListener<? super TResult> onSuccessListener) {
        zze zze = new zze(TaskExecutors.MAIN_THREAD, onSuccessListener);
        this.zzbNH.zza(zze);
        zza.zzw(activity).zzb(zze);
        zzTK();
        return this;
    }

    public Task<TResult> addOnSuccessListener(OnSuccessListener<? super TResult> onSuccessListener) {
        return addOnSuccessListener(TaskExecutors.MAIN_THREAD, onSuccessListener);
    }

    public Task<TResult> addOnSuccessListener(Executor executor, OnSuccessListener<? super TResult> onSuccessListener) {
        this.zzbNH.zza(new zze(executor, onSuccessListener));
        zzTK();
        return this;
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation) {
        return continueWith(TaskExecutors.MAIN_THREAD, continuation);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Executor executor, Continuation<TResult, TContinuationResult> continuation) {
        zzh zzh = new zzh();
        this.zzbNH.zza(new zza(executor, continuation, zzh));
        zzTK();
        return zzh;
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation) {
        return continueWithTask(TaskExecutors.MAIN_THREAD, continuation);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Executor executor, Continuation<TResult, Task<TContinuationResult>> continuation) {
        zzh zzh = new zzh();
        this.zzbNH.zza(new zzb(executor, continuation, zzh));
        zzTK();
        return zzh;
    }

    public Exception getException() {
        Exception exc;
        synchronized (this.zzrJ) {
            exc = this.zzbNK;
        }
        return exc;
    }

    public TResult getResult() {
        TResult tresult;
        synchronized (this.zzrJ) {
            zzTI();
            if (this.zzbNK != null) {
                throw new RuntimeExecutionException(this.zzbNK);
            }
            tresult = this.zzbNJ;
        }
        return tresult;
    }

    public <X extends Throwable> TResult getResult(Class<X> cls) {
        TResult tresult;
        synchronized (this.zzrJ) {
            zzTI();
            if (cls.isInstance(this.zzbNK)) {
                throw ((Throwable) cls.cast(this.zzbNK));
            } else if (this.zzbNK != null) {
                throw new RuntimeExecutionException(this.zzbNK);
            } else {
                tresult = this.zzbNJ;
            }
        }
        return tresult;
    }

    public boolean isComplete() {
        boolean z;
        synchronized (this.zzrJ) {
            z = this.zzbNI;
        }
        return z;
    }

    public boolean isSuccessful() {
        boolean z;
        synchronized (this.zzrJ) {
            z = this.zzbNI && this.zzbNK == null;
        }
        return z;
    }

    public void setException(Exception exc) {
        zzac.zzb(exc, "Exception must not be null");
        synchronized (this.zzrJ) {
            zzTJ();
            this.zzbNI = true;
            this.zzbNK = exc;
        }
        this.zzbNH.zza(this);
    }

    public void setResult(TResult tresult) {
        synchronized (this.zzrJ) {
            zzTJ();
            this.zzbNI = true;
            this.zzbNJ = tresult;
        }
        this.zzbNH.zza(this);
    }

    public boolean trySetException(Exception exc) {
        boolean z = true;
        zzac.zzb(exc, "Exception must not be null");
        synchronized (this.zzrJ) {
            if (this.zzbNI) {
                z = false;
            } else {
                this.zzbNI = true;
                this.zzbNK = exc;
                this.zzbNH.zza(this);
            }
        }
        return z;
    }

    public boolean trySetResult(TResult tresult) {
        boolean z = true;
        synchronized (this.zzrJ) {
            if (this.zzbNI) {
                z = false;
            } else {
                this.zzbNI = true;
                this.zzbNJ = tresult;
                this.zzbNH.zza(this);
            }
        }
        return z;
    }
}
