package com.google.android.gms.internal;

public abstract class zzbyw implements zzbzb {
    private final zzbzb zzcxZ;

    public void close() {
        this.zzcxZ.close();
    }

    public long read(zzbyr zzbyr, long j) {
        return this.zzcxZ.read(zzbyr, j);
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.zzcxZ.toString() + ")";
    }
}
