package com.google.android.gms.internal;

import com.google.android.gms.common.api.Api;

public final class zzabn {
    public final zzabm<Api.zzb, ?> zzazG;
    public final zzabz<Api.zzb, ?> zzazH;

    public zzabn(zzabm<Api.zzb, ?> zzabm, zzabz<Api.zzb, ?> zzabz) {
        this.zzazG = zzabm;
        this.zzazH = zzabz;
    }
}
