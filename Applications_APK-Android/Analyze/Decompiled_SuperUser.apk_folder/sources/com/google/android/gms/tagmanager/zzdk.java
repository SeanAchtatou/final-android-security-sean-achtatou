package com.google.android.gms.tagmanager;

class zzdk extends Number implements Comparable<zzdk> {
    private double zzbIC;
    private long zzbID;
    private boolean zzbIE = false;

    private zzdk(double d2) {
        this.zzbIC = d2;
    }

    private zzdk(long j) {
        this.zzbID = j;
    }

    public static zzdk zza(Double d2) {
        return new zzdk(d2.doubleValue());
    }

    public static zzdk zzaB(long j) {
        return new zzdk(j);
    }

    public static zzdk zzhv(String str) {
        try {
            return new zzdk(Long.parseLong(str));
        } catch (NumberFormatException e2) {
            try {
                return new zzdk(Double.parseDouble(str));
            } catch (NumberFormatException e3) {
                throw new NumberFormatException(String.valueOf(str).concat(" is not a valid TypedNumber"));
            }
        }
    }

    public byte byteValue() {
        return (byte) ((int) longValue());
    }

    public double doubleValue() {
        return zzRJ() ? (double) this.zzbID : this.zzbIC;
    }

    public boolean equals(Object obj) {
        return (obj instanceof zzdk) && compareTo((zzdk) obj) == 0;
    }

    public float floatValue() {
        return (float) doubleValue();
    }

    public int hashCode() {
        return new Long(longValue()).hashCode();
    }

    public int intValue() {
        return zzRL();
    }

    public long longValue() {
        return zzRK();
    }

    public short shortValue() {
        return zzRM();
    }

    public String toString() {
        return zzRJ() ? Long.toString(this.zzbID) : Double.toString(this.zzbIC);
    }

    public boolean zzRI() {
        return !zzRJ();
    }

    public boolean zzRJ() {
        return this.zzbIE;
    }

    public long zzRK() {
        return zzRJ() ? this.zzbID : (long) this.zzbIC;
    }

    public int zzRL() {
        return (int) longValue();
    }

    public short zzRM() {
        return (short) ((int) longValue());
    }

    /* renamed from: zza */
    public int compareTo(zzdk zzdk) {
        return (!zzRJ() || !zzdk.zzRJ()) ? Double.compare(doubleValue(), zzdk.doubleValue()) : new Long(this.zzbID).compareTo(Long.valueOf(zzdk.zzbID));
    }
}
