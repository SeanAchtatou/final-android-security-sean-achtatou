package com.google.android.gms.common.api;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.internal.zzf;
import com.google.android.gms.common.internal.zzr;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class Api<O extends ApiOptions> {
    private final String mName;
    private final zza<?, O> zzayH;
    private final zzh<?, O> zzayI = null;
    private final zzf<?> zzayJ;
    private final zzi<?> zzayK;

    public interface ApiOptions {

        public interface HasOptions extends ApiOptions {
        }

        public static final class NoOptions implements NotRequiredOptions {
            private NoOptions() {
            }
        }

        public interface NotRequiredOptions extends ApiOptions {
        }

        public interface Optional extends HasOptions, NotRequiredOptions {
        }
    }

    public static abstract class zza<T extends zze, O> extends zzd<T, O> {
        public abstract T zza(Context context, Looper looper, com.google.android.gms.common.internal.zzg zzg, O o, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener);
    }

    public interface zzb {
    }

    public static class zzc<C extends zzb> {
    }

    public static abstract class zzd<T extends zzb, O> {
        public int getPriority() {
            return Integer.MAX_VALUE;
        }

        public List<Scope> zzp(O o) {
            return Collections.emptyList();
        }
    }

    public interface zze extends zzb {
        void disconnect();

        void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        boolean isConnected();

        boolean isConnecting();

        void zza(zzf.C0053zzf zzf);

        void zza(zzr zzr, Set<Scope> set);

        boolean zzrd();

        boolean zzrr();

        Intent zzrs();

        boolean zzvh();

        IBinder zzvi();
    }

    public static final class zzf<C extends zze> extends zzc<C> {
    }

    public interface zzg<T extends IInterface> extends zzb {
        String zzeA();

        String zzez();

        T zzh(IBinder iBinder);
    }

    public static abstract class zzh<T extends zzg, O> extends zzd<T, O> {
    }

    public static final class zzi<C extends zzg> extends zzc<C> {
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.android.gms.common.api.Api$zza<?, O>, com.google.android.gms.common.api.Api$zza<C, O>, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v0, types: [com.google.android.gms.common.api.Api$zzf<C>, com.google.android.gms.common.api.Api$zzf<?>, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <C extends com.google.android.gms.common.api.Api.zze> Api(java.lang.String r3, com.google.android.gms.common.api.Api.zza<C, O> r4, com.google.android.gms.common.api.Api.zzf<C> r5) {
        /*
            r2 = this;
            r1 = 0
            r2.<init>()
            java.lang.String r0 = "Cannot construct an Api with a null ClientBuilder"
            com.google.android.gms.common.internal.zzac.zzb(r4, r0)
            java.lang.String r0 = "Cannot construct an Api with a null ClientKey"
            com.google.android.gms.common.internal.zzac.zzb(r5, r0)
            r2.mName = r3
            r2.zzayH = r4
            r2.zzayI = r1
            r2.zzayJ = r5
            r2.zzayK = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.Api.<init>(java.lang.String, com.google.android.gms.common.api.Api$zza, com.google.android.gms.common.api.Api$zzf):void");
    }

    public String getName() {
        return this.mName;
    }

    public zzd<?, O> zzve() {
        return this.zzayH;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    public zza<?, O> zzvf() {
        zzac.zza(this.zzayH != null, (Object) "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.zzayH;
    }

    public zzc<?> zzvg() {
        if (this.zzayJ != null) {
            return this.zzayJ;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }
}
