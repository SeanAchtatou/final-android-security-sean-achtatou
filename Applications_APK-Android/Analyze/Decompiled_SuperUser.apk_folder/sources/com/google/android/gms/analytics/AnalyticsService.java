package com.google.android.gms.analytics;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.google.android.gms.internal.zzti;

public final class AnalyticsService extends Service implements zzti.zza {
    private zzti zzabq;

    private zzti zzmq() {
        if (this.zzabq == null) {
            this.zzabq = new zzti(this);
        }
        return this.zzabq;
    }

    public boolean callServiceStopSelfResult(int i) {
        return stopSelfResult(i);
    }

    public Context getContext() {
        return this;
    }

    public IBinder onBind(Intent intent) {
        zzmq();
        return null;
    }

    public void onCreate() {
        super.onCreate();
        zzmq().onCreate();
    }

    public void onDestroy() {
        zzmq().onDestroy();
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return zzmq().onStartCommand(intent, i, i2);
    }
}
