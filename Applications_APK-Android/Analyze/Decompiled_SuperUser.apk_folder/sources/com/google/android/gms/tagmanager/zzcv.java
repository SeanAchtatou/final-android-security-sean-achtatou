package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzbjd;
import com.google.android.gms.internal.zzbjf;
import com.google.android.gms.internal.zzbyi;
import com.google.android.gms.internal.zzbyj;
import com.google.android.gms.tagmanager.zzbn;
import com.google.android.gms.tagmanager.zzcj;
import com.google.android.gms.tagmanager.zzp;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

class zzcv implements zzp.zzf {
    private final Context mContext;
    private final String zzbEX;
    private zzbn<zzbjd.zza> zzbHw;
    private final ExecutorService zzbtK = Executors.newSingleThreadExecutor();

    zzcv(Context context, String str) {
        this.mContext = context;
        this.zzbEX = str;
    }

    private zzbjf.zzc zzM(byte[] bArr) {
        try {
            zzbjf.zzc zzb = zzbjf.zzb(zzaj.zzf.zzf(bArr));
            if (zzb == null) {
                return zzb;
            }
            zzbo.v("The container was successfully loaded from the resource (using binary file)");
            return zzb;
        } catch (zzbyi e2) {
            zzbo.e("The resource file is corrupted. The container cannot be extracted from the binary file");
            return null;
        } catch (zzbjf.zzg e3) {
            zzbo.zzbh("The resource file is invalid. The container from the binary file is invalid");
            return null;
        }
    }

    private zzbjf.zzc zza(ByteArrayOutputStream byteArrayOutputStream) {
        try {
            return zzbh.zzhl(byteArrayOutputStream.toString("UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            zzbo.zzbf("Failed to convert binary resource to string for JSON parsing; the file format is not UTF-8 format.");
            return null;
        } catch (JSONException e3) {
            zzbo.zzbh("Failed to extract the container from the resource file. Resource is a UTF-8 encoded string but doesn't contain a JSON container");
            return null;
        }
    }

    private void zzd(zzbjd.zza zza) {
        if (zza.zzlr == null && zza.zzbNi == null) {
            throw new IllegalArgumentException("Resource and SupplementedResource are NULL.");
        }
    }

    public synchronized void release() {
        this.zzbtK.shutdown();
    }

    public void zzQr() {
        this.zzbtK.execute(new Runnable() {
            public void run() {
                zzcv.this.zzRo();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void zzRo() {
        if (this.zzbHw == null) {
            throw new IllegalStateException("Callback must be set before execute");
        }
        zzbo.v("Attempting to load resource from disk");
        if ((zzcj.zzRg().zzRh() == zzcj.zza.CONTAINER || zzcj.zzRg().zzRh() == zzcj.zza.CONTAINER_DEBUG) && this.zzbEX.equals(zzcj.zzRg().getContainerId())) {
            this.zzbHw.zza(zzbn.zza.NOT_AVAILABLE);
            return;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(zzRp());
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                zzbjf.zzc(fileInputStream, byteArrayOutputStream);
                zzbjd.zza zzQ = zzbjd.zza.zzQ(byteArrayOutputStream.toByteArray());
                zzd(zzQ);
                this.zzbHw.onSuccess(zzQ);
                try {
                    fileInputStream.close();
                } catch (IOException e2) {
                    zzbo.zzbh("Error closing stream for reading resource from disk");
                }
            } catch (IOException e3) {
                this.zzbHw.zza(zzbn.zza.IO_ERROR);
                zzbo.zzbh("Failed to read the resource from disk");
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    zzbo.zzbh("Error closing stream for reading resource from disk");
                }
            } catch (IllegalArgumentException e5) {
                this.zzbHw.zza(zzbn.zza.IO_ERROR);
                zzbo.zzbh("Failed to read the resource from disk. The resource is inconsistent");
                try {
                    fileInputStream.close();
                } catch (IOException e6) {
                    zzbo.zzbh("Error closing stream for reading resource from disk");
                }
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (IOException e7) {
                    zzbo.zzbh("Error closing stream for reading resource from disk");
                }
                throw th;
            }
            zzbo.v("The Disk resource was successfully read.");
        } catch (FileNotFoundException e8) {
            zzbo.zzbf("Failed to find the resource in the disk");
            this.zzbHw.zza(zzbn.zza.NOT_AVAILABLE);
        }
    }

    /* access modifiers changed from: package-private */
    public File zzRp() {
        String valueOf = String.valueOf("resource_");
        String valueOf2 = String.valueOf(this.zzbEX);
        return new File(this.mContext.getDir("google_tagmanager", 0), valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
    }

    public void zza(zzbn<zzbjd.zza> zzbn) {
        this.zzbHw = zzbn;
    }

    public void zzb(final zzbjd.zza zza) {
        this.zzbtK.execute(new Runnable() {
            public void run() {
                zzcv.this.zzc(zza);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean zzc(zzbjd.zza zza) {
        File zzRp = zzRp();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zzRp);
            try {
                fileOutputStream.write(zzbyj.zzf(zza));
                try {
                    fileOutputStream.close();
                } catch (IOException e2) {
                    zzbo.zzbh("error closing stream for writing resource to disk");
                }
                return true;
            } catch (IOException e3) {
                zzbo.zzbh("Error writing resource to disk. Removing resource from disk.");
                zzRp.delete();
                try {
                    fileOutputStream.close();
                    return false;
                } catch (IOException e4) {
                    zzbo.zzbh("error closing stream for writing resource to disk");
                    return false;
                }
            } catch (Throwable th) {
                try {
                    fileOutputStream.close();
                } catch (IOException e5) {
                    zzbo.zzbh("error closing stream for writing resource to disk");
                }
                throw th;
            }
        } catch (FileNotFoundException e6) {
            zzbo.e("Error opening resource file for writing");
            return false;
        }
    }

    public zzbjf.zzc zznz(int i) {
        try {
            InputStream openRawResource = this.mContext.getResources().openRawResource(i);
            String valueOf = String.valueOf(this.mContext.getResources().getResourceName(i));
            zzbo.v(new StringBuilder(String.valueOf(valueOf).length() + 66).append("Attempting to load a container from the resource ID ").append(i).append(" (").append(valueOf).append(")").toString());
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                zzbjf.zzc(openRawResource, byteArrayOutputStream);
                zzbjf.zzc zza = zza(byteArrayOutputStream);
                if (zza == null) {
                    return zzM(byteArrayOutputStream.toByteArray());
                }
                zzbo.v("The container was successfully loaded from the resource (using JSON file format)");
                return zza;
            } catch (IOException e2) {
                String valueOf2 = String.valueOf(this.mContext.getResources().getResourceName(i));
                zzbo.zzbh(new StringBuilder(String.valueOf(valueOf2).length() + 67).append("Error reading the default container with resource ID ").append(i).append(" (").append(valueOf2).append(")").toString());
                return null;
            }
        } catch (Resources.NotFoundException e3) {
            zzbo.zzbh(new StringBuilder(98).append("Failed to load the container. No default container resource found with the resource ID ").append(i).toString());
            return null;
        }
    }
}
