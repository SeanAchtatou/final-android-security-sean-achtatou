package com.google.android.gms.internal;

import com.google.android.gms.internal.zzaj;
import com.google.android.gms.internal.zzak;
import com.google.android.gms.tagmanager.zzbo;
import com.google.android.gms.tagmanager.zzdl;
import com.lody.virtual.helper.utils.FileUtils;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class zzbjf {

    public static class zza {
        private final zzak.zza zzbHW;
        private final Map<String, zzak.zza> zzbMA;

        private zza(Map<String, zzak.zza> map, zzak.zza zza) {
            this.zzbMA = map;
            this.zzbHW = zza;
        }

        public static zzb zzTy() {
            return new zzb();
        }

        public String toString() {
            String valueOf = String.valueOf(zzSY());
            String valueOf2 = String.valueOf(this.zzbHW);
            return new StringBuilder(String.valueOf(valueOf).length() + 32 + String.valueOf(valueOf2).length()).append("Properties: ").append(valueOf).append(" pushAfterEvaluate: ").append(valueOf2).toString();
        }

        public zzak.zza zzRt() {
            return this.zzbHW;
        }

        public Map<String, zzak.zza> zzSY() {
            return Collections.unmodifiableMap(this.zzbMA);
        }

        public void zza(String str, zzak.zza zza) {
            this.zzbMA.put(str, zza);
        }
    }

    public static class zzb {
        private zzak.zza zzbHW;
        private final Map<String, zzak.zza> zzbMA;

        private zzb() {
            this.zzbMA = new HashMap();
        }

        public zza zzTz() {
            return new zza(this.zzbMA, this.zzbHW);
        }

        public zzb zzb(String str, zzak.zza zza) {
            this.zzbMA.put(str, zza);
            return this;
        }

        public zzb zzo(zzak.zza zza) {
            this.zzbHW = zza;
            return this;
        }
    }

    public static class zzc {
        private final String zzavB;
        private final List<zze> zzbMx;
        private final Map<String, List<zza>> zzbMy;
        private final int zzbMz;

        private zzc(List<zze> list, Map<String, List<zza>> map, String str, int i) {
            this.zzbMx = Collections.unmodifiableList(list);
            this.zzbMy = Collections.unmodifiableMap(map);
            this.zzavB = str;
            this.zzbMz = i;
        }

        public static zzd zzTA() {
            return new zzd();
        }

        public String getVersion() {
            return this.zzavB;
        }

        public String toString() {
            String valueOf = String.valueOf(zzSW());
            String valueOf2 = String.valueOf(this.zzbMy);
            return new StringBuilder(String.valueOf(valueOf).length() + 17 + String.valueOf(valueOf2).length()).append("Rules: ").append(valueOf).append("  Macros: ").append(valueOf2).toString();
        }

        public List<zze> zzSW() {
            return this.zzbMx;
        }

        public Map<String, List<zza>> zzTB() {
            return this.zzbMy;
        }
    }

    public static class zzd {
        private String zzavB;
        private final List<zze> zzbMx;
        private final Map<String, List<zza>> zzbMy;
        private int zzbMz;

        private zzd() {
            this.zzbMx = new ArrayList();
            this.zzbMy = new HashMap();
            this.zzavB = "";
            this.zzbMz = 0;
        }

        public zzc zzTC() {
            return new zzc(this.zzbMx, this.zzbMy, this.zzavB, this.zzbMz);
        }

        public zzd zzb(zze zze) {
            this.zzbMx.add(zze);
            return this;
        }

        public zzd zzc(zza zza) {
            String zze = zzdl.zze(zza.zzSY().get(zzai.INSTANCE_NAME.toString()));
            Object obj = this.zzbMy.get(zze);
            if (obj == null) {
                obj = new ArrayList();
                this.zzbMy.put(zze, obj);
            }
            obj.add(zza);
            return this;
        }

        public zzd zzih(String str) {
            this.zzavB = str;
            return this;
        }

        public zzd zznO(int i) {
            this.zzbMz = i;
            return this;
        }
    }

    public static class zze {
        private final List<zza> zzbMC;
        private final List<zza> zzbMD;
        private final List<zza> zzbME;
        private final List<zza> zzbMF;
        private final List<zza> zzbNk;
        private final List<zza> zzbNl;
        private final List<String> zzbNm;
        private final List<String> zzbNn;
        private final List<String> zzbNo;
        private final List<String> zzbNp;

        private zze(List<zza> list, List<zza> list2, List<zza> list3, List<zza> list4, List<zza> list5, List<zza> list6, List<String> list7, List<String> list8, List<String> list9, List<String> list10) {
            this.zzbMC = Collections.unmodifiableList(list);
            this.zzbMD = Collections.unmodifiableList(list2);
            this.zzbME = Collections.unmodifiableList(list3);
            this.zzbMF = Collections.unmodifiableList(list4);
            this.zzbNk = Collections.unmodifiableList(list5);
            this.zzbNl = Collections.unmodifiableList(list6);
            this.zzbNm = Collections.unmodifiableList(list7);
            this.zzbNn = Collections.unmodifiableList(list8);
            this.zzbNo = Collections.unmodifiableList(list9);
            this.zzbNp = Collections.unmodifiableList(list10);
        }

        public static zzf zzTD() {
            return new zzf();
        }

        public String toString() {
            String valueOf = String.valueOf(zzTa());
            String valueOf2 = String.valueOf(zzTb());
            String valueOf3 = String.valueOf(zzTc());
            String valueOf4 = String.valueOf(zzTd());
            String valueOf5 = String.valueOf(zzTE());
            String valueOf6 = String.valueOf(zzTF());
            return new StringBuilder(String.valueOf(valueOf).length() + 102 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length() + String.valueOf(valueOf5).length() + String.valueOf(valueOf6).length()).append("Positive predicates: ").append(valueOf).append("  Negative predicates: ").append(valueOf2).append("  Add tags: ").append(valueOf3).append("  Remove tags: ").append(valueOf4).append("  Add macros: ").append(valueOf5).append("  Remove macros: ").append(valueOf6).toString();
        }

        public List<zza> zzTE() {
            return this.zzbNk;
        }

        public List<zza> zzTF() {
            return this.zzbNl;
        }

        public List<zza> zzTa() {
            return this.zzbMC;
        }

        public List<zza> zzTb() {
            return this.zzbMD;
        }

        public List<zza> zzTc() {
            return this.zzbME;
        }

        public List<zza> zzTd() {
            return this.zzbMF;
        }
    }

    public static class zzf {
        private final List<zza> zzbMC;
        private final List<zza> zzbMD;
        private final List<zza> zzbME;
        private final List<zza> zzbMF;
        private final List<zza> zzbNk;
        private final List<zza> zzbNl;
        private final List<String> zzbNm;
        private final List<String> zzbNn;
        private final List<String> zzbNo;
        private final List<String> zzbNp;

        private zzf() {
            this.zzbMC = new ArrayList();
            this.zzbMD = new ArrayList();
            this.zzbME = new ArrayList();
            this.zzbMF = new ArrayList();
            this.zzbNk = new ArrayList();
            this.zzbNl = new ArrayList();
            this.zzbNm = new ArrayList();
            this.zzbNn = new ArrayList();
            this.zzbNo = new ArrayList();
            this.zzbNp = new ArrayList();
        }

        public zze zzTG() {
            return new zze(this.zzbMC, this.zzbMD, this.zzbME, this.zzbMF, this.zzbNk, this.zzbNl, this.zzbNm, this.zzbNn, this.zzbNo, this.zzbNp);
        }

        public zzf zzd(zza zza) {
            this.zzbMC.add(zza);
            return this;
        }

        public zzf zze(zza zza) {
            this.zzbMD.add(zza);
            return this;
        }

        public zzf zzf(zza zza) {
            this.zzbME.add(zza);
            return this;
        }

        public zzf zzg(zza zza) {
            this.zzbMF.add(zza);
            return this;
        }

        public zzf zzh(zza zza) {
            this.zzbNk.add(zza);
            return this;
        }

        public zzf zzi(zza zza) {
            this.zzbNl.add(zza);
            return this;
        }

        public zzf zzii(String str) {
            this.zzbNo.add(str);
            return this;
        }

        public zzf zzij(String str) {
            this.zzbNp.add(str);
            return this;
        }

        public zzf zzik(String str) {
            this.zzbNm.add(str);
            return this;
        }

        public zzf zzil(String str) {
            this.zzbNn.add(str);
            return this;
        }
    }

    public static class zzg extends Exception {
        public zzg(String str) {
            super(str);
        }
    }

    private static zzak.zza zza(int i, zzaj.zzf zzf2, zzak.zza[] zzaArr, Set<Integer> set) {
        int i2 = 0;
        if (set.contains(Integer.valueOf(i))) {
            String valueOf = String.valueOf(set);
            zzhS(new StringBuilder(String.valueOf(valueOf).length() + 90).append("Value cycle detected.  Current value reference: ").append(i).append(".  Previous value references: ").append(valueOf).append(".").toString());
        }
        zzak.zza zza2 = (zzak.zza) zza(zzf2.zzkF, i, "values");
        if (zzaArr[i] != null) {
            return zzaArr[i];
        }
        zzak.zza zza3 = null;
        set.add(Integer.valueOf(i));
        switch (zza2.type) {
            case 1:
            case 5:
            case 6:
            case 8:
                zza3 = zza2;
                break;
            case 2:
                zzaj.zzh zzn = zzn(zza2);
                zza3 = zzm(zza2);
                zza3.zzlu = new zzak.zza[zzn.zzlg.length];
                int[] iArr = zzn.zzlg;
                int length = iArr.length;
                int i3 = 0;
                while (i2 < length) {
                    zza3.zzlu[i3] = zza(iArr[i2], zzf2, zzaArr, set);
                    i2++;
                    i3++;
                }
                break;
            case 3:
                zza3 = zzm(zza2);
                zzaj.zzh zzn2 = zzn(zza2);
                if (zzn2.zzlh.length != zzn2.zzli.length) {
                    zzhS(new StringBuilder(58).append("Uneven map keys (").append(zzn2.zzlh.length).append(") and map values (").append(zzn2.zzli.length).append(")").toString());
                }
                zza3.zzlv = new zzak.zza[zzn2.zzlh.length];
                zza3.zzlw = new zzak.zza[zzn2.zzlh.length];
                int[] iArr2 = zzn2.zzlh;
                int length2 = iArr2.length;
                int i4 = 0;
                int i5 = 0;
                while (i4 < length2) {
                    zza3.zzlv[i5] = zza(iArr2[i4], zzf2, zzaArr, set);
                    i4++;
                    i5++;
                }
                int[] iArr3 = zzn2.zzli;
                int length3 = iArr3.length;
                int i6 = 0;
                while (i2 < length3) {
                    zza3.zzlw[i6] = zza(iArr3[i2], zzf2, zzaArr, set);
                    i2++;
                    i6++;
                }
                break;
            case 4:
                zza3 = zzm(zza2);
                zza3.zzlx = zzdl.zze(zza(zzn(zza2).zzll, zzf2, zzaArr, set));
                break;
            case 7:
                zza3 = zzm(zza2);
                zzaj.zzh zzn3 = zzn(zza2);
                zza3.zzlB = new zzak.zza[zzn3.zzlk.length];
                int[] iArr4 = zzn3.zzlk;
                int length4 = iArr4.length;
                int i7 = 0;
                while (i2 < length4) {
                    zza3.zzlB[i7] = zza(iArr4[i2], zzf2, zzaArr, set);
                    i2++;
                    i7++;
                }
                break;
        }
        if (zza3 == null) {
            String valueOf2 = String.valueOf(zza2);
            zzhS(new StringBuilder(String.valueOf(valueOf2).length() + 15).append("Invalid value: ").append(valueOf2).toString());
        }
        zzaArr[i] = zza3;
        set.remove(Integer.valueOf(i));
        return zza3;
    }

    private static zza zza(zzaj.zzb zzb2, zzaj.zzf zzf2, zzak.zza[] zzaArr, int i) {
        zzb zzTy = zza.zzTy();
        for (int valueOf : zzb2.zzkq) {
            zzaj.zze zze2 = (zzaj.zze) zza(zzf2.zzkG, Integer.valueOf(valueOf).intValue(), "properties");
            String str = (String) zza(zzf2.zzkE, zze2.key, "keys");
            zzak.zza zza2 = (zzak.zza) zza(zzaArr, zze2.value, "values");
            if (zzai.PUSH_AFTER_EVALUATE.toString().equals(str)) {
                zzTy.zzo(zza2);
            } else {
                zzTy.zzb(str, zza2);
            }
        }
        return zzTy.zzTz();
    }

    private static zze zza(zzaj.zzg zzg2, List<zza> list, List<zza> list2, List<zza> list3, zzaj.zzf zzf2) {
        zzf zzTD = zze.zzTD();
        for (int valueOf : zzg2.zzkU) {
            zzTD.zzd(list3.get(Integer.valueOf(valueOf).intValue()));
        }
        for (int valueOf2 : zzg2.zzkV) {
            zzTD.zze(list3.get(Integer.valueOf(valueOf2).intValue()));
        }
        for (int valueOf3 : zzg2.zzkW) {
            zzTD.zzf(list.get(Integer.valueOf(valueOf3).intValue()));
        }
        for (int valueOf4 : zzg2.zzkY) {
            zzTD.zzii(zzf2.zzkF[Integer.valueOf(valueOf4).intValue()].string);
        }
        for (int valueOf5 : zzg2.zzkX) {
            zzTD.zzg(list.get(Integer.valueOf(valueOf5).intValue()));
        }
        for (int valueOf6 : zzg2.zzkZ) {
            zzTD.zzij(zzf2.zzkF[Integer.valueOf(valueOf6).intValue()].string);
        }
        for (int valueOf7 : zzg2.zzla) {
            zzTD.zzh(list2.get(Integer.valueOf(valueOf7).intValue()));
        }
        for (int valueOf8 : zzg2.zzlc) {
            zzTD.zzik(zzf2.zzkF[Integer.valueOf(valueOf8).intValue()].string);
        }
        for (int valueOf9 : zzg2.zzlb) {
            zzTD.zzi(list2.get(Integer.valueOf(valueOf9).intValue()));
        }
        for (int valueOf10 : zzg2.zzld) {
            zzTD.zzil(zzf2.zzkF[Integer.valueOf(valueOf10).intValue()].string);
        }
        return zzTD.zzTG();
    }

    private static <T> T zza(T[] tArr, int i, String str) {
        if (i < 0 || i >= tArr.length) {
            zzhS(new StringBuilder(String.valueOf(str).length() + 45).append("Index out of bounds detected: ").append(i).append(" in ").append(str).toString());
        }
        return tArr[i];
    }

    public static zzc zzb(zzaj.zzf zzf2) {
        zzak.zza[] zzaArr = new zzak.zza[zzf2.zzkF.length];
        for (int i = 0; i < zzf2.zzkF.length; i++) {
            zza(i, zzf2, zzaArr, new HashSet(0));
        }
        zzd zzTA = zzc.zzTA();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < zzf2.zzkI.length; i2++) {
            arrayList.add(zza(zzf2.zzkI[i2], zzf2, zzaArr, i2));
        }
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < zzf2.zzkJ.length; i3++) {
            arrayList2.add(zza(zzf2.zzkJ[i3], zzf2, zzaArr, i3));
        }
        ArrayList arrayList3 = new ArrayList();
        for (int i4 = 0; i4 < zzf2.zzkH.length; i4++) {
            zza zza2 = zza(zzf2.zzkH[i4], zzf2, zzaArr, i4);
            zzTA.zzc(zza2);
            arrayList3.add(zza2);
        }
        for (zzaj.zzg zza3 : zzf2.zzkK) {
            zzTA.zzb(zza(zza3, arrayList, arrayList3, arrayList2, zzf2));
        }
        zzTA.zzih(zzf2.version);
        zzTA.zznO(zzf2.zzkS);
        return zzTA.zzTC();
    }

    public static void zzc(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[FileUtils.FileMode.MODE_ISGID];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    private static void zzhS(String str) {
        zzbo.e(str);
        throw new zzg(str);
    }

    public static zzak.zza zzm(zzak.zza zza2) {
        zzak.zza zza3 = new zzak.zza();
        zza3.type = zza2.type;
        zza3.zzlC = (int[]) zza2.zzlC.clone();
        if (zza2.zzlD) {
            zza3.zzlD = zza2.zzlD;
        }
        return zza3;
    }

    private static zzaj.zzh zzn(zzak.zza zza2) {
        if (((zzaj.zzh) zza2.zza(zzaj.zzh.zzle)) == null) {
            String valueOf = String.valueOf(zza2);
            zzhS(new StringBuilder(String.valueOf(valueOf).length() + 54).append("Expected a ServingValue and didn't get one. Value is: ").append(valueOf).toString());
        }
        return (zzaj.zzh) zza2.zza(zzaj.zzh.zzle);
    }
}
