package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

class zzcm extends zzam {
    private static final String ID = zzah.REGEX_GROUP.toString();
    private static final String zzbHq = zzai.ARG0.toString();
    private static final String zzbHr = zzai.ARG1.toString();
    private static final String zzbHs = zzai.IGNORE_CASE.toString();
    private static final String zzbHt = zzai.GROUP.toString();

    public zzcm() {
        super(ID, zzbHq, zzbHr);
    }

    public boolean zzQd() {
        return true;
    }

    public zzak.zza zzZ(Map<String, zzak.zza> map) {
        int i;
        zzak.zza zza = map.get(zzbHq);
        zzak.zza zza2 = map.get(zzbHr);
        if (zza == null || zza == zzdl.zzRT() || zza2 == null || zza2 == zzdl.zzRT()) {
            return zzdl.zzRT();
        }
        int i2 = 64;
        if (zzdl.zzi(map.get(zzbHs)).booleanValue()) {
            i2 = 66;
        }
        zzak.zza zza3 = map.get(zzbHt);
        if (zza3 != null) {
            Long zzg = zzdl.zzg(zza3);
            if (zzg == zzdl.zzRO()) {
                return zzdl.zzRT();
            }
            i = zzg.intValue();
            if (i < 0) {
                return zzdl.zzRT();
            }
        } else {
            i = 1;
        }
        try {
            String zze = zzdl.zze(zza);
            String str = null;
            Matcher matcher = Pattern.compile(zzdl.zze(zza2), i2).matcher(zze);
            if (matcher.find() && matcher.groupCount() >= i) {
                str = matcher.group(i);
            }
            return str == null ? zzdl.zzRT() : zzdl.zzS(str);
        } catch (PatternSyntaxException e2) {
            return zzdl.zzRT();
        }
    }
}
