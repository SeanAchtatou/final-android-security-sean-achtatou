package com.google.android.gms.internal;

import java.io.Closeable;

public interface zzbzb extends Closeable {
    void close();

    long read(zzbyr zzbyr, long j);
}
