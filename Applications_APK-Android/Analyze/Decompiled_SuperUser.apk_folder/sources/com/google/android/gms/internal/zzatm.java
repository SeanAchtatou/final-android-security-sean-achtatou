package com.google.android.gms.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzac;
import java.util.Iterator;

public class zzatm {
    final String mAppId;
    final String mName;
    final String mOrigin;
    final long zzaxb;
    final zzato zzbrA;
    final long zzbrz;

    zzatm(zzaue zzaue, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        zzac.zzdr(str2);
        zzac.zzdr(str3);
        this.mAppId = str2;
        this.mName = str3;
        this.mOrigin = TextUtils.isEmpty(str) ? null : str;
        this.zzaxb = j;
        this.zzbrz = j2;
        if (this.zzbrz != 0 && this.zzbrz > this.zzaxb) {
            zzaue.zzKl().zzMb().zzj("Event created with reverse previous/current timestamps. appId", zzatx.zzfE(str2));
        }
        this.zzbrA = zza(zzaue, bundle);
    }

    private zzatm(zzaue zzaue, String str, String str2, String str3, long j, long j2, zzato zzato) {
        zzac.zzdr(str2);
        zzac.zzdr(str3);
        zzac.zzw(zzato);
        this.mAppId = str2;
        this.mName = str3;
        this.mOrigin = TextUtils.isEmpty(str) ? null : str;
        this.zzaxb = j;
        this.zzbrz = j2;
        if (this.zzbrz != 0 && this.zzbrz > this.zzaxb) {
            zzaue.zzKl().zzMb().zzj("Event created with reverse previous/current timestamps. appId", zzatx.zzfE(str2));
        }
        this.zzbrA = zzato;
    }

    static zzato zza(zzaue zzaue, Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return new zzato(new Bundle());
        }
        Bundle bundle2 = new Bundle(bundle);
        Iterator<String> it = bundle2.keySet().iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next == null) {
                zzaue.zzKl().zzLZ().log("Param name can't be null");
                it.remove();
            } else {
                Object zzk = zzaue.zzKh().zzk(next, bundle2.get(next));
                if (zzk == null) {
                    zzaue.zzKl().zzMb().zzj("Param value can't be null", next);
                    it.remove();
                } else {
                    zzaue.zzKh().zza(bundle2, next, zzk);
                }
            }
        }
        return new zzato(bundle2);
    }

    public String toString() {
        String str = this.mAppId;
        String str2 = this.mName;
        String valueOf = String.valueOf(this.zzbrA);
        return new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length()).append("Event{appId='").append(str).append("'").append(", name='").append(str2).append("'").append(", params=").append(valueOf).append("}").toString();
    }

    /* access modifiers changed from: package-private */
    public zzatm zza(zzaue zzaue, long j) {
        return new zzatm(zzaue, this.mOrigin, this.mAppId, this.mName, this.zzaxb, j, this.zzbrA);
    }
}
