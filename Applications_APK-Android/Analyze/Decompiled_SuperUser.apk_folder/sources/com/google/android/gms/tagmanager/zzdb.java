package com.google.android.gms.tagmanager;

public abstract class zzdb {
    public abstract void dispatch();

    public abstract void zzaS(boolean z);

    public abstract void zznO();
}
