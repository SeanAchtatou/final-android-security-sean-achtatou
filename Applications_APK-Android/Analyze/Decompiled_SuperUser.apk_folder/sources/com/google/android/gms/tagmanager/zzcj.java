package com.google.android.gms.tagmanager;

import android.net.Uri;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

class zzcj {
    private static zzcj zzbHg;
    private volatile String zzbEX;
    private volatile zza zzbHh;
    private volatile String zzbHi;
    private volatile String zzbHj;

    enum zza {
        NONE,
        CONTAINER,
        CONTAINER_DEBUG
    }

    zzcj() {
        clear();
    }

    static zzcj zzRg() {
        zzcj zzcj;
        synchronized (zzcj.class) {
            if (zzbHg == null) {
                zzbHg = new zzcj();
            }
            zzcj = zzbHg;
        }
        return zzcj;
    }

    private String zzhn(String str) {
        return str.split("&")[0].split("=")[1];
    }

    private String zzw(Uri uri) {
        return uri.getQuery().replace("&gtm_debug=x", "");
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.zzbHh = zza.NONE;
        this.zzbHi = null;
        this.zzbEX = null;
        this.zzbHj = null;
    }

    /* access modifiers changed from: package-private */
    public String getContainerId() {
        return this.zzbEX;
    }

    /* access modifiers changed from: package-private */
    public zza zzRh() {
        return this.zzbHh;
    }

    /* access modifiers changed from: package-private */
    public String zzRi() {
        return this.zzbHi;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean zzv(Uri uri) {
        boolean z = true;
        synchronized (this) {
            try {
                String decode = URLDecoder.decode(uri.toString(), "UTF-8");
                if (decode.matches("^tagmanager.c.\\S+:\\/\\/preview\\/p\\?id=\\S+&gtm_auth=\\S+&gtm_preview=\\d+(&gtm_debug=x)?$")) {
                    String valueOf = String.valueOf(decode);
                    zzbo.v(valueOf.length() != 0 ? "Container preview url: ".concat(valueOf) : new String("Container preview url: "));
                    if (decode.matches(".*?&gtm_debug=x$")) {
                        this.zzbHh = zza.CONTAINER_DEBUG;
                    } else {
                        this.zzbHh = zza.CONTAINER;
                    }
                    this.zzbHj = zzw(uri);
                    if (this.zzbHh == zza.CONTAINER || this.zzbHh == zza.CONTAINER_DEBUG) {
                        String valueOf2 = String.valueOf("/r?");
                        String valueOf3 = String.valueOf(this.zzbHj);
                        this.zzbHi = valueOf3.length() != 0 ? valueOf2.concat(valueOf3) : new String(valueOf2);
                    }
                    this.zzbEX = zzhn(this.zzbHj);
                } else if (!decode.matches("^tagmanager.c.\\S+:\\/\\/preview\\/p\\?id=\\S+&gtm_preview=$")) {
                    String valueOf4 = String.valueOf(decode);
                    zzbo.zzbh(valueOf4.length() != 0 ? "Invalid preview uri: ".concat(valueOf4) : new String("Invalid preview uri: "));
                    z = false;
                } else if (zzhn(uri.getQuery()).equals(this.zzbEX)) {
                    String valueOf5 = String.valueOf(this.zzbEX);
                    zzbo.v(valueOf5.length() != 0 ? "Exit preview mode for container: ".concat(valueOf5) : new String("Exit preview mode for container: "));
                    this.zzbHh = zza.NONE;
                    this.zzbHi = null;
                } else {
                    z = false;
                }
            } catch (UnsupportedEncodingException e2) {
                z = false;
            }
        }
        return z;
    }
}
