package com.google.android.gms.measurement;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.google.android.gms.internal.zzaum;

public final class AppMeasurementService extends Service implements zzaum.zza {
    private zzaum zzbqj;

    private zzaum zzJT() {
        if (this.zzbqj == null) {
            this.zzbqj = new zzaum(this);
        }
        return this.zzbqj;
    }

    public boolean callServiceStopSelfResult(int i) {
        return stopSelfResult(i);
    }

    public Context getContext() {
        return this;
    }

    public IBinder onBind(Intent intent) {
        return zzJT().onBind(intent);
    }

    public void onCreate() {
        super.onCreate();
        zzJT().onCreate();
    }

    public void onDestroy() {
        zzJT().onDestroy();
        super.onDestroy();
    }

    public void onRebind(Intent intent) {
        zzJT().onRebind(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        zzJT().onStartCommand(intent, i, i2);
        AppMeasurementReceiver.completeWakefulIntent(intent);
        return 2;
    }

    public boolean onUnbind(Intent intent) {
        return zzJT().onUnbind(intent);
    }
}
