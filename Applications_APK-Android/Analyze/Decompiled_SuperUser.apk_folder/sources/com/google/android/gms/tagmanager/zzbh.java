package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import com.google.android.gms.internal.zzbjf;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class zzbh {
    private static zzak.zza zzL(Object obj) {
        return zzdl.zzS(zzM(obj));
    }

    static Object zzM(Object obj) {
        if (obj instanceof JSONArray) {
            throw new RuntimeException("JSONArrays are not supported");
        } else if (JSONObject.NULL.equals(obj)) {
            throw new RuntimeException("JSON nulls are not supported");
        } else if (!(obj instanceof JSONObject)) {
            return obj;
        } else {
            JSONObject jSONObject = (JSONObject) obj;
            HashMap hashMap = new HashMap();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, zzM(jSONObject.get(next)));
            }
            return hashMap;
        }
    }

    public static zzbjf.zzc zzhl(String str) {
        zzak.zza zzL = zzL(new JSONObject(str));
        zzbjf.zzd zzTA = zzbjf.zzc.zzTA();
        for (int i = 0; i < zzL.zzlv.length; i++) {
            zzTA.zzc(zzbjf.zza.zzTy().zzb(zzai.INSTANCE_NAME.toString(), zzL.zzlv[i]).zzb(zzai.FUNCTION.toString(), zzdl.zzhw(zzn.zzQf())).zzb(zzn.zzQg(), zzL.zzlw[i]).zzTz());
        }
        return zzTA.zzTC();
    }
}
