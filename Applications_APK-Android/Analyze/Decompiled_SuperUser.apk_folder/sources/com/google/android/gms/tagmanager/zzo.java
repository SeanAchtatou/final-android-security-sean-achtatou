package com.google.android.gms.tagmanager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tagmanager.ContainerHolder;

class zzo implements ContainerHolder {
    private boolean zzaLQ;
    private Status zzair;
    private Container zzbFf;
    private Container zzbFg;
    private zzb zzbFh;
    private zza zzbFi;
    private TagManager zzbFj;
    private final Looper zzrs;

    public interface zza {
        String zzQj();

        void zzQl();

        void zzgW(String str);
    }

    private class zzb extends Handler {
        private final ContainerHolder.ContainerAvailableListener zzbFk;

        public zzb(ContainerHolder.ContainerAvailableListener containerAvailableListener, Looper looper) {
            super(looper);
            this.zzbFk = containerAvailableListener;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    zzgY((String) message.obj);
                    return;
                default:
                    zzbo.e("Don't know how to handle this message.");
                    return;
            }
        }

        public void zzgX(String str) {
            sendMessage(obtainMessage(1, str));
        }

        /* access modifiers changed from: protected */
        public void zzgY(String str) {
            this.zzbFk.onContainerAvailable(zzo.this, str);
        }
    }

    public zzo(Status status) {
        this.zzair = status;
        this.zzrs = null;
    }

    public zzo(TagManager tagManager, Looper looper, Container container, zza zza2) {
        this.zzbFj = tagManager;
        this.zzrs = looper == null ? Looper.getMainLooper() : looper;
        this.zzbFf = container;
        this.zzbFi = zza2;
        this.zzair = Status.zzazx;
        tagManager.zza(this);
    }

    private void zzQk() {
        if (this.zzbFh != null) {
            this.zzbFh.zzgX(this.zzbFg.zzQh());
        }
    }

    public synchronized Container getContainer() {
        Container container = null;
        synchronized (this) {
            if (this.zzaLQ) {
                zzbo.e("ContainerHolder is released.");
            } else {
                if (this.zzbFg != null) {
                    this.zzbFf = this.zzbFg;
                    this.zzbFg = null;
                }
                container = this.zzbFf;
            }
        }
        return container;
    }

    /* access modifiers changed from: package-private */
    public String getContainerId() {
        if (!this.zzaLQ) {
            return this.zzbFf.getContainerId();
        }
        zzbo.e("getContainerId called on a released ContainerHolder.");
        return "";
    }

    public Status getStatus() {
        return this.zzair;
    }

    public synchronized void refresh() {
        if (this.zzaLQ) {
            zzbo.e("Refreshing a released ContainerHolder.");
        } else {
            this.zzbFi.zzQl();
        }
    }

    public synchronized void release() {
        if (this.zzaLQ) {
            zzbo.e("Releasing a released ContainerHolder.");
        } else {
            this.zzaLQ = true;
            this.zzbFj.zzb(this);
            this.zzbFf.release();
            this.zzbFf = null;
            this.zzbFg = null;
            this.zzbFi = null;
            this.zzbFh = null;
        }
    }

    public synchronized void setContainerAvailableListener(ContainerHolder.ContainerAvailableListener containerAvailableListener) {
        if (this.zzaLQ) {
            zzbo.e("ContainerHolder is released.");
        } else if (containerAvailableListener == null) {
            this.zzbFh = null;
        } else {
            this.zzbFh = new zzb(containerAvailableListener, this.zzrs);
            if (this.zzbFg != null) {
                zzQk();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String zzQj() {
        if (!this.zzaLQ) {
            return this.zzbFi.zzQj();
        }
        zzbo.e("setCtfeUrlPathAndQuery called on a released ContainerHolder.");
        return "";
    }

    public synchronized void zza(Container container) {
        if (!this.zzaLQ) {
            if (container == null) {
                zzbo.e("Unexpected null container.");
            } else {
                this.zzbFg = container;
                zzQk();
            }
        }
    }

    public synchronized void zzgU(String str) {
        if (!this.zzaLQ) {
            this.zzbFf.zzgU(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void zzgW(String str) {
        if (this.zzaLQ) {
            zzbo.e("setCtfeUrlPathAndQuery called on a released ContainerHolder.");
        } else {
            this.zzbFi.zzgW(str);
        }
    }
}
