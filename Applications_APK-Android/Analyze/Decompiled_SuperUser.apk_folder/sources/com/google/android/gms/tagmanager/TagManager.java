package com.google.android.gms.tagmanager;

import android.annotation.TargetApi;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.tagmanager.DataLayer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class TagManager {
    private static TagManager zzbIx;
    private final Context mContext;
    private final DataLayer zzbEY;
    private final zzt zzbHx;
    private final zza zzbIu;
    private final zzdb zzbIv;
    private final ConcurrentMap<String, zzo> zzbIw;

    public interface zza {
        zzp zza(Context context, TagManager tagManager, Looper looper, String str, int i, zzt zzt);
    }

    TagManager(Context context, zza zza2, DataLayer dataLayer, zzdb zzdb) {
        if (context == null) {
            throw new NullPointerException("context cannot be null");
        }
        this.mContext = context.getApplicationContext();
        this.zzbIv = zzdb;
        this.zzbIu = zza2;
        this.zzbIw = new ConcurrentHashMap();
        this.zzbEY = dataLayer;
        this.zzbEY.zza(new DataLayer.zzb() {
            public void zzaa(Map<String, Object> map) {
                Object obj = map.get(DataLayer.EVENT_KEY);
                if (obj != null) {
                    TagManager.this.zzhs(obj.toString());
                }
            }
        });
        this.zzbEY.zza(new zzd(this.mContext));
        this.zzbHx = new zzt();
        zzRG();
        zzRH();
    }

    public static TagManager getInstance(Context context) {
        TagManager tagManager;
        synchronized (TagManager.class) {
            if (zzbIx == null) {
                if (context == null) {
                    zzbo.e("TagManager.getInstance requires non-null context.");
                    throw new NullPointerException();
                }
                zzbIx = new TagManager(context, new zza() {
                    public zzp zza(Context context, TagManager tagManager, Looper looper, String str, int i, zzt zzt) {
                        return new zzp(context, tagManager, looper, str, i, zzt);
                    }
                }, new DataLayer(new zzx(context)), zzdc.zzRA());
            }
            tagManager = zzbIx;
        }
        return tagManager;
    }

    @TargetApi(14)
    private void zzRG() {
        int i = Build.VERSION.SDK_INT;
        this.mContext.registerComponentCallbacks(new ComponentCallbacks2() {
            public void onConfigurationChanged(Configuration configuration) {
            }

            public void onLowMemory() {
            }

            public void onTrimMemory(int i) {
                if (i == 20) {
                    TagManager.this.dispatch();
                }
            }
        });
    }

    private void zzRH() {
        zza.zzbS(this.mContext);
    }

    /* access modifiers changed from: private */
    public void zzhs(String str) {
        for (zzo zzgU : this.zzbIw.values()) {
            zzgU.zzgU(str);
        }
    }

    public void dispatch() {
        this.zzbIv.dispatch();
    }

    public DataLayer getDataLayer() {
        return this.zzbEY;
    }

    public PendingResult<ContainerHolder> loadContainerDefaultOnly(String str, int i) {
        zzp zza2 = this.zzbIu.zza(this.mContext, this, null, str, i, this.zzbHx);
        zza2.zzQm();
        return zza2;
    }

    public PendingResult<ContainerHolder> loadContainerDefaultOnly(String str, int i, Handler handler) {
        zzp zza2 = this.zzbIu.zza(this.mContext, this, handler.getLooper(), str, i, this.zzbHx);
        zza2.zzQm();
        return zza2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferFresh(String str, int i) {
        zzp zza2 = this.zzbIu.zza(this.mContext, this, null, str, i, this.zzbHx);
        zza2.zzQo();
        return zza2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferFresh(String str, int i, Handler handler) {
        zzp zza2 = this.zzbIu.zza(this.mContext, this, handler.getLooper(), str, i, this.zzbHx);
        zza2.zzQo();
        return zza2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String str, int i) {
        zzp zza2 = this.zzbIu.zza(this.mContext, this, null, str, i, this.zzbHx);
        zza2.zzQn();
        return zza2;
    }

    public PendingResult<ContainerHolder> loadContainerPreferNonDefault(String str, int i, Handler handler) {
        zzp zza2 = this.zzbIu.zza(this.mContext, this, handler.getLooper(), str, i, this.zzbHx);
        zza2.zzQn();
        return zza2;
    }

    public void setVerboseLoggingEnabled(boolean z) {
        zzbo.setLogLevel(z ? 2 : 5);
    }

    public int zza(zzo zzo) {
        this.zzbIw.put(zzo.getContainerId(), zzo);
        return this.zzbIw.size();
    }

    public boolean zzb(zzo zzo) {
        return this.zzbIw.remove(zzo.getContainerId()) != null;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean zzv(Uri uri) {
        boolean z;
        zzcj zzRg = zzcj.zzRg();
        if (zzRg.zzv(uri)) {
            String containerId = zzRg.getContainerId();
            switch (zzRg.zzRh()) {
                case NONE:
                    zzo zzo = this.zzbIw.get(containerId);
                    if (zzo != null) {
                        zzo.zzgW(null);
                        zzo.refresh();
                        break;
                    }
                    break;
                case CONTAINER:
                case CONTAINER_DEBUG:
                    for (String next : this.zzbIw.keySet()) {
                        zzo zzo2 = this.zzbIw.get(next);
                        if (next.equals(containerId)) {
                            zzo2.zzgW(zzRg.zzRi());
                            zzo2.refresh();
                        } else if (zzo2.zzQj() != null) {
                            zzo2.zzgW(null);
                            zzo2.refresh();
                        }
                    }
                    break;
            }
            z = true;
        } else {
            z = false;
        }
        return z;
    }
}
