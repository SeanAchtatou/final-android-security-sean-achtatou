package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.internal.zzah;
import com.google.android.gms.internal.zzai;
import com.google.android.gms.internal.zzak;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class zzdm extends zzdj {
    private static final String ID = zzah.UNIVERSAL_ANALYTICS.toString();
    private static final String zzbIO = zzai.ACCOUNT.toString();
    private static final String zzbIP = zzai.ANALYTICS_PASS_THROUGH.toString();
    private static final String zzbIQ = zzai.ENABLE_ECOMMERCE.toString();
    private static final String zzbIR = zzai.ECOMMERCE_USE_DATA_LAYER.toString();
    private static final String zzbIS = zzai.ECOMMERCE_MACRO_DATA.toString();
    private static final String zzbIT = zzai.ANALYTICS_FIELDS.toString();
    private static final String zzbIU = zzai.TRACK_TRANSACTION.toString();
    private static final String zzbIV = zzai.TRANSACTION_DATALAYER_MAP.toString();
    private static final String zzbIW = zzai.TRANSACTION_ITEM_DATALAYER_MAP.toString();
    private static final List<String> zzbIX = Arrays.asList(ProductAction.ACTION_DETAIL, ProductAction.ACTION_CHECKOUT, "checkout_option", "click", ProductAction.ACTION_ADD, ProductAction.ACTION_REMOVE, ProductAction.ACTION_PURCHASE, ProductAction.ACTION_REFUND);
    private static final Pattern zzbIY = Pattern.compile("dimension(\\d+)");
    private static final Pattern zzbIZ = Pattern.compile("metric(\\d+)");
    private static Map<String, String> zzbJa;
    private static Map<String, String> zzbJb;
    private final DataLayer zzbEY;
    private final Set<String> zzbJc;
    private final zzdi zzbJd;

    public zzdm(Context context, DataLayer dataLayer) {
        this(context, dataLayer, new zzdi(context));
    }

    zzdm(Context context, DataLayer dataLayer, zzdi zzdi) {
        super(ID, new String[0]);
        this.zzbEY = dataLayer;
        this.zzbJd = zzdi;
        this.zzbJc = new HashSet();
        this.zzbJc.add("");
        this.zzbJc.add("0");
        this.zzbJc.add("false");
    }

    private Double zzW(Object obj) {
        if (obj instanceof String) {
            try {
                return Double.valueOf((String) obj);
            } catch (NumberFormatException e2) {
                String valueOf = String.valueOf(e2.getMessage());
                throw new RuntimeException(valueOf.length() != 0 ? "Cannot convert the object to Double: ".concat(valueOf) : new String("Cannot convert the object to Double: "));
            }
        } else if (obj instanceof Integer) {
            return Double.valueOf(((Integer) obj).doubleValue());
        } else {
            if (obj instanceof Double) {
                return (Double) obj;
            }
            String valueOf2 = String.valueOf(obj.toString());
            throw new RuntimeException(valueOf2.length() != 0 ? "Cannot convert the object to Double: ".concat(valueOf2) : new String("Cannot convert the object to Double: "));
        }
    }

    private Integer zzX(Object obj) {
        if (obj instanceof String) {
            try {
                return Integer.valueOf((String) obj);
            } catch (NumberFormatException e2) {
                String valueOf = String.valueOf(e2.getMessage());
                throw new RuntimeException(valueOf.length() != 0 ? "Cannot convert the object to Integer: ".concat(valueOf) : new String("Cannot convert the object to Integer: "));
            }
        } else if (obj instanceof Double) {
            return Integer.valueOf(((Double) obj).intValue());
        } else {
            if (obj instanceof Integer) {
                return (Integer) obj;
            }
            String valueOf2 = String.valueOf(obj.toString());
            throw new RuntimeException(valueOf2.length() != 0 ? "Cannot convert the object to Integer: ".concat(valueOf2) : new String("Cannot convert the object to Integer: "));
        }
    }

    private void zza(Tracker tracker, Map<String, zzak.zza> map) {
        String zzhB = zzhB("transactionId");
        if (zzhB == null) {
            zzbo.e("Cannot find transactionId in data layer.");
            return;
        }
        LinkedList<Map> linkedList = new LinkedList<>();
        try {
            Map<String, String> zzk = zzk(map.get(zzbIT));
            zzk.put("&t", "transaction");
            for (Map.Entry next : zzal(map).entrySet()) {
                zze(zzk, (String) next.getValue(), zzhB((String) next.getKey()));
            }
            linkedList.add(zzk);
            List<Map<String, String>> zzhC = zzhC("transactionProducts");
            if (zzhC != null) {
                for (Map next2 : zzhC) {
                    if (next2.get("name") == null) {
                        zzbo.e("Unable to send transaction item hit due to missing 'name' field.");
                        return;
                    }
                    Map<String, String> zzk2 = zzk(map.get(zzbIT));
                    zzk2.put("&t", "item");
                    zzk2.put("&ti", zzhB);
                    for (Map.Entry next3 : zzam(map).entrySet()) {
                        zze(zzk2, (String) next3.getValue(), (String) next2.get(next3.getKey()));
                    }
                    linkedList.add(zzk2);
                }
            }
            for (Map send : linkedList) {
                tracker.send(send);
            }
        } catch (IllegalArgumentException e2) {
            zzbo.zzb("Unable to send transaction", e2);
        }
    }

    private Promotion zzaj(Map<String, String> map) {
        Promotion promotion = new Promotion();
        String str = map.get("id");
        if (str != null) {
            promotion.setId(String.valueOf(str));
        }
        String str2 = map.get("name");
        if (str2 != null) {
            promotion.setName(String.valueOf(str2));
        }
        String str3 = map.get("creative");
        if (str3 != null) {
            promotion.setCreative(String.valueOf(str3));
        }
        String str4 = map.get("position");
        if (str4 != null) {
            promotion.setPosition(String.valueOf(str4));
        }
        return promotion;
    }

    private Product zzak(Map<String, Object> map) {
        Product product = new Product();
        Object obj = map.get("id");
        if (obj != null) {
            product.setId(String.valueOf(obj));
        }
        Object obj2 = map.get("name");
        if (obj2 != null) {
            product.setName(String.valueOf(obj2));
        }
        Object obj3 = map.get("brand");
        if (obj3 != null) {
            product.setBrand(String.valueOf(obj3));
        }
        Object obj4 = map.get("category");
        if (obj4 != null) {
            product.setCategory(String.valueOf(obj4));
        }
        Object obj5 = map.get("variant");
        if (obj5 != null) {
            product.setVariant(String.valueOf(obj5));
        }
        Object obj6 = map.get(FirebaseAnalytics.Param.COUPON);
        if (obj6 != null) {
            product.setCouponCode(String.valueOf(obj6));
        }
        Object obj7 = map.get("position");
        if (obj7 != null) {
            product.setPosition(zzX(obj7).intValue());
        }
        Object obj8 = map.get(FirebaseAnalytics.Param.PRICE);
        if (obj8 != null) {
            product.setPrice(zzW(obj8).doubleValue());
        }
        Object obj9 = map.get(FirebaseAnalytics.Param.QUANTITY);
        if (obj9 != null) {
            product.setQuantity(zzX(obj9).intValue());
        }
        for (String next : map.keySet()) {
            Matcher matcher = zzbIY.matcher(next);
            if (matcher.matches()) {
                try {
                    product.setCustomDimension(Integer.parseInt(matcher.group(1)), String.valueOf(map.get(next)));
                } catch (NumberFormatException e2) {
                    String valueOf = String.valueOf(next);
                    zzbo.zzbh(valueOf.length() != 0 ? "illegal number in custom dimension value: ".concat(valueOf) : new String("illegal number in custom dimension value: "));
                }
            } else {
                Matcher matcher2 = zzbIZ.matcher(next);
                if (matcher2.matches()) {
                    try {
                        product.setCustomMetric(Integer.parseInt(matcher2.group(1)), zzX(map.get(next)).intValue());
                    } catch (NumberFormatException e3) {
                        String valueOf2 = String.valueOf(next);
                        zzbo.zzbh(valueOf2.length() != 0 ? "illegal number in custom metric value: ".concat(valueOf2) : new String("illegal number in custom metric value: "));
                    }
                }
            }
        }
        return product;
    }

    private Map<String, String> zzal(Map<String, zzak.zza> map) {
        zzak.zza zza = map.get(zzbIV);
        if (zza != null) {
            return zzc(zza);
        }
        if (zzbJa == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("transactionId", "&ti");
            hashMap.put("transactionAffiliation", "&ta");
            hashMap.put("transactionTax", "&tt");
            hashMap.put("transactionShipping", "&ts");
            hashMap.put("transactionTotal", "&tr");
            hashMap.put("transactionCurrency", "&cu");
            zzbJa = hashMap;
        }
        return zzbJa;
    }

    private Map<String, String> zzam(Map<String, zzak.zza> map) {
        zzak.zza zza = map.get(zzbIW);
        if (zza != null) {
            return zzc(zza);
        }
        if (zzbJb == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("name", "&in");
            hashMap.put("sku", "&ic");
            hashMap.put("category", "&iv");
            hashMap.put(FirebaseAnalytics.Param.PRICE, "&ip");
            hashMap.put(FirebaseAnalytics.Param.QUANTITY, "&iq");
            hashMap.put(FirebaseAnalytics.Param.CURRENCY, "&cu");
            zzbJb = hashMap;
        }
        return zzbJb;
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void zzb(com.google.android.gms.analytics.Tracker r8, java.util.Map<java.lang.String, com.google.android.gms.internal.zzak.zza> r9) {
        /*
            r7 = this;
            r1 = 0
            com.google.android.gms.analytics.HitBuilders$ScreenViewBuilder r3 = new com.google.android.gms.analytics.HitBuilders$ScreenViewBuilder
            r3.<init>()
            java.lang.String r0 = com.google.android.gms.tagmanager.zzdm.zzbIT
            java.lang.Object r0 = r9.get(r0)
            com.google.android.gms.internal.zzak$zza r0 = (com.google.android.gms.internal.zzak.zza) r0
            java.util.Map r4 = r7.zzk(r0)
            r3.setAll(r4)
            java.lang.String r0 = com.google.android.gms.tagmanager.zzdm.zzbIR
            boolean r0 = r7.zzi(r9, r0)
            if (r0 == 0) goto L_0x008c
            com.google.android.gms.tagmanager.DataLayer r0 = r7.zzbEY
            java.lang.String r2 = "ecommerce"
            java.lang.Object r0 = r0.get(r2)
            boolean r2 = r0 instanceof java.util.Map
            if (r2 == 0) goto L_0x01d6
            java.util.Map r0 = (java.util.Map) r0
        L_0x002b:
            r2 = r0
        L_0x002c:
            if (r2 == 0) goto L_0x01a2
            java.lang.String r0 = "&cu"
            java.lang.Object r0 = r4.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0040
            java.lang.String r0 = "currencyCode"
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
        L_0x0040:
            if (r0 == 0) goto L_0x0047
            java.lang.String r4 = "&cu"
            r3.set(r4, r0)
        L_0x0047:
            java.lang.String r0 = "impressions"
            java.lang.Object r0 = r2.get(r0)
            boolean r4 = r0 instanceof java.util.List
            if (r4 == 0) goto L_0x00a6
            java.util.List r0 = (java.util.List) r0
            java.util.Iterator r4 = r0.iterator()
        L_0x0057:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00a6
            java.lang.Object r0 = r4.next()
            java.util.Map r0 = (java.util.Map) r0
            com.google.android.gms.analytics.ecommerce.Product r5 = r7.zzak(r0)     // Catch:{ RuntimeException -> 0x0073 }
            java.lang.String r6 = "list"
            java.lang.Object r0 = r0.get(r6)     // Catch:{ RuntimeException -> 0x0073 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ RuntimeException -> 0x0073 }
            r3.addImpression(r5, r0)     // Catch:{ RuntimeException -> 0x0073 }
            goto L_0x0057
        L_0x0073:
            r0 = move-exception
            java.lang.String r5 = "Failed to extract a product from DataLayer. "
            java.lang.String r0 = r0.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r6 = r0.length()
            if (r6 == 0) goto L_0x00a0
            java.lang.String r0 = r5.concat(r0)
        L_0x0088:
            com.google.android.gms.tagmanager.zzbo.e(r0)
            goto L_0x0057
        L_0x008c:
            java.lang.String r0 = com.google.android.gms.tagmanager.zzdm.zzbIS
            java.lang.Object r0 = r9.get(r0)
            com.google.android.gms.internal.zzak$zza r0 = (com.google.android.gms.internal.zzak.zza) r0
            java.lang.Object r0 = com.google.android.gms.tagmanager.zzdl.zzj(r0)
            boolean r2 = r0 instanceof java.util.Map
            if (r2 == 0) goto L_0x01d3
            java.util.Map r0 = (java.util.Map) r0
            r2 = r0
            goto L_0x002c
        L_0x00a0:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r5)
            goto L_0x0088
        L_0x00a6:
            java.lang.String r0 = "promoClick"
            boolean r0 = r2.containsKey(r0)
            if (r0 == 0) goto L_0x00f2
            java.lang.String r0 = "promoClick"
            java.lang.Object r0 = r2.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            java.lang.String r1 = "promotions"
            java.lang.Object r0 = r0.get(r1)
            java.util.List r0 = (java.util.List) r0
        L_0x00be:
            r1 = 1
            if (r0 == 0) goto L_0x0183
            java.util.Iterator r4 = r0.iterator()
        L_0x00c5:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0111
            java.lang.Object r0 = r4.next()
            java.util.Map r0 = (java.util.Map) r0
            com.google.android.gms.analytics.ecommerce.Promotion r0 = r7.zzaj(r0)     // Catch:{ RuntimeException -> 0x00d9 }
            r3.addPromotion(r0)     // Catch:{ RuntimeException -> 0x00d9 }
            goto L_0x00c5
        L_0x00d9:
            r0 = move-exception
            java.lang.String r5 = "Failed to extract a promotion from DataLayer. "
            java.lang.String r0 = r0.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r6 = r0.length()
            if (r6 == 0) goto L_0x010b
            java.lang.String r0 = r5.concat(r0)
        L_0x00ee:
            com.google.android.gms.tagmanager.zzbo.e(r0)
            goto L_0x00c5
        L_0x00f2:
            java.lang.String r0 = "promoView"
            boolean r0 = r2.containsKey(r0)
            if (r0 == 0) goto L_0x01d0
            java.lang.String r0 = "promoView"
            java.lang.Object r0 = r2.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            java.lang.String r1 = "promotions"
            java.lang.Object r0 = r0.get(r1)
            java.util.List r0 = (java.util.List) r0
            goto L_0x00be
        L_0x010b:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r5)
            goto L_0x00ee
        L_0x0111:
            java.lang.String r0 = "promoClick"
            boolean r0 = r2.containsKey(r0)
            if (r0 == 0) goto L_0x017c
            java.lang.String r0 = "&promoa"
            java.lang.String r1 = "click"
            r3.set(r0, r1)
            r0 = 0
        L_0x0121:
            if (r0 == 0) goto L_0x01a2
            java.util.List<java.lang.String> r0 = com.google.android.gms.tagmanager.zzdm.zzbIX
            java.util.Iterator r1 = r0.iterator()
        L_0x0129:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x01a2
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r4 = r2.containsKey(r0)
            if (r4 == 0) goto L_0x0129
            java.lang.Object r1 = r2.get(r0)
            java.util.Map r1 = (java.util.Map) r1
            java.lang.String r2 = "products"
            java.lang.Object r2 = r1.get(r2)
            java.util.List r2 = (java.util.List) r2
            if (r2 == 0) goto L_0x018b
            java.util.Iterator r4 = r2.iterator()
        L_0x014f:
            boolean r2 = r4.hasNext()
            if (r2 == 0) goto L_0x018b
            java.lang.Object r2 = r4.next()
            java.util.Map r2 = (java.util.Map) r2
            com.google.android.gms.analytics.ecommerce.Product r2 = r7.zzak(r2)     // Catch:{ RuntimeException -> 0x0163 }
            r3.addProduct(r2)     // Catch:{ RuntimeException -> 0x0163 }
            goto L_0x014f
        L_0x0163:
            r2 = move-exception
            java.lang.String r5 = "Failed to extract a product from DataLayer. "
            java.lang.String r2 = r2.getMessage()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            int r6 = r2.length()
            if (r6 == 0) goto L_0x0185
            java.lang.String r2 = r5.concat(r2)
        L_0x0178:
            com.google.android.gms.tagmanager.zzbo.e(r2)
            goto L_0x014f
        L_0x017c:
            java.lang.String r0 = "&promoa"
            java.lang.String r4 = "view"
            r3.set(r0, r4)
        L_0x0183:
            r0 = r1
            goto L_0x0121
        L_0x0185:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r5)
            goto L_0x0178
        L_0x018b:
            java.lang.String r2 = "actionField"
            boolean r2 = r1.containsKey(r2)     // Catch:{ RuntimeException -> 0x01b1 }
            if (r2 == 0) goto L_0x01aa
            java.lang.String r2 = "actionField"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ RuntimeException -> 0x01b1 }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ RuntimeException -> 0x01b1 }
            com.google.android.gms.analytics.ecommerce.ProductAction r0 = r7.zzh(r0, r1)     // Catch:{ RuntimeException -> 0x01b1 }
        L_0x019f:
            r3.setProductAction(r0)     // Catch:{ RuntimeException -> 0x01b1 }
        L_0x01a2:
            java.util.Map r0 = r3.build()
            r8.send(r0)
            return
        L_0x01aa:
            com.google.android.gms.analytics.ecommerce.ProductAction r1 = new com.google.android.gms.analytics.ecommerce.ProductAction     // Catch:{ RuntimeException -> 0x01b1 }
            r1.<init>(r0)     // Catch:{ RuntimeException -> 0x01b1 }
            r0 = r1
            goto L_0x019f
        L_0x01b1:
            r0 = move-exception
            java.lang.String r1 = "Failed to extract a product action from DataLayer. "
            java.lang.String r0 = r0.getMessage()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r2 = r0.length()
            if (r2 == 0) goto L_0x01ca
            java.lang.String r0 = r1.concat(r0)
        L_0x01c6:
            com.google.android.gms.tagmanager.zzbo.e(r0)
            goto L_0x01a2
        L_0x01ca:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
            goto L_0x01c6
        L_0x01d0:
            r0 = r1
            goto L_0x00be
        L_0x01d3:
            r2 = r1
            goto L_0x002c
        L_0x01d6:
            r0 = r1
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.zzdm.zzb(com.google.android.gms.analytics.Tracker, java.util.Map):void");
    }

    private Map<String, String> zzc(zzak.zza zza) {
        Object zzj = zzdl.zzj(zza);
        if (!(zzj instanceof Map)) {
            return null;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry entry : ((Map) zzj).entrySet()) {
            linkedHashMap.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return linkedHashMap;
    }

    private void zze(Map<String, String> map, String str, String str2) {
        if (str2 != null) {
            map.put(str, str2);
        }
    }

    private ProductAction zzh(String str, Map<String, Object> map) {
        ProductAction productAction = new ProductAction(str);
        Object obj = map.get("id");
        if (obj != null) {
            productAction.setTransactionId(String.valueOf(obj));
        }
        Object obj2 = map.get(FirebaseAnalytics.Param.AFFILIATION);
        if (obj2 != null) {
            productAction.setTransactionAffiliation(String.valueOf(obj2));
        }
        Object obj3 = map.get(FirebaseAnalytics.Param.COUPON);
        if (obj3 != null) {
            productAction.setTransactionCouponCode(String.valueOf(obj3));
        }
        Object obj4 = map.get("list");
        if (obj4 != null) {
            productAction.setProductActionList(String.valueOf(obj4));
        }
        Object obj5 = map.get("option");
        if (obj5 != null) {
            productAction.setCheckoutOptions(String.valueOf(obj5));
        }
        Object obj6 = map.get("revenue");
        if (obj6 != null) {
            productAction.setTransactionRevenue(zzW(obj6).doubleValue());
        }
        Object obj7 = map.get(FirebaseAnalytics.Param.TAX);
        if (obj7 != null) {
            productAction.setTransactionTax(zzW(obj7).doubleValue());
        }
        Object obj8 = map.get(FirebaseAnalytics.Param.SHIPPING);
        if (obj8 != null) {
            productAction.setTransactionShipping(zzW(obj8).doubleValue());
        }
        Object obj9 = map.get("step");
        if (obj9 != null) {
            productAction.setCheckoutStep(zzX(obj9).intValue());
        }
        return productAction;
    }

    private String zzhB(String str) {
        Object obj = this.zzbEY.get(str);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    private List<Map<String, String>> zzhC(String str) {
        Object obj = this.zzbEY.get(str);
        if (obj == null) {
            return null;
        }
        if (!(obj instanceof List)) {
            throw new IllegalArgumentException("transactionProducts should be of type List.");
        }
        for (Object obj2 : (List) obj) {
            if (!(obj2 instanceof Map)) {
                throw new IllegalArgumentException("Each element of transactionProducts should be of type Map.");
            }
        }
        return (List) obj;
    }

    private boolean zzi(Map<String, zzak.zza> map, String str) {
        zzak.zza zza = map.get(str);
        if (zza == null) {
            return false;
        }
        return zzdl.zzi(zza).booleanValue();
    }

    private Map<String, String> zzk(zzak.zza zza) {
        if (zza == null) {
            return new HashMap();
        }
        Map<String, String> zzc = zzc(zza);
        if (zzc == null) {
            return new HashMap();
        }
        String str = zzc.get("&aip");
        if (str != null && this.zzbJc.contains(str.toLowerCase())) {
            zzc.remove("&aip");
        }
        return zzc;
    }

    public /* bridge */ /* synthetic */ String zzQN() {
        return super.zzQN();
    }

    public /* bridge */ /* synthetic */ Set zzQO() {
        return super.zzQO();
    }

    public /* bridge */ /* synthetic */ boolean zzQd() {
        return super.zzQd();
    }

    public /* bridge */ /* synthetic */ zzak.zza zzZ(Map map) {
        return super.zzZ(map);
    }

    public void zzab(Map<String, zzak.zza> map) {
        Tracker zzht = this.zzbJd.zzht("_GTM_DEFAULT_TRACKER_");
        zzht.enableAdvertisingIdCollection(zzi(map, "collect_adid"));
        if (zzi(map, zzbIQ)) {
            zzb(zzht, map);
        } else if (zzi(map, zzbIP)) {
            zzht.send(zzk(map.get(zzbIT)));
        } else if (zzi(map, zzbIU)) {
            zza(zzht, map);
        } else {
            zzbo.zzbh("Ignoring unknown tag.");
        }
    }
}
