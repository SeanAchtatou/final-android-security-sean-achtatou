package com.google.android.gms.common.util;

import com.duapps.ad.AdError;
import java.util.regex.Pattern;

public final class zzm {
    private static Pattern zzaIh = null;

    public static int zzdp(int i) {
        return i / AdError.NETWORK_ERROR_CODE;
    }
}
