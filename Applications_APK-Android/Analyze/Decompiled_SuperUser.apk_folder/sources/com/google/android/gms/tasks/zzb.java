package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

class zzb<TResult, TContinuationResult> implements OnFailureListener, OnSuccessListener<TContinuationResult>, zzf<TResult> {
    private final Executor zzbFP;
    /* access modifiers changed from: private */
    public final Continuation<TResult, Task<TContinuationResult>> zzbNs;
    /* access modifiers changed from: private */
    public final zzh<TContinuationResult> zzbNt;

    public zzb(Executor executor, Continuation<TResult, Task<TContinuationResult>> continuation, zzh<TContinuationResult> zzh) {
        this.zzbFP = executor;
        this.zzbNs = continuation;
        this.zzbNt = zzh;
    }

    public void cancel() {
        throw new UnsupportedOperationException();
    }

    public void onComplete(final Task<TResult> task) {
        this.zzbFP.execute(new Runnable() {
            public void run() {
                try {
                    Task task = (Task) zzb.this.zzbNs.then(task);
                    if (task == null) {
                        zzb.this.onFailure(new NullPointerException("Continuation returned null"));
                        return;
                    }
                    task.addOnSuccessListener(TaskExecutors.zzbNG, zzb.this);
                    task.addOnFailureListener(TaskExecutors.zzbNG, zzb.this);
                } catch (RuntimeExecutionException e2) {
                    if (e2.getCause() instanceof Exception) {
                        zzb.this.zzbNt.setException((Exception) e2.getCause());
                    } else {
                        zzb.this.zzbNt.setException(e2);
                    }
                } catch (Exception e3) {
                    zzb.this.zzbNt.setException(e3);
                }
            }
        });
    }

    public void onFailure(Exception exc) {
        this.zzbNt.setException(exc);
    }

    public void onSuccess(TContinuationResult tcontinuationresult) {
        this.zzbNt.setResult(tcontinuationresult);
    }
}
