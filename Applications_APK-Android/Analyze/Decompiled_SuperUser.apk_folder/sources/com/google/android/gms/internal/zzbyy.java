package com.google.android.gms.internal;

final class zzbyy {
    static zzbyx zzcyc;
    static long zzcye;

    private zzbyy() {
    }

    static zzbyx zzafZ() {
        synchronized (zzbyy.class) {
            if (zzcyc == null) {
                return new zzbyx();
            }
            zzbyx zzbyx = zzcyc;
            zzcyc = zzbyx.zzcyc;
            zzbyx.zzcyc = null;
            zzcye -= 8192;
            return zzbyx;
        }
    }

    static void zzb(zzbyx zzbyx) {
        if (zzbyx.zzcyc != null || zzbyx.zzcyd != null) {
            throw new IllegalArgumentException();
        } else if (!zzbyx.zzcya) {
            synchronized (zzbyy.class) {
                if (zzcye + 8192 <= 65536) {
                    zzcye += 8192;
                    zzbyx.zzcyc = zzcyc;
                    zzbyx.limit = 0;
                    zzbyx.pos = 0;
                    zzcyc = zzbyx;
                }
            }
        }
    }
}
