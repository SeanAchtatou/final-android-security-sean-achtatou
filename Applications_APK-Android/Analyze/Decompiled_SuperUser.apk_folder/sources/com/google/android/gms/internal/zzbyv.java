package com.google.android.gms.internal;

public abstract class zzbyv implements zzbza {
    private final zzbza zzcxY;

    public void close() {
        this.zzcxY.close();
    }

    public void flush() {
        this.zzcxY.flush();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.zzcxY.toString() + ")";
    }

    public void write(zzbyr zzbyr, long j) {
        this.zzcxY.write(zzbyr, j);
    }
}
