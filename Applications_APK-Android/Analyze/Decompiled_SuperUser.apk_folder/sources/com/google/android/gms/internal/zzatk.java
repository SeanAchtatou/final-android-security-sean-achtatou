package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.zzac;

abstract class zzatk {
    private static volatile Handler zzafd;
    /* access modifiers changed from: private */
    public volatile long zzafe;
    /* access modifiers changed from: private */
    public final zzaue zzbqb;
    /* access modifiers changed from: private */
    public boolean zzbru = true;
    private final Runnable zzw = new Runnable() {
        public void run() {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                zzatk.this.zzbqb.zzKk().zzm(this);
                return;
            }
            boolean zzcy = zzatk.this.zzcy();
            long unused = zzatk.this.zzafe = 0;
            if (zzcy && zzatk.this.zzbru) {
                zzatk.this.run();
            }
        }
    };

    zzatk(zzaue zzaue) {
        zzac.zzw(zzaue);
        this.zzbqb = zzaue;
    }

    private Handler getHandler() {
        Handler handler;
        if (zzafd != null) {
            return zzafd;
        }
        synchronized (zzatk.class) {
            if (zzafd == null) {
                zzafd = new Handler(this.zzbqb.getContext().getMainLooper());
            }
            handler = zzafd;
        }
        return handler;
    }

    public void cancel() {
        this.zzafe = 0;
        getHandler().removeCallbacks(this.zzw);
    }

    public abstract void run();

    public boolean zzcy() {
        return this.zzafe != 0;
    }

    public void zzy(long j) {
        cancel();
        if (j >= 0) {
            this.zzafe = this.zzbqb.zznR().currentTimeMillis();
            if (!getHandler().postDelayed(this.zzw, j)) {
                this.zzbqb.zzKl().zzLZ().zzj("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }
}
