package com.google.firebase;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.c;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.util.zzt;
import com.google.android.gms.common.util.zzu;
import com.google.android.gms.internal.zzaac;
import com.google.android.gms.internal.zzbth;
import com.google.android.gms.internal.zzbti;
import com.google.android.gms.internal.zzbtj;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.GetTokenResult;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class FirebaseApp {
    public static final String DEFAULT_APP_NAME = "[DEFAULT]";
    private static final List<String> zzbWD = Arrays.asList("com.google.firebase.auth.FirebaseAuth", "com.google.firebase.iid.FirebaseInstanceId");
    private static final List<String> zzbWE = Collections.singletonList("com.google.firebase.crash.FirebaseCrash");
    private static final List<String> zzbWF = Arrays.asList("com.google.android.gms.measurement.AppMeasurement");
    private static final List<String> zzbWG = Arrays.asList(new String[0]);
    private static final Set<String> zzbWH = Collections.emptySet();
    static final Map<String, FirebaseApp> zzbhH = new ArrayMap();
    /* access modifiers changed from: private */
    public static final Object zztX = new Object();
    private final String mName;
    private final FirebaseOptions zzbWI;
    private final AtomicBoolean zzbWJ = new AtomicBoolean(false);
    private final AtomicBoolean zzbWK = new AtomicBoolean();
    private final List<zza> zzbWL = new CopyOnWriteArrayList();
    private final List<zzb> zzbWM = new CopyOnWriteArrayList();
    private final List<Object> zzbWN = new CopyOnWriteArrayList();
    private zzbti zzbWO;
    private final Context zzwi;

    public interface zza {
        void zzb(zzbtj zzbtj);
    }

    public interface zzb {
        void zzas(boolean z);
    }

    @TargetApi(24)
    private static class zzc extends BroadcastReceiver {
        private static AtomicReference<zzc> zzbWP = new AtomicReference<>();
        private final Context zzwi;

        public zzc(Context context) {
            this.zzwi = context;
        }

        /* access modifiers changed from: private */
        public static void zzcm(Context context) {
            if (zzbWP.get() == null) {
                zzc zzc = new zzc(context);
                if (zzbWP.compareAndSet(null, zzc)) {
                    context.registerReceiver(zzc, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        public void onReceive(Context context, Intent intent) {
            synchronized (FirebaseApp.zztX) {
                for (FirebaseApp zza : FirebaseApp.zzbhH.values()) {
                    zza.zzVa();
                }
            }
            unregister();
        }

        public void unregister() {
            this.zzwi.unregisterReceiver(this);
        }
    }

    protected FirebaseApp(Context context, String str, FirebaseOptions firebaseOptions) {
        this.zzwi = (Context) zzac.zzw(context);
        this.mName = zzac.zzdr(str);
        this.zzbWI = (FirebaseOptions) zzac.zzw(firebaseOptions);
    }

    public static List<FirebaseApp> getApps(Context context) {
        ArrayList arrayList;
        zzbth zzcx = zzbth.zzcx(context);
        synchronized (zztX) {
            arrayList = new ArrayList(zzbhH.values());
            Set<String> zzacb = zzbth.zzaca().zzacb();
            zzacb.removeAll(zzbhH.keySet());
            for (String next : zzacb) {
                zzcx.zzjC(next);
                arrayList.add(initializeApp(context, null, next));
            }
        }
        return arrayList;
    }

    public static FirebaseApp getInstance() {
        FirebaseApp firebaseApp;
        synchronized (zztX) {
            firebaseApp = zzbhH.get(DEFAULT_APP_NAME);
            if (firebaseApp == null) {
                String valueOf = String.valueOf(zzu.zzzr());
                throw new IllegalStateException(new StringBuilder(String.valueOf(valueOf).length() + 116).append("Default FirebaseApp is not initialized in this process ").append(valueOf).append(". Make sure to call FirebaseApp.initializeApp(Context) first.").toString());
            }
        }
        return firebaseApp;
    }

    public static FirebaseApp getInstance(String str) {
        FirebaseApp firebaseApp;
        String concat;
        synchronized (zztX) {
            firebaseApp = zzbhH.get(zzis(str));
            if (firebaseApp == null) {
                List<String> zzUZ = zzUZ();
                if (zzUZ.isEmpty()) {
                    concat = "";
                } else {
                    String valueOf = String.valueOf(TextUtils.join(", ", zzUZ));
                    concat = valueOf.length() != 0 ? "Available app names: ".concat(valueOf) : new String("Available app names: ");
                }
                throw new IllegalStateException(String.format("FirebaseApp with name %s doesn't exist. %s", str, concat));
            }
        }
        return firebaseApp;
    }

    public static FirebaseApp initializeApp(Context context) {
        FirebaseApp initializeApp;
        synchronized (zztX) {
            if (zzbhH.containsKey(DEFAULT_APP_NAME)) {
                initializeApp = getInstance();
            } else {
                FirebaseOptions fromResource = FirebaseOptions.fromResource(context);
                initializeApp = fromResource == null ? null : initializeApp(context, fromResource);
            }
        }
        return initializeApp;
    }

    public static FirebaseApp initializeApp(Context context, FirebaseOptions firebaseOptions) {
        return initializeApp(context, firebaseOptions, DEFAULT_APP_NAME);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    public static FirebaseApp initializeApp(Context context, FirebaseOptions firebaseOptions, String str) {
        FirebaseApp firebaseApp;
        zzbth zzcx = zzbth.zzcx(context);
        zzcl(context);
        String zzis = zzis(str);
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (zztX) {
            zzac.zza(!zzbhH.containsKey(zzis), (Object) new StringBuilder(String.valueOf(zzis).length() + 33).append("FirebaseApp name ").append(zzis).append(" already exists!").toString());
            zzac.zzb(context, "Application context cannot be null.");
            firebaseApp = new FirebaseApp(context, zzis, firebaseOptions);
            zzbhH.put(zzis, firebaseApp);
        }
        zzcx.zzg(firebaseApp);
        firebaseApp.zza(FirebaseApp.class, firebaseApp, zzbWD);
        if (firebaseApp.zzUX()) {
            firebaseApp.zza(FirebaseApp.class, firebaseApp, zzbWE);
            firebaseApp.zza(Context.class, firebaseApp.getApplicationContext(), zzbWF);
        }
        return firebaseApp;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    private void zzUW() {
        zzac.zza(!this.zzbWK.get(), (Object) "FirebaseApp was deleted");
    }

    private static List<String> zzUZ() {
        com.google.android.gms.common.util.zza zza2 = new com.google.android.gms.common.util.zza();
        synchronized (zztX) {
            for (FirebaseApp name : zzbhH.values()) {
                zza2.add(name.getName());
            }
            zzbth zzaca = zzbth.zzaca();
            if (zzaca != null) {
                zza2.addAll(zzaca.zzacb());
            }
        }
        ArrayList arrayList = new ArrayList(zza2);
        Collections.sort(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void zzVa() {
        zza(FirebaseApp.class, this, zzbWD);
        if (zzUX()) {
            zza(FirebaseApp.class, this, zzbWE);
            zza(Context.class, this.zzwi, zzbWF);
        }
    }

    private <T> void zza(Class<T> cls, T t, Iterable<String> iterable) {
        boolean b2 = c.b(this.zzwi);
        if (b2) {
            zzc.zzcm(this.zzwi);
        }
        for (String next : iterable) {
            if (b2) {
                try {
                    if (!zzbWG.contains(next)) {
                    }
                } catch (ClassNotFoundException e2) {
                    if (zzbWH.contains(next)) {
                        throw new IllegalStateException(String.valueOf(next).concat(" is missing, but is required. Check if it has been removed by Proguard."));
                    }
                    Log.d("FirebaseApp", String.valueOf(next).concat(" is not linked. Skipping initialization."));
                } catch (NoSuchMethodException e3) {
                    throw new IllegalStateException(String.valueOf(next).concat("#getInstance has been removed by Proguard. Add keep rule to prevent it."));
                } catch (InvocationTargetException e4) {
                    Log.wtf("FirebaseApp", "Firebase API initialization failure.", e4);
                } catch (IllegalAccessException e5) {
                    String valueOf = String.valueOf(next);
                    Log.wtf("FirebaseApp", valueOf.length() != 0 ? "Failed to initialize ".concat(valueOf) : new String("Failed to initialize "), e5);
                }
            }
            Method method = Class.forName(next).getMethod("getInstance", cls);
            int modifiers = method.getModifiers();
            if (Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers)) {
                method.invoke(null, t);
            }
        }
    }

    private void zzaV(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (zzb zzas : this.zzbWM) {
            zzas.zzas(z);
        }
    }

    public static void zzas(boolean z) {
        synchronized (zztX) {
            Iterator it = new ArrayList(zzbhH.values()).iterator();
            while (it.hasNext()) {
                FirebaseApp firebaseApp = (FirebaseApp) it.next();
                if (firebaseApp.zzbWJ.get()) {
                    firebaseApp.zzaV(z);
                }
            }
        }
    }

    @TargetApi(14)
    private static void zzcl(Context context) {
        zzt.zzzg();
        if (context.getApplicationContext() instanceof Application) {
            zzaac.zza((Application) context.getApplicationContext());
            zzaac.zzvB().zza(new zzaac.zza() {
                public void zzas(boolean z) {
                    FirebaseApp.zzas(z);
                }
            });
        }
    }

    private static String zzis(String str) {
        return str.trim();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FirebaseApp)) {
            return false;
        }
        return this.mName.equals(((FirebaseApp) obj).getName());
    }

    public Context getApplicationContext() {
        zzUW();
        return this.zzwi;
    }

    public String getName() {
        zzUW();
        return this.mName;
    }

    public FirebaseOptions getOptions() {
        zzUW();
        return this.zzbWI;
    }

    public Task<GetTokenResult> getToken(boolean z) {
        zzUW();
        return this.zzbWO == null ? Tasks.forException(new FirebaseApiNotAvailableException("firebase-auth is not linked, please fall back to unauthenticated mode.")) : this.zzbWO.zzaW(z);
    }

    public int hashCode() {
        return this.mName.hashCode();
    }

    public void setAutomaticResourceManagementEnabled(boolean z) {
        zzUW();
        if (this.zzbWJ.compareAndSet(!z, z)) {
            boolean zzvC = zzaac.zzvB().zzvC();
            if (z && zzvC) {
                zzaV(true);
            } else if (!z && zzvC) {
                zzaV(false);
            }
        }
    }

    public String toString() {
        return zzaa.zzv(this).zzg("name", this.mName).zzg("options", this.zzbWI).toString();
    }

    public boolean zzUX() {
        return DEFAULT_APP_NAME.equals(getName());
    }

    public String zzUY() {
        String valueOf = String.valueOf(com.google.android.gms.common.util.zzc.zzs(getName().getBytes()));
        String valueOf2 = String.valueOf(com.google.android.gms.common.util.zzc.zzs(getOptions().getApplicationId().getBytes()));
        return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append("+").append(valueOf2).toString();
    }

    public void zza(zzbti zzbti) {
        this.zzbWO = (zzbti) zzac.zzw(zzbti);
    }

    public void zza(zzbtj zzbtj) {
        Log.d("FirebaseApp", "Notifying auth state listeners.");
        int i = 0;
        for (zza zzb2 : this.zzbWL) {
            zzb2.zzb(zzbtj);
            i++;
        }
        Log.d("FirebaseApp", String.format("Notified %d auth state listeners.", Integer.valueOf(i)));
    }

    public void zza(zza zza2) {
        zzUW();
        zzac.zzw(zza2);
        this.zzbWL.add(zza2);
    }

    public void zza(zzb zzb2) {
        zzUW();
        if (this.zzbWJ.get() && zzaac.zzvB().zzvC()) {
            zzb2.zzas(true);
        }
        this.zzbWM.add(zzb2);
    }
}
