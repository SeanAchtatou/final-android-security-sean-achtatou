package com.google.firebase.auth;

public class GetTokenResult {
    private String zzaiJ;

    public GetTokenResult(String str) {
        this.zzaiJ = str;
    }

    public String getToken() {
        return this.zzaiJ;
    }
}
