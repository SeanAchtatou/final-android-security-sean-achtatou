package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import com.google.android.gms.common.util.zzt;
import com.google.firebase.iid.zzb;

public final class FirebaseInstanceIdInternalReceiver extends WakefulBroadcastReceiver {
    private static boolean zzbgs = false;
    private static zzb.zzc zzclp;
    private static zzb.zzc zzclq;

    static synchronized zzb.zzc zzL(Context context, String str) {
        zzb.zzc zzc;
        synchronized (FirebaseInstanceIdInternalReceiver.class) {
            if ("com.google.firebase.MESSAGING_EVENT".equals(str)) {
                if (zzclq == null) {
                    zzclq = new zzb.zzc(context, str);
                }
                zzc = zzclq;
            } else {
                if (zzclp == null) {
                    zzclp = new zzb.zzc(context, str);
                }
                zzc = zzclp;
            }
        }
        return zzc;
    }

    static boolean zzcs(Context context) {
        return zzt.zzzq() && context.getApplicationInfo().targetSdkVersion > 25;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Parcelable parcelableExtra = intent.getParcelableExtra("wrapped_intent");
            if (!(parcelableExtra instanceof Intent)) {
                Log.e("FirebaseInstanceId", "Missing or invalid wrapped intent");
                return;
            }
            Intent intent2 = (Intent) parcelableExtra;
            if (zzcs(context)) {
                zzL(context, intent.getAction()).zza(intent2, goAsync());
            } else {
                zzg.zzabW().zzb(context, intent.getAction(), intent2);
            }
        }
    }
}
