package com.google.firebase.iid;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class zzb extends Service {
    final ExecutorService zzbtK = Executors.newSingleThreadExecutor();
    private Binder zzckT;
    private int zzckU;
    private int zzckV = 0;
    private final Object zzrJ = new Object();

    static class zza {
        final Intent intent;
        private final BroadcastReceiver.PendingResult zzckY;
        private boolean zzckZ = false;
        private final ScheduledFuture<?> zzcla;

        zza(final Intent intent2, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
            this.intent = intent2;
            this.zzckY = pendingResult;
            this.zzcla = scheduledExecutorService.schedule(new Runnable() {
                public void run() {
                    String valueOf = String.valueOf(intent2.getAction());
                    Log.w("EnhancedIntentService", new StringBuilder(String.valueOf(valueOf).length() + 61).append("Service took too long to process intent: ").append(valueOf).append(" App may get closed.").toString());
                    zza.this.finish();
                }
            }, 9500, TimeUnit.MILLISECONDS);
        }

        /* access modifiers changed from: package-private */
        public synchronized void finish() {
            if (!this.zzckZ) {
                this.zzckY.finish();
                this.zzcla.cancel(false);
                this.zzckZ = true;
            }
        }
    }

    /* renamed from: com.google.firebase.iid.zzb$zzb  reason: collision with other inner class name */
    public static class C0083zzb extends Binder {
        /* access modifiers changed from: private */
        public final zzb zzclc;

        C0083zzb(zzb zzb) {
            this.zzclc = zzb;
        }

        public void zza(final zza zza) {
            if (Binder.getCallingUid() != Process.myUid()) {
                throw new SecurityException("Binding only allowed within app");
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (this.zzclc.zzE(zza.intent)) {
                zza.finish();
                return;
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.zzclc.zzbtK.execute(new Runnable() {
                public void run() {
                    if (Log.isLoggable("EnhancedIntentService", 3)) {
                        Log.d("EnhancedIntentService", "bg processing of the intent starting now");
                    }
                    C0083zzb.this.zzclc.handleIntent(zza.intent);
                    zza.finish();
                }
            });
        }
    }

    public static class zzc implements ServiceConnection {
        private final Intent zzclf;
        private final ScheduledExecutorService zzclg;
        private final Queue<zza> zzclh;
        private C0083zzb zzcli;
        private boolean zzclj;
        private final Context zzqn;

        public zzc(Context context, String str) {
            this(context, str, new ScheduledThreadPoolExecutor(0));
        }

        zzc(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
            this.zzclh = new LinkedList();
            this.zzclj = false;
            this.zzqn = context.getApplicationContext();
            this.zzclf = new Intent(str).setPackage(this.zzqn.getPackageName());
            this.zzclg = scheduledExecutorService;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:26:0x005e, code lost:
            if (android.util.Log.isLoggable("EnhancedIntentService", 3) == false) goto L_0x007f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0064, code lost:
            if (r4.zzclj != false) goto L_0x0098;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0067, code lost:
            android.util.Log.d("EnhancedIntentService", new java.lang.StringBuilder(39).append("binder is dead. start connection? ").append(r0).toString());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0081, code lost:
            if (r4.zzclj != false) goto L_0x0096;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0083, code lost:
            r4.zzclj = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0094, code lost:
            if (com.google.android.gms.common.stats.zza.zzyJ().zza(r4.zzqn, r4.zzclf, r4, 65) == false) goto L_0x009a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0098, code lost:
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            android.util.Log.e("EnhancedIntentService", "binding to the service failed");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b5, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b6, code lost:
            android.util.Log.e("EnhancedIntentService", "Exception while binding the service", r0);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private synchronized void zzwH() {
            /*
                r4 = this;
                r1 = 1
                monitor-enter(r4)
                java.lang.String r0 = "EnhancedIntentService"
                r2 = 3
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0054 }
                if (r0 == 0) goto L_0x0012
                java.lang.String r0 = "EnhancedIntentService"
                java.lang.String r2 = "flush queue called"
                android.util.Log.d(r0, r2)     // Catch:{ all -> 0x0054 }
            L_0x0012:
                java.util.Queue<com.google.firebase.iid.zzb$zza> r0 = r4.zzclh     // Catch:{ all -> 0x0054 }
                boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0054 }
                if (r0 != 0) goto L_0x0096
                java.lang.String r0 = "EnhancedIntentService"
                r2 = 3
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0054 }
                if (r0 == 0) goto L_0x002a
                java.lang.String r0 = "EnhancedIntentService"
                java.lang.String r2 = "found intent to be delivered"
                android.util.Log.d(r0, r2)     // Catch:{ all -> 0x0054 }
            L_0x002a:
                com.google.firebase.iid.zzb$zzb r0 = r4.zzcli     // Catch:{ all -> 0x0054 }
                if (r0 == 0) goto L_0x0057
                com.google.firebase.iid.zzb$zzb r0 = r4.zzcli     // Catch:{ all -> 0x0054 }
                boolean r0 = r0.isBinderAlive()     // Catch:{ all -> 0x0054 }
                if (r0 == 0) goto L_0x0057
                java.lang.String r0 = "EnhancedIntentService"
                r2 = 3
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0054 }
                if (r0 == 0) goto L_0x0046
                java.lang.String r0 = "EnhancedIntentService"
                java.lang.String r2 = "binder is alive, sending the intent."
                android.util.Log.d(r0, r2)     // Catch:{ all -> 0x0054 }
            L_0x0046:
                java.util.Queue<com.google.firebase.iid.zzb$zza> r0 = r4.zzclh     // Catch:{ all -> 0x0054 }
                java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0054 }
                com.google.firebase.iid.zzb$zza r0 = (com.google.firebase.iid.zzb.zza) r0     // Catch:{ all -> 0x0054 }
                com.google.firebase.iid.zzb$zzb r2 = r4.zzcli     // Catch:{ all -> 0x0054 }
                r2.zza(r0)     // Catch:{ all -> 0x0054 }
                goto L_0x0012
            L_0x0054:
                r0 = move-exception
                monitor-exit(r4)
                throw r0
            L_0x0057:
                java.lang.String r0 = "EnhancedIntentService"
                r2 = 3
                boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0054 }
                if (r0 == 0) goto L_0x007f
                java.lang.String r2 = "EnhancedIntentService"
                boolean r0 = r4.zzclj     // Catch:{ all -> 0x0054 }
                if (r0 != 0) goto L_0x0098
                r0 = r1
            L_0x0067:
                r1 = 39
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0054 }
                r3.<init>(r1)     // Catch:{ all -> 0x0054 }
                java.lang.String r1 = "binder is dead. start connection? "
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0054 }
                java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0054 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0054 }
                android.util.Log.d(r2, r0)     // Catch:{ all -> 0x0054 }
            L_0x007f:
                boolean r0 = r4.zzclj     // Catch:{ all -> 0x0054 }
                if (r0 != 0) goto L_0x0096
                r0 = 1
                r4.zzclj = r0     // Catch:{ all -> 0x0054 }
                com.google.android.gms.common.stats.zza r0 = com.google.android.gms.common.stats.zza.zzyJ()     // Catch:{ SecurityException -> 0x00b5 }
                android.content.Context r1 = r4.zzqn     // Catch:{ SecurityException -> 0x00b5 }
                android.content.Intent r2 = r4.zzclf     // Catch:{ SecurityException -> 0x00b5 }
                r3 = 65
                boolean r0 = r0.zza(r1, r2, r4, r3)     // Catch:{ SecurityException -> 0x00b5 }
                if (r0 == 0) goto L_0x009a
            L_0x0096:
                monitor-exit(r4)
                return
            L_0x0098:
                r0 = 0
                goto L_0x0067
            L_0x009a:
                java.lang.String r0 = "EnhancedIntentService"
                java.lang.String r1 = "binding to the service failed"
                android.util.Log.e(r0, r1)     // Catch:{ SecurityException -> 0x00b5 }
            L_0x00a1:
                java.util.Queue<com.google.firebase.iid.zzb$zza> r0 = r4.zzclh     // Catch:{ all -> 0x0054 }
                boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0054 }
                if (r0 != 0) goto L_0x0096
                java.util.Queue<com.google.firebase.iid.zzb$zza> r0 = r4.zzclh     // Catch:{ all -> 0x0054 }
                java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0054 }
                com.google.firebase.iid.zzb$zza r0 = (com.google.firebase.iid.zzb.zza) r0     // Catch:{ all -> 0x0054 }
                r0.finish()     // Catch:{ all -> 0x0054 }
                goto L_0x00a1
            L_0x00b5:
                r0 = move-exception
                java.lang.String r1 = "EnhancedIntentService"
                java.lang.String r2 = "Exception while binding the service"
                android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x0054 }
                goto L_0x00a1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.zzb.zzc.zzwH():void");
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            synchronized (this) {
                this.zzclj = false;
                this.zzcli = (C0083zzb) iBinder;
                if (Log.isLoggable("EnhancedIntentService", 3)) {
                    String valueOf = String.valueOf(componentName);
                    Log.d("EnhancedIntentService", new StringBuilder(String.valueOf(valueOf).length() + 20).append("onServiceConnected: ").append(valueOf).toString());
                }
                zzwH();
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                String valueOf = String.valueOf(componentName);
                Log.d("EnhancedIntentService", new StringBuilder(String.valueOf(valueOf).length() + 23).append("onServiceDisconnected: ").append(valueOf).toString());
            }
            zzwH();
        }

        public synchronized void zza(Intent intent, BroadcastReceiver.PendingResult pendingResult) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "new intent queued in the bind-strategy delivery");
            }
            this.zzclh.add(new zza(intent, pendingResult, this.zzclg));
            zzwH();
        }
    }

    /* access modifiers changed from: private */
    public void zzC(Intent intent) {
        if (intent != null) {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
        synchronized (this.zzrJ) {
            this.zzckV--;
            if (this.zzckV == 0) {
                zzqE(this.zzckU);
            }
        }
    }

    public abstract void handleIntent(Intent intent);

    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.zzckT == null) {
            this.zzckT = new C0083zzb(this);
        }
        return this.zzckT;
    }

    public final int onStartCommand(final Intent intent, int i, int i2) {
        synchronized (this.zzrJ) {
            this.zzckU = i2;
            this.zzckV++;
        }
        final Intent zzD = zzD(intent);
        if (zzD == null) {
            zzC(intent);
            return 2;
        } else if (zzE(zzD)) {
            zzC(intent);
            return 2;
        } else {
            this.zzbtK.execute(new Runnable() {
                public void run() {
                    zzb.this.handleIntent(zzD);
                    zzb.this.zzC(intent);
                }
            });
            return 3;
        }
    }

    /* access modifiers changed from: protected */
    public Intent zzD(Intent intent) {
        return intent;
    }

    public boolean zzE(Intent intent) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean zzqE(int i) {
        return stopSelfResult(i);
    }
}
