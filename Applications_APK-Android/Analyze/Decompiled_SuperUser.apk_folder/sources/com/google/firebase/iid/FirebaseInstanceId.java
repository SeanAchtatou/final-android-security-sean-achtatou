package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.v4.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.zzh;
import com.lody.virtual.os.VUserInfo;
import java.io.IOException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class FirebaseInstanceId {
    private static Map<String, FirebaseInstanceId> zzbhH = new ArrayMap();
    private static zze zzcll;
    private final FirebaseApp zzclm;
    private final zzd zzcln;
    private final String zzclo = zzabO();

    private FirebaseInstanceId(FirebaseApp firebaseApp, zzd zzd) {
        this.zzclm = firebaseApp;
        this.zzcln = zzd;
        if (this.zzclo == null) {
            throw new IllegalStateException("IID failing to initialize, FirebaseApp is missing project ID");
        }
        FirebaseInstanceIdService.zza(this.zzclm.getApplicationContext(), this);
    }

    public static FirebaseInstanceId getInstance() {
        return getInstance(FirebaseApp.getInstance());
    }

    @Keep
    public static synchronized FirebaseInstanceId getInstance(FirebaseApp firebaseApp) {
        FirebaseInstanceId firebaseInstanceId;
        synchronized (FirebaseInstanceId.class) {
            firebaseInstanceId = zzbhH.get(firebaseApp.getOptions().getApplicationId());
            if (firebaseInstanceId == null) {
                zzd zzb = zzd.zzb(firebaseApp.getApplicationContext(), null);
                if (zzcll == null) {
                    zzcll = new zze(zzb.zzabS());
                }
                firebaseInstanceId = new FirebaseInstanceId(firebaseApp, zzb);
                zzbhH.put(firebaseApp.getOptions().getApplicationId(), firebaseInstanceId);
            }
        }
        return firebaseInstanceId;
    }

    static int zzS(Context context, String str) {
        try {
            return context.getPackageManager().getPackageInfo(str, 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            String valueOf = String.valueOf(e2);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 23).append("Failed to find package ").append(valueOf).toString());
            return 0;
        }
    }

    private void zzT(Bundle bundle) {
        bundle.putString("gmp_app_id", this.zzclm.getOptions().getApplicationId());
    }

    static String zza(KeyPair keyPair) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(keyPair.getPublic().getEncoded());
            digest[0] = (byte) (((digest[0] & 15) + 112) & VUserInfo.FLAG_MASK_USER_TYPE);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException e2) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required alghorithms");
            return null;
        }
    }

    static void zza(Context context, zzh zzh) {
        zzh.zzHo();
        Intent intent = new Intent();
        intent.putExtra("CMD", "RST");
        zzg.zzabW().zzg(context, intent);
    }

    static String zzbx(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            String valueOf = String.valueOf(e2);
            Log.w("FirebaseInstanceId", new StringBuilder(String.valueOf(valueOf).length() + 38).append("Never happens: can't find own package ").append(valueOf).toString());
            return null;
        }
    }

    static void zzby(Context context) {
        Intent intent = new Intent();
        intent.putExtra("CMD", "SYNC");
        zzg.zzabW().zzg(context, intent);
    }

    static int zzcr(Context context) {
        return zzS(context, context.getPackageName());
    }

    static String zzv(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    public void deleteInstanceId() {
        this.zzcln.zzb("*", "*", null);
        this.zzcln.zzHi();
    }

    public void deleteToken(String str, String str2) {
        Bundle bundle = new Bundle();
        zzT(bundle);
        this.zzcln.zzb(str, str2, bundle);
    }

    public long getCreationTime() {
        return this.zzcln.getCreationTime();
    }

    public String getId() {
        return zza(this.zzcln.zzHh());
    }

    public String getToken() {
        zzh.zza zzabP = zzabP();
        if (zzabP == null || zzabP.zzjB(zzd.zzbhN)) {
            FirebaseInstanceIdService.zzct(this.zzclm.getApplicationContext());
        }
        if (zzabP != null) {
            return zzabP.zzbxW;
        }
        return null;
    }

    public String getToken(String str, String str2) {
        Bundle bundle = new Bundle();
        zzT(bundle);
        return this.zzcln.getToken(str, str2, bundle);
    }

    /* access modifiers changed from: package-private */
    public String zzabO() {
        String gcmSenderId = this.zzclm.getOptions().getGcmSenderId();
        if (gcmSenderId != null) {
            return gcmSenderId;
        }
        String applicationId = this.zzclm.getOptions().getApplicationId();
        if (!applicationId.startsWith("1:")) {
            return applicationId;
        }
        String[] split = applicationId.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public zzh.zza zzabP() {
        return this.zzcln.zzabS().zzu("", this.zzclo, "*");
    }

    /* access modifiers changed from: package-private */
    public String zzabQ() {
        return getToken(this.zzclo, "*");
    }

    /* access modifiers changed from: package-private */
    public zze zzabR() {
        return zzcll;
    }

    public String zzc(String str, String str2, Bundle bundle) {
        zzT(bundle);
        return this.zzcln.zzc(str, str2, bundle);
    }

    public void zzjt(String str) {
        zzcll.zzjt(str);
        FirebaseInstanceIdService.zzct(this.zzclm.getApplicationContext());
    }

    /* access modifiers changed from: package-private */
    public void zzju(String str) {
        zzh.zza zzabP = zzabP();
        if (zzabP == null || zzabP.zzjB(zzd.zzbhN)) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString("gcm.topic", valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        String str2 = zzabP.zzbxW;
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        zzc(str2, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }

    /* access modifiers changed from: package-private */
    public void zzjv(String str) {
        zzh.zza zzabP = zzabP();
        if (zzabP == null || zzabP.zzjB(zzd.zzbhN)) {
            throw new IOException("token not available");
        }
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf("/topics/");
        String valueOf2 = String.valueOf(str);
        bundle.putString("gcm.topic", valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        zzd zzd = this.zzcln;
        String str2 = zzabP.zzbxW;
        String valueOf3 = String.valueOf("/topics/");
        String valueOf4 = String.valueOf(str);
        zzd.zzb(str2, valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3), bundle);
    }
}
