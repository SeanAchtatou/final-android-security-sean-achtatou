package com.google.firebase.iid;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.util.ArrayMap;
import com.google.firebase.iid.zzh;
import java.io.IOException;
import java.security.KeyPair;
import java.util.Map;

public class zzd {
    static Map<String, zzd> zzbhH = new ArrayMap();
    static String zzbhN;
    private static zzh zzclv;
    private static zzf zzclw;
    Context mContext;
    KeyPair zzbhK;
    String zzbhL = "";

    protected zzd(Context context, String str, Bundle bundle) {
        this.mContext = context.getApplicationContext();
        this.zzbhL = str;
    }

    public static synchronized zzd zzb(Context context, Bundle bundle) {
        zzd zzd;
        synchronized (zzd.class) {
            String string = bundle == null ? "" : bundle.getString("subtype");
            String str = string == null ? "" : string;
            Context applicationContext = context.getApplicationContext();
            if (zzclv == null) {
                zzclv = new zzh(applicationContext);
                zzclw = new zzf(applicationContext);
            }
            zzbhN = Integer.toString(FirebaseInstanceId.zzcr(applicationContext));
            zzd = zzbhH.get(str);
            if (zzd == null) {
                zzd = new zzd(applicationContext, str, bundle);
                zzbhH.put(str, zzd);
            }
        }
        return zzd;
    }

    public long getCreationTime() {
        return zzclv.zzjy(this.zzbhL);
    }

    public String getToken(String str, String str2, Bundle bundle) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        if (bundle == null) {
            bundle = new Bundle();
        }
        boolean z = true;
        if (bundle.getString("ttl") != null || "jwt".equals(bundle.getString("type"))) {
            z = false;
        } else {
            zzh.zza zzu = zzclv.zzu(this.zzbhL, str, str2);
            if (zzu != null && !zzu.zzjB(zzbhN)) {
                return zzu.zzbxW;
            }
        }
        String zzc = zzc(str, str2, bundle);
        if (zzc == null || !z) {
            return zzc;
        }
        zzclv.zza(this.zzbhL, str, str2, zzc, zzbhN);
        return zzc;
    }

    /* access modifiers changed from: package-private */
    public KeyPair zzHh() {
        if (this.zzbhK == null) {
            this.zzbhK = zzclv.zzeI(this.zzbhL);
        }
        if (this.zzbhK == null) {
            this.zzbhK = zzclv.zzjz(this.zzbhL);
        }
        return this.zzbhK;
    }

    public void zzHi() {
        zzclv.zzeJ(this.zzbhL);
        this.zzbhK = null;
    }

    public zzh zzabS() {
        return zzclv;
    }

    public zzf zzabT() {
        return zzclw;
    }

    public void zzb(String str, String str2, Bundle bundle) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        zzclv.zzi(this.zzbhL, str, str2);
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("delete", "1");
        zzc(str, str2, bundle);
    }

    public String zzc(String str, String str2, Bundle bundle) {
        if (str2 != null) {
            bundle.putString("scope", str2);
        }
        bundle.putString("sender", str);
        if (!"".equals(this.zzbhL)) {
            str = this.zzbhL;
        }
        bundle.putString("subtype", str);
        bundle.putString("X-subtype", str);
        return zzclw.zzq(zzclw.zza(bundle, zzHh()));
    }
}
