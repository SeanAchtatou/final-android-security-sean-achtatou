package com.google.firebase.iid;

import com.kingouser.com.util.rsa.RSAUtils;
import com.lody.virtual.helper.utils.FileUtils;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

public class zza {
    public static KeyPair zzHg() {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance(RSAUtils.KEY_ALGORITHM);
            instance.initialize((int) FileUtils.FileMode.MODE_ISUID);
            return instance.generateKeyPair();
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }
}
