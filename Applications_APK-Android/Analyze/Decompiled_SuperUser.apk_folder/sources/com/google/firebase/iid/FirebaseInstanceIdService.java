package com.google.firebase.iid;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.duapps.ad.AdError;
import com.google.firebase.iid.zzh;
import java.io.IOException;

public class FirebaseInstanceIdService extends zzb {
    static final Object zzclr = new Object();
    static boolean zzcls = false;
    private boolean zzclt = false;

    private static class zza extends BroadcastReceiver {
        static BroadcastReceiver receiver;
        final int zzclu;

        zza(int i) {
            this.zzclu = i;
        }

        static synchronized void zzl(Context context, int i) {
            synchronized (zza.class) {
                if (receiver == null) {
                    receiver = new zza(i);
                    context.getApplicationContext().registerReceiver(receiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                }
            }
        }

        public void onReceive(Context context, Intent intent) {
            synchronized (zza.class) {
                if (receiver == this) {
                    if (FirebaseInstanceIdService.zzcu(context)) {
                        if (Log.isLoggable("FirebaseInstanceId", 3)) {
                            Log.d("FirebaseInstanceId", "connectivity changed. starting background sync.");
                        }
                        context.getApplicationContext().unregisterReceiver(this);
                        receiver = null;
                        zzg.zzabW().zzg(context, FirebaseInstanceIdService.zzqF(this.zzclu));
                    }
                }
            }
        }
    }

    private String zzG(Intent intent) {
        String stringExtra = intent.getStringExtra("subtype");
        return stringExtra == null ? "" : stringExtra;
    }

    private void zzU(Bundle bundle) {
        String zzbA = zzf.zzbA(this);
        if (zzbA == null) {
            Log.w("FirebaseInstanceId", "Unable to respond to ping due to missing target package");
            return;
        }
        Intent intent = new Intent("com.google.android.gcm.intent.SEND");
        intent.setPackage(zzbA);
        intent.putExtras(bundle);
        zzf.zzf(this, intent);
        intent.putExtra("google.to", "google.com/iid");
        intent.putExtra("google.message_id", zzf.zzHn());
        sendOrderedBroadcast(intent, "com.google.android.gtalkservice.permission.GTALK_SERVICE");
    }

    private int zza(Intent intent, boolean z) {
        int intExtra = intent == null ? 10 : intent.getIntExtra("next_retry_delay_in_seconds", 0);
        if (intExtra < 10 && !z) {
            return 30;
        }
        if (intExtra < 10) {
            return 10;
        }
        if (intExtra > 28800) {
            return 28800;
        }
        return intExtra;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        if (r0.zzjB(com.google.firebase.iid.zzd.zzbhN) != false) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        if (r3.zzabR().zzabU() == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        zzct(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r3.zzabP();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 == null) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void zza(android.content.Context r2, com.google.firebase.iid.FirebaseInstanceId r3) {
        /*
            java.lang.Object r1 = com.google.firebase.iid.FirebaseInstanceIdService.zzclr
            monitor-enter(r1)
            boolean r0 = com.google.firebase.iid.FirebaseInstanceIdService.zzcls     // Catch:{ all -> 0x0026 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            com.google.firebase.iid.zzh$zza r0 = r3.zzabP()
            if (r0 == 0) goto L_0x0022
            java.lang.String r1 = com.google.firebase.iid.zzd.zzbhN
            boolean r0 = r0.zzjB(r1)
            if (r0 != 0) goto L_0x0022
            com.google.firebase.iid.zze r0 = r3.zzabR()
            java.lang.String r0 = r0.zzabU()
            if (r0 == 0) goto L_0x0008
        L_0x0022:
            zzct(r2)
            goto L_0x0008
        L_0x0026:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.FirebaseInstanceIdService.zza(android.content.Context, com.google.firebase.iid.FirebaseInstanceId):void");
    }

    private void zza(Intent intent, boolean z, boolean z2) {
        synchronized (zzclr) {
            zzcls = false;
        }
        if (zzf.zzbA(this) != null) {
            FirebaseInstanceId instance = FirebaseInstanceId.getInstance();
            zzh.zza zzabP = instance.zzabP();
            if (zzabP == null || zzabP.zzjB(zzd.zzbhN)) {
                try {
                    String zzabQ = instance.zzabQ();
                    if (zzabQ != null) {
                        if (this.zzclt) {
                            Log.d("FirebaseInstanceId", "get master token succeeded");
                        }
                        zza(this, instance);
                        if (z2 || zzabP == null || (zzabP != null && !zzabQ.equals(zzabP.zzbxW))) {
                            onTokenRefresh();
                            return;
                        }
                        return;
                    }
                    zzd(intent, "returned token is null");
                } catch (IOException e2) {
                    zzd(intent, e2.getMessage());
                } catch (SecurityException e3) {
                    Log.e("FirebaseInstanceId", "Unable to get master token", e3);
                }
            } else {
                zze zzabR = instance.zzabR();
                for (String zzabU = zzabR.zzabU(); zzabU != null; zzabU = zzabR.zzabU()) {
                    String[] split = zzabU.split("!");
                    if (split.length == 2) {
                        String str = split[0];
                        String str2 = split[1];
                        char c2 = 65535;
                        try {
                            switch (str.hashCode()) {
                                case 83:
                                    if (str.equals("S")) {
                                        c2 = 0;
                                        break;
                                    }
                                    break;
                                case 85:
                                    if (str.equals("U")) {
                                        c2 = 1;
                                        break;
                                    }
                                    break;
                            }
                            switch (c2) {
                                case 0:
                                    FirebaseInstanceId.getInstance().zzju(str2);
                                    if (this.zzclt) {
                                        Log.d("FirebaseInstanceId", "subscribe operation succeeded");
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 1:
                                    FirebaseInstanceId.getInstance().zzjv(str2);
                                    if (this.zzclt) {
                                        Log.d("FirebaseInstanceId", "unsubscribe operation succeeded");
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        } catch (IOException e4) {
                            zzd(intent, e4.getMessage());
                            return;
                        }
                    }
                    zzabR.zzjx(zzabU);
                }
                Log.d("FirebaseInstanceId", "topic sync succeeded");
            }
        }
    }

    static void zzct(Context context) {
        if (zzf.zzbA(context) != null) {
            synchronized (zzclr) {
                if (!zzcls) {
                    zzg.zzabW().zzg(context, zzqF(0));
                    zzcls = true;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean zzcu(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void zzd(Intent intent, String str) {
        boolean zzcu = zzcu(this);
        int zza2 = zza(intent, zzcu);
        Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(str).length() + 47).append("background sync failed: ").append(str).append(", retry in ").append(zza2).append("s").toString());
        synchronized (zzclr) {
            zzqG(zza2);
            zzcls = true;
        }
        if (!zzcu) {
            if (this.zzclt) {
                Log.d("FirebaseInstanceId", "device not connected. Connectivity change received registered");
            }
            zza.zzl(this, zza2);
        }
    }

    private zzd zzjw(String str) {
        if (str == null) {
            return zzd.zzb(this, null);
        }
        Bundle bundle = new Bundle();
        bundle.putString("subtype", str);
        return zzd.zzb(this, bundle);
    }

    /* access modifiers changed from: private */
    public static Intent zzqF(int i) {
        Intent intent = new Intent("ACTION_TOKEN_REFRESH_RETRY");
        intent.putExtra("next_retry_delay_in_seconds", i);
        return intent;
    }

    private void zzqG(int i) {
        ((AlarmManager) getSystemService("alarm")).set(3, SystemClock.elapsedRealtime() + ((long) (i * AdError.NETWORK_ERROR_CODE)), zzg.zza(this, 0, zzqF(i * 2), 134217728));
    }

    public void handleIntent(Intent intent) {
        boolean z;
        String action = intent.getAction();
        if (action == null) {
            action = "";
        }
        switch (action.hashCode()) {
            case -1737547627:
                if (action.equals("ACTION_TOKEN_REFRESH_RETRY")) {
                    z = false;
                    break;
                }
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                zza(intent, false, false);
                return;
            default:
                zzF(intent);
                return;
        }
    }

    public void onTokenRefresh() {
    }

    /* access modifiers changed from: protected */
    public Intent zzD(Intent intent) {
        return zzg.zzabW().zzabX();
    }

    public boolean zzE(Intent intent) {
        this.zzclt = Log.isLoggable("FirebaseInstanceId", 3);
        if (intent.getStringExtra("error") == null && intent.getStringExtra("registration_id") == null) {
            return false;
        }
        String zzG = zzG(intent);
        if (this.zzclt) {
            String valueOf = String.valueOf(zzG);
            Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Register result in service ".concat(valueOf) : new String("Register result in service "));
        }
        zzjw(zzG).zzabT().zzs(intent);
        return true;
    }

    public void zzF(Intent intent) {
        String zzG = zzG(intent);
        zzd zzjw = zzjw(zzG);
        String stringExtra = intent.getStringExtra("CMD");
        if (this.zzclt) {
            String valueOf = String.valueOf(intent.getExtras());
            Log.d("FirebaseInstanceId", new StringBuilder(String.valueOf(zzG).length() + 18 + String.valueOf(stringExtra).length() + String.valueOf(valueOf).length()).append("Service command ").append(zzG).append(" ").append(stringExtra).append(" ").append(valueOf).toString());
        }
        if (intent.getStringExtra("unregistered") != null) {
            zzh zzabS = zzjw.zzabS();
            if (zzG == null) {
                zzG = "";
            }
            zzabS.zzeK(zzG);
            zzjw.zzabT().zzs(intent);
        } else if ("gcm.googleapis.com/refresh".equals(intent.getStringExtra("from"))) {
            zzjw.zzabS().zzeK(zzG);
            zza(intent, false, true);
        } else if ("RST".equals(stringExtra)) {
            zzjw.zzHi();
            zza(intent, true, true);
        } else if ("RST_FULL".equals(stringExtra)) {
            if (!zzjw.zzabS().isEmpty()) {
                zzjw.zzHi();
                zzjw.zzabS().zzHo();
                zza(intent, true, true);
            }
        } else if ("SYNC".equals(stringExtra)) {
            zzjw.zzabS().zzeK(zzG);
            zza(intent, false, true);
        } else if ("PING".equals(stringExtra)) {
            zzU(intent.getExtras());
        }
    }
}
