package com.google.firebase;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.common.internal.zzaa;
import com.google.android.gms.common.internal.zzac;
import com.google.android.gms.common.internal.zzam;
import com.google.android.gms.common.util.zzw;

public final class FirebaseOptions {
    /* access modifiers changed from: private */
    public final String zzamX;
    /* access modifiers changed from: private */
    public final String zzbWQ;
    /* access modifiers changed from: private */
    public final String zzbWR;
    /* access modifiers changed from: private */
    public final String zzbWS;
    /* access modifiers changed from: private */
    public final String zzbWT;
    /* access modifiers changed from: private */
    public final String zzbWU;

    public static final class Builder {
        private String zzamX;
        private String zzbWQ;
        private String zzbWR;
        private String zzbWS;
        private String zzbWT;
        private String zzbWU;

        public Builder() {
        }

        public Builder(FirebaseOptions firebaseOptions) {
            this.zzamX = firebaseOptions.zzamX;
            this.zzbWQ = firebaseOptions.zzbWQ;
            this.zzbWR = firebaseOptions.zzbWR;
            this.zzbWS = firebaseOptions.zzbWS;
            this.zzbWT = firebaseOptions.zzbWT;
            this.zzbWU = firebaseOptions.zzbWU;
        }

        public FirebaseOptions build() {
            return new FirebaseOptions(this.zzamX, this.zzbWQ, this.zzbWR, this.zzbWS, this.zzbWT, this.zzbWU);
        }

        public Builder setApiKey(String str) {
            this.zzbWQ = zzac.zzh(str, "ApiKey must be set.");
            return this;
        }

        public Builder setApplicationId(String str) {
            this.zzamX = zzac.zzh(str, "ApplicationId must be set.");
            return this;
        }

        public Builder setDatabaseUrl(String str) {
            this.zzbWR = str;
            return this;
        }

        public Builder setGcmSenderId(String str) {
            this.zzbWT = str;
            return this;
        }

        public Builder setStorageBucket(String str) {
            this.zzbWU = str;
            return this;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.zzac.zza(int, java.lang.Object):int
      com.google.android.gms.common.internal.zzac.zza(long, java.lang.Object):long
      com.google.android.gms.common.internal.zzac.zza(boolean, java.lang.Object):void */
    private FirebaseOptions(String str, String str2, String str3, String str4, String str5, String str6) {
        zzac.zza(!zzw.zzdz(str), (Object) "ApplicationId must be set.");
        this.zzamX = str;
        this.zzbWQ = str2;
        this.zzbWR = str3;
        this.zzbWS = str4;
        this.zzbWT = str5;
        this.zzbWU = str6;
    }

    public static FirebaseOptions fromResource(Context context) {
        zzam zzam = new zzam(context);
        String string = zzam.getString("google_app_id");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return new FirebaseOptions(string, zzam.getString("google_api_key"), zzam.getString("firebase_database_url"), zzam.getString("ga_trackingId"), zzam.getString("gcm_defaultSenderId"), zzam.getString("google_storage_bucket"));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FirebaseOptions)) {
            return false;
        }
        FirebaseOptions firebaseOptions = (FirebaseOptions) obj;
        return zzaa.equal(this.zzamX, firebaseOptions.zzamX) && zzaa.equal(this.zzbWQ, firebaseOptions.zzbWQ) && zzaa.equal(this.zzbWR, firebaseOptions.zzbWR) && zzaa.equal(this.zzbWS, firebaseOptions.zzbWS) && zzaa.equal(this.zzbWT, firebaseOptions.zzbWT) && zzaa.equal(this.zzbWU, firebaseOptions.zzbWU);
    }

    public String getApiKey() {
        return this.zzbWQ;
    }

    public String getApplicationId() {
        return this.zzamX;
    }

    public String getDatabaseUrl() {
        return this.zzbWR;
    }

    public String getGcmSenderId() {
        return this.zzbWT;
    }

    public String getStorageBucket() {
        return this.zzbWU;
    }

    public int hashCode() {
        return zzaa.hashCode(this.zzamX, this.zzbWQ, this.zzbWR, this.zzbWS, this.zzbWT, this.zzbWU);
    }

    public String toString() {
        return zzaa.zzv(this).zzg("applicationId", this.zzamX).zzg("apiKey", this.zzbWQ).zzg("databaseUrl", this.zzbWR).zzg("gcmSenderId", this.zzbWT).zzg("storageBucket", this.zzbWU).toString();
    }
}
