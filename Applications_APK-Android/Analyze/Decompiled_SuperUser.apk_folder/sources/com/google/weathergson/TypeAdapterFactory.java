package com.google.weathergson;

import com.google.weathergson.reflect.TypeToken;

public interface TypeAdapterFactory {
    TypeAdapter create(Gson gson, TypeToken typeToken);
}
