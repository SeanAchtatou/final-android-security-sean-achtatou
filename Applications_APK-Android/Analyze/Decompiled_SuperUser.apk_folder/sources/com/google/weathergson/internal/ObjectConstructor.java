package com.google.weathergson.internal;

public interface ObjectConstructor {
    Object construct();
}
