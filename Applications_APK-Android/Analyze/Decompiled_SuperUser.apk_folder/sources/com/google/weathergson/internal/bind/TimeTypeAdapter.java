package com.google.weathergson.internal.bind;

import com.google.weathergson.Gson;
import com.google.weathergson.JsonSyntaxException;
import com.google.weathergson.TypeAdapter;
import com.google.weathergson.TypeAdapterFactory;
import com.google.weathergson.reflect.TypeToken;
import com.google.weathergson.stream.JsonReader;
import com.google.weathergson.stream.JsonToken;
import com.google.weathergson.stream.JsonWriter;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class TimeTypeAdapter extends TypeAdapter {
    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        public TypeAdapter create(Gson gson, TypeToken typeToken) {
            if (typeToken.getRawType() == Time.class) {
                return new TimeTypeAdapter();
            }
            return null;
        }
    };
    private final DateFormat format = new SimpleDateFormat("hh:mm:ss a");

    public synchronized Time read(JsonReader jsonReader) {
        Time time;
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            time = null;
        } else {
            try {
                time = new Time(this.format.parse(jsonReader.nextString()).getTime());
            } catch (ParseException e2) {
                throw new JsonSyntaxException(e2);
            }
        }
        return time;
    }

    public synchronized void write(JsonWriter jsonWriter, Time time) {
        jsonWriter.value(time == null ? null : this.format.format(time));
    }
}
