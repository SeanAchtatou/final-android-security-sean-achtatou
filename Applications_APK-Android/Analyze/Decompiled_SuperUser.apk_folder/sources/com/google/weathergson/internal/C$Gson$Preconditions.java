package com.google.weathergson.internal;

/* renamed from: com.google.weathergson.internal.$Gson$Preconditions  reason: invalid class name */
public final class C$Gson$Preconditions {
    public static Object checkNotNull(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException();
    }

    public static void checkArgument(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }
}
