package com.google.weathergson.internal.bind;

import com.google.weathergson.FieldNamingStrategy;
import com.google.weathergson.Gson;
import com.google.weathergson.JsonSyntaxException;
import com.google.weathergson.TypeAdapter;
import com.google.weathergson.TypeAdapterFactory;
import com.google.weathergson.annotations.JsonAdapter;
import com.google.weathergson.annotations.SerializedName;
import com.google.weathergson.internal.C$Gson$Types;
import com.google.weathergson.internal.ConstructorConstructor;
import com.google.weathergson.internal.Excluder;
import com.google.weathergson.internal.ObjectConstructor;
import com.google.weathergson.internal.Primitives;
import com.google.weathergson.reflect.TypeToken;
import com.google.weathergson.stream.JsonReader;
import com.google.weathergson.stream.JsonToken;
import com.google.weathergson.stream.JsonWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ReflectiveTypeAdapterFactory implements TypeAdapterFactory {
    private final ConstructorConstructor constructorConstructor;
    private final Excluder excluder;
    private final FieldNamingStrategy fieldNamingPolicy;

    public ReflectiveTypeAdapterFactory(ConstructorConstructor constructorConstructor2, FieldNamingStrategy fieldNamingStrategy, Excluder excluder2) {
        this.constructorConstructor = constructorConstructor2;
        this.fieldNamingPolicy = fieldNamingStrategy;
        this.excluder = excluder2;
    }

    public boolean excludeField(Field field, boolean z) {
        return excludeField(field, z, this.excluder);
    }

    static boolean excludeField(Field field, boolean z, Excluder excluder2) {
        return !excluder2.excludeClass(field.getType(), z) && !excluder2.excludeField(field, z);
    }

    private String getFieldName(Field field) {
        return getFieldName(this.fieldNamingPolicy, field);
    }

    static String getFieldName(FieldNamingStrategy fieldNamingStrategy, Field field) {
        SerializedName serializedName = (SerializedName) field.getAnnotation(SerializedName.class);
        return serializedName == null ? fieldNamingStrategy.translateName(field) : serializedName.value();
    }

    public TypeAdapter create(Gson gson, TypeToken typeToken) {
        Class rawType = typeToken.getRawType();
        if (!Object.class.isAssignableFrom(rawType)) {
            return null;
        }
        return new Adapter(this.constructorConstructor.get(typeToken), getBoundFields(gson, typeToken, rawType));
    }

    private BoundField createBoundField(Gson gson, Field field, String str, TypeToken typeToken, boolean z, boolean z2) {
        final boolean isPrimitive = Primitives.isPrimitive(typeToken.getRawType());
        final Gson gson2 = gson;
        final Field field2 = field;
        final TypeToken typeToken2 = typeToken;
        return new BoundField(str, z, z2) {
            final TypeAdapter typeAdapter = ReflectiveTypeAdapterFactory.this.getFieldAdapter(gson2, field2, typeToken2);

            /* access modifiers changed from: package-private */
            public void write(JsonWriter jsonWriter, Object obj) {
                new TypeAdapterRuntimeTypeWrapper(gson2, this.typeAdapter, typeToken2.getType()).write(jsonWriter, field2.get(obj));
            }

            /* access modifiers changed from: package-private */
            public void read(JsonReader jsonReader, Object obj) {
                Object read = this.typeAdapter.read(jsonReader);
                if (read != null || !isPrimitive) {
                    field2.set(obj, read);
                }
            }

            public boolean writeField(Object obj) {
                if (this.serialized && field2.get(obj) != obj) {
                    return true;
                }
                return false;
            }
        };
    }

    /* access modifiers changed from: private */
    public TypeAdapter getFieldAdapter(Gson gson, Field field, TypeToken typeToken) {
        TypeAdapter typeAdapter;
        JsonAdapter jsonAdapter = (JsonAdapter) field.getAnnotation(JsonAdapter.class);
        return (jsonAdapter == null || (typeAdapter = JsonAdapterAnnotationTypeAdapterFactory.getTypeAdapter(this.constructorConstructor, gson, typeToken, jsonAdapter)) == null) ? gson.getAdapter(typeToken) : typeAdapter;
    }

    private Map getBoundFields(Gson gson, TypeToken typeToken, Class cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type type = typeToken.getType();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean excludeField = excludeField(field, true);
                boolean excludeField2 = excludeField(field, false);
                if (excludeField || excludeField2) {
                    field.setAccessible(true);
                    BoundField createBoundField = createBoundField(gson, field, getFieldName(field), TypeToken.get(C$Gson$Types.resolve(typeToken.getType(), cls, field.getGenericType())), excludeField, excludeField2);
                    BoundField boundField = (BoundField) linkedHashMap.put(createBoundField.name, createBoundField);
                    if (boundField != null) {
                        throw new IllegalArgumentException(type + " declares multiple JSON fields named " + boundField.name);
                    }
                }
            }
            typeToken = TypeToken.get(C$Gson$Types.resolve(typeToken.getType(), cls, cls.getGenericSuperclass()));
            cls = typeToken.getRawType();
        }
        return linkedHashMap;
    }

    static abstract class BoundField {
        final boolean deserialized;
        final String name;
        final boolean serialized;

        /* access modifiers changed from: package-private */
        public abstract void read(JsonReader jsonReader, Object obj);

        /* access modifiers changed from: package-private */
        public abstract void write(JsonWriter jsonWriter, Object obj);

        /* access modifiers changed from: package-private */
        public abstract boolean writeField(Object obj);

        protected BoundField(String str, boolean z, boolean z2) {
            this.name = str;
            this.serialized = z;
            this.deserialized = z2;
        }
    }

    public static final class Adapter extends TypeAdapter {
        private final Map boundFields;
        private final ObjectConstructor constructor;

        private Adapter(ObjectConstructor objectConstructor, Map map) {
            this.constructor = objectConstructor;
            this.boundFields = map;
        }

        public Object read(JsonReader jsonReader) {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            Object construct = this.constructor.construct();
            try {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    BoundField boundField = (BoundField) this.boundFields.get(jsonReader.nextName());
                    if (boundField == null || !boundField.deserialized) {
                        jsonReader.skipValue();
                    } else {
                        boundField.read(jsonReader, construct);
                    }
                }
                jsonReader.endObject();
                return construct;
            } catch (IllegalStateException e2) {
                throw new JsonSyntaxException(e2);
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }

        public void write(JsonWriter jsonWriter, Object obj) {
            if (obj == null) {
                jsonWriter.nullValue();
                return;
            }
            jsonWriter.beginObject();
            try {
                for (BoundField boundField : this.boundFields.values()) {
                    if (boundField.writeField(obj)) {
                        jsonWriter.name(boundField.name);
                        boundField.write(jsonWriter, obj);
                    }
                }
                jsonWriter.endObject();
            } catch (IllegalAccessException e2) {
                throw new AssertionError();
            }
        }
    }
}
