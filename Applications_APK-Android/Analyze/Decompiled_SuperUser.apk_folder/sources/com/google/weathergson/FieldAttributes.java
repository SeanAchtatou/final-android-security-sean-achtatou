package com.google.weathergson;

import com.google.weathergson.internal.C$Gson$Preconditions;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public final class FieldAttributes {
    private final Field field;

    public FieldAttributes(Field field2) {
        C$Gson$Preconditions.checkNotNull(field2);
        this.field = field2;
    }

    public Class getDeclaringClass() {
        return this.field.getDeclaringClass();
    }

    public String getName() {
        return this.field.getName();
    }

    public Type getDeclaredType() {
        return this.field.getGenericType();
    }

    public Class getDeclaredClass() {
        return this.field.getType();
    }

    public Annotation getAnnotation(Class cls) {
        return this.field.getAnnotation(cls);
    }

    public Collection getAnnotations() {
        return Arrays.asList(this.field.getAnnotations());
    }

    public boolean hasModifier(int i) {
        return (this.field.getModifiers() & i) != 0;
    }

    /* access modifiers changed from: package-private */
    public Object get(Object obj) {
        return this.field.get(obj);
    }

    /* access modifiers changed from: package-private */
    public boolean isSynthetic() {
        return this.field.isSynthetic();
    }
}
