package com.google.weathergson;

import java.lang.reflect.Type;

public interface InstanceCreator {
    Object createInstance(Type type);
}
