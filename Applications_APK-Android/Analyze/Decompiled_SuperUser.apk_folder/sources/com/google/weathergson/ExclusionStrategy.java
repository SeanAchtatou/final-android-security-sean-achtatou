package com.google.weathergson;

public interface ExclusionStrategy {
    boolean shouldSkipClass(Class cls);

    boolean shouldSkipField(FieldAttributes fieldAttributes);
}
