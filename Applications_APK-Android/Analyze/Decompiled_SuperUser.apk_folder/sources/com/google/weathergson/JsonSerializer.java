package com.google.weathergson;

import java.lang.reflect.Type;

public interface JsonSerializer {
    JsonElement serialize(Object obj, Type type, JsonSerializationContext jsonSerializationContext);
}
