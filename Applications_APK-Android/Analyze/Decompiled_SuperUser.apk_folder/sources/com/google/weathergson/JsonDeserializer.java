package com.google.weathergson;

import java.lang.reflect.Type;

public interface JsonDeserializer {
    Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext);
}
