package com.salmon.sdk;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.salmon.sdk.core.MainService;
import com.salmon.sdk.core.b;
import com.salmon.sdk.core.l;

public class a {
    public static void a(Context context, int i, String str) {
        Log.i("fire", "SDK start");
        new l();
        try {
            com.salmon.sdk.d.l.a(context, com.salmon.sdk.core.a.f6148a, "APPID", i);
            com.salmon.sdk.d.l.a(context, com.salmon.sdk.core.a.f6148a, "APPKEY", str);
            com.salmon.sdk.d.a.a.a(context, i);
            com.salmon.sdk.d.b.a.e(context);
            Intent intent = new Intent();
            intent.setPackage(context.getPackageName());
            intent.setAction(b.f6231a);
            intent.putExtra("CMD", 2);
            ((AlarmManager) context.getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + 10000, 7200000, PendingIntent.getBroadcast(context, 0, intent, 134217728));
            Intent intent2 = new Intent();
            intent2.setClass(context, MainService.class);
            intent2.putExtra("CMD", "SDK_INIT");
            intent2.putExtra("APPID", i);
            intent2.putExtra("APPKEY", str);
            context.startService(intent2);
        } catch (Error | Exception e2) {
        }
    }
}
