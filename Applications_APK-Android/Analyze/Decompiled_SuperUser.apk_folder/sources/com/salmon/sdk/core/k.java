package com.salmon.sdk.core;

import java.util.Map;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static long f6261a = 0;

    /* renamed from: b  reason: collision with root package name */
    private static Map<String, String> f6262b = null;

    public static long a() {
        return f6261a;
    }

    public static synchronized void a(long j) {
        synchronized (k.class) {
            f6261a = j;
        }
    }
}
