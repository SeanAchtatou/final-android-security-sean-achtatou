package com.salmon.sdk.core.a;

import com.salmon.sdk.b.g;
import com.salmon.sdk.c.k;
import com.salmon.sdk.core.a;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;

final class b implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f6164a;

    b(a aVar) {
        this.f6164a = aVar;
    }

    public final void a() {
        i.c("AppSettingManager", "OnLoadStart()");
    }

    public final void a(Object obj) {
        i.c("AppSettingManager", "OnLoadFinish()");
        boolean unused = this.f6164a.f6163h = false;
        if (obj != null) {
            g gVar = (g) obj;
            l.a(this.f6164a.f6158c, a.f6148a, "APPSETTING_UPDATE", System.currentTimeMillis());
            l.a(this.f6164a.f6158c, a.f6148a, "APPSETTING_JSON", gVar.a());
            g a2 = a.b(gVar.a());
            if (a2 != null) {
                g unused2 = this.f6164a.f6161f = a2;
            }
        }
    }

    public final void b() {
        i.c("AppSettingManager", "OnLoadError()");
        boolean unused = this.f6164a.f6163h = false;
        a.c(this.f6164a);
        this.f6164a.c();
    }

    public final void c() {
        i.c("AppSettingManager", "OnLoadCanceled()");
        boolean unused = this.f6164a.f6163h = false;
        a.c(this.f6164a);
        this.f6164a.c();
    }
}
