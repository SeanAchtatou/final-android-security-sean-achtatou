package com.salmon.sdk.core.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.salmon.sdk.b.d;
import com.salmon.sdk.d.c.a;
import com.salmon.sdk.d.c.c;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {

    /* renamed from: b  reason: collision with root package name */
    public static List<String> f6178b = new CopyOnWriteArrayList();

    /* renamed from: d  reason: collision with root package name */
    private static h f6179d = null;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public static CopyOnWriteArraySet<d> f6180f = new CopyOnWriteArraySet<>();

    /* renamed from: a  reason: collision with root package name */
    int f6181a;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f6182c;

    /* renamed from: e  reason: collision with root package name */
    private SharedPreferences f6183e;

    private h(Context context, int i) {
        this.f6182c = context;
        this.f6181a = i;
    }

    public static h a(Context context, int i) {
        if (f6179d == null) {
            synchronized (h.class) {
                if (f6179d == null) {
                    f6179d = new h(context, i);
                }
            }
        }
        return f6179d;
    }

    private void a(Set<d> set) {
        if (this.f6182c != null) {
            if (set == null || set.size() <= 0) {
                try {
                    d.a(set);
                    this.f6183e = this.f6182c.getSharedPreferences("installed", 0);
                    SharedPreferences.Editor edit = this.f6183e.edit();
                    edit.remove("_installed");
                    edit.commit();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                try {
                    String a2 = d.a(set);
                    this.f6183e = this.f6182c.getSharedPreferences("installed", 0);
                    SharedPreferences.Editor edit2 = this.f6183e.edit();
                    edit2.putString("_installed", a2);
                    edit2.commit();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
    }

    public static boolean a(String str) {
        if (f6178b != null) {
            if (f6178b.contains(str)) {
                return true;
            }
            if (f6180f != null && f6180f.size() > 0) {
                Iterator<d> it = f6180f.iterator();
                while (it.hasNext()) {
                    if (it.next().a().equalsIgnoreCase(str)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static CopyOnWriteArraySet<d> d() {
        if (f6180f == null) {
            f6180f = new CopyOnWriteArraySet<>();
        }
        return f6180f;
    }

    public final void a() {
        a.a().a((c) new i(this));
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void b() {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 == 0) goto L_0x000d
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 > 0) goto L_0x0013
        L_0x000d:
            java.util.concurrent.CopyOnWriteArraySet r0 = r10.e()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            com.salmon.sdk.core.a.h.f6180f = r0     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
        L_0x0013:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 == 0) goto L_0x001f
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 != 0) goto L_0x0021
        L_0x001f:
            monitor-exit(r10)
            return
        L_0x0021:
            java.util.concurrent.CopyOnWriteArraySet r3 = new java.util.concurrent.CopyOnWriteArraySet     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            r3.<init>()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
        L_0x002c:
            boolean r0 = r4.hasNext()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 == 0) goto L_0x007c
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x006e }
            com.salmon.sdk.b.d r0 = (com.salmon.sdk.b.d) r0     // Catch:{ Exception -> 0x006e }
            java.util.List<java.lang.String> r1 = com.salmon.sdk.core.a.h.f6178b     // Catch:{ Exception -> 0x006e }
            if (r1 == 0) goto L_0x002c
            java.util.List<java.lang.String> r1 = com.salmon.sdk.core.a.h.f6178b     // Catch:{ Exception -> 0x006e }
            int r1 = r1.size()     // Catch:{ Exception -> 0x006e }
            if (r1 <= 0) goto L_0x002c
            r1 = 0
            r2 = r1
        L_0x0046:
            java.util.List<java.lang.String> r1 = com.salmon.sdk.core.a.h.f6178b     // Catch:{ Exception -> 0x006e }
            int r1 = r1.size()     // Catch:{ Exception -> 0x006e }
            if (r2 >= r1) goto L_0x002c
            java.util.List<java.lang.String> r1 = com.salmon.sdk.core.a.h.f6178b     // Catch:{ Exception -> 0x006e }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x006e }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x006e }
            java.lang.String r5 = r0.a()     // Catch:{ Exception -> 0x006e }
            boolean r1 = r1.equals(r5)     // Catch:{ Exception -> 0x006e }
            if (r1 == 0) goto L_0x006a
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x006e }
            r0.a(r6)     // Catch:{ Exception -> 0x006e }
            r3.add(r0)     // Catch:{ Exception -> 0x006e }
        L_0x006a:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0046
        L_0x006e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            java.lang.String r0 = "InstallAppManager"
            java.lang.String r1 = "remove list error"
            com.salmon.sdk.d.i.e(r0, r1)     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            goto L_0x002c
        L_0x007a:
            r0 = move-exception
            goto L_0x001f
        L_0x007c:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 == 0) goto L_0x00af
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
        L_0x0086:
            boolean r0 = r1.hasNext()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 == 0) goto L_0x00af
            java.lang.Object r0 = r1.next()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            com.salmon.sdk.b.d r0 = (com.salmon.sdk.b.d) r0     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            boolean r2 = r3.contains(r0)     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r2 != 0) goto L_0x0086
            long r4 = r0.b()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            r8 = 432000000(0x19bfcc00, double:2.13436359E-315)
            long r6 = r6 - r8
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x0086
            r3.add(r0)     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            goto L_0x0086
        L_0x00ac:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x00af:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 == 0) goto L_0x00b8
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            r0.clear()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
        L_0x00b8:
            int r0 = r3.size()     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            if (r0 <= 0) goto L_0x00c3
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            r0.addAll(r3)     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
        L_0x00c3:
            java.util.concurrent.CopyOnWriteArraySet<com.salmon.sdk.b.d> r0 = com.salmon.sdk.core.a.h.f6180f     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            r10.a(r0)     // Catch:{ Throwable -> 0x007a, all -> 0x00ac }
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.salmon.sdk.core.a.h.b():void");
    }

    public final void c() {
        try {
            if (f6180f != null && f6180f.size() > 0) {
                a((Set<d>) f6180f);
            }
        } catch (Throwable th) {
        }
    }

    public final CopyOnWriteArraySet<d> e() {
        if (this.f6182c == null) {
            return null;
        }
        this.f6183e = this.f6182c.getSharedPreferences("installed", 0);
        CopyOnWriteArraySet<d> copyOnWriteArraySet = new CopyOnWriteArraySet<>();
        String string = this.f6183e.getString("_installed", "");
        if (TextUtils.isEmpty(string)) {
            return copyOnWriteArraySet;
        }
        try {
            JSONArray jSONArray = new JSONArray(string);
            for (int i = 0; i < jSONArray.length(); i++) {
                d dVar = new d();
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                dVar.a(jSONObject.optString("campaignId"));
                dVar.b(jSONObject.optString("packageName"));
                dVar.a(jSONObject.optLong("updateTime"));
                copyOnWriteArraySet.add(dVar);
            }
            return copyOnWriteArraySet;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return copyOnWriteArraySet;
        }
    }
}
