package com.salmon.sdk.core.a;

import com.salmon.sdk.a.b;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.a;
import com.salmon.sdk.b.c;
import com.salmon.sdk.c.k;
import com.salmon.sdk.d.i;
import java.util.Iterator;
import java.util.List;

final class t implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f6219a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f6220b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ int f6221c;

    /* renamed from: d  reason: collision with root package name */
    final /* synthetic */ boolean f6222d;

    /* renamed from: e  reason: collision with root package name */
    final /* synthetic */ j f6223e;

    t(j jVar, boolean z, String str, int i, boolean z2) {
        this.f6223e = jVar;
        this.f6219a = z;
        this.f6220b = str;
        this.f6221c = i;
        this.f6222d = z2;
    }

    public final void a() {
        if (this.f6219a) {
            i.b("rush", "download reload ad start");
        } else {
            i.b("rush", "install reload ad start");
        }
    }

    public final void a(Object obj) {
        if (this.f6219a) {
            i.b("rush", "download reload OnLoadFinish");
        } else {
            i.b("rush", "install reload OnLoadFinish");
        }
        try {
            a aVar = (a) obj;
            if (aVar != null && aVar.b() != null && aVar.b().size() > 0) {
                List<c> b2 = aVar.b();
                if (this.f6223e.i.w == 1) {
                    for (String next : b.a(g.a(this.f6223e.f6192g)).b(this.f6220b)) {
                        Iterator<c> it = b2.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            c next2 = it.next();
                            if (next.equals(String.valueOf(next2.a()))) {
                                b2.remove(next2);
                                break;
                            }
                        }
                    }
                }
                if (b2.size() != 0) {
                    if (this.f6219a) {
                        i.b("rush", "donwload loaded onreclick");
                    } else {
                        i.b("rush", "install loaded onreclick");
                    }
                    if (b2.size() > this.f6221c) {
                        for (int size = b2.size() - 1; size >= this.f6221c; size--) {
                            b2.remove(size);
                        }
                    }
                    if (b2.size() != 0) {
                        this.f6223e.a(b2, this.f6222d, this.f6219a, true, 0, false);
                    }
                }
            }
        } catch (Exception e2) {
        }
    }

    public final void b() {
        i.b("rush", "OnLoadError");
    }

    public final void c() {
    }
}
