package com.salmon.sdk.core.a;

import android.os.Build;
import com.salmon.sdk.core.MainService;
import com.salmon.sdk.d.a.a;
import com.salmon.sdk.d.k;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f6214a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f6215b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ String[] f6216c;

    /* renamed from: d  reason: collision with root package name */
    final /* synthetic */ j f6217d;

    r(j jVar, String str, String str2, String[] strArr) {
        this.f6217d = jVar;
        this.f6214a = str;
        this.f6215b = str2;
        this.f6216c = strArr;
    }

    public final void run() {
        this.f6217d.f6190d = false;
        this.f6217d.f6191e = 0;
        while (this.f6217d.f6191e < 50) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            if (this.f6217d.f6190d) {
                break;
            }
            if (Build.VERSION.SDK_INT < 21) {
                MainService.a(new s(this));
            } else if (this.f6217d.f6191e % 10 == 0) {
                k.a(this.f6217d.f6192g, this.f6214a, this.f6215b);
            }
            this.f6217d.f6191e++;
        }
        if (!this.f6217d.f6190d) {
            k.a(this.f6217d.f6192g, this.f6214a, this.f6215b);
            if (Build.VERSION.SDK_INT < 21) {
                for (String str : this.f6216c) {
                    a.a(1004312, "session_id=" + String.valueOf(a.b(String.valueOf(str))) + "&campaign_id=" + str + "&type=rush&pkg=" + this.f6214a);
                    a.c(String.valueOf(str));
                }
            }
        }
    }
}
