package com.salmon.sdk.core.a;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.salmon.sdk.b.g;
import com.salmon.sdk.d.h;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a {

    /* renamed from: b  reason: collision with root package name */
    private static a f6156b;

    /* renamed from: a  reason: collision with root package name */
    private final int f6157a = 3;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Context f6158c;

    /* renamed from: d  reason: collision with root package name */
    private int f6159d;

    /* renamed from: e  reason: collision with root package name */
    private String f6160e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public g f6161f;

    /* renamed from: g  reason: collision with root package name */
    private int f6162g = 0;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public boolean f6163h = false;
    private g i;

    private a() {
    }

    private a(Context context) {
        this.f6158c = context;
        this.f6159d = l.b(context, com.salmon.sdk.core.a.f6148a, "APPID", Integer.parseInt(com.salmon.sdk.core.a.j));
        this.f6160e = l.b(context, com.salmon.sdk.core.a.f6148a, "APPKEY", com.salmon.sdk.core.a.k);
    }

    public static a a(Context context) {
        if (f6156b == null) {
            f6156b = new a(context);
        }
        f6156b.f6158c = context;
        return f6156b;
    }

    public static String a(Context context, String str, g gVar) {
        if (gVar != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    String host = Uri.parse(str).getHost();
                    for (Map.Entry<String, String> key : gVar.b().entrySet()) {
                        String str2 = (String) key.getKey();
                        if (host.contains(str2)) {
                            String str3 = gVar.b().get(str2);
                            if (TextUtils.isEmpty(str3)) {
                                return "";
                            }
                            String replace = str3.replace("{gaid}", h.p(context));
                            return replace.contains("{android_id}") ? replace.replace("{android_id}", h.d(context)) : replace.contains("{android_id_md5_upper}") ? replace.replace("{android_id_md5_upper}", h.e(context)) : replace;
                        }
                    }
                    return "";
                }
            } catch (Throwable th) {
                return "";
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static g b(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            g gVar = new g();
            JSONArray optJSONArray = jSONObject.optJSONArray("jt");
            HashMap hashMap = new HashMap();
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i2);
                hashMap.put(optJSONObject.optString("domain"), optJSONObject.optString("format"));
            }
            gVar.a(hashMap);
            return gVar;
        } catch (Exception e2) {
            return null;
        }
    }

    static /* synthetic */ int c(a aVar) {
        int i2 = aVar.f6162g;
        aVar.f6162g = i2 + 1;
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    /* access modifiers changed from: private */
    public void c() {
        i.c("AppSettingManager", "updateAppSetting()");
        if (!this.f6163h) {
            if (this.f6162g >= 3) {
                i.c("AppSettingManager", "retrytimes >= MAX_RETRY");
                return;
            }
            try {
                if (System.currentTimeMillis() - l.a(this.f6158c, com.salmon.sdk.core.a.f6148a, "APPSETTING_LOAD_TIME", (Long) 0L).longValue() < 300000) {
                    i.c("AppSettingManager", "System.currentTimeMillis() - lastRequestTime < 5 * 60 * 1000");
                    return;
                }
                this.f6163h = true;
                l.a(this.f6158c, com.salmon.sdk.core.a.f6148a, "APPSETTING_LOAD_TIME", System.currentTimeMillis());
                new com.salmon.sdk.c.h(this.f6158c, this.f6159d, this.f6160e).a(new b(this));
            } catch (Exception e2) {
                e2.printStackTrace();
                this.f6163h = false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    public final void a() {
        try {
            long longValue = l.a(this.f6158c, com.salmon.sdk.core.a.f6148a, "APPSETTING_UPDATE", (Long) 0L).longValue();
            i.c("AppSettingManager", "lastUpdate = " + longValue);
            if (System.currentTimeMillis() - longValue > 86400000) {
                i.c("AppSettingManager", "update appsetting runtime = 86400000");
                this.f6162g = 0;
                c();
            }
        } catch (Error | Exception e2) {
        }
    }

    public final g b() {
        i.c("AppSettingManager", "getAPPSetting()");
        if (this.i == null) {
            String b2 = l.b(this.f6158c, com.salmon.sdk.core.a.f6148a, "APPSETTING_JSON", (String) null);
            if (b2 == null) {
                i.c("AppSettingManager", "initDefut()");
                g gVar = new g();
                HashMap hashMap = new HashMap();
                hashMap.put("app.appsflyer.com", "&android_id={android_id}&advertising_id={gaid}");
                hashMap.put("app.adjust.com", "&android_id={android_id}&gps_adid={gaid}");
                hashMap.put("app.adjust.io", "&android_id={android_id}&gps_adid={gaid}");
                hashMap.put("control.kochava.com", "&device_id={android_id}&append_app_conv_trk_params=1");
                hashMap.put("url.haloapps.com", "&d={android_id}");
                hashMap.put("ad.apsalar.com", "&andi={android_id}");
                hashMap.put("td.lenzmx.com", "&mb_devid={android_id}");
                hashMap.put("cd.lenzmx.com", "&mb_devid={android_id}");
                hashMap.put("rd.lenzmx.com", "&mb_devid={android_id}");
                hashMap.put("tracking.lenzmx.com", "&mb_devid={android_id}");
                gVar.a(hashMap);
                this.f6161f = gVar;
            } else {
                g b3 = b(b2);
                if (b3 != null) {
                    this.f6161f = b3;
                }
            }
        }
        return this.f6161f;
    }
}
