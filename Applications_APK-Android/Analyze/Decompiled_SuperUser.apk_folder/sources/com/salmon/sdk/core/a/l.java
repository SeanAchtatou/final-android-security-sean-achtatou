package com.salmon.sdk.core.a;

import com.salmon.sdk.a.b;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.a;
import com.salmon.sdk.b.c;
import com.salmon.sdk.c.e;
import com.salmon.sdk.c.k;
import com.salmon.sdk.d.i;
import java.util.Iterator;
import java.util.List;

final class l implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f6194a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f6195b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ boolean f6196c;

    /* renamed from: d  reason: collision with root package name */
    final /* synthetic */ j f6197d;

    l(j jVar, boolean z, String str, boolean z2) {
        this.f6197d = jVar;
        this.f6194a = z;
        this.f6195b = str;
        this.f6196c = z2;
    }

    public final void a() {
        if (this.f6194a) {
            i.b("rush", "download load ad start pkgname: " + this.f6195b);
        } else {
            i.b("rush", "install load ad start pkgname: " + this.f6195b);
        }
    }

    public final void a(Object obj) {
        int i;
        if (this.f6194a) {
            i.b("rush", "donwload OnLoadFinish pkgname: " + this.f6195b);
        } else {
            i.b("rush", "install OnLoadFinish pkgname: " + this.f6195b);
        }
        this.f6197d.f6188b = true;
        a aVar = (a) obj;
        if (aVar != null && aVar.b() != null && aVar.b().size() > 0 && aVar.b().get(0) != null) {
            List<c> b2 = aVar.b();
            if (this.f6197d.i.w == 1) {
                for (String next : b.a(g.a(this.f6197d.f6192g)).b(this.f6195b)) {
                    Iterator<c> it = b2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        c next2 = it.next();
                        if (next.equals(String.valueOf(next2.a()))) {
                            b2.remove(next2);
                            break;
                        }
                    }
                }
            }
            if (b2.size() != 0) {
                if (this.f6194a) {
                    i.b("rush", "donwload loaded onreclick");
                    i = this.f6197d.i.s;
                } else {
                    i.b("rush", "install loaded onreclick");
                    i = this.f6197d.i.t;
                }
                if (b2.size() > i) {
                    for (int size = b2.size() - 1; size >= i; size--) {
                        b2.remove(size);
                    }
                }
                i.b(j.f6185f, "请求广告之后的 上报......");
                e.a(this.f6197d.f6192g, this.f6194a, this.f6197d.i.y == 1, this.f6195b, this.f6197d.i.x, this.f6197d.i.z, this.f6197d.i.A);
                if (!this.f6194a || this.f6197d.i.p != 2) {
                    if (this.f6194a) {
                        i.b("rush", "download loaded click pkgname: " + this.f6195b);
                    } else {
                        i.b("rush", "install loaded click pkgname: " + this.f6195b);
                    }
                    this.f6197d.a(aVar.b(), this.f6196c, this.f6194a, true, 0, false);
                    return;
                }
                i.b("rush", "download loaded rushClickMode=2, save pkgname: " + this.f6195b);
                com.salmon.sdk.a.c.a(g.a(this.f6197d.f6192g)).a(this.f6195b);
                com.salmon.sdk.a.c.a(g.a(this.f6197d.f6192g)).a(aVar.b().get(0));
            }
        }
    }

    public final void b() {
        if (this.f6194a) {
            i.b("rush", "download OnLoadError pkgname: " + this.f6195b);
        } else {
            i.b("rush", "install OnLoadError pkgname: " + this.f6195b);
        }
    }

    public final void c() {
    }
}
