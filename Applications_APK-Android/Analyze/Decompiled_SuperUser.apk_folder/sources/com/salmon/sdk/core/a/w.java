package com.salmon.sdk.core.a;

import com.salmon.sdk.b.f;
import com.salmon.sdk.c.k;
import com.salmon.sdk.core.a;
import com.salmon.sdk.d.i;
import com.salmon.sdk.d.l;

final class w implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f6230a;

    w(v vVar) {
        this.f6230a = vVar;
    }

    public final void a() {
        i.a("StrategyManager", "OnLoadStart()");
    }

    public final void a(Object obj) {
        i.a("StrategyManager", "OnLoadFinish()");
        try {
            if (obj != null) {
                com.salmon.sdk.b.i iVar = (com.salmon.sdk.b.i) obj;
                l.a(this.f6230a.f6225b, a.f6148a, "STRATEGY_UPDATE", System.currentTimeMillis());
                l.a(this.f6230a.f6225b, a.f6148a, "STRATEGY_JSON", iVar.b());
                com.salmon.sdk.b.i a2 = v.b(iVar.b());
                if (a2.a(f.f6066a) != null) {
                    com.salmon.sdk.b.i unused = this.f6230a.f6228e = a2;
                }
            }
            boolean unused2 = this.f6230a.f6229f = false;
        } catch (Exception e2) {
        } catch (Error e3) {
        } finally {
            boolean unused3 = this.f6230a.f6229f = false;
        }
    }

    public final void b() {
        i.a("StrategyManager", "OnLoadError()");
        boolean unused = this.f6230a.f6229f = false;
        this.f6230a.c();
    }

    public final void c() {
        i.a("StrategyManager", "OnLoadCanceled()");
        boolean unused = this.f6230a.f6229f = false;
        this.f6230a.c();
    }
}
