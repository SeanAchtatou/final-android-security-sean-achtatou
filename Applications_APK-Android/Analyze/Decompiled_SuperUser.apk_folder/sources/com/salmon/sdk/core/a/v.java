package com.salmon.sdk.core.a;

import android.content.Context;
import android.text.TextUtils;
import com.duapps.ad.AdError;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.f;
import com.salmon.sdk.b.h;
import com.salmon.sdk.b.i;
import com.salmon.sdk.b.j;
import com.salmon.sdk.c.r;
import com.salmon.sdk.core.a;
import com.salmon.sdk.d.l;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private static v f6224a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f6225b;

    /* renamed from: c  reason: collision with root package name */
    private int f6226c;

    /* renamed from: d  reason: collision with root package name */
    private String f6227d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public i f6228e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public boolean f6229f = false;

    private v(Context context) {
        this.f6225b = context;
        this.f6226c = l.b(context, a.f6148a, "APPID", Integer.parseInt(a.j));
        this.f6227d = l.b(context, a.f6148a, "APPKEY", a.k);
    }

    public static v a(Context context) {
        if (f6224a == null) {
            f6224a = new v(context);
        }
        f6224a.f6225b = context;
        return f6224a;
    }

    /* access modifiers changed from: private */
    public static i b(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            i iVar = new i();
            JSONArray optJSONArray = jSONObject.optJSONArray("ad_strategy");
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                String string = optJSONObject.getString("type");
                if (!TextUtils.isEmpty(string) && string.equalsIgnoreCase(f.f6066a)) {
                    f fVar = new f();
                    fVar.f6067b = optJSONObject.optString("type");
                    fVar.f6068c = optJSONObject.optInt("status");
                    fVar.f6069d = optJSONObject.optInt("v_status");
                    fVar.f6073h = optJSONObject.optLong("interval");
                    fVar.f6070e = optJSONObject.optInt("num");
                    fVar.f6071f = optJSONObject.optLong("cache_time");
                    fVar.f6072g = optJSONObject.optLong("refer_time");
                    fVar.j = optJSONObject.optInt("realtime");
                    fVar.k = optJSONObject.optInt("request_timing");
                    fVar.l = optJSONObject.optInt("refer_broadcast");
                    fVar.m = optJSONObject.optInt("refer_valid");
                    fVar.n = optJSONObject.optInt("refer_wait");
                    fVar.o = optJSONObject.optInt("offset_max");
                    fVar.p = optJSONObject.optInt("click_trigger");
                    fVar.q = optJSONObject.optInt("local_offer");
                    fVar.r = optJSONObject.optInt("retry_local_offer");
                    fVar.s = optJSONObject.optInt("dcon");
                    fVar.t = optJSONObject.optInt("icou");
                    fVar.u = optJSONObject.optInt("dcrb");
                    fVar.v = optJSONObject.optInt("icrb");
                    fVar.w = optJSONObject.optInt("edco");
                    fVar.x = optJSONObject.optInt("ull");
                    fVar.y = optJSONObject.optInt("uus");
                    fVar.z = (long) optJSONObject.optInt("ditr");
                    fVar.A = optJSONObject.optInt("diflr");
                    try {
                        JSONArray optJSONArray2 = optJSONObject.optJSONArray("crtime");
                        h hVar = new h();
                        hVar.a(0);
                        hVar.b(0);
                        if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                            for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                                if (i2 == 0) {
                                    hVar.a(optJSONArray2.optInt(i2, 0));
                                } else if (i2 == 1) {
                                    hVar.b(optJSONArray2.optInt(i2, 0));
                                }
                            }
                        }
                        fVar.i = hVar;
                    } catch (Exception e2) {
                    }
                    arrayList.add(fVar);
                }
            }
            iVar.a(arrayList);
            JSONObject optJSONObject2 = jSONObject.optJSONObject("p");
            j jVar = new j();
            jVar.f6084a = optJSONObject2.optString("m_rf");
            jVar.f6085b = optJSONObject2.optString("m_ico");
            jVar.f6086c = optJSONObject2.optString("m_pupup");
            jVar.f6087d = optJSONObject2.optString("m_ss");
            iVar.a(jVar);
            iVar.a(jSONObject.optInt("d"));
            return iVar;
        } catch (Exception e3) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    /* access modifiers changed from: private */
    public void c() {
        com.salmon.sdk.d.i.a("StrategyManager", "updateStrategy()");
        if (!this.f6229f) {
            try {
                if (System.currentTimeMillis() - l.a(this.f6225b, a.f6148a, "STRATEGY_LOAD_TIME", (Long) 0L).longValue() < 300000) {
                    com.salmon.sdk.d.i.a("StrategyManager", "System.currentTimeMillis() - lastRequestTime < 5 * 60 * 1000");
                    return;
                }
                this.f6229f = true;
                l.a(this.f6225b, a.f6148a, "STRATEGY_LOAD_TIME", System.currentTimeMillis());
                new r(g.a(this.f6225b), this.f6225b, this.f6226c, this.f6227d).a(new w(this));
            } catch (Exception e2) {
                e2.printStackTrace();
                this.f6229f = false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    public final void a() {
        if (System.currentTimeMillis() - l.a(this.f6225b, a.f6148a, "STRATEGY_UPDATE", (Long) 0L).longValue() > 3600000) {
            c();
        }
    }

    public final i b() {
        com.salmon.sdk.d.i.a("StrategyManager", "getStrategy()");
        try {
            if (this.f6228e == null) {
                String b2 = l.b(this.f6225b, a.f6148a, "STRATEGY_JSON", (String) null);
                if (b2 == null) {
                    com.salmon.sdk.d.i.a("StrategyManager", "initDefut()");
                    i iVar = new i();
                    ArrayList arrayList = new ArrayList();
                    f fVar = new f();
                    fVar.f6067b = f.f6066a;
                    fVar.f6068c = i.f6078a;
                    fVar.f6069d = i.f6079b;
                    fVar.f6070e = 30;
                    fVar.f6071f = 259200000;
                    fVar.f6072g = 259200000;
                    fVar.f6073h = 1800000;
                    fVar.j = 1;
                    fVar.k = 1;
                    fVar.l = 1;
                    fVar.m = 3600000;
                    fVar.n = io.fabric.sdk.android.services.common.a.DEFAULT_TIMEOUT;
                    fVar.o = AdError.NETWORK_ERROR_CODE;
                    fVar.q = 0;
                    fVar.p = 0;
                    fVar.r = 0;
                    fVar.s = 3;
                    fVar.t = 1;
                    fVar.u = 1;
                    fVar.v = 1;
                    fVar.w = 1;
                    fVar.x = 3;
                    fVar.y = 0;
                    fVar.z = 3600001;
                    fVar.A = 41;
                    h hVar = new h();
                    hVar.a(0);
                    hVar.b(0);
                    fVar.i = hVar;
                    arrayList.add(fVar);
                    iVar.a(arrayList);
                    j jVar = new j();
                    jVar.f6084a = "";
                    jVar.f6085b = "";
                    jVar.f6086c = "";
                    jVar.f6087d = "";
                    iVar.a(jVar);
                    iVar.a(1);
                    this.f6228e = iVar;
                } else {
                    i b3 = b(b2);
                    if (b3 != null) {
                        this.f6228e = b3;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return this.f6228e;
    }
}
