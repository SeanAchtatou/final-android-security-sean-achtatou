package com.salmon.sdk.core.a;

import android.content.Context;
import android.text.TextUtils;
import com.salmon.sdk.a.g;
import com.salmon.sdk.b.f;
import com.salmon.sdk.b.i;
import com.salmon.sdk.b.j;
import com.salmon.sdk.c.d;
import com.salmon.sdk.core.a;
import com.salmon.sdk.d.l;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f6165a = c.class.getSimpleName();

    /* renamed from: c  reason: collision with root package name */
    private static c f6166c;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f6167b;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public f f6168d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public boolean f6169e = false;

    private c(Context context) {
        this.f6167b = context;
    }

    public static c a(Context context) {
        if (f6166c == null) {
            synchronized (c.class) {
                if (f6166c == null) {
                    f6166c = new c(context);
                }
            }
        }
        return f6166c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [android.content.Context, java.lang.String, java.lang.String, long]
     candidates:
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, long):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.salmon.sdk.d.l.a(android.content.Context, java.lang.String, java.lang.String, java.lang.Long):java.lang.Long */
    public final void a() {
        try {
            f a2 = v.a(this.f6167b).b().a(f.f6066a);
            if (a2 != null) {
                this.f6168d = a2;
            }
            if (this.f6168d.f6068c != i.f6079b && this.f6168d.k != i.f6079b) {
                if (System.currentTimeMillis() - l.a(this.f6167b, a.f6148a, "CACHE_COMPAIGN_TIME", (Long) 0L).longValue() > this.f6168d.f6073h) {
                    com.salmon.sdk.d.i.c(f6165a, " loadCompaign()");
                    if (!this.f6169e) {
                        this.f6169e = true;
                        d dVar = new d(this.f6167b, g.a(this.f6167b), "splash", com.salmon.sdk.b.a.f6036a);
                        dVar.a(l.b(this.f6167b, a.f6148a, "APPID", Integer.parseInt(a.j)));
                        dVar.c(l.b(this.f6167b, a.f6148a, "APPKEY", a.k));
                        dVar.b(this.f6168d.f6070e);
                        dVar.a("splash");
                        dVar.b("full_screen");
                        j a3 = v.a(this.f6167b).b().a();
                        if (a3 != null && !TextUtils.isEmpty(a3.f6084a)) {
                            dVar.e(a3.f6084a);
                        }
                        dVar.a(new d(this));
                    }
                    l.a(this.f6167b, a.f6148a, "CACHE_COMPAIGN_TIME", System.currentTimeMillis());
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b() {
        try {
            if (this.f6168d != null) {
                com.salmon.sdk.a.c.a(g.a(this.f6167b)).b(System.currentTimeMillis() - this.f6168d.f6071f);
            }
        } catch (Error | Exception e2) {
        }
    }
}
